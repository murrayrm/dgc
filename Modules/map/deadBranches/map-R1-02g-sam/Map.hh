/**********************************************************
 **
 **  MAP.HH
 **
 **    Time-stamp: <2007-05-17 19:06:27 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Fri Feb 16 21:11:33 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef MAP_H
#define MAP_H

#include <math.h>
#include <string>
#include <sstream>
#include <vector>

#include "MapPrior.hh"
#include "MapElement.hh"


class Map
{
  
public:
  
  Map();
  
  ~Map();




  /// Checks if a point label is an entry point
  bool isEntryPoint(const PointLabel &ptlabelin)
  {return prior.isEntryPoint(ptlabelin);}
  /// Checks if a point label is an exit point
  bool isExitPoint(const PointLabel &ptlabelin)
  {return prior.isExitPoint(ptlabelin);}
  /// Checks if a point label is a stop line
  bool isStopLine(const PointLabel &ptlabelin)
  {return prior.isStopLine(ptlabelin);}
  /// Checks if a point label is a check point
  bool isCheckPoint(const PointLabel &ptlabelin)
  {return prior.isCheckPoint(ptlabelin);}
  /// Checks if the two given lanes run the same direction
  bool isLaneSameDir(const LaneLabel &lanelabel1,const LaneLabel &lanelabel2)
  {return prior.isLaneSameDir(lanelabel1,lanelabel2);}
  /// Checks if a point is geometrically in the lane
  bool isPointInLane(const point2_uncertain &pt, const LaneLabel &lanelabelin)
  {return prior.isPointInLane(pt,lanelabelin);}  
  bool isPointInLane(const point2 &pt, const LaneLabel &lanelabelin)
  {return prior.isPointInLane(pt,lanelabelin);}  
  
  //--------------------------------------------------
  // RNDF label based queries
  //--------------------------------------------------

  /// Given a point, gets the best guess of what lane you're in.  Not
  /// guaranteed to return the correct lane especially in
  /// intersections.  Will return lane which has the shortest
  /// projected distance to that lane's centerline.
  int getLane(LaneLabel & lanelabel, const point2& pt);


  //  int getLane(LaneLabel & lanelabel, const point2& pt, const double heading);
   

  /// Gets the left bound geometry of the given lane
  int getLaneLeftBound(point2arr_uncertain &bound, 
                       const LaneLabel &lanelabelin)
  { return prior.getLaneLeftBound(bound,lanelabelin);}
  int getLaneLeftBound(point2arr &bound, 
                       const LaneLabel &lanelabelin){
    point2arr_uncertain tmppt;
    int retval = prior.getLaneLeftBound(tmppt,lanelabelin);
    bound.set(tmppt);
    return retval;
  }

  
  /// Gets the right bound geometry of the given lane
  int getLaneRightBound(point2arr_uncertain &bound, 
                        const LaneLabel &lanelabelin)
  { return prior.getLaneRightBound(bound,lanelabelin);}
  int getLaneRightBound(point2arr &bound, 
                        const LaneLabel &lanelabelin){
    point2arr_uncertain tmppt;
    int retval = prior.getLaneRightBound(tmppt,lanelabelin);
    bound.set(tmppt);
    return retval;
  }
  
  /// Gets both left and right bound geometry of the given lane
  int getLaneBounds(point2arr_uncertain &leftbound, 
                    point2arr_uncertain &rightbound, 
                    const LaneLabel &lanelabelin)
  { return prior.getLaneBounds(leftbound,rightbound,lanelabelin);}
  int getLaneBounds(point2arr &leftbound, 
                    point2arr &rightbound, 
                    const LaneLabel &lanelabelin){
    point2arr_uncertain lefttmppt,righttmppt;
    int retval = prior.getLaneBounds(lefttmppt,righttmppt,lanelabelin);
    leftbound.set(lefttmppt); rightbound.set(righttmppt);
    return retval;
  }
  

 /// Gets both left and right bound geometry of the given lane in a single polygon
  /// bounds is ordered clockwise starting at the first point of the left lane
  /// The return value is the size of the left lane
  int getLaneBoundsPoly(point2arr_uncertain &bound, 
                    const LaneLabel &lanelabelin)
  { return prior.getLaneBoundsPoly(bound,lanelabelin);}
  int getLaneBoundsPoly(point2arr &bound, 
                    const LaneLabel &lanelabelin){
    point2arr_uncertain tmppt;
    int retval = prior.getLaneBoundsPoly(tmppt,lanelabelin);
    bound.set(tmppt);
    return retval;
  }

  
  /// Gets the centerline of the given lane
  int getLaneCenterLine(point2arr_uncertain &centerline, 
                       const LaneLabel &lanelabelin)
  { return prior.getLaneCenterLine(centerline,lanelabelin);}
  int getLaneCenterLine(point2arr &centerline, 
                        const LaneLabel &lanelabelin){
    point2arr_uncertain tmppt;
    int retval = prior.getLaneCenterLine(tmppt,lanelabelin);
    centerline.set(tmppt);
    return retval;
  }

  
  /// Gets the coordinates (local frame) of the given point label
  int getWayPoint(point2_uncertain &pt, 
               const PointLabel &ptlabelin)
  {return prior.getWayPoint(pt,ptlabelin);}
  int getWayPoint(point2 &pt, 
               const PointLabel &ptlabelin){
    point2_uncertain tmppt;
    int retval = prior.getWayPoint(tmppt,ptlabelin);
    pt.set(tmppt);
    return retval;
  }
  
  /// Gets the coordinate array (local frame) of the given point labels
  int getWayPoint(point2arr_uncertain &ptarr, 
                  const vector<PointLabel> &ptlabelarrin)
  {return prior.getWayPoint(ptarr,ptlabelarrin);}
  int getWayPoint(point2arr &ptarr, 
                  const vector<PointLabel> &ptlabelarrin){
    point2arr_uncertain tmppt;
    int retval = prior.getWayPoint(tmppt,ptlabelarrin);
    ptarr.set(tmppt);
    return retval;
  }
 
  /// Gets the left and right outer bounds of the lanes going in the same
  /// direction as the given lane label
  int getSameDirLaneBounds(point2arr_uncertain &leftbound, 
                           point2arr_uncertain &rightbound, 
                           const LaneLabel &lanelabelin)
  {return prior.getSameDirLaneBounds(leftbound, rightbound, lanelabelin);}
  int getSameDirLaneBounds(point2arr &leftbound, 
                           point2arr &rightbound, 
                           const LaneLabel &lanelabelin){
    point2arr_uncertain lefttmppt,righttmppt;
    int retval = prior.getSameDirLaneBounds(lefttmppt,righttmppt,lanelabelin);
    leftbound.set(lefttmppt); rightbound.set(righttmppt);
    return retval;
  }


  /// Gets the left and right outer bounds of the lanes going in the opposite
  /// direction as the given lane label
  int getOppDirLaneBounds(point2arr_uncertain &leftbound, 
                          point2arr_uncertain &rightbound, 
                          const LaneLabel &lanelabelin)
  {return prior.getOppDirLaneBounds(leftbound, rightbound, lanelabelin);}
  int getOppDirLaneBounds(point2arr &leftbound, 
                          point2arr &rightbound, 
                          const LaneLabel &lanelabelin){
    point2arr_uncertain lefttmppt,righttmppt;
    int retval = prior.getOppDirLaneBounds(lefttmppt,righttmppt,lanelabelin);
    leftbound.set(lefttmppt); rightbound.set(righttmppt);
    return retval;
  }

 
  /// Gets the left and right outer bounds of the full segment
  int getSegmentBounds(point2arr_uncertain &leftbound, 
                           point2arr_uncertain &rightbound, 
                           const LaneLabel &lanelabelin)
  {return prior.getSegmentBounds(leftbound, rightbound, lanelabelin);}
  int getSegmentBounds(point2arr &leftbound, 
                           point2arr &rightbound, 
                           const LaneLabel &lanelabelin){
    point2arr_uncertain lefttmppt,righttmppt;
    int retval = prior.getSegmentBounds(lefttmppt,righttmppt,lanelabelin);
    leftbound.set(lefttmppt); rightbound.set(righttmppt);
    return retval;
  }



/// Gets the waypoint labels which from which traffic can enter the given waypoint 
int getWayPointEntries(vector<PointLabel>& ptlabelarr,
                       const PointLabel &ptlabelin)
  {return prior.getWayPointEntries(ptlabelarr,ptlabelin);}

  int getWayPointExits(vector<PointLabel>& ptlabelarr,
                         const PointLabel &ptlabelin)
  {return prior.getWayPointExits(ptlabelarr,ptlabelin);}


  int getLaneExits(vector<PointLabel>& thislabelarr, 
                   vector<PointLabel>& otherlabelarr,
                   const LaneLabel &lanelabelin)
  {return prior.getLaneExits(thislabelarr,otherlabelarr,lanelabelin);}

  int getLaneEntries(vector<PointLabel>& thislabelarr,
                     vector<PointLabel>& otherlabelarr,
                         const LaneLabel &lanelabelin)
 {return prior.getLaneEntries(thislabelarr,otherlabelarr,lanelabelin);}

  int getLaneStopLines(vector<PointLabel>& ptlabelarr,
                            const LaneLabel &lanelabelin)
 {return prior.getLaneStopLines(ptlabelarr,lanelabelin);}

  int getLaneCheckPoints(vector<PointLabel>& ptlabelarr,
                              const LaneLabel &lanelabelin)
 {return prior.getLaneCheckPoints(ptlabelarr,lanelabelin);}

  int getLaneCheckPointNumbers(vector<int>& ptnumarr,
                               const LaneLabel &lanelabelin)
 {return prior.getLaneCheckPointNumbers(ptnumarr,lanelabelin);}

  int getLaneName(string &name, 
                  const LaneLabel &lanelabelin)
 {return prior.getLaneName(name,lanelabelin);}


  /// Returns neighboring lane label offset from the given lane.  
  ///
  /// A negative value of the offset parameter will return a lane
  ///lable on the left with respect to nominal driving direction of
  ///the given lane.  A positive value of offset will return a lane to
  ///the right.  An offset of 0 will return the current lane.
  int getNeighborLane(LaneLabel& lanelabel, 
                           const LaneLabel &lanelabelin,
                           const int offset)
 {return prior.getNeighborLane(lanelabel,lanelabelin,offset);}


  int getSameDirLanes(vector<LaneLabel>& lanelabelarr, const LaneLabel &lanelabelin)
 {return prior.getSameDirLanes(lanelabelarr,lanelabelin);}

  int getOppDirLanes(vector<LaneLabel>& lanelabelarr, const LaneLabel &lanelabelin)
 {return prior.getOppDirLanes(lanelabelarr,lanelabelin);}

  int getAllDirLanes(vector<LaneLabel>& lanelabelarr, const LaneLabel &lanelabelin)
 {return prior.getAllDirLanes(lanelabelarr,lanelabelin);}


  int getSegmentName(string &name, const int seglabelin)
 {return prior.getSegmentName(name,seglabelin);}

  int getSegmentNumLanes(const PointLabel & pointlabelin)
  {return prior.getSegmentNumLanes(pointlabelin);}
  int getSegmentNumLanes(const LaneLabel & lanelabelin)
  {return prior.getSegmentNumLanes(lanelabelin);}
  int getSegmentNumLanes(const int seglabelin) 
  {return prior.getSegmentNumLanes(seglabelin);}



  int getPointAlongLine(point2 &ptout, const point2arr & line, const double dist);
  int getPointAlongLine(point2_uncertain &ptout, const point2arr_uncertain & line, const double dist){ 
    point2arr tmpline; point2 tmpptout;
    int retval= getPointAlongLine(tmpptout,tmpline,dist);
    ptout.set(tmpptout);
    return retval;
  }
 

  int getDistAlongLine(double &dist, const point2arr & line, const point2& pt);
  int getDistAlongLine(double &dist, const point2arr_uncertain & line, const point2_uncertain& pt){ 
    point2arr tmpline; point2 tmppt;
    return getDistAlongLine(dist,tmpline,tmppt);
  }
 
  int getDistAlongLine(double &dist, const point2arr & line, const point2& pta, const point2& ptb);
  int getDistAlongLine(double &dist, const point2arr_uncertain & line, const point2_uncertain& pta, const point2_uncertain& ptb){ 
    point2arr tmpline; point2 tmppta, tmpptb;
    return getDistAlongLine(dist,tmpline,tmppta,tmpptb);
  } 

  int getProjectToLine(point2 &projpt, const point2arr & line, const point2& pt);
  int getProjectToLine(point2_uncertain &projpt, const point2arr_uncertain & line, const point2_uncertain& pt){ 
    point2arr tmpline; point2 tmppt, tmpprojpt;
    int retval = getProjectToLine(tmpprojpt,tmpline,tmppt);
    projpt.set(tmpprojpt);
    return (retval);
  }


  int extendLine(point2arr &lineout, const point2arr & line, const double dist);
  int extendLine(point2arr_uncertain & lineout, const point2arr_uncertain & line, const double dist){
    point2arr tmpline(line), tmplineout;
    int retval=extendLine(tmplineout,tmpline,dist);
    lineout.set(tmplineout);
    return retval;
  }
  

  int getObsNearby(vector<MapElement> &elarr,const point2 &pt, const double dist);
  int getObsNearby(vector<MapElement> &elarr,const point2_uncertain &pt, const double dist){
    point2 tmppt(pt);
    return getObsNearby(elarr,tmppt,dist);
  }
  int getObsInLane(vector<MapElement> &elarr,const LaneLabel &lanelabelin);
  //int getObsInSegment(vector<MapElement> &elarr,const LaneLabel &lanelabelin);

  int getObsInBounds(vector<MapElement> &elarr, const point2arr &leftbound, const point2arr &rightbound);
  int getObsInBounds(vector<MapElement> &elarr, const point2arr_uncertain &leftbound, const point2arr_uncertain &rightbound){
    point2arr tmpleft(leftbound),tmpright(rightbound);
    return getObsInBounds(elarr,tmpleft,tmpright);
    
  }
  
  
  int getEl(MapElement &el, const MapId &id);
  
  //int getElGeometry(point2arr_uncertain &ptarr,const MapId &id);
  //int getElVelocity(point2_uncertain &vel,const MapId &id);
  //int getElPosition(point2_uncertain &pt,const MapId &id);


  

  bool loadRNDF(string filename);

  vector<MapElement> data;
  MapPrior prior;


  int addEl(const MapElement &el);
 
  void setTransform( const point2_uncertain &pt)
  {prior.delta =pt;}
  void setTransform( const point2 &pt)
  {prior.delta =pt;}
  point2_uncertain getTransform()
  {return prior.delta;}



//--------------------------------------------------
  // tested functions
  //--------------------------------------------------
  bool checkLaneID(const point2 pt, const int segment, const int lane);
  bool checkLaneID(const point2 pt, const LaneLabel &label);

  
  //--------------------------------------------------
  // 
  //--------------------------------------------------
  int getSegmentID(const point2 pt);
  int getLaneID(const point2 pt);
  //  int getZoneID(const point2 pt);
  int getNextPointID(PointLabel &label,const point2& pt);
  int getLastPointID(PointLabel &label,const point2& pt);
  int getClosestPointID(PointLabel &label, const point2& pt);

  int getLaneCenterPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset=0);
 int getLaneLeftPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset=0);
 int getLaneRightPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset=0);

  // PointLabel getNextPointID(const point2& pt);
  // PointLabel getClosestPointID(const point2& pt);

  
  int getLeftBound(point2arr& ptarr, const LaneLabel &label );
  int getRightBound(point2arr& ptarr, const LaneLabel &label );

  int getLeftBoundType(string& type, const LaneLabel &label);
  int getRightBoundType(string& type, const LaneLabel &label);

  int getStopline(point2& pt, PointLabel &label);
  int getNextStopline(point2& pt, PointLabel &label);
  int getStopline(point2& pt, point2 state);
  
  int getWaypoint(point2& pt, const PointLabel &label);

  
  int getBounds(point2arr& leftbound, point2arr& rightbound, 
                const LaneLabel &label,const point2 state, 
                const double range , const double backrange=10);
 int getBoundsReverse(point2arr& leftbound, point2arr& rightbound, 
                      const LaneLabel &label,const point2 state, 
                      const double range, const double backrange=10);
  
  int getTransitionBounds(point2arr &leftbound ,point2arr &rightbound , 
                          const PointLabel &exitlabel, 
                          const PointLabel &enterlabel, 
                          const point2 pt, const double range, 
                          const double backrange=10);

//   int getMatchedBounds(point2arr& leftbound, point2arr& rightbound, 
//                        const LaneLabel &label,const point2 state, 
//                        const double range , const double backrange=10);
//   int getMatchedBoundsReverse(point2arr& leftbound, 
//                               point2arr& rightbound, 
//                               const LaneLabel &label,
//                               const point2 state, 
//                               const double range, 
//                               const double backrange=10);
  
//   int getMatchedTransitionBounds(point2arr &leftbound ,
//                                  point2arr &rightbound , 
//                                  const PointLabel &exitlabel, 
//                                  const PointLabel &enterlabel, 
//                                  const point2 pt, const double range, 
//                                  const double backrange=10);



  int getHeading(double & ang, const PointLabel &label);
  int getHeading(double & ang, const point2 &pt);


 

  bool isStop(PointLabel &label);
  bool isExit(PointLabel &label);

  int getWaypointArr(point2arr& ptarr, const PointLabel &label);
  int getLaneDistToWaypoint(double& dist,const point2 state,const PointLabel &label);
  int getLaneDistToWaypoint(double &dist, const PointLabel &statelabel,const PointLabel &label);

 double getObstacleDist(const point2& state,const int lanedelta=0);

  point2 getObstaclePoint(const point2& state, const double offset=0);
  int getObstacleGeometry(point2arr& ptarr);

};



#endif
 
