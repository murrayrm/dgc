/**********************************************************
 **
 **  MAP.CC
 **
 **    Time-stamp: <2007-09-28 04:40:46 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Fri Feb 16 21:11:45 2007
 **
 **
 **********************************************************
 **
 ** Map Class  
 **
 **********************************************************/

#include <cassert>
#include <list>

#include <boost/array.hpp>
#include <boost/multi_array.hpp>
#include <interfaces/sn_types.h>

#include "Map.hh"

using namespace std;
using boost::multi_array;
using boost::array;


// Useful message macro
#ifdef __GNUC__
/* use the __PRETTY_FUNCTION__ gcc extension that expands to the current function name */
#define MSG(fmt, ...)\
  (fprintf(stderr, "%s:%d:%s: " fmt "\n", __FILE__, __LINE__, __PRETTY_FUNCTION__, ##__VA_ARGS__) ? 0 : 0 )
#else
//// do we ever use anything else other than gcc? just in case ...
#define MSG(fmt, ...)\
  (fprintf(stderr, "%s:%d: " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0 )
#endif

// Useful error macro
#define ERROR(fmt, ...) (MSG("*** ERROR: "  fmt, ##__VA_ARGS__) ? -1 : -1)


Map::Map(bool _lineFusion, bool lineFusOnAddEl)
  : lineFusion(_lineFusion && !lineFusOnAddEl),
    lastStateTimestamp(0), lineFusDebug(NULL)
{
  prior.internalLineFusion = _lineFusion;
}

Map::~Map()
{}


void Map::init()
{
  ///MSG("initializing map");
  numMapObjects = 0;

  usedIndices.clear();
  openIndices.clear();
  for(int i=MAXNUMOBJECTS-1; i >= 0; i--) {
    openIndices.push_back(i);
  }

  for(int i = 0; i < NUM_PERCEPTORS; i++) {
    for(int j = 0; j < MAXNUMPERCEPTOROBJECTS; j++) {
      IDtoINDEX[i][j] = -1;
    }
  }
  //  memset(IDtoINDEX, -1, NUM_PERCEPTORS*MAXNUMPERCEPTOROBJECTS*sizeof(int));

  //TODO:
  //initialize everything in newMap to 0...

}


bool Map::loadRNDF(string filename) 
{
  
  bool retval = prior.loadRNDF(filename);
  if (!retval){
    cerr << "in Map::loadRNDF() error loading " << filename << endl;
  }
  this->init();
  return retval;

  
}


void Map::copyMapData(Map *targetmap) 
{
  int index;
  int j;
 	targetmap->usedIndices =  this->usedIndices;
	targetmap->openIndices =  this->openIndices;
	for (j = 0; j < (int)this->usedIndices.size(); j++) {
		index = this->usedIndices[j];
		targetmap->newData[index].mergedMapElement = this->newData[index].mergedMapElement;
	}					
	targetmap->data = this->data;
  targetmap->prior.fulldataUpdateFlag = this->prior.fulldataUpdateFlag;
  for (j = 0 ; j < (int)this->prior.fulldata.size();++j){
    if (this->prior.fulldataUpdateFlag[j]){
      targetmap->prior.fulldataUpdateFlag[j] = false;
      targetmap->prior.fulldata[j] = this->prior.fulldata[j];
      this->prior.fulldataUpdateFlag[j] = false;
    } 
  }
	
  
}

/**
 * Sorts incoming elements by first # in their IDs -
 *   by convention, this is their module ID.
 * There are two different map data structures: one
 *  for 3-dimensional obtacles, one for everything else.
 *  Any element not coming from an obs-perceptor is 
 *  placed into the vector of MapElements, and anything
 *  else is placed into the array of ObjectData.
 *
 *  returns int action:
 *     -1   couldn't add el
 *     0    added to non-obstacle structure
 *     1    associated based on ID to obstacle
 *     2    associated based on geometry to obstacle
 *     3    added new obstacle
**/

int Map::addEl(const MapElement &el, bool fusionDisabled)
{
  int action = 0;

  int index = 0;

  int originatingPerceptorID, idFromPerceptor, originatingPerceptor;
  if(el.id.dat.size() == 0) {
		///MSG("incoming el had id size 0");
    return -1;
  } else {
    originatingPerceptorID = el.id.dat[0];
  }

  switch(originatingPerceptorID) {
    case 10://MODmapping
      originatingPerceptor = FUSED_FROM_MAPPER;
      break;

    case 7://MODtrafficplanner
      originatingPerceptor = PREDICTION;
      break;

    case 87: //MODladarCarPerceptorLeft: 
      originatingPerceptor = LF_LADAR;
      break;

    case 94: //MODladarCarPerceptorRoofLeft:
      originatingPerceptor = LF_ROOF_LADAR;

    case 88: //MODladarCarPerceptorCenter: 
      originatingPerceptor = MF_LADAR;
      break;

    case 89: //MODladarCarPerceptorRight: 
      originatingPerceptor = RF_LADAR;
      break;

    case 90: //MODladarCarPerceptorRear: 
      originatingPerceptor = REAR_LADAR;
      break;

    case 70: //MODladarObsPerceptor: 
      originatingPerceptor = LADAR_OBS;
      break;

    case 95: //MODladarCarPerceptorRiegl: 
      originatingPerceptor = RIEGL;
      break;

    case 79: //MODradarObsPerceptor: 
      originatingPerceptor = MF_RADAR;
      break;

    case 83: //MODstereoObsPerceptorLong: 
      originatingPerceptor = LONG_STEREO_OBS;
      break;

    case 82: //MODstereoObsPerceptorMedium: 
      originatingPerceptor = MEDIUM_STEREO_OBS;
      break;

    case 102: //MODstereoObsPerceptorRear: 
      originatingPerceptor = REAR_STEREO_OBS;
      break;

    case 0: //Mapper sent fused lane line: 
      originatingPerceptor = PRIOR_FROM_MAPPER;
      break;
    default:
      //unrecognized perceptor (including all lane lines)
      originatingPerceptor = NUM_PERCEPTORS;
    }
  
  // update local to global offset if the element contains valid state
  if (el.state.timestamp>lastStateTimestamp){
    prior.setLocalToGlobalOffset(el.state);
                lastStateTimestamp = el.state.timestamp;
  }
  
  // update prior with new data
  // used to enable external line fusion
  if(originatingPerceptor == PRIOR_FROM_MAPPER) {
    unsigned int i;
    for (i = 0; i < prior.fulldata.size(); ++i){
      if (el.id == prior.fulldata[i].id){
        prior.setElFull(el,i);
        break;
      }
    }
    return 0;
  }
  

    if(originatingPerceptor == NUM_PERCEPTORS) {

      MapId thisid = el.id;
      MapId tmpid;
      unsigned int i;
      for (i = 0; i < data.size(); ++i){
        tmpid = data[i].id;
        if (tmpid == thisid){
          if (el.type==ELEMENT_CLEAR){
            data.erase(data.begin()+i);
          }else{
            data[i] = el;
          }
          break;
        }
      }

      if(el.type != ELEMENT_CLEAR) {
        if (i == data.size()) { // not found, add as new
          data.push_back(el);
        }
        if (lineFusionEnabled() && el.isLine())
        {
          DBG_ASSOC_RESET(lineFusDebug);
          if (el.id[0] == MODladarCurbPerceptor)
            fuseCurb(el);
          else if(el.type != ELEMENT_STOPLINE)
            fuseLaneLine(el);
        }
      }
      return 0;

    }


    ///////NEW DATA STRUCTURE - only used for obstacles right now....ask sam what to do about lines////////
    if(el.id.dat.size() >= 2) {
      idFromPerceptor = el.id.dat[1];

      if(idFromPerceptor < 0 || idFromPerceptor >= MAXNUMPERCEPTOROBJECTS) {

        ///MSG("object ID out of range - could not add el");
        return -1;
      }

    } else {
      ///MSG("incoming element ID too short: %d", el.id.dat.size());
      return -1;
    }


    /** 
     * If map is running inside planner, and receiving map elements from
     * mapper, it needs to process the elements differently:
     * 
     * - a clear message actually entails removing the element from the map
     * - we want to update the merged geometry every time
     * - don't do association on incoming elements
     *
     * Note: these rules also apply to mapper's internal map when mapper is 
     *   running w/o fusion
     **/
    if((FUSED_FROM_MAPPER == originatingPerceptor) 
       || (PREDICTION == originatingPerceptor)
       || fusionDisabled) {

      index = IDtoINDEX[originatingPerceptor][idFromPerceptor];

      if(ELEMENT_CLEAR == el.type) {
  if (index != -1) {
    //put index back on openIndices array
    vector<int>::iterator mapIterator;
    IDtoINDEX[originatingPerceptor][idFromPerceptor] = -1;
    mapIterator =  usedIndices.begin();
    //find which object in the vector we need to erase
    int pos = -1;
    for(unsigned int tmppos = 0; tmppos < usedIndices.size(); tmppos++) {
      if(usedIndices.at(tmppos) == index) {
        pos = tmppos;
      }
    }
    if( -1 == pos) {
      ///MSG("didn't find index %d in usedIndices (size = %d, %d)", index, usedIndices.size(), openIndices.size());
    } else {
      mapIterator += pos;
      //          MSG("received clear msg from mapper: id = %d, type =%d, index = %d", idFromPerceptor, el.type,index);
      usedIndices.erase(mapIterator);
      openIndices.push_back(index);
    }
  }
  return 0;
      }

      //if we haven't yet seen this object, add it to map
      if(index == -1) {
  if(openIndices.size() > 0) {

    int newIndex = openIndices.back();
    openIndices.pop_back();
    usedIndices.push_back(newIndex);
    index = newIndex;
    //        MSG("received new el from mapper: id = %d, type =%d, index = %d", idFromPerceptor, el.type,index);
    //fill in lookup table
    IDtoINDEX[originatingPerceptor][idFromPerceptor] = index;

  } else {
		///MSG("No more space in map, not adding object");
          return -1;
  }
      }

      //for ALL elements coming from mapper or prediction, set fusedMapElement = geometry
      newData[index].mergedMapElement = el;
      newData[index].mergedMapElement.updateFromGeometry();
      newData[index].trackAlive[originatingPerceptor] = 1;
      newData[index].inputData[originatingPerceptor].lastSeen = el.timestamp; 

      return 0; //done fusing

    } //end of special case of data from Mapper


    //take care of clearing obstacles
    //warning: this assumes that anything coming from the ladar/radar/stereo-obs
    //  perceptors IS an obstacle...
    if((ELEMENT_CLEAR == el.type) && (NUM_PERCEPTORS != originatingPerceptor)) {
      index = IDtoINDEX[originatingPerceptor][idFromPerceptor];
      if(index != -1) {
  //reset entry in lookup table
  IDtoINDEX[originatingPerceptor][idFromPerceptor] = -1; 
  //in object data, note that this track is dead
  newData[index].trackAlive[originatingPerceptor] = 0;
      } 
      return 4; 
    }

    // anything left should be an obstacle
    if(!el.isObstacle()){
      ///MSG("tried to add non-obst to newData...");
      return -1;
    }

    index = IDtoINDEX[originatingPerceptor][idFromPerceptor];


    //addEl returns whether the ID allowed a lookup, there was 
    //a successful association, or if it became a new el
    if(index != -1) {
      action = 1; //associated w/ IDtoINDEX
    }

    if(index == -1) { //haven't seen this ID before; check for overlaps

      for(unsigned int i = 0; i < usedIndices.size(); i++) {
        unsigned int j = usedIndices.at(i);
        //TODO: BETTER ASSOCIATION CHECK
        if(el.isOverlap(newData[j].mergedMapElement.geometry) 
     || newData[j].mergedMapElement.isOverlap(el.geometry)) {
    action = 2; //associated w/ prev object
          index = j;
          IDtoINDEX[originatingPerceptor][idFromPerceptor] = index;
          //      MSG("found overlap w/ id %d - perceptor %d, ID %d", newData[j].id, originatingPerceptor, idFromPerceptor);
          break; 
        }
      }
    }

    //check if we need to add object (if index still =-1, then no overlap found)
    if(index == -1) {

      action = 3; //had to add new object

      if(openIndices.size() > 0) {
        int newIndex = openIndices.back();
        openIndices.pop_back();
        usedIndices.push_back(newIndex);
        index = newIndex;

        //fill in lookup table
        IDtoINDEX[originatingPerceptor][idFromPerceptor] = newIndex;

        //initialize appropriate fields in newData
  newData[index].inputData[originatingPerceptor].timesSeen = 0; 
  memset(newData[index].trackAlive, 0, NUM_PERCEPTORS*sizeof(int));
        newData[index].mergedMapElement = el;
        newData[index].mergedMapElement.updateFromGeometry();
  newData[index].mergedMapElement.geometryType = GEOMETRY_POLY;

  //set object id
  newData[index].id = numMapObjects;
        newData[index].mergedMapElement.id.clear();
        newData[index].mergedMapElement.setId(originatingPerceptorID, numMapObjects);
  numMapObjects ++;

      } else {
        ///MSG("No more space in map, not adding object");
        return -1;
      }
    }


    newData[index].inputData[originatingPerceptor].timesSeen ++;
    newData[index].inputData[originatingPerceptor].lastSeen = el.timestamp; 
    newData[index].inputData[originatingPerceptor].geometry.clear();
    newData[index].inputData[originatingPerceptor].geometry = point2arr(el.geometry);
    newData[index].inputData[originatingPerceptor].velocity = el.velocity;
    newData[index].inputData[originatingPerceptor].type = el.type;
    newData[index].inputData[originatingPerceptor].timeStopped = el.timeStopped;
    //note that track is still updating
    newData[index].trackAlive[originatingPerceptor] = 1;

    return action;
  
}


float LANE_POINTS_SPACING = 1.5; // 1m
float DIST_THRES = 1.5;
const float DECAY = 0.93; // decay speed to update prior

const float VOTE_THRES = 5; // minimum absolute number of votes required
const float VOTE_RATIO_THRES = 0.30; // ratio between votes and total sensed points
const float RMS_THRES = DIST_THRES;

const float PROB_FALSE_POSITIVE = 0.3; // fairly high, there's a lot of clutter
const float PROB_FALSE_NEGATIVE = 0.6; // yes, this is high, a line can even not be there at all!

static int nfull = 0;
static int nfast = 0;

/// Returns the number of "votes" for the association of the given
/// prior line with the given sensed line. A vote is a sensed
/// point within 'thres' distance of a point in the prior.
/// The actual value returned may be a function of the votes, the
/// actual distance and the number of points in the lines.
/// In any case, higher is better match.
/// It the 'prob' output argument is not NULL, the probability
/// of the association is returned into it. If the bounding box
/// check (using thres) fails, prob is unchanged.
/// FIXME: the probability is temporary replaced with the RMS^2 error.
static float getVotes(const MapElement& prior, int currPoint,
                      const MapElement& sensed, float thres, double* rms2Out = NULL,
                      int* firstPt = NULL, int* lastPt = NULL, LineFusionDebugData* dbg = NULL)
{
  // check bounding boxes first
  // enlarge bounding boxes by the given threshold
  // TODO: use uncertainty ...
  point2 min1 = prior.geometryMin - point2(thres/2, thres/2);
  point2 max1 = prior.geometryMax + point2(thres/2, thres/2);
  point2 min2 = sensed.geometryMin - point2(thres/2, thres/2);
  point2 max2 = sensed.geometryMax + point2(thres/2, thres/2);
  
  if (max1.x < min2.x || min1.x > max2.x
      || max1.y < min2.y || min1.y > max2.y)
  {
    nfast++;
    return 0; // note: prob is unchanged
  }

  nfull++;
  // bounding boxes overlap, stop being smart and do the full check
  
  const point2arr_uncertain& priorGeom = prior.geometry;
  const point2arr_uncertain& sensedGeom = sensed.geometry;

  double rms2 = 0;
  //double prob = 0;
  int votes = 0;
  int priorBegin = currPoint - int(25/LANE_POINTS_SPACING);
  int priorEnd = currPoint + int(25/LANE_POINTS_SPACING);
  if (priorBegin < 0)
      priorBegin = 0;
  if (priorEnd >= int(priorGeom.size()))
      priorEnd = priorGeom.size() - 1;

  // iterate for all interesting points in the prior and
  // find the closest point to the first and last sensed
  int minIdx1 = -1;
  double distFirst2 = numeric_limits<double>::max();
  int minIdx2 = -1;
  double distSecond2 = numeric_limits<double>::max();
  int i, j;
  for (i = priorBegin; i < priorEnd; i++)
  {
      point2 v = priorGeom[i] - sensedGeom[0];
      float dist2 = v.norm2();
      if (dist2 < distFirst2) {
          distFirst2 = dist2;
          minIdx1 = i;
      }
      v = priorGeom[i] - sensedGeom.arr.back();
      dist2 = v.norm2();
      if (dist2 < distSecond2) {
          distSecond2 = dist2;
          minIdx2 = i;
      }
  }

  if (firstPt != NULL)
      *firstPt = minIdx1;
  if (lastPt != NULL)
      *lastPt = minIdx2;

  priorBegin = minIdx1;
  priorEnd = minIdx2;
  int inc = priorBegin <= priorEnd ? 1 : -1;

  j = 0;
  priorEnd += inc;
  for(i = priorBegin; i != priorEnd; i += inc)
  {
      point2 v = priorGeom[i] - sensedGeom[j];
      float dist2 = v.norm2();
      while (j < int(sensedGeom.size()) - 1) {
          point2 next = priorGeom[i] - sensedGeom[j + 1];
          float distNext2 = next.norm2();
          if (distNext2 < dist2) {
              // before advancing, check if to use the current point
              if (dist2 < thres*thres) {
                  votes++;
                  DBG_ASSOC_ADD(dbg, priorGeom[i], sensedGeom[j]);
              }
              dist2 = distNext2;
              j++; 
          } else {
              break;
          }
      }
      if (dist2 < thres*thres) {
          votes++;
          DBG_ASSOC_ADD(dbg, priorGeom[i], sensedGeom[j]);
          j++;
      }
      rms2 += dist2;
  }

/*
  for (i = 0; i < sensedGeom.size(); i++)
  {
      bool vote = false;
      float mindist2 = thres*thres;
      for (j = 0; j < priorGeom.size(); j++)
      {
          point2 v = priorGeom[j] - sensedGeom[i];
          float dist2 = v.x*v.x + v.y*v.y;
          if (dist2 < mindist2) {
              vote = true;
              mindist2 = dist2;
          }
      }
      if (vote)
      {
          votes++;
          rms2 += mindist2;
      }
  }
*/

  if (rms2Out != NULL)
    *rms2Out = rms2/votes;
//#error "Implement line assoc probability"
  return votes;
}

#if 0

/**
 * Given the master association table and the current association
 * in 'assoc',computes the next association and updated 'assoc'.
 */
static bool nextAssoc(const multi_array<bool,2>& master,
                      multi_array<bool,2>* assoc)
{
    int rows = master.shape()[0];
    int cols = master.shape()[1];
    for (int i = rows - 1; i >= 0; i--)
    {
        int j;
        for(j = 0; j < cols; j++)
        {
            if ((*assoc)[i][j] == true)
                break;
        }
        assert(j < cols); // assoc MUST have a true for each row
        int old = j;
        for(j++; j < cols; j++)
        {
            if (master[i][j] == true)
            {
                (*assoc)[i][old] = false;
                (*assoc)[i][j] = true;
                break;
            }
        }
        if (j < cols)
            return true; // we got the next assoc, quit

        // else, we're at the last true column for this row,
        // let's reset back to the first and move on the the prev row
        for(j = 0; j < cols; j++)
        {
            if (master[i][j] == true)
                break;
        }
        assert(j < cols); // master MUST have at least one 'true' per row
        (*assoc)[i][old] = false;
        (*assoc)[i][j] = true;
    }
    return false;
}

/**
 * Associate sensed lane lines with lines in the current segment, passed as
 * the index of the current segment in the MapPrior::segments vector.
 * \param[out] assocOut The vector where to store the information about
 *   the most likely associations found, in the for of a 2D matrix:
 *   assoc[sensedIdx][k] = priorLaneIdx, (for the k-th association).
 * \param[out] likelihood The likelihood of each of the k associations
 *   returned in 'assocOut'.
 * \param sensedLines The sensed lines as received from the perceptors
 * \param priorSegment The RNDF segment to associate the lines with (usually
 *   the current segment we're in).
 */
void Map::associateLaneLines(multi_array<int,2>* assocOut, vector<float>* likelihood,
                             const vector<const MapElement*>& sensedLines,
                             int priorSegment)
{
    int numAssoc = assoc->shape()[1];
    const vector<int>& laneIdx = prior.segments[priorSegment].laneindex;
    int numSensed = sensedLines.size();
    int numPrior = laneIdx.size() * 2; // for each lane, we have left and right
    vector<int> lineIdx(numPrior);
    for (unsigned int k = 0; k < laneIdx.size(); k++)
    {
        lineIdx[2*k + 0] = laneIdx[k].leftbound_elindex;
        lineIdx[2*k + 1] = laneIdx[k].rightbound_elindex;
    }

    // current association matrix
    multi_array<bool, 2> assoc(boost::extents[numSensed + 1][numPrior + 1]);
    // probability of each (sensed, prior) association
    multi_array<double, 2> prob(boost::extents[numSensed + 1][numPrior + 1]);

    memset(assoc.data(), 0, rows*cols*sizeof(bool));
    fill(prob.data(), prob.data()+rows*cols, -1.0); // <0.0 means "don't know"

    // calculate the probability of the association
    for (int i = 0; i < numSensed; i++)
    {
        for (int j = 0; j < numPrior; j++)
        {
            float vote = getVotes(prior.fulldata[lineIdx[j]], sensedLines[i],
                                  DIST_THRES, &prob[i][j]);
            voteRatio = vote / sensedLines[i].size();
            if (vote > VOTE_THRES && voteRatio > VOTE_RATIO_THRES)
                assoc[i][j] = true;
        }
    }

    // fill false positive probability
    for (i = 0; i < numSensed; i++)
    {
        assoc[i][numPrior + 1] = true;
        prob[i][numPrior + 1] = PROB_FALSE_POSITIVE;
    }

    // fill false negative (clutter, prob of non existence) probability
    for (j = 0; j < numPrior; j++)
    {
        assoc[numSensed + 1][j] = true;
        prob[numSensed + 1][j] = PROB_FALSE_NEGATIVE;
    }

    // enumerate all the possible associations (the ones where each sensed is associated
    // with one and only one prior, or with clutter).
    
    
}

#endif


void Map::getClosestSegment(const VehicleState& state,
                            int* currSeg, int* currLane,
                            int* currPoint)
{
  // get the segment and lane we're in
  int minseg = -1;
  int minlane = -1;
  int minpoint = -1;
  float mindist2 = DIST_THRES * DIST_THRES * 16;//1e20;
  point2 here(state.localX, state.localY);

  for (unsigned int i = 0; i < prior.segments.size(); i++)
  {
      RNDFsegment& seg = prior.segments[i];
      for (unsigned int j = 0; j < seg.laneindex.size(); j++)
      {
          int idx = seg.laneindex[j];
          MapElement el;
          prior.getElFull(el, prior.lanes[idx].waypoint_elindex);
          for (unsigned int k = 0; k < el.geometry.size(); k++)
          {
              float dist2 = point2(el.geometry[k] - here).norm2();
              if (dist2 < mindist2) {
                  minseg = i;
                  minlane = j;
                  minpoint = k;
                  mindist2 = dist2;
              }
          }
      }
  }

  if (currSeg != NULL)
      *currSeg = minseg;
  if (currLane != NULL)
      *currLane = minlane;
  if (currPoint != NULL)
      *currPoint = minpoint;
}

void Map::fuseAllLines(const VehicleState& state)
{
  int currSeg, currLane, currPoint;

  getClosestSegment(state, &currSeg, &currLane, &currPoint);

  if (currSeg == -1)
      return; // driving off-road?

  DBG_ASSOC_RESET(lineFusDebug);

  bool updated = false;

  for (unsigned int i = 0; i < data.size(); i++)
  {
    if (data[i].type == ELEMENT_LANELINE || data[i].type == ELEMENT_LINE) {
      bool ret = false;
      if (data[i].id[0] == MODladarCurbPerceptor)
        fuseCurb(data[i], currSeg, currLane, currPoint);
      else
        ret = fuseLaneLine(data[i], currSeg, currLane, currPoint);
      updated = updated || ret;
    }
  }

  if (updated)
    postProcessSegment(currSeg);

}

void Map::fuseAllLines()
{
  DBG_ASSOC_RESET(lineFusDebug);

  for (unsigned int i = 0; i < data.size(); i++)
  {
    if (data[i].type == ELEMENT_LANELINE || data[i].type == ELEMENT_LINE) {
      if (data[i].id[0] == MODladarCurbPerceptor)
        fuseCurb(data[i]);
      else
        fuseLaneLine(data[i]);
    }
  }
}


bool Map::fuseLaneLine(const MapElement& laneEl)
{
    int currSeg, currLane, currPoint;
    getClosestSegment(laneEl.state, &currSeg, &currLane, &currPoint);

    if (currSeg == -1)
        return false; // driving off-road?

  bool ret = fuseLaneLine(laneEl, currSeg, currLane, currPoint);

  if (ret == true) // do this everytime instead?
      postProcessSegment(currSeg);

  return ret;
}

// fuse a lane line giving the segment and lane to be fused with.
bool Map::fuseLaneLine(const MapElement& laneEl, int currSeg, int currLane, int currPoint)
{
  //MSG("begin: el.id=%d.%d", laneEl.id[0], laneEl.id[1]);

  MapElement laneElMod = laneEl;
  laneElMod.setMinMaxFromGeometry(); // make sure it has bouding box info

  const point2arr_uncertain& sensedOrig = laneEl.geometry;
  if (sensedOrig.size() == 0)
      return false;

  point2arr_uncertain& sensed = laneElMod.geometry;
  sensed.clear();
  sensed.arr.reserve(sensedOrig.size()*2);

  // linearly interpolate sensed line, so each point is distant no more than
  // LANE_POINTS_SPACING from the previous
  for (int i = 0; i < int(sensedOrig.size()) - 1; i++)
  {
      point2_uncertain vec = sensedOrig[i + 1] - sensedOrig[i];
      float length = vec.norm();
      if (length > LANE_POINTS_SPACING) {
          int nparts = int(ceil(length / LANE_POINTS_SPACING));
          for (int k = 0; k < nparts; k++) {
              sensed.push_back(sensedOrig[i] + vec * k / nparts);
          }
      } else {
          sensed.push_back(sensedOrig[i]);
      }
  }
  if (sensedOrig.size() > 0)
      sensed.push_back(sensedOrig.arr.back());

  RNDFsegment& seg = prior.segments[currSeg];

  // load all lanes in current segment
  vector<MapElement> elems(seg.laneindex.size() * 2); // each lane has left and right bound
  vector<int> elemIdx(seg.laneindex.size() * 2);
  for (unsigned int j = 0; j < seg.laneindex.size(); j++)
  {
      int idx = seg.laneindex[j];
      elemIdx[2*j + 0] = prior.lanes[idx].leftbound_elindex;
      elemIdx[2*j + 1] = prior.lanes[idx].rightbound_elindex;
      prior.getElFull(elems[2*j + 0], elemIdx[2*j + 0]);
      prior.getElFull(elems[2*j + 1], elemIdx[2*j + 1]);
  }

  float minRms2 = RMS_THRES * RMS_THRES;
  float maxVoteRatio = VOTE_RATIO_THRES;
  int maxidx = -1;
  double vote, voteRatio, rms2;
  int firstIdx, lastIdx;
  for (unsigned int i = 0; i < elems.size(); i++)
  {
    int laneidx = seg.laneindex[i >> 1];
    bool left = !(i & 1);

    // check if this lane line is supposed to exist at all
    if (left && prior.lanes[laneidx].leftbound_type.empty())
      continue;
    if (!left && prior.lanes[laneidx].rightbound_type.empty())
      continue;

    //point2arr lane = elems[i].geometry; // convert from point2arr_uncertain to point2arr
    //float dist = lane.get_min_dist_line(laneEl.geometry);
    //float dist;
    int i1, i2;
    vote = getVotes(elems[i], currPoint, laneElMod, DIST_THRES, &rms2,
                    &i1, &i2, lineFusDebug);
    voteRatio = vote / sensed.size();

    // HACK to favor association with current lane
    if (laneidx != currLane)
        voteRatio *= 0.7;

    // note: sometimes the vote ratios are exactly the same, == 1, when all the sensed are
    // associated with the prior
    if (voteRatio > VOTE_RATIO_THRES && rms2 < minRms2)
    {
      maxidx = i;
      //maxVote = vote;
      maxVoteRatio = voteRatio;
      minRms2 = rms2;
      firstIdx = i1;
      lastIdx = i2;
      //MSG("+ Vote ratio for %s lane line (%d.%d) with sensed line (%d.%d) = %g, rms^2 = %g, may fuse",
      //    ((i & 1) ? "right" : "left"), seg.lane_label[i >> 1].segment,
      //    seg.lane_label[i >> 1].lane, laneEl.id[0], laneEl.id[1], voteRatio, rms2);
    } else if (vote > 0) {
      //MSG("- Vote ratio for %s lane line (%d.%d) with sensed line (%d.%d) = %g, rms^2 = %g, not fusing",
      //    ((i & 1) ? "right" : "left"), seg.lane_label[i >> 1].segment,
      //    seg.lane_label[i >> 1].lane, laneEl.id[0], laneEl.id[1], voteRatio, rms2);
    }
  }

  if (maxidx != -1)
  {
    int maxlane = maxidx >> 1;
    int maxlaneIdx = seg.laneindex[maxlane];
    //MSG("! Fusing %s lane line (%d.%d) with sensed line (%d.%d), vote ratio was = %g, rms^2 = %g",
    //    ((maxidx & 1) ? "right" : "left"), seg.lane_label[maxlane].segment,
    //    seg.lane_label[maxlane].lane, laneEl.id[0], laneEl.id[1], maxVoteRatio, minRms2);
    MapElement rndfEl;
    prior.getEl(rndfEl, elemIdx[maxidx]);

    vector<bool> isUpd;
    fuseLaneLineInternal(elems[maxidx], laneElMod, firstIdx, lastIdx, isUpd);

    elems[maxidx].updateFromGeometry();
    prior.setElFull(elems[maxidx], elemIdx[maxidx]);

    // FIXME: this part has still a complexity of O(Nprior) instead
    // of being constant wrt the prior number of points
    postProcessLane(currSeg, maxlane, isUpd);

    return true;
  }

  return false;
}

void Map::fuseLaneLineInternal(MapElement& priorEl, const MapElement& sensEl,
                       int priorBegin, int priorEnd, vector<bool>& isAssoc)
{
  assert(sensEl.isLine());
  // associate points in laneEl with points in elem
  point2arr_uncertain& priorGeom = priorEl.geometry;
  const point2arr_uncertain& sensedGeom = sensEl.geometry;

  // FIXME: this is probably fast, but still O(Nprior), we want the whole fusion be O(Nsensed)
  isAssoc.resize(priorGeom.size()); // true = priorGeom[j] was associated with something
  fill(isAssoc.begin(), isAssoc.end(), false);

  int inc = priorBegin <= priorEnd ? 1 : -1;
  priorEnd += inc; // let it be one beyond the end

  int i, j = 0;
  for(i = priorBegin; i != priorEnd; i += inc)
  {
    point2 avg(0, 0);
    int count = 0;

    point2 v = priorGeom[i] - sensedGeom[j];
    float dist2 = v.norm2();

    if (dist2 < DIST_THRES*DIST_THRES) {
      avg = avg + sensedGeom[j];
      count++;
    }

    while (j < int(sensedGeom.size()) - 1) {
      point2 next = priorGeom[i] - sensedGeom[j + 1];
      float distNext2 = next.norm2();
      if (distNext2 < dist2) {
        dist2 = distNext2;
        j++; 
        if (dist2 < DIST_THRES*DIST_THRES) {
          avg = avg + sensedGeom[j];
          count++;
        }
      } else {
        break;
      }
    }

    if (count > 0)
    {
      isAssoc[i] = true;
      avg = avg / count;
      // let the point only move along the normal
      point2 normal;
      if (i == 0) // beginning of the line (note: use 0 not 'begin')
        normal = priorGeom[1] - priorGeom[0];
      else if (i == int(priorGeom.size())-1) // end of the line (note: use size()-1 not 'end')
        normal = priorGeom[i] - priorGeom[i - 1];
      else // general case
        normal = priorGeom[i + 1] - priorGeom[i - 1];
      normal = point2(normal.y, -normal.x); // rotate by 90deg
      normal.normalize();

      avg = priorGeom[i] + normal * normal.dot(avg - priorGeom[i]);
      //MSG("sensed average: " << avg);
      priorGeom[i] = avg*DECAY + priorGeom[i]*(1-DECAY);
    }
  }

/*
  // associate points in laneEl with points in elem
  point2arr_uncertain& lane = priorEl.geometry;
  const point2arr_uncertain& sensed = sensEl.geometry;

  // list of associations of the type: (prior index, (list of sensed indexes))
  typedef pair<int, list<int> > assoc_pair_t;
  typedef list<assoc_pair_t> assoc_t;
  assoc_t assoc;
  assoc_t::iterator iter;
  assoc_pair_t* curass;
  isAssoc.resize(lane.size()); // true = lane[j] was associated with something
  fill(isAssoc.begin(), isAssoc.end(), false);

  for (unsigned int j = 0; j < sensed.size(); j++)
  {
    float mindist = 10000;
    int minidx = -1;
    int count = 0;
    // find all prior points within threshold distance and put them into 'assoc'
    for (unsigned int k = 0; k < lane.size(); k++)
    {
      assoc.push_back(assoc_pair_t(k, list<int>()));
      curass = &assoc.back();
      point2 vec = sensed[j] - lane[k];
      float distSq = vec.x*vec.x + vec.y*vec.y; // distance squared
      if (distSq < DIST_THRES*DIST_THRES)
      {
        curass->second.push_back(j); // associate k with j
        isAssoc[k] = true;
        count++;
      } else if (distSq < mindist * mindist) {
        minidx = k;
        mindist = sqrt(distSq);
      }
    }

    if (count == 0) {
      count = 1;
      isAssoc[minidx] = true;
      assoc.push_back(assoc_pair_t(minidx, list<int>()));
      curass = &assoc.back();
      curass->second.push_back(j); // associate k with j
    }

  } // end for (all sensed points)

  // use assoc array to correct prior data
  for (iter = assoc.begin(); iter != assoc.end(); ++iter)
  {
    point2_uncertain& ptlane = lane[iter->first];
    point2_uncertain ptsens(0, 0);

    // let the point only move along the normal
    point2 prev = lane[iter->first == 0 ? iter->first : iter->first-1];
    point2 next = lane[iter->first == (int(lane.size())-1) ? iter->first : iter->first+1];
    point2 normal = (next - prev);
    normal = point2(normal.y, -normal.x); // rotate by 90deg
    normal.normalize();

    // let ptsens be the average of all the sensed points associated with ptlane
    // projected on the normal of the line
    list<int>& sensAssoc = iter->second;
    if (sensAssoc.size() > 0)
    {
      //MSG("sensed points associated with " << ptlane << ":");
      for (list<int>::iterator sensIter = sensAssoc.begin();
           sensIter != sensAssoc.end();
           ++sensIter)
      {
        ptsens = ptsens + sensed[*sensIter];
        //cerr << sensed[*sensIter] << " ";
      }
      ptsens = ptsens / sensAssoc.size();
      ptsens = ptlane + normal * normal.dot(ptsens - ptlane);
      //MSG("sensed average: " << ptsens);
      // let the prior be updated using an exponential filter
      // (first order single pole filter, low pass filter, or whatever you want to call it)
      ptlane = ptlane*DECAY + ptsens*(1-DECAY);
    }
  }
*/

}

/// Given the sensed element representing a curb (right side only),
/// update the lane lines accordingly (all of them must be on the left
/// of the curb.
void Map::fuseCurb(const MapElement& curbEl)
{
  int currSeg, currLane, currPoint;
  getClosestSegment(curbEl.state, &currSeg, &currLane, &currPoint);
  
  if (currSeg == -1)
    return; // driving off-road?
  
  fuseCurb(curbEl, currSeg, currLane, currPoint);
}

// get around bug in curb perceptor, where curb is always parallel to alice heading
#define CURB_BUG 1

/// Fuse the curb with the given segment and lane. Faster than the
/// other version, that has to guess the current segment and lane
/// from the state in the MapElement.
void Map::fuseCurb(const MapElement& curbEl, int currSeg,
                   int currLane, int currPoint)
{
  // At the moment, the only useful information we get from the curb
  // perceptor is the distance of the curb to our right. We need the eq
  // of the straight line defining our boundary
  if (curbEl.geometry.size() == 0)
  {
    ERROR("Received empty curb!! (geometry.size() == 0)");
    return;
  }

  RNDFsegment& segm = prior.segments[currSeg];
  RNDFlane& lane = prior.lanes[segm.laneindex[currLane]];

  MapElement laneEl;
  prior.getElFull(laneEl, lane.rightbound_elindex);
  point2arr_uncertain& geom = laneEl.geometry;

  point2 curbStart; // the starting position of the known curb (from the back)
  point2 curbVec; // the direction of the curb (road is on left side)
  float curbLength; // the length, in meters, of the known curb

#if CURB_BUG
  // Get around bug in curb perceptor, where curb is always parallel to
  // alice heading.
  // Get the distance from the curb, and let the boundary be
  // parallel to the road, at that distance

  point2 alicePos(curbEl.state.localX, curbEl.state.localY);
  // vector pointing to Alice's right
  point2 rightVec(-sin(curbEl.state.localYaw), cos(curbEl.state.localYaw));
  float dist = rightVec.dot(curbEl.geometry[0] - alicePos);

  // get the tangent to the current segment and use that
  if (currPoint == 0)
    curbVec = geom[currPoint + 1] - geom[currPoint];
  else if (currPoint == int(geom.size())-1)
    curbVec = geom[currPoint] - geom[currPoint - 1];
  else
    curbVec = geom[currPoint + 1] - geom[currPoint - 1];

  curbVec.normalize();

  // craft the start pos given an arbitrary curb length
  curbLength = (curbEl.geometry[0] - curbEl.geometry.arr.back()).norm();
  //curbLength = 10; // MAGIC (but this is just a hack around a bug, it's ok)
  point2 curbCenter = rightVec*dist + alicePos;
  curbStart = curbCenter - curbVec*curbLength/2;

#else
  // at the moment, the curb is always straight, just use the
  // first and last points to define the line
  curbStart = curbEl.geometry[0];
  curbVec = curbEl.geometry[0] - curbEl.geometry.arr.back();
#warning "FIXME: actually check which side of the curb is alice!"
  curbLength = curbVec.norm();
  curbVec = curbVec / curbLength;
#endif

  vector<bool> isUpd(geom.size(), false);

  for (int k = 0; k < int(geom.size()); k++)
  {
    point2 vec = geom[k] - curbStart;
    // project the point onto the curb line
    float along = vec.dot(curbVec);
    if (along < 0 || along > curbLength)
      continue; // outside the curb region we want to fuse

    float dist = vec.cross(curbVec);
    if (dist < 0) {
      // bad, point is on the wrong side of the curb!!
      // project it back
      geom[k] = curbStart + curbVec*along;
      isUpd[k] = true;
    }
  }

  prior.setElFull(laneEl, lane.rightbound_elindex);
  // postprocess all lanes (update center, smoothing, width enforcement)
  for (int i = 0; i < int(segm.laneindex.size()); i++)
    postProcessLane(currSeg, i, isUpd);
  // then postprocess segment (non-overlapping lanes enforcement)
  postProcessSegment(currSeg);
}



int Map::getLane(LaneLabel & lanelabel, const point2& pt)
{
  point2arr centerline;
  unsigned int sze = prior.lanes.size();
  int retval;
  int index = -1;
  double dist=0; 
  double mindist = -1;
  LaneLabel thislabel,minlabel;
  point2 dpt,tmppt;
  for (unsigned int i = 0; i<sze; ++i){
    thislabel = prior.lanes[i].label;
    retval = getLaneCenterLine(centerline,thislabel);
    if (retval<0)
      continue;
    tmppt = centerline.project(pt);
    dpt = pt-tmppt;
    dist = dpt.norm();
    if (mindist<0){
      mindist = dist;
      minlabel = thislabel;
      index = i;
    }else if (dist<mindist){
      minlabel = thislabel;
      mindist=dist;
      index = i;
    }
    
  }
  lanelabel = minlabel;
  return index;
}

int Map::getStopLineSensed(point2_uncertain &pt, 
                           const PointLabel &ptlabelin)
{

  double alongthresh = 2;
  double edgethresh = 4;
  point2_uncertain priorpt;

  point2 stoppt,thispt,projectpt;
  point2arr midline,leftline,rightline;
  int i,numels,retval;
  
  //  getAllStopLines(labelarr,ptarr);
  if (!isStopLine(ptlabelin)){
    cerr <<"in Map::getStopLineSensed point label given " << ptlabelin << " is not a stopline" << endl;
    return -1;
  }
  retval = getWayPoint(priorpt,ptlabelin);
  numels = (int)data.size();

  // get the lane for the desired stopline
  LaneLabel lanelabel(ptlabelin.segment,ptlabelin.lane);
  getLaneCenterLine(midline,lanelabel);
  getLaneBounds(leftline,rightline,lanelabel);

  midline.set_extend(2*alongthresh);
  midline.set_extend(-2*alongthresh);

  leftline.set_extend(2*alongthresh);
  leftline.set_extend(-2*alongthresh);

  rightline.set_extend(2*alongthresh);
  rightline.set_extend(-2*alongthresh);

  

  int minindex = -1;
  double leftdist,rightdist;
  point2arr stopline;

  double distalong = alongthresh+1;
  // check all map elements for candidate stoplines
  for (i=0;i<numels;++i){
    if (data[i].type!=ELEMENT_STOPLINE)
      continue;

    stopline.set(data[i].geometry);
    stoppt.set(data[i].center);
    thispt.set(priorpt);

    //project the stopline into the lane midpoint
    projectpt = midline.project(stoppt);

    leftdist = leftline.get_min_dist_line(stopline);
    rightdist = rightline.get_min_dist_line(stopline);
    // check if stopline spans the lane within a threshold
    if (leftdist>edgethresh || rightdist>edgethresh)
      continue;

    // if the stopline is close enouch to the prior
    // line, then set this as a candidate stopline
    if(thispt.dist(projectpt)<distalong){
      distalong = thispt.dist(projectpt);
      minindex = i;
    }
   
  }
  
  // if a candidate sensed stopline was found, return it
  if (minindex>=0){
    pt = projectpt;//data[minindex].center;
    return 0;
  }
  
  pt = priorpt;
  return 1;
  
}

int Map::getIndexAlongLine(int &index, const point2arr & line, const double dist)
{
  cerr <<"getIndexAlongLine not implemented yet" << endl;
  index =0;
  return -1;
}   

int Map::getPointAlongLine(point2 &ptout, const point2arr & line, const double dist)
{
  ptout = line.get_point_along(dist);
  return 0;
}

int Map::getDistAlongLine(double &dist, const point2arr & line, const point2& pt)
{
  dist = line.project_along(pt);
  return 0;
}

int Map::getDistAlongLine(double &dist,const point2arr & line, const point2& pta, const point2& ptb) 
{
  dist = line.project_along(pta)-line.project_along(ptb);
  return 0;
}

int Map::getProjectToLine(point2 &projpt, const point2arr & line, const point2& pt)
{
  projpt = line.project(pt);
  return 0;
}

int Map::extendLine(point2arr &lineout, const point2arr & line, double dist)
{
  int sze = line.size();
  if (sze<2)
    return -1;
  
  lineout.set(line);
  point2 pta,ptb, dpt,newpt;
  double thisdist;
  if (dist<0){
    pta = line[0];
    ptb = line[1];
    dpt = ptb-pta;
    thisdist = dpt.norm();
    newpt = pta+dpt*dist/thisdist;
    lineout.insert(0,newpt);
  }else{
    pta = line[sze-1];
    ptb = line[sze-2];
    dpt = ptb-pta;
    thisdist = dpt.norm();
    newpt = pta-dpt*dist/thisdist;
    lineout.push_back(newpt);
  }
  return 0;
}


int Map::getObsNearby(vector<MapElement> &elarr,const point2 &pt, const double dist)
{
  MapElement thisEl;
  double thisdist;
  elarr.clear();
  point2 tmppt,dpt;
  point2arr ptarr;  

  //this is temporary, until I get spatial look-up working
  for(unsigned int i = 0; i < usedIndices.size(); i++) {
    int j = usedIndices.at(i);
    thisEl = newData[j].mergedMapElement;

    //no need to check isObstacle (all in newData are...)

    if (thisEl.isOverlap(pt)){
      elarr.push_back(thisEl);
      continue;
    }
    ptarr.set(thisEl.geometry);    
    dpt = pt-ptarr.project(pt);
    thisdist = dpt.norm();
    if (thisdist<dist){
      elarr.push_back(thisEl);
    }

  }

  return elarr.size();
}


int Map::getObsInLane(vector<MapElement> &elarr,const LaneLabel &lanelabelin)
{
  MapElement thisEl;
  elarr.clear();
  point2 tmppt,dpt;
  point2arr bounds;
  int retval = getLaneBoundsPoly(bounds,lanelabelin);
  if (retval<0) {
    //    MSG("getLaneBoundsPoly returned %d. exiting getObsInLane", retval);
    return retval;
  }

  //  MSG("using new data structure in getObsInLane");
  for (unsigned int i=0; i<usedIndices.size(); i++){
    int j = usedIndices.at(i);
    thisEl = newData[j].mergedMapElement;
    //    MSG("in getObsInlane, obj %d has area %f ", j, point2arr(newData[j].mergedMapElement.geometry).get_poly_area());
    if (thisEl.isOverlap(bounds)){
      elarr.push_back(thisEl);
      continue;
    }
  }

  return elarr.size(); 
}

int Map::getObsInZone(vector<MapElement> &elarr,int zonelabelin)
{
  MapElement thisEl;
  elarr.clear();
  point2 tmppt,dpt;
  point2arr bounds;
  int retval = getZonePerimeter(bounds,zonelabelin);
  if (retval<0) {

    return retval;
  }

  //  MSG("using new data structure in getObsInLane");
  for (unsigned int i=0; i<usedIndices.size(); i++){
    int j = usedIndices.at(i);
    thisEl = newData[j].mergedMapElement;
    //    MSG("in getObsInlane, obj %d has area %f ", j, point2arr(newData[j].mergedMapElement.geometry).get_poly_area());
    if (thisEl.isOverlap(bounds)){
      elarr.push_back(thisEl);
      continue;
    }
  }

  return elarr.size(); 
}



int Map::getObsInBounds(vector<MapElement> &elarr, const point2arr &leftbound, const point2arr &rightbound)
{
  MapElement thisEl;

  elarr.clear();
  point2 tmppt,dpt;
  point2arr bounds;
  point2arr tmpbound;
  tmpbound = rightbound;
  tmpbound.reverse();
  bounds = leftbound;
  bounds.connect(tmpbound);


  //this is temporary, until I get spatial look-up working
  for(unsigned int i = 0; i < usedIndices.size(); i++) {
    int j = usedIndices.at(i);
    thisEl = newData[j].mergedMapElement;

    if (thisEl.isOverlap(bounds)){
      elarr.push_back(thisEl);
      continue;
    }
  }

  return elarr.size(); 

}

int Map::getObsInPoly(vector<MapElement> &elarr, const point2arr &poly)
{
  MapElement thisEl;

  elarr.clear();

  //this is temporary, until I get spatial look-up working
  for(unsigned int i = 0; i < usedIndices.size(); i++) {
    int j = usedIndices.at(i);
    thisEl = newData[j].mergedMapElement;

    if (thisEl.isOverlap(poly)){
      elarr.push_back(thisEl);
      continue;
    }
  }

  return elarr.size(); 

}



int Map::getEl(MapElement &el, const MapId &id)
{
  MapElement thisEl;
  unsigned int sze = data.size();
  
  for (unsigned int i=0; i<sze; ++i){
    thisEl = data[i];
    
    if (thisEl.id==id){
      el =thisEl;
      return 0;
    }
  }
  return -1;
    
 
}

 
//--------------------------------------------------
//  These functions need to be updated and the interface
// may change slightly
//--------------------------------------------------


int Map::getHeading(double & ang,const PointLabel &label)
{
  int retval;
  point2 pt;
  retval = getWayPoint(pt,label);
  if (retval<0){
    cerr << "in Map::getHeading, point label " << label << " not found" << endl;
  }
  
  return getHeading(ang,pt);

}
int Map::getHeading(double & ang,const point2 &pt)
{

  double delta = .5;
  point2arr centerline;

  LaneLabel lanelabel;
  
  getLane(lanelabel,pt);

  getLaneCenterLine(centerline,lanelabel);


  point2 projpt,frontpt,backpt;

  projpt = centerline.project(pt);

  double dist,newdist;

  getDistAlongLine(dist,centerline,projpt);

  point2 newpt,dpt;

  getPointAlongLine(newpt,centerline,dist+delta);
  getDistAlongLine(newdist,centerline,newpt);

  if (newdist-dist < delta/2) {
    getPointAlongLine(newpt,centerline,dist-delta);
    frontpt = projpt;
    backpt = newpt;
  }else{
    frontpt = newpt;
    backpt = projpt;
  }    

  dpt = frontpt-backpt;

  ang = dpt.heading();
  return 0;
}

int Map::getHeading(double & ang, point2 &ptout, const LaneLabel &label, const point2 &pt)
{
  double delta = .5;
  point2arr centerline;

  getLaneCenterLine(centerline,label);


  point2 projpt,frontpt,backpt;

  projpt = centerline.project(pt);

  double dist,newdist;

  getDistAlongLine(dist,centerline,projpt);

  point2 newpt,dpt;

  getPointAlongLine(newpt,centerline,dist+delta);
  getDistAlongLine(newdist,centerline,newpt);

  if (newdist-dist < delta/2) {
    getPointAlongLine(newpt,centerline,dist-delta);
    frontpt = projpt;
    backpt = newpt;
  }else{
    frontpt = newpt;
    backpt = projpt;
  }    

  dpt = frontpt-backpt;

  ang = dpt.heading();
  ptout = newpt;
  return 0;
}

int Map::getLeftBoundType(string& type, const LaneLabel &label)
{
  unsigned int i;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label==label){
      type = prior.lanes[i].leftbound_type;
      return 0;
    }
  }
  return -1;
}
int Map::getRightBoundType(string& type, const LaneLabel &label )
{
  unsigned int i;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      type = prior.lanes[i].rightbound_type;
      return 0;
    }
  }
  return -1;

}










 

int Map::getTransitionBounds(point2arr &leftbound ,point2arr &rightbound , const PointLabel &exitlabel, const PointLabel &enterlabel, const point2 state, const double range, const double backrange)
{
  unsigned int i,j;
  int exit_elindex =-1;
  int exit_leftelindex =-1;
  int exit_rightelindex =-1;
  int exit_ptindex =-1;
  int enter_elindex =-1;
  int enter_leftelindex =-1;
  int enter_rightelindex =-1;

  int enter_ptindex =-1;
  
  point2 exitpt;
  point2 enterpt;
      
  bool samelaneflag = false;
  
  bool isvalid = false;
      

  LaneLabel exit_lanelabel, enter_lanelabel;

  exit_lanelabel.segment = exitlabel.segment;
  exit_lanelabel.lane = exitlabel.lane;

  enter_lanelabel.segment = enterlabel.segment;
  enter_lanelabel.lane = enterlabel.lane;

  PointLabel ptlabel(exitlabel);
  ptlabel.point = ptlabel.point+1;
      
  if (exit_lanelabel == enter_lanelabel){
    samelaneflag = true;
  }
  //      cout << "TRANSITION FROM " << exitlabel << " TO " <<ptlabel << endl;
  if (enterlabel == ptlabel){
    int retval = getBounds(leftbound, rightbound, enter_lanelabel,state, range,backrange);
    //cout << "returning current lane transition" << endl;
    return retval;
  }

  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label== exitlabel){
    
      for (j=0;j<prior.lanes[i].exit_label.size();++j){
        if (prior.lanes[i].exit_label[j]==exitlabel &&
            prior.lanes[i].exit_link[j]==enterlabel){
          

          isvalid = true;
        }
      }
      if (!isvalid)
        continue;
      
      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==exitlabel){  exit_elindex = prior.lanes[i].waypoint_elindex;
        exit_leftelindex = prior.lanes[i].leftbound_elindex;
        exit_rightelindex = prior.lanes[i].rightbound_elindex;
        exit_ptindex =j;
        
        break;
        }
      }
    }
  }
  if (!isvalid){
    cerr << "in Map::getTransitionBounds, invalid transition, exit = " << exitlabel << " enter = " << enterlabel <<endl;
    return -1;

  }
  isvalid = false;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label== enterlabel){
    
      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==enterlabel){
          enter_elindex = prior.lanes[i].waypoint_elindex;
          enter_leftelindex = prior.lanes[i].leftbound_elindex;
          enter_rightelindex = prior.lanes[i].rightbound_elindex;
          enter_ptindex =j;
          isvalid = true;
          break;
        }
      }
    }
  }

  if (!isvalid){
    cerr << "in Map::getTransitionBounds, invalid transition for an unexpected reason.  Likely error in RNDF parsing.  exit = " << exitlabel << " enter = " << enterlabel <<endl;
    return -1;
  }

  MapElement enter_leftel, enter_rightel;
  MapElement exit_leftel, exit_rightel;
  point2arr enter_leftarr, enter_rightarr;
  point2arr exit_leftarr, exit_rightarr;
  point2 enterpt_left, enterpt_right;
  point2 exitpt_left, exitpt_right;


  prior.getEl(enter_leftel,enter_leftelindex);
  enter_leftarr.set(enter_leftel.geometry);

  prior.getEl(enter_rightel,enter_rightelindex);
  enter_rightarr.set(enter_rightel.geometry);

  prior.getEl(exit_leftel,exit_leftelindex);
  exit_leftarr.set(exit_leftel.geometry);

  prior.getEl(exit_rightel,exit_rightelindex);
  exit_rightarr.set(exit_rightel.geometry);


      
  exitpt_left = exit_leftarr[exit_ptindex];  
  exitpt_right = exit_rightarr[exit_ptindex];  

  enterpt_left = enter_leftarr[enter_ptindex];  
  enterpt_right = enter_rightarr[enter_ptindex];  
  

  prior.getElFull(enter_leftel,enter_leftelindex);
  enter_leftarr.set(enter_leftel.geometry);

  prior.getElFull(enter_rightel,enter_rightelindex);
  enter_rightarr.set(enter_rightel.geometry);

  prior.getElFull(exit_leftel,exit_leftelindex);
  exit_leftarr.set(exit_leftel.geometry);

  prior.getElFull(exit_rightel,exit_rightelindex);
  exit_rightarr.set(exit_rightel.geometry);
 

  point2arr new_leftarr;
  point2arr new_rightarr;


  point2arr tmpexleft(exit_leftarr);
  point2arr tmpexright(exit_rightarr);
  point2arr tmpenleft(enter_leftarr);
  point2arr tmpenright(enter_rightarr);


      
  exit_leftarr.cut_front(exitpt_left);
  exit_rightarr.cut_front(exitpt_right);
     
  samelaneflag =false;

  if (samelaneflag){
    exit_leftarr.cut_back(enterpt_left);
    exit_rightarr.cut_back(enterpt_right);
  }else{
    exit_leftarr.cut_back_at_index(10);
    exit_rightarr.cut_back_at_index(10);

  }


  enter_leftarr.cut_back(enterpt_left);
  enter_rightarr.cut_back(enterpt_right);
      
  if (samelaneflag){
    enter_leftarr.cut_front(exitpt_left);
    enter_rightarr.cut_front(exitpt_right);
  }else{
    enter_leftarr.cut_front_at_index(10);
    enter_rightarr.cut_front_at_index(10);

  }

  new_leftarr.set(exit_leftarr);
  new_rightarr.set(exit_rightarr);

  double distleft = exitpt_left.dist(enterpt_left);
  double distright = exitpt_right.dist(enterpt_right);
  point2 midpt;


  //--------------------------------------------------
  // merging lane lines
  //--------------------------------------------------
  //      double distlr = fabs(distleft-distright);
  //      double estang = 2*atan2(distlr/2,exitpt_left.dist(exitpt_right));
  //cout << "est ang = " << estang << endl;

  int szeexit = new_leftarr.size();
  //      int szeenter = enter_leftarr.size();
  point2 dptexit, dptenter;
  dptexit= new_leftarr[szeexit-1] - new_leftarr[szeexit-2];
  dptenter= enter_leftarr[1]-enter_leftarr[0];
      
  double angexit = dptexit.heading();
  double angenter = dptenter.heading();

  double dang = fabs(acos(cos(angexit-angenter)));

      
  if (dang < .25){
    //cout << "GOING STRAIGHT" << endl;
    new_leftarr.connect(enter_leftarr);
    new_rightarr.connect(enter_rightarr);
        
  }else{
    if (distleft<distright){
      if (enter_rightarr.size()>0){
        midpt = (new_leftarr.back()+enter_leftarr[0])/2;
        new_leftarr.push_back(midpt);
        new_leftarr.connect(enter_leftarr);
        new_rightarr.connect_intersect(enter_rightarr);
      }
    }else{
      if (enter_leftarr.size()>0){
        midpt = (new_rightarr.back()+enter_rightarr[0])/2;
        new_rightarr.push_back(midpt);
        new_leftarr.connect_intersect(enter_leftarr);
        new_rightarr.connect(enter_rightarr);
      }
    }
  }
  //  waypts = waypt_el.geometry;
  //  if (left_neardist<0)
  //     left_neardist = 0;

  // if (right_neardist<0)
  //     right_neardist = 0;
  //      double BACK_RANGE = 5;  

      
  double thisrange  = range;
  double thisbackrange = backrange;
  if (range<0)
    thisrange = 1000000;
  if (backrange<0)
    thisbackrange = 1000000;
      

  int cutleftbacknum = 0;
  int cutrightbacknum = 0;
  int cutleftfrontnum = 0;
  int cutrightfrontnum = 0;
      

     
     
  cutleftbacknum = new_leftarr.cut_back_at_index(state,thisbackrange);
  cutrightbacknum = new_rightarr.cut_back_at_index(cutleftbacknum);
     
   
  
  cutleftfrontnum = new_leftarr.cut_front_at_index(state,thisrange);
  cutrightfrontnum = new_rightarr.cut_front_at_index(cutleftfrontnum);
   
  //       if (distleft>distright){
  //     new_leftarr.cut_front(state,thisrange);
  // new_rightarr.cut_front(new_leftarr.back());
  // }else{
  //  new_rightarr.cut_front(state,thisrange);
  //  new_leftarr.cut_front(new_rightarr.back());
  // }

      
  leftbound.set(new_leftarr);
  rightbound.set(new_rightarr);
  return 0; 

 
}




 
int Map::getShortTransitionBounds(point2arr &leftbound ,point2arr &rightbound , const PointLabel &exitlabel, const PointLabel &enterlabel)
{
  unsigned int i,j;
  int exit_elindex =-1;
  int exit_leftelindex =-1;
  int exit_rightelindex =-1;
  int exit_ptindex =-1;
  int enter_elindex =-1;
  int enter_leftelindex =-1;
  int enter_rightelindex =-1;

  int enter_ptindex =-1;
  
  point2 exitpt;
  point2 enterpt;
      
  bool samelaneflag = false;
  
  bool isvalid = false;
      
  point2arr new_leftarr;
  point2arr new_rightarr;

  MapElement enter_leftel, enter_rightel;
  MapElement exit_leftel, exit_rightel;
  point2arr enter_leftarr, enter_rightarr;
  point2arr exit_leftarr, exit_rightarr;
  point2 enterpt_left, enterpt_right;
  point2 exitpt_left, exitpt_right;

  int cutleftbacknum = 0;
  int cutrightbacknum = 0;
  int cutleftfrontnum = 0;
  int cutrightfrontnum = 0;


  LaneLabel exit_lanelabel, enter_lanelabel;

  exit_lanelabel.segment = exitlabel.segment;
  exit_lanelabel.lane = exitlabel.lane;

  enter_lanelabel.segment = enterlabel.segment;
  enter_lanelabel.lane = enterlabel.lane;

  PointLabel ptlabel(exitlabel);
  ptlabel.point = ptlabel.point+1;
      
  if (exit_lanelabel == enter_lanelabel){
    samelaneflag = true;
  }
  //      cout << "TRANSITION FROM " << exitlabel << " TO " <<ptlabel << endl;
     

  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label== exitlabel){
    
      for (j=0;j<prior.lanes[i].exit_label.size();++j){
        if (prior.lanes[i].exit_label[j]==exitlabel &&
            prior.lanes[i].exit_link[j]==enterlabel){
          

          isvalid = true;
        }
      }
      if (!isvalid)
        continue;
      
      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==exitlabel){  exit_elindex = prior.lanes[i].waypoint_elindex;
        exit_leftelindex = prior.lanes[i].leftbound_elindex;
        exit_rightelindex = prior.lanes[i].rightbound_elindex;
        exit_ptindex =j;
        
        break;
        }
      }
    }
  }
  if (!isvalid){
    cerr << "in Map::getTransitionBounds, invalid transition, exit = " << exitlabel << " enter = " << enterlabel <<endl;
    return -1;

  }
  isvalid = false;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label== enterlabel){
    
      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==enterlabel){
          enter_elindex = prior.lanes[i].waypoint_elindex;
          enter_leftelindex = prior.lanes[i].leftbound_elindex;
          enter_rightelindex = prior.lanes[i].rightbound_elindex;
          enter_ptindex =j;
          isvalid = true;
          break;
        }
      }
    }
  }

  if (!isvalid){
    cerr << "in Map::getTransitionBounds, invalid transition for an unexpected reason.  Likely error in RNDF parsing.  exit = " << exitlabel << " enter = " << enterlabel <<endl;
    return -1;
  }

      


  prior.getEl(enter_leftel,enter_leftelindex);
  enter_leftarr.set(enter_leftel.geometry);

  prior.getEl(enter_rightel,enter_rightelindex);
  enter_rightarr.set(enter_rightel.geometry);

  prior.getEl(exit_leftel,exit_leftelindex);
  exit_leftarr.set(exit_leftel.geometry);

  prior.getEl(exit_rightel,exit_rightelindex);
  exit_rightarr.set(exit_rightel.geometry);


      
  exitpt_left = exit_leftarr[exit_ptindex];  
  exitpt_right = exit_rightarr[exit_ptindex];  

  enterpt_left = enter_leftarr[enter_ptindex];  
  enterpt_right = enter_rightarr[enter_ptindex];  
  

  prior.getElFull(enter_leftel,enter_leftelindex);
  enter_leftarr.set(enter_leftel.geometry);

  prior.getElFull(enter_rightel,enter_rightelindex);
  enter_rightarr.set(enter_rightel.geometry);

  prior.getElFull(exit_leftel,exit_leftelindex);
  exit_leftarr.set(exit_leftel.geometry);

  prior.getElFull(exit_rightel,exit_rightelindex);
  exit_rightarr.set(exit_rightel.geometry);
 

  if (enterlabel == ptlabel){
    int retval = getLaneBounds(new_leftarr, new_rightarr, enter_lanelabel);
        
        

    cutleftbacknum =new_leftarr.cut_back_at_index(enterpt_left,1);
    cutrightbacknum = new_rightarr.cut_back_at_index(cutleftbacknum);      
        
        
    cutleftfrontnum = new_leftarr.cut_front_at_index(exitpt_left,1);
    cutrightfrontnum = new_rightarr.cut_front_at_index(cutleftfrontnum);
    leftbound.set(new_leftarr);
    rightbound.set(new_rightarr);
    //cout << "returning current lane transition" << endl;

    return retval;
  }


  point2arr tmpexleft(exit_leftarr);
  point2arr tmpexright(exit_rightarr);
  point2arr tmpenleft(enter_leftarr);
  point2arr tmpenright(enter_rightarr);


      
  exit_leftarr.cut_front(exitpt_left);
  exit_rightarr.cut_front(exitpt_right);
     
  samelaneflag =false;

  if (samelaneflag){
    exit_leftarr.cut_back(enterpt_left);
    exit_rightarr.cut_back(enterpt_right);
  }else{
    exit_leftarr.cut_back_at_index(10);
    exit_rightarr.cut_back_at_index(10);

  }


  enter_leftarr.cut_back(enterpt_left);
  enter_rightarr.cut_back(enterpt_right);
      
  if (samelaneflag){
    enter_leftarr.cut_front(exitpt_left);
    enter_rightarr.cut_front(exitpt_right);
  }else{
    enter_leftarr.cut_front_at_index(10);
    enter_rightarr.cut_front_at_index(10);

  }

  new_leftarr.set(exit_leftarr);
  new_rightarr.set(exit_rightarr);

  double distleft = exitpt_left.dist(enterpt_left);
  double distright = exitpt_right.dist(enterpt_right);
  point2 midpt;


    

  int szeexit = new_leftarr.size();
  //int szeenter = enter_leftarr.size();
  point2 dptexit, dptenter;
  dptexit= new_leftarr[szeexit-1] - new_leftarr[szeexit-2];
  dptenter= enter_leftarr[1]-enter_leftarr[0];
      
  double angexit = dptexit.heading();
  double angenter = dptenter.heading();

  double dang = fabs(acos(cos(angexit-angenter)));

  if (dang < .25  || dang >2.5){
    //cout << "GOING STRAIGHT" << endl;
    new_leftarr.connect(enter_leftarr);
    new_rightarr.connect(enter_rightarr);
        
  }else{
    if (distleft<distright){
      if (enter_rightarr.size()>0){
        midpt = (new_leftarr.back()+enter_leftarr[0])/2;
        new_leftarr.push_back(midpt);
        new_leftarr.connect(enter_leftarr);
        new_rightarr.connect_intersect(enter_rightarr);
      }
    }else{
      if (enter_leftarr.size()>0){
        midpt = (new_rightarr.back()+enter_rightarr[0])/2;
        new_rightarr.push_back(midpt);
        new_leftarr.connect_intersect(enter_leftarr);
        new_rightarr.connect(enter_rightarr);
      }
    }
  }

 

      
  point2arr tmpptarr(new_leftarr);
      
     
  cutleftbacknum =tmpptarr.cut_back_at_index(enterpt_left,1);
  //cout << "cutleftbacvk =  " <<cutleftbacknum << endl;     
  if (dang < 0.25 || dang >2.5)
    cutleftbacknum = new_leftarr.cut_back_at_index(cutleftbacknum);
  else 
    cutleftbacknum = new_leftarr.cut_back_at_index(cutleftbacknum+1);
  //cout << "cutleftbacvk2 =  " <<cutleftbacknum << endl;    
  cutrightbacknum = new_rightarr.cut_back_at_index(cutleftbacknum);      
     

  tmpptarr.set(new_leftarr);
  cutleftfrontnum = tmpptarr.cut_front_at_index(exitpt_left);
  if (dang <0.25 || dang >2.5)
    cutleftfrontnum = new_leftarr.cut_front_at_index(cutleftfrontnum);
  else
    cutleftfrontnum = new_leftarr.cut_front_at_index(cutleftfrontnum+1);

  //cout << "cutleftfrontk =  " <<cutleftfrontnum << endl;  
  cutrightfrontnum = new_rightarr.cut_front_at_index(cutleftfrontnum);
   

      
  leftbound.set(new_leftarr);
  rightbound.set(new_rightarr);
  return 0; 

 
}














//--------------------------------------------------
//  These functions have reasonable replacements using
// the newer functions
//--------------------------------------------------

bool Map::checkLaneID(const point2 pt, const LaneLabel &label)
{  return checkLaneID(pt, label.segment, label.lane);}

bool Map::checkLaneID(const point2 pt, const int segment, const int lane)
{
  LaneLabel labelin(segment,lane);
  point2_uncertain ptunc(pt);
  return isPointInLane(ptunc,labelin);

}


int Map::getSegmentID(const point2 pt)
{
  PointLabel label;
  int retval = getNextPointID(label,pt);
  if (retval<0){
    cerr << "in Map::getSegmentID : no matches found for point " << pt << endl;
    return -1;
  }
  
  return label.segment;

}

int Map::getLaneID(const point2 pt)
{

  PointLabel label;
  int retval = getNextPointID(label,pt);
  if (retval<0){
    cerr << "in Map::getLaneID : no matches found for point " << pt << endl;
    return -1;
  }
  
  return label.lane;

}

int Map::getClosestPointID(PointLabel &label, const point2 &pt)
{
  unsigned int i,j;
  MapElement el;
  int elindex =-1;
  point2 thispt,minpt;
  PointLabel minlabel;
  double mindist = -1;
  double thisdist = 0;
  for (i=0;i<prior.lanes.size(); ++i){
    elindex = prior.lanes[i].waypoint_elindex;
    prior.getEl(el,elindex);
    for (j=0;j<el.geometry.size();++j){
      thispt = el.geometry[j];
      thisdist = pt.dist(thispt);
      if ((thisdist < mindist) || mindist<0){
        minlabel = prior.lanes[i].waypoint_label[j];
        minpt = thispt;
        mindist = thisdist;
      }
    }
  }

  for (i=0;i<prior.zones.size(); ++i){
    elindex = prior.zones[i].perimeter_elindex;
    prior.getEl(el,elindex);
    for (j=0;j<el.geometry.size();++j){
      thispt = el.geometry[j];
      thisdist = pt.dist(thispt);
      if ((thisdist < mindist) || mindist<0){
        minlabel = prior.zones[i].perimeter_label[j];
        minpt = thispt;
        mindist = thisdist;
      }
    }
  }

  if (mindist<0){
    cerr << "in Map::getClosestPointID no point found " << endl;
    return -1;
  }

  label = minlabel;
  return 0;

}

        
int Map::getNextPointID(PointLabel &label,const point2& pt)
{
  unsigned int i;
  int left_index;
  int right_index;
  int left_side;
  int right_side;
  int left_elindex,right_elindex;
  MapElement left_el;
  MapElement right_el;
  point2arr left_lane, right_lane;
  //  bool found=false;
  int ptindex;
  double distalong;
  

  for (i=0;i<prior.lanes.size();++i){
   
    left_elindex = prior.lanes[i].leftbound_elindex;
    right_elindex = prior.lanes[i].rightbound_elindex;
    
    prior.getEl(left_el,left_elindex);
    prior.getEl(right_el,right_elindex);
    left_lane=left_el.geometry;
    right_lane=right_el.geometry;
    left_lane.get_side(pt,left_side,left_index);
    right_lane.get_side(pt,right_side,right_index);

    if (left_side!=right_side){
      //  cout << "LANE NUMBER " << i << " label =  " << prior.lanes[i].label << endl;
      // cout << "left_side = " << left_side << " right_side = " << right_side << endl;
      // cout << "left_index = " << left_index << " right_index = " << right_index << endl;
      // cout << "left_size = " << left_lane.size() << " right_size = " << right_lane.size() << endl;

      //  if (left_index>0 &&right_index>0 &&left_index<(double)(left_lane.size()-1) && right_index< (double) (right_lane.size()-1)){
      //cout << "leftindex = " << left_index 
      //     << " rightindex = " << right_index << endl;
      if (left_index==0 &&right_index==0){
    

        //--------------------------------------------------
        // should use center lane
        //--------------------------------------------------
        distalong =left_lane.project_along(pt);
        ptindex = left_lane.get_index_next(distalong);
        //       if (left_index>right_index)
        //   ptindex = (int)ceil(left_index);
        // else
        //  ptindex = (int)ceil(right_index);
      
        label = prior.lanes[i].waypoint_label[ptindex];
        return 0;
      }
    }
    
  }
  // cerr << "in Map::getNextPointID no point found " << endl;
  return -1;
}

int Map::getLastPointID(PointLabel &label, const point2& pt)
{
  unsigned int i;
  int left_index;
  int right_index;
  int left_side;
  int right_side;
  int left_elindex,right_elindex;
  MapElement left_el;
  MapElement right_el;
  point2arr left_lane, right_lane;
  //  bool found=false;
  int ptindex;
  double distalong ;
  for (i=0;i<prior.lanes.size();++i){
    left_elindex = prior.lanes[i].leftbound_elindex;
    right_elindex = prior.lanes[i].rightbound_elindex;
    prior.getEl(left_el,left_elindex);
    prior.getEl(right_el,right_elindex);
    left_lane=left_el.geometry;
    right_lane=right_el.geometry;
    left_lane.get_side(pt,left_side,left_index);
    right_lane.get_side(pt,right_side,right_index);
    if (left_side!=right_side){
      //      if (left_index>0 &&right_index>0 &&left_index<(double)(left_lane.size()-1) && right_index< (double) (right_lane.size()-1)){
      if (left_index==0 &&right_index==0){
        //--------------------------------------------------
        // should use center lane
        //--------------------------------------------------
        distalong =left_lane.project_along(pt);
        ptindex = left_lane.get_index_next(distalong)-1;
        //   if (left_index>right_index)
        //   ptindex = (int)floor(left_index);
        // else
        //  ptindex = (int)floor(right_index);
      
        label = prior.lanes[i].waypoint_label[ptindex];
        return 0;
      }
    }
    
  }
  // cerr << "in Map::getLastPointID no point found " << endl;
  return -1;
}


int Map::getLaneCenterPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset)
{

  unsigned int i;
  MapElement el;
  //  double left_index;
  //double right_index;
  //  double left_index2;
  // double right_index2;
  //int left_side;
  //int right_side;
  int waypt_elindex;
  MapElement left_el;
  MapElement right_el;
  MapElement waypt_el;
  point2arr left_lane, right_lane, waypts;

  bool validlabel = false;
  
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      waypt_elindex = prior.lanes[i].waypoint_elindex;
      prior.getEl(waypt_el,waypt_elindex);
      waypts=waypt_el.geometry;
      validlabel = true;
      break;
    }
  }
  if(!validlabel){
    cerr << "in Map::getLaneCenterPoint() passed bad lane label "<<label <<endl;
    return -1;
  }
  


  waypts = waypt_el.geometry;
  double statedist = waypts.project_along(pt);

  double newdist = statedist+offset;

  waypts.cut_front(newdist);//waypts.linelength()-neardist);
  cpt = waypts.back();

  return 0;

}



int Map::getLaneRightPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset)
{

  unsigned int i;
  MapElement el;
  int right_elindex;
  MapElement right_el;
  point2arr rightpts;

  bool validlabel = false;
  
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      right_elindex = prior.lanes[i].rightbound_elindex;
      prior.getEl(right_el,right_elindex);
      validlabel = true;
      break;
    }
  }
  if(!validlabel){
    cerr << "in Map::getLaneRightPoint() passed bad lane label "<<label <<endl;
    return -1;
  }
  
  rightpts = right_el.geometry;
  double statedist = rightpts.project_along(pt);

  double newdist = statedist+offset;

  rightpts.cut_front(newdist);
  cpt = rightpts.back();

  return 0;
}



int Map::getLaneLeftPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset)
{

  unsigned int i;
  MapElement el;
  int left_elindex;
  MapElement left_el;
  point2arr leftpts;

  bool validlabel = false;
  
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      left_elindex = prior.lanes[i].leftbound_elindex;
      prior.getEl(left_el,left_elindex);
      validlabel = true;
      break;
    }
  }
  if(!validlabel){
    cerr << "in Map::getLaneLeftPoint() passed bad lane label "<<label <<endl;
    return -1;
  }
  
  leftpts = left_el.geometry;
  double statedist = leftpts.project_along(pt);

  double newdist = statedist+offset;

  leftpts.cut_front(newdist);
  cpt = leftpts.back();

  return 0;
}





int Map::getLeftBound(point2arr& ptarr, const LaneLabel &label )
{
  unsigned int i;
  MapElement el;
  int elindex =-1;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].leftbound_elindex;
      prior.getElFull(el,elindex);
      ptarr.set(el.geometry);
    }
  }
  return 0;
}

int Map::getRightBound(point2arr& ptarr, const LaneLabel &label )
{
  unsigned int i;
  MapElement el;
  int elindex =-1;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].rightbound_elindex;
      prior.getElFull(el,elindex);
      ptarr.set(el.geometry);
    }
  }
  return 0;
}


int Map::getZonePerimeter(point2arr& ptarr, int zoneLabel)
{
  unsigned int i;
  MapElement el;
  int elindex =-1;
  for (i=0;i<prior.zones.size(); ++i){
    if (prior.zones[i].label == zoneLabel){
      elindex = prior.zones[i].perimeter_elindex;
      prior.getElFull(el,elindex);
      ptarr.set(el.geometry);
      return 0;
    }
  }
  return -1; // not found -- inexistent label
}

int Map::getZoneParkingSpots(vector<SpotLabel>& spotlabelarr, int zoneLabel)
{
  unsigned int i;
  MapElement el;

  for (i=0;i<prior.zones.size(); ++i){
    if (prior.zones[i].label == zoneLabel){
      spotlabelarr = prior.zones[i].spot_label;
      return 0;
    }
  }
  return -1; // not found -- inexistent label
}


int Map::getSpotWaypoints(point2& first, point2& second, const SpotLabel& spotLabel)
{
  unsigned int i;
  MapElement el;
  int elindex =-1;
  for (i=0;i<prior.spots.size(); ++i){
    if (prior.spots[i].label == spotLabel){
      elindex = prior.spots[i].waypt_elindex;
      prior.getElFull(el,elindex);
      assert(el.geometry.size() >= 2);
      if (el.geometry.size() > 2)
          cerr << "Map::getSpotBound():: WARNING: More than 2 waypoints for a parking spot" << endl;
      first = el.geometry[0];
      second = el.geometry[1];
      return 0;
    }
  }
  return -1; // not found -- inexistent label
}

int Map::getSpotWidth(double &width, const SpotLabel& spotLabel)
{
  unsigned int i;
  MapElement el;

  for (i=0;i<prior.spots.size(); ++i){
    if (prior.spots[i].label == spotLabel){
      width = prior.spots[i].width;
      return 0;
    }
  }
  return -1; // not found -- inexistent label
}


bool Map::isExit(PointLabel &label){
  unsigned int i,j;
  //  int elindex =-1;
  //  int ptindex =-1;
  //bool isexit = false;
  //MapElement el;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      //elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].exit_label.size();++j){
        if (prior.lanes[i].exit_label[j]==label){
          return true;
        }
      }
    }
  }
  return false;
}

bool Map::isStop(PointLabel &label){
  unsigned int i,j;
  //  int elindex =-1;
  //  int ptindex =-1;
  //  bool isstop = false;
  //MapElement el;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      //elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].stop_label.size();++j){
        if (prior.lanes[i].stop_label[j]==label){
          return true;
        }
      }
    }
  }
  return false;
}

int Map::getStopline(point2& pt, PointLabel &label)
{
  unsigned int i,j;
  int elindex =-1;
  int ptindex =-1;
  bool isstop = false;
  MapElement el;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].stop_label.size();++j){
        if (prior.lanes[i].stop_label[j]==label){
          isstop = true;
        }
      }
      if (!isstop)
        continue;

      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==label){
          
          ptindex =j;
          prior.getEl(el,elindex);
          pt = el.geometry[ptindex];
          return 0;
        }
      }
    }
  }
  cerr << "in Map::getStopline, stop line not found" << label << endl;
  
  return -1;
}
int Map::getStopline(point2& pt, point2 state)
{
  unsigned int i,j;
  int elindex =-1;
  int ptindex =-1;
  //  bool isstop = false;
  MapElement el;

  PointLabel label, stoplabel;
  
  int retval = getNextPointID(label,state);
  if (retval){
    return retval;
  }
  
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].stop_label.size();++j){
        if (prior.lanes[i].stop_label[j].point>=label.point){
          stoplabel = prior.lanes[i].stop_label[j];
          break;
        }
      }

      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==stoplabel){
          
          ptindex =j;
          prior.getEl(el,elindex);
          pt = el.geometry[ptindex];
          return 0;
        }
      }
    }
  }
  cerr << "in Map::getStopline, stop line not found" << label << endl;
  
  return -1;
}

int Map::getNextStopline(point2& pt, PointLabel &label)
{
  unsigned int i,j;
  int elindex =-1;
  int ptindex =-1;
  //  bool isstop = false;
  MapElement el;

  PointLabel stoplabel;
  

  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].stop_label.size();++j){
        if (prior.lanes[i].stop_label[j].point>=label.point){
          stoplabel = prior.lanes[i].stop_label[j];
          break;
        }
      }

      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==stoplabel){
          
          ptindex =j;
          prior.getEl(el,elindex);
          pt = el.geometry[ptindex];
          label = stoplabel;
          return 0;
        }
      }
    }
  }
  cerr << "in Map::getNextStopline, stop line not found" << label << endl;
  
  return -1;
}



int Map::getWaypoint(point2& pt, const PointLabel &label)
{
  unsigned int i,j;
  int elindex =-1;
  int ptindex =-1;
  MapElement el;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==label){
          ptindex =j;
          prior.getEl(el,elindex);
          pt = el.geometry[ptindex];

          return 0;
        }
      }
    }
  }
  cerr << "in Map::getWaypoint, waypoint not found" << label <<endl;
  return -1;

}



//   dpt = newpt-projpt;
//   if (dpt.norm()<delta/4)
  
  


//   getPointAlongLine(newpt,centerline,dist+delta);
  
  

//   PointLabel nextlabel,lastlabel;
  
//   int nextval = getNextPointID(nextlabel,pt);
//   int lastval = getLastPointID(lastlabel,pt);

//   //cout << "nextval = " << nextval
//   //    << " lastval = "<< lastval
//   //     <<endl;
//   if (nextval<0 ||lastval <0){
//     cerr << "in Map::getHeading, couldn't find surrounding waypts" << endl;
//     return -1;
//   }

//   point2 nextpt,lastpt,dpt;

//   nextval = getWaypoint(nextpt,nextlabel);
//   lastval = getWaypoint(lastpt,lastlabel);

//   if (nextval<0 ||lastval <0){
//     cerr << "in Map::getHeading, bad getWaypt call" << endl;
//     return -1;
//   }
  
//   dpt = nextpt-lastpt;
  
//   double tmpang = dpt.heading();
//   ang = tmpang;
//   return 0;
// }


int Map::getBounds(point2arr& leftbound, point2arr& rightbound,const LaneLabel &label, const point2 state, const double range, const double backrange )
{

      
  //unsigned int i;
  //MapElement el;
  //  double left_index;
  //double right_index;
  //  double left_index2;
  // double right_index2;
  //int left_side;
  //int right_side;
  //int left_elindex,right_elindex, waypt_elindex;
  //MapElement left_el;
  //MapElement right_el;
  //MapElement waypt_el;
  point2arr left_lane, right_lane, waypts;

  //       bool validlabel = false;
  
  //       for (i=0;i<prior.lanes.size(); ++i){
  //         if (prior.lanes[i].label == label){
  //           left_elindex = prior.lanes[i].leftbound_elindex;
  //           right_elindex = prior.lanes[i].rightbound_elindex;
  //           waypt_elindex = prior.lanes[i].waypoint_elindex;
  //           prior.getEl(left_el,left_elindex);
  //           prior.getEl(right_el,right_elindex);
  //           prior.getEl(waypt_el,waypt_elindex);
  //           left_lane=left_el.geometry;
  //           right_lane=right_el.geometry;
  //           waypts=waypt_el.geometry;
  //           validlabel = true;
  //           break;
  //         }
  //       }
  //       if(!validlabel){
  //         cout << "in Map::getBounds() passed bad lane label "<<label <<endl;
  //         return -1;
  //       }
  int retval;
  point2arr_uncertain tmpleft_unc, tmpright_unc, waypts_unc;
     
  retval= prior.getLaneBoundsFull(tmpleft_unc,tmpright_unc,label);
  retval= prior.getLaneCenterLineFull(waypts_unc,label);

  left_lane.set(tmpleft_unc);
  right_lane.set(tmpright_unc);
  waypts.set(waypts_unc);

  if(retval<0){
    cerr << "in Map::getBounds() passed bad lane label "<<label <<endl;
    return -1;
  }
      
      
  double thisrange  = range;
  double thisbackrange = backrange;
  if (range<0)
    thisrange = 1000000;
  if (backrange<0)
    thisbackrange = 1000000;

     
    
  int cutleftbacknum = 0;
  int cutrightbacknum = 0;
  int cutleftfrontnum = 0;
  int cutrightfrontnum = 0;
      
  // left_lane.cut_front_at_index(10);
  //  right_lane.cut_front_at_index(10);


  //  left_lane.cut_back_at_index(10);
  // right_lane.cut_back_at_index(10);
     
     
  cutleftbacknum = left_lane.cut_back_at_index(state,thisbackrange);
  cutrightbacknum = right_lane.cut_back_at_index(cutleftbacknum);
     
   
  
  cutleftfrontnum = left_lane.cut_front_at_index(state,thisrange);
  cutrightfrontnum =right_lane.cut_front_at_index(cutleftfrontnum);

       
  //         left_lane.cut_back_at_index(nextindex);
  //         left_lane.cut_front_at_index(lastindex);

  //       right_lane.cut_back_at_index(nextindex);
  //       right_lane.cut_front_at_index(lastindex);
      

  //      right_lane.cut_back(nearpt);
  //      right_lane.cut_front(farpt);

  leftbound = left_lane;
  rightbound = right_lane;
  return 0;
} 



 
int Map::getBoundsReverse(point2arr& leftbound, point2arr& rightbound,const LaneLabel &label, const point2 state, const double range, const double backrange )
{
  //     unsigned int i;
  //       MapElement el;
  //       //  double left_index;
  //       //double right_index;
  //       //  double left_index2;
  //       // double right_index2;
  //       //int left_side;
  //       //int right_side;
  //       int left_elindex,right_elindex, waypt_elindex;
  //       MapElement left_el;
  //       MapElement right_el;
  //       MapElement waypt_el;
  point2arr left_lane, right_lane, waypts;

  //       bool validlabel = false;
  
  //       for (i=0;i<prior.lanes.size(); ++i){
  //         if (prior.lanes[i].label == label){
  //           left_elindex = prior.lanes[i].leftbound_elindex;
  //           right_elindex = prior.lanes[i].rightbound_elindex;
  //           waypt_elindex = prior.lanes[i].waypoint_elindex;
  //           prior.getEl(left_el,left_elindex);
  //           prior.getEl(right_el,right_elindex);
  //           prior.getEl(waypt_el,waypt_elindex);
  //           left_lane=left_el.geometry;
  //           right_lane=right_el.geometry;
  //           waypts=waypt_el.geometry;
  //           validlabel = true;
  //           break;
  //         }
  //       }
  //       if(!validlabel){
  //         cout << "in Map::getBounds() passed bad lane label "<<label <<endl;
  //         return -1;
  //       }
  //       waypts = waypt_el.geometry;

  int retval;
  point2arr_uncertain tmpleft_unc, tmpright_unc, waypts_unc;
     
  retval= prior.getLaneBoundsFull(tmpleft_unc,tmpright_unc,label);
  retval= prior.getLaneCenterLine(waypts_unc,label);

  left_lane.set(tmpleft_unc);
  right_lane.set(tmpright_unc);
  waypts.set(waypts_unc);

  if(retval<0){
    cerr << "in Map::getBounds() passed bad lane label "<<label <<endl;
    return -1;
  }
      


  //--------------------------------------------------
  // reversal
  //--------------------------------------------------
  left_lane.reverse();
  right_lane.reverse();
  waypts.reverse();
  point2arr tmp;
  tmp = left_lane;
  left_lane = right_lane;
  right_lane = tmp;
  

  double thisrange  = range;
  double thisbackrange = backrange;
  if (range<0)
    thisrange = 1000000;
  if (backrange<0)
    thisbackrange = 1000000;
     
  
  double statedist = waypts.project_along(state);

  double neardist = statedist-thisbackrange;

  if (neardist<0)
    neardist = 0;


  waypts.cut_back(waypts.linelength()-neardist);
  waypts.cut_front(thisrange);

  point2 nearpt,farpt;
  nearpt = waypts[0];
  farpt = waypts.back();


  // int cutleftbacknum = 0;
  //int cutrightbacknum = 0;
  // int cutleftfrontnum = 0;
  // int cutrightfrontnum = 0;

  int valback = left_lane.cut_back_at_index(nearpt);
  int valfront= left_lane.cut_front_at_index(farpt);

  right_lane.cut_back_at_index(valback);
  right_lane.cut_front_at_index(valfront);

  // left_lane.cut_back(nearpt);
  //left_lane.cut_front(farpt);

  //right_lane.cut_back(nearpt);
  //right_lane.cut_front(farpt);

  leftbound = left_lane;
  rightbound = right_lane;
  return 0;
} 
 










int Map::getWaypointArr(point2arr& ptarr,const PointLabel &label)
{ 
  int waypt_elindex;
  MapElement waypt_el;
  for (uint i=0;i<prior.lanes.size();++i){
    if(label == prior.lanes[i].label){
      waypt_elindex = prior.lanes[i].waypoint_elindex;
      prior.getEl(waypt_el, waypt_elindex);  
      ptarr.set(waypt_el.geometry);
      return (0);
      break;
    }
  }
  
  cerr << "in Map::getWaypointArr, Failed finding a matching lane " << label <<endl;
  return -1;
}

int Map::getLaneDistToWaypoint(double &dist, const point2 state,const PointLabel &label)
{
  point2 waypt;
  int retval;
  retval = getWaypoint(waypt, label);
  if (retval<0){
    cerr << "in Map::getDistToWaypoint, Failed getting waypoint from label " << label <<endl;
    return retval;
  }
  point2arr ptarr;
  retval = getWaypointArr(ptarr,label);
  if (retval<0){
    cerr << "in Map::getDistToWaypoint, Failed getting waypoint array from label " << label <<endl;
    return retval;
  }

  

  double d1 = ptarr.project_along(waypt);
  double d2 = ptarr.project_along(state);

  dist = d1-d2;
  
  return 0;
}

int Map::getLaneDistToWaypoint(double &dist, const PointLabel &statelabel,const PointLabel &label)
{
  point2 waypt;
  int retval;
  retval = getWaypoint(waypt, label);
  if (retval<0){
    cerr << "in Map::getDistToWaypoint, Failed getting waypoint from label " << label <<endl;
    return retval;
  }

  point2 statewaypt;
  retval = getWaypoint(statewaypt, statelabel);
  if (retval<0){
    cerr << "in Map::getDistToWaypoint, Failed getting waypoint from label " << statelabel <<endl;
    return retval;
  }


  point2arr ptarr;
  retval = getWaypointArr(ptarr,label);
  if (retval<0){
    cerr << "in Map::getDistToWaypoint, Failed getting waypoint array from label " << label <<endl;
    return retval;
  }

  

  double d1 = ptarr.project_along(waypt);
  double d2 = ptarr.project_along(statewaypt);

  dist = d1-d2;
  
  return 0;
}





int Map::getObstacleGeometry(point2arr& ptarr)
{
  cerr <<" Map::getObstacleGeometry() not yet implemented!" << endl;
  return -1;
}



double Map::getObstacleDist(const point2& state, const int lanedelta)
{
  double mindist= 1000000;
  double thisdist;
  unsigned int i;
  MapElement el;
  int left_index;
  int right_index;
  int left_index2;
  int right_index2;
  int left_side;
  int right_side;
  int left_elindex,right_elindex;
  MapElement left_el;
  MapElement right_el;
  point2arr left_lane, right_lane;
  LaneLabel label;
  bool gotlane = false;
  for (i=0;i<prior.lanes.size();++i){
    left_elindex = prior.lanes[i].leftbound_elindex;
    right_elindex = prior.lanes[i].rightbound_elindex;
    prior.getEl(left_el,left_elindex);
    prior.getEl(right_el,right_elindex);
    left_lane=left_el.geometry;
    right_lane=right_el.geometry;
    left_lane.get_side(state,left_side,left_index);
    right_lane.get_side(state,right_side,right_index);
    if (left_side!=right_side){
      //      if (left_index>0 &&right_index>0 &&left_index<(double)(left_lane.size()-1) && right_index< (double) (right_lane.size()-1)){
      if (left_index==0 &&right_index==0){    
        label = prior.lanes[i].label;
        gotlane = true;
        break;
      }
    }
    
  }
  if (!gotlane) {
    cerr << "in Map::getObstacleDist() error, not in any lane at point " << state << endl;
    return -1;
  }
  //  cout << "in Map::getObstacleDist() getting obstacles in lane "<< label << endl;
  
  int obsid = -1;
  point2 cpt;
  
  for (i=0;i<data.size();++i){
    el = data[i];
    if (el.height<=0)
      continue;
    cpt = el.center;
    left_lane.get_side(cpt,left_side,left_index2);
    right_lane.get_side(cpt,right_side,right_index2);
    if (left_side!=right_side){
      if (left_index2==0 &&right_index2==0){    
        //if (left_index2>left_index &&right_index2>right_index &&left_index<(double)(left_lane.size()-1) && right_index< (double) (right_lane.size()-1)){
        
        thisdist = cpt.dist(state);
        if (thisdist < mindist){
          mindist = thisdist;
          obsid = i;
        }
      }
    }
    
  }
  if (obsid >=0){
    //cout << "in Map::getObstacleDist() found obstacle "
    //     << obsid << " at distance " << mindist 
    //     << " in lane " << label<< endl;
    return mindist;
  }
  //  else{
  //cout << "in Map::getObstacleDist() found no obstacles in lane " << label<< endl;}
  return -1;
}



point2 Map::getObstaclePoint(const point2& state, const double offset)
{
  double mindist= 1000000;
  double thisdist;
  unsigned int i;
  MapElement el;
  int left_index;
  int right_index;
  int left_index2;
  int right_index2;
  int left_side;
  int right_side;
  int left_elindex,right_elindex;
  MapElement left_el;
  MapElement right_el;
  point2arr left_lane, right_lane;
  LaneLabel label;
  int laneindex;
  bool gotlane = false;
  point2 outpt;

  for (i=0;i<prior.lanes.size();++i){
    left_elindex = prior.lanes[i].leftbound_elindex;
    right_elindex = prior.lanes[i].rightbound_elindex;
    prior.getEl(left_el,left_elindex);
    prior.getEl(right_el,right_elindex);
    left_lane=left_el.geometry;
    right_lane=right_el.geometry;
    left_lane.get_side(state,left_side,left_index);
    right_lane.get_side(state,right_side,right_index);
    if (left_side!=right_side){
      // if (left_index>0 &&right_index>0 &&left_index<(double)(left_lane.size()-1) && right_index< (double) (right_lane.size()-1)){
      if (left_index==0 &&right_index==0){     
        label = prior.lanes[i].label;
        laneindex = i;
        gotlane = true;
        break;
      }
    }
    
  }
  if (!gotlane) {
    //cout << "in Map::getObstaclePoint() warning, not in any lane at point " << state<< endl;
    return outpt;
  }

  //  cout << "in Map::getObstaclePoint() getting obstacles in lane "<< label << endl;
  
  int obsid = -1;
  point2 cpt,obspt;
  
  for (i=0;i<data.size();++i){
    el = data[i];
    if (el.height<=0)
      continue;
    cpt = el.center;
    left_lane.get_side(cpt,left_side,left_index2);
    right_lane.get_side(cpt,right_side,right_index2);
    if (left_side!=right_side){
      //      if (left_index2>left_index &&right_index2>right_index &&left_index<(double)(left_lane.size()-1) && right_index< (double) (right_lane.size()-1)){
      if (left_index2==0 &&right_index2==0){
        thisdist = cpt.dist(state);
        if (thisdist < mindist){
          mindist = thisdist;
          obsid = i;
          obspt = cpt;
        }
      }
    }
    
  }
  //if (obsid >=0)
  //cout << "in Map::getObstaclePoint() found obstacle "
  //     << obsid << " at distance " << mindist 
  //    << " in lane " << label<< endl;
  //else
  // cout << "in Map::getObstaclePoint() found no obstacles in lane " << label<< endl;



  if (obsid >=0){
    int waypt_elindex = prior.lanes[laneindex].waypoint_elindex;
    MapElement waypt_el;
    prior.getEl(waypt_el, waypt_elindex);  
    point2arr wayptarr;
    wayptarr.set(waypt_el.geometry);



    double wayptdist = wayptarr.cut_front(obspt);
    if (wayptdist>offset){
      wayptarr.cut_front(wayptdist-offset);
      outpt = wayptarr.back();
    }
    else {
      //cout << "in Map::getObstaclePoint , offset pushes us out of lane" << endl;
      outpt = state;
    }
  }
  return outpt;
}




//  int Map::getRoadBounds(point2arr& leftbound, point2arr& rightbound,const LaneLabel &label, const point2 state, const double range )
//     {

//       cerr << "WARNING getRoadBounds not implemented.  Use getBounds to return lane bounds." << endl;
      
//       unsigned int i;
//       MapElement el;
//       //  double left_index;
//       //double right_index;
//       //  double left_index2;
//       // double right_index2;
//       //int left_side;
//       //int right_side;
//       int left_elindex,right_elindex, waypt_elindex;
//       MapElement left_el;
//       MapElement right_el;
//       MapElement waypt_el;
//       point2arr left_lane, right_lane, waypts;

//       bool validlabel = false;
  
//       int lanenum = label.lane;
//       int segnum = label.segment;

//       LaneLabel maxlabel;
//       LaneLabel minlabel;
      
//       int numlanes = 0;
//       int segindex = i;
//       int laneindex = lanenum;

//       for (i=0;i<prior.segments.size();++i){
//         if (prior.segments[i].label == label.segment){
//           segindex = i;
//           numlanes = prior.segments[i].lane_label.size();
//         }

//       }

//       double thisang = 0;
//       if (getHeading(thisang,label)!=0)
//         cerr << "in getRoadBounds - cant find this label heading" << endl;

//       double tmpang = 0;
//       double dang;
//       vector<bool> sameheading; 
//       LaneLabel tmplanelabel;
//       vector<LaneLabel>

//       for (i=0;i<numlanes;++i){
//         tmplanelabel = prior.segment[segindex].lane_label[i];
//         getHeading(tmpang, tmplanelabel);
//         dang = tmpang-thisang;
//         if (fabs(dang)>(M_PI/2) && fabs(dang)<(3*M_PI/2)){
//           sameheading.push_back(false);
//         }
//         else{
//           sameheading.push_back(true);
//         }
//       }

//       for (i=0;i<numlanes;++i)
//         tmplanelabel = prior.segment[segindex].lane_label[i];
//       if (sameheading[i]){
//         val = getBounds(tmpleftptarr,tmprightptarr,tmplanelabel,state,range);
//       }

   
//       int val;
//       double minang, maxang, thisang;
//       if (getHeading(minang,minlabel)!=0)
//         cerr << "in getRoadBounds - cant find minlabel heading" << endl;
//       if (getHeading(maxang,maxlabel)!=0)
//         cerr << "in getRoadBounds - cant find maxlabel heading" << endl;
      
      
//       if (

//         for (i=0;i<prior.lanes.size(); ++i){
         
//           if (prior.lanes[i].label == templabel){ 
            
//           }

//         }

//       for (i=0;i<prior.lanes.size(); ++i){
//         if (prior.lanes[i].label == label){
//           left_elindex = prior.lanes[i].leftbound_elindex;
//           right_elindex = prior.lanes[i].rightbound_elindex;
//           waypt_elindex = prior.lanes[i].waypoint_elindex;
//           prior.getEl(left_elindex,left_el);
//           prior.getEl(right_elindex,right_el);
//           prior.getEl(waypt_elindex,waypt_el);
//           left_lane=left_el.geometry;
//           right_lane=right_el.geometry;
//           waypts=waypt_el.geometry;
//           validlabel = true;
//           break;
//         }
//       }
//       if(!validlabel){
//         cout << "in Map::getRoadBounds() passed bad lane label "<<label <<endl;
//         return -1;
//       }
  


//       double BACK_RANGE = 5;
//       waypts = waypt_el.geometry;
//       double statedist = waypts.project_along(state);

//       double neardist = statedist-BACK_RANGE;

//       if (neardist<0)
//         neardist = 0;


//       waypts.cut_back(waypts.linelength()-neardist);
//       waypts.cut_front(range);

//       point2 nearpt,farpt;
//       nearpt = waypts[0];
//       farpt = waypts.back();
//       left_lane.cut_back(nearpt);
//       left_lane.cut_front(farpt);

//       right_lane.cut_back(nearpt);
//       right_lane.cut_front(farpt);

//       leftbound = left_lane;
//       rightbound = right_lane;
//      return -1;
//    } 




 

//  int Map::getTransitionBounds(point2arr &leftbound ,point2arr &rightbound , PointLabel &exitlabel, PointLabel &enterlabel)
//     {
//       unsigned int i,j;
//       int exit_elindex =-1;
//       int exit_leftelindex =-1;
//       int exit_rightelindex =-1;
//       int exit_ptindex =-1;
//       int enter_elindex =-1;
//       int enter_leftelindex =-1;
//       int enter_rightelindex =-1;

//       int enter_ptindex =-1;
  
//       point2 exitpt;
//       point2 enterpt;
//       MapElement el;

//       bool isvalid = false;

//       LaneLabel tmplane(enterlabel.segment,enterlabel.lane);
//       if ((exitlabel.segment==enterlabel.segment) && (exitlabel.lane==enterlabel.lane)){
//         getLeftBound(leftbound, tmplane);
//         int retval = getRightBound(rightbound, tmplane);
//         cout << "returning current lane transition" << endl;
//         return retval;
//       }

//       for (i=0;i<prior.lanes.size(); ++i){
//         if (prior.lanes[i].label== exitlabel){
    
//           for (j=0;j<prior.lanes[i].exit_label.size();++j){
//             if (prior.lanes[i].exit_label[j]==exitlabel &&
//                 prior.lanes[i].exit_link[j]==enterlabel){
        

//               isvalid = true;
//             }
//           }
//           if (!isvalid)
//             continue;
      
//           for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
//             if (prior.lanes[i].waypoint_label[j]==exitlabel){  exit_elindex = prior.lanes[i].waypoint_elindex;
//             exit_leftelindex = prior.lanes[i].leftbound_elindex;
//             exit_rightelindex = prior.lanes[i].rightbound_elindex;
//             exit_ptindex =j;
//             break;
//             }
//           }
//         }
//       }
//       if (isvalid){
//         for (i=0;i<prior.lanes.size(); ++i){
//           if (prior.lanes[i].label== enterlabel){
    
//             for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
//               if (prior.lanes[i].waypoint_label[j]==enterlabel){
//                 enter_elindex = prior.lanes[i].waypoint_elindex;
//                 enter_leftelindex = prior.lanes[i].leftbound_elindex;
//                 enter_rightelindex = prior.lanes[i].rightbound_elindex;
//                 enter_ptindex =j;
//                 break;
//               }
//             }
//           }
//         }
//         prior.getEl(enter_leftel,elindex);
//         enterpt = el.geometry[enter_ptindex];  
//         prior.getEl(exit_leftel,elindex);
//         exitpt = el.geometry[exit_ptindex];  

//         leftbound.clear();
//         leftbound.push_back(enterpt);
//         leftbound.push_back(exitpt);

//         prior.getEl(enter_rightel,elindex);
//         enterpt = el.geometry[enter_ptindex];  
//         prior.getEl(exit_rightel,elindex);
//         exitpt = el.geometry[exit_ptindex];  

//         rightbound.clear();
//         rightbound.push_back(enterpt);
//         rightbound.push_back(exitpt);
  
//         return 0;
//       }


//       cerr << "in Map::getTransitionBounds, invalid transition, exit = " << exitlabel << " enter = " << enterlabel <<endl;
//       return -1;
//     }

