/**********************************************************
 **
 **  MAPELEMENT.HH
 **
 **    Time-stamp: <2007-03-09 11:21:18 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Feb  7 12:44:48 2007
 **
 **
 **********************************************************
 **
 **  A class to describe a geometric map element
 **
 **********************************************************/


#ifndef MAPELEMENT_HH
#define MAPELEMENT_HH

#include <vector>
#include <string>
#include <iostream>
#include "interfaces/VehicleState.h"
#include "frames/point2_uncertain.hh"

using namespace std;

enum MapElementGeometryType {
	GEOMETRY_UNDEF,	
	GEOMETRY_POINTS,
	GEOMETRY_ORDERED_POINTS,
	GEOMETRY_LINE,
	GEOMETRY_EDGE,
	GEOMETRY_POLY
};
enum MapElementCenterType {
	CENTER_UNDEF,	
	CENTER_POINT,
	CENTER_POSE,
	CENTER_BOUND_RADIUS,
	CENTER_BOUND_BOX,
	CENTER_BOUND_LINES
};
		
enum MapElementFrameType {
	FRAME_UNDEF,
	FRAME_LOCAL,
	FRAME_GLOBAL,
	FRAME_VEHICLE
};

enum MapElementType {
	ELEMENT_UNDEF,
	ELEMENT_CLEAR,

	ELEMENT_POINTS,
	ELEMENT_WAYPOINT,
	ELEMENT_CHECKPOINT,

	ELEMENT_LINE,
	ELEMENT_STOPLINE,
	ELEMENT_LANELINE,

	ELEMENT_PARKING_SPOT,
	ELEMENT_PERIMETER,

	ELEMENT_OBSTACLE,	
	ELEMENT_OBSTACLE_EDGE,
	//	ELEMENT_VEHICLE
};



// A class to describe a geometric map element
class MapElement
{
	
public:
	
	// A Constructor 
	MapElement();
	
	// A Destructor 
	~MapElement() {}
	
	// Subgroup id. only changed and used by talker;
	int subgroup;

	// Object id
	vector<int> id; 

	// Confidence that the object exists 
	double conf;

	// Object classification
	MapElementType type;
	// TODO: Need type list and enumeration
	
	// Type classification confidence bounds
	double type_conf;
	

	MapElementCenterType center_type;
	// Estimated object center position and covariance
	point2_uncertain center;
	//
	// point2_uncertain struct members (defined in frames/point2_uncertain.hh)
	//
	// center.x                x position
	// center.y                y position
	// center.max_var        max variance
	// center.min_var        min variance
	// center.axis             angle of max variance
	
	
	// Estimated bounding box parameter
	double length;  
	double width;
	double orientation;
	// Not meaningful for all objects, but can be useful
	

	// bounding box parameter variance
	double length_var;
	double width_var;
	double orientation_var;

	MapElementGeometryType geometry_type;
	// Estimated object geometry 
	vector<point2_uncertain> geometry;
	// geometry represents object contour
	// -list of ordered connected points for a line 
	// -list of ordered vertices for an obstacle
	// If the geometry is not specified, bounding box info only is used


	// Estimated object height
	double height;
	// height for road markings should be zero

	// Object height variance
	double height_var;
	

	// Object velocity
	point2_uncertain velocity;
	// The velocity x,y coordinates define a velocity vector
	// for static map elements x,y should be 0 

		
	// Frame of reference
	MapElementFrameType frame_type;

	// All map element coordinates should be given in local frame, 
	// not GPS global or vehicle frame.
	// Shouldn't need this eventually.
	// TODO: define frame enumeration

  /// Vehicle state data when element was detected
  VehicleState state;

	// Text label
	// Not critical but could be useful initially for debugging
	vector<string> label;


	void clear();
	void set_circle_obs(vector<int>& ident, point2 cpt, double radius);
	void set_poly_obs(vector<int>& ident, vector<point2>& ptarr);
	void set_block_obs(vector<int>& ident, point2 cpt, double ang, double len, double wid );
	void set_line(vector<int>& ident,  vector<point2>& ptarr);
	void set_stopline(vector<int>& ident, vector<point2>& ptarr);
	void set_laneline(vector<int>& ident, vector<point2>& ptarr);
	void set_points(vector<int>& ident, vector<point2>& ptarr);
	void print() const ;
void print_state() const ;
	void set_center_from_geometry();
};
#endif
