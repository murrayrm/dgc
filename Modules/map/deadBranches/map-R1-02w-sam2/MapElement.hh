/**********************************************************
 **
 **  MAPELEMENT.HH
 **
 **    Time-stamp: <2007-05-23 11:38:48 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Feb  7 12:44:48 2007
 **
 **
 **********************************************************
 **
 **  A class to describe a geometric map element
 **
 **********************************************************/


#ifndef MAPELEMENT_HH
#define MAPELEMENT_HH

#include <stdint.h>
#include <vector>
#include <string>
#include <iostream>
#include "interfaces/VehicleState.h"
#include "frames/point2_uncertain.hh"
#include "MapId.hh"

using namespace std;

/** The type of geometry, used mostly for plotting */
enum MapElementGeometryType {
  GEOMETRY_UNDEF,  
  GEOMETRY_POINTS,
  GEOMETRY_ORDERED_POINTS,
  GEOMETRY_LINE,
  GEOMETRY_EDGE,
  GEOMETRY_POLY
};


    
/** The frame of the element (global, local, etc */
enum MapElementFrameType {
  FRAME_UNDEF,
  FRAME_LOCAL, /*!< Alice's local frame */
  FRAME_GLOBAL, /*!< Global (UTM coords) */
  FRAME_VEHICLE /*!< Vehicle frame (pinned on Alice)*/
};

/** What color to draw the element */
enum MapElementColorType {
  MAP_COLOR_GREY,
  MAP_COLOR_RED,
  MAP_COLOR_BLUE,
  MAP_COLOR_LIGHT_BLUE,
  MAP_COLOR_BLUE_2,
  MAP_COLOR_GREEN,
  MAP_COLOR_DARK_GREEN,
  MAP_COLOR_CYAN,
  MAP_COLOR_YELLOW,
  MAP_COLOR_MAGENTA,
  MAP_COLOR_ORANGE,
  MAP_COLOR_PINK,
  MAP_COLOR_PURPLE
};

/* What kind of element we're representing */
enum MapElementType {
  ELEMENT_UNDEF, /*!< Undefined element */
  ELEMENT_CLEAR, /*!< Clears the element at the set ID */
  ELEMENT_ALICE, /*!< A representation of Alice */

  ELEMENT_POINTS, /*!< A generic set of points */
  ELEMENT_WAYPOINTS, /*!< A set of waypoint */
  ELEMENT_CHECKPOINTS, /*!< A set ofcheckpoint */

  ELEMENT_LINE, /*!< A generic line */
  ELEMENT_STOPLINE, /*!< A stop line */
  ELEMENT_LANELINE, /*!< A lane line */

  ELEMENT_PARKING_SPOT, /*!< A parking spot */
  ELEMENT_PERIMETER, /*!< The perimeter of a zone, such as a parking lot */

  ELEMENT_OBSTACLE,  /*!< An obstacle */
  ELEMENT_OBSTACLE_EDGE, /*!< The edge of an obstacle */
  ELEMENT_VEHICLE, /*!< A vehicle */

  ELEMENT_POLY, /*!< A generic polygon */
  ELEMENT_TRAV_PATH, /*!< Alice's traversed path */
  ELEMENT_PLANNING_TRAJ, /*!< A planned trajectory */

  ELEMENT_STEER_CMD, /*!< The commanded steering spline */
  ELEMENT_VEH_STEER, /*!< Spline for the actual steering angle */
  
  //  ELEMENT_TPLAN_PATH /*!< A spline describing Alice's planned path */
  ELEMENT_RDDF, /*!< An RDDF */
  ELEMENT_POLYTOPE, /*!< A polytope, for example like what is sent to dplanner */
  ELEMENT_INIT_COND, /*!< OCPparams initial condition */
  ELEMENT_FINAL_COND /*!< OCPparams final condition */

};



/** \brief Describes a geometric map element
 *
 * A MapElement represents information, such as vehicles, road lines,
 * trajectory splines, etc that we wanted to represent in the map.
 *
 * To send a MapElement, use MapElementTalker
 */
class MapElement
{
  
public:
  
  /** Default Constructor. Clears whatever is in the mapelement */
  MapElement();
  
  /** Destructor. Does nothing */
  ~MapElement() {}

  /// Element id
  MapId id;
  
  /** Subgroup id. only changed and used by talker */
  int subgroup;

  /// Confidence that the element exists 
  double conf;

  /// Element classification
  MapElementType type;
  // TODO: Need type list and enumeration
  
  /// Type classification confidence bounds
  double typeConf;

  /// \brief Estimated element position and covariance
  ///
  /// point2_uncertain struct members (defined in frames/point2_uncertain.hh)
  ///
  /// center.x                x position
  /// center.y                y position
  /// center.max_var        max variance
  /// center.min_var        min variance
  /// center.axis             angle of max variance
  point2_uncertain position;

  

  /// \brief Estimated element center position and covariance
  /// 
  /// The bounding box values length width and orientation are defined
  /// with respect to this center point.  This point is usually a
  /// function of the element geometry which can be volitile for
  /// sensed data.  The position field should be used for tracking
  /// elements over time
  point2_uncertain center;
  
  ///Estimated bounding box parameter
  double length;  
  /// Estimated bounding box parameter
  double width;
  /// Estimated bounding box parameter, Ranges from -pi to pi
  double orientation;
  

  /// bounding box parameter variance
  double lengthVar;
  /// bounding box parameter variance
  double widthVar;
  /// bounding box parameter variance
  double orientationVar;

  /// Element geometry type
  MapElementGeometryType geometryType;
  
  /** Estimated element geometry 
   *
   * geometry represents element contour
   * -list of ordered connected points for a line 
   * -list of ordered vertices for an obstacle
   * If the geometry is not specified, bounding box info only is used
   */
  point2arr_uncertain geometry;

  /** Estimated element height. 
   * height for road markings should be negative */
  double height;

  /// Element height variance
  double heightVar;
  
  /** Estimated elevation of lower bound of element
   * Used for overhanging objects
   */ 
  double elevation;
  /// Element elevation variance
  double elevationVar;


  /// Element velocity
  point2_uncertain velocity;

  /// Peak measured speed (m/s)
  double peakSpeed; 
  /// Variance of peak measured speed
  double peakSpeedVar; 
  /// Peak measured acceleration (m/s^2)
  double peakAccel; 
  /// Variance for peak measured acceleration
  double peakAccelVar; 
    
     
  /// Frame of reference
  /// All map element coordinates should be given in local frame, 
  /// not GPS global or vehicle frame.
  /// Shouldn't need this eventually.
  MapElementFrameType frameType;

  /// Timestamp when raw data was collected
  uint64_t timestamp;


  /// Vehicle state data when element was detected
  VehicleState state;

  /// The color of the element
  /// Note: the colors are declared in MapElement.hh and 
  /// interpreted in MapWindow.cc
  MapElementColorType plotColor;
  int plotValue;

  /// Text label
  /// Not critical but could be useful initially for debugging
  vector<string> label;

  /// The minimum values of the geometry point arr
  /// Useful for fast overlap checks.
  point2_uncertain geometryMin;
  /// The maximum values of the geometry point arr
  /// Useful for fast overlap checks.
  point2_uncertain geometryMax;


  /// Resets everything to zero, undefined, etc
  void clear();

  /// Set the element ID using a MapID
  void setId(MapId& id_in) {id = id_in;}
  /// Set the element id using an array of ints
  void setId(vector<int>& id_in) {id = id_in;}
  /// set the element ID using an integer
  void setId(int id_in) {id = id_in;}
  /// set the element ID using integers
  void setId(int id1_in,int id2_in) {id.set(id1_in,id2_in);}
  /// set the element ID using integers
  void setId(int id1_in,int id2_in,int id3_in) {id.set(id1_in,id2_in,id3_in);}
  /// set the element ID using integers
  void setId(int id1_in,int id2_in,int id3_in, int id4_in) {id.set( id1_in,id2_in,id3_in,id4_in);}
  /// set the element ID using integers
  void setId(int id1_in,int id2_in,int id3_in, int id4_in,int id5_in) {id.set( id1_in,id2_in,id3_in,id4_in,id5_in);}

  /// Set the element to become a clear command
  void setTypeClear();
  /// Set the element type to undefined
  void setTypeUndef();
  /// Set the element type to represent Alice herself
  void setTypeAlice();
  
  /// Set the element type to represent a set of points
  void setTypePoints();
  /// Set the element type to represent a set of waypoints
  void setTypeWayPoints();
  /// Set the element type to represent a set of checkpoints
  void setTypeCheckPoints();
  /// Set the element type to represent a polyline
  void setTypeLine();
  /// Set the element type to represent a stop line
  void setTypeStopLine();
  /// Set the element type to represent a lane line
  void setTypeLaneLine();
  /// Set the element type to represent a parking spot
  void setTypeParkingSpot();
  /// Set the element type to represent a zone perimeter
  void setTypePerimeter();
  /// Set the element type to represent an obstacle
  void setTypeObstacle();
  /// Set the element type to represent an obstacle edge
  void setTypeObstacleEdge();
  /// Set the element type to represent a dynamic obstacle
  void setTypeVehicle();
  /// Set the element type to represent a polygon
  void setTypePoly();
  /// Set the element type to represent a traversed path
  void setTypeTravPath();
  /// Set the element type to represent a trajectory
  void setTypePlanningTraj();
  /// Set the element type to represent an RDDF
  void setTypeRDDF();
  /// Set the element type to represent a steering command
  void setTypeSteerCmd();
  /// Set the element type to represent the vehicle steering
  void setTypeSteer();
  /// Set the element type to represent the a planning polytope
  void setTypePolytope();
  /// Set the element to represent the OCPparams initial condition
  void setTypeInitCond();
  /// Set the element to represent the OCPparams final condition
  void setTypeFinalCond();


  /// Set the position of the element
  void setPosition(point2 cpt);
  /// Set the position of the element
  void setPosition(point2_uncertain cpt);
  /// Set the position of the element
  void setPosition(double x, double y);
  /// Uses the element's geometry to find its position
  void setPositionFromGeometry();

  /// Uses the element's geometry to find its center and oriented bounding box
  void setCenterBoundsFromGeometry();


  /// Set the geometry of an element (single point) 
  void setGeometry(point2 &pt);
  /// Set the geometry of an element (single point) 
  void setGeometry(point2_uncertain &pt);
  /// Set the geometry of an element (two points) 
  void setGeometry(point2 &pta, point2 &ptb);
  /// Set the geometry of an element (two points) 
  void setGeometry(point2_uncertain &pta,point2_uncertain &ptb);

  /// Set the geometry of an element (center point and radius) 
  void setGeometry(point2 &pt, double radius);
  /// Set the geometry of an element (center point and radius) 
  void setGeometry(point2_uncertain &pt, double radius);

  /// Set the geometry of an element (center point and bound box) 
  void setGeometry(point2 &pt, double len, double wid, double orient=0);
  /// Set the geometry of an element (center point and bound box) 
  void setGeometry(point2_uncertain &pt, double len, double wid, double orient=0);

  /// Set the geometry of an element
  void setGeometry(point2arr &ptarr);
  /// Set the geometry of an element
  void setGeometry(point2arr_uncertain &ptarr);
  /// Set the geometry of an element
  void setGeometry(vector<point2> &ptarr);
  /// Set the geometry of an element
  void setGeometry(vector<point2_uncertain> &ptarr);
  /// Set the geometry of an element 
  void setGeometry(vector<double> &xarr, vector<double> &yarr);



  /// Set the vehicle state member
  void setState(VehicleState &statein) {state=statein;}
  /// Set the timestamp of detection
  void setTimestamp(uint64_t timein) {timestamp=timein;}

  /// Set the vehicle state member
  void setHeight(double val, double unc=0) 
  {height=val; heightVar=unc;}
  /// Set the vehicle state member
  void setElevation(double val, double unc=0) 
  {elevation=val; elevationVar=unc;}

  /// Set the min and max values explicitly 
  void setMinMax(double minx, double miny, double maxx, double maxy);
  /// Set the min and max values from the current geometry
  void setMinMaxFromGeometry();
  /// Updates center and bounds from the current geometry
  void updateFromGeometry();

  /// Set the element color and value
  void setColor(MapElementColorType col,int val=100)
  {plotColor=col;plotValue=val;}

  /// Set the reference frame type to global
  void setFrameTypeGlobal() {frameType=FRAME_GLOBAL;}
  /// Set the reference frame type to local
  void setFrameTypeLocal() {frameType=FRAME_LOCAL;}
  /// Set the reference frame type to vehicle
  void setFrameTypeVehicle() {frameType=FRAME_VEHICLE;}

 
  /// Checks if there is overlap with another MapElement
  bool isOverlap(const MapElement& el) const;
  bool isOverlap(const point2 &pt) const  ;
  bool isOverlap(const point2_uncertain &pt) const  ;
  bool isOverlap(const point2arr &ptarr, const MapElementGeometryType type = GEOMETRY_POLY) const  ;
  bool isOverlap(const point2arr_uncertain &ptarr, const MapElementGeometryType type = GEOMETRY_POLY) const  ;
 
  double dist(const MapElement& el) const;

  double dist(const point2 &pt) const  ;
  double dist(const point2_uncertain &pt) const  ;
  double dist(const point2arr &ptarr, const MapElementGeometryType type = GEOMETRY_POLY) const  ;
  double dist(const point2arr_uncertain &ptarr, const MapElementGeometryType type = GEOMETRY_POLY) const  ;



  bool isLine() const {return (type==ELEMENT_LINE|| type==ELEMENT_STOPLINE || type==ELEMENT_LANELINE);}
  bool isObstacle() const {return (type==ELEMENT_OBSTACLE|| type==ELEMENT_OBSTACLE_EDGE|| type==ELEMENT_VEHICLE);}
  friend ostream &operator<<(ostream &os, const MapElement &el);
  friend ostream &operator<<(ostream &os, const VehicleState &state);



  //--------------------------------------------------
  // old stuff
  //--------------------------------------------------


  void set_geometry(point2arr_uncertain &ptarr);
 
  void set_geometry(point2arr &ptarr);
  void set_geometry(point2 &pt);
  void set_geometry(point2 &pt, double radius);
  void set_geometry(vector<point2> &ptarr);
  void set_geometry(vector<point2_uncertain> &ptarr);
  void set_geometry(vector<double> &xarr, vector<double> &yarr);

  void  set_clear(vector<int>& ident);

  void  set_alice(VehicleState &statein);
  void  set_circle_obs(vector<int>& ident, point2 cpt, double radius);
  void  set_circle_obs();
  void  set_poly_obs(vector<int>& ident, vector<point2>& ptarr);
  void  set_block_obs(vector<int>& ident, point2 cpt, double ang, double len, double wid );
  void  set_line(vector<int>& ident,  vector<point2>& ptarr);
  void  set_line();
  void  set_stopline(vector<int>& ident, vector<point2>& ptarr);


  void  set_laneline(vector<int>& ident, vector<point2>& ptarr);
  

  void  set_laneline(); 
  void  set_points(vector<int>& ident, vector<point2>& ptarr);
  void  set_points();
  void  set_trav_path(vector<int>& ident,  vector<point2>& ptarr);
  



};


#endif
