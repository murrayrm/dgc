#ifndef LOGICALROADOBJECT_HH_
#define LOGICALROADOBJECT_HH_

#include <vector>
using std::vector;
#include "RoadObject.hh"

namespace Mapper
{
class Lane
{
public:
	/* Full constructor */
	Lane(int, LaneBoundary &, LaneBoundary &, 
	     vector<StopLine>, vector<Checkpoint>);
	/* Basic constructor, no stop lines or checkpoints. */
	Lane(int, LaneBoundary &, LaneBoundary &); 
	/* Lane spoofing constructor. With stoplines. */
	Lane(int, vector<Location>, vector<StopLine>); 
	/* Lane spoofing constructor. Without stoplines. */
  	Lane(int, vector<Location>); 

	/* Add a stop line to this lane.  It's likely that there's only one stop
	 * line per lane.*/
	void addStopLine(StopLine);
	/* Add a checkpoint to this lane. */
	void addCheckpoint(Checkpoint);
	/* Set the width of this lane. */
	void setWidth(double);
	
	/* Get methods */
	double getWidth() const;
	int getID() const;
	vector<Location> getLB() const;
	vector<Location> getRB() const;
	LaneBoundary::Divider getLBType() const;
	LaneBoundary::Divider getRBType() const;
	vector<StopLine> getStopLines() const;  //nok
	vector<Checkpoint> getCheckpoints() const;  //nok
		
	void print() const;
	virtual ~Lane();
private:
	double width; /* Width of the lane, in feet. */
	int ID;
	LaneBoundary left;
	LaneBoundary right;
	vector<StopLine> stopLines;
	vector<Checkpoint> checkPoints;

	/* Spoof boundaries by half the lane width based on a centerline. */	
	void spoofBoundaries(vector<Location>);
};

class Segment
{
public:
	/* Constructor */
	Segment(int, vector<Lane>);
	/* Methods */	
	void print() const;
	int getID() const;
	Lane getLane(int) const;
	vector<Lane> getAllLanes() const; //nok
	virtual ~Segment();
private:
	vector<Lane> lanes;
	int ID;
};
}
#endif /*LOGICALROADOBJECT_HH_*/
