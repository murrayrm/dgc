#include "LogicalRoadObject.hh"
#include "RoadObject.hh"
#include <math.h>

namespace Mapper
{
/* Lane Constructors */
/* Everything specified constructor */ 
Lane::Lane(int laneID, LaneBoundary &laneBoundary0, 
		   LaneBoundary &laneBoundary1, 
		   vector<StopLine> stopList, 
		   vector<Checkpoint> checkList)
		   : left(laneBoundary0), // copy constructor init lb0
		     right(laneBoundary1)  // copy constructor init lb1
{
	ID = laneID;
	stopLines = stopList;
	checkPoints = checkList;
	width = DEFAULT_LANE_WIDTH;
}
/* Constructor for a lane without any initial checkpoints or stop
 * lines. */
Lane::Lane(int laneID, 
		   LaneBoundary &laneBoundary0,
	  	   LaneBoundary &laneBoundary1)
	  	   : left(laneBoundary0),
	  	     right(laneBoundary1)
{
	ID = laneID;
	width = DEFAULT_LANE_WIDTH;
}
/* Lane spoofing constructor */
/* Now also includes stop lines. */
Lane::Lane(int laneID, 
	   vector<Location> centerline,
	   vector<StopLine> stopList)
{
	ID = laneID;
	width = DEFAULT_LANE_WIDTH;
	spoofBoundaries(centerline);
	stopLines = stopList;
}

/* Lane spoofing constructor */
Lane::Lane(int laneID, 
	   vector<Location> centerline)
{
	ID = laneID;
	width = DEFAULT_LANE_WIDTH;
	spoofBoundaries(centerline);
}

/* Note - I need to re-do this boundary "spoofing" 
 * so that it makes a well-defined corridor.  I would also
 * like to make some nice corners. -CS */
 //what if the lane crosses itself? even if it includes a u-turn, this gets a bit hairy 
#define PI 3.14159
void Lane::spoofBoundaries(vector<Location> locs)
{
  
    vector<Location> edge0;
    vector<Location> edge1; 
    const double halfWidth = width / 2.0;
    //double slope, offset;
    double angle;
    
        /** Actual point where the E-W, N-S decision is made. **/ 
    Location p1;
    Location p2;
    
    //Location *e0_p;
    //Location *e1_p;
            
    //first, let's assume that there are only two points per lane
    if (locs.size() < 2)
    //TODO change this to throw 
        std::cout << "locs.size() < 2, locs.size() = " << locs.size() << "\n";
        //cout << "Need at least two waypoints to spoof.\n";
    else // There are at least 2 waypoints.  
    // Check them to ensure they're collinear.
    {
        // Do nothing for now
        //TODO Check that the waypoints are a straight line. 
    } 
    
    //ok, now we just add an edge.  by default, we'll call edge0 the left and edge1 the right side 
    for (int ii=0;ii<(int)locs.size();ii++)
    {

            p1 = locs[ii];
            p2 = locs[ii+1];
            
            // on the last step, don't update the angle
            if (ii<( (int)locs.size()-1))
            {
                //atan2 returns radians
                //must use atan2 - it handles p2.x-p1.x = 0
                //as a reference, 
                //if p2.x-p1.x == 0 and p2.y > p1.y, angle should be PI/2
                angle = atan2(((double) p2.y - p1.y),((double) p2.x - p1.x));
            }
            
            //now, figure out the x,y points for each edge for each
            Location *e0_p = new Location;
            Location *e1_p = new Location;
            
            //angle to add
            e0_p->x = p1.x + halfWidth*cos(angle + PI/2);
            e0_p->y = p1.y + halfWidth*sin(angle + PI/2);
            
            e1_p->x = p1.x + halfWidth*cos(angle - PI/2);
            e1_p->y = p1.y + halfWidth*sin(angle - PI/2);
                
            edge0.push_back (*e0_p);
            edge1.push_back(*e1_p);
            
    }
    
    
    /* Create new spoofed lane boundaries and assign them */
    left = LaneBoundary("Left LaneBoundary", edge0, LaneBoundary::SOLID_WHITE); 
    right = LaneBoundary("Right LaneBoundary", edge1, LaneBoundary::SOLID_WHITE);
}


///* This function will spoof the lane boundaries based off
// * the waypoints presented to it.
// */
///* TODO There are some *critical* assumptions in this
// * function.  The first is that N-S and E-W are the 
// * ONLY possible directions for a road to go in.
// */
//void Lane::spoofBoundaries(vector<Location> locs)
//{
//	const double halfWidth = width / 2.0;
//
//	/* Decide if the lane is N-S or E-W. */
//	// TODO replace this with better math so 
//	// that lines don't have to be E-W, N-S only.
//	/** Error checking **/
//	if (locs.size() < 2)
//	//TODO change this to throw
//		std::cout << "locs.size() < 2, locs.size() = " << locs.size() << "\n";
//		//cout << "Need at least two waypoints to spoof.\n";
//	else // There are at least 2 waypoints.  
//	// Check them to ensure they're collinear.
//	{
//		// Do nothing for now
//		//TODO Check that the waypoints are a straight line. 
//	} 
//
//	/** Actual point where the E-W, N-S decision is made. **/
//	bool eastWest; // x-direction
//	Location first = locs[1];
//	Location second = locs[2];
//	
//	if (first.x == second.x)
//		eastWest = true;
//	else
//		eastWest = false;	
//		
//	/** Spoof the lane boundaries accordingly. **/
//	unsigned int i;
//	vector<Location> edge0;
//	vector<Location> edge1;
//	if (eastWest)
//	{
//		// Mutate y-values
//		for (i=0; i<locs.size(); i++)
//		{
//			Location *loc0_p = new Location;
//			Location *loc1_p = new Location;
//			// Upper
//			loc0_p->x = locs[i].x;
//			loc0_p->y = locs[i].y + halfWidth;
//			// Lower
//			loc1_p->x = locs[i].x;
//			loc1_p->y = locs[i].y - halfWidth;
//				
//			edge0.push_back(*loc0_p);
//			edge1.push_back(*loc1_p);
//		}
//	}
//	else
//	{
//		// Mutate x-values
//		for (i=0; i<locs.size(); i++)
//		{
//			Location *loc0_p = new Location;
//			Location *loc1_p = new Location;
//			// Upper
//			loc0_p->x = locs[i].x + halfWidth;
//			loc0_p->y = locs[i].y;
//			// Lower
//			loc1_p->x = locs[i].x - halfWidth;
//			loc1_p->y = locs[i].y;
//				
//			edge0.push_back(*loc0_p);
//			edge1.push_back(*loc1_p);
//		}	
//	}
//	
//	/* Create new spoofed lane boundaries and assign them */
//	left = LaneBoundary("Left LaneBoundary", edge0, LaneBoundary::SOLID_WHITE);
//	right = LaneBoundary("Right LaneBoundary", edge1, LaneBoundary::SOLID_WHITE);
//}
void Lane::addStopLine(StopLine sl)
{
	stopLines.push_back(sl);
}
void Lane::addCheckpoint(Checkpoint cp)
{
	checkPoints.push_back(cp);
}
void Lane::setWidth(double newWidth)
{
	width = newWidth;
}

/* Lane Get functions. */
int Lane::getID() const 
{
	return ID;	
}
double Lane::getWidth() const
{
	return width;
}
vector<Location> Lane::getLB() const
{
	return left.getCoords();
}
vector<Location> Lane::getRB() const
{
	return right.getCoords();
}
LaneBoundary::Divider Lane::getLBType() const
{
	return left.getBoundary();
}
LaneBoundary::Divider Lane::getRBType() const
{
	return right.getBoundary();
}
vector<StopLine> Lane::getStopLines() const
{
	return stopLines;
}
vector<Checkpoint> Lane::getCheckpoints() const
{
	return checkPoints;
}
/* Pretty print the lane object. */
void Lane::print() const 
{
	unsigned int i;
	
	std::cout << "Lane information\n";
	std::cout << "Left lane boundary:\n";
	left.print();
	std::cout << "Right lane boundary:\n";
	right.print();
	std::cout << "Stop Line(s):\n";
	for (i=0; i<stopLines.size(); i++)
	{
		stopLines[i].print();
	}
	std::cout << "Check Point(s):\n";
	for (i=0; i<checkPoints.size(); i++)
	{
		checkPoints[i].print();	
	}
}
/* Lane destructor */
//TODO fill this in.
Lane::~Lane() 
{
}

/*******************/
/* Segment Methods */
/*******************/
/* Vanilla segment constructor. */
Segment::Segment(int segID, 
				 vector<Lane> initialLanes) 
{
	ID = segID;
	lanes = initialLanes;
}
/* Return this segment's ID. */
int Segment::getID() const
{
	return ID;
}
/* Retrieve a lane associated with this segment. */
/* Checks to make sure the lane ID matches the ID requested. */ 
Lane Segment::getLane(int laneID) const
{
	Lane reqLane = lanes[laneID];
	int reqLaneID = reqLane.getID();
	
	/* Check to make sure the lane ID matches the requested ID. */
	/* For now, it's just internal discipline to make sure this */
	/* is true. */
	if (reqLaneID != laneID)
		throw "The requested lane ID does not match the actual ID of the lane returned.\n";
	 
	return reqLane;
}
/* Retrieve all the lanes associated with this segment. */
vector<Lane> Segment::getAllLanes() const
{
	return lanes;
}
/* Pretty print the segment object. */
void Segment::print() const
{
	unsigned int i;
	
	std::cout << "Segment: " << ID << "\n";
	for (i = 0; i < lanes.size(); i++)
	{
		lanes[i].print(); 
	}
}
/* Segment Destructor */
//TODO fill this in.
Segment::~Segment()
{
	
}
}
