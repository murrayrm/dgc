#include "LocalMapTalker.hh"
#include "interfaces/sn_types.h"

//using namespace std;
using namespace Mapper;
#define DEBUG_LEVEL 0

/* If "sender" is true, the talker transmits, if it's false, it receives. */
CLocalMapTalker::CLocalMapTalker(int modName, int sn_key, bool sender)
: CSkynetContainer(modName, sn_key) 
{
  m_pLocalMapDataBuffer = new char[LOCALMAP_MAX_BUFFER_SIZE];
  DGCcreateMutex(&m_localMapDataBufferMutex);
  if (sender)
  	localMapSocket = m_skynet.get_send_sock(SNtrafficLocalMap);
  else
  	localMapSocket = m_skynet.listen(SNtrafficLocalMap,  MODmapping);
  cout << "localMapSocket = " << localMapSocket << endl;
}

CLocalMapTalker::CLocalMapTalker()
{
  m_pLocalMapDataBuffer = new char[LOCALMAP_MAX_BUFFER_SIZE];
  DGCcreateMutex(&m_localMapDataBufferMutex);
}

CLocalMapTalker::~CLocalMapTalker() {
  delete [] m_pLocalMapDataBuffer;  
  DGCdeleteMutex(&m_localMapDataBufferMutex);
}


bool CLocalMapTalker::RecvLocalMap(Map* localMap, int* pSize) {
  return RecvLocalMap(localMapSocket, localMap, pSize);
}


// TODO: change this to take const references, not pointer 
bool CLocalMapTalker::RecvLocalMap(int localMapSocket, Map* localMap, int* pSize) {
  int bytesToReceive = LOCALMAP_MAX_BUFFER_SIZE;
  char* pBuffer = m_pLocalMapDataBuffer;

  // Build the mutex list. We want to protect the data buffer.
  int numMutices = 1;
  pthread_mutex_t* ppMutices[numMutices];
  ppMutices[0] = &m_localMapDataBufferMutex;

  // Get the localMap data from skynet. We want to receive the whole Map object, locking
  // the requesting the mutices to be locked and NOT unlocked at the end. This
  // is done because this data is still accessed afterwards and we manually
  // unlock the mutices later.
  *pSize = m_skynet.get_msg(localMapSocket, m_pLocalMapDataBuffer, bytesToReceive, 0,
      ppMutices, false, numMutices);
  if(*pSize <= 0)
  {
    cerr << "CLocalMapTalker::RecvLocalMap(): skynet error" << endl;
    DGCunlockMutex(&m_localMapDataBufferMutex);
    return false;
  }
  
  // Decode the message
  vector<Mapper::Segment> segments;
  vector<Mapper::Lane> lanes;
  vector<StopLine> stopLines;
  vector<Checkpoint> checkpoints;
  
  int numOfSegments;
  memcpy(&numOfSegments, pBuffer, sizeof(int));
  pBuffer += sizeof(int);
  
  for (int i=0; i<numOfSegments; i++)
  {
    int segmentID, numOfLanes;
    memcpy(&segmentID, pBuffer, sizeof(int));
    pBuffer += sizeof(int);
    memcpy(&numOfLanes, pBuffer, sizeof(int));
    pBuffer += sizeof(int);
  
    lanes.clear();
  
    // Lane
    for (int j=0; j<numOfLanes; j++)
    {
      int laneID, laneWidth;
    
      memcpy(&laneID, pBuffer, sizeof(int));
      pBuffer += sizeof(int);
      memcpy(&laneWidth, pBuffer, sizeof(double));
      pBuffer += sizeof(double);
        
      // Get other info about this lane
      LaneBoundary::Divider lbType, rbType;
      vector<Location> lbLoc, rbLoc;
      int numLocLB, numLocRB, numStopLines, numCheckpoints;
      stopLines.clear();
      checkpoints.clear();
    
      // Left Boundary
      memcpy(&lbType, pBuffer, sizeof(LaneBoundary::Divider));
      pBuffer += sizeof(LaneBoundary::Divider);
      memcpy(&numLocLB, pBuffer, sizeof(int));
      pBuffer += sizeof(int);
      for (int k=0; k<numLocLB; k++)
      {
        Location* loc = new Location();
        memcpy(loc, pBuffer, sizeof(Location));
        pBuffer += sizeof(Location);
        lbLoc.push_back(*loc);      
      }
      LaneBoundary* leftBoundary = new LaneBoundary("", lbLoc, lbType);
      if (DEBUG_LEVEL > 0)
      {
        cout << "Segment " << i << " Lane " << j << ": Left boundary :" << endl;
        leftBoundary->print();
        cout << endl;
      }
      
      // Right Boundary   
      memcpy(&rbType, pBuffer, sizeof(LaneBoundary::Divider));
      pBuffer += sizeof(LaneBoundary::Divider);
      memcpy(&numLocRB, pBuffer, sizeof(int));
      pBuffer += sizeof(int);
      for (int k=0; k<numLocRB; k++)
      {
        Location* loc = new Location();
        memcpy(loc, pBuffer, sizeof(Location));
        pBuffer += sizeof(Location);  
        rbLoc.push_back(*loc);      
      }
      LaneBoundary* rightBoundary = new LaneBoundary("", rbLoc, rbType);
      if (DEBUG_LEVEL > 0)
      {
        cout << "Segment " << i << " Lane " << j << ": Right boundary" << endl;
        rightBoundary->print();
        cout << endl;
      }
      
      // Stop lines
      memcpy(&numStopLines, pBuffer, sizeof(int));
      pBuffer += sizeof(int);
      for (int k=0; k<numStopLines; k++)
      {
        StopLine* stopLine = new StopLine("", 0, 0);
        memcpy(stopLine, pBuffer, sizeof(StopLine));
        pBuffer += sizeof(StopLine);  
        stopLines.push_back(*stopLine);   
      }
      if (DEBUG_LEVEL > 0)
      {
        for (int k=0; k<numStopLines; k++)
        {
          cout << "Segment " << i << " Lane " << j << ": Stop line " << k << " :" << endl;
          stopLines[k].print();
          cout << endl;
        }
      }
      
      // Checkpoints
      memcpy(&numCheckpoints, pBuffer, sizeof(int));
      pBuffer += sizeof(int);
      for (int k=0; k<numCheckpoints; k++)
      {
        Checkpoint* checkpoint = new Checkpoint("", 0, 0);
        memcpy(checkpoint, pBuffer, sizeof(Checkpoint));
        pBuffer += sizeof(Checkpoint);
        checkpoints.push_back(*checkpoint);   
      }
      if (DEBUG_LEVEL > 0)
      {
        for (int k=0; k<numCheckpoints; k++)
        {
          cout << "Segment " << i << " Lane " << j << ": checkpoint " << k << " :" << endl;
          checkpoints[k].print();
          cout << endl;
        }
      }
      
      // Construct the corresponding lane object
      Mapper::Lane* lane = new Mapper::Lane(laneID, *leftBoundary, *rightBoundary, stopLines,checkpoints);
      lanes.push_back(*lane);
    }
   
    if (DEBUG_LEVEL > 0)
    {
      for (int k=0; k<numOfLanes; k++)
      {
        cout << "Segment " << i << " Lane " << k << endl;
        lanes[k].print();
        cout << endl;
      }
    }
    cout << "creating segment" << segmentID << endl;
    Mapper::Segment* segment = new Mapper::Segment(segmentID, lanes);
    segment->print();
    cout << "adding segment " << segmentID << endl;
    segments.push_back(*segment);
    cout << "added segment " << segmentID << endl;
    cout << "loop # : " << i << endl;
  }
  
  if (DEBUG_LEVEL > 0)
  {
    for (int k=0; k<numOfSegments; k++)
    {
      cout << "Segment " << k << endl;
      segments[k].print();
      cout << endl;
    }
  }

  DGCunlockMutex(&m_localMapDataBufferMutex);
  cout << "creating map" << endl; 
  *localMap = Map(segments);
  if (DEBUG_LEVEL > 0)
  {
    localMap->print();
  }
  return true;
}


bool CLocalMapTalker::SendLocalMap(Map* localMap) {
  int bytesToSend = 0;
  int bytesSent;
  char* pBuffer = m_pLocalMapDataBuffer;
  
  vector<Mapper::Segment> segments = localMap->getAllSegs();
  int numOfSegments = (int)(segments.size());
  
  DGClockMutex(&m_localMapDataBufferMutex); 
  memcpy(pBuffer, &numOfSegments, sizeof(int));
  pBuffer += sizeof(int);
  bytesToSend += sizeof(int);
  
  for (int i=0; i<numOfSegments; i++)
  {
  	int segmentID = segments[i].getID();
  	vector<Mapper::Lane> lanes = segments[i].getAllLanes();
  	int numOfLanes = lanes.size();
	memcpy(pBuffer, &segmentID, sizeof(int));
	pBuffer += sizeof(int);
	bytesToSend += sizeof(int);  
	memcpy(pBuffer, &numOfLanes, sizeof(int));
	pBuffer += sizeof(int);
	bytesToSend += sizeof(int);
	
	// Lane
	for (int j=0; j<numOfLanes; j++)
	{
		Mapper::Lane currentLane = lanes[j];
		int laneID = currentLane.getID();
		double laneWidth = currentLane.getWidth();
		memcpy(pBuffer, &laneID, sizeof(int));
		pBuffer += sizeof(int);
		bytesToSend += sizeof(int);
		memcpy(pBuffer, &laneWidth, sizeof(double));
		pBuffer += sizeof(double);
		bytesToSend += sizeof(double);
		
		// Get other info about this lane
		LaneBoundary::Divider lbType = currentLane.getLBType();
		LaneBoundary::Divider rbType = currentLane.getRBType();
		vector<Location> lbLoc = currentLane.getLB();
		vector<Location> rbLoc = currentLane.getRB();
		int numLocLB = (int)(lbLoc.size());
		int numLocRB = (int)(rbLoc.size());
		vector<StopLine> stopLines = currentLane.getStopLines();
		int numStopLines = (int)(stopLines.size());
		vector<Checkpoint> checkpoints = currentLane.getCheckpoints();
		int numCheckpoints = (int)(checkpoints.size());
		
		// Left Boundary		
		memcpy(pBuffer, &lbType, sizeof(LaneBoundary::Divider));
		pBuffer += sizeof(LaneBoundary::Divider);
		bytesToSend += sizeof(LaneBoundary::Divider);
		memcpy(pBuffer, &numLocLB, sizeof(int));
		pBuffer += sizeof(int);
		bytesToSend += sizeof(int);
		for (int k=0; k<numLocLB; k++)
		{
			memcpy(pBuffer, (char*)(&lbLoc[k]), sizeof(Location));
			pBuffer += sizeof(Location);
			bytesToSend += sizeof(Location);			
		}
		
		// Right Boundary		
		memcpy(pBuffer, &rbType, sizeof(LaneBoundary::Divider));
		pBuffer += sizeof(LaneBoundary::Divider);
		bytesToSend += sizeof(LaneBoundary::Divider);
		memcpy(pBuffer, &numLocRB, sizeof(int));
		pBuffer += sizeof(int);
		bytesToSend += sizeof(int);
		for (int k=0; k<numLocRB; k++)
		{
			memcpy(pBuffer, (char*)(&rbLoc[k]), sizeof(Location));
			pBuffer += sizeof(Location);
			bytesToSend += sizeof(Location);			
		}
		
		// Stop lines
		memcpy(pBuffer, &numStopLines, sizeof(int));
		pBuffer += sizeof(int);
		bytesToSend += sizeof(int);
		for (int k=0; k<numStopLines; k++)
		{
			memcpy(pBuffer, (char*)(&stopLines[k]), sizeof(StopLine));
			pBuffer += sizeof(StopLine);
			bytesToSend += sizeof(StopLine);			
		}
		
		// Checkpoints
		memcpy(pBuffer, &numCheckpoints, sizeof(int));
		pBuffer += sizeof(int);
		bytesToSend += sizeof(int);
		for (int k=0; k<numCheckpoints; k++)
		{
			memcpy(pBuffer, (char*)(&checkpoints[k]), sizeof(Checkpoint));
			pBuffer += sizeof(Checkpoint);
			bytesToSend += sizeof(Checkpoint);			
		}
	}
  }

  bytesSent = m_skynet.send_msg(localMapSocket, m_pLocalMapDataBuffer, bytesToSend, 0);
  
  DGCunlockMutex(&m_localMapDataBufferMutex);  
  
  if(bytesSent != bytesToSend)
  {
    cerr << "CLocalMapTalker::SendLocalMap(): sent " << bytesSent << " bytes while expected to send " << bytesToSend << " bytes" << endl;
    return false;
  }

  return true;
}
