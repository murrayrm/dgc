/**********************************************************
 **
 **  MAP.CC
 **
 **    Time-stamp: <2007-03-14 15:44:44 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Fri Feb 16 21:11:45 2007
 **
 **
 **********************************************************
 **
 ** Map Class  
 **
 **********************************************************/

#include "Map.hh"

using namespace std;


Map::Map()
{}

Map::~Map()
{}

void initLaneLines()
{
  
}

int Map::getSegmentID(point2 pt)
{
  
  cout << "in Map::getSegmentID : no matches found for point " << pt << endl;
  return -1;
}

int Map::getLaneID(point2 pt)
{
  cout << "in Map::getLaneID : no matches found for point " << pt << endl;
  return -1;
}
PointLabel Map::getClosestPointID(point2 pt)
{
  unsigned int i,j;
  MapElement el;
  int elindex =-1;
  point2 thispt,minpt;
  PointLabel minlabel;
  double mindist = -1;
  double thisdist = 0;
  for (i=0;i<prior.lanes.size(); ++i){
    elindex = prior.lanes[i].waypoint_elindex;
    prior.getEl(elindex,el);
    for (j=0;j<el.geometry.size();++j){
      thispt = el.geometry[j];
      thisdist = pt.dist(thispt);
      if ((thisdist < mindist) || mindist<0){
        minlabel = prior.lanes[i].waypoint_label[j];
        minpt = thispt;
        mindist = thisdist;
      }
    }
  }
  if (mindist<0)
    cout << "in Map::getClosestPointID : No points found " << pt << endl;
    
  return minlabel;
}
        
PointLabel Map::getNextPointID(point2 pt)
{
  unsigned i;
  double left_index;
  double right_index;
  int left_side;
  int right_side;
  int left_elindex,right_elindex;
  MapElement left_el;
  MapElement right_el;
  point2arr left_lane, right_lane;
  //  bool found=false;
  PointLabel label;
  int ptindex;
  
  for (i=0;i<prior.lanes.size();++i){
    left_elindex = prior.lanes[i].leftbound_elindex;
    right_elindex = prior.lanes[i].rightbound_elindex;
    prior.getEl(left_elindex,left_el);
    prior.getEl(right_elindex,right_el);
    left_lane=left_el.geometry;
    right_lane=right_el.geometry;
    left_lane.get_side(pt,left_side,left_index);
    right_lane.get_side(pt,right_side,right_index);
    if (left_side!=right_side){
      if (left_index>0 &&right_index>0 &&left_index<(double)left_lane.size() && right_index< (double) right_lane.size()){
    
        if (left_index>right_index)
          ptindex = (int)ceil(left_index);
        else
          ptindex = (int)ceil(right_index);
      
        label = prior.lanes[i].waypoint_label[ptindex];
        return label;
      }
    }
    
  }
  label = PointLabel(0,0,0);
  cout << "in Map::getNextPointID : no matches found for point " << pt << endl;
  return label;
}

int Map::getZoneID(point2 pt)
{
  return -1;
}



int Map::getLeftBound(point2arr& ptarr, LaneLabel &label )
{
  unsigned int i;
  MapElement el;
  int elindex =-1;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].leftbound_elindex;
      prior.getEl(elindex,el);
      ptarr = el.geometry;
    }
  }
  return 0;
}

int Map::getRightBound(point2arr& ptarr, LaneLabel &label )
{
  unsigned int i;
  MapElement el;
  int elindex =-1;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].rightbound_elindex;
      prior.getEl(elindex,el);
      ptarr = el.geometry;
    }
  }
  return 0;
}


int Map::getLeftBoundType(string& type, LaneLabel &label)
{
  unsigned int i;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label==label){
      type = prior.lanes[i].leftbound_type;
      return 0;
    }
  }
  return -1;
}
int Map::getRightBoundType(string& type, LaneLabel &label )
{
  unsigned int i;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      type = prior.lanes[i].rightbound_type;
      return 0;
    }
  }
  return -1;

}

double Map::getObstacleDist(const point2& state, const int lanedelta)
{
  double mindist= 1000000;
  double thisdist;
  unsigned i;
  MapElement el;
  double left_index;
  double right_index;
  double left_index2;
  double right_index2;
  int left_side;
  int right_side;
  int left_elindex,right_elindex;
  MapElement left_el;
  MapElement right_el;
  point2arr left_lane, right_lane;
  LaneLabel label;

  for (i=0;i<prior.lanes.size();++i){
    left_elindex = prior.lanes[i].leftbound_elindex;
    right_elindex = prior.lanes[i].rightbound_elindex;
    prior.getEl(left_elindex,left_el);
    prior.getEl(right_elindex,right_el);
    left_lane=left_el.geometry;
    right_lane=right_el.geometry;
    left_lane.get_side(state,left_side,left_index);
    right_lane.get_side(state,right_side,right_index);
    if (left_side!=right_side){
      if (left_index>0 &&right_index>0 &&left_index<(double)left_lane.size() && right_index< (double) right_lane.size()){
        label = prior.lanes[i].label;
        break;
      }
    }
    
  }
  cout << "in Map::getObstacleDist() getting obstacles in lane "<< label << endl;
  
  int obsid = -1;
  point2 cpt;
  
  for (i=0;i<data.size();++i){
    el = data[i];
    if (el.height<=0)
      continue;
    cpt = el.center;
    left_lane.get_side(cpt,left_side,left_index2);
    right_lane.get_side(cpt,right_side,right_index2);
    if (left_side!=right_side){
      if (left_index2>left_index &&right_index2>right_index &&left_index<(double)left_lane.size() && right_index< (double) right_lane.size()){
        
        thisdist = cpt.dist(state);
        if (thisdist < mindist){
          mindist = thisdist;
          obsid = i;
        }
      }
    }
    
  }
  if (obsid >=0)
    cout << "in Map::getObstacleDist() found obstacle "
         << obsid << " at distance " << mindist 
         << " in lane " << label<< endl;
  else
    cout << "in Map::getObstacleDist() found no obstacles in lane " << label<< endl;
  return mindist;
}



point2 Map::getObstaclePoint(const point2& state, const double offset)
{
  double mindist= 1000000;
  double thisdist;
  unsigned i;
  MapElement el;
  double left_index;
  double right_index;
  double left_index2;
  double right_index2;
  int left_side;
  int right_side;
  int left_elindex,right_elindex;
  MapElement left_el;
  MapElement right_el;
  point2arr left_lane, right_lane;
  LaneLabel label;
  int laneindex;

  for (i=0;i<prior.lanes.size();++i){
    left_elindex = prior.lanes[i].leftbound_elindex;
    right_elindex = prior.lanes[i].rightbound_elindex;
    prior.getEl(left_elindex,left_el);
    prior.getEl(right_elindex,right_el);
    left_lane=left_el.geometry;
    right_lane=right_el.geometry;
    left_lane.get_side(state,left_side,left_index);
    right_lane.get_side(state,right_side,right_index);
    if (left_side!=right_side){
      if (left_index>0 &&right_index>0 &&left_index<(double)left_lane.size() && right_index< (double) right_lane.size()){
        label = prior.lanes[i].label;
        laneindex = i;
        break;
      }
    }
    
  }
  cout << "in Map::getObstaclePoint() getting obstacles in lane "<< label << endl;
  
  int obsid = -1;
  point2 cpt,obspt;
  
  for (i=0;i<data.size();++i){
    el = data[i];
    if (el.height<=0)
      continue;
    cpt = el.center;
    left_lane.get_side(cpt,left_side,left_index2);
    right_lane.get_side(cpt,right_side,right_index2);
    if (left_side!=right_side){
      if (left_index2>left_index &&right_index2>right_index &&left_index<(double)left_lane.size() && right_index< (double) right_lane.size()){
        
        thisdist = cpt.dist(state);
        if (thisdist < mindist){
          mindist = thisdist;
          obsid = i;
          obspt = cpt;
        }
      }
    }
    
  }
  if (obsid >=0)
    cout << "in Map::getObstaclePoint() found obstacle "
         << obsid << " at distance " << mindist 
         << " in lane " << label<< endl;
  else
    cout << "in Map::getObstaclePoint() found no obstacles in lane " << label<< endl;



  point2 outpt;
  if (obsid >=0){
    int waypt_elindex = prior.lanes[laneindex].waypoint_elindex;
    MapElement waypt_el;
    prior.getEl(waypt_elindex, waypt_el);  
    point2arr wayptarr;
    wayptarr =waypt_el.geometry;



    double wayptdist = wayptarr.cut_front(obspt);
    if (wayptdist>offset){
      wayptarr.cut_front(wayptdist-offset);
      outpt = wayptarr.back();
    }
    else {
      cout << "in Map::getObstaclePoint , offset pushes us out of lane" << endl;
      outpt = state;
    }
  }
  return outpt;
}


bool Map::isExit(PointLabel &label){
  unsigned int i,j;
  //  int elindex =-1;
  //  int ptindex =-1;
  //bool isexit = false;
  //MapElement el;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      //elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].exit_label.size();++j){
        if (prior.lanes[i].exit_label[j]==label){
          return true;
        }
      }
    }
  }
  return false;
}

bool Map::isStop(PointLabel &label){
  unsigned int i,j;
  //  int elindex =-1;
  //  int ptindex =-1;
  //  bool isstop = false;
  //MapElement el;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      //elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].stop_label.size();++j){
        if (prior.lanes[i].stop_label[j]==label){
          return true;
        }
      }
    }
  }
  return false;
}

int Map::getStopline(point2& pt, PointLabel &label)
{
  unsigned int i,j;
  int elindex =-1;
  int ptindex =-1;
  bool isstop = false;
  MapElement el;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].stop_label.size();++j){
        if (prior.lanes[i].stop_label[j]==label){
          isstop = true;
        }
      }
      if (!isstop)
        continue;

      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==label){
          
          ptindex =j;
          prior.getEl(elindex,el);
          pt = el.geometry[ptindex];
          return 0;
        }
      }
    }
  }
  cerr << "in Map::getStopline, stop line not found" << label << endl;
  
  return -1;
}
int Map::getStopline(point2& pt, point2 state)
{
  unsigned int i,j;
  int elindex =-1;
  int ptindex =-1;
  //  bool isstop = false;
  MapElement el;

  PointLabel label, stoplabel;
  

  label = getNextPointID(state);
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].stop_label.size();++j){
        if (prior.lanes[i].stop_label[j].point>=label.point){
          stoplabel = prior.lanes[i].stop_label[j];
          break;
        }
      }

      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==stoplabel){
          
          ptindex =j;
          prior.getEl(elindex,el);
          pt = el.geometry[ptindex];
          return 0;
        }
      }
    }
  }
  cerr << "in Map::getStopline, stop line not found" << label << endl;
  
  return -1;
}

int Map::getNextStopline(point2& pt, PointLabel &label)
{
  unsigned int i,j;
  int elindex =-1;
  int ptindex =-1;
  //  bool isstop = false;
  MapElement el;

  PointLabel stoplabel;
  

  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].stop_label.size();++j){
        if (prior.lanes[i].stop_label[j].point>=label.point){
          stoplabel = prior.lanes[i].stop_label[j];
          break;
        }
      }

      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==stoplabel){
          
          ptindex =j;
          prior.getEl(elindex,el);
          pt = el.geometry[ptindex];
          label = stoplabel;
          return 0;
        }
      }
    }
  }
  cerr << "in Map::getNextStopline, stop line not found" << label << endl;
  
  return -1;
}



int Map::getWaypoint(point2& pt, PointLabel &label)
{
  unsigned int i,j;
  int elindex =-1;
  int ptindex =-1;
  MapElement el;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==label){
          ptindex =j;
          prior.getEl(elindex,el);
          pt = el.geometry[ptindex];

          return 0;
        }
      }
    }
  }
  cerr << "in Map::getWaypoint, waypoint not found" << label <<endl;
  return -1;

}

int Map::getHeading(double & ang,const PointLabel &label)
{
  unsigned int i,j;
  int elindex =-1;
  int ptindex =-1;
  MapElement el;
  point2 pt,lastpt,nextpt,nextdpt,lastdpt;
  double nextang, lastang;
  bool nextflag = false;
  bool lastflag = false;
  
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==label){
          ptindex =j;
          prior.getEl(elindex,el);
          pt = el.geometry[ptindex];
          if (ptindex>0){
            lastpt = el.geometry[ptindex-1];
            lastdpt = pt-lastpt;
            lastang = lastdpt.heading();
            lastflag = true;
          } 
          if( ptindex<(el.geometry.size()-1)){
            nextpt = el.geometry[ptindex+1];
            nextdpt = nextpt-pt;
            nextang = nextdpt.heading();
            nextflag = true;
          }
          if(lastflag&&nextflag){
            double tmpang = 0.5*(nextang+lastang);           
            if (fabs(nextang-lastang)>M_PI){
              tmpang = tmpang+M_PI;
              if (tmpang > M_PI)
                tmpang = tmpang-2*M_PI;
            }
            ang = tmpang;
            return 0;
          }else if(lastflag){
            ang = lastang;
            return 0;
          }else if (nextflag){
            ang = nextang;
            return 0;
          }else{
            cerr << "in Map::getWaypoint, lane too short?" << label <<endl;
            return -1;
          }
        }
      }
    }
  }
  cerr << "in Map::getWaypoint, waypoint not found" << label <<endl;
  return -1;
  
}
 

  int Map::getTransitionBounds(point2arr &leftbound ,point2arr &rightbound , PointLabel &exitlabel, PointLabel &enterlabel)
    {
      unsigned int i,j;
      int exit_elindex =-1;
      int exit_leftelindex =-1;
      int exit_rightelindex =-1;
      int exit_ptindex =-1;
      int enter_elindex =-1;
      int enter_leftelindex =-1;
      int enter_rightelindex =-1;

      int enter_ptindex =-1;
  
      point2 exitpt;
      point2 enterpt;
      MapElement el;

      bool isvalid = false;

      LaneLabel tmplane(enterlabel.segment,enterlabel.lane);
      if ((exitlabel.segment==enterlabel.segment) && (exitlabel.lane==enterlabel.lane)){
        getLeftBound(leftbound, tmplane);
        int retval = getRightBound(rightbound, tmplane);
        cout << "returning current lane transition" << endl;
        return retval;
      }

      for (i=0;i<prior.lanes.size(); ++i){
        if (prior.lanes[i].label== exitlabel){
    
          for (j=0;j<prior.lanes[i].exit_label.size();++j){
            if (prior.lanes[i].exit_label[j]==exitlabel &&
                prior.lanes[i].exit_link[j]==enterlabel){
        

              isvalid = true;
            }
          }
          if (!isvalid)
            continue;
      
          for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
            if (prior.lanes[i].waypoint_label[j]==exitlabel){  exit_elindex = prior.lanes[i].waypoint_elindex;
            exit_leftelindex = prior.lanes[i].leftbound_elindex;
            exit_rightelindex = prior.lanes[i].rightbound_elindex;
            exit_ptindex =j;
            break;
            }
          }
        }
      }
      if (isvalid){
        for (i=0;i<prior.lanes.size(); ++i){
          if (prior.lanes[i].label== enterlabel){
    
            for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
              if (prior.lanes[i].waypoint_label[j]==enterlabel){
                enter_elindex = prior.lanes[i].waypoint_elindex;
                enter_leftelindex = prior.lanes[i].leftbound_elindex;
                enter_rightelindex = prior.lanes[i].rightbound_elindex;
                enter_ptindex =j;
                break;
              }
            }
          }
        }
        prior.getEl(enter_leftelindex,el);
        enterpt = el.geometry[enter_ptindex];  
        prior.getEl(exit_leftelindex,el);
        exitpt = el.geometry[exit_ptindex];  

        leftbound.clear();
        leftbound.push_back(enterpt);
        leftbound.push_back(exitpt);

        prior.getEl(enter_rightelindex,el);
        enterpt = el.geometry[enter_ptindex];  
        prior.getEl(exit_rightelindex,el);
        exitpt = el.geometry[exit_ptindex];  

        rightbound.clear();
        rightbound.push_back(enterpt);
        rightbound.push_back(exitpt);
  
        return 0;
      }


      cerr << "in Map::getTransitionBounds, invalid transition, exit = " << exitlabel << " enter = " << enterlabel <<endl;
      return -1;
    }


  int Map::getBounds(point2arr& leftbound, point2arr& rightbound,const LaneLabel &label, const point2 state, const double range )
    {
      unsigned i;
      MapElement el;
      //  double left_index;
      //double right_index;
      //  double left_index2;
      // double right_index2;
      //int left_side;
      //int right_side;
      int left_elindex,right_elindex, waypt_elindex;
      MapElement left_el;
      MapElement right_el;
      MapElement waypt_el;
      point2arr left_lane, right_lane, waypts;

      bool validlabel = false;
  
      for (i=0;i<prior.lanes.size(); ++i){
        if (prior.lanes[i].label == label){
          left_elindex = prior.lanes[i].leftbound_elindex;
          right_elindex = prior.lanes[i].rightbound_elindex;
          waypt_elindex = prior.lanes[i].waypoint_elindex;
          prior.getEl(left_elindex,left_el);
          prior.getEl(right_elindex,right_el);
          prior.getEl(waypt_elindex,waypt_el);
          left_lane=left_el.geometry;
          right_lane=right_el.geometry;
          waypts=waypt_el.geometry;
          validlabel = true;
          break;
        }
      }
      if(!validlabel){
        cout << "in Map::getBounds() passed bad lane label "<<label <<endl;
        return -1;
      }
  


      double BACK_RANGE = 5;
      waypts = waypt_el.geometry;
      double statedist = waypts.project_along(state);

      double neardist = statedist-BACK_RANGE;

      if (neardist<0)
        neardist = 0;


      waypts.cut_back(waypts.linelength()-neardist);
      waypts.cut_front(range);

      point2 nearpt,farpt;
      nearpt = waypts[0];
      farpt = waypts.back();
      left_lane.cut_back(nearpt);
      left_lane.cut_front(farpt);

      right_lane.cut_back(nearpt);
      right_lane.cut_front(farpt);

      leftbound = left_lane;
      rightbound = right_lane;
      return 0;
    } 
 
  int Map::getBoundsReverse(point2arr& leftbound, point2arr& rightbound,const LaneLabel &label, const point2 state, const double range )
    {
      unsigned i;
      MapElement el;
      //  double left_index;
      //double right_index;
      //  double left_index2;
      // double right_index2;
      //int left_side;
      //int right_side;
      int left_elindex,right_elindex, waypt_elindex;
      MapElement left_el;
      MapElement right_el;
      MapElement waypt_el;
      point2arr left_lane, right_lane, waypts;

      bool validlabel = false;
  
      for (i=0;i<prior.lanes.size(); ++i){
        if (prior.lanes[i].label == label){
          left_elindex = prior.lanes[i].leftbound_elindex;
          right_elindex = prior.lanes[i].rightbound_elindex;
          waypt_elindex = prior.lanes[i].waypoint_elindex;
          prior.getEl(left_elindex,left_el);
          prior.getEl(right_elindex,right_el);
          prior.getEl(waypt_elindex,waypt_el);
          left_lane=left_el.geometry;
          right_lane=right_el.geometry;
          waypts=waypt_el.geometry;
          validlabel = true;
          break;
        }
      }
      if(!validlabel){
        cout << "in Map::getBounds() passed bad lane label "<<label <<endl;
        return -1;
      }
      waypts = waypt_el.geometry;

      //--------------------------------------------------
      // reversal
      //--------------------------------------------------
      left_lane.reverse();
      right_lane.reverse();
      waypts.reverse();
      point2arr tmp;
      tmp = left_lane;
      left_lane = right_lane;
      right_lane = tmp;
  


      double BACK_RANGE = 5;
  
      double statedist = waypts.project_along(state);

      double neardist = statedist-BACK_RANGE;

      if (neardist<0)
        neardist = 0;


      waypts.cut_back(waypts.linelength()-neardist);
      waypts.cut_front(range);

      point2 nearpt,farpt;
      nearpt = waypts[0];
      farpt = waypts.back();
      left_lane.cut_back(nearpt);
      left_lane.cut_front(farpt);

      right_lane.cut_back(nearpt);
      right_lane.cut_front(farpt);

      leftbound = left_lane;
      rightbound = right_lane;
      return 0;
    } 
 

  int Map::getTransitionBounds(point2arr &leftbound ,point2arr &rightbound , const PointLabel &exitlabel, const PointLabel &enterlabel, const point2 state, const double range)
    {
      unsigned int i,j;
      int exit_elindex =-1;
      int exit_leftelindex =-1;
      int exit_rightelindex =-1;
      int exit_ptindex =-1;
      int enter_elindex =-1;
      int enter_leftelindex =-1;
      int enter_rightelindex =-1;

      int enter_ptindex =-1;
  
      point2 exitpt;
      point2 enterpt;
 
  
      bool isvalid = false;

      LaneLabel exit_lanelabel, enter_lanelabel;

      exit_lanelabel.segment = exitlabel.segment;
      exit_lanelabel.lane = exitlabel.lane;

      enter_lanelabel.segment = enterlabel.segment;
      enter_lanelabel.lane = enterlabel.lane;

      if (exit_lanelabel == enter_lanelabel){
        int retval = getBounds(leftbound, rightbound, enter_lanelabel,state, range);
        cout << "returning current lane transition" << endl;
        return retval;
      }

      for (i=0;i<prior.lanes.size(); ++i){
        if (prior.lanes[i].label== exitlabel){
    
          for (j=0;j<prior.lanes[i].exit_label.size();++j){
            if (prior.lanes[i].exit_label[j]==exitlabel &&
                prior.lanes[i].exit_link[j]==enterlabel){
          

              isvalid = true;
            }
          }
          if (!isvalid)
            continue;
      
          for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
            if (prior.lanes[i].waypoint_label[j]==exitlabel){  exit_elindex = prior.lanes[i].waypoint_elindex;
            exit_leftelindex = prior.lanes[i].leftbound_elindex;
            exit_rightelindex = prior.lanes[i].rightbound_elindex;
            exit_ptindex =j;
        
            break;
            }
          }
        }
      }
      if (!isvalid){
        cerr << "in Map::getTransitionBounds, invalid transition, exit = " << exitlabel << " enter = " << enterlabel <<endl;
        return -1;

      }
      isvalid = false;
      for (i=0;i<prior.lanes.size(); ++i){
        if (prior.lanes[i].label== enterlabel){
    
          for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
            if (prior.lanes[i].waypoint_label[j]==enterlabel){
              enter_elindex = prior.lanes[i].waypoint_elindex;
              enter_leftelindex = prior.lanes[i].leftbound_elindex;
              enter_rightelindex = prior.lanes[i].rightbound_elindex;
              enter_ptindex =j;
              isvalid = true;
              break;
            }
          }
        }
      }

      if (!isvalid){
        cerr << "in Map::getTransitionBounds, invalid transition for an unexpected reason.  Likely error in RNDF parsing.  exit = " << exitlabel << " enter = " << enterlabel <<endl;
        return -1;
      }

      MapElement enter_leftel, enter_rightel;
      MapElement exit_leftel, exit_rightel;
      point2arr enter_leftarr, enter_rightarr;
      point2arr exit_leftarr, exit_rightarr;
      point2 enterpt_left, enterpt_right;
      point2 exitpt_left, exitpt_right;


      prior.getEl(enter_leftelindex,enter_leftel);
      enter_leftarr = enter_leftel.geometry;

      prior.getEl(enter_rightelindex,enter_rightel);
      enter_rightarr = enter_rightel.geometry;

      prior.getEl(exit_leftelindex,exit_leftel);
      exit_leftarr = exit_leftel.geometry;

      prior.getEl(exit_rightelindex,exit_rightel);
      exit_rightarr = exit_rightel.geometry;

      exitpt_left = exit_leftarr[exit_ptindex];  
      exitpt_right = exit_rightarr[exit_ptindex];  

      enterpt_left = enter_leftarr[enter_ptindex];  
      enterpt_right = enter_rightarr[enter_ptindex];  
  


 

      point2arr new_leftarr;
      point2arr new_rightarr;

      exit_leftarr.cut_front(exitpt_left);
      exit_rightarr.cut_front(exitpt_right);

      enter_leftarr.cut_back(enterpt_left);
      enter_rightarr.cut_back(enterpt_right);

      new_leftarr = exit_leftarr;
      new_rightarr = exit_rightarr;

      double distleft = exitpt_left.dist(enterpt_left);
      double distright = exitpt_right.dist(enterpt_right);



      if (distleft<distright){
        new_leftarr.connect(enter_leftarr);
        new_rightarr.connect_intersect(enter_rightarr);
      }else{
        new_leftarr.connect_intersect(enter_leftarr);
        new_rightarr.connect(enter_rightarr);
      }

      //  waypts = waypt_el.geometry;
      //  if (left_neardist<0)
      //     left_neardist = 0;

      // if (right_neardist<0)
      //     right_neardist = 0;
      cout << "SETTING BACK_RANGE TO 20 FOR NOW 3/14" << endl;
      double BACK_RANGE = 20;  
      //      double BACK_RANGE = 5;  
      
      new_leftarr.cut_back(state,BACK_RANGE);
      new_rightarr.cut_back(new_leftarr[0]);
      

      if (distleft>distright){
        new_leftarr.cut_front(range);
        new_rightarr.cut_front(new_leftarr.back());
      }else{
        new_rightarr.cut_front(range);
        new_leftarr.cut_front(new_rightarr.back());
      }

      //   waypts.cut_front(range);

      //   point2 nearpt,farpt;
      //   nearpt = waypts[0];
      //   farpt = waypts.back();
      //   left_lane.cut_back(nearpt);
      //   left_lane.cut_front(farpt);

      //   right_lane.cut_back(nearpt);
      //   right_lane.cut_front(farpt);

      leftbound = new_leftarr;
      rightbound = new_rightarr;
      return 0; 

 
    }








  int Map::getObstacleGeometry(point2arr& ptarr)
    {
      return 0;
    }
