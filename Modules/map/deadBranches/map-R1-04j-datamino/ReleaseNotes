              Release Notes for "map" module

Release R1-04j (Fri Sep 28  6:36:08 2007):
	Updated MapElementTalker to include a blocking read with timeout :
recvMapElementTimedBlock.

	Added ELEMENT_DEBUG map element type which can be used to send
formatted debugging data.

	Added function Map::copyMapData which is a fast method to copy only
the data to a second map which is accessed by the map queries and by
planner. Secondary data used for fusion is not copied, and non-updated
lane-lines are not copied.  This scales better with large RNDFs.
	 
	Updated line fusion option in Map such that lane lines are not
densified with interpolation if line fusioin is not turned on.
	
Commented out MSG calls which can slow down the receive loop.  


Release R1-04i (Thu Sep 27  1:44:53 2007):
	adding rear stereo obs perceptor and ladar obs perceptor to enum

Release R1-04h (Fri Sep 21 10:29:44 2007):
        New features:
	- Implemented association of lane lines with the whole segment, not
	just the current lane.
	- Implemented proper curb handling (tested on logs from El Toro).
	- Now taking into consideration if a line should be present or not,
	as specified by the rndf file (left/right_boundary).
        Other bug fixes and minor changes:
	- Fixed bug that sometimes was causing wrong associations. Note: This
        won't solve the problems we had following the wrong lane ... yet.
	- Added Map::fuseAllLines(state), which is faster because it determines
        the current segment only once instead of for each sensed line (using
        its MapElement::state).
	- Now smoothing the center of the lane, instead of whichever side was
        updated, giving better results.
        - Added testLineFus, a reduced version of mapviewer and mapper just to test
        line fusion without having to send MapElement over the net. This way, the
        information shown on the screen match exactly the data in memory.

Release R1-04g (Fri Sep 21  4:54:55 2007):
Added functionality to maintain an RNDF frame offset.  Added
	getElGlobal and getElRndf methods to return elements in those
	frames. 

Release R1-04f (Tue Sep 18 11:32:19 2007):
	added getObsInPoly function which checks for obstacles overlapping
	a given polygon

Release R1-04e (Mon Sep 17  8:25:16 2007):

	Implemented functions to support allowing the local frame to drift
	with respectto global frame.  The default query functions still return
	everything in local frame for now, so adjustments are still needed
	to the planner interface for full integration of a drifting
	local-global transformation.

	Added functionality to allow for the updating of the fused lane
	information from an external source which should enable lane
	fusion to run separately from planner and obstacle fusion if
	desired.  Fused lane map elements added to the map which match ids
	with a lane element in the map prior can now just replace the
	corresponding prior element instead of having to recompute the
	line fusion in the loop.



Release R1-04d (Fri Sep 14  5:18:57 2007):
	adding special handling of elements from prediction - works in simulation. Christian is planning on making the planner changes and the appropriate releases in a few hours.

	new MapElement.setGeometry(int type) function 

Release R1-04c (Thu Sep 13 16:10:05 2007):
	Cleaning up after quick fix made w/ Vanessa. So long as all
	perceptors use their module ID as first # in element ID, addEl will
	properly handle clears. (I've checked with everybody responsible
	for a perceptor)

	adding ELEMENT_VEHICLE_PREDICTION and appropriate accessor functions 
	to MapElement

Release R1-04b (Thu Sep 13 13:30:45 2007):
	Fixed bug in which map elements were not being cleared properly.  

Release R1-04a (Tue Sep 11  1:55:43 2007):
	*adding test function for getObsInBounds, self-overlap
	*switching order of line/obstacle handling in addEl  
	*calling updateFromGeometry() in addEl (min/max used in isOverlap, better to calculate them BEFORE calling it)
	*now checking in isOverlap that geometryMin/geometryMax have been initialized before they're used. 


Release R1-04 (Mon Sep 10 15:37:55 2007):
	added getZoneEntries and getZoneExits function to map query
	function list.	Works the same as getLaneEntries/Exits but for
	zones.

Release R1-03z (Mon Sep 10 15:02:56 2007):
Added mapElementTalker diagnostic functionality to
	testSendMapElement and testRecvMapElement.  Running either with no
	command line prints options for various tests which can be run to
	send and receive map elements.	Test 1 in testRecvMapElement
	outputs timing statistics for groups of received messages.  Test 2
	in testRecvMapElement outputs general statistics of the rates
	modules are sending map elements on a channel.	Test 1 in
	testSendMapElement sends a flood of a given number of map elements
	of a given size at a given rate.  All times are in useconds unless
	otherwise stated.  

Release R1-03y (Mon Sep 10 10:56:18 2007):
	Added getObsInZone function to map interface and a test in testMap

Release R1-03x (Sun Sep  9 16:47:08 2007):
	Updated MapElement.isOverlap functions to take advantage of new 
faster intersection tests.

Release R1-03w (Fri Sep  7  7:55:18 2007):
	Added timesSeen variable to MapElementTalker, so it actually gets forwarded now

Release R1-03v (Wed Sep  5 13:05:47 2007):
	Added timesSeen variable to MapElement class. This is mostly for simulation use.

Release R1-03u (Wed Sep  5 11:46:06 2007):
	Changing addEl to properly update the IDtoINDEX array for
	mapper->planner messages

Release R1-03t (Tue Sep  4  0:11:40 2007):
	*now properly handles the time stopped field in map elements

	*gets rid of duplicate fields in the objectData struct

	Changes made and tested in field on Mon evening: 

	* addEl returns -1 on failure (same as before) but returns 1-3 to signify whether the element was added using the IDtoINDEX lookup, was associated w/ a pre-existing element, or if it created a new element. This is useful for the debugging display in mapper.  

	*changing the order of an enum


Release R1-03s (Mon Sep  3 15:25:05 2007):
	Added new zone parking spot access functions.  getZoneParkingSpots
	updates by reference an array of parking spot labels in a given
	zone.  getSpotWidth updates by reference the width of the spot in
	meters.  Both functions return -1 if the given label is invalid and
	0 if there is no error

Release R1-03r (Sun Sep  2 16:04:03 2007):
	*adding special handling of messages from mapper, so planner's map
	and mapper's map can be kept identical.

        *adding left roof ladar to perceptor enum

Release R1-03q (Sat Sep  1  7:51:27 2007):
	Implementing new data structure that allows for better association/fusion between different sensors. 

Release R1-03p (Thu Aug 30 18:25:23 2007):
	Fixed: now correctly setting the z coordinate for position, center,
	velocity, geometryMin, geometryMax in MapElementTalker.

Release R1-03o (Wed Aug 29 19:52:03 2007):
        Made the line fusion a lot more robust when given bad data. Lanes
        are never squeezed (or shouldn't at least ...), the line connecting
        corresponding points in the left and right bound is always orthogonal
        to the center of the lane, fixed a bug when converging back to the
        original rndf.

Release R1-03n (Tue Aug 28 17:19:05 2007):
	Fixed typo in previous release (MapId::operator> and MapId::operator<)

Release R1-03m (Tue Aug 28 14:43:47 2007):
	Added operator> and operator< to MapId so that class can be used as a key in std::map. (Run test #6 to check capability if desired)

Release R1-03l (Mon Aug 27 17:19:05 2007):
	Fixed bugs related to returning zone entrance and exits in the map
	query functions getWayPointExits/Entries and getLaneExits/Entries. 
	Changed default MapElement creation behavior remove bounding box
	calculation to reduce computational complexity.

Release R1-03k (Thu Aug 23 11:28:02 2007):
	Changed color of trav path to yellow so it's more low-key

Release R1-03j (Wed Aug 22 14:49:06 2007):
	Fixed bug when sensed line has size 0. This may be the cause of
	what we've seen in
        http://gc.caltech.edu/wiki07/index.php/Spread_debugging_2007-08-18#Run_5_-_18:34
        even though I think nobody should send 0-sized lines (or maybe I'm
        trying to fuse something that is not a line, I'll debug it further).
        Asserted that prior lane line has size > 0.

Release R1-03i (Mon Aug 20 16:52:24 2007):
	changing the MapElementTalker to actually copy over the z
	coordinates of the point2arr


Release R1-03h (Wed Aug 15 23:45:28 2007):
	This version should be able to receive and fuse data from the
        ladar-curb-perceptor, although it will treat curbs as if they
        were lane lines. Proper curb handling will be implemented in
        some future release.
        Field changes: interpolating sensed line before association, since
	I use votes instead of just minimum distance. Some dbg messages.

Release R1-03g (Tue Aug 14 23:09:05 2007):
        First release with lane fusion implemented and working to some
        extend. This version was tested at stluke ad worked well.
        At the moment, no check is done to make sure that two lanes in
        the same segment don't overlap (coming soon).
        The parameters of the fusion algorithm are hardcoded, need to make
        them tunable in a later release.

Release R1-03f (Thu Aug  2 13:33:20 2007):
	Implemented Map::getZonePerimeter() and Map::getSpotWaypoints().

Release R1-03e (Tue Jul 31 10:48:25 2007):
	adding more options to the mapElementType enum, to help with fusion

Release R1-03d (Tue Jul 17  7:59:23 2007):
	added timeStopped field to MapElement class.  This will maintain
	the amount of time the obstacle has not been perceived to have
	moved which is useful for planning decisions.	Also cleaned up a
	few comments

Release R1-03c (Wed Jul 11  0:59:52 2007):
	Rolled in field changes which fixed transition bounds for uturn
	transitions in lane.

Release R1-03b (Thu Jun 28  2:36:24 2007):
	Added new version of getHeading function which takes a lane label
	and a point and returns the point projected into the lane and the
	heading of the lane at that projected point

Release R1-03a (Fri Jun 22 14:03:24 2007):
	removed cout comments which mess up tplanner console.  Cerr
	comments are still there, but nothing that outputs text in nominal
	operation.

Release R1-03 (Thu Jun 21 17:05:11 2007):
	updated get transition bounds function to be more robust across
	straight but offset transitions.

Release R1-02z (Sun Jun 10 10:36:01 2007):
	updated some test functions for the point2arr class

Release R1-02y (Sat Jun  9  9:24:06 2007):
	Added explicit copy constructor and assignment operator for the
	MapElement class.  Added ELEMENT_FREE_SPACE type to MapElement
	class which will represent empty space.

Release R1-02x (Thu Jun  7 18:21:22 2007):
	Fixed bugs in getShortTransitionBounds and in getObsInBounds

Release R1-02w (Thu Jun  7  9:58:24 2007):
	Added function getShortTransitionBounds which just returns the
	portion of the transition bound inside the intersection.

Release R1-02v (Wed Jun  6 19:50:11 2007):
	Updated existing map stopline functions used by tplanner to call
	the function setStopLineSensed so that if a stopline is detected,
	and within 2 meters of the rndf stopline, the sensed stopline point
	will be returned.  Nothing will change if the stopline perceptors
	are not on.

Release R1-02u (Tue Jun  5 16:33:44 2007):
	Fixed bug in function isPointInLane in MapPrior

Release R1-02t (Mon Jun  4  9:00:20 2007):
	fixed bug in function in MapElement which sets the geometry given a
	centerpoint, length, width and orientation.  This function is
	unused in the current software (probably because it was broken and
	there were alternatives) but it works and has been tested now.

Release R1-02s (Sat Jun  2  0:44:28 2007):
	updated getTransitionBounds function to return interpolated corners

Release R1-02r (Fri Jun  1  5:03:50 2007):
	Small update to lane checking functions to move overlap queries to
	use interpolated lanes


Release R1-02q (Fri Jun  1  4:35:47 2007):
	updated lane functions to all return full interpolated lane.  Fixed
	some problems in old functions which should be retired but are still being used.
	added getStopLineSensed function to get the sensed stopline if we
	see one. 	updated some map functions to help mapper send
	obstacles to the mplanner in a more robust way.



Release R1-02p (Tue May 29 23:37:19 2007):
	Added elements for initial and final conditions from OCPspecs

Release R1-02o (Mon May 28 13:03:10 2007):
	Changed color for polytopes so they wouldn't be confused w/visible obstacles

Release R1-02n (Sat May 26  0:21:32 2007):
	updated getBounds and getBoundsReverse functions to return the
	interpolated data around some turns.  This is a temporary fix, as
	in the future, funtionality should be built on top of the
	getLaneBounds functions. Note that the range argument of the
	funcitons specify the minimum total length of the lines to be
	returned, and the last optional argument, backrange, specifies the
	distance behind state to start the line.  If range and backrange
	are both 20 for example, then the lane lines will start around 20
	meters behind state and extend up to the state point given.  To
	specify lanes that start 20 meters behind and extend 20 meters in
	front, specify range=40 and backrange=20.  This function will
	return left and right lane lines of equal size and will trim to the 
	nearest waypoint.

Release R1-02m (Fri May 25 14:05:11 2007):
	updated interpolation to make it sparser for now to help with the
	polytope creation of planning

Release R1-02l (Wed May 23 13:18:33 2007):
	Updated dist functions in MapElement which calculate the min dist
	to other map elements as well as points or polylines.  Can be used
	to check for partial blocking of lanes.  These functions will
	return 0 if there is overlap between the geometries.  They haven't
	been fully tested but the interface shouldn't change.

Release R1-02k (Sat May 19 17:54:25 2007):
	Added polytope and planner traj elements

Release R1-02j (Sat May 19 10:26:55 2007):
	Fixed some other issues with getTransitionBounds.  Added some new
	transition handling functionality but it's not used yet.  

Release R1-02i (Fri May 18 15:42:31 2007):
	changed retired getTransitionBounds function to not use ptarr
	assignment operator.  May fix a bug in tplanner.  Some intermediate
	work is done on a priori transition bounds calculations.

Release R1-02h (Fri May 18 10:16:54 2007):
	updated getTransitionBounds to work a bit better

Release R1-02g (Fri May 18  9:46:41 2007):
	quick fix for issue getTransitionBounds function in Map.cc has with
	santa anita rndf which loops back on itself.  need a better fix for
	long term and that function itself is going to be retired.


Release R1-02f (Thu May 17 17:31:20 2007):
	reorganized MapPrior RNDF parsing, fixed bug in old function getObstacleDist and GetobstaclePoint.


Release R1-02e (Thu May 17 15:27:39 2007):
	updated interpolation functions in MapPrior.  Updated the
	getWayPointEntries and getWayPointExits functions to return the
	possible transition in the same lane across

Release R1-02d (Wed May 16 23:20:39 2007):
  Very minor tweak to includes for cross-platform build compatability.

Release R1-02c (Tue May 15 23:34:49 2007):
	Fixed bug in getObs functions which would return sensed lanes as
	well as obstacles and vehicles.  Added functions isObstacle() and
	isLine() to MapElement.  Other small bug fixes.

Release R1-02b (Mon May 14 23:29:43 2007):
	Added RDDF, steering, and commanded steering splines

Release R1-02a (Mon May 14 11:42:24 2007):
	Updated the map interface to a cleaner, more uniform structure. 
	Had to change the format of some map interface functions, but will
	migrate all modules which use map to the new version. Implemented a
	cleaner more complete library of RNDF and obstacle query functions
	for the planners.  Cleaned up MapElement object interface which
	might also break some code, but will be easy to fix and easier to
	use from now on.  Implemented some MapElement overlap checking
	functions which use the improved bounding box functionality on the
	MapElement.  Added an elevation term to the MapElement structure to
	handle obstacles like overhanging trees which have freespace
	underneath.  Added additional timestamp to MapElement and
	additional max and min bound terms.

Release R1-01w (Thu May 10 13:52:11 2007):
	Added StateTalker, a small program to send alice info to the mapviewer. Right now it only sends alice's location and traversed path, but eventually it will send actuator state, such as steering commands. Also added a bunch of comments and cleaned up MapElement a bit.

Release R1-01v (Mon Apr 30 12:25:45 2007):
	Updated MapElementTalker to take advantage of new skynet
	functionality to send messages on a sub channel.  Implemented
	variable length skynet messages.  For modules which send map
	elements, no code change is needed, just a recompile against the
	updated map and frames modules.  For modules which receive map
	elements, add the intended subnet group to the initRecvMapElement
	function as follows : initRecvMapElement(skynetKey,recvChannel);
	Without this, change only elements on channel 0 will be received.  
	  Added map interface functions getLaneDistToWaypoint which
	calculate distance along corridor between two points.  Updated
	getBounds, getBoundsReverse and getTransitionBounds to return equal
	sized left and right bounds.

Release R1-01u (Sun Apr 29  0:12:00 2007):
	Fixing MapElement::print_state(), broken by the recent change to
	the VehicleState structure.

Release R1-01t (Wed Apr 25 17:19:24 2007):
	added getLaneID, getSegmentID, checkLaneID functions to tplanner
	interface with map.

Release R1-01s (Fri Mar 30 20:12:18 2007):
	test functions for ellipse class

Release R1-01r (Fri Mar 30  2:16:51 2007):
	<* Pleaase insert a release notes entry here. If none, then please delete these lines *>

Release R1-01q (Sat Mar 17 11:21:09 2007):
	changed color enums from COLOR_RED to MAP_COLOR_RED to avoid some
	collision when compiling lineperceptor

Release R1-01p (Sat Mar 17  0:47:19 2007):
	Added getLaneCenterPoint, getLaneRightPoint, and getLaneLeftPoint
	functions for tplanner queries.  Added functions to test map
	queries and geometry operations.

Release R1-01o (Fri Mar 16  0:27:27 2007):
	added new getHeading function which gets an estimate of the lane
	heading at some point if that point is inside valid lane bounds.

Release R1-01n (Thu Mar 15 13:59:49 2007):
	added addElement function which adds an element if the id is new
	and updates the element if it has been seen.

Release R1-01m (Wed Mar 14 15:57:56 2007):
	added getTransitionBounds and getBounds functions which are more
	functional and useful for tplanner than existing versions.  Added
	getHeading to get the heading angle at a waypoint.  Added
	getBoundsReverse for getting lanebounds in reverse.

Release R1-01l (Wed Mar 14 10:14:55 2007):
	Added getObstaclePoint(offset) function for tplanner use which
	returns a point offset from closest obstacle in lane by specified
	amount. Added more geometric test functions

Release R1-01k (Tue Mar 13 15:45:32 2007):
		Added testGeometry function to test and visualize 2D geometric
	functions.  Implemented getObstacleDist() which given a point in
	local frame returns the distance to nearest obstacle in current
	lane in front of Alice.

Release R1-01j (Mon Mar 12 15:22:34 2007):
	added getNextStopline function.  Need to debug an RNDF mirroring
	issue but this should work for short term planner testing.
	
Release R1-01i (Sun Mar 11 11:49:20 2007):
	Added clear and state messages to map elements. added test programs testMapElement, and testMapId.

Release R1-01h (Sat Mar 10 17:22:19 2007):
	Fixed bug in getStopline.

Release R1-01g (Fri Mar  9 16:33:35 2007):
	added isStop and isExit query functions to map.

Release R1-01f (Fri Mar  9 12:15:10 2007):
	Moved MapElementTalker, testSendMapElement, testRecvMapElement, and
	MapElement.hh to map module.  This is part of a general
	restructuring which moves MapElement functionality into map.  All
	users of the MapElementTalker will need to include
	map/MapElementTalker.hh and map/MapElement.hh instead of
	skynettalker/MapElementTalker and interfaces/MapElement.hh.  Also
	-lmap should be added to LIBS in the Makefile.yam if it isn't there
	already 

Release R1-01e (Thu Mar  8 18:33:06 2007):
	Updated some map functions for use in new tplanner (Sam).

Release R1-01d (Sat Mar  3 15:55:33 2007):
	Updated getStopline function to take only a point in space not a
	stopline label.

Release R1-01c (Thu Mar  1 19:34:02 2007):
	implemented first cut of getTransitionBounds and added test_map
	program to test map queries and plot the results.	Implemented the getLeftBound and getRightBound functions.

Release R1-01b (Tue Feb 27  8:39:07 2007):
	Complete overhaul of map module.  Moved new map structure from mapper into map module.  Map now parses RNDF files with an internal method and has a very different internal structure.  Still need to implement query functions. 

Release R1-01a (Sun Feb 25 12:56:28 2007):
	Updated to work with new SkynetContainer and interfaces paths

Release R1-00b (Thu Feb  1 19:27:02 2007):
	Fixed links in Makefile.yam and updated Map object for short
	term use in tplanner.

Release R1-00a (Sat Jan 27 18:25:30 2007):
	Added Map class and LocalMapTalker.

Release R1-00 (Sat Jan 27 18:14:18 2007):
	Created.



