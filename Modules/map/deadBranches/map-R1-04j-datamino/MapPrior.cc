/**********************************************************
 **
 **  MAPPRIOR.CC
 **
 **    Time-stamp: <2007-09-28 02:06:34 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Mon Feb 19 12:51:18 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include <algorithm>

#include "MapPrior.hh"

using namespace std;

bool PointLabel::operator==(const LaneLabel &label) const
{
  return (segment==label.segment && lane==label.lane);
}

bool LaneLabel::operator==(const PointLabel &label) const
{ return (segment==label.segment && lane==label.lane);}

ostream &operator<<(ostream &os, const PointLabel &label)
{
  os << "(" << label.segment <<", " << label.lane << ", " << label.point <<")" ;
  return os;
}

ostream &operator<<(ostream &os, const vector<PointLabel> &label)
{
  os << endl;
  for (unsigned int i=0;i<label.size();++i){
    os << "[" << i <<"]" << " "<< label[i] << endl;
  }
  return os;
}

ostream &operator<<(ostream &os, const LaneLabel &label)
{
  os << "(" << label.segment <<", " << label.lane <<")" ;
  return os;
}
ostream &operator<<(ostream &os, const vector<LaneLabel> &label)
{
  os << endl;
  for (unsigned int i=0;i<label.size();++i){
    os  << "[" << i << "]" << " " <<label[i] << endl;
  }  
  return os;
}

ostream &operator<<(ostream &os, const SpotLabel &label)
{
  os << "(" << label.zone <<", " << label.spot <<")" ;
  return os;
}
ostream &operator<<(ostream &os, const vector<SpotLabel> &label)
{
  os << endl;
  for (unsigned int i=0;i<label.size();++i){
    os  << "[" << i << "]" << " " <<label[i] << endl;
  }  
  return os;
}


MapPrior::MapPrior()
  : lineFusDebug(NULL), deltaYaw(0.0)
{
  internalLineFusion = false;
}

MapPrior::~MapPrior()
{}

bool MapPrior::loadRNDF(string filename)
{
  ifstream ifs;
  vector<string> strfile;
  string line;

  segments.clear();
  lanes.clear();
  zones.clear();
  spots.clear();
  checkpoints.clear();

  numSegments= -1;
  numZones = -1;
  RNDFversion = -1;

  ifs.open(filename.c_str(),ios::in);
  if (!ifs){
    cerr << "Error in MapPrior.cc, " << filename << " not found." << endl;
    return false;
  }
  
  while(getline(ifs,line)) {
    strfile.push_back(line);
  }
  
  //cout << "Parsing RNDF" << endl;
  if (!parseRNDF(strfile)){
    cerr << "MapPrior.cc: Error parsing RNDF in " << filename << endl;
    return false;
  }

  
  return true;
}

// comparator used to sort lane labels (just in case some evil guy
// stores them in random order in the rndf)
class CompareLabels
{
  RNDFsegment& seg;
public:
  CompareLabels(RNDFsegment& s) : seg(s) { }
  bool operator()(int a, int b) {
    return seg.lane_label[a].lane < seg.lane_label[b].lane;
  }
};


bool MapPrior::parseRNDF(vector<string>& strfile)
{

  int rndfIdPrefix = 0;
  int i;
  int filesize = strfile.size();
  string word;
  int thisSegment = -1;
  int thisSegmentIndex = -1;
 
  int thisNumLanes = -1;
  int thisNumPoints = -1;
  int thisNumSpots = -1;

  double FOOT_PER_METER = 3.2808399;
  //  int thisLane = -1;
  int thisLaneIndex = -1;

  int thisDataIndex = -1;

  int thisCheckPoint = -1;

  string laneString, pointString, spotString;
  point2 thisPoint;
 
  MapElement el;
  LaneLabel laneLabel;
  PointLabel pointLabel;
  SpotLabel spotLabel;
  
  int thisZone = -1;
  int thisZoneIndex = -1;
  int thisSpotIndex = -1;
  
  int parsemode = 0;
  GisCoordLatLon latlon;
  GisCoordUTM utm;

  double offset;
  point2arr ptarr;
  point2arr_uncertain ptarr_unc, cptarr_unc;

  for (i=0; i<filesize; ++i){
    istringstream lstream(strfile[i]);    
    lstream >> word;  



    if (parsemode ==0) {// base mode
    
      if (word =="RNDF_name"){ lstream >> RNDFname; continue;}
      if (word =="num_segments"){ lstream >> numSegments;continue;}
      if (word == "num_zones"){  lstream >> numZones;continue;  }
      if (word == "format_version"){lstream >> RNDFversion;  continue;}
      if (word == "creation_date"){lstream >> RNDFdate;continue;}

      if (word =="segment"){ 
        lstream >> thisSegment;
        thisSegmentIndex = segments.size();
        segments.push_back(RNDFsegment(thisSegment));
        parsemode = 1;// switch to segment mode
        continue;
      }
      if (word =="zone"){ 
        lstream >> thisZone;
        thisZoneIndex = zones.size();
        zones.push_back(RNDFzone(thisZone));
        parsemode = 3; //switch to zone mode
        continue;
      }
    } //end base mode
    if (parsemode == 1){ // segment mode
      if (word =="num_lanes"){ lstream >> thisNumLanes; continue;}
      if (word =="segment_name"){ lstream >> segments[thisSegmentIndex].name; continue;}
      if (word =="end_segment"){
        if (thisNumLanes != (int)segments[thisSegmentIndex].laneindex.size()){
          cerr << "size mismatch in segment " << thisSegmentIndex << endl;
          return false;
        }
        thisSegment = -1; 
        thisNumLanes = -1;
        parsemode = 0;  // switch to base mode
        continue;
      }
      if (word =="lane"){ 
        lstream >> laneString;
        if (!parseLaneLabel(laneString,laneLabel)) return false;
        thisLaneIndex = lanes.size();
        lanes.push_back(RNDFlane(laneLabel));
        segments[thisSegmentIndex].laneindex.push_back(thisLaneIndex);
        segments[thisSegmentIndex].lane_label.push_back(laneLabel);
        segments[thisSegmentIndex].lanedirection.push_back(0);
        
        thisDataIndex = data.size();
        el.clear();
        el.setId(rndfIdPrefix,laneLabel.segment,laneLabel.lane);
        el.setTypeWayPoints();
        el.setFrameTypeGlobal();
       
        data.push_back(el);
        lanes[thisLaneIndex].waypoint_elindex = thisDataIndex;
        parsemode = 2; // switch to lane mode;
        
        continue;
      }
    }



    if (parsemode ==2){ //lane mode
      if (word =="num_waypoints"){ lstream >> thisNumPoints; continue;}
      if (word =="lane_width"){ lstream >> lanes[thisLaneIndex].width;
      lanes[thisLaneIndex].width = lanes[thisLaneIndex].width/FOOT_PER_METER;
      continue;}
      if (word =="left_boundary"){ lstream >> lanes[thisLaneIndex].leftbound_type; continue;}
      if (word =="right_boundary"){ lstream >> lanes[thisLaneIndex].rightbound_type; continue;}
      if (word =="checkpoint"){ 
        lstream >> pointString;
        if (!parsePointLabel(pointString,pointLabel)) return false;
        lanes[thisLaneIndex].checkpt_label.push_back(pointLabel);
        lstream >> thisCheckPoint;
        lanes[thisLaneIndex].checkpt_num.push_back(thisCheckPoint);
        
        thisCheckPoint = -1;
        continue;
      }
      if (word =="stop"){ 
        lstream >> pointString;
        if (!parsePointLabel(pointString,pointLabel)) return false;
        lanes[thisLaneIndex].stop_label.push_back(pointLabel);
        continue;
      }    
      if (word =="exit"){ 
        lstream >> pointString;
        if (!parsePointLabel(pointString,pointLabel)) return false;
        lanes[thisLaneIndex].exit_label.push_back(pointLabel);
        lstream >> pointString;
        if (!parsePointLabel(pointString,pointLabel)) return false;
        lanes[thisLaneIndex].exit_link.push_back(pointLabel);
        continue;
      }
      if (word[0] >='0' && word[0] <='9'){
        if (!parsePointLabel(word,pointLabel)) return false;
        lstream >> latlon.latitude;
        lstream >> latlon.longitude;
        gis_coord_latlon_to_utm(&latlon,&utm,GEODETIC_MODEL);
        thisPoint.y = utm.e;
        thisPoint.x = utm.n;
        lanes[thisLaneIndex].waypoint_label.push_back(pointLabel);
        
        data[thisDataIndex].geometry.push_back(thisPoint);
        
        continue;
      } 
      if (word =="end_lane"){
        if (thisNumPoints != (int)data[lanes[thisLaneIndex].waypoint_elindex].geometry.size()){
          cerr << "size mismatch in lane " << thisLaneIndex << endl;
          return false;
        }
        //cout << "Closing up lane " << lanes[thisLaneIndex].label << endl;
        data[thisDataIndex].updateFromGeometry();

        lanes[thisLaneIndex].waypoint_elindex = thisDataIndex;

        cptarr_unc.set(data[thisDataIndex].geometry);

        ptarr.set(cptarr_unc);
        
        offset = lanes[thisLaneIndex].width/2;
        //        cout << "Given Offset = " << offset;        
        if (offset<.5){
          offset = 6;
        }
     
        ptarr.set(ptarr.get_offset(-offset));

        ptarr_unc = ptarr;

        thisDataIndex = data.size();
        el.clear();
        el.setId(rndfIdPrefix,laneLabel.segment,laneLabel.lane,0);
        el.setTypeLaneLine();
        el.setFrameTypeGlobal();
        el.setGeometry(ptarr_unc);
        data.push_back(el);
        lanes[thisLaneIndex].leftbound_elindex = thisDataIndex;


        ptarr.set(cptarr_unc);
        
        ptarr.set(ptarr.get_offset(offset));
        
        ptarr_unc.set(ptarr);

        thisDataIndex = data.size();
        el.clear();
        el.setId(rndfIdPrefix,laneLabel.segment,laneLabel.lane,1);
        el.setTypeLaneLine();
        el.setFrameTypeGlobal();
        el.setGeometry(ptarr_unc);
        data.push_back(el);
        lanes[thisLaneIndex].rightbound_elindex = thisDataIndex;


        parsemode = 1; //switch to segment mode
        continue;
      }
    } // end if lane mode
    


    if (parsemode ==3){ //zone mode
      if (word =="num_spots"){ lstream >> thisNumSpots; continue;}      
      if (word =="zone_name"){ lstream >> zones[thisZoneIndex].name; continue;}
      if (word =="perimeter"){ 
        lstream >> spotString;
        if (!parseSpotLabel(spotString,spotLabel)) return false;
        //zones[thisZoneIndex].perimeter_label = spotLabel;
        thisDataIndex= data.size();

        el.clear();
        el.setId(rndfIdPrefix,spotLabel.zone,spotLabel.spot);
        el.setTypePerimeter();
        el.setFrameTypeGlobal();
        data.push_back(el);
        zones[thisZoneIndex].perimeter_elindex = thisDataIndex;
        parsemode =4; // switch to perimeter mode
        continue;
      }
      if (word =="spot"){ 
       
        lstream >> spotString;
        if (!parseSpotLabel(spotString,spotLabel)) return false;
        //  cout << "spot = " << spotString << endl;
        thisSpotIndex = spots.size();
        spots.push_back(RNDFspot(spotLabel));
        zones[thisZoneIndex].spotindex.push_back(thisSpotIndex);
        zones[thisZoneIndex].spot_label.push_back(spotLabel);

        thisDataIndex= data.size();
        el.clear();
        el.setId(rndfIdPrefix,spotLabel.zone,spotLabel.spot);
        el.setTypeParkingSpot();
        el.setFrameTypeGlobal();
        data.push_back(el);
        spots[thisSpotIndex].waypt_elindex=(thisDataIndex);
        
        parsemode =5; // switch to spot mode
        continue;
      }


      if (word =="end_zone"){ 
        if (thisNumSpots != (int)zones[thisZoneIndex].spotindex.size()){
          cerr << "size mismatch in zone " << thisZoneIndex << endl;
          return false;
        }
        parsemode = 0; //switch to base mode
        continue;
      }
    } //end zone mode


    if (parsemode ==4){ //perimeter mode
      if (word =="num_perimeterpoints"){ lstream >> thisNumPoints; continue;}
      if (word =="exit"){ 
        lstream >> pointString;
        if (!parsePointLabel(pointString,pointLabel)) return false;
        zones[thisZoneIndex].exit_label.push_back(pointLabel);
        lstream >> pointString;
        if (!parsePointLabel(pointString,pointLabel)) return false;
        zones[thisZoneIndex].exit_link.push_back(pointLabel);
        continue;
      }
      if (word[0] >='0' && word[0] <='9'){
        if (!parsePointLabel(word,pointLabel)) return false;
        lstream >> latlon.latitude;
        lstream >> latlon.longitude;
        gis_coord_latlon_to_utm(&latlon,&utm,GEODETIC_MODEL);
        thisPoint.y = utm.e;
        thisPoint.x = utm.n;
        zones[thisZoneIndex].perimeter_label.push_back(pointLabel);
        data[thisDataIndex].geometry.push_back(thisPoint);
        
        continue;
      }
      if (word =="end_perimeter"){
        if (thisNumPoints != (int)data[zones[thisZoneIndex].perimeter_elindex].geometry.size()){
          cerr << "size mismatch in zone " << thisZoneIndex << endl;
          return false;
        }
        data[thisDataIndex].updateFromGeometry();
          
        parsemode = 3; //switch to zone mode
        continue;
      }

    } // end perimeter mode


    if (parsemode ==5){ //spot mode
      if (word =="spot_width"){ lstream >> spots[thisSpotIndex].width;
      spots[thisSpotIndex].width = spots[thisSpotIndex].width/FOOT_PER_METER;
      continue;}
      if (word =="checkpoint"){ 
        lstream >> pointString;
        if (!parsePointLabel(pointString,pointLabel)) return false;
        spots[thisSpotIndex].checkpt_label.push_back(pointLabel);
        lstream >> thisCheckPoint;
        spots[thisSpotIndex].checkpt_num.push_back(thisCheckPoint);
        thisCheckPoint = -1;
        continue;
      }

      if (word[0] >='0' && word[0] <='9'){
        if (!parsePointLabel(word,pointLabel)) return false;
        lstream >> latlon.latitude;
        lstream >> latlon.longitude;
        gis_coord_latlon_to_utm(&latlon,&utm,GEODETIC_MODEL);
        thisPoint.y = utm.e;
        thisPoint.x = utm.n;
        spots[thisSpotIndex].waypoint_label.push_back(pointLabel);
        data[thisDataIndex].geometry.push_back(thisPoint);
        continue;
      }
      if (word =="end_spot"){
        if (data[thisDataIndex].geometry.size() != 2){
          cerr << "size mismatch in spot " << thisSpotIndex << endl;
          return false;
        }
        data[thisDataIndex].updateFromGeometry();
        parsemode = 3; //switch to zone mode
        continue;
      }
     
    } // end spot mode
    
  } 
  fulldata.clear();
  fulldata.reserve(data.size()); // preallocate some memory
  for (i=0;i<(int)data.size();++i){
    fulldata.push_back(data[i]);
    fulldataUpdateFlag.push_back(false);
  }

  // Make sure each segment has the lanes sorted by lane label
  // The RNDF specs say lane labels will be sorted from west to east
  // or north to south. (ref: http://www.darpa.mil/grandchallenge/rules.asp,
  // "Route Network Definition File (RNDF) and Mission Data File (MDF) Formats",
  // paragraph 2.3.3). This way, there's no need to determine neighbors
  // using computational geometry and other such things.
  for (unsigned int i = 0; i < segments.size(); i++)
  {
    RNDFsegment& seg = segments[i];
    int nlanes = seg.laneindex.size();
    vector<int> perm(nlanes);
    // initialize permutation with the identity
    for (int j = 0; j < nlanes; j++)
      perm[j] = j;
    // sort indices using labels, generating a permutation
    std::sort(perm.begin(), perm.end(), CompareLabels(seg));
    // apply permutation (most of the times it will be the identity)
    RNDFsegment tmp = seg; // copy the old value (it can be done in place, but this is easier)
    for (int j = 0; j < nlanes; j++)
    {
      seg.laneindex[j] = tmp.laneindex[perm[j]];
      seg.lane_label[j] = tmp.lane_label[perm[j]];
      //seg.lanedirection[j] = tmp.lanedirection[perm[j]]; // computed later
    }
  }
  
  //--------------------------------------------------
  // Set global-local delta to the first waypoint initially
  // This will be updated by state eventually
  //--------------------------------------------------

  // need to do this before parseLaneInterpolation to avoid numeric errors
  // when line fusion is enabled
  if (data.size()>0){
    if (data[0].geometry.size()>0){
      delta = data[0].geometry[0];
    }
  }

  if (!parseExitPoints())
    return false;
  // there should be no need for this actually, lane labels already define the
  // order of the lanes as stated above
  if (!parseLaneNeighbors())
    return false;
  if (!parseLaneDirection())
    return false;
  if (!parseLaneInterpolation())
    return false;
  //  if (!parseTransitions())
  //  return false;

  return true;
}

extern const float LANE_POINTS_SPACING; // in Map.cc, FIXME: to be moved as a cmdline option
extern const float DIST_THRES; // in Map.cc, FIXME: to be moved as a cmdline option

// line fusion related functions

#define SMOOTH_ENERGY 0

static void smoothLine(point2arr_uncertain& lane, const point2arr_uncertain& rndflane,
                       int start, int end, const vector<bool>& isAssoc)
{

  if (lane.size() <= 2)
    return;

#if SMOOTH_ENERGY

  // use a cost function (energy) and try to minimize it doing one newton step
  // (follow the gradient, i.e. potential fields like)

  const float KEL = 1; // elastic constant
  const float DIST = 0; // minimum energy distance between points
  const float RATIO = 0.2; // ratio between curvature energy and elastic energy
  const float DIST_MIN = 0.01; // set distances less than this to this value (avoid div by 0)
  const float LAMBDA = 0.20; // must NOT be bigger than 0.5/KEL, maybe lower
  
  vector<point2_uncertain>::const_iterator rndfIt = rndflane.arr.begin();
  vector<point2_uncertain>::const_iterator rndfNext = rndfIt;
  ++rndfNext;

  point2 DEahead(0, 0); // apply this in the next iteration

  float prevDist[2] = {0, 0}; // keep the distances two steps behind
    
  for (int k = start + 1; k < end; k++)
  { 
    // incorporate the RNDF
    point2 rndfvec = *rndfIt - lane[k];
    float dist2 = rndfvec.norm2();

    // check for distance local maxima (at k - 1)
    if (prevDist[0] > prevDist[1] && prevDist[0] > dist2)
    {
      point2 normal = (k >= 2) ? (lane[k] - lane[k - 2]) : (lane[k] - lane[k - 1]);
      normal = point2(normal.y, -normal.x); // rotate by 90deg
      normal.normalize();
      lane[k - 1] = lane[k - 1] + 0.25 * normal * normal.dot(*rndfIt - lane[k - 1]);
    }

    prevDist[1] = prevDist[0];
    prevDist[0] = dist2;

    // check if the next point in the rndf is closer, and if so advance
    if (rndfNext != rndflane.arr.end()) {
      point2 rndfvecNext = *rndfNext - lane[k];
      float distNext2 = rndfvecNext.norm2();
      if (distNext2 < dist2) {
        dist2 = distNext2;
        rndfvec = rndfvecNext;
        ++rndfIt, ++rndfNext;
      }
    }

    if ((isAssoc.empty() || !isAssoc[k]) &&  dist2 < DIST_THRES * DIST_THRES * 16)
    {
      point2 normal = lane[k + 1] - lane[k - 1];
      normal = point2(normal.y, -normal.x); // rotate by 90deg
      normal.normalize();
      //float p = 0.25 / (dist * dist + 0.1); // sum 0.1 to avoid div by zero
      float p = 0.5 * exp(- 0.5 * dist2 / (LANE_POINTS_SPACING*LANE_POINTS_SPACING));
      lane[k] = lane[k] + p / (p + 1.0) * normal * normal.dot(rndfvec);
      //lane[k] = lane[k] +  0.5 * normal * normal.dot(rndfvec);

      //const float THRES = LANE_POINTS_SPACING*4;
      //float pdist = normal.dot(rndfvec);
      //if (pdist > THRES)
      //{
      //  lane[k] = lane[k] - LAMBDA*2*KEL*((THRES*THRES)/(pdist*pdist)) * normal;
      //} else {
      //  lane[k] = lane[k] - LAMBDA*2*KEL*(pdist/THRES) * normal;
      //}
    }
    // minimize distance between points
    point2 prev = lane[k - 1];
    point2 next = lane[k + 1];
    point2 v1 = lane[k] - prev;
    point2 v2 = lane[k] - next;
    float distPrev = v1.norm();
    float distNext = v2.norm();
    if (distPrev < DIST_MIN)
      distPrev = DIST_MIN;
    if (distNext < DIST_MIN)
      distNext = DIST_MIN;
    point2 DEdistP = 2*KEL*(1.0 - DIST/distPrev)*v1;
    point2 DEdistN = 2*KEL*(1.0 - DIST/distNext)*v2;
    point2 DEdist = DEdistP + DEdistN;

    lane[k] = lane[k] - LAMBDA * DEdist - DEahead; 

/*
    // try to minimize curvature
    point2 DEcurvP =  point2(v1.y, -v1.x) * (v1.cross(v2) / (distPrev*distPrev*distPrev*distNext));
    point2 DEcurvN =  point2(v2.y, -v2.x) * (v2.cross(v1) / (distNext*distNext*distNext*distPrev));

    if (k > 1) // don't update the very first point (yet)
      lane[k - 1] = lane[k - 1] - LAMBDA*RATIO*DEcurvP;
    //if (k < int(lane.size()-2))
    //  lane[k + 1] = lane[k + 1] - LAMBDA*RATIO*DEcurvN;

    lane[k] = lane[k] - LAMBDA*RATIO*DEahead; // apply the values from the previous iteration too (DEahead)
    DEahead = DEcurvN;
*/
  }

//#warning "FIXME: do use the RNDF!!!!"
      
#else

  // simple FIR filter, but use original rndf data as input
  // for not associated points
  const float w[3] = {1, 1, 1};
  const float invsum = 1.0 / (w[0] + w[1] + w[2]);
  assert(lane.size() > 0);

  vector<point2_uncertain>::const_iterator it = rndflane.arr.begin();
  vector<point2_uncertain>::const_iterator next = it;
  ++next;

  point2 prev = lane[start];
  //point2 prev2 = prev;
  for (int k = start; k < end; k++)
  { 
      point2 old = lane[k];
      //point2 next2 = (k < int(lane.size()) - 2) ? lane[k + 2] : lane[k + 1];
      //lane[k] = (prev2 * w[0] + prev * w[1] + lane[k] * w[2]
      //           + lane[k + 1] * w[3] + next2 * w[4]) * invsum;
      lane[k] = (prev * w[0] + lane[k] * w[1] + lane[k + 1] * w[2]) * invsum;

      point2 rndfvec = *it - lane[k];
      float dist2 = rndfvec.norm2();

      // check if the next point in the rndf is closer, and if so advance
      if (next != rndflane.arr.end()) {
          point2 rndfvecNext = *next - lane[k];
          float distNext2 = rndfvecNext.norm2();
          if (distNext2 < dist2) {
              dist2 = distNext2;
              rndfvec = rndfvecNext;
              ++it, ++next;
          }
      }

      if ((isAssoc.empty() || !isAssoc[k]) &&  dist2 < DIST_THRES * DIST_THRES * 16)
      {
          point2 normal = lane[k + 1] - lane[k - 1];
          normal = point2(normal.y, -normal.x); // rotate by 90deg
          normal.normalize();
          //float p = 0.25 / (dist * dist + 0.1); // sum 0.1 to avoid div by zero
          float p = 0.5 * exp(- 0.5 * dist2 / (LANE_POINTS_SPACING*LANE_POINTS_SPACING));
          lane[k] = lane[k] + p / (p + 1.0) * normal * normal.dot(rndfvec);
      }

      //prev2 = prev;
      prev = old;
  }

  // special case for the first and last point
  if (isAssoc.empty() || !isAssoc[start])
  {
      float p = 0.1;
      lane[start] = (1 - p) * lane[start] + p * rndflane[0];
  }
  if (isAssoc.empty() || !isAssoc[end])
  {
      float p = 0.1;
      lane[end] = (1 - p) * lane[end] + p * rndflane.arr.back();
  }

#endif

}

void MapPrior::postProcessLane(int segIdx, int laneIdx, const vector<bool>& isUpd)
{
  // just process the lane, not the whole segment at the moment

  RNDFsegment& segm = segments[segIdx];
  RNDFlane& lane = lanes[segm.laneindex[laneIdx]];

  float width = 3;
  if (lane.width > width) // lane.width could even be zero (not specified)
    width = lane.width;

  //MapElement left, right, center;
  //getElFull(center, lane.waypoint_elindex);
  //getElFull(left,   lane.leftbound_elindex);
  //getElFull(right,  lane.rightbound_elindex);

  // nothing here depends on the exact frame (local or global)
  // so avoid unnecessary copying
  MapElement& center = fulldata[lane.waypoint_elindex];
  MapElement& left = fulldata[lane.leftbound_elindex];
  MapElement& right = fulldata[lane.rightbound_elindex];

  MapElement& rndfCenter = data[lane.waypoint_elindex];
  //MapElement rndfCenter;
  //getEl(rndfCenter, lane.waypoint_elindex);

  point2arr_uncertain& geomC = center.geometry;
  point2arr_uncertain& geomL = left.geometry;
  point2arr_uncertain& geomR = right.geometry;

  assert(geomC.size() == geomL.size());
  assert(geomC.size() == geomR.size());
  assert(geomC.size() >= 2);

  // need to convert everything in local frame because of otherwise
  // bad numerical conditioning
  // still faster than calling getElFull because everything is done
  // in place, no need to allocate new memory
  for (unsigned int i = 0; i < geomC.size(); i++)
  {
    geomC[i] = geomC[i] - delta;
    geomL[i] = geomL[i] - delta;
    geomR[i] = geomR[i] - delta;
  }
    
  point2arr_uncertain& rndf = rndfCenter.geometry; 
  for (unsigned int i = 0; i < rndf.size(); i++)
    rndf[i] = rndf[i] - delta;
  
  for (unsigned int i = 0; i < geomC.size(); i++)
  {
    point2 c = 0.5 * (geomL[i] + geomR[i]);
    geomL[i] = geomL[i] - geomC[i];
    geomR[i] = geomR[i] - geomC[i];
    geomC[i] = c;
  }

  smoothLine(geomC, rndfCenter.geometry, lane.geomStart, lane.geomEnd, isUpd);
    
  for (unsigned int i = 0; i < geomC.size(); i++)
  {
    point2 vec = geomR[i] - geomL[i];
    point2 prev = geomC[i == 0 ? i : (i - 1)];
    point2 next = geomC[i == (geomC.size()-1) ? i : (i + 1)];
    point2 n = next - prev;
    //if (which & 1) // 1 == right
    //  n = point2(n.y, -n.x); // rotate by 90°, get the normal pointing to the left
    //else
      n = point2(-n.y, n.x); // rotate by 90°, get the normal pointing to the right
    double nl = n.norm();
    double w = vec.norm();
    if (w < width) // enforce minimum width
      w = width;
    // enforce points to stay on the normal
    geomL[i] = geomC[i] - n * 0.5 * width / nl;
    geomR[i] = geomC[i] + n * 0.5 * width / nl;
  }
  
  // now, back to global frame
  for (unsigned int i = 0; i < geomC.size(); i++)
  {
    geomC[i] = geomC[i] + delta;
    geomL[i] = geomL[i] + delta;
    geomR[i] = geomR[i] + delta;
  }
  
  for (unsigned int i = 0; i < rndf.size(); i++)
    rndf[i] = rndf[i] + delta;

  center.updateFromGeometry();
  left.updateFromGeometry();
  right.updateFromGeometry();
  //setElFull(center, lane.waypoint_elindex);
  //setElFull(left,   lane.leftbound_elindex);
  //setElFull(right,  lane.rightbound_elindex);
}


// FIXME: this should be added to point2_uncertain.hh
bool operator==(const point2_uncertain& p1, const point2_uncertain& p2)
{
  return p1.x == p2.x && p1.y == p2.y;
  //return p1.x == p2.x && p1.y == p2.y && p1.z == p2.z // I don't need z in thic context
}
bool operator!=(const point2_uncertain& p1, const point2_uncertain& p2)
{
  return !(p1 == p2);
}

template<typename Container1, typename Container2, typename IntIter>
void rearrangeContainer(const Container1& src, Container2& dst, IntIter begin, IntIter end)
{
  // typedef typename Container1::value_type T; // not needed at the moment

  dst.resize(end - begin);

  //for (int i = 0; i < int(indexes.size()); i++)
  //  dst[i] = src[indexes[i]];
  int i = 0;
  for(IntIter it = begin; it != end; ++it, ++i)
    dst[i] = src[*it];
}

template<typename Container1, typename Container2>
void rearrangeContainer(const Container1& src, Container2& dst, const vector<int>& indexes)
{
    rearrangeContainer(src, dst, indexes.begin(), indexes.end());
}

// make the two given lines have the same number of points
// adding duplicate points if necessary.
// The output is produced as an array of the size of the new line,
// specifying the indexes of the points in the original vector.
static void equalizeLines(const point2arr_uncertain& line1,
                          const point2arr_uncertain& line2,
                          vector<int>& out1, vector<int>& out2,
                          int start1, int end1,
                          int start2, int end2,
                          LineFusionDebugData* dbg = NULL)
{

  out1.clear();
  out1.reserve(line1.size() + line2.size());
  out2.clear();
  out2.reserve(line1.size() + line2.size());

  int k1 = start1, k2 = start2;
  while(k1 <= end1 - 1 && k2 <= end2 - 1) // iterate up to one before the end
  {
    DBG_EQUAL_ADD(dbg, line1[k1], line2[k2]);
    out1.push_back(k1);
    out2.push_back(k2);

    //point2 distVec = line2[k2] - line1[k1];
    //float dist2 = distVec.norm2();
    // distance^2 from point in line1 to point in line2
    float d2Next1 = point2(line2[k2] - line1[k1 + 1]).norm2();
    // distance^2 using the next point in line1 (resp line2)
    float d2Next2 = point2(line2[k2 + 1] - line1[k1]).norm2();
    // distance^2 using the next point in both lines
    float d2Next12 = point2(line2[k2 + 1] - line1[k1 + 1]).norm2();

    // find minimum dist incrementing at least one index
    float minD2 = d2Next12;
    int minK1 = k1 + 1;
    int minK2 = k2 + 1;

    if (d2Next1 < minD2) {
      minD2 = d2Next1;
      minK1 = k1 + 1;
      minK2 = k2;
    }
    if (d2Next2 < minD2) {
      minD2 = d2Next2;
      minK1 = k1;
      minK2 = k2 + 1;
    }
    
    k1 = minK1;
    k2 = minK2;
    
  }

  // complete the job adding remaining points at the end
  if (k2 == end2) {
    for (; k1 <= end1; k1++)
    {
      DBG_EQUAL_ADD(dbg, line1[k1], line2[k2]);
      out1.push_back(k1);
      out2.push_back(k2);
    }

  } else if (k1 == end1) {
    for (; k2 <= end2; k2++)
    {
      DBG_EQUAL_ADD(dbg, line1[k1], line2[k2]);
      out1.push_back(k1);
      out2.push_back(k2);
    }
  }
}

class LineData
{
    vector<vector<point2_uncertain>*> m_lines;
    vector<vector<int> > m_indexes;
    vector<pair<int, int> > m_ranges; // start and end "labels"

public:
    LineFusionDebugData* lineFusDebug;

public:
    LineData() : lineFusDebug(NULL) { }
    LineData(int nLines)
        : m_lines(nLines), m_indexes(nLines), m_ranges(nLines), lineFusDebug(NULL) { }

    int numLines() { return m_lines.size(); }
    void setNumLines(int nLines) {
        m_lines.resize(nLines);
        m_indexes.resize(nLines);
        m_ranges.resize(nLines);
    }

    void setLine(int idx, vector<point2_uncertain>* line, int start = 0, int end = -1)
    {
        assert(idx >= 0 && idx < int(m_lines.size()));
        m_lines[idx] = line;
        m_indexes[idx].clear(); // empty indexes imply identity transform
        m_ranges[idx].first = start;
        m_ranges[idx].second = end < 0 ? line->size() - 1 : end;
    }

    void setLine(int idx, point2arr_uncertain* line, int start = 0, int end = -1)
    {
        setLine(idx, &line->arr, start, end);
    }

    // some read-only accessors for internal members

    const vector<pair<int, int> >& getRanges() const { return m_ranges; };
    const vector<vector<int> >& getIndexes() const { return m_indexes; };
    const vector<vector<point2_uncertain>*>& getLines() const { return m_lines; };

    void getRange(int idx, int* start, int* end)
    {
        assert(idx >= 0 && idx < int(m_lines.size()));
        if (start != NULL)
            *start = m_ranges[idx].first;
        if (end != NULL)
            *end = m_ranges[idx].second;
    }
    
    // make all the lines have the same number of point, in such a way that
    // the point in a different lane with the same index is the closest one in
    // that lane. Makes easy to find corresponding points and manipulate
    // whole segments as opposed to single lanes.
    void equalize();
};

void LineData::equalize()
{
  int nLines = numLines();
  int j;
  for (j = 0; j < nLines - 1; j++)
  {
    // make lines j and j + 1 have the same number of points
    unsigned int N1 = m_lines[j]->size();
    unsigned int N2 = m_lines[j + 1]->size();

    equalizeLines(*m_lines[j], *m_lines[j + 1], m_indexes[j], m_indexes[j + 1],
                  m_ranges[j].first, m_ranges[j].second,
                  m_ranges[j + 1].first, m_ranges[j + 1].second,
                  lineFusDebug);
    assert(m_indexes[j].size() == m_indexes[j + 1].size());
    assert(m_indexes[j].size() >= N1);
    assert(m_indexes[j + 1].size() >= N2);

    // only apply the transformation to line j + 1, line j will be handled later
    vector<point2_uncertain> newLine2;
    rearrangeContainer(*m_lines[j + 1], newLine2, m_indexes[j + 1]);
    *m_lines[j + 1] = newLine2;
    m_ranges[j + 1].second = newLine2.size() - 1; // FIXME ...
  }

  // update what we've left behind
  for (j = nLines - 2; j >= 0; j--)
  {
    vector<int> tmp;
    if (j > 0) {
      rearrangeContainer(m_indexes[j - 1], tmp, m_indexes[j]);
      m_indexes[j - 1] = tmp;
    }
    vector<point2_uncertain> newLine;
    rearrangeContainer(*m_lines[j], newLine, m_indexes[j]);
    *m_lines[j] = newLine;
  }

  // find out start and end of geometry for each line, looking at the molteplicity
  // of the first and last point
  if (nLines == 1)
  {
    // only one line, no change was needed, start is 0, end is size()-1
    m_ranges[0].first = 0;
    m_ranges[0].second = m_lines[0]->size() - 1;
  }
  else
  {
    for (j = 0; j < nLines; j++)
    {
      int k;
      int first = m_indexes[j][0];
      assert(first == 0); // it should really be always zero!!!
      // find the first index different from the first
      for (k = 1; k < int(m_indexes[j].size()); k++)
        if (m_indexes[j][k] != first)
          break;
      m_ranges[j].first = k - 1;
      int last = m_indexes[j].back();
      // find the first index from the end different from the last
      for (k = m_indexes[j].size() - 2; k >= 0; k--)
        if (m_indexes[j][k] != last)
          break;
      m_ranges[j].second = k + 1;
    }
  }

}


bool MapPrior::parseLaneInterpolation()
{
  
  
  //cout << "Parsing data interpolation " << endl;
  unsigned int i,j;
  MapElement tmpEl;
  point2arr tmpcenter, tmpleft, tmpright, tmpptarr;
  point2arr_uncertain tmpptarr_unc;
  vector<bool> isInterp; // true if the point wasn't found in the rndf
  vector<PointLabel> tmplabelarr;
  int centerindex,leftindex,rightindex;
  vector<double> dheadingarr;
  vector<double> headingarr;
  double thresh = .4;//angle thresh to trigger corner interpolation
  double offset = 0;
  unsigned int idx;

  point2arr tmpinterp;
  for (i=0;i<lanes.size();++i){
    //   cout << "lane " << lanes[i].label << endl;
    centerindex= lanes[i].waypoint_elindex;
    leftindex= lanes[i].leftbound_elindex;
    rightindex= lanes[i].rightbound_elindex;
    tmpEl = data[centerindex];
    tmpcenter.set(tmpEl.geometry);

    if (internalLineFusion){
      isInterp.clear();
      isInterp.reserve(tmpcenter.size());
    }

    offset = lanes[i].width/2;
    if (offset<.5){
      offset = 6;
    }


    //tmpcenter.set_extend(20);
    //tmpcenter.set_extend(-20);

    dheadingarr = tmpcenter.get_relative_heading();
    headingarr = tmpcenter.get_heading();
  
    // sets the index to the center_data array
    //    lanes[i].datindex.resize(tmpcenter.size()-1);

    tmpptarr.clear();    
    for (j=0; j<tmpcenter.size()-1; ++j){

  
      tmpptarr.push_back(tmpcenter[j]);
      
      
        
        
      //--------------------------------------------------
      // detect curves which can use interpolation
      //--------------------------------------------------
      if (internalLineFusion) {
        idx = tmpptarr.size() - 1;
        isInterp.push_back(false);      
        // if the segment is too long, make is easier to be fused with sensed
        // data by splitting into an adequate number of parts
        point2 vec = tmpcenter[j+1] - tmpcenter[j];
        float length = vec.norm();
        if (length > LANE_POINTS_SPACING) {
          //    cerr << "splitting line " << lanes[i].label << endl;
          int nparts = int(ceil(length / LANE_POINTS_SPACING));
          for (int k = 1; k < nparts; k++) {
            tmpptarr.push_back(tmpcenter[j] + vec * k / nparts);
          }
        }
      } else {
        // don't do this when doing line fusion, because it smooths the lanes anyway,
        // and this only confuses it (kind of)
        if (fabs(dheadingarr[j+1])>thresh &&
            fabs(dheadingarr[j])>thresh){
          tmpinterp.set_arc_curve(tmpcenter[j],headingarr[j-1],tmpcenter[j+1],headingarr[j+1],2);
          //cout <<"interp = " << tmpinterp << endl;
          tmpptarr.connect(tmpinterp);
        }
      }

      if (internalLineFusion){
        
        for (; idx < tmpptarr.size() - 1; idx++) {
          isInterp.push_back(true);
        }
        
        
        if (j==tmpcenter.size()-2){ // sets last point extension
          tmpptarr.push_back(tmpcenter[j+1]);
          isInterp.push_back(false);
          
        }
        
      } else{ // no internal line fusion
        
        if (j==tmpcenter.size()-2){ // sets last point extension
          tmpptarr.push_back(tmpcenter[j+1]);
        }      
      }

    }   

    if (false)  { //if (internalLineFusion) { // this code is not needed at all
  
      // HACK: since uncertainty isn't used anywhere, use it to mark a waypoint
      // as interpolated (>0) or given (found in the rndf, =0)
      // FIXME: this is not used anywhere (was meant to be useful for line fusion)
      tmpptarr_unc.resize(tmpptarr.size());
      for (unsigned int i = 0; i < tmpptarr.size(); i++) {
        tmpptarr_unc[i] = tmpptarr[i];
        tmpptarr_unc[i].max_var = 1 * isInterp[i];
        tmpptarr_unc[i].min_var = 1 * isInterp[i];
      }
      
      fulldata[centerindex].setGeometry(tmpptarr_unc);
      tmpleft = tmpptarr.get_offset(-offset);
      tmpright = tmpptarr.get_offset(offset);    

      tmpptarr_unc.resize(tmpleft.size());
      for (unsigned int i = 0; i < tmpleft.size(); i++) {
        tmpptarr_unc[i] = tmpleft[i];
        tmpptarr_unc[i].max_var = 1 * isInterp[i];
        tmpptarr_unc[i].min_var = 1 * isInterp[i];
      }
      fulldata[leftindex].setGeometry(tmpptarr_unc);
    
      tmpptarr_unc.resize(tmpright.size());
      for (unsigned int i = 0; i < tmpright.size(); i++) {
        tmpptarr_unc[i] = tmpright[i];
        tmpptarr_unc[i].max_var = 1 * isInterp[i];
        tmpptarr_unc[i].min_var = 1 * isInterp[i];
      }
      fulldata[rightindex].setGeometry(tmpptarr_unc);

  
    } else{ // no internal line fusion

      if (j==tmpcenter.size()-2){ // sets last point extension
        tmpptarr.push_back(tmpcenter[j+1]);
      }      
      
      
      fulldata[centerindex].setGeometry(tmpptarr);
      tmpleft = tmpptarr.get_offset(-offset);
      tmpright = tmpptarr.get_offset(offset);
      fulldata[rightindex].setGeometry(tmpright);
      fulldata[leftindex].setGeometry(tmpleft);
      
    }  
    
  }


  if (internalLineFusion) {
    DBG_EQUAL_RESET(lineFusDebug);
    // Add points to lanes such as to have the same number of points for each lane
    // in the same segment. Everything is easier this way.
    for (unsigned int i = 0; i < segments.size(); i++)
    {
      int j; // important: must be 'int' not 'unsigned int'
      RNDFsegment& seg = segments[i];
      for (j = 0; j < int(seg.laneindex.size()); j++)
      {
        RNDFlane& laneObj = lanes[seg.laneindex[j]];
        point2arr_uncertain& lane = fulldata[laneObj.waypoint_elindex].geometry;
        laneObj.geomStart = 0;
        laneObj.geomEnd = lane.size() - 1;

        // remove 'delta' (in place, no memory allocation/copy) to achieve
        // better numerical conditioning
        for (unsigned int k = 0; k < lane.size(); k++)
          lane[k] = lane[k] - delta;
        // invert lane direction if needed to aid in matching points
        if (laneObj.direction == -1) {
          int size = lane.size();
          for (int k = 0; k < size/2; k++)
            swap(lane[k], lane[size - k - 1]);
        }
      }

      int numLanes = seg.laneindex.size();
      LineData data(numLanes);
#ifndef NDEBUG
      data.lineFusDebug = lineFusDebug;
#endif
      for (j = 0; j < numLanes; j++) {
        RNDFlane& laneObj = lanes[seg.laneindex[j]];
        point2arr_uncertain& lane = fulldata[laneObj.waypoint_elindex].geometry;
        data.setLine(j, &lane);
      }

      data.equalize();

      for (j = 0; j < numLanes; j++) {
        RNDFlane& laneObj = lanes[seg.laneindex[j]];
        data.getRange(j, &laneObj.geomStart, &laneObj.geomEnd);
      }

/*
      vector<vector<int> > indexes(numLanes);
      for (j = 0; j < numLanes - 1; j++)
      {
        // make lanes j and j + 1 have the same number of waypoints
        // by adding points where needed
        RNDFlane& laneObj1 = lanes[seg.laneindex[j]];
        RNDFlane& laneObj2 = lanes[seg.laneindex[j + 1]];
        point2arr_uncertain& lane1 = fulldata[laneObj1.waypoint_elindex].geometry;
        point2arr_uncertain& lane2 = fulldata[laneObj2.waypoint_elindex].geometry;
        unsigned int N1 = lane1.size();
        unsigned int N2 = lane2.size();

        //equalizeLines(lane1, lane2,
        //              laneObj1.geomStart, laneObj1.geomEnd,
        //              laneObj2.geomStart, laneObj2.geomEnd, lineFusDebug);
        equalizeLines(lane1, lane2, indexes[j], indexes[j + 1],
                      0, lane1.size()-1, 0, lane2.size()-1,
                      lineFusDebug);
        // only apply the transformation to lane2, lane1 will be handled later
        point2arr_uncertain newLane2;
        rearrangeContainer(lane2, newLane2, indexes[j + 1]);
        assert(indexes[j].size() == indexes[j + 1].size());
        assert(indexes[j].size() >= N1);
        assert(indexes[j + 1].size() >= N2);
        lane2 = newLane2;
      }

      // update what we've left behind
      for (j = numLanes - 2; j >= 0; j--)
      {
        vector<int> tmp;
        if (j > 0) {
          rearrangeContainer(indexes[j - 1], tmp, indexes[j]);
          indexes[j - 1] = tmp;
        }
        RNDFlane& laneObj = lanes[seg.laneindex[j]];
        point2arr_uncertain& lane = fulldata[laneObj.waypoint_elindex].geometry;
        
        point2arr_uncertain newLane;
        rearrangeContainer(lane, newLane, indexes[j]);
        lane = newLane;
      }

      // find out start and end of geometry for each lane, looking at the molteplicity
      // of the first and last point
      if (numLanes == 1)
      {
        // only one lane, no change was needed, start is 0, end is size()-1
        RNDFlane& laneObj = lanes[seg.laneindex[0]];
        point2arr_uncertain& lane = fulldata[laneObj.waypoint_elindex].geometry;
        laneObj.geomStart = 0;
        laneObj.geomEnd = lane.size() - 1;
      }
      else
      {
        for (j = 0; j < numLanes; j++)
        {
          RNDFlane& laneObj = lanes[seg.laneindex[j]];
          int k;
          int first = indexes[j][0];
          assert(first == 0); // it should really be always zero!!!
          // find the first index different from the first
          for (k = 1; k < int(indexes[j].size()); k++)
            if (indexes[j][k] != first)
              break;
          laneObj.geomStart = k - 1;
          int last = indexes[j].back();
          // find the first index from the end different from the last
          for (k = indexes[j].size() - 2; k >= 0; k--)
            if (indexes[j][k] != last)
              break;
          laneObj.geomEnd = k + 1;
        }
      }
*/
/*
      // do it again, in reverse order, updating lanes whose neighbors have changed
      for (j = numLanes - 3; j >= 0; j--)
      {
        RNDFlane& laneObj1 = lanes[seg.laneindex[j]];
        RNDFlane& laneObj2 = lanes[seg.laneindex[j + 1]];
        point2arr_uncertain& lane1 = fulldata[laneObj1.waypoint_elindex].geometry;
        point2arr_uncertain& lane2 = fulldata[laneObj2.waypoint_elindex].geometry;
        unsigned int N1 = lane1.size();
        unsigned int N2 = lane2.size();

        if (N1 != N2) 
        {
          assert(N2 > N1); // they were equal, only lane2 has changed since

          // only lane1 should change now ...
          equalizeLines(lane1, lane2,
                        laneObj1.geomStart, laneObj1.geomEnd,
                        laneObj2.geomStart, laneObj2.geomEnd);
          assert(lane1.size() == lane2.size());
          assert(lane1.size() >= N1);
          assert(lane2.size() == N2); // lane2 shouldn't have changed at all ...
        }
      }
*/

#ifndef NDEBUG
      // make sure all this hard work actually yielded the correct result
      for (j = 0; j < numLanes - 1; j++) {
        RNDFlane& laneObj1 = lanes[seg.laneindex[j]];
        RNDFlane& laneObj2 = lanes[seg.laneindex[j + 1]];
        point2arr_uncertain& lane1 = fulldata[laneObj1.waypoint_elindex].geometry;
        point2arr_uncertain& lane2 = fulldata[laneObj2.waypoint_elindex].geometry;
        assert(lane1.size() == lane2.size());
      }
#endif

      // restore original coordinates (global frame)
      for (j = 0; j < int(seg.laneindex.size()); j++)
      {
        RNDFlane& laneObj = lanes[seg.laneindex[j]];
        point2arr_uncertain& lane = fulldata[laneObj.waypoint_elindex].geometry;
        for (unsigned int k = 0; k < lane.size(); k++)
          lane[k] = lane[k] + delta;

        // restore lane direction if was reversed
        if (laneObj.direction == -1) {
          int size = lane.size();
          for (int k = 0; k < size/2; k++)
            swap(lane[k], lane[size - k - 1]);
          laneObj.geomStart = size - laneObj.geomEnd - 1;
          laneObj.geomEnd = size - laneObj.geomStart - 1;
        }
        // generate left and right bounds
        point2arr_uncertain& left = fulldata[laneObj.leftbound_elindex].geometry;
        point2arr_uncertain& right = fulldata[laneObj.rightbound_elindex].geometry;
        // actually, just copy the center and let postProcessLane enforce the lane width ;-)
        left = lane;
        right = lane;
      }
/*
      for (int count = 0; count < 5; count++) {
        // do one step of smoothing and constraint enforcement
        for (j = 0; j < int(seg.laneindex.size()) ; j++)
          postProcessLane(i, j);
      }
*/
    }
  }

  return true;
}


bool MapPrior::parseLaneDirection()
{

  //cout << "Parsing lane direction " << endl;
  int seglabel;
  //  int numlanes;
  int val;
  int i,j,k;
  int thisdirection;
  int tmpindex;
  int numlanes;
  LaneLabel tmplabel,thislabel;
  for (i=0;i<(int)segments.size();++i){
    //    segment[i].lanedirection.assign(numlanes);
    seglabel = segments[i].label;
    numlanes = getSegmentNumLanes(seglabel);
    if (numlanes<1){
      cerr << "in MapPrior::parseRNDF, segment " << seglabel <<" is poorly formed or has no lanes  " << endl;
      continue;
    }
    thislabel = segments[i].lane_label[0];
    for (j=-numlanes;j<=numlanes;++j){
      val = getNeighborLane(tmplabel,thislabel,j);
      if (val<=0)
        continue;
      else if (val ==1){
        thisdirection = 1;
      }
      else if (val==2){
        thisdirection =-1;
      }
      tmpindex = getLaneIndex(tmplabel);
      lanes[tmpindex].direction = thisdirection;

      for (k=0;k<numlanes;k++){
        if (segments[i].lane_label[k]==tmplabel){
          segments[i].lanedirection[k] = thisdirection;
        }
      }
    }
  }
  if (0){
    for (i=0;i<(int)segments.size();++i){
      cout << "Segment " << segments[i].label << endl;
      for (j=0;j<(int)segments[i].lane_label.size();++j){
        cout << "this lane = " << segments[i].lane_label[j] << "  direction " << segments[i].lanedirection[j] << endl;
      }
    }
  }
  return true;
}

bool MapPrior::parseLaneNeighbors()
{

  int i, j;
  point2arr thisarr,prevarr,nextarr;
  point2arr_uncertain thisarr_unc,prevarr_unc, nextarr_unc;
  LaneLabel thislabel, prevlabel, nextlabel;
  LaneLabel zerolabel(0,0);
  //int thisindex;
  int previndex,nextindex,numlanes;
  int retval;
  int prevside, nextside;
  int thisindex;
  //cout << "Parsing lane neighbors " << endl;
  for (i=0;i<(int)segments.size();++i){
    
    thislabel.segment = segments[i].label;
    numlanes = (int)segments[i].lane_label.size();
    for (j=0;j<numlanes;++j){

      thislabel = segments[i].lane_label[j];
      thisindex = segments[i].laneindex[j];
      retval = getLaneCenterLine(thisarr_unc,thislabel);
      thisarr = thisarr_unc;
      prevlabel = thislabel;
      prevlabel.lane = thislabel.lane-1;

      nextlabel = thislabel;
      nextlabel.lane = thislabel.lane+1;


      // cout << "prevlabel = " << prevlabel 
      //     <<" nextlabel = " << nextlabel << endl;

      previndex = getLaneIndex(prevlabel);
      //cout << "previndex = " << previndex<< endl;
      prevside =0;
      if (previndex>=0){
        getLaneCenterLine(prevarr_unc,prevlabel);
        prevarr = prevarr_unc;
        thisarr.get_side(prevarr,prevside);
      }

      nextindex = getLaneIndex(nextlabel);
      //      cout << "nextindex = " << nextindex<< endl;
      nextside = 0;
      if (nextindex>=0){
        getLaneCenterLine(nextarr_unc,nextlabel);
        nextarr = nextarr_unc;
        thisarr.get_side(nextarr,nextside);
      }
      //  cout << "prevside = " << prevside 
      //     <<" nextside = " << nextside << endl;
      if (prevside==0 && nextside==0){
        lanes[thisindex].leftneighbor_label = zerolabel;
        lanes[thisindex].rightneighbor_label = zerolabel;
      }else{
        if (prevside==nextside){
          cerr << "in MapPrior::parseRNDF, got two lanes " << prevlabel <<" , " << nextlabel << " to be on the same side of " << thislabel << endl;
          
        }
        if (prevside==-1){
          lanes[thisindex].leftneighbor_label = prevlabel;
        }
        else if (prevside==1){
          lanes[thisindex].rightneighbor_label = prevlabel;
        }else if (prevside==0){
          if (nextside==1){
            lanes[thisindex].leftneighbor_label = zerolabel;
          }else if (nextside==-1){
            lanes[thisindex].rightneighbor_label = zerolabel;
          }
        }
        if (nextside==-1){
          lanes[thisindex].leftneighbor_label = nextlabel;
        }
        else if (nextside==1){
          lanes[thisindex].rightneighbor_label = nextlabel;
        }else if (nextside==0){
          if (prevside==1){
            lanes[thisindex].leftneighbor_label = zerolabel;
          }else if (prevside==-1){
            lanes[thisindex].rightneighbor_label = zerolabel;
          }
        }


      }
    }
  }
  if (0){  
    for (i=0;i<(int)lanes.size();++i){
      cout << "Lane " << lanes[i].label << endl
           << " left neigbor "<< lanes[i].leftneighbor_label << endl
           << " right neigbor "<< lanes[i].rightneighbor_label  << " direction " << lanes[i].direction << endl;;
      
    }
  }
  return true;
}



/// Parses the exit/entry points given by RNDF and puts the data into the MapPrior structure

bool MapPrior::parseExitPoints()
{
  int i,j;
  int exitsize= 0;
  PointLabel thisptlabel, thislinklabel;
  int entrylaneindex, entrypointindex,entryzoneindex,entryperimeterindex;;
  
  // parsing lane exit points
  for (i=0;i<(int)lanes.size();++i){
    //   cout << "lane " << lanes[i].label << endl;
    exitsize = lanes[i].exit_label.size();
    if (exitsize!=(int)lanes[i].exit_link.size()){
      cerr << "in MapPrior::parseRNDF size mismatch in parsed exit labels for lane index " << i<< endl;
      return false;
    }
    for (j=0;j<exitsize;++j){
      thisptlabel = lanes[i].exit_label[j];
      thislinklabel =  lanes[i].exit_link[j];
      //cout << "exit " << thisptlabel << " link to " << thislinklabel << endl;      

      entrylaneindex = getLaneIndex(thislinklabel);
      entrypointindex = getLanePointIndex(thislinklabel);
      
      if (entrylaneindex>=0 && entrypointindex >=0){
        lanes[entrylaneindex].entry_label.push_back(thislinklabel);
        lanes[entrylaneindex].entry_link.push_back(thisptlabel);
        continue;
      }

      entryzoneindex = getZoneIndex(thislinklabel);
      entryperimeterindex = getZoneIndex(thislinklabel);
      if (entryzoneindex>=0 && entryperimeterindex>=0){
        zones[entryzoneindex].entry_label.push_back(thislinklabel);
        zones[entryzoneindex].entry_link.push_back(thisptlabel);
        continue;
      }
      cerr << "in MapPrior::parseRNDF, bad link specified from "<< thisptlabel << " to " << thislinklabel << ".   Target point or lane not found" << endl;

    }
  }

  // parsing zone exit points
  for (i=0;i<(int)zones.size();++i){
    //   cout << "lane " << lanes[i].label << endl;
    exitsize = zones[i].exit_label.size();
    if (exitsize!=(int)zones[i].exit_link.size()){
      cerr << "in MapPrior::parseRNDF size mismatch in parsed exit labels for zone index " << i<< endl;
      return false;
    }
    for (j=0;j<exitsize;++j){
      thisptlabel = zones[i].exit_label[j];
      thislinklabel = zones[i].exit_link[j];
      //cout << "exit " << thisptlabel << " link to " << thislinklabel << endl;      

      entrylaneindex = getLaneIndex(thislinklabel);
      entrypointindex = getLanePointIndex(thislinklabel);
      
      if (entrylaneindex>=0 && entrypointindex >=0){
        lanes[entrylaneindex].entry_label.push_back(thislinklabel);
        lanes[entrylaneindex].entry_link.push_back(thisptlabel);
        continue;
      }

      entryzoneindex = getZoneIndex(thislinklabel);
      entryperimeterindex = getZoneIndex(thislinklabel);
      if (entryzoneindex>=0 && entryperimeterindex>=0){
        zones[entryzoneindex].entry_label.push_back(thislinklabel);
        zones[entryzoneindex].entry_link.push_back(thisptlabel);
        continue;
      }
      cerr << "in MapPrior::parseRNDF, bad link specified from "<< thisptlabel << " to " << thislinklabel << ".   Target point or lane not found" << endl;

    }
  }



  return true;
}

/// Parses the transitions
bool MapPrior::parseTransitions()
{
  int i,j,k;
  int exitsize= 0;
  PointLabel thisptlabel, thislinklabel;
  //  int entrylaneindex, entrypointindex,entryzoneindex,entryperimeterindex;;
  point2 exitpt_left,preexitpt_left;
  point2 enterpt_left,postenterpt_left;
  point2 exitpt_right,preexitpt_right;
  point2 enterpt_right,postenterpt_right;
  line2 exitline_left,exitline_right;
  line2 enterline_right,enterline_left;

  MapElement el;
  MapElement exitLeftEl,exitRightEl,enterLeftEl,enterRightEl;
  //point2arr_uncertain exitLeftBound, exitRightBound, enterLeftBound, enterRightBound;
  LaneLabel thislinklanelabel;
  int linkindex;
  int exitindex;
  int enterindex;
  // bool boundsflag = false;
  // bool leftinnerflag ;
  int numpts;
  point2 interpt;
  point2arr interptarr;
  point2arr tmpbound;
  //cout << "Parsing transitions " << endl;
  for (i=0;i<(int)lanes.size();++i){
    // cout << "lane " << lanes[i].label << endl;
    exitsize = lanes[i].exit_label.size();
    if (exitsize!=(int)lanes[i].exit_link.size()){
      cerr << "in MapPrior::parseRNDF size mismatch in parsed exit labels for lane index " << i<< endl;
      return false;
    }
    exitLeftEl = data[lanes[i].leftbound_elindex];
    exitRightEl = data[lanes[i].rightbound_elindex];
    lanes[i].exit_leftbound_elindex.resize(lanes[i].exit_label.size());
    lanes[i].exit_rightbound_elindex.resize(lanes[i].exit_label.size());
    lanes[i].exit_waypoint_elindex.resize(lanes[i].exit_label.size());

    for (j=0;j<exitsize;++j){
      thisptlabel = lanes[i].exit_label[j];
      thislinklabel =  lanes[i].exit_link[j];
      //cout <<"from " << thisptlabel << " to " << thislinklabel << endl;
      linkindex = getLaneIndex(thislinklabel);

      enterLeftEl = data[lanes[linkindex].leftbound_elindex];
      enterRightEl = data[lanes[linkindex].rightbound_elindex];
      
      exitindex = getLanePointIndex(thisptlabel);
      enterindex = getLanePointIndex(thislinklabel);

      //cout << "exit index = " << exitindex << " enterindex = " << enterindex << endl;

      if ((exitindex<1||exitindex>=(int)exitLeftEl.geometry.size())||
          (enterindex<0||enterindex>=(int)enterLeftEl.geometry.size()-1)){
        cerr << "in MapPrior::parseTransitions, got bad index for lane bounds"<< endl;
        return false;
      }

      exitpt_left = exitLeftEl.geometry[exitindex];
      enterpt_left = enterLeftEl.geometry[enterindex];
      preexitpt_left = exitLeftEl.geometry[exitindex-1];
      postenterpt_left = enterLeftEl.geometry[enterindex+1];


      exitpt_right = exitRightEl.geometry[exitindex];
      enterpt_right = enterRightEl.geometry[enterindex];
      preexitpt_right = exitRightEl.geometry[exitindex-1];
      postenterpt_right = enterRightEl.geometry[enterindex+1];

      exitline_left.set(preexitpt_left,exitpt_left);
      enterline_left.set(enterpt_left,postenterpt_left);
      exitline_right.set(preexitpt_right,exitpt_right);
      enterline_right.set(enterpt_right,postenterpt_right);
      //--------------------------------------------------
      // left bound
      //--------------------------------------------------
      numpts = 3;
      tmpbound.clear();
      tmpbound.push_back(exitline_left.ptA);
      if (exitline_left.is_cross(enterline_left) ||
          enterline_left.is_cross(exitline_left)){
        interpt = exitline_left.intersect(enterline_left);
        for (k=0;k<numpts;++k){
          tmpbound.push_back(interpt);
        }
      }else{
        interpt = exitline_left.intersect(enterline_left);
        interptarr.set_arc_curve(exitline_left.ptB,interpt,enterline_left.ptA,numpts);
        tmpbound.connect(interptarr);
      }
      tmpbound.push_back(enterline_left.ptB);

      el.clear();
      el.setId(0,thisptlabel.segment,thisptlabel.lane,thisptlabel.point,-2);
      el.setTypeLaneLine();
      el.setFrameTypeGlobal();
      el.setGeometry(tmpbound);
      lanes[i].exit_leftbound_elindex[j] = transdata.size();

      transdata.push_back(el);
      //--------------------------------------------------
      // outer bound
      //--------------------------------------------------
      
      tmpbound.clear();
      tmpbound.push_back(exitline_right.ptA);
      if (exitline_right.is_cross(enterline_right) ||
          enterline_right.is_cross(exitline_right)){
        interpt = exitline_right.intersect(enterline_right);
        for (k=0;k<numpts;++k){
          tmpbound.push_back(interpt);
        }
      }else{
        interpt = exitline_right.intersect(enterline_right);
        interptarr.set_arc_curve(exitline_right.ptB,interpt,enterline_right.ptA,numpts);
        tmpbound.connect(interptarr);
      }
      tmpbound.push_back(enterline_right.ptB);
      el.clear();
      el.setId(0,thisptlabel.segment,thisptlabel.lane,thisptlabel.point,2);
      el.setTypeLaneLine();
      el.setFrameTypeGlobal();
      el.setGeometry(tmpbound);
      lanes[i].exit_rightbound_elindex[j] = transdata.size();
      transdata.push_back(el);
      
      //--------------------------------------------------
      // 

      //=========================================================
      //
      // 
      //
      //=========================================================
      //--------------------------------------------------
      transdata.push_back(el);

    }
  }
  return true;
}


bool MapPrior::parseLaneLabel(string& str, LaneLabel& label)
{
  int startpos= 0;
  int endpos = 0;
  string tmpstr;
  
  startpos = endpos;
  endpos = str.find_first_of('.',startpos);
  if (endpos==-1){
    cerr<<"error parsing lane label "<<str<<endl;
    return false;}
  tmpstr = str.substr(startpos,endpos);
  label.segment = atoi(tmpstr.c_str());
  startpos = endpos+1;
  endpos = str.find_first_of('.',startpos);
  tmpstr = str.substr(startpos,endpos);
  label.lane = atoi(tmpstr.c_str());  

  return true;
}
 
bool MapPrior::parsePointLabel(string& str, PointLabel& label)
{
  int startpos= 0;
  int endpos = 0;
  string tmpstr;
  startpos = endpos;
  endpos = str.find_first_of('.',startpos);
  if (endpos==-1){ 
    cerr<<"error parsing point label "<<str<<endl;
    return false;}
  tmpstr = str.substr(startpos,endpos);
  label.segment = atoi(tmpstr.c_str());
  startpos = endpos+1;
  endpos = str.find_first_of('.',startpos);
  if (endpos==-1){
    cerr<<"error parsing point label "<<str<<endl;
    return false;}
  tmpstr = str.substr(startpos,endpos);
  label.lane = atoi(tmpstr.c_str());
  
  startpos = endpos+1;
  endpos = str.find_first_of('.',startpos);
  tmpstr = str.substr(startpos,endpos);
  label.point = atoi(tmpstr.c_str());  

  return true;
}

bool MapPrior::parseSpotLabel(string& str, SpotLabel& label)
{
  int startpos= 0;
  int endpos = 0;
  string tmpstr;
  
  startpos = endpos;
  endpos = str.find_first_of('.',startpos);
  if (endpos==-1){
    cerr<<"error parsing spot label "<<str<<endl;
    return false;}
  tmpstr = str.substr(startpos,endpos);
  label.zone = atoi(tmpstr.c_str());
  startpos = endpos+1;
  endpos = str.find_first_of('.',startpos);
  tmpstr = str.substr(startpos,endpos);
  label.spot = atoi(tmpstr.c_str());  

  return true;
}

// // Convert one element from global to local frame, using the
// // specified delta. The input and the output elements can be
// // the same object.
// static void elementGlobalToLocal(MapElement& out, const MapElement& in,
//                                  const point2& delta)
// {
//   out.center.x = in.center.x - delta.x;
//   out.center.y = in.center.y - delta.y;

//   out.position.x = in.position.x - delta.x;
//   out.position.y = in.position.y - delta.y;

//   out.geometryMax.x = in.geometryMax.x - delta.x;
//   out.geometryMax.y = in.geometryMax.y - delta.y;

//   out.geometryMin.x = in.geometryMin.x - delta.x;
//   out.geometryMin.y = in.geometryMin.y - delta.y;
  
//   unsigned int sze = in.geometry.size();
//   out.geometry.resize(sze);
//   for (unsigned int i = 0; i <sze; ++i){
//     out.geometry[i].x = in.geometry[i].x - delta.x;
//     out.geometry[i].y = in.geometry[i].y - delta.y;
//   }
// }



// static void elementLocalToGlobal(MapElement& out, const MapElement& in,
//                                  const point2& delta)
// {
//   elementGlobalToLocal(out, in, point2(0, 0)-delta);
// }

// static void elementRot(MapElement& el,const double ang)
// {
// 	if (fabs(ang)>.0000001){
// 		double sa = sin(ang);
// 		double ca = cos(ang);
// 		el.center.x = el.center.x*ca-el.center.y*sa;
// 		el.center.y = el.center.x*sa+el.center.y*ca;
		
// 		el.position.x = el.position.x*ca-el.position.y*sa;
// 		el.position.y = el.position.x*sa+el.position.y*ca;
		
// 		el.geometryMax.x = el.geometryMax.x*ca-el.geometryMax.y*sa;
// 		el.geometryMax.y = el.geometryMax.x*sa+el.geometryMax.y*ca;
		
// 		el.geometryMin.x = el.geometryMin.x*ca-el.geometryMin.y*sa;
// 		el.geometryMin.y = el.geometryMin.x*sa+el.geometryMin.y*ca;
		
// 		unsigned int sze = el.geometry.size();
// 		for (unsigned int i = 0; i <sze; ++i){
// 			el.geometry[i].x = el.geometry[i].x*ca-el.geometry[i].y*sa;
// 			el.geometry[i].y = el.geometry[i].x*sa+el.geometry[i].y*ca;
// 		}
// 	}
// }
// // Convert one element from global to local frame, using the
// // specified delta. The input and the output elements can be
// // the same object.
// static void elementGlobalToLocal(MapElement& out, const MapElement& in,
//                                 const point2& delta, const double ang )
// {
// 	out = in;
// 	elementGlobalToLocal(out,in,delta);
// 	elementRot(out,-ang);
// }
// static void elementLocalToGlobal(MapElement& out, const MapElement& in,
//                                  const point2& delta, const double ang)
// {
// 	out = in;
// 	elementRot(out,ang);
// 	elementLocalToGlobal(out,in,delta);
// } 

bool MapPrior::getElFull(MapElement & el, int index)
{
  if (index <0 || index > (int)fulldata.size()){
    cerr << "in MapPrior::getElFull(), invalid index " << index << endl;
    return false;
  }

  el = fulldata[index];
  el.translate(-delta.x,-delta.y);
  el.rotate(-deltaYaw);
  //  elementGlobalToLocal(el, el, delta,deltaYaw);

  return true;

}

bool MapPrior::getEl(MapElement & el, int index)
{
  if (index <0 || index > (int)data.size()){
    cerr << "in MapPrior::getEl(), invalid index " << index << endl;
    return false;
  }

  el = data[index];
  el.translate(-delta.x,-delta.y);
  el.rotate(-deltaYaw);
  //elementGlobalToLocal(el, el, delta, deltaYaw);

  return true;

}

bool MapPrior::setEl(const MapElement & el, int index)
{
  if (index <0 || index > (int)data.size()){
    cerr << "in MapPrior::setEl(), invalid index " << index << endl;
    return false;
  }

  data[index] = el;
  data[index].rotate(deltaYaw);
  data[index].translate(delta.x,delta.y);
  //elementLocalToGlobal(data[index], data[index], delta, deltaYaw);
  return true;

}

bool MapPrior::setElFull(const MapElement & el, int index)
{
  if (index <0 || index > (int)fulldata.size()){
    cerr << "in MapPrior::setElFull(), invalid index " << index << endl;
    return false;
  }
  fulldataUpdateFlag[index] = true;
  fulldata[index] = el;
  fulldata[index].rotate(deltaYaw);	
  fulldata[index].translate(delta.x,delta.y);
  //elementLocalToGlobal(fulldata[index], fulldata[index], delta, deltaYaw);
  return true;

}



/** 
 * Gets the internal vector index of the lane given a lane label
 */
int MapPrior::getLaneIndex(const LaneLabel &label)
{
  return (getLaneIndex(label.segment,label.lane));
}
int MapPrior::getLaneIndex(const PointLabel &label)
{
  return (getLaneIndex(label.segment,label.lane));
}
int MapPrior::getLaneIndex(const int segnum, const int lanenum)
{
  unsigned int i;

  for (i=0; i<lanes.size();++i){
    if (segnum == lanes[i].label.segment &&
        lanenum ==lanes[i].label.lane){
      
      return i;
    }
  } 
  // cerr << "Error in MapPrior::getLaneIndex, Segment " << segnum <<
  //  " lane " << lanenum << " not found" <<endl;
  return -1;
}


int MapPrior::getLanePointIndex(const PointLabel &label)
{
  return (getLanePointIndex(label.segment,label.lane,label.point));
}
int MapPrior::getLanePointIndex(const int segnum, const int lanenum, const int ptnum)
{
  unsigned int i;

  int index = getLaneIndex(segnum,lanenum);
  if (index<0)
    return false;

  PointLabel thislabel; 

  for (i=0; i<lanes[index].waypoint_label.size();++i){
    thislabel = lanes[index].waypoint_label[i];
    if (segnum == thislabel.segment &&
        lanenum ==thislabel.lane &&
        ptnum == thislabel.point){
      
      return i;
    }
  } 
  //cerr << "Error in MapPrior::getLanePointIndex, Segment " << segnum <<
  //  " lane " << lanenum << " point " << ptnum <<" not found" <<endl;
  return -1;
}

int MapPrior::getZoneIndex(const LaneLabel &label)
{
  return (getZoneIndex(label.segment));
}
int MapPrior::getZoneIndex(const PointLabel &label)
{
  return (getZoneIndex(label.segment));
}

int MapPrior::getZoneIndex(const SpotLabel &label)
{
  return (getZoneIndex(label.zone));
}
int MapPrior::getZoneIndex(const int zonenum)
{
  unsigned int i;
  unsigned int sze = zones.size();
  for (i=0; i<sze;++i){
    if (zonenum == zones[i].label){
      return i;
    }
  } 
  return -1;
}

int MapPrior::getZoneSpotIndex(const SpotLabel &label)
{
  return (getZoneSpotIndex(label.zone,label.spot));
}
int MapPrior::getZoneSpotIndex(const int zonenum, const int spotnum)
{
  unsigned int i;
  unsigned int sze = spots.size();

  for (i=0; i<sze;++i){
    if (zonenum == spots[i].label.zone &&
        spotnum == spots[i].label.spot){
      return i;
    }
  } 
  //cerr << "Error in MapPrior::getLanePointIndex, Segment " << segnum <<
  //  " lane " << lanenum << " point " << ptnum <<" not found" <<endl;
  return -1;
}
int MapPrior::getZonePointIndex(const PointLabel &label)
{
  return (getZonePointIndex(label.segment,label.lane,label.point));
}
int MapPrior::getZonePointIndex(const int segnum, const int lanenum, const int ptnum)
{
  unsigned int i;

  int index = getZoneIndex(segnum);
  if (index<0)
    return -1;

  PointLabel thislabel; 
  unsigned int sze = zones[index].perimeter_label.size();
  for (i=0; i<sze;++i){
    thislabel = zones[index].perimeter_label[i];
    if (segnum == thislabel.segment &&
        lanenum ==thislabel.lane &&
        ptnum == thislabel.point){
      
      return i;
    }
  } 
  //cerr << "Error in MapPrior::getLanePointIndex, Segment " << segnum <<
  //  " lane " << lanenum << " point " << ptnum <<" not found" <<endl;
  return -1;
}

/** 
 * Gets the internal vector index of the segment given the segment label
 */
int MapPrior::getSegmentIndex(const LaneLabel &label)
{
  return (getSegmentIndex(label.segment));
}
int MapPrior::getSegmentIndex(const PointLabel &label)
{
  return (getSegmentIndex(label.segment));
}
int MapPrior::getSegmentIndex(const int segnum)
{
  unsigned int i;

  for (i=0; i<segments.size();++i){
    if (segnum == segments[i].label){
      return i;
    }
  } 
  // cerr << "Error in MapPrior::getSegmentIndex, Segment " << segnum <<
  //  " lane " << lanenum << " not found" <<endl;
  return -1;
}


/** 
 *
 */
bool MapPrior::isEntryPoint(const PointLabel &ptlabelin)
{
  int index = getLaneIndex(ptlabelin);
  if (index<0)
    return false;

  unsigned int i;

  for (i=0;i<lanes[index].entry_label.size();++i){
    if (ptlabelin==lanes[index].entry_label[i]){
      return true;
    }
  }

  return false;
}

/** 
 *
 */

bool MapPrior::isExitPoint(const PointLabel &ptlabelin)
{
  int index = getLaneIndex(ptlabelin);
  if (index<0)
    return false;

  unsigned int i;

  for (i=0;i<lanes[index].exit_label.size();++i){
    if (ptlabelin==lanes[index].exit_label[i]){
      return true;
    }
  }

  return false;
}

/** 
 *
 */

bool MapPrior::isStopLine(const PointLabel &ptlabelin)
{
  int index = getLaneIndex(ptlabelin);
  if (index<0)
    return false;

  unsigned int i;

  for (i=0;i<lanes[index].stop_label.size();++i){
    if (ptlabelin==lanes[index].stop_label[i]){
      return true;
    }
  }

  return false;
}

/** 
 *
 */

bool MapPrior::isCheckPoint(const PointLabel &ptlabelin)
{
  int index = getLaneIndex(ptlabelin);
  if (index<0)
    return false;

  unsigned int i;

  for (i=0;i<lanes[index].checkpt_label.size();++i){
    if (ptlabelin==lanes[index].checkpt_label[i]){
      return true;
    }
  }

  return false;
}

/** 
 *
 */
bool MapPrior::isLaneSameDir(const LaneLabel &lanelabel1,const LaneLabel &lanelabel2)
{
  int index1 = getLaneIndex(lanelabel1);
  if (index1<0)
    return false;
  int index2 = getLaneIndex(lanelabel2);
  if (index2<0)
    return false;

  if (lanes[index1].direction == lanes[index2].direction){
    return true;
  }

  return false;
    

}

bool MapPrior::isPointInLane(const point2_uncertain &pt, const LaneLabel &lanelabelin)
{
  // point2arr_uncertain leftbound_unc, rightbound_unc;
  //   point2arr leftbound,rightbound;
  //   point2 thispt;
  //   int retval = getLaneBounds(leftbound_unc,rightbound_unc,lanelabelin);
  
  //   thispt = pt;
  //   leftbound = leftbound_unc;
  //   rightbound = rightbound_unc;

  //   if (retval<0)
  //     return false;


  //   int leftindex;
  //   int rightindex;
  //   int leftside;
  //   int rightside;

  
  //   //--------------------------------------------------
  //   // these funcitons need to be updated to work with uncertain points
  //   //--------------------------------------------------
  //   leftbound.get_side(thispt,leftside,leftindex);
  //   rightbound.get_side(thispt,rightside,rightindex);
  
  //   if (leftside!=rightside){
  //     if (leftindex==0 &&rightindex==0){
  //       return true;
  //     }
  //   }
  //   return false;
  point2arr_uncertain bounds_unc;
  point2arr bounds;
  point2 thispt(pt);
  int retval = getLaneBoundsPolyFull(bounds_unc,lanelabelin);
  if (retval <0) {
    cerr << "in isPointInLane() error finding lane bounds polygon"<< endl;
    return false;
  }
  bounds.set(bounds_unc);
  return (bounds.is_inside_poly(thispt));
}

bool MapPrior::isPointInLane(const point2arr_uncertain &ptarr, const LaneLabel &lanelabelin)
{
  point2arr_uncertain bounds_unc;
  point2arr bounds;
  point2arr thisptarr(ptarr);
  int retval = getLaneBoundsPolyFull(bounds_unc,lanelabelin);
  if (retval <0) {
    cerr << "in isPointInLane() error finding lane bounds polygon"<< endl;
    return false;
  }  
  return (bounds.is_poly_point_overlap(thisptarr));
  
}
bool MapPrior::isPolyInLane(const point2arr_uncertain &ptarr, const LaneLabel &lanelabelin)
{
  point2arr_uncertain bounds_unc;
  point2arr bounds;
  point2arr thisptarr(ptarr);
  int retval = getLaneBoundsPolyFull(bounds_unc,lanelabelin);
  if (retval <0) {
    cerr << "in isPolyInLane() error finding lane bounds polygon"<< endl;
    return false;
  }  
  return (bounds.is_poly_overlap(thisptarr));
}
bool MapPrior::isLineInLane(const point2arr_uncertain &ptarr, const LaneLabel &lanelabelin)
{
  point2arr_uncertain bounds_unc;
  point2arr bounds;
  point2arr thisptarr(ptarr);
  int retval = getLaneBoundsPolyFull(bounds_unc,lanelabelin);
  if (retval <0) {
    cerr << "in isLineInLane() error finding lane bounds polygon"<< endl;
    return false;
  } 
  return (bounds.is_poly_line_overlap(thisptarr));
}


bool MapPrior::isElementInLane(const MapElement &el, const LaneLabel &lanelabelin)
{
  point2arr_uncertain bounds_unc;
  int retval = getLaneBoundsPolyFull(bounds_unc,lanelabelin);
  if (retval <0) {
    cerr << "in isElementInLane() error finding lane bounds polygon"<< endl;
    return false;
  }  
  
  return el.isOverlap(bounds_unc,GEOMETRY_POLY);
}

int MapPrior::getLanesInElement(vector<LaneLabel> labelarr, const MapElement &el)
{
  unsigned int i;
  LaneLabel thislabel;
  for (i=0; i<lanes.size();++i){
    thislabel = lanes[i].label;
    if (isElementInLane(el,thislabel)){
      labelarr.push_back(thislabel);
    }
  } 
  return (int)labelarr.size();
}


int MapPrior::getTransitionBounds(point2arr_uncertain &leftbound, 
                                  point2arr_uncertain &rightbound, 
                                  const PointLabel &ptlabelfrom,
                                  const PointLabel &ptlabelto)
{
  int index = getLaneIndex(ptlabelfrom);
  if (index<0)
    return -1;
  int leftindex, rightindex;
  unsigned int j, numtrans;
  numtrans = lanes[index].exit_label.size();
  for (j=0;j<numtrans;++j){
    if(lanes[index].exit_label[j]==ptlabelfrom &&
       lanes[index].exit_link[j]==ptlabelto){
      leftindex = lanes[index].exit_leftbound_elindex[j];
      rightindex = lanes[index].exit_rightbound_elindex[j];
      leftbound.set(transdata[leftindex].geometry);
      rightbound.set(transdata[rightindex].geometry);
      return leftbound.size();
    }
  }
  leftbound.clear();
  rightbound.clear();
  return -1;

}



/** 
 *
 */
int MapPrior::getLaneLeftBound(point2arr_uncertain &bound, 
                               const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;
  int elindex = lanes[index].leftbound_elindex;
  MapElement el;
  if (!getEl(el,elindex)){
    return -1;    
  }
  bound = el.geometry;
  return bound.size();


}
int MapPrior::getLaneLeftBoundFull(point2arr_uncertain &bound, 
                                   const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;
  int elindex = lanes[index].leftbound_elindex;
  MapElement el;
  if (!getElFull(el,elindex)){
    return -1;    
  }
  bound = el.geometry;
  return bound.size();


}


/** 
 *
 */
int MapPrior::getLaneRightBound(point2arr_uncertain &bound, 
                                const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;
  int elindex = lanes[index].rightbound_elindex;
  MapElement el;
  if (!getEl(el,elindex)){
    return -1;
  }
  bound = el.geometry;
  return bound.size();
}
int MapPrior::getLaneRightBoundFull(point2arr_uncertain &bound, 
                                    const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;
  int elindex = lanes[index].rightbound_elindex;
  MapElement el;
  if (!getElFull(el,elindex)){
    return -1;
  }
  bound = el.geometry;
  return bound.size();
}

/** 
 *
 */
int MapPrior::getLaneBounds(point2arr_uncertain &leftbound, 
                            point2arr_uncertain &rightbound, 
                            const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;
  int elindex = lanes[index].leftbound_elindex;
  MapElement leftel, rightel;
  if (!getEl(leftel,elindex)){
    return -1; 
  }
  leftbound = leftel.geometry;
  elindex = lanes[index].rightbound_elindex;
  if (!getEl(rightel,elindex)){
    return -1; 
  }
  rightbound = rightel.geometry;
  return leftbound.size();
}
int MapPrior::getLaneBoundsFull(point2arr_uncertain &leftbound, 
                                point2arr_uncertain &rightbound, 
                                const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;
  int elindex = lanes[index].leftbound_elindex;
  MapElement leftel, rightel;
  if (!getElFull(leftel,elindex)){
    return -1; 
  }
  leftbound = leftel.geometry;
  elindex = lanes[index].rightbound_elindex;
  if (!getElFull(rightel,elindex)){
    return -1; 
  }
  rightbound = rightel.geometry;
  return leftbound.size();
}
/** 
 *
 */
int MapPrior::getLaneBoundsPoly(point2arr_uncertain &polybound, 
                                const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;
  int elindex = lanes[index].leftbound_elindex;

  MapElement el;
  if (!getEl(el,elindex)){
    return -1; 
  }
  polybound = el.geometry;
  int tmpsize = polybound.size();

  elindex = lanes[index].rightbound_elindex;
  if (!getEl(el,elindex)){
    return -1; 
  }
  point2arr_uncertain tmpbound;
  tmpbound = el.geometry;
  tmpbound.reverse();
  polybound.connect(tmpbound);

  return tmpsize;
}


int MapPrior::getLaneBoundsPolyFull(point2arr_uncertain &polybound, 
                                    const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;
  int elindex = lanes[index].leftbound_elindex;

  MapElement el;
  if (!getElFull(el,elindex)){
    return -1; 
  }
  polybound = el.geometry;
  int tmpsize = polybound.size();

  elindex = lanes[index].rightbound_elindex;
  if (!getElFull(el,elindex)){
    return -1; 
  }
  point2arr_uncertain tmpbound;
  tmpbound = el.geometry;
  tmpbound.reverse();
  polybound.connect(tmpbound);

  return tmpsize;
}

/** 
 *
 */
int MapPrior::getLaneCenterLine(point2arr_uncertain &centerline, 
                                const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;
  int elindex = lanes[index].waypoint_elindex;

  MapElement el;
  if (!getEl(el,elindex)){
    return -1; 
  }

  centerline= el.geometry;
  
  return centerline.size();

}


int MapPrior::getLaneCenterLineFull(point2arr_uncertain &centerline, 
                                    const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;
  int elindex = lanes[index].waypoint_elindex;

  point2arr_uncertain waypts,tmparr;
  
  MapElement el;
  if (!getElFull(el,elindex)){
    return -1; 
  }
  centerline= el.geometry;
  return centerline.size();

}


/** 
 *
 */
int MapPrior::getWayPoint(point2_uncertain &pt, 
                          const PointLabel &ptlabelin)
{
  int index, ptindex;
  point2arr_uncertain waypts;
  MapElement el;
  
  // check for point in lane
  index =  getLaneIndex(ptlabelin);
  if (index>=0){
    ptindex = getLanePointIndex(ptlabelin);
  
    if (ptindex<0){
      cerr << "in MapPrior::getPoint(), point " << ptlabelin << " not found "<< endl;
      return -1;  
    }
    if (getEl(el,lanes[index].waypoint_elindex)){
      waypts = el.geometry;
      pt = waypts[ptindex];
    }else{
      cerr << "in MapPrior::getPoint(), element " << index << " not found "<< endl;
      return -1;
    }
    return 0;    
  }
  
  // check for point in zone
  index = getZoneIndex(ptlabelin);
  if (index>=0){
    ptindex = getZonePointIndex(ptlabelin);
    
    if (ptindex<0){
      cerr << "in MapPrior::getPoint(), point " << ptlabelin << " not found "<< endl;
      return -1;  
    }
    if (getEl(el,zones[index].perimeter_elindex)){
      waypts = el.geometry;
      pt = waypts[ptindex];
    }else{
      cerr << "in MapPrior::getPoint(), element " << index << " not found "<< endl;
      return -1;
    }
    return 0;    
  }
  // shouldn't get here
  return 0;
  
}


/** 
 *
 */
int MapPrior::getWayPoint(point2arr_uncertain &ptarr, 
                          const vector<PointLabel> &ptlabelarrin)
{
  ptarr.clear();
  point2_uncertain pt;
  int retval;
  for (unsigned int i=0; i< ptlabelarrin.size();++i){
    retval = getWayPoint(pt,ptlabelarrin[i]);
    if (retval>=0){
      ptarr.push_back(pt);
    }else{
      ptarr.clear();
      cerr << "in MapPrior::getPointArr(), point " << ptlabelarrin[i] << " not found "<< endl;
      return retval;
    }
  }
  return 0;
}



//intersection queries
/** 
 *
 */
int MapPrior::getWayPointEntries(vector<PointLabel>& ptlabelarr,
                                 const PointLabel &ptlabelin)
{

  PointLabel tmplabel;
  unsigned int i,j;
  ptlabelarr.clear();
  unsigned int sze;
  int index;
      

  // check for a lane point
  index = getLaneIndex(ptlabelin);
  if (index>=0){
    // get out of lane transitions
    sze = lanes[index].entry_label.size();
    for (i=0;i<sze;++i){
      if (ptlabelin==lanes[index].entry_label[i]){
        ptlabelarr.push_back(lanes[index].entry_link[i]);
      }
    }

    // get inlane transitions
    sze = lanes[index].waypoint_label.size();
    //cout <<"labelarr = " << lanes[index].waypoint_label << endl;
    for (i=0;i<sze;++i){
      if (ptlabelin==lanes[index].waypoint_label[i]){
        if (i>0){
          tmplabel = lanes[index].waypoint_label[i-1];
          for (j= 0; j<ptlabelarr.size();++j){
            if (ptlabelarr[j]==tmplabel){
              return ptlabelarr.size();
            }
          }
          ptlabelarr.push_back(lanes[index].waypoint_label[i-1]);
          return ptlabelarr.size();
        }
      }
    }
    return ptlabelarr.size();
  }

  //  check for a perimeter point
  index = getZoneIndex(ptlabelin);
  if (index>=0){
    // get out of lane transitions
    sze = zones[index].entry_label.size();
    for (i=0;i<sze;++i){
      if (ptlabelin==zones[index].entry_label[i]){
        ptlabelarr.push_back(zones[index].entry_link[i]);
      }
    }
    return ptlabelarr.size();
  }
  return -1;
}

/** 
 *
 */
int MapPrior::getWayPointExits(vector<PointLabel>& ptlabelarr,
                               const PointLabel &ptlabelin)
{
  PointLabel tmplabel;
  unsigned int i,j;
  ptlabelarr.clear();
  unsigned int sze;
  int index;

  // check for a lane point
  index = getLaneIndex(ptlabelin);
  if (index>=0){
   
    //get out of lane transitions
    sze = lanes[index].exit_label.size();
    for (i=0;i<sze;++i){
      if (ptlabelin==lanes[index].exit_label[i]){
        ptlabelarr.push_back(lanes[index].exit_link[i]);
      }
    }
    // get in lane transitions
    sze = lanes[index].waypoint_label.size();
    for (i=0;i<sze;++i){
      if (ptlabelin==lanes[index].waypoint_label[i]){
        if (i<(sze-1)){
          tmplabel = lanes[index].waypoint_label[i+1];
          for (j= 0; j<ptlabelarr.size();++j){
            if (ptlabelarr[j]==tmplabel){
              return ptlabelarr.size();
            }
          }
          ptlabelarr.push_back(lanes[index].waypoint_label[i+1]);
          return ptlabelarr.size();
        }
      }
    }
    return ptlabelarr.size();
  }

  //  check for a perimeter point
  index = getZoneIndex(ptlabelin);
  if (index>=0){
    // get out of lane transitions
    sze = zones[index].exit_label.size();
    for (i=0;i<sze;++i){
      if (ptlabelin==zones[index].exit_label[i]){
        ptlabelarr.push_back(zones[index].exit_link[i]);
      }
    }
    return ptlabelarr.size();
  }
  return -1;


}


int MapPrior::getLaneExits(vector<PointLabel>& thislabelarr,
                           vector<PointLabel>& otherlabelarr, 
                           const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;
  thislabelarr.clear();
  thislabelarr = lanes[index].exit_label;
  otherlabelarr = lanes[index].exit_link;
  if (thislabelarr.size()!=otherlabelarr.size()){
    cerr << "in MapPrior::getLaneExits() size mismatch in entry and exit point labels" << endl;
    return -1;
  }
  
  return thislabelarr.size();

}


int MapPrior::getLaneEntries(vector<PointLabel>& thislabelarr,
                             vector<PointLabel>& otherlabelarr, 
                             const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;
  thislabelarr.clear();
  thislabelarr = lanes[index].entry_label;
  otherlabelarr = lanes[index].entry_link;
  if (thislabelarr.size()!=otherlabelarr.size()){
    cerr << "in MapPrior::getLaneEntries() size mismatch in entry and exit point labels" << endl;
    return -1;
  }

  return thislabelarr.size();

}



int MapPrior::getZoneExits(vector<PointLabel>& thislabelarr,
                           vector<PointLabel>& otherlabelarr, 
                           const int zonelabelin)
{
  int index = getZoneIndex(zonelabelin);
  if (index<0)
    return -1;
  thislabelarr.clear();
  thislabelarr = zones[index].exit_label;
  otherlabelarr = zones[index].exit_link;
  if (thislabelarr.size()!=otherlabelarr.size()){
    cerr << "in MapPrior::getZoneExits() size mismatch in entry and exit point labels" << endl;
    return -1;
  }
  
  return thislabelarr.size();

}


int MapPrior::getZoneEntries(vector<PointLabel>& thislabelarr,
                             vector<PointLabel>& otherlabelarr, 
                             const int zonelabelin)
{
  int index = getZoneIndex(zonelabelin);
  if (index<0)
    return -1;
  thislabelarr.clear();
  thislabelarr = zones[index].entry_label;
  otherlabelarr = zones[index].entry_link;
  if (thislabelarr.size()!=otherlabelarr.size()){
    cerr << "in MapPrior::getZoneEntries() size mismatch in entry and exit point labels" << endl;
    return -1;
  }

  return thislabelarr.size();

}


/** 
 *
 */
int MapPrior::getLaneStopLines(vector<PointLabel>& ptlabelarr,
                               const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;
  ptlabelarr.clear();
  ptlabelarr = lanes[index].stop_label;
  return ptlabelarr.size();
}

/** 
 *
 */
int MapPrior::getLaneCheckPoints(vector<PointLabel>& ptlabelarr,
                                 const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;
  ptlabelarr.clear();
  ptlabelarr = lanes[index].checkpt_label;
  return ptlabelarr.size();
}

/** 
 *
 */
int MapPrior::getLaneCheckPointNumbers(vector<int>& ptnumarr,
                                       const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;
  ptnumarr.clear();
  ptnumarr = lanes[index].checkpt_num;
  return ptnumarr.size();
}



/** 
 *
 */
int MapPrior::getLaneName(string &name, 
                          const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;
  name = lanes[index].name;
  return 0;
}

/** 
 *
 */
int MapPrior::getNeighborLane(LaneLabel& lanelabel, 
                              const LaneLabel &lanelabelin, const int offset)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;

  int retval = 1;
  LaneLabel nextlabel,thislabel,lastlabel;
  LaneLabel zerolabel(0,0);

  lastlabel = lanes[index].label;
  nextlabel = lastlabel;
  int thisoffset;
  bool flipflag;
  if (offset>0){
    thisoffset = offset;
    flipflag = true;
  }
  else if (offset<0){
    thisoffset = -offset;
    flipflag = false;
  }else{ //offset == 0
    lanelabel = lanelabelin;
    return retval;
  }
  
  for (int i=0; i<=thisoffset;++i){
    thislabel = lanes[index].label;

    if (flipflag){
      nextlabel = lanes[index].rightneighbor_label;
    }else{
      nextlabel = lanes[index].leftneighbor_label;
    }
    if (nextlabel == lastlabel){ // reverse
      if (retval!=2) retval = 2;
      else retval = 1;

      flipflag = !flipflag;
      if (flipflag){
        nextlabel = lanes[index].rightneighbor_label;
      }else{
        nextlabel = lanes[index].leftneighbor_label;
      }
    }
    if (i==thisoffset)
      break;

    lanelabel = nextlabel;

    lastlabel = thislabel;
    if (nextlabel == zerolabel){
      break;
    }
    index = getLaneIndex(nextlabel);
    if (index<0){
      cerr << "in MapPrior::getLaneLabelToLeft(), bad neighbor index found " << thislabel<< " from " << lanelabelin << endl; 
      return -1;
    }
    
  }

  if (lanelabel ==zerolabel) retval = 0;
  return retval;
}



/** 
 *
 */
int MapPrior::getSameDirLanes(vector<LaneLabel>& lanelabelarr, const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;
 
  int numlanes = getSegmentNumLanes(lanelabelin);
  //  int lanenum = lanelabelin.lane;
  LaneLabel tmplabel;
  int retval;
  lanelabelarr.clear();
 
  for (int i=-numlanes-1;i<=numlanes;++i){
    retval = getNeighborLane(tmplabel,lanelabelin,i);
    //    cout << "Sameoffset = " << i << " retval = " << retval <<" tmplabel = " << tmplabel << endl;
    if (retval==1){
      lanelabelarr.push_back(tmplabel);
    }
  }
  return lanelabelarr.size();
}

/** 
 *
 */
int MapPrior::getOppDirLanes(vector<LaneLabel>& lanelabelarr, const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;
  
  int numlanes = getSegmentNumLanes(lanelabelin);
  //  int lanenum = lanelabelin.lane;
  LaneLabel tmplabel;
  int retval;
  lanelabelarr.clear();
  for (int i=-numlanes-1;i<=numlanes;++i){
    retval = getNeighborLane(tmplabel,lanelabelin,i);
    
    if (retval==2){
      lanelabelarr.push_back(tmplabel);
    }
  }
  return lanelabelarr.size();
}

/** 
 *
 */
int MapPrior::getAllDirLanes(vector<LaneLabel>& lanelabelarr, const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;
  
  int numlanes = getSegmentNumLanes(lanelabelin);
  //  int lanenum = lanelabelin.lane;
  LaneLabel tmplabel;
  int retval;
  lanelabelarr.clear();
  for (int i=-numlanes-1;i<=numlanes;++i){
    retval = getNeighborLane(tmplabel,lanelabelin,i);

    
    if (retval>0){
      lanelabelarr.push_back(tmplabel);
    }
  }
  return lanelabelarr.size();
}


/** 
 *
 */  
int MapPrior::getSameDirLaneBounds(point2arr_uncertain &leftbound, 
                                   point2arr_uncertain &rightbound, 
                                   const LaneLabel &lanelabelin)
{
  vector<LaneLabel> lanelabelarr;
  int retval = getSameDirLanes(lanelabelarr,lanelabelin);
  if (retval <=0)
    return -1;
  leftbound.clear();
  rightbound.clear();
  
  int numlanes = retval;
  if (getLaneLeftBound(leftbound,lanelabelarr[0]) <0)
    return -1;

  if (getLaneRightBound(rightbound,lanelabelarr[numlanes-1]) <0)
    return -1;

  
  
  return numlanes;
}

/** 
 *
 */
int MapPrior::getOppDirLaneBounds(point2arr_uncertain &leftbound, 
                                  point2arr_uncertain &rightbound, 
                                  const LaneLabel &lanelabelin)
{
  vector<LaneLabel> lanelabelarr;
  int retval = getOppDirLanes(lanelabelarr,lanelabelin);
  if (retval <=0)
    return -1;
  leftbound.clear();
  rightbound.clear();
  
  int numlanes = retval;
  if (getLaneRightBound(leftbound,lanelabelarr[0]) <0)
    return -1;

  if (getLaneLeftBound(rightbound,lanelabelarr[numlanes-1]) <0)
    return -1;

  
  
  return numlanes;
}

/** 
 *
 */
int MapPrior::getSegmentBounds(point2arr_uncertain &leftbound, 
                               point2arr_uncertain &rightbound, 
                               const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;

  int thisdirection= lanes[index].direction;

  vector<LaneLabel> lanelabelarr;
  int retval = getAllDirLanes(lanelabelarr,lanelabelin);
  if (retval <=0)
    return -1;
  leftbound.clear();
  rightbound.clear();
  
  int numlanes = retval;
  
  int leftindex = getLaneIndex(lanelabelarr[0]);
  if (leftindex<0)
    return -1;

  int rightindex = getLaneIndex(lanelabelarr[numlanes-1]);
  if (rightindex<0)
    return -1;

  if (lanes[leftindex].direction==thisdirection){
    if(getLaneLeftBound(leftbound,lanelabelarr[0]) <0)
      return -1;
  }else{
    if(getLaneRightBound(leftbound,lanelabelarr[0]) <0)
      return -1;
  }

  if (lanes[rightindex].direction==thisdirection){
    if(getLaneRightBound(rightbound,lanelabelarr[numlanes-1]) <0)
      return -1;
  }else{
    if(getLaneLeftBound(rightbound,lanelabelarr[numlanes-1]) <0)
      return -1;
  }
  
  return numlanes;
}


/** 
 *
 */
int MapPrior::getSegmentLanes(vector<LaneLabel>& lanelabelarr, const int seglabelin)
{
  int index = getSegmentIndex(seglabelin);
  if (index<0)
    return -1;
  
  lanelabelarr = segments[index].lane_label;
  return 0;
}




/** 
 *
 */
int MapPrior::getSegmentName(string &name, const int seglabelin)
{
  int index = getSegmentIndex(seglabelin);
  if (index<0)
    return -1;
  name = segments[index].name;
  return 0;
}


/** 
 *
 */
//int MapPrior::getSegmentEntryFromLabels(vector<PointLabel>& lanelabelarr,
//                                        const int seglabelin)
//{
//  return 0;
//}

/** 
 *
 */

int MapPrior::getSegmentNumLanes(const PointLabel & pointlabelin)
{
  return getSegmentNumLanes(pointlabelin.segment);
}

int MapPrior::getSegmentNumLanes(const LaneLabel & lanelabelin)
{
  return getSegmentNumLanes(lanelabelin.segment);
}

int MapPrior::getSegmentNumLanes(const int seglabelin)
{
  int index = getSegmentIndex(seglabelin);
  if (index<0)
    return -1;
  return segments[index].lane_label.size();
}

int MapPrior::setLocalToGlobalOffset(const VehicleState &state)
{
  if (state.timestamp<=0)
    return -1;

  delta.set(state.utmNorthing-state.localX, state.utmEasting-state.localY);
  deltaYaw = acos(cos(state.utmYaw-state.localYaw));
  return 0;
}

  
