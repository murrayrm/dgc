/**********************************************************
 **
 **  TESTMAPELEMENTLISTENER.CC
 **
 **    Time-stamp: <2007-09-22 19:10:54 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Feb  7 13:39:57 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include <iostream>
#include "MapElementTalker.hh"
#include "MapElement.hh"
#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"

using namespace std;




int main(int argc, char **argv)
{

  int skynetKey = 0;
  int subgroup = 0;
  int testnum = 0;
  int thistest=0;
  bool testflag =0;
  string  testname;

  skynetKey = atoi(getenv("SKYNET_KEY"));  

 cout << endl << "Usage :" << endl;
  cout << " testRecvMapElement [testnum] [subgroup] [skynet key]" << endl << endl;


  

	if(argc >1)
    testnum = atoi(argv[1]);
  if(argc >2)
    subgroup = atoi(argv[2]);
  if(argc >3)
    skynetKey = atoi(argv[3]);

  int bytesRecv = 0;

  cout << "Skynet Key = " << skynetKey << 
    "  Subgroup = "  << subgroup << endl;
  

  CMapElementTalker testtalker;

  MapElement el;
	uint64_t dtime =0;
	uint64_t maxdtime =0;
  double avgdtime =0.0;

	cout <<"========================================" << endl;
	testname = "Testing flood read timed block";
	thistest++;
	testflag = (thistest==testnum || 
							(testnum<0 &&-thistest<=testnum));
	cout <<"Test #" << thistest << "  " << testname;
	if (!testflag)
		cout << "  NOT RUN" << endl;
	else{
		cout << "  RUNNING" << endl;
		
		int count = 0; 
    int msgper = 1;
    int sleeptime = 0;
	uint64_t lasttime;
		uint64_t timestart;
		int printdtime;
		sleeptime = 10;
    cout << "Waiting to receive..."<< endl;
    if (msgper<1)
      msgper = 1;

    testtalker.initRecvMapElement(skynetKey,subgroup);		
		lasttime = DGCgettime();
		while(1){

      

			bytesRecv = testtalker.recvMapElementTimedBlock(&el,sleeptime);
			
			if (bytesRecv > 0){
				
				dtime = DGCgettime()-el.timestamp;
				if (dtime>maxdtime)
					maxdtime = dtime;
				 
				avgdtime+=dtime;
				count++;
				
			
			}

			if (DGCgettime()-lasttime>1000000){
				if (count>0){		
					avgdtime = avgdtime/count; 
				}else { avgdtime = 0;}
				cout << "count= " << count << " bytes= " << bytesRecv << "  dtime= " << dtime << "  avg= " << avgdtime << "  max= " << maxdtime << endl;
				maxdtime = 0;
				avgdtime = 0;
				count = 0;
				lasttime = DGCgettime();
			}


		}
	}

   
  
	cout <<"========================================" << endl;
	testname = "Testing flood read ";
	thistest++;
	testflag = (thistest==testnum || 
							(testnum<0 &&-thistest<=testnum));
	cout <<"Test #" << thistest << "  " << testname;
	if (!testflag)
		cout << "  NOT RUN" << endl;
	else{
		cout << "  RUNNING" << endl;
		
		int count = 0; 
    int msgper = 1;
    int sleeptime = 0;
		cout << "Enter usleep delay in receiving loop (0 for no usleep call)" << endl;
		cin>>sleeptime;	
    cout << "Enter number of messages per status printout " << endl;
		cin>>msgper;	
    cout << "Waiting to receive with forced loop delay " << sleeptime<< "..."<< endl;
    if (msgper<1)
      msgper = 1;

    testtalker.initRecvMapElement(skynetKey,subgroup);		
		while(1){

      if (sleeptime>0)
        usleep(sleeptime);
      

			bytesRecv = testtalker.recvMapElementBlock(&el,subgroup);
			
		
 
		 
			if (bytesRecv > 0){
				
				dtime = DGCgettime()-el.timestamp;
				if (dtime>maxdtime)
					maxdtime = dtime;
				 
				avgdtime+=dtime;
				count++;
				
				if (count%msgper==0){ 
					avgdtime = avgdtime/count; 
					
					cout << "count= " << count << " bytes= " << bytesRecv << "  dtime= " << dtime << "  avg= " << avgdtime << "  max= " << maxdtime << endl;
					maxdtime = 0;
					avgdtime = 0;
					
				}
			}
		}
	}
	
		
  cout <<"========================================" << endl;
	testname = "Testing Element ID Distribution Stats";
	thistest++;
	testflag = (thistest==testnum || 
							(testnum<0 &&-thistest<=testnum));
	cout <<"Test #" << thistest << "  " << testname;
	if (!testflag)
		cout << "  NOT RUN" << endl;
	else{
		cout << "  RUNNING" << endl;
		vector<int> idarr;
		vector<int> valarr;
		vector<int> totarr;
		vector<int> typearr;
    vector<int> typecntarr;
		vector<MapId> fidarr;
		vector<int> idcntarr;

    double timesec =0;
    double dtimesec =0;

		bool newflag;
		int i; 
		int count;
		int every = 10;
		uint64_t starttime = 0;
		int reltime = 0;
		int dreltime = 0;
    int lastreltime = 0;
		int timecount = 1;
		uint64_t thistime;
		int thisid =0;
	  int subview = 0;
	
    cout << "Output format : "<< endl;
    cout << "Time delta: id=rate(total count) id=rate(total count) ..." << endl;

    cout << "====================================" << endl;
		cout << "Enter ID for secondary type data viewing  (0 for none)" << endl;
		cin>>subview;			
    cout << "Waiting to receive ..."<< endl;
    testtalker.initRecvMapElement(skynetKey,subgroup);
		while(1){
  
			bytesRecv = testtalker.recvMapElementBlock(&el,subgroup);
			if (bytesRecv>0){
				newflag = true;
				thistime = el.state.timestamp;
				if (starttime==0 && thistime>0)
					starttime = thistime;
      
				if (starttime>0 && thistime>0){
					reltime = (int)(thistime-starttime);
				}

				if (el.id.size()==0)
					thisid = -1;
				else
					thisid = el.id[0];
      
      
      
				for (i=0;i<(int)idarr.size();++i){
					if (thisid == idarr[i]){
						valarr[i] = valarr[i]+1;
						totarr[i] = totarr[i]+1;
						newflag = false;
						break;
					}
				}
				if (newflag){
					idarr.push_back(thisid);
					valarr.push_back(1);
					totarr.push_back(1);
				}
      
   
				if (thisid == subview){
					newflag = true;
					for (i=0;i<(int)typearr.size();++i){
						if ((int)el.type == typearr[i]){
							typecntarr[i] = typecntarr[i]+1;
							newflag = false;
							break;
						}
					}
      
					if (newflag){
						typearr.push_back((int)el.type);
						typecntarr.push_back(1);
					}
        
					newflag = true;
					for (i=0;i<(int)fidarr.size();++i){
						if (el.id == fidarr[i]){
							idcntarr[i] = idcntarr[i]+1;
							newflag = false;
							break;
						}
					}
      
					if (newflag){
						fidarr.push_back(el.id);
						idcntarr.push_back(1);
					}
      



    
				}
      
        
				count++;
				if (reltime>timecount*1000000){
					//if (count%every==0){
          
					timecount++;
          dreltime = reltime-lastreltime;
          lastreltime = reltime;

					timesec = ((double)reltime)/1000000;
          dtimesec = ((double)dreltime)/1000000;

          cout << timesec << ": IDs ";
					for(i=0;i<(int)idarr.size();++i){
						cout  <<idarr[i] << "=" << ((double)valarr[i])/dtimesec << "(" << totarr[i] << ") " ;
						valarr[i]=0;
					}
					cout << endl;

					if (subview!=0){
						cout << "    ID "  << subview << ": Types ";
						for(i=0;i<(int)typearr.size();++i){
							cout  << typearr[i] << "=" << ((double)typecntarr[i])/dtimesec << "  " ;
							typecntarr[i]=0;
						}
						cout << endl;

          
						cout << "    ID " << subview << ": Full IDs  ";
						for(i=0;i<(int)fidarr.size();++i){
							cout << fidarr[i] << "=" << ((double)idcntarr[i])/dtimesec << "  " ;
							idcntarr[i]=0;
						}
						cout << endl;
					}
        
				}




			}else{
				usleep(100);      
			}
		}

          
	}


	cout <<"========================================" << endl;
	testname = "Testing blocking read";
	thistest++;
	testflag = (thistest==testnum || 
							(testnum<0 &&-thistest<=testnum));
	cout <<"Test #" << thistest << "  " << testname;
	if (!testflag)
		cout << "  NOT RUN" << endl;
	else{
		cout << "  RUNNING" << endl;
    testtalker.initRecvMapElement(skynetKey,subgroup);
		while(1){
				
			bytesRecv = testtalker.recvMapElementBlock(&el,subgroup);
			if (bytesRecv>0){
				cout << "bytes received = " << bytesRecv << endl;
				cout << "reveived el :" << el<< endl; 
			}
		}
			
	}
	cout <<"========================================" << endl;
	testname = "Testing non-blocking read";
	thistest++;
	testflag = (thistest==testnum || 
							(testnum<0 &&-thistest<=testnum));
	cout <<"Test #" << thistest << "  " << testname;
	if (!testflag)
		cout << "  NOT RUN" << endl;
	else{
		cout << "  RUNNING" << endl;
    testtalker.initRecvMapElement(skynetKey,subgroup);
		while(1){
				
			bytesRecv = testtalker.recvMapElementNoBlock(&el,subgroup);
			if (bytesRecv>0){
				cout << "bytes received = " << bytesRecv << endl;
				cout << "reveived el :" << el<< endl; 
			}else{
				usleep(100000);      
			}
		}
			
	}

  if (0){ 
    //--------------------------------------------------
    // starttemplate for new test
    //--------------------------------------------------
   
    cout <<"========================================" << endl;
    testname = "Testing ";
    thistest++;
    testflag = (thistest==testnum || 
                (testnum<0 &&-thistest<=testnum));
    cout <<"Test #" << thistest << "  " << testname;
    if (!testflag)
      cout << "  NOT RUN" << endl;
    else{
      cout << "  RUNNING" << endl;
          
    }

    //--------------------------------------------------
    // end template for new test
    //--------------------------------------------------   
  }
 
	// testtalker.finiRecvMapElement();
 
  cout <<"========================================" 
       << endl << endl;
  return(0);
}

