Sat Jun  2  0:44:24 2007	Sam Pfister (sam)

	* version R1-02r-sam
	BUGS:  
	FILES: Map.cc(26131), testMap.cc(26131)
	updated getTransitionBounds function to return interpolated corners

Fri Jun  1  5:03:47 2007	Sam Pfister (sam)

	* version R1-02r
	BUGS:  
	FILES: MapPrior.cc(25983)
	Small update to lane checking functions to move overlap queries to
	use interpolated lanes

Fri Jun  1  4:35:39 2007	Sam Pfister (sam)

	* version R1-02q
	BUGS:  
	FILES: Map.cc(25932), Map.hh(25932), MapPrior.cc(25932),
		MapPrior.hh(25932), testGeometry.cc(25932),
		testMap.cc(25932)
	updated lane functions to all return full interpolated lane.  Fixed
	some problems in old functions which should be retired but are still being used

	FILES: Map.cc(25955), Map.hh(25955), testGeometry.cc(25955),
		testMap.cc(25955)
	added getStopLineSensed function to get the sensed stopline if we
	see one

	FILES: Map.hh(25937), MapPrior.cc(25937), MapPrior.hh(25937),
		testMap.cc(25937)
	updated some map functions to help mapper send obstacles to the
	mplanner in a more robust way.
	



	
Tue May 29 23:37:09 2007	Jessica Gonzalez (jengo)

	* version R1-02p
	BUGS:  
	FILES: MapElement.cc(25474), MapElement.hh(25474)
	added elements for initial and final conditions from ocpspecs

Mon May 28 13:03:04 2007	Jessica Gonzalez (jengo)

	* version R1-02o
	BUGS:  
	FILES: MapElement.cc(25232), MapElement.hh(25232)
	changed color for polytopes

Sat May 26  0:21:29 2007	Sam Pfister (sam)

	* version R1-02n
	BUGS:  
	FILES: Map.cc(25028), testMap.cc(25028)
	updated getBounds and getBoundsReverse functions to return the
	interpolated data around some turns.  This is a temporary fix, as
	in the future, funtionality should be built on top of the
	getLaneBounds functions. Note that the range argument of the
	funcitons specify the minimum total length of the lines to be
	returned, and the last optional argument, backrange, specifies the
	distance behind state to start the line.  If range and backrange
	are both 20 for example, then the lane lines will start around 20
	meters behind state and extend up to the state point given.  To
	specify lanes that start 20 meters behind and extend 20 meters in
	front, specify range=40 and backrange=20.  This function will
	return left and right lane lines of equal size and will trim to
	the nearest waypoint.

Fri May 25 14:05:07 2007	Sam Pfister (sam)

	* version R1-02m
	BUGS:  
	FILES: MapPrior.cc(24989), testMapPrior.cc(24989)
	updated interpolation to make it sparser for now to help with the
	polytope creation of planning

Wed May 23 13:18:22 2007	Sam Pfister (sam)

	* version R1-02l
	BUGS:  
	FILES: Map.cc(24668), Map.hh(24668), MapElement.cc(24668),
		MapElement.hh(24668), testGeometry.cc(24668),
		testMapPrior.cc(24668)
	Updated dist functions in MapElement which calculate the min dist
	to other map elements as well as points or polylines.  Can be used
	to check for partial blocking of lanes.  These functions will
	return 0 if there is overlap between the geometries.  They haven't
	been fully tested but the interface shouldn't change.

Sat May 19 17:54:20 2007	Jessica Gonzalez (jengo)

	* version R1-02k
	BUGS:  
	FILES: MapElement.cc(24051), MapElement.hh(24051)
	added polytope and planner traj elements

Sat May 19 10:26:41 2007	Sam Pfister (sam)

	* version R1-02j
	BUGS:  
	FILES: Map.cc(23943), Map.hh(23943), MapElement.hh(23943),
		MapId.cc(23943), MapId.hh(23943), MapPrior.cc(23943),
		MapPrior.hh(23943), testMap.cc(23943),
		testMapPrior.cc(23943)
	Fixed some other issues with getTransitionBounds.  Added some new
	transition handling functionality but it's not used yet.  

Fri May 18 15:42:25 2007	Sam Pfister (sam)

	* version R1-02i
	BUGS:  
	FILES: Map.cc(23818), MapPrior.cc(23818), MapPrior.hh(23818),
		testMapPrior.cc(23818)
	changed retired getTransitionBounds function to not use ptarr
	assignment operator.  May fix a bug in tplanner.  Some intermediate
	work is done on a priori transition bounds calculations.

Fri May 18 10:16:50 2007	Sam Pfister (sam)

	* version R1-02h
	BUGS:  
	FILES: Map.cc(23757), testMap.cc(23757)
	updated getTransitionBounds to work a bit better

Fri May 18  9:46:29 2007	Sam Pfister (sam)

	* version R1-02g
	BUGS:  
	FILES: Map.cc(23747), Map.hh(23747), MapId.hh(23747),
		MapPrior.cc(23747), MapPrior.hh(23747), testMap.cc(23747),
		testMapPrior.cc(23747)
	quick fix for issue getTransitionBounds function in Map.cc has with
	santa anita rndf which loops back on itself.  need a better fix for
	long term and that function itself is going to be retired.

Thu May 17 17:31:06 2007	Sam Pfister (sam)

	* version R1-02f
	BUGS:  
	FILES: Map.cc(23586), Map.hh(23586), MapPrior.cc(23586),
		MapPrior.hh(23586), testMap.cc(23586),
		testMapPrior.cc(23586)
	reorganized MapPrior RNDF parsing, fixed bug in old function
	getObstacleDist and GetobstaclePoint.

Thu May 17 15:37:18 2007	Sam Pfister (sam)

	* version R1-02e
	BUGS:  
	FILES: MapPrior.cc(23536), MapPrior.hh(23536),
		testGeometry.cc(23536), testMap.cc(23536),
		testMapPrior.cc(23536)
	Updated MapPrior to incorporate waypoint and lane line
	interpolation.	Updated getWayPointExits and getWayPointEntries to
	return entry and exit possibilities in same lane.

	FILES: MapPrior.cc(23528), MapPrior.hh(23528),
		testGeometry.cc(23528), testMap.cc(23528),
		testMapPrior.cc(23528)
	updated interpolation functions in MapPrior.  Updated the
	getWayPointEntries and getWayPointExits functions to return the
	possible transition in the same lane across

Wed May 16 23:20:28 2007	 (ahoward)

	* version R1-02d
	BUGS:  
	FILES: MapElement.hh(23404)
	Tweaked for cross-platform compatability

Tue May 15 23:34:40 2007	Sam Pfister (sam)

	* version R1-02c
	BUGS:  
	New files: testMapPrior.cc
	FILES: Makefile.yam(23258), Map.cc(23258), Map.hh(23258),
		MapElement.cc(23258), MapElement.hh(23258),
		testMap.cc(23258)
	Fixed bug in getObs functions which would return sensed lanes as
	well as obstacles and vehicles.  Added functions isObstacle() and
	isLine() to MapElement.  Other small bug fixes.

Mon May 14 23:29:36 2007	Jessica Gonzalez (jengo)

	* version R1-02b
	BUGS:  
	FILES: Makefile.yam(23128)
	removed MapPrior

	FILES: MapElement.cc(23123), MapElement.hh(23123)
	added elements for steer commands, rddf, etc

Mon May 14 11:42:04 2007	Sam Pfister (sam)

	* version R1-02a
	BUGS:  
	FILES: Makefile.yam(22998), Map.cc(22998), Map.hh(22998),
		MapElement.cc(22998), MapElement.hh(22998),
		MapElementTalker.cc(22998), MapElementTalker.hh(22998),
		MapId.cc(22998), MapId.hh(22998), MapPrior.cc(22998),
		MapPrior.hh(22998), testGeometry.cc(22998),
		testMap.cc(22998), testRecvMapElement.cc(22998),
		testSendMapElement.cc(22998)
	Updated the map interface to a cleaner, more uniform structure. 
	Had to change the format of some map interface functions, but will
	migrate all modules which use map to the new version. Implemented a
	cleaner more complete library of RNDF and obstacle query functions
	for the planners.  Cleaned up MapElement object interface which
	might also break some code, but will be easy to fix and easier to
	use from now on.  Implemented some MapElement overlap checking
	functions which use the improved bounding box functionality on the
	MapElement.  Added an elevation term to the MapElement structure to
	handle obstacles like overhanging trees which have freespace
	underneath.  Added additional timestamp to MapElement and
	additional max and min bound terms.

Thu May 10 13:52:05 2007	Jessica Gonzalez (jengo)

	* version R1-01w
	BUGS:  
	New files: StateTalker.cc
	FILES: Makefile.yam(22690), MapElement.hh(22690)
	added program to send alice info to mapviewer

	FILES: MapElement.cc(22627), MapElement.hh(22627)
	commented everything

	FILES: MapElement.cc(22693), MapElement.hh(22693)
	added traversed path

Mon Apr 30 12:25:29 2007	Sam Pfister (sam)

	* version R1-01v
	BUGS:  
	FILES: Map.cc(21706), Map.hh(21706), MapElement.cc(21706),
		MapElementTalker.cc(21706), MapElementTalker.hh(21706),
		MapPrior.cc(21706), MapPrior.hh(21706),
		testGeometry.cc(21706), testMap.cc(21706),
		testRecvMapElement.cc(21706), testSendMapElement.cc(21706)
	Updated MapElementTalker to take advantage of new skynet
	functionality to send messages on a sub channel.  Implemented
	variable length skynet messages.  For modules which send map
	elements, no code change is needed, just a recompile against the
	updated map and frames modules.  For modules which receive map
	elements, add the intended subnet group to the initRecvMapElement
	function as follows : initRecvMapElement(skynetKey,recvChannel);
	Without this, change only elements on channel 0 will be received.  
	  Added map interface functions getLaneDistToWaypoint which
	calculate distance along corridor between two points.  Updated
	getBounds, getBoundsReverse and getTransitionBounds to return equal
	sized left and right bounds.

Sun Apr 29  0:11:57 2007	datamino (datamino)

	* version R1-01u
	BUGS:  
	FILES: MapElement.cc(21506)
	Fixing MapElement::print_state(), broken by the recent change to
	the VehicleState structure.

Wed Apr 25 17:19:17 2007	Sam Pfister (sam)

	* version R1-01t
	BUGS:  
	FILES: Map.cc(20714), Map.hh(20714), testMap.cc(20714)
	added getLaneID, getSegmentID, checkLaneID functions to tplanner
	interface with map.

Fri Mar 30 20:12:14 2007	Laura Lindzey (lindzey)

	* version R1-01s
	BUGS: 
	FILES: testGeometry.cc(19038)
	test functions for ellipse class

Fri Mar 30  2:16:48 2007	Laura Lindzey (lindzey)

	* version R1-01r
	BUGS: 
	FILES: testGeometry.cc(18403)
	code for testing point2 average 

	FILES: testGeometry.cc(18531)
	adding test for fitline

	FILES: testGeometry.cc(18533)
	added get_inside function

	FILES: testGeometry.cc(18560)
	test functions for merge and get_overlap

	FILES: testGeometry.cc(18976)
	changes to test functions

Sat Mar 17 11:21:01 2007	Sam Pfister (sam)

	* version R1-01q
	BUGS: 
	FILES: MapElement.cc(18258), MapElement.hh(18258),
		MapPrior.cc(18258), testGeometry.cc(18258),
		testMap.cc(18258)
	changed color enums from COLOR_RED to MAP_COLOR_RED to avoid some
	collision when compiling lineperceptor

Sat Mar 17  0:47:13 2007	Sam Pfister (sam)

	* version R1-01p
	BUGS: 
	FILES: Map.cc(18177), Map.hh(18177), testGeometry.cc(18177),
		testMap.cc(18177)
	Added getLaneCenterPoint, getLaneRightPoint, and getLaneLeftPoint
	functions for tplanner queries.  Added functions to test map
	queries and geometry operations.

Fri Mar 16  0:27:21 2007	Sam Pfister (sam)

	* version R1-01o
	BUGS: 
	FILES: Map.cc(17906), Map.hh(17906), testMap.cc(17906)
	added new getHeading function which gets an estimate of the lane
	heading at some point if that point is inside valid lane bounds.

Thu Mar 15 13:59:39 2007	Sam Pfister (sam)

	* version R1-01n
	BUGS: 
	FILES: Map.cc(17802), Map.hh(17802)
	added addElement function which adds an element if the id is new
	and updates the element if it has been seen.

Wed Mar 14 15:57:50 2007	Sam Pfister (sam)

	* version R1-01m
	BUGS: 
	FILES: Map.cc(17594), Map.hh(17594), testMap.cc(17594)
	added getTransitionBounds and getBounds functions which are more
	functional and useful for tplanner than existing versions.  Added
	getHeading to get the heading angle at a waypoint.  Added
	getBoundsReverse for getting lanebounds in reverse.

Wed Mar 14 10:14:48 2007	Sam Pfister (sam)

	* version R1-01l
	BUGS: 
	FILES: Map.cc(17568), Map.hh(17568), testGeometry.cc(17568),
		testMap.cc(17568)
	Added getObstaclePoint(offset) function for tplanner use which
	returns a point offset from closest obstacle in lane by specified
	amount

	FILES: testGeometry.cc(17548), testMap.cc(17548)
	more geometric test functions

Tue Mar 13 15:45:16 2007	Sam Pfister (sam)

	* version R1-01k
	BUGS: 
	New files: testGeometry.cc
	FILES: Makefile.yam(17469), Map.cc(17469), Map.hh(17469),
		MapElement.cc(17469), MapElement.hh(17469),
		MapPrior.cc(17469), rndf.txt(17469), testMap.cc(17469)
	Added testGeometry function to test and visualize 2D geometric
	functions.  Implemented getObstacleDist() which given a point in
	local frame returns the distance to nearest obstacle in current
	lane in front of Alice.

Mon Mar 12 15:22:21 2007	Sam Pfister (sam)

	* version R1-01j
	BUGS: 
	FILES: Map.cc(17247), Map.hh(17247), MapElement.cc(17247),
		MapPrior.cc(17247), testMap.cc(17247)
	added getNextStopline function.  Need to debug an RNDF reversing
	issue

	FILES: MapId.cc(17226), MapId.hh(17226), testMapId.cc(17226)
	updated testMapId program and MapId class functionality

Sun Mar 11 11:49:13 2007	Sam Pfister (sam)

	* version R1-01i
	BUGS: 
	New files: testMapElement.cc testMapId.cc
	FILES: MapElement.cc(17098), MapElement.hh(17098),
		MapElementTalker.cc(17098), testSendMapElement.cc(17098)
	Added clear and alice state messages to map element.

Sat Mar 10 17:22:05 2007	Sam Pfister (sam)

	* version R1-01h
	BUGS: 
	New files: MapId.cc MapId.hh testMap.cc
	Deleted files: test_map.cc
	FILES: Makefile.yam(17025), Map.cc(17025), Map.hh(17025),
		MapElement.hh(17025), MapElementTalker.cc(17025),
		MapElementTalker.hh(17025), MapPrior.cc(17025),
		MapPrior.hh(17025), testRecvMapElement.cc(17025),
		testSendMapElement.cc(17025)
	removed tabs in place of spaces.  Fixed bug in getStopline
	function.  Moved test_map.cc to testMap.cc.

Fri Mar  9 16:33:29 2007	Sam Pfister (sam)

	* version R1-01g
	BUGS: 
	FILES: Map.cc(16900), Map.hh(16900), test_map.cc(16900)
	added isStop and isExit query functions to map.

Fri Mar  9 12:15:02 2007	Sam Pfister (sam)

	* version R1-01f
	BUGS: 
	New files: MapElement.cc MapElement.hh MapElementTalker.cc
		MapElementTalker.hh testRecvMapElement.cc
		testSendMapElement.cc
	FILES: Makefile.yam(16849), Map.hh(16849), MapPrior.hh(16849),
		test_map.cc(16849)
	Moved MapElementTalker, testSendMapElement, testRecvMapElement, and
	MapElement.hh to map module.  This is part of a general
	restructuring which moves MapElement functionality into map.  All
	users of the MapElementTalker will need to include
	map/MapElementTalker.hh and map/MapElement.hh instead of
	skynettalker/MapElementTalker and interfaces/MapElement.hh.  Also
	-lmap should be added to LIBS in the Makefile.yam if it isn't there
	already 

Thu Mar  8 18:32:56 2007	Noel duToit (ndutoit)

	* version R1-01e
	BUGS: 
	FILES: Map.cc(16816), Map.hh(16816), rndf.txt(16816),
		test_map.cc(16816)
	Some updates in map to interface with new tplanner.

	FILES: MapPrior.cc(16751), test_map.cc(16751)
	local changes by Sam

Sat Mar  3 15:55:28 2007	Sam Pfister (sam)

	* version R1-01d
	BUGS: 
	FILES: Map.cc(16394), Map.hh(16394), test_map.cc(16394)
	Updated getStopline function to take only a point in space not a
	stopline label

Thu Mar  1 19:33:50 2007	Sam Pfister (sam)

	* version R1-01c
	BUGS: 
	New files: rndf.txt test_map.cc
	FILES: Makefile.yam(16013), Map.cc(16013), Map.hh(16013),
		MapPrior.cc(16013), MapPrior.hh(16013)
	implemented first cut of getTransitionBounds and added test_map
	program to test map queries and plot the results

	FILES: Map.cc(15922), Map.hh(15922)
	added base query function declarations to Map.hh .

	FILES: Map.cc(15999), Map.hh(15999), MapPrior.cc(15999),
		MapPrior.hh(15999)
	Implemented the getLeftBound and getRightBound functions.  Still
	need intersection transition.

Tue Feb 27  8:39:02 2007	Sam Pfister (sam)

	* version R1-01b
	BUGS: 
	New files: MapPrior.cc MapPrior.hh
	Deleted files: LocalMapTalker.cc LocalMapTalker.hh
		LogicalRoadObject.cc LogicalRoadObject.hh RoadObject.cc
		RoadObject.hh
	FILES: Makefile.yam(15900), Map.cc(15900), Map.hh(15900)
	Moved new map structure from mapper into map module.  Map now
	parses RNDF files with an internal method.

Sun Feb 25 12:56:24 2007	murray (murray)

	* version R1-01a
	BUGS: 
	FILES: LocalMapTalker.cc(15691), LocalMapTalker.hh(15691)
	updated to work with new interfaces release

2007-02-25  murray  <murray@gclab.dgc.caltech.edu>

	* LocalMapTalker.hh: updated SkynetContainer path
	* LocalMapTalker.cc: added interfaces/sn_types.h
	(CSkynetContainer): updated modName to type int

Thu Feb  1 19:26:58 2007	Sam Pfister (sam)

	* version R1-00b
	BUGS: 
	FILES: Makefile.yam(13982)
	commented out LIB_MODULE_LINKS := libmap line in Makefile.yam.	It
	was making a broken link.

	FILES: Map.hh(13981)
	Adding simpler endpoint data structure to map for short term use.  

Sat Jan 27 18:25:27 2007	Nok Wongpiromsarn (nok)

	* version R1-00a
	BUGS: 
	New files: LocalMapTalker.cc LocalMapTalker.hh LogicalRoadObject.cc
		LogicalRoadObject.hh Map.cc Map.hh RoadObject.cc
		RoadObject.hh
	FILES: Makefile.yam(13230)
	Added Map class and LocalMapTalker

Sat Jan 27 18:14:18 2007	Nok Wongpiromsarn (nok)

	* version R1-00
	Created map module.














































