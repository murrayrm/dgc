#include <FL/gl.h>
#include <GL/glu.h>
#include <FL/gl.h>
#include <GL/glu.h>
#ifdef MACOSX
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <iomanip>

#include <alice/AliceConstants.h>
#include <dgcutils/DGCutils.hh>
#include "DebugWindow.hh"

using namespace std;


DebugWindow::DebugWindow(int X,int Y,int W,int H,const char*L) 
  : Fl_Gl_Window(X,Y,W,H,L)
{
  center_x = 0;
  center_y = 0;
  scale = 0;
  selected = -1;
  selectedRndf = -1;

  map = NULL;
  
  latestState.timestamp = 0; // this will prevent it from being drawn
  latestState.localX = latestState.localY = 0;
  stateDelta = point2(0, 0);

  //--------------------------------------------------
  // this is to reverse y axis for left handed coordinates
  //--------------------------------------------------
  ymult = -1;
  xAxisRight = true;
  drawStateFlag = true;
  centerOnAlice = true;
  showTrav = true;
  debugLevel=0;
  elrate = 0;
  showUncertainty = false;
  showIDs = 0;
}


void DebugWindow::draw() {
	
  if (!valid()) {
    gl_font(FL_HELVETICA ,12);
    valid(1);
  }

/*
  if (centerOnAlice && latestState.timestamp > 0)
      set_view(latestState.localX, ymult*latestState.localY);
*/
  if (centerOnAlice && (stateDelta.x != 0 || stateDelta.y != 0))
  {
      // don't really center, just follow Alice
      set_view(center_x + stateDelta.x, center_y + ymult*stateDelta.y);
      // state delta is set by updateState to defer set_view() to a
      // time where the opengl context is set (like here), now reset it
      stateDelta = point2(0, 0);
  }

  reshape();			
  glClear(GL_COLOR_BUFFER_BIT);
  //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  //glEnable(GL_DEPTH_TEST);

  DrawGrid();
  DrawElements();

  DrawDebugData();

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  gluOrtho2D(0,w(),0,h());
	

  string status_str = "Position x = " 
      + to_string(mouse_local_x) + 
      " y = " + to_string(mouse_local_y) + "     ";

  if (map != NULL)
  {
      const vector<MapElement>& localmap = map->data;
      if (selected <0 || selected >= (int)localmap.size()){
          selected = -1;
          if (selectedRndf < 0
              || selectedRndf > int(map->prior.fulldata.size()))
              status_str = status_str + "  None selected";
          else
              status_str = status_str + "  RNDF Element " + 
                  id_to_string(map->prior.fulldata[selectedRndf].id) + " selected";
      } else {
          status_str = status_str + "  Element " + 
              id_to_string(localmap[selected].id) + " selected";
      }
      //sprintf(buf, "x=%f, y=%f ", mouse_local_x, mouse_local_y);
  }
	
  glColor3f(1.0f, 1.0f, 0.0f);
  gl_font(FL_HELVETICA, 12);
  
  gl_draw(status_str.c_str() , 20,10);

  string state_str;
  glColor3f(1.0f, 1.0f, 1.0f);
  int numel = map == NULL ? 0 : map->data.size();
  state_str = "Number of elements = " + to_string(numel);
  if (centerOnAlice){
      state_str = state_str+"  Follow Alice    ";
  }
  
  gl_draw(state_str.c_str() , 20,22);

}


string DebugWindow::id_to_string(const MapId& id)
{
  string out = "";
  int thisid;
  for (int i = 0; i < (int)id.size(); ++i){
    if (i>0)
      out = out + ".";
    thisid = id[i];
    out = out + to_string(thisid);
  }
  return out;
}


void DebugWindow::reset_map()
{
  memset(&latestState, 0, sizeof(latestState));
  if (map)
      *map = Map(); // there doesn't seem to be a better way to reset it ...
                    // map->data.clear() won't reset the prior and other stuff
  reshape();
  redraw();
}

void DebugWindow::reshape(){
  glViewport(0,0,w(),h());
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  double tmpscale = scale_to_mult(scale);
	
  double x1 = center_x-tmpscale*w()/2;
  double x2 = center_x+tmpscale*w()/2;
  double y1 = center_y-tmpscale*h()/2;
  double y2 = center_y+tmpscale*h()/2;
	

	
  gluOrtho2D(x1,x2,y1,y2);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

}

void DebugWindow::DrawEllipse(point2_uncertain pt){

  double major = pt.max_var;
  double minor = pt.min_var;
  double ang = pt.axis;
  point2 center(pt.x,pt.y);
  int numpts = 100;
  point2 ptarr[numpts];
  point2 thispt;
  double dang = 2*M_PI/(numpts);
  double thisR = 0;
  double thisAng = 0;
  double smallval = .000000001;

  if (major<=0)
    return;
  if (minor<=0)
    minor = smallval;
	
			
  for (int i= 0; i<numpts; ++i){
    thisR = sqrt(1/ (pow((cos(thisAng)/major),2) 
                     + pow((sin(thisAng)/minor),2)) );
    thispt.x = thisR*cos(thisAng+ang);
    thispt.y = thisR*sin(thisAng+ang);
    thispt = thispt+center;
    ptarr[i] = thispt;
		
    thisAng = thisAng+dang;
		
  }
	
  glLineWidth(1);
  glPointSize(3);			
  glColor3f(1.0, 0.0, 0.0);
  glBegin(GL_LINE_LOOP);
  for (int i =0 ; i<numpts; ++i){
    glVertex2f(ptarr[i].x,ymult*(ptarr[i].y));
  }
  glEnd();

}

void DebugWindow::DrawText(string txt, double x, double y , double width)
{
  int len = (int)txt.size();
  double tmpscale = width/104.76;

  float widthtot =0;
  for (int i =0 ; i < len ; ++i){
    widthtot = widthtot + glutStrokeWidth(GLUT_STROKE_ROMAN,txt[i]);
  }
  widthtot = widthtot*tmpscale;

  glPushMatrix();
  glTranslatef(x-(double)widthtot/2, ymult*(y+width/2), 0);
  glScalef(tmpscale,tmpscale,tmpscale);
  for (int i =0 ; i < len ; ++i){
    glutStrokeCharacter(GLUT_STROKE_ROMAN, txt[i]);
  }
  glPopMatrix();

}

void DebugWindow::DrawGrid()
{
  double minx, maxx, miny, maxy;
  double tmpscale = scale_to_mult(scale);
  minx = center_x-tmpscale*w()/2;
  maxx = center_x+tmpscale*w()/2;
  miny = center_y-tmpscale*h()/2;
  maxy = center_y+tmpscale*h()/2;
	
  //cout << "minx = " << minx 
  //		 << "maxx = " << maxx 
  //		 << "miny = " << miny 
  //		 << "maxy = " << maxy << endl;
	
  double exponent = log10(tmpscale);
  double rem = exponent-floor(exponent);
  exponent = floor(exponent);
  double delta = pow(10,exponent+2);
  if (rem > log10(5.0))
    delta = 5*delta;
  else if (rem > log10(2.0))
    delta = 2*delta;


  double minx_grid = floor((minx)/delta)*delta;
  double maxx_grid = ceil((maxx)/delta)*delta;

  double miny_grid = floor((miny)/delta)*delta;
  double maxy_grid = ceil((maxy)/delta)*delta;
	

  glLineWidth(1);
  glColor3f(0.3, 0.3, 0.3);
  glBegin(GL_LINE_STRIP);
  for (double x =minx_grid ; x<=maxx_grid ; x+=delta){
    glVertex2f(x,miny_grid);
    glVertex2f(x,maxy_grid);
    glVertex2f(x+delta,maxy_grid);
  }
  glEnd();
	


  glBegin(GL_LINE_STRIP);
  for (double y =miny_grid ; y<=maxy_grid ; y+=delta){
    glVertex2f(minx_grid,(y));
    glVertex2f(maxx_grid,(y));
    glVertex2f(maxx_grid,(y+delta));
  }
  glEnd();

  glColor3f(0.5, 0.5, 0.5);

	
  string axis_str;
	
  gl_font(FL_HELVETICA,12);
  axis_str = "(m)" ;

  double xoffset = tmpscale*10;
  double yoffset = tmpscale*20;

  gl_draw(axis_str.c_str(),(float)(minx+xoffset),
          (float)(maxy-yoffset));
	
  for (double x =minx_grid ; x<=maxx_grid ; x+=delta){
    axis_str =  to_string(x) ;
    gl_font(FL_HELVETICA, 12);
    if (x> minx+3*xoffset)
      gl_draw(axis_str.c_str() , (float)x, (float)((maxy-yoffset)));
  }

  for (double y =miny_grid ; y<=maxy_grid ; y+=delta){
    axis_str =  to_string(ymult*y) ;
    gl_font(FL_HELVETICA, 12);
    if (y < maxy-2*yoffset)
      gl_draw(axis_str.c_str() , (float)(minx+xoffset), (float)(y));
  }

  double csze = tmpscale*5;
  glBegin(GL_LINE_STRIP);
  glVertex2f(center_x+csze,(center_y+csze));
  glVertex2f(center_x-csze,(center_y-csze));
  glEnd();

  glBegin(GL_LINE_STRIP);
  glVertex2f(center_x-csze,(center_y+csze));
  glVertex2f(center_x+csze,(center_y-csze));
  glEnd();

	
  //	cout << "delta "<< delta << "  mingrid " << minx_grid << endl;
	
	
}

bool DebugWindow::isElementVisible(const MapElement & el)
{
  double minx, maxx, miny, maxy;
  double tmpscale = scale_to_mult(scale);

  double width = w();
  double height = h();

  if (el.geometryMax.x==0 && el.geometryMin.x==0)
    return true;


  minx = center_x-tmpscale*width/2;
  maxx = center_x+tmpscale*width/2;
  if (ymult==-1){
    miny = -(center_y+tmpscale*height/2);
    maxy = -(center_y-tmpscale*height/2);   
  }else{
    miny = (center_y-tmpscale*height/2);
    maxy = (center_y+tmpscale*height/2);
  }

  
  if (el.geometryMax.x<minx)
    return false;
  if (el.geometryMin.x>maxx)
    return false;
  if (el.geometryMax.y<miny)
    return false;
  if (el.geometryMin.y>maxy)
    return false;

  return true;

}


void DebugWindow::DrawElement(const MapElement& element, bool isSel)
{
    MapElement el = element; // make a copy so we can write on it

    if (!isElementVisible(el)){
        return;
    }

    if (el.state.timestamp>0){
        updateState(el.state);
/*
        if (latestState.timestamp<el.state.timestamp){
            float diffX = el.state.localX - latestState.localX + stateDelta.x;
            float diffY = el.state.localY - latestState.localY + stateDelta.y;
            if (centerOnAlice && latestState.timestamp > 0) {
                // don't really center, just follow Alice
                set_view(center_x + diffX, center_y + ymult*diffY);
                // state delta is set by updateState to defer set_view() to a
                // time where the opengl context is set (like here), now reset it
                stateDelta = point2(0, 0);
            }
            latestState = el.state;
        }
*/
    }
  
    if (el.type==ELEMENT_TRAV_PATH)
        if (!showTrav)
            return;

    if (el.type==ELEMENT_ALICE){
        if (drawStateFlag){
            DrawVehicle(el.state);
        }
        return;
    } else if(el.type==ELEMENT_CLEAR) {
        return;
    }

    bool drawboundflag =false;
    bool drawgeometryflag =false;
    bool drawgeometryptflag =false;


    int glmode =0; 
    double boundx[4];
    double boundy[4];
    if (el.length > 0 && isSel){
        
        drawboundflag = true;
        
        double len,wid,orient;
        len = el.length/2;
        wid = el.width/2;
        orient = el.orientation;
        double co = cos(orient);
        double so = sin(orient);
        boundx[0] = el.center.x+(len*co -wid*(so));
        boundy[0] = el.center.y+(len*so +wid*(co));
        boundx[1] = el.center.x+(-len*co -wid*(so));
        boundy[1] = el.center.y+(-len*so +wid*(co));
        boundx[2] = el.center.x+(-len*co +wid*(so));
        boundy[2] = el.center.y+(-len*so -wid*(co));
        boundx[3] = el.center.x+(len*co +wid*(so));
        boundy[3] = el.center.y+(len*so -wid*(co));
    }
    if (el.geometry.size() > 0){
        drawgeometryflag = true;
        drawgeometryptflag = false;
        //--------------------------------------------------
        // draw geometry
        //--------------------------------------------------
        // switch(el.type){
        // case ELEMENT_WAYPOINTS:
        //case ELEMENT_PERIMETER:
        //case ELEMENT_PARKING_SPOT:
        //	drawgeometryptflag = true;
        //  break;
        //default:
        //  drawgeometryptflag = false;
        // }
        
        switch(el.geometryType){
        case GEOMETRY_UNDEF:			
        case GEOMETRY_POINTS:
            glmode = GL_POINTS;
            break;
        case GEOMETRY_ORDERED_POINTS:
            glmode = GL_LINE_STRIP;
            drawgeometryptflag = true;
            break;
        case GEOMETRY_LINE:
            glmode =GL_LINE_STRIP;
            break;
        case GEOMETRY_EDGE:
            glmode =GL_LINE_STRIP;
            break;
        case GEOMETRY_POLY:
            glmode = GL_LINE_LOOP;
            break;
        default:
            glmode = GL_POINTS;
        }
    }

    // draw the time stopped 
    if (showTimeStopped && el.timeStopped > 0) {
        stringstream s;
        s << fixed << setprecision(1) << el.timeStopped;
        string text = s.str();
        DrawText(text,el.position.x+1,el.position.y+1,1);
    }

    //	glColor3f(0.0, .50, .50);
	

    //if (el.label.size()>0){
    //	for (int i=0; i <el.label.size(); ++i){
    //		gl_draw(el->label[i].c_str(),(int)(el->center.x), (int)(el->center.y + i*12));
    //	}
    //}
    if (showUncertainty){
        if (el.position.max_var>0 && el.position.min_var>0){
            DrawEllipse(el.center);
        }
    }

    //--------------------------------------------------
    // Drawing here
    //--------------------------------------------------
    glLineWidth(1);
    glPointSize(3);
    if (isSel){
        glLineWidth(2);
        glPointSize(5);	
        el.plotColor = MAP_COLOR_RED;
        el.plotValue = 100;
        drawgeometryptflag = true;
        DrawVehicle(el.state);
        //	textsize = 2*textsize;
    }
    int i;

    if (drawboundflag){
        GLint factor =1;
        GLushort pattern = 0x5555;
        glEnable(GL_LINE_STIPPLE);
        glLineStipple(factor,pattern);
 
        set_color(el.plotColor, 20);		
        if (xAxisRight){
        
            glBegin(GL_LINE_LOOP);
            for (i =0 ; i<4; ++i){
                glVertex2f(boundx[i],ymult*(boundy[i]));
            }
            glEnd();
        }else{
            glBegin(GL_LINE_LOOP);
            for (i =0 ; i<4; ++i){
                glVertex2f(boundy[i],(boundx[i]));
            }
            glEnd();
        }
        glDisable(GL_LINE_STIPPLE);
    }      
    
    if (drawgeometryflag){
        set_color(el.plotColor, el.plotValue);		
        if (xAxisRight){
            glBegin(glmode);
            for (i =0 ; i< (int)el.geometry.size() ; ++i){
                glVertex2f(el.geometry[i].x, ymult*(el.geometry[i].y));
            }
            glEnd();
        }else{
            glBegin(glmode);
            for (i =0 ; i< (int)el.geometry.size() ; ++i){
                glVertex2f(el.geometry[i].x, ymult*(el.geometry[i].y));
            }
            glEnd();
        }
    }      
    if (drawgeometryptflag){
        set_color(el.plotColor, el.plotValue);		
        if (xAxisRight){
            glBegin(GL_POINTS);
            for (i =0 ; i< (int)el.geometry.size() ; ++i){
                glVertex2f(el.geometry[i].x, ymult*(el.geometry[i].y));
            }
            glEnd();
        }else{
            glBegin(glmode);
            for (i =0 ; i< (int)el.geometry.size() ; ++i){
                glVertex2f(el.geometry[i].x, ymult*(el.geometry[i].y));
            }
            glEnd();
        }
    }      



    //  set_color(el.plotColor, el.plotValue);		
    //  if (xAxisRight){
    //  glBegin(GL_POINTS);
    //  glVertex2f(el.position.x,ymult*(el.position.y));
    //  glEnd();
    //}else{
    //  glBegin(GL_POINTS);
    //  glVertex2f(el.position.x,ymult*(el.position.y));
    //  glEnd();
    // }
    double textsize = 1;
    MapId tempID;
    //    fprintf(stderr, "show ids: %d \n", showIDs);		
    if(showIDs) {
        int temp = el.id.size() - 1;
        tempID.push_back(el.id.dat.at(temp));
        DrawText(id_to_string(tempID),el.position.x, (el.position.y), textsize);
    }
    
		
    if (isSel){
        glLineWidth(1);
        glPointSize(3);


        glColor3f(0, 1.0, 0);
        gl_font(FL_HELVETICA,10);
        string idstr = id_to_string(el.id);
        gl_draw(idstr.c_str(),(float)(selectedpt.x), (float)(ymult*selectedpt.y));
        double offset;
        for (int i=0;i<(int)el.label.size();++i){
            offset = scale_to_mult(scale)*(i+1)*12;
            gl_draw(el.label[i].c_str(),(float)(selectedpt.x), (float)(ymult*(selectedpt.y+offset)));
        }

    }
		
}

void DebugWindow::DrawVehicle(const VehicleState &state) {
    if (state.timestamp<=0)
        return;

    double cx = state.localX;
    double cy = state.localY;
    double orient = state.localYaw;

    double xvel = state.localXVel;
    double yvel = state.localYVel;

    double dfront = DIST_REAR_AXLE_TO_FRONT;
    double drear = DIST_REAR_TO_REAR_AXLE;
    double dside = VEHICLE_WIDTH/2;

    double tmpx[4];
    double tmpy[4];
	
    double co = cos(orient);
    double so = sin(orient);

    tmpx[0] = cx+(dfront*co -dside*so);
    tmpy[0] = cy+(dfront*so +dside*co);

    tmpx[1] = cx+(-drear*co -dside*so);
    tmpy[1] = cy+(-drear*so +dside*co);

    tmpx[2] = cx+(-drear*co +dside*so);
    tmpy[2] = cy+(-drear*so -dside*co);

    tmpx[3] = cx+(dfront*co +dside*so);
    tmpy[3] = cy+(dfront*so -dside*co);

    glLineWidth(1);
    glPointSize(4);			
    glColor3f(.5, .5, .5);
    glBegin(GL_POLYGON);
    for (int i =0 ; i<4; ++i){
        glVertex2f(tmpx[i],ymult*(tmpy[i]));
    }
    glEnd();
	
    glColor3f(1, 1, 1);
    glBegin(GL_LINE_LOOP);
    for (int i =0 ; i<4; ++i){
        glVertex2f(tmpx[i],ymult*(tmpy[i]));
    }
    glEnd();
	

    //  glColor3f(1, 1, 1);
    glBegin(GL_LINE_STRIP);
    glVertex2f(cx,ymult*(cy));
    glVertex2f(cx+xvel,ymult*(cy+yvel));
    glEnd();
	
    glBegin(GL_POINTS);
    glVertex2f(cx,ymult*cy);
    glEnd();

}

void DebugWindow::DrawElements(){
    if (map == NULL)
        return;

    const vector<MapElement>& data = map->data;
    const objectData* newData = map->newData;
    const vector<MapElement>& rndf = map->prior.fulldata;

    for (int i=0; i< (int)data.size(); ++i){
        DrawElement(data[i]);
    }
/*
    for (int i=0; i< (int)newData.size(); ++i){
        DrawElement(newData[i].mergedMapElement);
    }
*/
    for (int i=0; i< (int)rndf.size(); ++i){
        MapElement el;
        map->prior.getElFull(el, i);
        DrawElement(el);
        map->prior.getEl(el, i);
        el.plotColor = MAP_COLOR_DARK_YELLOW;
        DrawElement(el);
    }

    if (selected >=0) {
        DrawElement(data[selected], true);
    }

    if (selectedRndf >=0){
        MapElement el;
        map->prior.getElFull(el, selectedRndf);
        DrawElement(el, true);
    }
    if (drawStateFlag){
        DrawVehicle(latestState);
    }
  
  
}
	
void DebugWindow::DrawShape() {
       
    // Draw shape
    glColor3f(1.0, 1.0, 1.0);
      
    glBegin(GL_LINE_LOOP);
    glVertex2f(0.0, 0.0);
    glVertex2f(.20, .30);
    glVertex2f(.30, .20);
    glVertex2f(0.0, 0.0);
    glVertex2f(20, 30);
    glVertex2f(30, 20);
		
    glEnd();
			
        
}


void DebugWindow::select_element(double x, double y)
{
    if (map == NULL)
        return;

#if 1
    point2 pt(x,y);

    vector<MapElement>& data = map->data;
    vector<MapElement>& rndf = map->prior.fulldata;

    float mindist = 10*scale_to_mult(scale);
    float mindist2 = mindist * mindist;
    int minidx = -1;
    for (unsigned int i = 0; i < data.size(); i++)
    {
        MapElement& el = data[i];
        if (el.geometry.size() == 0)
            continue;
        point2 project = point2arr(el.geometry).project(pt);
        float dist2 = (project - pt).norm2();

        if (dist2 < mindist2)
        {
            mindist2 = dist2;
            minidx = i;
            selectedpt = project;
        }
    }

    bool rndfSelected = false;
    pt = pt + map->prior.delta;

    for (unsigned int i = 0; i < rndf.size(); i++)
    {
        MapElement& el = rndf[i];
        if (el.geometry.size() == 0)
            continue;
        point2 project = point2arr(el.geometry).project(pt);
        float dist2 = (project - pt).norm2();

        if (dist2 < mindist2)
        {
            rndfSelected = true;
            mindist2 = dist2;
            minidx = i;
            selectedpt = project - map->prior.delta;
        }
    }
    
    if (rndfSelected)
    {
        selected = -1;
        selectedRndf = minidx;
        cout << map->prior.fulldata[selectedRndf] << endl;
    } else if (minidx >= 0)
    {
        selected = minidx;
        selectedRndf = -1;
        cout << map->data[selected] << endl;
    } else
    {
        selected = -1;
        selectedRndf = -1;
    }

#else

    vector<MapElement>& localmap = map->data;

    //  double dx, dy, orient, length, width;
    //  double newdx, newdy;
    vector<int> tmpselected;
    point2arr tmpptselected;
    //  double minboundthresh = 2; 
    MapElement el;
    point2arr ptarr;
    point2 pt(x,y);
    point2 projectedpt;
    for (int i = 0 ; i < (int)localmap.size() ; ++i){
        el = localmap[i];
        if (el.isOverlap(pt)){
            tmpselected.push_back(i);
            tmpptselected.push_back(pt);
            continue;
        }
    
        if (el.geometry.size() <= 0)
            continue;
        ptarr = el.geometry;

        projectedpt = ptarr.project(pt);
   
        if (pt.dist(projectedpt)< 2*scale_to_mult(scale)){
            tmpselected.push_back(i);
            tmpptselected.push_back(projectedpt);
        }
    }

    if (tmpselected == selectedarr){
        if (selectedarr.size()){
            selectedarr_index++;
            if (selectedarr_index>=(int)selectedarr.size())
                selectedarr_index = -1;
        }
    }
    else{
        selectedarr = tmpselected;
        selectedptarr = tmpptselected;
        selectedarr_index = 0;
    }
    if (selectedarr.size() && selectedarr_index>=0){
        selected = selectedarr[selectedarr_index];
        selectedpt = selectedptarr[selectedarr_index];
    }  else{
        selected = -1;
    }
    if (selected >=0){
        cout << localmap[selected] << endl;
    
    }

#endif

    reshape();
    redraw();
}

void DebugWindow::set_color(MapElementColorType color, int value)
{ 
    double val = (double)value/100;
  

    if (val< 0)
        val=0;
    if (val>1)
        val=1; 
    switch (color){
    case MAP_COLOR_GREY :
        glColor3f(val, val, val);
        break;

    case MAP_COLOR_BLUE :
        glColor3f(0.0, 0.0, val);
        break;

    case MAP_COLOR_LIGHT_BLUE :
        glColor3f(val/2, val/2, val);
        break;

    case MAP_COLOR_BLUE_2 :
        glColor3f(val/4, val/4, val);
        break;

    case MAP_COLOR_RED :
        glColor3f(val, 0.0, 0.0);
        break;
    case MAP_COLOR_GREEN :
        glColor3f(0.0, val, 0.0);
        break;
    case MAP_COLOR_DARK_GREEN :
        glColor3f(0.0, val*.7, 0.0);
        break;
    case MAP_COLOR_MAGENTA : 
        glColor3f(val, 0.0, val);
        break;
    case MAP_COLOR_CYAN :
        glColor3f(0.0, val, val);
        break;
    case MAP_COLOR_YELLOW :
        glColor3f(val, val, 0.0);
        break;
    case MAP_COLOR_DARK_YELLOW :
        glColor3f(val*.5, val*.5, 0.0);
        break;
    case MAP_COLOR_ORANGE :
        glColor3f(val, val/1.5, 0.0);
        break;
    case MAP_COLOR_PINK :
        glColor3f(val, val/1.5, val);
        break;
    case MAP_COLOR_PURPLE :
        glColor3f(val/1.5, val/4, val/1.5);
        break;


    default :
        glColor3f(val, val, val);

    }

}

double DebugWindow::scale_to_mult(double s){
	
    return (pow(2,s-3));
    //	if (s>=0)
    //		return(.1*(1+s));
    //	else
    //		return(.1/(1-s));
}

int DebugWindow::set_view_screen_delta(double dx, double dy){
    center_x = center_x + scale_to_mult(scale)*(dx);
    center_y = center_y - (scale_to_mult(scale)*(dy));
    reshape();
    redraw();
    return 0;
}

int DebugWindow::set_view(double x, double y){
  
    center_x = x;
    center_y = y;
    reshape();
    redraw();
    return 0;
}
int DebugWindow::set_scale_delta(double ds, double cx, double cy){

    double scalerat = scale_to_mult(scale)-scale_to_mult(scale+ds);
    double dx = scalerat*(cx-0.5*w())/scale_to_mult(scale+ds);
    double dy = scalerat*(cy-0.5*h())/scale_to_mult(scale+ds);
	
    scale  = scale+ds;
    return(set_view_screen_delta(dx, dy));
}
int DebugWindow::set_scale(double s){
    scale = s;
    reshape();
    redraw();
    return 0;
}

int DebugWindow::handle(int event) {
    switch(event) {
    case FL_PUSH:
        mouse_button = Fl::event_button();
        mouse_x = Fl::event_x();
        mouse_y = Fl::event_y();
        mouse_x_press = Fl::event_x();
        mouse_y_press = Fl::event_y();
        //if (mouse_button==2){
        //  set_view_screen_delta(Fl::event_x()-0.5*w(),Fl::event_y()-0.5*h());
        //}
        mouse_local_x = center_x + scale_to_mult(scale)*(mouse_x-0.5*w());
        mouse_local_y = center_y - scale_to_mult(scale)*(mouse_y-0.5*h());; 
		
        //		cout << "mlx = " << mouse_local_x << " mly = " << mouse_local_y <<endl;

        if (mouse_button==2){
            select_element(mouse_local_x, mouse_local_y);
		
        }

        reshape();
        redraw();
        return 1;

		
    case FL_DRAG:
				
        if (mouse_button == 1){
            set_view_screen_delta(mouse_x-Fl::event_x(),mouse_y-Fl::event_y());
        }
        if (mouse_button == 3){
				
            double dscale_x = (mouse_x-Fl::event_x())/20;
            double dscale_y = (-mouse_y+Fl::event_y())/20;
				
            if (fabs(dscale_x) > fabs(dscale_y))
                set_scale_delta(dscale_x,mouse_x_press,mouse_y_press);
            else
                set_scale_delta(dscale_y,mouse_x_press,mouse_y_press);
				
        }
        mouse_x = Fl::event_x();
        mouse_y = Fl::event_y();
	

        return 1;
    case FL_MOUSEWHEEL:
			
        if (Fl::event_key()==65261){
            set_scale(scale+.5);
        }
        else if (Fl::event_key()==65260){
            set_scale(scale-.5);
        }
        return 1;
			
    case FL_RELEASE:

		
        if ((mouse_x_press == Fl::event_x()) 
            && (mouse_y_press == Fl::event_y())){
				
            if (mouse_button==1){
                select_element(mouse_local_x, ymult*(mouse_local_y));

            }

            reshape();
            redraw();
        }
   
        return 1;
    case FL_FOCUS :
    case FL_UNFOCUS :
		
        return 1;
    case FL_KEYBOARD:
        switch(Fl::event_key()) {
        case 'q':
            exit(1);
            break;
        case 'r':
            reset_map();
            return 1;
        case 'z':
            set_scale(0);
            return 1;
        case 'c':
            set_view(0,0);
            return 1;
        case 'a':
            if (centerOnAlice) centerOnAlice = false;
            else centerOnAlice = true;
            reshape();
            redraw();
            return 1;
        case 'd':
            if (drawStateFlag) drawStateFlag = false;
            else drawStateFlag = true;
            return 1;
        case 's':
            mouse_local_x = center_x;
            mouse_local_y = center_y;
            select_element(center_x,ymult*(center_y));
            return 1;
        case 't':
            if (showTrav) showTrav = false;
            else showTrav = true;
            redraw();
            return 1;
        case 65365: // pgup, zoom in
            set_scale(scale-0.1);
            return 1;
        case 65366: // pgdn, zoom out
            set_scale(scale+0.1);
            return 1;
        case 65364: // down arrow, scroll down
            set_view_screen_delta(0,10);
            return 1;
        case 65362: // up arrow, scroll up
            set_view_screen_delta(0,-10);
            return 1;
        case 65361: // left arrow, scroll left
            set_view_screen_delta(-10,0);
            return 1;
        case 65363: // right arrow, scroll right
            set_view_screen_delta(10,0);
            return 1;
        default:
            return 1;
        }
        return 1;
    case FL_SHORTCUT:
        return 1;
    default:
        // pass other events to the base class...
        return Fl_Gl_Window::handle(event);
    }
}

void DebugWindow::DrawDebugData()
{
    if (map == NULL)
        return;
    LineFusionDebugData* d = map->lineFusDebug;
    if (d == NULL)
        return;

    assert((d->assocLines.size() & 1) == 0); // must have an even number of points

    glColor3f(1.0, 0.0, 1.0); // pink!!!

    // think about enable transparency and blending to show to
    // lines that overlap exactly

    glBegin(GL_LINES);
    for (int i = 0; i < int(d->assocLines.size()); i++)
        glVertex2f(d->assocLines[i].x, ymult*d->assocLines[i].y);
    glEnd();
}

void DebugWindow::updateState(const VehicleState& state)
{
    const uint64_t BIG_INTERVAL = 10000000UL; // 10sec

    // try to be smart, if the timestamp is a lot smaller than the current,
    // maybe someone just restarted a log from the beginning
    if (state.timestamp > latestState.timestamp ||
        (latestState.timestamp - state.timestamp) > BIG_INTERVAL)
    {
        if (latestState.timestamp > 0)
        {
            point2 delta(state.localX - latestState.localX,
                         state.localY - latestState.localY);
            stateDelta = stateDelta + delta;
        } else {
            stateDelta = point2(state.localX, state.localY);
        }
        latestState = state;
    }
}
