#include <iostream>
#include <algorithm>
#include <vector>
#include <cassert>
#include <cstring>

#include <FL/Fl.H>
#include <FL/Fl_Gl_Window.H>
#include <FL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <dgcutils/DGCutils.hh>

#include "MapElementTalker.hh"
#include "DebugWindow.hh"

#define LINE_FUSION_IN_MAP 0
#define LINE_FUSION_PERIOD 500000 //100000 // 100ms
#define STATISTICS_PERIOD 1000000 // 1 sec

class LineFusTest
{
    Map map;
    Fl_Window* mainwin;
    DebugWindow* win;

    CMapElementTalker talker;

    int skynetKey, recvSubGroup;

    uint64_t lineFusTime;

    uint64_t statsTime;
    uint64_t lineFusElapsed;
    int lineFusCount;
    VehicleState lastState;

public:

    LineFusTest(int _skynetKey, int _recvSubGroup, string rndf)
        : map(true, LINE_FUSION_IN_MAP),
          skynetKey(_skynetKey), recvSubGroup(_recvSubGroup),
          lineFusTime(0), statsTime(0), lineFusElapsed(0), lineFusCount(0)
    {
        map.lineFusDebug = new LineFusionDebugData(); // we want debugging info
        map.prior.lineFusDebug = map.lineFusDebug;
        //--------------------------------------------------
        // initialize the map prior data
        //--------------------------------------------------
        if (!map.loadRNDF(rndf.c_str())){
            cerr << "Error:  Unable to load RNDF file to map " << rndf
                 << ", exiting program" << endl;
            exit(1);
        }

        mainwin = new Fl_Window(700, 700, "Line fusion debug win");
        win = new DebugWindow(10, 10, mainwin->w()-20, mainwin->h()-20);
        win->setMap(&map);
        assert(mainwin);
        assert(win);
        mainwin->resizable(win);
        talker.initRecvMapElement(skynetKey, recvSubGroup);
        talker.initSendMapElement(skynetKey);

        cerr << "Number of segments: " << map.prior.segments.size() << endl;
        cerr << "Number of lanes: " << map.prior.lanes.size() << endl;
        cerr << "Number of prior map elements: " << map.prior.data.size() << endl;
        unsigned long count1 = 0;
        unsigned long count2 = 0;
        for (int i = 0; i < int(map.prior.data.size()); i++)
        {
            count1 += map.prior.data[i].geometry.size();
            count2 += map.prior.fulldata[i].geometry.size();
        }
        cerr << "Total number of nodes in RNDF: " << count1 << endl;
        cerr << "Total number of nodes in fulldata: " << count2 << endl;
        cerr << "Total number of nodes: " << count1 + count2 << endl;
        cerr << "Total number bytes for the nodes: "
             << sizeof(point2_uncertain)*(count1 + count2) << endl;
    }

    ~LineFusTest()
    {
        delete win;
    }

    void loop();

    int run();

    void setLocalFrameOffset(const VehicleState& state)
    {
        map.setLocalToGlobalOffset(state);
    }
};

void LineFusTest::loop()
{
  int bytesRecv = 0;
  int numEl = 0;
  const int MAX_NUM_EL = 10;
  MapElement recvEl,sendEl;
  
  bool recvflag = false;

  do {
      bytesRecv = talker.recvMapElementNoBlock(&recvEl, recvSubGroup);

      if (bytesRecv > 0 /* && !ispaused */)
      {
          if (recvEl.state.timestamp > 0) {
              setLocalFrameOffset(recvEl.state);
              lastState = recvEl.state; // don't care if timestamp is less than before
          }
          if (recvEl.type == ELEMENT_ALICE)
              win->updateState(recvEl.state);
          else
              map.addEl(recvEl);
          recvflag = true;
          win->redraw();
          numEl++;
      } else {
          recvflag = false;
      }

  } while(recvflag && numEl < MAX_NUM_EL);

#if !LINE_FUSION_IN_MAP
  if (DGCgettime() - statsTime >= STATISTICS_PERIOD && lineFusCount > 0)
  {
      statsTime = DGCgettime();
      cerr << "Line fusion time (avg): "
           << double(lineFusElapsed)*1e-3/lineFusCount
           << " ms" << endl;
      lineFusElapsed = 0;
      lineFusCount = 0;
  }

  if (DGCgettime() - lineFusTime >= LINE_FUSION_PERIOD)
  {
      lineFusTime = DGCgettime();
      map.fuseAllLines(lastState);
      lineFusElapsed += DGCgettime() - lineFusTime;
      lineFusCount++;
      win->redraw();
      
/*
      // REMOVE ME: this is gonna be sloooow ....
      for (unsigned int i = 0; i < map.prior.segments.size(); i++)
      {
          RNDFsegment& seg = map.prior.segments[i];
          for (int j = 0; j < int(seg.laneindex.size()) ; j++)
              map.prior.postProcessLane(i, j, vector<bool>());
      }
*/
      
  } else
#endif
  {
      if (recvflag == false)
          usleep(0);
  }

}


void main_idle(LineFusTest *self)
{
    self->loop();
}

int LineFusTest::run()
{
    Fl::add_idle((void (*) (void*)) main_idle, this);
    mainwin->show();
    return Fl::run();
}

int main(int argc, char** argv)
{
    int skynetKey = -1, recvSubGroup = 0;
    string rndf = "rndf.txt";

    // simple cmdline parser
    for (int i = 1; i < argc; i++)
    {
        char* endp;
        if (strncmp(argv[i], "--skynet-key=", 16) == 0)
        {
            skynetKey = strtol(argv[i] + 14, &endp, 0);
            if (*endp != '\0')
            {
                cerr << "Invalid skynet key " << argv[i]+14 << endl;
                return 1;
            }
        }
        else if(strncmp(argv[i], "--recv-subgroup=", 16) == 0)
        {
            recvSubGroup = strtol(argv[i] + 16, &endp, 0);
            if (*endp != '\0')
            {
                cerr << "Invalid recv subgroup " << argv[i]+16 << endl;
                return 1;
            }
        }
        else if(strncmp(argv[i], "--rndf=", 7) == 0)
        {
            rndf = string(argv[i]+7);
        }
    }

    if (skynetKey == -1)
    {
        char* str = getenv("SKYNET_KEY");
        char* endp;
        skynetKey = strtol(str, &endp, 0);
        if (*endp != '\0')
        {
            cerr << "Invalid env variable SKYNET_KEY = " << str << endl;
            return 2;
        }
    }

    glutInit(&argc, argv);
    LineFusTest app(skynetKey, recvSubGroup, rndf);

    return app.run();
}

