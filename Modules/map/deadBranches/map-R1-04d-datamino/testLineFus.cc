#include <iostream>
#include <algorithm>
#include <vector>
#include <cassert>
#include <cstring>

#include <FL/Fl.H>
#include <FL/Fl_Gl_Window.H>
#include <FL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include "MapElementTalker.hh"
#include "DebugWindow.hh"


class LineFusTest
{
    Map map;
    Fl_Window* mainwin;
    DebugWindow* win;

    CMapElementTalker talker;

    int skynetKey, recvSubGroup;

public:

    LineFusTest(int _skynetKey, int _recvSubGroup, string rndf)
        : map(true), // line fusion enabled
          skynetKey(_skynetKey), recvSubGroup(_recvSubGroup)
    {
        //--------------------------------------------------
        // initialize the map prior data
        //--------------------------------------------------
        if (!map.loadRNDF(rndf.c_str())){
            cerr << "Error:  Unable to load RNDF file to map " << rndf
                 << ", exiting program" << endl;
            exit(1);
        }

        mainwin = new Fl_Window(700, 700, "Line fusion debug win");
        win = new DebugWindow(10, 10, mainwin->w()-20, mainwin->h()-20);
        win->setMap(&map);
        assert(mainwin);
        assert(win);
        mainwin->resizable(win);
        talker.initRecvMapElement(skynetKey, recvSubGroup);
        talker.initSendMapElement(skynetKey);
    }

    ~LineFusTest()
    {
        delete win;
    }

    void loop();

    int run();

    void setLocalFrameOffset(VehicleState state)
    {
	point2 statedelta,stateNE, statelocal;
	statelocal.set(state.localX,state.localY);
	stateNE.set(state.utmNorthing,state.utmEasting);
	statedelta = stateNE-statelocal;
        map.setTransform(statedelta);
    }
};

void LineFusTest::loop()
{
  int bytesRecv = 0;
  int numEl = 0;
  const int MAX_NUM_EL = 10;
  MapElement recvEl,sendEl;
  
  bool recvflag = false;

  do {
      bytesRecv = talker.recvMapElementNoBlock(&recvEl, recvSubGroup);

      if (bytesRecv > 0 /* && !ispaused */)
      {
          if (recvEl.state.timestamp > 0)
              setLocalFrameOffset(recvEl.state);
          map.addEl(recvEl);
          recvflag = true;
          win->redraw();
          numEl++;
      } else {
          recvflag = false;
          usleep(0);
      }

  } while(recvflag && numEl < MAX_NUM_EL);
}


void main_idle(LineFusTest *self)
{
    self->loop();
}

int LineFusTest::run()
{
    Fl::add_idle((void (*) (void*)) main_idle, this);
    mainwin->show();
    return Fl::run();
}

int main(int argc, char** argv)
{
    int skynetKey = -1, recvSubGroup = 0;
    string rndf = "rndf.txt";

    // simple cmdline parser
    for (int i = 1; i < argc; i++)
    {
        char* endp;
        if (strncmp(argv[i], "--skynet-key=", 16) == 0)
        {
            skynetKey = strtol(argv[i] + 14, &endp, 0);
            if (*endp != '\0')
            {
                cerr << "Invalid skynet key " << argv[i]+14 << endl;
                return 1;
            }
        }
        else if(strncmp(argv[i], "--recv-subgroup=", 16) == 0)
        {
            recvSubGroup = strtol(argv[i] + 16, &endp, 0);
            if (*endp != '\0')
            {
                cerr << "Invalid recv subgroup " << argv[i]+16 << endl;
                return 1;
            }
        }
        else if(strncmp(argv[i], "--rndf=", 7) == 0)
        {
            rndf = string(argv[i]+7);
        }
    }

    if (skynetKey == -1)
    {
        char* str = getenv("SKYNET_KEY");
        char* endp;
        skynetKey = strtol(str, &endp, 0);
        if (*endp != '\0')
        {
            cerr << "Invalid env variable SKYNET_KEY = " << str << endl;
            return 2;
        }
    }

    glutInit(&argc, argv);
    LineFusTest app(skynetKey, recvSubGroup, rndf);

    return app.run();
}

