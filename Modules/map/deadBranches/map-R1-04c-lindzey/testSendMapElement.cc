/**********************************************************
 **
 **  TESTMAPELEMENTTALKER.CC
 **
 **    Time-stamp: <2007-09-10 14:20:11 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Feb  7 13:39:57 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include <iostream>
#include "MapElementTalker.hh"
#include "MapElement.hh"
#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"

using namespace std;     



int main(int argc, char **argv)
{
  int skynetKey = 0;
  int subgroup = 0;
	int testnum = 0; 
  int thistest=0;    
  bool testflag =0;
  string  testname; 

  skynetKey = atoi(getenv("SKYNET_KEY"));  

  cout << endl << "Usage :" << endl;
  cout << " testSendMapElement [testnum] [subgroup] [skynet key]" << endl << endl;

	if(argc >1)
    testnum = atoi(argv[1]);
  if(argc >2)
    subgroup = atoi(argv[2]);
  if(argc >3)
    skynetKey = atoi(argv[3]);
  
  cout << "Test Number = " << testnum << endl<< " Skynet Key = " << skynetKey << 
    "  Subgroup = "  << subgroup << endl;
	
  int bytesSent = 0;
	
 
  CMapElementTalker testtalker;
  testtalker.initSendMapElement(skynetKey);
  MapElement el;


	cout <<"========================================" << endl;
	testname = "Testing flood send";
	thistest++;
	testflag = (thistest==testnum || 
							(testnum<0 &&-thistest<=testnum));
	cout <<"Test #" << thistest << "  " << testname;
	if (!testflag)
		cout << "  NOT RUN" << endl;
	else{
		cout << "  RUNNING" << endl;
		int numsend,numpts;
		point2arr ptarr;
		point2 pt;
		uint64_t timestart;
		uint64_t dtime;
    int sleeptime;
		
		cout << "====================================" << endl;
		cout << "Enter number of points per element" << endl;
		cin>>numpts;			
		cout << "Enter forced usleep delay in sending loop (0 for no usleep call)" << endl;
		cin>>sleeptime;	
		while (1){
      cout << "Enter number of elements to send (num points = " << numpts << " delay = " << sleeptime <<")"  << endl;
		cin>>numsend;		 

			ptarr.clear();
			el.clear();
			for (int i=0 ; i<numpts;++i){
				ptarr.push_back(pt);
			}
			el.setGeometry(ptarr);
			timestart = DGCgettime();
			for (int i=0 ; i<numsend;++i){
				el.setGeometry(ptarr);			
				el.timestamp = DGCgettime();
				el.state.timestamp = el.timestamp;
				bytesSent =testtalker.sendMapElement(&el);
        if (sleeptime>0)
          usleep(sleeptime);

			}
			dtime = DGCgettime()-timestart;
			cout << "Sent " <<numsend << " elements of size " << bytesSent << " in " << dtime << " avg "  << dtime/numsend << endl;
	

		}

	}


	cout <<"========================================" << endl;
	testname = "Testing send various obstacle types";
	thistest++;
	testflag = (thistest==testnum || 
							(testnum<0 &&-thistest<=testnum));
	cout <<"Test #" << thistest << "  " << testname;
	if (!testflag)
		cout << "  NOT RUN" << endl;
	else{
		cout << "  RUNNING" << endl;
		int id_input;
		point2 cpoint(14,25);
 
  
		vector<point2> ptarr;
		ptarr.push_back(cpoint+point2(-3,3));
		ptarr.push_back(cpoint+point2(-2,0));
		ptarr.push_back(cpoint+point2(-1,-2));
		ptarr.push_back(cpoint+point2(1,-1));
		ptarr.push_back(cpoint+point2(3,3));
  
		double angle = .2;
		double radius = 3;
		double length = 4;
		double width = 2;
  
		/////////////////////////////////////
		cout << "====================================" << endl;
		cout << "Sending obstacle, radius bound" << endl;
		cout << "Enter id number and hit enter to send" << endl;
		cin>>id_input;
  
		el.clear(); 
		el.setId(id_input);
		el.setTypeObstacle();
		el.setGeometry(cpoint,radius);

		bytesSent = testtalker.sendMapElement(&el,subgroup);
		cout << "sending el :" << el<< endl;
		cout << "bytes sent = " << bytesSent << endl;
 
		///////////////////////////////////// 
		cout << "====================================" << endl;
		cout << "Sending obstacle, block bound" << endl;
		cout << "Enter id number and hit enter to send" << endl;
		cin>>id_input;
		el.clear();
		el.setId(id_input);
		el.setTypeObstacle();
		el.setGeometry(cpoint,length,width,angle);
		bytesSent = testtalker.sendMapElement(&el,subgroup);
		cout << "sending el :" << el<< endl;
		cout << "bytes sent = " << bytesSent << endl;

		/////////////////////////////////////
		cout << "====================================" << endl;
		cout << "Sending obstacle, polygon bound" << endl;
		cout << "Enter id number and hit enter to send" << endl;
		cin>>id_input;
		el.clear();
		el.setId(id_input);
		el.setTypeObstacle();
		el.setGeometry(ptarr);
		bytesSent = testtalker.sendMapElement(&el,subgroup);
		cout << "sending el :" << el<< endl;
		cout << "bytes sent = " << bytesSent << endl;

		/////////////////////////////////////
		cout << "====================================" << endl;
		cout << "Sending generic line" << endl;
		cout << "Enter id number and hit enter to send" << endl;
		cin>>id_input;
		el.clear();
		el.setId(id_input);
		el.setTypeLine();  
		el.setGeometry(ptarr); 
		bytesSent = testtalker.sendMapElement(&el,subgroup);
		cout << "sending el :" << el<< endl;
		cout << "bytes sent = " << bytesSent << endl;

		/////////////////////////////////////
		cout << "====================================" << endl;
		cout << "Sending laneline" << endl;
		cout << "Enter id number and hit enter to send" << endl;
		cin>>id_input;
		el.clear();
		el.setId(id_input);
		el.setTypeLaneLine();
		el.setGeometry(ptarr);
		bytesSent = testtalker.sendMapElement(&el,subgroup);
		cout << "sending el :" << el<< endl;
		cout << "bytes sent = " << bytesSent << endl;


		/////////////////////////////////////
		cout << "====================================" << endl;
		cout << "Sending stop line" << endl;
		cout << "Enter id number and hit enter to send" << endl;
		cin>>id_input;
		el.clear();
		el.setId(id_input);
		el.setTypeStopLine();
		el.setGeometry(ptarr);
		bytesSent = testtalker.sendMapElement(&el,subgroup);
		cout << "sending el :" << el<< endl;
		cout << "bytes sent = " << bytesSent << endl;

		/////////////////////////////////////
		cout << "====================================" << endl;
		cout << "Sending points" << endl;
		cout << "Enter id number and hit enter to send" << endl;
		cin>>id_input;
		el.clear();
		el.setId(id_input);
		el.setTypePoints();
		el.setGeometry(ptarr);
		bytesSent = testtalker.sendMapElement(&el,subgroup);
		cout << "sending el :" << el<< endl;
		cout << "bytes sent = " << bytesSent << endl;

 

		/////////////////////////////////////
		cout << "====================================" << endl;
		cout << "Sending Alice" << endl;
		cout << "Enter id number and hit enter to send" << endl;
		cin>>id_input;
		VehicleState state;
		memset(&state, 0, sizeof(state));
		state.localX = 10;
		state.localY = 15;
		state.localYaw = 1;
  

		el.clear();
		el.setId(id_input);
		el.setTypeAlice();
		el.setState(state);
		bytesSent = testtalker.sendMapElement(&el,subgroup);
		cout << "sending el :" << el<< endl;
		cout << "bytes sent = " << bytesSent << endl;

		/////////////////////////////////////
		cout << "====================================" << endl;
		cout << "Sending clear" << endl;
		cout << "Enter id number and hit enter to send" << endl;
		cin>>id_input;
		el.clear();
		el.setId(id_input);
		el.setTypeClear();
		bytesSent = testtalker.sendMapElement(&el,subgroup);
		cout << "sending el :" << el<< endl;
		cout << "bytes sent = " << bytesSent << endl;

          
	}
 
 
  if (0){ 
    //--------------------------------------------------
    // starttemplate for new test
    //--------------------------------------------------
   
    cout <<"========================================" << endl;
    testname = "Testing ";
    thistest++;
    testflag = (thistest==testnum || 
                (testnum<0 &&-thistest<=testnum));
    cout <<"Test #" << thistest << "  " << testname;
    if (!testflag)
      cout << "  NOT RUN" << endl;
    else{
      cout << "  RUNNING" << endl;
          
    }

    //--------------------------------------------------
    // end template for new test
    //--------------------------------------------------   
  }
 
 
 
  cout <<"========================================" 
       << endl << endl;
  return(0);
}

 
  
 
