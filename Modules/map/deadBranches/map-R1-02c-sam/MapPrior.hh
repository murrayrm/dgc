/**********************************************************
 **
 **  MAPPRIOR.HH
 **
 **    Time-stamp: <2007-05-17 15:00:34 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Mon Feb 19 12:51:26 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef MAPPRIOR_H
#define MAPPRIOR_H

#include <math.h>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>

#include "dgcutils/ggis.h"

#include "MapElement.hh"
#include "frames/point2_uncertain.hh"

class LaneLabel;

class PointLabel
{
public:
  PointLabel() {}
  PointLabel(int s, int l, int p){segment = s; lane = l; point = p;}
  ~PointLabel() {}

  bool operator==(const PointLabel &label) const{ return (segment==label.segment && lane==label.lane && point==label.point);}
  bool operator==(const LaneLabel &label) const;

  friend ostream &operator<<(ostream &os, const PointLabel &label);
  friend ostream &operator<<(ostream &os, const vector<PointLabel> &label);
  void print()  { cout << segment <<" "<< lane <<" " << point << endl; }
  int segment, lane, point;
};




class LaneLabel
{
public:
  LaneLabel() {}
  LaneLabel(int s, int l) {segment = s; lane = l;}
  ~LaneLabel() {}

  bool operator==(const LaneLabel &label) const { return (segment==label.segment && lane==label.lane);}
  bool operator==(const PointLabel &label) const;
  friend ostream &operator<<(ostream &os, const LaneLabel &label);
  friend ostream &operator<<(ostream &os, const vector<LaneLabel> &label);


  void print() { cout << segment <<" "<< lane <<endl; }  
  int segment, lane;
};

class SpotLabel
{
public:
  SpotLabel() {}
  SpotLabel(int z, int s) {zone = z; spot = s;}
  ~SpotLabel() {}

  bool operator==(const SpotLabel &label) { return (zone==label.zone && spot==label.spot);}
  void print() { cout << zone <<" "<< spot <<endl; }  
  int zone, spot;
};



class RNDFsegment
{
public:
  RNDFsegment() {}
  RNDFsegment(int val) {label=val;}

  ~RNDFsegment() {}
  int label;
  string name;
  vector<int> laneindex;
  vector<LaneLabel> lane_label;
  vector<int> lanedirection;
};

class RNDFlane
{
public:
  RNDFlane() {}
  RNDFlane(LaneLabel& val) {label=val;}
  ~RNDFlane() {}
  LaneLabel label;
  string name;
  double width;
  int direction;

  int waypoint_elindex;
  int leftbound_elindex;
  int rightbound_elindex;
  vector<PointLabel> waypoint_label;
  vector<int> datindex;
  
  

  string leftbound_type;
  string rightbound_type;
  
  LaneLabel leftneighbor_label;
  LaneLabel rightneighbor_label;
  
  //vector<int> exit_ptindex;
  vector<PointLabel> exit_label;
  vector<PointLabel> exit_link;
  vector<int> exit_datindex;


  // vector<int> entry_ptindex;
  vector<PointLabel> entry_label;
  vector<PointLabel> entry_link;
  vector<int> entry_datindex;
  // vector<int> entry_intindex;

  //  vector<int> stop_ptindex;
  vector<PointLabel> stop_label;
  vector<int> stop_datindex;

  //vector<int> check_ptindex;
  vector<PointLabel> checkpt_label;
  vector<int> checkpt_num;

};


class RNDFzone
{
public:
  RNDFzone() {}
  RNDFzone(int val) {label=val;}
  ~RNDFzone() {}
  int label;  
  string name;
  int perimeter_elindex;
  vector<PointLabel> perimeter_label;

  //  SpotLabel perimeter_label;

  vector<int> spotindex;
  vector<SpotLabel> spot_label;


  //vector<int> exit_ptindex;
  vector<PointLabel> exit_link;
  vector<PointLabel> exit_label;
  //  vector<int> exit_intindex;

  // vector<int> entry_ptindex;
  vector<PointLabel> entry_label;
  vector<PointLabel> entry_link;
  // vector<int> entry_intindex;

  
};

class RNDFspot
{
public:
  RNDFspot() {}
  RNDFspot(SpotLabel& val) {label=val;}
  ~RNDFspot() {}
  SpotLabel label;
  string name;
  
  double width;
  
  int waypt_elindex;
  vector<PointLabel> waypoint_label;

  int leftbound_elindex;
  int rightbound_elindex;
  
  vector<int> check_ptindex;
  vector<PointLabel> checkpt_label;
  vector<int> checkpt_num;

};


//! CLASS DESCRIPTION
/*! CLASS DETAILS */
class MapPrior
{
  
public:
  
  //! A Constructor 
  /*! DETAILS */
  MapPrior();
  
  //! A Destructor 
  /*! DETAILS */
  ~MapPrior();

  vector<MapElement> data;
  vector<MapElement> initdata;

  //these are all the same size
  vector<point2arr_uncertain> leftbound_data;
  vector<point2arr_uncertain> rightbound_data;
  vector<point2arr_uncertain> center_data;

  vector<point2arr_uncertain> stop_data;



  bool loadRNDF(string filename);
  bool parseRNDF(vector<string>& strfile);
  //  bool parseHeader(vector<string>& strfile);
  //  bool parseSegments(vector<string>& strfile);
  //  bool parseZones(vector<string>& strfile);
  bool parsePointLabel(string&, PointLabel& label);
  bool parseLaneLabel(string&, LaneLabel& label);
  bool parseSpotLabel(string&, SpotLabel& label);
  bool parseDataInterpolation();

  bool getEl(MapElement & el, int index);


  vector<RNDFsegment> segments;
  vector<RNDFlane> lanes;
  vector<RNDFzone> zones;  
  vector<RNDFspot> spots;
  vector<PointLabel> checkpoints;
  
  int numSegments;
  int numZones;
  string RNDFname;
  string RNDFdate;
  double RNDFversion;


  //--------------------------------------------------
  // 
  //--------------------------------------------------
private:
  void toLocal(point2arr_uncertain & ptarrout, const point2arr_uncertain &ptarrin);

  int getLaneIndex(const LaneLabel &label);
  int getLaneIndex(const PointLabel &label);
  int getLaneIndex(const int segnum, const int lanenum);

  int getLanePointIndex(const PointLabel &label);
  int getLanePointIndex(const int segnum, const int lanenum, const int ptnum);
  
 int getZoneIndex(const LaneLabel &label);
  int getZoneIndex(const PointLabel &label);
  int getZoneIndex(const SpotLabel &label);
  int getZoneIndex(const int zonenum);

  int getZoneSpotIndex(const SpotLabel &label);
  int getZoneSpotIndex(const int zonenum, const int sponnum);
  int getZonePointIndex(const PointLabel &label);
  int getZonePointIndex(const int segnum, const int lanenum, const int ptnum);


  int getSegmentIndex(const LaneLabel &label);
  int getSegmentIndex(const PointLabel &label);
  int getSegmentIndex(const int segnum);

public:


  //general queries
  bool isEntryPoint(const PointLabel &ptlabelin);
  bool isExitPoint(const PointLabel &ptlabelin);
  bool isStopLine(const PointLabel &ptlabelin);
  bool isCheckPoint(const PointLabel &ptlabelin);

  bool isLaneSameDir(const LaneLabel &lanelabel1,const LaneLabel &lanelabel2);
  bool isPointInLane(const point2_uncertain &pt, const LaneLabel &lanelabelin);
  bool isPointInLane(const point2arr_uncertain &pt, const LaneLabel &lanelabelin);
 bool isPolyInLane(const point2arr_uncertain &pt, const LaneLabel &lanelabelin);
 bool isLineInLane(const point2arr_uncertain &pt, const LaneLabel &lanelabelin);
  bool isElementInLane(const MapElement &el, const LaneLabel &lanelabelin);

  int getLaneLeftBound(point2arr_uncertain &bound, 
                       const LaneLabel &lanelabelin);
  int getLaneRightBound(point2arr_uncertain &bound, 
                        const LaneLabel &lanelabelin);
  int getLaneBounds(point2arr_uncertain &leftbound, 
                    point2arr_uncertain &rightbound, 
                    const LaneLabel &lanelabelin);
  int getLaneBoundsPoly(point2arr_uncertain &bound, 
                        const LaneLabel &lanelabelin);
  int getLaneCenterLine(point2arr_uncertain &centerline, 
                       const LaneLabel &lanelabelin);
  int getLaneCenterLineFull(point2arr_uncertain &centerline,                                const LaneLabel &lanelabelin);

  
  int getWayPoint(point2_uncertain &pt, 
               const PointLabel &ptlabelin);
  int getWayPoint(point2arr_uncertain &ptarr, 
                  const vector<PointLabel> &ptlabelarrin);

  int getSameDirLaneBounds(point2arr_uncertain &leftbound, 
                           point2arr_uncertain &rightbound, 
                           const LaneLabel &lanelabelin);

  int getOppDirLaneBounds(point2arr_uncertain &leftbound, 
                          point2arr_uncertain &rightbound, 
                          const LaneLabel &lanelabelin);
 
  int getSegmentBounds(point2arr_uncertain &leftbound, 
                           point2arr_uncertain &rightbound, 
                           const LaneLabel &lanelabelin);

  


  //intersection queries
  int getWayPointEntries(vector<PointLabel>& ptlabelarr,
                          const PointLabel &ptlabelin);
  int getWayPointExits(vector<PointLabel>& ptlabelarr,
                         const PointLabel &ptlabelin);


  int getLaneExits(vector<PointLabel>& thisplabelarr,
                   vector<PointLabel>& otherlabelarr, 
                        const LaneLabel &lanelabelin);

  int getLaneEntries(vector<PointLabel>& thislabelarr,
                     vector<PointLabel>& otherlabelarr, 
                         const LaneLabel &lanelabelin);

  int getLaneStopLines(vector<PointLabel>& ptlabelarr,
                            const LaneLabel &lanelabelin);
  int getLaneCheckPoints(vector<PointLabel>& ptlabelarr,
                              const LaneLabel &lanelabelin);
  int getLaneCheckPointNumbers(vector<int>& ptnumarr,
                               const LaneLabel &lanelabelin);
  int getLaneName(string &name, 
                  const LaneLabel &lanelabelin);



  int getNeighborLane(LaneLabel& lanelabel, 
                           const LaneLabel &lanelabelin,
                           const int offset);

  int getSameDirLanes(vector<LaneLabel>& lanelabelarr, const LaneLabel &lanelabelin);

  int getOppDirLanes(vector<LaneLabel>& lanelabelarr, const LaneLabel &lanelabelin);

  int getAllDirLanes(vector<LaneLabel>& lanelabelarr, const LaneLabel &lanelabelin);




  



 
  int getSegmentLanes(vector<LaneLabel>& lanelabelarr, const int seglabelin);

  int getSegmentName(string &name, const int seglabelin);
  //  int getSegmentEntryFromLabels(vector<PointLabel>& ptlabelarr,
  //                             const int seglabelin);
 

  int getSegmentNumLanes(const PointLabel & pointlabelin);
  int getSegmentNumLanes(const LaneLabel & lanelabelin);
  int getSegmentNumLanes(const int seglabelin); 

 //   int getLaneLeftIndex(const LaneLabel &lanelabelin);
  //   int getLaneRightIndex(const LaneLabel &lanelabelin);
  //   int getLaneIndex(const LaneLabel &lanelabelin);
  //   int getPointIndex(const PointLabel &ptlabelin);
  //   int getLaneSameDirLeftIndex(const LaneLabel &lanelabelin);
  //   int getLaneSameDirRightIndex(const LaneLabel &lanelabelin);
  //   int getLaneOppDirLeftIndex(const LaneLabel &lanelabelin);
  //   int getLaneOppDirRightIndex(const LaneLabel &lanelabelin);
  //     int getLaneSegmentLeftIndex(const int seglabelin);
  //    int getLaneSegmentRightIndex(const int seglabelin);

  point2_uncertain delta; // global to local delta

};
#endif
