/**********************************************************
 **
 **  MAP.CC
 **
 **    Time-stamp: <2007-03-12 14:57:43 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Fri Feb 16 21:11:45 2007
 **
 **
 **********************************************************
 **
 ** Map Class  
 **
 **********************************************************/

#include "Map.hh"

using namespace std;
 
Map::Map()
{}

Map::~Map()
{}

void initLaneLines()
{
  
}

int Map::getSegmentID(point2 pt)
{
  
  cout << "in Map::getSegmentID : no matches found for point " << pt << endl;
  return -1;
}

int Map::getLaneID(point2 pt)
{
  cout << "in Map::getLaneID : no matches found for point " << pt << endl;
  return -1;
}


PointLabel Map::getNextPointID(point2 pt)
{
  unsigned i;
  double left_index;
  double right_index;
  int left_side;
  int right_side;
  int left_elindex,right_elindex;
  MapElement left_el;
  MapElement right_el;
  point2arr left_lane, right_lane;
  bool found=false;
  PointLabel label;
  int ptindex;
  
  for (i=0;i<prior.lanes.size();++i){
    left_elindex = prior.lanes[i].leftbound_elindex;
    right_elindex = prior.lanes[i].rightbound_elindex;
    prior.getEl(left_elindex,left_el);
    prior.getEl(right_elindex,right_el);
    left_lane=left_el.geometry;
    right_lane=right_el.geometry;
    left_lane.get_side(pt,left_side,left_index);
    right_lane.get_side(pt,right_side,right_index);
    if (left_side!=right_side){
      if (left_index>0 &&right_index>0 &&left_index<(double)left_lane.size() && right_index< (double) right_lane.size()){
    
        if (left_index>right_index)
          ptindex = ceil(left_index);
        else
          ptindex = ceil(right_index);
      
        label = prior.lanes[i].waypoint_label[ptindex];
        return label;
      }
    }
    
  }
  label = PointLabel(0,0,0);
  cout << "in Map::getSegmentID : no matches found for point " << pt << endl;
  return label;
}

int Map::getZoneID(point2 pt)
{
  return -1;
}


int Map::getLeftBound(point2arr& ptarr, LaneLabel &label )
{
  unsigned int i;
  MapElement el;
  int elindex =-1;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].leftbound_elindex;
      prior.getEl(elindex,el);
      ptarr = el.geometry;
    }
  }
  return 0;
}

int Map::getRightBound(point2arr& ptarr, LaneLabel &label )
{
  unsigned int i;
  MapElement el;
  int elindex =-1;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].rightbound_elindex;
      prior.getEl(elindex,el);
      ptarr = el.geometry;
    }
  }
  return 0;
}


int Map::getLeftBoundType(string& type, LaneLabel &label)
{
  unsigned int i;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label==label){
      type = prior.lanes[i].leftbound_type;
      return 0;
    }
  }
  return -1;
}
int Map::getRightBoundType(string& type, LaneLabel &label )
{
  unsigned int i;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      type = prior.lanes[i].rightbound_type;
      return 0;
    }
  }
  return -1;

}

bool Map::isExit(PointLabel &label){
  unsigned int i,j;
  int elindex =-1;
  int ptindex =-1;
  //bool isexit = false;
  //MapElement el;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      //elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].exit_label.size();++j){
        if (prior.lanes[i].exit_label[j]==label){
          return true;
        }
      }
    }
  }
  return false;
}

bool Map::isStop(PointLabel &label){
  unsigned int i,j;
  int elindex =-1;
  int ptindex =-1;
  bool isstop = false;
  //MapElement el;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      //elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].stop_label.size();++j){
        if (prior.lanes[i].stop_label[j]==label){
          return true;
        }
      }
    }
  }
  return false;
}

int Map::getStopline(point2& pt, PointLabel &label)
{
  unsigned int i,j;
  int elindex =-1;
  int ptindex =-1;
  bool isstop = false;
  MapElement el;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].stop_label.size();++j){
        if (prior.lanes[i].stop_label[j]==label){
          isstop = true;
        }
      }
      if (!isstop)
        continue;

      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==label){
          
          ptindex =j;
          prior.getEl(elindex,el);
          pt = el.geometry[ptindex];
          return 0;
        }
      }
    }
  }
  cerr << "in Map::getStopline, stop line not found" << label << endl;
  
  return -1;
}
int Map::getStopline(point2& pt, point2 state)
{
  unsigned int i,j;
  int elindex =-1;
  int ptindex =-1;
  bool isstop = false;
  MapElement el;

  PointLabel label, stoplabel;
  

  label = getNextPointID(state);
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].stop_label.size();++j){
        if (prior.lanes[i].stop_label[j].point>=label.point){
          stoplabel = prior.lanes[i].stop_label[j];
          break;
        }
      }

      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==stoplabel){
          
          ptindex =j;
          prior.getEl(elindex,el);
          pt = el.geometry[ptindex];
          return 0;
        }
      }
    }
  }
  cerr << "in Map::getStopline, stop line not found" << label << endl;
  
  return -1;
}

int Map::getNextStopline(point2& pt, PointLabel &label)
{
  unsigned int i,j;
  int elindex =-1;
  int ptindex =-1;
  bool isstop = false;
  MapElement el;

  PointLabel stoplabel;
  

  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].stop_label.size();++j){
        if (prior.lanes[i].stop_label[j].point>=label.point){
          stoplabel = prior.lanes[i].stop_label[j];
          break;
        }
      }

      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==stoplabel){
          
          ptindex =j;
          prior.getEl(elindex,el);
          pt = el.geometry[ptindex];
          label = stoplabel;
          return 0;
        }
      }
    }
  }
  cerr << "in Map::getNextStopline, stop line not found" << label << endl;
  
  return -1;
}



int Map::getWaypoint(point2& pt, PointLabel &label)
{
  unsigned int i,j;
  int elindex =-1;
  int ptindex =-1;
  MapElement el;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==label){
          ptindex =j;
          prior.getEl(elindex,el);
          pt = el.geometry[ptindex];

          return 0;
        }
      }
    }
  }
  cerr << "in Map::getWaypoint, waypoint not found" << label <<endl;
  return -1;

}

int Map::getTransitionBounds(point2arr &leftbound ,point2arr &rightbound , PointLabel &exitlabel, PointLabel &enterlabel)
{
  unsigned int i,j;
  int exit_elindex =-1;
  int exit_leftelindex =-1;
  int exit_rightelindex =-1;
  int exit_ptindex =-1;
  int enter_elindex =-1;
  int enter_leftelindex =-1;
  int enter_rightelindex =-1;

  int enter_ptindex =-1;
  
  point2 exitpt;
  point2 enterpt;
  MapElement el;

  bool isvalid = false;

  LaneLabel tmplane(enterlabel.segment,enterlabel.lane);
  if ((exitlabel.segment==enterlabel.segment) && (exitlabel.lane==enterlabel.lane)){
    getLeftBound(leftbound, tmplane);
    int retval = getRightBound(rightbound, tmplane);
    cout << "returning current lane transition" << endl;
    return retval;
  }

  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label== exitlabel){
    
      for (j=0;j<prior.lanes[i].exit_label.size();++j){
        if (prior.lanes[i].exit_label[j]==exitlabel &&
            prior.lanes[i].exit_link[j]==enterlabel){
        

          isvalid = true;
        }
      }
      if (!isvalid)
        continue;
      
      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==exitlabel){  exit_elindex = prior.lanes[i].waypoint_elindex;
        exit_leftelindex = prior.lanes[i].leftbound_elindex;
        exit_rightelindex = prior.lanes[i].rightbound_elindex;
        exit_ptindex =j;
        break;
        }
      }
    }
  }
  if (isvalid){
    for (i=0;i<prior.lanes.size(); ++i){
      if (prior.lanes[i].label== enterlabel){
    
        for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
          if (prior.lanes[i].waypoint_label[j]==enterlabel){
            enter_elindex = prior.lanes[i].waypoint_elindex;
            enter_leftelindex = prior.lanes[i].leftbound_elindex;
            enter_rightelindex = prior.lanes[i].rightbound_elindex;
            enter_ptindex =j;
            break;
          }
        }
      }
    }
    prior.getEl(enter_leftelindex,el);
    enterpt = el.geometry[enter_ptindex];  
    prior.getEl(exit_leftelindex,el);
    exitpt = el.geometry[exit_ptindex];  

    leftbound.clear();
    leftbound.push_back(enterpt);
    leftbound.push_back(exitpt);

    prior.getEl(enter_rightelindex,el);
    enterpt = el.geometry[enter_ptindex];  
    prior.getEl(exit_rightelindex,el);
    exitpt = el.geometry[exit_ptindex];  

    rightbound.clear();
    rightbound.push_back(enterpt);
    rightbound.push_back(exitpt);
  
    return 0;
  }


  cerr << "in Map::getTransitionBounds, invalid transition, exit = " << exitlabel << " enter = " << enterlabel <<endl;
  return -1;
}





int Map::getObstacleGeometry(point2arr& ptarr)
{
  return 0;
}
