/**********************************************************
 **
 **  TESTMAPELEMENTTALKER.CC
 **
 **    Time-stamp: <2007-05-13 15:00:13 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Feb  7 13:39:57 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include <iostream>
#include "MapElementTalker.hh"
#include "MapElement.hh"
#include "interfaces/sn_types.h"


using namespace std;


class CTestSendMapElement : public CMapElementTalker{

public:
  /*! Constructor */
  CTestSendMapElement(){} 
      
  /*! Standard destructor */
  ~CTestSendMapElement() {}

};

int main(int argc, char **argv)
{
  int skynetKey = 0;
  int subgroup = 0;
  
  skynetKey = atoi(getenv("SKYNET_KEY"));  

  cout << endl << "Usage :" << endl;
  cout << " testSendMapElement [skynet key] [subgroup]" << endl << endl;

  if(argc >1)
    subgroup = atoi(argv[1]);
  if(argc >2)
    skynetKey = atoi(argv[2]);
  
  cout << "Skynet Key = " << skynetKey << 
    "  Subgroup = "  << subgroup << endl;

  int bytesSent = 0;

 
  CTestSendMapElement testtalker;
  testtalker.initSendMapElement(skynetKey);
  MapElement el;


  int id_input;
  point2 cpoint(14,25);
 
  
  vector<point2> ptarr;
  ptarr.push_back(cpoint+point2(-3,3));
  ptarr.push_back(cpoint+point2(-2,0));
  ptarr.push_back(cpoint+point2(-1,-2));
  ptarr.push_back(cpoint+point2(1,-1));
  ptarr.push_back(cpoint+point2(3,3));
  
  double angle = .2;
  double radius = 3;
  double length = 4;
  double width = 2;
  
  /////////////////////////////////////
  cout << "====================================" << endl;
  cout << "Sending obstacle, radius bound" << endl;
  cout << "Enter id number and hit enter to send" << endl;
  cin>>id_input;
  
  el.clear(); 
  el.setId(id_input);
  el.setTypeObstacle();
  el.setGeometry(cpoint,radius);

  bytesSent = testtalker.sendMapElement(&el,subgroup);
  cout << "sending el :" << el<< endl;
  cout << "bytes sent = " << bytesSent << endl;
 
  /////////////////////////////////////
  cout << "====================================" << endl;
  cout << "Sending obstacle, block bound" << endl;
  cout << "Enter id number and hit enter to send" << endl;
  cin>>id_input;
  el.clear();
  el.setId(id_input);
  el.setTypeObstacle();
  el.setGeometry(cpoint,length,width,angle);
  bytesSent = testtalker.sendMapElement(&el,subgroup);
  cout << "sending el :" << el<< endl;
  cout << "bytes sent = " << bytesSent << endl;

  /////////////////////////////////////
  cout << "====================================" << endl;
  cout << "Sending obstacle, polygon bound" << endl;
  cout << "Enter id number and hit enter to send" << endl;
  cin>>id_input;
  el.clear();
  el.setId(id_input);
  el.setTypeObstacle();
  el.setGeometry(ptarr);
  bytesSent = testtalker.sendMapElement(&el,subgroup);
  cout << "sending el :" << el<< endl;
  cout << "bytes sent = " << bytesSent << endl;

  /////////////////////////////////////
  cout << "====================================" << endl;
  cout << "Sending generic line" << endl;
  cout << "Enter id number and hit enter to send" << endl;
  cin>>id_input;
  el.clear();
  el.setId(id_input);
  el.setTypeLine();  
  el.setGeometry(ptarr); 
  bytesSent = testtalker.sendMapElement(&el,subgroup);
  cout << "sending el :" << el<< endl;
  cout << "bytes sent = " << bytesSent << endl;

  /////////////////////////////////////
  cout << "====================================" << endl;
  cout << "Sending laneline" << endl;
  cout << "Enter id number and hit enter to send" << endl;
  cin>>id_input;
  el.clear();
  el.setId(id_input);
  el.setTypeLaneLine();
  el.setGeometry(ptarr);
  bytesSent = testtalker.sendMapElement(&el,subgroup);
  cout << "sending el :" << el<< endl;
  cout << "bytes sent = " << bytesSent << endl;


  /////////////////////////////////////
  cout << "====================================" << endl;
  cout << "Sending stop line" << endl;
  cout << "Enter id number and hit enter to send" << endl;
  cin>>id_input;
  el.clear();
  el.setId(id_input);
  el.setTypeStopLine();
  el.setGeometry(ptarr);
  bytesSent = testtalker.sendMapElement(&el,subgroup);
  cout << "sending el :" << el<< endl;
  cout << "bytes sent = " << bytesSent << endl;

  /////////////////////////////////////
  cout << "====================================" << endl;
  cout << "Sending points" << endl;
  cout << "Enter id number and hit enter to send" << endl;
  cin>>id_input;
  el.clear();
  el.setId(id_input);
  el.setTypePoints();
  el.setGeometry(ptarr);
  bytesSent = testtalker.sendMapElement(&el,subgroup);
  cout << "sending el :" << el<< endl;
  cout << "bytes sent = " << bytesSent << endl;

 

  /////////////////////////////////////
  cout << "====================================" << endl;
  cout << "Sending Alice" << endl;
  cout << "Enter id number and hit enter to send" << endl;
  cin>>id_input;
  VehicleState state;
  memset(&state, 0, sizeof(state));
  state.localX = 10;
  state.localY = 15;
  state.localYaw = 1;
  

  el.clear();
  el.setId(id_input);
  el.setTypeAlice();
  el.setState(state);
  bytesSent = testtalker.sendMapElement(&el,subgroup);
  cout << "sending el :" << el<< endl;
  cout << "bytes sent = " << bytesSent << endl;

  /////////////////////////////////////
  cout << "====================================" << endl;
  cout << "Sending clear" << endl;
  cout << "Enter id number and hit enter to send" << endl;
  cin>>id_input;
  el.clear();
  el.setId(id_input);
  el.setTypeClear();
  bytesSent = testtalker.sendMapElement(&el,subgroup);
  cout << "sending el :" << el<< endl;
  cout << "bytes sent = " << bytesSent << endl;
 

  return(0);
}

 
  
 
