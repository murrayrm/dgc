/**********************************************************
 **
 **  MAPELEMENT.CC
 **
 **    Time-stamp: <2007-10-05 21:57:00 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Fri Mar  9 10:57:28 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "MapElement.hh"
//#include "temp-planner-interfaces/CmdArgs.hh"

using namespace std;

#define MSG(fmt, ...)\
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0 )

// Useful error macro
#define ERRMSG(arg) MSG("*** ERROR: " << arg)


MapElement::MapElement()
{
  // Note: do we need this??
  clear();
  
  //Default for now---conf = 1;
  conf = 1;
}

MapElement::MapElement(const MapElement &el)
{
  this->id = el.id;
  this->subgroup = el.subgroup;
  this->conf = el.conf;
  this->type = el.type;
  this->typeConf = el.typeConf;
  this->position = el.position;
  this->center = el.center;
  this->length = el.length;
  this->width = el.width;
  this->orientation = el.orientation;
  this->lengthVar = el.lengthVar;
  this->widthVar = el.widthVar;
  this->orientationVar = el.orientationVar;
  this->geometryType = el.geometryType;
  this->geometry = el.geometry;
  this->expandGeo = el.expandGeo;
  this->height = el.height;
  this->heightVar = el.heightVar;
  this->elevation = el.elevation;
  this->elevationVar = el.elevationVar;
  this->velocity = el.velocity;
  this->timeStopped= el.timeStopped;
  this->peakSpeed = el.peakSpeed;
  this->peakSpeedVar = el.peakSpeedVar;
  this->peakAccel = el.peakAccel;
  this->peakAccelVar = el.peakAccelVar;
  this->timesSeen = el.timesSeen;
  this->frameType = el.frameType;
  this->timestamp = el.timestamp;
  this->state = el.state;
  this->plotColor = el.plotColor;
  this->plotValue = el.plotValue;
  this->geometryMin = el.geometryMin;
  this->geometryMax = el.geometryMax;
  this->expandMin = el.expandMin;
  this->expandMax = el.expandMax;

  this->label.clear();
  for (unsigned int i = 0; i < el.label.size(); ++i){
    this->label.push_back(el.label[i]);
  }
}

MapElement &MapElement::operator=(const MapElement &el)
{
this->id = el.id;
  this->subgroup = el.subgroup;
  this->conf = el.conf;
  this->type = el.type;
  this->typeConf = el.typeConf;
  this->position = el.position;
  this->center = el.center;
  this->length = el.length;
  this->width = el.width;
  this->orientation = el.orientation;
  this->lengthVar = el.lengthVar;
  this->widthVar = el.widthVar;
  this->orientationVar = el.orientationVar;
  this->geometryType = el.geometryType;
  this->geometry = el.geometry;
  this->expandGeo = el.expandGeo;
  this->height = el.height;
  this->heightVar = el.heightVar;
  this->elevation = el.elevation;
  this->elevationVar = el.elevationVar;
  this->velocity = el.velocity;
  this->timeStopped = el.timeStopped;
  this->peakSpeed = el.peakSpeed;
  this->peakSpeedVar = el.peakSpeedVar;
  this->peakAccel = el.peakAccel;
  this->peakAccelVar = el.peakAccelVar;
  this->timesSeen = el.timesSeen;
  this->frameType = el.frameType;
  this->timestamp = el.timestamp;
  this->state = el.state;
  this->plotColor = el.plotColor;
  this->plotValue = el.plotValue;
  this->geometryMin = el.geometryMin;
  this->geometryMax = el.geometryMax;
  this->expandMin = el.expandMin;
  this->expandMax = el.expandMax;

  this->label.clear();
  for (unsigned int i = 0; i < el.label.size(); ++i){
    this->label.push_back(el.label[i]);
  }
  return *this;
}

void  MapElement::clear(){
  subgroup = 0;
  id.clear();
  conf = 0;
  type = ELEMENT_UNDEF;
  typeConf = 0;
  position.clear();
  center.clear();
  length = -1;
  width = -1;
  orientation =0;
  lengthVar = 0;
  widthVar = 0;
  orientationVar = 0;
  height =  -1;
  heightVar = 0;
  elevation =  -1;
  elevationVar = 0;
  velocity.clear();
  timeStopped =0;
  peakSpeed=-1;
  peakSpeedVar=0;
  peakAccel=-1;
  peakAccelVar=0;
  timesSeen = 0;

  frameType = FRAME_UNDEF;
  timestamp=0;
  memset(&state, 0, sizeof(state));
  plotColor = MAP_COLOR_GREY;
  plotValue = 100;
  label.clear();

  geometryMin.clear();
  geometryMax.clear();
  expandMin.clear();
  expandMax.clear();
  geometryType = GEOMETRY_UNDEF;
  geometry.clear();
  expandGeo.clear();

}

void MapElement::setTypeClear()
{
  type=ELEMENT_CLEAR;
}

void MapElement::setTypeUndef()
{
  geometryType=GEOMETRY_UNDEF;
  type=ELEMENT_UNDEF;
}
void MapElement::setTypeAlice()
{
  geometryType=GEOMETRY_POLY;
  type=ELEMENT_ALICE;
}


void MapElement::setTypePoints()
{
  geometryType = GEOMETRY_POINTS;
  type=ELEMENT_POINTS;
}
void MapElement::setTypeWayPoints()
{
  geometryType = GEOMETRY_ORDERED_POINTS;
  type=ELEMENT_WAYPOINTS;
  plotColor = MAP_COLOR_BLUE;
  plotValue = 50;     
}
void MapElement::setTypeCheckPoints()
{
  geometryType = GEOMETRY_ORDERED_POINTS;
  type=ELEMENT_CHECKPOINTS;
}


void MapElement::setTypeLine()
{
  geometryType = GEOMETRY_LINE;
  type=ELEMENT_LINE;
}

void MapElement::setTypeWhiteLine()
{
  geometryType = GEOMETRY_LINE;
  type = ELEMENT_LINE_WHITE;
  plotColor = MAP_COLOR_GREY;  //for now....
  plotValue = 100;
}

void MapElement::setTypeYellowLine()
{
  geometryType = GEOMETRY_LINE;
  type = ELEMENT_LINE_YELLOW;
  plotColor = MAP_COLOR_YELLOW;
  plotValue = 100;
}

void MapElement::setTypeStopLine()
{
  geometryType = GEOMETRY_LINE;
  type=ELEMENT_STOPLINE;
}
void MapElement::setTypeLaneLine()
{
  geometryType = GEOMETRY_LINE;
  type=ELEMENT_LANELINE;
  plotColor = MAP_COLOR_BLUE;
  plotValue = 100;     
}


void MapElement::setTypeParkingSpot()
{
  geometryType = GEOMETRY_ORDERED_POINTS;
  type=ELEMENT_PARKING_SPOT;
}
void MapElement::setTypePerimeter()
{
  geometryType = GEOMETRY_POLY;
  type=ELEMENT_PERIMETER;
}


void MapElement::setTypeObstacle()
{
  geometryType = GEOMETRY_POLY;
  type=ELEMENT_OBSTACLE;
}

void MapElement::setTypeStereoObstacle()
{
  geometryType = GEOMETRY_POLY;
  type=ELEMENT_OBSTACLE_STEREO;
}
void MapElement::setTypePredictedVehicle()
{
  geometryType = GEOMETRY_POLY;
  type=ELEMENT_VEHICLE_PREDICTION;
}

void MapElement::setTypeLadarObstacle()
{
  geometryType = GEOMETRY_POLY;
  type=ELEMENT_OBSTACLE_LADAR;
}
void MapElement::setTypeRadarObstacle()
{
  geometryType = GEOMETRY_POLY;
  type=ELEMENT_OBSTACLE_RADAR;
}
void MapElement::setTypeObstacleEdge()
{
  geometryType = GEOMETRY_EDGE;
  type=ELEMENT_OBSTACLE_EDGE;
}
void MapElement::setTypeVehicle()
{
  geometryType = GEOMETRY_POLY;
  type=ELEMENT_VEHICLE;
}

void MapElement::setTypePoly()
{
  geometryType = GEOMETRY_POLY;
  type=ELEMENT_POLY;
}

void MapElement::setTypeTravPath()
{
  geometryType = GEOMETRY_LINE;
  type=ELEMENT_TRAV_PATH;
  plotColor = MAP_COLOR_DARK_YELLOW;
}

void MapElement::setTypePlanningTraj()
{
  geometryType = GEOMETRY_LINE;
  type=ELEMENT_PLANNING_TRAJ;
  plotColor=MAP_COLOR_MAGENTA;
}

void MapElement::setTypeRDDF()
{
  geometryType = GEOMETRY_LINE;
  type = ELEMENT_RDDF;
  plotColor = MAP_COLOR_LIGHT_BLUE;
}

void MapElement::setTypeSteerCmd()
{
  geometryType = GEOMETRY_LINE;
  type = ELEMENT_STEER_CMD;
  plotColor = MAP_COLOR_GREEN;
}

void MapElement::setTypeSteer()
{
  geometryType = GEOMETRY_LINE;
  type = ELEMENT_VEH_STEER;
  plotColor = MAP_COLOR_BLUE_2;
}

void MapElement::setTypePolytope()
{
  geometryType = GEOMETRY_LINE;
  type = ELEMENT_POLYTOPE;
  plotColor = MAP_COLOR_DARK_GREEN;
}

void MapElement::setTypeInitCond()
{
  geometryType = GEOMETRY_LINE;
  type = ELEMENT_INIT_COND;
  plotColor = MAP_COLOR_ORANGE;
}

void MapElement::setTypeFinalCond()
{
  geometryType = GEOMETRY_LINE;
  type = ELEMENT_FINAL_COND;
  plotColor = MAP_COLOR_ORANGE;
}

void MapElement::setTypeFreeSpace()
{
  geometryType = GEOMETRY_POLY;
  type = ELEMENT_FREE_SPACE;
  plotColor = MAP_COLOR_GREEN;
  plotValue = 50;
}

void MapElement::setTypeDebug()
{
  geometryType = GEOMETRY_UNDEF;
  type = ELEMENT_DEBUG;
  plotColor = MAP_COLOR_GREY;
}


void MapElement::setType(int elType)
{
  switch(elType) {

    case ELEMENT_UNDEF:
      geometryType = GEOMETRY_UNDEF;
      type = ELEMENT_UNDEF;
      break;

    case ELEMENT_CLEAR:
      type = ELEMENT_CLEAR;
      break;

    case ELEMENT_ALICE:
      type = ELEMENT_ALICE;
      geometryType = GEOMETRY_POLY;
      break;

    case ELEMENT_POINTS:
      type = ELEMENT_POINTS;
      geometryType = GEOMETRY_POINTS;
      break;

    case ELEMENT_WAYPOINTS:
      type = ELEMENT_WAYPOINTS;
      geometryType = GEOMETRY_ORDERED_POINTS;
      plotColor = MAP_COLOR_BLUE;
      plotValue = 50;
      break;

    case ELEMENT_CHECKPOINTS:
      type = ELEMENT_CHECKPOINTS;
      geometryType = GEOMETRY_ORDERED_POINTS;
      break;

    case ELEMENT_LINE:
      type = ELEMENT_LINE;
      geometryType = GEOMETRY_LINE;
      break;
      
    case ELEMENT_STOPLINE:
      type = ELEMENT_STOPLINE;
      geometryType = GEOMETRY_LINE;
      break;

     case ELEMENT_LANELINE:
      type = ELEMENT_LANELINE;
      geometryType = GEOMETRY_LINE;
      plotColor = MAP_COLOR_BLUE;
      plotValue = 100;
      break;
      
    case ELEMENT_LINE_WHITE:
      type = ELEMENT_LINE_WHITE;
      geometryType = GEOMETRY_LINE;
      plotColor = MAP_COLOR_GREY;
      plotValue = 100;
      break;

    case ELEMENT_LINE_YELLOW:
      type = ELEMENT_LINE_YELLOW;
      geometryType = GEOMETRY_LINE;
      plotColor = MAP_COLOR_YELLOW;
      plotValue = 100;
      break;
	
    case ELEMENT_PARKING_SPOT:
      type = ELEMENT_PARKING_SPOT;
      geometryType = GEOMETRY_ORDERED_POINTS;
      break;

    case ELEMENT_PERIMETER:
      type = ELEMENT_PERIMETER;
      geometryType = GEOMETRY_POLY;
      break;

    case ELEMENT_OBSTACLE:
      type = ELEMENT_OBSTACLE;
      geometryType = GEOMETRY_POLY;
      break;

    case ELEMENT_OBSTACLE_EDGE:
      type = ELEMENT_OBSTACLE_EDGE;
      geometryType = GEOMETRY_EDGE;
      break;

    case ELEMENT_VEHICLE:
      type = ELEMENT_VEHICLE;
      geometryType = GEOMETRY_POLY;
      break;

    case ELEMENT_POLY:
      type = ELEMENT_POLY;
      geometryType = GEOMETRY_POLY;
      break;

    case ELEMENT_TRAV_PATH:
      type = ELEMENT_TRAV_PATH;
      geometryType = GEOMETRY_LINE;
      plotColor = MAP_COLOR_DARK_YELLOW;
      break;

    case ELEMENT_PLANNING_TRAJ:
      type = ELEMENT_PLANNING_TRAJ;
      geometryType = GEOMETRY_LINE;
      plotColor = MAP_COLOR_MAGENTA;
      break;

    case ELEMENT_STEER_CMD:
      type = ELEMENT_STEER_CMD;
      geometryType = GEOMETRY_LINE;
      plotColor = MAP_COLOR_GREEN;
      break;

    case ELEMENT_VEH_STEER:
      type = ELEMENT_VEH_STEER;
      geometryType = GEOMETRY_LINE;
      plotColor = MAP_COLOR_BLUE_2;
      break;

   case ELEMENT_RDDF:
     type = ELEMENT_RDDF;
     geometryType = GEOMETRY_LINE;
      break;

   case ELEMENT_POLYTOPE:
     type = ELEMENT_POLYTOPE;
     geometryType = GEOMETRY_LINE;
      plotColor = MAP_COLOR_DARK_GREEN;
      break;

   case ELEMENT_INIT_COND:
     type = ELEMENT_INIT_COND;
     geometryType = GEOMETRY_LINE;
      plotColor = MAP_COLOR_ORANGE;
      break;

   case ELEMENT_FINAL_COND:
     type = ELEMENT_FINAL_COND;
     geometryType = GEOMETRY_LINE;
      plotColor = MAP_COLOR_ORANGE;
      break;

   case ELEMENT_FREE_SPACE:
     type = ELEMENT_FREE_SPACE;
     geometryType = GEOMETRY_POLY;
     plotColor = MAP_COLOR_GREEN;
     plotValue = 50;
      break;

   case ELEMENT_OBSTACLE_STEREO:
     type = ELEMENT_OBSTACLE_STEREO;
     geometryType = GEOMETRY_POLY;
      break;

   case ELEMENT_OBSTACLE_RADAR:
     type = ELEMENT_OBSTACLE_RADAR;
     geometryType = GEOMETRY_POLY;
      break;

   case ELEMENT_OBSTACLE_LADAR:
     type = ELEMENT_OBSTACLE_LADAR;
     geometryType = GEOMETRY_POLY;
      break;

  case ELEMENT_VEHICLE_PREDICTION:
     type = ELEMENT_VEHICLE_PREDICTION;
     geometryType = GEOMETRY_POLY;
      break;

  default:
    break;

  }
  return;
}

void MapElement::setGeometry(point2arr_uncertain &ptarr)
{
  geometry = ptarr;
  //setExpandGeo(1.645);
  updateFromGeometry();   
}
 
void MapElement::setGeometry(point2arr &ptarr)
{
  geometry=ptarr;  
  updateFromGeometry();
}

void MapElement::setGeometry(point2 &pt)
{
  point2arr ptarr(pt);
  geometry = ptarr;
  updateFromGeometry();
}
void MapElement::setGeometry(point2_uncertain &pt)
{
  geometry.clear();
  geometry.push_back(pt);
  //setExpandGeo(1.645);
  updateFromGeometry(); 
}

void MapElement::setGeometry(point2 &pt, double radius)
{
  point2_uncertain pt_unc(pt);
  setGeometry(pt_unc,radius);
  updateFromGeometry();
}
void MapElement::setGeometry(point2_uncertain &pt, double radius)
{
  geometry.clear();
  int numpts = 20;
  point2 tmppt(radius,0);
  for (int i=0;i<numpts;++i){
    tmppt =tmppt.rot(2*M_PI/(numpts));
    geometry.push_back(pt+tmppt);
  }
  
  //setExpandGeo(1.645);
  updateFromGeometry();
}

void MapElement::setGeometry(point2 &pt, double len, double wid, double orient)
{
  point2_uncertain pt_unc(pt);
  setGeometry(pt_unc,len,wid,orient);
  updateFromGeometry();
}

void MapElement::setGeometry(point2_uncertain &pt, double len, double wid, double orient)
{
  point2_uncertain tmppt;
 
  geometry.clear();
  tmppt.set(len/2,wid/2);
  geometry.push_back(tmppt);

  tmppt.set(len/2,-wid/2);
  geometry.push_back(tmppt);
   
  tmppt.set(-len/2,-wid/2);
  geometry.push_back(tmppt);

  tmppt.set(-len/2,wid/2);
  geometry.push_back(tmppt);
  geometry = geometry.rot(orient);
  
  geometry = geometry+pt;


  //setExpandGeo(1.645);

  updateFromGeometry();
}



void MapElement::setGeometry(point2 &pta,point2 &ptb)
{
  geometry.clear();
  geometry.push_back(pta);
  geometry.push_back(ptb);
  updateFromGeometry();
}

void MapElement::setGeometry(point2_uncertain &pta,point2_uncertain &ptb)
{
  geometry.clear();
  geometry.push_back(pta);
  geometry.push_back(ptb);
  //setExpandGeo(1.645);
  updateFromGeometry();
}


void MapElement::setGeometry(vector<point2> &ptarr)
{
  point2arr tmpptarr(ptarr);
  geometry = tmpptarr;  
  updateFromGeometry();
}

void MapElement::setGeometry(vector<point2_uncertain> &ptarr)
{
  geometry=ptarr;
  //setExpandGeo(1.645);
  updateFromGeometry();
}

void MapElement::setGeometry(vector<double> &xarr, vector<double> &yarr)
{
  int sze1 = xarr.size();
  int sze2 = yarr.size();
  if (sze1!=sze2){
    cerr << "in MapElement::setGeometry mismatched sizes of x, y arrays given"<<endl;
    return;
  }
  geometry.clear();
  point2_uncertain pt;
  for (int i=0;i<sze1; ++i){
    pt.x = xarr[i];
    pt.y = yarr[i];
    geometry.push_back(pt);
  }
  updateFromGeometry();
}


void MapElement::setPosition(point2 cpt)
{
  position.clear();
  position = cpt;
}

void MapElement::setPosition(point2_uncertain cpt)
{
  position.clear();
  position = cpt;
}
 
void MapElement::setPosition(double x, double y)
{
  position.clear();
  position.x = x;
  position.y = y;
}
 

void MapElement::setCenterBoundsFromGeometry(){
  point2arr ptarr(geometry);
  point2 cpt;
  // cout <<"getting bounds " << ptarr << endl;
  ptarr.get_bound_box(cpt,length,width,orientation);
  // cout << "cnetr " << cpt 
  //      << "  len " << length
  //     << "  wid " << width
  //     << "  orin " << orientation << endl;
  center = cpt;

  //--------------------------------------------------
  // still need to define the variance terms using point uncertainty
  //--------------------------------------------------
  return;
}

void MapElement::setCenterBoundsFromMinMax(){
  point2arr ptarr;
  point2 cpt;

  double cx = 0.5*(geometryMin.x+geometryMax.x);
  double cy = 0.5*(geometryMin.y+geometryMax.y);
  double dx = fabs(geometryMax.x-geometryMin.x);
  double dy = fabs(geometryMax.y-geometryMin.y);

  cpt.set(cx,cy);
  center = cpt;
  
  orientation = 0;
  length = dx;
  width = dy;
  return;
}

void MapElement::setPositionFromGeometry(){
  int i;
  int size = (int)geometry.size();
  if (size==0)
    return;
  
  double fulldist = 0;
  double dist = 0;
  point2 dpt;
  double ratio = 1;
  point2 sum(0,0);
  int lowindex =0,highindex=0;

  
  switch (geometryType) {
  case GEOMETRY_UNDEF:
  case GEOMETRY_POINTS:
    position = geometry[0];
    return;
  case GEOMETRY_ORDERED_POINTS:
  case GEOMETRY_LINE:
  case GEOMETRY_EDGE:
    if (size==1){
      position = geometry[0];
      return;
    }
    for (i = 0; i < size-1; ++i){
      dpt = geometry[i+1]-geometry[i];
      fulldist = fulldist+dpt.norm();
    }
      
    for (i = 0; i < size-1; ++i){
      dpt = geometry[i+1]-geometry[i];
        
      if (dist < fulldist/2 &&
          dist+dpt.norm() >= fulldist/2){
        lowindex = i;
        highindex = i+1;
        
        break;
      }
      dist = dist+dpt.norm();
    }
    ratio = (fulldist/2-dist)/dpt.norm();
    position = geometry[lowindex]+dpt*ratio;
    return;
  case GEOMETRY_POLY:
    for (i = 0; i < size; ++i){
      sum = sum+geometry[i];
    }
    position = sum/size;
    return;
    
  }
}
void MapElement::setMinMax(double minx, double miny, double maxx, double maxy)
{
  geometryMin.x = minx;
  geometryMin.y = miny;
  geometryMax.x = maxx;
  geometryMax.y = maxy;

}

void MapElement::setMinMaxFromGeometry()
{

  int size = (int)geometry.size();
  if (size==0){
    geometryMin.clear();
    geometryMax.clear();
    return;
  }

  geometryMin = geometry.min();
  geometryMax = geometry.max();

  return;
}

void MapElement::setExpandMinMaxFromGeometry()
{
  if((int)expandGeo.size() == 0)
  {
    expandMin.clear();
    expandMax.clear();
  }
  else
  {
    expandMin = expandGeo.min();
    expandMax = expandGeo.max();
  }

  return;
}

void MapElement::updateFromGeometry()
{
  // Removing bounding box calc from default update 
  // setCenterBoundsFromGeometry();
  
  setPositionFromGeometry();
  setMinMaxFromGeometry();
  // calculating center bounds from min max values
  // non oriented bounding box
  setCenterBoundsFromMinMax();
}

bool MapElement::isOverlap(const MapElement& el) const
{
  point2arr thatptarr(el.geometry);

  //check min/max have been initialized before using 
  if((el.geometryMin.x == el.geometryMax.x) && (el.geometryMin.y == el.geometryMax.y)) {
    point2 elMin, elMax;
    elMin = el.geometry.min();
    elMax = el.geometry.max();
    ///MSG("called isOverlap on mapElement w/o initialized min/max");
    return isOverlap(thatptarr,elMin,elMax,el.geometryType);
  } else {
    return isOverlap(thatptarr,point2(el.geometryMin),point2(el.geometryMax),el.geometryType);
  }
}

bool MapElement::isOverlap(const point2 &pt) const
{
  if (geometryType != GEOMETRY_POLY)
    return false;

  //check that min/max have been initialized
  if((geometryMin.x == geometryMax.x) && (geometryMin.y == geometryMax.y)) {
    point2 elMin, elMax;
    elMin = geometry.min();
    elMax = geometry.max();
    ///MSG("called isOverlap on mapElement w/o initialized min/max");

    if (elMax.x < pt.x) 
      return false;
    if (elMin.x > pt.x) 
      return false;
    if (elMax.y < pt.y) 
      return false;
    if (elMin.y > pt.y) 
      return false;


  } else {

    if (geometryMax.x < pt.x) 
      return false;
    if (geometryMin.x > pt.x) 
      return false;
    if (geometryMax.y < pt.y) 
      return false;
    if (geometryMin.y > pt.y) 
      return false;
  }

  point2arr ptarr(geometry);
  return (ptarr.is_poly_point_overlap(pt));
}
bool MapElement::isOverlap(const point2_uncertain &pt) const
{
  point2 tmppt(pt);
  return isOverlap(tmppt);
}

bool MapElement::isOverlap(const point2arr &ptarr, const MapElementGeometryType type) const 
{
  return isOverlap(ptarr,ptarr.min(),ptarr.max(),type);
}

// This is the main isOverlap function
bool MapElement::isOverlap(const point2arr& ptarr, point2 pmin, point2 pmax, const MapElementGeometryType type) const
{

  double s1,s2;

  //check that min/max have been initialized
  if((geometryMin.x == geometryMax.x) && (geometryMin.y == geometryMax.y)) {
    point2 elMin, elMax;
    elMin = geometry.min();
    elMax = geometry.max();
    ///MSG("called isOverlap on mapElement w/o initialized min/max");

    // find areas of original bounding boxes
    s1 = (elMax.x-elMin.x)*(elMax.y-elMin.y);
    s2 = (pmax.x-pmin.x)*(pmax.y-pmin.y);

    pmin.set(max(elMin.x,pmin.x),max(elMin.y,pmin.y));
    pmax.set(min(elMax.x,pmax.x),min(elMax.y,pmax.y));

    if (pmin.x >= pmax.x || pmin.y >= pmax.y)
      return false;



  } else {
    // find areas of original bounding boxes
    s1 = (geometryMax.x-geometryMin.x)*(geometryMax.y-geometryMin.y);
    s2 = (pmax.x-pmin.x)*(pmax.y-pmin.y);

    pmin.set(max(geometryMin.x,pmin.x),max(geometryMin.y,pmin.y));
    pmax.set(min(geometryMax.x,pmax.x),min(geometryMax.y,pmax.y));

    if (pmin.x >= pmax.x || pmin.y >= pmax.y)
      return false;

  }

  bool swapOver = false;

  if (s1 > s2)
    swapOver = true;

  point2arr thisptarr(geometry);
  const point2arr& thatptarr = ptarr;

  switch (geometryType)
  {
  case GEOMETRY_LINE:
    if (type ==GEOMETRY_LINE){
      return (thisptarr.is_intersect(thatptarr));
    }else if (type == GEOMETRY_POLY){
      return (thisptarr.is_line_poly_overlap(thatptarr));
    }else if (type == GEOMETRY_POINTS||
              type == GEOMETRY_ORDERED_POINTS){
      return false;
    } else if (type == GEOMETRY_EDGE){
      cout <<"in MapElement::check_overlap, need to handle GEOMETRY_EDGE better" << endl;
      return (thisptarr.is_intersect(thatptarr));
    }
    break;
    //doesnt handle GEOMETRY_UNDEF here

  case GEOMETRY_POLY:
    if (type ==GEOMETRY_LINE){
      return (thisptarr.is_poly_line_overlap(thatptarr));
    }else if (type == GEOMETRY_POLY){
      // this is the important one, swap for speed
      if (swapOver)
	return (thatptarr.is_poly_overlap(thisptarr,pmin,pmax));
      else
	return (thisptarr.is_poly_overlap(thatptarr,pmin,pmax));	  
    }else if (type == GEOMETRY_POINTS||
              type == GEOMETRY_ORDERED_POINTS){
      return (thisptarr.is_poly_point_overlap(thatptarr));
    }else if (type == GEOMETRY_EDGE){
      cout <<"in MapElement::check_overlap, need to handle GEOMETRY_EDGE better" << endl;
      return (thisptarr.is_poly_line_overlap(thatptarr));
    }
    break;

  case GEOMETRY_POINTS:
  case GEOMETRY_ORDERED_POINTS:
    if (type ==GEOMETRY_LINE){
      return false;
    }else if (type == GEOMETRY_POLY){
      return (thisptarr.is_point_poly_overlap(thatptarr));
    }else if (type == GEOMETRY_POINTS||
              type == GEOMETRY_ORDERED_POINTS){
      return false;
    }else if (type == GEOMETRY_EDGE){
      cout <<"in MapElement::check_overlap, need to handle GEOMETRY_EDGE better" << endl;
      return (thisptarr.is_intersect(thatptarr));
    }
    break;

  case GEOMETRY_EDGE:
    if (type ==GEOMETRY_LINE){
      return (thisptarr.is_intersect(thatptarr));
    }else if (type == GEOMETRY_POLY){
      return (thisptarr.is_line_poly_overlap(thatptarr));
    }else if (type == GEOMETRY_POINTS||
              type == GEOMETRY_ORDERED_POINTS){
      return false;
    }else if (type == GEOMETRY_EDGE){
      cout <<"in MapElement::check_overlap, need to handle GEOMETRY_EDGE better" << endl;
      return false;
    }
    break;

  case GEOMETRY_UNDEF:
  default:
      break;
  }

  cout <<"in MapElement::check_overlap, checking element overlap with UNDEF geometry in map element(s). Returning false" << endl;
  return false;
}

bool MapElement::isOverlap(const point2arr_uncertain &ptarr, const MapElementGeometryType type) const  
{
  point2arr tmpptarr(ptarr);
  return (isOverlap(tmpptarr,type));
}




double MapElement::dist(const MapElement& el) const
{
  return dist(el.geometry,el.geometryType);
}

double MapElement::dist(const point2 &pt) const
{
  point2arr ptarr(geometry);
  
  if (isOverlap(pt))
    return 0;
  
  switch (geometryType) {
  case GEOMETRY_UNDEF:
  case GEOMETRY_POINTS:
  case GEOMETRY_ORDERED_POINTS:
    return ptarr.get_min_dist_points(pt);
  case GEOMETRY_LINE:
  case GEOMETRY_EDGE:
    return ptarr.get_min_dist_line(pt);
  case GEOMETRY_POLY:
    return ptarr.get_min_dist_poly(pt);
  default:
    return -1;
  }
  
}
 
double MapElement::dist(const point2_uncertain &pt) const
{
  point2 tmppt(pt);
  return dist(tmppt);
}

double MapElement::dist(const point2arr &ptarr, const MapElementGeometryType type) const 
{
  
  point2arr thisptarr,thatptarr;
  thisptarr = geometry;
  thatptarr = ptarr;
  
  if (geometryType == GEOMETRY_LINE ||
      geometryType == GEOMETRY_EDGE){
    if (type ==GEOMETRY_LINE){
      return (thisptarr.get_min_dist_line(thatptarr));
    }else if (type == GEOMETRY_POLY){
      return (thisptarr.get_min_dist_line_poly(thatptarr));
    }else if (type == GEOMETRY_POINTS||
              type == GEOMETRY_ORDERED_POINTS){
      // This should be handling points
      return (thisptarr.get_min_dist_line(thatptarr));
    } else if (type == GEOMETRY_EDGE){
      cout <<"in MapElement::dist, need to handle GEOMETRY_EDGE better" << endl;
      return (thisptarr.get_min_dist_line(thatptarr));
    }
    //doesnt handle GEOMETRY_UNDEF here
  }else if (geometryType == GEOMETRY_POLY){
    if (type ==GEOMETRY_LINE){
      return (thisptarr.get_min_dist_poly_line(thatptarr));
    }else if (type == GEOMETRY_POLY){
      return (thisptarr.get_min_dist_poly(thatptarr));
    }else if (type == GEOMETRY_POINTS||
              type == GEOMETRY_ORDERED_POINTS){
      //This should be handling points
      return (thisptarr.get_min_dist_poly_line(thatptarr));
    }else if (type == GEOMETRY_EDGE){
      cout <<"in MapElement::dist, need to handle GEOMETRY_EDGE better" << endl;
      return (thisptarr.is_poly_line_overlap(thatptarr));
    }


  }else if (geometryType == GEOMETRY_POINTS ||
            geometryType == GEOMETRY_ORDERED_POINTS){
    if (type ==GEOMETRY_LINE){
      // This should be handling points
      return (thisptarr.get_min_dist_line(thatptarr));
    }else if (type == GEOMETRY_POLY){
      return (thisptarr.get_min_dist_line_poly(thatptarr));
    }else if (type == GEOMETRY_POINTS||
              type == GEOMETRY_ORDERED_POINTS){
      return (thisptarr.get_min_dist_points(thatptarr));
    }else if (type == GEOMETRY_EDGE){
      cout <<"in MapElement::dist, need to handle GEOMETRY_EDGE better" << endl;
      return (thisptarr.get_min_dist_line(thatptarr));
    }
  }
  cout <<"in MapElement::dist, checking element dist with UNDEF geometry in map element(s). Returning -1" << endl;
  return -1;
}

double MapElement::dist(const point2arr_uncertain &ptarr, const MapElementGeometryType type) const  
{
  point2arr tmpptarr(ptarr);
  return (dist(tmpptarr,type));
}


void MapElement::translate(const double dx, const double dy)
{
if (fabs(dx)<.0000001 && fabs(dy<.0000001)){
  return;
}


	this->center.x = this->center.x + dx;
  this->center.y = this->center.y + dy;

  this->position.x = this->position.x + dx;
  this->position.y = this->position.y + dy;

  this->geometryMax.x = this->geometryMax.x + dx;
  this->geometryMax.y = this->geometryMax.y + dy;

  this->geometryMin.x = this->geometryMin.x + dx;
  this->geometryMin.y = this->geometryMin.y + dy;
  
  unsigned int sze = this->geometry.size();
	for (unsigned int i = 0; i <sze; ++i){
    this->geometry[i].x = this->geometry[i].x + dx;
    this->geometry[i].y = this->geometry[i].y + dy;
  }
}

void MapElement::rotate(const double ang)
{
if (fabs(ang)<.0000001){
  return;
}

		double sa = sin(ang);
		double ca = cos(ang);
		this->center.x = this->center.x*ca-this->center.y*sa;
		this->center.y = this->center.x*sa+this->center.y*ca;
		
		this->position.x = this->position.x*ca-this->position.y*sa;
		this->position.y = this->position.x*sa+this->position.y*ca;
		
		this->geometryMax.x = this->geometryMax.x*ca-this->geometryMax.y*sa;
		this->geometryMax.y = this->geometryMax.x*sa+this->geometryMax.y*ca;
		
		this->geometryMin.x = this->geometryMin.x*ca-this->geometryMin.y*sa;
		this->geometryMin.y = this->geometryMin.x*sa+this->geometryMin.y*ca;
		
		unsigned int sze = this->geometry.size();
		for (unsigned int i = 0; i <sze; ++i){
			this->geometry[i].x = this->geometry[i].x*ca-this->geometry[i].y*sa;
			this->geometry[i].y = this->geometry[i].x*sa+this->geometry[i].y*ca;
		}
	
}

int MapElement::setLabel(const int index,const string str)
{
	if (index>=0 && index<(int)label.size()){
		label[index] = str;
		return index;
	}else{
		label.push_back(str);
		return ((int)label.size()-1);
	}
}

int MapElement::setLabel(const int index,const string str, const int val)
{
	ostringstream o;
	 
	o<< str;
	o<< val;
	
	if (index>=0 && index<(int)label.size()){
		label[index] = o.str();
		return index;
	}else{
		label.push_back(o.str());
		return ((int)label.size()-1);
	}
	
}

int MapElement::setLabel(const int index,const string str, const double val)
{
	ostringstream o;
	 
	o<< str;
	o<< val;
	
	if (index>=0 && index<(int)label.size()){
		label[index] = o.str();
		return index;
	}else{
		label.push_back(o.str());
		return ((int)label.size()-1);
	}
	
}

int MapElement::setLabel(const int index,const string str, const uint64_t val)
{
	ostringstream o;
	 
	o<< str; 
	o<< val;
	
	if (index>=0 && index<(int)label.size()){
		label[index] = o.str();
		return index;
	}else{
		label.push_back(o.str());
		return ((int)label.size()-1);
	}
	
}


ostream &operator<<(ostream &os, const MapElement &el)
{
  unsigned int i;
  os << endl<<"------Start Printing MapElement------" << endl;
  os << "subgroup= " << el.subgroup << endl;
  os << "id= " << el.id << endl;
  os << " conf= " << el.conf 
     << " type= " << el.type
     << " typeConf= " << el.typeConf << endl;
  os << "isObstacle= " << el.isObstacle() 
     << " isLine= " << el.isLine() << endl;
  os << "position: " << el.position << endl;
  os << "center:  " << el.center << endl;
  os << "length= " << el.length
     << " width= " << el.width
     << " orientation= " << el.orientation << endl;
  os << "lengthVar= " << el.lengthVar
     << " widthVar= " << el.widthVar
     << " orientationVar= " << el.orientationVar << endl;
  os << "geometryType= " << el.geometryType <<endl;
  os << "geometry :  " << el.geometry << endl;
  os << "geometryMin :  " << el.geometryMin << endl;
  os << "geometryMax :  " << el.geometryMax << endl;
   

  os <<"height= " << el.height 
     << " heightVar= " << el.heightVar << endl;
  os <<"elevation= " << el.elevation 
     << " elevationVar= " << el.elevationVar << endl;

  os << "velocity:  " << el.velocity << endl ;
  os <<"timeStopped= " << el.timeStopped << endl;
    os <<"peakSpeed= " << el.peakSpeed 
     << " peakSpeedVar= " << el.peakSpeedVar << endl;
  os <<"peakAccel= " << el.peakAccel 
     << " peakAccelVar= " << el.peakAccelVar << endl;
  

  os <<"frame= " << el.frameType << endl;
  os <<"timestamp= " << el.timestamp << endl;
  

  os <<"plotColor= " << el.plotColor << endl;
  os <<"plotValue= " << el.plotValue << endl;
  
  
  for (i = 0; i < el.label.size(); ++i){
    os << "label line " << i << ":  " << el.label[i]<< endl;
  }

  if (el.state.timestamp>0){
    cout << el.state << endl;
  }
  os << "------End Printing MapElement--------" << endl;
  return os;
}
ostream &operator<<(ostream &os, const VehicleState &state)
{
  
  os << endl<<"------Start Printing VehicleState------" << endl;
  os << "timestamp= " << state.timestamp << endl;
  os << "utmNorthing= " << state.utmNorthing 
     << " utmEasting= " << state.utmEasting 
     << " utmAltitude= " << state.utmAltitude 
     << endl;

  os << "utmNorthVel= " << state.utmNorthVel 
     << " utmEastVel= " << state.utmEastVel 
     << " utmAltitudeVel= " << state.utmAltitudeVel 
     << endl;

  os << "Acc_N_deprecated= " << state.Acc_N_deprecated
     << "Acc_E_deprecated= " << state.Acc_E_deprecated
     << "Acc_D_deprecated= " << state.Acc_D_deprecated
     << endl;

  os << "utmRoll= " << state.utmRoll 
     << " utmPitch= " << state.utmPitch 
     << " utmYaw= " << state.utmYaw 
     << endl;

  os << "utmRollRate= " << state.utmRollRate 
     << " utmPitchRate= " << state.utmPitchRate 
     << " utmYawRate= " << state.utmYawRate 
     << endl;

  os << "utmNorthConfidence= " << state.utmNorthConfidence  
     << " utmEastConfidence= " << state.utmEastConfidence 
     << " utmAltitudeConfidence= " <<  state.utmAltitudeConfidence  
     << endl;

  os << "rollConfidence= " << state.rollConfidence 
     << " pitchConfidence= " << state.pitchConfidence
     << " yawConfidence= " << state.yawConfidence
     << endl;

  os << "gpsGamma= " << state.vehSpeed
     << " utmZone= " << state.utmZone 
     << " utmLetter= " << state.utmLetter
     << endl;

  os << "vehXVel= " << state.vehXVel 
     << " vehYVel= " << state.vehYVel 
     << " vehZVel= " << state.vehZVel
     << endl;
  
  os << "vehRollRate= " << state.vehRollRate 
     << " vehPitchRate= " << state.vehPitchRate 
     << " vehYawRate= " << state.vehYawRate
     << endl;

  os << "siteNorthing (x)= " << state.siteNorthing
     << " siteEasting (y)= " << state.siteEasting 
     << " siteAltitude= " << state.siteAltitude
     << endl;

  os << "siteRoll= " << state.siteRoll 
     << " sitePitch= " << state.sitePitch 
     << " siteYaw= " << state.siteYaw
     << endl;


  os << "localX= " << state.localX 
     << " localY= " << state.localY 
     << " localZ= " << state.localZ
     << endl;

  os << "localRoll= " << state.localRoll 
     << " localPitch= " << state.localPitch 
     << " localYaw= " << state.localYaw
     << endl;



  os << "localXVel= " << state.localXVel 
     << " localYVel= " << state.localYVel 
     << " localZVel= " << state.localZVel
     << endl;

  os << "localRollRate= " << state.localRollRate 
     << " localPitchRate= " << state.localPitchRate 
     << " localYawRate= " << state.localYawRate
     << endl; 

  os << "------End Printing VehicleState--------" << endl;
  return os;
}



//Methods for expanding geometry to incorporate position uncertainty

//Expands geometry of map element to account for uncertainty
//Input: confidence value to use; 1.645 for 90% confidence
void MapElement::setExpandGeo(double c)
{

  if((int)geometry.size() < 1)
    return;

 expandGeo.clear();

 if((int)geometry.size() == 1)  //expanded geometry is smallest rectangle that covers uncertainty ellipse of point
 {
    double maxDist = c*sqrt(geometry[0].max_var);
    double minDist = c*sqrt(geometry[0].min_var);
    double x = geometry[0].x;
    double y = geometry[0].y;
    double axis = geometry[0].axis;

    if(maxDist < .0000001)
      expandGeo = geometry;
    else
    {
      expandGeo.push_back(point2(x + maxDist*cos(axis) - minDist*sin(axis), y + maxDist*sin(axis) + minDist*cos(axis)));
      expandGeo.push_back(point2(x - maxDist*cos(axis) - minDist*sin(axis), y - maxDist*sin(axis) + minDist*cos(axis)));
      expandGeo.push_back(point2(x - maxDist*cos(axis) + minDist*sin(axis), y - maxDist*sin(axis) - minDist*cos(axis)));
      expandGeo.push_back(point2(x + maxDist*cos(axis) + minDist*sin(axis), y + maxDist*sin(axis) - minDist*cos(axis)));
  
    }
 }
  else
    expandGeometryLines(c);  //call helper method to expand geometry using edges of object

 setExpandMinMaxFromGeometry();  //set min, max from expanded geometry for quick bounding

 return;
}


//Expands geometry of map element with at least two points to account for uncertainty
// by expanding along the edges of the polygonal shape
//Input:  confidence value to use for ellipses; 1.645 for 90% confidence
void MapElement::expandGeometryLines(double sigma)
{
 
  assert(geometry.size() > 1);
 
  point2_uncertain p0, p1;
  point2 tangent1_p, tangent2_p, a, b, c, d;
  point2 firstPoint, prevPoint;
  double b1, b2, b3, b4,  x1, x2, x3, x4, y1, y2, y3, y4;
  double d_x, d_y, m_1, m_2;
  matrix2 line, tangent1, tangent2, cov_0, cov_1, tangent;
 
  for(int i = 0; i < (int)geometry.size(); i++)
  {
    //FOR TESTING PROB_PLANNER
    //cout << "i: " << i << endl;
    if(i + 1 < (int)geometry.size())
    {
      p0 = geometry[i];
      p1 = geometry[i + 1];
    }
    else
    {
      p0 = geometry[i];
      p1 = geometry[0];
    }

    // cout << "p0: " << p0 << ", p1: " << p1 << endl;

    d_x = p1.x - p0.x;
    d_y = p1.y - p0.y;

    //adjacent points are the same! As the edge, thus, has a length of zero can skip
    if(d_x == 0 && d_y == 0)
      break;


    line = matrix2(d_x, 0, d_y, 0);   //matrix representation of slope  of line
         
    //slope of line
    if(d_x != 0)
      m_1 = d_y/d_x;
    else
      m_1 = INFINITY;

    //cout << "d_x: " << d_x << ", d_y: " << d_y << endl;

    //matrix representation of tangent line in both directions with associated vector (2D point) representation
    tangent1 = matrix2(d_y, 0, -d_x, 0); 
    tangent1_p = point2(d_y, -d_x);
    tangent2 = matrix2(-d_y, 0, d_x, 0);  
    tangent2_p = point2(-d_y, d_x);

    //slope of tangent line
    if(m_1 != 0)
      m_2 = -1/m_1;
    else
      m_2 = INFINITY;

    //cout << "m_1: " << m_1 << endl;
    //cout << "m_2: " << m_2 << endl;

    //covariance matrices for point a and point b
    cov_0 = covarianceMatrix(sigma*sqrt(p0.max_var), sigma*sqrt(p0.min_var), p0.axis);
    cov_1 = covarianceMatrix(sigma*sqrt(p1.max_var), sigma*sqrt(p1.min_var), p1.axis);

    if(geometry.size() == 2)
    {
      if(cov_0.a < .0000001 && cov_0.b <.0000001 && cov_0.c < .0000001 && cov_1.a < .0000001 && cov_1.b <.0000001 && cov_1.c < .0000001)
	expandGeo = geometry;
      else
      {

	//points furthest on ellipses in tangent directions
	a = growLine(tangent1, cov_0, cov_1) + p0;
	b = growLine(tangent2, cov_0, cov_1) + p0;
	
	//points furthest on ellipses in direction of line
	c = growSide(matrix2(0,0,0,0)-line, cov_0) + p0;
	d = growSide(line, cov_1) + p1;

	//Now that have points that fall on four sides
	// of rectangle can solve for vertices
	
	if(m_1 == INFINITY)
	  {
	    x1 = a.x;
	    y1 = c.y;
	    x2 = b.x;
	    y2 = c.y;
	    x3 = b.x;
	    y3 = d.y;
	    x4 = a.x;
	    y4 = d.y;
	  }
	else if(m_1 == 0)
	  {
	    x1 = c.x;
	    y1 = a.y;
	    x2 = c.x;
	    y2 = b.y;
	    x3 = d.x;
	    y3 = b.y;
	    x4 = d.x;
	    y4 = a.y;
	  }
	else
	  {
	    b1 = a.y - m_1*a.x;
	    b2 = b.y - m_1*b.x;
	    b3 = c.y - m_2*c.x;
	    b4 = d.y - m_2*d.x;
      
	    //ordered x,y coordinates for four vertices of rectangle
	    x1 = (b3 - b1)/(m_1 - m_2);
	    y1 = m_1*x1 + b1;
	    x2 = (b4 - b1)/(m_1 - m_2);
	    y2 = m_1*x2 + b1;
	    x3 = (b4 - b2)/(m_1 - m_2);
	    y3 = m_1*x3 + b2;
	    x4 = (b3 - b2)/(m_1 - m_2);
	    y4 = m_1*x4 +  b2;
	  }

	expandGeo.push_back(point2(x1, y1));
	expandGeo.push_back(point2(x2, y2));
	expandGeo.push_back(point2(x3, y3));
	expandGeo.push_back(point2(x4, y4));
	
	return;  //don't need to loop through, already found four vertices
      }
    }
    else  //geometry.size() > 2
    {
      //if uncertainty of two edge points is very small, simply push back original points
      if(cov_0.a < .0000001 && cov_0.b <.0000001 && cov_0.c < .0000001 && cov_1.a < .0000001 && cov_1.b <.0000001 && cov_1.c < .0000001)
	{
	  //ensures that don't push back adjacent points which are equivalent
	  if(i == 0)
	  {
	    firstPoint =p0;
	    prevPoint = p1;
	    
	    expandGeo.push_back(p0);
	    expandGeo.push_back(p1);
	  }
	  else if(i == (int)geometry.size() - 1)
	  {
	    if(prevPoint != p0)
	      expandGeo.push_back(p0);
	    
	    if(firstPoint != p1)
	      expandGeo.push_back(p1);
	  }
	  else
	  {
	    if(prevPoint != p0)
	      expandGeo.push_back(p0);
	    
	    prevPoint = p1;
	    expandGeo.push_back(prevPoint);
	  }
		
	}
      else
	{

	  //determine which of the tangent directions points to the outside of the object
	  if((p0 + tangent1_p/tangent1_p.norm() - position).norm() >= (p0 + tangent2_p/tangent2_p.norm() - position).norm())
	    tangent = tangent1;
	  else
	    tangent = tangent2;
	  
	  //calculate furthest point on uncertainty ellipses in tangent direction
	  a = growLine(tangent, cov_0, cov_1) + p0;
	  
	  //calculate furthest points on uncertaty ellipses in direction of line
	  b = growSide(matrix2(0,0,0,0) -line, cov_0) + p0;
	  c = growSide(line, cov_1) + p1;
	  


	  //cout << "a = " << a << ", b = " << b << ", c = " << c <<  endl;

	  //use furthest points to find expanded line vertices using equations of line
     
	  if(m_1 == INFINITY)
	    {
	      x1 = a.x;
	      y1 = b.y;
	      x2 = a.x;
	      y2 = c.y;
	    }
	  else if(m_1 == 0)
	    {
	      x1 = b.x;
	      y1 = a.y;
	      x2 = c.x;
	      y2 = a.y;
	    }
	  else
	    {
	      b1 = a.y - m_1*a.x;
	      b2 = b.y - m_2*b.x;
	      b3 = c.y - m_2*c.x;
	      x1 = (b2-b1)/(m_1 - m_2);
	      y1 = m_1*x1 + b1;
	      x2 = (b3-b1)/(m_1 - m_2);
	      y2 = m_1*x2 + b1;
	    }
      

	  //Method 1, but ensures that not pushing back adjacent points which are identical
	  if(i == 0)
	  {
	    firstPoint = point2(x1, y1);
	    prevPoint = point2(x2, y2);

	    expandGeo.push_back(firstPoint);
	    expandGeo.push_back(prevPoint);
	  }
	  else if (i == (int)geometry.size() - 1)
	  {
	    if(prevPoint != point2(x1, y1))
	      expandGeo.push_back(point2(x1, y1));
	    
	    if(firstPoint != point2(x2, y2))
	      expandGeo.push_back(point2(x2, y2));
	  }
	  else
	  {
	    if(prevPoint != point2(x1, y1))
	      expandGeo.push_back(point2(x1, y1));

	    prevPoint = point2(x2, y2);
	    expandGeo.push_back(point2(x2, y2));

	  }	  

      

	  //METHOD 1:  doubles number of points of geometry; doesn't work very well---creates odd shapes
	  //expandGeo.push_back(point2(x1, y1));
	  //expandGeo.push_back(point2(x2, y2));
      

	  //METHOD 2:  doesn't work for many geometries; maintains number of points by picking one furthest from
	  //center of geometry; thus, doesn't necessarily cover the ellipses, which is bad
	  
	  // curPoint = point2(x1, y1);
	  
	  //cout << "curPoint: " << curPoint.x << ", " << curPoint.y << endl;

	  //determines which point to use from two intersecting expanded lines
	  //   if(i == 0)
	  //	firstPoint = point2(x1, y1);
	  //else
	  //{
	  //if((curPoint - center).norm() > (prevPoint - center).norm())
	  //  expandGeo.push_back(curPoint);
	  //else
	  //  expandGeo.push_back(prevPoint);
	  // }

	  // prevPoint = point2(x2, y2);
	  
	  // cout << "prevPoint: " << prevPoint.x << ", " << prevPoint.y << endl;
	  
	  //if(i == (int)geometry.size() -1)
	  //{
	  //	if((prevPoint - center).norm() > (firstPoint - center).norm())
	  //  expandGeo.push_back(prevPoint);
	  //else
	  //  expandGeo.push_back(firstPoint);
	  //}
	}
    }
  }

  return;
}
 


//Uses the point2 uncertain parameters to calculate the covariance matrix
//Covariance Matrix: C = [cxx, cxy; cxy, cyy]
//Equation for ellipse: D^2 = a*x^2 + b*y^2 + 2*cx*y --> D^2 = x^t*C^-1*x
matrix2 MapElement::covarianceMatrix(double max, double min, double axis)
{

  double maxDist, minDist;

  // assert (max >= min);
  //assert (min >= 0); 

  //hack; should create a very thin ellipse when uncertain geometry has no variance....
  if(max == 0)
  {
    maxDist = .000000001;
  }
  else
  {
    maxDist = max;
  }

  if(min == 0)
  {
    minDist = .000000001;
  }
  else
  {
    minDist = min;
  }

  //cout << "minDist: " << minDist << ", maxDist: " << maxDist << endl;


  double a, b, c, c_xx, c_yy, c_xy;
    
  a = pow(cos(axis), 2)*pow(1/maxDist, 2) + pow(sin(axis), 2)*pow(1/minDist, 2);
  b = pow(sin(axis), 2)*pow(1/maxDist, 2) + pow(cos(axis), 2)*pow(1/minDist, 2);
  c = sin(axis)*cos(axis)*(pow(1/maxDist, 2) - pow(1/minDist, 2));

  if((a*b - pow(c, 2)) == 0)
  {
    //cout << "Denominator is zero!" << endl;
    c_xx = 0;
    c_yy = 0;
    c_xy = 0;
  }
  else
  {
    c_xx = b/(a*b - pow(c, 2));
    c_yy = a/(a*b - pow(c, 2));
    c_xy = -c/(a*b - pow(c, 2));
  }

  //FOR TESTING PROB_PLANNER
  //cout << "c_xx: " << c_xx << ", c_yy: " << c_yy << ", c_xy: " << c_xy << endl; 

  return matrix2(c_xx, c_xy, c_xy, c_yy);

}


//Helper method of expandGeometry
//Inputs:  tangent direction of line, covariance matrices of both end points
//Outputs:  furthest point on ellipses in direction of tangent line 
point2 MapElement::growLine(matrix2 tangent, matrix2 cov_0, matrix2 cov_1)
{

  matrix2 t_tangent = matrix2(tangent.a, tangent.c, 0, 0);   //transpose of tangent line
  point2 a;  //furthest point on ellipse in direction tangent to line 

  if((sqrt((t_tangent*cov_0*tangent).a) >= sqrt((t_tangent*cov_1*tangent).a)) && ((t_tangent*cov_0*tangent).a != 0))  //determines whether furthest point is on ellipse 0 or ellipse 1
    a = point2((cov_0*tangent).a, (cov_0*tangent).c)/sqrt((t_tangent*cov_0*tangent).a);
  else if((t_tangent*cov_1*tangent).a != 0)
    a = point2((cov_1*tangent).a, (cov_1*tangent).c)/sqrt((t_tangent*cov_1*tangent).a);
  else
    a = point2(0, 0);
  return a;
}


//Helper method of expandGeometry
//Inputs:  direction of line, covariance matrix of end point
//Output:  furthest distance on ellipse parametrized by covariance
//in direction of line
point2 MapElement::growSide(matrix2 line, matrix2 cov)
{
  point2 a;
  matrix2 t_line = matrix2(line.a, line.c, 0, 0); //transpose of line
  
  if((t_line*cov*line).a != 0)
    a = point2((cov*line).a, (cov*line).c)/sqrt((t_line*cov*line).a);  //distance to furthest point of ellipse in direction of line
  else
    a = point2(0,0);

  return a;
}








//--------------------------------------------------
// old stuff
//--------------------------------------------------


void MapElement::set_geometry(point2arr_uncertain &ptarr)
{
  geometry = ptarr;
  updateFromGeometry();
}
 
void MapElement::set_geometry(point2arr &ptarr)
{
  geometry = ptarr;
  updateFromGeometry();
}

void MapElement::set_geometry(point2 &pt)
{
  geometry.clear();
  geometry.push_back(pt);
  updateFromGeometry();

}

void MapElement::set_geometry(point2 &pt, double radius)
{
  geometry.clear();
  geometry.push_back(pt);
  updateFromGeometry();
  width = radius;
  length = radius;

}

void MapElement::set_geometry(vector<point2> &ptarr){
  point2arr tmparr;
  tmparr = ptarr;
  geometry = tmparr;
  updateFromGeometry();
}
void MapElement::set_geometry(vector<point2_uncertain> &ptarr)
{
  geometry = ptarr;
  updateFromGeometry();
}

void MapElement::set_geometry(vector<double> &xarr, vector<double> &yarr)
{
  // NOT IMPLEMENTED!!
}

void  MapElement::set_clear(vector<int>& ident)
{
  clear();
  id = ident;
  type = ELEMENT_CLEAR;
  
}
void  MapElement::set_alice(VehicleState &statein)
{
  clear();
  id = 0;
  type = ELEMENT_ALICE;
  state = statein;
}
void  MapElement::set_circle_obs(vector<int>& ident, point2 cpt, double radius){
  id = ident;
  center = cpt;
  length = radius;
  width = radius;
}
void  MapElement::set_circle_obs()
{
  height = 10;
  type = ELEMENT_OBSTACLE;
}

void  MapElement::set_poly_obs(vector<int>& ident, vector<point2>& ptarr){
  id = ident;
  geometry.clear();
  for (int i = 0; i < (int)ptarr.size(); ++i){
    geometry.push_back(ptarr[i]);
  }
  height = 10;
  type = ELEMENT_OBSTACLE;
  //center_type = CENTER_BOUND_BOX;
  geometryType = GEOMETRY_POLY;
  updateFromGeometry();
}
void  MapElement::set_block_obs(vector<int>& ident, point2 cpt, double ang, double len, double wid ){
  id = ident;
  center.x = cpt.x;
  center.y = cpt.y;
  orientation = ang;
  length = len;
  width = wid;
  height = 10;
  type = ELEMENT_OBSTACLE;
  geometryType = GEOMETRY_UNDEF;
}
void  MapElement::set_line(vector<int>& ident,  vector<point2>& ptarr){
  id = ident;
  geometry.clear();
  for (int i = 0; i < (int)ptarr.size(); ++i){
    geometry.push_back(ptarr[i]);
  }
  height = 0;
  set_line();
  updateFromGeometry();
}
void  MapElement::set_line(){
  type = ELEMENT_LINE;
  geometryType = GEOMETRY_LINE;

}

void  MapElement::set_stopline(vector<int>& ident, vector<point2>& ptarr){
  clear();
  id = ident;
  geometry.clear();
  for (int i = 0; i < (int)ptarr.size(); ++i){
    geometry.push_back(ptarr[i]);
  }  
  height = 0;
  type = ELEMENT_STOPLINE;
  geometryType = GEOMETRY_LINE;
  updateFromGeometry();
}



void  MapElement::set_laneline(vector<int>& ident, vector<point2>& ptarr){
  
  setId(ident);
  set_geometry(ptarr);
  set_laneline();

  plotColor = MAP_COLOR_BLUE;

  
}

void  MapElement::set_laneline(){
 
  height = 0;
  type = ELEMENT_LANELINE;
  geometryType = GEOMETRY_LINE;
  //  plot_color = MAP_COLOR_BLUE;
  //updateFromGeometry();
  
}

void  MapElement::set_points(vector<int>& ident, vector<point2>& ptarr){
  id = ident;
  geometry.clear();
  for (int i = 0; i < (int)ptarr.size(); ++i){
    geometry.push_back(ptarr[i]);
  }  
  type = ELEMENT_POINTS;
  geometryType = GEOMETRY_POINTS;
  updateFromGeometry();

}
void  MapElement::set_points(){
  height = 0;
  type = ELEMENT_POINTS;
  geometryType = GEOMETRY_POINTS;
}

void  MapElement::set_trav_path(vector<int>& ident,  vector<point2>& ptarr){

  type = ELEMENT_TRAV_PATH;
  geometryType = GEOMETRY_LINE;
  plotColor = MAP_COLOR_RED;
  id = ident;
  geometry.clear();
  for (int i = 0; i < (int)ptarr.size(); ++i){
    geometry.push_back(ptarr[i]);
  }
  height = 0;
  set_line();
  updateFromGeometry();
}
