/*
 *  Stage1Planner.hh
 *  Stage1Planner
 *
 *  Created by Noel duToit
 *  2/28/07
 */

#ifndef STAGE1PLANNER_HH
#define STAGE1PLANNER_HH

#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include "dgcutils/RDDF.hh"
#include "skynettalker/StateClient.hh"
#include "trajutils/TrajTalker.hh"
#include "skynettalker/RDDFTalker.hh"
#include "trajutils/CPath.hh"
#include "cmap/CMapPlus.hh"
#include "cmap/MapdeltaTalker.hh"
#include "dgcutils/DGCutils.hh"
#include "skynet/sn_msg.hh"
#include "interfaces/sn_types.h"

class CStage1Planner:public CStateClient, public CTrajTalker, public CRDDFTalker
{
    public:
    	CStage1Planner(int SkynetKey, bool WAIT_STATE);
       ~CStage1Planner(void);

		void ActiveLoop(void);

    private:		
		RDDF*     m_RDDF;
		CTraj*    m_Traj;
		CMapPlus* m_CostMapPlus;

		RDDFVector m_RDDFVector;

		int m_SendSocket;
		int m_RDDFSocket;
		
};

#endif /* STAGE1PLANNER_HH*/
