/*
 * ClothoidPlanner.hh
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 1 May 07- Kenny Oslund
	- Created file
 */

#ifndef CLOTHOIDPLANNER_HH
#define CLOTHOIDPLANNER_HH

#include "frames/pose2.hh"
#include "ElementaryPath.hh"
#include "../Environment.hh"
#include "../Stage1Planner.hh"
#include "dgcutils/DGCutils.hh"

// variables determining the number and distribution of precomputed clothoids
// NUM_SIGMAS should be odd or there won't a precomputed clothoid that is just
// straight
// NOTE: All the CLOTHOID_DB_* constants should be tuned a bit more
#define CLOTHOID_DB_NUM_SIGMAS			13
// how many different lengths to compute
#define CLOTHOID_DB_NUM_LENGTHS			4
// precomputed clothoid have curvature from +MAX_SIGMA to -MAX_SIGMA
#define CLOTHOID_DB_MAX_SIGMA			0.060
#define CLOTHOID_DB_MAX_LENGTH			12.0
// scale factor to make higher resolution (ie, shorter) clothoids just a little sharper
#define CLOTHOID_DB_SIGMA_ADJUSTMENT		0.015
// maximum resolution we will explore at (corresponds to highest length index and 
// shortest path in the m_pathDB array of precomputed clothoids
#define CLOTHOID_MAX_RESOLUTION			CLOTHOID_DB_NUM_LENGTHS
// max distance that the goal can move without having to recompute all the goal nodes
// beyond this threshold, all the finsh nodes will be deleted and recomputed
#define CLOTHOID_MAX_GOAL_CHANGE		0.5


class CClothoidPlanner
{
   protected:

	/* Database for precomputed clothoids, to use in the explore function */
	CElementaryPath *m_pathDB[CLOTHOID_DB_NUM_LENGTHS][CLOTHOID_DB_NUM_SIGMAS];

	/* The tree of nodes to search over- should contain the point to start
	   planning from (not a complete clothoid), and pointers to all the other
	   clothoids to search over */
	CElementaryPath *m_searchTreeBase;

	/* pointer to the finish node which is arrived at by the lowest cost path */
	CElementaryPath *m_bestFinish;

	/* value of the cost of the best finish node */
	double bestFinishCost;

	/* instance of the environment class, which is a container for info
	   passed from tplanner, such as legal driving area, cost map, etc.
	   note that only the pointer is stored, so the environment class
	   instance can be updated outside of the Clothoid planner class and
	   those updates will show here */
	CEnvironment *m_aliceEnv;

	/* instace of the vehicle state struct
	   note that only the pointer is stored, so the state struct
	   instance can be updated outside of the Clothoid planner class and
	   those updates will show here */
	VehicleState *m_aliceState;

	/* the resolution to explore at */
	int cur_resolution;

	/* store the finish location so that we can tell if its changed */
	pose2 goalPose;

   public:
	/* -------- Main interface functions for external programs ------- */
	/* each call to this updates the clothoid tree and all costs- this is
	   essentially all the work */
	bool generatePaths(double runTime, S1PlanMode planMode);

	/* get the direction that we need to go for the clothoid we're on */
	trajFmode getDirection();

	/* convert the lowest cost branch of the clothoid tree into a traj and
	   return it. generatePaths must have been called before this */
	bool getTraj(CTraj* m_Traj);

	/* -------------- Worker functions --------------------------*/
	SearchStatus search(CElementaryPath *m_nodeToSearch,
		bool in_reverse = false);
	
	bool explore(CElementaryPath *m_nodeToExplore,
		bool in_reverse = false);


	void setStartPoint();

	// update the start location. This includes deleting nodes that are bellow us => we will 
	// not be driving on them.
	void updateStart();

	// update the finish location. This involves deleting all the finish nodes and relinking 
	// them to the new finish
	void updateEnd();

	vector<CElementaryPath *> getBestClothoidSequence();

	vector<pose2> getBestPath();
	
    void generateVelocityProfile(CEnvironment *m_aliceEnv, vector<pose2> path, vector<double>& vProfile, vector<double>& aProfile);

	/* -------------- Initialization functions ----------------- */
	/* populate the clothoidDB. only run once at startup */

	/* constructor */
	CClothoidPlanner(CEnvironment *env, VehicleState *state);

	// initialize everything to an empty search tree- this is used on startup and
	// if the actual position gets too far from the previously generated trajectory
	void initialize();

	void precomputeClothoids();




};

#endif  // CLOTHOIDPLANNER_HH


