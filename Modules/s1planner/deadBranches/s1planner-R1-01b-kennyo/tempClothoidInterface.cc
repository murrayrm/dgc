/*
 * tempClothoidInterface.cc
 * temporary interface functions for 
 * the new planner stack to run the clothoid planner.
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 26 July 07- Kenny Oslund
	- Created file
 *
 */

#include "s1planner/tempClothoidInterface.hh"

GraphNode ClothoidPlannerInterface::nodeArr[GRAPH_PATH_MAX_NODES];

/* Do everything needed to generate a single trajectory and then clean up. All needed memory
   is allocated and freed in this function */
Err_t ClothoidPlannerInterface::GenerateTrajOneShot(int sn_key, bool disp_costmap, CSpecs_t cSpecs, Path_t *path, double runtime)
{
  CEnvironment *problem = new CEnvironment(&cSpecs);
  
  CClothoidPlanner *planner = new CClothoidPlanner(problem, NULL);
  
  planner->generatePaths(runtime);
  populatePath(planner, path);

	double pathLength;
	bool reaches_end;
        vector<pose2> solution = planner->getBestPath(&pathLength, &reaches_end);

	ofstream log("clothoidSolution.dat", ios_base::app);
	log << "****************** new planning cycle ***************** " << endl;
	log << " CEnvironment info:" << endl;
	log << " planning from: "; (problem->getStartPoint()).display(&log);
	log << " to: "; (problem->getEndPoint()).display(&log);
	if (problem->isPointLegal(problem->getStartPoint()))
		log << " Start point IS legal" << endl;
	else
		log << " Start point is NOT legal" << endl;
	if (problem->isPointLegal(problem->getEndPoint()))
		log << " End point IS legal" << endl;
	else
		log << " End point is NOT legal" << endl;

	planner->dumpBestSolution(&log);
	log << " num points in solution: " << solution.size() << endl;

  delete planner;
  delete problem; 
  return LP_OK;
}

void ClothoidPlannerInterface::populatePath(CClothoidPlanner *planner, Path_t *path)
{
	bool reaches_end;
	vector<CElementaryPath *> solution = 
		planner->getBestClothoidSequence(&reaches_end);

	if (NULL != path) {
	  path->valid = true;
	  path->collideObs = 0;
	  path->collideCar = 0;
	  
	  int i = 0;
	  for (vector<CElementaryPath *>::iterator j = solution.begin(); 
	       j != solution.end(); j++)
	  {
		vector<pose2> clothoid = (*j)->getClothoidPoints();
		for (vector<pose2>::iterator k = clothoid.begin(); k != clothoid.end();
			k++, i++)
		{
			nodeArr[i].pose.pos.x = k->x;
			nodeArr[i].pose.pos.y = k->y;
			nodeArr[i].pose.pos.z = 0;
			nodeArr[i].pose.rot = quat_from_rpy(0, 0, k->ang);
			// TODO: populate this with the correct info for either GRAPH_PATH_FWD 
			// or GRAPH_PATH_REV
			if ( (*j)->m_reverse )
				nodeArr[i].pathDir = GRAPH_PATH_REV;
			else
				nodeArr[i].pathDir = GRAPH_PATH_FWD;
			path->path[i] = &(nodeArr[i]);
		}
	  }
	  path->goalDist = i * PATH_SECTION_LENGTH;
	  path->pathLen = i;
	}

	return;
}

