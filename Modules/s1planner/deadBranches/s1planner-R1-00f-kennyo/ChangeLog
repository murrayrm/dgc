Tue May 22 17:46:58 2007	Kenny Oslund (kennyo)

	* version R1-00f-kennyo
	BUGS:  
	FILES: ClothoidPlanner.cc(24388), ClothoidPlanner.hh(24388),
		Environment.cc(24388), Stage1Planner.cc(24388),
		Stage1Planner.hh(24388)
	moved velocity profile generation code from clithoid planner to
	s1planner, since clothoid planner is really just a spacial planner.
	clothoid planner now just returns a vector of pose2's rather than a
	ctraj because it doesn't have the velocity profile. s1planner gets
	the velocity profile and fills in the ctraj (it also handles the
	stop condition)

	FILES: ClothoidPlanner.cc(24480), ClothoidPlanner.hh(24480),
		ElementaryPath.cc(24480), ElementaryPath.hh(24480),
		Environment.cc(24480), Environment.hh(24480),
		Stage1Planner.cc(24480)
	integrated stefanos new heuristic estimation function  initial
	implementation of code to update- rather than replace- the tree
	that we search over

	FILES: ClothoidPlanner.cc(24497)
	minor changes; preparing to release with improved heuristic

Mon May 21 17:30:09 2007	Kenny Oslund (kennyo)

	* version R1-00f
	BUGS:  
	FILES: ClothoidPlanner.cc(24294), ElementaryPath.cc(24294),
		Environment.cc(24294), Stage1Planner.cc(24294),
		Stage1Planner.hh(24294)
	added check for stop condition	removed check on the start point-
	now, if the start point isn't legal, s1planner will fail to find a
	traj, and the previous traj will be used. if we reach the end of
	the traj and the start point still isn't legal, s1planner will be
	stuck, but theoretically this shouldn't be possible because a traj
	will always end in a point that was legal when the traj was
	planned.

Sat May 19 15:16:26 2007	Kenny Oslund (kennyo)

	* version R1-00e
	BUGS:  
	FILES: ClothoidPlanner.cc(24037), Stage1Planner.cc(24037)
	removed check on whether point we're exploring is further away from
	us than the finish point  preparing to commit

Sat May 19 13:16:48 2007	Stefano Di Cairano (stefano)

	* version R1-00d
	BUGS:  
	FILES: ClothoidPlanner.cc(23187), ClothoidPlanner.hh(23187)
	added velocity profiler

	FILES: ClothoidPlanner.cc(23223), ClothoidPlanner.hh(23223),
		Environment.cc(23223), Stage1Planner.cc(23223)
	Test in closed-llop with tplanner. Something works but we still
	have problem with the corridor generation and with memory leaks

	FILES: ClothoidPlanner.cc(23358), Stage1Planner.cc(23358)
	debugging info

	FILES: ClothoidPlanner.cc(23606), ClothoidPlanner.hh(23606),
		ElementaryPath.cc(23606), Stage1Planner.cc(23606)
	Updated the cycle run time calculation, but this does not fix the
	latency problem.

	FILES: ClothoidPlanner.cc(23797)
	vevlocity profiler updated to use second order data

	FILES: ClothoidPlanner.cc(23862), ElementaryPath.cc(23862),
		Environment.cc(23862)
	implemented some logic to try to find the point closes to the end
	point if the end point is outside of the legal driving region 
	otherwise works pretty well

	FILES: ClothoidPlanner.cc(23980), ClothoidPlanner.hh(23980),
		ElementaryPath.cc(23980), Environment.cc(23980),
		Stage1Planner.cc(23980)
	Added check not to send an empty Ctraj. This makes Alice following
	the previous legal path. Added a check whether the initial point is
	inside the corridor with a connected switch to enable/disable
	corridor checks.

	FILES: ElementaryPath.cc(23626), ElementaryPath.hh(23626),
		Stage1Planner.cc(23626)
	-fixed a couple minor bugs from capitalization error in
	Stage1Planner.cc -changed it so that corridor checking is
	controlled by a constant defined  in
	clothoidplanner/ElementaryPath.hh

Mon May 14 14:03:56 2007	Kenny Oslund (kennyo)

	* version R1-00c
	BUGS:  
	New files: Environment.cc Environment.hh
		clothoidplanner/ClothoidPlanner.cc
		clothoidplanner/ClothoidPlanner.hh
		clothoidplanner/ClothoidUtils.cc
		clothoidplanner/ClothoidUtils.hh
		clothoidplanner/ElementaryPath.cc
		clothoidplanner/ElementaryPath.hh clothoidplanner/complex.h
		plot_clothoids.m
	FILES: Makefile.yam(19975), Stage1Planner.cc(19975),
		Stage1Planner.hh(19975), Stage1Planner_Main.cc(19975)
	set up additional structure for s1 planner:  * s1planner is now an
	abstract class. The class which extends it must implement the	the
	GenerateTraj function and fill in the m_Traj instance of the Ctraj
	class and the	scCmd instance of the superConTrajFcmd struct. *
	Created a CClothoidPlanner class, which extends CStage1Planner and
	implements the	 above things.

	FILES: Makefile.yam(20288), Stage1Planner.cc(20288),
		Stage1Planner.hh(20288), Stage1Planner_Main.cc(20288)
	started to port in code- just saving it to subversion

	FILES: Makefile.yam(20495), Stage1Planner.cc(20495)
	eliminated CCLandMark class- merged into CCElementaryPath  search
	almost implemented

	FILES: Makefile.yam(20549), Stage1Planner.cc(20549),
		Stage1Planner.hh(20549), Stage1Planner_Main.cc(20549)
	this is a mess

	FILES: Makefile.yam(20559), Stage1Planner.cc(20559),
		Stage1Planner.hh(20559), Stage1Planner_Main.cc(20559)
	attempted drop in use of dave knowles code; results in empty trajs.

	FILES: Makefile.yam(22307), Stage1Planner.cc(22307),
		Stage1Planner.hh(22307)
	Search basically works- it can connect symmetric points with a
	clothoid, and any arbitrary points with a bielementary Clothoid.
	There is a slight bug which sometimes causes it to connect a point
	to -x,-y when it should have connected the point to x,y, but it
	only does this on very obtuse and not useful paths anyways, and
	there is check to see if it actually worked, so this isn't a very
	big problem.  search is implemented in the CElementaryPath class,
	and the main function is called connectToFinish()

	FILES: Makefile.yam(22373)
	added libAlgGeom.a to the makefile (specifying the correct
	location).

	FILES: Makefile.yam(22596), Stage1Planner.hh(22596)
	search and explore both work  basic search-explore loop working
	using stub cost functions

	FILES: Makefile.yam(22762), Stage1Planner.cc(22762)
	first integration with ocpspecs for cost map and corridor  first
	attempt at running in closed loop

	FILES: Makefile.yam(23022), Stage1Planner.cc(23022)
	commit for intial release of s1planner	basically everything works
	except for the velocityplanner is a stub

	FILES: Stage1Planner.cc(20101), Stage1Planner.hh(20101),
		Stage1Planner_Main.cc(20101)
	fixed a bug with the constructor for the ClothoidPlanner class- the
	class which extends Stage1Planner must directly call the
	CSkynetContainer constructor in order for state data to work. the
	constructor now looks like this: 
	CClothoidPlanner::CClothoidPlanner(int SkynetKey, bool WAIT_STATE) 
	:
	CSkynetContainer(MODdynamicplanner,SkynetKey),CStage1Planner(Skynet
	Key, WAIT_STATE)

	FILES: Stage1Planner.cc(20561)
	hack to get around invalid initial heading from OCPspecs- I sent
	the current heading from the state data instead.

	FILES: Stage1Planner.cc(21823), Stage1Planner.hh(21823)
	My attempts to port pretty much failed- I'm now going to rewrite it
	from scratch  committing a version with most of my previous changes
	removed

	New files: Environment.cc Environment.hh
		clothoidplanner/ClothoidPlanner.cc
		clothoidplanner/ClothoidPlanner.hh
		clothoidplanner/ClothoidUtils.cc
		clothoidplanner/ClothoidUtils.hh
		clothoidplanner/ElementaryPath.cc
		clothoidplanner/ElementaryPath.hh clothoidplanner/complex.h
		plot_clothoids.m
	FILES: Makefile.yam(19975), Stage1Planner.cc(19975),
		Stage1Planner.hh(19975), Stage1Planner_Main.cc(19975)
	set up additional structure for s1 planner:  * s1planner is now an
	abstract class. The class which extends it must implement the	the
	GenerateTraj function and fill in the m_Traj instance of the Ctraj
	class and the	scCmd instance of the superConTrajFcmd struct. *
	Created a CClothoidPlanner class, which extends CStage1Planner and
	implements the	 above things.

	FILES: Makefile.yam(20288), Stage1Planner.cc(20288),
		Stage1Planner.hh(20288), Stage1Planner_Main.cc(20288)
	started to port in code- just saving it to subversion

	FILES: Makefile.yam(20495), Stage1Planner.cc(20495)
	eliminated CCLandMark class- merged into CCElementaryPath  search
	almost implemented

	FILES: Makefile.yam(20549), Stage1Planner.cc(20549),
		Stage1Planner.hh(20549), Stage1Planner_Main.cc(20549)
	this is a mess

	FILES: Makefile.yam(20559), Stage1Planner.cc(20559),
		Stage1Planner.hh(20559), Stage1Planner_Main.cc(20559)
	attempted drop in use of dave knowles code; results in empty trajs.

	FILES: Makefile.yam(22307), Stage1Planner.cc(22307),
		Stage1Planner.hh(22307)
	Search basically works- it can connect symmetric points with a
	clothoid, and any arbitrary points with a bielementary Clothoid.
	There is a slight bug which sometimes causes it to connect a point
	to -x,-y when it should have connected the point to x,y, but it
	only does this on very obtuse and not useful paths anyways, and
	there is check to see if it actually worked, so this isn't a very
	big problem.  search is implemented in the CElementaryPath class,
	and the main function is called connectToFinish()

	FILES: Makefile.yam(22373)
	added libAlgGeom.a to the makefile (specifying the correct
	location).

	FILES: Makefile.yam(22596), Stage1Planner.hh(22596)
	search and explore both work  basic search-explore loop working
	using stub cost functions

	FILES: Makefile.yam(22762), Stage1Planner.cc(22762)
	first integration with ocpspecs for cost map and corridor  first
	attempt at running in closed loop

	FILES: Makefile.yam(23022), Stage1Planner.cc(23022)
	commit for intial release of s1planner	basically everything works
	except for the velocityplanner is a stub

	FILES: Stage1Planner.cc(20101), Stage1Planner.hh(20101),
		Stage1Planner_Main.cc(20101)
	fixed a bug with the constructor for the ClothoidPlanner class- the
	class which extends Stage1Planner must directly call the
	CSkynetContainer constructor in order for state data to work. the
	constructor now looks like this: 
	CClothoidPlanner::CClothoidPlanner(int SkynetKey, bool WAIT_STATE) 
	:
	CSkynetContainer(MODdynamicplanner,SkynetKey),CStage1Planner(Skynet
	Key, WAIT_STATE)

	FILES: Stage1Planner.cc(20561)
	hack to get around invalid initial heading from OCPspecs- I sent
	the current heading from the state data instead.

	FILES: Stage1Planner.cc(21823), Stage1Planner.hh(21823)
	My attempts to port pretty much failed- I'm now going to rewrite it
	from scratch  committing a version with most of my previous changes
	removed

Wed Feb 28 18:00:59 2007	Stefano Di Cairano (stefano)

	* version R1-00b
	BUGS: 
	FILES: Makefile.yam(15987), Stage1Planner.cc(15987),
		Stage1Planner.hh(15987)
	Included interface to ocpspecs, which contains the problem specs
	and provides the interface to the specs.

Wed Feb 28 15:11:30 2007	Noel duToit (ndutoit)

	* version R1-00a
	BUGS: 
	New files: Stage1Planner.cc Stage1Planner.hh Stage1Planner_Main.cc
	FILES: Makefile.yam(15970)
	Shell for the stage 1 planner. The shell compiles and should be able to 
	receive the cmap and rddf, and send trajectory. Now needs to be populated 
	by David and Russell.

Wed Feb 28 10:21:48 2007	Noel duToit (ndutoit)

	* version R1-00
	Created s1planner module.







