(* arc

in	a starting x-y-phi configuration, e.g. {2, -3, Pi/2}
	an arc type, e.g. "s+"
	a distance, e.g. 10

out	an ending configuration and a plot, e.g. {{2, 7, Pi/2}, -Graphics-}

*)

arc/: arc[start_, type_, dist_] :=
Block[{x = start[[1]], y = start[[2]], phi = start[[3]],
       C = Cos[phi], S = Sin[phi], plot},
  Switch[type,
    "s+", Block[{},
            plot = ParametricPlot[{x+C t, y+S t}, {t,0,dist}];
            {{x + C dist, y + S dist, phi}, plot}],
    "s-", Block[{},
            plot = ParametricPlot[{x-C t, y-S t}, {t,0,dist}];
            {{x - C dist, y - S dist, phi}, plot}],
    "l+", Block[{},
            plot = ParametricPlot[{x - S + Sin[phi+t], 
                                   y + C - Cos[phi+t]}, {t,0,dist}];
            {{x-S+Sin[phi+dist], y+C-Cos[phi+dist], phi+dist}, plot}],
    "l-", Block[{},
            plot = ParametricPlot[{x - S + Sin[phi-t], 
                                   y + C - Cos[phi-t]}, {t,0,dist}];
            {{x-S+Sin[phi-dist], y+C-Cos[phi-dist], phi-dist}, plot}],
    "r+", Block[{},
            plot = ParametricPlot[{x + S + Sin[-phi+t], 
                                   y - C + Cos[-phi+t]}, {t,0,dist}];
            {{x+S+Sin[-phi+dist], y-C+Cos[-phi+dist], phi-dist}, plot}],
    "r-", Block[{},
            plot = ParametricPlot[{x + S + Sin[-phi-t], 
                                   y - C + Cos[-phi-t]}, {t,0,dist}];
            {{x+S+Sin[-phi-dist], y-C+Cos[-phi-dist], phi+dist}, plot}]
  ]
]

(* trace

in	a list of arc types, e.g. {"l+", "r-", "s-"}
	a list of corresponding distances, e.g. {3, 2, 7}

out	a list of plots, e.g. {-Graphics-, -Graphics-, -Graphics-}

*)

trace/: trace[types_, dists_] :=
Block[{n = Length[types], plots  = {}, state = {0,0,0}, plot},
  Do[
    Block[{},
      {state, plot} = arc[state, types[[i]], dists[[i]]];
      plots = Append[plots, plot]],
    {i, n}];
  plots]

view/: view[pnum_, dists_] :=
Block[{types = 
  Switch[pnum,
    1, {"l+", "r-", "l+"},		2, {"l-", "r+", "l-"},
    3, {"r+", "l-", "r+"},		4, {"r-", "l+", "r-"},
    5, {"l+", "r-", "l-"},		6, {"l-", "r+", "l+"},
    7, {"r+", "l-", "r-"},		8, {"r-", "l+", "r+"},
    9, {"l+", "s+", "l+"},		10, {"r+", "s+", "r+"},
    11, {"l-", "s-", "l-"},		12, {"r-", "s-", "r-"},
    13, {"l+", "s+", "r+"},		14, {"r+", "s+", "l+"},
    15, {"l-", "s-", "r-"},		16, {"r-", "s-", "l-"},
    17, {"l+", "r+", "l-", "r-"},	18, {"r+", "l+", "r-", "l-"},
    19, {"l-", "r-", "l+", "r+"},	20, {"r-", "l-", "r+", "l+"},
    21, {"l+", "r-", "l-", "r+"},	22, {"r+", "l-", "r-", "l+"},
    23, {"l-", "r+", "l+", "r-"},	24, {"r-", "l+", "r+", "l-"},
    25, {"l+", "r-", "s-", "l-"},	26, {"r+", "l-", "s-", "r-"},
    27, {"l-", "r+", "s+", "l+"},	28, {"r-", "l+", "s+", "r+"},
    29, {"l+", "r-", "s-", "r-"},	30, {"r+", "l-", "s-", "l-"},
    31, {"l-", "r+", "s+", "r+"},	32, {"r-", "l+", "s+", "l+"},
    33, {"l+", "r-", "s-", "l-", "r+"}, 34, {"r+", "l-", "s-", "r-", "l+"},
    35, {"l-", "r+", "s+", "l+", "r-"}, 36, {"r-", "l+", "s+", "r+", "l-"},
    37, {"l+", "r+", "l-"},		38, {"r+", "l+", "r-"},
    39, {"l-", "r-", "l+"},		40, {"r-", "l-", "r+"},
    41, {"l+", "s+", "r+", "l-"},	42, {"r+", "s+", "l+", "r-"},
    43, {"l-", "s-", "r-", "l+"},	44, {"r-", "s-", "l-", "r+"},
    45, {"l+", "s+", "l+", "r-"},	46, {"r+", "s+", "r+", "l-"},
    47, {"l-", "s-", "l-", "r+"},	48, {"r-", "s-", "r-", "l+"}
  ], plots},
  plots = trace[types,dists];
  Apply[Show,plots]
]

s/: s[] :=
Show[plot,
  AspectRatio->Automatic,
  Ticks->{{-.4,0,.4,.8},{.5,1,1.5,2,2.5}},
  PlotLabel->"R-S path of class R- L+ S+ R+ L-"
]

a = {0.346865, Pi/2, 0.062019, Pi/2, 0.346865}

