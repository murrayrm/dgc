/*
 * tempClothoidInterface.cc
 * temporary interface functions for 
 * the new planner stack to run the clothoid planner.
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 26 July 07- Kenny Oslund
	- Created file
 *
 */

#include "s1planner/tempClothoidInterface.hh"

GraphNode ClothoidPlannerInterface::nodeArr[GRAPH_PATH_MAX_NODES];

/* Do everything needed to generate a single trajectory and then clean up. All needed memory
   is allocated and freed in this function */
Err_t ClothoidPlannerInterface::GenerateTrajOneShot(int sn_key, bool disp_costmap, CSpecs_t cSpecs, Path_t *path, double runtime)
{
	Console::addMessage("CLOTHOIDPLANNER:: ******** new planning cycle ******** ");
	
	// create objects
	CEnvironment *problem = new CEnvironment(&cSpecs);
	CClothoidPlanner *planner = new CClothoidPlanner(problem, NULL);
	
	// do the actual work
	planner->generatePaths(runtime);
	populatePath(planner, path);

	// logging
	stringstream str;
	pose2 pt;
	// log inital point
	pt= problem->getStartPoint();
	Console::addMessage("CLOTHOIDPLANNER:: planning from: x: %lf y: %lf ang: %lf", pt.x, pt.y, pt.ang);
	// log final point
	pt = problem->getEndPoint();
	Console::addMessage("CLOTHOIDPLANNER:: to: x: %lf y: %lf ang: %lf", pt.x, pt.y, pt.ang);
/*	if (problem->isPointLegal(problem->getStartPoint()))
		Console::addMessage("CLOTHOIDPLANNER:: Start point IS legal");
	else
		Console::addMessage("CLOTHOIDPLANNER:: Start point is NOT legal");
	if (problem->isPointLegal(problem->getEndPoint()))
		Console::addMessage("CLOTHOIDPLANNER:: End point IS legal");
	else
		Console::addMessage("CLOTHOIDPLANNER:: End point is NOT legal"); */
	//planner->dumpBestSolution(&log);
	//log << " num points in solution: " << solution.size() << endl;

	// clean up
	delete planner;
	delete problem; 
	return LP_OK;
}

void ClothoidPlannerInterface::populatePath(CClothoidPlanner *planner, Path_t *path)
{
	bool reaches_end;
	vector<CElementaryPath *> solution = planner->getBestClothoidSequence(&reaches_end);

	if (NULL != path) {
	  path->valid = true;
	  path->collideObs = 0;
	  path->collideCar = 0;
	  
    if (solution.size() > 0) {
	  // we skip the first point of each clothoid so that we don't end up with
	  // points right on top each other (that point was in the previous clothoid).
	  // however, we do want the first point of the first clothoid because there was
	  // no previous clothoid
      // handle the special case of the first point
      pose2 firstPt = solution.at(0)->getFirstPoint();
	setPoint(0, firstPt, solution.at(0)->m_reverse, path);

      // The rest of the points
      int i = 1;
      for (vector<CElementaryPath *>::iterator j = solution.begin(); 
           j != solution.end(); j++) {
        vector<pose2> clothoid = (*j)->getClothoidPoints();
        // skip the first point of each clothoid so that we don't end up
        // with 2 points in exactly the same place
        for (vector<pose2>::iterator k = clothoid.begin() + 1; k != clothoid.end();
             k++, i++) {
		setPoint(i, *k, (*j)->m_reverse, path);
        }
      }
      path->goalDist = i * PATH_SECTION_LENGTH;
      path->pathLen = i;
    }
  }
  return;
}

void ClothoidPlannerInterface::setPoint(int index, pose2 newPt, bool reverse, Path_t *path)
{
          nodeArr[index].pose.pos.x = newPt.x;
          nodeArr[index].pose.pos.y = newPt.y;
          nodeArr[index].pose.pos.z = 0;
          if ( reverse ) {
            nodeArr[index].pathDir = GRAPH_PATH_REV;
	    // reverse the direction of the pose to point in the direction we're going rather
	    // than the direction we're pointing if we're going in reverse
            newPt.rotateInPlace(M_PI);
          } else
            nodeArr[index].pathDir = GRAPH_PATH_FWD;
          nodeArr[index].pose.rot = quat_from_rpy(0, 0, newPt.ang);
          path->path[index] = &(nodeArr[index]);
}





