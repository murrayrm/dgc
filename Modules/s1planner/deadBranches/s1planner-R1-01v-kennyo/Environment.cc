/*
 * Environment.cc
 * Container for information about the enviroment Alice is working in.
 * This is pretty much just a wrapper for OCPspecs- ideally, everything
 * would be implemented in OCPspecs (which this class extends), and this
 * doesn't have any members explicitly in it. However, somethings we may
 * not want to put in OCPspecs, or we may want to override what is in
 * OCPspeacs for testing purposes. In those cases, methods can be put in
 * put in here. However, methods in here should be kept general and
 * independent of any structures unique to a specific planner.
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 1 May 07- Kenny Oslund
	- Created file
 *
 */

#include "Environment.hh"

#include "reed-shepp/reed_shepp.h"


/* normal constructor */
CEnvironment::CEnvironment(CSpecs_t *problem)
{
	update(problem);
	return;
}

/* normal constructor - BE SURE TO CALL update WITH A POINTER TO A CSPECS BEFORE 
   YOU TRY TRY TO USE THIS IF YOU CALL THIS CONSTRUCTOR*/
CEnvironment::CEnvironment()
{
	update();
	return;
}


// populate any CEnvironment variables that need to be populated from CSpecs vairables
void CEnvironment::update(CSpecs_t *problem)
{
	m_cspecs = problem;

	if (m_cspecs != NULL)
	{
		// Log::getStream(1) << "CLOTHOIDPLANNER: Number of obstacles: " << (m_cspecs->getPolyObstacles()).size() << endl;
			
		updateSafety(SAFETY_PERIMETER);
		updateSafety(SAFETY_OBSTACLE);
		updateCorridor();
	}
}

// populate the corrodor in CEnvironement based on the corridor in cspecs. this will 
// grow the corridor so that the initial and final poses are legal.
void CEnvironment::updateCorridor()
{
	m_grownCorridor = m_cspecs->getBoundingPolygon();
	// Log::getStream(1) << "CLOTHOIDPLANNER: corridor before growing: " << m_grownCorridor << endl;
	if ( !isInsideCorridor(getStartPoint()) )
		growCorridor(getStartPoint());
	if ( !isInsideCorridor(getEndPoint()) )
		growCorridor(getEndPoint());
	// Log::getStream(1) << "CLOTHOIDPLANNER: corridor after growing: " << m_grownCorridor << endl;
}


// set a polygon representing alice and the specified safety region around her
void CEnvironment::updateSafety(int safety_type)
{
	point2arr *safetyZone;
	vector<double> safetyMargins;
	switch (safety_type)
	{
		case SAFETY_PERIMETER:
			safetyZone = &m_perimeterSafety;
			safetyMargins = m_cspecs->getPerimeterSafetyMargins();
			break;
		case SAFETY_OBSTACLE:
			safetyZone = &m_obstacleSafety;
			safetyMargins = m_cspecs->getObstacleSafetyMargins();
			break;
	}
	safetyZone->clear();
	// populate a polygon which represents alice with the safety margins
	// if she's at the origin with 0 angle
	double front = DIST_REAR_AXLE_TO_FRONT + safetyMargins.at(SAFETY_FRONT);
	double back = -1 * (DIST_REAR_TO_REAR_AXLE + safetyMargins.at(SAFETY_BACK));
	double right = -1 * (VEHICLE_WIDTH / 2 + safetyMargins.at(SAFETY_RIGHT));
	double left = VEHICLE_WIDTH / 2 + safetyMargins.at(SAFETY_LEFT);
	point2 frontLeft(front, left);
	point2 frontRt(front, right);
	point2 backRt(back, right);
	point2 backLeft(back, left);
	safetyZone->push_back(frontLeft);
	safetyZone->push_back(frontRt);
	safetyZone->push_back(backRt);
	safetyZone->push_back(backLeft);
	if ( safety_type == SAFETY_PERIMETER )
		m_aliceDiagonalDist = (frontLeft - backRt).norm();
}

// assuming that alice is at aliceLocation and that at least some of her is sticking
// into the corridor, grow the corridor by unioning an area around 
// with the existing corridor. This is to ensure that alice's location is legal
void CEnvironment::growCorridor(pose2 aliceLocation)
{
	// the amount beyond alice to grow the boundary
	#define ENVIRONMENT_GROW_BEYOND_ALICE		2
	vector<double> safetyMargins = m_cspecs->getPerimeterSafetyMargins();
	point2arr grownAlice;
	vector<point2arr> grownRegion;

	// populate a polygon which represents alice with the safety margins
	// if she's at the origin with 0 angle
	double front = DIST_REAR_AXLE_TO_FRONT + safetyMargins.at(SAFETY_FRONT) + ENVIRONMENT_GROW_BEYOND_ALICE;
	double back = -1 * (DIST_REAR_TO_REAR_AXLE + safetyMargins.at(SAFETY_BACK) + ENVIRONMENT_GROW_BEYOND_ALICE);
	double right = -1 * (VEHICLE_WIDTH / 2 + safetyMargins.at(SAFETY_RIGHT) + ENVIRONMENT_GROW_BEYOND_ALICE);
	double left = VEHICLE_WIDTH / 2 + safetyMargins.at(SAFETY_LEFT) + ENVIRONMENT_GROW_BEYOND_ALICE;
	point2 frontLeft(front, left);
	point2 frontRt(front, right);
	point2 backRt(back, right);
	point2 backLeft(back, left);
	grownAlice.push_back(frontLeft);
	grownAlice.push_back(frontRt);
	grownAlice.push_back(backRt);
	grownAlice.push_back(backLeft);
	
	point2 pt(aliceLocation.x, aliceLocation.y);
	// rotate alice about the origin and translate her to where she is
	point2arr grownAliceTranslated = grownAlice.rot(aliceLocation.ang) + pt;
	int numPolygons = m_grownCorridor.get_poly_union(grownAliceTranslated, grownRegion);
	if (numPolygons == 1)
		m_grownCorridor = grownRegion.front();
	else
		Log::getStream(1) << "CLOTHOIDPLANNER: " << __FILE__ << ", line " << __LINE__ <<
			": ERROR:  Alice is completely out of the corridor; this algorithm cannot " <<
			"grow the corridor to compensate for this" << endl;
	return;
}

// get alice at the origin, with a safety zone around her
point2arr CEnvironment::getSafety(int safety_type)
{
	point2arr tmp;
	switch (safety_type)
	{
		case SAFETY_PERIMETER:
			return m_perimeterSafety;
		case SAFETY_OBSTACLE:
			return m_obstacleSafety;
	}
	return tmp;	// return something empty as the default; mainly to get rid or compiler warnings
}
// get alice rotated and translated to the origin, with a safety region around her
point2arr CEnvironment::getSafety(int safety_type, pose2 location)
{
	point2arr grownAlice = getSafety(safety_type);
	point2 pt(location.x, location.y);
	// rotate alice about the origin
	point2arr alice_rotated = grownAlice.rot(location.ang);
	// translate her to where she is
	point2arr alice_trans = alice_rotated + pt;
	return alice_trans;
}


/* return the average of the lower and upperbounds for the
   start and end points. if pointers to lowerbound and upper
   bound pose2 structs are provided, those objects will be 
   filled in appropiately */
pose2 CEnvironment::getStartPoint()
{
	vector<double> conds = m_cspecs->getStartingState();
	pose2 start(conds.at(STATE_IDX_X), conds.at(STATE_IDX_Y), conds.at(STATE_IDX_THETA));
	return start;
}


pose2 CEnvironment::getEndPoint()
{
	vector<double> conds = m_cspecs->getFinalState();
	pose2 goal(conds.at(STATE_IDX_X), conds.at(STATE_IDX_Y), conds.at(STATE_IDX_THETA));
	return goal;
}


// a pointer to a point2arr which contains the surrounding polygon can optionally 
// be passed in for speed, or it can be generated here.
bool CEnvironment::isPointLegal(pose2 point)
{
	// debugging info
	// cout << "+++ translated Alice: " << alice_trans << endl;
	// cout << "+++ alice in boundary: " << intersection << endl;
	// cout << "+++ boundary: " << boundary << endl;

	#define CHECK_ALICE_IN_CORRIDOR
	#ifdef CHECK_ALICE_IN_CORRIDOR
	return ( isInsideCorridor(point) && !intersectsObstacle(point) );
	// Log::getStream(1) << "CLOTHOIDPLANNER: point is outside of corridor at "; point.display(&Log::getStream(1));
	// Log::getStream(1) << "CLOTHOIDPLANNER: point causes alice to hit an obstacle at "; point.display(&Log::getStream(1));
	#else
	point2 pt(point.x, point.y);
	return boundary.is_inside_poly(pt);
	#endif
}

bool CEnvironment::isInsideCorridor(pose2 point)
{
	/* --------------- Corridor check: is all of alice in the corridor ---------------- */
	// take the intersection of alice and the boundary region. the result is the part of alice
	// that is inside the boundary. then we just have to check whether the intersection is equal 
	// to the original alice.
	point2arr alice = getSafety(SAFETY_PERIMETER, point);
	vector<point2arr> alice_in_boundary;
	int num_polygon_intersections = alice.get_poly_intersection(m_grownCorridor, alice_in_boundary);

	// Taking the intersection can change the direction of the order of the points,
	// and the first point can be changed. so there are 3 conditions to check for whether the
	// intersection is equal to the original alice:
	// 1) there is still only 1 polygon in the intersection (alice didn't get cut into >1 piece)
	// 2) the polygon still has 4 points (its still a rectangle)
	// 3) the points are still in order, so points 1 and 3 of the intersection will still lie 
	// on a diagonal, as will points 2 and 4. since we took the intersection the diagonal distances
	// would necessarily be equal if the shape didn't change, and shorter if it did since it can
	// only get smaller; therefore we can just check those

	if ( num_polygon_intersections == 1 )
	{
		point2arr intersection = alice_in_boundary.front();
		if ( ! (intersection.size() != 4
			|| fabs((intersection[1] - intersection[3]).norm() - m_aliceDiagonalDist) > 1e-3
			|| fabs((intersection[0] - intersection[2]).norm() - m_aliceDiagonalDist) > 1e-3) )
				return true;
	}
	return false;
}

bool CEnvironment::intersectsObstacle(pose2 point)
{
	/* ----------- Obstacle check: does alice intersect any obstacles (hard constraints) ---------- */
	point2arr alice = getSafety(SAFETY_OBSTACLE, point);
	vector<point2arr> obstacles = m_cspecs->getPolyObstacles();
	for (vector<point2arr>::iterator i = obstacles.begin(); i != obstacles.end(); i++)
	{
		if ( alice.is_poly_overlap(*i) )
			return true;
	}
	return false;
}

bool CEnvironment::isPathInsideCorridor(vector<pose2> *path)
{
	// if even 1 point is illegal, the whole path is illegal
	// thanks to the continuity of the path, and the fact that we're checking whether all
	// of alice is inside of the zone, we don't really have to check every point, as long
	// the front of alice at one point we check overlaps with the back of alice at the 
	// next point that we check. 
	int increment = LEGALITY_CHECK_INCREMENT; 
	for (vector<pose2>::iterator i = path->begin(); i < path->end(); i += increment)
		if ( !isInsideCorridor(*i) )
			return false;
	// also check the last point so we don't miss it
	if (!isInsideCorridor(path->back()))
		return false;

	return true;
}

bool CEnvironment::doesPathIntersectObstacle(vector<pose2> *path)
{
	int increment = LEGALITY_CHECK_INCREMENT; 
	for (vector<pose2>::iterator i = path->begin(); i < path->end(); i += increment)
		if ( intersectsObstacle(*i) )
			return true;
	// also check the last point so we don't miss it
	if (intersectsObstacle(path->back()))
		return true;

	return false;
}

/* return whether all the points in the specified path are within
	the legal driving region; should use the function isPointLegal.
	also note that it takes a pointer to a vector of points, not
	an actual vector (this is done for speed- passing a pointer is
	faster), so it needs to be sure NOT to modify the points */
bool CEnvironment::isPathLegal(vector<pose2> *path)
{
	return ( isPathInsideCorridor(path) && !doesPathIntersectObstacle(path) );
}

/* get the cost a the given point */
double CEnvironment::getPointCost(pose2 point)
{
	CostMap *cmap = m_cspecs->getCostMap();
	point2 p = point2(point.x,point.y);
	// multiplying by PATH_SECTION_LENGTH is to normalize the cost
//	double res = PATH_SECTION_LENGTH * cmap->getPixelLoc(p);
 	double res = 1;
// 	Log::getStream(1) << "point Value: " << res << "at "; point.display();
	return res;
}


/* sum up the cost along all the points in the path and return
	that. should use the getPointCost function and should NOT 
	modify the points (it takes a pointer like isPathLegal) */
double CEnvironment::getPathCost(vector<pose2> *path)
{
	double totalCost = 0;

	// skip the first point because it would have been taken into account in the previous path
	for (vector<pose2>::iterator i = path->begin() + 1; i != path->end(); i++)
		totalCost += getPointCost(*i);

	return totalCost;
}

/* return the cost heuristic, the estimated cost from point to
	the finish */
double CEnvironment::getHeuristicToGoal(pose2 start, bool is_reverse)
{
	return getHeuristic(start, getEndPoint(), is_reverse);
}

/* return the heuristic between 2 arbitrary points */
double CEnvironment::getHeuristic(pose2 start, pose2 goal, bool is_reverse)
{

	#ifdef HEURISTIC_USE_RS
	/****** Reed-Shepp metric heuristic ******/

	double start_arr[3], goal_arr[3], length, t_r, u_r, v_r;
	int num_reverses, first_dir, rs_opposite_dir;
	int solution_num;

	// project the goal backwards when calculating the heuristic- this is because 
	// reed-shepp assumes you can turn the wheel infinitely fast, while we can't
	// => we can't actually follow a reed shepp path (we need more room)
	pose2 heuristic_min = pose2( HEURISTIC_REED_SHEPP_PROJECT_DIST * cos( goal.ang + M_PI ), 
		HEURISTIC_REED_SHEPP_PROJECT_DIST * sin( goal.ang + M_PI ), 0 );
	if ( getPlanMode() != reverse_required )
		heuristic_min += goal;
	else
		heuristic_min -= goal;

	start_arr[0] = start.x;
	start_arr[1] = start.y;
	start_arr[2] = start.ang;

	goal_arr[0] = heuristic_min.x;
	goal_arr[1] = heuristic_min.y;
	goal_arr[2] = heuristic_min.ang;

	length = reed_shepp(start_arr, goal_arr, &solution_num, &num_reverses, &first_dir, &t_r, &u_r, &v_r);
	// if the heuristic would require immediately switching direction, take that cost into account
	if ( (first_dir == 1 && is_reverse) || (first_dir == -1 && !is_reverse) )
	{
		rs_opposite_dir = 1;
		// Log::getStream(1) << "CLOTHOIDPLANNER:: heuristic switches direction. first_dir = " << first_dir << endl;
	}
	else
	{
		// Log::getStream(1) << "CLOTHOIDPLANNER:: heuristic does NOT switch direction. first_dir = " << first_dir << endl;
		rs_opposite_dir = 0;
	}
	

	/* Log::getStream(1) << "CLOTHOIDPLANNER:: reed-shepp heuristic measure: " << length << " and switches direction " 
	<< num_reverses << " in solution number " << solution_num << " too "; heuristic_min.display(&Log::getStream(1));
	Log::getStream(1) << "CLOTHOIDPLANNER:: linear distance: " << (start - goal).magnitude() << endl; */

	// we include in the heuristic the cost for the distance to go (basically 1m = cost of 1), and the cost for
	// switching directions from forward to reverse
	return HEURISTIC_RS_SCALE_FACTOR * (length + HEURISTIC_REED_SHEPP_PROJECT_DIST ) 
		+ PATH_COST_CHANGE_DIRECTION * (num_reverses + rs_opposite_dir);

	#else

	#define HEURISTIC_GAUSSIAN_WIDTH		6
	#define HEURISTIC_GAUSSIAN_HEIGHT		15
	#define HEURISTIC_DIST_TO_MIN			6

	#define HEURISTIC_PARK_ENTER_DIST_SCALE		0.04
	#define HEURISTIC_PARK_ENTER_ANGLE_SCALE	2
	#define HEURISTIC_PARK_ENTER_SCALE_FACTOR	15.0

	#define HEURISTIC_PARK_EXIT_DIST_SCALE		0.02
	#define HEURISTIC_PARK_EXIT_ANGLE_SCALE		5
	#define HEURISTIC_PARK_EXIT_SCALE_FACTOR	14.0

	#define HEURISTIC_UTURN_DIST_SCALE		0.03
	#define HEURISTIC_UTURN_ANGLE_SCALE		6
	#define HEURISTIC_UTURN_SCALE_FACTOR		12.0

	// note: on a road region, the distance term is linear (on the
	// others its quadratic)	
	#define HEURISTIC_ROAD_DIST_SCALE		0.3
	#define HEURISTIC_ROAD_ANGLE_SCALE		1
	#define HEURISTIC_ROAD_SCALE_FACTOR		12.0

	// There are circular areas around the goal which we don't want to be in-
	// because of the vehicle dynamics, once we're in those areas, we
	// can't reach the goal without going out of them (and therefore 
	// getting further from the goal) or reversing a lot. so, set the 
	// heuristic to vew very high in those regions
	pose2 center1, center2, center3;
	pose2 trans1(VEHICLE_MIN_TURNING_RADIUS * sin(goal.ang),
		-1 * VEHICLE_MIN_TURNING_RADIUS * cos(goal.ang), 0);
	pose2 trans2(VEHICLE_MIN_TURNING_RADIUS * cos(goal.ang),
		VEHICLE_MIN_TURNING_RADIUS * sin(goal.ang), 0);
	center1 = goal + trans1;
	center2 = goal - trans1;
	center3 = goal + trans2;
// 	cout << "center 1 is at "; center1.display();
// 	cout << "center 2 is at "; center2.display();
// 	cout << "center 3 is at "; center3.display();
	// how far are we from the edge of our cirular areas?
	double d1 = (start - center1).magnitude() - VEHICLE_MIN_TURNING_RADIUS;
	double d2 = (start - center2).magnitude() - VEHICLE_MIN_TURNING_RADIUS;
	double d3 = (start - center3).magnitude() - VEHICLE_MIN_TURNING_RADIUS;
	double gauss_dist = min( min(d1, d2), d3);
	double close_to_goal_cost;

	// we also want to project the minimum of the heuristic out from our goal, in the
	// direction we will be approaching from
	pose2 heuristic_min = pose2( HEURISTIC_DIST_TO_MIN * cos( goal.ang + M_PI ), 
		HEURISTIC_DIST_TO_MIN * sin( goal.ang + M_PI ), 0 );
	if ( getPlanMode() != reverse_required )
		heuristic_min += goal;
	else
		heuristic_min -= goal;
// 	cout << "heuristic min is at "; heuristic_min.display();

	pose2 diff_start_heuristic_min = heuristic_min - start;
	double dist_to_heur_min_sq = diff_start_heuristic_min.x * diff_start_heuristic_min.x +
		diff_start_heuristic_min.y * diff_start_heuristic_min.y;
	double diff_angle = fabs( (start - goal).ang);
	double distance_factor;
	double angle_factor;

	switch( m_cspecs->getTrafficState() )
	{
		case TRAFFIC_STATE_ENTER_PARK:
			if (gauss_dist < 0)		// inside of one of these circular areas;
				close_to_goal_cost = HEURISTIC_GAUSSIAN_HEIGHT;
			else
				close_to_goal_cost =
					HEURISTIC_GAUSSIAN_HEIGHT * exp( -1 * (gauss_dist * gauss_dist) / 
					( HEURISTIC_GAUSSIAN_WIDTH * HEURISTIC_GAUSSIAN_WIDTH) );
			distance_factor = HEURISTIC_PARK_ENTER_DIST_SCALE * dist_to_heur_min_sq;
			angle_factor = HEURISTIC_PARK_ENTER_ANGLE_SCALE * diff_angle;

// 			Log::getStream(1) << " Close to goal cost: " << close_to_goal_cost << endl;
// 			Log::getStream(1) << " distance factor = " << distance_factor << endl;
// 			Log::getStream(1) << " angle factor = " << angle_factor << endl;
// 			Log::getStream(1) << " sum = " << angle_factor + distance_factor << endl;

			return HEURISTIC_PARK_ENTER_SCALE_FACTOR * 
				max ( close_to_goal_cost, angle_factor + distance_factor
					/*+ HEURISTIC_DIST_TO_MIN */);
		break;

		case TRAFFIC_STATE_EXIT_PARK:
			if (gauss_dist < 0)		// inside of one of these circular areas;
				close_to_goal_cost = HEURISTIC_GAUSSIAN_HEIGHT;
			else
				close_to_goal_cost =
					HEURISTIC_GAUSSIAN_HEIGHT * exp( -1 * (gauss_dist * gauss_dist) / 
					( HEURISTIC_GAUSSIAN_WIDTH * HEURISTIC_GAUSSIAN_WIDTH) );

			distance_factor = HEURISTIC_PARK_EXIT_DIST_SCALE * dist_to_heur_min_sq;
			angle_factor = HEURISTIC_PARK_EXIT_ANGLE_SCALE * diff_angle;
		
			return HEURISTIC_PARK_EXIT_SCALE_FACTOR * 
				max ( close_to_goal_cost, angle_factor + distance_factor
					/*+ HEURISTIC_DIST_TO_MIN */);

		break;

		case TRAFFIC_STATE_UTURN:
                        if (gauss_dist < 0)             // inside of one of these circular areas;
                                close_to_goal_cost = HEURISTIC_GAUSSIAN_HEIGHT;
                        else
                                close_to_goal_cost =
                                        HEURISTIC_GAUSSIAN_HEIGHT * exp( -1 * (gauss_dist * gauss_dist) /
                                        ( HEURISTIC_GAUSSIAN_WIDTH * HEURISTIC_GAUSSIAN_WIDTH) );

                        distance_factor = HEURISTIC_UTURN_DIST_SCALE * dist_to_heur_min_sq;
                        angle_factor = HEURISTIC_UTURN_ANGLE_SCALE * diff_angle;

                        return HEURISTIC_UTURN_SCALE_FACTOR *
                                max ( close_to_goal_cost, angle_factor + distance_factor
                                        /*+ HEURISTIC_DIST_TO_MIN */);
		break;

		case TRAFFIC_STATE_BACKUP:
		case TRAFFIC_STATE_ROAD:
                        if (gauss_dist < 0)             // inside of one of these circular areas;
                                close_to_goal_cost = HEURISTIC_GAUSSIAN_HEIGHT;
                        else
                                close_to_goal_cost =
                                        HEURISTIC_GAUSSIAN_HEIGHT * exp( -1 * (gauss_dist * gauss_dist) /
                                        ( HEURISTIC_GAUSSIAN_WIDTH * HEURISTIC_GAUSSIAN_WIDTH) );

                        distance_factor = HEURISTIC_ROAD_DIST_SCALE * diff_start_heuristic_min.magnitude();
                        angle_factor = HEURISTIC_ROAD_ANGLE_SCALE * diff_angle;

                        return HEURISTIC_ROAD_SCALE_FACTOR *
                                max ( close_to_goal_cost, angle_factor + distance_factor
                                        /*+ HEURISTIC_DIST_TO_MIN */);
		break;

		default:
			return 7 * (start-goal).magnitude() + 5 * fabs((start-goal).ang);
		break;
	}

	#endif
}

// return the average of the upper and lower bounds on velocity
// if ub and/or lb are not null, then they will be populated with the
// appropriate value
double CEnvironment::getInitialVelocity()
{
	vector<double> conds = m_cspecs->getStartingState();
	double v = conds.at(STATE_IDX_V);
	return v;
}

double CEnvironment::getGoalVelocity()
{
	vector<double> conds = m_cspecs->getFinalState();
	double v = conds.at(STATE_IDX_V);
	return v;
}

S1PlanMode CEnvironment::getPlanMode()
{
	switch( m_cspecs->getTrafficState() )
	{
		case TRAFFIC_STATE_BACKUP:
			return reverse_required;
		case TRAFFIC_STATE_ROAD:
		case TRAFFIC_STATE_ENTER_PARK:
		case TRAFFIC_STATE_EXIT_PARK:
		case TRAFFIC_STATE_UTURN:
		default:
			return reverse_allowed;
			//return no_reverse;
	}
}

int CEnvironment::getTrafficState()
{
	return m_cspecs->getTrafficState();
}

