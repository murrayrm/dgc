// s1planner unit test- independent of OCPspecs

#include "tempClothoidInterface.hh"


int main(int argc, char **argv)
{
	if (argc < 2)
	{
		cout	<< "Usage: UT_clothoidplanner UT_spec.dat" << endl
			<< "where UT_spec.dat is a file in the following format:" << endl
			<< "initCond.x, initCond.y, initCond.yaw" << endl
			<< "finalCond.x, finalCond.y, finalCond.yaw" << endl
			<< "lowerLeftCorner.x, lowerLeftCorner.y" << endl
			<< "upperRightCorner.x, upperRightCorner.y" << endl;
		return 1;
	}

	FILE *ut_spec = fopen(argv[1], "r");

	/* read in the unit test specification, which is in the format
	   (in the following order; each on its own line):
	initCond.x, initCond.y, initCond.yaw
	finalCond.x, finalCond.y, finalCond.yaw
	lowerLeftCorner.x, lowerLeftCorner.y
	upperRightCorner.x, upperRightCorner.y
	*/

	pose2 start, end, ll, ur;
	//pose2 start = pose2(20,2.2,0), end = pose2( .1, 6.8, M_PI-.01),
	//	ll = pose2(0,0,0), ur = pose2(30, 9, 0);


	fscanf(ut_spec, "%lf,%lf,%lf\n", &(start.x), &(start.y), &(start.ang));
	fscanf(ut_spec, "%lf,%lf,%lf\n", &(end.x), &(end.y), &(end.ang));
	fscanf(ut_spec, "%lf,%lf\n", &(ll.x), &(ll.y));
	fscanf(ut_spec, "%lf,%lf\n", &(ur.x), &(ur.y));


	CEnvironment *cspecs = new CEnvironment(start, end, ll, ur);
	GenerateTrajOneShot(cspecs, NULL, 0.1);


	return 0;
}

