/*
 * tempClothoidInterface.cc
 * temporary interface functions for 
 * the new planner stack to run the clothoid planner.
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 26 July 07- Kenny Oslund
	- Created file
 *
 */

#include "tempClothoidInterface.hh"


/* Do everything needed to generate a single trajectory and then clean up. All needed memory
   is allocated and freed in this function */
Err_t GenerateTrajOneShot(CEnvironment *probSpecs, GraphPath *path, double runtime)
{
	CClothoidPlanner *planner = new CClothoidPlanner(probSpecs, NULL);

	planner->generatePaths(runtime);
	populatePath(planner, path);

	delete planner;

	return LP_OK;
}





void populatePath(CClothoidPlanner *planner, GraphPath *path)
{
	/* I don't know how save the path to a GraphPath object
	   so for now just dump the whole thing to a file. */

	ofstream sol_file("clothoidSolution.dat");
	planner->dumpBestSolution(&sol_file);

	ofstream tree_file("clothoidTree.dat");
	planner->dumpTree(&tree_file);

}




