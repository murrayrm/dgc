close all;
clear all;

UT_senario = 'UT_parking_enter2.dat';
%UT_senario = 'UT_uturn.dat';

unix('make -f Makefile.yam all');
unix(['UT_clothoidplanner ' UT_senario] );


specs = dlmread(UT_senario);

% plot a single clothoid or clothoid tree
figure;
dlmread clothoidSolution.dat;
clothoids = ans;
hold on;
% plot the corridor from the upper left and lower right boundaries
patch([specs(3,1) specs(3,1) specs(4,1) specs(4,1)], [specs(3,2) specs(4,2) specs(4,2) specs(3,2)], 'w');
% plot the clothoids
plot(clothoids(:,1), clothoids(:,2), '-','MarkerSize',1);
% plot the initial and final positions
plot(specs(1:2,1), specs(1:2,2), 'r+');
xlabel('X (meters)');
ylabel('Y (meters)');
title('Clothoid Tree');
% determine whether our area is larger in the x or y direction and set the
% scaling in that direction
if ( (specs(4,1) - specs(3,1)) > (specs(4,2) - specs(3,2)) )
    xlim([specs(3,1)-1 specs(4,1) + 1]);
else
    ylim([specs(3,2)-1 specs(4,2) + 1]);
end
axis equal;

figure;
dlmread clothoidTree.dat;
clothoids = ans;
hold on;
% plot the corridor from the upper left and lower right boundaries
patch([specs(3,1) specs(3,1) specs(4,1) specs(4,1)], [specs(3,2) specs(4,2) specs(4,2) specs(3,2)], 'w');
% plot the clothoids
plot(clothoids(:,1), clothoids(:,2), '.','MarkerSize',1);
% plot the initial and final positions
plot(specs(1:2,1), specs(1:2,2), 'r+');
xlabel('X (meters)');
ylabel('Y (meters)');
title('Clothoid Tree');
% determine whether our area is larger in the x or y direction and set the
% scaling in that direction
if ( (specs(4,1) - specs(3,1)) > (specs(4,2) - specs(3,2)) )
    xlim([specs(3,1)-1 specs(4,1) + 1]);
else
    ylim([specs(3,2)-1 specs(4,2) + 1]);
end
axis equal;
