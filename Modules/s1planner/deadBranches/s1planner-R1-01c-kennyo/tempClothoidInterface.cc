/*
 * tempClothoidInterface.cc
 * temporary interface functions for 
 * the new planner stack to run the clothoid planner.
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 26 July 07- Kenny Oslund
	- Created file
 *
 */

#include "s1planner/tempClothoidInterface.hh"

GraphNode ClothoidPlannerInterface::nodeArr[GRAPH_PATH_MAX_NODES];

/* Do everything needed to generate a single trajectory and then clean up. All needed memory
   is allocated and freed in this function */
Err_t ClothoidPlannerInterface::GenerateTrajOneShot(int sn_key, bool disp_costmap, CSpecs_t cSpecs, Path_t *path, double runtime)
{
  CEnvironment *problem = new CEnvironment(&cSpecs);
  
  CClothoidPlanner *planner = new CClothoidPlanner(problem, NULL);
  
  planner->generatePaths(runtime);
  populatePath(planner, path);

	double pathLength;
	bool reaches_end;
        vector<pose2> solution = planner->getBestPath(&pathLength, &reaches_end);

	ofstream log("clothoidSolution.dat", ios_base::app);
	log << "****************** new planning cycle ***************** " << endl;
	log << " CEnvironment info:" << endl;
	log << " planning from: "; (problem->getStartPoint()).display(&log);
	log << " to: "; (problem->getEndPoint()).display(&log);
	if (problem->isPointLegal(problem->getStartPoint()))
		log << " Start point IS legal" << endl;
	else
		log << " Start point is NOT legal" << endl;
	if (problem->isPointLegal(problem->getEndPoint()))
		log << " End point IS legal" << endl;
	else
		log << " End point is NOT legal" << endl;

	planner->dumpBestSolution(&log);
	log << " num points in solution: " << solution.size() << endl;

  delete planner;
  delete problem; 
  return LP_OK;
}

void ClothoidPlannerInterface::populatePath(CClothoidPlanner *planner, Path_t *path)
{
	bool reaches_end;
	vector<CElementaryPath *> solution = planner->getBestClothoidSequence(&reaches_end);

	if (NULL != path) {
	  path->valid = true;
	  path->collideObs = 0;
	  path->collideCar = 0;
	  
	  // we skip the first point of each clothoid so that we don't end up with
	  // points right on top each other (that point was in the previous clothoid).
	  // however, we do want the first point of the first clothoid because there was
	  // no previous clothoid
    if (solution.size() > 0) {
      pose2 firstPt = solution.at(0)->getFirstPoint();
      nodeArr[0].pose.pos.x = firstPt.x;
      nodeArr[0].pose.pos.y = firstPt.y;
      nodeArr[0].pose.pos.z = 0;
      nodeArr[0].pose.rot = quat_from_rpy(0, 0, firstPt.ang);
      if ( solution.at(0)->m_reverse )
        nodeArr[0].pathDir = GRAPH_PATH_REV;
      else
        nodeArr[0].pathDir = GRAPH_PATH_FWD;
      path->path[0] = &(nodeArr[0]);
      
      int i = 1;
      for (vector<CElementaryPath *>::iterator j = solution.begin(); 
           j != solution.end(); j++) {
        vector<pose2> clothoid = (*j)->getClothoidPoints();
        // skip the first point of each clothoid so that we don't end up
        // with 2 points in exactly the same place
        for (vector<pose2>::iterator k = clothoid.begin() + 1; k != clothoid.end();
             k++, i++) {
          nodeArr[i].pose.pos.x = k->x;
          nodeArr[i].pose.pos.y = k->y;
          nodeArr[i].pose.pos.z = 0;
          if ( (*j)->m_reverse ) {
            nodeArr[i].pathDir = GRAPH_PATH_REV;
            k->rotateInPlace(M_PI);
          } else
            nodeArr[i].pathDir = GRAPH_PATH_FWD;
          nodeArr[i].pose.rot = quat_from_rpy(0, 0, k->ang);
          path->path[i] = &(nodeArr[i]);
        }
      }
      path->goalDist = i * PATH_SECTION_LENGTH;
      path->pathLen = i;
    }
  }
  return;
}

