/*
 * ElementaryPath.cc
 * Helper class for the ClothoidPlanner. This class is a container for
 * basic curves that we're using to build our trajectories
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 1 May 07- Kenny Oslund
	- Created file
 *
 */



#include "ElementaryPath.hh"
#include "ClothoidUtils.hh"


/* this constructor should be used most of the time so that the cothoid can have access to 
   environment information */
CElementaryPath::CElementaryPath(CEnvironment *env)
{
	clearClothoid();
	m_aliceEnv = env;
	clearClothoid();
	m_searchParent = NULL;
}

/* basic constructor */
CElementaryPath::CElementaryPath()
{
	cout << "in basic constructor" << endl;
	m_aliceEnv = NULL;
	clearClothoid();
	m_searchParent = NULL;

}

/* constructor which fills in the clothoid based on 
   passed parameters */
CElementaryPath::CElementaryPath(CEnvironment *env, double new_sigma,
	double new_length, bool new_reverse, pose2 startPose)
{
	/* call the standard constructor */
	clearClothoid();
	m_aliceEnv = env;

	sigma = new_sigma; length = new_length; reverse = new_reverse;
	calculateClothoid(startPose);
}


void CElementaryPath::clearClothoid()
{
	sigma = 0; length = 0;
	costToNode = 0; costToFinish = 0;
	atFinish = false;
	reverse = false;
	explored_resolution = PATH_UNEXPLORED;

	m_clothoidPoints.clear();
}


CElementaryPath::~CElementaryPath()
{
	/* destructor needs to recursively delete all the children of 
	   the current node. */
        if ( !m_searchChildren.empty() )
               for (vector<CElementaryPath*>::iterator i = m_searchChildren.begin();
		i != m_searchChildren.end(); i++)
		{
			delete (*i);
		}


}


void CElementaryPath::calculateClothoid(pose2 startPose)
{
	pose2 point(startPose), pointCopy;

	m_clothoidPoints.clear();
	/* save the start point */
	m_clothoidPoints.push_back(point);

	/* if we're driving in reverse, changing the start pose to be in the exact
	   opposite direction is the same thing as driving backwards */
	if (reverse)
		point.rotateInPlace(M_PI);

	double s, cosTheta, sinTheta;
	double prevCosTheta = cos(point.ang);
	double prevSinTheta = sin(point.ang);
	double prevk = 0.0, curvature = 0.0;
	int segmentPoints = (int)(length/PATH_SECTION_LENGTH) + 1;

	// iterate along trajectory calculating curvature, ang, x and y
	for (int j=1; j<segmentPoints; j++)
	{
		s = j * PATH_SECTION_LENGTH;
		
		// symmetric curvature
		if (j < segmentPoints/2) 
			curvature = sigma * s;
		else
			curvature = sigma * (length - s);

		// Use trapezoidal numerical integration
		// theta(s) = /int^s_0 k(t) dt
		point.rotateInPlace( (prevk + curvature) * 
			PATH_SECTION_LENGTH * 0.5 );
 
		// to do this once the algorithm is in place
		cosTheta = cos(point.ang);
		sinTheta = sin(point.ang);
		// x(s) = /int^s_0 cos /theta (t) dt
		point.x +=  (cosTheta + prevCosTheta) * PATH_SECTION_LENGTH * 0.5;
		// y(s) = /int^s_0 sin /theta (t) dt
		point.y +=  (sinTheta + prevSinTheta) * PATH_SECTION_LENGTH * 0.5;

		prevk = curvature;
		prevCosTheta = cosTheta;
		prevSinTheta = sinTheta;

		/* flip the point back around to point in the same direction as alice
		   before saving if we're going in reverse (points for the running 
		   calculation are still pointing backwards) */
		pointCopy = point;
		if (reverse)
			pointCopy.rotateInPlace(M_PI);
		appendPoint(pointCopy);
	}


	return;
}

/** Generate sharpness, sigma and length, l of an elementary clothoid curve linking symmetric postures
startPose and finishPose. Formula used are from Scheuer 98. */
/* this almost works- there is still just some special case in which the
   startPose is connected to -finishPose = (-x,-y), where (x,y) is the desired
   finish pose. I haven't been able to deduce the cause of this, but one
   pair of x,y,theta points that causes this is 
   (0,0,0) connected to (-10,-10,3PI/4) */
void CElementaryPath::findSigmaL(pose2 startPose, pose2 finishPose)
{
	pose2 point1(startPose), point2(finishPose);
	pose2 rel;
	/* reversing the direction of the vectors we're connecting is the same 
	   as driving along them in reverse */
	if (reverse)
	{
		point1.rotateInPlace(M_PI);
		point2.rotateInPlace(M_PI);
	}
	
	pose2 d = point2 - point1;
	double separation = d.magnitude();
	double separationSquared = separation * separation;
	// We need to figure out whether we need to turn left (alpha > 0)
	// or right- its needed b/c sometimes we want to make a turn of
	// > 180 degrees => alpha should not between -PI and PI
	// but sometimes turning > 180 degrees => we just loop around and
	// back over our path, so we need to unwrap the angle first. the logic 
	// bellow tries to figure out which case we're in.
	double alpha = d.ang * 0.5;
	// get the relative potition of the finish if alice was at the 
	// origin with 0 rad angle
	rel = d;
	rel.rotateAroundOrigin(-1 * point1.ang);
	// we need to go left by x radians, but instead are turning right by
	// x - 2*PI radians or
	if ( (rel.y > 0 && alpha < 0) )
		alpha += M_PI;
	// we need to right by x radians, but instead are turning left by
	// x + 2*PI radians
	if( (rel.y < 0 && alpha > 0) )
		alpha -= M_PI;
	// from Scheuer 98
	double D1 = integralD1(fabs(alpha));
	sigma = 4 * M_PI * sign(alpha) * (D1 * D1) / separationSquared; 
/*	cout << "alpha: " << alpha << " point1.ang: " << point1.ang << 
		" sigma: " << sigma << " rel: " << " ";
	rel.display(); */
	length = 2 * sqrt( 2 * alpha / sigma );
	return;
}


/* fill in the clothoidPoints with a symmetric clothoid between the 
   start and finish pose */
CElementaryPath *CElementaryPath::generateSymmetricClothoid(pose2 startPose, 
	pose2 finishPose, bool in_reverse)
{
	CElementaryPath *path = new CElementaryPath(m_aliceEnv);
	path->reverse = in_reverse;

	path->findSigmaL(startPose, finishPose);
	path->calculateClothoid(startPose);

	return path;
}

/* fill in the clothoidPoints with a bielementary clothoid between the
   start and finish pose */
CElementaryPath *CElementaryPath::generateBielementaryClothoid(pose2 startPose, 
	pose2 finishPose, bool in_reverse)
{
	
	// try to find a bielementary path from p1 to p2
	pose2 diff, sum, q;	// q is the intermediate pose
	double deflection;	// alpha
	double beta;
	CElementaryPath *path1, *path2;
	path1 = new CElementaryPath(m_aliceEnv);
	path2 = new CElementaryPath(m_aliceEnv);

	path1->reverse = path2->reverse = in_reverse;

	diff = finishPose - startPose;	// diff = displacement between poses
	sum = finishPose + startPose;
	deflection = diff.ang;
	
	// calculation is simple if poses happen to be parallel
	if ( fabs(deflection) < PATH_EPS_POSES_PARALLEL ) // parallel postures 
	{
		// the intermediate posture is at the midpoint of p1 and p2
		q = sum / 2;
		// angle of the line between the start and finish poses
		beta = diff.argument();
		// theta1 - beta = - (theta2 - beta) from Kanayama =>
		// => theta2 = beta - (theta1 - beta) = 2 * beta - theta1
		q.setAngle(2 * beta - startPose.ang);

		path1->findSigmaL(startPose,q);
		path2->findSigmaL(q,finishPose);
	}
	else
	{
		// from Kanayama this circle is the locus of the 
		// intermediate posture
		pose2 centre;
		
		double c = 1.0 / tan(deflection / 2); 
		centre.x = (sum.x - c * diff.y)/2;
		centre.y = (sum.y + c * diff.x)/2;
		pose2 diffFinishCenter = finishPose - centre;
		pose2 diffStartCenter = startPose - centre;
		pose2 d, minIntermediatePose;
		double radius = diffFinishCenter.magnitude();

		// Kanayama uses Newton-Raphson iteration to minimise curvature
		// by varying q. For the moment just iterate around correct bit 
		// of circle.
		// parameterize q in terms of angle from centre of circle
		double minCost = 10000000.0;
		double minAngle = 0.0;
		double start = diffStartCenter.argument();
		double finish = diffFinishCenter.argument();
		double increment =
			sign(deflection) * PATH_INTERMEDIATE_INTERVAL / radius;
		bool intermediateFound = false; 
		CElementaryPath dummy1(m_aliceEnv), dummy2(m_aliceEnv);

		// Iterate around circle
		for (double angle = unwrap_angle(start + increment); 
		     inbetween(angle,start,finish,increment<0);
		     angle = angle + increment )
		{
			d.y = radius * sin(angle);
			d.x = radius * cos(angle);
			q = centre + d;
			beta = (q - startPose).argument();
			q.setAngle( 2 * beta - startPose.ang );
		
			dummy1.findSigmaL(startPose,q);
			dummy2.findSigmaL(q,finishPose);

/*			dummy1.calculateClothoid(startPose);
			dummy1.printClothoid(&log);
			dummy2.calculateClothoid(q);
			dummy2.printClothoid(&log);
			q.print(&log);
*/
			// should we base this on our main cost function? then we'd have to calculate
			// every point in the clothoid
			double cost = fabs(dummy1.getMaxCurvature()) +
				fabs(dummy2.getMaxCurvature());
			
			if (cost < minCost && (dummy1.length + dummy2.length) < PATH_MAX_SEARCH_LENGTH)
			{
				intermediateFound = true; 
				minCost = cost;
				minAngle = angle;
				path1->sigma = dummy1.sigma; path1->length = dummy1.length;
				path2->sigma = dummy2.sigma; path2->length = dummy2.length;
				minIntermediatePose = q;
			}
		}
		if (!intermediateFound)	{
			delete path1; delete path2;
			//cout << "Failed to find biel path: no intermediate or path too long (this is normal and ok)" << endl;
			return NULL; }

		q = minIntermediatePose;
	}
	path1->calculateClothoid(startPose);
	path2->calculateClothoid(q);

	/* some combination of poses cause complete failure of the method,
	   so check that the the 2 clothoids are actually continuous and
	   that we actually ended up where we want to be */
	if ( (path1->getLastPoint() - path2->getFirstPoint()).magnitude() > 
		PATH_POINTS_CLOSE_ENOUGH ||
	     (path2->getLastPoint() - finishPose).magnitude() >
		PATH_POINTS_CLOSE_ENOUGH )
	{
		//cout << "Failed to find biel path: path doesn't reach end point (this is ok as long as it doesn't happen too much)"
		//	<< endl;
		delete path1; delete path2;
		return NULL;
	}
	path1->appendPath(path2);
/*	cout << "start = ";
	startPose.display();
	cout << "finish = ";
	finishPose.display();
	cout << "q = ";
	q.display();
	cout << "path1: sigma: " << path1->sigma << " length: " 
		<< path1->length << endl;
	cout << "path2: sigma: " << path2->sigma << " length: " 
		<< path2->length << endl;
	ofstream log("tmp_clothoids.dat");
	path1->printAllClothoids(&log); */

	return path1;



}

CElementaryPath *CElementaryPath::copyClothoid(pose2 startPose,
		CElementaryPath *clothoidToCopy,
		bool in_reverse)
{
	CElementaryPath *new_clothoid = new CElementaryPath(m_aliceEnv);
	
	new_clothoid->sigma = clothoidToCopy->sigma;
	new_clothoid->length = clothoidToCopy->length;
	new_clothoid->reverse = in_reverse;

	pose2 translation = startPose - clothoidToCopy->getFirstPoint();

	if (clothoidToCopy->m_clothoidPoints.size() > 0)
		for (vector<pose2>::iterator i =
			clothoidToCopy->m_clothoidPoints.begin();
			i != clothoidToCopy->m_clothoidPoints.end(); i++)
		{
			pose2 pt = *i;
			pt.translate(translation);
			double rotation = translation.ang;
			if (in_reverse)
				rotation = unwrap_angle(translation.ang + M_PI);
			pt.rotateAroundPointPreserveAngle(translation, rotation);
			pt.rotateInPlace(translation.ang);
			new_clothoid->appendPoint(pt);
		}
	return new_clothoid;

}

bool CElementaryPath::connectClothoidCopy(CElementaryPath *clothoidToCopy,
	bool in_reverse)
{
	CElementaryPath *path = 
		copyClothoid(getLastPoint(), clothoidToCopy, in_reverse);
	// check that the trajectory doesn't leave the allowed driving area.
	// since these are precomputed clothoids, we'll assume they aren't too sharp
	if ( path != NULL && path -> isClothoidLegal() )
	{
		appendPath(path);
		return true;
	}
	if ( path != NULL)
	{
		delete path;
	}
	return false;
}

SearchStatus CElementaryPath::connectToTarget(pose2 targetPose, bool in_reverse)
{
	CElementaryPath *path = NULL;
	// start from the end of this clothoid
	pose2 startPose = getLastPoint();

	pose2 d = targetPose - startPose;
	double beta = d.argument();

	// check if the poses happen to be symmetric
	if (fabs( startPose.ang + targetPose.ang - (2 * beta))
		< PATH_EPS_POSES_SYMMETRIC)
	{
		path = generateSymmetricClothoid(startPose, targetPose, in_reverse);
		// check the clothoid for legality: is it in the driving area
		// and is it too sharp?
		if ( path != NULL && path->isClothoidLegal() )
		{
			appendPath(path);
			return search_simple_path;
		}
	}
	else
	{
		path = generateBielementaryClothoid(startPose, targetPose,
			in_reverse);
		if ( path != NULL )
		{
			CElementaryPath *path2 = path->m_searchChildren.front();
			// check the clothoid for legality: is it in the driving area
			// and is it too sharp?
			if ( path->isClothoidLegal() && path2->isClothoidLegal() )
			{
				appendPath(path);
				return search_biel_path;
			}
		}
	}
	if ( path != NULL )
		delete path;
	return search_failed;
}

/* check whether a clothoid is in the legal driving zone and whether it is dynamically feasible */
bool CElementaryPath::isClothoidLegal()
{
#ifdef	PATH_CHECK_CORRIDOR
	return ( m_aliceEnv->isPathLegal(&m_clothoidPoints) 
		&& getMaxCurvature() <= PATH_MAX_DYN_FEASIBLE_CURVATURE);

/*	// if the start point is outside of the corridor, then the corridor check will cause s1planner
	// to fail, so just don't even do a corridor check
    if(m_aliceEnv->isPointLegal(m_aliceEnv->getStartPoint()))
		return ( m_aliceEnv->isPathLegal(&m_clothoidPoints) 
			&& getMaxCurvature() <= PATH_MAX_DYN_FEASIBLE_CURVATURE);
	else
		return true; */
#else
	return true; 	// stub for testing
#endif
}


double CElementaryPath::getMaxCurvature()
{
	return sigma * length * 0.5;
}


/* append the specified path to the end of this one */
void CElementaryPath::appendPath(CElementaryPath *path)
{
	path->setCost(costToNode);
	m_searchChildren.push_back(path);
	path->setParent(this);
	/* check for a circularly linked list */
// 	vector<CElementaryPath *> pathsChildren = path->getSearchChildren();
// 	for (vector<CElementaryPath *>::iterator i = pathsChildren.begin();
// 		i != pathsChildren.end(); i++)
// 		if ( (*i) == this )
// 		{
// 			cout << "ERROR ERROR ERROR ERROR ERROR ERROR" << endl;
// 			cout << "circularly linked list detected! abort! abort!"
// 				<< endl;
// 			abort();
// 		}

}

void CElementaryPath::appendPoint(pose2 point)
{
	m_clothoidPoints.push_back(point);
}

/* delete the link between the current node and its parent (it no longer
   points to parent, parent no longer points to it) */
void CElementaryPath::severLink(CElementaryPath *childToSever)
{
	vector<CElementaryPath *>::iterator i = m_searchChildren.begin();
	while ( (*i) != childToSever )
	{
		if (i == m_searchChildren.end() )
		{
			cout << "ERROR: node to sever isn't a child of this node. "
				<< "You called this with the wrong argument" << endl;
			break;
		}
		i++;
	}

	m_searchChildren.erase(i);
	childToSever->setParent(NULL);
}

/* delete all the points in the clothoid between indexes indL and indH (inclusive) */
void CElementaryPath::deleteClothoidPoints(int indL, int indH)
{
	int ind = 0;
	// handle the default case
	if ( indH == -1 )
		indH = m_clothoidPoints.size() - 1;
	// error checking on the indeces
	if ( indL < 0 || indL > ((int)m_clothoidPoints.size() - 1)
		|| indH < 0 || indH > ((int)m_clothoidPoints.size() - 1)
		|| indH < indL )
	{
		cout << "ERROR: Cannot delete clothoid points- indices to delete "
			<< "are outside of the clothoid or lowerIndex > higherIndex"
			<< endl;
		return;
	}
	for (vector<pose2>::iterator i = m_clothoidPoints.begin() + indL;
		ind <= indH && i != m_clothoidPoints.end(); ind++)
		m_clothoidPoints.erase(i);	// this also effectively shifts the
						// iterator to the next point

}

/* set the parent of this node to be null */
void CElementaryPath::setParent(CElementaryPath *parent)
{
	m_searchParent = parent;
}

/* mark the finish nodes as no longer at the finish-- used when the goal location changed */
void CElementaryPath::setNodesAsNotAtGoal()
{
	if ( atFinish )
	{
		atFinish = false;
	}
	if ( !m_searchChildren.empty() )
		for (vector<CElementaryPath*>::iterator i = m_searchChildren.begin();
			i != m_searchChildren.end(); i++)
		{
			(*i)->setNodesAsNotAtGoal();
		}
}


/* set the cost to this node (argument is the cost to the end of the previous 
   node, not the total cost (which is the cost to the end of the node 
   + the heuristic ) */
void CElementaryPath::setCost(double prevNodeCost)
{
//	the real cost calculation
	costToNode = prevNodeCost + m_aliceEnv->getPathCost(&m_clothoidPoints);
//	temporary one for testing
//	costToNode = prevNodeCost + length;
	// get the estimated cost from the end of this clothoid to the finish
	costToFinish = m_aliceEnv->getHeuristicToGoal(getLastPoint());
	/* recursively set the cost to all the children */
        if (m_searchChildren.size() > 0)
		for (vector<CElementaryPath*>::iterator i = m_searchChildren.begin();
			i != m_searchChildren.end(); i++)
		{
			(*i)->setCost(costToNode);
		}
}

/* return the estemated total path cost (cost to end of node + heuristic) */
double CElementaryPath::getCost()
{
	return costToNode + costToFinish;
}

/* find the unexplored node with the lowest estimated cost */
CElementaryPath *CElementaryPath::findLowestCost(int resolution)
{
	bool alreadyExplored = !(explored_resolution < resolution);
	if (atFinish)
		return NULL;
	// if the node hasn't already been explored, then mark it as 
	// the node with the lowest current cost. otherwise set
	// that there is currently no node with the lowest cost.
	double lowestCost = alreadyExplored ? 1000000 : getCost();
	CElementaryPath *lowestCostPath = alreadyExplored ? NULL : this;
	// check all the decendents to see if one has a lower estimated total cost than
	// this node
        if (m_searchChildren.size() > 0)
		for (vector<CElementaryPath*>::iterator i = m_searchChildren.begin();
			i != m_searchChildren.end(); i++)
		{
			CElementaryPath *next = (*i)->findLowestCost(resolution);
			if ( (next != NULL) && (next->getCost() < lowestCost) )
			{
				lowestCost = next->getCost();
				lowestCostPath = next;
			}
		}

	return lowestCostPath;
}




void CElementaryPath::printClothoid(ostream *f)
{
        if (m_clothoidPoints.size() > 0)
                for (vector<pose2>::iterator i = m_clothoidPoints.begin(); 
			i != m_clothoidPoints.end(); i++)
                {
                        i->print(f);
                }
        return;
}

void CElementaryPath::printAllClothoids(ostream *f)
{
        printClothoid(f);
        if (m_searchChildren.size() > 0)
		for (vector<CElementaryPath*>::iterator i = m_searchChildren.begin();
			i != m_searchChildren.end(); i++)
		{
			(*i)->printAllClothoids(f);
		}
        return;
}




