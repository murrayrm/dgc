// s1planner unit test- independent of OCPspecs

#include "s1planner/tempClothoidInterface.hh"
#include "planner/ZoneCorridor.hh"
#include <stdio.h>

using namespace std; 


int main(int argc, char **argv)
{



	if (argc < 2)
	{
		cout	<< "Usage: UT_clothoidplanner UT_spec.dat" << endl
			<< "where UT_spec.dat is a file in the following format:" << endl
			<< "initCond.x, initCond.y, initCond.yaw" << endl
			<< "finalCond.x, finalCond.y, finalCond.yaw" << endl
			<< "number of parking spaces" << endl
			<< "space1.x1, space1.y1, space1.x2, space1.y2" << endl
			<< "space2.x1, space2.y1, space2.x2, space2.y2" << endl
			<< "..." << endl
			<< "spaceN.x1, spaceN.y1, spaceN.x2, spaceN.y2" << endl
			<< "perimiterPt1.x, perimiterPt1.y" << endl
			<< "perimiterPt2.x, perimiterPt2.y" << endl
			<< "..." << endl
			<< "perimiterPt1N.x, perimiterPtN.y" << endl;
		return 1;
	}

	FILE *ut_spec = fopen(argv[1], "r");

	/* read in the unit test specification */

	double start[STATE_NUM_ELEMENTS], end[STATE_NUM_ELEMENTS];
	int numParkingSpaces;
	vector<point2arr> parkingSpaces;
	//pose2 start = pose2(20,2.2,0), end = pose2( .1, 6.8, M_PI-.01),
	//	ll = pose2(0,0,0), ur = pose2(30, 9, 0);


	fscanf(ut_spec, "%lf,%lf,%lf\n", start + STATE_IDX_X, start + STATE_IDX_Y, start + STATE_IDX_THETA);
	cout << "read in initial state: " << start[STATE_IDX_X] << " " << start[STATE_IDX_Y] << " " << start[STATE_IDX_THETA] << endl;
	fscanf(ut_spec, "%lf,%lf,%lf\n", end + STATE_IDX_X, end + STATE_IDX_Y, end + STATE_IDX_THETA);
	cout << "read in final state: " << end[STATE_IDX_X] << " " << end[STATE_IDX_Y] << " " << end[STATE_IDX_THETA] << endl;
	fscanf(ut_spec, "%d\n", &numParkingSpaces);
	cout << numParkingSpaces << " parking spaces" << endl;
	for ( int i = 0; i < numParkingSpaces; i++)
	{
		point2 Pt1, Pt2;
		fscanf(ut_spec, "%lf,%lf,%lf,%lf\n", &(Pt1.x), &(Pt1.y), &(Pt2.x), &(Pt2.y) );
		point2arr space;
		space.push_back(Pt1); space.push_back(Pt2);
		parkingSpaces.push_back(space);
		cout << "parking space num " << i << " is at " << Pt1 << " to " << Pt2 << endl;
	}

	point2arr boundary;
	point2 Pt;
	while ( fscanf(ut_spec, "%lf, %lf\n", &(Pt.x), &(Pt.y) ) != EOF )
	{
		cout << "reading in boundary point " << Pt << endl;
		boundary.push_back(Pt);
	}
	
	point2 startPt(start[STATE_IDX_X], start[STATE_IDX_Y]);
	cout << "start point " << startPt << " is ";
	if ( boundary.is_inside_poly(startPt) )
		cout << "inside";
	else
		cout << "NOT inside";
	cout << " the boundary" << endl;

	// generate the bitmap params
	BitmapParams bmParams;
	PolygonParams polygonParams;
	bmParams.resX = 0.1;
	bmParams.resY = 0.1;
	bmParams.width = 800;
	bmParams.height = 800;
	bmParams.baseVal = 1 ;
	bmParams.outOfBounds = 100;
	polygonParams.centerlaneVal = 1;
	polygonParams.obsCost = 100;
	
	point2 initPos(start[STATE_IDX_X], start[STATE_IDX_Y]);
	ZoneCorridor::getBitmapParams(bmParams, polygonParams, initPos, boundary, parkingSpaces);

	// populate the cspecs
	CSpecs_t *cSpecs = new CSpecs();
	cSpecs->setStartingState(start);
	cSpecs->setFinalState(end);
	cSpecs->setBoundingPolygon(boundary);
	cSpecs->setCostMap(bmParams);

	// create objects
	CEnvironment *problem = new CEnvironment(cSpecs);
	CClothoidPlanner *planner = new CClothoidPlanner(problem, NULL);
	
	// do the actual work
	planner->generatePaths(0.5);

	ofstream sol_file("clothoidSolution.dat");
	planner->dumpBestSolution(&sol_file);
	ofstream tree_file("clothoidTree.dat");
	planner->dumpTree(&tree_file);

	// test analytic solution
	/* CElementaryPath *test1 = new CElementaryPath(cspecs);
	CElementaryPath *testend;
	ofstream path_file("tempClothoids.dat");
	test1->appendPoint(pose2(35.7582,0.369155, 2.22045e-16));
	test1->connectToTarget(end, false, &testend);
	test1->printAllClothoids(&path_file);
	*/



	return 0;
}

