/*
 * Environment.hh
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 1 May 07- Kenny Oslund
	- Created file
 */


#ifndef ENVIRONMENT_HH
#define ENVIRONMENT_HH

#include "frames/pose2.hh"
#include "frames/point2.hh"
#include "s1planner/ClothoidUtils.hh"
#include <bitmap/Polygon.hh>
#include <bitmap/BitmapParams.hh>
#include <alice/AliceConstants.h>
#include <cspecs/CSpecs.hh>
#include "temp-planner-interfaces/PlannerInterfaces.h"

// somewhere within skynetTalker or its includes is the definition of 
// DEFINE_ENUM, which is used bellow. that is the only reason this is here.
#include <skynettalker/SkynetTalker.hh>

// NOTE: Northing corresponds to x, and Easting corresponds to y

// weight for the heuristic. I think that since both the heuristic and the cost 
//   increase with length
// higher heuristic weight => cost for traversed parts is lower in comparison =>
//   we will preferentially explore longer trajectories
// lower heuristic weight => cost for traversed parts is higher in comparison =>
//   we will preferentiallyh explore/ refine shorter traj's
// this is untested
#define ENVIRONMENT_GOAL_HEURISTIC_SCALE_FACTOR		1.0



// whether or not we can plan in forward only, forward and reverse, or reverse only
#define S1_PLAN_MODE_LIST(_) \
  _( no_reverse, = 0 ) \
  _( reverse_allowed, ) \
  _( reverse_required, )
DEFINE_ENUM(S1PlanMode, S1_PLAN_MODE_LIST)



class CEnvironment 
{
   private:


   public:
	#ifdef ENVIRONMENT_TEST
	pose2 initCond;
	pose2 finalCond;

	// for now, we'll just that the corridor is a rectangle defined by its
	// lower left and upper right verticies
	pose2 lowleft, upright;

	S1PlanMode mode;

	// constructor for use in the unit test
	CEnvironment(pose2 start, pose2 goal, pose2 ll, pose2 ur, S1PlanMode pmode = reverse_allowed);

	#else
	CSpecs_t *m_cspecs;
	#endif

	/* Constuctor */
	CEnvironment(CSpecs_t *problem);

	/* accessor for the start and end points */
	/* return the average of the lower and upperbounds for the
	   start and end points. if pointers to lowerbound and upper
	   bound pose2 structs are provided, those objects will be 
	   filled in appropiately */
	pose2 getStartPoint();
	pose2 getEndPoint();

	/* return whether the specified point is in the legal driving area. */
	bool isPointLegal(pose2 point, point2arr *boundArr = NULL);


	/* return whether all the points in the specified path are within
	   the legal driving region; should use the function isPointLegal.
	   also note that it takes a pointer to a vector of points, not
	   an actual vector (this is done for speed- passing a pointer is
	   faster), so it needs to be sure NOT to modify the points */
	bool isPathLegal(vector<pose2> *path);

	/* get the cost a the given point */
	double getPointCost(pose2 point);

	/* sum up the cost along all the points in the path and return
	   that. should use the getPointCost function and should NOT 
	   modify the points (it takes a pointer like isPathLegal) */
	double getPathCost(vector<pose2> *path);

	/* return the cost heuristic, the estimated cost from point to
	   the finish */
	double getHeuristicToGoal(pose2 point);

	/* return the heuristic between arbitrary points */
	double getHeuristic(pose2 point1, pose2 point2);

	double getInitialVelocity();

	double getGoalVelocity();

	/* whether to plan in forward or reverse or both */
	S1PlanMode getPlanMode();


};

#endif



