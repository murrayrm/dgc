/*
 * utils.hh
 * header file for random general functions from dave knowles code
 */

#ifndef CLOTHOIDUTILS_HH
#define CLOTHOIDUTILS_HH

#include <math.h>
#include "frames/pose2.hh"


#define EPS		6.0e-8 
#define MAXIT		100 
#define FPMIN		1.0E-30 
#define XMIN		1.5 
#define TRUE		1
#define ONE		Complex(1.0,0.0)

// Computes the Fresnel integrals S(x) and C(x) for all real x
void fresnel(float x, float *s, float *c);

// This subprogram produces the gauss-legendre numerical integral
double integrate(double a, double b, int n, double (*f)(double));

double integralD1(double alpha);

bool inbetween(double angle,double start,double finish,bool clockwise);

/* average 2 angles, but also put the average along the shorter of the 2 arcs between
   the angles. also get the average between -M_PI and M_PI */
double averageAngles(double angle1, double angle2);

/* get the angle between -M_PI and M_PI */
double unwrap_angle(double angle);

/* find the value of sigma and length for 2 symmetric paths */
bool findSigmaL(pose2 startPose, pose2 finishPose, bool reverse, double& sigma, double& new_length);


inline int sign(double a) { return (a == 0.0) ? 0 : (a<0.0 ? -1 : 1); };


#endif		// CLOTHOIDUTILS_HH


