/*
 * Environment.cc
 * Container for information about the enviroment Alice is working in.
 * This is pretty much just a wrapper for OCPspecs- ideally, everything
 * would be implemented in OCPspecs (which this class extends), and this
 * doesn't have any members explicitly in it. However, somethings we may
 * not want to put in OCPspecs, or we may want to override what is in
 * OCPspeacs for testing purposes. In those cases, methods can be put in
 * put in here. However, methods in here should be kept general and
 * independent of any structures unique to a specific planner.
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 1 May 07- Kenny Oslund
	- Created file
 *
 */

#include "Environment.hh"


/* normal constructor */
CEnvironment::CEnvironment(CSpecs_t *problem)
{
	m_cspecs = problem;

	update();

	return;
}

// populate any CEnvironment variables that need to be populated from CSpecs vairables
void CEnvironment::update()
{
	updateAliceSafety();
	updateCorridor();
}

// populate the corrodor in CEnvironement based on the corridor in cspecs. this will 
// grow the corridor so that the initial and final poses are legal.
void CEnvironment::updateCorridor()
{
	m_grownCorridor = m_cspecs->getBoundingPolygon();
	if ( !isPointLegal(getStartPoint()) )
		growCorridor(getStartPoint());
	if ( !isPointLegal(getEndPoint()) )
		growCorridor(getEndPoint());
}


// set a polygon representing alice and the specified safety region around her
void CEnvironment::updateAliceSafety()
{
	vector<double> safetyMargins = m_cspecs->getSafetyMargins();
	// populate a polygon which represents alice with the safety margins
	// if she's at the origin with 0 angle
	double front = DIST_REAR_AXLE_TO_FRONT + safetyMargins.at(SAFETY_FRONT);
	double back = -1 * (DIST_REAR_TO_REAR_AXLE + safetyMargins.at(SAFETY_BACK));
	double right = -1 * (VEHICLE_WIDTH / 2 + safetyMargins.at(SAFETY_RIGHT));
	double left = VEHICLE_WIDTH / 2 + safetyMargins.at(SAFETY_LEFT);
	point2 frontLeft(front, left);
	point2 frontRt(front, right);
	point2 backRt(back, right);
	point2 backLeft(back, left);
	m_aliceSafety.push_back(frontLeft);
	m_aliceSafety.push_back(frontRt);
	m_aliceSafety.push_back(backRt);
	m_aliceSafety.push_back(backLeft);
	m_aliceDiagonalDist = (frontLeft - backRt).norm();
}

// assuming that alice is at aliceLocation and that at least some of her is sticking
// into the corridor, grow the corridor by unioning an area around 
// with the existing corridor. This is to ensure that alice's location is legal
void CEnvironment::growCorridor(pose2 aliceLocation)
{
	// the amount beyond alice to grow the boundary
	#define ENVIRONMENT_GROW_BEYOND_ALICE		2
	vector<double> safetyMargins = m_cspecs->getSafetyMargins();
	point2arr grownAlice;
	vector<point2arr> grownRegion;

	// populate a polygon which represents alice with the safety margins
	// if she's at the origin with 0 angle
	double front = DIST_REAR_AXLE_TO_FRONT + safetyMargins.at(SAFETY_FRONT) + ENVIRONMENT_GROW_BEYOND_ALICE;
	double back = -1 * (DIST_REAR_TO_REAR_AXLE + safetyMargins.at(SAFETY_BACK) + ENVIRONMENT_GROW_BEYOND_ALICE);
	double right = -1 * (VEHICLE_WIDTH / 2 + safetyMargins.at(SAFETY_RIGHT) + ENVIRONMENT_GROW_BEYOND_ALICE);
	double left = VEHICLE_WIDTH / 2 + safetyMargins.at(SAFETY_LEFT) + ENVIRONMENT_GROW_BEYOND_ALICE;
	point2 frontLeft(front, left);
	point2 frontRt(front, right);
	point2 backRt(back, right);
	point2 backLeft(back, left);
	grownAlice.push_back(frontLeft);
	grownAlice.push_back(frontRt);
	grownAlice.push_back(backRt);
	grownAlice.push_back(backLeft);
	
	point2 pt(aliceLocation.x, aliceLocation.y);
	// rotate alice about the origin and translate her to where she is
	point2arr grownAliceTranslated = grownAlice.rot(aliceLocation.ang) + pt;
	int numPolygons = m_grownCorridor.get_poly_union(grownAliceTranslated, grownRegion);
	if (numPolygons == 1)
		m_grownCorridor = grownRegion.front();
	else
		Log::getStream(1) << "CLOTHOIDPLANNER: " << __FILE__ << ", line " << __LINE__ <<
			": ERROR:  Alice is completely out of the corridor; this algorithm cannot " <<
			"grow the corridor to compensate for this" << endl;
	return;
}

// get alice at the origin, with a safety zone around her
point2arr CEnvironment::getAliceSafety()
{
	return m_aliceSafety;
}
// get alice rotated and translated to the origin, with a safety region around her
point2arr CEnvironment::getAliceSafety(pose2 location)
{
	point2 pt(location.x, location.y);
	// rotate alice about the origin
	point2arr alice_rotated = m_aliceSafety.rot(location.ang);
	// translate her to where she is
	point2arr alice_trans = alice_rotated + pt;
	return alice_trans;
}


/* return the average of the lower and upperbounds for the
   start and end points. if pointers to lowerbound and upper
   bound pose2 structs are provided, those objects will be 
   filled in appropiately */
pose2 CEnvironment::getStartPoint()
{
	vector<double> conds = m_cspecs->getStartingState();
	pose2 start(conds.at(STATE_IDX_X), conds.at(STATE_IDX_Y), conds.at(STATE_IDX_THETA));
	return start;
}


pose2 CEnvironment::getEndPoint()
{
	vector<double> conds = m_cspecs->getFinalState();
	pose2 goal(conds.at(STATE_IDX_X), conds.at(STATE_IDX_Y), conds.at(STATE_IDX_THETA));
	return goal;
}


// a pointer to a point2arr which contains the surrounding polygon can optionally 
// be passed in for speed, or it can be generated here.
bool CEnvironment::isPointLegal(pose2 point)
{
	point2 pt(point.x, point.y);
	point2arr boundary = m_grownCorridor;
	#define CHECK_ALICE_IN_CORRIDOR
	#ifdef CHECK_ALICE_IN_CORRIDOR
	point2arr alice_trans = getAliceSafety(point);
	// take the intersection of alice and the boundary region. the result is the part of alice
	// that is inside the boundary. then we just have to check whether the intersection is equal 
	// to the original alice.
	vector<point2arr> alice_in_boundary;
	int num_polygon_intersections = alice_trans.get_poly_intersection(boundary, alice_in_boundary);
	point2arr intersection = alice_in_boundary.front();

	// debugging info
//	cout << "+++ translated Alice: " << alice_trans << endl;
//	cout << "+++ alice in boundary: " << intersection << endl;
//	cout << "+++ boundary: " << boundary << endl;

	// Taking the intersection can change the direction of the order of the points,
	// and the first point can be changed. so there are 3 conditions to check for whether the
	// intersection is equal to the original alice:
	// 1) there is still only 1 polygon in the intersection (alice didn't get cut into >1 piece)
	// 2) the polygon still has 4 points (its still a rectangle)
	// 3) the points are still in order, so points 1 and 3 of the intersection will still lie 
	// on a diagonal, as will points 2 and 4. since we took the intersection the diagonal distances
	// would necessarily be equal if the shape didn't change, and shorter if it did since it can
	// only get smaller; therefore we can just check those

	if (num_polygon_intersections != 1 || intersection.size() != 4
		|| fabs((intersection[1] - intersection[3]).norm() - m_aliceDiagonalDist) > 1e-3
		|| fabs((intersection[0] - intersection[2]).norm() - m_aliceDiagonalDist) > 1e-3)
		return false;
	else
		return true;
	#else
	return boundary.is_inside_poly(pt);
	#endif
}

/* return whether all the points in the specified path are within
	the legal driving region; should use the function isPointLegal.
	also note that it takes a pointer to a vector of points, not
	an actual vector (this is done for speed- passing a pointer is
	faster), so it needs to be sure NOT to modify the points */
bool CEnvironment::isPathLegal(vector<pose2> *path)
{
	// if even 1 point is illegal, the whole path is illegal
	for (vector<pose2>::iterator i = path->begin(); i < path->end(); i += 5)
		if ( !isPointLegal(*i) )
			return false;
	// also check the last point so we don't miss it
	if (!isPointLegal(path->back()))
		return false;

	return true;

}

/* get the cost a the given point */
double CEnvironment::getPointCost(pose2 point)
{
	CostMap *cmap = m_cspecs->getCostMap();
	point2 p = point2(point.x,point.y);
	double res = cmap->getPixelLoc(p);
//	Log::getStream(1) << "point Value: " << res << endl;
	return res;
}


/* sum up the cost along all the points in the path and return
	that. should use the getPointCost function and should NOT 
	modify the points (it takes a pointer like isPathLegal) */
double CEnvironment::getPathCost(vector<pose2> *path)
{
	double totalCost = 0;

	for (vector<pose2>::iterator i = path->begin(); i != path->end(); i++)
		totalCost += getPointCost(*i);

	return totalCost;
}

/* return the cost heuristic, the estimated cost from point to
	the finish */
double CEnvironment::getHeuristicToGoal(pose2 start)
{
	return getHeuristic(start, getEndPoint());
}

/* return the heuristic between 2 arbitrary points */
double CEnvironment::getHeuristic(pose2 start, pose2 goal)
{
	#define HEURISTIC_GAUSSIAN_WIDTH		4
	#define HEURISTIC_GAUSSIAN_HEIGHT		12
	#define HEURISTIC_DIST_TO_MIN			10
	#define HEURISTIC_DIST_SCALE			0.03
	#define HEURISTIC_ANGLE_SCALE			0
	#define HEURISTIC_SCALE_FACTOR			15.0

	

	// There are circular areas around the goal which we don't want to be in-
	// because of the vehicle dynamics, once we're in those areas, we
	// can't reach the goal without going out of them (and therefore 
	// getting further from the goal) or reversing a lot. so, set the 
	// heuristic to vew very high in those regions
	pose2 center1, center2, center3;
	pose2 trans1(VEHICLE_MIN_TURNING_RADIUS * sin(goal.ang),
		-1 * VEHICLE_MIN_TURNING_RADIUS * cos(goal.ang), 0);
	pose2 trans2(VEHICLE_MIN_TURNING_RADIUS * cos(goal.ang),
		VEHICLE_MIN_TURNING_RADIUS * sin(goal.ang), 0);
	center1 = goal + trans1;
	center2 = goal - trans1;
	center3 = goal + trans2;
// 	cout << "center 1 is at "; center1.display();
// 	cout << "center 2 is at "; center2.display();
// 	cout << "center 3 is at "; center3.display();
	// how far are we from the edge of our cirular areas?
	double d1 = (goal - center1).magnitude() - VEHICLE_MIN_TURNING_RADIUS;
	double d2 = (goal - center2).magnitude() - VEHICLE_MIN_TURNING_RADIUS;
	double d3 = (goal - center3).magnitude() - VEHICLE_MIN_TURNING_RADIUS;
	double gauss_dist = min( min(d1, d2), d3);
	double close_to_goal_cost;
	if (gauss_dist < 0)		// inside of one of these circular areas;
		close_to_goal_cost = HEURISTIC_GAUSSIAN_HEIGHT;
	else
		close_to_goal_cost =
			  HEURISTIC_GAUSSIAN_HEIGHT * exp( -1 * (gauss_dist * gauss_dist) / 
			    ( HEURISTIC_GAUSSIAN_WIDTH * HEURISTIC_GAUSSIAN_WIDTH) );

	// we also want to slide the minimum of the heuristic to be in front of our goal
	pose2 heuristic_min = pose2( HEURISTIC_DIST_TO_MIN * cos( goal.ang + M_PI ), 
		HEURISTIC_DIST_TO_MIN * sin( goal.ang + M_PI ), 0 ) + goal;
// 	cout << "heuristic min is at "; heuristic_min.display();

	pose2 diff_goal_heuristic_min = heuristic_min - start;

	return HEURISTIC_SCALE_FACTOR * 
		max ( close_to_goal_cost, 
			HEURISTIC_DIST_SCALE * (diff_goal_heuristic_min.x * diff_goal_heuristic_min.x 
				+ diff_goal_heuristic_min.y * diff_goal_heuristic_min.y)
			+ HEURISTIC_ANGLE_SCALE * fabs( (start - goal).ang)
			/*+ HEURISTIC_DIST_TO_MIN */);
//	return 2 * (4 * fabs((point2 - point1).ang) + (point2 - point1).magnitude());	// parking/ other zone
}

// return the average of the upper and lower bounds on velocity
// if ub and/or lb are not null, then they will be populated with the
// appropriate value
double CEnvironment::getInitialVelocity()
{
	vector<double> conds = m_cspecs->getStartingState();
	double v = conds.at(STATE_IDX_V);
	return v;
}

double CEnvironment::getGoalVelocity()
{
	vector<double> conds = m_cspecs->getFinalState();
	double v = conds.at(STATE_IDX_V);
	return v;
}

S1PlanMode CEnvironment::getPlanMode()
{
	return reverse_allowed;
}




