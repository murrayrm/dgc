/*
 * tempClothoidInterface.cc
 * temporary interface functions for 
 * the new planner stack to run the clothoid planner.
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 26 July 07- Kenny Oslund
	- Created file
 *
 */

#include "s1planner/tempClothoidInterface.hh"

GraphNode ClothoidPlannerInterface::nodeArr[GRAPH_PATH_MAX_NODES];

/* Do everything needed to generate a single trajectory and then clean up. All needed memory
   is allocated and freed in this function */
Err_t ClothoidPlannerInterface::GenerateTrajOneShot(int sn_key, bool disp_costmap, CSpecs_t cSpecs, Path_t *path, double runtime)
{
  CEnvironment *problem = new CEnvironment(&cSpecs);
  
  CClothoidPlanner *planner = new CClothoidPlanner(problem, NULL);
  
  planner->generatePaths(runtime);
  populatePath(planner, path);

	double pathLength;
	bool reaches_end;
        vector<pose2> solution = planner->getBestPath(&pathLength, &reaches_end);

	ofstream log("clothoidSolution.dat", ios_base::app);
	log << "****************** new planning cycle ***************** " << endl;
	log << " CEnvironment info:" << endl;
	log << " planning from: "; (problem->getStartPoint()).display(&log);
	log << " to: "; (problem->getEndPoint()).display(&log);
	if (problem->isPointLegal(problem->getStartPoint()))
		log << " Start point IS legal" << endl;
	else
		log << " Start point is NOT legal" << endl;
	if (problem->isPointLegal(problem->getEndPoint()))
		log << " End point IS legal" << endl;
	else
		log << " End point is NOT legal" << endl;

	planner->dumpBestSolution(&log);
	log << " num points in solution: " << solution.size() << endl;

  delete planner;
  delete problem; 
  return LP_OK;
}

void ClothoidPlannerInterface::populatePath(CClothoidPlanner *planner, Path_t *path)
{
	double pathLength;
	bool reaches_end;
  vector<pose2> solution = planner->getBestPath(&pathLength, &reaches_end);

	if (NULL != path) {
	  path->valid = true;
	  path->collideObs = 0;
	  path->collideCar = 0;
	  path->goalDist = pathLength;
	  path->pathLen = solution.size();
	  
	  int i = 0;
	  for (vector<pose2>::iterator j = solution.begin(); 
	       j != solution.end(); j++, i++)
	    {
        nodeArr[i].pose.pos.x = j->x;
	      nodeArr[i].pose.pos.y = j->y;
	      nodeArr[i].pose.pos.z = 0;
	      nodeArr[i].pose.rot = quat_from_rpy(0, 0, j->ang);
        // TODO: populate this with the correct info for either GRAPH_PATH_FWD or GRAPH_PATH_REV
        nodeArr[i].pathDir = GRAPH_PATH_FWD;
	      path->path[i] = &(nodeArr[i]);
	    }
	}

	return;
}

#ifdef NEVER
// trying to get rid of OCPspecs dependencies
void ClothoidPlannerInterface::setOCPparams(OCPparams& ocpParams, double velMax, double velMin, int mode, double* startingState, double* startingControls, double* finalState, double* finalControls)
{
  // set min and max speed to curr segment min/max speed (from mdf)
  ocpParams.parameters[VMIN_IDX_P] = velMin;
  ocpParams.parameters[VMAX_IDX_P] = velMax;

  // populate the initial conditions - lower bound
  ocpParams.initialConditionLB[EASTING_IDX_C] = startingState[1];
  ocpParams.initialConditionLB[NORTHING_IDX_C] = startingState[0];
  ocpParams.initialConditionLB[VELOCITY_IDX_C] = startingState[2];
  ocpParams.initialConditionLB[HEADING_IDX_C] = startingState[3];
  ocpParams.initialConditionLB[ACCELERATION_IDX_C] = startingControls[0];
  ocpParams.initialConditionLB[STEERING_IDX_C] = startingControls[1];

  // populate the initial conditions - upper bound
  ocpParams.initialConditionUB[EASTING_IDX_C] = startingState[1];
  ocpParams.initialConditionUB[NORTHING_IDX_C] = startingState[0];
  ocpParams.initialConditionUB[VELOCITY_IDX_C] = startingState[2];
  ocpParams.initialConditionUB[HEADING_IDX_C] = startingState[3];
  ocpParams.initialConditionUB[ACCELERATION_IDX_C] = startingControls[0];
  ocpParams.initialConditionUB[STEERING_IDX_C] = startingControls[1];

  // initialize to fwd mode by default
  ocpParams.mode = mode;

  // populate the initial conditions - lower bound
  ocpParams.finalConditionLB[EASTING_IDX_C] = finalState[1];
  ocpParams.finalConditionLB[NORTHING_IDX_C] = finalState[0];
  ocpParams.finalConditionLB[VELOCITY_IDX_C] = finalState[2];
  ocpParams.finalConditionLB[HEADING_IDX_C] = finalState[3];
  ocpParams.finalConditionLB[ACCELERATION_IDX_C] = finalControls[0];
  ocpParams.finalConditionLB[STEERING_IDX_C] = finalControls[1];

  // populate the initial conditions - upper bound
  ocpParams.finalConditionUB[EASTING_IDX_C] = finalState[1];
  ocpParams.finalConditionUB[NORTHING_IDX_C] = finalState[0];
  ocpParams.finalConditionUB[VELOCITY_IDX_C] = finalState[2];
  ocpParams.finalConditionUB[HEADING_IDX_C] = finalState[3];
  ocpParams.finalConditionUB[ACCELERATION_IDX_C] = finalControls[0];
  ocpParams.finalConditionUB[STEERING_IDX_C] = finalControls[1];
}

void ClothoidPlannerInterface::generatePolyCorridor(CPolytope** polyCorridor, int &nPolytopes, point2arr zonePerimeter)
{  
  bool isLegal = true;
  bool polyCorridorLegal;
  
  int NofDim = 2;
  CAlgebraicGeometry* algGeom = new CAlgebraicGeometry();
  int NofVtx = zonePerimeter.size();
  double*** vertices;
  vertices = new double**[nPolytopes];
  for(int k=0; k<nPolytopes;k++) {
    vertices[k] = new double*[NofDim];
    for(int i=0;i<NofDim;i++) {
      vertices[k][i] = new double[NofVtx];
    }
  }
  
  for (int k=0; k<NofVtx; k++) {
    vertices[0][0][k]=zonePerimeter[k].x;
    vertices[0][1][k]=zonePerimeter[k].y;
  }

  *polyCorridor = new CPolytope( &NofDim, &NofVtx, (const double** const)  vertices[0] );
  ALGErrorType algErr = algGeom->VertexAndFacetEnumeration(*polyCorridor);
  
  if( algErr != alg_NoError) {
    // cout << "CORRIDOR: error when building polytopes" << endl;
    isLegal = false;
  }
  
  for (int i=0; i<nPolytopes; i++) {
    for(int j=0; j<NofDim; j++)
      delete[] vertices[i][j];
    delete[] vertices[i];
  }
  delete[] vertices;
  delete algGeom;
  
  if(isLegal)
    polyCorridorLegal = true;
  else {   
    // cout << "CORRIDOR: FAILED - Corridor generation " << endl;
    polyCorridorLegal = false;
  }
}
 #endif
