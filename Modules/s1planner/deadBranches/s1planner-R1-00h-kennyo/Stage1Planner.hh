/*
 *  Stage1Planner.hh
 *  Stage1Planner
 *
 *  Created by Noel duToit
 *  2/28/07
 */

#ifndef STAGE1PLANNER_HH
#define STAGE1PLANNER_HH

#include <time.h>
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>
#include <iomanip>

#include "Environment.hh"
#include "clothoidplanner/ClothoidPlanner.hh"

//#include "frames/coords.hh"
//#include "dgcutils/RDDF.hh"
#include "skynettalker/StateClient.hh"
#include "trajutils/TrajTalker.hh"
#include "skynettalker/SkynetTalker.hh"
//#include "skynettalker/RDDFTalker.hh"
#include "trajutils/CPath.hh"
//#include "cmap/CMapPlus.hh"
#include "cmap/MapdeltaTalker.hh"
#include "dgcutils/DGCutils.hh"
#include "skynet/sn_msg.hh"
#include "interfaces/sn_types.h"
#include "ocpspecs/OCPtSpecs.hh"
#include "interfaces/TpDpInterface.hh"
#include "pseudocon/interface_superCon_trajF.hh"



using namespace sc_interface;
using namespace std;

#define DEBUG

/* theoretically the signal from tplanner for a stop is that the initial and final
   position are the same. however, This is not the case- they will be off by some
   amount. this is the max difference that there can be between the initial and
   final positions for it to still be considered a stop, in meters (final velocity
   must also be 0) */
#define S1PLANNER_STOP_CONDITION_THREASHOLD		0.5



class CStage1Planner:public CStateClient, public CTrajTalker, public CRDDFTalker
{
    public:
    	CStage1Planner(int SkynetKey, bool WAIT_STATE);
	~CStage1Planner(void);

	/* main loop */
	void ActiveLoop(void);

	/* generate a traj in the condition that we're stopped */
	bool getStoppedTraj();

	/* convert the lowest cost branch of the clothoid tree into a traj and
	   return it. generatePaths must have been called before this */
	bool getS1Traj();

	void generateVelocityProfile(vector<pose2> path, 
		vector<double>& vProfile, vector<double>& aProfile);

    private:		

	/* variables for sending data over skynet */
	int m_TrajFollowSendSocket;
	int m_SuperConSendSocket;

	CClothoidPlanner *planner;

    protected:
	  /* class that contains the trajectory and 2 time derivatives in northing 
	     and easting coordinates. to add points to it, use the command
	     m_Traj->addPoint(N, Nd, Ndd, E, Ed, Edd)
	     where N is northing, E is easting, and d denotes a derivative */
	  CTraj*    m_Traj;
	  /* direction the vehicle needs to travel at the current time- must set
	     scCmd.commandType to either tf_forwards or tf_reverse */
	  struct superConTrajFcmd scCmd;

	/* the following variables, classes and methods are setup for you and 
	   may be used by the extending class */

        CEnvironment* m_pProblemSpecs;
	double runtime;
	S1PlanMode PlanMode;

};

#endif /* STAGE1PLANNER_HH */
