/*
 *  Stage1Planner.cc
 *  Stage1Planner
 *
 *  Created by Noel duToit
 */

#include <time.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <math.h>

#include "Stage1Planner.hh"
#include "clothoidplanner/ClothoidPlanner.hh"
#include "dgcutils/DGCutils.hh"


CStage1Planner:: CStage1Planner(int SkynetKey, bool WAIT_STATE)
: CSkynetContainer(MODdynamicplanner,SkynetKey),CStateClient(WAIT_STATE), CRDDFTalker(true)
{
  /*** Create various objects ***/

  cout << "Constructing CState1Planner object" << endl;

  /* Create an OCPspecs object */
   m_pProblemSpecs = new CEnvironment(SkynetKey);
   if (m_pProblemSpecs == NULL)
      cout << "ERROR: OCPtSpecs object was NOT created successfully" << endl;
  
  /*** Initialize objects and variables ***/

  m_TrajFollowSendSocket = m_skynet.get_send_sock(SNtraj);
  m_SuperConSendSocket = m_skynet.get_send_sock(SNsuperconTrajfCmd);

  cout << "Construction Complete" << endl; 
}

CStage1Planner::~CStage1Planner(void)
{
//  if(m_Traj != NULL)
//    delete m_Traj;
  if (m_pProblemSpecs != NULL)
    delete m_pProblemSpecs;
}

void CStage1Planner::ActiveLoop(void)
{
  int numTrajs = 1;

  CClothoidPlanner *planner = new CClothoidPlanner(m_pProblemSpecs, &m_state);
  scCmd.commandType = tf_forwards;

  ofstream log("cycle_times.log");

  /* wait a couple of seconds so that hopefully we'll get good data on the 
     first time through */
  sleep(2);
  unsigned long long dgcTime, dgcDiffTime, dgcStartTime = DGCgettime();
  double timeTaken;
  bool haveNewTraj=false;
  pose2 initCond, finalCond;
  double initV, finalV;

  /*** The main planning loop- each time through this loop we generate 
       and send a trajectory ***/
//  for (int i= 0; i < 1; i++)
  while (1)
  {
	cout << " ***** Loop Starting ***** " << endl;
	/** Set up some variables and objects **/
	  /* eventually these will be determined from the driving environment state, but for
	     now just set them statically */
	  /* set up our mode- for now we'll only work on planning in the forward direction */
	  PlanMode = no_reverse;
	  runtime = 0.50;

	  /* get the most recent state data */
	  UpdateState();

	  // create the instance of the ctraj object here since its better toallocate and 
	  // free memory in the same function
	  m_Traj = new CTraj(3);
	  /* Initialize the trajectory */
	  m_Traj->startDataInput();

	  /* give time for ocpspecs to update */
	  usleep(100000);

	/** main work- this is a very simple state machine to handle a few cases **/

	// start to solve the problem- I think this locks variables while you're using them
	m_pProblemSpecs->startSolve();

	initCond = m_pProblemSpecs->getStartPoint();
	finalCond = m_pProblemSpecs-> getEndPoint();
	initV = m_pProblemSpecs->getInitialVelocity();
	finalV = m_pProblemSpecs->getGoalVelocity();

	// ---- Stopped Condition ----
	// should probably also check whether we're done with the mission...
	if ( fabs(finalV) < 1e-3
		&& (finalCond - initCond).magnitude() < 0.2 )
	{
		cout << "Stop Condition Detected" << endl;
		haveNewTraj = getStoppedTraj();
		usleep(200000);
	}
	// ---- Nominal Driving condition ----
	else
	{
		/********************************/
		/*** Calculate the trajectory ***/
		/********************************/
		planner->generatePaths(runtime, PlanMode);
		haveNewTraj = planner->getTraj(m_Traj);
	}

	// unlock variables
	m_pProblemSpecs->endSolve();

	/** send the data by skynet... Only if we have a new Traj **/
	if(haveNewTraj)
	{
		cout << "sending traj number " << numTrajs++ << endl;
		SendTraj(m_TrajFollowSendSocket, m_Traj);
	}
	else
	   cout << "Empty new trajectory... keeping the previous one" << endl;

	//m_Traj->print();

	  /* command the correct gear */
	  m_skynet.send_msg(m_SuperConSendSocket, &scCmd, sizeof(scCmd), 0);

	delete m_Traj;

	dgcTime = DGCgettime();
	dgcDiffTime = (unsigned long long)(dgcTime-dgcStartTime);
	timeTaken = DGCtimetosec(dgcDiffTime, false);
	log << "Time Taken: " << timeTaken << endl;
	dgcStartTime = dgcTime;

  }

  return;
}


// generate a trajectory which will cause the vehicle to just remain stopped in place
bool CStage1Planner::getStoppedTraj()
{
	pose2 initCond, finalCond;
	initCond = m_pProblemSpecs->getStartPoint();
	finalCond = m_pProblemSpecs-> getEndPoint();

	// the traj will have 1 points: one where the vehicle is now, and one
	// that is 10cm directly in front of the vehicle
	finalCond.x += 0.1 * cos(initCond.ang);
	finalCond.y += 0.1 * sin(initCond.ang);
	
	m_Traj->addPoint(initCond.x, 0, 0, initCond.y, 0, 0);
	m_Traj->addPoint(finalCond.x, 0, 0, finalCond.y, 0, 0);
	return true;
}





