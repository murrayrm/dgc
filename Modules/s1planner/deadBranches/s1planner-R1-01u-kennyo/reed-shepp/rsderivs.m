(* requires the functions defines in rs.m *)

delta = 0.001     (* delta too small causes inaccurate numerical evaluations
		     of derivatives due to round off error *)

(* numerical computation of derivatives *)

nd/: nd[x_, y_, phi_] :=
  Block[{f0 = N[dist[x,y,phi]],
         f1 = N[dist[x+delta,y,phi]],
         f2 = N[dist[x,y+delta,phi]]},
    If[f0 == "inv" || f1 == "inv" || f2 == "inv", "inv",
	{N[(f1 - f0)/delta], N[(f2 - f0)/delta]}]]

(* analytic evaluation of derivatives *)

ad/: ad[x_, y_, phi_] :=
  Block[{xflip = If[MemberQ[{2,4,6,8,11,12,15,16,19,20,23,24,27,28,31,
                             32,35,36,39,40,43,44,47,48},type], -1, 1],
         yflip = If[MemberQ[{3,4,7,8,10,12,14,16,18,20,22,24,26,28,30,
                             32,34,36,38,40,42,44,46,48},type], -1, 1],
	 phiflip = If[MemberQ[{2,3,6,7,10,11,14,15,18,19,22,23,26,27,30,
                               31,34,35,38,39,42,43,46,47},type], -1, 1],
	 derivs},
         derivs = Apply[ Switch[Floor[(type - 1)/4]+1,
                      1, Dc$c$c, 2, Dc$cc, 3, Dcsca, 4, Dcscb, 
                      5, Dccu$cuc, 6, Dc$cucu$c, 7, Dc$c2sca, 8, Dc$c2scb,
                      9, Dc$c2sc2$c, 10, Dcc$c, 11, Dcsc2$ca, 12, Dcsc2$cb],
                    {x * xflip, y * yflip, phi * phiflip}];
      {derivs[[1]] * xflip, derivs[[2]] * yflip}]

ad2/: ad2[x_,y_,phi_] :=
  Block[{derivs = Apply[Dcsca, {x, y, phi}]},
	derivs]

(* derivative functions *)

Dc$c$c/: Dc$c$c[x_, y_, phi_] :=
  {0.0, 0.0}

Dc$cc/: Dc$cc[x_, y_, phi_] :=
  Block[{a = x - Sin[phi],
         b = y + Cos[phi] - 1, u, v},
       u = Sqrt[a a + b b];
       v = Sqrt[16 - u u];
       N[{2 ((a u - b v) / (u u v)), 2 ((a v + b u) / (u u v))}]]

Dcsca/: Dcsca[x_, y_, phi_] :=
  Block[{a = x - Sin[phi],
         b = y + Cos[phi] - 1, u},
        u = Sqrt[a a + b b];
	N[{a / u, b / u}]]

Dcscb/: Dcscb[x_, y_, phi_] :=
  Block[{a = x + Sin[phi],
         b = y - Cos[phi] - 1, a2b2, u},
       a2b2 = a a + b b;
       u = Sqrt[a2b2 - 4];
       N[{(u a - 2 b) / a2b2, (u b + 2 a) / a2b2}]]

Dccu$cuc/: Dccu$cuc[x_, y_, phi_] :=
  Block[{a = x + Sin[phi],
         b = y - Cos[phi] - 1, u},
       u = Sqrt[a a + b b];
       If[u > 2.0,
         N[{(4a / u) / Sqrt[(6 - u)(2 + u)], (4b / u) / Sqrt[(6 - u)(2 + u)]}],
	 N[{(-4a / u) / Sqrt[(6 + u)(2 - u)], 
            (-4b / u) / Sqrt[(6 + u)(2 - u)]}]]]

Dc$cucu$c/: Dc$cucu$c[x_, y_, phi_] :=
  Block[{a = x + Sin[phi],
         b = y - Cos[phi] - 1, u},
       u = Sqrt[a a + b b];
       v = Sqrt[(6-u)(6+u)(u-2)(u+2)];
       N[{2 ((u u + 12) a - v b) / (u u v), 2 ((u u + 12) b + v a) / (u u v)}]]

Dc$c2sca/: Dc$c2sca[x_, y_, phi_] := 
  Block[{a = x - Sin[phi],
         b = y + Cos[phi] - 1, a2b2, u},
       a2b2 = a a + b b;
       u = Sqrt[a2b2 - 4];
       N[{(u a - 2 b) / a2b2, (u b + 2 a) / a2b2}]]

Dc$c2scb/: Dc$c2scb[x_, y_, phi_] := 
  Block[{a = x + Sin[phi],
         b = y - Cos[phi] - 1, u},
       u = Sqrt[a a + b b];
       N[{a / u, b / u}]]

Dc$c2sc2$c/: Dc$c2sc2$c[x_, y_, phi_] :=
  Dcscb[x, y, phi]

Dcc$c/: Dcc$c[x_, y_, phi_] :=
  Block[{a = x - Sin[phi],
         b = y + Cos[phi] - 1, u, v},
       u = Sqrt[a a + b b];
       v = Sqrt[16 - u u];
       N[{2 ((a u - b v) / (u u v)), 2 ((a v + b u) / (u u v))}]];

Dcsc2$ca/: Dcsc2$ca[x_, y_, phi_] :=
  Block[{a = x - Sin[phi],
         b = y + Cos[phi] - 1, u, v},
       a2b2 = a a + b b;
       u = Sqrt[a2b2 - 4];
       N[{(u a - 2 b) / a2b2, (u b + 2 a) / a2b2}]]

Dcsc2$cb/: Dcsc2$cb[x_, y_, phi_] :=
  Block[{a = x + Sin[phi],
         b = y - Cos[phi] - 1, u},
       u = Sqrt[a a + b b];
       N[{a / u, b / u}]]

(* compare numeric and closed form derivative values *)

test/: test[] :=
Block[{numericD, analyticD, err, maxerr = 0},
  Print["testing path type ", type, " =========================="];
  Do[Block[{},
    Print["phi = ",phi];
    Do[
      Do[Block[{},
        numericD = nd[x,y,phi];
	If[!SameQ[numericD,"inv"],
           Block[{},
             analyticD = ad[x,y,phi];
	     err = Max[Abs[numericD - analyticD]];
             If[err > maxerr, maxerr = err]
	     If[err > 0.001, 
               Print[{x,y,phi}, " : ", numericD, " ", analyticD, " ", err]]]]],
      {y, -6, 6, 2}],
    {x, -6, 6, 2}]],
  {phi, -3, 3, 1}];
  Print["maximum error = ", maxerr];
  maxerr]

test/: test[newtype_] :=
  Block[{}, type = newtype; test[]]

testMany/: testMany[types_] := Map[ test, types]

