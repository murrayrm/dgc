/*
 * Environment.cc
 * Container for information about the enviroment Alice is working in.
 * This is pretty much just a wrapper for OCPspecs- ideally, everything
 * would be implemented in OCPspecs (which this class extends), and this
 * doesn't have any members explicitly in it. However, somethings we may
 * not want to put in OCPspecs, or we may want to override what is in
 * OCPspeacs for testing purposes. In those cases, methods can be put in
 * put in here. However, methods in here should be kept general and
 * independent of any structures unique to a specific planner.
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 1 May 07- Kenny Oslund
	- Created file
 *
 */

#include "Environment.hh"




/* Constructor */
CEnvironment::CEnvironment(int SkynetKey, bool display_costmap) : 
	CSkynetContainer(MODdynamicplanner,SkynetKey), 
	OCPtSpecs(SkynetKey,false,3,true,false,true,false,true,display_costmap)
{


	cout << "created OCPtspecs object" << endl;


	return;
}



/* return the average of the lower and upperbounds for the
   start and end points. if pointers to lowerbound and upper
   bound pose2 structs are provided, those objects will be 
   filled in appropiately */
pose2 CEnvironment::getStartPoint(pose2 *lowerBound, pose2 *upperBound)
{
	double condsUB[COND_NUMBER];
	double condsLB[COND_NUMBER];
	BoundsType bType;

	// Get start upper and lower bounds
	bType = ubt_UpperBound;
	getInitialCondition(&bType, condsUB);
	bType = ubt_LowerBound;
	getInitialCondition(&bType, condsLB);


	pose2 ub(condsUB[NORTHING_IDX_C], condsUB[EASTING_IDX_C], 
		condsUB[HEADING_IDX_C]);
	pose2 lb(condsLB[NORTHING_IDX_C], condsLB[EASTING_IDX_C], 
		condsLB[HEADING_IDX_C]);
	if (lowerBound != NULL)
		*lowerBound = lb;
	if (upperBound != NULL)
		*upperBound = ub;

	pose2 average = (ub + lb) / 2;
	average.setAngle(averageAngles(ub.ang, lb.ang));

	return average;

	// stub for testing
	//return pose2(-25,10,M_PI/4);

}


// Note: upper and lower bounds are NOT adjusted if the end point is outside 
// of the corridor
pose2 CEnvironment::getEndPoint(pose2 *lowerBound, pose2 *upperBound)
{
	double condsUB[COND_NUMBER];
	double condsLB[COND_NUMBER];
	BoundsType bType;

	// Get start upper and lower bounds
	bType = ubt_UpperBound;
	getFinalCondition(&bType, condsUB);
	bType = ubt_LowerBound;
	getFinalCondition(&bType, condsLB);

	pose2 ub(condsUB[NORTHING_IDX_C], condsUB[EASTING_IDX_C], 
		condsUB[HEADING_IDX_C]);
	pose2 lb(condsLB[NORTHING_IDX_C], condsLB[EASTING_IDX_C], 
		condsLB[HEADING_IDX_C]);

	pose2 average = (ub + lb) / 2;

	// and attempt to get a legal end point if the end point is outside of the
	// legal driving zone, but it creates problems if we just need to wait
	// for a new end point
  	if( !isPointLegal(average) && getNpolytopes() > 0 )
  	{
  		double new_end[2];
		double old_end[2];
		old_end[0] = average.x ;
		old_end[1] = average.y ;
  		getLegalEndPoint(2, old_end, new_end);
  		average.x = new_end[0];
  		average.y = new_end[1];
  	}

	if (lowerBound != NULL)
		*lowerBound = lb;
	if (upperBound != NULL)
		*upperBound = ub;
	average.setAngle(averageAngles(ub.ang, lb.ang));

	return average;
	// stub for testing
	//return pose2(60,-15,-M_PI / 2);

}


bool CEnvironment::isPointLegal(pose2 point, int& startInd)
{
	double x[2];
	x[0] = point.x;
	x[1] = point.y;

	if (startInd < 0)
		startInd = isInsideCorridor(x, 2);
	else
		// this won't handle reverse right now
		startInd = isInsideCorridor(x, 2, startInd);

//	startInd = isInsideCorridor(x, 2);

	if ( startInd < 0 )
		return false;
	else
		return true;
}

bool CEnvironment::isPointLegal(pose2 point)
{
	int ind = -1;
	return isPointLegal(point, ind);
}


/* return whether all the points in the specified path are within
	the legal driving region; should use the function isPointLegal.
	also note that it takes a pointer to a vector of points, not
	an actual vector (this is done for speed- passing a pointer is
	faster), so it needs to be sure NOT to modify the points */
bool CEnvironment::isPathLegal(vector<pose2> *path)
{
	int index = -1;

	// if even 1 point is illegal, the whole path is illegal
	for (vector<pose2>::iterator i = path->begin(); i != path->end(); i++)
		if (!isPointLegal(*i, index))
			return false;
	return true;

}

/* get the cost a the given point */
double CEnvironment::getPointCost(pose2 point)
{
	return getCostMapValue(point.x, point.y);
}


/* sum up the cost along all the points in the path and return
	that. should use the getPointCost function and should NOT 
	modify the points (it takes a pointer like isPathLegal) */
double CEnvironment::getPathCost(vector<pose2> *path)
{
	double totalCost = 0;

	for (vector<pose2>::iterator i = path->begin(); i != path->end(); i++)
		totalCost += getPointCost(*i);

	return totalCost;

	// stub for testing
//	pose2 d = path->back() - path->front();
//	return d.magnitude();
}

/* return the cost heuristic, the estimated cost from point to
	the finish */
double CEnvironment::getHeuristicToGoal(pose2 point)
{
	return ENVIRONMENT_GOAL_HEURISTIC_SCALE_FACTOR * 
		getHeuristic(point, getEndPoint());
}

/* return the heuristic between 2 arbitrary points */
double CEnvironment::getHeuristic(pose2 point1, pose2 point2)
{
	#ifdef ENVIRONMENT_HEURISTIC_USE_OCPSPECS
	double pt1[2], pt2[2];
	pt1[0] = point1.x; pt1[1] = point1.y;
	pt2[0] = point2.x; pt2[1] = point2.y;

	return corridorCostToGo(pt1, pt2);
	#else
	return (point2 - point1).magnitude();
	#endif
}

// return the average of the upper and lower bounds on velocity
// if ub and/or lb are not null, then they will be populated with the
// appropriate value
double CEnvironment::getInitialVelocity(double *lb, double *ub)
{
	double condsUB[COND_NUMBER];
	double condsLB[COND_NUMBER];
	BoundsType bType;

	// Get start upper and lower bounds
	bType = ubt_UpperBound;
	getInitialCondition(&bType, condsUB);
	bType = ubt_LowerBound;
	getInitialCondition(&bType, condsLB);

	if (lb != NULL)
		*lb = condsLB[VELOCITY_IDX_C];
	if (ub != NULL)
		*ub = condsUB[VELOCITY_IDX_C];

	return (condsLB[VELOCITY_IDX_C] + condsUB[VELOCITY_IDX_C]) / 2;
}

double CEnvironment::getGoalVelocity(double *lb, double *ub)
{
	double condsUB[COND_NUMBER];
	double condsLB[COND_NUMBER];
	BoundsType bType;

	// Get start upper and lower bounds
	bType = ubt_UpperBound;
	getFinalCondition(&bType, condsUB);
	bType = ubt_LowerBound;
	getFinalCondition(&bType, condsLB);

	if (lb != NULL)
		*lb = condsLB[VELOCITY_IDX_C];
	if (ub != NULL)
		*ub = condsUB[VELOCITY_IDX_C];

	return (condsLB[VELOCITY_IDX_C] + condsUB[VELOCITY_IDX_C]) / 2;
}

S1PlanMode CEnvironment::getPlanMode()
{
	  /* set up the planning mode- forward or reverse */
	  int OCPspecsPlanMode;
	  getMode(&OCPspecsPlanMode);
	  if ( OCPspecsPlanMode == md_REV )
		return reverse_required;
	  else
		return no_reverse;
}




