/*
 * tempClothoidInterface.cc
 * temporary interface functions for 
 * the new planner stack to run the clothoid planner.
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 26 July 07- Kenny Oslund
	- Created file
 *
 */

#include "s1planner/tempClothoidInterface.hh"

GraphNode ClothoidPlannerInterface::nodeArr[GRAPH_PATH_MAX_NODES];
CEnvironment *ClothoidPlannerInterface::problem = new CEnvironment();
CClothoidPlanner *ClothoidPlannerInterface::planner = new CClothoidPlanner(problem, NULL);
int ClothoidPlannerInterface::cycleCount = 0;
int ClothoidPlannerInterface::failureCount = 0;

/* reset the clothoid tree by deleting all the clothoids, so that we plan from scratch */
void ClothoidPlannerInterface::clearClothoidTree(CSpecs_t *cSpecs)
{
	Log::getStream(6) << "CLOTHOIDPLANNERINTERFACE: clearing clothoid tree"<< endl;
	problem->update(cSpecs);
	failureCount = 0;
	planner->initialize();
}

/* update the clothoid tree and return the path */
Err_t ClothoidPlannerInterface::GenerateTraj(CSpecs_t *cSpecs, Path_t *path, double runtime, int sn_key, bool disp_costmap)
{
	Err_t error = S1PLANNER_OK;

	if (cSpecs != NULL)
		problem->update(cSpecs);

	/** some debugging info **/
	pose2 pt;
	Console::addMessage("CLOTHOIDPLANNER:: ******** new planning cycle ******** ");
	pt= problem->getStartPoint();
	Console::addMessage("CLOTHOIDPLANNER:: planning from: x: %lf y: %lf ang: %lf", pt.x, pt.y, pt.ang);
	// log final point
	pt = problem->getEndPoint();
	Console::addMessage("CLOTHOIDPLANNER:: to: x: %lf y: %lf ang: %lf", pt.x, pt.y, pt.ang);
 	/* 
	if (problem->isPointLegal(problem->getStartPoint()))
 		Console::addMessage("CLOTHOIDPLANNER:: Start point IS legal");
 	else
 		Console::addMessage("CLOTHOIDPLANNER:: Start point is NOT legal");
 	if (problem->isPointLegal(problem->getEndPoint()))
 		Console::addMessage("CLOTHOIDPLANNER:: End point IS legal");
	else
 		Console::addMessage("CLOTHOIDPLANNER:: End point is NOT legal");
	*/

	/** the actual work **/
	if ( problem->intersectsObstacle(problem->getStartPoint()) )
		error |= S1PLANNER_START_BLOCKED;
	else
	{
		// don't even bother to plan if the start point is blocked- we won't get anywhere,
		// but we can go ahead and plan if the end point is blocked- we'll get closer, just
		// not all the way there. still report if the end point is blocked though
		if ( problem->intersectsObstacle(problem->getEndPoint()) )
			error |= S1PLANNER_END_BLOCKED;
		planner->generatePaths(runtime);
	}
	error |= populatePath(path);
	Log::getStream(7) << "CLOTHOIDPLANNER: Error status: " << error << endl;
	if ( error != S1PLANNER_OK )
		failureCount++;
	else
		failureCount = 0;

	/** log the solution- this is VERY time intensive! */
	/*
 	char logFile[40];
 	sprintf(logFile, "cTree%d.dat", cycleCount);
 	ofstream cTreeLog(logFile);
 	planner->dumpTree(&cTreeLog);
 
 	sprintf(logFile, "cSol%d.dat", cycleCount);
 	ofstream cSolLog(logFile);
 	planner->dumpBestSolution(&cSolLog);
	*/

 	cycleCount++;
	// only report out S1PLANNER_FAILED if we've failed MAX_ERROR_COUNT times or more
	// so mask off that bit if we haven't failed enough times
	if ( failureCount <= MAX_ERROR_COUNT )
		return ( error & (~S1PLANNER_FAILED) );
	else 	
	 	return error;
}

Err_t ClothoidPlannerInterface::populatePath(Path_t *path)
{
	bool reaches_end;
	Err_t error = S1PLANNER_OK;
	vector<CElementaryPath *> solution = planner->getBestClothoidSequence(&reaches_end);

	if (NULL != path) {
	  path->valid = true;
	  path->collideObs = 0;
	  path->collideCar = 0;
	  
	  if (solution.size() > 0) {
		  CElementaryPath *lastPath = solution.back();
		  if ( !lastPath->m_atFinish )
			error |= S1PLANNER_FAILED;
		  if ( lastPath->getCost() > S1PLANNER_HIGH_COST_VALUE )
			error |= S1PLANNER_COST_TOO_HIGH;

		  // we skip the first point of each clothoid so that we don't end up with
		  // points right on top each other (that point was in the previous clothoid).
		  // however, we do want the first point of the first clothoid because there was
		  // no previous clothoid
		  // handle the special case of the first point
	          pose2 firstPt = solution.at(0)->getFirstPoint();
		  setPoint(0, firstPt, solution.at(0)->m_reverse, path);

	          // The rest of the points
	          int i = 1;
	          // iterator over the clothoids
	          for (vector<CElementaryPath *>::iterator j = solution.begin(); 
				j != solution.end(); j++) {
			vector<pose2> clothoid = (*j)->getClothoidPoints();
			// iterate over the points in the current clothoid
			// skip the first point of each clothoid so that we don't end up
			// with 2 points in exactly the same place
			for (vector<pose2>::iterator k = clothoid.begin() + 1; k != clothoid.end();
					k++, i++) {
				setPoint(i, *k, (*j)->m_reverse, path);
			}
		  }
		  path->goalDist = i * PATH_SECTION_LENGTH;
		  path->pathLen = i;
	  }
	  else
		error |= S1PLANNER_FAILED;
	}
	else
		error |= S1PLANNER_FAILED;
	return error;
}

void ClothoidPlannerInterface::setPoint(int index, pose2 newPt, bool reverse, Path_t *path)
{
	  nodeArr[index].index = index;
	  nodeArr[index].type = GRAPH_NODE_ZONE;
          nodeArr[index].pose.pos.x = newPt.x;
          nodeArr[index].pose.pos.y = newPt.y;
          nodeArr[index].pose.pos.z = 0;
          if ( reverse ) {
            nodeArr[index].pathDir = GRAPH_PATH_REV;
	    // reverse the direction of the pose to point in the direction we're going rather
	    // than the direction we're pointing if we're going in reverse
            newPt.rotateInPlace(M_PI);
          } else
            nodeArr[index].pathDir = GRAPH_PATH_FWD;
          nodeArr[index].pose.rot = quat_from_rpy(0, 0, newPt.ang);
          path->path[index] = &(nodeArr[index]);
}


/* Do everything needed to generate a single trajectory and then clean up. All needed memory
   is allocated and freed in this function --- DEPRECIATED */
Err_t ClothoidPlannerInterface::GenerateTrajOneShot(int sn_key, bool disp_costmap, CSpecs_t cSpecs, Path_t *path, double runtime)
{
	Console::addMessage("CLOTHOIDPLANNER:: ******** new planning cycle ******** ");
	
	// create objects
	CEnvironment *problem = new CEnvironment(&cSpecs);
	CClothoidPlanner *planner = new CClothoidPlanner(problem, NULL);
	
	// do the actual work
	planner->generatePaths(runtime);
	populatePath(path);

	// logging
	stringstream str;
	pose2 pt;
	// log inital point
	pt= problem->getStartPoint();
	Console::addMessage("CLOTHOIDPLANNER:: planning from: x: %lf y: %lf ang: %lf", pt.x, pt.y, pt.ang);
	// log final point
	pt = problem->getEndPoint();
	Console::addMessage("CLOTHOIDPLANNER:: to: x: %lf y: %lf ang: %lf", pt.x, pt.y, pt.ang);
/*	if (problem->isPointLegal(problem->getStartPoint()))
		Console::addMessage("CLOTHOIDPLANNER:: Start point IS legal");
	else
		Console::addMessage("CLOTHOIDPLANNER:: Start point is NOT legal");
	if (problem->isPointLegal(problem->getEndPoint()))
		Console::addMessage("CLOTHOIDPLANNER:: End point IS legal");
	else
		Console::addMessage("CLOTHOIDPLANNER:: End point is NOT legal"); */
	//planner->dumpBestSolution(&log);
	//log << " num points in solution: " << solution.size() << endl;

	// clean up
	delete planner;
	delete problem; 
	return LP_OK;
}



