/*
 *  Stage1Planner.cc
 *  Stage1Planner
 *
 *  Created by Noel duToit
 */

#include <time.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <math.h>

#include "Stage1Planner.hh"
#include "clothoidplanner/ClothoidPlanner.hh"



CStage1Planner:: CStage1Planner(int SkynetKey, bool WAIT_STATE)
: CSkynetContainer(MODdynamicplanner,SkynetKey),CStateClient(WAIT_STATE), CRDDFTalker(true)
{
  /*** Create various objects ***/

  cout << "Constructing CState1Planner object" << endl;

  /* Create an OCPspecs object */
   m_pProblemSpecs = new CEnvironment(SkynetKey);
   if (m_pProblemSpecs == NULL)
      cout << "ERROR: OCPtSpecs object was NOT created successfully" << endl;
  
  /*** Initialize objects and variables ***/

  m_TrajFollowSendSocket = m_skynet.get_send_sock(SNtraj);
  m_SuperConSendSocket = m_skynet.get_send_sock(SNsuperconTrajfCmd);

  cout << "Construction Complete" << endl; 
}

CStage1Planner::~CStage1Planner(void)
{
//  if(m_Traj != NULL)
//    delete m_Traj;
  if (m_pProblemSpecs != NULL)
    delete m_pProblemSpecs;
}

void CStage1Planner::ActiveLoop(void)
{
  int numTrajs = 1;

  CClothoidPlanner *planner = new CClothoidPlanner(m_pProblemSpecs, &m_state);
  scCmd.commandType = tf_forwards;

  ofstream log("cycle_times.log");

  /* wait a couple of seconds so that hopefully we'll get good data on the 
     first time through */
  sleep(2);
  double time, prevTime = (double)clock() / (double)CLOCKS_PER_SEC;

  /*** The main planning loop- each time through this loop we generate 
       and send a trajectory ***/
//  for (int i= 0; i < 1; i++)
  while (1)
  {
	/*** Set up some variables and objects ***/

	/** Set some other variables **/
	  /* eventually these will be determined from the driving environment state, but for
	     now just set them statically */
	  /* set up our mode- for now we'll only work on planning in the forward direction */
	  PlanMode = no_reverse;
	  runtime = 0.10;

	  /* get the most recent state data */
	  UpdateState();

	/********************************/
	/*** Calculate the trajectory ***/
	/********************************/
	planner->generatePaths(runtime, PlanMode);

	cout << "sending traj number " << numTrajs << endl;
	numTrajs++;

	m_Traj = planner->getTraj();


	/*** send the data by skynet ***/
	  /* send the trajectory */
	  SendTraj(m_TrajFollowSendSocket, m_Traj);

	//m_Traj->print();

	  /* command the correct gear */
	  m_skynet.send_msg(m_SuperConSendSocket, &scCmd, sizeof(scCmd), 0);

	delete m_Traj;

	time = (double)clock() / (double)CLOCKS_PER_SEC;
	log << "Time Taken: " << (time - prevTime) << endl;
	prevTime = time;

  }

  return;
}








