/*
 *  Stage1Planner.cc
 *  Stage1Planner
 *
 *  Created by Noel duToit
 */

#include <time.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>

#include "Stage1Planner.hh"

CStage1Planner:: CStage1Planner(int SkynetKey, bool WAIT_STATE):CSkynetContainer(MODdynamicplanner,SkynetKey),CStateClient(WAIT_STATE), CRDDFTalker(true)
{
  /* Create an RDDF object */
  m_RDDF = NULL; 				//new RDDF("rddf.dat");
  if(m_RDDF != NULL)
    {
      cout << "RDDF object was created successfully" << endl;		
    }
  
  /* Create a Traj object (y,yd,ydd,x,xd,xdd) */
  m_Traj = new CTraj(3);
  if(m_Traj != NULL)
    {
      cout << "Traj object was created successfully" << endl;		
    }
  
  m_CostMapPlus = new CMapPlus();
  if(m_CostMapPlus != NULL)
    {
      cout << "Cost Map Plus object was created successfully" << endl;		
    }
  
  m_SendSocket = m_skynet.get_send_sock(SNtraj);
  
}

CStage1Planner::~CStage1Planner(void)
{
  delete m_RDDF;
  delete m_Traj;
  delete m_CostMapPlus;
}

void CStage1Planner::ActiveLoop(void)
{
  /* Get the current RDDF from tlanner via RDDFTalker */
  cout << "about to receive an rddf ..."<< endl;
  //WaitForRDDF();
  cout << "done waiting ..." << endl;
  //UpdateRDDF();
  cout << "done updating ..." << endl;
  //m_RDDF = &m_newRddf;
  cout << "done getting RDDF object ..."<< endl;
  cout.flush();
    
  //cout << "Printing RDDF ... " << endl;
  //m_RDDF->print();
    
  /* Get current state from astate */
  UpdateState();	
    
  /* Initialize the trajectory */
  //m_Traj->startDataInput();
  
  /* Sending  trajectory */
  //SendTraj(m_SendSocket, m_Traj);
  
  return;
}
