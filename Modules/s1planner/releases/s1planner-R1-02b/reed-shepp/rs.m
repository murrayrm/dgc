(* ====================================================================

   This code handles the Mathematica end of the interface between
   Mathematica and the C program for computing Reeds-Shepp paths.  The
   other end of the interface is the rsMathInt.c program.

   ====================================================================  *)

(* 
dist computes the length of the shortest path from the origin to a final
(x,y,phi) configuration using some subset of the 48 RS candidate paths.
If the global variable type is set to a number from 1 to 48, only that
particular path type is checked.  If type is set to a string such as
"cc_c" the corresponding subset of 4 candidate paths is checked.  Setting
type to "all" forces all the paths to be checked.
*)

dist/: dist[x_, y_, phi_] :=
  RunThrough[
    Switch[type,
 1,"rsMathInt  1",  2,"rsMathInt  2",  3,"rsMathInt  3",  4,"rsMathInt  4",
 5,"rsMathInt  5",  6,"rsMathInt  6",  7,"rsMathInt  7",  8,"rsMathInt  8", 
 9,"rsMathInt  9", 10,"rsMathInt 10", 11,"rsMathInt 11", 12,"rsMathInt 12",
13,"rsMathInt 13", 14,"rsMathInt 14", 15,"rsMathInt 15", 16,"rsMathInt 16",
17,"rsMathInt 17", 18,"rsMathInt 18", 19,"rsMathInt 19", 20,"rsMathInt 20",
21,"rsMathInt 21", 22,"rsMathInt 22", 23,"rsMathInt 23", 24,"rsMathInt 24",
25,"rsMathInt 25", 26,"rsMathInt 26", 27,"rsMathInt 27", 28,"rsMathInt 28",
29,"rsMathInt 29", 30,"rsMathInt 30", 31,"rsMathInt 31", 32,"rsMathInt 32",
33,"rsMathInt 33", 34,"rsMathInt 34", 35,"rsMathInt 35", 36,"rsMathInt 36",
37,"rsMathInt 37", 38,"rsMathInt 38", 39,"rsMathInt 39", 40,"rsMathInt 40",
41,"rsMathInt 41", 42,"rsMathInt 42", 43,"rsMathInt 43", 44,"rsMathInt 44",
45,"rsMathInt 45", 46,"rsMathInt 46", 47,"rsMathInt 47", 48,"rsMathInt 48",
"all",        "rsMathInt",              "c_c_c",      "rsMathInt 1 2 3 4",
"c_cc",       "rsMathInt 5 6 7 8",      "csca",       "rsMathInt 9 10 11 12",
"cscb",       "rsMathInt 13 14 15 16",  "ccu_cuc",    "rsMathInt 17 18 19 20",
"c_cucu_c",   "rsMathInt 21 22 23 24",  "c_c2sca",    "rsMathInt 25 26 27 28",
"c_c2scb",    "rsMathInt 29 30 31 32",  "c_c2sc2_c",  "rsMathInt 33 34 35 36",
"cc_c",       "rsMathInt 37 38 39 40",  "csc2_ca",    "rsMathInt 41 42 43 44",
"csc2_cb",    "rsMathInt 45 46 47 48"],
    N[CForm[{x,y,phi}]]]

(* Function RSdist is equivalent to function dist with type set to "all". *)

RSdist/: RSdist[x_, y_, phi_] := 
      RunThrough["rsMathInt", N[CForm[{x,y,phi}]]]

(* radial is useful for checking star-shapedness of the phi slices of ball *)
 
radial/: radial[phi_, dir_, r_] :=
	RSdist[r*Cos[dir], r*Sin[dir], phi]

