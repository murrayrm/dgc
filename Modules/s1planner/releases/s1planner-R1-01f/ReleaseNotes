              Release Notes for "s1planner" module

Release R1-01f (Mon Sep  3 18:17:31 2007):
	implemented checking cost map and checking for alice + a safety margin being in the corridor, however checking for whether alice is in the 
corridor is commented out currently because our starting and end points aren't ready for that

Release R1-01e (Thu Aug 30 18:02:17 2007):
	Fixed a small bug in the heading specification along a reverse path.

Release R1-01d (Wed Aug 29 20:34:43 2007):
	Added the direction of the path.
	Made the path population function more robust.

Release R1-01c (Wed Aug 29 14:32:11 2007):
	set up to use new version of cspecs

	added ability to populate the direction field

Release R1-01b (Wed Aug 29 10:57:11 2007):
	Fixed a memory leak in the path population function. The path is now correctly populated.

Release R1-01a (Tue Aug 21 16:48:14 2007):
	Removed a few couts and now rather displays some info in the planner Console.

Release R1-01 (Fri Aug 17 18:38:47 2007):
	Fixed some memory issues in ClothoidPlanner and tempClothoidInteface.  Fixed the 
copying of Ocpspecs to an Environment object.  Added code safety checks (null pointers, data 
structure of out bounds,...).  

Release R1-00z (Thu Aug 16 13:55:37 2007):
	Gave tempClothoidPlanner static member functions and fixed some memory leaks. Fixed call to these in 
UT_clothoidplanner.  

Release R1-00y (Mon Jul 23 20:14:39 2007):
	updated Makefile.yam to compile testS1 using g++ (required on OS X)

Release R1-00y-kennyo (Fri Aug  3 13:43:18 2007):
	<* Pleaase insert a release notes entry here. If none, then please delete these lines *>

Release R1-00x (Sun Jul 22 23:22:07 2007):
	mainly code clean up to make it more readable and maintainable, but also at least 1 bug from 
	s1planner-R1-00w has been fixed and a couple other minor changes.

Release R1-00w (Sun Jun 17 13:51:54 2007):
	major upgrade to s1planner- I changed the definition of a clothoid from a curv with 0 curvature at the end
	points and some max curvature in the middle, to a curve with an initial curvature at the start point and a 
	final curvature at the end point. 

	this allows us to create trajectories which correspond to turning the
	steering wheel and holding it turned; before it was impossible to create a traj which corresponded to 
	holding the steering wheel at any constant angle other than 0, so this upgrade vastly increases the
	set of possible paths, and allows us to more fully exploit alice's capabilities in terms of being able to
	maneuver. Thus we can now generate paths for maneuvering in much smaller spaces because we don't have to 
	have large sections of traj where we're driving essentially straight.

	all paths are now kinematically feasible as well (at least if the min 
	turning radius in alice_constants.hh is correct)

Release R1-00v (Wed Jun 13 17:17:33 2007):
	Post site visit release. Stefano changed the order in which the trajectory vs the 
commanded direction is sent to try to get around the shifting problem in gcdrive/gcfollower.

Release R1-00u (Sun Jun 10 13:55:18 2007):
    Now if the initial point is infeasible, we project that into the closest centroid of the polytopes, we plan from there, and we let Follower do the dirty job.

Release R1-00t (Sat Jun  9 13:57:47 2007):
    Resolved a race condition, inserted command line arguments to define speed,acceleration, and safe turn parameters

Release R1-00s (Fri Jun  8 12:30:54 2007):
    Version used in the Mock site visit 06-07. Avoid the oscillation by reducing the velocity when we are turning. For the moment we are maybe a little too slow (have youever seen an italian driving slow??). Performance tuning must be done on Alice.

Release R1-00r (Wed Jun  6 18:10:30 2007):
    Inserted (yet) another velocity Profiler. By default we still use the std one, to use the new one change the function call  "generateVelocityProfile(...)" in Stage1Planner.cc into "generateVelocityProfile3".

Release R1-00q (Tue Jun  5 14:19:03 2007):
	just minor tweaks and changes- about the only notible thing is that this switches back to stefano's velocity planner (change made at 
	noel's request), and increased max feasible curvature back to 0.25, but this check on whether a path is feasible or not has more problems

Release R1-00p (Sun Jun  3 21:57:13 2007):
        added command line option to show cost map (its diabled by default-
		add -C or --display-costmap on the commandline to bring up
		the display)

        reduced max curvature that is legal (it should be more accurate of
        alice's capabilities now, but still needs work  

	fixed problem with indeces which was causing me to delete valid paths and 
	reinitializewhen i shouldn't

Release R1-00o (Fri Jun  1 18:06:17 2007):
   Changed the condition to extend the search on the tree when goal point changes. Tuned the new velocity profiler to be less aggressive. Modified the function call to getLegalEndPoint.

Release R1-00n (Thu May 31 22:38:53 2007):
	put back in re-running search on nodes that used to be connected by 1 clothoid to the goal point. this is 
	because not re-searching these could mean that a change of end point eliminates a possiblity that passes 
	through the new end point, but yet that path isn't considered. this could create a sudden oscillation in the 
	trajectory.

	cleaned up access to information about whether to plan in reverse by putting an accessor in CEnvironment

	took out code to project in invalid end point into the last polytope because it was segfaulting

Release R1-00m (Thu May 31 17:38:36 2007):
	Added new dynamically feasible velocity profiler. Interfaced the profilers with the max velocity commanded by tplanner.
        The old velocity profiler is still there but it is not the default one.

Release R1-00l (Thu May 31 15:53:27 2007):
	initial implementation of reverse
	implementation of stefano's improved isInside corridor function
	decreased cycle time

Release R1-00k (Sun May 27  2:00:46 2007):
	a few minor tweaks:
	- switched back to using distance as the heuristic since the latest updates broke stefano's heuristic
	function which uses the polytope corridor. we can switch back to his function once its fixed
	- decreased the max accelleration and decelleration
	- tweaked the time that OCPspecs uses to update
	- set it to block if we've gone 10 (up from 5) planning cycles without the start /or/ end condtions changing
	(checking whether the end conditions change is new)

Release R1-00j (Thu May 24 20:59:09 2007):
	removed checks on nan's in velocity profile- figuring out what to replace them with was more trouble than 
	its worth (they'll pretty much always be fixed in future planning cycles anyways since the actual bug was
	somehere else (that was and still is fixed))

	modified update so that the start point for planning will always be projected onto the closest point on
	the previously planned trajectory. This is because previously it was somewhat non-deterministic whether 
	your next planning cycle planned from a point starting on the old traj, or the point that the vehicle is
	actually at. It is no longer non-deterministed- if the vehicle is too far from the old traj, you reset
	entirely and plan from scratch from where you are (currently too far is > 2 meters (reduced from 5 meters)). 
	otherwise you plan from the point on your previous traj which is closest to tplanner's specified inital 
	condition.

	reduced min acceleration (aka max deceleration) for smoother stops.

	eliminated immediate re-searching of old goal nodes when the finish is updated. those nodes will still be
	re-searched and explored if they look like they have a good cost (just like any other node will be)


Release R1-00i (Thu May 24 11:30:52 2007):
	updating between planning cycles is implemented. When you compile, that is the default method that it uses
	between planning cycles, but there is a constant defined in the file clothoidplanner/ClothoidPlanner.hh 
	called CLOTHOID_UPDATE_TREE. undefining this and recompiling will cause clothoid planner to switch back to
	the method where it reinitializes the search tree and plans from scratch everytime (this is the method all 
	the previous releases use- updating should be better, but it just wasn't implemented and debugged until now)

	I haven't done a lot of checking for memory leaks as a result of updating the clothoid tree, but it should 
	be ok. It is at least capable of runs up to several minutes without crashing (and probably longer), which 
	should enough for now.

	added some logic and error checks to make sure that we always produce traj's with a non-zero initial 
	velocity (unless we're in a stop condition of course), and to make sure that we don't end up
	with points in the traj which have NaN's in the velocity and acceleration columns (this was causing alice
	to stop and not move even though there was a traj).

	issues- 
	-the latest version to tplanner (R2-03) is horribly broken and will seg fault immediately if you try 
	to use it. I recomend using R2-02z (which i did my testing with), but that doesn't send polytopes by 
	default. However, its an easy fix to make. Just check it out as a work module, and uncomment the following 
	lines in TrafficPlanner.cc:
	          corridor.generatePolyCorridor();
                  cout << "about to send polytope corridor" << endl;
                  corridor.sendPolyCorridor();
	and
                  corridor.generatePolyCorridor();
                  corridor.sendPolyCorridor();
	- This stabilized the solution ALOT, but gcfollower is still doing a really bad job of following. in 
	simulation at least, you can now watch alice just get further and further from the trajectory while the traj 
	stays in exactly the same place. I can now say that this is because gcfollower is doing a horrible job, not 
	because I'm replanning and zeroing out the error. my planned trajectories are usually pretty good, but
	because gcfollower can't follow, we swing way wide while going around corners despite the fact that my 
	trajectory was good. (note that after we reach some error from the traj, the old traj is scrapped and we 
	replan from where we are, which does zero out the error and cause oscillations in the solution, but  
	if you watch it in simulation, you can see this is clearly caused by the error, not a cause of the error)



Release R1-00h (Wed May 23  1:12:49 2007):
	I realized that I wasn't actually using the cost map to calculate cost- I was using the length of the path. the actual cost map is
	now used, so we do a lot better job of staying in the lane.

	more inplementation and testing of functions to update the tree- what's there seems work, although its not complete, and so still
	isn't used (we still scrap the tree and replan from scratch each cycle)

	issues:
	- There was a bug in bitmap which caused s1planner to seg fault ~70% of the time- its fixed, but not released yet. use danielle's
	branch of bitmap until he releases a new version
	- latest release of tplanner is broken (again...) and doesn't send the polytopes. this has also been fixed, but not released, so use
	R2-02x until something > R2-02z is released

Release R1-00g (Tue May 22 17:47:10 2007):
	initial integration of stefano's improved heuristic function

	restructuring of code and partial implementation of routines to update the clothoid tree between planning cycles rather than
	scrapping it and recomputing from scratch. most of this still isn't called yet- planning is still restarted from scratch
	between planning cycles right now, but I'm beginning to implement the functions needed to update

	velocity planner and function to populate the Ctraj moved to CStage1Planner from CClothoidPlanner. CClothoidPlanner
	doesn't deal with Ctraj's at all now

Release R1-00f (Mon May 21 17:30:16 2007):
	added check for stop condition, and also send correct traj when we need to be stopped (so alice stays stopped)

	removed check on the start point-
        now, if the start point isn't legal, s1planner will fail to find a
        traj, and the previous traj will be used. if we reach the end of
        the traj and the start point still isn't legal, s1planner will be
        stuck, but theoretically this shouldn't happen because a traj
        will always end in a point that was legal when the traj was
        planned.

	issues/ problems/ bugs/ features:
	* the stop condition is /supposed/ to be that the initial and final position are the same (in rddf planner, 
	  richard checks whether the initial and final positions are within 10^-5 metersof each other), and the final velocity is 0.
	  This is wishful thinking. In reality, I've seen the initial and final condition differ by up to 0.35m when we
	  are supposed to be stopped (to state the obvious, 0.35 >>>>>> 10^-5). currently, I have the threashold for detecting
	  whether we should be stopped set to whether the difference between the initial and final conditions is < 0.20m.
	  This seems to detect the stop pretty consistantly, but it still misses it sometimes. tplanner really should send a flag when
	  we're supposed to be stopped... however, this does behave well when we detect the stop
	* the most recent version of tplanner (R2-02w) is once again telling us to plan a traj for the entire santa anita loop at once. this is
	  something like a 500m long trajectory. s1planner is a local planner, and asking it to plan a 500 meter trajectory makes it
	  sad and unhappy, so it fails. This is especially the case since the improved heuristic and updating between planning cycles are not
	  yet implemented (both of those will improve s1planner's ability to plan longer traj's)
	* gcfollower was seg faulting a fair amount... don't know why


Release R1-00e (Sat May 19 15:16:29 2007):
	minor update to deal alittle with being able to plan very long trajectories, but this doesn't work very well

Release R1-00d (Sat May 19 13:17:00 2007):
   Working version of S1planner. It will (almost) always continue to drive, even if it is outside of the corridor or the current trajectory is empty.

Release R1-00c (Mon May 14 13:35:47 2007):
	First release with actual functionality (ie, not just a framework). This basically has everything that it needs for initial closed loop testing except for a velocity planner.

	more specifically, the tree of clothoids is built and the best clothoid is chosen from that using the cost function. the corridor constraints are also taken into account.
	the only major thing that is not yet implemented is updating the tree and reusing it between planning cycles. currently it just deletes the entire tree and recomputes it
	from scratch. The only major problem this can create is causing the trajectory that was found to change between planning cycles

	The relative weighting between the cost function and the heuristic for determining which node should be explored next also needs to be worked on.

	This also doesn't work with the current version of OCPtspecs- There is a transformation that is applied to the initial and final conditions on the angle that causes
	s1planner to plan for initial and final angles that are PI/2 radians off from where they should be, causing it to fail most of the time (because there is no dynamically
	feasible trajectory that stays in the corridor). this needs to be fixed in ocptspecs

Release R1-00b (Wed Feb 28 18:01:06 2007):
        Added a pointer to a OCPtSpecs object, which provides the access to the problem specs generated by tplanner. The functions to be used to access the problem specs are in the virtual class IOCPcomponent, in OTG_InterfacesX.hh,  defined by Melvin Flores. Code compiles.  	

Release R1-00a (Wed Feb 28 15:11:33 2007):
	Shell for stage 1 planner. Should be able to receive rddf's and cost 
map and send trajectories. Still needs to be integrated with ocpspecs. Compiles 
and ready to be populated.	

Release R1-00 (Wed Feb 28 10:21:48 2007):
	Created.




































