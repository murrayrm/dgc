/*----------------------------------------------------------------------
  Fichier              : /RIA/idefix/users/taix/c/Trajectoire/Reed_Shepp/reed_shepp.c
  Fonction             : cette fct retourne la longueur minimum entre
  2 conf. avec point de rebroussement ( voir l'article ).
  Date de creation     : Jeudi 27 Juillet 14:52:23 1989
  Date de modification : Mardi 17 Avril 10:09:42 1990
  Nb de lignes         : 407 
  Auteur               : Michel Taix
----------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/* #include "structure.h" */
/* #include "declare_fct.h" */
/* #include "varglob.h" */
/* #include "constante.h" */

#define infini HUGE_VAL

double mod2pi();
double c_c_c();
double c_cc();
double csca();
double cscb();
double ccu_cuc();
double c_cucu_c();
double c_c2sca();
double c_c2scb();
double c_c2sc2_c();
double cc_c();
double csc2_ca();
double csc2_cb();

double reed_shepp();
double reed_shepp2();

char *pathnames[48] = {
  "l+ r- l+", "l- r+ l-", "r+ l- r+", "r- l+ r-",
  "l+ r- l-", "l- r+ l+", "r+ l- r-", "r- l+ r+",
  "l+ s+ l+", "r+ s+ r+", "l- s- l-", "r- s- r-",
  "l+ s+ r+", "r+ s+ l+", "l- s- r-", "r- s- l-",
  "l+ r+ l- r-", "r+ l+ r- l-", "l- r- l+ r+", "r- l- r+ l+",
  "l+ r- l- r+", "r+ l- r- l+", "l- r+ l+ r-", "r- l+ r+ l-",
  "l+ r- s- l-", "r+ l- s- r-", "l- r+ s+ l+", "r- l+ s+ r+",
  "l+ r- s- r-", "r+ l- s- l-", "l- r+ s+ r+", "r- l+ s+ l+",
  "l+ r- s- l- r+", "r+ l- s- r- l+", "l- r+ s+ l+ r-", "r- l+ s+ r+ l-",
  "l+ r+ l-", "r+ l+ r-", "l- r- l+", "r- l- r+",
  "l+ s+ r+ l-", "r+ s+ l+ r-", "l- s- r- l+", "r- s- l- r+",
  "l+ s+ l+ r-", "r+ s+ r+ l-", "l- s- l- r+", "r- s- r- l+"};



/*----------------------------------------------------------------------

 Fonction  Entiere   : reed_shepp -  calcule la longueur min entre c1
 et c2 .Pour cela il faut passer en revue les 48 cas de R&S pour
 determiner le min et la structure CCSCC|| correspondante.
 Cette fonction retourne la longueur.

 longueur = longueur a parcourir entre c1 et c2
 num = numero de la structure CCSCC||
 
    *arg1  : c1  -  config. de depart
    *arg2  : c2  -  config. d'arrivee
----------------------------------------------------------------------*/

// c1[], c2[]- specify (x,y,phi) as an array of 3 doubles
double reed_shepp(double *c1 , double *c2  , int *numero , int *num_reverses ,
        int *first_dir , double *t_r , double *u_r , double *v_r)
{
  double x , y , phi;
  double t , u , v , t1 , u1 , v1;
  int num;
  double var , var_d, theta , alpha , dx , dy , longueur;

  /* Changement de repere,les courbes sont toujours calculees 
     de (0 0 0)--> (x , y , phi) */
  dx = c2[0] - c1[0];
  dy = c2[1] - c1[1];
  /* following line added by BVM to prevent sol'n blowing up when c2 is already
     at the origin (i.e. only phi changes) */
  if (dx == 0.0 && dy == 0.0) theta = 0.0; else
  theta = atan2( dy , dx);
  alpha = theta - c1[2];
  var_d = sqrt( dx*dx + dy*dy );
  x = cos(alpha)*var_d;
  y = sin(alpha)*var_d;
  var = c2[2] -c1[2];
  if( fabs( var) <= M_PI) phi = var;
  else {
    if( c2[2] >= c1[2] ) phi = var- 2*M_PI;
    else phi = mod2pi(var);
  }
                      /****  C | C | C ***/

  longueur = c_c_c(x , y , phi , &t1 , &u1 , &v1); /* l+ r- l+ */
      num = 1;                             
      t = t1; u = u1; v = v1;
      *num_reverses = 2;
      *first_dir = 1;
  var = c_c_c(-x , y , -phi , &t1 , &u1 , &v1); /* l- r+ l- */
  if( var < longueur){
      longueur = var;
      num = 2;
      t = t1; u = u1; v = v1;
      *num_reverses = 2;
      *first_dir = -1;
    }
  var = c_c_c(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ l- r+ */
  if( var < longueur){
      longueur = var;
      num = 3;
      t = t1; u = u1; v = v1;
      *num_reverses = 2;
      *first_dir = 1;
    }
  var = c_c_c(-x ,-y , phi , &t1 , &u1 , &v1); /* r- l+ r- */
  if( var < longueur){
      longueur = var;
      num = 4;
      t = t1; u = u1; v = v1;
      *num_reverses = 2;
      *first_dir = -1;
  }
                     /****  C | C C  ***/

  var = c_cc(x , y , phi , &t1 , &u1 , &v1); /* l+ r- l- */
  if( var < longueur){
    longueur = var;
    num = 5;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = 1;
  }
  var = c_cc(-x , y , -phi , &t1 , &u1 , &v1); /* l- r+ l+ */
  if( var < longueur){
    longueur = var;
    num = 6;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = -1;
  }
  var = c_cc(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ l- r- */
  if( var < longueur){
    longueur = var;
    num = 7;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = 1;
  }
  var = c_cc(-x ,-y , phi , &t1 , &u1 , &v1); /* r- l+ r+ */
  if( var < longueur){
    longueur = var;
    num = 8;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = -1;
  }
                   /****  C S C ****/

  var = csca(x , y , phi , &t1 , &u1 , &v1); /* l+ s+ l+ */
  if( var < longueur){
    longueur = var;
    num = 9;
    t = t1; u = u1; v = v1;
    *num_reverses = 0;
    *first_dir = 1;
  }
  var = csca(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ s+ r+ */
  if( var < longueur){
    longueur = var;
    num = 10;
    t = t1; u = u1; v = v1;
    *num_reverses = 0;
    *first_dir = 1;
  }
  var = csca(-x , y , -phi , &t1 , &u1 , &v1); /* l- s- l- */
  if( var < longueur){
    longueur = var;
    num = 11;
    t = t1; u = u1; v = v1;
    *num_reverses = 0;
    *first_dir = -1;
  }
  var = csca(-x ,-y , phi , &t1 , &u1 , &v1); /* r- s- r- */
  if( var < longueur){
    longueur = var;
    num = 12;
    t = t1; u = u1; v = v1;
    *num_reverses = 0;
    *first_dir = -1;
  }
  
  var = cscb(x , y , phi , &t1 , &u1 , &v1); /* l+ s+ r+ */
  if( var < longueur){
    longueur = var;
    num = 13;
    t = t1; u = u1; v = v1;
    *num_reverses = 0;
    *first_dir = 1;
  }
  var = cscb(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ s+ l+ */
  if( var < longueur){
    longueur = var;
    num = 14;
    t = t1; u = u1; v = v1;
    *num_reverses = 0;
    *first_dir = 1;
  }
  var = cscb(-x , y , -phi , &t1 , &u1 , &v1); /* l- s- r- */
  if( var < longueur){
    longueur = var;
    num = 15;
    t = t1; u = u1; v = v1;
    *num_reverses = 0;
    *first_dir = -1;
  }
  var = cscb(-x ,-y , phi , &t1 , &u1 , &v1); /* r- s- l- */
  if( var < longueur){
    longueur = var;
    num = 16;
    t = t1; u = u1; v = v1;
    *num_reverses = 0;
    *first_dir = -1;
  }

                      /*** C Cu | Cu C ***/
  var = ccu_cuc(x , y , phi , &t1 , &u1 , &v1); /* l+ r+ l- r- */
  if( var < longueur){
    longueur = var;
    num = 17;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = 1;
  }
  var = ccu_cuc(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ l+ r- l- */
  if( var < longueur){
    longueur = var;
    num = 18;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = 1;
  }
  var = ccu_cuc(-x , y , -phi , &t1 , &u1 , &v1); /* l- r- l+ r+ */
  if( var < longueur){
    longueur = var;
    num = 19;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = -1;
  }
  var = ccu_cuc(-x ,-y , phi , &t1 , &u1 , &v1); /* r- l- r+ l+ */
  if( var < longueur){
    longueur = var;
    num = 20;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = -1;
  }
  
                    /*** C | Cu Cu | C  ***/
  var = c_cucu_c(x , y , phi , &t1 , &u1 , &v1); /* l+ r- l- r+ */
  if( var < longueur){
    longueur = var;
    num = 21;
    t = t1; u = u1; v = v1;
    *num_reverses = 2;
    *first_dir = 1;
  }
  var = c_cucu_c(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ l- r- l+ */
  if( var < longueur){
    longueur = var;
    num = 22;
    t = t1; u = u1; v = v1;
    *num_reverses = 2;
    *first_dir = 1;
  }
  var = c_cucu_c(-x , y , -phi , &t1 , &u1 , &v1); /* l- r+ l+ r- */
  if( var < longueur){
    longueur = var;
    num = 23;
    t = t1; u = u1; v = v1;
    *num_reverses = 2;
    *first_dir = -1;
  }
  var = c_cucu_c(-x ,-y , phi , &t1 , &u1 , &v1); /* r- l+ r+ l- */
  if( var < longueur){
    longueur = var;
    num = 24;
    t = t1; u = u1; v = v1;
    *num_reverses = 2;
    *first_dir = -1;
  }
  
                 /*** C | C2 S C  ***/
  var = c_c2sca(x , y , phi , &t1 , &u1 , &v1); /* l+ r- s- l- */
  if( var < longueur){
    longueur = var;
    num = 25;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = 1;
  }
  var = c_c2sca(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ l- s- r- */
  if( var < longueur){
    longueur = var;
    num = 26;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = 1;
  }
  var = c_c2sca(-x , y , -phi , &t1 , &u1 , &v1); /* l- r+ s+ l+ */
  if( var < longueur){
    longueur = var;
    num = 27;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = -1;
  }
  var = c_c2sca(-x ,-y , phi , &t1 , &u1 , &v1); /* r- l+ s+ r+ */
  if( var < longueur){
    longueur = var;
    num = 28;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = -1;
  }
  var = c_c2scb(x , y , phi , &t1 , &u1 , &v1); /* l+ r- s- r- */
  if( var < longueur){
    longueur = var;
    num = 29;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = 1;
  }
  var = c_c2scb(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ l- s- l- */
  if( var < longueur){
    longueur = var;
    num = 30;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = 1;
  }
  var = c_c2scb(-x , y , -phi , &t1 , &u1 , &v1); /* l- r+ s+ r+ */
  if( var < longueur){
    longueur = var;
    num = 31;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = -1;
  }
  var = c_c2scb(-x ,-y , phi , &t1 , &u1 , &v1); /* r- l+ s+ l+ */
  if( var < longueur){
    longueur = var;
    num = 32;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = -1;
  }

              /*** C | C2 S C2 | C  ***/

  var = c_c2sc2_c(x , y , phi , &t1 , &u1 , &v1); /* l+ r- s- l- r+ */
  if( var < longueur){
    longueur = var;
    num = 33;
    t = t1; u = u1; v = v1;
    *num_reverses = 2;
    *first_dir = 1;
  }
  var = c_c2sc2_c(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ l- s- r- l+ */
  if( var < longueur){
    longueur = var;
    num = 34;
    t = t1; u = u1; v = v1;
    *num_reverses = 2;
    *first_dir = 1;
  }
  var = c_c2sc2_c(-x , y , -phi , &t1 , &u1 , &v1); /* l- r+ s+ l+ r- */
  if( var < longueur){
    longueur = var;
    num = 35;
    t = t1; u = u1; v = v1;
    *num_reverses = 2;
    *first_dir = -1;
  }
  var = c_c2sc2_c(-x ,-y , phi , &t1 , &u1 , &v1); /* r- l+ s+ r+ l- */
  if( var < longueur){
    longueur = var;
    num = 36;
    t = t1; u = u1; v = v1;
    *num_reverses = 2;
    *first_dir = -1;
  }  
  
               /***  C C | C  ****/

  var = cc_c(x , y , phi , &t1 , &u1 , &v1); /* l+ r+ l- */
  if( var < longueur){
    longueur = var;
    num = 37;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = 1;
  }
  var = cc_c(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ l+ r- */
  if( var < longueur){
    longueur = var;
    num = 38;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = 1;
  }
  var = cc_c(-x , y , -phi , &t1 , &u1 , &v1); /* l- r- l+ */
  if( var < longueur){
    longueur = var;
    num = 39;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = -1;
  }
  var = cc_c(-x ,-y , phi , &t1 , &u1 , &v1); /* r- l- r+ */
  if( var < longueur){
    longueur = var;
    num = 40;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = -1;
  }

              /*** C S C2 | C  ***/

  var = csc2_ca(x , y , phi , &t1 , &u1 , &v1); /* l+ s+ r+ l- */
  if( var < longueur){
    longueur = var;
    num = 41;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = 1;
  }
  var = csc2_ca(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ s+ l+ r- */
  if( var < longueur){
    longueur = var;
    num = 42;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = 1;
  }
  var = csc2_ca(-x , y , -phi , &t1 , &u1 , &v1); /* l- s- r- l+ */
  if( var < longueur){
    longueur = var;
    num = 43;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = -1;
  }
  var = csc2_ca(-x ,-y , phi , &t1 , &u1 , &v1); /* r- s- l- r+ */
  if( var < longueur){
    longueur = var;
    num = 44;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = -1;
  }

  var = csc2_cb(x , y , phi , &t1 , &u1 , &v1); /* l+ s+ l+ r- */
  if( var < longueur){
    longueur = var;
    num = 45;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = 1;
  }
  var = csc2_cb(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ s+ r+ l- */
  if( var < longueur){
    longueur = var;
    num = 46;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = 1;
  }
  var = csc2_cb(-x , y , -phi , &t1 , &u1 , &v1); /* l- s- l- r+ */
  if( var < longueur){
    longueur = var;
    num = 47;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = -1;
  }
  var = csc2_cb(-x ,-y , phi , &t1 , &u1 , &v1); /* r- s- r- l+ */
  if( var < longueur){
    longueur = var;
    num = 48;
    t = t1; u = u1; v = v1;
    *num_reverses = 1;
    *first_dir = -1;
  }
  *t_r = t;  *u_r = u;  *v_r = v;
  *numero = num;
  return( longueur);
}

/*----------------------------------------------------------------------

  Just like the reed_shepp function above, except now we only check
  one specific path type (1-48).

  Numero is passed in rather than out.

----------------------------------------------------------------------*/
double reed_shepp2(c1 , c2  , numero , t_r , u_r , v_r)
     /* Stconfig *c1 , *c2; */
     double c1[], c2[];  /* specify (x,y,phi) as an array of 3 doubles */
     double *t_r , *u_r , *v_r;
     int numero;
{
  double x , y , phi;
  double t , u , v , t1 , u1 , v1;
  int num;
  double var , var_d, theta , alpha , dx , dy , longueur;

  /* Changement de repere,les courbes sont toujours calculees 
     de (0 0 0)--> (x , y , phi) */
  dx = c2[0] - c1[0];
  dy = c2[1] - c1[1];
  /* following line added by BVM to prevent sol'n blowing up when c2 is already
     at the origin (i.e. only phi changes) */
  if (dx == 0.0 && dy == 0.0) theta = 0.0; else
  theta = atan2( dy , dx);
  alpha = theta - c1[2];
  var_d = sqrt( dx*dx + dy*dy );
  x = cos(alpha)*var_d;
  y = sin(alpha)*var_d;
  var = c2[2] -c1[2];
  if( fabs( var) <= M_PI) phi = var;
  else {
    if( c2[2] >= c1[2] ) phi = var- 2*M_PI;
    else phi = mod2pi(var);
  }

  switch (numero) {
  case 1:
    longueur = c_c_c(x , y , phi , &t1 , &u1 , &v1); /* l+ r- l+ */
    t = t1; u = u1; v = v1;
    break;
  case 2:
  longueur = c_c_c(-x , y , -phi , &t1 , &u1 , &v1); /* l- r+ l- */
      t = t1; u = u1; v = v1;
    break;
  case 3:
  longueur = c_c_c(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ l- r+ */
      t = t1; u = u1; v = v1;
    break;
  case 4:
    longueur = c_c_c(-x ,-y , phi , &t1 , &u1 , &v1); /* r- l+ r- */
    t = t1; u = u1; v = v1;
    break;
  case 5:
    longueur = c_cc(x , y , phi , &t1 , &u1 , &v1); /* l+ r- l- */
    t = t1; u = u1; v = v1;
    break;
  case 6:
    longueur = c_cc(-x , y , -phi , &t1 , &u1 , &v1); /* l- r+ l+ */
    t = t1; u = u1; v = v1;
    break;
  case 7:
    longueur = c_cc(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ l- r- */
    t = t1; u = u1; v = v1;
    break;
  case 8:
    longueur = c_cc(-x ,-y , phi , &t1 , &u1 , &v1); /* r- l+ r+ */
    t = t1; u = u1; v = v1;
    break;
  case 9:
    longueur = csca(x , y , phi , &t1 , &u1 , &v1); /* l+ s+ l+ */
    t = t1; u = u1; v = v1;
    break;
  case 10:
    longueur = csca(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ s+ r+ */
    t = t1; u = u1; v = v1;
    break;
  case 11:
    longueur = csca(-x , y , -phi , &t1 , &u1 , &v1); /* l- s- l- */
    t = t1; u = u1; v = v1;
    break;
  case 12:
    longueur = csca(-x ,-y , phi , &t1 , &u1 , &v1); /* r- s- r- */
    t = t1; u = u1; v = v1;
    break;
  case 13:
    longueur = cscb(x , y , phi , &t1 , &u1 , &v1); /* l+ s+ r+ */
    t = t1; u = u1; v = v1;
    break;
  case 14:
    longueur = cscb(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ s+ l+ */
    t = t1; u = u1; v = v1;
    break;
  case 15:
    longueur = cscb(-x , y , -phi , &t1 , &u1 , &v1); /* l- s- r- */
    t = t1; u = u1; v = v1;
    break;
  case 16:
    longueur = cscb(-x ,-y , phi , &t1 , &u1 , &v1); /* r- s- l- */
    t = t1; u = u1; v = v1;
    break;
  case 17:
    longueur = ccu_cuc(x , y , phi , &t1 , &u1 , &v1); /* l+ r+ l- r- */
    t = t1; u = u1; v = v1;
    break;
  case 18:
    longueur = ccu_cuc(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ l+ r- l- */
    t = t1; u = u1; v = v1;
    break;
  case 19:
    longueur = ccu_cuc(-x , y , -phi , &t1 , &u1 , &v1); /* l- r- l+ r+ */
    t = t1; u = u1; v = v1;
    break;
  case 20:
    longueur = ccu_cuc(-x ,-y , phi , &t1 , &u1 , &v1); /* r- l- r+ l+ */
    t = t1; u = u1; v = v1;
    break;
  case 21:
    longueur = c_cucu_c(x , y , phi , &t1 , &u1 , &v1); /* l+ r- l- r+ */
    t = t1; u = u1; v = v1;
    break;
  case 22:
    longueur = c_cucu_c(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ l- r- l+ */
    t = t1; u = u1; v = v1;
    break;
  case 23:
    longueur = c_cucu_c(-x , y , -phi , &t1 , &u1 , &v1); /* l- r+ l+ r- */
    t = t1; u = u1; v = v1;
    break;
  case 24:
    longueur = c_cucu_c(-x ,-y , phi , &t1 , &u1 , &v1); /* r- l+ r+ l- */
    t = t1; u = u1; v = v1;
    break;
  case 25:
    longueur = c_c2sca(x , y , phi , &t1 , &u1 , &v1); /* l+ r- s- l- */
    t = t1; u = u1; v = v1;
    break;
  case 26:
    longueur = c_c2sca(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ l- s- r- */
    t = t1; u = u1; v = v1;
    break;
  case 27:
    longueur = c_c2sca(-x , y , -phi , &t1 , &u1 , &v1); /* l- r+ s+ l+ */
    t = t1; u = u1; v = v1;
    break;
  case 28:
    longueur = c_c2sca(-x ,-y , phi , &t1 , &u1 , &v1); /* r- l+ s+ r+ */
    t = t1; u = u1; v = v1;
    break;
  case 29:
    longueur = c_c2scb(x , y , phi , &t1 , &u1 , &v1); /* l+ r- s- r- */
    t = t1; u = u1; v = v1;
    break;
  case 30:
    longueur = c_c2scb(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ l- s- l- */
    t = t1; u = u1; v = v1;
    break;
  case 31:
    longueur = c_c2scb(-x , y , -phi , &t1 , &u1 , &v1); /* l- r+ s+ r+ */
    t = t1; u = u1; v = v1;
    break;
  case 32:
    longueur = c_c2scb(-x ,-y , phi , &t1 , &u1 , &v1); /* r- l+ s+ l+ */
    t = t1; u = u1; v = v1;
    break;
  case 33:
    longueur = c_c2sc2_c(x , y , phi , &t1 , &u1 , &v1); /* l+ r- s- l- r+ */
    t = t1; u = u1; v = v1;
    break;
  case 34:
    longueur = c_c2sc2_c(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ l- s- r- l+ */
    t = t1; u = u1; v = v1;
    break;
  case 35:
    longueur = c_c2sc2_c(-x , y , -phi , &t1 , &u1 , &v1); /* l- r+ s+ l+ r- */
    t = t1; u = u1; v = v1;
    break;
  case 36:
    longueur = c_c2sc2_c(-x ,-y , phi , &t1 , &u1 , &v1); /* r- l+ s+ r+ l- */
    t = t1; u = u1; v = v1;
    break;
  case 37:
    longueur = cc_c(x , y , phi , &t1 , &u1 , &v1); /* l+ r+ l- */
    t = t1; u = u1; v = v1;
    break;
  case 38:
    longueur = cc_c(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ l+ r- */
    t = t1; u = u1; v = v1;
    break;
  case 39:
    longueur = cc_c(-x , y , -phi , &t1 , &u1 , &v1); /* l- r- l+ */
    t = t1; u = u1; v = v1;
    break;
  case 40:
    longueur = cc_c(-x ,-y , phi , &t1 , &u1 , &v1); /* r- l- r+ */
    t = t1; u = u1; v = v1;
    break;
  case 41:
    longueur = csc2_ca(x , y , phi , &t1 , &u1 , &v1); /* l+ s+ r+ l- */
    t = t1; u = u1; v = v1;
    break;
  case 42:
    longueur = csc2_ca(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ s+ l+ r- */
    t = t1; u = u1; v = v1;
    break;
  case 43:
    longueur = csc2_ca(-x , y , -phi , &t1 , &u1 , &v1); /* l- s- r- l+ */
    t = t1; u = u1; v = v1;
    break;
  case 44:
    longueur = csc2_ca(-x ,-y , phi , &t1 , &u1 , &v1); /* r- s- l- r+ */
    t = t1; u = u1; v = v1;
    break;
  case 45:
    longueur = csc2_cb(x , y , phi , &t1 , &u1 , &v1); /* l+ s+ l+ r- */
    t = t1; u = u1; v = v1;
    break;
  case 46:
    longueur = csc2_cb(x ,-y , -phi , &t1 , &u1 , &v1); /* r+ s+ r+ l- */
    t = t1; u = u1; v = v1;
    break;
  case 47:
    longueur = csc2_cb(-x , y , -phi , &t1 , &u1 , &v1); /* l- s- l- r+ */
    t = t1; u = u1; v = v1;
    break;
  case 48:
    longueur = csc2_cb(-x ,-y , phi , &t1 , &u1 , &v1); /* r- s- r- l+ */
    t = t1; u = u1; v = v1;
    break;
  }

  *t_r = t;  *u_r = u;  *v_r = v;
  return( longueur);
}

