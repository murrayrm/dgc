/*
 * ElementaryPath.hh
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 1 May 07- Kenny Oslund
	- Created file
 */

#ifndef ELEMENTARYPATH_HH
#define ELEMENTARYPATH_HH

#include <vector.h>
#include "frames/pose2.hh"
#include "trajutils/CPath.hh"
#include "alice/AliceConstants.h"
#include <temp-planner-interfaces/Console.hh>
#include "s1planner/Environment.hh"
#include "s1planner/ClothoidUtils.hh"

/* #define SEARCH_STATUS_LIST(_) \
  _( search_failed, = 0 ) \
  _( search_simple_path, ) \
  _( search_biel_path, )
DEFINE_ENUM(SearchStatus, SEARCH_STATUS_LIST) */


// whether the path end point has not been explored
#define PATH_UNEXPLORED				0
#define PATH_EXPLORED				1

// the reason's that a path can be invalid- this is a bit field for different errors
#define PATH_LEGAL				0
#define PATH_OUTSIDE_CORRIDOR			1
// path is blocked by an obstacle
#define PATH_BLOCKED				2
#define PATH_NOT_FEASIBLE			4


// max difference in angle (radians) between 2 poses for them to 
// be cosidered "parallel" (resulting path will have exactly parallel
// start and end points)
#define PATH_EPS_POSES_PARALLEL			0.001
// max ammount that points can be off from true symmetric to be considered
// "close enough" to symmetric, in radians
#define PATH_EPS_POSES_SYMMETRIC		0.002
// distance between intermediate poses to check when generating a
// a bielementary path, in meters
#define PATH_INTERMEDIATE_INTERVAL		1
// max distance between points for the points to be "close enough"
// together (ie, in determining whether the path succeeded in reaching
// the end)
// NOTE: I think that this needs to be so large right now b/c of inaccuracies
// with the integration to calculate the clothoid. *hopefully* a better
// integration method will allow me to decrease this
#define PATH_POINTS_CLOSE_ENOUGH		0.5
// Maxmimum length (in meters) that a searched path can be; this is to prevent massive
// looping paths that are 1km in length with a net displacement of 20m.
#define PATH_MAX_SEARCH_LENGTH			100
// maximum curvature for a path that alice can physically drive
#define PATH_MAX_DYN_FEASIBLE_CURVATURE		(1/VEHICLE_MIN_TURNING_RADIUS)
// distance overwhich we will zero the max steering angle so that we can
// use dave knowles analytic solution (less steering angle will be zeroed
// over proportionally less distance)
#define DISTANCE_TO_ZERO_CURVATURE		2



class CElementaryPath
{
   protected:
	/* instance of the environment class, which is a container for info
	   passed from tplanner, such as legal driving area, cost map, etc.
	   note that only the pointer is stored, so the environment class
	   instance can be updated outside of the Clothoid planner class and
	   those updates will show here */
	CEnvironment *m_aliceEnv;

	/* stores the points along the clothoid */
	vector<pose2> m_clothoidPoints;

	/* new parameters */
	double m_initCurv, m_finalCurv, m_length;

	/* cost from start to this point, and estimated cost 
	   from this point to finish (heuristic), respectively */
	double m_costToNode, m_costToFinish, m_density;

	/* the branches of the search tree (nodes that this node is a parent for) */
	vector<CElementaryPath *> m_searchChildren;

	/* pointer back to the parent node of this one, so that we can traverse the
	   list in reverse */
	CElementaryPath *m_searchParent;

	/* wether alice should drive this path in reverse */
	bool m_reverse;

	/* whether we've explored the node */
	int m_explored;

	/* whether the path intersects an obstacle */
	bool m_intersectsObstacle;

   public:
	/* whether the end of this node gets us to the finish location */
	bool m_atFinish;

	/* -------- Constuctors/ destructors -------- */

	/* save the instance of the environment class to use in determining whether
	   paths are valid */
	CElementaryPath(CEnvironment *env);
	/* basic constructor which just sets everything to null/ 0 or false */
	CElementaryPath();

	CElementaryPath(CEnvironment *env,double new_initCurv, double new_finalCurv,
		double new_length, bool new_reverse = false, pose2 startPose = pose2(0,0,0));

	/* destructor- recursively deletes all children */
	~CElementaryPath();

	/* --------- Initializers/ Low level math for clothoid calculation ---------- */

	/* erase the points in the clothoid */
	void clearClothoid();

	/* set the clothoid based on the passed in parameters */
	void setClothoid(double new_initCurv, double new_finalCurv, double new_length, 
		bool new_reverse = false, pose2 startPose = pose2(0,0,0));

	/* given the start pose, the clothoid sigma and length, fill in the rest of
	   rest of the clothoid (sigma and length must already be set in the instance 
	   of the class */
	void calculateClothoid(pose2 startPose = pose2(0,0,0));

	/* ---------------- Clothoid Creation: Analytic clothoids -------------------- */
	/* Dave knowles functions to analytically connect a pair or points with a clothoid */

	/* connect the end of the current clothoid to the given pose using either a
	   simple path (if the poses are symmetric) or a bielementary path (if not
	   symmetric). also checks it for dynamic and cooridor feasability. this is
	   the meat of the search function */
	bool connectToTarget(pose2 targetPose, bool in_reverse = false,
		CElementaryPath **finalClothoid = NULL);

	/* fill in the clothoidPoints with a bielementary clothoid between the
	   start and finish pose */
	CElementaryPath *generateBielementaryClothoid(pose2 startPose, 
		pose2 finishPose, bool in_reverse = false);

	/* fill in the clothoidPoints with a symmetric clothoid between the 
	   start and finish pose */
	CElementaryPath *generateSymmetricClothoid(pose2 startPose,
		pose2 finishPose, bool in_reverse = false);

	/* --------------------- Clothoid Creation: copy clothoids --------------------- */

	/* append a copy of clothoidToCopy to the end of this one, rotating
	   and translating it to fit */
	bool connectClothoidCopy(CElementaryPath *clothoidToCopy,
		bool in_reverse = false, CElementaryPath **new_clothoid = NULL);

	/* fill in the clothoid based on a precomputed clothoid, but also rotate
	   and it and translate it by the amount specified by translation, which
	   is a delta */
	CElementaryPath *copyClothoid(pose2 startPose,
		CElementaryPath *clothoidToCopy,
		bool in_reverse = false);

	/* --------------------- Accessors: retrieving information --------------------- */

	/* return the maximum curvature for a given path */
	double getMaxCurvature();

	/* return the last clothoid in a series of linked clothoids- this assumes that each
	   clothoid has only 1 child (its intended to be used on searched path, in which case
	   this is true) */
	CElementaryPath *getLastClothoid();

	/* recursively check clothoid and all children for legality. return legal
	   only if all are legal. otherwise return whether the clothoids are blocked or
	   go outside of the corridor (reasons for illegal paths are bitwise ORed together) */
	int areClothoidsLegal();

	/* determine if the path is legal based on the dynamic constraints of the
	   vehicle and the area that it drives in */
	int isClothoidLegal();

	/* return the first point in the clothoid */
	pose2 getFirstPoint(double *initCurv = NULL);

	/* return the last point in the clothoid */
	pose2 getLastPoint(double *finalCurv = NULL);

	/* return the estemated total path cost (cost to end of node + heuristic) */
	double getCost();

	/* return just the heuristic */
	double getHeuristic();

	/* return the curvature at a point at position along the clothoid
	   (position measured in meters from the beginning of the clothoid) */
	double getCurvature(double position);

	/* return the clothoid */
	vector<pose2> getClothoidPoints() { return m_clothoidPoints; }

	/* return the previous node in the search tree */
	CElementaryPath *getSearchParent() { return m_searchParent; }

	/* return a vector of pointers to the children of this clothoid */
	vector<CElementaryPath *> getSearchChildren() { return m_searchChildren; }

	/* function to print all the points in the current clothoid void */
	void printClothoid(ostream *f = &cout);

	/* recursively print the points in this clothoid and all children */
	void printAllClothoids(ostream *f = &cout);

	/* whether the path has been explored */
	bool isPathExplored() { return m_explored == PATH_EXPLORED; }

	/* whether the path is forward */
	bool isPathReverse() { return m_reverse; }

	/* ------------------ Accessors: setting information ------------------------ */

	/* append the specified path to the end of this one */
	void appendPath(CElementaryPath *path);

	/* add a point to the clothoid */
	void appendPoint(pose2 point);

	/* delete the link between the current node and specified child node
	   (it doesn't point to childToSever; childToSever doesn't point to it */
	void severLink(CElementaryPath *childToSever);

	/* delete all the points in the clothoid between indexes indL (inclusive) 
	   and indH (exclusive) */
	void deleteClothoidPoints(int indL = 0, int indH = -1);

	/* check whether the clothoid and its children hit an obstacle- this is to deal
	   with dynamic obstacles. it also deletes anything that hits an obstacle */
	bool checkObstacleIntersection();

	/* set the parent to be the specified path */
	void setParent(CElementaryPath *parent = NULL);

	void setCurvLength(double initCurv, double finalCurv, double l);

	void setInitCurv(double new_initCurv);

	void setFinalCurv(double new_finalCurv);

	/* delete the finish nodes; used when the finish has changed */
	vector<CElementaryPath *> setNodesAsNotAtGoal();

	/* set the cost to this node (argument is the cost to the end of the previous 
	   node, not the total cost (which is the cost to the end of the node 
	   + the heuristic ) */
	void setCost(double prevNodeCost = 0);

	/* set the path as explored */
	void setAsExplored() { m_explored = PATH_EXPLORED; }

	/* --------------------- Tree search ------------------------ */
	/* functions to perform the graph search on our clothoid tree- these determine
	   where is the next best place to look for a solution */

	/* find the node in the tree below the current node with the lowest total cost */
	CElementaryPath *findLowestCost(double pSkipNode = 0,
		vector<CElementaryPath *> *allNodes = NULL);
};



#endif		// ELEMENTARYPATH_HH

