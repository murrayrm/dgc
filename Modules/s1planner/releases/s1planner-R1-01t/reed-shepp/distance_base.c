/*----------------------------------------------------------------------
     
  Fichier              : /RIA/auto/idefix/users/taix/c/Trajectoire/Reed_Shepp/distance_base.c
  Ce fichier contient les fonctions de base servant a calculer les 
  t , u , v des 48 cas de R&S (voir fichier reed_shepp.c)
     
  Date de creation     : Jeudi 29 Mars 09:30:03 1990
  Date de modification : Jeudi 26 Avril 16:15:13 1990
  Nb de lignes         : 292 
     
  Auteur               : Michel Taix
     
----------------------------------------------------------------------*/
#include <stdio.h>
#include <math.h>
/* #include "structure.h" */
/* #include "declare_fct.h" */
/* #include "varglob.h" */
/* #include "constante.h" */

#define infini HUGE_VAL
#define EPS3 0.001         /* floating point tolerance */
#define radcurv		1.0

double mod2pi(theta)
double theta;
{
  /* return mod(theta + pi, 2pi) - pi, this is always in range [-pi,+pi) */
  /* return (theta + M_PI) - 2*M_PI*floor((theta+M_PI)/(2*M_PI)) - M_PI; */
  
  /* return mod(theta, 2pi), this is always in range [0,2pi) */
    double t;

    t = theta - 2*M_PI*floor(theta/(2*M_PI));
    if (fabs(t - 2*M_PI) < EPS3) return 0.0; else return t;
}

int fegalite(x,y,tol)
double x,y,tol;
{
return (fabs(x - y) < tol);
}

        /****  C | C | C ***/
double c_c_c(x , y , phi , t , u ,v)
     double x , y , phi , *t , *u , *v;
{
  double a , b , u1 , theta , alpha , long_rs , va;
  
  a = x - radcurv*sin(phi);
  b = y + radcurv*(cos(phi) -1);
  u1 = sqrt( a*a + b*b );
  if( u1 > 4*radcurv ) return( infini );
  else{
    theta = atan2( b , a);
    alpha = acos( u1/(4*radcurv) );
    va = M_PI/2 + alpha;
    *t = mod2pi( va + theta);
    *u = mod2pi( 2*(M_PI - va) );
    *v = mod2pi( phi - (*t) - (*u));
    long_rs = radcurv*( *t + *u + *v );
    return(long_rs);
  }
}
      /****  C | C C  *****/
double c_cc(x , y , phi , t , u ,v)
     double x , y , phi , *t , *u , *v;
{
  double a , b , u1 , theta , alpha , long_rs , va;
  
  a = x - radcurv*sin(phi);
  b = y + radcurv*(cos(phi) -1);
  u1 = sqrt( a*a + b*b );
  if( u1 > 4*radcurv ) return( infini );
  else{
    theta = atan2( b , a);
    alpha = acos( u1/(4*radcurv) );
    va = M_PI/2 + alpha;
    *t = mod2pi( va + theta);
    *u = mod2pi( 2*(M_PI - va) );
    *v = mod2pi( *t + *u - phi );
    long_rs = radcurv*( *t + *u + *v );
    return(long_rs);
  }
}
     /****  C S C  ****/
double csca(x , y , phi , t , u ,v)
     double x , y , phi , *t , *u , *v;
{
  double a , b , long_rs;

  a = x - radcurv*sin(phi);
  b = y + radcurv*(cos(phi) -1);
  *u = sqrt( a*a + b*b );
  *t = mod2pi(atan2( b , a));
  *v = mod2pi( phi - *t );
  long_rs = radcurv*( *t + *v) + *u;
  return(long_rs);
}
double cscb(x , y , phi , t , u ,v)
     double x , y , phi , *t , *u , *v;
{
  double a , b , u1 , theta , alpha , long_rs , va;
  
  a = x + radcurv*sin(phi);
  b = y - radcurv*(cos(phi) +1);
  u1 = sqrt( a*a + b*b );
  va = 2*radcurv;
  if( u1 < va ) return( infini );
  else{
    theta = atan2( b , a);
    *u = sqrt( u1*u1 - va*va );
    alpha = atan2( va , *u);
    *t = mod2pi( theta + alpha);
    *v = mod2pi( *t - phi);
    long_rs = radcurv*( *t + *v) + *u;
    return(long_rs);
  }
}

           /*** C Cu | Cu C  ***/

double ccu_cuc(x , y , phi , t , u ,v)
     double x , y , phi , *t , *u , *v;
{
  double a , b , u1 , theta , alpha , long_rs , va;
  
  a = x + radcurv*sin(phi);
  b = y - radcurv*(cos(phi) +1);
  u1 = sqrt( a*a + b*b );
  va = 4*radcurv;
  if( u1 > va) return( infini );
  else{
    theta = atan2( b , a);
    if( u1 > 2*radcurv ) {
      alpha = acos( (u1/2 - radcurv)/(2*radcurv) );
      *t = mod2pi( M_PI/2 + theta - alpha );
      *u = mod2pi( M_PI - alpha );
      *v = mod2pi( phi - *t + 2*(*u) );
    }
    else{
      alpha = acos( (radcurv + u1/2)/(2*radcurv) );
      *t = mod2pi( M_PI/2 + theta + alpha );
      *u = mod2pi( alpha);
      *v = mod2pi( phi - *t + 2*(*u) );
    }
    long_rs = radcurv*( 2**u + *t + *v );
    return(long_rs);
  }
}

/****************  C | Cu Cu | C  **************/
double c_cucu_c(x , y , phi , t , u ,v)
     double x , y , phi , *t , *u , *v;
{
  double a , b , u1 , theta , alpha , long_rs , va;
  double toto;

  a = x + radcurv*sin(phi);
  b = y - radcurv*(cos(phi) +1);
  u1 = sqrt( a*a + b*b );
  if( u1 > 6*radcurv )  return( infini );
  else{
    theta = atan2( b , a);
    va = (5*radcurv*radcurv - u1*u1/4)/(4*radcurv*radcurv);
    if( (va<0.)||(va>1.) ) return( infini );
    else{
      *u = acos( va );
      toto = sin(*u);
      if( fegalite(toto ,0. ,EPS3) ) toto = 0.;
      alpha = asin( radcurv*toto*2/u1 );
      *t = mod2pi(M_PI/2 + theta + alpha);
      *v = mod2pi( *t - phi);
      long_rs = radcurv*( 2**u + *t + *v );
      return(long_rs);
    }
  }
}

/****************  C | C2 S C ******************/
double c_c2sca(x , y , phi , t , u ,v)
     double x , y , phi , *t , *u , *v;
{
  double a , b , u1 , theta , alpha , long_rs , va;

  a = x - radcurv*sin(phi);
  b = y + radcurv*(cos(phi) -1);
  u1 = sqrt( a*a + b*b );
  va = 2*radcurv;
  if( u1 < va) return(infini );
  else{
    theta = atan2( b , a);
    *u = sqrt(u1*u1 - va*va) - va;
    if( *u < 0.) return(infini );
    else{
      alpha = atan2( va , (*u + va) );
      *t = mod2pi(M_PI/2 + theta + alpha);
      *v = mod2pi( *t +M_PI/2 - phi);
      long_rs = radcurv*( *t + M_PI/2 + *v) + *u;
      return(long_rs);
    }
  }
}
double c_c2scb(x , y , phi , t , u ,v)
     double x , y , phi , *t , *u , *v;
{
  double a , b , u1 , theta , alpha , long_rs , va;
  a = x + radcurv*sin(phi);
  b = y - radcurv*(cos(phi) +1);
  u1 = sqrt( a*a + b*b );
  va = 2*radcurv;
  if( u1 < va) return(infini );
  else{
    theta = atan2( b , a);
    *t = mod2pi(M_PI/2 + theta);
    *u = u1 - va;
    *v = mod2pi( phi - *t - M_PI/2);
    long_rs = radcurv*( *t + M_PI/2 + *v) + *u;
    return(long_rs);
  }
}

/****************  C | C2 S C2 | C  ***********/
double c_c2sc2_c(x , y , phi , t , u ,v)
     double x , y , phi , *t , *u , *v;
{
  double a , b , u1 , theta , alpha , long_rs , va;
  
  a = x + radcurv*sin(phi);
  b = y - radcurv*(cos(phi) +1);
  u1 = sqrt( a*a + b*b );
  va = 4*radcurv;
  if( u1 < va) return(infini );
  else{
    theta = atan2( b , a);
    *u = sqrt( u1*u1 - va*radcurv ) - va;
    if( *u < 0.) return(infini );
    else{
      alpha = atan2( 2*radcurv , (*u + va));
      *t = mod2pi(M_PI/2 + alpha + theta);
      *v = mod2pi(*t - phi);
      long_rs = radcurv*( *t + M_PI + *v) + *u;
      return(long_rs);
    }
  }
}

/****************  C C | C  ****************/
double cc_c(x , y , phi , t , u ,v)
     double x , y , phi , *t , *u , *v;
{
  double a , b , u1 , theta , alpha , long_rs , va;
  double toto;

  a = x - radcurv*sin(phi);
  b = y + radcurv*(cos(phi) -1);
  u1 = sqrt( a*a + b*b );
  va = 4*radcurv;
  if( u1 > va) return(infini );
  else{
    theta = atan2( b , a);
    *u = acos( (va*va/2 - u1*u1)/(va*va/2) );
    toto = sin(*u);
    if( fegalite(toto ,0. ,EPS3) ) toto = 0.;
    alpha = asin( (2*radcurv*toto)/(u1) );
    *t = mod2pi(M_PI/2 + theta -alpha);
    *v = mod2pi(*t - *u - phi);
    long_rs = radcurv*( *t + *u + *v);
    return(long_rs);
  }
}

/****************  C S C2 | C  ************/
double csc2_ca(x , y , phi , t , u ,v)
     double x , y , phi , *t , *u , *v;
{
  double a , b , u1 , theta , alpha , long_rs , va;
  
  a = x - radcurv*sin(phi);
  b = y + radcurv*(cos(phi) -1);
  u1 = sqrt( a*a + b*b );
  va = 2*radcurv;
  if( u1 < va ) return(infini );
  else{
    theta = atan2( b , a);
    *u = sqrt( u1*u1 - va*va ) - va;
    if( *u < 0.) return(infini );
    else{
      alpha = atan2( (*u + va), va );
      *t = mod2pi( M_PI/2 + theta - alpha);
      *v = mod2pi(*t - M_PI/2 - phi);
      long_rs = radcurv*( *t + M_PI/2 + *v) + *u;
      return(long_rs);
    }
  }
}
double csc2_cb(x , y , phi , t , u ,v)
     double x , y , phi , *t , *u , *v;
{
  double a , b , u1 , theta , alpha , long_rs , va;
  
  a = x +  radcurv*sin(phi);
  b = y - radcurv*(cos(phi) + 1);
  u1 = sqrt( a*a + b*b );
  va = 2*radcurv;
  if( u1 < va ) return(infini );
  else{
    theta = atan2( b , a);
    *u = u1 - va;
    *t = mod2pi(theta);
    *v = mod2pi( phi - M_PI/2 - *t);
    long_rs = radcurv*( *t + M_PI/2 + *v) + *u;
    return(long_rs);
  }
}



