/* header file for reed shepp metric calculation.
   code originally implemented by Richard Murray et al.
   header file written by Kenny Oslund for DUC 2007
   
   Oct 3 07
*/


#ifndef REED_SHEPP_H
#define REED_SHEPP_H


#include <alice/AliceConstants.h>

double reed_shepp(double *c1 , double *c2  , int *numero , 
	double *t_r , double *u_r , double *v_r);





#endif
