/*
 * Environment.cc
 * Container for information about the enviroment Alice is working in.
 * This is pretty much just a wrapper for OCPspecs- ideally, everything
 * would be implemented in OCPspecs (which this class extends), and this
 * doesn't have any members explicitly in it. However, somethings we may
 * not want to put in OCPspecs, or we may want to override what is in
 * OCPspeacs for testing purposes. In those cases, methods can be put in
 * put in here. However, methods in here should be kept general and
 * independent of any structures unique to a specific planner.
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 1 May 07- Kenny Oslund
	- Created file
 *
 */

#include "Environment.hh"


/* normal constructor */
CEnvironment::CEnvironment(CSpecs_t *problem)
{
	m_cspecs = problem;
	return;
}

/* return the average of the lower and upperbounds for the
   start and end points. if pointers to lowerbound and upper
   bound pose2 structs are provided, those objects will be 
   filled in appropiately */
pose2 CEnvironment::getStartPoint()
{
	vector<double> conds = m_cspecs->getStartingState();
	pose2 start(conds.at(STATE_IDX_X), conds.at(STATE_IDX_Y), conds.at(STATE_IDX_THETA));
	return start;
}


pose2 CEnvironment::getEndPoint()
{
	vector<double> conds = m_cspecs->getFinalState();
	pose2 goal(conds.at(STATE_IDX_X), conds.at(STATE_IDX_Y), conds.at(STATE_IDX_THETA));
	return goal;
}


// a pointer to a point2arr which contains the surrounding polygon can optionally 
// be passed in for speed, or it can be generated here.
bool CEnvironment::isPointLegal(pose2 point)
{
	point2 pt(point.x, point.y);

	point2arr boundary = m_cspecs->getBoundingPolygon();
	return boundary.is_inside_poly(pt);
}

/* return whether all the points in the specified path are within
	the legal driving region; should use the function isPointLegal.
	also note that it takes a pointer to a vector of points, not
	an actual vector (this is done for speed- passing a pointer is
	faster), so it needs to be sure NOT to modify the points */
bool CEnvironment::isPathLegal(vector<pose2> *path)
{
	// if even 1 point is illegal, the whole path is illegal
	for (vector<pose2>::iterator i = path->begin(); i != path->end(); i++)
		if ( !isPointLegal(*i) )
			return false;
	return true;

}

/* get the cost a the given point */
double CEnvironment::getPointCost(pose2 point)
{
	return 1;
//	return getCostMapValue(point.x, point.y);
}


/* sum up the cost along all the points in the path and return
	that. should use the getPointCost function and should NOT 
	modify the points (it takes a pointer like isPathLegal) */
double CEnvironment::getPathCost(vector<pose2> *path)
{
	double totalCost = 0;

	for (vector<pose2>::iterator i = path->begin(); i != path->end(); i++)
		totalCost += getPointCost(*i);

	return totalCost;
}

/* return the cost heuristic, the estimated cost from point to
	the finish */
double CEnvironment::getHeuristicToGoal(pose2 point)
{
	#define HEURISTIC_GAUSSIAN_WIDTH		2
	#define HEURISTIC_GAUSSIAN_HEIGHT		20
	// There are circular areas around the goal which we don't want to be in-
	// because of the vehicle dynamics, once we're in those areas, we
	// can't reach the goal without going out of them (and therefore 
	// getting further from the goal) or reversing a lot. so, set the 
	// heuristic to vew very high in those regions
	pose2 center1, center2, goal = getEndPoint();
	pose2 trans(VEHICLE_MIN_TURNING_RADIUS * sin(goal.ang), 
		-1 * VEHICLE_MIN_TURNING_RADIUS * cos(goal.ang), 0);
	center1 = goal + trans;
	center2 = goal - trans;
	// how far are we from the edge of our cirular area?
	double d1 = (point - center1).magnitude() - VEHICLE_MIN_TURNING_RADIUS;
	double d2 = (point - center2).magnitude() - VEHICLE_MIN_TURNING_RADIUS;
	double close_to_goal_cost;
	if (d1 < 0 || d2 < 2)		// inside of one of these circular areas;
		close_to_goal_cost = HEURISTIC_GAUSSIAN_HEIGHT;
	else
		close_to_goal_cost =
			  HEURISTIC_GAUSSIAN_HEIGHT * exp( -1 * (d1 * d1) / 
			    ( HEURISTIC_GAUSSIAN_WIDTH * HEURISTIC_GAUSSIAN_WIDTH) )
			+ HEURISTIC_GAUSSIAN_HEIGHT * exp( -1 * (d2 * d2) / 
			    ( HEURISTIC_GAUSSIAN_WIDTH * HEURISTIC_GAUSSIAN_WIDTH) );

//	cout << "Close to goal heuristic: " << close_to_goal_cost << " at: ";
//		point.display();
	return ENVIRONMENT_GOAL_HEURISTIC_SCALE_FACTOR * 
		getHeuristic(point, getEndPoint()) + close_to_goal_cost;

}

/* return the heuristic between 2 arbitrary points */
double CEnvironment::getHeuristic(pose2 point1, pose2 point2)
{
	// also penalize for having theta be off...
//	return 16 * fabs((point2 - point1).ang);	// uturn
	return 1.5 * (2 * fabs((point2 - point1).ang) + (point2 - point1).magnitude());	// parking/ other zone
}

// return the average of the upper and lower bounds on velocity
// if ub and/or lb are not null, then they will be populated with the
// appropriate value
double CEnvironment::getInitialVelocity()
{
	vector<double> conds = m_cspecs->getStartingState();
	double v = conds.at(STATE_IDX_V);
	return v;
}

double CEnvironment::getGoalVelocity()
{
	vector<double> conds = m_cspecs->getFinalState();
	double v = conds.at(STATE_IDX_V);
	return v;
}

S1PlanMode CEnvironment::getPlanMode()
{
	return reverse_allowed;
}




