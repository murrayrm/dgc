/*
 *  Stage1Planner.hh
 *  Stage1Planner
 *
 *  Created by Noel duToit
 *  2/28/07
 */

#ifndef STAGE1PLANNER_HH
#define STAGE1PLANNER_HH

#include <time.h>
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>
#include <iomanip>

#include "frames/coords.hh"
#include "dgcutils/RDDF.hh"
#include "skynettalker/StateClient.hh"
#include "trajutils/TrajTalker.hh"
#include "skynettalker/SkynetTalker.hh"
#include "skynettalker/RDDFTalker.hh"
#include "trajutils/CPath.hh"
#include "cmap/CMapPlus.hh"
#include "cmap/MapdeltaTalker.hh"
#include "dgcutils/DGCutils.hh"
#include "skynet/sn_msg.hh"
#include "interfaces/sn_types.h"
#include "ocpspecs/OCPtSpecs.hh"
#include "interfaces/TpDpInterface.hh"
#include "pseudocon/interface_superCon_trajF.hh"
#include "Environment.hh"

using namespace sc_interface;
using namespace std;

#define DEBUG


#define S1_PLAN_MODE_LIST(_) \
  _( no_reverse, = 0 ) \
  _( reverse_allowed, ) \
  _( reverse_required, )
DEFINE_ENUM(S1PlanMode, S1_PLAN_MODE_LIST)

/* useful function */
inline double pointAngle(NEcoord d) {return atan2(d.E,d.N); };


class CStage1Planner:public CStateClient, public CTrajTalker, public CRDDFTalker
{
    public:
    	CStage1Planner(int SkynetKey, bool WAIT_STATE);
	~CStage1Planner(void);

	/* main loop */
	void ActiveLoop(void);


    private:		

	/* variables for sending data over skynet */
	int m_TrajFollowSendSocket;
	int m_SuperConSendSocket;

    protected:
	  /* class that contains the trajectory and 2 time derivatives in northing 
	     and easting coordinates. to add points to it, use the command
	     m_Traj->addPoint(N, Nd, Ndd, E, Ed, Edd)
	     where N is northing, E is easting, and d denotes a derivative */
	  CTraj*    m_Traj;
	  /* direction the vehicle needs to travel at the current time- must set
	     scCmd.commandType to either tf_forwards or tf_reverse */
	  struct superConTrajFcmd scCmd;

	/* the following variables, classes and methods are setup for you and 
	   may be used by the extending class */

        CEnvironment* m_pProblemSpecs;
	double runtime;
	S1PlanMode PlanMode;

	/* methods which access/ relate to the corridore and RDDFs- 
	   all references to RDDF's should be encapsulated in these methods so that
	   we can easily switch to a different representation */
	bool isPointInCorridor(NEcoord point, double bonus_ratio = 1, double bonus_const = 0);
};

#endif /* STAGE1PLANNER_HH */
