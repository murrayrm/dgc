/*
 * Environment.hh
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 1 May 07- Kenny Oslund
	- Created file
 */


#ifndef ENVIRONMENT_HH
#define ENVIRONMENT_HH

#include "frames/pose2.hh"
#include "frames/point2.hh"
#include "s1planner/ClothoidUtils.hh"
#include <bitmap/Polygon.hh>
#include <bitmap/BitmapParams.hh>
#include <alice/AliceConstants.h>
#include <cspecs/CSpecs.hh>
#include "temp-planner-interfaces/PlannerInterfaces.h"
#include <temp-planner-interfaces/Console.hh>
#include <temp-planner-interfaces/Log.hh>



// somewhere within skynetTalker or its includes is the definition of 
// DEFINE_ENUM, which is used bellow. that is the only reason this is here.
#include <skynettalker/SkynetTalker.hh>

// NOTE: Northing corresponds to x, and Easting corresponds to y

// distance between points in the traj
#define PATH_SECTION_LENGTH                     0.2


// weight for the heuristic. I think that since both the heuristic and the cost 
//   increase with length
// higher heuristic weight => cost for traversed parts is lower in comparison =>
//   we will preferentially explore longer trajectories
// lower heuristic weight => cost for traversed parts is higher in comparison =>
//   we will preferentiallyh explore/ refine shorter traj's
// this is untested



// whether or not we can plan in forward only, forward and reverse, or reverse only
#define S1_PLAN_MODE_LIST(_) \
  _( no_reverse, = 0 ) \
  _( reverse_allowed, ) \
  _( reverse_required, )
DEFINE_ENUM(S1PlanMode, S1_PLAN_MODE_LIST)



class CEnvironment 
{
   private:
	CSpecs_t *m_cspecs;
	// the corridor grown so that alice is legal
	point2arr m_grownCorridor;
	// a ploygon which represents alice plus a safety zone around her
	point2arr m_aliceSafety;
	// distance between diagonal points; used in the point legality check
	double m_aliceDiagonalDist;

   public:

	/* Constuctor */
	CEnvironment(CSpecs_t *problem);

	/* normal constructor - BE SURE TO CALL update WITH A POINTER TO A CSPECS BEFORE 
	YOU TRY TRY TO USE THIS IF YOU CALL THIS CONSTRUCTOR*/
	CEnvironment();

	// populate CEnvironment variables from cspecs variables to update CEnvironment
	void update(CSpecs_t *problem = NULL);

	// populate the corrodor in CEnvironement based on the corridor in cspecs. this will 
	// grow the corridor so that the initial and final poses are legal.
	void updateCorridor();

	// set a polygon representing alice and the specified safety region around her
	void updateAliceSafety();

	// assuming that alice is at aliceLocation and that at least some of her is sticking
	// into the corridor, grow the corridor by unioning an area around 
	// with the existing corridor. This is to ensure that alice's location is legal
	void growCorridor(pose2 aliceLocation);

	// return a rectangle which represents alice + the safety region at the origin 
	// with 0 radians heading
	point2arr getAliceSafety();
	// return a rectangle which represents alice + the safety region rotated and 
	// translated to location
	point2arr getAliceSafety(pose2 location);

	/* accessor for the start and end points */
	/* return the average of the lower and upperbounds for the
	   start and end points. if pointers to lowerbound and upper
	   bound pose2 structs are provided, those objects will be 
	   filled in appropiately */
	pose2 getStartPoint();
	pose2 getEndPoint();

	/* return whether the specified point is in the legal driving area. */
	bool isPointLegal(pose2 point);


	/* return whether all the points in the specified path are within
	   the legal driving region; should use the function isPointLegal.
	   also note that it takes a pointer to a vector of points, not
	   an actual vector (this is done for speed- passing a pointer is
	   faster), so it needs to be sure NOT to modify the points */
	bool isPathLegal(vector<pose2> *path);

	/* get the cost a the given point */
	double getPointCost(pose2 point);

	/* sum up the cost along all the points in the path and return
	   that. should use the getPointCost function and should NOT 
	   modify the points (it takes a pointer like isPathLegal) */
	double getPathCost(vector<pose2> *path);

	/* return the cost heuristic, the estimated cost from point to
	   the finish */
	double getHeuristicToGoal(pose2 start);

	/* return the heuristic between arbitrary points */
	double getHeuristic(pose2 start, pose2 goal);

	double getInitialVelocity();

	double getGoalVelocity();

	/* whether to plan in forward or reverse or both */
	S1PlanMode getPlanMode();


};

#endif



