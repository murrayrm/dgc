/* header file for reed shepp metric calculation.
   code originally implemented by Richard Murray et al.
   header file written by Kenny Oslund for DUC 2007
   
   Oct 3 07
*/


#ifndef REED_SHEPP_H
#define REED_SHEPP_H



#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


double reed_shepp(double *c1 , double *c2  , int *numero , int *num_reverses ,
	int *first_dir , double *t_r , double *u_r , double *v_r);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* REED_SHEPP_H */
