/*
 * Environment.cc
 * Container for information about the enviroment Alice is working in.
 * This particular version has exactly the same functions, with the 
 * exception of the constructor (and therefore
 * same header file) as the regular one, but ti replaces all the return 
 * values with fudged data for testing
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 1 May 07- Kenny Oslund
	- Created file
 *
 */

#include "Environment.hh"


/* normal constructor */
CEnvironment::CEnvironment(pose2 start, pose2 goal, pose2 ll, pose2 ur, S1PlanMode pmode)
{
	initCond = start;
	finalCond = goal;
	lowleft = ll;
	upright = ur;
	mode = pmode;
}



/* return the average of the lower and upperbounds for the
   start and end points. if pointers to lowerbound and upper
   bound pose2 structs are provided, those objects will be 
   filled in appropiately */
pose2 CEnvironment::getStartPoint()
{
	return initCond;
}


// Note: upper and lower bounds are NOT adjusted if the end point is outside 
// of the corridor
pose2 CEnvironment::getEndPoint()
{
	return finalCond;
}


bool CEnvironment::isPointLegal(pose2 point, point2arr *boundArr)
{
	return ( point.x >= lowleft.x && point.x <= upright.x
	  && point.y >= lowleft.y && point.y <= upright.y );
}


/* return whether all the points in the specified path are within
	the legal driving region; should use the function isPointLegal.
	also note that it takes a pointer to a vector of points, not
	an actual vector (this is done for speed- passing a pointer is
	faster), so it needs to be sure NOT to modify the points */
bool CEnvironment::isPathLegal(vector<pose2> *path)
{
	// if even 1 point is illegal, the whole path is illegal
	for (vector<pose2>::iterator i = path->begin(); i != path->end(); i++)
		if (!isPointLegal(*i))
			return false;
	return true;

}

/* get the cost a the given point */
double CEnvironment::getPointCost(pose2 point)
{
	#define COST_PARKING_AREA	30
/*	if (point.y < 7)
		return COST_PARKING_AREA;
	else if ( point.y < 18 )
		return COST_PARKING_AREA - ((point.y - 7) * (COST_PARKING_AREA / 15) );
	else*/
	return 1;
}


/* sum up the cost along all the points in the path and return
	that. should use the getPointCost function and should NOT 
	modify the points (it takes a pointer like isPathLegal) */
double CEnvironment::getPathCost(vector<pose2> *path)
{
	double totalCost = 0;

	for (vector<pose2>::iterator i = path->begin(); i != path->end(); i++)
		totalCost += getPointCost(*i);

	return totalCost;
}

/* return the cost heuristic, the estimated cost from point to
	the finish */
double CEnvironment::getHeuristicToGoal(pose2 point)
{
	#define HEURISTIC_GAUSSIAN_WIDTH		2
	#define HEURISTIC_GAUSSIAN_HEIGHT		20
	// There are circular areas around the goal which we don't want to be in-
	// because of the vehicle dynamics, once we're in those areas, we
	// can't reach the goal without going out of them (and therefore 
	// getting further from the goal) or reversing a lot. so, set the 
	// heuristic to vew very high in those regions
	pose2 center1, center2, goal = getEndPoint();
	pose2 trans(VEHICLE_MIN_TURNING_RADIUS * sin(goal.ang), 
		-1 * VEHICLE_MIN_TURNING_RADIUS * cos(goal.ang), 0);
	center1 = goal + trans;
	center2 = goal - trans;
	// how far are we from the edge of our cirular area?
	double d1 = (point - center1).magnitude() - VEHICLE_MIN_TURNING_RADIUS;
	double d2 = (point - center2).magnitude() - VEHICLE_MIN_TURNING_RADIUS;
	double close_to_goal_cost;
	if (d1 < 0 || d2 < 2)		// inside of one of these circular areas;
		close_to_goal_cost = HEURISTIC_GAUSSIAN_HEIGHT;
	else
		close_to_goal_cost =
			  HEURISTIC_GAUSSIAN_HEIGHT * exp( -1 * (d1 * d1) / 
			    ( HEURISTIC_GAUSSIAN_WIDTH * HEURISTIC_GAUSSIAN_WIDTH) )
			+ HEURISTIC_GAUSSIAN_HEIGHT * exp( -1 * (d2 * d2) / 
			    ( HEURISTIC_GAUSSIAN_WIDTH * HEURISTIC_GAUSSIAN_WIDTH) );

//	cout << "Close to goal heuristic: " << close_to_goal_cost << " at: ";
//		point.display();
	return ENVIRONMENT_GOAL_HEURISTIC_SCALE_FACTOR * 
		getHeuristic(point, getEndPoint()) + close_to_goal_cost;

}

/* return the heuristic between 2 arbitrary points */
double CEnvironment::getHeuristic(pose2 point1, pose2 point2)
{
	// also penalize for having theta be off...
//	return 16 * fabs((point2 - point1).ang);	// uturn

	return 1.5 * (2 * fabs((point2 - point1).ang) + (point2 - point1).magnitude());	// parking

}

// return the average of the upper and lower bounds on velocity
// if ub and/or lb are not null, then they will be populated with the
// appropriate value
double CEnvironment::getInitialVelocity()
{
	return 0;
}

double CEnvironment::getGoalVelocity()
{
	return 5;
}

S1PlanMode CEnvironment::getPlanMode()
{
	return mode;
}




