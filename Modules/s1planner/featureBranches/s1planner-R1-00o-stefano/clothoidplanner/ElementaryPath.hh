/*
 * ElementaryPath.hh
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 1 May 07- Kenny Oslund
	- Created file
 */

#ifndef ELEMENTARYPATH_HH
#define ELEMENTARYPATH_HH

#include <vector.h>
#include "frames/pose2.hh"
#include "trajutils/CPath.hh"
#include "../Environment.hh"
#include "ClothoidUtils.hh"

#define SEARCH_STATUS_LIST(_) \
  _( search_failed, = 0 ) \
  _( search_simple_path, ) \
  _( search_biel_path, )
DEFINE_ENUM(SearchStatus, SEARCH_STATUS_LIST)

// define PATH_CHECK_CORRIDOR to have s1planner check wether a path is in the
// legal driving zone. undefine it to assume all paths are in the corridor.
#define PATH_CHECK_CORRIDOR


// the path end point has not been explored
#define PATH_UNEXPLORED				-1
// the path end point is further away from the start than the goal,
// so we're not going to explore it unless the goal moves
#define PATH_TOO_DISTANT_TO_EXPLORE			100

// distance between points in traj, in meters
#define PATH_SECTION_LENGTH			0.2
// max difference in angle (radians) between 2 poses for them to 
// be cosidered "parallel" (resulting path will have exactly parallel
// start and end points)
#define PATH_EPS_POSES_PARALLEL			0.01
// max ammount that points can be off from true symmetric to be considered
// "close enough" to symmetric, in radians
#define PATH_EPS_POSES_SYMMETRIC		0.02
// distance between intermediate poses to check when generating a
// a bielementary path, in meters
#define PATH_INTERMEDIATE_INTERVAL		1
// max distance between points for the points to be "close enough"
// together (ie, in determining whether the path succeeded in reaching
// the end)
// NOTE: I think that this needs to be so large right now b/c of inaccuracies
// with the integration to calculate the clothoid. *hopefully* a better
// integration method will allow me to decrease this
#define PATH_POINTS_CLOSE_ENOUGH		0.5
// Maxmimum length (in meters) that a searched path can be; this is to prevent massive
// looping paths that are 1km in length with a net displacement of 20m.
#define PATH_MAX_SEARCH_LENGTH			100
// maximum curvature for a path that alice can physically drive
// NOTE: this is a pretty wild guess right now- should be refined
#define PATH_MAX_DYN_FEASIBLE_CURVATURE		0.2
/*struct CPoint : public pose2
{
	double curvature; 
	CPoint() {x=y=0.0; ang = 0.0; curvature = 0.0;}
	CPoint(pose2 pt) { x = pt.x; y = pt.y; ang = pt.ang; curvature = 0; }
};
*/

class CElementaryPath
{
   protected:
	/* instance of the environment class, which is a container for info
	   passed from tplanner, such as legal driving area, cost map, etc.
	   note that only the pointer is stored, so the environment class
	   instance can be updated outside of the Clothoid planner class and
	   those updates will show here */
	CEnvironment *m_aliceEnv;

	/* stores the points along the clothoid */
	vector<pose2> m_clothoidPoints;

	/* parameters to describe the clothoid */
	double sigma, length;

	/* cost from start to this point, and estimated cost 
	   from this point to finish (heuristic), respectively */
	double costToNode, costToFinish;

	/* the branches of the search tree (nodes that this node is a parent for) */
	vector<CElementaryPath *> m_searchChildren;

	/* pointer back to the parent node of this one, so that we can traverse the
	   list in reverse */
	CElementaryPath *m_searchParent;

   public:
	/* whether the end of this node gets us to the finish location */
	bool atFinish;

	/* wether alice should drive this path in reverse */
	bool reverse;

	/* whether we've explored the node */
	int explored_resolution;


	/* -------- Constuctors -------- */
	/* save the instance of the environment class to use in determining whether paths are valid */
	CElementaryPath(CEnvironment *env);
	/* basic constructor which just sets everything to null/ 0 or false */
	CElementaryPath();
	/* create a path and populate the clothoid based on the given parameters */
	CElementaryPath(CEnvironment *env, double new_sigma, double new_length, 
		bool new_reverse = false, pose2 startPose = pose2(0,0,0));

	/* destructor- recursively deletes all children */
	~CElementaryPath();

	/* given the start pose, the clothoid sigma and length, fill in the rest of
	   rest of the clothoid (sigma and length must already be set in the instance 
	   of the class */
	void calculateClothoid(pose2 startPose = pose2(0,0,0));

	/* fill in the clothoid based on a precomputed clothoid, but also rotate
	   and it and translate it by the amount specified by translation, which
	   is a delta */
	CElementaryPath *copyClothoid(pose2 startPose,
		CElementaryPath *clothoidToCopy,
		bool in_reverse = false);

	/* append a copy of clothoidToCopy to the end of this one, rotating
	   and translating it to fit */
	bool connectClothoidCopy(CElementaryPath *clothoidToCopy,
		bool in_reverse = false);

	/* connect the end of the current clothoid to the given pose using either a
	   simple path (if the poses are symmetric) or a bielementary path (if not
	   symmetric). also checks it for dynamic and cooridor feasability. this is
	   the meat of the search function */
	SearchStatus connectToTarget(pose2 targetPose, bool in_reverse = false);

	/* fill in the clothoidPoints with a symmetric clothoid between the 
	   start and finish pose */
	CElementaryPath *generateSymmetricClothoid(pose2 startPose,
		pose2 finishPose, bool in_reverse = false);

	/* fill in the clothoidPoints with a bielementary clothoid between the
	   start and finish pose */
	CElementaryPath *generateBielementaryClothoid(pose2 startPose, 
		pose2 finishPose, bool in_reverse = false);

	/* find the value of sigma and length for 2 symmetric paths */
	void findSigmaL(pose2 startPose, pose2 finishPose);

	/* set the cost to this node (argument is the cost to the end of the previous 
	   node, not the total cost (which is the cost to the end of the node 
	   + the heuristic ) */
	void setCost(double prevNodeCost);

	/* return the estemated total path cost (cost to end of node + heuristic) */
	double getCost();

	/* find the node in the tree below the current node with the lowest total cost */
	CElementaryPath *findLowestCost(int resolution);

	/* return the maximum curvature for a given path */
	double getMaxCurvature();

	/* return the first point in the clothoid */
	pose2 getFirstPoint() { return m_clothoidPoints.front(); }

	/* return the last point in the clothoid */
	pose2 getLastPoint() { return m_clothoidPoints.back(); }

	/* return the clothoid */
	vector<pose2> getClothoidPoints() { return m_clothoidPoints; }

	/* return the previous node in the search tree */
	CElementaryPath *getSearchParent() { return m_searchParent; }

	/* return a vector of pointers to the children of this clothoid */
	vector<CElementaryPath *> getSearchChildren() { return m_searchChildren; }

	/* append the specified path to the end of this one */
	void appendPath(CElementaryPath *path);

	/* add a point to the clothoid */
	void appendPoint(pose2 point);

	/* delete the link between the current node and its parent (it no longer
	   points to parent, parent no longer points to it) */
	void severLink(CElementaryPath *childToSever);

	/* set the parent to be the specified path */
	void setParent(CElementaryPath *parent = NULL);

	/* erase the points in the clothoid */
	void clearClothoid();

	/* delete all the points in the clothoid between indexes indL and 
	   indH (inclusive) */
	void deleteClothoidPoints(int indL = 0, int indH = -1);

	/* delete the finish nodes; used when the finish has changed */
	vector<CElementaryPath *> setNodesAsNotAtGoal();

	/* determine if the path is legal based on the dynamic constraints of the
	   vehicle and the area that it drives in */
	bool isClothoidLegal();

	/* function to print all the points in the current clothoid void */
	void printClothoid(ostream *f = &cout);

	/* recursively print the points in this clothoid and all children */
	void printAllClothoids(ostream *f = &cout);
};



#endif		// ELEMENTARYPATH_HH

