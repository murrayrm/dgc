/*
 * Environment.hh
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 1 May 07- Kenny Oslund
	- Created file
 */


#ifndef ENVIRONMENT_HH
#define ENVIRONMENT_HH

#include "frames/pose2.hh"
#include "ocpspecs/OCPtSpecs.hh"
#include "interfaces/TpDpInterface.hh"
#include "clothoidplanner/ClothoidUtils.hh"

// NOTE: Northing corresponds to x, and Easting corresponds to y

#define ENVIRONMENT_HEURISTIC_SCALE_FACTOR		1.0



class CEnvironment : public OCPtSpecs
{
   private:



   public:
	/* Constuctor */
	CEnvironment(int SkynetKey);

	/* accessor for the start and end points */
	/* return the average of the lower and upperbounds for the
	   start and end points. if pointers to lowerbound and upper
	   bound pose2 structs are provided, those objects will be 
	   filled in appropiately */
	pose2 getStartPoint(pose2 *lowerBound = NULL, pose2 *upperBound = NULL);
	pose2 getEndPoint(pose2 *lowerBound = NULL, pose2 *upperBound = NULL);

	/* return whether the specified point is in the legal driving area.
	   this should take into account alice's size and orientation */
	bool isPointLegal(pose2 point);

	/* return whether all the points in the specified path are within
	   the legal driving region; should use the function isPointLegal.
	   also note that it takes a pointer to a vector of points, not
	   an actual vector (this is done for speed- passing a pointer is
	   faster), so it needs to be sure NOT to modify the points */
	bool isPathLegal(vector<pose2> *path);

	/* get the cost a the given point */
	double getPointCost(pose2 point);

	/* sum up the cost along all the points in the path and return
	   that. should use the getPointCost function and should NOT 
	   modify the points (it takes a pointer like isPathLegal) */
	double getPathCost(vector<pose2> *path);

	/* return the cost heuristic, the estimated cost from point to
	   the finish */
	double getHeuristicToGoal(pose2 point);

	/* return the heuristic between arbitrary points */
	double getHeuristic(pose2 point1, pose2 point2);

	double getInitialVelocity(double *lb = NULL, double *ub = NULL);

	double getGoalVelocity(double *lb = NULL, double *ub = NULL);



};

#endif



