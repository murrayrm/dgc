(* uses functions in rs.m *)

PHISTEP = 2.0

RSproj/: RSproj[x_,y_] :=
Block[{dists = {}, phi},
  Do[dists = Append[dists,RSdist[x,y,phi]], {phi, -Pi, Pi, PHISTEP}];
  Min[dists]]

phiSlice/: phiSlice[phi_,size_,title_] :=
Block[{x,y},
  ContourPlot[RSdist[x,y,phi],
    {x,-size,size},{y,-size,size},
    (* PlotLabel->title, *)
    PlotPoints->50,
    AxesLabel->{x,y},
    PlotRange->{1.0,5.0}, 
    ContourLevels->9
  ]
]

p1/: p1[] := phiSlice[0,5.5,"Slice of Reeds-Shepp Ball at Phi = 0"]

p2/: p2[] := phiSlice[Pi/2,5.5,"Slice of Reeds-Shepp Ball at Phi = Pi/2"]

(* notes

The ContourPlot function seems to simply skip curves if the resolution
is not high enough for an accurate contour.  The function phiSlice above
has a z-value range of 1.0 to 5.0 and 9 contour levels.  For p1, the
plot at phi=0, we get the expected result.  Namely, 9 curves at values
1.0, 1.5, ..., 4.5, 5.0.  For p2, the plot at phi=pi/2, we only get 7
curves; the level curves for 1.0 & 1.5 are omitted.  The curves that
are shown are at the proper values, however (2.0, 2.5, ..., 5.0).

*)