close all;
clear all;



figure;
dlmread ctree_25.dat;
clothoids = ans;
plot(clothoids(:,1), -1 *clothoids(:,2), '.','MarkerSize',1);
xlabel('X (meters)');
ylabel('Y (meters)');
title('Clothoid Tree');
axis equal

return

figure;
dlmread tmp_clothoids.dat;
clothoids = ans;
plot(clothoids(:,1), -1*clothoids(:,2), '.','MarkerSize',1);
xlabel('X (meters)');
ylabel('Y (meters)');
title('Clothoid Tree');
axis equal

figure;
dlmread best_clothoids.dat;
clothoids = ans;
plot(clothoids(:,1), -1*clothoids(:,2), '.');
xlabel('X (meters)');
ylabel('Y (meters)');
title('Clothoid Chosen based on minimizing cost function');
axis equal
