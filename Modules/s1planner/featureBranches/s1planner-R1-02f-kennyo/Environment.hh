/*
 * Environment.hh
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 1 May 07- Kenny Oslund
	- Created file
 */


#ifndef ENVIRONMENT_HH
#define ENVIRONMENT_HH

#include "frames/pose2.hh"
#include "frames/point2.hh"
#include "s1planner/ClothoidUtils.hh"

#include <bitmap/Polygon.hh>
#include <bitmap/BitmapParams.hh>
#include <alice/AliceConstants.h>
#include <cspecs/CSpecs.hh>
#include "temp-planner-interfaces/PlannerInterfaces.h"
#include <temp-planner-interfaces/Console.hh>
#include <temp-planner-interfaces/Log.hh>

// NOTE: Northing corresponds to x, and Easting corresponds to y

// somewhere within skynetTalker or its includes is the definition of 
// DEFINE_ENUM, which is used bellow. that is the only reason this is here.
#include <skynettalker/SkynetTalker.hh>
// whether or not we can plan in forward only, forward and reverse, or reverse only
#define S1_PLAN_MODE_LIST(_) \
  _( no_reverse, = 0 ) \
  _( reverse_allowed, ) \
  _( reverse_required, )
DEFINE_ENUM(S1PlanMode, S1_PLAN_MODE_LIST)

// distance between points in the traj
#define PATH_SECTION_LENGTH                     0.2
// there is no reason to run the corridor check on every single point in the
// path since they are so close together. instead, just run the check every 
// LEGALITY_CHECK_INCREMENT points (currently set to be half the vehicle length)
#define LEGALITY_CHECK_INCREMENT		(int)floor( 0.5 * VEHICLE_LENGTH / PATH_SECTION_LENGTH )

// types of safety margins
#define SAFETY_PERIMETER			1
#define SAFETY_OBSTACLE				2

// define this to overide accessing the costmap for cost (for running UT_clothoidplanner- this should
// NOT be defined for actual runs with the DUC planner)
// #define ENVIRNMENT_TEST


/***** Heuristic/ cost constants *****/
// the scale factor to increase the cost of a node by if we have to change the
// direction of motion (forward to reverse or vis versa)
#define PATH_COST_CHANGE_DIRECTION              15
// an added cost for going backwards
#define PATH_COST_REVERSE                       1.0
// scale factor for RS heurisitic
#define HEURISTIC_RS_SCALE_FACTOR		3
// project the goal backwards when calculating the heuristic- this is because 
// reed-shepp assumes you can turn the wheel infinitely fast, while we can't
// => we can't actually follow a reed shepp path (we need more room)
#define HEURISTIC_REED_SHEPP_PROJECT_DIST       4
// define this to use Reed-Shepp heurisitic
#define HEURISTIC_USE_RS



class CEnvironment 
{
   private:
	CSpecs_t *m_cspecs;
	// the corridor grown so that alice is legal
	point2arr m_grownCorridor;
	// a ploygon which represents alice plus a safety zone around her (used
	// corridor check)
	point2arr m_perimeterSafety;
	// a ploygon which represents alice plus a safety zone around her (used
	// obstacle check)
	point2arr m_obstacleSafety;
	// distance between diagonal points; used in the corridor legality check
	double m_aliceDiagonalDist;

   public:

	/* Constuctor */
	CEnvironment(CSpecs_t *problem);

	/* normal constructor - BE SURE TO CALL update WITH A POINTER TO A CSPECS BEFORE 
	YOU TRY TRY TO USE THIS IF YOU CALL THIS CONSTRUCTOR*/
	CEnvironment();

	// populate CEnvironment variables from cspecs variables to update CEnvironment
	void update(CSpecs_t *problem = NULL);

	// populate the corrodor in CEnvironement based on the corridor in cspecs. this will 
	// grow the corridor so that the initial and final poses are legal.
	void updateCorridor();

	// set a polygon representing alice and the specified safety region around her
	void updateSafety(int safety_type);

	// assuming that alice is at aliceLocation and that at least some of her is sticking
	// into the corridor, grow the corridor by unioning an area around 
	// with the existing corridor. This is to ensure that alice's location is legal
	void growCorridor(pose2 aliceLocation);

	// return a rectangle which represents alice + the safety region at the origin 
	// with 0 radians heading
	point2arr getSafety(int safety_type);
	// return a rectangle which represents alice + the safety region rotated and 
	// translated to location
	point2arr getSafety(int safety_type, pose2 location);

	/* accessor for the start and end points */
	/* return the average of the lower and upperbounds for the
	   start and end points. if pointers to lowerbound and upper
	   bound pose2 structs are provided, those objects will be 
	   filled in appropiately */
	pose2 getStartPoint();
	pose2 getEndPoint();

	/* return whether the specified point is in the legal to drive though */
	bool isPointLegal(pose2 point);

	/* return whether a point is inside the given corridor */
	bool isInsideCorridor(pose2 point);

	/* return whether the point intersects an obstacle in the zone */
	bool intersectsObstacle(pose2 point);

	/* check whether the whole path remains inside the corridor */
	bool isPathInsideCorridor(vector<pose2> *path);

	/* check whether the path intersects an obstacle at any point */
	bool doesPathIntersectObstacle(vector<pose2> *path);

	/* return whether all the points in the specified path are within
	   the legal driving region; should use the function isPointLegal.
	   also note that it takes a pointer to a vector of points, not
	   an actual vector (this is done for speed- passing a pointer is
	   faster), so it needs to be sure NOT to modify the points */
	bool isPathLegal(vector<pose2> *path);

	/* get the cost a the given point */
	double getPointCost(pose2 point);

	/* sum up the cost along all the points in the path and return
	   that. should use the getPointCost function and should NOT 
	   modify the points (it takes a pointer like isPathLegal) */
	double getPathCost(vector<pose2> *path);

	/* return the cost heuristic, the estimated cost from point to
	   the finish */
	double getHeuristicToGoal(pose2 start, bool is_reverse);

	/* return the heuristic between arbitrary points */
	double getHeuristic(pose2 start, pose2 goal, bool is_reverse);

	double getInitialVelocity();

	double getGoalVelocity();

	/* whether to plan in forward or reverse or both */
	S1PlanMode getPlanMode();

	/* return cspecs traffic state */
	int getTrafficState();

	point2arr getPolyBoundary() { return m_grownCorridor; }

	vector<point2arr> getObstacles() { return m_cspecs->getPolyObstacles(); }
};

#endif



