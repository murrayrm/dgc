/*
 *  Stage1Planner.cc
 *  Stage1Planner
 *
 *  Created by Noel duToit
 */

#include <time.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <math.h>

#include "Stage1Planner.hh"


CStage1Planner:: CStage1Planner(int SkynetKey, bool WAIT_STATE)
: CSkynetContainer(MODdynamicplanner,SkynetKey),CStateClient(WAIT_STATE), CRDDFTalker(true)
{
  /*** Create various objects ***/

  cout << "Constructing CState1Planner object" << endl;

  /* Create an OCPspecs object */
   m_pProblemSpecs = new CEnvironment(SkynetKey);
   if (m_pProblemSpecs == NULL)
      cout << "ERROR: OCPtSpecs object was NOT created successfully" << endl;
  
  /*** Initialize objects and variables ***/

  m_TrajFollowSendSocket = m_skynet.get_send_sock(SNtraj);
  m_SuperConSendSocket = m_skynet.get_send_sock(SNsuperconTrajfCmd);

  planner = new CClothoidPlanner(m_pProblemSpecs, &m_state);

  cout << "Construction Complete" << endl; 
}

CStage1Planner::~CStage1Planner(void)
{
//  if(m_Traj != NULL)
//    delete m_Traj;
  if (m_pProblemSpecs != NULL)
    delete m_pProblemSpecs;
}

/* main work function- s1planner basically stays in this function the entire time
   that it runs. This loops through generating a trajectory and sending it */
void CStage1Planner::ActiveLoop(void)
{
  int numTrajs = 1;
  scCmd.commandType = tf_forwards;

  ofstream log("cycle_times.log");

  /* wait a couple of seconds so that hopefully we'll get good data on the 
     first time through */
  sleep(2);
  unsigned long long dgcTime, dgcDiffTime, dgcStartTime = DGCgettime();
  double timeTaken;
  bool haveNewTraj=false;
  pose2 initCond, finalCond;
  double initV, finalV;

  /*** The main planning loop- each time through this loop we generate 
       and send a trajectory ***/
//  for (int j= 0; j < 1; j++)
  while (1)
  {
	cout << " ***** Loop Starting ***** " << endl;
	/** Set up some variables and objects **/
	  /* eventually these will be determined from the driving environment state,
	     but for now just set them statically */
	  /* set up our mode- for now we'll only work on planning in the forward 
	     direction */
	  PlanMode = no_reverse;
	  runtime = 0.50;

	  /* get the most recent state data */
	  UpdateState();

	  // create the instance of the ctraj object here since its better
	  // to allocate and free memory in the same function
	  m_Traj = new CTraj(3);
	  /* Initialize the trajectory */
	  m_Traj->startDataInput();

	  /* give time for ocpspecs to update */
	  usleep(100000);

	/** main work- this is a very simple state machine to handle a few cases **/

	// start to solve the problem- I think this locks variables while you're 
	// using them
	m_pProblemSpecs->startSolve();

	initCond = m_pProblemSpecs->getStartPoint();
	finalCond = m_pProblemSpecs-> getEndPoint();
	initV = m_pProblemSpecs->getInitialVelocity();
	finalV = m_pProblemSpecs->getGoalVelocity();


	// ---- Stopped Condition ----
	// should probably also check whether we're done with the mission...
	if ( fabs(finalV) < 1e-3
		&& (finalCond - initCond).magnitude() < 
		    S1PLANNER_STOP_CONDITION_THREASHOLD )
	{
		cout << "Stop Condition Detected" << endl;
		haveNewTraj = getStoppedTraj();
		usleep(200000);
	}
	// ---- Nominal Driving condition ----
	else
	{
		/********************************/
		/*** Calculate the trajectory ***/
		/********************************/
		planner->generatePaths(runtime, PlanMode);
		haveNewTraj = getS1Traj();
	}


	// unlock variables
	m_pProblemSpecs->endSolve();

	/** send the data by skynet... Only if we have a new Traj **/
	if(haveNewTraj)
	{
		cout << "sending traj number " << numTrajs++ << endl;
		SendTraj(m_TrajFollowSendSocket, m_Traj);
	}
	else
	   cout << "Empty new trajectory... keeping the previous one" << endl;

	//ofstream traj("traj_clothoids.dat");
	//m_Traj->print(traj);

	  /* command the correct gear */
	  m_skynet.send_msg(m_SuperConSendSocket, &scCmd, sizeof(scCmd), 0);

	delete m_Traj;

	dgcTime = DGCgettime();
	dgcDiffTime = (unsigned long long)(dgcTime-dgcStartTime);
	timeTaken = DGCtimetosec(dgcDiffTime, false);
	log << "Time Taken: " << timeTaken << endl;
	dgcStartTime = dgcTime;

  }

  return;
}


// generate a trajectory which will cause the vehicle to just remain stopped in place
bool CStage1Planner::getStoppedTraj()
{
	pose2 initCond, finalCond;
	initCond = m_pProblemSpecs->getStartPoint();
	finalCond = m_pProblemSpecs-> getEndPoint();

	// the traj will have 1 points: one where the vehicle is now, and one
	// that is 10cm directly in front of the vehicle
	finalCond.x += 0.1 * cos(initCond.ang);
	finalCond.y += 0.1 * sin(initCond.ang);
	
	m_Traj->addPoint(initCond.x, 0, 0, initCond.y, 0, 0);
	m_Traj->addPoint(finalCond.x, 0, 0, finalCond.y, 0, 0);
	return true;
}

/* convert the lowest cost branch of the clothoid tree into a traj and
   return it. generatePaths must have been called before this */
bool CStage1Planner::getS1Traj()
{
	bool haveNewTraj = false;
	/* the ctraj should be created outside, and initialized with startDataInput
	 * it is a good practice to create and delete memory in the same function
	 * CTraj *m_Traj = new CTraj(3); */

	vector<pose2> clothoid = planner->getBestPath();
	vector<double> velocityProfile, accelerationProfile;
	generateVelocityProfile(clothoid, velocityProfile, accelerationProfile);

	pose2 Pt, nextPt, diff;
	double vx, vy, dvx, dvy;
	int j = 0;

        if ( !clothoid.empty() )
	{
		haveNewTraj = true;
                for (vector<pose2>::iterator i = clothoid.begin(); 
			i != (clothoid.end() - 1); i++, j++)
                {
			Pt = (*i);
			nextPt = *(i + 1);
			diff = nextPt - Pt;
			if ( diff.magnitude() < 0.05 )
				continue;
			vx = velocityProfile[j] * diff.x / diff.magnitude();
			vy = velocityProfile[j] * diff.y / diff.magnitude();
			dvx = accelerationProfile[j] * diff.x / diff.magnitude();
			dvy = accelerationProfile[j] * diff.y / diff.magnitude();
			m_Traj->addPoint(i->x, vx, dvx, i->y, vy, dvy);
                }

		// use the same components of the derivative as the 2nd to last point to 
		// get the direction for last point
		pose2 lastPt = clothoid.back();
		vx = velocityProfile.back() * diff.x / diff.magnitude();
		vy = velocityProfile.back() * diff.y / diff.magnitude();
		dvx = accelerationProfile[j] * diff.x / diff.magnitude();
		dvy = accelerationProfile[j] * diff.y / diff.magnitude();
		m_Traj->addPoint(lastPt.x, vx, dvx, lastPt.y, vy, dvy);
	}
	else
	{
		cout << "ERROR: Failed to generate a trajectory" << endl;
		haveNewTraj = false;
	}
	return haveNewTraj;
}

// temporary function until we can get a real velocity planner

#define S1P_V_MAX 	4.0
#define S1P_A_MAX 	0.6
#define S1P_A_MIN 	-1.2
#define VERBOSE 	0
#define MIN_INIT_V	0.5

void CStage1Planner::generateVelocityProfile(vector<pose2> path,
	vector<double>& vProfile, vector<double>& aProfile)
{  
    if (path.empty())
       return;

    double v0, v0u;
    double vf, vfu; 
    m_pProblemSpecs->getGoalVelocity(&vf,&vfu);
    m_pProblemSpecs->getInitialVelocity(&v0,&v0u);

	// an alice at rest will tend to stay at rest- this is an attempt to make sure
	// that alice starts to move down the traj and doesn't sit at the start point
	// because the velocity at that point is 0.
	if (v0 < MIN_INIT_V)
		v0 = MIN_INIT_V;
	if (v0u < MIN_INIT_V)
		v0u = MIN_INIT_V;
    
    int Npoints = path.size();
    vProfile.clear();
    aProfile.clear();
    vector<double> dist;
    pose2 diff;
    for(int i=0;i<Npoints-1;i++)
    {
    	diff = path[i+1]-path[i];
        dist.push_back(diff.magnitude());
    }

    vProfile.push_back(v0) ;
    double t;
      for(int i=1; i<Npoints; i++)
    { 
       // t =S/v, approximated using Euler fwd
        t =(-vProfile[i-1]+sqrt(pow(vProfile[i-1],2)+2*S1P_A_MAX*dist[i-1]))/S1P_A_MAX;
        	vProfile.push_back( min(vProfile[i-1]+S1P_A_MAX*t,S1P_V_MAX) );
    }
    vProfile[Npoints-1]=min(vf, S1P_V_MAX);
//  
//    cout << Npoints << endl;
    for(int i=Npoints-2; i>=0; i--)
    {
        t=( -vProfile[i+1]+sqrt(pow(vProfile[i+1],2)+2*fabs(S1P_A_MIN)*dist[i]) )/fabs(S1P_A_MIN);
        	vProfile[i]=min(vProfile[i+1]+fabs(S1P_A_MIN)*t, vProfile[i]);
    }
  
    for(int i=0; i<Npoints-1; i++)
    {
    		aProfile.push_back( ( pow(vProfile[i+1],2)- pow(vProfile[i],2) )/(2*dist[i]) ); 
    }
    aProfile.push_back(0.0);
    
    if(VERBOSE)
    {
	cout << "path.size()=" << path.size()<< ", "
	     << "vProfile.size()=" << vProfile.size() << ", "
	     << "aProfile.size()=" << aProfile.size() << endl;

        for(int i=1; i<Npoints; i++)
    	   cout <<i<< " d: " << dist[i] << " v: " << vProfile.at(i)<< " a: " << aProfile.at(i)<< endl;     

    }
}




