clear all;
UT_senario = 'UT_park_stluke_exit1.dat';
%UT_senario = 'UT_uturn.dat';

%unix('rm i486-gentoo-linux/UT_clothoidplanner');
%unix('make -f Makefile.yam all MODULE_LINK_FLAGS="-s"');
unix(['UT_clothoidplanner ' UT_senario] );
close all;



specs = dlmread(UT_senario);
% define some high cost areas
turn_rad = 7.3489;  % alice's minimum turning radius
th_g = specs(2,3);
trans_g = [ turn_rad*sin(th_g) -1*turn_rad*cos(th_g) ];
center_g1 = specs(2,1:2) + trans_g;
center_g2 = specs(2,1:2) - trans_g;
th_s = specs(1,3);
trans_s = [ turn_rad*sin(th_s) -1*turn_rad*cos(th_s) ];
center_s1 = specs(1,1:2) + trans_s;
center_s2 = specs(1,1:2) - trans_s;

% plot a single clothoid or clothoid tree
figure;
clothoids = dlmread('clothoidSolution.dat');

hold on;
% get the parking spaces
numParkingSpaces = specs(3,1);
% plot the corridor from the upper left and lower right boundaries
xcoords = specs(5 + numParkingSpaces: size(specs, 1), 1);
ycoords = specs(5 + numParkingSpaces: size(specs, 1), 2);
patch(xcoords, ycoords, 'w');
%plot the areas that are high heuristic cost b/c they are hard to get to
%the goal from them
fnplt(rsmak('circle',turn_rad, center_g1), 'g');
fnplt(rsmak('circle',turn_rad, center_g2), 'g');
fnplt(rsmak('circle',turn_rad, center_s1), 'b');
fnplt(rsmak('circle',turn_rad, center_s2), 'b');
% plot the initial and final positions
plot(specs(1:2,1), specs(1:2,2), 'r+');
% plot the clothoids
plot(clothoids(:,1), clothoids(:,2), '-','MarkerSize',1);
% labels
xlabel('X (meters)');
ylabel('Y (meters)');
title('Clothoid Tree');
% determine whether our area is larger in the x or y direction and set the
% scaling in that direction
if ( (max(xcoords) - min(xcoords)) > (max(ycoords) - min(ycoords)))
    xlim([min(xcoords)-1 max(xcoords)+1]);
else
    ylim([min(ycoords)-1 max(ycoords)+1]);
end
axis equal;

figure;
clothoids = dlmread('clothoidTree.dat');


hold on;
% get the parking spaces
numParkingSpaces = specs(3,1);
% plot the corridor from the upper left and lower right boundaries
xcoords = specs(5 + numParkingSpaces: size(specs, 1), 1);
ycoords = specs(5 + numParkingSpaces: size(specs, 1), 2);
patch(xcoords, ycoords, 'w');
%plot the areas that are high heuristic cost b/c they are hard to get to
%the goal from them
fnplt(rsmak('circle',turn_rad, center_g1), 'g');
fnplt(rsmak('circle',turn_rad, center_g2), 'g');
fnplt(rsmak('circle',turn_rad, center_s1), 'b');
fnplt(rsmak('circle',turn_rad, center_s2), 'b');
% plot the initial and final positions
plot(specs(1:2,1), specs(1:2,2), 'r+');
% plot the clothoids
plot(clothoids(:,1), clothoids(:,2), '.','MarkerSize',1);
% labels
xlabel('X (meters)');
ylabel('Y (meters)');
title('Clothoid Tree');
% determine whether our area is larger in the x or y direction and set the
% scaling in that direction
if ( (max(xcoords) - min(xcoords)) > (max(ycoords) - min(ycoords)))
    xlim([min(xcoords)-1 max(xcoords)+1]);
else
    ylim([min(ycoords)-1 max(ycoords)+1]);
end
axis equal;

