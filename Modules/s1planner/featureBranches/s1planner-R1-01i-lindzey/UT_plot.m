clear all;
UT_senario = 'UT_park_stluke_enter1.dat';
%UT_senario = 'UT_uturn.dat';

unix('rm i486-gentoo-linux/UT_clothoidplanner');
unix('make -f Makefile.yam all');
unix(['UT_clothoidplanner ' UT_senario] );
close all;



specs = dlmread(UT_senario);
% define some high cost areas
turn_rad = 7.3489;  % alice's minimum turning radius
th_f = specs(2,3);
trans = [ turn_rad*sin(th_f) -1*turn_rad*cos(th_f) ];
center1 = specs(2,1:2) + trans;
center2 = specs(2,1:2) - trans;


% plot a single clothoid or clothoid tree
figure;
clothoids = dlmread('clothoidSolution.dat');
hold on;
% get the parking spaces
numParkingSpaces = specs(3,1);
% plot the corridor from the upper left and lower right boundaries
xcoords = specs(4 + numParkingSpaces: size(specs, 1), 1);
ycoords = specs(4 + numParkingSpaces: size(specs, 1), 2);
patch(xcoords, ycoords, 'w');
%plot the areas that are high heuristic cost b/c they are hard to get to
%the goal from them
fnplt(rsmak('circle',turn_rad, center1), 'g');
fnplt(rsmak('circle',turn_rad, center2), 'g');
% plot the initial and final positions
plot(specs(1:2,1), specs(1:2,2), 'r+');
% plot the clothoids
plot(clothoids(:,1), clothoids(:,2), '-','MarkerSize',1);
% labels
xlabel('X (meters)');
ylabel('Y (meters)');
title('Clothoid Tree');
% determine whether our area is larger in the x or y direction and set the
% scaling in that direction
if ( (max(xcoords) - min(xcoords)) > (max(ycoords) - min(ycoords)))
    xlim([min(xcoords)-1 max(xcoords)+1]);
else
    ylim([min(ycoords)-1 max(ycoords)+1]);
end
axis equal;

figure;
clothoids = dlmread('clothoidTree.dat');
hold on;
% get the parking spaces
numParkingSpaces = specs(3,1);
% plot the corridor from the upper left and lower right boundaries
xcoords = specs(4 + numParkingSpaces: size(specs, 1), 1);
ycoords = specs(4 + numParkingSpaces: size(specs, 1), 2);
patch(xcoords, ycoords, 'w');
%plot the areas that are high heuristic cost b/c they are hard to get to
%the goal from them
fnplt(rsmak('circle',turn_rad, center1), 'g');
fnplt(rsmak('circle',turn_rad, center2), 'g');
% plot the initial and final positions
plot(specs(1:2,1), specs(1:2,2), 'r+');
% plot the clothoids
plot(clothoids(:,1), clothoids(:,2), '.','MarkerSize',1);
% labels
xlabel('X (meters)');
ylabel('Y (meters)');
title('Clothoid Tree');
% determine whether our area is larger in the x or y direction and set the
% scaling in that direction
if ( (max(xcoords) - min(xcoords)) > (max(ycoords) - min(ycoords)))
    xlim([min(xcoords)-1 max(xcoords)+1]);
else
    ylim([min(ycoords)-1 max(ycoords)+1]);
end
axis equal;

