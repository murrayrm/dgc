/*
 * ClothoidPlanner.cc
 * Main methods and framework for the Clothoid implementation of the
 * Stage 1 planner
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 1 May 07- Kenny Oslund
	- Created file
 *
 */

#include "ClothoidPlanner.hh"

/*------------------ initialization functions ------------------- */

CClothoidPlanner::CClothoidPlanner(CEnvironment *env, VehicleState *state)
{
	m_aliceEnv = env;
	m_aliceState = state;

	m_searchTreeBase = NULL;
	initialize();
	/* build the clothoid db to use in explore */
	precomputeClothoids();
	cycle_count = 0;

	return;

}

CClothoidPlanner::~CClothoidPlanner()
{
	if ( m_searchTreeBase != NULL)
		delete m_searchTreeBase;
	for (int j = 0; j < CLOTHOID_DB_NUM_LENGTHS; j++)
		for (int i = 0; i < CLOTHOID_DB_INIT_CURV_IDX_BOUND; i++)
			for (int h = 0; h < CLOTHOID_DB_FINAL_CURV_IDX_BOUND; h++)
				if ( m_pathDB[j][i][h] != NULL )
					delete m_pathDB[j][i][h];
	
}

// initialize everything to an empty search tree- this is used on startup and
// if the actual position gets too far from the previously generated trajectory
void CClothoidPlanner::initialize()
{
	// delete any already existing search tree
	//cout << "initialize: deleting clothoid tree and planning from scratch" << endl;

	if ( m_searchTreeBase != NULL )
		delete m_searchTreeBase;
	m_searchTreeBase = new CElementaryPath(m_aliceEnv);

	// discard whatever was previously found as the best path,
	// so that setStartPoint will use tplanner's initial conidtions
	m_bestFinish = NULL;
	updateStartCount = 0;
	cur_resolution = 0;
	// initialize the search tree
	setStartPoint();
	updateEnd(true);
	//search(m_searchTreeBase);

}

void CClothoidPlanner::precomputeClothoids()
{
	double new_initCurv, new_finalCurv, new_length;

	//cout << "S1Planner is precomputing clothoid paths..." << endl;

	// iterate over all lengths
	for (int j=0; j<CLOTHOID_DB_NUM_LENGTHS; j++)
	{
		new_length = CLOTHOID_DB_MAX_LENGTH *
			( 1.0 - ((double)j / CLOTHOID_DB_NUM_LENGTHS) );

		for (int i = 0; i < CLOTHOID_DB_INIT_CURV_IDX_BOUND; i++)
		{
			new_initCurv = PATH_MAX_DYN_FEASIBLE_CURVATURE * (i - CLOTHOID_DB_NUM_CURVATURES) 
				/ CLOTHOID_DB_NUM_CURVATURES;
			// iterate over all the final curvatures
			for (int h = 0; h < CLOTHOID_DB_FINAL_CURV_IDX_BOUND; h++)
			{
				new_finalCurv = PATH_MAX_DYN_FEASIBLE_CURVATURE * 
					(h + (i - CLOTHOID_DB_NUM_CURVATURES) - CLOTHOID_DB_NUM_FINAL_CURVATURES) 
					/ CLOTHOID_DB_NUM_CURVATURES;
				if ( fabs(new_finalCurv) <= PATH_MAX_DYN_FEASIBLE_CURVATURE )
				{
/*					//cout << "precomputing: length: " << new_length << " initCurve: " 
						<< new_initCurv << " finalCurv: " << new_finalCurv << endl;*/
					m_pathDB[j][i][h] =
						new CElementaryPath(NULL, new_initCurv, new_finalCurv, new_length);
				}
				else
					m_pathDB[j][i][h] = NULL;
			}
		}
	}
	//cout << "Precomputing complete. " << endl;
	return;
}

/* ---------------------------- Main work functions ---------------------------------- */
bool CClothoidPlanner::generatePaths(double runTime)
{

	/* update the tree. There are 3 things that can happen between planning cycles
	   which can cause us to have to update the tree:
	   * bad trajectory following:
		- alice isn't following the trajectory, so our current start possition isn't
		on the tree that we've built to search over
		=> need to completely scrap the tree and restart planning from where we are
	   * start point progesses along graph
		- this is what we hope will happen- alice has traveled down the trajectory that
		we've made.
		=> need to delete all the paths and their decendants that originate from a node 
		behind us, delete any paths that we've traversed complete and gone beyond the
		end of, update the pose in m_searchTreeBase, update the child paths of
		m_searchTreeBase to point to whatever paths are now directly ahead of us, and 
		update the parant node of those to point to us.
	   * goal point has moved
		- we got a new goal
		=> need to mark all the goal as no longer being at the goal and update costs.
	*/

	#ifdef CLOTHOID_UPDATE_TREE
	// check that we haven't gotten too far from the previously generated traj
	int pathInd;
	CElementaryPath *curClothoid = getCurrentClothoid(pathInd);
	if (curClothoid != NULL)
	{
		pose2 closestPoint = (curClothoid->getClothoidPoints())[pathInd];
		if ( (closestPoint - m_aliceEnv->getStartPoint()).magnitude() > 
			CLOTHOID_MAX_REINITIALIZE_CHANGE )
		{
			initialize();
			//cout << "error from previously computed traj is too great- "
			//	<< "reinitializing from scratch." << endl;
		}
		else
		{
			// update the search tree by checking for changed start/end point and
			// deleting nodes that we've passed
			// if start or end point has changed, reset the number of cycles 
			// that have gone by without them changing
			bool startMoved = updateStart();
			bool endMoved = updateEnd();

			if ( startMoved || endMoved )
				updateStartCount = 0;
			else
				updateStartCount++;
		}
	}
	else
	{
		initialize();
		//cout << "no current solution- reinitializing" << endl;
	}

	if ( updateStartCount > MAX_UPDATE_START_COUNT )
	{
		//cout << "Alice hasn't moved in the last " << MAX_UPDATE_START_COUNT
		//<< " planning cycles, so not doing any additional planning until she's moved"
		//<< endl;
		usleep((int)(1000000 * runTime));
		return false;
	}

	#else
	initialize();
	#endif

	cur_resolution = 0;
	CElementaryPath *lowestF;
	bool status;
	S1PlanMode planMode = m_aliceEnv->getPlanMode();

	unsigned long long dgcTime, dgcDiffTime, dgcStartTime = DGCgettime();
	double timeTaken = 0; 
	int iterations = 0;


	// debugging info
	//cout << "Search Tree initialized and starting at (x,y,theta): ";
// 	m_searchTreeBase->printClothoid();
	//cout << "Tplanner IC: "; (m_aliceEnv->getStartPoint()).display();
	//cout << "Going to end point: ";
	//(m_aliceEnv->getEndPoint()).display();
/*	if(m_aliceEnv->isPointLegal(m_aliceEnv->getEndPoint()))
		Console::addMessage("End Point IS legal");
	else
		Console::addMessage("End Point is NOT Legal");
	//cout << "Cost Heuristic to end: " << m_searchTreeBase->getCost() << endl;
	if ( planMode == reverse_required || planMode == reverse_allowed )
		//cout << "Planning in reverse allowed" << endl;
*/
// 	if ( m_aliceEnv->getHeuristicToGoal(getInitialPlanningPoint()) < 25 && cur_resolution < 2 )
// 	{
// 		// planning in a smaller areas, so increase the resolution
// 		//cout << "Goal is less than 25 meters away- increasing resolution" << endl;
// 		cur_resolution = 2;
// 	}

	#define NUM_ITERATIONS		200
	for (int i = 0; i < NUM_ITERATIONS; i++)

	/* this is the main work loop- it finds the lowest cost unexplored node, explores it
	   (lays down precomputed clothoids), searches it (tries to connect it to the goal with
	   a simple path using an analytical solution), and repeats until time is up. */
	//while ( timeTaken < runTime )
	{
		//lowestF = getNextClothoidToExplore(CLOTHOID_EXPLORE_NEXT_OUTWARD);
		//lowestF = getNextClothoidToExplore(CLOTHOID_EXPLORE_NEXT_RANDOM);
		lowestF = getNextClothoidToExplore(CLOTHOID_EXPLORE_NEXT_A_STAR);

/*		//cout << "Iteration: " << i;
		if ( lowestF == NULL )
			//cout << "no node to explore found" << endl;
		else
			//cout << " exploring node starting at "; lowestF->getLastPoint().display(); */

		if ( lowestF != NULL )
		{
			// search/ explore in forward
			if ( planMode == no_reverse || planMode == reverse_allowed )
			{
				explore(lowestF, false);
				status = search(lowestF, false);
			}
			// search/ explore in reverse
			if ( planMode == reverse_required || planMode == reverse_allowed )
			{
				explore(lowestF, true);
				// search doesn't work in reverse right now, and I think it rare that
				// we'll want to reach the check point in reverse.
				// status = search(lowestF, true);
			}
		}
		else
		{
			if ( cur_resolution < CLOTHOID_MAX_RESOLUTION - 1 )
			{
				//cout << "No more nodes to explore at this resolution; "
				//	<< "increasing resolution" << endl;
				cur_resolution++;
			}
			else
				break;
		}
		dgcTime = DGCgettime();
		dgcDiffTime = (unsigned long long)(dgcTime-dgcStartTime);
		timeTaken = DGCtimetosec(dgcDiffTime, false);
		iterations++;
	}

	cycle_count++;
	//cout << "Completed " << iterations << " iterations in " << timeTaken << " seconds in cycle: " << cycle_count << endl;

// 	if (cycle_count > 200 )
// 		exit(0);

// 	char file[30];
// 	sprintf(file, "ctree_%d.dat", cycle_count);
// 	ofstream ctree(file);
// 	m_searchTreeBase->printAllClothoids(&ctree);

	return true;
}

#ifdef NEVER
some various bits of old test code


	//CElementaryPath *path = new CElementaryPath(m_aliceEnv, .08, 5, false, pose2(0,0,M_PI_2 / 2));
//	CElementaryPath *path = new //CElementaryPath();
//	path->generateBielementaryClothoid(pose2(0,0,0), pose2(-10,10,-M_PI / 4), true);
//	search(m_searchTreeBase, true);
//	m_searchTreeBase->connectClothoidCopy(
//		m_pathDB[0][0]);
//m_searchTreeBase.connectToFinish(pose2(10,10,M_PI / 4));

	explore(m_searchTreeBase, true);

	vector<CElementaryPath *> nodes = m_searchTreeBase->getSearchChildren();
        for (vector<CElementaryPath*>::iterator i = nodes.begin();
			i != nodes.end(); i++)
		search(*i);



#endif

/* search and explore are the core of the algorithm */
bool CClothoidPlanner::search(CElementaryPath *m_nodeToSearch,
	bool in_reverse)
{
	CElementaryPath *lastClothoid;
	bool status;
	status = m_nodeToSearch->connectToTarget(m_aliceEnv->getEndPoint(),
		in_reverse, &lastClothoid);

	if ( status )
	{
		lastClothoid -> m_atFinish = true;
		// save this node as the best finish if its cost is less than
		// the previous best finish
		// TODO: may want to include some penalty for choosing a 
		// different path to end, so alternative path must be better by
		// x amount, not just slightly better. this would look like
		// if (new_path->getCost() + X < bestFinishCost)
		// where X is a constant cost penalty for choosing a different 
		// solution
		if ( m_bestFinish == NULL || !m_bestFinish->m_atFinish ||
				lastClothoid->getCost() < m_bestFinish->getCost() )
			m_bestFinish = lastClothoid;
	}
	return status;
}

bool CClothoidPlanner::explore(CElementaryPath *nodeToExplore, bool in_reverse)
{
	double initCurv;
	pose2 initPt = nodeToExplore->getLastPoint(&initCurv);
	if (in_reverse)
		initCurv *= -1;	
	int initCurvIdx = getCurvIdx(initCurv);
	CElementaryPath *new_clothoid;

	for (int i = 0; i < CLOTHOID_DB_FINAL_CURV_IDX_BOUND; i++)
	{
		if ( nodeToExplore->connectClothoidCopy(m_pathDB[cur_resolution][initCurvIdx][i], 
			in_reverse, &new_clothoid) )
		{
			// if no nodes so far have reached the finish, and one of the clothoids 
			// that we just layed down is closer to the finish than any other best 
			// end node then save it as the new best end node.
			// note that we leave the cost at its (high) initial value so that if we 
			// find any node that reaches the end, we'll choose it over this one. 
			// this entire check is just to make sure we find something in the event 
			// that our goal is outside of the legal driving region
			if ( m_bestFinish == NULL || (!m_bestFinish->m_atFinish
					&& m_aliceEnv->getHeuristicToGoal(new_clothoid->getLastPoint()) 
					 < m_aliceEnv->getHeuristicToGoal(m_bestFinish->getLastPoint())) )
				m_bestFinish = new_clothoid;
		}
	}

	/* mark the node as explored at this resolution */
	nodeToExplore->m_explored_resolution = cur_resolution;
	return true;
}


/* ----------------------accessor/ reporter fucntions -------------------------*/

/* functions for selecting the best/ lowest cost clothoid sequence to return */
vector<pose2> CClothoidPlanner::getBestPath(double *length, bool *reaches_goal)
{
	vector<CElementaryPath *> bestClothoids;
	vector<pose2> bestPath, clothoid;

	bestClothoids = getBestClothoidSequence(reaches_goal);

        if ( !bestClothoids.empty() )
                for (vector<CElementaryPath *>::iterator i = bestClothoids.begin(); 
                        i != bestClothoids.end(); i++)
		{
			clothoid = (*i)->getClothoidPoints();
			if ( !clothoid.empty() )
				for (vector<pose2>::iterator j = clothoid.begin(); 
					j != clothoid.end(); j++)
				{
					bestPath.push_back(*j);
				}
		}
	if ( length != NULL )
	*length = PATH_SECTION_LENGTH * bestPath.size();

	return bestPath;

}

/* return the direction that we need to drive along the current section */
trajFmode CClothoidPlanner::getDirection()
{
	vector<CElementaryPath *> bestSolution = getBestClothoidSequence();
	if ( !bestSolution.empty() )
	{
		CElementaryPath *firstClothoid = bestSolution.front();
		if (!firstClothoid->m_reverse)
			return tf_forwards;
		else
			return tf_reverse;
	}
	else
		return tf_forwards;
}

vector<CElementaryPath *> CClothoidPlanner::getBestClothoidSequence(bool *reaches_goal)
{
	CElementaryPath *m_endNode;
	vector<CElementaryPath *> bestPath, bestPathRev;

	// if we reached the goal, use the lowest cost goal node. otherwise
	// just plan to the node with the lowest cost estimate
	if ( m_bestFinish != NULL )	// solution to goal found
		m_endNode = m_bestFinish;
	else
		return bestPath;	// and empty path indicates failure

	// start at the endpoint that we've already determined is the best,
	// then just iterate from the end back to the beginning
	for ( CElementaryPath *node = m_endNode; node != NULL; 
			node = node->getSearchParent() )
		bestPathRev.push_back(node);

	// the last node in bestPath is our starting node, and the only point in its 
	// clothoid is also in the preceding node, so just delete it since its
	// redundant
	bestPathRev.pop_back();

	// now the clothoids are in the opposite order they need to be put 
	// into the traj, so reverse the order
        if ( !bestPathRev.empty() )
                for (vector<CElementaryPath *>::reverse_iterator i = bestPathRev.rbegin(); 
                        i != bestPathRev.rend(); i++)
                {
			bestPath.push_back(*i);
                }

	if ( reaches_goal != NULL )
	{
		if (!bestPath.empty()) {
		  if ( (bestPath.back())->m_atFinish )
		    *reaches_goal = true;
		  else
		    *reaches_goal = false;
		}
	}

	return bestPath;
}

/* function to determine which clothoid in the sequence of our clothoids we are currently
   along (after alice has progressed down the traj) */
CElementaryPath *CClothoidPlanner::getCurrentClothoid(int& index)
{
	// stub for now- say we're in the first clothoid...
	vector<CElementaryPath *> bestClothoids = getBestClothoidSequence();
	CElementaryPath *closestClothoid = NULL;
	int closestIndex, ind;
	double newdToClosest, dToClosest = 1000000;
	pose2 startPt = m_aliceEnv->getStartPoint();

	if (bestClothoids.empty())
	{
		closestIndex = 0;
		closestClothoid = NULL;
	}
	else
	{
		// iterate along the previous solution to find the point that we are closest to
		// (clothoid that we are in, and the index to that clothoid)
		for ( vector<CElementaryPath *>::iterator i = bestClothoids.begin();
			i != bestClothoids.end(); i++ )
		{
			// iterate over the point in the clothoid
			vector<pose2> clothoidPts = (*i)->getClothoidPoints();
			ind = 0;
			for ( vector<pose2>::iterator j = clothoidPts.begin();
				j != clothoidPts.end(); j++, ind++ )
			{
				newdToClosest = ((*j) - startPt).magnitude();
				if ( newdToClosest < dToClosest)
				{
					closestIndex = ind;
					dToClosest = newdToClosest;
					closestClothoid = (*i);
				}
			}
		}
	}

	index = closestIndex;
	return closestClothoid;
}

/* return the point that planning will start from. this may be tplanner's intial 
   condition or the initial condition projected onto our previous trajectory */
pose2 CClothoidPlanner::getInitialPlanningPoint()
{
	return m_searchTreeBase->getFirstPoint();
}

/* return the index for the clothoids in the matrix of precomputed clothoids which
   have the same inital curvature as the passed value */
int CClothoidPlanner::getCurvIdx(double initCurv)
{
	double curvDiff = 10000000, curv;
	int curvIdx;
	for (int i = 0; i < CLOTHOID_DB_INIT_CURV_IDX_BOUND; i++)
	{
		// first just find the first non-null row in the column
		int j;
		for (j = 0; m_pathDB[cur_resolution][i][j] == NULL; j++) {}
		m_pathDB[cur_resolution][i][j]->getFirstPoint(&curv);
		if ( fabs(initCurv - curv) < curvDiff )
		{
			curvDiff = fabs(initCurv - curv);
			curvIdx = i;
		}
	}
	return curvIdx;
}

/* figure out which node we should run search/ explore on based on the passed in
   method */
CElementaryPath *CClothoidPlanner::getNextClothoidToExplore(int method)
{
	switch (method)
	{
		case CLOTHOID_EXPLORE_NEXT_RANDOM:
		{
			// first make a list of all the unexplored nodes in the graph
			vector<CElementaryPath *> unexploredNodes;
			aggregateFlatNodeList(unexploredNodes, m_searchTreeBase,
				CLOTHOID_NODE_TYPE_UNEXPLORED);
			int rand = random();
			int idx = (int) round ( (float)rand / RAND_MAX * 
					((float)unexploredNodes.size() - 1 ) );
			return unexploredNodes.at(idx);
			break;
		}
		case CLOTHOID_EXPLORE_NEXT_A_STAR:
		{
			// first try to select another node while randomly skipping some-
			// this is an attempt to get it to spread out.
			CElementaryPath *nextNode = m_searchTreeBase->findLowestCost(cur_resolution, 0.1);
			// if there are very few nodes, randomly skipping some could cause us to 
			// not find any nodes, so if we didn't find one, try again while not skipping
			// any nodes
			if ( nextNode == NULL )
				nextNode = m_searchTreeBase->findLowestCost(cur_resolution, 0);
			return nextNode;
			break;
		}
		case CLOTHOID_EXPLORE_NEXT_OUTWARD:
		{
			vector<CElementaryPath *> unexploredNodes;
			aggregateFlatNodeList(unexploredNodes, m_searchTreeBase,
				CLOTHOID_NODE_TYPE_EXPLORED);
			CElementaryPath *nextNode = m_searchTreeBase->findLowestCost(cur_resolution, 0.4, &unexploredNodes);
			if ( nextNode == NULL )
				nextNode = m_searchTreeBase->findLowestCost(cur_resolution, 0, &unexploredNodes);
			return nextNode;
			break;
		}
		case CLOTHOID_EXPLORE_NEXT_GENETIC:
		{
			return NULL;
			break;
		}
	}
	return NULL;
}

/* create a vector which just contains pointers to every unexplored node in our
   graph. this allows us to easily pick one at random, something that is hard
   to do while it is in the linked list structure */
void CClothoidPlanner::aggregateFlatNodeList(vector<CElementaryPath *> &list,
	CElementaryPath *curNode, int nodeType)
{
	if ( nodeType == CLOTHOID_NODE_TYPE_ALL
		|| ( curNode->m_explored_resolution < cur_resolution && // node has not been explored
			nodeType == CLOTHOID_NODE_TYPE_UNEXPLORED )	
		|| ( curNode->m_explored_resolution >= cur_resolution && // node has been explored
			nodeType == CLOTHOID_NODE_TYPE_EXPLORED ) )
		list.push_back(curNode);
	
	vector<CElementaryPath *> children = curNode->getSearchChildren();
	if ( !children.empty() )
		for ( vector<CElementaryPath *>::iterator i = children.begin();
			i != children.end(); i++ )
		{
			aggregateFlatNodeList(list, *i);
		}
	return;
}

/* -------------------------- functions to dump data for debugging ------------- */

/* print every point in every clothoid in the tree to the specified file */
void CClothoidPlanner::dumpTree(ostream *f)
{
	m_searchTreeBase->printAllClothoids(f);
}

void CClothoidPlanner::dumpBestSolution(ostream *f)
{
	vector<pose2> sol = getBestPath();
	for (vector<pose2>::iterator j = sol.begin(); 
		j != sol.end(); j++)
	{
		j->print(f);
	}
}

void CClothoidPlanner::dumpPrecomputedClothoids(ostream *log)
{
	for (int j = 0; j < CLOTHOID_DB_NUM_LENGTHS; j++)
		for (int i = 0; i < CLOTHOID_DB_INIT_CURV_IDX_BOUND; i++)
			for (int h = 0; h < CLOTHOID_DB_FINAL_CURV_IDX_BOUND; h++)
				if ( m_pathDB[j][i][h] != NULL )
					m_pathDB[j][i][h]->printClothoid(log);
}


/* --------------------------- update functions--------------------------------- */
//these handle updating the clothoid tree between planning cycles.


/* create a new start node and set it up correctly */
void CClothoidPlanner::setStartPoint()
{
	m_searchTreeBase->clearClothoid();

	// if there was a previous solution, project tplanner's initial condition 
	// onto that and use that as the initial condition
	int clothoidInd;
	CElementaryPath *curClothoid = getCurrentClothoid(clothoidInd);
	if (curClothoid != NULL)
	{
		double curv;
		pose2 closestPoint = curClothoid->getFirstPoint(&curv);
		m_searchTreeBase->setCurvLength(curv, curv, 0);
		m_searchTreeBase->appendPoint(closestPoint);
	}
	else
	{
		m_searchTreeBase->appendPoint(m_aliceEnv->getStartPoint());
		m_searchTreeBase->setCurvLength(0, 0, 0);
	}

		
	m_searchTreeBase->setCost();	// no previous nodes
}


// update the start location. This includes deleting nodes that are bellow us => we will 
// not be driving on them.
// returns true if the start changed and was updated
bool CClothoidPlanner::updateStart(bool force)
{
	int clothoidInd = 0;
	CElementaryPath *curClothoid = getCurrentClothoid(clothoidInd);
	if (curClothoid == NULL)	// there is no previous solution
	{
		setStartPoint();
		//cout << "updateStart: no previous solution- planning from current location" << endl;
		return true;
	}
	// otherwise check whether the initial point has moved
	else if ( (getInitialPlanningPoint() - m_aliceEnv->getStartPoint()).magnitude()
		> CLOTHOID_MAX_START_SPACIAL_CHANGE 
		|| (getInitialPlanningPoint() - m_aliceEnv->getStartPoint()).ang
		> CLOTHOID_MAX_START_ANGLE_CHANGE || force)
	{
		//cout << "updateStart: start Pose changed- updating search tree" << endl;
		/* first determine which clothoid in our previous solution we are along */
		CElementaryPath *curClothoidParent = curClothoid->getSearchParent();
	
		// all the branches that originate from the node before this node need to be deleted.
		// to do this, just sever the link between the current clothoid's parent and itself,
		// then delete the base. this will recursively delete everything except the branch
		// of the tree that we're in.
		curClothoidParent->severLink(curClothoid);

		// also delete the points in the current clothoid up to where are
		curClothoid->deleteClothoidPoints(0, clothoidInd);

		// now create a new search tree base and link it to the clothoid we're currently in.
		// delete any already existing search tree
		if ( m_searchTreeBase != NULL )
			delete m_searchTreeBase;
		m_searchTreeBase = new CElementaryPath(m_aliceEnv);
		m_searchTreeBase->appendPath(curClothoid);
		m_searchTreeBase->m_explored_resolution = cur_resolution;
		setStartPoint();
		return true;
	}
	else
		return false;
}

// update the finish location. This involves deleting all the finish nodes and relinking 
// them to the new finish
// returns true if the end point has changed and was updated 
bool CClothoidPlanner::updateEnd(bool force)
{	
	pose2 newGoalPose = m_aliceEnv->getEndPoint();
	vector<CElementaryPath *> wasAtGoal;
	if ( (goalPose - newGoalPose).magnitude() > CLOTHOID_MAX_GOAL_SPACIAL_CHANGE 
		|| (goalPose - newGoalPose).ang > CLOTHOID_MAX_GOAL_ANGLE_CHANGE 
		|| force)
	{
		//cout << "updateEnd: Goal Pose changed by " << (goalPose - newGoalPose).magnitude()
		//	<< " - updating search tree" << endl;
		goalPose = newGoalPose;

		// if we're just starting up, m_searchTreeBase may not be initialized yet
		if (m_searchTreeBase != NULL)
		{
			/* mark all the goal nodes as no longer being goal nodes */
			wasAtGoal = m_searchTreeBase->setNodesAsNotAtGoal();
			m_searchTreeBase->setCost();
		}

		// no finish now
		m_bestFinish = NULL;
		// reset the resolution
		cur_resolution = 0;

		/* rerun search on points that search was run on before. this is because 
		   otherwise if a goal point moves backwards, it may cause odd trajectories */
		S1PlanMode planMode = m_aliceEnv->getPlanMode();
		for ( vector<CElementaryPath *>::iterator i = wasAtGoal.begin(); 
			i != wasAtGoal.end(); i++)
		{
			// search in forward
			if ( planMode == no_reverse || planMode == reverse_allowed )
				search(*i, false);
			// search in reverse
			if ( planMode == reverse_required || planMode == reverse_allowed )
				search(*i, true);
		}

		return true;
	}
	else
		return false;
}

