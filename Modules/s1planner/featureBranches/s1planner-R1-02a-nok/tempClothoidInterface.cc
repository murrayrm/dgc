/*
 * tempClothoidInterface.cc
 * temporary interface functions for 
 * the new planner stack to run the clothoid planner.
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 26 July 07- Kenny Oslund
	- Created file
 *
 */

#include "s1planner/tempClothoidInterface.hh"
#include <temp-planner-interfaces/Utils.hh>

GraphNode ClothoidPlannerInterface::nodeArr[GRAPH_PATH_MAX_NODES];
CEnvironment *ClothoidPlannerInterface::problem = new CEnvironment();
CClothoidPlanner *ClothoidPlannerInterface::planner = new CClothoidPlanner(problem, NULL);
int ClothoidPlannerInterface::cycleCount = 0;
int ClothoidPlannerInterface::failureCount = 0;
CMapElementTalker ClothoidPlannerInterface::mapTalker;

/* reset the clothoid tree by deleting all the clothoids, so that we plan from scratch */
void ClothoidPlannerInterface::clearClothoidTree(CSpecs_t *cSpecs)
{
	Log::getStream(4) << "CLOTHOIDPLANNERINTERFACE: clearing clothoid tree"<< endl;
	problem->update(cSpecs);
	failureCount = 0;
	planner->initialize();
}

/* update the clothoid tree and return the path */
Err_t ClothoidPlannerInterface::GenerateTraj(CSpecs_t *cSpecs, Path_t *path, double runtime, int sn_key, bool disp_costmap)
{
	Err_t error = S1PLANNER_OK;

	if (cSpecs != NULL)
		problem->update(cSpecs);

	/** some debugging info **/

	/* send some things to map viewer */
	mapTalker.initSendMapElement(sn_key);
	
	// safety box
        point2arr safetyBox = problem->getSafety( SAFETY_OBSTACLE, problem->getStartPoint() );
        MapElement MapElSafetyBox;
	MapId mapIdSafetyBox(14500);
	MapElSafetyBox.setId(mapIdSafetyBox);
	MapElSafetyBox.setGeometry(safetyBox);
        MapElSafetyBox.plotColor = MAP_COLOR_ORANGE;
	MapElSafetyBox.setTypePoly();
	mapTalker.sendMapElement(&MapElSafetyBox, -2);

	// bounding box
	/*
	point2arr zonePerimeter = problem->getPolyBoundary();
        MapElement MapElZonePerimeter;
	MapId mapIdZonePerimeter(14501);
	MapElZonePerimeter.setId(mapIdZonePerimeter);
	MapElZonePerimeter.setGeometry(zonePerimeter);
        MapElZonePerimeter.plotColor = MAP_COLOR_PURPLE;
	MapElZonePerimeter.setTypePoly();
	mapTalker.sendMapElement(&MapElZonePerimeter, -2);
	*/

	// console debug output
	pose2 pt;
	Console::addMessage("CLOTHOIDPLANNER:: ******** new planning cycle ******** ");
	pt= problem->getStartPoint();
	Console::addMessage("CLOTHOIDPLANNER:: planning from: x: %lf y: %lf ang: %lf", pt.x, pt.y, pt.ang);
	// log final point
	pt = problem->getEndPoint();
	Console::addMessage("CLOTHOIDPLANNER:: to: x: %lf y: %lf ang: %lf", pt.x, pt.y, pt.ang);
 	/* 
	if (problem->isPointLegal(problem->getStartPoint()))
 		Console::addMessage("CLOTHOIDPLANNER:: Start point IS legal");
 	else
 		Console::addMessage("CLOTHOIDPLANNER:: Start point is NOT legal");
 	if (problem->isPointLegal(problem->getEndPoint()))
 		Console::addMessage("CLOTHOIDPLANNER:: End point IS legal");
	else
 		Console::addMessage("CLOTHOIDPLANNER:: End point is NOT legal");
	*/


	/** the actual work **/
	if ( problem->intersectsObstacle(problem->getStartPoint()) )
	{
		Log::getStream(4) << "CLOTHOIDPLANNERINTERFACE: Start point is blocked; problem is invalid- NOT PLANNING" << endl;
		error |= S1PLANNER_START_BLOCKED;
	}
	else
	{
		// don't even bother to plan if the start point is blocked- we won't get anywhere,
		// but we can go ahead and plan if the end point is blocked- we'll get closer, just
		// not all the way there. still report if the end point is blocked though
		if ( problem->intersectsObstacle(problem->getEndPoint()) )
		{
			Log::getStream(4) << "CLOTHOIDPLANNERINTERFACE: End point is blocked- solution won't reach goal" << endl;
			error |= S1PLANNER_END_BLOCKED;
		}
		// recheck the entire tree for new intersections with obstacles every n cycles
		#ifdef RECHECK_OBSTACLE_INTERSECTION_INTERVAL
		if ( (cycleCount % RECHECK_OBSTACLE_INTERSECTION_INTERVAL) == 0 )
			planner->recheckTreeLegality();
		#endif
		planner->generatePaths(runtime);
	}
	double distToLastPlanPoint;
	error |= populatePath(path, distToLastPlanPoint);
	if ( ( error & (S1PLANNER_FAILED | S1PLANNER_END_BLOCKED) ) && distToLastPlanPoint < 5)
		failureCount++;
	else
		failureCount = 0;

	/** log the solution- this is VERY time intensive! */
	/*
 	char logFile[40];
 	sprintf(logFile, "cTree%d.dat", cycleCount);
 	ofstream cTreeLog(logFile);
 	planner->dumpTree(&cTreeLog);
 
 	sprintf(logFile, "cSol%d.dat", cycleCount);
 	ofstream cSolLog(logFile);
 	planner->dumpBestSolution(&cSolLog);
	*/

 	cycleCount++;
	Err_t returnErr;
	// only report out S1PLANNER_FAILED if we've failed MAX_ERROR_COUNT times or more
	// so mask off that bit if we haven't failed enough times
	if ( failureCount <= MAX_ERROR_COUNT )
	{
		returnErr =  error & (~S1PLANNER_FAILED & ~S1PLANNER_END_BLOCKED);
	}
	else
	{
		Log::getStream(4) << "CLOTHOIDPLANNERINTERFACE: We have failed to reach the goal " 
			<< "at least " << MAX_ERROR_COUNT << " times- reporting failure to planner." << endl;
	 	returnErr = error;
	}
	Log::getStream(4) << "CLOTHOIDPLANNERINTERFACE: Reported error status: " << returnErr << endl;
	return returnErr;
}

Err_t ClothoidPlannerInterface::populatePath(Path_t *path, double &distToLastPlanPoint)
{
	bool reaches_end;
	Err_t error = S1PLANNER_OK;
	vector<CElementaryPath *> solution = planner->getBestClothoidSequence(&reaches_end);

	if (NULL != path) {
	  path->valid = true;
	  path->collideObs = 0;
	  path->collideCar = 0;
	  
	  if (solution.size() > 0) {
		  CElementaryPath *lastPath = solution.back();
		  CElementaryPath *firstPath = solution.front();
		  if ( !lastPath->m_atFinish )
        {
          Log::getStream(4) << "CLOTHOIDPLANNERINTERFACE: did not reach goal" << endl;
          error |= S1PLANNER_FAILED;
        }
		  if ( lastPath->getCost() > S1PLANNER_HIGH_COST_VALUE )
		  {
        Log::getStream(4) << "CLOTHOIDPLANNERINTERFACE: cost is too high- we probably planned "
                          << "through an illegal/ off road area" << endl;
        error |= S1PLANNER_COST_TOO_HIGH;
		  }
		  distToLastPlanPoint = ( (lastPath->getLastPoint()) - (firstPath->getFirstPoint()) ).magnitude(); 

		  // we skip the first point of each clothoid so that we don't end up with
		  // points right on top each other (that point was in the previous clothoid).
		  // however, we do want the first point of the first clothoid because there was
		  // no previous clothoid
		  // handle the special case of the first point
      pose2 firstPt = solution.at(0)->getFirstPoint();
		  setPoint(0, firstPt, solution.at(0)->isPathReverse(), path);

      // The rest of the points
      int i = 1;
      // iterator over the clothoids
      for (vector<CElementaryPath *>::iterator j = solution.begin(); 
           j != solution.end(); j++) {
        vector<pose2> clothoid = (*j)->getClothoidPoints();
        // iterate over the points in the current clothoid
        // skip the first point of each clothoid so that we don't end up
        // with 2 points in exactly the same place
        for (vector<pose2>::iterator k = clothoid.begin() + 1; k != clothoid.end();
             k++, i++) {
          setPoint(i, *k, (*j)->isPathReverse(), path);
        }
		  }
		  path->goalDist = i * PATH_SECTION_LENGTH;
		  path->pathLen = i;
	  }
	  else
      error |= S1PLANNER_FAILED;
	}
	else
		error |= S1PLANNER_FAILED;
	return error;
}

void ClothoidPlannerInterface::setPoint(int index, pose2 newPt, bool reverse, Path_t *path)
{
  nodeArr[index].index = index; 	 
  nodeArr[index].type = GRAPH_NODE_ZONE;
  if ( reverse ) {
    nodeArr[index].pathDir = GRAPH_PATH_REV;
    // reverse the direction of the pose to point in the direction we're going rather
    // than the direction we're pointing if we're going in reverse
    newPt.rotateInPlace(M_PI);
  } else 
    nodeArr[index].pathDir = GRAPH_PATH_FWD;
  path->path[index] = &(nodeArr[index]);
  
  // OLD DEPRECIATED POSE STRUCTURES
  nodeArr[index].pose.pos.x = newPt.x;
  nodeArr[index].pose.pos.y = newPt.y;
  nodeArr[index].pose.pos.z = 0;
  nodeArr[index].pose.rot = quat_from_rpy(0, 0, newPt.ang);


  // NEW POSE STUCTUREs 	 
  nodeArr[index].pose_x = newPt.x; 	 
  nodeArr[index].pose_y = newPt.y; 	 
  nodeArr[index].pose_h = newPt.ang;
}





