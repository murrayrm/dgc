/*----------------------------------------------------------------------
  Fichier              : /RIA/idefix/users/taix/c/Trajectoire/Reed_Shepp/reed_shepp.c
  Fonction             : cette fct retourne la longueur minimum entre
  2 conf. avec point de rebroussement ( voir l'article ).
  Date de creation     : Jeudi 27 Juillet 14:52:23 1989
  Date de modification : Mardi 17 Avril 10:09:42 1990
  Nb de lignes         : 407 
  Auteur               : Michel Taix
----------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define infini HUGE_VAL


double reed_shepp();
double reed_shepp2();

extern char *pathnames[];

main(argc,argv)
int argc;
char *argv[];
{
double Cinit[3],Cfinal[3];
double t, u, v, length, best_length;
int pathtype, best;
int i;

if (argc < 3+1) {
  printf("usage:  reed_shepp x y phi [option]\n");
  return 1;
}

Cinit[0] = Cinit[1] = Cinit[2] = 0.0;
Cfinal[0] = atof(argv[1]);
Cfinal[1] = atof(argv[2]);
Cfinal[2] = atof(argv[3]);

if (argc >= 4+1)
  switch (argv[4][0]) {

  case 'a':  /* show all mode; print lengths of all path types */
    reed_shepp(Cinit, Cfinal, &best, &t, &u, &v);
    for (pathtype=0; pathtype < 48; pathtype++) {
      length = reed_shepp2(Cinit, Cfinal, pathtype+1, &t, &u, &v);
      printf("(#%2d)%c  %-17s", pathtype+1,
	     (pathtype + 1 == best) ? '*':' ',  pathnames[pathtype]);
      if (length < infini)
	printf("t=%-9f u=%-9f v=%-9f  length=%f\n", t, u, v, length);
      else printf("\n");
    }
    break;

  case 'p': /* only examine specific path types, print lengths & info */
    reed_shepp(Cinit, Cfinal, &best, &t, &u, &v);
    best_length = infini;
    for (i = 5; i < argc; i++) {
      pathtype = atoi(argv[i]) - 1;
      length = reed_shepp2(Cinit, Cfinal, pathtype+1, &t, &u, &v);
      printf("(#%2d)%c  %-17s", pathtype+1,
	     (pathtype + 1 == best) ? '*':' ',  pathnames[pathtype]);
      if (length < infini) {
	printf("t=%-9f u=%-9f v=%-9f  length=%f\n", t, u, v, length);
	if (length < best_length) best_length = length;}
      else printf("\n");
    }

  }

else { /* default mode */
  length = reed_shepp(Cinit, Cfinal, &pathtype, &t, &u, &v);
  printf("\npath type %s (#%d)\nt=%-10f u=%-10f v=%-10f  length=%f\n\n",
	 pathnames[pathtype-1], pathtype, t, u, v, length);
}


return 0;
}
    
