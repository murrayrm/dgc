/*
 * ElementaryPath.cc
 * Helper class for the ClothoidPlanner. This class is a container for
 * basic curves that we're using to build our trajectories
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 1 May 07- Kenny Oslund
	- Created file
 *
 */



#include "ElementaryPath.hh"
#include "ClothoidUtils.hh"


/* ------------------ constructors/ destructor -----------------------*/

/* this constructor should be used most of the time so that the cothoid can have access to 
   environment information */
CElementaryPath::CElementaryPath(CEnvironment *env)
{
	clearClothoid();
	m_searchParent = NULL;
	m_aliceEnv = env;
}

/* basic constructor- doesn't have an environment class.
   This should pretty much be unused */
CElementaryPath::CElementaryPath()
{
	cout << "WARNING: in basic constructor- this is probably wrong" << endl;
	clearClothoid();
	m_searchParent = NULL;
	m_aliceEnv = NULL;
}

/* constructor which fills in the clothoid based on passed parameters- used mainly
   for precomputed clothoids */
CElementaryPath::CElementaryPath(CEnvironment *env, double new_initCurv, double new_finalCurv,
	double new_length, bool new_reverse, pose2 startPose)
{
	/* call the standard constructor */
	clearClothoid();
	m_aliceEnv = env;

	setCurvLength(new_initCurv, new_finalCurv, new_length);
	m_reverse = new_reverse;
	calculateClothoid(startPose);
}

/* delete this node and all children */
CElementaryPath::~CElementaryPath()
{
	/* destructor needs to recursively delete all the children of 
	   the current node. */
        if ( !m_searchChildren.empty() )
               for (vector<CElementaryPath*>::iterator i = m_searchChildren.begin();
		i != m_searchChildren.end(); i++)
		{
			delete (*i);
		}
}

/* ------------------ Initialization/ low level calculation ---------------------- */

/* delete all the information in the clothoid */
void CElementaryPath::clearClothoid()
{
	setCurvLength(0, 0, 0);
	m_costToNode = 0; m_costToFinish = 0;
	m_atFinish = false;
	m_reverse = false;
	m_explored_resolution = PATH_UNEXPLORED;

	m_clothoidPoints.clear();
}

/* fill in the clothoid points based on the parameters for curvature and length
   in the current object */
void CElementaryPath::calculateClothoid(pose2 startPose)
{
	pose2 point(startPose), pointCopy;

	m_clothoidPoints.clear();
	/* save the start point */
	m_clothoidPoints.push_back(point);

	/* if we're driving in reverse, changing the start pose to be in the exact
	   opposite direction is the same thing as driving backwards */
	if (m_reverse)
		point.rotateInPlace(M_PI);

	double s, cosTheta, sinTheta;
	double prevCosTheta = cos(point.ang);
	double prevSinTheta = sin(point.ang);
	double prevCurv = m_initCurv, curCurv;
	int segmentPoints = (int)round(m_length/PATH_SECTION_LENGTH) + 1;

	// iterate along trajectory calculating curvature, ang, x and y
	for (int j=1; j<segmentPoints; j++)
	{
		s = j * PATH_SECTION_LENGTH;
		
		curCurv = getCurvature(s);

		// Use trapezoidal numerical integration
		// theta(s) = /int^s_0 k(t) dt
		point.rotateInPlace( (prevCurv + curCurv) * 
			PATH_SECTION_LENGTH * 0.5 );
 
		cosTheta = cos(point.ang);
		sinTheta = sin(point.ang);
		// x(s) = /int^s_0 cos /theta (t) dt
		point.x +=  (cosTheta + prevCosTheta) * PATH_SECTION_LENGTH * 0.5;
		// y(s) = /int^s_0 sin /theta (t) dt
		point.y +=  (sinTheta + prevSinTheta) * PATH_SECTION_LENGTH * 0.5;

		prevCurv = curCurv;
		prevCosTheta = cosTheta;
		prevSinTheta = sinTheta;

		/* flip the point back around to point in the same direction as alice
		   before saving if we're going in reverse (points for the running 
		   calculation are still pointing backwards) */
		pointCopy = point;
		if (m_reverse)
			pointCopy.rotateInPlace(M_PI);
		appendPoint(pointCopy);
	}
	return;
}


/* ---------------- Clothoid Creation: Analytic clothoids -------------------- */
/* Dave knowles functions to analytically connect a pair or points with a clothoid */


/* append to the current clothoid a series of clothoids which connect in a smooth way
   and arrive at the specified location. This also checks for whether a clothoid 
   is legal and discards it if its not, and sets finalClothoid to point the clothoid
   which actually arrives at the specified pose */
bool CElementaryPath::connectToTarget(pose2 targetPose, bool in_reverse,
	CElementaryPath **finalClothoid)
{
	CElementaryPath *path = NULL, *bielPath;
	// start from the end of this clothoid
	pose2 startPose = getLastPoint();
	pose2 bielStartPose;

	/* my new clothoids dont necessarily have 0 curvature at the end points, but
	   dave knowles analytic solution assumes 0 curvature at the end points, so 
	   first we have to zero the curvature (the zeroing the steering angle) or else
	   our clothoids will have a discontinuity in curvature and steering angle */
	if ( fabs(m_finalCurv) < 10e-4 )
	{
		// if the steering angle is already 0, we can 
		// just use dave knowles algorithm
		bielStartPose = startPose;
	}
	else	// append a simple path which just takes the curvature to 0 over some distance
	{
		path = new CElementaryPath(m_aliceEnv, m_finalCurv, 0,
			fabs(DISTANCE_TO_ZERO_CURVATURE * m_finalCurv /
			PATH_MAX_DYN_FEASIBLE_CURVATURE), in_reverse, startPose);
		bielStartPose = path->getLastPoint();
	}

	pose2 d = targetPose - bielStartPose;
	double beta = d.argument();

	// now connect the point with 0 steering angle to the goal with dave knowles 
	// analytic solution.
	// check if the poses happen to be symmetric
	if (fabs( startPose.ang + targetPose.ang - (2 * beta))
			< PATH_EPS_POSES_SYMMETRIC)
		bielPath = generateSymmetricClothoid(bielStartPose, targetPose, in_reverse);
	else
		bielPath = generateBielementaryClothoid(bielStartPose, targetPose, in_reverse);

	if ( bielPath != NULL )
	{
		// if we had to zero the steering angle, append our searched solution to 
		// the end of that; otherwise just start from where we are
		if ( path != NULL )
			path->appendPath(bielPath);
		else
			path = bielPath;
	
		if ( path->areClothoidsLegal() )
		{
			// if the clothoids are legal, append them to the current node
			appendPath(path);
			if ( finalClothoid != NULL )
				*finalClothoid = path->getLastClothoid();
			return true;
		}
	}
	if ( path != NULL )
		delete path;

	*finalClothoid = NULL;
/*	cout << "search failed from point: ";
	bielStartPose.print();
	cout << "to point: ";
	targetPose.print();*/
	return false;
}


/* fill in the clothoidPoints with a bielementary clothoid between the
   start and finish pose. This allocates memory for the new clothoid
   TODO: maybe this should be a constructor? */
CElementaryPath *CElementaryPath::generateBielementaryClothoid(pose2 startPose, 
	pose2 finishPose, bool in_reverse)
{
	
	// try to find a bielementary path from p1 to p2
	pose2 diff, sum, q, minIntermediatePose;	// q is the intermediate pose
	double deflection;	// alpha
	double beta;
	CElementaryPath *path1 = NULL, *path2 = NULL;

	diff = finishPose - startPose;	// diff = displacement between poses
	sum = finishPose + startPose;
	deflection = diff.ang;
	
	// calculation is simple if poses happen to be parallel
	if ( fabs(deflection) < PATH_EPS_POSES_PARALLEL ) // parallel postures 
	{
		// the intermediate posture is at the midpoint of p1 and p2
		minIntermediatePose = sum / 2;
		// angle of the line between the start and finish poses
		beta = diff.argument();
		// theta1 - beta = - (theta2 - beta) from Kanayama =>
		// => theta2 = beta - (theta1 - beta) = 2 * beta - theta1
		minIntermediatePose.setAngle(2 * beta - startPose.ang);
	}
	else
	{
		// from Kanayama this circle is the locus of the 
		// intermediate posture
		pose2 centre;
		
		double c = 1.0 / tan(deflection / 2); 
		centre.x = (sum.x - c * diff.y)/2;
		centre.y = (sum.y + c * diff.x)/2;
		pose2 diffFinishCenter = finishPose - centre;
		pose2 diffStartCenter = startPose - centre;
		pose2 d;
		double radius = diffFinishCenter.magnitude();

		// Kanayama uses Newton-Raphson iteration to minimise curvature
		// by varying q. For the moment just iterate around correct bit 
		// of circle.
		// parameterize q in terms of angle from centre of circle
		double minCost = 10000000.0;
		double start = diffStartCenter.argument();
		double finish = diffFinishCenter.argument();
		double increment =
			sign(deflection) * PATH_INTERMEDIATE_INTERVAL / radius;
		bool intermediateFound = false; 
		double new_s1, new_s2, new_l1, new_l2;

		// Iterate around circle
		for (double angle = unwrap_angle(start + increment); 
		     inbetween(angle,start,finish,increment<0);
		     angle = angle + increment )
		{
			d.y = radius * sin(angle);
			d.x = radius * cos(angle);
			q = centre + d;
			beta = (q - startPose).argument();
			q.setAngle( 2 * beta - startPose.ang );
		
			findSigmaL(startPose, q, in_reverse, new_s1, new_l1);
			findSigmaL(q, finishPose, in_reverse, new_s2, new_l2);

			// select the pair of clothoids with the lowest total curvature as 
			// the best bielementary clothoid
			double cost = 0.5 * ( (new_s1 * new_l1) + (new_s2 * new_l2) );
			
			if (cost < minCost && 2 * (new_l1 + new_l2) < PATH_MAX_SEARCH_LENGTH)
			{
				intermediateFound = true; 
				minCost = cost;
				minIntermediatePose = q;
			}
		}
		if (!intermediateFound)
		{
			// cout << "BielPath: failure- no intermediate" << endl;
			return NULL;
		}
	}

	path1 = generateSymmetricClothoid(startPose,minIntermediatePose, in_reverse);
	path2 = generateSymmetricClothoid(minIntermediatePose,finishPose, in_reverse);

	if (path1 == NULL || path2 == NULL)
	{
		return NULL;
		// cout << "BielPath: failure- generating symmetric clothoids failed" << endl;
	}

	CElementaryPath *midpath = path1->getLastClothoid();
	midpath->appendPath(path2);

	/* some combination of poses cause complete failure of the method,
	   so check that the the 2 clothoids are actually continuous and
	   that we actually ended up where we want to be */
	if ( (midpath->getLastPoint() - path2->getFirstPoint()).magnitude() > 
		PATH_POINTS_CLOSE_ENOUGH ||
	     ( (path2->getLastClothoid())->getLastPoint() - finishPose).magnitude() >
		PATH_POINTS_CLOSE_ENOUGH )
	{
		//cout << "Failed to find biel path: path doesn't reach end point (this is ok"
		//	" as long as it doesn't happen too much)" << endl;
		// cout << "BielPath: failure- paths don't match up" << endl;
		delete path1;
		return NULL;
	}
/*	cout << "start = ";
	startPose.display();
	cout << "finish = ";
	finishPose.display();
	cout << "q = ";
	q.display();
	cout << "path1: sigma: " << path1->sigma << " length: " 
		<< path1->m_length << endl;
	cout << "path2: sigma: " << path2->sigma << " length: " 
		<< path2->m_length << endl;
	ofstream log("tmp_clothoids.dat");
	path1->printAllClothoids(&log); */
	return path1;
}




/* fill in the clothoidPoints with a symmetric clothoid between the 
   start and finish pose. This now actually generates a path with 2 elementsNOTE: this allocates memory
   TODO: maybe this should be a constructor? */
CElementaryPath *CElementaryPath::generateSymmetricClothoid(pose2 startPose, 
	pose2 finishPose, bool in_reverse)
{
	double sigma, new_l;
	findSigmaL(startPose, finishPose, in_reverse, sigma, new_l);

	CElementaryPath *path1 = new CElementaryPath(m_aliceEnv, 0, sigma * new_l / 2,
		new_l / 2, in_reverse, startPose);
	CElementaryPath *path2 = new CElementaryPath(m_aliceEnv, sigma * new_l / 2, 0,
		new_l / 2, in_reverse, path1->getLastPoint());

	path1->appendPath(path2);

	return path1;
}

/* --------------------- Clothoid Creation: copy clothoids --------------------- */

/* append to the current clothoid a copy of the specified clothoid which is rotated
   and translated to connect to the current clothoid in a smooth manner. This also
   checks for whether a clothoid is legal and discards it if its not */
bool CElementaryPath::connectClothoidCopy(CElementaryPath *clothoidToCopy,
	bool in_reverse)
{
	CElementaryPath *path = 
		copyClothoid(getLastPoint(), clothoidToCopy, in_reverse);
	// check that the trajectory doesn't leave the allowed driving area.
	// since these are precomputed clothoids, we'll assume they aren't too sharp
	if ( path != NULL && path -> isClothoidLegal() )
	{
		appendPath(path);
		return true;
	}
	if ( path != NULL)
		delete path;
	return false;
}

/* copy the given clothoid so that it starts at the specified position and 
   orientation. NOTE: this allocates memory */
CElementaryPath *CElementaryPath::copyClothoid(pose2 startPose,
	CElementaryPath *clothoidToCopy, bool in_reverse)
{
	if ( clothoidToCopy == NULL )
		return NULL;
	CElementaryPath *new_clothoid = new CElementaryPath(m_aliceEnv);
	
	new_clothoid->setCurvLength(clothoidToCopy->m_initCurv, 
		clothoidToCopy->m_finalCurv, clothoidToCopy->m_length);
	new_clothoid->m_reverse = in_reverse;

	pose2 translation = startPose - clothoidToCopy->getFirstPoint();

	if (clothoidToCopy->m_clothoidPoints.size() > 0)
		for (vector<pose2>::iterator i =
			clothoidToCopy->m_clothoidPoints.begin();
			i != clothoidToCopy->m_clothoidPoints.end(); i++)
		{
			pose2 pt = *i;
			pt.translate(translation);
			double rotation = translation.ang;
			if (in_reverse)
				rotation = unwrap_angle(translation.ang + M_PI);
			pt.rotateAroundPointPreserveAngle(translation, rotation);
			pt.rotateInPlace(translation.ang);
			new_clothoid->appendPoint(pt);
		}
	return new_clothoid;
}


/* --------------------- Accessors: retrieving information --------------------- */

/* return the maximum magnitude at any point along the clothoid */
double CElementaryPath::getMaxCurvature()
{
	return max(fabs(m_initCurv), fabs(m_finalCurv));
}

/* return the last clothoid in a series of linked clothoids- this assumes that each
   clothoid has only 1 child (its intended to be used on searched path, in which case
   this is true) */
CElementaryPath *CElementaryPath::getLastClothoid()
{
	if ( m_searchChildren.empty() )
		return this;
	else
		return (m_searchChildren.front())->getLastClothoid();

}

// recursively check clothoid and all children for legality. return true only if all are legal
bool CElementaryPath::areClothoidsLegal()
{
	if ( !isClothoidLegal() )
		return false;
        if (m_searchChildren.size() > 0)
		for (vector<CElementaryPath*>::iterator i = m_searchChildren.begin();
				i != m_searchChildren.end(); i++)
			if ( !((*i)->areClothoidsLegal()) )
				return false;
	// this clothoid and all its children are legal
	return true;
}


/* check whether a clothoid is in the legal driving zone and whether it is dynamically feasible */
bool CElementaryPath::isClothoidLegal()
{
	return ( m_aliceEnv->isPathLegal(&m_clothoidPoints) 
		&& getMaxCurvature() <= (PATH_MAX_DYN_FEASIBLE_CURVATURE + 10e-3) );
		// add a small amount to the allowed curvature so that rounding errors don't
		// cause you to elimate otherwise good paths

/*	// if the start point is outside of the corridor, then the corridor check will cause s1planner
	// to fail, so just don't even do a corridor check
    if(m_aliceEnv->isPointLegal(m_aliceEnv->getStartPoint()))
		return ( m_aliceEnv->isPathLegal(&m_clothoidPoints) 
			&& getMaxCurvature() <= PATH_MAX_DYN_FEASIBLE_CURVATURE);
	else
		return true; */
}

/* return the first point in the clothoid and optionally the curvature at that point */
pose2 CElementaryPath::getFirstPoint(double *initCurv) 
{
	if ( initCurv != NULL )
		*initCurv = m_initCurv;
	return m_clothoidPoints.front();
}

/* return the last point in the clothoid and optionally the curvature at that point */
pose2 CElementaryPath::getLastPoint(double *finalCurv)
{
	if ( finalCurv != NULL )
		*finalCurv = m_finalCurv;
	return m_clothoidPoints.back();
}


/* return the estemated total path cost (cost to end of node + heuristic) */
double CElementaryPath::getCost()
{
	if (m_atFinish)
		return m_costToNode;
	else
		return m_costToNode + m_costToFinish;
}

/* return the curvature at a point at position along the clothoid
   (position measured in meters from the beginning of the clothoid) */
double CElementaryPath::getCurvature(double position)
{
	return position * (m_finalCurv - m_initCurv) / m_length + m_initCurv;
}

/* next two functions are for dumping information about the internal data
   structure- for debugging ONLY! they will probably slow the program down alot */
void CElementaryPath::printClothoid(ostream *f)
{
        if (m_clothoidPoints.size() > 0)
                for (vector<pose2>::iterator i = m_clothoidPoints.begin(); 
			i != m_clothoidPoints.end(); i++)
                {
                        i->print(f);
                }
        return;
}

void CElementaryPath::printAllClothoids(ostream *f)
{
        printClothoid(f);
        if (m_searchChildren.size() > 0)
		for (vector<CElementaryPath*>::iterator i = m_searchChildren.begin();
			i != m_searchChildren.end(); i++)
		{
			(*i)->printAllClothoids(f);
		}
        return;
}


/* ------------------ Accessors: setting information ------------------------ */

/* append the specified path to the end of this one, and also make sure the
   reverse pointer is correct and set the cost */
void CElementaryPath::appendPath(CElementaryPath *path)
{
	path->setCost(m_costToNode);
	m_searchChildren.push_back(path);
	path->setParent(this);
}

/* add a point to this clothoid */
void CElementaryPath::appendPoint(pose2 point)
{
	m_clothoidPoints.push_back(point);
}

/* delete the link between the current node and its parent (it no longer
   points to parent, parent no longer points to it) */
void CElementaryPath::severLink(CElementaryPath *childToSever)
{
	vector<CElementaryPath *>::iterator i = m_searchChildren.begin();
	while ( (*i) != childToSever )
	{
		if (i == m_searchChildren.end() )
		{
			cout << "ERROR: node to sever isn't a child of this node. "
				<< "You called this with the wrong argument" << endl;
			break;
		}
		i++;
	}

	m_searchChildren.erase(i);
	childToSever->setParent(NULL);
}

/* delete all the points in the clothoid between indexes indL (inclusive) and indH (exclusive) */
void CElementaryPath::deleteClothoidPoints(int indL, int indH)
{
	if ( !(indL == 0 || indH == (int)m_clothoidPoints.size()) )
	{
		cout << "ERROR: you need to delete a contiguous set of points "
			<< "including either the beginning or the end of the clothoid "
			<< "ie, you can't only delete points in the middle. You aren't "
			<< "calling this function correctly. exiting." << endl;
		exit(1);
	}

	// error checking on the indeces
	if ( indL < 0 || indL > ((int)m_clothoidPoints.size())
		|| indH < 0 || indH > ((int)m_clothoidPoints.size())
		|| indH < indL )
	{
		cout << "ERROR: Cannot delete clothoid points- indices to delete "
			<< "are outside of the clothoid or lowerIndex > higherIndex"
			<< endl;
		return;
	}

	int ind = indL;
	for (vector<pose2>::iterator i = m_clothoidPoints.begin() + indL;
		ind < indH && i != m_clothoidPoints.end(); ind++)
		m_clothoidPoints.erase(i);	// this also effectively shifts the
						// iterator to the next point

	// now we have to update some of the clothoid parameters to reflect that this
	// clothoid has been shortened
	double new_length;
	new_length = m_clothoidPoints.size() * PATH_SECTION_LENGTH;
	// adjust the inital curvature
	if ( indL == 0 ) 	// we deleted from the beginning
		setInitCurv(getCurvature(m_length - new_length));
	else 			// we deleted from the end
		setFinalCurv(getCurvature(new_length));
	m_length = new_length;
}

/* set the parent of this node to be null */
void CElementaryPath::setParent(CElementaryPath *parent)
{
	m_searchParent = parent;
}

/* set the inital curvature, final curvature and length */
void CElementaryPath::setCurvLength(double initCurv, double finalCurv, double l)
{
	setInitCurv(initCurv);
	setFinalCurv(finalCurv);
	m_length = l;
}

/* set the initial or final curvature- errors in this (particularly nan's) will probably cause a seg
   fault later, so this accessor allows for error checking and generating a core dump when the
   error occurs, rather than when it causes a problem */
void CElementaryPath::setInitCurv(double new_initCurv)
{
	if ( isnan(new_initCurv) )
	{
		cout << "setInitCurv: ERROR: new inital curvature is a NAN. THIS IS WRONG." << endl;
		cout << "Aborting: look at the core file to determine what went wrong" << endl;
		abort();
	}
	m_initCurv = new_initCurv;

}

void CElementaryPath::setFinalCurv(double new_finalCurv)
{
	if ( isnan(new_finalCurv) )
	{
		cout << "setFinalCurv: ERROR: new final curvature is a NAN. THIS IS WRONG." << endl;
		cout << "Aborting: look at the core file to determine what went wrong" << endl;
		abort();
	}
	m_finalCurv = new_finalCurv;
}

/* mark the finish nodes as no longer at the finish-- used when the goal location changed
   return a list of nodes that where simply connected to the goal before (ie, search had
   succeeded on them), so that search can be re-run on them */
vector<CElementaryPath *> CElementaryPath::setNodesAsNotAtGoal()
{
	/* we want to note the nodes that used to be connected to the finish 
	   so that we can re-run search on them */
	vector<CElementaryPath *> wasAtFinish, tmpPaths;
	if ( m_atFinish )
	{
		m_atFinish = false;
		if ( m_searchParent != NULL )
			wasAtFinish.push_back(m_searchParent);
	}
	if ( !m_searchChildren.empty() )
		for (vector<CElementaryPath*>::iterator i = m_searchChildren.begin();
			i != m_searchChildren.end(); i++)
		{
			tmpPaths = (*i)->setNodesAsNotAtGoal();
			if ( !tmpPaths.empty() )
				wasAtFinish.insert(
					wasAtFinish.end(),
					tmpPaths.begin(), tmpPaths.end());
		}
	return wasAtFinish;
}

/* set the cost to this node (argument is the cost to the end of the previous 
   node, not the total cost (which is the cost to the end of the node 
   + the heuristic ) */
void CElementaryPath::setCost(double prevNodeCost)
{
//	the real cost calculation
	m_costToNode = prevNodeCost + 
		(m_aliceEnv->getPathCost(&m_clothoidPoints) * PATH_SECTION_LENGTH);
//	temporary one for testing
//	m_costToNode = prevNodeCost + m_length;
	// get the estimated cost from the end of this clothoid to the finish
	m_costToFinish = m_aliceEnv->getHeuristicToGoal(getLastPoint());
	/* recursively set the cost to all the children */
        if (m_searchChildren.size() > 0)
		for (vector<CElementaryPath*>::iterator i = m_searchChildren.begin();
			i != m_searchChildren.end(); i++)
		{
			(*i)->setCost(m_costToNode);
		}
}

/* --------------------- Tree search ------------------------ */
/* functions to perform the graph search on our clothoid tree- these determine
   where is the next best place to look for a solution */

/* find the unexplored node with the lowest estimated cost */
CElementaryPath *CElementaryPath::findLowestCost(int resolution)
{
	bool alreadyExplored = !(m_explored_resolution < resolution);
	if (m_atFinish)
		return NULL;
	// if the node hasn't already been explored, then mark it as 
	// the node with the lowest current cost. otherwise set
	// that there is currently no node with the lowest cost.
	double lowestCost = alreadyExplored ? 1000000 : getCost();
	CElementaryPath *lowestCostPath = alreadyExplored ? NULL : this;
	// check all the decendents to see if one has a lower estimated total cost than
	// this node
        if (m_searchChildren.size() > 0)
		for (vector<CElementaryPath*>::iterator i = m_searchChildren.begin();
			i != m_searchChildren.end(); i++)
		{
			CElementaryPath *next = (*i)->findLowestCost(resolution);
			if ( (next != NULL) && (next->getCost() < lowestCost) )
			{
				lowestCost = next->getCost();
				lowestCostPath = next;
			}
		}

	return lowestCostPath;
}




