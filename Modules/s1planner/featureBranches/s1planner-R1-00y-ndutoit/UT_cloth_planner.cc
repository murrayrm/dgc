/**********************************************************
 **
 **  UT_CLOTH-PLANNER.CC
 **
 **
 **    Author: Noel du Toit
 **    Created: Thu Jul 26 16:38:57 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "Environment.hh"
#include <ocpspecs/OCPtSpecs.hh>
#include <frames/point2.hh>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>

using namespace std;

void setOCPinitCond()
{
  // set min and max speed to curr segment min/max speed (from mdf)
  m_ocpParams.parameters[VMIN_IDX_P] = 0;
  m_ocpParams.parameters[VMAX_IDX_P] = 10;

  // populate the initial conditions - lower bound
  m_ocpParams.initialConditionLB[EASTING_IDX_C] = 0;
  m_ocpParams.initialConditionLB[NORTHING_IDX_C] = 0;
  m_ocpParams.initialConditionLB[VELOCITY_IDX_C] = 0;
  m_ocpParams.initialConditionLB[HEADING_IDX_C] = 0;
  m_ocpParams.initialConditionLB[ACCELERATION_IDX_C] = 0;
  m_ocpParams.initialConditionLB[STEERING_IDX_C] = 0;

  // populate the initial conditions - upper bound
  m_ocpParams.initialConditionUB[EASTING_IDX_C] = 0;
  m_ocpParams.initialConditionUB[NORTHING_IDX_C] = 0;
  m_ocpParams.initialConditionUB[VELOCITY_IDX_C] = 0;
  m_ocpParams.initialConditionUB[HEADING_IDX_C] = 0;
  m_ocpParams.initialConditionUB[ACCELERATION_IDX_C] = 0;
  m_ocpParams.initialConditionUB[STEERING_IDX_C] = 0;

  // initialize to fwd mode by default
  m_ocpParams.mode = (int)md_FWD;
}

void setOCPfinalCond()
{
  // populate the initial conditions - lower bound
  m_ocpParams.finalConditionLB[EASTING_IDX_C] = -10;
  m_ocpParams.finalConditionLB[NORTHING_IDX_C] = 5;
  m_ocpParams.finalConditionLB[VELOCITY_IDX_C] = 0;
  m_ocpParams.finalConditionLB[HEADING_IDX_C] = -M_PI;
  m_ocpParams.finalConditionLB[ACCELERATION_IDX_C] = 0;
  m_ocpParams.finalConditionLB[STEERING_IDX_C] = 0;

  // populate the initial conditions - upper bound
  m_ocpParams.finalConditionUB[EASTING_IDX_C] = -10;
  m_ocpParams.finalConditionUB[NORTHING_IDX_C] = 5;
  m_ocpParams.finalConditionUB[VELOCITY_IDX_C] = 0;
  m_ocpParams.finalConditionUB[HEADING_IDX_C] = -M_PI;
  m_ocpParams.finalConditionUB[ACCELERATION_IDX_C] = 0;
  m_ocpParams.finalConditionUB[STEERING_IDX_C] = 0;
}

void generatePolyCorridor()
{  
  bool isLegal = true;
  
  //You should always have right and left boundary
  if (m_polylines.size() > 1 ) { 
	int Ngates=m_polylines[0].size();
	CPolytope** rawPoly;
	rawPoly=new CPolytope*[Ngates-1];
	point2arr leftBound(m_polylines[0]);
	point2arr rightBound(m_polylines[1]);
	if ((leftBound.size() > 1) && (rightBound.size() > 1)) {
	  if (0 != m_polyCorridor) {
		delete[] m_polyCorridor;  //clearing the previous corridor
	  }

	  m_nPolytopes = Ngates-1;
	  m_polyCorridor = new CPolytope [m_nPolytopes]; //creating the empty corridor
	  int NofDim = 2;
	  CAlgebraicGeometry* algGeom = new CAlgebraicGeometry();
	  int NofVtx = 4;
	  double*** vertices;
	  vertices = new double**[m_nPolytopes];
	  for(int k=0; k<Ngates-1;k++) {
		vertices[k] = new double*[NofDim];
		for(int i=0;i<NofDim;i++) {
		  vertices[k][i] = new double[NofVtx];
		}
	  }
	  int i = 0 ;
	  while( i<m_nPolytopes) {
		vertices[i][0][1]=leftBound[i].x;
		vertices[i][1][1]=leftBound[i].y;
		vertices[i][0][2]=leftBound[i+1].x;
		vertices[i][1][2]=leftBound[i+1].y;
		vertices[i][0][3]=rightBound[i+1].x;
		vertices[i][1][3]=rightBound[i+1].y;
		vertices[i][0][0]=rightBound[i].x;
		vertices[i][1][0]=rightBound[i].y;
		rawPoly[i] = new CPolytope( &NofDim, &NofVtx, (const double** const)  vertices[i] ); //Ask Melvin: is this Correct?
		
		ALGErrorType algErr = algGeom->VertexAndFacetEnumeration(rawPoly[i]);
		m_polyCorridor[i] = new CPolytope( &NofDim, &NofVtx, (const double** const)  vertices[i] ); //Ask Melvin: is this Correct?
		algErr = algGeom->VertexAndFacetEnumeration(&m_polyCorridor[i]);
        
		if( algErr != alg_NoError) {
		  cout << "CORRIDOR: error when building raw polytopes" << endl;
		  isLegal = false;
		}
		int nCompVtx = rawPoly[i]->getNofVertices() ;
		if( nCompVtx != 4) {
		  cout << "CORRIDOR: The gates: " << i << "," << i+1 << " are degenerate " << endl;
		  cout << i <<  "L: ("<<leftBound[i].x<< ","<< leftBound[i].y << "), " <<  "R: ("<<rightBound[i].x<< ","<< rightBound[i].y << "), " <<  endl;
		  cout << i+1 <<  "L: ("<<leftBound[i+1].x<< ","<< leftBound[i+1].y << "), " <<  "R: ("<<rightBound[i+1].x<< ","<< rightBound[i+1].y << "), " <<  endl;
		  cout << rawPoly[i] << endl;
		}
		i++;
	  }
	  for(int i=0; i<Ngates-1; i++) {
		for(int j=0; j<NofDim; j++)
		  delete[] vertices[i][j];
		delete[] vertices[i];
	  }
	  delete[] vertices;
	  for(int i=0; i<m_nPolytopes; i++) {
		delete rawPoly[i];
	  }
	  delete[] rawPoly;
	  delete algGeom;
	} else {
	  isLegal = false;
	  cout <<"CORRIDOR: FAILED - leftBound.size()< 2,rightBound.size()< 2"<<endl; 
	} 
  } else { 
	isLegal = false;
	cout <<"CORRIDOR: FAILED - m_polylines.size()< 2"<<endl; 
  }
  if(isLegal)
	m_polyCorridorLegal = true;
  else {   
	Log::getStream(1) << "CORRIDOR: FAILED - Corridor generation " << endl;
	m_polyCorridorLegal = false;
  }
}

void display(int sendSubgroup)
{
  CMapElementTalker meTalker;
  int counter=12000;
  vector<point2> points;
  point2 start, end;
  MapId mapId;
  MapElement me;
  
  mapId = counter;

  // display the corridor
  for (int i=0; i<m_polylines[0].arr.size() < i++) {
	points.push_back(m_polylines[0].arr[i]);
  }
  for (int i=0; i<m_polylines[1].arr.size() < i++) {
	points.push_back(m_polylines[1].arr[i]);
  }
  points.push_back(m_polylines[0],arr[0]);
  me.setId(mapId);
  me.setTypeLine();
  me.setColor(MAP_COLOR_DARK_GREEN, 100);
  me.setGeometry(points);
  meTalker.sendMapElement(&me,sendSubgroup);

  // print the initial and final cond's
  start.set(m_ocpParams.initialConditionUB[NORTHING_IDX_C], m_ocpParams.initialConditionUB[EASTING_IDX_C]);
  mapId = 12001;
  me.setId(mapId);
  me.setTypeInitCond();
  me.setGeometry(start);
  meTalker.sendMapElement(&me,sendSubgroup);
  end.set(m_ocpParams.finalConditionUB[NORTHING_IDX_C], m_ocpParams.finalConditionUB[EASTING_IDX_C]);
  mapId = 12002;
  me.setId(mapId);
  me.setTypeFinalCond();
  me.setGeometry(end);
  meTalker.sendMapElement(&me,sendSubgroup);
}

int main(int argc, char **args)
{
  int sn_key = skynet_findkey(argc, args);
  OCPparams m_ocpParams();
  setOCPinitCond();
  setOCPfinalCond();
  
  // define the poly corridor
  vector<point2arr> m_polylines;
  point2arr line;
  point2 pt1, pt2;
  // left boundary
  pt1.set(-15, 7.5);
  pt2.set(15, 7.5);
  line(pt1, pt2);
  pt1.set(-15, 7.5);
  pt2.set( 15, 7.5);
  line(pt1, pt2);
  // right boundary
  pt1.set(-15, -2.5);
  pt2.set( 15, -2.5);
  line(pt1, pt2);
  generatePolyCorridor();

  // display the corridor
  display(10);
}


