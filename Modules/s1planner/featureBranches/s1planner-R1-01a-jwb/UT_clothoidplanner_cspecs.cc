/**********************************************************
 **
 **  UT_CLOTH-PLANNER.CC
 **
 **
 **    Author: Noel du Toit
 **    Created: Thu Jul 26 16:38:57 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include <ocpspecs/OCPtSpecs.hh>
#include <frames/point2.hh>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>
#include "interfaces/TpDpInterface.hh"
#include "bitmap/Polygon.hh"
#include "bitmap/BitmapParams.hh"
#include "tempClothoidInterface.hh"

// Error handling
#define MSG(fmt, ...) \
  (fprintf(stdout, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
using namespace std;

struct PolygonParams
{
  float centerlaneVal;
  float obsCost;
  
  PolygonParams()
  {
    centerlaneVal = 0;
    obsCost = 10000;
  }
};


void setOCPparams(OCPparams& m_OCPparams, double velMax, double velMin, point2 initPos, double initHeading, double initVel, double initAcc, double initSteer, int mode, point2 finPos, double finHeading, double finVel, double finAcc, double finSteer)
{
  // set min and max speed to curr segment min/max speed (from mdf)
  m_OCPparams.parameters[VMIN_IDX_P] = velMin;
  m_OCPparams.parameters[VMAX_IDX_P] = velMax;

  // populate the initial conditions - lower bound
  m_OCPparams.initialConditionLB[EASTING_IDX_C] = initPos.y;
  m_OCPparams.initialConditionLB[NORTHING_IDX_C] = initPos.x;
  m_OCPparams.initialConditionLB[VELOCITY_IDX_C] = initVel;
  m_OCPparams.initialConditionLB[HEADING_IDX_C] = initHeading;
  m_OCPparams.initialConditionLB[ACCELERATION_IDX_C] = initAcc;
  m_OCPparams.initialConditionLB[STEERING_IDX_C] = initSteer;

  // populate the initial conditions - upper bound
  m_OCPparams.initialConditionUB[EASTING_IDX_C] = initPos.y;
  m_OCPparams.initialConditionUB[NORTHING_IDX_C] = initPos.x;
  m_OCPparams.initialConditionUB[VELOCITY_IDX_C] = initVel;
  m_OCPparams.initialConditionUB[HEADING_IDX_C] = initHeading;
  m_OCPparams.initialConditionUB[ACCELERATION_IDX_C] = initAcc;
  m_OCPparams.initialConditionUB[STEERING_IDX_C] = initSteer;

  // initialize to fwd mode by default
  m_OCPparams.mode = (int)md_FWD;

  // populate the initial conditions - lower bound
  m_OCPparams.finalConditionLB[EASTING_IDX_C] = finPos.y;
  m_OCPparams.finalConditionLB[NORTHING_IDX_C] = finPos.x;
  m_OCPparams.finalConditionLB[VELOCITY_IDX_C] = finVel;
  m_OCPparams.finalConditionLB[HEADING_IDX_C] = finHeading;
  m_OCPparams.finalConditionLB[ACCELERATION_IDX_C] = finAcc;
  m_OCPparams.finalConditionLB[STEERING_IDX_C] = finSteer;

  // populate the initial conditions - upper bound
  m_OCPparams.finalConditionUB[EASTING_IDX_C] = finPos.y;
  m_OCPparams.finalConditionUB[NORTHING_IDX_C] = finPos.x;
  m_OCPparams.finalConditionUB[VELOCITY_IDX_C] = finVel;
  m_OCPparams.finalConditionUB[HEADING_IDX_C] = finHeading;
  m_OCPparams.finalConditionUB[ACCELERATION_IDX_C] = finAcc;
  m_OCPparams.finalConditionUB[STEERING_IDX_C] = finSteer;
}

void generatePolyCorridor(CPolytope*& m_polyCorridor, int& m_nPolytopes, vector<point2arr> m_polylines)
{  
  bool isLegal = true;
  bool m_polyCorridorLegal;
  //You should always have right and left boundary
  if (m_polylines.size() > 1 ) { 
    int Ngates=m_polylines[0].size();
    CPolytope** rawPoly;
    rawPoly=new CPolytope*[Ngates-1];
    point2arr leftBound(m_polylines[0]);
    point2arr rightBound(m_polylines[1]);
    if ((leftBound.size() > 1) && (rightBound.size() > 1)) {
      m_nPolytopes = Ngates-1;
      m_polyCorridor = new CPolytope[m_nPolytopes]; //creating the empty corridor
      int NofDim = 2;
      CAlgebraicGeometry* algGeom = new CAlgebraicGeometry();
      int NofVtx = 4;
      double*** vertices;
      vertices = new double**[m_nPolytopes];
      for(int k=0; k<Ngates-1;k++) {
        vertices[k] = new double*[NofDim];
        for(int i=0;i<NofDim;i++) {
          vertices[k][i] = new double[NofVtx];
        }
      }
      int i = 0 ;
      while( i<m_nPolytopes) {
        vertices[i][0][1]=leftBound[i].x;
        vertices[i][1][1]=leftBound[i].y;
        vertices[i][0][2]=leftBound[i+1].x;
        vertices[i][1][2]=leftBound[i+1].y;
        vertices[i][0][3]=rightBound[i+1].x;
        vertices[i][1][3]=rightBound[i+1].y;
        vertices[i][0][0]=rightBound[i].x;
        vertices[i][1][0]=rightBound[i].y;
        rawPoly[i] = new CPolytope( &NofDim, &NofVtx, (const double** const)  vertices[i] ); //Ask Melvin: is this Correct?
		
        ALGErrorType algErr = algGeom->VertexAndFacetEnumeration(rawPoly[i]);
        m_polyCorridor[i] = new CPolytope( &NofDim, &NofVtx, (const double** const)  vertices[i] ); //Ask Melvin: is this Correct?
        algErr = algGeom->VertexAndFacetEnumeration(&m_polyCorridor[i]);
        
        if( algErr != alg_NoError) {
          cout << "CORRIDOR: error when building raw polytopes" << endl;
          isLegal = false;
        }
        int nCompVtx = rawPoly[i]->getNofVertices() ;
        if( nCompVtx != 4) {
          cout << "CORRIDOR: The gates: " << i << "," << i+1 << " are degenerate " << endl;
          cout << i <<  "L: ("<<leftBound[i].x<< ","<< leftBound[i].y << "), " <<  "R: ("<<rightBound[i].x<< ","<< rightBound[i].y << "), " <<  endl;
          cout << i+1 <<  "L: ("<<leftBound[i+1].x<< ","<< leftBound[i+1].y << "), " <<  "R: ("<<rightBound[i+1].x<< ","<< rightBound[i+1].y << "), " <<  endl;
          cout << rawPoly[i] << endl;
        }
        i++;
      }
      for(int i=0; i<Ngates-1; i++) {
        for(int j=0; j<NofDim; j++)
          delete[] vertices[i][j];
        delete[] vertices[i];
      }
      delete[] vertices;
      for(int i=0; i<m_nPolytopes; i++) {
        delete rawPoly[i];
      }
      delete[] rawPoly;
      delete algGeom;
    } else {
      isLegal = false;
      cout <<"CORRIDOR: FAILED - leftBound.size()< 2,rightBound.size()< 2"<<endl; 
    } 
  } else { 
    isLegal = false;
    cout <<"CORRIDOR: FAILED - m_polylines.size()< 2"<<endl; 
  }
  if(isLegal)
    m_polyCorridorLegal = true;
  else {   
    cout << "CORRIDOR: FAILED - Corridor generation " << endl;
    m_polyCorridorLegal = false;
  }
}

void convertLaneToPolygon(vector<Polygon>& polygons, point2arr& leftbound, 
                                    point2arr &rightbound, float bval, float cval)
{
  bval = sqrt(bval);
  cval = sqrt(cval);

  if (leftbound.size() <= 1 || rightbound.size() <= 1) {
    cerr << "ERROR: " <<  __FILE__ << ":" << __LINE__ << " leftbound.size() = " 
         << leftbound.size() << " rightbound.size() = " << rightbound.size() << endl;
    return;
  }

  unsigned leftboundInd = 0;
  unsigned rightboundInd = 0;
  unsigned lastLeftInd = 0;
  unsigned lastRightInd = 0;
  bool useLeft = true;

  while (leftboundInd < leftbound.size() - 1 || rightboundInd < rightbound.size() - 1) {
    if (leftboundInd >= leftbound.size() || rightboundInd >= rightbound.size() ){
      cerr << "ERROR: " <<  __FILE__ << ":" << __LINE__ << "leftboundInd = " 
           << leftboundInd << " leftbound.size() = " << leftbound.size()
           << " rightboundInd = " << rightboundInd << " rightbound.size() = " 
           << rightbound.size() << endl;
      return;
    }

    vector<float> cost1, cost2;
    point2arr_uncertain vertices1;
    point2arr_uncertain vertices2;

    if (leftboundInd == leftbound.size() - 1)
      useLeft = false;
    else if (rightboundInd == rightbound.size() - 1)
      useLeft = true;
    else if (leftbound.size() == rightbound.size() && 
             leftboundInd > rightboundInd) {
      useLeft = false;
    }
    else if (leftbound.size() == rightbound.size() && 
             leftboundInd < rightboundInd) {
      useLeft = true;
    }
    else {
      float leftDist = leftbound[lastLeftInd].dist(leftbound[leftboundInd + 1]);
      float rightDist = rightbound[lastRightInd].dist(rightbound[rightboundInd + 1]);
      if (leftDist < rightDist)
        useLeft = true;
      else
        useLeft = false;
    }

    if (useLeft) {
      lastRightInd = rightboundInd;
      // Trapezoid
      vertices1.push_back(leftbound[leftboundInd]);
      cost1.push_back(bval);
      vertices1.push_back((leftbound[leftboundInd] + rightbound[rightboundInd])/2);
      cost1.push_back(cval);
      vertices1.push_back((leftbound[leftboundInd + 1] + rightbound[rightboundInd])/2);
      cost1.push_back(cval);
      vertices1.push_back(leftbound[leftboundInd + 1]);
      cost1.push_back(bval);

      // Triangle
      vertices2.push_back((leftbound[leftboundInd] + rightbound[rightboundInd])/2);
      cost2.push_back(cval);
      vertices2.push_back(rightbound[rightboundInd]);
      cost2.push_back(bval);
      vertices2.push_back((leftbound[leftboundInd + 1] + rightbound[rightboundInd])/2);
      cost2.push_back(cval);

      leftboundInd++;
    }
    else {
      lastLeftInd = leftboundInd;

      // Triangle
      vertices1.push_back(leftbound[leftboundInd]);
      cost1.push_back(bval);
      vertices1.push_back((leftbound[leftboundInd] + rightbound[rightboundInd])/2);
      cost1.push_back(cval);
      vertices1.push_back((leftbound[leftboundInd] + rightbound[rightboundInd + 1])/2);
      cost1.push_back(cval);

      // Trapezoid
      vertices2.push_back((leftbound[leftboundInd] + rightbound[rightboundInd])/2);
      cost2.push_back(cval);
      vertices2.push_back(rightbound[rightboundInd]);
      cost2.push_back(bval);
      vertices2.push_back(rightbound[rightboundInd + 1]);
      cost2.push_back(bval);
      vertices2.push_back((leftbound[leftboundInd] + rightbound[rightboundInd + 1])/2);
      cost2.push_back(cval);

      rightboundInd++;
    }

    Polygon tmpPoly;
    tmpPoly.setVertices(vertices1, cost1);
    tmpPoly.setFillFunc(FILL_SQUARE);
    tmpPoly.setCombFunc(COMB_REPLACE);
    polygons.push_back(tmpPoly);

    tmpPoly.setVertices(vertices2, cost2);
    polygons.push_back(tmpPoly);
  }
}

void getBitmapParams(BitmapParams& bmparams, PolygonParams &polygonParams,
                     point2 alicePos, vector<point2arr> m_polylines)
{
  bmparams.centerX = alicePos.x;
  bmparams.centerY = alicePos.y;

  bmparams.polygons.clear();
  point2arr leftptarr, rightptarr;
  leftptarr.set(m_polylines[0]);
  rightptarr.set(m_polylines[1]);

  convertLaneToPolygon(bmparams.polygons, leftptarr, rightptarr, 
                       polygonParams.centerlaneVal, polygonParams.centerlaneVal);
}

void display(int sn_key, int sendSubgroup, vector<point2arr> m_polylines, OCPparams m_OCPparams, BitmapParams m_bmparams)
{
  CMapElementTalker meTalker;
  meTalker.initSendMapElement(sn_key);
  int counter=12000;
  vector<point2> points;
  point2 point;
  MapId mapId;
  MapElement me;
  
  mapId = counter;

  // display the corridor
  for (int i=0; i<m_polylines[0].arr.size() ; i++) {
    points.push_back(m_polylines[0].arr[i]);
  }
  for (int i=m_polylines[1].arr.size()-1; i>=0 ; i--) {
    points.push_back(m_polylines[1].arr[i]);
  }
  points.push_back(m_polylines[0].arr[0]);
  me.setId(mapId);
  me.setTypeLine();
  me.setColor(MAP_COLOR_DARK_GREEN, 100);
  me.setGeometry(points);
  meTalker.sendMapElement(&me,sendSubgroup);
  points.clear();
  
  // print the initial and final cond's
  point.set(m_OCPparams.initialConditionUB[NORTHING_IDX_C], m_OCPparams.initialConditionUB[EASTING_IDX_C]);
  points.push_back(point);
  point.set(m_OCPparams.finalConditionUB[NORTHING_IDX_C], m_OCPparams.finalConditionUB[EASTING_IDX_C]);
  points.push_back(point);
  mapId = 12001;
  me.setId(mapId);
  me.setTypeLine();
  me.setColor(MAP_COLOR_BLUE, 100);
  me.setGeometry(points);
  meTalker.sendMapElement(&me,sendSubgroup);

  // send the costmap too
  // Instantiate a talker
  SkynetTalker< BitmapParams > polyTalker(sn_key, SNbitmapParams, 
                                          MODtrafficplanner);
  polyTalker.send(&m_bmparams);


}

void print(vector<point2arr> m_polylines, OCPparams m_OCPparams, CPolytope* m_polyCorridor)
{
  MSG("Init Pos = (%6.2f,%6.2f)",m_OCPparams.initialConditionLB[NORTHING_IDX_C], m_OCPparams.initialConditionLB[EASTING_IDX_C]);
  MSG("Fin Pos = (%6.2f,%6.2f)",m_OCPparams.finalConditionLB[NORTHING_IDX_C], m_OCPparams.finalConditionLB[EASTING_IDX_C]);
  cout << "polycorridor:" << endl << m_polyCorridor << endl;
}

int main(int argc, char **args)
{
  int m_skynetKey = skynet_findkey(argc, args);
  
  // set up ocp params problem
  OCPparams m_OCPparams = OCPparams();
  point2 initPos, finPos;
  double velMax, velMin, initHeading, initVel, initAcc, initSteer;
  int mode;
  double finVel, finHeading, finAcc, finSteer;

  initPos.set(-10,0);
  velMax=10;
  velMin=0;
  initHeading = 0;
  initVel = 0;
  initAcc = 0;
  initSteer = 0;
  mode = 1;
  finPos.set(10,0);
  finHeading = 0;
  finVel = 0;
  finAcc = 0;
  finSteer = 0;
  setOCPparams(m_OCPparams, velMax, velMin, initPos, initHeading, initVel, initAcc, initSteer, mode, finPos, finHeading, finVel, finAcc, finSteer);

  // define the poly corridor
  vector<point2arr> m_polylines;
  // do stuff to m_polylines here
  point2arr line;
  point2 pt1, pt2;
  // left boundary
  pt1.set(-15, 7.5);
  pt2.set(15, 7.5);
  line.push_back(pt1);
  line.push_back(pt2);
  m_polylines.push_back(line);
  line.clear();

  // right boundary
  pt1.set(-15, -2.5);
  pt2.set( 15, -2.5);
  line.push_back(pt1);
  line.push_back(pt2);
  m_polylines.push_back(line);
  line.clear();

  CPolytope* m_polyCorridor;
  int m_nPolytopes;
  generatePolyCorridor(m_polyCorridor, m_nPolytopes, m_polylines);

  // define the cost map
  BitmapParams m_bmparams;
  PolygonParams m_polygonParams;
  m_bmparams.resX = 0.1;
  m_bmparams.resY = 0.1;
  m_bmparams.width =800;
  m_bmparams.height = 800;
  m_bmparams.baseVal = 100 ;
  m_bmparams.outOfBounds = 200;
  m_polygonParams.centerlaneVal = 1;
  m_polygonParams.obsCost = 1000;
  getBitmapParams(m_bmparams, m_polygonParams, initPos, m_polylines);
  
  // some debug info ...
  if (1) {
    // print corridor
    print(m_polylines, m_OCPparams, m_polyCorridor);
    
    // display the corridor
    display(m_skynetKey, 10, m_polylines, m_OCPparams, m_bmparams);
  }

  // construct the OCPtSpecs object and populate the variables
  CEnvironment* m_ocpSpecs;
  //  OCPtSpecs(sn_key, useAstate, verbose, useCostMap, useObst, useParams, useRDDF, usePolyCorridor, showCostmap, computeGradient, showGradient);
  //m_ocpSpecs = new OCPtSpecs(m_skynetKey, false, 0, false, false, false, false, false, true, false, false);
  m_ocpSpecs = new CEnvironment(m_skynetKey, true);  

  m_ocpSpecs->setOCPspecs(m_OCPparams, m_polyCorridor, m_bmparams);
  
  //CPolytope* tmpPolyCorridor;
  //OCPparams tmpOCPparams;
  //  m_ocpSpecs->getParams(&tmpOCPparams);
  // m_ocpSpecs->getPolytopes(tmpPolyCorridor);
  // print(m_polylines, tmpOCPparams, tmpPolyCorridor);

  GenerateTrajOneShot(m_ocpSpecs, NULL, 0.5);  

}


