/*
 *  Stage1Planner_Main.cpp
 *  Stage1Planner
 *
 *  Created by Noel duToit
 *  2/28/07
 */

//#include <conio.h>


#include "Stage1Planner.hh"
//#include "clothoidplanner/ClothoidPlanner.hh"

using namespace std;

int main(int argc, char** const argv)
{ 
  cout << "Stage 1 Planner is starting..." << endl;
  cout << "Parsing command line..." << endl;

  gengetopt_args_info cmdline;

  if (cmdline_parser(argc, argv, &cmdline) != 0)
    exit (1);


  int SkynetKey   = skynet_findkey(argc, argv);
  bool WAIT_STATE =  true;
  
  
  /*! Create a instance of dplanner */

    CStage1Planner* Stage1Planner = new CStage1Planner(SkynetKey, WAIT_STATE, cmdline);

  

  cout << "entering activeloop" << endl;
  Stage1Planner->ActiveLoop();

//  cout << "Creating clothoidplanner object" << endl;
//  CClothoidPlanner* ClothoidPlanner = new CClothoidPlanner(SkynetKey, WAIT_STATE);
//  ClothoidPlanner->ActiveLoop();
  
  cout << "Stage 1 Planner is ending." << endl;
  
  /* Restore memory resources */
//  delete Stage1Planner;
//  delete ClothoidPlanner;
  
  return (int) 0;
} 
