/*
 * ClothoidPlanner.cc
 * Main methods and framework for the Clothoid implementation of the
 * Stage 1 planner
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 1 May 07- Kenny Oslund
	- Created file
 *
 */


// temporary function until we can get a real velocity planner



#include "ClothoidPlanner.hh"
void generateVelocityProfile(CEnvironment *m_aliceEnv, vector<pose2> path, vector<double> *velocityProfile, vector<double> *accelerationProfile);


CClothoidPlanner::CClothoidPlanner(CEnvironment *env, VehicleState *state)
{
	m_aliceEnv = env;
	m_aliceState = state;

	m_searchTreeBase = NULL;
	initialize();
	/* build the clothoid db to use in explore */
	precomputeClothoids();

	return;

}

// initialize everything to an empty search tree- this is used on startup and
// if the actual position gets too far from the previously generated trajectory
void CClothoidPlanner::initialize()
{
	// delete any already existing search tree
	if ( m_searchTreeBase != NULL )
		delete m_searchTreeBase;

	// Set up some variables
	// set best cost to a rediculuously large number so the first path we search
	// will be lower in cost
	bestFinishCost = 100000;
	cur_resolution = 0;
	m_bestFinish = NULL;

	// initialize the search tree
	m_searchTreeBase = new CElementaryPath(m_aliceEnv);
	setStartPoint();
	search(m_searchTreeBase);

	// debugging info
	cout << "Search Tree initialized and starting at (x,y,theta): ";
	m_searchTreeBase->printClothoid();
	cout << "Going to end point: ";
	(m_aliceEnv->getEndPoint()).display();
	cout << "Cost Heuristic to end: " << m_searchTreeBase->getCost() << endl;


}


bool CClothoidPlanner::generatePaths(double runTime, S1PlanMode planMode)
{
	cout << "in generatePaths" << endl;
	m_aliceEnv->printPolyCorridor();


	/* TODO: update the tree. There are 3 things that can happen between planning cycles
	   which can cause us to have to update the tree:
	   * bad trajectory following:
		- alice isn't following the trajectory, so our current start possition isn't
		on the tree that we've built to search over
		=> need to completely scrap the tree and restart planning from where we are
	   * start point progesses along graph
		- this is what we hope will happen- alice has traveled down the trajectory that
		we've made.
		=> need to delete all the paths and their decendants that originate from a node 
		behind us, delete any paths that we've traversed complete and gone beyond the
		end of, update the pose in m_searchTreeBase, update the child paths of
		m_searchTreeBase to point to whatever paths are now directly ahead of us, and 
		update the parant node of those to point to us.
	   * goal point has moved
		- we got a new goal
		=> need to delete all the goal points, update costs, and rerun search on all 
		the nodes that were directly connected to the old finish.
	   This is very annoying to code (particular the 2nd one), so for now just plan from
		scratch every time and hope that we find a similar solution each time
	*/
	initialize();

	cur_resolution = 0;
	CElementaryPath *lowestF;
	SearchStatus status;
	pose2 startPoint = m_aliceEnv->getStartPoint();
	double distStartGoal = (m_aliceEnv->getEndPoint() - startPoint).magnitude();
	pose2 clothoidEndPoint;
	double distToClothoidEndPoint;

	double timeTaken = 0, startTime = (double)clock() / (double)CLOCKS_PER_SEC;
	int iterations = 0;

	#define NUM_ITERATIONS		5
	//for (int i = 0; i < NUM_ITERATIONS; i++)
	while ( timeTaken < runTime )
	{
		lowestF = m_searchTreeBase->findLowestCost(cur_resolution);

		if ( lowestF != NULL )
		{
			// if we've explored until the end of the lowest cost clothoid is further away 
			// from us than the goal, don't explore any more
			clothoidEndPoint = lowestF->getLastPoint();
			distToClothoidEndPoint = (clothoidEndPoint - startPoint).magnitude();
			if ( distToClothoidEndPoint > distStartGoal )
			{
				lowestF->explored_resolution = PATH_TOO_DISTANT_TO_EXPLORE;
				cout << "Clothoid further away from start than goal is; not exploring" << endl;
			}
			else
			{
//				cout << "Found lowestF of " << lowestF->getCost() << " ending at: ";
//				(lowestF->getLastPoint()).display();
				explore(lowestF);
				status = search(lowestF);
			}
		}
		else
		{
			if ( cur_resolution < CLOTHOID_MAX_RESOLUTION - 1 )
			{
				cout << "No more nodes to explore at this resolution; increasing resolution" << endl;
				cur_resolution++;
			}
			else
				break;
		}
		timeTaken = ((double)clock() / (double)CLOCKS_PER_SEC) - startTime;
		iterations++;
	}

	cout << "Completed " << iterations << " iterations in " << timeTaken << " seconds." << endl;

//	ofstream log("tmp_clothoids.dat");
//	m_searchTreeBase->printAllClothoids(&log);


	return true;
}

#ifdef NEVER
some various bits of old test code


	//CElementaryPath *path = new CElementaryPath(m_aliceEnv, .08, 5, false, pose2(0,0,M_PI_2 / 2));
//	CElementaryPath *path = new //CElementaryPath();
//	path->generateBielementaryClothoid(pose2(0,0,0), pose2(-10,10,-M_PI / 4), true);
//	search(m_searchTreeBase, true);
//	m_searchTreeBase->connectClothoidCopy(
//		m_pathDB[0][0]);
//m_searchTreeBase.connectToFinish(pose2(10,10,M_PI / 4));

	explore(m_searchTreeBase, true);

	vector<CElementaryPath *> nodes = m_searchTreeBase->getSearchChildren();
        for (vector<CElementaryPath*>::iterator i = nodes.begin();
			i != nodes.end(); i++)
		search(*i);



#endif



void CClothoidPlanner::precomputeClothoids()
{
//	ofstream precomp_log("precomputed_clothoids.dat");

	double new_sigma, new_length;

	cout << "S1Planner is precomputing clothoid paths..." << endl;
	for (int j=0; j<CLOTHOID_DB_NUM_LENGTHS; j++)
		for (int i=0; i<CLOTHOID_DB_NUM_SIGMAS; i++)
		{
			// we want the clothoids to be clustered more around
			// low curvature, so use a quadratic distribution of
			// curvatures, rather than a linear one.
			new_sigma = sqrt(CLOTHOID_DB_MAX_SIGMA + (double)j * CLOTHOID_DB_SIGMA_ADJUSTMENT) *
				( 1.0 - (2.0 * (double)i /
				(CLOTHOID_DB_NUM_SIGMAS - 1)) );
			new_sigma *= sign(new_sigma) * new_sigma;
			new_length = CLOTHOID_DB_MAX_LENGTH *
				( 1.0 - ((double)j / CLOTHOID_DB_NUM_LENGTHS) );

			m_pathDB[j][i] = 	
				new CElementaryPath(NULL, new_sigma, new_length);

//			m_pathDB[j][i]->printClothoid(&precomp_log);
			//cout << "max Curvature: " << m_pathDB[j][i]->getMaxCurvature() << endl;
		}

	cout << "Precomputing complete. " << endl;
	return;
}


SearchStatus CClothoidPlanner::search(CElementaryPath *m_nodeToSearch,
	bool in_reverse)
{
	SearchStatus status;
	status = m_nodeToSearch->connectToTarget(m_aliceEnv->getEndPoint(),
		in_reverse);

	if ( status == search_simple_path )
	{
		CElementaryPath *new_path = 
			(m_nodeToSearch->getSearchChildren()).back();
		new_path -> atFinish = true;
		// save this node as the best finish if its cost is less than
		// the previous best finish
		// TODO: may want to include some penalty for choosing a 
		// different path to end, so alternative path must be better by
		// x amount, not just slightly better. this would look like
		// if (new_path->getCost() + X < bestFinishCost)
		// where X is a constant cost penalty for choosing a different 
		// solution
		if ( new_path->getCost() < bestFinishCost )
		{
			m_bestFinish = new_path;
			bestFinishCost = new_path->getCost();
		}
	}
	else if ( status == search_biel_path )
	{
		CElementaryPath *new_path1 = 
			(m_nodeToSearch->getSearchChildren()).back();
		CElementaryPath *new_path2 = 
			(new_path1->getSearchChildren()).front();
		new_path2 -> atFinish = true;
		// save this node as the best finish if its cost is less than
		// the previous best finish
		if ( new_path2->getCost() < bestFinishCost )
		{
			m_bestFinish = new_path2;
			bestFinishCost = new_path2->getCost();
		}
	}
	return status;
}


bool CClothoidPlanner::explore(CElementaryPath *m_nodeToExplore, bool in_reverse)
{

	for (int i=0; i<CLOTHOID_DB_NUM_SIGMAS; i++)
	{
		m_nodeToExplore->connectClothoidCopy(m_pathDB[cur_resolution][i], in_reverse);
		/* mark the node as explored at this resolution */
		m_nodeToExplore->explored_resolution = cur_resolution;
	}

	return true;
}


void CClothoidPlanner::setStartPoint()
{
	m_searchTreeBase->clearClothoid();
	m_searchTreeBase->appendPoint(m_aliceEnv->getStartPoint());
	m_searchTreeBase->setCost(0);	// no previous nodes
	//if(m_searchTreeBase->isClothoidLegal())
	if(m_aliceEnv->isPointLegal(m_aliceEnv->getStartPoint()))
		cout << "Start Point IS legal" << endl;
	else
		cout << "Start Point is NOT Legal" << endl;
}

/* convert the lowest cost branch of the clothoid tree into a traj and
   return it. generatePaths must have been called before this */
CTraj *CClothoidPlanner::getTraj()
{
	CTraj *m_Traj = new CTraj(3);
        /* Initialize the trajectory */
        m_Traj->startDataInput();

	vector<pose2> clothoid = getBestPath();
	vector<double> velocityProfile, accelerationProfile;
	generateVelocityProfile(m_aliceEnv, clothoid, &velocityProfile, &accelerationProfile);

	pose2 Pt, nextPt, diff;
	double vx, vy, dvx, dvy;
	int j = 0;

        if ( !clothoid.empty() )
	{
                for (vector<pose2>::iterator i = clothoid.begin(); 
			i != (clothoid.end() - 1); i++, j++)
                {
			Pt = (*i);
			nextPt = *(i + 1);
			diff = nextPt - Pt;
			vx = velocityProfile[j] * diff.x / diff.magnitude();
			vy = velocityProfile[j] * diff.y / diff.magnitude();
			dvx = accelerationProfile[j] * diff.x / diff.magnitude();
			dvy = accelerationProfile[j] * diff.y / diff.magnitude();
                        m_Traj->addPoint(i->x, vx, dvx, i->y, vy, dvy);
                }

		// use the same components of the derivative as the 2nd to last point to get the direction 
		// for last point
		pose2 lastPt = clothoid.back();
		vx = velocityProfile.back() * diff.x / diff.magnitude();
		vy = velocityProfile.back() * diff.y / diff.magnitude();
		dvx = accelerationProfile[j] * diff.x / diff.magnitude();
		dvy = accelerationProfile[j] * diff.y / diff.magnitude();
		m_Traj->addPoint(lastPt.x, vx, dvx, lastPt.y, vy, dvy);
	}
	else
		cout << "ERROR: Failed to generate a trajectory" << endl;

//	ofstream traj("traj_clothoids.dat");
//	m_Traj->print(traj);
	return m_Traj;
}

vector<pose2> CClothoidPlanner::getBestPath()
{
	ofstream log("best_clothoids.dat");
	
	vector<CElementaryPath *> bestClothoids;
	vector<pose2> bestPath, clothoid;

	bestClothoids = getBestClothoidSequence();

        if ( !bestClothoids.empty() )
                for (vector<CElementaryPath *>::iterator i = bestClothoids.begin(); 
                        i != bestClothoids.end(); i++)
		{
			clothoid = (*i)->getClothoidPoints();
			(*i)->printClothoid(&log);
			if ( !clothoid.empty() )
				for (vector<pose2>::iterator j = clothoid.begin(); 
					j != clothoid.end(); j++)
				{
					bestPath.push_back(*j);
				}
		}

	return bestPath;

}

vector<CElementaryPath *> CClothoidPlanner::getBestClothoidSequence()
{
	CElementaryPath *m_endNode;
	vector<CElementaryPath *> bestPath, bestPathRev;

	// if we reached the goal, use the lowest cost goal node. otherwise
	// just plan to the node with the lowest cost estimate
	if ( m_bestFinish != NULL )	// solution to goal found
		m_endNode = m_bestFinish;
	else
		m_endNode = m_searchTreeBase->findLowestCost(CLOTHOID_MAX_RESOLUTION);

	// start at the endpoint that we've already determined is the best,
	// then just iterate from the end back to the beginning
	for ( CElementaryPath *node = m_endNode; node != NULL; 
			node = node->getSearchParent() )
		bestPathRev.push_back(node);

	// the last node in bestPath is our starting node, and the only point in its 
	// clothoid is also in the preceding node, so just delete it since its redundant
	bestPathRev.pop_back();

	// now the clothoids are in the opposite order they need to be put 
	// into the traj, so reverse the order
	cout << "num clothoids in solution: " << bestPathRev.size() << endl;
        if ( !bestPathRev.empty() )
                for (vector<CElementaryPath *>::reverse_iterator i = bestPathRev.rbegin(); 
                        i != bestPathRev.rend(); i++)
                {
			bestPath.push_back(*i);
                }
	return bestPath;
}


// generate a velocity and acceleration profile associated with each point in path
// the input argument is the vector of points in the path, and the function needs to fill in
// velocityProfile and accelerationProfile
void generateVelocityProfile(CEnvironment *m_aliceEnv, vector<pose2> path, vector<double> *velocityProfile, vector<double> *accelerationProfile)
{
	// the number of points that need to be in the velocity and acceleration profiles
	int numPoints = path.size();
	// upper and lower bounds on the velicty- these are initialized by the function call bellow
	double initVlb, initVub, finalVlb, finalVub;
	// the average initial and final velocity constraints from OCPtspecs, and also fill in the upper and lower bounds:
	double initV = m_aliceEnv->getInitialVelocity(&initVlb, &initVub);
	double finalV = m_aliceEnv->getGoalVelocity(&finalVlb, &finalVub);

//	cout << "initial vel: lb: " << initVlb << " average: " << initV << " ub: " << initVub << endl;
//	cout << "final vel: lb: " << finalVlb << " average: " << finalV << " ub: " << finalVub << endl;

	// nominally the distance between points is 0.2m, but it can vary (particularly when clothoids are joined)
	// if you want the actual distance between clothoid i and i+1, do something like this:
        if ( !path.empty() )
	{
                for (vector<pose2>::iterator i = path.begin(); 
			i != (path.end() - 1); i++)
		{
			double distance = ((*i) - *(i + 1)).magnitude();	// distance between points i and i+1

			// ....
			// fill in more code here
			double speed = 0, accel = 0;
			velocityProfile -> push_back(speed);
			accelerationProfile -> push_back(accel);
		}

		// not that the above for loop only goes to numPoints - 1 because it uses points i and i + 1 in the same iteration, 
		// so we still need fill in the last point
		double finalSpeed = 0, finalAccel = 0;
		velocityProfile -> push_back(finalSpeed);
		accelerationProfile -> push_back(finalAccel);
	}

	return;
}

// update the start location. This includes deleting nodes that are bellow us => we will 
// not be driving on them.
void CClothoidPlanner::updateStart()
{


}

// update the finish location. This involves deleting all the finish nodes and relinking 
// them to the new finish
void CClothoidPlanner::updateEnd()
{
	pose2 newGoalPose = m_aliceEnv->getEndPoint();
	if ( (goalPose - newGoalPose).magnitude() > CLOTHOID_MAX_GOAL_CHANGE )
	{
		cout << "Goal Pose changed- updating search tree" << endl;
		goalPose = newGoalPose;
		/* delete all the goal nodes */
		// TODO -- it seems like search should be rerun on all the nodes that
		// were connected to the goal by a single elementary path (ie, all the
		// nodes that were connected to a deleted node)
		m_searchTreeBase->deleteGoalNodes();
		m_searchTreeBase->setCost(0);
		m_bestFinish = NULL;
		bestFinishCost = 1000000;
		// TODO: mark all nodes that were marked as too far away to explore as unexplored
		// since they may not be too far away any more
	}
}




