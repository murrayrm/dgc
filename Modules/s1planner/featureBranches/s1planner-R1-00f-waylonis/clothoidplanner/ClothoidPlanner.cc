/*
 * ClothoidPlanner.cc
 * Main methods and framework for the Clothoid implementation of the
 * Stage 1 planner
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 1 May 07- Kenny Oslund
	- Created file
 *
 */

#include "ClothoidPlanner.hh"


CClothoidPlanner::CClothoidPlanner(CEnvironment *env, VehicleState *state)
{
	m_aliceEnv = env;
	m_aliceState = state;

	m_searchTreeBase = NULL;
	initialize();
	/* build the clothoid db to use in explore */
	precomputeClothoids();

	return;

}

// initialize everything to an empty search tree- this is used on startup and
// if the actual position gets too far from the previously generated trajectory
void CClothoidPlanner::initialize()
{
	// delete any already existing search tree
	if ( m_searchTreeBase != NULL )
		delete m_searchTreeBase;

	// Set up some variables
	// set best cost to a rediculuously large number so the first path we search
	// will be lower in cost
	bestFinishCost = 100000;
	cur_resolution = 0;
	m_bestFinish = NULL;

	// initialize the search tree
	m_searchTreeBase = new CElementaryPath(m_aliceEnv);
	setStartPoint();
	search(m_searchTreeBase);

	// debugging info
	cout << "Search Tree initialized and starting at (x,y,theta): ";
	m_searchTreeBase->printClothoid();
	cout << "Going to end point: ";
	(m_aliceEnv->getEndPoint()).display();
	if(m_aliceEnv->isPointLegal(m_aliceEnv->getEndPoint()))
		cout << "End Point IS legal" << endl;
	else
        {
		cout << "End Point is NOT Legal" << endl;
//	        m_aliceEnv->printPolyCorridor();
        }
        cout << "Num Polytopes: " << m_aliceEnv->getNpolytopes() << endl;
	cout << "Cost Heuristic to end: " << m_searchTreeBase->getCost() << endl;


}


bool CClothoidPlanner::generatePaths(double runTime, S1PlanMode planMode)
{
	cout << "in generatePaths" << endl;
	//m_aliceEnv->printPolyCorridor();


	/* TODO: update the tree. There are 3 things that can happen between planning cycles
	   which can cause us to have to update the tree:
	   * bad trajectory following:
		- alice isn't following the trajectory, so our current start possition isn't
		on the tree that we've built to search over
		=> need to completely scrap the tree and restart planning from where we are
	   * start point progesses along graph
		- this is what we hope will happen- alice has traveled down the trajectory that
		we've made.
		=> need to delete all the paths and their decendants that originate from a node 
		behind us, delete any paths that we've traversed complete and gone beyond the
		end of, update the pose in m_searchTreeBase, update the child paths of
		m_searchTreeBase to point to whatever paths are now directly ahead of us, and 
		update the parant node of those to point to us.
	   * goal point has moved
		- we got a new goal
		=> need to delete all the goal points, update costs, and rerun search on all 
		the nodes that were directly connected to the old finish.
	   This is very annoying to code (particular the 2nd one), so for now just plan from
		scratch every time and hope that we find a similar solution each time
	*/
	initialize();

	cur_resolution = 0;
	CElementaryPath *lowestF;
	SearchStatus status;
	pose2 startPoint = m_aliceEnv->getStartPoint();
	double distStartGoal = (m_aliceEnv->getEndPoint() - startPoint).magnitude();
	pose2 clothoidEndPoint;
	double distToClothoidEndPoint;

	unsigned long long dgcTime, dgcDiffTime, dgcStartTime = DGCgettime();
	double timeTaken = 0; 
	int iterations = 0;

	#define NUM_ITERATIONS		5
	//for (int i = 0; i < NUM_ITERATIONS; i++)
	while ( timeTaken < runTime )
	{
		lowestF = m_searchTreeBase->findLowestCost(cur_resolution);

		if ( lowestF != NULL )
		{
			// if we've explored until the end of the lowest cost clothoid is further away 
			// from us than the goal, don't explore any more
			clothoidEndPoint = lowestF->getLastPoint();
/*			distToClothoidEndPoint = (clothoidEndPoint - startPoint).magnitude();
			if ( distToClothoidEndPoint > distStartGoal )
			{
				lowestF->explored_resolution = PATH_TOO_DISTANT_TO_EXPLORE;
			}
			else
			{ */
				explore(lowestF);
				status = search(lowestF);
//			}
		}
		else
		{
			if ( cur_resolution < CLOTHOID_MAX_RESOLUTION - 1 )
			{
				cout << "No more nodes to explore at this resolution; increasing resolution" << endl;
				cur_resolution++;
			}
			else
				break;
		}
		dgcTime = DGCgettime();
		dgcDiffTime = (unsigned long long)(dgcTime-dgcStartTime);
		timeTaken = DGCtimetosec(dgcDiffTime, false);
		iterations++;
	}

	cout << "Completed " << iterations << " iterations in " << timeTaken << " seconds." << endl;

//	ofstream log("tmp_clothoids.dat");
//	m_searchTreeBase->printAllClothoids(&log);


	return true;
}

#ifdef NEVER
some various bits of old test code


	//CElementaryPath *path = new CElementaryPath(m_aliceEnv, .08, 5, false, pose2(0,0,M_PI_2 / 2));
//	CElementaryPath *path = new //CElementaryPath();
//	path->generateBielementaryClothoid(pose2(0,0,0), pose2(-10,10,-M_PI / 4), true);
//	search(m_searchTreeBase, true);
//	m_searchTreeBase->connectClothoidCopy(
//		m_pathDB[0][0]);
//m_searchTreeBase.connectToFinish(pose2(10,10,M_PI / 4));

	explore(m_searchTreeBase, true);

	vector<CElementaryPath *> nodes = m_searchTreeBase->getSearchChildren();
        for (vector<CElementaryPath*>::iterator i = nodes.begin();
			i != nodes.end(); i++)
		search(*i);



#endif



void CClothoidPlanner::precomputeClothoids()
{
//	ofstream precomp_log("precomputed_clothoids.dat");

	double new_sigma, new_length;

	cout << "S1Planner is precomputing clothoid paths..." << endl;
	for (int j=0; j<CLOTHOID_DB_NUM_LENGTHS; j++)
		for (int i=0; i<CLOTHOID_DB_NUM_SIGMAS; i++)
		{
			// we want the clothoids to be clustered more around
			// low curvature, so use a quadratic distribution of
			// curvatures, rather than a linear one.
			new_sigma = sqrt(CLOTHOID_DB_MAX_SIGMA + (double)j * CLOTHOID_DB_SIGMA_ADJUSTMENT) *
				( 1.0 - (2.0 * (double)i /
				(CLOTHOID_DB_NUM_SIGMAS - 1)) );
			new_sigma *= sign(new_sigma) * new_sigma;
			new_length = CLOTHOID_DB_MAX_LENGTH *
				( 1.0 - ((double)j / CLOTHOID_DB_NUM_LENGTHS) );

			m_pathDB[j][i] = 	
				new CElementaryPath(NULL, new_sigma, new_length);

//			m_pathDB[j][i]->printClothoid(&precomp_log);
			//cout << "max Curvature: " << m_pathDB[j][i]->getMaxCurvature() << endl;
		}

	cout << "Precomputing complete. " << endl;
	return;
}


SearchStatus CClothoidPlanner::search(CElementaryPath *m_nodeToSearch,
	bool in_reverse)
{
	SearchStatus status;
	status = m_nodeToSearch->connectToTarget(m_aliceEnv->getEndPoint(),
		in_reverse);

	if ( status == search_simple_path )
	{
		CElementaryPath *new_path = 
			(m_nodeToSearch->getSearchChildren()).back();
		new_path -> atFinish = true;
		// save this node as the best finish if its cost is less than
		// the previous best finish
		// TODO: may want to include some penalty for choosing a 
		// different path to end, so alternative path must be better by
		// x amount, not just slightly better. this would look like
		// if (new_path->getCost() + X < bestFinishCost)
		// where X is a constant cost penalty for choosing a different 
		// solution
		if ( new_path->getCost() < bestFinishCost )
		{
			m_bestFinish = new_path;
			bestFinishCost = new_path->getCost();
		}
	}
	else if ( status == search_biel_path )
	{
		CElementaryPath *new_path1 = 
			(m_nodeToSearch->getSearchChildren()).back();
		CElementaryPath *new_path2 = 
			(new_path1->getSearchChildren()).front();
		new_path2 -> atFinish = true;
		// save this node as the best finish if its cost is less than
		// the previous best finish
		if ( new_path2->getCost() < bestFinishCost )
		{
			m_bestFinish = new_path2;
			bestFinishCost = new_path2->getCost();
		}
	}
	return status;
}


bool CClothoidPlanner::explore(CElementaryPath *m_nodeToExplore, bool in_reverse)
{

	for (int i=0; i<CLOTHOID_DB_NUM_SIGMAS; i++)
	{
		if (m_nodeToExplore->connectClothoidCopy(m_pathDB[cur_resolution][i], in_reverse))
		{
			// if no nodes so far have reached the finish, and one of the clothoids that we just
			// layed down is closer to the finish than any other best end node then save it as
			// the new best end node.
			// note that we leave the cost at its (high) initial value so that if we find any
			// node that reaches the end, we'll choose it over this one. this entire check
			// is just to make sure we find something in the event that our goal is outside
			// of the legal driving region
			CElementaryPath *new_clothoid = (m_nodeToExplore->getSearchChildren()).back();
			if ( m_bestFinish == NULL || (!m_bestFinish->atFinish
				&& m_aliceEnv->getPointHeuristic(new_clothoid->getLastPoint()) 
					< m_aliceEnv->getPointHeuristic(m_bestFinish->getLastPoint())) )
				m_bestFinish = new_clothoid;
		}
	}

	/* mark the node as explored at this resolution */
	m_nodeToExplore->explored_resolution = cur_resolution;


	return true;
}


void CClothoidPlanner::setStartPoint()
{
	m_searchTreeBase->clearClothoid();
	m_searchTreeBase->appendPoint(m_aliceEnv->getStartPoint());
	m_searchTreeBase->setCost(0);	// no previous nodes
	//if(m_searchTreeBase->isClothoidLegal())
	if(m_aliceEnv->isPointLegal(m_aliceEnv->getStartPoint()))
		cout << "Start Point IS legal" << endl;
	else
		cout << "Start Point is NOT Legal" << endl;
}

/* convert the lowest cost branch of the clothoid tree into a traj and
   return it. generatePaths must have been called before this */
bool CClothoidPlanner::getTraj(CTraj *m_Traj)
{
    bool haveNewTraj = false;
	/* the object should be created outside, and initialized with a call to startDataInput
	 * it is a good practice to create and delete memory in the same function, if possible
	 * CTraj *m_Traj = new CTraj(3); */

	vector<pose2> clothoid = getBestPath();
	vector<double> velocityProfile, accelerationProfile;
	generateVelocityProfile(m_aliceEnv, clothoid, velocityProfile, accelerationProfile);

	pose2 Pt, nextPt, diff;
	double vx, vy, dvx, dvy;
	int j = 0;

        if ( !clothoid.empty() )
	{
		haveNewTraj = true;
                for (vector<pose2>::iterator i = clothoid.begin(); 
			i != (clothoid.end() - 1); i++, j++)
                {
			Pt = (*i);
			nextPt = *(i + 1);
			diff = nextPt - Pt;
			vx = velocityProfile[j] * diff.x / diff.magnitude();
			vy = velocityProfile[j] * diff.y / diff.magnitude();
			dvx = accelerationProfile[j] * diff.x / diff.magnitude();
			dvy = accelerationProfile[j] * diff.y / diff.magnitude();
			m_Traj->addPoint(i->x, vx, dvx, i->y, vy, dvy);
                }

		// use the same components of the derivative as the 2nd to last point to 
		// get the direction for last point
		pose2 lastPt = clothoid.back();
		vx = velocityProfile.back() * diff.x / diff.magnitude();
		vy = velocityProfile.back() * diff.y / diff.magnitude();
		dvx = accelerationProfile[j] * diff.x / diff.magnitude();
		dvy = accelerationProfile[j] * diff.y / diff.magnitude();
		m_Traj->addPoint(lastPt.x, vx, dvx, lastPt.y, vy, dvy);
	}
	else
	{
		cout << "ERROR: Failed to generate a trajectory" << endl;
		haveNewTraj = false;
	}
//	ofstream traj("traj_clothoids.dat");
//	m_Traj->print(traj);
	return haveNewTraj;
}

vector<pose2> CClothoidPlanner::getBestPath()
{
//	ofstream log("best_clothoids.dat");
	
	vector<CElementaryPath *> bestClothoids;
	vector<pose2> bestPath, clothoid;

	bestClothoids = getBestClothoidSequence();

        if ( !bestClothoids.empty() )
                for (vector<CElementaryPath *>::iterator i = bestClothoids.begin(); 
                        i != bestClothoids.end(); i++)
		{
			clothoid = (*i)->getClothoidPoints();
//			(*i)->printClothoid(&log);
			if ( !clothoid.empty() )
				for (vector<pose2>::iterator j = clothoid.begin(); 
					j != clothoid.end(); j++)
				{
					bestPath.push_back(*j);
				}
		}

	return bestPath;

}

vector<CElementaryPath *> CClothoidPlanner::getBestClothoidSequence()
{

	CElementaryPath *m_endNode;
	vector<CElementaryPath *> bestPath, bestPathRev;

	// if we reached the goal, use the lowest cost goal node. otherwise
	// just plan to the node with the lowest cost estimate
	if ( m_bestFinish != NULL )	// solution to goal found
		m_endNode = m_bestFinish;
	else
		m_endNode = m_searchTreeBase->findLowestCost(CLOTHOID_MAX_RESOLUTION);

	// start at the endpoint that we've already determined is the best,
	// then just iterate from the end back to the beginning
	for ( CElementaryPath *node = m_endNode; node != NULL; 
			node = node->getSearchParent() )
		bestPathRev.push_back(node);

	// the last node in bestPath is our starting node, and the only point in its 
	// clothoid is also in the preceding node, so just delete it since its redundant
	bestPathRev.pop_back();

	// now the clothoids are in the opposite order they need to be put 
	// into the traj, so reverse the order
	cout << "num clothoids in solution: " << bestPathRev.size() << endl;
        if ( !bestPathRev.empty() )
                for (vector<CElementaryPath *>::reverse_iterator i = bestPathRev.rbegin(); 
                        i != bestPathRev.rend(); i++)
                {
			bestPath.push_back(*i);
                }
	return bestPath;
}


// update the start location. This includes deleting nodes that are bellow us => we will 
// not be driving on them.
void CClothoidPlanner::updateStart()
{


}

// update the finish location. This involves deleting all the finish nodes and relinking 
// them to the new finish
void CClothoidPlanner::updateEnd()
{
	pose2 newGoalPose = m_aliceEnv->getEndPoint();
	if ( (goalPose - newGoalPose).magnitude() > CLOTHOID_MAX_GOAL_CHANGE )
	{
		cout << "Goal Pose changed- updating search tree" << endl;
		goalPose = newGoalPose;
		/* delete all the goal nodes */
		// TODO -- it seems like search should be rerun on all the nodes that
		// were connected to the goal by a single elementary path (ie, all the
		// nodes that were connected to a deleted node)
		m_searchTreeBase->deleteGoalNodes();
		m_searchTreeBase->setCost(0);
		m_bestFinish = NULL;
		bestFinishCost = 1000000;
		// TODO: mark all nodes that were marked as too far away to explore as unexplored
		// since they may not be too far away any more
	}
}


#define max_speed 30
#define turn_const 0.2
#define accel_const 2
#define decel_const 2
#define steer_const 0.1

/* TODO:
   -- get initial speed for trajectory.
*/

void CClothoidPlanner::generateVelocityProfile2(CEnvironment *m_aliceEnv, vector<pose2> path, vector<double>& vProfile, vector<double>& aProfile) {

  if (path.empty())
    return;

  vector<double> aa;
  vector<double> dist;

  vProfile.clear();
  aProfile.clear();

  // Calculate angular acceleration between points:

  aa.push_back(0);
  
  for (int i = 0; i < path.size() - 1; i++) {
    double angleDiff = path[i].ang - path[i + 1].ang;
    
    if (angleDiff > 3.14159265) {
      angleDiff -= 3.14159265 * 2;
    }
    
    if (angleDiff < -3.14159265) {
      angleDiff += 3.14159265 * 2;
    }
    
    aa.push_back(angleDiff / (sqrt((path[i + 1].x - path[i].x) * (path[i + 1].x - path[i].x) + (path[i + 1].y - path[i].y) * (path[i + 1].y - path[i].y))));
  }
  
  aa.push_back(0);

  // Set initial velocity to current velocity:
  
  double v0, v0u;
  m_aliceEnv->getInitialVelocity(&v0, &v0u);
  
  vProfile.push_back(v0);

  // Cap velocity at maximum turning rate and maximum legal speed:

  for (int i = 1; i < path.size(); i++) {
    vProfile.push_back(turn_const / (abs(aa[i]) + 0.01));
    
    if (vProfile[i] > max_speed) {
      vProfile[i] = max_speed;
    }
  }

  // Calculate distance between points:

  for (int i = 0; i < path.size() - 1; i++) {
    dist.push_back(sqrt((path[i + 1].x - path[i].x) * (path[i + 1].x - path[i].x) + (path[i + 1].y - path[i].y) * (path[i + 1].y - path[i].y)));
  }
  
  vProfile[path.size() - 1] = 0;
  
  // Cap velocity to ensure time for steering wheel turn:

  for (int i = 1; i < path.size() - 1; i++) {
    if (vProfile[i] * abs(aa[i] - aa[i + 1]) > steer_const) {
      vProfile[i] = steer_const / abs(aa[i] - aa[i + 1]);
    }
  }

  // Smooth velocity going forwards to prevent excessive acceleration:

  for (int i = 1; i < path.size() - 1; i++) {
    if (vProfile[i] > vProfile[i - 1]) {
      if (vProfile[i] > sqrt(vProfile[i - 1] * vProfile[i - 1] + 2 * sqrt(accel_const * accel_const - (vProfile[i] * aa[i]) * (vProfile[i] * aa[i])) * dist[i - 1])) {
	vProfile[i] = sqrt(vProfile[i - 1] * vProfile[i - 1] + 2 * sqrt(accel_const * accel_const - (vProfile[i] * aa[i]) * (vProfile[i] * aa[i])) * dist[i - 1]);
      }
    }
  }

  // Smooth velocity going backwards to prevent excessive deceleration:

  for (int i = path.size() - 2; i > 0; i--) {
    if (vProfile[i + 1] < vProfile[i]) {
      if (vProfile[i] > sqrt(vProfile[i + 1] * vProfile[i + 1] + 2 * sqrt(decel_const * decel_const - (vProfile[i] * aa[i]) * (vProfile[i] * aa[i])) * dist[i])) {
	vProfile[i] = sqrt(vProfile[i + 1] * vProfile[i + 1] + 2 * sqrt(decel_const * decel_const - (vProfile[i] * aa[i]) * (vProfile[i] * aa[i])) * dist[i]);
      }
    }
  }

  // Generate aProfile from vProfile:

  for (int i = 0; i < path.size() - 1; i++) {
    aProfile.push_back((vProfile[i + 1] - vProfile[i]) / dist[i]);
  }

  aProfile.push_back(0);
}


// temporary function until we can get a real velocity planner

#define S1P_V_MAX 4.0
#define S1P_A_MAX 0.6
#define S1P_A_MIN -2.0
#define VERBOSE 0 

void CClothoidPlanner::generateVelocityProfile(CEnvironment *m_aliceEnv, vector<pose2> path, vector<double>& vProfile, vector<double>& aProfile)
{  
    if (path.empty())
       return;

    double v0, v0u;
    double vf, vfu; 
    m_aliceEnv->getGoalVelocity(&vf,&vfu);
    m_aliceEnv->getInitialVelocity(&v0,&v0u);
    
    int Npoints = path.size();
    vProfile.clear();
    aProfile.clear();
    vector<double> dist;
    pose2 diff;
    for(int i=0;i<Npoints-1;i++)
    {
    	diff = path[i+1]-path[i];
        dist.push_back(diff.magnitude());
    }

    vProfile.push_back(v0) ;
    double t;
      for(int i=1; i<Npoints; i++)
    { 
       // t =S/v, approximated using Euler fwd
        t =(-vProfile[i-1]+sqrt(pow(vProfile[i-1],2)+2*S1P_A_MAX*dist[i-1]))/S1P_A_MAX;
        vProfile.push_back( min(vProfile[i-1]+S1P_A_MAX*t,S1P_V_MAX) );
    }
    vProfile[Npoints-1]=min(vf, S1P_V_MAX);
//  
//    cout << Npoints << endl;
    for(int i=Npoints-2; i>=0; i--)
    {
        t=( -vProfile[i+1]+sqrt(pow(vProfile[i+1],2)+2*fabs(S1P_A_MIN)*dist[i]) )/fabs(S1P_A_MIN);
        vProfile[i]=min(vProfile[i+1]+fabs(S1P_A_MIN)*t, vProfile[i]);
    }
  
    for(int i=0; i<Npoints-1; i++)
    {   
    	aProfile.push_back( ( pow(vProfile[i+1],2)- pow(vProfile[i],2) )/(2*dist[i]) ); 
    }
    aProfile.push_back(0.0);
    
    if(VERBOSE)
    {
	cout << "path.size()=" << path.size()<< ", "
	     << "vProfile.size()=" << vProfile.size() << ", "
	     << "aProfile.size()=" << aProfile.size() << endl;

        for(int i=1; i<Npoints; i++)
    	   cout <<i<< " d: " << dist[i] << " v: " << vProfile.at(i)<< " a: " << aProfile.at(i)<< endl;     

    }
}


