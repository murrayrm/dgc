#include "clothoidplanner/ElementaryPath.hh"
#include "frames/pose2.hh"
#include <math.h>
#include <signal.h>


volatile sig_atomic_t quit;




int main()
{

	CElementaryPath *path = new CElementaryPath(NULL);

	CElementaryPath *bielthing = 
	//	path->generateSymmetricClothoid(
		path->generateBielementaryClothoid(
		pose2(0.677684, -8.46949, -1.42223), pose2(  0.0677832, -16.5422, -1.58098 ) );
	
	ofstream log("out.log");


	bielthing->printAllClothoids(&log);

	return 0;

}