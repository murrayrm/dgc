close all;
clear all;

% plot a single clothoid or clothoid tree
figure;
dlmread out.log;
clothoids = ans;
plot(clothoids(:,1), clothoids(:,2), '.','MarkerSize',1);
xlabel('X (meters)');
ylabel('Y (meters)');
title('Clothoid Tree');
axis equal
 return;

% plot a whole series of clothoids/ the clothoid tree- results in an animation like thing
figure(1);
clf;
for i = 1:60
    figure(1);
    clf;
    file = ['ctree_' num2str(i) '.dat'];
    clothoids = dlmread(file);
    plot(clothoids(:,1), -1 *clothoids(:,2), '.', 'MarkerSize', 1);
    axis equal
    xlabel('X (meters)');
    ylabel('Y (meters)');
    title(['Clothoid Tree ' num2str(i)]);
    clear clothoids
end;

return;



figure;
dlmread best_clothoids.dat;
clothoids = ans;
plot(clothoids(:,1), -1*clothoids(:,2), '.');
xlabel('X (meters)');
ylabel('Y (meters)');
title('Clothoid Chosen based on minimizing cost function');
axis equal
