/*
 *  Stage1Planner.cc
 *  Stage1Planner
 *
 *  Created by Noel duToit
 */

#include <time.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <math.h>

#include "Stage1Planner.hh"


CStage1Planner:: CStage1Planner(int SkynetKey, bool WAIT_STATE, gengetopt_args_info cmdline)
: CSkynetContainer(MODdynamicplanner,SkynetKey),CStateClient(WAIT_STATE), CRDDFTalker(true)
{
  /*** Create various objects ***/

  cout << "Constructing CState1Planner object" << endl;

  /* Create an OCPspecs object */
   if (cmdline.display_costmap_flag == 0)
	m_pProblemSpecs = new CEnvironment(SkynetKey, false);
   else
	m_pProblemSpecs = new CEnvironment(SkynetKey, true);
   if (m_pProblemSpecs == NULL)
      cout << "ERROR: OCPtSpecs object was NOT created successfully" << endl;
 
   m_maxSpeed = cmdline.speed_limit_arg ; 
   m_maxAccel = cmdline.accel_limit_arg ; 
   m_maxBrake = cmdline.brake_limit_arg ;
   m_safeSpeed = cmdline.safe_speed_arg ;
   m_safeTurn = cmdline.safe_turn_arg ;
  /*** Initialize objects and variables ***/

  m_TrajFollowSendSocket = m_skynet.get_send_sock(SNtraj);
  m_SuperConSendSocket = m_skynet.get_send_sock(SNsuperconTrajfCmd);

  planner = new CClothoidPlanner(m_pProblemSpecs, &m_state);

  cout << "Construction Complete" << endl;
  cout<< " mS: "<< m_maxSpeed << " mA: "<< m_maxAccel << " mB: " << m_maxBrake << " sS : " << m_safeSpeed << " sT: "<<m_safeTurn << endl;
}

CStage1Planner::~CStage1Planner(void)
{
//  if(m_Traj != NULL)
//    delete m_Traj;
  if (m_pProblemSpecs != NULL)
    delete m_pProblemSpecs;
}

/* main work function- s1planner basically stays in this function the entire time
   that it runs. This loops through generating a trajectory and sending it */
void CStage1Planner::ActiveLoop(void)
{
  int numTrajs = 1;
  scCmd.commandType = tf_forwards;

  ofstream log("cycle_times.log");

  /* wait a couple of seconds so that hopefully we'll get good data on the 
     first time through */
  sleep(2);
  unsigned long long dgcTime, dgcDiffTime, dgcStartTime = DGCgettime();
  double timeTaken;
  bool haveNewTraj=false;
  pose2 initCond, finalCond;
  double initV, finalV;

  /*** The main planning loop- each time through this loop we generate 
       and send a trajectory ***/
//  for (int j= 0; j < 1; j++)
  while (1)
  {
	cout << " ***** Loop Starting ***** " << endl;
	/** Set up some variables and objects **/
	  /* eventually these will be determined from the driving environment state,
	     but for now just set them statically */
	  runtime = 0.2;

	  /* get the most recent state data */
	  UpdateState();

	  // create the instance of the ctraj object here since its better
	  // to allocate and free memory in the same function
	  m_Traj = new CTraj(3);
	  /* Initialize the trajectory */
	  m_Traj->startDataInput();

	  /* give time for ocpspecs to update */
	  usleep(100000);

	  // start to solve the problem- I think this locks variables while you're 
	  // using them
	  m_pProblemSpecs->startSolve();
	  /* initialize variables for out start and end conditions- used to check for
	     the stop condition */
	  initCond = m_pProblemSpecs->getStartPoint();
	  finalCond = m_pProblemSpecs-> getEndPoint();
	  initV = m_pProblemSpecs->getInitialVelocity();
	  finalV = m_pProblemSpecs->getGoalVelocity();


	/** main work- this is a very simple state machine to handle a few cases **/


	// ---- Stopped Condition ----
	// should probably also check whether we're done with the mission...
	if ( fabs(finalV) < 1e-3
		&& (finalCond - initCond).magnitude() < 
		    S1PLANNER_STOP_CONDITION_THREASHOLD )
	{
		scCmd.commandType = tf_forwards;
		cout << "Stop Condition Detected" << endl;
		haveNewTraj = getStoppedTraj();
		usleep(200000);
	}
	// ---- Nominal Driving condition ----
	else
	{
		/********************************/
		/*** Calculate the trajectory ***/
		/********************************/
		planner->generatePaths(runtime);
		haveNewTraj = getS1Traj();
		scCmd.commandType = planner->getDirection();

	}


	// unlock variables
	m_pProblemSpecs->endSolve();

	/** send the data by skynet... Only if we have a new Traj **/
	if(haveNewTraj)
	{
		cout << "sending traj number " << numTrajs++ << endl;
		SendTraj(m_TrajFollowSendSocket, m_Traj);
	}
	else
	   cout << "Empty new trajectory... keeping the previous one" << endl;

	#ifdef LOG_TRAJS
	// log the traj
	char file_name[30];
	sprintf(file_name, "traj_clothoids%d.dat", numTrajs);
	ofstream traj(file_name);
	m_Traj->print(traj);
	#endif

	  /* command the correct gear */
	  m_skynet.send_msg(m_SuperConSendSocket, &scCmd, sizeof(scCmd), 0);

	delete m_Traj;

	dgcTime = DGCgettime();
	dgcDiffTime = (unsigned long long)(dgcTime-dgcStartTime);
	timeTaken = DGCtimetosec(dgcDiffTime, false);
	log << "Time Taken: " << timeTaken << endl;
	dgcStartTime = dgcTime;

  }

  return;
}


// generate a trajectory which will cause the vehicle to just remain stopped in place
bool CStage1Planner::getStoppedTraj()
{
	pose2 initCond, finalCond;
	initCond = m_pProblemSpecs->getStartPoint();
	finalCond = m_pProblemSpecs-> getEndPoint();

	// the traj will have 1 points: one where the vehicle is now, and one
	// that is 10cm directly in front of the vehicle
	finalCond.x += 0.1 * cos(initCond.ang);
	finalCond.y += 0.1 * sin(initCond.ang);
	
	m_Traj->addPoint(initCond.x, 0, 0, initCond.y, 0, 0);
	m_Traj->addPoint(finalCond.x, 0, 0, finalCond.y, 0, 0);
	return true;
}

/* convert the lowest cost branch of the clothoid tree into a traj and
   return it. generatePaths must have been called before this */
bool CStage1Planner::getS1Traj()
{
	bool haveNewTraj = false;
	/* the ctraj should be created outside, and initialized with startDataInput
	 * it is a good practice to create and delete memory in the same function
	 * CTraj *m_Traj = new CTraj(3); */

	vector<pose2> clothoid = planner->getBestPath();
	vector<double> velocityProfile, accelerationProfile;
	generateVelocityProfile3(clothoid, velocityProfile, accelerationProfile);

	pose2 Pt, nextPt, diff;
	double vx, vy, dvx, dvy;
	int j = 0;

        if ( !clothoid.empty() )
	{
		haveNewTraj = true;
                for (vector<pose2>::iterator i = clothoid.begin(); 
			i != (clothoid.end() - 1); i++, j++)
                {
			Pt = (*i);
			nextPt = *(i + 1);
			diff = nextPt - Pt;
			if ( diff.magnitude() < 0.05 )
				continue;
			vx = velocityProfile[j] * diff.x / diff.magnitude();
			vy = velocityProfile[j] * diff.y / diff.magnitude();
			dvx = accelerationProfile[j] * diff.x / diff.magnitude();
			dvy = accelerationProfile[j] * diff.y / diff.magnitude();
			m_Traj->addPoint(i->x, vx, dvx, i->y, vy, dvy);
                }

		// use the same components of the derivative as the 2nd to last point to 
		// get the direction for last point
		pose2 lastPt = clothoid.back();
		vx = velocityProfile.back() * diff.x / diff.magnitude();
		vy = velocityProfile.back() * diff.y / diff.magnitude();
		dvx = accelerationProfile[j] * diff.x / diff.magnitude();
		dvy = accelerationProfile[j] * diff.y / diff.magnitude();
		m_Traj->addPoint(lastPt.x, vx, dvx, lastPt.y, vy, dvy);
	}
	else
	{
		cout << "ERROR: Failed to generate a trajectory" << endl;
		haveNewTraj = false;
	}
	return haveNewTraj;
}

// temporary function until we can get a real velocity planner

//#define S1P_V_MAX 	4.0
//#define S1P_A_MAX 	0.4
//#define S1P_A_MIN 	-0.8
#define VERBOSE 0	
//#define MIN_INIT_V	0.5

void CStage1Planner::generateVelocityProfile(vector<pose2> path,
	vector<double>& vProfile, vector<double>& aProfile)
{  
    if (path.empty())
       return;

    double v0, v0u;
    double vf, vfu; 
    m_pProblemSpecs->getGoalVelocity(&vf,&vfu);
    m_pProblemSpecs->getInitialVelocity(&v0,&v0u);

     double max_speed = min(m_pProblemSpecs->getMaxVelocity(),m_maxSpeed);
	#ifdef MIN_INIT_V
	// an alice at rest will tend to stay at rest- this is an attempt to make sure
	// that alice starts to move down the traj and doesn't sit at the start point
	// because the velocity at that point is 0.
	if (v0 < MIN_INIT_V)
		v0 = MIN_INIT_V;
	if (v0u < MIN_INIT_V)
		v0u = MIN_INIT_V;
	#endif
    
    int Npoints = path.size();
    vProfile.clear();
    aProfile.clear();
    vector<double> dist;
    pose2 diff;
    for(int i=0;i<Npoints-1;i++)
    {
    	diff = path[i+1]-path[i];
        dist.push_back(diff.magnitude());
    }

    vProfile.push_back(v0) ;
    double t;
      for(int i=1; i<Npoints; i++)
    { 
        t =(-vProfile[i-1]+sqrt(pow(vProfile[i-1],2)+2*m_maxAccel*dist[i-1]))/m_maxAccel;
        	vProfile.push_back( min(vProfile[i-1]+m_maxAccel*t,max_speed) );
    }
    vProfile[Npoints-1]=min(vf, max_speed);
    
    for(int i=Npoints-2; i>=0; i--)
    {
        t=( -vProfile[i+1]+sqrt(pow(vProfile[i+1],2)+2*fabs(m_maxBrake)*dist[i]) )/fabs(m_maxBrake);
        	vProfile[i]=min(vProfile[i+1]+fabs(m_maxBrake)*t, vProfile[i]);
    }
  
    for(int i=0; i<Npoints-1; i++)
    {       
            if(dist[i]>0)
    		    aProfile.push_back( ( pow(vProfile[i+1],2)- pow(vProfile[i],2) )/(2*dist[i]) ); 
            else
                aProfile.push_back(0.0); //the two points are coincident... we cannot accelerate
    }
    aProfile.push_back(0.0);
    
    if(VERBOSE)
    {
	cout << "path.size()=" << path.size()<< ", "
	     << "vProfile.size()=" << vProfile.size() << ", "
	     << "aProfile.size()=" << aProfile.size() << endl;

        for(int i=1; i<Npoints; i++)
    	   cout <<i<< " d: " << dist[i] <<  " v: " << vProfile.at(i)<< " a: " << aProfile.at(i)<< endl;     

    }
}

//#define SAFE_SPEED  1.5
//#define MAX_TURN_RATE 0.04   //evaluated in SantaAnitaSimpleLoop and stlukeSmallIntersectionRight 
void CStage1Planner::generateVelocityProfile3(vector<pose2> path,
	vector<double>& vProfile, vector<double>& aProfile)
{  
    if (path.empty())
       return;

    double v0, v0u;
    double vf, vfu; 
    m_pProblemSpecs->getGoalVelocity(&vf,&vfu);
    m_pProblemSpecs->getInitialVelocity(&v0,&v0u);

     double max_speed = min(m_pProblemSpecs->getMaxVelocity(),m_maxSpeed);
	#ifdef MIN_INIT_V
	// an alice at rest will tend to stay at rest- this is an attempt to make sure
	// that alice starts to move down the traj and doesn't sit at the start point
	// because the velocity at that point is 0.
	if (v0 < MIN_INIT_V)
		v0 = MIN_INIT_V;
	if (v0u < MIN_INIT_V)
		v0u = MIN_INIT_V;
	#endif
    
    int Npoints = path.size();
    vProfile.clear();
    aProfile.clear();
    vector<double> dist;
    vector<double> vMax;     //vector of maximum speed computed by approximating the turning rate for now we assume the point have equal distance
	double pointVmax=max_speed;
    pose2 diff;
    for(int i=0;i<Npoints-1;i++)
    {
    	diff = path[i+1]-path[i];
        dist.push_back(diff.magnitude());
		diff.unwrap_angle();    // make sure we have something in -pi,pi
       // dTheta = fabs(diff.ang);
		pointVmax = max_speed*(1-fabs(diff.ang)/m_safeTurn)+m_safeSpeed*fabs(diff.ang)/m_safeTurn;
		
		if(pointVmax < m_safeSpeed)           //we use SAFE_SPEED as minimum limit 
		{
		    pointVmax = m_safeSpeed;
    	}
        vMax.push_back(pointVmax);
		//cout <<i<< " : " << vMax[i]<<" "  << fabs(diff.ang)<<" " << max_speed <<  endl;     
    }

    vMax.push_back(max_speed);   //adding the last point max velocity

   
    vProfile.push_back(v0) ;
    double t;
    for(int i=1; i<Npoints; i++)
    { 
        t =(-vProfile[i-1]+sqrt(pow(vProfile[i-1],2)+2*m_maxAccel*dist[i-1]))/m_maxAccel;  //computing the time interval by 2nd order kinematics equation (on a line)
        	vProfile.push_back( min(vProfile[i-1]+m_maxAccel*t,vMax[i]) );   //now we saturate the velocity with the vector vMax
    }
    vProfile[Npoints-1]=min(vf, vMax[Npoints-1]);
  
//    cout << Npoints << endl;
    for(int i=Npoints-2; i>=0; i--)
    {
        t=( -vProfile[i+1]+sqrt(pow(vProfile[i+1],2)+2*fabs(m_maxBrake)*dist[i]) )/fabs(m_maxBrake); //same as before but going bwd: we use deceleration instead of acceleration
        	vProfile[i]=min(vProfile[i+1]+fabs(m_maxBrake)*t, vProfile[i]);
    }
  
    for(int i=0; i<Npoints-1; i++)
    {
            if(dist[i]>0)
    		    aProfile.push_back( ( pow(vProfile[i+1],2)- pow(vProfile[i],2) )/(2*dist[i]) ); 
            else
                aProfile.push_back(0.0); //the two points are coincident... we cannot accelerate
    }

    aProfile.push_back(0.0);
    
    if(VERBOSE)
    {
	cout << "path.size()=" << path.size()<< ", "
	     << "vProfile.size()=" << vProfile.size() << ", "
	     << "aProfile.size()=" << aProfile.size() << endl;

        for(int i=1; i<Npoints; i++)
    	   cout <<i<< " d: " << dist[i] << " vMax " << vMax[i] <<" v: " << vProfile.at(i)<< " a: " << aProfile.at(i)<<" Max Speed: "<< m_maxSpeed<<" " << max_speed  << endl;     

    }
}


#define turn_const 0.2
#define accel_const 0.2 
#define decel_const 0.4
#define steer_const 0.1

/* TODO:
   -- get initial speed for trajectory.
*/

void CStage1Planner::generateVelocityProfile2( vector<pose2> path, vector<double>& vProfile, vector<double>& aProfile) {

  if (path.empty())
    return;

  vector<double> aa;
  vector<double> dist;

  vProfile.clear();
  aProfile.clear();

  // Calculate angular acceleration between points:

  aa.push_back(0);
  
  for (unsigned int i = 0; i < path.size() - 1; i++) {
    double angleDiff = path[i].ang - path[i + 1].ang;
    
    if (angleDiff > 3.14159265) {
      angleDiff -= 3.14159265 * 2;
    }
    
    if (angleDiff < -3.14159265) {
      angleDiff += 3.14159265 * 2;
    }
    
    aa.push_back(angleDiff / (sqrt((path[i + 1].x - path[i].x) * (path[i + 1].x - path[i].x) + (path[i + 1].y - path[i].y) * (path[i + 1].y - path[i].y))));
  }
  
  aa.push_back(0);

  // Set initial velocity to current velocity:
  
  double v0, v0u;
  double vF, vFu;
  double max_speed = min(m_pProblemSpecs->getMaxVelocity(),m_maxSpeed) ;
  m_pProblemSpecs->getInitialVelocity(&v0, &v0u);
  m_pProblemSpecs->getGoalVelocity(&vF, &vFu);

  vProfile.push_back(v0);

  // Cap velocity at maximum turning rate and maximum legal speed:

  for (unsigned int i = 1; i < path.size() - 1; i++) {
    vProfile.push_back(turn_const / (fabs(aa[i]) + 0.01));
    
    if (vProfile[i] > max_speed) {
      vProfile[i] = max_speed;
    }
  }

  vProfile.push_back(vF);

  // Calculate distance between points:

  for (unsigned int i = 0; i < path.size() - 1; i++) {
    dist.push_back(sqrt((path[i + 1].x - path[i].x) * (path[i + 1].x - path[i].x) + (path[i + 1].y - path[i].y) * (path[i + 1].y - path[i].y)));
  }
  
  // Cap velocity to ensure time for steering wheel turn:

  for (unsigned int i = 1; i < path.size() - 1; i++) {
    if (vProfile[i] * fabs(aa[i] - aa[i + 1]) > steer_const) {
      vProfile[i] = steer_const / fabs(aa[i] - aa[i + 1]);
    }
  }

  // Smooth velocity going forwards to prevent excessive acceleration:

  for (unsigned int i = 1; i < path.size() - 1; i++) {
    if (vProfile[i] > vProfile[i - 1]) {
      if (vProfile[i] > sqrt(vProfile[i - 1] * vProfile[i - 1] + 2 * sqrt(accel_const * accel_const - (vProfile[i] * aa[i]) * (vProfile[i] * aa[i])) * dist[i - 1])) {
	vProfile[i] = sqrt(vProfile[i - 1] * vProfile[i - 1] + 2 * sqrt(accel_const * accel_const - (vProfile[i] * aa[i]) * (vProfile[i] * aa[i])) * dist[i - 1]);
      }
    }
  }

  // Smooth velocity going backwards to prevent excessive deceleration:

  for (int i = path.size() - 2; i > 0; i--) {
    if (vProfile[i + 1] < vProfile[i]) {
      if (vProfile[i] > sqrt(vProfile[i + 1] * vProfile[i + 1] + 2 * sqrt(decel_const * decel_const - (vProfile[i] * aa[i]) * (vProfile[i] * aa[i])) * dist[i])) {
	vProfile[i] = sqrt(vProfile[i + 1] * vProfile[i + 1] + 2 * sqrt(decel_const * decel_const - (vProfile[i] * aa[i]) * (vProfile[i] * aa[i])) * dist[i]);
      }
    }
  }

  // Generate aProfile from vProfile:

  for (int i = 0; i < (int)path.size() - 1; i++) {
    aProfile.push_back((vProfile[i + 1] - vProfile[i]) / dist[i]);
  }

  aProfile.push_back(0);
}



