/*
 * ClothoidPlanner.hh
 *
 * Originally written by Kenny Oslund, May 07
 * Revision history:
   * 1 May 07- Kenny Oslund
	- Created file
 */

#ifndef CLOTHOIDPLANNER_HH
#define CLOTHOIDPLANNER_HH

#include <stdlib.h>

#include "frames/pose2.hh"
#include "s1planner/ElementaryPath.hh"
#include "s1planner/Environment.hh"

#include "dgcutils/DGCutils.hh"
#include "trajfollower/trajF_status_struct.hh"
#include <temp-planner-interfaces/Console.hh>

// define CLOTHOID_UPDATE_TREE to have the tree updated rather than deleted and
// reinitialized between planning cycles. undefine it to plan from scratch everytime
#define CLOTHOID_UPDATE_TREE

// methods to choose the next node to explore
// randomly choose the next 
#define CLOTHOID_EXPLORE_NEXT_RANDOM		1
// use the a* like heuristic and cost to choose 
#define CLOTHOID_EXPLORE_NEXT_A_STAR		2
// genetic algorithm or some other psuedo-random thing- not yet implemented
#define CLOTHOID_EXPLORE_NEXT_GENETIC		3
// an attempt to force the planner to search nodes that are further away from other nodes
#define CLOTHOID_EXPLORE_NEXT_OUTWARD		4
// node types- explored, unexplored, or all
#define CLOTHOID_NODE_TYPE_EXPLORED		1
#define CLOTHOID_NODE_TYPE_UNEXPLORED		2
#define CLOTHOID_NODE_TYPE_ALL			3

// an added cost for changing the solution- in order to choose a different solution 
// its cost must be lower by some amount, not just slightly lower- specifically the condition
// new_cost * CLOTHOID_CHANGE_SOL_COST_SCALE < old cost
// must be met
#define CLOTHOID_CHANGE_SOL_COST_SCALE		1.2

/* --------- constants for setting the set of precomputed clothoids ---------- */

// mas number of initial/ final curvatures for any of the clothoid trees 
// (they can have fewer curvatures)
#define MAX_INIT_CURVATURES		20
#define MAX_FINAL_CURVATURES		20


/****** constants defining a dense clothoid tree, intended for use where its fairly
	clear where we need to go and we need a fine gridation (such as driving down a road) */
// then number of discrete increments in curvature between 0 
// and the max curvature
#define CLOTHOID_DB_DENSE_NUM_CURVATURES		3
// there will be initial curvature = 0 (the +1 term) and a number of paths = 
// DB_NUM_CURVATURES between 0 and the max curvature on either side (the 2* term)
#define CLOTHOID_DB_DENSE_INIT_CURV_IDX_BOUND		(2 * CLOTHOID_DB_DENSE_NUM_CURVATURES + 1)
// from an initial curvature, the set of possible final curvatures will be:
// {same curvature as initial, and a number of increments of curvature (same 
// increment size as for intial curvature) = DB_NUM_FINAL_CURVATURES on eitherside
// of the intial curvature. this must be <= DB_NUM_CURVATURES
#define CLOTHOID_DB_DENSE_NUM_FINAL_CURVATURES		3
// works the same way as DB_INIT_CURV_IDX_BOUND
#define CLOTHOID_DB_DENSE_FINAL_CURV_IDX_BOUND		(2 * CLOTHOID_DB_DENSE_NUM_FINAL_CURVATURES + 1)
// how many different discrete lengths to compute
#define CLOTHOID_DB_DENSE_NUM_LENGTHS			3
#define CLOTHOID_DB_DENSE_MAX_LENGTH			6.0
// max distance (in meters) and angle (radians) that the goal can change without 
// having to recompute all the goal nodes beyond this threshold, all the finish 
// nodes will be deleted and recomputed


/****** constants defining a dense clothoid tree, intended for use where the set of possible
	paths is much large/ there are more places we can drive and we don't need as fine
	a gridation (such as a parking log) */
// number of different length clothoids to create
#define CLOTHOID_DB_SPARSE_NUM_LENGTHS			3
//shortest clothoids. others are n * initial longer, where n is an integer
// < CLOTHOID_NUM_LENGTHS
#define CLOTHOID_DB_SPARSE_INITIAL_LENGTH		2
#define CLOTHOID_DB_SPARSE_INIT_CURV_IDX_BOUND		5
#define CLOTHOID_DB_SPARSE_FINAL_CURV_IDX_BOUND		10




#define CLOTHOID_MAX_GOAL_SPACIAL_CHANGE	0.5
#define CLOTHOID_MAX_GOAL_ANGLE_CHANGE		0.1
// max distance that the start can move without having to prune and update the tree
#define CLOTHOID_MAX_START_SPACIAL_CHANGE	0.3
#define CLOTHOID_MAX_START_ANGLE_CHANGE		0.1
// max distance alice can be from the trajectory without having to scrap and 
// recompute the entire tree
#define CLOTHOID_MAX_REINITIALIZE_CHANGE	1
// number of times we can update without moving, to prevent expanding tree without
// deleting anything
#define MAX_UPDATE_START_COUNT			10


/* basic way to use this program:
1- construct it by passing the vehicle state and the CEnvironment object
2- call generatePaths
3- call getBestPath to get the trajectory in a vector of pose2 points (you have 
	to generate your own velocity profile)
*/


class CClothoidPlanner
{
   private:
	// array dimensions are [initial curvature][final curvature]
	// array to use:
	CElementaryPath *(*m_pathDB)[MAX_INIT_CURVATURES][MAX_FINAL_CURVATURES];
	int m_pathDB_num_final_curvatures;
	int m_pathDB_num_init_curvatures;

	// set of paths meant to be as spread out as possible
	CElementaryPath *m_pathDB_sparse[MAX_INIT_CURVATURES][MAX_FINAL_CURVATURES];
	int m_pathDB_sparse_num_init_curvs;
	int m_pathDB_sparse_num_final_curvs;
	// set of paths that is denser and has less rapid curvature change
	CElementaryPath *m_pathDB_dense[MAX_INIT_CURVATURES][MAX_FINAL_CURVATURES];
	int m_pathDB_dense_num_init_curvs;
	int m_pathDB_dense_num_final_curvs;

	/* The tree of nodes to search over- should contain the point to start
	   planning from (not a complete clothoid), and pointers to all the other
	   clothoids to search over */
	CElementaryPath *m_searchTreeBase;

	/* pointer to the finish node which is arrived at by the lowest cost path */
	CElementaryPath *m_bestFinish;

	/* instance of the environment class, which is a container for info
	   passed from tplanner, such as legal driving area, cost map, etc.
	   note that only the pointer is stored, so the environment class
	   instance can be updated outside of the Clothoid planner class and
	   those updates will show here */
	CEnvironment *m_aliceEnv;

	/* instace of the vehicle state struct
	   note that only the pointer is stored, so the state struct
	   instance can be updated outside of the Clothoid planner class and
	   those updates will show here */
	VehicleState *m_aliceState;

	/* the resolution to explore at */
	int cur_resolution;

	/* store the finish location so that we can tell if its changed */
	pose2 goalPose;

	/* with updating enabled, nodes are only deleted if we pass them. if we
	   are stopped, then no nodes will be deleted, but new ones will be added
	   => memory usage increases linearly and if we're stopped for too long,
	   we'll seg fault. so, just keep track of how many times we've updated
	   without moving, and stop planning if its too many */
	int updateStartCount;

   public:
	// number of complete planning cycles that have occured
	int cycle_count;

	/* -------------- Initialization functions ----------------- */

	/* constructor */
	CClothoidPlanner(CEnvironment *env, VehicleState *state);

	/* destructor */
	~CClothoidPlanner();

	/* populate the clothoidDB. only run once at startup */
	void precomputeClothoids();

	/* parse the config files describing the different trees of precomputed clothoids */
	void readPrecomputedCurvatures(char *file_name, CElementaryPath *(*clothoidDB)[MAX_INIT_CURVATURES][MAX_FINAL_CURVATURES]);

	// initialize everything to an empty search tree- this is used on startup and
	// if the actual position gets too far from the previously generated trajectory
	void initialize();

	/* -------------- Worker functions --------------------------*/
	/* each call to this updates the clothoid tree and all costs- this is
	   essentially all the work */
	bool generatePaths(double runTime);

	bool search(CElementaryPath *m_nodeToSearch, bool in_reverse = false);
	
	bool explore(CElementaryPath *nodeToExplore, bool in_reverse = false);

	/* ---------------- access/ reporter functions ------------------- */
	/* get the direction that we need to go for the clothoid we're on */
	trajFmode getDirection();

	/* return the best trajectory as pose2 coordinates (no velocity profile)
	   This is how you get the solution that clothoid planner generated */
	vector<pose2> getBestPath(double *length = NULL, bool *reaches_goal = NULL);

	/* helper function used internally by getBestPath() */
	vector<CElementaryPath *> getBestClothoidSequence(bool *reaches_goal = NULL);

	CElementaryPath *getCurrentClothoid(int& index);

	pose2 getInitialPlanningPoint();

	CElementaryPath *getNextClothoidToExplore(int method);

	void aggregateFlatNodeList(vector<CElementaryPath *> &list, 
		CElementaryPath *curNode, int nodeType = CLOTHOID_NODE_TYPE_ALL);

	/* return the index for the clothoids in the matrix of precomputed clothoids which
	have the same inital curvature as the passed value */
	int getCurvIdx(double initCurv);

	/* ----------------- debugging output function -------------------*/

	void dumpTree(ostream *f = &cout);

	void dumpBestSolution(ostream *f = &cout);

	void dumpPrecomputedClothoids(ostream *log = &cout);

	/* ------------------- update functions ----------------------------*/

	void setStartPoint();

	// update the start location. This includes deleting nodes that are 
	// bellow us => we will not be driving on them.
	// returns whether the start point moved
	bool updateStart(bool force = false);

	// update the finish location. This involves deleting all the finish 
	// nodes and relinking them to the new finish
	// if force is true, then this will update the end even if it doesn't appear
	// to have moved
	// returns whether the end point has moved
	bool updateEnd(bool force = false);

	// recheck the tree for intersections with obstacles, and update the clothoids 
	// to reflect that. This will also update the goal node if the old goal node is now
	// unreachable 
	void recheckTreeLegality();


};

#endif  // CLOTHOIDPLANNER_HH


