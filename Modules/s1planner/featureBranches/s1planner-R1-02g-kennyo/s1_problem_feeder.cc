// Kenny Oslund, May 8, 2008
// simple program to emulate a logic planner and feed a problem to clothoid planner
// sends a traj to trajfollower

#include <temp-planner-interfaces/Utils.hh>
#include <temp-planner-interfaces/PlanGraph.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>
#include "cspecs/CSpecs.hh"
#include "map/MapElementTalker.hh"
#include "map/MapElement.hh"
#include <temp-planner-interfaces/PlanGraphPath.hh>
#include <trajutils/TrajTalker.hh>
#include "skynet/SkynetContainer.hh"
#include <unistd.h>
#include <interfaces/VehicleState.h>
#include <skynettalker/StateClient.hh>
#include <alice/AliceConstants.h>
#include <temp-planner-interfaces/AliceStateHelper.hh>
#include <bitmap/BitmapParams.hh>

#include <s1planner/tempClothoidInterface.hh>


#define SNKEY	1985


// vars
CMapElementTalker mapTalker;
CTrajTalker *trajTalker;

// functions
void setupCSpecs(CSpecs_t& cSpecs, VehicleState vehState, ActuatorState actState, 
     StateProblem_t stateProblem, pose2 finPose);
void paintCost(CSpecs_t& cSpecs, point2 alicePos);
void sendTraj(PlanGraphPath *path, CTraj *traj, bool print_traj = false);
void velPlannerPrimative(PlanGraphPath *path);
void genTestTraj(CTraj *traj);
void printMatlab(CTraj* traj);

// wrapper class for state
class stateEst : public CStateClient
{
  public:
    stateEst(bool waitForStateFill) 
      : CSkynetContainer(MODtrafficplanner, SNKEY)
      , CStateClient(waitForStateFill)
    {}

    VehicleState getVehState()
    {
      UpdateState();
      return m_state;
    }

    ActuatorState getActState()
    {
      UpdateState();
      return m_actuatorState;
    }
};


int main()
{
  // variables
    PlanGraphPath *path;
    CTraj mtraj;
    stateEst *state;
    VehicleState vehState;
    ActuatorState actState;
    CSpecs_t cspecs;
    StateProblem_t stateProblem;
    pose2 goalPose;
    double runtime;

  // initializations
    // talker to send things to mapviewer
    mapTalker.initSendMapElement(SNKEY);
    // talker to send trajs
    trajTalker = new CTrajTalker(MODtrafficplanner, SNKEY);
    // the traj
    mtraj = CTraj(3);
    // allocate the vehicle state struct
    state = new stateEst(true);
    // initialize the cspecs, which will hold the problem def.
    cspecs = CSpecs();
    // allocate the PlanGraphPath, which will be populated by s1planner and then 
    // converted into a traj
    path = new PlanGraphPath();
    // allocate the nodes for the PlanGraphPath- not needed for now because the
    // nodes are allocated another way
    /* for ( int i = 0; i < PLAN_GRAPH_PATH_MAX_NODES; i++ )
      path->nodes[i] = new PlanGraphNode(); */
    runtime = 0.2;

  // The actual work

  // get the current vehicle state
  vehState = state->getVehState();
  actState = state->getActState();
  stateProblem.obstacle = OBSTACLE_SAFETY;
  stateProblem.region = ROAD_REGION;
  goalPose = pose2(30,20,M_PI/4);

  setupCSpecs(cspecs, vehState, actState, stateProblem, goalPose);
  ClothoidPlannerInterface::clearClothoidTree(&cspecs);
  // sendTraj(path, &mtraj, true);
  while (1) {
    // set up the problem
    vehState = state->getVehState();
    actState = state->getActState();
    setupCSpecs(cspecs, vehState, actState, stateProblem, goalPose);

    // solve the problem
    ClothoidPlannerInterface::GenerateTraj(&cspecs, path, runtime, SNKEY, false);

    // send the problem
    sendTraj(path, &mtraj);
    cout << "traj sent" << endl;
    usleep(200000);
  }

  #ifdef VEL_PLANNER_TEST
  path->pathLen = 51;
  velPlannerPrimative(path);
  sendTraj(path, &mtraj);
  #endif

  #ifdef SEND_TRAJ_TEST
  while (1)
  {
    genTestTraj(&mtraj);
    usleep(100000);
  }

  printMatlab(&mtraj);
  #endif

  return 0;
}

void setupCSpecs(CSpecs_t& cSpecs, VehicleState vehState, ActuatorState actState, 
     StateProblem_t stateProblem, pose2 finPose)
{
  point2 initPos = AliceStateHelper::getPositionRearAxle(vehState);
  double initHeading = AliceStateHelper::getHeading(vehState);

  // set up the costmap
  paintCost(cSpecs, initPos);

  #ifdef COSTMAP
  // Set up the corridor and paint the costs
  switch (stateProblem.region) {
  case ROAD_REGION:

    Log::getStream(4)<<"ZONE/ROAD_REGION: Exit label  "<<exitLabel<<endl;

    /* set the zone action*/ 
    if (stateProblem.state == UTURN) {
      // set baseVal to so everything outside the road has high cost. convertLaneToPolygon
      // will set the cost of the road to some low value and it uses COMB_REPLACE
      // bmParams.baseVal = 1.4;
      bmParams.baseVal = 150;	// RMM, 28 Oct 07 for NQE, run 3
      cSpecs.setTrafficState(TRAFFIC_STATE_UTURN);
    } else if (stateProblem.state == BACKUP) {
      bmParams.baseVal = 10;
      cSpecs.setTrafficState(TRAFFIC_STATE_BACKUP);
    } else {
      bmParams.baseVal = 50;
      cSpecs.setTrafficState(TRAFFIC_STATE_ROAD);
    }
    
    /* Paint the cost*/
    paintCostRoadRegion(cSpecs, bmParams, corridor, polygonParams, map, initPos, 
currSegGoals);
    
    break;
      
  case ZONE_REGION:

    pathParams.zoneId = currSegGoals.entrySegmentID;
    if (pathParams.zoneId <= (int)map->prior.segments.size()) pathParams.zoneId = 
currSegGoals.exitSegmentID;
    
    /* Determine what we plan to do in the zone based on exit point */
    Log::getStream(4)<<"ZONE: Exit label  "<<exitLabel<<endl;
    
    /* set the zone action*/ 
    cSpecs.setTrafficState(determineZoneAction(exitLabel, pathParams.zoneId, map));
    
    // Actually paint the cost
    // set baseVal to 1 because we also paint zone perimeter and the comb function is 
COMB_MAX
    bmParams.baseVal = 1;
    paintCostZone(cSpecs,bmParams,corridor, polygonParams, map, initPos, 
pathParams.zoneId);
            
    break;
	
  case INTERSECTION:

    /* Get the corridor*/

    Log::getStream(4)<<"ZONE/ROAD_REGION: Exit label  "<<exitLabel<<endl;
    
    /* set the zone action*/ 
    cSpecs.setTrafficState(TRAFFIC_STATE_ROAD);


    /* Paint the cost*/
    // set baseVal to 100 so everything outside the road has high cost. 
convertLaneToPolygon
    // will set the cost of the road to some low value and it uses COMB_REPLACE
    bmParams.baseVal = 50;
    paintCostRoadRegion(cSpecs, bmParams,corridor, polygonParams, map, initPos, 
currSegGoals);
    
    //    // Assign the cost map in cSpecs
    //    cSpecs.setCostMap(bmParams);
    
    break;
    
  default:
    Console::addMessage("ZONECORRIDOR: gen corridor: SHOULD NEVER GET HERE");
    Log::getStream(1) <<"ZONECORRIDOR: gen corridor: SHOULD NEVER GET HERE" << endl;
	
  }
  #endif

  /// Set up initial conditions
  double startingState[4];
  startingState[0] = initPos.x;
  startingState[1] = initPos.y;
  startingState[2] = AliceStateHelper::getVelocityMag(vehState);
  startingState[3] = initHeading;
  cSpecs.setStartingState(startingState);

  /// starting controls
  double startingControls[2];
  startingControls[0] = AliceStateHelper::getAccelerationMag(vehState);
  startingControls[1] = actState.m_steerpos*VEHICLE_MAX_AVG_STEER;
  cSpecs.setStartingControls(startingControls);

  /// Final state
  double finalState[4];
  finalState[0] = finPose.x - DIST_REAR_AXLE_TO_FRONT*cos(finPose.ang);
  finalState[1] = finPose.y - DIST_REAR_AXLE_TO_FRONT*sin(finPose.ang);
  finalState[2] = 0.0;
  finalState[3] = finPose.ang;
  cSpecs.setFinalState(finalState);

  /// final controls
  double finalControls[2];
  finalControls[0] = 0;
  finalControls[1] = 0;
  cSpecs.setFinalControls(finalControls);
  
  /// set maximums 
  cSpecs.setMaxVelocity(4);
  cSpecs.setMaxAcc(VEHICLE_MAX_ACCEL);
  cSpecs.setMaxBraking(VEHICLE_MAX_DECEL);
  cSpecs.setMaxSteeringAngle(VEHICLE_MAX_AVG_STEER);
  cSpecs.setMaxSteeringRate(M_PI/2);
  
  /// set safety margins for obstacles
  vector<double> obstacle_safety_margin;
  double front, side, rear;
  switch (stateProblem.obstacle) {
  case OBSTACLE_SAFETY:
    front = 2.0;
    side = 1.0;
    rear = 1.0;
    break;
  case OBSTACLE_AGGRESSIVE:
    front = 1.0;
    side = 0.75;
    rear = 0.75;
    break;
  case OBSTACLE_BARE:
    front = 0.5;
    side = 0.5;
    rear = 0.5;
    break;    
  default:
    Log::getStream(1) <<"ZONECORRIDOR: dealing with obstacles: SHOULD NEVER GET HERE" << endl;
    
  }
  obstacle_safety_margin.push_back(front);
  obstacle_safety_margin.push_back(side); // right
  obstacle_safety_margin.push_back(rear);
  obstacle_safety_margin.push_back(side); // left
  Log::getStream(7) << "ZONE: setting a obstacle safety margin of " << 
    obstacle_safety_margin << " around Alice" << endl;
  cSpecs.setObstacleSafetyMargins(obstacle_safety_margin);

  // vector<double> perimeter_safety_margin;
  //  perimeter_safety_margin.push_back(1.0);
  double perimeter_safety_margin = 1.0;
  Log::getStream(7) << "ZONE: setting a perimeter safety margin of " << 
    perimeter_safety_margin << " around Alice" << endl;
  cSpecs.setPerimeterSafetyMargins(perimeter_safety_margin);

  return;
}


void paintCost(CSpecs_t& cSpecs, point2 alicePos)
{
  /// COSTMAP definition
  point2arr corridor;
  BitmapParams bmParams;
  bmParams.resX = 0.1;
  bmParams.resY = 0.1;
  bmParams.width = 800;
  bmParams.height = 800;
  bmParams.baseVal = 1 ;
  bmParams.outOfBounds = 100;
  // PolygonParams polygonParams;
  // polygonParams.centerlaneVal = 1;
  // polygonParams.obsCost = 500;

  bmParams.centerX = alicePos.x;
  bmParams.centerY = alicePos.y;

  bmParams.polygons.clear();

  /* Paint the obstacle and perimeter cost and parking space cost*/
  // paintCost(bmparams,polygonParams, alicePos, corridor, obstacles);
  // Assign the cost map in cSpecs
  cSpecs.setCostMap(bmParams);

}




// fill in velocities, generate the traj structure and send it to follower and to map viewer
void sendTraj(PlanGraphPath *path, CTraj *traj, bool print_traj)
{
  point2arr trajpts;

  velPlannerPrimative(path);

  traj->startDataInput();
  if ((path != NULL) && (path->nodes[0] != NULL)) {
    PlanGraphPathDirection direction = path->directions[0];
    for (int i = 0; i < path->pathLen; i++) {    
      double speed = path->speeds[i];
      double yaw = path->nodes[i]->pose.rot;
      double x = path->nodes[i]->pose.pos.x;
      double y = path->nodes[i]->pose.pos.y;

      // populate the traj structure
      if (path->directions[i] == direction) {
         traj->addPoint(x, speed * cos(yaw),
                   path->accelerations[i] * cos(yaw),
                     // - speed * speed * path->curvatures[i] * sin(yaw),
                   y, speed * sin(yaw),
                   path->accelerations[i] * sin(yaw)
                     // + speed * speed * path->curvatures[i] * cos(yaw)
                   );
      }
    
      // populate the point2arr for sending to mapviewer
      trajpts.push_back(point2(x, y));

    }

    if (direction == PLAN_GRAPH_PATH_FWD)
      traj->setDirection(1);
    else 
      traj->setDirection(-1);
  }

  // send the traj to trajfollower
  int trajSocket = trajTalker->m_skynet.get_send_sock(SNtraj);
  traj->setOverrideROA(true);
  trajTalker->SendTraj(trajSocket, traj);

  // send the traj to the mapviewer
  MapElement MapElTraj;
  MapId mapIdTraj(15500);
  MapElTraj.setId(mapIdTraj);
  MapElTraj.setGeometry(trajpts);
  MapElTraj.plotColor = MAP_COLOR_GREEN;
  MapElTraj.setTypeLine();
  mapTalker.sendMapElement(&MapElTraj, -2);

  if (print_traj)
    printMatlab(traj);

  return;
}

// fill in a very simple velocity profile- this simply assumes that the points are 20cm 
// apart (as is the case for s1planner) and that we are acclerating or decelerating at
// constant rate. it then fills in the velocities for going from stopped -> 4m/s and 
// 4m/s -> stopped (and 4m/s in the middle)
// if the traj is too short, it will just just accelerate as long as it can, then start
// to decelerate immediately without ever reaching 4m/s
void velPlannerPrimative(PlanGraphPath *path)
{
  // velocities at points 20cm apart, assuming that you start stopped
  // and accelerate at 0.8m/s^2
  double vels_accel[] = 
     { 0.1, 0.56569, 0.8, 0.9798, 1.1314, 1.2649, 1.3856, 1.4967, 1.6, 1.6971, 
     1.7889, 1.8762, 1.9596, 2.0396, 2.1166, 2.1909, 2.2627, 2.3324, 2.4, 2.4658, 2.5298, 
     2.5923, 2.6533, 2.7129, 2.7713, 2.8284, 2.8844, 2.9394, 2.9933, 3.0463, 3.0984, 3.1496, 
     3.2, 3.2496, 3.2985, 3.3466, 3.3941, 3.4409, 3.4871, 3.5327, 3.5777, 3.6222, 3.6661, 
     3.7094, 3.7523, 3.7947, 3.8367, 3.8781, 3.9192, 3.9598 };
  double accel = 0.8;
  int num_pts_accel = 50;

  // velocities at points 20cm apart, assuming that you start at 4m/s
  // and decelerate at 1.6m/s^2
  double vels_decel[] = 
     { 3.9192, 3.8367, 3.7523, 3.6661, 3.5777, 3.4871, 3.3941, 3.2985, 3.2, 3.0984, 2.9933, 
     2.8844, 2.7713, 2.6533, 2.5298, 2.4, 2.2627, 2.1166, 1.9596, 1.7889, 1.6, 1.3856, 
     1.1314 };
  double decel = -1.6;
  int num_pts_decel = 23;

  double v_des = 4; // m/s
  int num_pts = path->pathLen;

  // nominal case
  for (int i = 0; i < num_pts; i++) {
    path->speeds[i] = v_des;
    path->accelerations[i] = 0;
  }

  // number of points overwhich velocity varies
  int num_pts_dv = num_pts_accel + num_pts_decel;
  int i;

  // accelerating at the beginning
  for (i = 0; i < num_pts_accel && i < num_pts * num_pts_accel / num_pts_dv; i++) {
    path->speeds[i] = vels_accel[i];
    path->accelerations[i] = accel;
  }

  // decelerating at the end
  if ( path->pathLen < num_pts_dv )
    i = num_pts_decel * num_pts_decel / num_pts_dv;
  else
    i = 0;
  for (; i < num_pts_decel; i++) {
    int ind = path->pathLen - num_pts_decel + i;
    path->speeds[ind] = vels_decel[i];
    path->accelerations[ind] = decel;
  }

}


// generate and send a test trajectory which is just a semicircular arc
void genTestTraj(CTraj *traj)
{
  #define NUM_TEST_PTS	50
  #define TEST_RAD	30
  #define TEST_VEL	5
  #define PERCENT_CIRC	0.5
  traj->startDataInput();
  point2arr trajpts;
  for (int i = 0; i < NUM_TEST_PTS; i++)
  {
    // generate a test trajectory, which is a circle
    double x = sin(2 * PERCENT_CIRC * M_PI*i / NUM_TEST_PTS);
    double x_dot = TEST_VEL * cos(2 * PERCENT_CIRC *M_PI*i / NUM_TEST_PTS);
    double x_ddot = 0;

    double y = sqrt(1-x*x);
    double y_dot = TEST_VEL * sin(2 * PERCENT_CIRC *M_PI*i / NUM_TEST_PTS);
    double y_ddot = 0;
    if (cos(2 * PERCENT_CIRC *M_PI*i / NUM_TEST_PTS) > 0)
      y *= -1;
    y += 1;
    x *= TEST_RAD; y *= TEST_RAD;

    // populat the traj
    traj->addPoint(x,x_dot,x_ddot,y,y_dot,y_ddot);

    // populate the point2arr for sending to mapviewer
    trajpts.push_back(point2(x, y));
    // cout << "point: " << x << "," << y << endl;
  }

  // send the traj to the mapviewer
  MapElement MapElTraj;
  MapId mapIdTraj(15500);
  MapElTraj.setId(mapIdTraj);
  MapElTraj.setGeometry(trajpts);
  MapElTraj.plotColor = MAP_COLOR_GREEN;
  MapElTraj.setTypeLine();
  mapTalker.sendMapElement(&MapElTraj, -2);
  
  // send the traj to trajfollower
  int trajSocket = trajTalker->m_skynet.get_send_sock(SNtraj);
  traj->setOverrideROA(true);
  traj->setDirection(1);
  trajTalker->SendTraj(trajSocket, traj);


}

// output the traj to a matlab file for plotting
void printMatlab(CTraj* traj)
{
  ofstream file_out;
  file_out.open("plot_graph.m");
  file_out<< "clear all; close all;" << endl;
  
  if (1) {
    ostringstream x, y, dx, dy, ddx, ddy;
    x << "traj_x = [ ";
    y << "traj_y = [ ";
    dx << "traj_dx = [ ";
    dy << "traj_dy = [ ";
    ddx << "traj_ddx = [ ";
    ddy << "traj_ddy = [ ";
    for (int i=0; i<traj->getNumPoints(); i++) {
      x << traj->getNorthing(i) << " ";
      y << traj->getEasting(i) << " ";
      dx << traj->getNorthingDiff(i, 1) << " ";
      dy << traj->getEastingDiff(i, 1) << " ";
      ddx << traj->getNorthingDiff(i, 2) << " ";
      ddy << traj->getEastingDiff(i, 2) << " ";
    }
    x << "];" << endl;
    y << "];" << endl;
    dx << "];" << endl;
    dy << "];" << endl;
    ddx << "];" << endl;
    ddy << "];" << endl;
    file_out << "% traj output" << endl << x.str() << y.str() << dx.str() << dy.str() << ddx.str() << ddy.str();
    file_out << "speed = sqrt(traj_dx.^2 + traj_dy.^2);" << endl;
    file_out << "acc = sqrt(traj_ddx.^2 + traj_ddy.^2);" << endl;
    file_out << "arclen(1) = 0;" << endl;
    file_out << "for ii=2:length(traj_x) arclen(ii)=arclen(ii-1)+sqrt((traj_x(ii)-traj_x(ii-1))^2 + (traj_y(ii)-traj_y(ii-1))^2);  end" << endl;
    file_out << "plot(traj_x, traj_y, 'g-'); hold off;" << endl;
    file_out << "figure;" << endl;
    file_out << "plot(arclen, speed, 'g'); hold on;" << endl << "plot(arclen, acc, 'r');" << endl;
  }
  file_out.close();

  cout << "printed traj to plot_graph.m" << endl;

  return;
}

