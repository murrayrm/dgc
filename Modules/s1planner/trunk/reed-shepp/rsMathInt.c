#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* 
============================================================================

   This stub is used to interface the C program for computing the Reeds
   Shepp path length with Mathematica.  The Mathematica code which handles
   the other end of the interface is in the file rs.m .

============================================================================
*/

#define infini HUGE_VAL

extern double reed_shepp();
extern double reed_shepp2();

main(argc,argv)
int argc;
char *argv[];

/*
 If no command line arguments are specified, we check all 48 path types.
 If arguments are present, they are interpreted as the list of path types
 to check.
*/

{
static double origin[3] = {0.0, 0.0, 0.0};
float x, y, phi;
double xyphi[3];
double t, u, v, length, best_length;
int pathtype, i;

scanf("List(%f,%f,%f)", &x, &y, &phi);       /* read in CForm output */
xyphi[0] = x; xyphi[1] = y; xyphi[2] = phi;

if (argc == 1) {
  length = reed_shepp(origin, xyphi, &pathtype, &t, &u, &v);
  printf("%f\n", length);
}

else {
  best_length = infini;
  for (i = 1; i < argc; i++) {
      pathtype = atoi(argv[i]);
      length = reed_shepp2(origin, xyphi, pathtype, &t, &u, &v);
      if (length < best_length) best_length = length;
    }
  if (best_length < infini) printf("%f\n", best_length);
  else printf("\"inv\"\n"); /* flag to Mathmatica -- no valid paths */
}

return 0;
}
