/**
 * File: LadarCarMap.cc
 * Description: (see p. 110)
 *   A simple grid-map that tracks freespace
 *   (Ambrus convinced me to give this a try)
 *   The purpose is to add another check before deciding that an object is
 *   actualy moving (if it's in what this map thinks is free space, it's moving
 *   otherwise, it's probably just poor association/wall partially occluded
 *   
 *   All cells start out with value 0, value is decremented if cell is
 *   observed to be free, incremented if observed to be occupied
 *
 * Last Changed: May 5
 **/

#include "LadarCarPerceptor.hh"
#include "../emap/emapViewer.cc"
using namespace std;

void LadarCarPerceptor::mapInit()
{
  tamasMap = new EMap(2*MAPRADIUS + 1);
  tamasMap->clear(MAPCLEARVALUE2);
  if(useMapDisplay) {
    startViewer(NULL, NULL,2*MAPRADIUS+1);
  }

  //Center map at 0,0 - at first iteration, it will be shifted to alice's position
  //(I don't think that we have state info when init is called)
  mapCenterX = 0;
  mapCenterY = 0;

  return;
}

/**
 * This function cycles through each cell in the map, updating them if
 * necessary. 
 **/
void LadarCarPerceptor::mapUpdate()
{
  if(5 >= numFrames) //TODO: WHY on the first iteration do we run processScan 5 times??
    {
      //      fprintf(stderr, "at first frame - shifting origin \n");
      mapCenterX = currState.X;
      mapCenterY = currState.Y;
    }
  //first, move map center to Alice's origin
  while(MAPCELLDIMENSION < (currState.X - mapCenterX))
    {
      tamasMap->shiftClear(-1,0,MAPCLEARVALUE);
      mapCenterX += MAPCELLDIMENSION;
      //      fprintf(stderr, "shifted map center +x to %f (frame = %d) \n", mapCenterX, numFrames);
    }
  while(-MAPCELLDIMENSION > (currState.X - mapCenterX))
    {
      tamasMap->shiftClear(1,0, MAPCLEARVALUE);
      mapCenterX -= MAPCELLDIMENSION;
      //      fprintf(stderr, "shifted map center -x \n");
    }
  while(MAPCELLDIMENSION < (currState.Y - mapCenterY))
    { 
      tamasMap->shiftClear(0,-1, MAPCLEARVALUE2);
      mapCenterY += MAPCELLDIMENSION;
      //      fprintf(stderr, "shifted map center +y \n");
    }
  while(-MAPCELLDIMENSION > (currState.Y - mapCenterY))
    {
      tamasMap->shiftClear(1,0, MAPCLEARVALUE2);
      mapCenterY -= MAPCELLDIMENSION;
      //      fprintf(stderr, "shifted map center -y \n");
    }

  /*************using Tamas's map class*********/
  double returnRange, minRange;
  int x1, x2, x3, x4, x5, y1, y2, y3, y4, y5;
  double lastX, lastY, dist;
  lastX = 0;
  lastY = 0;

  for(int i = 0; i < (NUMSCANPOINTS - 1); i++) {
    returnRange = rawData[i][RANGE];


    dist = sqrt(pow(lastY - xyzData[i][1],2)+ pow(lastX - xyzData[i][0],2));
    //Only update if returns are sufficiently far apart
    if(0 == i || dist > MAPCELLDIMENSION); {

    lastX = xyzData[i][0];
    lastY = xyzData[i][1];


    //if no-return point, plot free space out to 35m
    if(returnRange > MAXRANGE) {
      //first vertex is ladar's origin (for front ladar, at center of map)
      x3 = MAPRADIUS;
      y3 = MAPRADIUS;


      x1 = MAPRADIUS + (int)round( occupiedData[i][MINANGLE_35][0] / MAPCELLDIMENSION);
      y1 = MAPRADIUS + (int)round( occupiedData[i][MINANGLE_35][1] / MAPCELLDIMENSION);

      x2 = MAPRADIUS + (int)round( occupiedData[i][MAXANGLE_35][0] / MAPCELLDIMENSION);
      y2 = MAPRADIUS + (int)round( occupiedData[i][MAXANGLE_35][1] / MAPCELLDIMENSION);

      tamasMap->decTriangle(x1,y1,x2,y2,x3,y3, 256*MAPFREESPACEINCREMENT);
      //now, adjust for the overlapping triangles
      tamasMap->incTriangle(x1,y1,x3,y3,x3,y3, 256*MAPFREESPACEINCREMENT);

    } else if(returnRange > MINRANGE) { //draw both free space and occupied space
      minRange = min(returnRange, rawData[i+1][RANGE]);

      x3 = MAPRADIUS;
      y3 = MAPRADIUS;


      x1 = MAPRADIUS + (int)round( occupiedData[i][MINANGLE_MINRANGE][0] / MAPCELLDIMENSION);
      y1 = MAPRADIUS + (int)round( occupiedData[i][MINANGLE_MINRANGE][1] / MAPCELLDIMENSION);

      x2 = MAPRADIUS + (int)round( occupiedData[i][MAXANGLE_MINRANGE][0] / MAPCELLDIMENSION);
      y2 = MAPRADIUS + (int)round( occupiedData[i][MAXANGLE_MINRANGE][1] / MAPCELLDIMENSION);

      tamasMap->decTriangle(x1,y1,x2,y2,x3,y3, 256*MAPFREESPACEINCREMENT);
      //      tamasMap->incTriangle(x1,y1,x3,y3,x3,y3, 256*MAPFREESPACEINCREMENT);

      x4 = MAPRADIUS + (int)round( occupiedData[i][MINANGLE_MAXRANGE][0] / MAPCELLDIMENSION);
      y4 = MAPRADIUS + (int)round( occupiedData[i][MINANGLE_MAXRANGE][1] / MAPCELLDIMENSION);

      x5 = MAPRADIUS + (int)round( occupiedData[i][MAXANGLE_MAXRANGE][0] / MAPCELLDIMENSION);
      y5 = MAPRADIUS + (int)round( occupiedData[i][MAXANGLE_MAXRANGE][1] / MAPCELLDIMENSION);
      tamasMap->incTriangle(x1,y1,x2,y2,x4,y4,256*MAPOBJECTINCREMENT);
      tamasMap->incTriangle(x5,y5,x1,y1,x4,y4,256*MAPOBJECTINCREMENT);
      //and, decrement the line we just doubled up on
      tamasMap->decTriangle(x1,y1,x1,y1,x4,y4,256*MAPOBJECTINCREMENT);
    }


    }//end checking point separation

  }

    //send the map to viewer
  //    MSG("calling setEMap");
  if(useMap && useMapDisplay) {
    setEMap(this->tamasMap, 0, 0xFFFF);
  }

    return;
}
