Thu Jul 24  2:00:42 2008	Glenn Wagner (wagner)

	* version R1-01
	BUGS:  
	FILES: LadarCarPerceptor.hh(48099), LadarCarTracking.cc(48099)
	File Edit Options Buffers Tools Help				   
		    Added new form  of corner identification	    based
	on finding the closest corner of \ a car to the point where the
	tracked corner was	  identified, rather than finding\  the
	closest corner to the center of mass of detected points.  Added
	support to\  synthetic ladar processing to	  determine dStart 
	      and dEnd, for foreground determ\ ination.  Added	      a
	requirement that objects move more than MINCARDIST from their\ 
	initial point before they can become cars (currently 1	     
	meter).  Error	in initi\ al velocity was	correct (may have
	been  fixed before, but subversion is acting u\ p at the moment,
	and can't check).  Fixed a problem with sending map elements t\ hat
	kept the uncertainty for static objects from being	 set.  Was
	careful to turn off all debuging loging

Tue Jul 22 14:51:15 2008	Glenn Wagner (wagner)
Slight modification of the previous release, with the command line arguments handling correct so that other people can compile the code
	* version R1-00z
	BUGS:  
	FILES: LadarCarPerceptor.cc(48024), LadarCarTracking.cc(48024),
		cmdline.ggo(48024)
	Moved the command line arguments to the .ggo file, where it is
	supposed to go.  Also swapped the rectangular representaiton to
	using a Kalman filter estimate of the car corner position, but the
	current raw fit of the car for the geometry

Tue Jul 22 12:48:18 2008	Glenn Wagner (wagner)
This release adds support for "synthetic ladar" as a means of merging data from the ladars.  The data is mapped to the world frame, and processed there, using a euclidean distance requirement for segementation rather than the standard ladar approach of looking for a range jump (won't work here).  An option to filter the data based on its source is provided, to give greater priority to the bumper ladars, which generally see a better part of the car than the roof ladars would.  A problem with the initialization of the object Kalman filters was also fixed, removing the gigantic velocity estimation error that occured when objects were created.

--use-synth enables the synthetic ladar
--filter-by-sensor turns on sensor source filtering, as well as synthetic ladar
--use-rect represents cars by their fit rectangle, rather than raw ladar points.
	
	
	* version R1-00y
	BUGS:  
	FILES: LadarCarMap.cc(47635), LadarCarPerceptor.cc(47635),
		LadarCarPerceptor.hh(47635), LadarCarTracking.cc(47635)
	Added code to generate a synthetic ladar sensor from data returned
	from multiple individual ladars.  It is turned on by the
	--use-synth flag.  It functions by finding all points from each
	ladar that actually produces a detection (i.e. the range is between
	MINRANGE and MAXRANGE), and dumps these in a vector.  The points
	are then sorted by angle relative to the synthetic ladar origin. 
	Non-detection points are then inserted into any hole greater than
	1.5 degrees in size, at 1 degree intervals (because the ladars may
	pickup obstacles in another ladar's non-detection invterval.  The
	synthetic ladar's origin is currently positioned at the origin of
	the vehicle frame, which is an Alice length behind where the front
	bumper ladars are, which increases paralax.  Shifting the synthetic
	ladar's origin to the middle front bumpers' location should help. 
	If not, a form of segmentation could be used to estimate where
	overlaps occur.

	FILES: LadarCarPerceptor.cc(47624), LadarCarPerceptor.hh(47624),
		LadarCarTracking.cc(47624), Makefile.yam(47624)
	Made changes to LadarCarTracking and LadarCarPerceptor to allow for
	representing cars in the map using only estimated (rather than raw
	or instantaneous fit) data.  This consisted of adding an additional
	Kalman filter to the Car Representation structure to estimate the
	size and orientation of the car, and adapting the existing code to
	display cars as a square (based on the current square fit) to
	instead use the size and orientation from the Kalman filter to draw
	the car.  No changes were made in associtation (the shape Kalman
	filter isn't used for this).  The other change is that the dynamics
	matricies for the Kalman filters are updateed based on the current
	time step.

	FILES: LadarCarPerceptor.cc(47725), LadarCarPerceptor.hh(47725)
	Added synthetic ladar scans.  This is the commit before I try to
	rewrite the segmentation code to no longer rely exclusively on
	angular order

	FILES: LadarCarPerceptor.cc(47733), LadarCarPerceptor.hh(47733),
		LadarCarTracking.cc(47733)
	The map elements now have their uncertainty fields filled by
	copying the terms of the uncertainty matrix of the tracking Kalman
	filter into them.  There are now command line arguments to control
	both whether synthetic ladar data is used and whether to use a
	rectangular fit or raw data to represent an object or car on the
	map.  --use-synth    uses synthetic data, which merges the three
	front bumper ladars into a single artificial ladar scan.  This
	gives better detection range as points detected in different scans
	can be combined when determining if a return is an object or not,
	but seems to give worse segmentation.  --use-rect	represents
	cars by their rectangular fit (used for edge detection), instead of
	raw data.  Changing commented out code in LadarCarTracking allows
	the use of a shape estimate to be used to generate the rectangle,
	instead of instantaneous fitting, but performs rather worse.

	FILES: LadarCarPerceptor.cc(47743), LadarCarPerceptor.hh(47743),
		LadarCarTracking.cc(47743)
	Implemented a new segmentation system.	It takes all the local
	frame coordinates of the bumper ladar scans (all of them), and
	sorts them in angular order from the MF bumper ladar.  It then does
	segmentation based on euclidean distance from points.  This gives
	better resolution, with increased detection range, and seems to
	reduce noise.  The roof ladars can be added to the data set, but it
	is not yet clear what this does to car detection and tracking.	The
	previous form of synthetic ladar, i.e. treating the synthetic data
	as a single scan, is no longer implemnted, although the code is
	still there to reimplement it

	FILES: LadarCarPerceptor.cc(47766), LadarCarPerceptor.hh(47766),
		LadarCarTracking.cc(47766)
	The code for the old style (synthetic ladar scan, as opposed to
	local frame) synthetic ladar has been cleaned out, and everything
	still works :).  The data structures for type taging are still in
	place, so next step is to start the taging (which always behaves
	poorly)

	FILES: LadarCarPerceptor.cc(47817), LadarCarPerceptor.hh(47817),
		LadarCarTracking.cc(47817)
	Have the filtering by sensor type partially done.  Currently the
	code up to classify object has been modified to permit filtering. 
	Need to go and update obj2car and update car to get it all working
	(as well as the car portion of classifyObject)

	FILES: LadarCarPerceptor.cc(47824), LadarCarPerceptor.hh(47824),
		LadarCarTracking.cc(47824)
	The code for filtering points representing cars based on which
	sensor generated them is now in place. It checks if there are
	enough points of the most prefered type to be considered a car, if
	not, it adds points from less desireable sensors, until there are
	enough points.	Added a new command line argument to control
	whether or not the filtering is preformed.  --filter-by-sensor
	turns on the synthetic ladar scan, as well as turning on the
	filtering.

	FILES: LadarCarPerceptor.cc(47827), LadarCarTracking.cc(47827)
	Fixed an assertion failure (actually the result of writing out of
	the bounds of the pointSource array, which appears to have only
	screwed up a matrix, instead of causing the seg fault I would ahve
	expected), which occured when you ran the code without either the
	--use-synth or --filter-by-sensor flags, because of additional
	sensors

	FILES: LadarCarPerceptor.cc(47978), LadarCarTracking.cc(47978)
	This is a revision for use by others.  It uses the pure car fit
	rectangle, as this gives a far less noisy element than the
	estimated version.  Prediction based association is also turned
	off, as it is not fully reliable without a less noisy state
	estimation.

	FILES: LadarCarPerceptor.hh(47758)
	I screwed up before doing this, so I want to be able to revert a
	shorter distance.  Added the data structure background for melding
	different types of sensors by prioritizing more desireable sensors
	when generating car geometries.  Objects won't require this, as we
	don't need to track individual features of the objects

	FILES: LadarCarTracking.cc(47625)
	Minor change to test svn

	FILES: LadarCarTracking.cc(47848)
	Fixed a bug in the kalman filter for position where, because the
	prediction matricies were not initialized to non-zero values prior
	to the first update step, all objects would be shifted to the
	origin briefly, causing strange velocity behavior.

Sat Sep 15  0:21:57 2007	Laura Lindzey (lindzey)

	* version R1-00x
	BUGS:  
	FILES: LadarCarTracking.cc(38885)
	fixing bug found @ St. Luke added missing zero ... timeStopped is
	now in seconds, rather than .1 seconds. d'oh

Thu Sep 13 16:50:12 2007	Laura Lindzey (lindzey)

	* version R1-00w
	BUGS:  
	FILES: LadarCarPerceptor.hh(38624), LadarCarTracking.cc(38624)
	changes from field, improving handling of element IDs sent to
	planner

Sun Sep  9 16:44:19 2007	Laura Lindzey (lindzey)

	* version R1-00v
	BUGS:  
	FILES: LadarCarTracking.cc(37984)
	now calling simplify before sending elements to mapper; removed
	deprecated call to get_inside

Tue Sep  4  0:23:18 2007	Laura Lindzey (lindzey)

	* version R1-00u
	BUGS:  
	FILES: LadarCarPerceptor.hh(37364), LadarCarTracking.cc(37364)
	Now checks lastUpdated timestamp against lastSent on tracked
	elements before sending; this makes sure it has more recent data
	than the previous message. Meant to help cut down on spread
	traffic.  (Change made/tested in field Monday evening)

Sat Sep  1  8:06:08 2007	Laura Lindzey (lindzey)

	* version R1-00t
	BUGS:  
	FILES: LadarCarTracking.cc(36602)
	changing IDs to match fusion  conventions

Wed Aug 29 10:53:02 2007	Laura Lindzey (lindzey)

	* version R1-00s
	BUGS:  
	FILES: LadarCarPerceptor.hh(36120), LadarCarTracking.cc(36120)
	First attempt at variable time step Kalman Filter for car velocity.
	Still needs some tuning, but as best I can tell from replaying
	logs, this helps fix the problem where any delays in the system
	cause a too-large velocity estimate. 

	FILES: LadarCarPerceptor.hh(36168), LadarCarTracking.cc(36168)
	If car moving slowly, no longer use velocity to decide orientation.

Thu Aug 23  5:05:59 2007	Laura Lindzey (lindzey)

	* version R1-00r
	BUGS:  
	FILES: LadarCarTracking.cc(35078), emapViewer.cc(35078)
	* viewer no longer plots occupancy map flipped	
	* all objects sent
	to map now include elevation

Tue Aug 21 19:32:17 2007	Laura Lindzey (lindzey)

	* version R1-00q
	BUGS:  
	New files: emapViewer.cc
	FILES: LadarCarMap.cc(34792), LadarCarPerceptor.cc(34792),
		LadarCarPerceptor.hh(34792), LadarCarTracking.cc(34792),
		cmdline.ggo(34792)
	adding command-line option to control how often messages are sent
	to mapper; fixing mistake in occupancy map

Mon Aug 20 23:42:26 2007	Laura Lindzey (lindzey)

	* version R1-00p
	BUGS:  
	FILES: LadarCarMap.cc(32942), LadarCarPerceptor.cc(32942),
		LadarCarPerceptor.hh(32942), LadarCarTracking.cc(32942)
	the free-space map now compiles and runs with ladar-car-perceptor
	(but ladar-car-perceptor doesn't actually use it ... just using it
	to debug the free-space map)

	FILES: LadarCarMap.cc(33712), LadarCarPerceptor.cc(33712),
		LadarCarPerceptor.hh(33712), LadarCarTracking.cc(33712),
		Makefile.yam(33712)
	first cut at using tamas's map rather than my own (sloooow) attempt

	FILES: LadarCarMap.cc(34087), LadarCarPerceptor.cc(34087),
		LadarCarPerceptor.hh(34087), cmdline.ggo(34087)
	making map smaller (actually works better w/ coarser resolution)
	adding cmd line option to turn off displaying occupancy map

	FILES: LadarCarMap.cc(34495), LadarCarPerceptor.hh(34495),
		LadarCarTracking.cc(34495), Makefile.yam(34495)
	now using the released version of Tamas's emap class for the
	occupancy map. Cleaned out all the old, slow code. The occupancy
	map was tested on Alice, and provided a significant reduction in
	false positives. 

	FILES: LadarCarPerceptor.hh(34081), LadarCarTracking.cc(34081)
	changes to occupancy map - reduces number of false positives, but
	not yet great

	FILES: LadarCarPerceptor.hh(34221), LadarCarTracking.cc(34221)
	trying to add point3 to ladar-car-perceptor

	FILES: Makefile.yam(34162)
	adding framework - the new emap stuff now works on my computer

Mon Aug  6  9:53:23 2007	Laura Lindzey (lindzey)

	* version R1-00o
	BUGS:  
	FILES: LadarCarPerceptor.cc(32209), LadarCarTracking.cc(32209)
	changes from the field: *Fixing such that obstacles no longer
	flicker between obs/vehicle *now actually works with only roof
	right/left ladars.

Sat Aug  4  8:10:59 2007	Laura Lindzey (lindzey)

	* version R1-00n
	BUGS:  
	FILES: LadarCarPerceptor.cc(31475), LadarCarPerceptor.hh(31475),
		LadarCarTracking.cc(31475), cmdline.c(31475),
		cmdline.ggo(31475), cmdline.h(31475)
	* adding command line option for obstacles to have depth (helps
	fusion)  * getting rid of flickering obstacles when running
	multiple copies of ladar-car-perceptor	* fixing bug where cars
	were sent in hourglass shape  * adding command-line options for 2
	new bumper ladars

	FILES: LadarCarTracking.cc(31486)
	now correctly handles depth when updating one object with multiple
	segments

	FILES: cmdline.c(31961), cmdline.h(31961)
	committing the .c and .h files for gengetopt (don't know why they
	weren't there before)

Thu Jul 26  9:30:24 2007	Laura Lindzey (lindzey)

	* version R1-00m
	BUGS:  
	FILES: LadarCarMap.cc(30234), LadarCarPerceptor.hh(30234),
		LadarCarTracking.cc(30234)
	cleanup of fitCar

	FILES: LadarCarMap.cc(30431), LadarCarPerceptor.hh(30431),
		LadarCarTracking.cc(30431)
	more cleanup, moving almost all the sendmapelement calls to one
	place

	FILES: LadarCarPerceptor.cc(30233), LadarCarPerceptor.hh(30233),
		LadarCarTracking.cc(30233)
	fixing fitCar error when multiple segments are used to update the
	same car (ordering of points matters, and they'd gotten reversed)

	FILES: LadarCarPerceptor.cc(30432), LadarCarPerceptor.hh(30432),
		LadarCarTracking.cc(30432), cmdline.ggo(30432)
	more general code cleanup, and getting rid of now-default command
	line option

	FILES: LadarCarPerceptor.cc(30438), LadarCarPerceptor.hh(30438),
		LadarCarTracking.cc(30438), cmdline.c(30438),
		cmdline.h(30438)
	mainly, committing so I can make a release (tweaked CARMARGIN, ran
	indent in emacs)

	FILES: LadarCarPerceptor.hh(30413), LadarCarTracking.cc(30413)
	minor cleanup, committing to switch computers

	FILES: LadarCarPerceptor.hh(30429), LadarCarTracking.cc(30429)
	more improvements to fitCar, such that association for tracking is
	much cleaner

	FILES: LadarCarPerceptor.hh(30430), LadarCarTracking.cc(30430)
	changing KFupdate() function to use current best corner, rather
	than back right.

Thu Jul 19 19:12:04 2007	Laura Lindzey (lindzey)

	* version R1-00l
	BUGS:  
	FILES: LadarCarPerceptor.cc(29682)
	changes to allow ladar-car-perceptor to properly interact with
	process-control. Now sends heartbeat messages, and quits when
	commanded.

Tue Jul 17 16:36:53 2007	Laura Lindzey (lindzey)

	* version R1-00k
	BUGS:  
	FILES: LadarCarPerceptor.cc(29317), LadarCarPerceptor.hh(29317),
		LadarCarTracking.cc(29317), cmdline.c(29317),
		cmdline.ggo(29317), cmdline.h(29317)
	now send velocity variances, time stopped and a confidence value in
	the mapelements

	FILES: Makefile.yam(29324)
	enabling doxygen

Fri Jul 13 18:57:22 2007	Laura Lindzey (lindzey)

	* version R1-00j
	BUGS:  
	FILES: LadarCarMap.cc(29074), LadarCarTracking.cc(29074)
	adding velocity to sent mapelements

	FILES: LadarCarPerceptor.cc(29073), LadarCarPerceptor.hh(29073),
		LadarCarTracking.cc(29073)
	Changes from the field: * Slow down sending of map elements * send
	points corresponding to objects with both ends occluded * first
	attempt at sending heartbeat message (not working yet)

Thu Jul 12 18:05:30 2007	Laura Lindzey (lindzey)

	* version R1-00i
	BUGS:  
	FILES: LadarCarPerceptor.cc(28877), LadarCarPerceptor.hh(28877),
		LadarCarTracking.cc(28877)
	mainly reformatting provided by sam's emacs config

	FILES: LadarCarPerceptor.cc(28913), LadarCarPerceptor.hh(28913),
		Makefile.yam(28913), cmdline.c(28913), cmdline.ggo(28913),
		cmdline.h(28913)
	compiles under mac osx (I made the changes before, but they got
	lost) added command-line options to allow a separate instance of
	ladar-car-perceptor for each sensor

	FILES: Makefile.yam(28923)
	fixing makefile (had accidentally deleted a library)

Thu Jul 12  9:36:52 2007	Laura Lindzey (lindzey)

	* version R1-00h
	BUGS:  
Mon Jun  4 19:32:48 2007	Laura Lindzey (lindzey)

	* version R1-00g
	BUGS:  
	FILES: LadarCarPerceptor.cc(26401), LadarCarPerceptor.hh(26401),
		LadarCarTracking.cc(26401)
	ladar-car-perceptor now sends objects from current scan that are
	NOT classified as cars (id's are 151.ladar#.obs#) in addition to
	cars (id's are 150.1.car#). Note that while cars are tracked over
	time, obstacles are not, and may be doubled up, if the same
	obstacle appears in multiple scans  also, removing unused code

Fri Jun  1  9:17:35 2007	Laura Lindzey (lindzey)

	* version R1-00f
	BUGS:  
	FILES: LadarCarPerceptor.cc(26041), LadarCarPerceptor.hh(26041),
		LadarCarTracking.cc(26041)
	- only send cars to map at 10Hz - set upper size bound for cars
	(filters out groundstrikes) - throws out points w/ small range (we
	were getting returns from either the ladar itself or it's mount)

Fri Jun  1  7:32:20 2007	Laura Lindzey (lindzey)

	* version R1-00e
	BUGS:  
	FILES: LadarCarPerceptor.hh(25789), LadarCarTracking.cc(25789)
	now shows predicted position of cars on debugging display

	FILES: LadarCarPerceptor.hh(26020), LadarCarTracking.cc(26020)
	tuning parameters for better performance on groundstrikes, in
	particular, those happening when Alice is at a stop. got rid of
	redundant 'center' element in the objectRepresentation struct. 
	works well on logs_26may/{acr1_bm, acr2_bm, acr3_bm}, but still has
	many false positives on acr4_bm. 

Fri May 25  6:56:51 2007	Laura Lindzey (lindzey)

	* version R1-00d
	BUGS:  
	FILES: LadarCarMap.cc(24940), LadarCarPerceptor.cc(24940),
		LadarCarPerceptor.hh(24940), LadarCarTracking.cc(24940),
		cmdline.c(24940), cmdline.ggo(24940), cmdline.h(24940)
	resolved conflicts caused by merge

	FILES: LadarCarMap.cc(23833), LadarCarPerceptor.cc(23833),
		LadarCarPerceptor.hh(23833), LadarCarTracking.cc(23833),
		cmdline.c(23833), cmdline.ggo(23833), cmdline.h(23833)
	improvements to occupancy map, and command line option to turn it
	off

	FILES: LadarCarPerceptor.cc(24046), LadarCarPerceptor.hh(24046),
		LadarCarTracking.cc(24046), cmdline.c(24046),
		cmdline.ggo(24046), cmdline.h(24046)
	-adding ability to log objects -fixed car association 

	FILES: LadarCarPerceptor.hh(24079), LadarCarTracking.cc(24079)
	changes to compile against mapElement changes

	FILES: LadarCarPerceptor.hh(24277), LadarCarTracking.cc(24277)
	changing classify objects so that it only updates each car once per
	scan

	FILES: LadarCarPerceptor.hh(24933)
	changing #defines, committing so I can release ladar-car-perceptor
	for testing Friday

Thu May 24 12:59:08 2007	Sam Pfister (sam)

	* version R1-00c
	BUGS:  
	FILES: LadarCarTracking.cc(24819)
	updated map element setting functions to work with the new map
	interface.

Sat May  5 10:13:19 2007	Laura Lindzey (lindzey)

	* version R1-00b
	BUGS:  
	New files: LadarCarMap.cc LadarCarPerceptor.cc LadarCarPerceptor.hh
		LadarCarTracking.cc
	Deleted files: BlobLadar.cc BlobLadar.hh BlobTracking.cc
	FILES: Makefile.yam(22267)
	actually changing the name in the code

	FILES: Makefile.yam(22272)
	changing executable name to match convention  adding occupancy map
	structure (not functional yet)

Fri May  4 21:22:07 2007	Laura Lindzey (lindzey)

	* version R1-00a
	BUGS:  
	New files: BlobLadar.cc BlobLadar.hh BlobTracking.cc Matrix.cc
		Matrix.hh cmdline.c cmdline.ggo cmdline.h
	FILES: Makefile.yam(22241), README(22241)
	copying over files from blobladar

Fri May  4 20:53:58 2007	Laura Lindzey (lindzey)

	* version R1-00
	Created ladar-car-perceptor module.





























