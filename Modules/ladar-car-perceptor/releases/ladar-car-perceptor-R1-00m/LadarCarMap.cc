/**
 * File: LadarCarMap.cc
 * Description: (see p. 110)
 *   A simple grid-map that tracks freespace
 *   (Ambrus convinced me to give this a try)
 *   The purpose is to add another check before deciding that an object is
 *   actualy moving (if it's in what this map thinks is free space, it's moving
 *   otherwise, it's probably just poor association/wall partially occluded
 *   
 *   All cells start out with value 0, value is decremented if cell is
 *   observed to be free, incremented if observed to be occupied
 *
 * Last Changed: May 5
 **/

#include "LadarCarPerceptor.hh"
using namespace std;

void LadarCarPerceptor::mapInit()
{
  vector<double> mapRow(MAPSIZE,0.0);
  //map starts out as square grid of 0's
  for(int i=0; i < MAPSIZE; i++) 
    {
       occupancyMap.push_back(mapRow);
    }

  //Center map at 0,0 - at first iteration, it will be shifted to alice's position
  //(I don't think that we have state info when init is called)
  mapCenterX = 0;
  mapCenterY = 0;

  //testing the file saving ability...
  mapPrint();

  return;
}

/**
 * This function cycles through each cell in the map, updating them if
 * necessary. 
 **/
void LadarCarPerceptor::mapUpdate()
{
  if(5 >= numFrames) //TODO: WHY on the first iteration do we run processScan 5 times??
    {
      //      fprintf(stderr, "at first frame - shifting origin \n");
      mapCenterX = currState.X;
      mapCenterY = currState.Y;
    }
  //first, move map center to Alice's origin
  while(MAPCELLDIMENSION < (currState.X - mapCenterX))
    {
      mapShiftPosX();
      //      fprintf(stderr, "shifted map center +x to %f (frame = %d) \n", mapCenterX, numFrames);
    }
  while(-MAPCELLDIMENSION > (currState.X - mapCenterX))
    {
      mapShiftNegX();
      //      fprintf(stderr, "shifted map center -x \n");
    }
  while(MAPCELLDIMENSION < (currState.Y - mapCenterY))
    { 
      mapShiftPosY();
      //      fprintf(stderr, "shifted map center +y \n");
    }
  while(-MAPCELLDIMENSION > (currState.Y - mapCenterY))
    {
      mapShiftNegY();
      //      fprintf(stderr, "shifted map center -y \n");
    }



  //now, update the cells
  double cellX, cellY, cellTheta, dTheta, cellDist;
  int scan_index;
  int updateCount = 0;
  for(int i=0; i<MAPSIZE; i++) 
    {
      cellX = mapCenterX + MAPCELLDIMENSION * (i - MAPCENTERINDEX);
      for(int j = 0; j < MAPSIZE; j++)
	{
	  cellY = mapCenterY + MAPCELLDIMENSION * (j - MAPCENTERINDEX);
#warning "HACK - somewhere, my angle calculations are messed up. this makes it match the scan"
	  cellTheta = atan2(cellY - ladarState.Y, cellX - ladarState.X) - 3*PI/180;
	  dTheta = cellTheta - ladarState.Theta;
	  while(dTheta > PI)
	    dTheta -= PI;
	  while(dTheta < -PI)
	    dTheta += PI;

	  //if we expect this point to be in range of current ladar unit
#warning "This won't work for Riegl unit";
	  if(0 < cos(dTheta))
	    {
	      cellDist = dist(cellX, cellY, ladarState.X, ladarState.Y);
	      scan_index = (int)round(180*(PI/2 + dTheta)/PI); 
	      if((0 > scan_index) || (NUMSCANPOINTS <= scan_index))
		{
		  fprintf(stderr, "whoops! ln 84 in CarMap, index out of range \n");
		  fprintf(stderr, "(i,j) = (%d, %d), and scan_index = %d \n", i, j, scan_index);
		} else if(.5*pow(2,.5)*MAPCELLDIMENSION > fabs(cellDist - rawData[scan_index][RANGE]))
		{
		  //there was a return inside this cell
		  mapUpdateElement(i,j,MAPOBJECTINCREMENT);
		  updateCount++;

		} else if(cellDist < rawData[scan_index][RANGE])
		{
		  //there was a return farther away than this cell
		  //TODO: may want to adjust this for no-return points...
		  mapUpdateElement(i,j,MAPFREESPACEINCREMENT);
		  updateCount++;
		}
	    } //end checking in range
	}//end cycling through Y direction
    } // end cycling through X direction
  //  fprintf(stderr, "ladar state = %f, %f.  Alice state = %f, %f \n", ladarState.X, ladarState.Y, currState.X, currState.Y);
  //  fprintf(stderr, "in map update, touched %d cells \n", updateCount);
}

/**
 * This function increments the cell at index x,y by delta
 **/
void LadarCarPerceptor::mapUpdateElement(int x, int y, double delta)
{
  occupancyMap[y][x] += delta;
}

/**
 * This function shifts the center of the map MAPCELLSIZE in the positive
 * X direction
 *
 * In each row, remove element at start and push new one at end
 **/
void LadarCarPerceptor::mapShiftPosX()
{
  mapCenterX += MAPCELLDIMENSION;
  for(int i=0; i < MAPSIZE; i++) 
    {
      occupancyMap.at(i).push_back(0.0);
      vector<double>::iterator tempIterator = occupancyMap.at(i).begin();
      occupancyMap.at(i).erase(tempIterator);
    }
}


/**
 * This function shifts the center of the map MAPCELLSIZE in the negative
 * X direction.
 *
 * In each row, remove element at end, and push new one at beginning
 **/
void LadarCarPerceptor::mapShiftNegX()
{
  mapCenterX -= MAPCELLDIMENSION;
  for(int i=0; i < MAPSIZE; i++) 
    {
      occupancyMap.at(i).pop_back();
      vector<double>::iterator tempIterator = occupancyMap.at(i).begin();
      occupancyMap.at(i).insert(tempIterator, 0.0);
    }
}

/**
 * This function shifts the center of the map mapcellsize in the 
 * positive Y direction.
 *
 * removes row at start, inserts new one at end
 **/
void LadarCarPerceptor::mapShiftPosY()
{
  mapCenterY += MAPCELLDIMENSION;
  vector<double> mapRow(MAPSIZE,0.0);
  occupancyMap.push_back(mapRow);

  vector<vector<double> >::iterator tempIterator = occupancyMap.begin();
  occupancyMap.erase(tempIterator);
}


/**
 * Shifts the center of the map mapcellsize m in the negative Y direction
 *
 * removes row at end, adds new one at start
 **/
void LadarCarPerceptor::mapShiftNegY()
{
  mapCenterY -= MAPCELLDIMENSION;
  vector<double> mapRow(MAPSIZE,0.0);
  occupancyMap.pop_back();

  vector<vector<double> >::iterator tempIterator = occupancyMap.begin();
  occupancyMap.insert(tempIterator,1,mapRow);
}

/**
 * This function is used to test the above operations on the map
 **/
void LadarCarPerceptor::mapTest() 
{
  mapUpdateElement(2,2,2);
  fprintf(stderr, "after incrementing element at (2,2), map is: \n");
  mapPrintTest();

  mapShiftNegX();
  fprintf(stderr, "after shift to neg x, map is: \n");
  mapPrintTest();

  mapShiftNegY();
  fprintf(stderr, "after shift to neg Y, map is: \n");
  mapPrintTest();

  mapShiftPosX();
  fprintf(stderr, "after shift to pos X, map is: \n");
  mapPrintTest();

  mapShiftPosY();
  fprintf(stderr, "after shift to pos Y, map is: \n");
  mapPrintTest();

}

void LadarCarPerceptor::mapPrintTest()
{
  fprintf(stderr, "    %f, %f, %f, %f, %f \n", occupancyMap[4][0], occupancyMap[4][1], occupancyMap[4][2], occupancyMap[4][3], occupancyMap[4][4]);
  fprintf(stderr, "    %f, %f, %f, %f, %f \n", occupancyMap[3][0], occupancyMap[3][1], occupancyMap[3][2], occupancyMap[3][3], occupancyMap[3][4]);
  fprintf(stderr, "    %f, %f, %f, %f, %f \n", occupancyMap[2][0], occupancyMap[2][1], occupancyMap[2][2], occupancyMap[2][3], occupancyMap[2][4]);
  fprintf(stderr, "    %f, %f, %f, %f, %f \n", occupancyMap[1][0], occupancyMap[1][1], occupancyMap[1][2], occupancyMap[1][3], occupancyMap[1][4]);
  fprintf(stderr, "    %f, %f, %f, %f, %f \n", occupancyMap[0][0], occupancyMap[0][1], occupancyMap[0][2], occupancyMap[0][3], occupancyMap[0][4]);
}


/**
 *gah. not going to try for now...

void LadarCarPerceptor::mapDisplay()
{
  GLuint texture;
  glGenTextures(1,&texture);
  glBindTexture(GL_TEXTURE_2D, texture); //what's this parameter??

  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE); //not sure this is what I need...maybe GL_DECAL

  glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB, MAPSIZE, MAPSIZE, 0, GL_RGB, GL_UNSIGNED_BYTE, occupancyMap);

}
**/


void LadarCarPerceptor::mapPrint()
{
  FILE * tempfile;
  char fileName[256];
  sprintf(fileName, "image_%d.ppm", numFrames);

  double mapval;
  double prob;
  int colorval;

  int tempMap[MAPSIZE][MAPSIZE] = {0};

  double tempX, tempY;
  int indexX, indexY;
  //  int carIndex;
  //  double dX, dY;
  //printing out cars
  for(unsigned int i=0; i < usedCars.size(); i++) 
    {
      int j = usedCars.at(i);
      tempX = cars[j].corners[BACK_RIGHT].x;
      tempY = cars[j].corners[BACK_RIGHT].y;
      indexX = MAPCENTERINDEX + (int)round((mapCenterX - tempX)/MAPCELLDIMENSION);
      indexY = MAPCENTERINDEX + (int)round((tempY - mapCenterY)/MAPCELLDIMENSION);
	  //checking that point belongs in map
      if((0 <= min(indexY, indexX)) && (MAPSIZE > max(indexX, indexY))) 
	{
          tempMap[indexX-1][indexY-1] = -1;
          tempMap[indexX-1][indexY] = -1;
          tempMap[indexX-1][indexY+1] = -1;
          tempMap[indexX][indexY-1] = -1;
          tempMap[indexX][indexY] = -1;
          tempMap[indexX][indexY+1] = -1;
          tempMap[indexX+1][indexY-1] = -1;
          tempMap[indexX+1][indexY] = -1;
          tempMap[indexX+1][indexY+1] = -1;
	  fprintf(stderr, "trying to print car! \n");
 	}

    }

  //printing out scan points
  for(int i=0; i < NUMSCANPOINTS; i++) 
    {
      if(rawData[i][RANGE] < 80.0)
	{
	  tempX = xyzData[i][0];
	  tempY = xyzData[i][1];

          indexX = MAPCENTERINDEX + (int)round((mapCenterX - tempX)/MAPCELLDIMENSION);
          indexY = MAPCENTERINDEX + (int)round((tempY - mapCenterY)/MAPCELLDIMENSION);
	  //checking that point belongs in map
          if((0 <= min(indexX, indexY)) && (MAPSIZE > max(indexX, indexY))) 
	    {
              tempMap[indexX][indexY] = 1;
	    }
	}
    }

  tempfile = fopen(fileName, "w");
  fprintf(tempfile, "P3 256 256 255\n"); //header for ppm file
  for(int j =0; j < MAPSIZE; j++)
    {
      for(int i = 0; i < MAPSIZE; i++)
	{
	  mapval = occupancyMap[j][i];
	  prob = 1-1/(1+exp(mapval));
	  colorval = (int)round(255*prob);
	  if(tempMap[i][j] == 1){
  	    fprintf(tempfile, "%d %d %d ", 0,255,0 );
	  } else if(tempMap[i][j] == -1) {
  	    fprintf(tempfile, "%d %d %d ", 255,0,0);
	  } else {
	    fprintf(tempfile, "%d %d %d ", colorval, colorval, colorval);
	  }

	}
      fprintf(tempfile, "\n");
    }

  fclose(tempfile);

}

double LadarCarPerceptor::checkMap(double x, double y)
{
  int indexX, indexY;

  indexX = MAPCENTERINDEX + (int)round((mapCenterX - x)/MAPCELLDIMENSION);
  indexY = MAPCENTERINDEX + (int)round((y - mapCenterY)/MAPCELLDIMENSION);

  if((0 > min(indexX, indexY)) || (MAPSIZE <=  max(indexX, indexY))) 
    {
      return 1;
    }

  double mapval = occupancyMap[indexY][indexX];
  double  prob = 1-1/(1+exp(mapval));

  return prob;

  if(prob < MAPOCCUPIEDTHRESHOLD)
    {
      return 1;
    }  else {
      return 0;
    }

}
