/**
 * File: LadarCarTracking.cc
 * Description:
 *   Uses bumper ladar to track objects.
 *   This file has the functions handling all
 *   LADAR interfaces, as well as initializing
 *   everything and running the main loop
 * Based on the Obsperceptor template by Andrew
 * Last Changed: July 12
 **/

#include "LadarCarPerceptor.hh"
using namespace std;

int LadarCarPerceptor::trackerInit()
{
  numFrames = 0;
  numObjects = 0;

  //first element of vector identifying map element
  mapCarID = 150;  
  mapObjID = 151;

  //second element of vector identifying map element
  moduleObjectID = 0;
  moduleCarID = 1;
  moduleEllipseID = 2;
  moduleHistoryID = 3; 
  objectVelocityID = 4;
  carVelocityID = 5;
  modulePointsID = 6;
  moduleSegmentsID = 7;
  moduleSegmentsID2 = 8;
  moduleCarBoundID = 9;
  moduleCarSeg1ID = 10;
  moduleCarSeg2ID = 11;
  carKFVelocityID = 12;
  predictedCarID = 13;
  moduleCarHistoryID = 14;

  initSendMapElement(skynetKey);


  DGCgettime(lastSentCars);

  //initializes the stack of open positions in the car array
  for(int i=(MAXNUMOBJS-1);i>=0; i--){
    openObjs.push_back(i);
    openCars.push_back(i);
  }

  //setting up matrics for KF calculations
  //#warning "these need to be tuned!"
  double dt = .0133; 
  double sa = .05; //std dev of system noise -- tune!
  double sz = .1; //std dev of measurement noise -- should be fxn of range/theta!!!

  A.resetSize(2,2);
  double Aelems[] = {1,dt,0,1};
  A.setelems(Aelems);

  C.resetSize(1,2);
  double Celems[] = {1,0};
  C.setelems(Celems);

  I.resetSize(2,2);
  double Ielems[] = {1,0,0,1};
  I.setelems(Ielems);

  R.resetSize(2,2);
  double Relems[] = {0,0,0,sa};
  R.setelems(Relems);

  Q.resetSize(1,1);
  double Qelems[] = {sz};
  Q.setelems(Qelems);

  //call the map init
  if(useMap)
    {
      mapInit();
      mapTest();
    }

  //TODO: actually add error checking 
  return 0;
}


void LadarCarPerceptor::processScan(int ladarID)
{
  //  fprintf(stderr, "processScan called for ladar # %d \n", ladarID);
  numFrames++;

  unsigned long long startTime, endTime;
  unsigned long long origStartTime;

  double diff;

  DGCgettime(origStartTime);

  segmentScan(); 
  createObjects();
  classifyObjects();

  DGCgettime(startTime);
  if(useMap) 
    {
      //  fprintf(stderr, "calling map update... \n");
      if(5 < numFrames) 
        {
          mapUpdate();
          //      mapPrint();
        }
      
      DGCgettime(endTime);
      diff = 1.0*(endTime - startTime);
      fprintf(stderr, "time elapsed for mapupdate = %f \n", diff);
    }

  checkMotion();

  //useDisplay is whether to send ALL my debug info to the map
  if(useDisplay) {
    sendObjects();
  }

  DGCgettime(startTime);
  if(100000 < (startTime - lastSentCars)) {
    sendCars();
    if(useCar2obj) {
      sendTplanner();
    }
    DGCgettime(lastSentCars);
  }

  sendSegments();

  cleanObjects();
  cleanCars();
  
  if(useLogging) {
    writeLogs();
  }

  KFupdate();
  KFpredict();


  DGCgettime(endTime);
  diff = 1.0*(endTime - origStartTime);
  //  fprintf(stderr, "time elapsed for whole process = %f \n \n", diff);


  return;
}


/**
 * This function segments the scan, breaking wherever there is 
 * a discontinuity. (GAPDISTANCE is the parameter in the .hh
 * file controlling how large a discontinuity is permissible)
 * The code will skip discontinuities caused by up to two 
 * no-return points, and only reports segments consising of 
 * MINNUMPOINTS or more points.
 **/
void LadarCarPerceptor::segmentScan()
{

  for(int i=0; i<NUMSCANPOINTS-1; i++) {
    diffs[i] = rawData[i+1][RANGE] - rawData[i][RANGE];
  }
  for(int i=0; i<NUMSCANPOINTS-2; i++) {
    diffs2[i] = rawData[i+2][RANGE] - rawData[i][RANGE];
  }
  for(int i=0; i<NUMSCANPOINTS-3; i++) {
    diffs3[i] = rawData[i+3][RANGE] - rawData[i][RANGE];
  }

  //number objects segmented from curr scan
  int tempNum = 0; 
  //size of current object
  int currSize = 1; 

  int tempPos[NUMSCANPOINTS][2];
  int tempSize[NUMSCANPOINTS];

  tempSize[0] = 0;

  double currRange, currRange2, currRange3;
  double distThresh, distThresh2, distThresh3;

  int noReturns = 0;

  //can't check last 2 points - they wouldn't become an obj anyways
  for(int i=0; i<NUMSCANPOINTS-3; i++) {
    if((rawData[i][RANGE] < MINRANGE) &&(rawData[i][RANGE] > MAXRANGE))
      noReturns++;
    //threshold scaled based on range
    currRange = min(rawData[i][RANGE],rawData[i+1][RANGE]);
    distThresh = sqrt(max(0,pow(GAPDISTANCE,2) - pow(currRange*SINTHETA,2)));

    //this angle is 2deg, and at such small angles sin(2*th) ~= 2*sin(th)
    currRange2 = min(rawData[i][RANGE],rawData[i+2][RANGE]);
    distThresh2 = sqrt(max(0,pow(GAPDISTANCE,2) - pow(currRange2*2*SINTHETA,2)));

    currRange3 = min(rawData[i][RANGE],rawData[i+3][RANGE]);
    distThresh3 = sqrt(max(0,pow(GAPDISTANCE,2) - pow(currRange3*3*SINTHETA,2)));

    bool thisPtHasReturn, nextPtHasReturn, next2PtHasReturn;
    thisPtHasReturn = (rawData[i][RANGE] < MAXRANGE ) && (rawData[i][RANGE] > MINRANGE);
    nextPtHasReturn = (rawData[i+1][RANGE] < MAXRANGE ) && (rawData[i+1][RANGE] > MINRANGE);
    next2PtHasReturn = (rawData[i+2][RANGE] < MAXRANGE ) && (rawData[i+2][RANGE] > MINRANGE);

    bool thisDiffInRange, nextDiffInRange, next2DiffInRange;  
    thisDiffInRange = fabs(diffs[i]) < distThresh;
    nextDiffInRange = fabs(diffs2[i]) < distThresh2;
    next2DiffInRange = fabs(diffs3[i]) < distThresh3;

    //eventually, need some idea of convexity?
#warning "this won't catch all cases where you have two cars < 1.5 m apart"
    //only skip no-return points
    if( !thisPtHasReturn ||
        ( !thisDiffInRange &&
          ((nextPtHasReturn && nextDiffInRange) || !nextDiffInRange) &&
          (((nextPtHasReturn || next2PtHasReturn) && next2DiffInRange) || !next2DiffInRange))) {
      
      currSize = 1;
      tempNum++;
 
      //set endpoint of previous object
      tempPos[tempNum-1][1] = i; 

      //set beginning point of new object
      tempPos[tempNum][0] = i+1;

      //initialize size of new object
      tempSize[tempNum] = currSize;


    } else { //this point belongs to current object

      //If we're skipping over a point, need to skip the index
      if(!thisDiffInRange) {
       	i++;
        //also need to inc currSize, which is used to index points in obj
        currSize++;
        //	fprintf(stderr, "skipped 1 incrementing i to %d. diffs1 = %f, diffs2 = %f \n", i, diffs[i-1], diffs2[i-1]);
        //	fprintf(stderr, "range = %f. thresholds: %f, %f, %f \n", rawData[i-1][RANGE],distThresh, distThresh2, distThresh3);
        if(!nextDiffInRange) {
          i++;
          currSize++;
          //  	  fprintf(stderr, "skipped 2...incrementing i to %d \n", i);
        }
      }
      currSize++;
      tempSize[tempNum] = currSize;

    } //end checking diff

  } //end cycling through points
  //  fprintf(stderr, "There were %d no returns in the scan \n", noReturns);


  tempPos[0][0] = 0;
  tempPos[tempNum][1] = NUMSCANPOINTS-1;

  int st,en;
  numSegments = 0;

  //checking that this isn't a group of no-return points
  for(int i=0; i<=tempNum; i++) {
    if(tempSize[i] >= MINNUMPOINTS) {
      st = tempPos[i][0];
      en = tempPos[i][1];
      posSegments[numSegments][0]=st;
      posSegments[numSegments][1]=en;
      sizeSegments[numSegments] = tempSize[i];
      numSegments++;
    }
  }


  /**
   * the below code plots the segments, for use in debugging/tuning
   * the segmentScan function
   **/
  if(useDisplay) {
    double x1[numSegments];
    double x2[numSegments];
    double y1[numSegments];
    double y2[numSegments];
    double x3[numSegments];
    double y3[numSegments];
    int tmpindex;
  
    MapElement el;
    vector <point2> scanPoints;
    int bytesSent;
    scanPoints.clear();
    for(int i=0; i<NUMSCANPOINTS; i++) {
      scanPoints.push_back(point2(xyzData[i][0],xyzData[i][1]));
    }
    el.clear();
    el.setId(mapCarID,modulePointsID,1);
    el.setTypePoints();
    el.setGeometry(scanPoints);
    el.plotColor = MAP_COLOR_GREEN;
    el.state = rawState;

    bytesSent = sendMapElement(&el, debugSubgroup);
  
    vector <point2> segmentStartPoints, segmentEndPoints;
    segmentStartPoints.clear();
    segmentEndPoints.clear();

    for(int i=0; i<numSegments;i++) {
  
      x1[i]=ladarState.X;
      y1[i]=ladarState.Y;
      tmpindex = posSegments[i][0];
      x2[i]=xyzData[tmpindex][0];
      y2[i]=xyzData[tmpindex][1];
      segmentStartPoints.push_back(point2(x1[i],y1[i]));
      segmentStartPoints.push_back(point2(x2[i],y2[i]));
      tmpindex = posSegments[i][1];
      x3[i]=xyzData[tmpindex][0];
      y3[i]=xyzData[tmpindex][1];
      segmentEndPoints.push_back(point2(x1[i],y1[i]));
      segmentEndPoints.push_back(point2(x3[i],y3[i]));
    }


    el.clear();

    el.setTypeObstacle();
    el.height=2;
    el.setId(mapCarID,moduleSegmentsID);
    el.setGeometry(segmentStartPoints);
    el.plotColor = MAP_COLOR_BLUE;
    el.height=2;

    //    bytesSent = sendMapElement(&el, debugSubgroup);
  
    el.clear();

    el.setTypeObstacle();
    el.height=2;
    el.setId(mapCarID,moduleSegmentsID2);
    el.setGeometry(segmentEndPoints);
    el.plotColor = MAP_COLOR_CYAN;
    el.height=2;
    //    bytesSent = sendMapElement(&el, debugSubgroup);

  }

  // end of plotting functionality

  return;
}


/**
 * This code goes through all the new segments
 * and creates new objects for them. 
 * This includes calculating the occlusions at each edge,
 * calculating the center (excluding no-return points),
 * and storing the relevant x,y,z/range,theta data
 *
 * Fields filled in for objectRepresentation:
 *   dStart, dEnd, seenStart, seenEnd, startPoint, endPoint
 *   lastScan, center, el
 *
 * Parameters defined in .hh file that change operation:
 *   ONE_END_IN_FOREGROUND vs. BOTH_ENDS_IN_FOREGROUND
 **/
void LadarCarPerceptor::createObjects() {

  numNewObjects = 0;

  int objStart, objEnd;
  double dStart, dEnd;

  point2arr tmparr;

  for(int i=0; i<numSegments; i++) 
    {
      tmparr.clear();
      int objectsize = sizeSegments[i];

      objStart = posSegments[i][0];
      objEnd = posSegments[i][1];

      //if dStart or dEnd > 0, that means that that end of 
      //the object is in the foreground, <0 means the 
      //object next to it is in the foreground, and 0 means 
      //that it's at the edge of a scan
      if(objStart == 0) {
        dStart = 0;
      } else {
        dStart = -diffs[objStart - 1];
      }
      if(objEnd == NUMSCANPOINTS - 1) {
        dEnd = 0;
      } else {
        dEnd = diffs[objEnd];
      }

      //will ignore any segment w/ both(either) ends in 
      //background
#ifdef ONE_END_IN_FOREGROUND
      if((dStart > 0) || (dEnd > 0)) {
#endif
#ifdef BOTH_ENDS_IN_FOREGROUND
        if((dStart > 0) && (dEnd > 0)) {
#endif

          newObjects[numNewObjects].dStart = dStart;
          newObjects[numNewObjects].dEnd = dEnd; 

          newObjects[numNewObjects].startIndex = objStart;
          newObjects[numNewObjects].endIndex = objEnd;

          newObjects[numNewObjects].startPoint = point2(xyzData[objStart][0],xyzData[objStart][1]);
          newObjects[numNewObjects].endPoint = point2(xyzData[objStart + objectsize - 1][0],xyzData[objStart+objectsize - 1][1]);

          if(0 != dStart) {
            newObjects[numNewObjects].seenStart = (int) (dStart / fabs(dStart));
          } else {
            newObjects[numNewObjects].seenStart = 0;
          }
          if(0 != dEnd) {
            newObjects[numNewObjects].seenEnd = (int) (dEnd / fabs(dEnd));
          } else {
            newObjects[numNewObjects].seenEnd = 0;
          }
          //      fprintf(stderr, "for obj %d, dStart and dEnd: %f, %f \n", numNewObjects, dStart, dEnd);
          //      fprintf(stderr, "....and seenStart, seenEnd: %f, %f \n", dStart / fabs(dStart), dEnd / fabs(dEnd));
          point2arr objpts;
          vector<double> rngs;
          vector<double> angs;

          point2 tmppt;

#warning "keeping this many points may be a problem"
          for(int m=objectsize-1;m>=0;m--) {
            //don't include no-return points in average
            double temprange = rawData[m+objStart][RANGE];
            if((MAXRANGE > temprange) && (MINRANGE < temprange)) {
              tmppt.x = xyzData[m+objStart][0];
              tmppt.y = xyzData[m+objStart][1];
              objpts.push_back(tmppt);
              rngs.push_back(rawData[m+objStart][RANGE]);
              angs.push_back(rawData[m+objStart][ANGLE]);
            }
          }

          //including depth in the objects 
          /**
             for(int m=0;m<objectsize;m++) {
             tmp.x = depthData[m+objStart][0];
             tmp.y = depthData[m+objStart][1];
             tmppts.push_back(tmp);
             }
          **/

          newObjects[numNewObjects].lastScan = objpts;
  
          //      newObjects[numNewObjects].center = objpts.average();
          newObjects[numNewObjects].el = ellipse(objpts);

          numNewObjects++; 

        }   //end chekcing fg/bg
      }   //end cycling through segments
    }   //end of createObjects()



  /**
   * This function assumes that the scan has been segmented,
   * and classifies the new objects into: 
   *    already-seen object
   *    already-seen car
   *    new object 
   * Then, it calls the appropriate functions to update
   * the representations.
   *
   * Parameters:
   *   USE_PREDICTION_FOR_ASSOCIATION 
   *
   * Improvements needed:
   *  1) Only update at end of cycle, not each time - do 
   *     this when I find data set requiring it
   *     20070302_ladar_v4/Tintersection/t2_bm, near the end
   **/
  void LadarCarPerceptor::classifyObjects() 
    {

      for(int i=0; i<MAXNUMCARS; i++) {
        carsToUpdate[i].clear();
      }
      for(int i=0; i<MAXNUMOBJS; i++) {
        objsToUpdate[i].clear();
      }

      double distTraveled;
      int inside1, inside2;

      //whether this segment has already been assigned
      int taken[numSegments];
      for(int i=0;i<numNewObjects;i++) {
        taken[i] = 0;
      }


      for(int i = 0; i<61; i++) {
        matchedCar[i] = 0;
      }

      double oldX, oldY;
      double newX, newY;

      /**
       * First, we cycle through all previously seen objects 
       * to see if the new object matches one of them
       **/
      for(unsigned int i=0; i<usedObjs.size(); i++) {
        int k = usedObjs.at(i);
        oldX = objects[k].el.center.x;
        oldY = objects[k].el.center.y;

        //    fprintf(stderr, "old obj position: %f %f \n", oldX, oldY);
        for(int j=0; j<numNewObjects; j++) {
          if(taken[j] == 0) { //if new object hasn't already been classified
            newX = newObjects[j].el.center.x;
            newY = newObjects[j].el.center.y;
            distTraveled = dist(oldX, oldY, newX, newY);

            //Check both ways, in case we're matching a 
            //fragment to a larger object
            inside1 = objects[k].el.inside(newObjects[j].el.center, MARGIN);
            inside2 = newObjects[j].el.inside(objects[k].el.center, MARGIN);

            //	fprintf(stderr, "in classify objects, oldobj %d, newobj %d, inside1 = %d, inside2 = %d \n", k, j, inside1, inside2);
            if((1 == inside1) || (1 == inside2)) {
              taken[j] = 1;
              //	  fprintf(stderr,"updating object #%d with new object #%d, at a distance = %f\n",k, j, distTraveled, inside1, inside2);
              objsToUpdate[k].push_back(j);

            } //end checking if they match
          } //end checking of newObj is taken
        } //end cycling through new objects
      } //end cycling through old objects


      /**
       * Next, we cycle through all previously seen cars 
       * to see if obj matches. This functionality goes 
       * AFTER matching newobjs to oldobjs cuz we don't want 
       * to match an already seen lamppost in the car's path 
       * to the car...
       **/

      double dx1, dy1, dx2, dy2;

      line2 side1, side2, side3, side4;
      point2 br, bl, fr, fl;

      point2 newPt;

      for(unsigned int i=0; i<usedCars.size(); i++) {

        int k = usedCars.at(i);
#ifdef USE_PREDICTION_FOR_ASSOCIATION
        oldX = cars[k].Xmu_hat.getelem(0,0);
        oldY = cars[k].Ymu_hat.getelem(0,0);
#else
        oldX = cars[k].X;
        oldY = cars[k].Y;
#endif
        dx1 = cars[k].dx1;
        dy1 = cars[k].dy1;
        dx2 = cars[k].dx2;
        dy2 = cars[k].dy2;

        point2 old_r1 = point2(dx1, dy1);
        point2 old_r2 = point2(dx2, dy2);
        point2 old_corner = point2(oldX, oldY);
        double diag = atan2(dy1+dy2, dx1+dx2);
        point2 new_corner = old_corner - CARMARGIN*point2(cos(diag),sin(diag));
        point2 new_r1 = ((2*CARMARGIN + old_r1.norm())/old_r1.norm())*old_r1;
        point2 new_r2 = ((2*CARMARGIN + old_r2.norm())/old_r2.norm())*old_r2;

        point2arr boundingbox;
        boundingbox.push_back(new_corner);
        boundingbox.push_back(new_corner+new_r1);
        boundingbox.push_back(new_corner+new_r1+new_r2);
        boundingbox.push_back(new_corner+new_r2);

        //    fprintf(stderr, "bounding box: \n  corner#1: %f, %f \n  corner#2: %f, %f \n  corner#3: %f, %f \n  corner#4: %f, %f \n", new_corner.x, new_corner.y, new_corner.x+new_r1.x, new_corner.y+new_r1.y, new_corner.x+new_r1.x+new_r2.x,new_corner.y+new_r1.y+new_r2.y, new_corner.x+new_r2.x, new_corner.y+new_r2.y);


        for(int j=0; j<numNewObjects; j++) {
          if(taken[j] == 0) { //this obj has not already been claimed
            newX = newObjects[j].el.center.x;
            newY = newObjects[j].el.center.y;
            newPt = point2(newX, newY);

            int isinside = boundingbox.get_inside(newPt);

            //check if this obj matches car
            //	if((0 >= insideLength) && (0 >= insideWidth)) { 
            if(1 == isinside) {
              //the obj and car should be associated
              //	  fprintf(stderr, "pt %f, %f is inside bounding box \n", newX, newY);
              taken[j] = 1;
              //	  fprintf(stderr, "new obj matched car # %d at %f, %ff-- updating!! \n", k, newX, newY);
              carsToUpdate[k].push_back(j);
              matchedCar[j] = 1;
            }  //end of checking if newObj and car match
          }   //end of checking if obj already claimed
        }    //end cycling thru newObjs
      }     //end cycling thru numCars


      /**
       * Finally, we take care of the new objects that don't match anything we've
       * seen before
       **/
      for(int i=0; i<numNewObjects; i++) {
        if(taken[i] == 0) { 
          //      fprintf(stderr, "no match for new obj at position: %f %f \n", newX, newY);
          addObject(&newObjects[i]);
        } //end checking if object already classified
      } //end cycling through numSegments


      //cycle through, and update each object once
      unsigned int length;
      unsigned int j;
      unsigned int l;
      double dStart, dEnd;
      int startIndex = 181;
      int endIndex = -1;

      point2arr newpts;
      for(int i = 0; i < MAXNUMOBJS; i++) {
        length = objsToUpdate[i].size();
        if(0 < length) {
          newpts.clear();
          for(j = 0; j < length; j++) {
            int k = objsToUpdate[i].at(j);

            if(endIndex < newObjects[k].endIndex) {
              endIndex = newObjects[k].endIndex;
              dEnd = newObjects[k].dEnd;
            }
            if(startIndex > newObjects[k].startIndex) {
              startIndex = newObjects[k].endIndex;
              dStart = newObjects[k].dStart;
            }

            //get all points
            point2arr tmppts;
            tmppts = point2arr(newObjects[k].lastScan);
            point2 center = tmppts.average();
            //	fprintf(stderr, "updating object %d, chunk %d, w/ %d points centered at %f, %f \n", i, k, tmppts.size(),center.x, center.y);

            for(l = 0; l < tmppts.size(); l++) {
              newpts.push_back(tmppts.arr[l]);
            }

          }
          //now, update the object
          //      fprintf(stderr, "updating object #%d with %d points. dstart = %f, dend = %f \n", i, newpts.size(), dStart, dEnd);
          updateObject(&objects[i], newpts, dStart, dEnd);
        }
      }

      //NOW update the cars
      objectRepresentation newObject;
      carRepresentation newCar;

      for(int i=0; i<MAXNUMCARS; i++) {
        //reset vars for each new car
        newpts.clear();
        startIndex = 181;
        endIndex = -1;

        length = carsToUpdate[i].size();
        if(0 < length) {
          for(j = 0; j < length; j++) {
            //keep track of occlusions at ends of obj
            int k = carsToUpdate[i].at(j);
            if(endIndex < newObjects[k].endIndex) {
              endIndex = newObjects[k].endIndex;
              dEnd = newObjects[k].dEnd;
            }
            if(startIndex > newObjects[k].startIndex) {
              startIndex = newObjects[k].endIndex;
              dStart = newObjects[k].dStart;
            }

            //get all points
            point2arr tmppts;
            tmppts = point2arr(newObjects[k].lastScan);
            point2 center = tmppts.average();
            //	fprintf(stderr, "updating car %d, chunk %d, w/ %d points centered at %f, %f \n", i, k, tmppts.size(),center.x, center.y);

            for(l = 0; l < tmppts.size(); l++) {
              newpts.push_back(tmppts.arr[l]);
            }

          }
          //now, update the car
          //      fprintf(stderr, "updating car #%d with %d points \n", i, newpts.size());
          updateCar(&cars[i], newpts, dStart, dEnd);

        }
      }



    } // end classifyObjects()

  /**
   * This function cycles through all segments, and sends those that 
   * were not turned into cars
   **/

  void LadarCarPerceptor::sendSegments() {
    int numSent = 0;

    MapElement el;
    vector<point2> objectPoints;
    point2 tmppt;
    int bytesSent;

    int startIndex, endIndex;

    for(int i=0; i<numNewObjects; i++) {
      if(matchedCar[i] == 0) {

        objectPoints.clear();
        startIndex = newObjects[i].startIndex;
        endIndex = newObjects[i].endIndex;

        for(int j=startIndex; j <= endIndex; j++) {
          double temprange = rawData[j][RANGE];
          if((MAXRANGE > temprange) && (MINRANGE < temprange)) {
            tmppt = point2(xyzData[j][0],xyzData[j][1]);
            objectPoints.push_back(tmppt);
          }
        }
        for(int j = endIndex; j>= startIndex; j--) {
          double temprange = rawData[j][RANGE];
          if((MAXRANGE > temprange) && (MINRANGE < temprange)) {
            tmppt = point2(depthData[j][0],depthData[j][1]);
            objectPoints.push_back(tmppt);
          }
        }

        el.setId(mapObjID,currSensor,numSent);
        el.setTypeObstacle();
        el.setGeometry(objectPoints);
        //       fprintf(stderr, "current sensor = %d \n", currSensor);
        if(currSensor == 0) {
          el.plotColor = MAP_COLOR_GREEN;
        } else if(currSensor == 1) {
          el.plotColor = MAP_COLOR_BLUE;
        } else if(currSensor == 2) {
          el.plotColor = MAP_COLOR_CYAN;
        } else {
          el.plotColor = MAP_COLOR_GREY;
        }
        el.state = rawState;
        bytesSent = sendMapElement(&el, debugSubgroup);
        numSent++;
      }
    }

    //if we sent more objects last time, need to remove the extras
    for(int i = numSent; i < numSentSegments[currSensor]; i++) {
      el.setId(mapObjID, currSensor, i);
      el.setTypeClear();
      el.state = rawState;
      bytesSent = sendMapElement(&el, debugSubgroup);

      //     fprintf(stderr,"removing %d map elements \n", numSentSegments[currSensor] - numSent);
    }
    numSentSegments[currSensor] = numSent;
  }




  /**
   * This function is called when it is determined that a newobject 
   * matches an old one. As such, it simply replaces the center position, 
   * and adds the latest scan data
   *
   * Updates the following fields:
   *  dStart, dEnd, seenStart, seenEnd, startPoint, endPoint
   *  el, center, velocity, ptsinvelocity, history
   *  lastScan, timesSeen, lastSeen
   **/
  void LadarCarPerceptor::updateObject(objectRepresentation* oldObj, point2arr points, double dStart, double dEnd) {


    //  fprintf(stderr,"updated object at: %f, %f \n", newObj->N, newObj->E);

    oldObj->dStart = max(oldObj->dStart, dStart);
    oldObj->dEnd = max(oldObj->dEnd, dEnd);
    point2 dObj;

    //  point2arr newscans = point2arr(newObj->scans.back());
    //  vector<double> newranges(newObj->ranges.back());
    //  vector<double> newangles(newObj->angles.back());

    //    fprintf(stderr, "startpoints: %f, %f (new) %f, %f (old) \n", newObj->startPoint.x, newObj->startPoint.y, oldObj->startPoint.x, oldObj->startPoint.y);
    //    fprintf(stderr, "endpoints: %f, %f (new) %f, %f (old) \n", newObj->endPoint.x, newObj->endPoint.y, oldObj->endPoint.x, oldObj->endPoint.y);



	  //  dObj = newObj->center - oldObj->center;
    //  fprintf(stderr, "testing. dObj = %f, %f \n", dObj.x, dObj.y);
	  //  oldObj->el.center = oldObj->el.center + dObj;
	  //  oldObj->center = newObj->center;

    //are these even used anywhere else after this?

    //  oldObj->endPoint = newObj->endPoint;
    //  oldObj->startPoint = newObj->startPoint;
    if(0 != oldObj->seenStart) {
      oldObj->seenStart = (int) (oldObj->dStart / fabs(oldObj->dStart));
    } else {
      oldObj->seenStart = 0;
    }
    if(0!= oldObj->seenEnd) {
      oldObj->seenEnd = (int) (oldObj->dEnd / fabs(oldObj->dEnd));
    } else {
      oldObj->seenEnd = 0;
    }
    //  fprintf(stderr, "called updateObject with dstart = %f, dend = %f. dstart = %f, dend = %f,  seenstart = %d, seenend = %d \n", dStart, dEnd, oldObj->dStart, oldObj->dEnd, oldObj->seenStart, oldObj->seenEnd);

    point2 oldCenter, newCenter;
    oldCenter = oldObj->el.center;
    oldObj->el.add_points(points);
    newCenter = oldObj->el.center;
    dObj = newCenter - oldCenter;

    point2 tempvel = oldObj->ptsinvelocity*oldObj->velocity + dObj;
    oldObj->ptsinvelocity++;
    oldObj->velocity = tempvel/oldObj->ptsinvelocity;

    oldObj->history.push_back(oldObj->el.center);

    oldObj->lastScan = points;
    //  oldObj->ranges.push_back(newranges);
    //  oldObj->angles.push_back(newangles);

    oldObj->timesSeen = oldObj->timesSeen + 1;
    //  fprintf(stderr, "updating object %d. seen %d times \n", oldObj->ID, oldObj->timesSeen);
    oldObj->lastSeen = numFrames;

    //collecting data for training set
    if(oldObj->timesSeen < 30) {
      oldObj->logData[oldObj->timesSeen].el_x = oldObj->el.center.x;
      oldObj->logData[oldObj->timesSeen].el_y = oldObj->el.center.y;
      oldObj->logData[oldObj->timesSeen].el_a = oldObj->el.a;
      oldObj->logData[oldObj->timesSeen].el_b = oldObj->el.b;

      oldObj->logData[oldObj->timesSeen].alice_x = currState.X;
      oldObj->logData[oldObj->timesSeen].alice_y = currState.Y;
      oldObj->logData[oldObj->timesSeen].alice_theta = currState.Theta;

      oldObj->logData[oldObj->timesSeen].vel_x = oldObj->velocity.x;
      oldObj->logData[oldObj->timesSeen].vel_y = oldObj->velocity.y;

      double len = dist(oldObj->endPoint.x, oldObj->endPoint.y, oldObj->startPoint.x, oldObj->startPoint.y);
      oldObj->logData[oldObj->timesSeen].length = len;

      double tempa, tempb;
      point2 t1, t2;
      double standardError = points.fit_line(tempa, tempb, t1, t2);
      oldObj->logData[oldObj->timesSeen].std_linefit = standardError;
    }
  }

  /**
   * This function is called when it is determined that a new object
   * has no match in already seen objects or cars
   * It's job is to add an object to the object array, if there is room.
   *
   * Fields set in object's representation:
   *   ID, velocity, ptsinvelocity, center, el
   *   dStart, dEnd, seenStart, seenEnd, startPoint, endPoint
   *   timesSeen, lastSeen, history, lastScan
   *   Xmu, Ymu, Xsigma, Ysigma, Xmu_hat, Ymu_hat, 
   *     Xsigma_hat, Ysigma-hat
   **/
  void LadarCarPerceptor::addObject(objectRepresentation* newObj) {
    numObjects = numObjects + 1;

    newObj->velocity = point2(0,0);
    newObj->ptsinvelocity = 0;

    //  fprintf(stderr, "entering add object, w/ object # %d \n", numObjects);
    if(usedObjs.size() < MAXNUMOBJS) {

      int newIndex;
      newIndex = openObjs.back();
      openObjs.pop_back();
      usedObjs.push_back(newIndex);

      objects[newIndex].ID = numObjects;
      objects[newIndex].dStart = newObj->dStart;
      objects[newIndex].dEnd = newObj->dEnd;

      objects[newIndex].timesSeen = 1;
      objects[newIndex].lastSeen = numFrames;


      //transferring vectors of scan/range/angle history
      point2arr newscans = point2arr(newObj->lastScan);
      objects[newIndex].lastScan = newscans;
 
      //    objects[newIndex].center = newscans.average();
      objects[newIndex].el = ellipse(newscans);

      objects[newIndex].history.clear();
      objects[newIndex].history.push_back(newscans.average());


      objects[newIndex].seenStart = newObj->seenStart;
      objects[newIndex].seenEnd = newObj->seenEnd;
      objects[newIndex].startPoint = point2(newObj->startPoint);
      objects[newIndex].endPoint = point2(newObj->endPoint);

      //initizlizing KF variables
      objects[newIndex].Xmu.resetSize(2,1);
      objects[newIndex].Ymu.resetSize(2,1);
      objects[newIndex].Xsigma.resetSize(2,2);
      objects[newIndex].Ysigma.resetSize(2,2);
      objects[newIndex].Xmu_hat.resetSize(2,1);
      objects[newIndex].Ymu_hat.resetSize(2,1);
      objects[newIndex].Xsigma_hat.resetSize(2,2);
      objects[newIndex].Ysigma_hat.resetSize(2,2);

      double XMelems[] = {objects[newIndex].el.center.x, 0};
      double YMelems[] = {objects[newIndex].el.center.y, 0};
      double XSelems[] = {1,0,0,1};
      double YSelems[] = {1,0,0,1};

      objects[newIndex].Xmu.setelems(XMelems);
      objects[newIndex].Xsigma.setelems(XSelems);
      objects[newIndex].Ymu.setelems(YMelems);
      objects[newIndex].Ysigma.setelems(YSelems);

    } else {

      fprintf(stderr,"trying to add obj, no space in buffer \n");

    } //end checking that there's enough room to add new object
  } //end addObject


  /**
   * there are 3 cases:
   *   object already classified as car -> was sent as type car (in send cars)
   *   object seen fewer than X times -> send as type car
   *   object seen greater than X times -> send as type obj
   **/

  void LadarCarPerceptor::sendTplanner()
    {
      //cycle through each object

      for(unsigned int k=0; k<usedObjs.size(); k++) {
        int j = usedObjs.at(k);

        MapElement el;
        vector <point2> objectPoints;
        int bytesSent;

        //plotting the actual points

        objectPoints.clear();
        point2arr lastScan = point2arr(objects[j].lastScan);
        point2 tmppt;

        //this is SLOOOWW (Just adding this makes it run < real time)
        for(unsigned int m=0; m < lastScan.size(); m++) {
          tmppt = point2(lastScan.arr[m].x, lastScan.arr[m].y);
          objectPoints.push_back(tmppt);
        }
        el.clear();

        el.setId(mapCarID,moduleObjectID,objects[j].ID);
        if(200 <= objects[j].timesSeen) {
          el.setTypeVehicle();
          el.plotColor = MAP_COLOR_BLUE;
        } else {
          el.setTypeObstacle();
          el.plotColor = MAP_COLOR_GREEN;
        }

        el.setGeometry(objectPoints);
        el.state = rawState;
        bytesSent = sendMapElement(&el, outputSubgroup);

      } // end checking each object

      return;
    } //end of sendTplanner


  /**
   * At this point, this is purely a debugging function,
   * as blobladar is only responsible for detecting and
   * reporting cars, not background objects. 
   **/
  void LadarCarPerceptor::sendObjects() {

    //  fprintf(stderr, "number of objects: %d \n", usedObjs.size());
    int sent = 0;

    for(unsigned int k=0; k<usedObjs.size(); k++) {
      int j = usedObjs.at(k);
  
      sent++;

      MapElement el;
      vector <point2> objectPoints;
      int bytesSent;

      //plotting the actual points

      objectPoints.clear();
      point2arr lastScan = point2arr(objects[j].lastScan);
      point2 tmppt;
      //this is SLOOOWW (Just adding this makes it run < real time)
      for(unsigned int m=0; m < lastScan.size(); m++) {
        tmppt = point2(lastScan.arr[m].x, lastScan.arr[m].y);
        objectPoints.push_back(tmppt);
      }
      el.clear();

      el.setId(mapCarID,moduleObjectID,objects[j].ID);
      el.setTypePoints();
      el.setGeometry(objectPoints);
      el.plotColor = MAP_COLOR_GREEN;
      el.state = rawState;
      bytesSent = sendMapElement(&el, debugSubgroup);
      //      fprintf(stderr, "sent map element (objectPoints)! bytesSent = %d \n", bytesSent);

      //plotting object's velocity
      objectPoints.clear();
      objectPoints.push_back(objects[j].el.center);
      //	point2 velocity = point2(objects[j].Xmu.getelem(1,0),objects[j].Ymu.getelem(1,0));
      point2 velocity = point2(objects[j].velocity);
      objectPoints.push_back(objects[j].el.center + 75*velocity);
      el.clear();

      el.setTypeObstacle();
      el.height=2;
      el.setGeometry(objectPoints);
      el.setId(mapCarID,objectVelocityID,objects[j].ID);
      el.plotColor = MAP_COLOR_RED;
      el.state = rawState;
      bytesSent = sendMapElement(&el, debugSubgroup);
      //      fprintf(stderr, "send map element (ellipse)! bytesSent = %d \n", bytesSent);

      //sending the outline of the ellipse
      point2arr objel = objects[j].el.border(MARGIN);
      objectPoints.clear();
      for(unsigned int i=0; i<objel.size(); i++) {
        objectPoints.push_back(objel[i]);
      }
      el.clear();

      el.setId(mapCarID,moduleEllipseID,objects[j].ID);
      el.setTypePoints();
      el.setGeometry(objectPoints);
      el.plotColor = MAP_COLOR_YELLOW;
      el.state = rawState;
      bytesSent = sendMapElement(&el, debugSubgroup);
      //      fprintf(stderr, "send map element (ellipse)! bytesSent = %d \n", bytesSent);
    

      //sending this object's history
      el.clear();

      el.setId(mapCarID,moduleHistoryID,objects[j].ID);
      el.setTypePoints();
      point2arr tmparr = point2arr(objects[j].history);
      //    fprintf(stderr, "sending history of size: %d. obj's age = %d \n", tmparr.size(), objects[j].timesSeen);
      el.setGeometry(tmparr);
      el.plotColor = MAP_COLOR_MAGENTA;

      //      el.set_poly_obs(id, objects[j].history);
      el.state = rawState;
      bytesSent = sendMapElement(&el, debugSubgroup);
      //      fprintf(stderr, "send map element (ellipse history)! bytesSent = %d \n", bytesSent);

    } // end checking each object
    return;
  }

  void LadarCarPerceptor::sendCars()
    {

      //  fprintf(stderr, "number of cars: %d \n", usedCars.size());
      int sent = 0;
      double tempY, tempX, dX, dY;
      MapElement el;
      vector <point2> cornerPoints;

      for(unsigned int k=0; k<usedCars.size(); k++) {
        int j = usedCars.at(k);
  
        sent++;

        double corners[8];
        corners[0] = cars[j].X;
        corners[1] = cars[j].Y;
        corners[2] = cars[j].X+cars[j].dx1;
        corners[3] = cars[j].Y+cars[j].dy1;
        corners[4] = cars[j].X+cars[j].dx2;
        corners[5] = cars[j].Y+cars[j].dy2;
        corners[6] = cars[j].X+cars[j].dx1+cars[j].dx2;
        corners[7] = cars[j].Y+cars[j].dy1+cars[j].dy2;

        //    fprintf(stderr, "sending car at: %f, %f. side 1: %f, %f. side 2: %f, %f \n", corners[0], corners[1], cars[j].dx1, cars[j].dy1, cars[j].dx2, cars[j].dy2);

        tempX = cars[j].Xmu.getelem(0,0);
        tempY = cars[j].Ymu.getelem(0,0);

        double deltaX = tempX - corners[0];
        double deltaY = tempY - corners[1];

        dX = cars[j].Xmu.getelem(1,0);
        dY = cars[j].Ymu.getelem(1,0);

        int bytesSent;
        cornerPoints.clear();
        point2 tmppt;
     
        //fill in the corners
        tmppt = point2(corners[0], corners[1]);
        cornerPoints.push_back(tmppt);
        tmppt = point2(corners[4], corners[5]);
        cornerPoints.push_back(tmppt);
        tmppt = point2(corners[6], corners[7]);
        cornerPoints.push_back(tmppt);
        tmppt = point2(corners[2], corners[3]);
        cornerPoints.push_back(tmppt);

        //set the originating module and blob ID numbers
     
        el.clear();
        el.setGeometry(cornerPoints);
        el.setTypeVehicle();
        el.height=2;
        el.setId(mapCarID,moduleCarID,cars[j].ID);
        el.plotColor = MAP_COLOR_RED;

        //TODO: Add velocity (point2_uncertain el.velocity)
        //TODO: Add state (something about map centering itself?)
        el.state = rawState;
        bytesSent = sendMapElement(&el, debugSubgroup);
        bytesSent = sendMapElement(&el, outputSubgroup);

        //      fprintf(stderr, "send map element (corner points)! bytesSent = %d \n", bytesSent);

        if(useDisplay) {

          cornerPoints.clear();
          tmppt = point2(corners[0]+deltaX, deltaY+ corners[1]);
          cornerPoints.push_back(tmppt);
          tmppt = point2(corners[4] + deltaX,deltaY+ corners[5]);
          cornerPoints.push_back(tmppt);
          tmppt = point2(corners[6] + deltaX, deltaY+ corners[7]);
          cornerPoints.push_back(tmppt);
          tmppt = point2(corners[2] + deltaX, deltaY+ corners[3]);
          cornerPoints.push_back(tmppt);

          //set the originating module and blob ID numbers
     
          el.clear();
          el.setGeometry(cornerPoints);
          el.setTypeObstacle();

          el.height=2;
          el.setId(mapCarID,predictedCarID,cars[j].ID);
          el.plotColor = MAP_COLOR_GREY;
          el.state = rawState;
          bytesSent = sendMapElement(&el, debugSubgroup);


          vector<point2> seg;
          //sending sides in diff colors
          el.clear();
          seg.clear();
          seg.push_back(point2(corners[0],corners[1]));
          seg.push_back(point2(corners[2],corners[3]));

          el.setTypeObstacle();
          el.height=2;
          el.setId(mapCarID,moduleCarSeg1ID,cars[j].ID);
          el.setGeometry(seg);
          el.plotColor = MAP_COLOR_YELLOW;
          el.state = rawState;
          bytesSent = sendMapElement(&el, debugSubgroup);

          el.clear();
          seg.clear();
          seg.push_back(point2(corners[0],corners[1]));
          seg.push_back(point2(corners[4],corners[5]));

          el.setTypeObstacle();
          el.height=2;
          el.setId(mapCarID,moduleCarSeg2ID,cars[j].ID);
          el.setGeometry(seg);
          el.plotColor = MAP_COLOR_MAGENTA;
          el.state = rawState;
          bytesSent = sendMapElement(&el, debugSubgroup);

          //sending car's history

  
          el.clear();
          el.setTypePoints();
          el.setGeometry(cars[j].history);
          el.setId(mapCarID,moduleCarHistoryID,cars[j].ID);
          el.plotColor=MAP_COLOR_RED;
          //        bytesSent = sendMapElement(&el, debugSubgroup);
          //      fprintf(stderr, "send map element (history)! bytesSent = %d \n", bytesSent);

          //sending car's velocity
  
          vector <point2> objectPoints;
          objectPoints.clear();
          point2 carcorner = point2(cars[j].X, cars[j].Y);
          point2 carcenter = point2(cars[j].dx1 + cars[j].dx2, cars[j].dy1 + cars[j].dy2);
          objectPoints.push_back(carcorner + .5*carcenter);
          objectPoints.push_back(carcorner + .5*carcenter + 75*cars[j].velocity);

          el.clear();
          el.setTypeObstacle();
          el.height=2;
          el.setGeometry(objectPoints);
          el.setId(mapCarID,carVelocityID,cars[j].ID);
          el.plotColor = MAP_COLOR_GREEN;
          el.state = rawState;
          bytesSent = sendMapElement(&el, debugSubgroup);
          //      fprintf(stderr, "send map element (ellipse)! bytesSent = %d \n", bytesSent);
  
          //sending car's KF velocity
          objectPoints.clear();
          objectPoints.push_back(carcorner);

          point2 velocity = point2(cars[j].Xmu.getelem(1,0),cars[j].Ymu.getelem(1,0));
          //	fprintf(stderr, "trying to plot velocity %f %f \n", velocity.x, velocity.y);
          objectPoints.push_back(carcorner + velocity);

          el.clear();
          el.setTypeObstacle();
          el.height=2;
          el.setGeometry(objectPoints);
          el.setId(mapCarID,carKFVelocityID,cars[j].ID);
          el.plotColor = MAP_COLOR_BLUE;
          el.state = rawState;
          bytesSent = sendMapElement(&el, debugSubgroup);
          //      fprintf(stderr, "send map element (ellipse)! bytesSent = %d \n", bytesSent);

        }//end sending debug info
      } // end cycling through each car

      //  fprintf(logfile, "\n");

      return;
    }

  void LadarCarPerceptor::writeLogs() {
    for(unsigned int i = 0; i < usedObjs.size(); i++) {
      int index = usedObjs.at(i);

      if(objects[index].timesSeen == 30) {
        fprintf(stderr, "writing log!! \n");
        fprintf(logfile, "\n object %d \n", objects[index].ID);
        for(int j = 0; j < 30; j++) {
          fprintf(logfile, "%f %f %f %f %f %f %f %f %f %f %f \n", \
                  objects[index].logData[j].el_x, objects[index].logData[j].el_y, \
                  objects[index].logData[j].el_a, objects[index].logData[j].el_b, \
                  objects[index].logData[j].alice_x, objects[index].logData[j].alice_y, \
                  objects[index].logData[j].alice_theta, \
                  objects[index].logData[j].vel_x, objects[index].logData[j].vel_y, \
                  objects[index].logData[j].length, objects[index].logData[j].std_linefit);

        }
      }
    }
  }

  /**
   * This function cycles through every currently tracked
   * object and checks whether it is moving. If an object
   * is moving, it is turned into a car.
   *
   * There are two choices for determination of motion:
   * 1) use calculated velocity, either from the Kalman
   *    filter, or from an average of deltas
   * 2) Whether its average position over history is
   *    outside the ellipse describing its points
   *
   * Behaviour is controlled by the parameters in .hh file:
   *   USE_KALMAN_FILTER_VELOCITY vs. USE_AVERAGE_VELOCITY
   *   USE_ELLIPSE_FOR_CHECKMOTION vs. USE_VELOCITY_FOR_CHECKMOTION
   *   MINSPEED
   **/
  void LadarCarPerceptor::checkMotion() {
    unsigned int histLength;
    point2 sumPt;
    point2 avgPt;
    int moving;

    for(unsigned int i = 0; i< usedObjs.size(); i++) {
      int index = usedObjs.at(i);
      moving = 0;
      sumPt.clear();
      avgPt.clear();
      if(objects[index].timesSeen > 30 && !objects[index].seenStart && !objects[index].seenEnd) {
        fprintf(stderr, "object %d, seen %d times, seenSTart = %d, seenEnd = %d \n", objects[index].ID, objects[index].timesSeen, objects[index].seenStart, objects[index].seenEnd);
      }
      if(objects[index].timesSeen > 30 && (objects[index].seenStart || objects[index].seenEnd)) {
        histLength = objects[index].history.size();
        for(unsigned int j=0; j<histLength; j++) {
          sumPt = sumPt + objects[index].history.at(j);
        }
        avgPt = sumPt/(1.0*histLength);
        //this returns 1 if avgpt is inside ellipse
        //      fprintf(stderr, "checking motion for obj %d.  \n", objects[index].ID);
        moving = objects[index].el.inside(avgPt, 0.0, .75);

        double velX, velY, vel;

#ifdef USE_KALMAN_FILTER_VELOCITY
        velX = objects[index].Xmu.getelem(1,0);
        velY = objects[index].Ymu.getelem(1,0);
#endif
#ifdef USE_AVERAGE_VELOCITY
        velX = objects[index].velocity.x;
        velY = objects[index].velocity.y;
#endif
        vel = 75*sqrt(velX*velX+velY*velY);

        double freeMapCell;
        //check if the center of object is on free map cell
        if(useMap) 
          {      
            freeMapCell = checkMap(objects[index].el.center.x, objects[index].el.center.y);
          } 

        if(-1 == moving) {
          if(!useMap || (.75 > freeMapCell))
            {
              double maxdim = max(objects[index].el.a, objects[index].el.b);

#ifdef USE_ELLIPSE_FOR_CHECKMOTION
              if(MAXCARELLIPSEDIM > maxdim) {
                obj2car(i);
              }
#endif
              //	    fprintf(stderr, "obj at %f, %f  is moving, avg vel = %f, %f, prob = %f \n", objects[index].el.center.x, objects[index].el.center.y, 75*objects[index].velocity.x, 75*objects[index].velocity.y, freeMapCell);
            } else {
              //	  fprintf(stderr, "obj at %f, %f was moving, but in occupied space. prob = %f \n", objects[index].el.center.x, objects[index].el.center.y, freeMapCell);
            }
          //        fprintf(stderr, "for obj %d, moving = %d  ellipse center = %f, %f; a,b = %f, %f,  avg hist = %f, %f \n", objects[index].ID, moving,  objects[index].el.center.x, objects[index].el.center.y, objects[index].el.a, objects[index].el.b, avgPt.x, avgPt.y);
        } else if(vel > MINSPEED) {
          //	  fprintf(stderr, "THIS OBJ IS MOVING!!! obj posn: %f, %f, vel = %f, timesSeen = %d, turning into car %d \n ", objects[index].el.center.x, objects[index].el.center.y, vel, objects[index].timesSeen, numCars);
#ifdef USE_VELOCITY_FOR_CHECKMOTION
          obj2car(i);
#endif
        }
        //      }
      }
    }
  } //end checkMotion



  /**
   * This function turns an object into a car
   *
   * Sets the following fields in carRepresentation:
   *   X, Y, dx1, dy1, dx2, dy2
   *   seenFront, seenBack, seenCorner
   *   Xmu, Xsigma, Ymu, Ysigma
   *   ID, lastSeen, timesSeen, history
   *   velocity, ptsinvelocity
   **/
  //#warning "this code should remove the object at index, in addition to making a new car...possibly replace with addCar(&object), and removeObject(index)"
  void LadarCarPerceptor::obj2car(int index) {
    //  cout<<"trying to turn an object into a car"<<endl;
    if(usedCars.size() < MAXNUMCARS-1) { 
      int i = usedObjs.at(index);

      fprintf(stderr, "turning object %d into car. a = %f, b = %f \n", objects[i].ID, objects[i].el.a, objects[i].el.b);

      point2arr points(objects[i].lastScan);
      
      int temp;
      
      temp = openCars.back();
      openCars.pop_back();
      usedCars.push_back(temp);
      
      //  cout<<"this has "<<numPoints<<" points..."<<endl;
      
      int corner;

      //  corner = fitCar(points,&cars[temp],objects[i].Xmu.getelem(1,0), objects[i].Ymu.getelem(1,0)); 
      corner = fitCar(points,objects[i].seenStart, objects[i].seenEnd,&cars[temp],objects[i].velocity.x, objects[i].velocity.y, 0.0, 0.0); 

      cars[temp].seenCorner = corner;    

      //  fprintf(stderr, "fit car! corner: %f, %f \n", cars[temp].N, cars[temp].E);
      cars[temp].Xmu.resetSize(2,1);
      cars[temp].Ymu.resetSize(2,1);
      cars[temp].Xsigma.resetSize(2,2);
      cars[temp].Ysigma.resetSize(2,2);
      cars[temp].Xmu_hat.resetSize(2,1);
      cars[temp].Ymu_hat.resetSize(2,1);
      cars[temp].Xsigma_hat.resetSize(2,2);
      cars[temp].Ysigma_hat.resetSize(2,2);
  
      double XMelems[] = {cars[temp].X, objects[i].Xmu.getelem(1,0)};
      double YMelems[] = {cars[temp].Y, objects[i].Ymu.getelem(1,0)};
      double XSelems[] = {0,0,0,0};
      double YSelems[] = {0,0,0,0};
      cars[temp].Xmu.setelems(XMelems);
      cars[temp].Xsigma.setelems(XSelems);
      cars[temp].Ymu.setelems(YMelems);
      cars[temp].Ysigma.setelems(YSelems);
      
      cars[temp].ID = numCars;
      cars[temp].lastSeen = objects[i].lastSeen;  
      cars[temp].timesSeen = 1;
    
      cars[temp].history.clear();
      for(unsigned int j=0; j<objects[i].history.size(); j++) {
        cars[temp].history.push_back(objects[i].history[j]);
      }

      cars[temp].velocity = point2(objects[i].velocity);
      cars[temp].ptsinvelocity = objects[i].ptsinvelocity;

      numCars++; //we've found a car
    
      removeObject(index);
      
    } else {
      cerr<<"not enough space in cars array!!"<<endl;
    }  // end of checking space
  }  // end of obj2car



  /**
   * function: fitCar
   * input: takes in set of NE points that have been 
   *   grouped together as an object, and finds the two 
   *   lines that fit them best (think rectangle).
   *   Once these have been calculated, car is represented
   *   by the point corresponding to its back-right corner
   *   and by deltas corresponding to length and width
   *   The length/width of car are determined from its 
   *   direction of motion (input vX and vY)
   * output: parameters describing this rectangle
   *
   * Sets the following fields in carRepresentation:
   *   X, Y, dx1, dy1, dx2, dy2
   *   seenFront, seenBack
   *  
   **/
  int LadarCarPerceptor::fitCar(point2arr points, int seenStart, int seenEnd, carRepresentation* newCar, double vX, double vY, double mind1, double mind2) {
    int corner = 0;

    int numPoints = points.size();
    double Ypoints[numPoints];
    double Xpoints[numPoints];
    for(int i=0; i<numPoints; i++) {
      Ypoints[i] = points.arr[i].y;
      Xpoints[i] = points.arr[i].x;
    }

    double minerror, lineerror, MSE1, MSE2, MSE3, MSE4;
    int breakpoint;
    //current best fit
    lineSegmentFit seg1;
    lineSegmentFit seg2;
    //candidate fits
    lineSegmentFit tempSeg1;
    lineSegmentFit tempSeg2;
    lineSegmentFit tempSeg3;
    lineSegmentFit tempSeg4;

    //Calculating the fit for all points
    //somewhere, order of points gets swapped...
    //   swapping back here
    minerror = points.fit_line(seg1.a, seg1.b, seg1.pt2, seg1.pt1);
    lineerror = minerror;
    //now, find min error for corner, but requiring each 
    //leg to have at least 3 points, and each leg contains 
    //the breakpoint
    for(int i=3; i<numPoints-3; i++) {
      double Xpoints1[i];
      double Ypoints1[i];
      double Xpoints2[numPoints-i+1];
      double Ypoints2[numPoints-i+1];

      for(int j=0; j<i; j++) {
        Xpoints1[j] = Xpoints[j];
        Ypoints1[j] = Ypoints[j];
      }
      for(int j=0; j<(numPoints-i+1); j++) {
        Xpoints2[j] = Xpoints[j+2*(i-1)];
        Ypoints2[j] = Ypoints[j+2*(i-1)];
      }

      point2arr ptarr1, ptarr2;
      points.split_at(i, ptarr1, ptarr2);

      //again, order of pts in ptarr is reversed, so this
      // gives the fit with the convention of seg1 and 
      // seg2 being in scan-order (R to L)

      //fitting seg1 first, seg2 perpendicular
      MSE1 = ptarr2.fit_line(tempSeg1.a, tempSeg1.b, tempSeg1.pt2, tempSeg1.pt1);
      MSE2 = ptarr1.fit_line_perp(tempSeg2.a, tempSeg2.b, tempSeg2.pt2, tempSeg2.pt1, tempSeg1.b);

      //fitting seg2 first, seg1 perpendicular
      MSE3 = ptarr1.fit_line(tempSeg3.a, tempSeg3.b, tempSeg3.pt2, tempSeg3.pt1);
      MSE4 = ptarr2.fit_line_perp(tempSeg4.a, tempSeg4.b, tempSeg4.pt2, tempSeg4.pt1, tempSeg3.b);


      if((minerror > MSE1 + MSE2)&&(lineerror > 2*(MSE1+MSE2)))
        {
          minerror = MSE1 + MSE2;
          breakpoint = i;
          corner = 1;

          seg1.a = tempSeg1.a;
          seg1.b = tempSeg1.b;
          seg1.pt1 = point2(tempSeg1.pt1);
          seg1.pt2 = point2(tempSeg1.pt2);

          seg2.a = tempSeg2.a;
          seg2.b = tempSeg2.b;
          seg2.pt1 = point2(tempSeg2.pt1);
          seg2.pt2 = point2(tempSeg2.pt2);

          //      fprintf(stderr, "line1 endpoints: pt1 = %f, %f. pt2 = %f, %f \n", tempSeg1.pt1.x, tempSeg1.pt1.y, tempSeg1.pt2.x, tempSeg1.pt2.y);
          //      fprintf(stderr, "line2 endpoints: pt1 = %f, %f. pt2 = %f, %f \n", tempSeg2.pt1.x, tempSeg2.pt1.y, tempSeg2.pt2.x, tempSeg2.pt2.y);
          //      fprintf(stderr, "new min error at breakpoint: %d \n", breakpoint);
          //      fprintf(stderr, "line fits are: seg1 %f, %f and seg2 %f, %f \n", seg1.a, seg1.b, seg2.a, seg2.b);
        }

      if((minerror > MSE3 + MSE4)&&(lineerror > 2*(MSE3+MSE4)))
        {
          minerror = MSE3 + MSE4;
          breakpoint = i;
          corner = 1;

          seg1.a = tempSeg4.a;
          seg1.b = tempSeg4.b;
          seg1.pt1 = point2(tempSeg4.pt1);
          seg1.pt2 = point2(tempSeg4.pt2);

          seg2.a = tempSeg3.a;
          seg2.b = tempSeg3.b;
          seg2.pt1 = point2(tempSeg3.pt1);
          seg2.pt2 = point2(tempSeg3.pt2);
        }

    } //end of finding best-fit line segments

    //if we didn't see a corner, add one
    //this was tested by turning off corner fitting, 
    // and checking that this gave right shape
    if(corner == 0)
      {
        double x,y; //new n,e pts 
        double thC; //angle from start to end of car
        double th;
        thC = atan2((seg1.pt1.x - seg1.pt2.x), \
                    (seg1.pt1.y - seg1.pt2.y));
        //since pt1, pt2 are in scan order, +pi/2 always 
        //points away from alice
        th = (M_PI/2) + thC; 

        y = seg1.pt2.y + MINDIMENSION * cos(th);
        x = seg1.pt2.x + MINDIMENSION * sin(th);

        seg2.a = 0;
        seg2.b = 0;
        seg2.pt1 = point2(seg1.pt2);
        seg2.pt2 = point2(x,y);
      }


    //calculating the intersection of seg1 and seg2
    double xi, yi; //northing and easting intersections
    point2 d1, d2;

    if(corner == 0 )
      { //corner is end of seg1
        xi = seg1.pt2.x;
        yi = seg1.pt2.y;
      } else { //corner is intersect of the lines
        xi = (seg1.a - seg2.a)/(seg2.b - seg1.b);
        yi = seg1.a + xi*seg1.b;
        //    fprintf(stderr, "corner = %f %f \n", xi, yi);
      }

    d1.x = seg1.pt1.x - xi; //seg 1 deltas
    d1.y = seg1.pt1.y - yi;
    d2.x = seg2.pt2.x - xi; //seg 2 deltas
    d2.y = seg2.pt2.y - yi;

    point2 pti = point2(xi,yi);

    //  newCar->X = xi; //corner coords
    //  newCar->Y = yi;
    //  newCar->dx1 = dx1; //seg 1 deltas
    //  newCar->dy1 = dy1;
    //  newCar->dx2 = dx2; //seg 2 deltas
    //  newCar->dy2 = dy2;

    /**
     * up until this point, seg1 and seg2 were in scan order,
     * with pt1 and pt2 also in scan order. Thus, the corner
     * is at pt2 of seg1 and pt1 of seg2
     *
     * Now, we wish to put the corner at the back right of
     * the car, with seg1 pointing along car's length, and 
     * seg2 along its width
     * see p. 85
     **/




    double thv, th1, th2; //velocity and segment angles
    thv = atan2(vY, vX);
    th1 = atan2(d1.y, d1.x);
    th2 = atan2(d2.y, d2.x);
    int d1count = 0;

    if(0 == corner) {
      mind1 = 0.0;
      mind2 = 0.0;
    }
    double tempTh;
    point2 tempPt;
    point2 zeroPt = point2(0.0, 0.0);

    /**
     * first, putting xi,yi at back bumper
     */
    //if (dx1, dy1) is parallel direction of velocity
    if( fabs(sin(thv-th1)) < fabs(sin(thv-th2))) { 
      //already have d1, d2 right, so can enforce length
      if(mind1 > d1.norm()) {
        tempTh = d1.heading();
        d1.x = mind1 * cos(tempTh);
        d1.y = mind1 * sin(tempTh);
      }
      if(mind2 > d2.norm()) {
        tempTh = d2.heading();
        d2.x = mind2 * cos(tempTh);
        d2.y = mind2 * sin(tempTh);
      }
      if(cos(thv-th1) > 0) { //they're oriented the same

      } else {
        //oriented opposite, so need to flip d1
        pti = pti + d1;
        d1 = zeroPt - d1;
        d1count++;
      }
    } else {
      //(dx2, dy2) is parallel to velocity...need to 
      //  switch d1 and d2
      tempPt = d1;
      d1 = d2;
      d2 = tempPt;
      d1count++;

      //now we have d1, d2 right, so can enforce length
      if(mind1 > d1.norm()) {
        tempTh = d1.heading();
        d1.x = mind1 * cos(tempTh);
        d1.y = mind1 * sin(tempTh);
      }
      if(mind2 > d2.norm()) {
        tempTh = d2.heading();
        d2.x = mind2 * cos(tempTh);
        d2.y = mind2 * sin(tempTh);
      }

      if(cos(thv-th2) > 0) { //same orientation
        // xi,yi are fine, only need to switch d1 and d2
      } else { //opp orientation
        //need to switch orientation as well
        pti = pti + d1;
        d1 = zeroPt - d1;
        d1count++;
      }
    }

    //  newCar->X = xi; //corner coords
    //  newCar->Y = yi;
    //  newCar->dx1 = dx1; //seg 1 deltas
    //  newCar->dy1 = dy1;
    //  newCar->dx2 = dx2; //seg 2 deltas
    //  newCar->dy2 = dy2;



    /**
     * Next, putting xi,yi at RIGHT side of back bumper 
     * (i.e. th2 and thv+PI/2 are in same direction)
     **/
    th2 = atan2(d2.y, d2.x); 
    // if they're in same direction
    if(cos(thv + M_PI/2 - th2) < 0) { 
      //everything is set up properly
    } else { //need to switch
      pti = pti + d2;
      d2 = zeroPt - d2;
    }

    //WHEW. car is set up correctly
    newCar->X = pti.x; //corner coords
    newCar->Y = pti.y;
    newCar->dx1 = d1.x; //seg 1 deltas
    newCar->dy1 = d1.y;
    newCar->dx2 = d2.x; //seg 2 deltas
    newCar->dy2 = d2.y;

    //assigning seenFront,seenBack. see p.107 for explanation
    if(1 == d1count) {
      newCar->seenFront = seenEnd;
      newCar->seenBack = seenStart;
    } else {
      newCar->seenFront = seenStart;
      newCar->seenBack = seenEnd;
    }

    return corner;
  } //end fitCar



  //removes object by putting its index back on openObjs, removing 
  //the index from usedObjs, and sending msg to the map to clear
  //corresponding map elements
  void LadarCarPerceptor::removeObject(int index) {

    int bytesSent;
    MapElement el;
    int j = usedObjs.at(index); 
    el.clear();
    el.setTypeClear();

    if(useCar2obj) {
      el.setId(mapCarID, moduleObjectID, objects[j].ID);
      el.state = rawState;
      bytesSent = sendMapElement(&el, outputSubgroup);
    }

    if(useDisplay) {
      el.setId(mapCarID,moduleObjectID,objects[j].ID);
      bytesSent = sendMapElement(&el, debugSubgroup);

      el.setId(mapCarID,moduleEllipseID,objects[j].ID);
      bytesSent = sendMapElement(&el, debugSubgroup);

      el.setId(mapCarID,moduleHistoryID,objects[j].ID);
      bytesSent = sendMapElement(&el, debugSubgroup);

      el.setId(mapCarID,objectVelocityID,objects[j].ID);
      bytesSent = sendMapElement(&el, debugSubgroup);
    } 



    openObjs.push_back(usedObjs.at(index));
    objIterator = usedObjs.begin();
    objIterator+=index;
    usedObjs.erase(objIterator);


  }
  //removes car by putting it's index on openCars
  void LadarCarPerceptor::removeCar(int index) {
    int bytesSent;
    MapElement el;
    int j = usedCars.at(index); 
    el.clear();
    el.setId(mapCarID,moduleCarID,cars[j].ID);
    el.setTypeClear();
    bytesSent = sendMapElement(&el, debugSubgroup);
    bytesSent = sendMapElement(&el, outputSubgroup);

    if(useDisplay) {
      el.clear();
      el.setTypeClear();

      el.setId(mapCarID,moduleCarHistoryID,cars[j].ID);
      bytesSent = sendMapElement(&el, debugSubgroup);

      el.setId(mapCarID,carVelocityID,cars[j].ID);
      bytesSent = sendMapElement(&el, debugSubgroup);

      el.setId(mapCarID,carKFVelocityID,cars[j].ID);
      bytesSent = sendMapElement(&el, debugSubgroup);

      el.setId(mapCarID,predictedCarID,cars[j].ID);
      bytesSent = sendMapElement(&el, debugSubgroup);

      el.setId(mapCarID,moduleCarSeg1ID,cars[j].ID);
      bytesSent = sendMapElement(&el, debugSubgroup);

      el.setId(mapCarID,moduleCarSeg2ID,cars[j].ID);
      bytesSent = sendMapElement(&el, debugSubgroup);
    }

    openCars.push_back(usedCars.at(index));
    carIterator = usedCars.begin();
    carIterator+=index;
    usedCars.erase(carIterator);

  }




  void LadarCarPerceptor::KFupdate() {

    //  cout<<"entering KFupdate"<<endl;

    Matrix K1;
    K1 = Matrix(2,2);
    Matrix K2;
    K2 = Matrix(2,2);
    Matrix Xerr;
    Xerr = Matrix(1,1);
    Matrix Yerr;
    Yerr = Matrix(1,1);
    Matrix tmp;
    tmp = Matrix(1,1);
    Matrix tmp2;
    tmp2 = Matrix(1,1);
    for(unsigned int i=0; i<usedObjs.size(); i++) {
      int j = usedObjs.at(i);
      //    cout<<"last seen: "<<objects[j].lastSeen<<"  and curr frame: "<<numFrames<<endl;
      if(objects[j].timesSeen > 1) {
        if(objects[j].lastSeen == numFrames) {
          double grr[] = {objects[j].el.center.x};
          tmp.setelems(grr);
          //	cout<<"Northing: "<<objects[j].N<<" and, predicted northing: "<<objects[j].Xmu_hat.getelem(0,0)<<endl;
          Xerr = tmp - C*objects[j].Xmu_hat;
          //	cout<<"Xerror: "<<Xerr.getelem(0,0)<<' ';
          K1 = objects[j].Xsigma_hat * C.transpose() * (C*objects[j].Xsigma_hat*C.transpose() + Q).inverse();
          objects[j].Xmu = objects[j].Xmu_hat + K1 * Xerr;
          objects[j].Xsigma = (I - K1*C) * objects[j].Xsigma_hat;

          double grr2[] = {objects[j].el.center.y};
          tmp2.setelems(grr2);

          Yerr = tmp2 - C*objects[j].Ymu_hat;
          //	cout<<" and, Yerror: "<<Yerr.getelem(0,0)<<endl;
          K2 = objects[j].Ysigma_hat * C.transpose() * (C*objects[j].Ysigma_hat*C.transpose() + Q).inverse();
          objects[j].Ymu = objects[j].Ymu_hat + K2 * Yerr;
          objects[j].Ysigma = (I - K2*C) * objects[j].Ysigma_hat;
          //	cout<<"new vel (actually calculated) "<<objects[j].Xmu.getelem(1,0)<<' '<<objects[j].Ymu.getelem(1,0)<<endl;

        } else {

          objects[j].Xmu = objects[j].Xmu_hat;
          objects[j].Xsigma = objects[j].Xsigma_hat;
          objects[j].Ymu = objects[j].Ymu_hat;
          objects[j].Ysigma = objects[j].Ysigma_hat;

        }

      }
    }


    for(unsigned int i=0; i<usedCars.size(); i++) {
      //    cout<<"entering loop w/ i = "<<i<<endl;
      int j = usedCars.at(i);
      //    cout<<"last seen: "<<cars[j].lastSeen<<"  and curr frame: "<<numFrames<<endl;
      if(cars[j].timesSeen > 1) {
        if(cars[j].lastSeen == numFrames) {

          double grr[] = {cars[j].X};
          tmp.setelems(grr);

          //	cout<<"line 2239"<<endl;
          Xerr = tmp - C*cars[j].Xmu_hat;
          //	cout<<"Xerr for car: "<<Xerr.getelem(0,0)<<endl;
          K1 = cars[j].Xsigma_hat * C.transpose() * (C*cars[j].Xsigma_hat*C.transpose() + Q).inverse();
          //	cout<<"line 2242"<<endl;
          cars[j].Xmu = cars[j].Xmu_hat + K1 * Xerr;
          //	cout<<"new n,ndot: "<<cars[j].Xmu.getelem(0,0)<<' '<<cars[j].Xmu.getelem(1,0)<<endl;
          cars[j].Xsigma = (I - K1*C) * cars[j].Xsigma_hat;

          //	cout<<"ln 2246"<<endl;
          double grr2[] = {cars[j].Y};
          tmp2.setelems(grr2);

          //	cout<<"ln 2250"<<endl;
          Yerr = tmp2 - C*cars[j].Ymu_hat;
          K2 = cars[j].Ysigma_hat * C.transpose() * (C*cars[j].Ysigma_hat*C.transpose() + Q).inverse();
          //	cout<<"ln 2253"<<endl;
          cars[j].Ymu = cars[j].Ymu_hat + K2 * Yerr;
          cars[j].Ysigma = (I - K2*C) * cars[j].Ysigma_hat;

        } else {
          //	cout<<"ln 2258"<<endl;
          cars[j].Xmu = cars[j].Xmu_hat;
          cars[j].Xsigma = cars[j].Xsigma_hat;
          //	cout<<"ln 2261"<<endl;
          cars[j].Ymu = cars[j].Ymu_hat;
          //	cout<<"ln 2263"<<endl;
          cars[j].Ysigma = cars[j].Ysigma_hat;
          //	cout<<"ln 2264"<<endl;
        }
      }
    }

    //  cout<<"exiting KFupdate"<<endl;
  }
 
  void LadarCarPerceptor::KFpredict() {

    //  cout<<"entering KFpredict()"<<endl;

    for(unsigned int i=0; i<usedObjs.size() ; i++) {
      int j = usedObjs.at(i);

      objects[j].Xmu_hat =    A*objects[j].Xmu;
      objects[j].Xsigma_hat = A*objects[j].Xsigma*A.transpose() + R;
      objects[j].Ymu_hat =    A*objects[j].Ymu;
      objects[j].Ysigma_hat = A*objects[j].Ysigma*A.transpose() + R;

    } //end cycling through vector of objects


    //and, objcars

    for(unsigned int i=0; i<usedCars.size(); i++) {
      int j = usedCars.at(i);

      //    cout<<"ln 2300"<<endl;
      cars[j].Xmu_hat =    A*cars[j].Xmu;
      cars[j].Xsigma_hat = A*cars[j].Xsigma*A.transpose() + R;
      //    cout<<"ln2303"<<endl;
      cars[j].Ymu_hat =    A*cars[j].Ymu;
      cars[j].Ysigma_hat = A*cars[j].Ysigma*A.transpose() + R;
    } //end cycling through numCars

  } //end KFpredict()


  void LadarCarPerceptor::cleanObjects()
    {
  
      int j, m; 
      int diff;
      for(unsigned int i=0; i<usedObjs.size(); i++)
        {
          //checking if obj should be removed based on time
          j = usedObjs.at(i);
          diff = numFrames - objects[j].lastSeen;
          //    fprintf(stderr, "current frame: %d, last seen: %d \n", numFrames, objects[j].lastSeen);
          if(diff > OBJECTTIMEOUT) {
            //      fprintf(stderr, "tryng to remove object # %d! \n", j);
            removeObject(i);
          }

          //checking if two objects should be merged
          for(unsigned int k=0; k<usedObjs.size(); k++)
            {
              m = usedObjs.at(k);
              int overlap = objects[j].el.overlap(&objects[m].el, MARGIN);
              //if they overlap and aren't the same object
              if((1 == overlap) && (j!=m)) {
                //	  fprintf(stderr, "objs %d and %d overlap! merging ... \n",objects[j].ID, objects[m].ID);
                //	  fprintf(stderr, "center and params for obj 1: %f, %f, %f, %f, %f \n", objects[j].el.center.x, objects[j].el.center.y, objects[j].el.a, objects[j].el.b, objects[j].el.theta);
                //	  fprintf(stderr, "center and params for obj 2: %f, %f, %f, %f, %f \n", objects[m].el.center.x, objects[m].el.center.y, objects[m].el.a, objects[m].el.b, objects[m].el.theta);
                //FIXME: would it be better to:
                // keep the one w/ longer history...or
                //would it be better to use most recently seen?

                //this causes problems (can't remove object i)
                //	  if(objects[j].timesSeen > objects[m].timesSeen) 
                //	  {
                mergeObjects(&objects[j], &objects[m]);
                removeObject(k);
                //	  } else {
                //	    mergeObjects(&objects[m], &objects[j]);
                //	    removeObject(i);
                //	  }

              }
            }

        }

    }

  void LadarCarPerceptor::mergeObjects(objectRepresentation* oldObj, objectRepresentation* newObj)
    {
      //TODO: 
      //doesn't update startPoint/endPoint -> not currently a problem, as nothing uses these vars
      //push back most recent scan -- better way to do this??
      //do anything about the KF??

      //merge the ellipses
      oldObj->el.merge(newObj->el);

      //update timesseen, and lastseeen
      oldObj->timesSeen = max(oldObj->timesSeen, newObj->timesSeen);
      oldObj->lastSeen = max(oldObj->lastSeen, newObj->lastSeen);

    }

  //TODO: add a free-space condition for removing cars
  void LadarCarPerceptor::cleanCars()
    {
      int j; 
      int diff;
      for(unsigned int i=0; i<usedCars.size(); i++)
        {
          j = usedCars.at(i);
          diff = numFrames - cars[j].lastSeen;
          //    fprintf(stderr, "current frame: %d, last seen: %d \n", numFrames, objects[j].lastSeen);
          if(diff > CARTIMEOUT) {
            //      fprintf(stderr, "tryng to remove car # %d! \n", j);
            removeCar(i);
          }
        }

    }


  /**
   * this function is called when it is determined that a 
   * new object matches an already seen car.
   * see p. 109 for (some of) the logic
   *
   * Sets the following fields in carRepresentation: 
   *   velocity, ptsinvelocity
   *   lastSeen, timesSeen, history
   **/
  void LadarCarPerceptor::updateCar(carRepresentation* Car, point2arr points, double dStart, double dEnd) {

    double dX, dY;
    point2 dCar;

    int corner;
    //don't allow car to shrink...
    double mind1, mind2;
    mind1 = sqrt(pow(Car->dx1,2) + pow(Car->dy1,2));
    mind2 = sqrt(pow(Car->dx2,2) + pow(Car->dy2,2));
    carRepresentation newCar;

    int seenStart = int(dStart/fabs(dStart));
    int seenEnd = int(dEnd/fabs(dEnd));
    //  corner = fitCar(points, newObj->seenStart, newObj->seenEnd, &newCar, Car->Xmu.getelem(1,0), Car->Ymu.getelem(1,0), mind1, mind2);
    //  corner = fitCar(points, seenStart, seenEnd, &newCar, Car->velocity.x, Car->velocity.y, mind1, mind2);
#warning "fixme - add back velocity to fitcar"
    corner = fitCar(points, seenStart, seenEnd, &newCar, Car->velocity.x, Car->velocity.y, mind1, mind2);
    newCar.seenCorner = corner;

    //TODO: why not add KF for dx1 ....?
    //TODO: insert the 4 cases here

    //  if((1 == Car->seenCorner) && (1 == newCar.seenCorner)) {
    if(1) {
      //    fprintf(stderr, "Case 1: ");
      dX = newCar.X - Car->X;
      dY = newCar.Y - Car->Y;
      dCar = point2(dX,dY);
      //    fprintf(stderr, "car's velocity = %f, %f ", 75*Car->velocity.x, 75*Car->velocity.y);
      //    fprintf(stderr, "deltas = %f, %f \n", dX, dY);

      Car->X = newCar.X;
      Car->Y = newCar.Y;
      Car->dx1 = newCar.dx1;
      Car->dy1 = newCar.dy1;
      Car->dx2 = newCar.dx2;
      Car->dy2 = newCar.dy2;


    } else {
      if((Car->seenBack && newCar.seenBack) || !Car->seenFront || !newCar.seenFront) {
        dX = newCar.X - Car->X;
        dY = newCar.Y - Car->Y;
      } else {
        dX = (newCar.X + newCar.dx1) - (Car->X + Car->dx1);
        dY = (newCar.Y + newCar.dy1) - (Car->Y + Car->dy1);
      }
      dCar = point2(dX,dY);


      Car->X = Car->X + dX;
      Car->Y = Car->Y + dY;


      //    if((1 != Car->seenCorner) && (1 == newCar.seenCorner)) {
      if((1 == Car->seenCorner) || (1 == newCar.seenCorner)) {
        //      fprintf(stderr, "Case 2: ");
        //      fprintf(stderr, "deltas = %f, %f \n", dX, dY);
        //since saw corner in the new car, we know that the
        //deltas are >= those of old car, so might as well
        //just copy them over
        Car->dx1 = newCar.dx1;
        Car->dy1 = newCar.dy1;
        Car->dx2 = newCar.dx2;
        Car->dy2 = newCar.dy2;

      } else {
        //      fprintf(stderr, "Case 3 or 4: deltas = %f, %f \n", dX, dY);
        //if we've seen a new side longer than one already
        //observed, might as well replace it
        point2 newd1, newd2;
        newd1 = point2(newCar.dx1, newCar.dy1);
        newd2 = point2(newCar.dx2, newCar.dy2);
        if(newd1.norm() > mind1) {
          Car->dx1 = newd1.norm() * cos(newd1.heading());
          Car->dy1 = newd1.norm() * sin(newd1.heading());
        }
        if(newd2.norm() > mind2) {
          Car->dx2 = newd2.norm() * cos(newd2.heading());
          Car->dy2 = newd2.norm() * sin(newd2.heading());
        }
        /** not currently distinguishing between these...
            if((1 == Car->seenCorner) && (1 != newCar.seenCorner)) {
            fprintf(stderr, "Case 3: \n");
            fprintf(stderr, "deltas = %f, %f \n", dX, dY);

            } else {
            fprintf(stderr, "Case 4: \n");
            fprintf(stderr, "deltas = %f, %f \n", dX, dY);
            }
        **/

      }  // end case 3-4
    }  // end cases 2-4

    Car->seenFront = max(Car->seenFront, newCar.seenFront);
    Car->seenBack = max(Car->seenBack, newCar.seenBack);
    Car->seenCorner = max(Car->seenCorner, newCar.seenCorner);

    point2 tempvel = Car->ptsinvelocity*Car->velocity + dCar;
    Car->ptsinvelocity++;
    Car->velocity = tempvel/Car->ptsinvelocity;

    Car->lastSeen = numFrames;  
    Car->timesSeen = Car->timesSeen + 1;
    Car->history.push_back(point2(Car->X, Car->Y));

  } //end updateCar

  
