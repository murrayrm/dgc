/**
 * Implements car tracking with ladars using Multi-hypothesis tracking
 */
#include "LadarCarPerceptor.hh"
using namespace std;
ofstream debugingfile;

int LadarCarPerceptor::MHTtrackerInit(){
  debugingfile.open("debug.log");
  numHypotheses = 1;
  for(int i = 0; i < MAXNUMHYPOTHESES; i++){
    hypotheses[i] = hypothesis();
  }
  numFrames =  0;

  initSendMapElement(skynetKey);

  DGCgettime(lastSentCars);
  DGCgettime(lastSentObjs);
  DGCgettime(lastKFupdate);

  // car/obj arrays are intialzied in the hypothesis default constructor

  // setting up matrices for KF calculations
  double dt = 0.0133;
  double sa = 0.05; // std dev of system noise
  double sz = .1; //std dev of measurement noise
  
  A.resetSize(2,2);
  double Aelems[] = {1,dt,0,1};
  A.setelems(Aelems);

  C.resetSize(1,2);
  double Celems[] = {1,0};
  C.setelems(Celems);

  I.resetSize(2,2);
  double Ielems[] = {1,0,0,1};
  I.setelems(Ielems);

  R.resetSize(2,2);
  double Relems[] = {0,0,0,sa};
  R.setelems(Relems);

  
  Q.resetSize(1,1);
  double Qelems[] = {sz};
  Q.setelems(Qelems);
  
  SA.resetSize(3,3);
  double SAelems[] = {1,0,0,0,1,0,0,0,1};
  SA.setelems(SAelems);
  // Needs to be 4x3 so we can get two different measurements of
  // theta
  SC.resetSize(4,3);
  double SCelems[] = {1,0,0,0,1,0,0,0,1,0,0,1};
  SC.setelems(SCelems);

  SI.resetSize(3,3);
  double SIelems[] = {1,0,0,0,1,0,0,0,1};
  SI.setelems(SIelems);

  // Disturbance
  SR.resetSize(3,3);
  double SRelems[] = {.01, 0, 0, 0, .1, 0, 0, 0, .1};
  SR.setelems(SRelems);

  // Noise
  SQ.resetSize(4,4);
  double SQelems[] = {0.1,0,0,0, 0,.1,0,0, 0,0,.2,0, 0,0,0,.1};
  SQ.setelems(SQelems);
  

  //call the map init
  //  MSG("calling map init: useMap = %d \n", useMap);
  if(useMap)
    {
      //      MSG("Initializing map");
      mapInit();
      //      mapTest();

    }
  return 0;
}


/**
 * Assumes synthetic data
 */
void LadarCarPerceptor::MHTprocessScan(){
  debugingfile << "Processing Scan" << endl;
  numFrames++;
  DGCgettime(origStartTime);

  double dt = (DGCgettime() - lastKFupdate)/1000000.0;
  MHTKFpredict(dt);
  DGCgettime(lastKFupdate);

  segmentSynthScan();
  createSynthObjects();
  MHTclassifyObjects2(dt);

  DGCgettime(startTime);
  checkMotion2();
  DGCgettime(startTime);
  if(outputRate < (startTime - lastSentCars)){
    MHTsendToMap();
    DGCgettime(lastSentCars);
  }
  MHTcleanObjects();
  MHTcleanCars();
  MHTKFupdate();
  DGCgettime(endTime);
  diffTime = 1.0*(endTime-origStartTime);
}


/**
 * This function assumes that the scan has been segmented,
 * and classifies the new objects into:
 *   already-seen object
 *   already-seen car
 *   new object
 * Then, it calss the appropriate functions to update the representation
 * assumes synthetic data
 * Performs classification once for each hypothesis
 *
 * Pure multi-hypothesis association (no overlap test prior)
 *
 * Largely copied from classifyObjects, written by Laura Lindzey
 **/
void LadarCarPerceptor::MHTclassifyObjects2(double dt){
  hypothesis* hypArray = new hypothesis[MAXNUMHYPOTHESES*MAXNUMHYPOTHESES];
  int hypIndex = 0;
  for(int w = 0; w < numHypotheses; w++){
    // iterate through all hypotheses
    hypothesis *hyp = &(hypotheses[w]);
    
    for(int i = 0; i < MAXNUMCARS; i++){
      hyp->carsToUpdate[i].clear();
    }
    for(int i = 0; i < MAXNUMOBJS; i++){
      hyp->objsToUpdate[i].clear();
    }

    unsigned int l;
    double dStart, dEnd;
    int startIndex;
    vector<short> sources;
    startIndex = numSynthScanPoints;
    int endIndex = -1;

    point2arr newpts;
    point2arr newdepthpts;

    /**
     * Finally, we take car of the new objects that didn't immediately match
     * anything we've seen thus far.  This is where the multi-hypothesis
     * tracking takes place, as the assumption is that if objects lie
     * on top of each other, they are very likely to be the same thing so
     * there is no need to increase the cost of MHT by including them.
     **/
    //create vector of all untaken measurements
    vector<int> MHTmeasure;
    for(int i = 0; i < numNewObjects; i++){
	MHTmeasure.push_back(i);
    }
    vector<int> MHTobj;
    for(int i = 0; i < hyp->usedObjs.size(); i++){
      if(hyp->objsToUpdate[hyp->usedObjs[i]].size() == 0){
	MHTobj.push_back(i);
      }
    }
    vector<int> MHTcar;
    for(int i = 0; i < hyp->usedCars.size(); i++){
      if(hyp->carsToUpdate[hyp->usedObjs[i]].size() == 0){
	MHTcar.push_back(i);
      }
    }
    int dim = MHTmeasure.size()+MHTobj.size()+MHTcar.size();
    double *cost = createCostMatrix(MHTmeasure, MHTobj, hyp->objects, MHTcar, hyp->cars, dt);
    // don't want more hypotheses than objects to match against, technically,
    // would want to look at combinatorics, but this will do for now
    int numHyp = min(MAXNUMHYPOTHESES,max(MHTobj.size()+MHTcar.size(),1));
    vector<murtyProb> *solutions = murty(cost, dim, numHyp);
    debugingfile << numHyp << " " << solutions->size()<< " " << numHypotheses << endl;
    int numObjs = MHTobj.size();
    int numCars = MHTcar.size();
    int n = numObjs+numCars;
    int m = MHTmeasure.size();
    for(int i = 0; i < solutions->size(); i++){
      hypothesis tempHyp = *hyp; // copies over the contents of the current hypothesis
      murtyProb sol = solutions->at(i);
      // update objects assigned to a measurement by MHT
      for(int j = 0; j < numObjs; j++){
	startIndex = numSynthScanPoints;
	endIndex = -1;
	if (sol.rowsol[j] < m){
	  // assignend to a measure, so perform update on this object
	  newpts.clear();
	  newdepthpts.clear();
	  sources.clear();
	  int k = MHTmeasure.at(sol.rowsol[j]);
	  if(endIndex < newObjects[k].endIndex){
	    endIndex = newObjects[k].endIndex;
	    dEnd = newObjects[k].dEnd;
	  }
	  if(startIndex > newObjects[k].startIndex){
	    startIndex = newObjects[k].endIndex;
	    dStart = newObjects[k].dStart;
	  }
	  // get all points
	  point2arr tmppts;
	  tmppts = point2arr(newObjects[k].lastScan);
	  for(l = 0; l < tmppts.size(); l++){
	    newpts.push_back(tmppts.arr[l]);
	  }
	  if(sortBySensor){
	    for(int l = 0; l < newObjects[k].source.size(); l++){
	      sources.push_back(newObjects[k].source.at(l));
	    }
	  }
	  tmppts.clear();
	  tmppts = point2arr(newObjects[k].lastScanDepth);
	  for(l = 0; l < tmppts.size(); l++){
	    newdepthpts.push_back(tmppts.arr[l]);
	  }
	  debugingfile << "Updating Object" << endl;
	  // update the the object
	  if(sortBySensor){
	    updateObject(&(tempHyp.objects[MHTobj.at(j)]),newpts,newdepthpts,sources,dStart,dEnd);
	  }else{
	    updateObject(&(tempHyp.objects[MHTobj.at(j)]),newpts,newdepthpts,dStart, dEnd);
	  }
	}
      }	  
      // update cars assigned to a measurement by MHT
      for(int j = numObjs; j < n; j++){
	if(sol.rowsol[j] < m){
	  // assigned to a measure, so perform update on this car
	  int k = MHTmeasure.at(sol.rowsol[j]);
	  
	  //reset vars for each new car
	  point2arr newdepthpts;
	  newpts.clear();
	  sources.clear();
	  startIndex = numSynthScanPoints;
	  endIndex = -1;
	  
	  if(endIndex < newObjects[k].endIndex){
	    endIndex = newObjects[k].endIndex;
	    dEnd = newObjects[k].dEnd;
	  }
	  if(startIndex > newObjects[k].startIndex){
	    startIndex = newObjects[k].endIndex;
	    dStart = newObjects[k].dStart;
	  }
	  
	  // get all points
	  point2arr tmppts, tmpdepthpts;
	  tmppts = point2arr(newObjects[k].lastScan);
	  tmpdepthpts = point2arr(newObjects[k].lastScanDepth);
	  point2 center = tmppts.average();
	  //get the source sensor for each point
	  if(sortBySensor){
	    for(int l = 0; l < newObjects[k].source.size(); l++){
	      sources.push_back(newObjects[k].source.at(l));
	    }
	  }
	  for(l = 0; l < tmppts.size(); l++){
	    newpts.push_back(tmppts.arr[l]);
	  }
	  for(l = 0; l < tmpdepthpts.size(); l++){
	    newdepthpts.push_back(tmpdepthpts.arr[l]);
	  }
	  debugingfile << "Update car" << endl;
	  // now update the car
	  if(sortBySensor){
	    updateCar(&(tempHyp.cars[MHTcar[j-numObjs]]), newpts, newdepthpts, sources, dStart, dEnd);
	  }else{
	    updateCar(&(tempHyp.cars[MHTcar.at(j-numObjs)]),newpts, newdepthpts, dStart, dEnd);
	  }
	}
      }
      // Finally, look for new tracks and add them to the temporary hypothesis
      for(int j = 0; j < m; j++){
	if(sol.rowsol[j+n] < m){
	  // the measurement is assigned to a new track, so add a new track to
	  // the temporary hypothesis
	  int k = MHTmeasure.at(j);
	  debugingfile << "Adding Car" << endl;
	  MHTaddObject(tempHyp, &(newObjects[k]));
	}
      }
     tempHyp.cost = sol.cost;
      // get the probabillity of the hypothesis by converting the cost
      // back into a probabillity, and multiplying by the parent hypothesis
      // probability
      tempHyp.prob = hyp->prob*exp(-1.0*tempHyp.cost);
      // finally, add this hypothesis to the que of possible hypotheses
      //      newHypotheses.push_back(tempHyp);
      hypArray[hypIndex] = tempHyp;
      hypIndex++;
    }
  }
  vector<hypothesis> newHypotheses; // stores the new hypotheses
  for(int z = 0; z < hypIndex; z++){
    newHypotheses.push_back(hypArray[z]);
  }
  // select the new hypotheses to use
  // NOTE: I reversed the sense of the < operator for hypothesis so that this
  // will sort in descending order of probabillity
  sort(newHypotheses.begin(),newHypotheses.end());
  numHypotheses = min(MAXNUMHYPOTHESES, newHypotheses.size());
  double normProb = 0;
  for(int i = 0; i < numHypotheses; i++){
    hypotheses[i] = newHypotheses[i];
    normProb += hypotheses[i].prob;
  }
  // normalize the probabillities to keep the numbers reasonable
  for(int i = 0; i < numHypotheses; i++){
    hypotheses[i].prob /= normProb;
  }
  delete[] hypArray;
}


/**
 * Creates the cost matrix for assigning tracks to detections
 * 
 * Costs are the negative log of the probability.  Below a threshold, they
 * are set to a max value
 */
double *LadarCarPerceptor::createCostMatrix(vector<int> &MHTmeasure, vector<int> &MHTobj, objectRepresentation *objs, vector<int> &MHTcar, carRepresentation *cars, double dt){
  dt = dt/1000.0; // Think I need to do this to convert into seconds
  double max = 10000; // max cost
  double thresh = .0001; // prob threshold to max cost
  int m = MHTmeasure.size();
  int n = MHTobj.size()+MHTcar.size();
  int dim = n+m;
  double *cost = new double[dim*dim]; // initialize the probability matrix (easier
  // to think of directly than the cost matrix
  // For a first cut, base the probability of a miss and the proability of a new
  // track based on the probability of an edge response based on a new object
  double probMiss = 1.0/sqrt((1+pow(1.0/dt,2))*2.0*PI)*exp(-.5*pow(SPEEDLIMIT*dt,2)/(1+pow(1.0/dt,2)));
  double probNew = probMiss; // have no idea what a better guess would be.
  //The lower right corner should be zero cost, thus one probability
  for(int i = n; i < dim; i++){
    for(int j = m; j < dim; j++){
      cost[i*dim+j]=0;
    }
  }
  //set the diagonals of the upper right and lower left corner blocks
  for(int i = 0; i < n; i++){
    for(int j = m; j < dim; j++){
      if(i != j-m){
	cost[i*dim+j] = max;
      }else{
	if(probMiss < thresh){
	  cost[i*dim+j] = max;
	}else{
	  cost[i*dim+j] = -1.0*log(probMiss);
	}
      }
    }
  }
  for(int i = n; i < dim; i++){
    for(int j = 0; j < m; j++){
      if(i-n != j){
	// not on the diagonal, so max cost
	cost[i*dim+j] = max;
      }else{
	if(probNew < thresh){
	  cost[i*dim+j] = max;
	}else{
	  cost[i*dim+j] = -1.0*log(probNew);
	}
      }
    }
  }
  // finally, need to compute the probabilities of each match, using the
  // range gate from the speed limit.  Start with the objects
  for(int i = 0; i < MHTobj.size(); i++){
    for(int j = 0; j < m; j++){
      objectRepresentation *tempTrack = &(objs[MHTobj[i]]);
      objectRepresentation *tempMeasure = &(newObjects[MHTmeasure[j]]);
      double trackX = tempTrack->Xmu_hat.getelem(0,0);
      double trackY = tempTrack->Ymu_hat.getelem(0,0);
      double trackSX = tempTrack->Xsigma_hat.getelem(0,0);
      double trackSY = tempTrack->Ysigma_hat.getelem(0,0);
      double mX = tempMeasure->el.center.x;
      double mY = tempMeasure->el.center.y;
      //check distance gating
      double dist = sqrt(pow(trackX-mX,2)+pow(trackY-mY,2));
      if(dist > dt*SPEEDLIMIT){
	cost[i*dim+j] = max;
      }else{
	Matrix pos(2);//[x;y]
	pos.setelem(0,0,mX-trackX);
	pos.setelem(1,0,mY-trackY);
	Matrix sigma(2,2);
	sigma.setelem(0,0,trackSX);
	sigma.setelem(1,1,trackSY);
	Matrix expF = pos.transpose()*sigma.inverse()*pos*-0.5;
	double prob = 1.0/sqrt(2*PI*(trackSX*trackSY))*exp(expF.getelem(0,0));
	if(prob < thresh){
	  cost[i*dim+j] = max;
	}else{
	  cost[i*dim+j] = -1.0*log(prob);
	}
      }
    }
  }
  return cost;
}  
      
      
  
  
  


/*
 * helper function for Murty's algorithm.  Forces the given row-column assignment
 * to be used as a solution to this problem.  Side-effects assigncost
 */
void LadarCarPerceptor::forceUse(int dim, double *assigncost, int row, int col){
  double max = 1000000;
  for(int i = 0; i < dim; i++){
    if( i != row )
	   assigncost[i*dim+col] = max;
    else
      assigncost[i*dim+col] = 0;
    if (i != col)
      assigncost[row*dim+i] = max;
    else
      assigncost[row*dim+col] = max;
  }
}

/*
 * helper funciton for Murty's algorithm.  Forces the given row-column
 * assignment not to be used as a solution to this problem. Side-efffects
 * assigncost
 */
void LadarCarPerceptor::forceSkip(int dim, double *assigncost, int row, int col){
  double max = 1000000;
  assigncost[row*dim+col] = max;
}

/*
 * Creates the NUM_HYPOTHESIS best solutions to the linear assignment problem
 * given in the cost matrix c, using murty's algorithm.  c is the cost matrix,
 * dim the dimension of the cost matrix (must be square), and num the number
 * of hypotheses to generate
 */
vector<murtyProb>* LadarCarPerceptor::murty(double *c, int dim, int num){
  vector<murtyProb> solutions;
  vector<murtyProb> probQue; // Holds problems to expand
  vector<murtyProbSorter> probQueOrder; // auxillary used to help sort the problem que (only need the best value)
  int *tempsol = new int[dim]; // allocate room to store the solution
  double cost = lap(dim,c,tempsol); // calculate the solution
  murtyProb prob(dim);
  prob.cost = cost;
  delete[] prob.costMat; // copy in the elements of the cost matrix
  prob.costMat = new double[dim*dim];
  for(int i = 0; i < dim*dim; i++){
    prob.costMat[i] = c[i];
  }
  //  prob.costMat = c;
  prob.dim = dim;
  delete[] prob.rowsol;
  prob.rowsol = new int[dim];
  for(int i = 0; i < dim; i++){
    prob.rowsol[i] = tempsol[i];
  }
  solutions.push_back(prob);
  debugingfile << "solutions pushed" << endl;
  probQue.push_back(prob); // need copies of the data so they can
  murtyProbSorter tempSorter;
  tempSorter.cost = prob.cost;
  tempSorter.problemIndex = 0;
  probQueOrder.push_back(tempSorter);
  //behave seperately (assignment for murtyProb clears the content of the variable so can't just reassign prob)
  int numFound = 1;
  while (numFound < num){
    // the first element of probQueOrder gives the lowest cost option
    prob = probQue[probQueOrder[0].problemIndex]; // performs deep copy, so can delete the source object
    probQue.erase(probQue.begin()+probQueOrder[0].problemIndex);
    for (int i = 0; i < dim; i++){
      int row = i;
      int col = prob.rowsol[i];
      forceSkip(dim, prob.costMat,row,col);
      double cost = lap(dim, prob.costMat, prob.rowsol); // side effects solution
      prob.cost = cost;
      probQue.push_back(prob); // should call copy constructor
      forceUse(dim, prob.costMat, row, col);
    }
    // create an auxillary vector of sorting elements, which can be manipulated
    // faster than murtyProbs can
    probQueOrder.clear(); // remove the current best
    for(int z = 0; z < probQue.size(); z++){
      murtyProbSorter temp;
      temp.problemIndex = z;
      temp.cost = probQue[z].cost;
    }
    // sort by cost
    sort(probQueOrder.begin(), probQueOrder.end());
    //sort(probQue.begin(), probQue.end());
    // add the best solutions from the prob que to the solutions
    solutions.push_back(probQue[probQueOrder[0].problemIndex]);
    debugingfile << "solution pushed" << " " << solutions.size() << endl;
    numFound++; // need to increment the number of found solutions if we
    // want the loop to terminate :)
  }
  vector<murtyProb> *tempRand = new vector<murtyProb>();
  for(int i = 0; i < solutions.size(); i++){
    murtyProb temp(solutions[i]);
    tempRand->push_back(temp);
  }
  return tempRand;
}   



/**
 * Implments the linear assignment problem according to
 "A Shortest Augmenting Path Algorithm for Dense and Sparse Linear Assignment
 Problems," Computing 38 325-340, 1987"

 written by Roy Jonker
 modified by Glenn Wagner
*/
double LadarCarPerceptor::lap(int dim, 
        double *assigncost,
			      int *rowsol){

// input:
// dim        - problem size
// assigncost - cost matrix indexed by (row*dim+column)

// output:
// rowsol     - column assigned to row in solution (assumed to be allocated)
// colsol     - row assigned to column in solution
// u          - dual variables, row reduction numbers
// v          - dual variables, column reduction numbers

  bool unassignedfound;
  int  i, imin, numfree = 0, prvnumfree, f, i0, k, freerow, *pred, *free;
 int j, j1, j2, endofpath, last, low, up, *collist, *matches;
  double min, h, umin, usubmin, v2, *d;

  int *colsol = new int[dim]; // row assigned to column in solution
  double *u = new double[dim]; // dual variable, row reduction numbers
  double *v = new double[dim]; // dual variables, column reduction numbers
  free = new int[dim];       // list of unassigned rows.
  collist = new int[dim];    // list of columns to be scanned in various ways.
  matches = new int[dim];    // counts how many times a row could be assigned.
  d = new double[dim];         // 'cost-distance' in augmenting path calculation.
  pred = new int[dim];       // row-predecessor of column in augmenting/alternating path.
  double BIG = 10000;
  // init how many times a row will be assigned in the column reduction.
  for (i = 0; i < dim; i++)  
    matches[i] = 0;

  // COLUMN REDUCTION 
  for (j = dim-1; j >= 0; j--)    // reverse order gives better results.
  {
    // find minimum cost over rows.
    min = assigncost[0*dim+j]; 
    imin = 0;
    for (i = 1; i < dim; i++)  
      if (assigncost[i*dim+j] < min) 
      { 
        min = assigncost[i*dim+j]; 
        imin = i;
      }
    v[j] = min; 

    if (++matches[imin] == 1) 
    { 
      // init assignment if minimum row assigned for first time.
      rowsol[imin] = j; 
      colsol[j] = imin; 
    }
    else
      colsol[j] = -1;        // row already assigned, column not assigned.
  }

  // REDUCTION TRANSFER
  for (i = 0; i < dim; i++) 
    if (matches[i] == 0)     // fill list of unassigned 'free' rows.
      free[numfree++] = i;
    else
      if (matches[i] == 1)   // transfer reduction from rows that are assigned once.
      {
        j1 = rowsol[i]; 
        min = BIG;
        for (j = 0; j < dim; j++)  
          if (j != j1)
            if (assigncost[i*dim+j] - v[j] < min) 
              min = assigncost[i*dim+j] - v[j];
        v[j1] = v[j1] - min;
      }

  // AUGMENTING ROW REDUCTION 
  int loopcnt = 0;           // do-loop to be done twice.
  do
  {
    loopcnt++;

    // scan all free rows.
    // in some cases, a free row may be replaced with another one to be scanned next.
    k = 0; 
    prvnumfree = numfree; 
    numfree = 0;             // start list of rows still free after augmenting row reduction.
    while (k < prvnumfree)
    {
      i = free[k]; 
      k++;

      // find minimum and second minimum reduced cost over columns.
      umin = assigncost[i*dim+0] - v[0]; 
      j1 = 0; 
      usubmin = BIG;
      for (j = 1; j < dim; j++) 
      {
        h = assigncost[i*dim+j] - v[j];
        if (h < usubmin)
          if (h >= umin) 
          { 
            usubmin = h; 
            j2 = j;
          }
          else 
          { 
            usubmin = umin; 
            umin = h; 
            j2 = j1; 
            j1 = j;
          }
      }

      i0 = colsol[j1];
      if (umin < usubmin) 
        // change the reduction of the minimum column to increase the minimum
        // reduced cost in the row to the subminimum.
        v[j1] = v[j1] - (usubmin - umin);
      else                   // minimum and subminimum equal.
        if (i0 >= 0)         // minimum column j1 is assigned.
        { 
          // swap columns j1 and j2, as j2 may be unassigned.
          j1 = j2; 
          i0 = colsol[j2];
        }

      // (re-)assign i to j1, possibly de-assigning an i0.
      rowsol[i] = j1; 
      colsol[j1] = i;

      if (i0 >= 0)           // minimum column j1 assigned earlier.
        if (umin < usubmin) 
          // put in current k, and go back to that k.
          // continue augmenting path i - j1 with i0.
          free[--k] = i0; 
        else 
          // no further augmenting reduction possible.
          // store i0 in list of free rows for next phase.
          free[numfree++] = i0; 
    }
  }
  while (loopcnt < 2);       // repeat once.

  // AUGMENT SOLUTION for each free row.
  for (f = 0; f < numfree; f++) 
  {
    freerow = free[f];       // start row of augmenting path.

    // Dijkstra shortest path algorithm.
    // runs until unassigned column added to shortest path tree.
    for (j = 0; j < dim; j++)  
    { 
      d[j] = assigncost[freerow*dim+j] - v[j]; 
      pred[j] = freerow;
      collist[j] = j;        // init column list.
    }

    low = 0; // columns in 0..low-1 are ready, now none.
    up = 0;  // columns in low..up-1 are to be scanned for current minimum, now none.
             // columns in up..dim-1 are to be considered later to find new minimum, 
             // at this stage the list simply contains all columns 
    unassignedfound = FALSE;
    do
    {
      if (up == low)         // no more columns to be scanned for current minimum.
      {
        last = low - 1; 

        // scan columns for up..dim-1 to find all indices for which new minimum occurs.
        // store these indices between low..up-1 (increasing up). 
        min = d[collist[up++]]; 
        for (k = up; k < dim; k++) 
        {
          j = collist[k]; 
          h = d[j];
          if (h <= min)
          {
            if (h < min)     // new minimum.
            { 
              up = low;      // restart list at index low.
              min = h;
            }
            // new index with same minimum, put on undex up, and extend list.
            collist[k] = collist[up]; 
            collist[up++] = j; 
          }
        }

        // check if any of the minimum columns happens to be unassigned.
        // if so, we have an augmenting path right away.
        for (k = low; k < up; k++) 
          if (colsol[collist[k]] < 0) 
          {
            endofpath = collist[k];
            unassignedfound = TRUE;
            break;
          }
      }

      if (!unassignedfound) 
      {
        // update 'distances' between freerow and all unscanned columns, via next scanned column.
        j1 = collist[low]; 
        low++; 
        i = colsol[j1]; 
        h = assigncost[i*dim+j1] - v[j1] - min;

        for (k = up; k < dim; k++) 
        {
          j = collist[k]; 
          v2 = assigncost[i*dim+j] - v[j] - h;
          if (v2 < d[j])
          {
            pred[j] = i;
            if (v2 == min)   // new column found at same minimum value
              if (colsol[j] < 0) 
              {
                // if unassigned, shortest augmenting path is complete.
                endofpath = j;
                unassignedfound = TRUE;
                break;
              }
              // else add to list to be scanned right away.
              else 
              { 
                collist[k] = collist[up]; 
                collist[up++] = j; 
              }
            d[j] = v2;
          }
        }
      } 
    }
    while (!unassignedfound);

    // update column prices.
    for (k = 0; k <= last; k++)  
    { 
      j1 = collist[k]; 
      v[j1] = v[j1] + d[j1] - min;
    }

    // reset row and column assignments along the alternating path.
    do
    {
      i = pred[endofpath]; 
      colsol[endofpath] = i; 
      j1 = endofpath; 
      endofpath = rowsol[i]; 
      rowsol[i] = j1;
    }
    while (i != freerow);
  }

  // calculate optimal cost.
  double lapcost = 0;
  for (i = 0; i < dim; i++)  
  {
    j = rowsol[i];
    u[i] = assigncost[i*dim+j] - v[j];
    lapcost = lapcost + assigncost[i*dim+j]; 
  }

  // free reserved memory.
  delete[] pred;
  delete[] free;
  delete[] collist;
  delete[] matches;
  delete[] d;
  delete[] colsol;
  delete[] u;
  delete[] v;

  return lapcost;
}


/**
 * This function is called to add a new track to the passed hypothesis
 *
 * Fields set in the object's representation:
 * ID, velocity, ptsinvelocity, center, el
 * dStart, dEnd, seenStart, seenEnd, startPoint, endPoint,
 * timesSeen, lastSeen, history, lastScan
 * Xmu, Ymu, Xsigma, Ysigma, Xmu_hat, Ymu_hat,
 * Xsigma_hat, Ysigma_hat
 **/
void LadarCarPerceptor::MHTaddObject(hypothesis &hyp, objectRepresentation* newObj){
  hyp.numObjects = hyp.numObjects+1;
  
  newObj->velocity = point2(0,0);
  newObj->ptsinvelocity = 0;
  if((hyp.mapIndicies.size() > 0) && (hyp.usedObjs.size() < MAXNUMOBJS)){
    // find out which spot in object array is next up
    int newIndex = hyp.openObjs.back();
    hyp.openObjs.pop_back();
    hyp.usedObjs.push_back(newIndex);
    
    hyp.objects[newIndex].ID = numObjects;
    hyp.objects[newIndex].mapID = hyp.mapIndicies.back();
    hyp.mapIndicies.pop_back();

    hyp.objects[newIndex].dStart = newObj->dStart;
    hyp.objects[newIndex].dEnd = newObj->dEnd;
    hyp.objects[newIndex].timesSeen = 1;
    hyp.objects[newIndex].lastSeen = numFrames;
    // transfering vectors of scan/range/angle history
    point2arr newscans = point2arr(newObj->lastScan);
    hyp.objects[newIndex].lastScan = newObj->lastScan;
    hyp.objects[newIndex].lastScanDepth = newObj->lastScanDepth;
    hyp.objects[newIndex].el = ellipse(newscans);

    hyp.objects[newIndex].history.clear();
    hyp.objects[newIndex].history.push_back(newscans.average());

    
    hyp.objects[newIndex].isStopped = false;
    hyp.objects[newIndex].lastTimeMoving = DGCgettime();

    hyp.objects[newIndex].seenStart = newObj->seenStart;
    hyp.objects[newIndex].seenEnd = newObj->seenEnd;
    hyp.objects[newIndex].startPoint = point2(newObj->startPoint);
    hyp.objects[newIndex].endPoint = point2(newObj->endPoint);
    hyp.objects[newIndex].startIndex = newObj->startIndex;
    hyp.objects[newIndex].endIndex = newObj->endIndex;

    // Store the initial position
    hyp.objects[newIndex].initPos = hyp.objects[newIndex].el.center;

    //initizlizing KF variables
    hyp.objects[newIndex].Xmu.resetSize(2,1);
    hyp.objects[newIndex].Ymu.resetSize(2,1);
    hyp.objects[newIndex].Xsigma.resetSize(2,2);
    hyp.objects[newIndex].Ysigma.resetSize(2,2);
    hyp.objects[newIndex].Xmu_hat.resetSize(2,1);
    hyp.objects[newIndex].Ymu_hat.resetSize(2,1);
    hyp.objects[newIndex].Xsigma_hat.resetSize(2,2);
    hyp.objects[newIndex].Ysigma_hat.resetSize(2,2);

    double XMelems[] = {hyp.objects[newIndex].el.center.x, 0};
    double YMelems[] = {hyp.objects[newIndex].el.center.y, 0};
    double XSelems[] = {1,0,0,1};
    double YSelems[] = {1,0,0,1};

    hyp.objects[newIndex].Xmu.setelems(XMelems);
    hyp.objects[newIndex].Xsigma.setelems(XSelems);
    hyp.objects[newIndex].Ymu.setelems(YMelems);
    hyp.objects[newIndex].Ysigma.setelems(YSelems);
    // Need to have the prediction matricies initialized so that the kalman
    // filter updating step can be performed correctly.  Since the update should
    // be called the same scan as the object creation, can just copy the 
    // current values into the prediction.
    hyp.objects[newIndex].Xmu_hat.setelems(XMelems);
    hyp.objects[newIndex].Xsigma_hat.setelems(XSelems);
    hyp.objects[newIndex].Ymu_hat.setelems(YMelems);
    hyp.objects[newIndex].Ysigma_hat.setelems(YSelems);
  } else {

    ERROR("trying to add obj; usedObjs size = %d, mapIndices size = %d", usedObjs.size(), mapIndices.size());
  }
}

/**
 * Checks if the velocity has been above a threshold for a specified time,
 * and didn't change direction by more than a given angular value
 * between any pair of scans
 */
void LadarCarPerceptor::MHTcheckMotion2(){
  for(int w = 0; w < numHypotheses; w++){
    // need to check all hypotheses
    hypothesis *hyp = &(hypotheses[w]);
    for(int i = 0; i < hyp->usedObjs.size(); i++){
      int index = hyp->usedObjs.at(i);
      double vX = hyp->objects[index].Xmu.getelem(1,0);
      double vY = hyp->objects[index].Ymu.getelem(1,0);
      if(hyp->objects[index].timesSeen > 30){
	if(sqrt(pow(vX,2)+pow(vY,2)) > carVelThresh){
	  if((hyp->objects[index].oldVel.x*vX+hyp->objects[index].oldVel.y*vY)/(hyp->objects[index].oldVel.norm()*sqrt(vX*vX+vY*vY)) >=1.0/sqrt(2)){
	    // direction of velocity has changed by less than 45 degrees
	    hyp->objects[index].timeMoving++;
	    if(hyp->objects[index].timeMoving > carTimeThresh)
	      MHTobj2car(hyp, i);
	  }else{
	    hyp->objects[index].timeMoving = 0;
	  }
	}else{
	  hyp->objects[index].timeMoving = 0;
	}
      }
      hyp->objects[index].oldVel.x = vX;
      hyp->objects[index].oldVel.y = vY;
    }
  }
}
	      

  /**
   * This function turns an object into a car within the specified hypothesis
   *
   * Sets the following fields in carRepresentation:   *   X, Y, dx1, dy1, dx2, dy2
   *   seenFront, seenBack, seenCorner
   *   Xmu, Xsigma, Ymu, Ysigma
   *   ID, lastSeen, timesSeen, history
   *   velocity, ptsinvelocity
   **/
  //#warning "this code should remove the object at index, in addition to making a new car...possibly replace with addCar(&object), and removeObject(index)"
void LadarCarPerceptor::MHTobj2car(hypothesis *hyp, int index) {
  //  cout<<"trying to turn an object into a car"<<endl;
  if((hyp->usedCars.size() < MAXNUMCARS-1) && (hyp->mapIndicies.size() > 0)) { 
    int i = hyp->usedObjs.at(index);

    point2arr points(hyp->objects[i].lastScan);
      
    //figure out which spot in car array new car will occupy
    int temp;
    temp = hyp->openCars.back();
    hyp->openCars.pop_back();
    hyp->usedCars.push_back(temp);
    
    if(sortBySensor){
      // copy over the source vector
      hyp->cars[temp].source = hyp->objects[i].source;
      // if necessary, filter the points by source sensor
      int bumperPts = 0;
      int roofPts = 0;
      int stereoPts = 0;
      for(int w = 0; w < hyp->cars[temp].source.size(); w++){
	if(hyp->cars[temp].source.at(w) == BUMPER){
	  bumperPts++;
	}else if(hyp->cars[temp].source.at(w) == ROOF){
	  roofPts++;
	}else{
	  stereoPts++;
	}
      }
      int thresh = 0;
      if(bumperPts > MINNUMPOINTS){
	thresh = BUMPER;
      }else if(bumperPts + roofPts > MINNUMPOINTS){
	thresh = ROOF;
      }else{
	thresh = STEREO;
      }
      point2arr sortpts;
      for(int w = 0; w < hyp->cars[temp].source.size(); w++){
	if(hyp->cars[temp].source.at(w) <=thresh){
	  sortpts.push_back(points[w]);
	}
      }
      points = sortpts;
    }

    // Initialize variables for shape kalman filter before call to fitCar
    hyp->cars[temp].Smu.resetSize(3,1);
    hyp->cars[temp].Smu_hat.resetSize(3,1);
    hyp->cars[temp].Ssigma.resetSize(3,3);
    hyp->cars[temp].Ssigma_hat.resetSize(3,3);
    // Need to initialize the shape filter with reasonable values
    double smuelem[] = {1.5,4,atan2(objects[i].Ymu.getelem(1,0),objects[i].Xmu.getelem(1,0))};
    hyp->cars[temp].Smu.setelems(smuelem);
    hyp->cars[temp].Smu_hat.setelems(smuelem);
    double tempx = objects[i].Xmu.getelem(1,0);
    double tempy = objects[i].Ymu.getelem(1,0);
    double ssigmaelem[] = {.3,0,0,0,2,0,0,0,.1};
    hyp->cars[temp].Ssigma.setelems(ssigmaelem);
    hyp->cars[temp].Ssigma_hat.setelems(ssigmaelem);
    //    MSG("Initial Theta  %f",hyp->cars[temp].Smu.getelem(2,0));
    int corner;


    //  fprintf(stderr, "fit car! corner: %f, %f \n", cars[temp].N, cars[temp].E);
    hyp->cars[temp].Xmu_hat = hyp->objects[i].Xmu_hat;
    hyp->cars[temp].Ymu_hat = hyp->objects[i].Ymu_hat;
    hyp->cars[temp].seg1 = point2(0,0);
    hyp->cars[temp].seg2 = point2(0,0);
    corner = fitCar2(points, UNDEFINED_CORNER, point2(0.0, 0.0), hyp->objects[i].seenStart, hyp->objects[i].seenEnd,&(hyp->cars[temp]), hyp->objects[i].Xmu.getelem(1,0), hyp->objects[i].Ymu.getelem(1,0), 0.0, 0.0); 
    hyp->cars[temp].seenCorner = corner;    


    //TODO: do I want to carry over any more from the obj's KF?

    hyp->cars[temp].Xmu.resetSize(2,1);
    hyp->cars[temp].Ymu.resetSize(2,1);
    hyp->cars[temp].Xsigma.resetSize(2,2);
    hyp->cars[temp].Ysigma.resetSize(2,2);
    hyp->cars[temp].Xmu_hat.resetSize(2,1);
    hyp->cars[temp].Ymu_hat.resetSize(2,1);
    hyp->cars[temp].Xsigma_hat.resetSize(2,2);
    hyp->cars[temp].Ysigma_hat.resetSize(2,2);
    point2 currCorner = hyp->cars[temp].corners[hyp->cars[temp].trackedCorner];
    double XMelems[] = {currCorner.x, hyp->objects[i].Xmu.getelem(1,0)};
    double YMelems[] = {currCorner.y, hyp->objects[i].Ymu.getelem(1,0)};
    //    double XSelems[] = {0,0,0,0};
    //    double YSelems[] = {0,0,0,0};
    double XSelems[] = {hyp->objects[i].Xsigma.getelem(0,0), hyp->objects[i].Xsigma.getelem(0,1),hyp->objects[i].Xsigma.getelem(1,0), hyp->objects[i].Xsigma.getelem(1,1) };
    double YSelems[] = {hyp->objects[i].Ysigma.getelem(0,0), hyp->objects[i].Ysigma.getelem(0,1),hyp->objects[i].Ysigma.getelem(1,0),hyp->objects[i].Ysigma.getelem(1,1) };
    hyp->cars[temp].Xmu.setelems(XMelems);
    hyp->cars[temp].Xsigma.setelems(XSelems);
    hyp->cars[temp].Ymu.setelems(YMelems);
    hyp->cars[temp].Ysigma.setelems(YSelems);
      
    hyp->cars[temp].ID = hyp->numCars;
    hyp->cars[temp].mapID = hyp->mapIndicies.back();
    hyp->mapIndicies.pop_back();

    hyp->cars[temp].lastSeen = hyp->objects[i].lastSeen;  
    hyp->cars[temp].timesSeen = 1;
    hyp->cars[temp].dTrackedCorner = point2(0,0);
    
    hyp->cars[temp].lastScan = point2arr(hyp->objects[i].lastScan);
    hyp->cars[temp].lastScanDepth = point2arr(hyp->objects[i].lastScanDepth);

    hyp->cars[temp].history.clear();
    for(unsigned int j=0; j<hyp->objects[i].history.size(); j++) {
      hyp->cars[temp].history.push_back(hyp->objects[i].history[j]);
    }

    hyp->cars[temp].velocity = point2(0.0, 0.0);
    hyp->cars[temp].ptsinvelocity = 0;

    hyp->cars[temp].lastUpdated = DGCgettime();

    hyp->numCars++; //we've found a car
    
    removeObject(index);
      
  } else {
    cerr<<"not enough space in cars array!!, or mapIndices size = "<<mapIndices.size()<<endl;
  }  // end of checking space
}  // end of obj2car


/**
 * removes the object from hypothesis by putting its index back in openObjs,
 * removing the index from usedObjs, ,and sending msg to the map to clear the
 * corresponding map elements
 */
void LadarCarPerceptor::MHTremoveObject(hypothesis *hyp, int index){
  int bytesSent;
  MapElement el;
  int j = hyp->usedObjs.at(index);
  el.clear();
  el.setTypeClear();

  hyp->mapIndicies.push_back(hyp->objects[j].mapID);

  if(!onlySendCars){
    el.setId(mapCarID, hyp->objects[j].mapID);
    el.state = rawState;
    bytesSent = sendMapElement(&el, outputSubgroup);

    if(useDisplay){
      el.setId(mapCarID,moduleObjectID,hyp->objects[j].ID);
      bytesSent = sendMapElement(&el, debugSubgroup);

      el.setId(mapCarID,moduleEllipseID,hyp->objects[j].ID);
      bytesSent = sendMapElement(&el, debugSubgroup);

      el.setId(mapCarID,objectVelocityID,hyp->objects[j].ID);
      bytesSent = sendMapElement(&el, debugSubgroup);
    }
  }
  hyp->openObjs.push_back(usedObjs.at(index));
}

/**
 * removes car from hypothesis by putting it's index on openCars
 */
void LadarCarPerceptor::MHTremoveCar(hypothesis *hyp, int index){
  int bytesSent;
  MapElement el;
  int j = hyp->usedCars.at(index);
  el.clear();

  mapIndices.push_back(hyp->cars[j].mapID);

  el.setId(mapCarID, hyp->cars[j].mapID);
  el.setTypeClear();
  bytesSent = sendMapElement(&el, outputSubgroup);

  if(useDisplay){
    el.clear();
    el.setTypeClear();


    el.setId(mapCarID,moduleCarHistoryID,hyp->cars[j].ID);
    bytesSent = sendMapElement(&el, debugSubgroup);

    el.setId(mapCarID,carVelocityID,hyp->cars[j].ID);
    bytesSent = sendMapElement(&el, debugSubgroup);

    el.setId(mapCarID,carKFVelocityID,hyp->cars[j].ID);
    bytesSent = sendMapElement(&el, debugSubgroup);

    el.setId(mapCarID,predictedCarID,hyp->cars[j].ID);
    bytesSent = sendMapElement(&el, debugSubgroup);

    el.setId(mapCarID,moduleCarSeg1ID,hyp->cars[j].ID);
    bytesSent = sendMapElement(&el, debugSubgroup);

    el.setId(mapCarID,moduleCarSeg2ID,hyp->cars[j].ID);
    bytesSent = sendMapElement(&el, debugSubgroup);
  }

  openCars.push_back(usedCars.at(index));
  carIterator = usedCars.begin();
  carIterator+=index;
  usedCars.erase(carIterator);

}




void LadarCarPerceptor::MHTKFupdate() {

  Matrix K1;
  K1 = Matrix(2,2);
  Matrix K2;
  K2 = Matrix(2,2);
  Matrix Xerr;
  Xerr = Matrix(1,1);
  Matrix Yerr;
  Yerr = Matrix(1,1);
  Matrix tmp;
  tmp = Matrix(1,1);
  Matrix tmp2;
  tmp2 = Matrix(1,1);
  for(int w = 0; w <  numHypotheses; w++){
    hypothesis *hyp = &(hypotheses[w]);
    for(unsigned int i=0; i<hyp->usedObjs.size(); i++) {
      int j = hyp->usedObjs.at(i);
      if(hyp->objects[j].timesSeen > 1) {
	if(hyp->objects[j].lastSeen == numFrames) {
	  double grr[] = {hyp->objects[j].el.center.x};
	  tmp.setelems(grr);
	  Xerr = tmp - C*hyp->objects[j].Xmu_hat;
	  K1 = hyp->objects[j].Xsigma_hat * C.transpose() * (C*hyp->objects[j].Xsigma_hat*C.transpose() + Q).inverse();
	  hyp->objects[j].Xmu = hyp->objects[j].Xmu_hat + K1 * Xerr;
	  hyp->objects[j].Xsigma = (I - K1*C) * hyp->objects[j].Xsigma_hat;
	  
	  double grr2[] = {hyp->objects[j].el.center.y};
	  tmp2.setelems(grr2);
	  
	  Yerr = tmp2 - C*hyp->objects[j].Ymu_hat;
	  K2 = hyp->objects[j].Ysigma_hat * C.transpose() * (C*hyp->objects[j].Ysigma_hat*C.transpose() + Q).inverse();
	  hyp->objects[j].Ymu = hyp->objects[j].Ymu_hat + K2 * Yerr;
	  hyp->objects[j].Ysigma = (I - K2*C) * hyp->objects[j].Ysigma_hat;
	} else {
	  
	  hyp->objects[j].Xmu = hyp->objects[j].Xmu_hat;
	  hyp->objects[j].Xsigma = hyp->objects[j].Xsigma_hat;
	  hyp->objects[j].Ymu = hyp->objects[j].Ymu_hat;
	  hyp->objects[j].Ysigma = hyp->objects[j].Ysigma_hat;
	  
	}
      }
    }
    

    for(unsigned int i=0; i<hyp->usedCars.size(); i++) {
      int j = hyp->usedCars.at(i);
      if(hyp->cars[j].timesSeen > 1) {
	if(hyp->cars[j].lastSeen == numFrames) {
	  //shift tracked corner, if necessary
	  if(0 != hyp->cars[j].dTrackedCorner.norm()) {
	    point2 shiftPt = hyp->cars[j].dTrackedCorner;
	    //	    double newMatrix[2];
	    double newXmu[] = {hyp->cars[j].Xmu.getelem(0,0) + shiftPt.x, hyp->cars[j].Xmu.getelem(1,0)};
	    hyp->cars[j].Xmu.setelems(newXmu);
	    double newYmu[] = {hyp->cars[j].Ymu.getelem(0,0) + shiftPt.y, hyp->cars[j].Ymu.getelem(1,0)};
	    hyp->cars[j].Ymu.setelems(newYmu);
	    
	    double newXmuHat[] = {hyp->cars[j].Xmu_hat.getelem(0,0) + shiftPt.x, hyp->cars[j].Xmu_hat.getelem(1,0)};
	    hyp->cars[j].Xmu_hat.setelems(newXmuHat);
	    double newYmuHat[] = {hyp->cars[j].Ymu_hat.getelem(0,0) + shiftPt.y, hyp->cars[j].Ymu_hat.getelem(1,0)};
	    hyp->cars[j].Ymu_hat.setelems(newYmuHat);
	    
	    //	    MSG("switched tracked corner in KF");
	  }
	  
	  
	  point2 currTrackedPoint = hyp->cars[j].corners[cars[j].trackedCorner];
	  double grr[] = {currTrackedPoint.x};
	  tmp.setelems(grr);
	  
	  Xerr = tmp - C*hyp->cars[j].Xmu_hat;
	  K1 = hyp->cars[j].Xsigma_hat * C.transpose() * (C*hyp->cars[j].Xsigma_hat*C.transpose() + Q).inverse();
	  hyp->cars[j].Xmu = hyp->cars[j].Xmu_hat + K1 * Xerr;
	  hyp->cars[j].Xsigma = (I - K1*C) * hyp->cars[j].Xsigma_hat;
	  
	  double grr2[] = {currTrackedPoint.y};
	  tmp2.setelems(grr2);
	  
	  Yerr = tmp2 - C*hyp->cars[j].Ymu_hat;
	  K2 = hyp->cars[j].Ysigma_hat * C.transpose() * (C*hyp->cars[j].Ysigma_hat*C.transpose() + Q).inverse();
	  hyp->cars[j].Ymu = hyp->cars[j].Ymu_hat + K2 * Yerr;
	  hyp->cars[j].Ysigma = (I - K2*C) * hyp->cars[j].Ysigma_hat;
	} else {
	  
	  hyp->cars[j].Xmu = hyp->cars[j].Xmu_hat;
	  hyp->cars[j].Xsigma = hyp->cars[j].Xsigma_hat;
	  hyp->cars[j].Ymu = hyp->cars[j].Ymu_hat;
	  hyp->cars[j].Ysigma = hyp->cars[j].Ysigma_hat;
	}
      }
    }
  }
}


 
void LadarCarPerceptor::MHTKFpredict(double dt) {


  A.resetSize(2,2);
  double Aelems[] = {1,dt,0,1};
  A.setelems(Aelems);

  for(int w = 0; w < numHypotheses; w++){
    hypothesis *hyp = &(hypotheses[w]);
    for(unsigned int i=0; i<hyp->usedObjs.size() ; i++) {
      int j = hyp->usedObjs.at(i);
      if(hyp->objects[j].timesSeen > 1) {
	hyp->objects[j].Xmu_hat =    A*hyp->objects[j].Xmu;
	hyp->objects[j].Xsigma_hat = A*hyp->objects[j].Xsigma*A.transpose() + R;
	hyp->objects[j].Ymu_hat =    A*hyp->objects[j].Ymu;
	hyp->objects[j].Ysigma_hat = A*hyp->objects[j].Ysigma*A.transpose() + R; 
      }
    } //end cycling through vector of objects
    
    
    //and, objcars
    
    for(unsigned int i=0; i<hyp->usedCars.size(); i++) {
      int j = hyp->usedCars.at(i);
      hyp->cars[j].Xmu_hat =    A*hyp->cars[j].Xmu;
      hyp->cars[j].Xsigma_hat = A*hyp->cars[j].Xsigma*A.transpose() + R;
      hyp->cars[j].Ymu_hat =    A*hyp->cars[j].Ymu;
      hyp->cars[j].Ysigma_hat = A*hyp->cars[j].Ysigma*A.transpose() + R;
      hyp->cars[j].Smu_hat = SA*hyp->cars[j].Smu;
      hyp->cars[j].Ssigma_hat = SA*hyp->cars[j].Ssigma*SA.transpose() + SR;
    } //end cycling through numCars
  }// end cycling through hypotheses
} //end KFpredict()



void LadarCarPerceptor::MHTcleanObjects()
{
  
  int j, m; 
  int diff;
  for(int w = 0; w < numHypotheses; w++){
    hypothesis *hyp = &(hypotheses[w]);
    for(unsigned int i=0; i<usedObjs.size(); i++)
      {
	//checking if obj should be removed based on time
	j = hyp->usedObjs.at(i);
	diff = numFrames - hyp->objects[j].lastSeen;
	if(diff > OBJECTTIMEOUT) {
	  MHTremoveObject(hyp,i);
	}
	
	//checking if two objects should be merged
	for(unsigned int k=0; k<hyp->usedObjs.size(); k++)
	  {
	    m = hyp->usedObjs.at(k);
	    int overlap = hyp->objects[j].el.overlap(&(hyp->objects[m].el), MARGIN);
	    //if they overlap and aren't the same object
	    if((1 == overlap) && (j!=m)) {
	      //TODO: this doesn't handle object histories intelligently
	      
	      mergeObjects(&(hyp->objects[j]), &(hyp->objects[m]));
	      MHTremoveObject(hyp,k);
	      
	    }
	  }
      }
  }
}


//TODO: add a free-space condition for removing cars
void LadarCarPerceptor::MHTcleanCars()
{
  int j; 
  int diff;
  for(int w = 0; w < numHypotheses; w++){
    hypothesis *hyp = &(hypotheses[w]);
    for(unsigned int i=0; i<hyp->usedCars.size(); i++)
      {
	j = hyp->usedCars.at(i);
	diff = numFrames - hyp->cars[j].lastSeen;
	//    fprintf(stderr, "current frame: %d, last seen: %d \n", numFrames, objects[j].lastSeen);
	if(diff > CARTIMEOUT) {
	  //      fprintf(stderr, "tryng to remove car # %d! \n", j);
	  MHTremoveCar(hyp,i);
	}
      }
  }
}



  /**
   * collecting all the sendMapElement function in one place
   * 
   * for type object, there are 3 cases:
   *   object already classified as car -> was sent as type car (in send cars)
   *   object seen fewer than X times -> send as type car
   *   object seen greater than X times -> send as type obj
   *
   * all tracked cars are sent as type vehicle (duh)
   *
   * ONLY SENDS ELEMENTS IN THE MOST PROBABLE HYPOTHESIS
   *
   * TODO: move sending untracked points here. 
   **/
void  LadarCarPerceptor::MHTsendToMap() 
{
  hypothesis *hyp = &(hypotheses[0]);
  MapElement el;
  point2_uncertain tmppt;
  //  vector <point2> objectPoints;
  point2arr_uncertain objectPoints;
  int bytesSent;


  if(!onlySendCars){
    // *************sending tracked objects*****************
    for(unsigned int k=0; k<hyp->usedObjs.size(); k++) {
      int j = hyp->usedObjs.at(k);
      if(hyp->objects[j].lastUpdated > lastSentCars) {
	el.clear();
	objectPoints.clear();
	point2arr lastScan = hyp->objects[j].lastScanDepth;
	point2arr tmparr = hyp->objects[j].lastScan;
	tmparr.reverse();
	lastScan.connect(tmparr);
	
#warning "magic # - add better function here"
	objectPoints = point2arr(lastScan.simplify(.2));
	
	el.setId(mapCarID,hyp->objects[j].mapID);
	//if we've tracked an object w/o observing motion, can
	//call it stationary. otherwise, planner wants type vehicle
	//TODO: more sophisticated check?
#warning "magic #"
	if(200 <= hyp->objects[j].timesSeen) {
	  el.setTypeObstacle();
	  el.plotColor = MAP_COLOR_YELLOW;
	} else {
	  el.setTypeLadarObstacle();
	  el.plotColor = MAP_COLOR_GREEN;
	}
	
	double xSigma = hyp->objects[j].Xsigma.getelem(0,0);
	double ySigma = hyp->objects[j].Ysigma.getelem(0,0);
	for(int w = 0; w < objectPoints.size(); w++){
	  if(fabs(xSigma)>fabs(ySigma)){
	    objectPoints.arr.at(w).max_var = fabs(xSigma);
	    objectPoints.arr.at(w).min_var = fabs(ySigma);
	    objectPoints.arr.at(w).axis = 0;
	  }else{
	    objectPoints.arr.at(w).max_var = fabs(ySigma);
	    objectPoints.arr.at(w).min_var = fabs(xSigma);
	    objectPoints.arr.at(w).axis = PI/2;
	  }
	}
	el.setGeometry(objectPoints);
	
	double velocityUncertainty = sqrt(sqrt(hyp->objects[j].Xsigma.getelem(1,1)) + sqrt(hyp->objects[j].Ysigma.getelem(1,1)));
	el.velocity = point2_uncertain(hyp->objects[j].Xmu.getelem(1,0), hyp->objects[j].Ymu.getelem(1,0), velocityUncertainty);
	
	el.state = rawState;
	if(hyp->objects[j].isStopped) {
	  DGCgettime(currTime);
	  el.timeStopped = (currTime - hyp->objects[j].lastTimeMoving)/1000000;
	} else {
	  el.timeStopped = 0.0;
	}
	
	
	//TODO: this is totally arbitrary, just wanting to give some idea to the map
	el.conf = min(1.0, hyp->objects[j].timesSeen/100);
	
	//    MSG("zcoord = %f", el.geometry[0].z);
	bytesSent = sendMapElement(&el, outputSubgroup);
      } else {//end checking if object is recent
	//      MSG("not sending object %d, not recently updated", j);
      }
    } // end checking each object
  }
  
  // ***************** sending cars***************************
  double corners[8];
  for(unsigned int k=0; k<hyp->usedCars.size(); k++) {
    int j = hyp->usedCars.at(k);
    if(hyp->cars[j].lastUpdated > lastSentCars) {
      // send the raw scan as the data
      objectPoints.clear();
      el.clear();
      if(!useRect){
	point2arr lastScan = hyp->cars[j].lastScanDepth;
	point2arr tmparr = hyp->cars[j].lastScan;
	tmparr.reverse();
	lastScan.connect(tmparr);
	
	
#warning "magic # - add better function here"
	objectPoints = point2arr(lastScan.simplify(.2));
	for(unsigned int m=0; m < lastScan.size(); m++) {
	  tmppt = point2(lastScan.arr[m].x, lastScan.arr[m].y, lastScan.arr[m].z);
	  objectPoints.push_back(tmppt);
	}
      }else{
	// Send a fitted rectangle
	/*
	  corners[0] = cars[j].corners[BACK_RIGHT].x;
	  corners[1] = cars[j].corners[BACK_RIGHT].y;
	  corners[2] = cars[j].corners[FRONT_RIGHT].x;
	  corners[3] = cars[j].corners[FRONT_RIGHT].y;
	  corners[4] = cars[j].corners[FRONT_LEFT].x;
	  corners[5] = cars[j].corners[FRONT_LEFT].y;
	  corners[6] = cars[j].corners[BACK_LEFT].x;
	  corners[7] = cars[j].corners[BACK_LEFT].y;
	*/
	// Use estimated position to get display
	//fill in the corners
	//Xmu follows the tracked corner
	double tempx = hyp->cars[j].Xmu.getelem(0,0);
	double tempy = hyp->cars[j].Ymu.getelem(0,0);
	switch (hyp->cars[j].trackedCorner) {
	case BACK_RIGHT:
	  corners[0] = tempx;
	  corners[1] = tempy;
	  corners[2] = tempx+hyp->cars[j].length.x;
	  corners[3] = tempy+hyp->cars[j].length.y;
	  corners[4] = tempx+hyp->cars[j].length.x+hyp->cars[j].width.x;
	  corners[5]=tempy+hyp->cars[j].length.y+hyp->cars[j].width.y;
	  corners[6] = tempx+hyp->cars[j].width.x;
	  corners[7] = tempy+hyp->cars[j].width.y;
	  /*
	    corners[2] = tempx + hyp->cars[j].Smu.getelem(1,0)*cos(hyp->cars[j].Smu.getelem(2,0));
	    corners[3] = tempy+hyp->cars[j].Smu.getelem(1,0)*sin(hyp->cars[j].Smu.getelem(2,0));
	    corners[4] = tempx+hyp->cars[j].Smu.getelem(1,0)*cos(hyp->cars[j].Smu.getelem(2,0))+hyp->cars[j].Smu.getelem(0,0)*cos(hyp->cars[j].Smu.getelem(2,0)+PI/2);
	    corners[5] = tempy+hyp->cars[j].Smu.getelem(1,0)*sin(hyp->cars[j].Smu.getelem(2,0))+hyp->cars[j].Smu.getelem(0,0)*sin(hyp->cars[j].Smu.getelem(2,0)+1.5707);
	    corners[6] = tempx + hyp->cars[j].Smu.getelem(0,0)*cos(hyp->cars[j].Smu.getelem(2,0)+PI/2);
	    corners[7] = tempy + hyp->cars[j].Smu.getelem(0,0)*sin(hyp->cars[j].Smu.getelem(2,0)+PI/2);*/
	  
	  break;
	  
	case BACK_LEFT:
	  // Don't care about order, but need to reverse the width terms
	  corners[6] = tempx;
	  corners[7] = tempy;
	  corners[4] = tempx+hyp->cars[j].length.x;
	  corners[5] = tempy+hyp->cars[j].length.y;
	  corners[2] = tempx+hyp->cars[j].length.x-hyp->cars[j].width.x;
	  corners[3]=tempy+hyp->cars[j].length.y-hyp->cars[j].width.y;
	  corners[0] = tempx-hyp->cars[j].width.x;
	  corners[1] = tempy-hyp->cars[j].width.y;
	  /*
	  corners[4] = tempx + hyp->cars[j].Smu.getelem(1,0)*cos(hyp->cars[j].Smu.getelem(2,0));
	  corners[5] = tempy+hyp->cars[j].Smu.getelem(1,0)*sin(hyp->cars[j].Smu.getelem(2,0));
	  corners[2] = tempx + hyp->cars[j].Smu.getelem(1,0)*cos(hyp->cars[j].Smu.getelem(2,0))-hyp->cars[j].Smu.getelem(0,0)*cos(hyp->cars[j].Smu.getelem(2,0)+PI/2);
	  corners[3] = tempy+hyp->cars[j].Smu.getelem(1,0)*sin(hyp->cars[j].Smu.getelem(2,0))-hyp->cars[j].Smu.getelem(0,0)*sin(hyp->cars[j].Smu.getelem(2,0)+1.5707);
	  corners[0] = tempx - hyp->cars[j].Smu.getelem(0,0)*cos(hyp->cars[j].Smu.getelem(2,0)+PI/2);
	  corners[1] = tempy - hyp->cars[j].Smu.getelem(0,0)*sin(hyp->cars[j].Smu.getelem(2,0)+PI/2);*/
	  
	  break;
	  
	case FRONT_RIGHT:
	  // need to reverse the length terms
	  corners[2] = tempx;
	  corners[3] = tempy;
	  corners[0] = tempx-hyp->cars[j].length.x;
	    corners[1] = tempy-hyp->cars[j].length.y;
	    corners[6] = tempx-hyp->cars[j].length.x+hyp->cars[j].width.x;
	    corners[7]=tempy-hyp->cars[j].length.y+hyp->cars[j].width.y;
	    corners[4] = tempx+hyp->cars[j].width.x;
	    corners[5] = tempy+hyp->cars[j].width.y;
	    /*
	  corners[0] = tempx - hyp->cars[j].Smu.getelem(1,0)*cos(hyp->cars[j].Smu.getelem(2,0));
	  corners[1] = tempy - hyp->cars[j].Smu.getelem(1,0)*sin(hyp->cars[j].Smu.getelem(2,0));
	  corners[6] = tempx-hyp->cars[j].Smu.getelem(1,0)*cos(hyp->cars[j].Smu.getelem(2,0))+hyp->cars[j].Smu.getelem(0,0)*cos(hyp->cars[j].Smu.getelem(2,0)+PI/2);
	  corners[7] = tempy - hyp->cars[j].Smu.getelem(1,0)*sin(hyp->cars[j].Smu.getelem(2,0))+hyp->cars[j].Smu.getelem(0,0)*sin(hyp->cars[j].Smu.getelem(2,0)+1.5707);
	  corners[4] = tempx + hyp->cars[j].Smu.getelem(0,0)*cos(hyp->cars[j].Smu.getelem(2,0)+PI/2);
	  corners[5] = tempy + hyp->cars[j].Smu.getelem(0,0)*sin(hyp->cars[j].Smu.getelem(2,0)+PI/2);*/
	    
	  break;
	  
	case FRONT_LEFT:
	  // need to reverse all d terms
	  corners[4] = tempx;
	  corners[5] = tempy;
	  corners[6] = tempx-hyp->cars[j].length.x;
	  corners[7] = tempy-hyp->cars[j].length.y;
	  corners[0] = tempx-hyp->cars[j].length.x-hyp->cars[j].width.x;
	  corners[1]=tempy-hyp->cars[j].length.y-hyp->cars[j].width.y;
	  corners[2] = tempx-hyp->cars[j].width.x;
	  corners[3] = tempy-hyp->cars[j].width.y;
		/*
	  corners[6] = tempx - hyp->cars[j].Smu.getelem(1,0)*cos(hyp->cars[j].Smu.getelem(2,0));
	  corners[7] = tempy - hyp->cars[j].Smu.getelem(1,0)*sin(hyp->cars[j].Smu.getelem(2,0));
	  corners[0] = tempx - hyp->cars[j].Smu.getelem(1,0)*cos(hyp->cars[j].Smu.getelem(2,0))-hyp->cars[j].Smu.getelem(0,0)*cos(hyp->cars[j].Smu.getelem(2,0)+PI/2);
	  corners[1] = tempy - hyp->cars[j].Smu.getelem(1,0)*sin(hyp->cars[j].Smu.getelem(2,0))-hyp->cars[j].Smu.getelem(0,0)*sin(hyp->cars[j].Smu.getelem(2,0)+1.5707);
	  corners[2] = tempx - hyp->cars[j].Smu.getelem(0,0)*cos(hyp->cars[j].Smu.getelem(2,0)+PI/2);
	  corners[3] = tempy - hyp->cars[j].Smu.getelem(0,0)*sin(hyp->cars[j].Smu.getelem(2,0)+PI/2);*/
	  
	  break;
	  
	default:
	  ERROR("shouldn't ever reach this point");
	}
	/*	
	corners[0] = hyp->cars[j].corners[BACK_RIGHT].x;
	corners[1] = hyp->cars[j].corners[BACK_RIGHT].y;
	corners[2] = hyp->cars[j].corners[FRONT_RIGHT].x;
	corners[3] = hyp->cars[j].corners[FRONT_RIGHT].y;
	corners[4] = hyp->cars[j].corners[FRONT_LEFT].x;
	corners[5] = hyp->cars[j].corners[FRONT_LEFT].y;
	corners[6] = hyp->cars[j].corners[BACK_LEFT].x;
	corners[7] = hyp->cars[j].corners[BACK_LEFT].y;
*/	

	tmppt = point2(corners[0], corners[1]);
	objectPoints.push_back(tmppt);
	tmppt = point2(corners[2], corners[3]);
	objectPoints.push_back(tmppt);
	tmppt = point2(corners[4], corners[5]);
	objectPoints.push_back(tmppt);
	tmppt = point2(corners[6], corners[7]);
	objectPoints.push_back(tmppt);
      }
      //set the originating module and blob ID numbers
      
      double xSigma = hyp->cars[j].Xsigma.getelem(0,0);
      double ySigma = hyp->cars[j].Ysigma.getelem(0,0);
      for(int w = 0; w < objectPoints.size(); w++){
	if(fabs(xSigma)>fabs(ySigma)){
	  objectPoints.arr.at(w).max_var = fabs(xSigma);
	  objectPoints.arr.at(w).min_var = fabs(ySigma);
	  objectPoints.arr.at(w).axis = 0;
	}else{
	  objectPoints.arr.at(w).max_var = fabs(ySigma);
	  objectPoints.arr.at(w).min_var = fabs(xSigma);
	  objectPoints.arr.at(w).axis = PI/2;
	}
      }
      
      el.setId(mapCarID,hyp->cars[j].mapID);
      el.setGeometry(objectPoints);
      el.setTypeVehicle();
      el.height=2;
      el.plotColor = MAP_COLOR_GREY;

      if(hyp->cars[j].isStopped) {
	DGCgettime(currTime);
	el.timeStopped = (currTime - hyp->cars[j].lastTimeMoving)/100000;
      } else {
	el.timeStopped = 0.0;
      }

      //TODO: this is totally arbitrary, just wanting to give some idea to the map
      el.conf = min(1.0, hyp->cars[j].timesSeen/100);

      //Xsigma is the KF variances for X position. the 1,1 element is the velocity's squared variance
      //TODO: check units
      double velocityUncertainty = sqrt(sqrt(hyp->cars[j].Xsigma.getelem(1,1)) + sqrt(hyp->cars[j].Ysigma.getelem(1,1)));
      el.velocity = point2_uncertain(hyp->cars[j].Xmu.getelem(1,0), hyp->cars[j].Ymu.getelem(1,0), velocityUncertainty);

      //TODO: Add state (something about map centering itself?)
      el.state = rawState;
    
      bytesSent = sendMapElement(&el, outputSubgroup);
    } //end checking for recent data
  }// end cycling through cars



  return;
}


/**
 * This function assumes that the scan has been segmented,
 * and classifies the new objects into:
 *   already-seen object
 *   already-seen car
 *   new object
 * Then, it calss the appropriate functions to update the representation
 * assumes synthetic data
 * Performs classification once for each hypothesis
 *
 * Largely copied from classifyObjects, written by Laura Lindzey
 **/
void LadarCarPerceptor::MHTclassifyObjects(double dt){
  hypothesis* hypArray = new hypothesis[MAXNUMHYPOTHESES*MAXNUMHYPOTHESES];
  int hypIndex = 0;
  for(int w = 0; w < numHypotheses; w++){
    // iterate through all hypotheses
    hypothesis *hyp = &(hypotheses[w]);
    
    for(int i = 0; i < MAXNUMCARS; i++){
      hyp->carsToUpdate[i].clear();
    }
    for(int i = 0; i < MAXNUMOBJS; i++){
      hyp->objsToUpdate[i].clear();
    }

    double distTraveled;
    int inside1, inside2;
    // whether this segment has already been assigned
    int taken[numSegments];
    for(int i = 0; i < numNewObjects; i++){
      taken[i] = 0;
    }

    double oldX, oldY;
    double newX, newY;

    /**
     * First, we cycle through all previously seen objects to see if the
     * new object matches one of them
     **/
    for(unsigned int i = 0; i < hyp->usedObjs.size(); i++){
      int k = hyp->usedObjs.at(i);
#ifdef USE_PREDICTION_FOR_OBJ_ASSOCIATION
      ellipse predEllipse = hyp->objects[k].el;
      predEllipse.x += hyp->objects[k].Xmu_hat.getelem(0,0)-hyp->objects[k].Xmu.getelem(0,0);
      predEllipse.y += hyp->objects[k].Ymu_hat.getelem(0,0)-hyp->objects[k].Ymu.getelem(0,0);
#endif
      oldX = hyp->objects[k].el.center.x;
      oldY = hyp->objects[k].el.center.y;

      for(int j = 0; j < numNewObjects; j++){
	if(taken[j] == 0){
	  newX = newObjects[j].el.center.x;
	  newY = newObjects[j].el.center.y;
	  

	  //Check both ways, in case we're matching a fragment to a 
	  // larger object
#ifdef USE_PREDICTION_FOR_OBJ_ASSOCIATION
	  inside1 = predEllipse.inside(newObjects[j].el.center, MARGIN);
	  insde2 = newObjects[j].el.inside(predEllipse.center, MARGIN);
#else
	  inside1 = hyp->objects[k].el.inside(newObjects[j].el.center, MARGIN);
	  inside2 = newObjects[j].el.inside(hyp->objects[k].el.center, MARGIN);
#endif
	  if((1 == inside1) || (1 == inside2)){
	    taken[j] = 1;
	    hyp->objsToUpdate[k].push_back(j);
	  }//end checking if they match
	}//end checking if newObj is taken
      }//end cycling through new objects
    }// end cycling through old objects
    /**
     * Next, we cycle through all previously seen cars to see if obj matches.
     * this goes AFTER matching newobjs to oldobjs because we don't want to
     * match an already seen lamppost in the car's path to the car...
     **/
    // Do I actually want to do this, or leave it to the multihypothesis system?
    point2 old_r1, old_r2, old_corner;
    point2 new_r1, new_r2, new_corner;
    double diag;
    line2 side1, side2, side3, side4;
    point2 br, bl, fr, fl;
    point2arr boundingbox;
    point2 newPt;
    double tempX, tempy;

    for(unsigned int i = 0; i < hyp->usedCars.size(); i++){

      int k = hyp->usedCars.at(i);
#ifdef USE_PREDICTION_FOR_ASSOCIATION
      tempX = hyp->cars[k].Xmu_hat.getelem(0,0);
      tempY = hyp->cars[k].Ymu_hat.getelem(0,0);

      switch(hyp->cars[k].trackedCorner){
      case BACK_RIGHT:
	oldX = tempX;
	oldY = tempY;
	break;

      case BACK_LEFT:
	oldX = tempX - hyp->casr[k].width.x;
	oldY = tempY - hyp->cars[k].width.y;
	break;

      case FRONT_RIGHT:
	oldX = tempX - hyp->cars[k].length.x;
	oldY = tempY - hyp->cars[k].length.y;
	break;

      case FRONT_LEFT:
	oldX = tempX - hyp->cars[k].length.x - hyp->cars[k].width.x;
	oldY = tempY - hyp->cars[k].length.y - hyp->cars[k].width.y;
	break;

      default:
	ERROR("");
      }
#else
      oldX = hyp->cars[k].corners[BACK_RIGHT].x;
      oldY = hyp->cars[k].corners[BACK_RIGHT].y;
#endif
      //generate box extending by CARMARGIN on all sides of observed car
      old_r1 = hyp->cars[k].length;
      old_r2 = hyp->cars[k].width;
      old_corner = point2(oldX, oldY);
      diag = atan2(old_r1.y+old_r1.y, old_r1.x+old_r2.x);
      new_corner = old_corner - CARMARGIN*point2(cos(diag),sin(diag));
      new_r1 = ((2*CARMARGIN + old_r1.norm())/old_r1.norm())*old_r1;
      new_r2 = ((2*CARMARGIN + old_r2.norm())/old_r2.norm())*old_r2;

      boundingbox.clear();
      boundingbox.push_back(new_corner);
      boundingbox.push_back(new_corner+new_r1);
      boundingbox.push_back(new_corner+new_r1+new_r2);
      boundingbox.push_back(new_corner+new_r2);

      for(int j = 0; j < numNewObjects; j++){
	if(taken[i] == 0){ // this obj has not already been claimed
	  int isinside = boundingbox.is_inside_poly(newObjects[j].el.center);

	  // check if this obj matches the car
	  if(1 == isinside){
	    // the obj and car should be associated
	    taken[j] = 1;
	    hyp->carsToUpdate[k].push_back(j);
	  }// end of checkinf if newObj and car match
	} // end oc checking if obj already claimed
      } // end cycling through newObjs
    }// end cycling thru numCars

    // update the absolutely assigned objects
    //cycle through and update each ab assigned object once
    unsigned int arrlength;
    int j;
    unsigned int l;
    double dStart, dEnd;
    int startIndex;
    vector<short> sources;
    startIndex = numSynthScanPoints;
    int endIndex = -1;
    
    point2arr newpts;
    point2arr newdepthpts;
    for(int i = 0; i < MAXNUMOBJS; i++){
      arrlength = hyp->objsToUpdate[i].size();
      if(0 < arrlength){
	// assigned an object to this
	newpts.clear();
	newdepthpts.clear();
	sources.clear();
	for(j = arrlength - 1; j >= 0; j--){
	  int k = objsToUpdate[i].at(j);
	  if(endIndex < newObjects[k].endIndex){
	    endIndex = newObjects[k].endIndex;
	    dEnd = newObjects[k].dEnd;
	  }
	  if(startIndex > newObjects[k].startIndex){
	    startIndex = newObjects[k].endIndex;
	    dStart = newObjects[k].dStart;
	  }
	  // get all points
	  point2arr tmppts;
	  tmppts = point2arr(newObjects[k].lastScan);
	  for(l = 0; l < tmppts.size(); l++){
	    newpts.push_back(tmppts.arr[l]);
	  }
	  if(sortBySensor){
	    for(int l = 0; l < newObjects[k].source.size(); l++){
	      sources.push_back(newObjects[k].source.at(l));
	    }
	  }
	  tmppts.clear();
	  tmppts = point2arr(newObjects[k].lastScanDepth);
	  for(l = 0; l < tmppts.size(); l++){
	    newdepthpts.push_back(tmppts.arr[l]);
	  }
	}
	// update the the object
	if(sortBySensor){
	  updateObject(&(hyp->objects[i]),newpts,newdepthpts,sources,dStart,dEnd);
	}else{
	  updateObject(&(hyp->objects[i]),newpts,newdepthpts,dStart, dEnd);
	}
      }
    }
    // update the cars
    objectRepresentation newObject;
    carRepresentation newCar;
    for(int i = 0; i < MAXNUMCARS; i++){
      //reset vars for each new car
      point2arr newdepthpts;
      newpts.clear();
      sources.clear();
      startIndex = numSynthScanPoints;
      endIndex = -1;
      
      arrlength = carsToUpdate[i].size();
      if(0 < arrlength){
	for(j = arrlength - 1; j >= 0; j--){
	  int k = carsToUpdate[i].at(j);
	  if(endIndex < newObjects[k].endIndex){
	    endIndex = newObjects[k].endIndex;
	    dEnd = newObjects[k].dEnd;
	  }
	  if(startIndex > newObjects[k].startIndex){
	    startIndex = newObjects[k].endIndex;
	    dStart = newObjects[k].dStart;
	  }

	  // get all points
	  point2arr tmppts, tmpdepthpts;
	  tmppts = point2arr(newObjects[k].lastScan);
	  tmpdepthpts = point2arr(newObjects[k].lastScanDepth);
	  point2 center = tmppts.average();
	  //get the source sensor for each point
	  if(sortBySensor){
	    for(int l = 0; l < newObjects[k].source.size(); l++){
	      sources.push_back(newObjects[k].source.at(l));
	    }
	  }
	  for(l = 0; l < tmppts.size(); l++){
	    newpts.push_back(tmppts.arr[l]);
	  }
	  for(l = 0; l < tmpdepthpts.size(); l++){
	    newdepthpts.push_back(tmpdepthpts.arr[l]);
	  }
	}
	// now update the car
	if(sortBySensor){
	  updateCar(&(hyp->cars[i]), newpts, newdepthpts, sources, dStart, dEnd);
	}else{
	  updateCar(&(hyp->cars[i]),newpts, newdepthpts, dStart, dEnd);
	}
      }
    }

    /**
     * Finally, we take car of the new objects that didn't immediately match
     * anything we've seen thus far.  This is where the multi-hypothesis
     * tracking takes place, as the assumption is that if objects lie
     * on top of each other, they are very likely to be the same thing so
     * there is no need to increase the cost of MHT by including them.
     **/
    //create vector of all untaken measurements
    vector<int> MHTmeasure;
    for(int i = 0; i < numNewObjects; i++){
      if(taken[i] == 0){
	MHTmeasure.push_back(i);
      }
    }
    vector<int> MHTobj;
    for(int i = 0; i < hyp->usedObjs.size(); i++){
      if(hyp->objsToUpdate[hyp->usedObjs[i]].size() == 0){
	MHTobj.push_back(i);
      }
    }
    vector<int> MHTcar;
    for(int i = 0; i < hyp->usedCars.size(); i++){
      if(hyp->carsToUpdate[hyp->usedObjs[i]].size() == 0){
	MHTcar.push_back(i);
      }
    }
    int dim = MHTmeasure.size()+MHTobj.size()+MHTcar.size();
    double *cost = createCostMatrix(MHTmeasure, MHTobj, objects, MHTcar, cars, dt);
    // don't want more hypotheses than objects to match against
    int numHyp = min(MAXNUMHYPOTHESES,max(MHTobj.size()+MHTcar.size(),1));
    vector<murtyProb> *solutions = murty(cost, dim, numHyp);
    int numObjs = MHTobj.size();
    int numCars = MHTcar.size();
    int n = numObjs+numCars;
    int m = MHTmeasure.size();
    for(int i = 0; i < solutions->size(); i++){
      hypothesis tempHyp = *hyp; // copies over the contents of the current hypothesis
      murtyProb sol = solutions->at(i);
      // update objects assigned to a measurement by MHT
      for(int j = 0; j < numObjs; j++){
	startIndex = numSynthScanPoints;
	endIndex = -1;
	if (sol.rowsol[j] < m){
	  // assignend to a measure, so perform update on this object
	  newpts.clear();
	  newdepthpts.clear();
	  sources.clear();
	  int k = MHTmeasure.at(sol.rowsol[j]);
	  if(endIndex < newObjects[k].endIndex){
	    endIndex = newObjects[k].endIndex;
	    dEnd = newObjects[k].dEnd;
	  }
	  if(startIndex > newObjects[k].startIndex){
	    startIndex = newObjects[k].endIndex;
	    dStart = newObjects[k].dStart;
	  }
	  // get all points
	  point2arr tmppts;
	  tmppts = point2arr(newObjects[k].lastScan);
	  for(l = 0; l < tmppts.size(); l++){
	    newpts.push_back(tmppts.arr[l]);
	  }
	  if(sortBySensor){
	    for(int l = 0; l < newObjects[k].source.size(); l++){
	      sources.push_back(newObjects[k].source.at(l));
	    }
	  }
	  tmppts.clear();
	  tmppts = point2arr(newObjects[k].lastScanDepth);
	  for(l = 0; l < tmppts.size(); l++){
	    newdepthpts.push_back(tmppts.arr[l]);
	  }
	  // update the the object
	  if(sortBySensor){
	    updateObject(&(tempHyp.objects[MHTobj.at(j)]),newpts,newdepthpts,sources,dStart,dEnd);
	  }else{
	    updateObject(&(tempHyp.objects[MHTobj.at(j)]),newpts,newdepthpts,dStart, dEnd);
	  }
	}
      }	  
      // update cars assigned to a measurement by MHT
      for(int j = numObjs; j < n; j++){
	if(sol.rowsol[j] < m){
	  // assigned to a measure, so perform update on this car
	  int k = MHTmeasure.at(sol.rowsol[j]);
	  
	  //reset vars for each new car
	  point2arr newdepthpts;
	  newpts.clear();
	  sources.clear();
	  startIndex = numSynthScanPoints;
	  endIndex = -1;
	  
	  if(endIndex < newObjects[k].endIndex){
	    endIndex = newObjects[k].endIndex;
	    dEnd = newObjects[k].dEnd;
	  }
	  if(startIndex > newObjects[k].startIndex){
	    startIndex = newObjects[k].endIndex;
	    dStart = newObjects[k].dStart;
	  }
	  
	  // get all points
	  point2arr tmppts, tmpdepthpts;
	  tmppts = point2arr(newObjects[k].lastScan);
	  tmpdepthpts = point2arr(newObjects[k].lastScanDepth);
	  point2 center = tmppts.average();
	  //get the source sensor for each point
	  if(sortBySensor){
	    for(int l = 0; l < newObjects[k].source.size(); l++){
	      sources.push_back(newObjects[k].source.at(l));
	    }
	  }
	  for(l = 0; l < tmppts.size(); l++){
	    newpts.push_back(tmppts.arr[l]);
	  }
	  for(l = 0; l < tmpdepthpts.size(); l++){
	    newdepthpts.push_back(tmpdepthpts.arr[l]);
	  }
	  // now update the car
	  if(sortBySensor){
	    updateCar(&(tempHyp.cars[MHTcar[j-numObjs]]), newpts, newdepthpts, sources, dStart, dEnd);
	  }else{
	    updateCar(&(tempHyp.cars[MHTcar.at(j-numObjs)]),newpts, newdepthpts, dStart, dEnd);
	  }
	}
      }
      // Finally, look for new tracks and add them to the temporary hypothesis
      for(int j = 0; j < m; j++){
	if(sol.rowsol[j+n] < m){
	  // the measurement is assigned to a new track, so add a new track to
	  // the temporary hypothesis
	  int k = MHTmeasure.at(j);
	  MHTaddObject(tempHyp, &(newObjects[k]));
	}
      }
     tempHyp.cost = sol.cost;
      // get the probabillity of the hypothesis by converting the cost
      // back into a probabillity, and multiplying by the parent hypothesis
      // probability
      tempHyp.prob = hyp->prob*exp(-1.0*tempHyp.cost);
      // finally, add this hypothesis to the que of possible hypotheses
      //      newHypotheses.push_back(tempHyp);
      hypArray[hypIndex] = tempHyp;
      hypIndex++;
    }
  }
  vector<hypothesis> newHypotheses; // stores the new hypotheses
  for(int z = 0; z < hypIndex; z++){
    newHypotheses.push_back(hypArray[z]);
  }
  // select the new hypotheses to use
  // NOTE: I reversed the sense of the < operator for hypothesis so that this
  // will sort in descending order of probabillity
  sort(newHypotheses.begin(),newHypotheses.end());
  numHypotheses = min(MAXNUMHYPOTHESES, newHypotheses.size());
  double normProb = 0;
  for(int i = 0; i < numHypotheses; i++){
    hypotheses[i] = newHypotheses[i];
    normProb += hypotheses[i].prob;
  }
  // normalize the probabillities to keep the numbers reasonable
  for(int i = 0; i < numHypotheses; i++){
    hypotheses[i].prob /= normProb;
  }
  delete[] hypArray;
}

