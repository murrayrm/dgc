/**
 * File: LadarCarPerceptor.cc
 * Description:
 *   Uses bumper ladar to track objects.
 *   This file has the functions handling all
 *   LADAR interfaces, as well as initializing
 *   everything and running the main loop
 * Based on the Obsperceptor template by Andrew
 * Last Changed: March 9
 **/

#include "LadarCarPerceptor.hh"

int llama = 0;
ofstream perpdebugfile;
// Default constructor
LadarCarPerceptor::LadarCarPerceptor()
{
  memset(this, 0, sizeof(*this));
  useSynth = false;
  useRect = false;
  sortBySensor = false;
  for(int i = 0; i < NUMUSEDLADARS; i++){
    scanUpdated[i] = false;
  }
  perpdebugfile.open("perpdebug.log");
  return;
}


// Default destructor
LadarCarPerceptor::~LadarCarPerceptor()
{
  fclose(logfile);
}


// Parse the command line
int LadarCarPerceptor::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;

  if(this->options.debug_subgroup_given)
    debugSubgroup = this->options.debug_subgroup_arg;
  else 
    debugSubgroup = -3;

  if(this->options.output_subgroup_given)
    outputSubgroup = this->options.output_subgroup_arg;
  else
    outputSubgroup = 0;

  if(this->options.output_rate_given)
    outputRate = this->options.output_rate_arg;
  else
    outputRate = 100000;
  if(this->options.use_synth_flag)
    useSynth = true;
  if(this->options.use_rect_flag)
    useRect = true;
  if(this->options.filter_by_sensor_flag){
    useSynth = true;
    sortBySensor = true;
  }
  if(this->options.car_time_thresh_given){
    carTimeThresh = this->options.car_time_thresh_arg;
  }else{
    carTimeThresh = 15;
  }
  if(this->options.car_vel_thresh_given){
    carVelThresh = this->options.car_vel_thresh_arg;
  }else{
    carVelThresh = 0;
  }
  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);

  
  if(this->options.only_left_given) {
    onlyLeft = 1;
    this->moduleId = MODladarCarPerceptorLeft;
  }
  else
    onlyLeft = 0;

  if(this->options.only_roof_left_given) {
    onlyRoofLeft = 1;
    this->moduleId = MODladarCarPerceptorRoofLeft;
  }
  else
    onlyRoofLeft = 0;

  if(this->options.only_right_given) {
    onlyRight = 1;
    this->moduleId = MODladarCarPerceptorRight;
  }
  else
    onlyRight = 0;


  if(this->options.only_roof_right_given) {
    onlyRoofRight = 1;
    this->moduleId = MODladarCarPerceptorRoofRight;
  }
  else
    onlyRoofRight = 0;


  if(this->options.only_riegl_given) {
    onlyRiegl = 1;
    this->moduleId = MODladarCarPerceptorRiegl;
  }
  else
    onlyRiegl = 0;


  if(this->options.only_center_given) {
    onlyCenter = 1;
    this->moduleId = MODladarCarPerceptorCenter;
  }
  else
    onlyCenter = 0;

  if(this->options.only_rear_given) {
    onlyRear = 1;
    this->moduleId = MODladarCarPerceptorRear;
  }
  else
    onlyRear = 0;

  mapCarID = this->moduleId;

  string logfilename = this->options.log_name_arg;
  logfile = fopen(this->options.log_name_arg,"w");
  if(logfile==NULL) {
    fprintf(stderr, "logfile open failed \n");
  } else {
    fprintf(logfile, "testing! \n");
  }

  if(this->options.object_depth_given) {
    objectDepth = this->options.object_depth_arg;
  } else {
    objectDepth = 0.0;
  }


  // If I'm only sending cars, or sending debug info
  //as well to the mapviewer
  if (this->options.use_debug_display_flag)
    useDisplay = 1;
  else
    useDisplay = 0;

  if (this->options.use_map_flag)
    useMap = 1;
  else
    useMap = 0;

  if (this->options.use_map_display_flag)
    useMapDisplay = 1;
  else
    useMapDisplay = 0;

  if(this->options.enable_logging_flag)
    useLogging = 1;
  else
    useLogging = 0;
  

  return 0;
}



// Initialize sensnet
int LadarCarPerceptor::initSensnet()
{
  int i;

  sensnet_id_t sensorIds[16];
  Ladar *ladar;
    
  // Create sensnet interface
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return -1;

  // Default ladar set
  numSensorIds = 0;
  if(this->onlyLeft) {
    sensorIds[numSensorIds++] = SENSNET_LF_BUMPER_LADAR;
  } else if (this->onlyCenter) {
    sensorIds[numSensorIds++] = SENSNET_MF_BUMPER_LADAR;
  } else if (this->onlyRight) {
    sensorIds[numSensorIds++] = SENSNET_RF_BUMPER_LADAR;
  } else if (this->onlyRear) {
    sensorIds[numSensorIds++] = SENSNET_REAR_BUMPER_LADAR;
  } else if (this->onlyRoofRight) {
    sensorIds[numSensorIds++] = SENSNET_RF_ROOF_LADAR;
  } else if (this->onlyRoofLeft) {
    sensorIds[numSensorIds++] = SENSNET_LF_ROOF_LADAR;
  } else if (this->onlyRiegl) {
    sensorIds[numSensorIds++] = SENSNET_RIEGL;
  } else {
    sensorIds[numSensorIds++] = SENSNET_LF_BUMPER_LADAR;
    sensorIds[numSensorIds++] = SENSNET_MF_BUMPER_LADAR;
    sensorIds[numSensorIds++] = SENSNET_RF_BUMPER_LADAR;
    sensorIds[numSensorIds++] = SENSNET_REAR_BUMPER_LADAR;
    sensorIds[numSensorIds++] = SENSNET_RF_ROOF_LADAR;
    if(!useSynth){
      sensorIds[numSensorIds++] = SENSNET_LF_ROOF_LADAR;
      sensorIds[numSensorIds++] = SENSNET_RIEGL;
    }
  }


  // Initialize ladar list
  for (i = 0; i < numSensorIds; i++)
    {
      assert((size_t) this->numLadars < sizeof(this->ladars) / sizeof(this->ladars[0]));
      ladar = this->ladars + this->numLadars++;

      // Initialize ladar data
      ladar->sensorId = sensorIds[i];

      // Join the ladar data group
      if (this->sensnet)
        {
          if (sensnet_join(this->sensnet, ladar->sensorId,
                           SENSNET_LADAR_BLOB, sizeof(LadarRangeBlob)) != 0)
            return ERROR("unable to join %d", ladar->sensorId);
        }
    }
  
  // Subscribe to process control messages
  if(sensnet_join(this->sensnet, this->moduleId, SNprocessRequest, sizeof(ProcessRequest)) != 0)
    return ERROR("unable to join process group");
  //  perpdebugfile << numLadars << endl;
  return 0;
}


// Clean up sensnet
int LadarCarPerceptor::finiSensnet()
{
  if (this->sensnet)
    {
      int i;
      Ladar *ladar;
      for (i = 0; i < this->numLadars; i++)
        {
          ladar = this->ladars + i;
          sensnet_leave(this->sensnet, ladar->sensorId, SENSNET_LADAR_BLOB);
        }
      sensnet_free(this->sensnet);
      this->sensnet = NULL;
    }
  else if (this->replay)
    {
      sensnet_replay_close(this->replay);
      sensnet_replay_free(this->replay);
      this->replay = NULL;
    }


  return 0;
}


// Update the map with new range data
int LadarCarPerceptor::update()
{
  //  fprintf(stderr, "entering update() \n");

  int i;
  Ladar *ladar;
  int blobId, blobLen;
  LadarRangeBlob blob;
  bool needNew = true;

  // In live mode wait for new data, but timeout if
  // we dont get anything new for a while.
  if (sensnet_wait(this->sensnet, 200) != 0)
    return 0;

  for (i = 0; i < this->numLadars; i++)
    {
      ladar = this->ladars + i;
      currSensor = i;

      if (this->sensnet)
	{
	  // Check the latest blob id
	  if (sensnet_peek(this->sensnet, ladar->sensorId,
			   SENSNET_LADAR_BLOB, &blobId, &blobLen) != 0)
	    continue;

	  // Is this a new blob?
	  if (blobId == ladar->blobId)
	    continue;

	  // If this is a new blob, read it    
	  if (sensnet_read(this->sensnet, ladar->sensorId,
			   SENSNET_LADAR_BLOB, &ladar->blobId, sizeof(blob), &blob) != 0)
	    break;
	}
      else if (this->replay)
	{
	  // Advance log one step
	  if (needNew){
	    if (sensnet_replay_next(this->replay, 0) != 0)
	      return -1;
	  }
 
	  // Read new blob
	  if (sensnet_replay_read(this->replay, ladar->sensorId,
				  SENSNET_LADAR_BLOB, &ladar->blobId, sizeof(blob), &blob) != 0){
	    break;
	    needNew = false;
	  }
	}
      // Note that this ladar has been updated
      scanUpdated[i] = true;
      //transformed to sensor frame
      float sfx, sfy, sfz; //sensor frame vars
      LadarRangeBlobScanToSensor(&blob, blob.points[45][0], blob.points[45][1],&sfx, &sfy, &sfz);

      //transformed to vehicle frame
      float vfx, vfy, vfz; //vehicle frame vars
      LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);

      //transformed to local frame
      float lfx, lfy, lfz; //local frame vars
      LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);

      for(int j=0; j < NUMSCANPOINTS; j++) 
        {
	  if(sortBySensor){
	    // Record the generating ladar scan
	    if((ladar->sensorId == SENSNET_MF_BUMPER_LADAR) ||
	       (ladar->sensorId == SENSNET_RF_BUMPER_LADAR) ||
	       (ladar->sensorId == SENSNET_LF_BUMPER_LADAR) ||
	       (ladar->sensorId == SENSNET_REAR_BUMPER_LADAR)){
	      pointSourceSensor[j+i*NUMSCANPOINTS] = BUMPER;
	    }else if((ladar->sensorId == SENSNET_RF_ROOF_LADAR) ||
		     (ladar->sensorId == SENSNET_LF_ROOF_LADAR)){
	      pointSourceSensor[j+i*NUMSCANPOINTS] = ROOF;
	    }else{
	      pointSourceSensor[j+i*NUMSCANPOINTS] = STEREO;
	    }
	  }
          rawData[j][ANGLE] = blob.points[j][ANGLE];
          rawData[j][RANGE] = blob.points[j][RANGE];
          LadarRangeBlobScanToSensor(&blob, blob.points[j][ANGLE], blob.points[j][RANGE],&sfx, &sfy, &sfz);
          LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
	  // Use the vehicle frame to generate the raw data as seen from a
	  // theoretical ladar unit at a positioin vertically aligned with
	  // the MF_BUMPER_LADAR
	  double synthx = .99141*vfx-0.03*vfy+0.007*vfz-4.60831;
	  double synthy = -0.9988*vfy-0.041*vfx+0.02347*vfz+.213048;
	  double synthz = vfz; // Want to keep elevation info in original value
	  rawSynth[j+i*NUMSCANPOINTS][ANGLE]=atan2(synthy,synthx);//atan2(synthy,synthx);
	  float tempx;
	  float tempy;
	  float tempz;
	  LadarRangeBlobVehicleToLocal(&blob, 4.60831,-.213048,.326,&(tempx),&(tempy),&(tempz));
	  synthOrig.set(tempx,tempy,tempz);
	  //	  perpdebugfile << i << " "<< j+i*NUMSCANPOINTS << endl;
	  // Need to perform the non-return filtering here to prevent 
	  // ladar housing returns from showing up
	  if(rawData[j][RANGE] < MAXRANGE && rawData[j][RANGE] > MINRANGE){
	    rawSynth[j+i*NUMSCANPOINTS][RANGE]=sqrt(pow(synthx,2)+pow(synthy,2)+pow(synthz,2));
	  }else{
	    rawSynth[j+i*NUMSCANPOINTS][RANGE] = MAXRANGE*2;
	    }
	  //rawSynth[j+i][RANGE]=sqrt(sfx*sfx+sfy*sfy+sfz*sfz);
          LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
          xyzData[j][0] = lfx;
          xyzData[j][1] = lfy;
          xyzData[j][2] = lfz;
	  // Copy the data into the local frame array for the synthetic ladar
	  xyzSynth[j+i*NUMSCANPOINTS][0] = lfx;
	  xyzSynth[j+i*NUMSCANPOINTS][1] = lfy;
	  xyzSynth[j+i*NUMSCANPOINTS][2] = lfz;
        }

      for(int j=0; j < NUMSCANPOINTS; j++) 
        {
          LadarRangeBlobScanToSensor(&blob, blob.points[j][ANGLE], blob.points[j][RANGE] + objectDepth, &sfx, &sfy, &sfz);
          LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
          LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
          depthData[j][0] = lfx;
          depthData[j][1] = lfy;
          depthData[j][2] = lfz;
	  depthSynth[j+i*NUMSCANPOINTS][0] = lfx;
	  depthSynth[j+i*NUMSCANPOINTS][1] = lfy;
	  depthSynth[j+i*NUMSCANPOINTS][2] = lfz;
        }
      double da = PI/360;
      
      //for plotting the segmentation
      LadarRangeBlobScanToSensor(&blob, 0.0, 0.0, &sfx, &sfy, &sfz);
      LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
      LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
      ladarState.X = lfx;
      ladarState.Y = lfy;
      ladarState.Z = lfz;
      ladarState.Theta = atan2(xyzData[90][1] - lfy, xyzData[90][0] - lfx); 


      for(int j=0; j < NUMSCANPOINTS; j++) 
        {
          LadarRangeBlobScanToSensor(&blob, blob.points[j][ANGLE]-da,MAPVISIBLE, &sfx, &sfy, &sfz);
          LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
          LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
          occupiedData[j][MINANGLE_35][0] = lfx - ladarState.X;
          occupiedData[j][MINANGLE_35][1] = lfy - ladarState.Y;
          occupiedData[j][MINANGLE_35][2] = lfz - ladarState.Z;
	  occupiedSynthData[j+i*NUMSCANPOINTS][MINANGLE_35][0] = lfx - ladarState.X;
	  occupiedSynthData[j+i*NUMSCANPOINTS][MINANGLE_35][1] = lfy - ladarState.Y;
	  occupiedSynthData[j+i*NUMSCANPOINTS][MINANGLE_35][2] = lfz - ladarState.Z;


          LadarRangeBlobScanToSensor(&blob, blob.points[j][ANGLE]+da,MAPVISIBLE, &sfx, &sfy, &sfz);
          LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
          LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
          occupiedData[j][MAXANGLE_35][0] = lfx - ladarState.X;
          occupiedData[j][MAXANGLE_35][1] = lfy - ladarState.Y;
          occupiedData[j][MAXANGLE_35][2] = lfz - ladarState.Z;
	  occupiedSynthData[j+i*NUMSCANPOINTS][MAXANGLE_35][0] = lfx - ladarState.X;
	  occupiedSynthData[j+i*NUMSCANPOINTS][MAXANGLE_35][1] = lfy - ladarState.Y;
	  occupiedSynthData[j+i*NUMSCANPOINTS][MAXANGLE_35][2] = lfz - ladarState.Z;


          LadarRangeBlobScanToSensor(&blob, blob.points[j][ANGLE]-da, blob.points[j][RANGE] - MAPCELLDIMENSION/2, &sfx, &sfy, &sfz);
          LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
          LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
          occupiedData[j][MINANGLE_MINRANGE][0] = lfx - ladarState.X;
          occupiedData[j][MINANGLE_MINRANGE][1] = lfy - ladarState.Y;
          occupiedData[j][MINANGLE_MINRANGE][2] = lfz - ladarState.Z;
	  occupiedSynthData[j+i*NUMSCANPOINTS][MINANGLE_MINRANGE][0] = lfx - ladarState.X;
	  occupiedSynthData[j+i*NUMSCANPOINTS][MINANGLE_MINRANGE][1] = lfy - ladarState.Y;
	  occupiedSynthData[j+i*NUMSCANPOINTS][MINANGLE_MINRANGE][2] = lfz - ladarState.Z;


          LadarRangeBlobScanToSensor(&blob, blob.points[j][ANGLE]-da, blob.points[j][RANGE] + MAPCELLDIMENSION/2, &sfx, &sfy, &sfz);
          LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
          LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
          occupiedData[j][MINANGLE_MAXRANGE][0] = lfx - ladarState.X;
          occupiedData[j][MINANGLE_MAXRANGE][1] = lfy - ladarState.Y;
          occupiedData[j][MINANGLE_MAXRANGE][2] = lfz - ladarState.Z;
	  occupiedSynthData[j+i*NUMSCANPOINTS][MINANGLE_MAXRANGE][0] = lfx - ladarState.X;
	  occupiedSynthData[j+i*NUMSCANPOINTS][MINANGLE_MAXRANGE][1] = lfy - ladarState.Y;
	  occupiedSynthData[j+i*NUMSCANPOINTS][MINANGLE_MAXRANGE][2] = lfz - ladarState.Z;


          LadarRangeBlobScanToSensor(&blob, blob.points[j][ANGLE]+da, blob.points[j][RANGE] - MAPCELLDIMENSION/2, &sfx, &sfy, &sfz);
          LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
          LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
          occupiedData[j][MAXANGLE_MINRANGE][0] = lfx - ladarState.X;
          occupiedData[j][MAXANGLE_MINRANGE][1] = lfy - ladarState.Y;
          occupiedData[j][MAXANGLE_MINRANGE][2] = lfz - ladarState.Z;
	  occupiedSynthData[j+i*NUMSCANPOINTS][MAXANGLE_MINRANGE][0] = lfx - ladarState.X;
	  occupiedSynthData[j+i*NUMSCANPOINTS][MAXANGLE_MINRANGE][1] = lfy - ladarState.Y;
	  occupiedSynthData[j+i*NUMSCANPOINTS][MAXANGLE_MINRANGE][2] = lfz - ladarState.Z;


          LadarRangeBlobScanToSensor(&blob, blob.points[j][ANGLE]+da, blob.points[j][RANGE] + MAPCELLDIMENSION/2, &sfx, &sfy, &sfz);
          LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
          LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
          occupiedData[j][MAXANGLE_MAXRANGE][0] = lfx - ladarState.X;
          occupiedData[j][MAXANGLE_MAXRANGE][1] = lfy - ladarState.Y;
          occupiedData[j][MAXANGLE_MAXRANGE][2] = lfz - ladarState.Z;
	  occupiedSynthData[j+i*NUMSCANPOINTS][MAXANGLE_MAXRANGE][0] = lfx - ladarState.X;
	  occupiedSynthData[j+i*NUMSCANPOINTS][MAXANGLE_MAXRANGE][1] = lfy - ladarState.Y;
	  occupiedSynthData[j+i*NUMSCANPOINTS][MAXANGLE_MAXRANGE][2] = lfz - ladarState.Z;

        }


      currState.X = blob.state.localX;
      currState.Y = blob.state.localY;
      currState.Z = blob.state.localZ;

      // Setting full state for mapper purposes
      rawState = blob.state;

      //call all the tracking functions
      if(!useSynth){
	for(int k = 0; k < NUMSCANPOINTS; k++){
	}
	processScan(blob.sensorId);
      }

      if (this->console)
        {
	  char *whichCorner[4] = {"FrontLeft","FrontRight","BackLeft","BackRight"};
	  char token[64];
          snprintf(token, sizeof(token), "%%ladar%d%%", i);
          cotk_printf(this->console, token, A_NORMAL, "%s %d %8.3f",
                      sensnet_id_to_name(ladar->sensorId),
                      blob.scanId, fmod((double) blob.timestamp * 1e-6, 10000));

	  snprintf(token, sizeof(token), "%%carstracked%%");
	  cotk_printf(this->console, token, A_NORMAL, "%d",
	        usedCars.size());
	  snprintf(token, sizeof(token), "%%objstracked%%");
	  cotk_printf(this->console, token, A_NORMAL, "%d",
		      usedObjs.size());

	  snprintf(token, sizeof(token), "%%timeelapsed%%");
	  cotk_printf(this->console, token, A_NORMAL, "%f",
		      diffTime);
	  
	  int numdispcars = min(6, usedCars.size());
	  for(int m=0; m<numdispcars; m++) {
	    int n = usedCars.at(m);
            snprintf(token, sizeof(token), "%%corner%d%%", m);
            cotk_printf(this->console, token, A_NORMAL, "%s",
			whichCorner[cars[n].trackedCorner]);
	    point2 vel = point2(cars[n].Xmu.getelem(1,0),cars[n].Ymu.getelem(1,0));
            snprintf(token, sizeof(token), "%%velocity%d%%", m);
            cotk_printf(this->console, token, A_NORMAL, "%f",
			vel.norm());

            snprintf(token, sizeof(token), "%%seen%d%%", m);
            cotk_printf(this->console, token, A_NORMAL, "%d",
			cars[n].timesSeen);
	  }

        }
    }
  //end cycling through ladars
  // Check if all ladars have updated yet
  bool procScan = true;
  for(int i = 0; i < NUMUSEDLADARS; i++){
    procScan = procScan && scanUpdated[i];
  }
  if(useSynth && procScan){
    ladarPoint tempPoint;
    // Create the synthetic the synthetic data
    for (int i = 0; i < NUMSCANPOINTS*NUMUSEDLADARS; i++){
      if(rawSynth[i][RANGE] < MAXRANGE && rawSynth[i][RANGE]>MINRANGE){
	// Point is a valid detection
	tempPoint.range = rawSynth[i][RANGE];
	tempPoint.theta = rawSynth[i][ANGLE];
	tempPoint.x = xyzSynth[i][0];
	tempPoint.y = xyzSynth[i][1];
	tempPoint.z = xyzSynth[i][2];
	tempPoint.depthx = depthSynth[i][0];
	tempPoint.depthy = depthSynth[i][1];
	tempPoint.depthz = depthSynth[i][2];
	tempPoint.segNum = -1;
	tempPoint.source = pointSourceSensor[i];
	synthPoints.push_back(tempPoint);
      }
    } // all relavent points are now stored in the vector, so sort it
    sort(synthPoints.begin(),synthPoints.end());
    numSynthScanPoints = (int) synthPoints.size();
    processScan(blob.sensorId);// The ladarID which is this argument doesn't do anything
    synthPoints.clear();
    for(int i = 0 ; i < NUMUSEDLADARS; i++){
      scanUpdated[i] = false;
    }
  }
  needNew = true;
  return 0;
} //end update()


// Template for console
/*
  01234567890123456789012345678901234567890123456789012345678901234567890123456789
*/
static char *consoleTemplate =
"LadarCarPerceptor $Revision$                                               \n"
"                                                                           \n"
"Skynet: %spread%                                                           \n"
"                                                                           \n"
"Ladar[0]: %ladar0%                                                         \n"
"Ladar[1]: %ladar1%                                                         \n"
"Ladar[2]: %ladar2%                                                         \n"
"Ladar[3]: %ladar3%                                                         \n"
"Ladar[4]: %ladar4%                                                         \n"
"Ladar[5]: %ladar5%                                                         \n"
"                                                                           \n"
"Cars Tracked: %carstracked%         Objects Tracked: %objstracked%         \n"
"Elapsed Time: %timeelapsed%                                                \n"
"                                                                           \n"
"          corner       velocity      times seen                            \n"
"Car 0:    %corner0%    %velocity0%   %seen0%                               \n"
"Car 1:    %corner1%    %velocity1%   %seen1%                               \n"
"Car 2:    %corner2%    %velocity2%   %seen2%                               \n"
"Car 3:    %corner3%    %velocity3%   %seen3%                               \n"
"Car 4:    %corner4%    %velocity4%   %seen4%                               \n"
"Car 5:    %corner5%    %velocity5%   %seen5%                               \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%]                                                           \n";


// Initialize console display
int LadarCarPerceptor::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
    
  // Initialize the display
  cotk_open(this->console,NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));

  return 0;
}


// Finalize sparrow display
int LadarCarPerceptor::finiConsole()
{
  // Clean up the CLI
  if (this->console)
    {
      cotk_close(this->console);
      cotk_free(this->console);
      this->console = NULL;
    }

  return 0;
}


// Handle button callbacks
int LadarCarPerceptor::onUserQuit(cotk_t *console, LadarCarPerceptor *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int LadarCarPerceptor::onUserPause(cotk_t *console, LadarCarPerceptor *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}


// Main program thread
int main(int argc, char **argv)
{
  LadarCarPerceptor *percept;
  


  percept = new LadarCarPerceptor();
  assert(percept);

  // Parse command line options
  if (percept->parseCmdLine(argc, argv) != 0)
    return -1;
  
  if (percept->trackerInit() != 0)
    return -1;

  // Initialize sensnet
  if (percept->initSensnet() != 0)
    return -1;

  // Initialize cotk display
  if (!percept->options.disable_console_flag)
    if (percept->initConsole() != 0)
      return -1;


  fprintf(stderr, "entering main thread of LadarCarPerceptor \n");


  percept->loopCount =0;
  unsigned long long loopStart, loopEnd;  
  while (!percept->quit)
    {
      DGCgettime(loopStart);
      int tempCount = percept->loopCount;
      percept->loopCount = tempCount+1;

      if(percept->loopCount % 15 == 0) {
	//	fprintf(stderr, "trying to call update process state: \n");
	percept->quit = percept->updateProcessState();
	//	fprintf(stderr, "update process state returned %d \n", percept->quit);
      }

      // Update the console
      if (percept->console)
        cotk_update(percept->console);

      // If we are paused, dont do anything
      if (percept->pause)
        {
          usleep(0);
          continue;
        }

      // Wait for new data
      //  if (sensnet_step(percept->sensnet, 100) != 0)
      //  {
      //     break;
      //  } else {
      //      fprintf(stderr, "should have new data! \n");
      //  }

      // Update the map
      if (percept->update() != 0)
        {	
          break;
        } else {
          //      fprintf(stderr, "should be updating the map! \n");
        }



      DGCgettime(loopEnd);
      //     fprintf(stderr, "time elapsed in ladar-car-perceptor: %llu \n", loopEnd - loopStart);
    }

  if (percept->console)
    percept->finiConsole();
  percept->finiSensnet();

  MSG("exited cleanly");
  
  return 0;
}




// Update the process state
int LadarCarPerceptor::updateProcessState()
{
  //  fprintf(stderr, "calling update proceess state \n");
  int blobId;
  ProcessRequest request;
  ProcessResponse response;

  // Send heart-beat message
  memset(&response, 0, sizeof(response));  
  response.moduleId = this->moduleId;
  response.timestamp = DGCgettime();
  response.logSize = 0;
  sensnet_write(sensnet, SENSNET_METHOD_CHUNK,
                this->moduleId, SNprocessResponse, 0, sizeof(response), &response);
  
  // Read process request
  if (sensnet_read(this->sensnet, this->moduleId, SNprocessRequest,
                   &blobId, sizeof(request), &request) != 0) {
    fprintf(stderr, "bad read. ln 590. \n");
    return 0;
  }
  if (blobId < 0) {
    //    fprintf(stderr, "negative blob id: %d \n", blobId);
    return 0;
  }

  // If we have request data, override the console values
  //  percept->quit = request.quit;

  if (request.quit) 
    MSG("remote quit request");
  
  return request.quit;
}

