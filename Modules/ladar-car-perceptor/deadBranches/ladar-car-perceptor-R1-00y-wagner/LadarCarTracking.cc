/**
 * \file LadarCarTracking.cc
 * \author Laura Lindzey
 * Description:
 *   Uses bumper ladar to track objects.
 *   This file has the functions handling all
 *   LADAR interfaces, as well as initializing
 *   everything and running the main loop
 * Based on the Obsperceptor template by Andrew
 * Last Changed: July 12
 **/


#include "LadarCarPerceptor.hh"
using namespace std;

int stateupdatedcount = 0;
ofstream debugingfile;

int LadarCarPerceptor::trackerInit()
{
  debugingfile.open("debug.log");
  numFrames = 0;
  numObjects = 0;
  numCars = 0;

  initSendMapElement(skynetKey);

  DGCgettime(lastSentCars);
  DGCgettime(lastSentObjs);
  DGCgettime(lastKFupdate);

  //initializes the stack of open positions in the car/obj arrays
  for(int i=(MAXNUMOBJS-1);i>=0; i--){
    openObjs.push_back(i);
  }
  for(int i=(MAXNUMCARS-1);i>=0; i--){
    openCars.push_back(i);
  }
  for(int i = 499; i >=0; i--) {
    mapIndices.push_back(i);
  }

  //setting up matrics for KF calculations
  //TODO: these could be better tuned
  double dt = .0133; 
  double sa = .05; //std dev of system noise
  double sz = .1; //std dev of measurement noise 
                  //-- should be fxn of range/theta for a ladar

  A.resetSize(2,2);
  double Aelems[] = {1,dt,0,1};
  A.setelems(Aelems);

  C.resetSize(1,2);
  double Celems[] = {1,0};
  C.setelems(Celems);

  I.resetSize(2,2);
  double Ielems[] = {1,0,0,1};
  I.setelems(Ielems);

  R.resetSize(2,2);
  double Relems[] = {0,0,0,sa};
  R.setelems(Relems);

  Q.resetSize(1,1);
  double Qelems[] = {sz};
  Q.setelems(Qelems);
  
  SA.resetSize(3,3);
  double SAelems[] = {1,0,0,0,1,0,0,0,1};
  SA.setelems(SAelems);
  // Needs to be 4x3 so we can get two different measurements of
  // theta
  SC.resetSize(4,3);
  double SCelems[] = {1,0,0,0,1,0,0,0,1,0,0,1};
  SC.setelems(SCelems);

  SI.resetSize(3,3);
  double SIelems[] = {1,0,0,0,1,0,0,0,1};
  SI.setelems(SIelems);

  // Disturbance
  SR.resetSize(3,3);
  double SRelems[] = {.01, 0, 0, 0, .1, 0, 0, 0, .1};
  SR.setelems(SRelems);

  // Noise
  SQ.resetSize(4,4);
  double SQelems[] = {0.1,0,0,0, 0,.1,0,0, 0,0,.2,0, 0,0,0,.1};
  SQ.setelems(SQelems);
  

  //call the map init
  //  MSG("calling map init: useMap = %d \n", useMap);
  if(useMap)
    {
      //      MSG("Initializing map");
      mapInit();
      //      mapTest();

    }

  return 0;
}


void LadarCarPerceptor::processScan(int ladarID)
{
  //  fprintf(stderr, "processScan called for ladar theta %f \n", ladarState.Theta);
  //  fprintf(stderr, "\n");
  numFrames++;

  DGCgettime(origStartTime);

  //
  double dt = (DGCgettime() - lastKFupdate)/1000000.0;
  KFpredict(dt);
  DGCgettime(lastKFupdate);


  //TODO: the KF assumes being called ~every 1/75 seconds. should I add checks
  //      here for timing? (especially in case of multiple ladars running, and one
  //      cutting out?) at the least, the KF dt needs to be changed if we're
  //      running with more than one ladar

  if(useSynth){
    segmentSynthScan();
    createSynthObjects();
  }else{
    segmentScan(); 
    createObjects();
  }
  classifyObjects();

  DGCgettime(startTime);

  if(useMap) 
    {
      //      if(5 < numFrames) 
      if(0 == numFrames % 3) 
        {
          mapUpdate();
        }
           
      DGCgettime(endTime);
      diffTime = 1.0*(endTime - startTime);
      //      fprintf(stderr, "time elapsed for mapupdate = %f \n", diffTime);
    }

  checkMotion();


  DGCgettime(startTime);
  if(outputRate < (startTime - lastSentCars)) {
    sendToMap();
    //useDisplay is whether to send ALL my debug info to the map
    if(useDisplay) {
      //sendDebug();
    }

    DGCgettime(lastSentCars);
  }

  cleanObjects();
  cleanCars();
  
  if(useLogging) {
    writeLogs();
  }

  //now that cars have been updated, update the KF
  KFupdate();

  DGCgettime(endTime);
  diffTime = 1.0*(endTime - origStartTime);
  if(numFrames % 75 == 0) {
    //    fprintf(stderr, "time elapsed for whole process = %f \n \n", diffTime);
  }


  return;
}


/**
 * This function segments the scan, breaking wherever there is 
 * a discontinuity. (GAPDISTANCE is the parameter in the .hh
 * file controlling how large a discontinuity is permissible)
 * The code will skip discontinuities caused by up to two 
 * no-return points, and only reports segments consising of 
 * MINNUMPOINTS or more points.
 **/
void LadarCarPerceptor::segmentScan()
{

  for(int i=0; i<NUMSCANPOINTS-1; i++) {
    diffs[i] = rawData[i+1][RANGE] - rawData[i][RANGE];
  }
  for(int i=0; i<NUMSCANPOINTS-2; i++) {
    diffs2[i] = rawData[i+2][RANGE] - rawData[i][RANGE];
  }
  for(int i=0; i<NUMSCANPOINTS-3; i++) {
    diffs3[i] = rawData[i+3][RANGE] - rawData[i][RANGE];
  }

  //number objects segmented from curr scan
  int tempNum = 0; 
  //size of current object
  int currSize = 1; 

  int tempPos[NUMSCANPOINTS][2];
  int tempSize[NUMSCANPOINTS];

  tempSize[0] = 0;

  double currRange, currRange2, currRange3;
  double distThresh, distThresh2, distThresh3;

  int noReturns = 0;

  //can't check last 2 points - they wouldn't become an obj anyways
  for(int i=0; i<NUMSCANPOINTS-3; i++) {
    if((rawData[i][RANGE] < MINRANGE) || (rawData[i][RANGE] > MAXRANGE))
      noReturns++;
    //threshold scaled based on range
    currRange = min(rawData[i][RANGE],rawData[i+1][RANGE]);
    distThresh = sqrt(max(0,pow(GAPDISTANCE,2) - pow(currRange*SINTHETA,2)));

    //this angle is 2deg, and at such small angles sin(2*th) ~= 2*sin(th)
    currRange2 = min(rawData[i][RANGE],rawData[i+2][RANGE]);
    distThresh2 = sqrt(max(0,pow(GAPDISTANCE,2) - pow(currRange2*2*SINTHETA,2)));

    currRange3 = min(rawData[i][RANGE],rawData[i+3][RANGE]);
    distThresh3 = sqrt(max(0,pow(GAPDISTANCE,2) - pow(currRange3*3*SINTHETA,2)));

    bool thisPtHasReturn, nextPtHasReturn, next2PtHasReturn;
    thisPtHasReturn = (rawData[i][RANGE] < MAXRANGE ) && (rawData[i][RANGE] > MINRANGE);
    nextPtHasReturn = (rawData[i+1][RANGE] < MAXRANGE ) && (rawData[i+1][RANGE] > MINRANGE);
    next2PtHasReturn = (rawData[i+2][RANGE] < MAXRANGE ) && (rawData[i+2][RANGE] > MINRANGE);

    bool thisDiffInRange, nextDiffInRange, next2DiffInRange;  
    thisDiffInRange = fabs(diffs[i]) < distThresh;
    nextDiffInRange = fabs(diffs2[i]) < distThresh2;
    next2DiffInRange = fabs(diffs3[i]) < distThresh3;

    //TODO: eventually, do we want some idea of convexity?
#warning "this won't catch all cases where you have two cars < 1.5 m apart"
    //only skip no-return points
    if( !thisPtHasReturn ||
        ( !thisDiffInRange &&
          ((nextPtHasReturn && nextDiffInRange) || !nextDiffInRange) &&
          (((nextPtHasReturn || next2PtHasReturn) && next2DiffInRange) || !next2DiffInRange))) {
      
      currSize = 1;
      tempNum++;
 
      //set endpoint of previous object
      tempPos[tempNum-1][1] = i; 

      //set beginning point of new object
      tempPos[tempNum][0] = i+1;

      //initialize size of new object
      tempSize[tempNum] = currSize;


    } else { //this point belongs to current object

      //If we're skipping over a point, need to skip the index
      if(!thisDiffInRange) {
       	i++;
        //also need to inc currSize, which is used to index points in obj
        currSize++;
        //	fprintf(stderr, "skipped 1 incrementing i to %d. diffs1 = %f, diffs2 = %f \n", i, diffs[i-1], diffs2[i-1]);
        //	fprintf(stderr, "range = %f. thresholds: %f, %f, %f \n", rawData[i-1][RANGE],distThresh, distThresh2, distThresh3);
        if(!nextDiffInRange) {
          i++;
          currSize++;
          //  	  fprintf(stderr, "skipped 2...incrementing i to %d \n", i);
        }
      }
      currSize++;
      tempSize[tempNum] = currSize;

    } //end checking diff

  } //end cycling through points
  //  fprintf(stderr, "There were %d no returns in the scan \n", noReturns);


  tempPos[0][0] = 0;
  tempPos[tempNum][1] = NUMSCANPOINTS-1;

  int st,en;
  numSegments = 0;

  //checking that this isn't a group of no-return points
  for(int i=0; i<=tempNum; i++) {
    if(tempSize[i] >= MINNUMPOINTS) {
      st = tempPos[i][0];
      en = tempPos[i][1];
      posSegments[numSegments][0]=st;
      posSegments[numSegments][1]=en;
      sizeSegments[numSegments] = tempSize[i];
      numSegments++;
    }
  }

  return;
}

/**
 * Searches for unassigned ladar poitns within SYNTHGAPDISTANCE of the point in
 * synthPoints at index, and assigns them to the segment seg.  You should not
 * assign the point at index to seg prior to calling this function
 */
void LadarCarPerceptor::segmentPoint(int index, ladarSeg & seg){
  assert(index >= 0 && index < synthPoints.size()); // check index is valid
  if(synthPoints.at(index).segNum != -1){
    // This point has already been assigned to a segment, so return without
    // doing anything
    return;
  }
  synthPoints.at(index).segNum = seg.segNum;
  seg.points.push_back(synthPoints.at(index)); // Copy the point into the segment vector
  bool search = true;
  int checkDex = index+1;
  
  double x = synthPoints.at(index).x;
  double y = synthPoints.at(index).y;
  double theta = synthPoints.at(index).theta;
  
  while(search && checkDex < synthPoints.size()){
    // Search forwards until the difference in angles is greater than
    // GAPANGLE or you run off the side of the scan
    if(synthPoints.at(checkDex).theta - theta > GAPANGLE){
      search = false;
    }else{
      if(synthPoints.at(checkDex).segNum == -1){
	if(sqrt(pow(synthPoints.at(checkDex).x-x,2)+
		pow(synthPoints.at(checkDex).y-y,2))<SYNTHGAPDISTANCE){
	  segmentPoint(checkDex,seg);
	}
      }
    }
    checkDex++;
  }
  checkDex = index -1;
  search = true;
  while(search && checkDex >= 0){
    // Search backwards until the difference in angles is greater than
    // GAPANGLE or you run off the side of the scan
    if(theta-synthPoints.at(checkDex).theta  > GAPANGLE){
      search = false;
    }else{
      if(synthPoints.at(checkDex).segNum == -1){
	if(sqrt(pow(synthPoints.at(checkDex).x-x,2)+
		pow(synthPoints.at(checkDex).y-y,2))<SYNTHGAPDISTANCE){
	  segmentPoint(checkDex,seg);
	}
      }
    }
    checkDex--;
  }
}

/**
 * Segments the synthetic data based on euclidean distance
 */
void LadarCarPerceptor::segmentSynthScan(){
  ladarSeg tempSeg;
  synthSegments.clear();
  tempSeg.segNum = 0;
  for(int i = 0; i < synthPoints.size(); i++){
    if(synthPoints.at(i).segNum == -1){
      segmentPoint(i, tempSeg);
      // Check that this segment has enough points to count
      if(tempSeg.points.size() >=MINNUMPOINTS){
	sort(tempSeg.points.begin(),tempSeg.points.end());
	synthSegments.push_back(tempSeg);
	tempSeg.segNum++;
      }
      tempSeg.points.clear();
    }
  }
  sort(synthSegments.begin(),synthSegments.end());
}


/**
 * This code goes through all the new segments
 * and creates new objects for them. 
 * This includes calculating the occlusions at each edge,
 * calculating the center (excluding no-return points),
 * and storing the relevant x,y,z/range,theta data
 *
 * Fields filled in for objectRepresentation:
 *   dStart, dEnd, seenStart, seenEnd, startPoint, endPoint
 *   lastScan, center, el
 *
 * Parameters defined in .hh file that change operation:
 *   ONE_END_IN_FOREGROUND vs. BOTH_ENDS_IN_FOREGROUND
 **/
void LadarCarPerceptor::createObjects() {

  numNewObjects = 0;

  int objStart, objEnd;
  double dStart, dEnd;

  point2arr tmparr;

  MapElement el;
  vector <point2> objectPoints;
  point2 tempPt;
  int bytesSent;
  int tempNumNonCarObjects = 0;

  for(int i=0; i<numSegments; i++) 
    {
      tmparr.clear();
      int objectsize = sizeSegments[i];

      objStart = posSegments[i][0];
      objEnd = posSegments[i][1];

      //if dStart or dEnd > 0, that means that that end of 
      //the object is in the foreground, <0 means the 
      //object next to it is in the foreground, and 0 means 
      //that it's at the edge of a scan

      //FIXME: may want to check beyond one no-return point...
      //       in Tintersection/t2_bm, we get a noreturn between
      //       the two cars that causes that one scan to have
      //       wrong occlusion values and thus be misassociated
      if(objStart == 0) {
        dStart = 0;
      } else {
        dStart = -diffs[objStart - 1];
      }
      if(objEnd == NUMSCANPOINTS - 1) {
        dEnd = 0;
      } else {
        dEnd = diffs[objEnd];
      }


      //will ignore any segment w/ both(either) ends in background
      bool useSeg;
#ifdef ONE_END_IN_FOREGROUND
      useSeg = (dStart > 0) || (dEnd > 0);
#endif
#ifdef BOTH_ENDS_IN_FOREGROUND
      useSeg = (dStart > 0) && (dEnd > 0);
#endif
#ifdef NO_FOREGROUND_REQUIREMENT
      useSeg = true;
#endif 
      point2arr objpts;
      point2arr objdepthpts;
      point2 tmppt;

      if(useSeg) {

	objpts.clear();
	objdepthpts.clear();

        newObjects[numNewObjects].dStart = dStart;
        newObjects[numNewObjects].dEnd = dEnd; 

        newObjects[numNewObjects].startIndex = objStart;
        newObjects[numNewObjects].endIndex = objEnd;

        newObjects[numNewObjects].startPoint = point2(xyzData[objStart][0],xyzData[objStart][1]);
        newObjects[numNewObjects].endPoint = point2(xyzData[objStart + objectsize - 1][0],xyzData[objStart+objectsize - 1][1]);
        if(0 != dStart) {
          newObjects[numNewObjects].seenStart = (int) (dStart / fabs(dStart));
        } else {
          newObjects[numNewObjects].seenStart = 0;
        }
        if(0 != dEnd) {
          newObjects[numNewObjects].seenEnd = (int) (dEnd / fabs(dEnd));
        } else {
          newObjects[numNewObjects].seenEnd = 0;
        }
	//      fprintf(stderr, "for obj %d, dStart and dEnd: %f, %f \n", numNewObjects, dStart, dEnd);
	//      fprintf(stderr, "....and seenStart, seenEnd: %f, %f \n", dStart / fabs(dStart), dEnd / fabs(dEnd));

#warning "keeping this many points may be a problem"
        for(int m=objectsize-1;m>=0;m--) {
	  //don't include no-return points in average
	  double temprange = rawData[m+objStart][RANGE];
	  if((MAXRANGE > temprange) && (MINRANGE < temprange)) {
	    tmppt.x = xyzData[m+objStart][0];
	    tmppt.y = xyzData[m+objStart][1];
	    tmppt.z = xyzData[m+objStart][2];
	    objpts.push_back(tmppt);
	    //	    objdepthpts.push_back(tmppt);
	  }
	}

        //including depth in the objects 
	//TODO: enabling this messes up cars...need to separate it if map	
	//        ever wants to use this data
       if(objectDepth > 0) {
	 for(int m=objectsize-1;m>=0;m--) {
	   double temprange = rawData[m+objStart][RANGE];
	   if( (MAXRANGE>temprange) && (MINRANGE < temprange)){ 
             tmppt.x = depthData[m+objStart][0];
	     tmppt.y = depthData[m+objStart][1];
	     tmppt.z = depthData[m+objStart][2];
	     objdepthpts.push_back(tmppt);
	   }
	 }
       }

	newObjects[numNewObjects].lastScanDepth = objdepthpts;
	newObjects[numNewObjects].lastScan = objpts;
  
	newObjects[numNewObjects].el = ellipse(objpts);

	numNewObjects++; 

      } else if(outputRate < (DGCgettime() - lastSentObjs)) { //we haven't seen enough ends to track
	//object lacks the foreground edges to be a candidate car, and this module is 
	//responsible for sending all obstacles to the map

	DGCgettime(lastSentObjs);

	objectPoints.clear();
	el.clear();

	for(int m=0; m<objectsize;m++) {
	  double temprange = rawData[m+objStart][RANGE];
	  if((MAXRANGE > temprange) && (MINRANGE < temprange)) {
	    tempPt.x = depthData[m+objStart][0];
	    tempPt.y = depthData[m+objStart][1];
	    tempPt.z = depthData[m+objStart][2];
	    objectPoints.push_back(tempPt);
	  }
	}

	for(int m=objectsize-1;m>=0;m--) {
	  //don't include no-return points in message
	  double temprange = rawData[m+objStart][RANGE];
	  if((MAXRANGE > temprange) && (MINRANGE < temprange)) {
	    tempPt.x = xyzData[m+objStart][0];
	    tempPt.y = xyzData[m+objStart][1];
	    tempPt.z = xyzData[m+objStart][2];
	    objectPoints.push_back(tempPt);
	  }
	}

	//	el.setId(mapCarID, currSensor, moduleNonCarPtsID,tempNumNonCarObjects);
#warning "need better way to handle this"
	el.setId(mapCarID,tempNumNonCarObjects%100);
	el.setTypeClear(); //since IDs are tracked, need to clear out t
	//	bytesSent = sendMapElement(&el, outputSubgroup);

	el.setGeometry(objectPoints);
	el.setTypeLadarObstacle();
	el.plotColor = MAP_COLOR_PINK;
	el.state = rawState;

#warning "these objects don't keep the same ID!"
	if(objectPoints.size() > 0) { //only send if non-empty group of points

	  //	  bytesSent = sendMapElement(&el, outputSubgroup);
	  //	  fprintf(stderr, "Sending non-car points!! \n");
	  tempNumNonCarObjects ++;
	}

      }  //end chekcing fg/bg
    }   //end cycling through segments

  numNonCarObjects = tempNumNonCarObjects;

}   //end of createObjects()



/**
 * This code goes through all the new segments
 * and creates new objects for them. 
 * This includes calculating the occlusions at each edge,
 * calculating the center (excluding no-return points),
 * and storing the relevant x,y,z/range,theta data
 *
 * Fields filled in for objectRepresentation:
 *   dStart, dEnd, seenStart, seenEnd, startPoint, endPoint
 *   lastScan, center, el
 *
 * Uses the synthetic ladar scan
 *
 * Parameters defined in .hh file that change operation:
 *   ONE_END_IN_FOREGROUND vs. BOTH_ENDS_IN_FOREGROUND
 **/
void LadarCarPerceptor::createSynthObjects() {

  numNewObjects = 0;

  double dStart, dEnd;

  point2arr tmparr;

  MapElement el;
  vector <point2> objectPoints;
  point2 tempPt;
  int bytesSent;
  int tempNumNonCarObjects = 0;
  int objectsize;

  for(int i=0; i<synthSegments.size(); i++) 
    {
      tmparr.clear();
      point2arr objpts;
      point2arr objdepthpts;
      vector<short> source;
      point2 tmppt;
      objectsize = synthSegments.at(i).size();
      bool useSeg = true;
      if(useSeg) {

	objpts.clear();
	objdepthpts.clear();

        newObjects[numNewObjects].dStart = dStart;
        newObjects[numNewObjects].dEnd = dEnd; 

        newObjects[numNewObjects].startPoint = synthSegments.at(i).points.at(0).getPoint();
        newObjects[numNewObjects].endPoint = synthSegments.at(i).points.at(objectsize-1).getPoint();

	// THIS NEEDS TO BE EVALUATED
        if(0 != dStart) {
          newObjects[numNewObjects].seenStart = (int) (dStart / fabs(dStart));
        } else {
          newObjects[numNewObjects].seenStart = 0;
        }
        if(0 != dEnd) {
          newObjects[numNewObjects].seenEnd = (int) (dEnd / fabs(dEnd));
        } else {
          newObjects[numNewObjects].seenEnd = 0;
        }
        for(int m=objectsize-1;m>=0;m--) {
	  ladarPoint temp = synthSegments.at(i).points.at(m);
	  tmppt.x = temp.x;
	  tmppt.y = temp.y;
	  tmppt.z = temp.z;
	  objpts.push_back(tmppt);
	  if(sortBySensor){
	    source.push_back(temp.source);
	  }
	}

        //including depth in the objects 
	//TODO: enabling this messes up cars...need to separate it if map	
	//        ever wants to use this data
       if(objectDepth > 0) {
	 for(int m=objectsize-1;m>=0;m--) {
	   ladarPoint temp = synthSegments.at(i).points.at(m);
	   tmppt.x = temp.depthx;
	   tmppt.y = temp.depthy;
	   tmppt.z = temp.depthz;
	   objdepthpts.push_back(tmppt);
	 }
       }

	newObjects[numNewObjects].lastScanDepth = objdepthpts;
	newObjects[numNewObjects].lastScan = objpts;
	if(sortBySensor){
	  newObjects[numNewObjects].source = source;
	}
	newObjects[numNewObjects].el = ellipse(objpts);
	
	numNewObjects++; 

      } else if(outputRate < (DGCgettime() - lastSentObjs)) { //we haven't seen enough ends to track
	//object lacks the foreground edges to be a candidate car, and this module is 
	//responsible for sending all obstacles to the map

	DGCgettime(lastSentObjs);

	objectPoints.clear();
	el.clear();

	for(int m=0; m<objectsize;m++) {
	  ladarPoint temp = synthSegments.at(i).points.at(m);
	  tempPt.x = temp.x;
	  tempPt.y = temp.y;
	  tempPt.z = temp.z;
	  objectPoints.push_back(tempPt);
	}

	for(int m=objectsize-1;m>=0;m--) {
	  ladarPoint temp = synthSegments.at(i).points.at(m);
	  tempPt.x = temp.x;
	  tempPt.y = temp.y;
	  tempPt.z = temp.z;
	  objectPoints.push_back(tempPt);
	}

	//	el.setId(mapCarID, currSensor, moduleNonCarPtsID,tempNumNonCarObjects);
#warning "need better way to handle this"
	el.setId(mapCarID,tempNumNonCarObjects%100);
	el.setTypeClear(); //since IDs are tracked, need to clear out t
	//	bytesSent = sendMapElement(&el, outputSubgroup);

	el.setGeometry(objectPoints);
	el.setTypeLadarObstacle();
	el.plotColor = MAP_COLOR_PINK;
	el.state = rawState;

#warning "these objects don't keep the same ID!"
	if(objectPoints.size() > 0) { //only send if non-empty group of points

	  //	  bytesSent = sendMapElement(&el, outputSubgroup);
	  //	  fprintf(stderr, "Sending non-car points!! \n");
	  tempNumNonCarObjects ++;
	}

      }  //end chekcing fg/bg
    }   //end cycling through segments

  numNonCarObjects = tempNumNonCarObjects;

}   //end of createSynthObjects()



/**
 * This function assumes that the scan has been segmented,
 * and classifies the new objects into: 
 *    already-seen object
 *    already-seen car
 *    new object 
 * Then, it calls the appropriate functions to update
 * the representations.
 *
 * Parameters:
 *   USESE_PREDICTION_FOR_ASSOCIATION 
 *
 * Improvements needed:
 *  1) Only update at end of cycle, not each time - do 
 *     this when I find data set requiring it
 *     20070302_ladar_v4/Tintersection/t2_bm, near the end
 **/
void LadarCarPerceptor::classifyObjects() 
{

  for(int i=0; i<MAXNUMCARS; i++) {
    carsToUpdate[i].clear();
  }
  for(int i=0; i<MAXNUMOBJS; i++) {
    objsToUpdate[i].clear();
  }

  double distTraveled;
  int inside1, inside2;

  //whether this segment has already been assigned
  int taken[numSegments];
  for(int i=0;i<numNewObjects;i++) {
    taken[i] = 0;
  }

  //#warning "magic #"
  for(int i = 0; i<61; i++) {
    matchedCar[i] = 0;
    matchedObj[i] = 0;
  }

  double oldX, oldY;
  double newX, newY;

  /**
   * First, we cycle through all previously seen objects 
   * to see if the new object matches one of them
   **/
  for(unsigned int i=0; i<usedObjs.size(); i++) {
    int k = usedObjs.at(i);
    oldX = objects[k].el.center.x;
    oldY = objects[k].el.center.y;

    //    fprintf(stderr, "old obj position: %f %f \n", oldX, oldY);
    for(int j=0; j<numNewObjects; j++) {
      if(taken[j] == 0) { //if new object hasn't already been classified
	newX = newObjects[j].el.center.x;
	newY = newObjects[j].el.center.y;
	distTraveled = dist(oldX, oldY, newX, newY);

	//Check both ways, in case we're matching a 
	//fragment to a larger object
	inside1 = objects[k].el.inside(newObjects[j].el.center, MARGIN);
	inside2 = newObjects[j].el.inside(objects[k].el.center, MARGIN);

	//	fprintf(stderr, "in classify objects, oldobj %d, newobj %d, inside1 = %d, inside2 = %d \n", k, j, inside1, inside2);
	if((1 == inside1) || (1 == inside2)) {
	  taken[j] = 1;
	  //	  fprintf(stderr,"updating object #%d with new object #%d, at a distance = %f\n",k, j, distTraveled, inside1, inside2);
	  objsToUpdate[k].push_back(j);
	  matchedObj[i] = 1;
	} //end checking if they match
      } //end checking of newObj is taken
    } //end cycling through new objects
  } //end cycling through old objects


  /**
   * Next, we cycle through all previously seen cars 
   * to see if obj matches. This functionality goes 
   * AFTER matching newobjs to oldobjs cuz we don't want 
   * to match an already seen lamppost in the car's path 
   * to the car...
   **/
  //FIXME: may need to look for the MINIMUM distance to any car, because
  //       when two are quite close, I've seen erroneous association 
  //       (Tintersection/t2_bm)
  point2 old_r1, old_r2, old_corner;
  point2 new_r1, new_r2, new_corner;
  double diag;
  line2 side1, side2, side3, side4;
  point2 br, bl, fr, fl;
  point2arr boundingbox;
  point2 newPt;
  double tempX, tempY;

  for(unsigned int i=0; i<usedCars.size(); i++) {

    int k = usedCars.at(i);
#ifdef USE_PREDICTION_FOR_ASSOCIATION
    tempX = cars[k].Xmu_hat.getelem(0,0);
    tempY = cars[k].Ymu_hat.getelem(0,0);

    switch(cars[k].trackedCorner) {
    case BACK_RIGHT:
      oldX = tempX;
      oldY = tempY;
      break;

    case BACK_LEFT:
      oldX = tempX - cars[k].width.x;
      oldY = tempY - cars[k].width.y;
      break;

    case FRONT_RIGHT:
      oldX = tempX - cars[k].length.x;
      oldY = tempY - cars[k].length.y;
      break;

    case FRONT_LEFT:
      oldX = tempX - cars[k].length.x - cars[k].width.x;
      oldY = tempY - cars[k].length.y - cars[k].width.y;
      break;

    default:
      ERROR("");

    }

#else
    oldX = cars[k].corners[BACK_RIGHT].x;
    oldY = cars[k].corners[BACK_RIGHT].y;
#endif
    //generate box extending by CARMARGIN on all sides of observed car
    old_r1 = cars[k].length;
    old_r2 = cars[k].width;
    old_corner = point2(oldX, oldY);
    diag = atan2(old_r1.y+old_r1.y, old_r1.x+old_r2.x);
    new_corner = old_corner - CARMARGIN*point2(cos(diag),sin(diag));
    new_r1 = ((2*CARMARGIN + old_r1.norm())/old_r1.norm())*old_r1;
    new_r2 = ((2*CARMARGIN + old_r2.norm())/old_r2.norm())*old_r2;

    boundingbox.clear();
    boundingbox.push_back(new_corner);
    boundingbox.push_back(new_corner+new_r1);
    boundingbox.push_back(new_corner+new_r1+new_r2);
    boundingbox.push_back(new_corner+new_r2);

    for(int j=0; j<numNewObjects; j++) {
      if(taken[j] == 0) { //this obj has not already been claimed
	int isinside = boundingbox.is_inside_poly(newObjects[j].el.center);

	//check if this obj matches car
	if(1 == isinside) {
	  //the obj and car should be associated
	  taken[j] = 1;
	  //	  fprintf(stderr, "new obj matched car # %d at %f, %ff-- updating!! \n", k, newX, newY);
	  carsToUpdate[k].push_back(j);
	  matchedCar[j] = 1;
	}  //end of checking if newObj and car match
      }   //end of checking if obj already claimed
    }    //end cycling thru newObjs
  }     //end cycling thru numCars


  /**
   * Finally, we take care of the new objects that don't match anything we've
   * seen before
   **/
  for(int i=0; i<numNewObjects; i++) {
    if(taken[i] == 0) { 
      //      fprintf(stderr, "no match for new obj at position: %f %f \n", newX, newY);
      addObject(&newObjects[i]);
    } //end checking if object already classified
  } //end cycling through numSegments


  //cycle through, and update each object once
  unsigned int arrlength;
  int j; //yes, this will complain, but it keeps the for loop from segfaulting when counting down
  unsigned int l;
  double dStart, dEnd;
  int startIndex;
  vector<short> sources;
  if(useSynth){
    startIndex = numSynthScanPoints;
  }else{
    startIndex = 181;
  }
  int endIndex = -1;

  point2arr newpts;
  point2arr newdepthpts;
  for(int i = 0; i < MAXNUMOBJS; i++) {
    arrlength= objsToUpdate[i].size();
    if(0 < arrlength) {
      newpts.clear();
      newdepthpts.clear();
      sources.clear();
      for(j = arrlength-1; j >= 0; j--) {
	int k = objsToUpdate[i].at(j);

	if(endIndex < newObjects[k].endIndex) {
	  endIndex = newObjects[k].endIndex;
	  dEnd = newObjects[k].dEnd;
	}
	if(startIndex > newObjects[k].startIndex) {
	  startIndex = newObjects[k].endIndex;
	  dStart = newObjects[k].dStart;
	}

	//get all points
	point2arr tmppts;
	tmppts = point2arr(newObjects[k].lastScan);
	//	point2 center = tmppts.average();
	//	fprintf(stderr, "updating object %d, chunk %d, w/ %d points centered at %f, %f \n", i, k, tmppts.size(),center.x, center.y);

	for(l = 0; l < tmppts.size(); l++) {
	  newpts.push_back(tmppts.arr[l]);
	}

	if(sortBySensor){
	  for(int l = 0; l < newObjects[k].source.size(); l++){
	    sources.push_back(newObjects[k].source.at(l));
	  }
	}

	//FIXME: this won't handle multiple segs updating same object...
	tmppts.clear();
	tmppts = point2arr(newObjects[k].lastScanDepth);
	for(l = 0; l < tmppts.size(); l++) {
	  newdepthpts.push_back(tmppts.arr[l]);
	}
	

      }
      //now, update the object
      //      fprintf(stderr, "updating object #%d with %d points. dstart = %f, dend = %f \n", i, newpts.size(), dStart, dEnd);
      if(sortBySensor){
	updateObject(&objects[i], newpts, newdepthpts, sources, dStart, dEnd);	
      }else{
	updateObject(&objects[i], newpts, newdepthpts, dStart, dEnd);
      }
    }
  }

  //NOW update the cars
  objectRepresentation newObject;
  carRepresentation newCar;

  for(int i=0; i<MAXNUMCARS; i++) {
    //reset vars for each new car
    point2arr newdepthpts;
    newpts.clear();
    sources.clear();
    if(useSynth){
      startIndex = numSynthScanPoints;
    }else{
      startIndex = 181;
    }
    endIndex = -1;

    arrlength= carsToUpdate[i].size();
    if(0 < arrlength) {
      //          for(j = 0; j < arrlength; j++) {
      for(j = arrlength-1; j>=0; j--) {
	//keep track of occlusions at ends of obj
	int k = carsToUpdate[i].at(j);
	if(endIndex < newObjects[k].endIndex) {
	  endIndex = newObjects[k].endIndex;
	  dEnd = newObjects[k].dEnd;
	}
	if(startIndex > newObjects[k].startIndex) {
	  startIndex = newObjects[k].endIndex;
	  dStart = newObjects[k].dStart;
	}

	//get all points
	point2arr tmppts, tmpdepthpts;
	tmppts = point2arr(newObjects[k].lastScan);
	tmpdepthpts = point2arr(newObjects[k].lastScanDepth);
	point2 center = tmppts.average();
	//            fprintf(stderr, "updating car %d, chunk %d, w/ %d points centered at %f, %f \n", i, k, tmppts.size(),center.x, center.y);
	//	    fprintf(stderr, "   pts: \n");
	// Get the source sensor for each point
	if(sortBySensor){
	  for(int l = 0; l < newObjects[k].source.size(); l++){
	    sources.push_back(newObjects[k].source.at(l));
	  }
	}
	for(l = 0; l < tmppts.size(); l++) {
	  //	      fprintf(stderr,"(%f, %f) \n", tmppts.arr[l].x, tmppts.arr[l].y);
	  newpts.push_back(tmppts.arr[l]);

	}
#warning "got segfault here - but why aren't tmppts and tmpdepthpts the same size??"
	for(l = 0; l < tmpdepthpts.size(); l++) {
	  newdepthpts.push_back(tmpdepthpts.arr[l]);
	}


      }
      //now, update the car
      //      fprintf(stderr, "updating car #%d with %d points \n", i, newpts.size());

      if(sortBySensor){
	updateCar(&cars[i], newpts, newdepthpts, sources, dStart, dEnd);
      }else{
	updateCar(&cars[i], newpts, newdepthpts, dStart, dEnd);
      }

    }
  }



} // end classifyObjects()



  /**
   * This function is called when it is determined that a newobject 
   * matches an old one. As such, it simply replaces the center position, 
   * and adds the latest scan data
   *
   * Updates the following fields:
   *  dStart, dEnd, seenStart, seenEnd, startPoint, endPoint
   *  el, center, velocity, ptsinvelocity, history
   *  lastScan, timesSeen, lastSeen
   **/
void LadarCarPerceptor::updateObject(objectRepresentation* oldObj, point2arr points, point2arr depthpoints, double dStart, double dEnd) {

  oldObj->lastUpdated = DGCgettime();

  oldObj->dStart = max(oldObj->dStart, dStart);
  oldObj->dEnd = max(oldObj->dEnd, dEnd);
  point2 dObj;

  if(0 != oldObj->seenStart) {
    oldObj->seenStart = (int) (oldObj->dStart / fabs(oldObj->dStart));
  } else {
    oldObj->seenStart = 0;
  }
  if(0!= oldObj->seenEnd) {
    oldObj->seenEnd = (int) (oldObj->dEnd / fabs(oldObj->dEnd));
  } else {
    oldObj->seenEnd = 0;
  }

  point2 oldCenter, newCenter;
  oldCenter = oldObj->el.center;
  oldObj->el.add_points(points);
  newCenter = oldObj->el.center;
  dObj = newCenter - oldCenter;

  //update non-KF velocity estimate
  point2 tempvel = oldObj->ptsinvelocity*oldObj->velocity + dObj;
  oldObj->ptsinvelocity++;
  oldObj->velocity = tempvel/oldObj->ptsinvelocity;

  oldObj->history.push_back(oldObj->el.center);

  oldObj->lastScan = points;
  oldObj->lastScanDepth = depthpoints;
  oldObj->timesSeen = oldObj->timesSeen + 1;
  oldObj->lastSeen = numFrames;

  //check if tracked obj is stationary, update appropriate fields in struct. 
  //for now, "stationary" is defined to be a constant number.
  //TODO: what assumptions does planner make about an object 
  //      declared stationary?? (I need same threshold)
  double currentvel = sqrt(pow(oldObj->Xmu.getelem(1,0),2) + pow(oldObj->Ymu.getelem(1,0),2));
  if(STATIONARYVELOCITY > currentvel) {
    oldObj->isStopped = true;
  } else {
    oldObj->isStopped = false;
    oldObj->lastTimeMoving = DGCgettime();
  }

  //collecting data for training set
  if(oldObj->timesSeen < 30) {
    oldObj->logData[oldObj->timesSeen].el_x = oldObj->el.center.x;
    oldObj->logData[oldObj->timesSeen].el_y = oldObj->el.center.y;
    oldObj->logData[oldObj->timesSeen].el_a = oldObj->el.a;
    oldObj->logData[oldObj->timesSeen].el_b = oldObj->el.b;

    oldObj->logData[oldObj->timesSeen].alice_x = currState.X;
    oldObj->logData[oldObj->timesSeen].alice_y = currState.Y;
    oldObj->logData[oldObj->timesSeen].alice_theta = currState.Theta;

    oldObj->logData[oldObj->timesSeen].vel_x = oldObj->velocity.x;
    oldObj->logData[oldObj->timesSeen].vel_y = oldObj->velocity.y;

    double len2 = dist(oldObj->endPoint.x, oldObj->endPoint.y, oldObj->startPoint.x, oldObj->startPoint.y);
    oldObj->logData[oldObj->timesSeen].obj_length= len2;

    double tempa, tempb;
    point2 t1, t2;
    double standardError = points.fit_line(tempa, tempb, t1, t2);
    oldObj->logData[oldObj->timesSeen].std_linefit = standardError;
  }
}


  /**
   * This function is called when it is determined that a newobject 
   * matches an old one. As such, it simply replaces the center position, 
   * and adds the latest scan data
   *
   * Updates the following fields:
   *  dStart, dEnd, seenStart, seenEnd, startPoint, endPoint
   *  el, center, velocity, ptsinvelocity, history
   *  lastScan, timesSeen, lastSeen
   **/
void LadarCarPerceptor::updateObject(objectRepresentation* oldObj, point2arr points, point2arr depthpoints, vector<short> sources, double dStart, double dEnd) {

  oldObj->source = sources;
  oldObj->lastUpdated = DGCgettime();

  oldObj->dStart = max(oldObj->dStart, dStart);
  oldObj->dEnd = max(oldObj->dEnd, dEnd);
  point2 dObj;

  int bumperPoints = 0;
  int roofPoints = 0;
  int stereoPoints = 0;
  for(int i = 0; i < points.size(); i++){
    if(sources.at(i) == BUMPER){
      bumperPoints++;
    }else if(sources.at(i) == ROOF){
      roofPoints++;
    }else{
      stereoPoints++;
    }
  }

  if(0 != oldObj->seenStart) {
    oldObj->seenStart = (int) (oldObj->dStart / fabs(oldObj->dStart));
  } else {
    oldObj->seenStart = 0;
  }
  if(0!= oldObj->seenEnd) {
    oldObj->seenEnd = (int) (oldObj->dEnd / fabs(oldObj->dEnd));
  } else {
    oldObj->seenEnd = 0;
  }

  point2 oldCenter, newCenter;
  oldCenter = oldObj->el.center;
  oldObj->el.add_points(points);
  newCenter = oldObj->el.center;
  dObj = newCenter - oldCenter;

  //update non-KF velocity estimate
  point2 tempvel = oldObj->ptsinvelocity*oldObj->velocity + dObj;
  oldObj->ptsinvelocity++;
  oldObj->velocity = tempvel/oldObj->ptsinvelocity;

  oldObj->history.push_back(oldObj->el.center);

  oldObj->lastScan = points;
  oldObj->lastScanDepth = depthpoints;
  oldObj->timesSeen = oldObj->timesSeen + 1;
  oldObj->lastSeen = numFrames;

  //check if tracked obj is stationary, update appropriate fields in struct. 
  //for now, "stationary" is defined to be a constant number.
  //TODO: what assumptions does planner make about an object 
  //      declared stationary?? (I need same threshold)
  double currentvel = sqrt(pow(oldObj->Xmu.getelem(1,0),2) + pow(oldObj->Ymu.getelem(1,0),2));
  if(STATIONARYVELOCITY > currentvel) {
    oldObj->isStopped = true;
  } else {
    oldObj->isStopped = false;
    oldObj->lastTimeMoving = DGCgettime();
  }

  //collecting data for training set
  if(oldObj->timesSeen < 30) {
    oldObj->logData[oldObj->timesSeen].el_x = oldObj->el.center.x;
    oldObj->logData[oldObj->timesSeen].el_y = oldObj->el.center.y;
    oldObj->logData[oldObj->timesSeen].el_a = oldObj->el.a;
    oldObj->logData[oldObj->timesSeen].el_b = oldObj->el.b;

    oldObj->logData[oldObj->timesSeen].alice_x = currState.X;
    oldObj->logData[oldObj->timesSeen].alice_y = currState.Y;
    oldObj->logData[oldObj->timesSeen].alice_theta = currState.Theta;

    oldObj->logData[oldObj->timesSeen].vel_x = oldObj->velocity.x;
    oldObj->logData[oldObj->timesSeen].vel_y = oldObj->velocity.y;

    double len2 = dist(oldObj->endPoint.x, oldObj->endPoint.y, oldObj->startPoint.x, oldObj->startPoint.y);
    oldObj->logData[oldObj->timesSeen].obj_length= len2;

    double tempa, tempb;
    point2 t1, t2;
    double standardError = points.fit_line(tempa, tempb, t1, t2);
    oldObj->logData[oldObj->timesSeen].std_linefit = standardError;
  }
}

/**
 * This function is called when it is determined that a new object
 * has no match in already seen objects or cars
 * It's job is to add an object to the object array, if there is room.
 *
 * Fields set in object's representation:
 *   ID, velocity, ptsinvelocity, center, el
 *   dStart, dEnd, seenStart, seenEnd, startPoint, endPoint
 *   timesSeen, lastSeen, history, lastScan
 *   Xmu, Ymu, Xsigma, Ysigma, Xmu_hat, Ymu_hat, 
 *     Xsigma_hat, Ysigma-hat
 **/
void LadarCarPerceptor::addObject(objectRepresentation* newObj) {
  numObjects = numObjects + 1;

  newObj->velocity = point2(0,0);
  newObj->ptsinvelocity = 0;

  if((mapIndices.size() > 0) && (usedObjs.size() < MAXNUMOBJS)) {

    //find out which spot in object array is next up
    int newIndex;
    newIndex = openObjs.back();
    openObjs.pop_back();
    usedObjs.push_back(newIndex);

    objects[newIndex].ID = numObjects;
    objects[newIndex].mapID = mapIndices.back();
    mapIndices.pop_back();

    objects[newIndex].dStart = newObj->dStart;
    objects[newIndex].dEnd = newObj->dEnd;

    objects[newIndex].timesSeen = 1;
    objects[newIndex].lastSeen = numFrames;


    //transferring vectors of scan/range/angle history
    point2arr newscans = point2arr(newObj->lastScan);
    objects[newIndex].lastScan = newObj->lastScan;
    objects[newIndex].lastScanDepth = newObj->lastScanDepth;
 
    objects[newIndex].el = ellipse(newscans);

    objects[newIndex].history.clear();
    objects[newIndex].history.push_back(newscans.average());

    objects[newIndex].isStopped = false;
    objects[newIndex].lastTimeMoving = DGCgettime();

    objects[newIndex].seenStart = newObj->seenStart;
    objects[newIndex].seenEnd = newObj->seenEnd;
    objects[newIndex].startPoint = point2(newObj->startPoint);
    objects[newIndex].endPoint = point2(newObj->endPoint);
    objects[newIndex].startIndex = newObj->startIndex;
    objects[newIndex].endIndex = newObj->endIndex;

    //initizlizing KF variables
    objects[newIndex].Xmu.resetSize(2,1);
    objects[newIndex].Ymu.resetSize(2,1);
    objects[newIndex].Xsigma.resetSize(2,2);
    objects[newIndex].Ysigma.resetSize(2,2);
    objects[newIndex].Xmu_hat.resetSize(2,1);
    objects[newIndex].Ymu_hat.resetSize(2,1);
    objects[newIndex].Xsigma_hat.resetSize(2,2);
    objects[newIndex].Ysigma_hat.resetSize(2,2);

    double XMelems[] = {objects[newIndex].el.center.x, 0};
    double YMelems[] = {objects[newIndex].el.center.y, 0};
    double XSelems[] = {1,0,0,1};
    double YSelems[] = {1,0,0,1};

    objects[newIndex].Xmu.setelems(XMelems);
    objects[newIndex].Xsigma.setelems(XSelems);
    objects[newIndex].Ymu.setelems(YMelems);
    objects[newIndex].Ysigma.setelems(YSelems);
    // Need to have the prediction matricies initialized so that the kalman
    // filter updating step can be performed correctly.  Since the update should
    // be called the same scan as the object creation, can just copy the 
    // current values into the prediction.
    objects[newIndex].Xmu_hat.setelems(XMelems);
    objects[newIndex].Xsigma_hat.setelems(XSelems);
    objects[newIndex].Ymu_hat.setelems(YMelems);
    objects[newIndex].Ysigma_hat.setelems(YSelems);
  } else {

    ERROR("trying to add obj; usedObjs size = %d, mapIndices size = %d", usedObjs.size(), mapIndices.size());

  } //end checking that there's enough room to add new object
} //end addObject



void LadarCarPerceptor::writeLogs() {
  for(unsigned int i = 0; i < usedObjs.size(); i++) {
    int index = usedObjs.at(i);

    if(objects[index].timesSeen == 30) {
      fprintf(stderr, "writing log!! \n");
      fprintf(logfile, "\n object %d \n", objects[index].ID);
      for(int j = 0; j < 30; j++) {
	fprintf(logfile, "%f %f %f %f %f %f %f %f %f %f %f \n", \
		objects[index].logData[j].el_x, objects[index].logData[j].el_y, \
		objects[index].logData[j].el_a, objects[index].logData[j].el_b, \
		objects[index].logData[j].alice_x, objects[index].logData[j].alice_y, \
		objects[index].logData[j].alice_theta, \
		objects[index].logData[j].vel_x, objects[index].logData[j].vel_y, \
		objects[index].logData[j].obj_length, objects[index].logData[j].std_linefit);

      }
    }
  }
}

/**
 * This function cycles through every currently tracked
 * object and checks whether it is moving. If an object
 * is moving, it is turned into a car.
 *
 * There are two choices for determination of motion:
 * 1) use calculated velocity, either from the Kalman
 *    filter, or from an average of deltas
 * 2) Whether its average position over history is
 *    outside the ellipse describing its points
 *
 * Behaviour is controlled by the parameters in .hh file:
 *   USE_KALMAN_FILTER_VELOCITY vs. USE_AVERAGE_VELOCITY
 *   USE_ELLIPSE_FOR_CHECKMOTION vs. USE_VELOCITY_FOR_CHECKMOTION
 *   MINSPEED
 **/
//FIXME: current check works very well for cars far away, but sometimes
//       misses cars driving across ~< 10m away (see any Tintersection)
void LadarCarPerceptor::checkMotion() {
  unsigned int histLength;
  point2 sumPt;
  point2 avgPt;
  int moving;

  for(unsigned int i = 0; i< usedObjs.size(); i++) {
    int index = usedObjs.at(i);
    moving = 0;
    sumPt.clear();
    avgPt.clear();
    if(objects[index].timesSeen > 30 && !objects[index].seenStart && !objects[index].seenEnd) {
      fprintf(stderr, "object %d, seen %d times, seenSTart = %d, seenEnd = %d \n", objects[index].ID, objects[index].timesSeen, objects[index].seenStart, objects[index].seenEnd);
    }
    if(objects[index].timesSeen > 30 && (objects[index].seenStart || objects[index].seenEnd)) {
      histLength = objects[index].history.size();
      for(unsigned int j=0; j<histLength; j++) {
	sumPt = sumPt + objects[index].history.at(j);
      }
      avgPt = sumPt/(1.0*histLength);
      //this returns 1 if avgpt is inside ellipse
      //      fprintf(stderr, "checking motion for obj %d.  \n", objects[index].ID);
      moving = objects[index].el.inside(avgPt, 0.0, .75);

      double velX, velY, vel;

#ifdef USE_KALMAN_FILTER_VELOCITY
      velX = objects[index].Xmu.getelem(1,0);
      velY = objects[index].Ymu.getelem(1,0);
#endif
#ifdef USE_AVERAGE_VELOCITY
      velX = objects[index].velocity.x;
      velY = objects[index].velocity.y;
#endif
      vel = 75*sqrt(velX*velX+velY*velY);


       

      if(-1 == moving) {
      double maxval = 0.0;
      double freeMapCell;
      //check if the center of object is on free map cell
      if(useMap) 
	//      if(false) 
	{      
	  //	  freeMapCell = checkMap(objects[index].el.center.x, objects[index].el.center.y);

	  point2arr ptarr = objects[index].lastScan;
	  unsigned int sze = ptarr.size();

	  double prob, xpt, ypt;
	  int xval, yval;
	  for(unsigned int z=0; z<sze; z++) {
	    xpt = ptarr[z].x;
	    ypt = ptarr[z].y;
	    xval = (int)(xpt - mapCenterX)/MAPCELLDIMENSION + MAPRADIUS;
	    yval = (int)(ypt - mapCenterY)/MAPCELLDIMENSION + MAPRADIUS;

  	    emap_t val = *(tamasMap->getData() + yval * tamasMap->getWidth() + xval);
	    prob = 1-1/(1+exp(0.1*(val/256-MAPCLEARVALUE)));
	    //            MSG("checking object at coord %d, %d, prob is %f", xval, yval, prob);
	    maxval = max(maxval, prob);
	    //	    drawEMapPt(xval, yval);
	  }

	}
	if(!useMap || (MAPOCCUPIEDTHRESHOLD > maxval))
	  //	if(true)
	  {
	    double maxdim = max(objects[index].el.a, objects[index].el.b);

#ifdef USE_ELLIPSE_FOR_CHECKMOTION
	    if(MAXCARELLIPSEDIM > maxdim) {
	      obj2car(i);
	      //	    fprintf(stderr, "obj at %f, %f is moving, but in occupied space. prob = %f \n", objects[index].el.center.x, objects[index].el.center.y, maxval);
	    }
#endif
	    //	    fprintf(stderr, "obj at %f, %f  is moving, avg vel = %f, %f, prob = %f \n", objects[index].el.center.x, objects[index].el.center.y, 75*objects[index].velocity.x, 75*objects[index].velocity.y, freeMapCell);
	  } else {
	    fprintf(stderr, "obj at %f, %f was moving, but in occupied space. prob = %f \n", objects[index].el.center.x, objects[index].el.center.y, maxval);
	  }
	//        fprintf(stderr, "for obj %d, moving = %d  ellipse center = %f, %f; a,b = %f, %f,  avg hist = %f, %f \n", objects[index].ID, moving,  objects[index].el.center.x, objects[index].el.center.y, objects[index].el.a, objects[index].el.b, avgPt.x, avgPt.y);
      } else if(vel > MINSPEED) {
	//	  fprintf(stderr, "THIS OBJ IS MOVING!!! obj posn: %f, %f, vel = %f, timesSeen = %d, turning into car %d \n ", objects[index].el.center.x, objects[index].el.center.y, vel, objects[index].timesSeen, numCars);
#ifdef USE_VELOCITY_FOR_CHECKMOTION
	obj2car(i);
#endif
      }
      //      }
    }
  }
} //end checkMotion



  /**
   * This function turns an object into a car
   *
   * Sets the following fields in carRepresentation:
   *   X, Y, dx1, dy1, dx2, dy2
   *   seenFront, seenBack, seenCorner
   *   Xmu, Xsigma, Ymu, Ysigma
   *   ID, lastSeen, timesSeen, history
   *   velocity, ptsinvelocity
   **/
  //#warning "this code should remove the object at index, in addition to making a new car...possibly replace with addCar(&object), and removeObject(index)"
void LadarCarPerceptor::obj2car(int index) {
  //  cout<<"trying to turn an object into a car"<<endl;
  if((usedCars.size() < MAXNUMCARS-1) && (mapIndices.size() > 0)) { 
    int i = usedObjs.at(index);

    //    fprintf(stderr, "turning object %d into car. a = %f, b = %f \n", objects[i].ID, objects[i].el.a, objects[i].el.b);

    point2arr points(objects[i].lastScan);
      
    //figure out which spot in car array new car will occupy
    int temp;
    temp = openCars.back();
    openCars.pop_back();
    usedCars.push_back(temp);
    
    if(sortBySensor){
      // copy over the source vector
      cars[temp].source = objects[i].source;
      // if necessary, filter the points by source sensor
      int bumperPts = 0;
      int roofPts = 0;
      int stereoPts = 0;
      for(int w = 0; w < cars[temp].source.size(); w++){
	if(cars[temp].source.at(w) == BUMPER){
	  bumperPts++;
	}else if(cars[temp].source.at(w) == ROOF){
	  roofPts++;
	}else{
	  stereoPts++;
	}
      }
      int thresh = 0;
      if(bumperPts > MINNUMPOINTS){
	thresh = BUMPER;
      }else if(bumperPts + roofPts > MINNUMPOINTS){
	thresh = ROOF;
      }else{
	thresh = STEREO;
      }
      point2arr sortpts;
      for(int w = 0; w < cars[temp].source.size(); w++){
	if(cars[temp].source.at(w) <=thresh){
	  sortpts.push_back(points[w]);
	}
      }
      points = sortpts;
    }

    // Initialize variables for shape kalman filter before call to fitCar
    cars[temp].Smu.resetSize(3,1);
    cars[temp].Smu_hat.resetSize(3,1);
    cars[temp].Ssigma.resetSize(3,3);
    cars[temp].Ssigma_hat.resetSize(3,3);
    // Need to initialize the shape filter with reasonable values
    double smuelem[] = {1.5,4,atan2(objects[i].Ymu.getelem(1,0),objects[i].Xmu.getelem(1,0))};
    cars[temp].Smu.setelems(smuelem);
    cars[temp].Smu_hat.setelems(smuelem);
    double tempx = objects[i].Xmu.getelem(1,0);
    double tempy = objects[i].Ymu.getelem(1,0);
    double ssigmaelem[] = {.3,0,0,0,2,0,0,0,.1};//pow(1/(1+pow(tempy/tempx,2))*2*tempy/tempx/tempx*(objects[i].Ysigma.getelem(1,1)),2)+sqrt(pow(1/(1+pow(tempy/tempx,2))*-2*tempy*tempy/pow(tempx,3)*(objects[i].Xsigma.getelem(1,1)),2))};
    cars[temp].Ssigma.setelems(ssigmaelem);
    cars[temp].Ssigma_hat.setelems(ssigmaelem);
    //    MSG("Initial Theta  %f",cars[temp].Smu.getelem(2,0));
    int corner;
    corner = fitCar(points, UNDEFINED_CORNER, point2(0.0, 0.0), objects[i].seenStart, objects[i].seenEnd,&cars[temp], objects[i].Xmu.getelem(1,0), objects[i].Ymu.getelem(1,0), 0.0, 0.0); 
    cars[temp].seenCorner = corner;    

    //  fprintf(stderr, "fit car! corner: %f, %f \n", cars[temp].N, cars[temp].E);
    cars[temp].Xmu.resetSize(2,1);
    cars[temp].Ymu.resetSize(2,1);
    cars[temp].Xsigma.resetSize(2,2);
    cars[temp].Ysigma.resetSize(2,2);
    cars[temp].Xmu_hat.resetSize(2,1);
    cars[temp].Ymu_hat.resetSize(2,1);
    cars[temp].Xsigma_hat.resetSize(2,2);
    cars[temp].Ysigma_hat.resetSize(2,2);

    //TODO: do I want to carry over any more from the obj's KF?
    point2 currCorner = cars[temp].corners[cars[temp].trackedCorner];
    double XMelems[] = {currCorner.x, objects[i].Xmu.getelem(1,0)};
    double YMelems[] = {currCorner.y, objects[i].Ymu.getelem(1,0)};
    //    double XSelems[] = {0,0,0,0};
    //    double YSelems[] = {0,0,0,0};
    double XSelems[] = {objects[i].Xsigma.getelem(0,0), objects[i].Xsigma.getelem(0,1),objects[i].Xsigma.getelem(1,0), objects[i].Xsigma.getelem(1,1) };
    double YSelems[] = {objects[i].Ysigma.getelem(0,0), objects[i].Ysigma.getelem(0,1),objects[i].Ysigma.getelem(1,0), objects[i].Ysigma.getelem(1,1) };
    cars[temp].Xmu.setelems(XMelems);
    cars[temp].Xsigma.setelems(XSelems);
    cars[temp].Ymu.setelems(YMelems);
    cars[temp].Ysigma.setelems(YSelems);
      
    cars[temp].ID = numCars;
    cars[temp].mapID = mapIndices.back();
    mapIndices.pop_back();

    cars[temp].lastSeen = objects[i].lastSeen;  
    cars[temp].timesSeen = 1;
    cars[temp].dTrackedCorner = point2(0,0);
    
    cars[temp].lastScan = point2arr(objects[i].lastScan);
    cars[temp].lastScanDepth = point2arr(objects[i].lastScanDepth);

    cars[temp].history.clear();
    for(unsigned int j=0; j<objects[i].history.size(); j++) {
      cars[temp].history.push_back(objects[i].history[j]);
    }

    cars[temp].velocity = point2(0.0, 0.0);
    cars[temp].ptsinvelocity = 0;

    cars[temp].lastUpdated = DGCgettime();

    numCars++; //we've found a car
    
    removeObject(index);
      
  } else {
    cerr<<"not enough space in cars array!!, or mapIndices size = "<<mapIndices.size()<<endl;
  }  // end of checking space
}  // end of obj2car



/**
 * function: fitCar
 * input: takes in set of NE points that have been 
 *   grouped together as an object, and finds the two 
 *   lines that fit them best (think rectangle).
 *   Once these have been calculated, car is represented
 *   by the point corresponding to its back-right corner
 *   and by deltas corresponding to length and width
 *   The length/width of car are determined from its 
 *   direction of motion (input vX and vY)
 * output: parameters describing this rectangle
 *
 * Sets the following fields in carRepresentation:
 *   X, Y, dx1, dy1, dx2, dy2
 *   seenFront, seenBack
 *  
 **/
int LadarCarPerceptor::fitCar(point2arr points, carCorner currentCorner, point2 currentCornerCoords, int seenStart, int seenEnd, carRepresentation* newCar, double vX, double vY, double minLen, double minWid) {
  int corner = 0;
  point2 centerPt;
  centerPt = points.average();
  //    fprintf(stderr, "enterint fitcar w/ poitns centered at %f, %f and vel = %f, %f\n", 
  //	    centerPt.x, centerPt.y, vX, vY);
  int numPoints = points.size();
  double minerror, lineerror, MSE1, MSE2, MSE3, MSE4;
  int breakpoint;

  //current best fit
  lineSegmentFit seg1;
  lineSegmentFit seg2;

  //candidate fits
  lineSegmentFit tempSeg1;
  lineSegmentFit tempSeg2;
  lineSegmentFit tempSeg3;
  lineSegmentFit tempSeg4;

  //Calculating the fit for all points
  //somewhere, order of points gets swapped...
  //   swapping back here
  minerror = points.fit_line(seg1.a, seg1.b, seg1.pt2, seg1.pt1);
  lineerror = minerror;

  //now, find min error for corner, but requiring each 
  //leg to have at least 3 points, and each leg contains 
  //the breakpoint
  for(int i=3; i<numPoints-3; i++) {
    point2arr ptarr1, ptarr2;
    points.split_at(i, ptarr1, ptarr2);

    //again, order of pts in ptarr is reversed, so this
    // gives the fit with the convention of seg1 and 
    // seg2 being in scan-order (R to L)

    //fitting seg1 first, seg2 perpendicular
    MSE1 = ptarr2.fit_line(tempSeg1.a, tempSeg1.b, tempSeg1.pt2, tempSeg1.pt1);
    MSE2 = ptarr1.fit_line_perp(tempSeg2.a, tempSeg2.b, tempSeg2.pt2, tempSeg2.pt1, tempSeg1.b);

    //fitting seg2 first, seg1 perpendicular
    MSE3 = ptarr1.fit_line(tempSeg3.a, tempSeg3.b, tempSeg3.pt2, tempSeg3.pt1);
    MSE4 = ptarr2.fit_line_perp(tempSeg4.a, tempSeg4.b, tempSeg4.pt2, tempSeg4.pt1, tempSeg3.b);

    //FIXME: magic #
    if((minerror > MSE1 + MSE2)&&(lineerror > 2*(MSE1+MSE2)))
      {
	minerror = MSE1 + MSE2;
	breakpoint = i;
	corner = 1;

	seg1 = tempSeg1;
	seg2 = tempSeg2;
	//      fprintf(stderr, "line1 endpoints: pt1 = %f, %f. pt2 = %f, %f \n", tempSeg1.pt1.x, tempSeg1.pt1.y, tempSeg1.pt2.x, tempSeg1.pt2.y);
	//      fprintf(stderr, "line2 endpoints: pt1 = %f, %f. pt2 = %f, %f \n", tempSeg2.pt1.x, tempSeg2.pt1.y, tempSeg2.pt2.x, tempSeg2.pt2.y);
	//      fprintf(stderr, "new min error at breakpoint: %d \n", breakpoint);
	//      fprintf(stderr, "line fits are: seg1 %f, %f and seg2 %f, %f \n", seg1.a, seg1.b, seg2.a, seg2.b);
      }

    if((minerror > MSE3 + MSE4)&&(lineerror > 2*(MSE3+MSE4)))
      {
	minerror = MSE3 + MSE4;
	breakpoint = i;
	corner = 1;

	seg1 = tempSeg4;
	seg2 = tempSeg3;

      }

  } //end of finding best-fit line segments

    //if we didn't see a corner, add one
    //this was tested by turning off corner fitting, 
    // and checking that this gave right shape
  if(corner == 0)
    {

      double x,y; //new n,e pts 
      double thC; //angle from start to end of car
      double th;
      thC = atan2((seg1.pt1.x - seg1.pt2.x), \
		  (seg1.pt1.y - seg1.pt2.y));
      //since pt1, pt2 are in scan order, +pi/2 always 
      //points away from alice
      th = (M_PI/2) + thC; 

      y = seg1.pt2.y + MINDIMENSION * cos(th);
      x = seg1.pt2.x + MINDIMENSION * sin(th);

      seg2.a = 0;
      seg2.b = 0;
      seg2.pt1 = point2(seg1.pt2);
      seg2.pt2 = point2(x,y);

      //	fprintf(stderr, "fitCar didn't find a corner. adding seg from (%f, %f) to (%f, %f) \n", seg2.pt1.x, seg2.pt1.y, seg2.pt2.x, seg2.pt2.y);
    } else {
      //	fprintf(stderr, "fitCar found corner. error = %f \n", minerror);
    }


  //calculating the intersection of seg1 and seg2
  double xi, yi; //northing and easting intersections
  point2 d1, d2;

  if(corner == 0 )
    { //corner is end of seg1
      xi = seg1.pt2.x;
      yi = seg1.pt2.y;
    } else { //corner is intersect of the lines
      xi = (seg1.a - seg2.a)/(seg2.b - seg1.b);
      yi = seg1.a + xi*seg1.b;
      //    fprintf(stderr, "corner = %f %f \n", xi, yi);
    }
  point2 pti = point2(xi,yi);
  d1 = seg1.pt1 - pti; //seg 1 deltas
  d2 = seg2.pt2 - pti;


  /**
   * up until this point, seg1 and seg2 were in scan order,
   * with pt1 and pt2 also in scan order. Thus, the corner
   * is at pt2 of seg1 and pt1 of seg2
   **/

  double thv, th1, th2; //velocity and segment angles
  thv = atan2(vY, vX);
  th1 = atan2(d1.y, d1.x);
  th2 = atan2(d2.y, d2.x);
  int d1count = 0;

  point2 tempPt;
  point2 zeroPt = point2(0.0, 0.0);

  /**
   * Now, we wish to put the corner at the back right of
   * the car, with seg1 pointing along car's length, and 
   * seg2 along its width
   * see p. 85
   **/

  double velNorm = sqrt(vX*vX + vY*vY);
#warning "magic number"
  if((currentCorner != UNDEFINED_CORNER)&&(velNorm < 1.0)) {
    //find corner closest to currently tracked corner

    double d_br2, d_bl2, d_fr2, d_fl2;
    double mindist2;
      d_br2 = currentCornerCoords.dist(pti);
      d_bl2 = currentCornerCoords.dist(pti+d2);
      d_fr2 = currentCornerCoords.dist(pti+d1);
      d_fl2 = currentCornerCoords.dist(pti+d1+d2);

      /**
	 switch(currentCorner) {
	 case BACK_RIGHT:

	 break;
	 case FRONT_RIGHT:

	 break;
	 case BACK_LEFT:

	 break;
	 case FRONT_LEFT:

	 break;
	 default:
	 break;

	 }
      **/


      mindist2 = min(d_br2, min(d_bl2, min(d_fr2, d_fl2)));

      //TODO: need switch statement here, pass in tracked corner rather than BR corner

      if(mindist2 == d_br2) {
	//do nothing - back righ tcorner is already closes to corner passed in
	newCar->seenFront = seenStart;
	newCar->seenBack = seenEnd;
      } else if(mindist2 == d_bl2) {
	//need to switch br and bl
	pti = pti + d2;
	d2 = zeroPt-d2;
	newCar->seenFront = seenStart;
	newCar->seenBack = seenEnd;
      } else if(mindist2 == d_fr2) {
	//need to switch br and fr
	pti = pti + d1;
	d1 = zeroPt-d1;
	newCar->seenFront = seenEnd;
	newCar->seenBack = seenStart;
      } else if(mindist2 == d_fl2) {
	pti = pti+d1+d2;
	d1 = zeroPt-d1;
	d2 = zeroPt-d2;
	newCar->seenFront = seenEnd;
	newCar->seenBack = seenStart;
      } else { //this shouldn't happen!
	ERROR("no corner matched closest corner....");
      }


  } else {
    // first, putting xi,yi at back bumper

    //if d1 is parallel direction of velocity
    if( fabs(sin(thv-th1)) < fabs(sin(thv-th2))) { 
      //already have d1 as length, so check directions
      if(cos(thv-th1) > 0) { 
	//they're oriented the same, do nothing
      } else {
	//oriented opposite, so need to flip d1
	pti = pti + d1;
	d1 = zeroPt - d1;
	d1count++;
      }
    } else {
      // d2 is parallel to velocity...need to switch d1 and d2
      tempPt = d1;
      d1 = d2;
      d2 = tempPt;
      d1count++;

      if(cos(thv-th2) > 0) { //same orientation
	// xi,yi are fine, only needed to switch d1 and d2
      } else { //opp orientation
	//need to switch orientation as well
	pti = pti + d1;
	d1 = zeroPt - d1;
	d1count++;
      }
    }

    /**
     * Next, putting xi,yi at RIGHT side of back bumper 
     * (i.e. th2 and thv+PI/2 are in same direction)
     **/
    th2 = atan2(d2.y, d2.x); 
    // if they're in same direction
    if(cos(thv + M_PI/2 - th2) < 0) { 
      //everything is set up properly
    } else { //need to switch
      pti = pti + d2;
      d2 = zeroPt - d2;
    }

    //assigning seenFront,seenBack. see p.107 for explanation
    if(1 == d1count) {
      newCar->seenFront = seenEnd;
      newCar->seenBack = seenStart;
    } else {
      newCar->seenFront = seenStart;
      newCar->seenBack = seenEnd;
    }

  } // end checking if velocity large enough to reliably use  

    //    fprintf(stderr, "for this car, seenFront = %d, seenBack = %d \n",
    //	    newCar->seenFront, newCar->seenBack);

    //         fprintf(stderr, "car corner = %f, %f; length = %f, %f; width = %f, %f \n", 
    //    	     pti.x, pti.y, d1.x, d1.y, d2.x, d2.y);

    double d_br, d_bl, d_fr, d_fl;
    double mindist;
    d_br = centerPt.dist(pti);
    d_bl = centerPt.dist(pti+d2);
    d_fr = centerPt.dist(pti+d1);
    d_fl = centerPt.dist(pti+d1+d2);

    //    fprintf(stderr, "distances to corners: d_br = %f, d_bl = %f, d_fr = %f, d_fl = %f \n",
    //	    d_br, d_bl, d_fr, d_fl);

    //now, we worry about occlusions (don't want to track occluded side)
    if(1 != newCar->seenFront) {
      d_fr = 1000; //TODO: is there an actual inf value in c?
      d_fl = 1000;
      //      fprintf(stderr, "ln 1835, front occluded: ");
    }
    if(1 != newCar->seenBack) {
      d_bl = 1000;
      d_br = 1000;
      //      fprintf(stderr, "ln 1840, back occluded: ");
    }

    mindist = min(d_br, min(d_bl, min(d_fr, d_fl)));
    if((1 != newCar->seenFront) || (1 != newCar->seenBack)) {
      //      fprintf(stderr, " mindist = %f \n", mindist);
    }

    if(mindist == d_br) {
      newCar->trackedCorner = BACK_RIGHT;
    } else if(mindist == d_bl) {
      newCar->trackedCorner = BACK_LEFT;
    } else if(mindist == d_fr) {
      newCar->trackedCorner = FRONT_RIGHT;
    } else if(mindist == d_fl) {
      newCar->trackedCorner = FRONT_LEFT;
    } else { //this shouldn't happen!
      ERROR("no corner matched closest corner....");
    }

  // now, enforcing minimum dimensions.
  double lengthFactor;
  double widthFactor;
  lengthFactor = min(MAXCARLENGTH, max(d1.norm(), minLen))/d1.norm();
  widthFactor = min(MAXCARWIDTH, max(d2.norm(), minWid))/d2.norm();
  //    MSG("length factor = %f, width factor = %f \n", lengthFactor, widthFactor);

  switch (newCar->trackedCorner) {
  case BACK_RIGHT:
    newCar->corners[BACK_RIGHT] = pti;
    newCar->corners[FRONT_RIGHT] = pti + lengthFactor*d1;
    newCar->corners[BACK_LEFT] = pti + widthFactor*d2;
    newCar->corners[FRONT_LEFT] = pti + lengthFactor*d1 + widthFactor*d2;
    //      MSG("tracking back right corner");
    break;

  case BACK_LEFT:
    newCar->corners[BACK_LEFT] = pti + d2;
    newCar->corners[FRONT_LEFT] = pti + d2 + lengthFactor*d1;
    newCar->corners[BACK_RIGHT] = point2(pti + d2 - widthFactor*d2);
    newCar->corners[FRONT_RIGHT] = pti + d2 - widthFactor*d2 + lengthFactor*d1;
    //      MSG("tracking back left corner");
    break;

  case FRONT_RIGHT:
    newCar->corners[FRONT_RIGHT] = point2(pti + d1);
    newCar->corners[BACK_RIGHT] = point2(pti + d1 - lengthFactor*d1);
    newCar->corners[FRONT_LEFT] = pti + d1 + widthFactor*d2;
    newCar->corners[BACK_LEFT] = pti + d1 + widthFactor*d2 - lengthFactor*d1;
    //      MSG("tracking front right corner");
    break;

  case FRONT_LEFT:
    newCar->corners[FRONT_LEFT] = pti + d1 + d2;
    newCar->corners[BACK_LEFT] = pti + d1 + d2 - lengthFactor*d1;
    newCar->corners[FRONT_RIGHT] = pti + d1 + d2 - widthFactor*d2;
    newCar->corners[BACK_RIGHT] = point2(pti + d1 + d2 - widthFactor*d2 - lengthFactor*d1);
    //      MSG("tracking front left corner");
    break;

  default:
    ERROR("shouldn't ever reach this point");
  }

  //     fprintf(stderr, "car corner = %f, %f; \n", 
  //	     newCar->corners[BACK_RIGHT].x, newCar->corners[BACK_RIGHT].y);


  //WHEW. car is set up correctly
  newCar->length = newCar->corners[FRONT_RIGHT] - newCar->corners[BACK_RIGHT];
  newCar->width = newCar->corners[BACK_LEFT] - newCar->corners[BACK_RIGHT];

  double width = newCar->width.norm();
  double length = newCar->length.norm();
  // Use longer side (probably more accurate) to get theta measurement
  double theta;
  if(length>width){
    theta = atan2((newCar->length).y,(newCar->length).x);
  }else{
    theta = atan2((newCar->width).y,(newCar->width).x)-PI/2.0;
  }
  double theta2 = atan2(vY,vX);
  bool iterate = true; // whether angles need modification
  // want theta, theta2 within pi of Smu.getelem(2,0)
  while(iterate){
    if((newCar->Smu).getelem(2,0)-theta<-PI){
      theta-=2*PI;
    }else if((newCar->Smu).getelem(2,0)-theta>PI){
      theta+=2*PI;
    }else{
      iterate = false;
    }
  }
  while(iterate){
    if((newCar->Smu).getelem(2,0)-theta2<-PI){
      theta2-=2*PI;
    }else if((newCar->Smu).getelem(2,0)-theta2>PI){
      theta2+=2*PI;
    }else{
      iterate = false;
    }
  }
  Matrix Serr;
  Serr = Matrix(3,1);
  Matrix tmp3;
  tmp3 = Matrix(4,1);
  double temp3elem[] = {width, length, theta, theta2};
  tmp3.setelems(temp3elem);
  assert(tmp3.getsize() == 4);
  assert(SC.getsize() == 12);
  assert((newCar->Smu_hat).getsize() == 3);
  Serr = tmp3-SC*(newCar->Smu_hat);
  Matrix L;
  //  L = Matrix(4,4);
  //  SQ.setelem(3,3,5);//5.0/(sqrt(vX*vX+vY+vY)));// Try to eliminate low speed estimate
  L = (newCar->Ssigma_hat)*SC.transpose()*(SC*(newCar->Ssigma_hat)*SC.transpose()+SQ).inverse();
  newCar->Smu = (newCar->Smu_hat)+L*Serr;
  newCar->Ssigma = (SI-L*SC)*(newCar->Ssigma_hat);
  // want to limit theta from -pi to pi
  iterate = true;
  while(iterate){
    if((newCar->Smu).getelem(2,0)>PI){
      (newCar->Smu).setelem(2,0,(newCar->Smu).getelem(2,0)-2*PI);
    }else if((newCar->Smu).getelem(2,0)<-PI){
      (newCar->Smu).setelem(2,0,(newCar->Smu).getelem(2,0)+2*PI);
    }else{
      iterate = false;
    }
  }
  return corner;
} //end fitCar



  //removes object by putting its index back on openObjs, removing 
  //the index from usedObjs, and sending msg to the map to clear
  //corresponding map elements
void LadarCarPerceptor::removeObject(int index) {

  int bytesSent;
  MapElement el;
  int j = usedObjs.at(index); 
  el.clear();
  el.setTypeClear();

  mapIndices.push_back(objects[j].mapID);

  //  el.setId(mapCarID, moduleObjectID, objects[j].ID);
  el.setId(mapCarID, objects[j].mapID);
  el.state = rawState;
  bytesSent = sendMapElement(&el, outputSubgroup);

  if(useDisplay) {
    el.setId(mapCarID,moduleObjectID,objects[j].ID);
    bytesSent = sendMapElement(&el, debugSubgroup);

    el.setId(mapCarID,moduleEllipseID,objects[j].ID);
    bytesSent = sendMapElement(&el, debugSubgroup);

    el.setId(mapCarID,moduleHistoryID,objects[j].ID);
    bytesSent = sendMapElement(&el, debugSubgroup);

    el.setId(mapCarID,objectVelocityID,objects[j].ID);
    bytesSent = sendMapElement(&el, debugSubgroup);
  } 



  openObjs.push_back(usedObjs.at(index));
  objIterator = usedObjs.begin();
  objIterator+=index;
  usedObjs.erase(objIterator);


}
//removes car by putting it's index on openCars
void LadarCarPerceptor::removeCar(int index) {
  int bytesSent;
  MapElement el;
  int j = usedCars.at(index); 
  el.clear();

  mapIndices.push_back(cars[j].mapID);

  el.setId(mapCarID,cars[j].mapID);
  el.setTypeClear();
  bytesSent = sendMapElement(&el, outputSubgroup);

  if(useDisplay) {
    el.clear();
    el.setTypeClear();

    el.setId(mapCarID,moduleCarHistoryID,cars[j].ID);
    bytesSent = sendMapElement(&el, debugSubgroup);

    el.setId(mapCarID,carVelocityID,cars[j].ID);
    bytesSent = sendMapElement(&el, debugSubgroup);

    el.setId(mapCarID,carKFVelocityID,cars[j].ID);
    bytesSent = sendMapElement(&el, debugSubgroup);

    el.setId(mapCarID,predictedCarID,cars[j].ID);
    bytesSent = sendMapElement(&el, debugSubgroup);

    el.setId(mapCarID,moduleCarSeg1ID,cars[j].ID);
    bytesSent = sendMapElement(&el, debugSubgroup);

    el.setId(mapCarID,moduleCarSeg2ID,cars[j].ID);
    bytesSent = sendMapElement(&el, debugSubgroup);
  }

  openCars.push_back(usedCars.at(index));
  carIterator = usedCars.begin();
  carIterator+=index;
  usedCars.erase(carIterator);

}




void LadarCarPerceptor::KFupdate() {

  //  cout<<"entering KFupdate"<<endl;

  Matrix K1;
  K1 = Matrix(2,2);
  Matrix K2;
  K2 = Matrix(2,2);
  Matrix Xerr;
  Xerr = Matrix(1,1);
  Matrix Yerr;
  Yerr = Matrix(1,1);
  Matrix tmp;
  tmp = Matrix(1,1);
  Matrix tmp2;
  tmp2 = Matrix(1,1);
  for(unsigned int i=0; i<usedObjs.size(); i++) {
    int j = usedObjs.at(i);
    //    cout<<"last seen: "<<objects[j].lastSeen<<"  and curr frame: "<<numFrames<<endl;
    if(objects[j].timesSeen > 1) {
      if(objects[j].lastSeen == numFrames) {
	double grr[] = {objects[j].el.center.x};
	tmp.setelems(grr);
	//	cout<<"Northing: "<<objects[j].N<<" and, predicted northing: "<<objects[j].Xmu_hat.getelem(0,0)<<endl;
	Xerr = tmp - C*objects[j].Xmu_hat;
	//	cout<<"Xerror: "<<Xerr.getelem(0,0)<<' ';
	K1 = objects[j].Xsigma_hat * C.transpose() * (C*objects[j].Xsigma_hat*C.transpose() + Q).inverse();
	objects[j].Xmu = objects[j].Xmu_hat + K1 * Xerr;
	objects[j].Xsigma = (I - K1*C) * objects[j].Xsigma_hat;

	double grr2[] = {objects[j].el.center.y};
	tmp2.setelems(grr2);

	Yerr = tmp2 - C*objects[j].Ymu_hat;
	//	cout<<" and, Yerror: "<<Yerr.getelem(0,0)<<endl;
	K2 = objects[j].Ysigma_hat * C.transpose() * (C*objects[j].Ysigma_hat*C.transpose() + Q).inverse();
	objects[j].Ymu = objects[j].Ymu_hat + K2 * Yerr;
	objects[j].Ysigma = (I - K2*C) * objects[j].Ysigma_hat;
	//	cout<<"new vel (actually calculated) "<<objects[j].Xmu.getelem(1,0)<<' '<<objects[j].Ymu.getelem(1,0)<<endl;

      } else {

	objects[j].Xmu = objects[j].Xmu_hat;
	objects[j].Xsigma = objects[j].Xsigma_hat;
	objects[j].Ymu = objects[j].Ymu_hat;
	objects[j].Ysigma = objects[j].Ysigma_hat;

      }

    }
  }


  for(unsigned int i=0; i<usedCars.size(); i++) {
    //    cout<<"entering loop w/ i = "<<i<<endl;
    int j = usedCars.at(i);
    //    cout<<"last seen: "<<cars[j].lastSeen<<"  and curr frame: "<<numFrames<<endl;
    if(cars[j].timesSeen > 1) {
      if(cars[j].lastSeen == numFrames) {
	//shift tracked corner, if necessary
	if(0 != cars[j].dTrackedCorner.norm()) {
	  point2 shiftPt = cars[j].dTrackedCorner;
	  //	    double newMatrix[2];
	  double newXmu[] = {cars[j].Xmu.getelem(0,0) + shiftPt.x, cars[j].Xmu.getelem(1,0)};
	  cars[j].Xmu.setelems(newXmu);
	  double newYmu[] = {cars[j].Ymu.getelem(0,0) + shiftPt.y, cars[j].Ymu.getelem(1,0)};
	  cars[j].Ymu.setelems(newYmu);

	  double newXmuHat[] = {cars[j].Xmu_hat.getelem(0,0) + shiftPt.x, cars[j].Xmu_hat.getelem(1,0)};
	  cars[j].Xmu_hat.setelems(newXmuHat);
	  double newYmuHat[] = {cars[j].Ymu_hat.getelem(0,0) + shiftPt.y, cars[j].Ymu_hat.getelem(1,0)};
	  cars[j].Ymu_hat.setelems(newYmuHat);

	  //	    MSG("switched tracked corner in KF");
	}


	point2 currTrackedPoint = cars[j].corners[cars[j].trackedCorner];
	double grr[] = {currTrackedPoint.x};
	tmp.setelems(grr);

	Xerr = tmp - C*cars[j].Xmu_hat;
	K1 = cars[j].Xsigma_hat * C.transpose() * (C*cars[j].Xsigma_hat*C.transpose() + Q).inverse();
	cars[j].Xmu = cars[j].Xmu_hat + K1 * Xerr;
	cars[j].Xsigma = (I - K1*C) * cars[j].Xsigma_hat;

	double grr2[] = {currTrackedPoint.y};
	tmp2.setelems(grr2);

	Yerr = tmp2 - C*cars[j].Ymu_hat;
	K2 = cars[j].Ysigma_hat * C.transpose() * (C*cars[j].Ysigma_hat*C.transpose() + Q).inverse();
	cars[j].Ymu = cars[j].Ymu_hat + K2 * Yerr;
	cars[j].Ysigma = (I - K2*C) * cars[j].Ysigma_hat;
	stateupdatedcount++;
      } else {

	cars[j].Xmu = cars[j].Xmu_hat;
	cars[j].Xsigma = cars[j].Xsigma_hat;
	cars[j].Ymu = cars[j].Ymu_hat;
	cars[j].Ysigma = cars[j].Ysigma_hat;
      }
    }
  }

  //  cout<<"exiting KFupdate"<<endl;
}
 
void LadarCarPerceptor::KFpredict(double dt) {


  A.resetSize(2,2);
  double Aelems[] = {1,dt,0,1};
  A.setelems(Aelems);

  //  cout<<"entering KFpredict()"<<endl;

  for(unsigned int i=0; i<usedObjs.size() ; i++) {
    int j = usedObjs.at(i);
    if(objects[j].timesSeen > 1) {
      objects[j].Xmu_hat =    A*objects[j].Xmu;
      objects[j].Xsigma_hat = A*objects[j].Xsigma*A.transpose() + R;
      objects[j].Ymu_hat =    A*objects[j].Ymu;
      objects[j].Ysigma_hat = A*objects[j].Ysigma*A.transpose() + R; 
    }
  } //end cycling through vector of objects


    //and, objcars

  for(unsigned int i=0; i<usedCars.size(); i++) {
    int j = usedCars.at(i);
      //    cout<<"ln 2300"<<endl;
      cars[j].Xmu_hat =    A*cars[j].Xmu;
      cars[j].Xsigma_hat = A*cars[j].Xsigma*A.transpose() + R;
      //    cout<<"ln2303"<<endl;
      cars[j].Ymu_hat =    A*cars[j].Ymu;
      cars[j].Ysigma_hat = A*cars[j].Ysigma*A.transpose() + R;
      cars[j].Smu_hat = SA*cars[j].Smu;
      cars[j].Ssigma_hat = SA*cars[j].Ssigma*SA.transpose() + SR;
  } //end cycling through numCars

} //end KFpredict()


void LadarCarPerceptor::cleanObjects()
{
  
  int j, m; 
  int diff;
  for(unsigned int i=0; i<usedObjs.size(); i++)
    {
      //checking if obj should be removed based on time
      j = usedObjs.at(i);
      diff = numFrames - objects[j].lastSeen;
      //    fprintf(stderr, "current frame: %d, last seen: %d \n", numFrames, objects[j].lastSeen);
      if(diff > OBJECTTIMEOUT) {
	//      fprintf(stderr, "tryng to remove object # %d! \n", j);
	removeObject(i);
      }

      //checking if two objects should be merged
      for(unsigned int k=0; k<usedObjs.size(); k++)
	{
	  m = usedObjs.at(k);
	  int overlap = objects[j].el.overlap(&objects[m].el, MARGIN);
	  //if they overlap and aren't the same object
	  if((1 == overlap) && (j!=m)) {
	    //	  fprintf(stderr, "objs %d and %d overlap! merging ... \n",objects[j].ID, objects[m].ID);
	    //	  fprintf(stderr, "center and params for obj 1: %f, %f, %f, %f, %f \n", objects[j].el.center.x, objects[j].el.center.y, objects[j].el.a, objects[j].el.b, objects[j].el.theta);
	    //	  fprintf(stderr, "center and params for obj 2: %f, %f, %f, %f, %f \n", objects[m].el.center.x, objects[m].el.center.y, objects[m].el.a, objects[m].el.b, objects[m].el.theta);
	    //TODO: this doesn't handle object histories intelligently

	    mergeObjects(&objects[j], &objects[m]);
	    removeObject(k);

	  }
	}

    }

}

void LadarCarPerceptor::mergeObjects(objectRepresentation* oldObj, objectRepresentation* newObj)
{
  //TODO: 
  //doesn't update startPoint/endPoint -> not currently a problem, as nothing uses these vars
  //push back most recent scan -- better way to do this??
  //do anything about the KF??

  //merge the ellipses
  oldObj->el.merge(newObj->el);

  //update timesseen, and lastseeen
  oldObj->timesSeen = max(oldObj->timesSeen, newObj->timesSeen);
  oldObj->lastSeen = max(oldObj->lastSeen, newObj->lastSeen);

}

//TODO: add a free-space condition for removing cars
void LadarCarPerceptor::cleanCars()
{
  int j; 
  int diff;
  for(unsigned int i=0; i<usedCars.size(); i++)
    {
      j = usedCars.at(i);
      diff = numFrames - cars[j].lastSeen;
      //    fprintf(stderr, "current frame: %d, last seen: %d \n", numFrames, objects[j].lastSeen);
      if(diff > CARTIMEOUT) {
	//      fprintf(stderr, "tryng to remove car # %d! \n", j);
	removeCar(i);
      }
    }

}


/**
 * this function is called when it is determined that a 
 * new object matches an already seen car.
 * see p. 109 for (some of) the logic
 *
 * Sets the following fields in carRepresentation: 
 *   velocity, ptsinvelocity
 *   lastSeen, timesSeen, history
 **/

void LadarCarPerceptor::updateCar(carRepresentation* Car, point2arr points, point2arr depthpoints, double dStart, double dEnd) {

  Car->lastUpdated = DGCgettime();

  point2 dCar;

  int corner;
  //don't allow car to shrink...call fitcar w/ min dimensions
  double minLen, minWid;
  minLen = Car->length.norm();
  minWid = Car->width.norm();
  carRepresentation newCar;

  int seenStart = int(dStart/fabs(dStart));
  int seenEnd = int(dEnd/fabs(dEnd));
  //            corner = fitCar(points, seenStart, seenEnd, &newCar, Car->Xmu.getelem(1,0), Car->Ymu.getelem(1,0), 0.0, 0.0);
  //FIXME: once fitcar works better, should pass in tracked corner, not back right
  // Need to copy over the shape kalman filter matrices S*, so fitCar can update
  // them in the new car represenation
  newCar.Smu = Car->Smu;
  newCar.Smu_hat = Car->Smu_hat;
  newCar.Ssigma = Car->Ssigma;
  newCar.Ssigma_hat = Car->Ssigma_hat;
  corner = fitCar(points, Car->trackedCorner, Car->corners[BACK_RIGHT], seenStart, seenEnd, &newCar, Car->Xmu.getelem(1,0), Car->Ymu.getelem(1,0), minLen, minWid);
  newCar.seenCorner = corner;

  //TODO: why not add KF for length/width ....?

  dCar = newCar.corners[BACK_RIGHT] - Car->corners[BACK_RIGHT];

  Car->lastScan = points;
  Car->lastScanDepth = depthpoints;

  //for KF -> if we're changing which corner to track, we need to shift
  // X and Xmu by their relationship on the previous car, before updating
  // with corner of new car observation
  Car->dTrackedCorner = Car->corners[newCar.trackedCorner] - Car->corners[Car->trackedCorner];
  Car->trackedCorner = newCar.trackedCorner;
  Car->corners[0] = newCar.corners[0];
  Car->corners[1] = newCar.corners[1];
  Car->corners[2] = newCar.corners[2];
  Car->corners[3] = newCar.corners[3];

  Car->Smu = newCar.Smu;
  Car->Ssigma = newCar.Ssigma;

  Car->length = newCar.length;
  Car->width = newCar.width;

  Car->seenFront = max(Car->seenFront, newCar.seenFront);
  Car->seenBack = max(Car->seenBack, newCar.seenBack);
  Car->seenCorner = max(Car->seenCorner, newCar.seenCorner);

  point2 tempvel = Car->ptsinvelocity*Car->velocity + dCar;
  Car->ptsinvelocity++;
  Car->velocity = tempvel/Car->ptsinvelocity;

  //     fprintf(stderr, "deltas = %f, %f \n", dCar.x, dCar.y);
  //     fprintf(stderr, "car corner = %f, %f; length = %f, %f; width = %f, %f \n", 
  //	     Car->corners[BACK_RIGHT].x, Car->corners[BACK_RIGHT].y, Car->length.x, Car->length.y,
  //	     Car->width.x, Car->width.y);

  Car->lastSeen = numFrames;  
  Car->timesSeen = Car->timesSeen + 1;
  Car->history.push_back(Car->corners[BACK_RIGHT]);

  //check if car is moving, update appropriate fields in struct. for now, 
  //"stationary" is defined to be a constant number.
  //TODO: should stationary be defined as some uncertainty about zero?
  double currentvel = sqrt(pow(Car->Xmu.getelem(1,0),2) + pow(Car->Ymu.getelem(1,0),2));
  if(STATIONARYVELOCITY > currentvel) {
    Car->isStopped = true;
  } else {
    Car->isStopped = false;
    Car->lastTimeMoving = DGCgettime();
  }

} //end updateCar



/**
 * this function is called when it is determined that a 
 * new object matches an already seen car.
 * see p. 109 for (some of) the logic
 *
 * Uses the sensor source information in sources to prioritize data points
 *
 * Sets the following fields in carRepresentation: 
 *   velocity, ptsinvelocity
 *   lastSeen, timesSeen, history
 **/

void LadarCarPerceptor::updateCar(carRepresentation* Car, point2arr points, point2arr depthpoints, vector<short> sources, double dStart, double dEnd) {

  Car->lastUpdated = DGCgettime();

  point2 dCar;

  // Sort the data points
 // if necessary, filter the points by source sensor
  if(sortBySensor){
    int bumperPts = 0;
    int roofPts = 0;
    int stereoPts = 0;
    for(int w = 0; w < sources.size(); w++){
      if(sources.at(w) == BUMPER){
	bumperPts++;
      }else if(sources.at(w) == ROOF){
	roofPts++;
      }else{
	stereoPts++;
      }
    }
    int thresh = 0;
    if(bumperPts > MINNUMPOINTS){
      thresh = BUMPER;
    }else if(bumperPts + roofPts > MINNUMPOINTS){
      thresh = ROOF;
    }else{
      thresh = STEREO;
    }
    point2arr sortpts;
    for(int w = 0; w < sources.size(); w++){
      if(sources.at(w) <=thresh){
	sortpts.push_back(points[w]);
      }
    }
    points = sortpts;
  }
  
  int corner;
  //don't allow car to shrink...call fitcar w/ min dimensions
  double minLen, minWid;
  minLen = Car->length.norm();
  minWid = Car->width.norm();
  carRepresentation newCar;

  int seenStart = int(dStart/fabs(dStart));
  int seenEnd = int(dEnd/fabs(dEnd));
  //            corner = fitCar(points, seenStart, seenEnd, &newCar, Car->Xmu.getelem(1,0), Car->Ymu.getelem(1,0), 0.0, 0.0);
  //FIXME: once fitcar works better, should pass in tracked corner, not back right
  // Need to copy over the shape kalman filter matrices S*, so fitCar can update
  // them in the new car represenation
  newCar.Smu = Car->Smu;
  newCar.Smu_hat = Car->Smu_hat;
  newCar.Ssigma = Car->Ssigma;
  newCar.Ssigma_hat = Car->Ssigma_hat;
  corner = fitCar(points, Car->trackedCorner, Car->corners[BACK_RIGHT], seenStart, seenEnd, &newCar, Car->Xmu.getelem(1,0), Car->Ymu.getelem(1,0), minLen, minWid);
  newCar.seenCorner = corner;

  //TODO: why not add KF for length/width ....?

  dCar = newCar.corners[BACK_RIGHT] - Car->corners[BACK_RIGHT];

  Car->lastScan = points;
  Car->lastScanDepth = depthpoints;

  //for KF -> if we're changing which corner to track, we need to shift
  // X and Xmu by their relationship on the previous car, before updating
  // with corner of new car observation
  Car->dTrackedCorner = Car->corners[newCar.trackedCorner] - Car->corners[Car->trackedCorner];
  Car->trackedCorner = newCar.trackedCorner;
  Car->corners[0] = newCar.corners[0];
  Car->corners[1] = newCar.corners[1];
  Car->corners[2] = newCar.corners[2];
  Car->corners[3] = newCar.corners[3];

  Car->Smu = newCar.Smu;
  Car->Ssigma = newCar.Ssigma;

  Car->length = newCar.length;
  Car->width = newCar.width;

  Car->seenFront = max(Car->seenFront, newCar.seenFront);
  Car->seenBack = max(Car->seenBack, newCar.seenBack);
  Car->seenCorner = max(Car->seenCorner, newCar.seenCorner);

  point2 tempvel = Car->ptsinvelocity*Car->velocity + dCar;
  Car->ptsinvelocity++;
  Car->velocity = tempvel/Car->ptsinvelocity;

  //     fprintf(stderr, "deltas = %f, %f \n", dCar.x, dCar.y);
  //     fprintf(stderr, "car corner = %f, %f; length = %f, %f; width = %f, %f \n", 
  //	     Car->corners[BACK_RIGHT].x, Car->corners[BACK_RIGHT].y, Car->length.x, Car->length.y,
  //	     Car->width.x, Car->width.y);

  Car->lastSeen = numFrames;  
  Car->timesSeen = Car->timesSeen + 1;
  Car->history.push_back(Car->corners[BACK_RIGHT]);

  //check if car is moving, update appropriate fields in struct. for now, 
  //"stationary" is defined to be a constant number.
  //TODO: should stationary be defined as some uncertainty about zero?
  double currentvel = sqrt(pow(Car->Xmu.getelem(1,0),2) + pow(Car->Ymu.getelem(1,0),2));
  if(STATIONARYVELOCITY > currentvel) {
    Car->isStopped = true;
  } else {
    Car->isStopped = false;
    Car->lastTimeMoving = DGCgettime();
  }

} //end updateCar



  /**
   * collecting all the sendMapElement function in one place
   * 
   * for type object, there are 3 cases:
   *   object already classified as car -> was sent as type car (in send cars)
   *   object seen fewer than X times -> send as type car
   *   object seen greater than X times -> send as type obj
   *
   * all tracked cars are sent as type vehicle (duh)
   *
   * TODO: move sending untracked points here. 
   **/
void  LadarCarPerceptor::sendToMap() 
{

  MapElement el;
  point2_uncertain tmppt;
  //  vector <point2> objectPoints;
  point2arr_uncertain objectPoints;
  int bytesSent;


  // *************sending tracked objects*****************
  for(unsigned int k=0; k<usedObjs.size(); k++) {
    int j = usedObjs.at(k);
    if(objects[j].lastUpdated > lastSentCars) {
      el.clear();
      objectPoints.clear();
      point2arr lastScan = objects[j].lastScanDepth;
      point2arr tmparr = objects[j].lastScan;
      tmparr.reverse();
      lastScan.connect(tmparr);

#warning "magic # - add better function here"
      objectPoints = point2arr(lastScan.simplify(.2));

      /**
      for(unsigned int m=0; m < lastScan.size(); m++) {
	tmppt = point2(lastScan.arr[m].x, lastScan.arr[m].y, lastScan.arr[m].z);
	objectPoints.push_back(tmppt);
      }
      **/

      el.setId(mapCarID,objects[j].mapID);
      //if we've tracked an object w/o observing motion, can
      //call it stationary. otherwise, planner wants type vehicle
      //TODO: more sophisticated check?
#warning "magic #"
      if(200 <= objects[j].timesSeen) {
	el.setTypeObstacle();
	el.plotColor = MAP_COLOR_YELLOW;
      } else {
	el.setTypeLadarObstacle();
	el.plotColor = MAP_COLOR_GREEN;
      }


      el.setGeometry(objectPoints);
      double xSigma = objects[j].Xsigma.getelem(0,0);
      double ySigma = objects[j].Ysigma.getelem(0,0);
      for(int w = 0; w < objectPoints.size(); w++){
	if(fabs(xSigma)>fabs(ySigma)){
	  objectPoints.arr.at(w).max_var = xSigma;
	  objectPoints.arr.at(w).min_var = ySigma;
	  objectPoints.arr.at(w).axis = 0;
	}else{
	  objectPoints.arr.at(w).max_var = ySigma;
	  objectPoints.arr.at(w).min_var = xSigma;
	  objectPoints.arr.at(w).axis = PI/2;
	}
      }



      double velocityUncertainty = sqrt(sqrt(objects[j].Xsigma.getelem(1,1)) + sqrt(objects[j].Ysigma.getelem(1,1)));
      el.velocity = point2_uncertain(objects[j].Xmu.getelem(1,0), objects[j].Ymu.getelem(1,0), velocityUncertainty);

      el.state = rawState;
      if(objects[j].isStopped) {
	DGCgettime(currTime);
	el.timeStopped = (currTime - objects[j].lastTimeMoving)/1000000;
      } else {
	el.timeStopped = 0.0;
      }


      //TODO: this is totally arbitrary, just wanting to give some idea to the map
      el.conf = min(1.0, objects[j].timesSeen/100);

      //    MSG("zcoord = %f", el.geometry[0].z);
      bytesSent = sendMapElement(&el, outputSubgroup);
    } else {//end checking if object is recent
      //      MSG("not sending object %d, not recently updated", j);
    }
  } // end checking each object


  // ***************** sending cars***************************
  double corners[8];
  for(unsigned int k=0; k<usedCars.size(); k++) {
    int j = usedCars.at(k);
    if(cars[j].lastUpdated > lastSentCars) {
      // send the raw scan as the data
      objectPoints.clear();
      el.clear();
      if(!useRect){
	point2arr lastScan = cars[j].lastScanDepth;
	point2arr tmparr = cars[j].lastScan;
	tmparr.reverse();
	lastScan.connect(tmparr);
	
	
#warning "magic # - add better function here"
	objectPoints = point2arr(lastScan.simplify(.2));
	for(unsigned int m=0; m < lastScan.size(); m++) {
	  tmppt = point2(lastScan.arr[m].x, lastScan.arr[m].y, lastScan.arr[m].z);
	  objectPoints.push_back(tmppt);
	}
      }else{
	// Send a fitted rectangle
	/*
	  corners[0] = cars[j].corners[BACK_RIGHT].x;
	  corners[1] = cars[j].corners[BACK_RIGHT].y;
	  corners[2] = cars[j].corners[FRONT_RIGHT].x;
	  corners[3] = cars[j].corners[FRONT_RIGHT].y;
	  corners[4] = cars[j].corners[FRONT_LEFT].x;
	  corners[5] = cars[j].corners[FRONT_LEFT].y;
	  corners[6] = cars[j].corners[BACK_LEFT].x;
	  corners[7] = cars[j].corners[BACK_LEFT].y;
	*/
	// Use estimated position to get display
	//fill in the corners
	//Xmu follows the tracked corner
	double tempx = cars[j].Xmu.getelem(0,0);
	double tempy = cars[j].Ymu.getelem(0,0);
	switch (cars[j].trackedCorner) {
	case BACK_RIGHT:
	  corners[0] = tempx;
	  corners[1] = tempy;
	  corners[2] = tempx+cars[j].length.x;
	    corners[3] = tempy+cars[j].length.y;
	    corners[4] = tempx+cars[j].length.x+cars[j].width.x;
	    corners[5]=tempy+cars[j].length.y+cars[j].width.y;
	    corners[6] = tempx+cars[j].width.x;
	    corners[7] = tempy+cars[j].width.y;
	    /*
	  corners[2] = tempx + cars[j].Smu.getelem(1,0)*cos(cars[j].Smu.getelem(2,0));
	  corners[3] = tempy+cars[j].Smu.getelem(1,0)*sin(cars[j].Smu.getelem(2,0));
	  corners[4] = tempx+cars[j].Smu.getelem(1,0)*cos(cars[j].Smu.getelem(2,0))+cars[j].Smu.getelem(0,0)*cos(cars[j].Smu.getelem(2,0)+PI/2);
	  corners[5] = tempy+cars[j].Smu.getelem(1,0)*sin(cars[j].Smu.getelem(2,0))+cars[j].Smu.getelem(0,0)*sin(cars[j].Smu.getelem(2,0)+1.5707);
	  corners[6] = tempx + cars[j].Smu.getelem(0,0)*cos(cars[j].Smu.getelem(2,0)+PI/2);
	  corners[7] = tempy + cars[j].Smu.getelem(0,0)*sin(cars[j].Smu.getelem(2,0)+PI/2);
	  debugingfile << "br " << tempx << " " << tempy  << " " << atan2(cars[j].length.y,cars[j].length.x) << endl;
*/
	  break;
	  
	case BACK_LEFT:
	  // Don't care about order, but need to reverse the width terms
	  corners[6] = tempx;
	  corners[7] = tempy;
	  corners[4] = tempx+cars[j].length.x;
	    corners[5] = tempy+cars[j].length.y;
	    corners[2] = tempx+cars[j].length.x-cars[j].width.x;
	    corners[3]=tempy+cars[j].length.y-cars[j].width.y;
	    corners[0] = tempx-cars[j].width.x;
	    corners[1] = tempy-cars[j].width.y;
	  /*
	  corners[4] = tempx + cars[j].Smu.getelem(1,0)*cos(cars[j].Smu.getelem(2,0));
	  corners[5] = tempy+cars[j].Smu.getelem(1,0)*sin(cars[j].Smu.getelem(2,0));
	  corners[2] = tempx + cars[j].Smu.getelem(1,0)*cos(cars[j].Smu.getelem(2,0))-cars[j].Smu.getelem(0,0)*cos(cars[j].Smu.getelem(2,0)+PI/2);
	  corners[3] = tempy+cars[j].Smu.getelem(1,0)*sin(cars[j].Smu.getelem(2,0))-cars[j].Smu.getelem(0,0)*sin(cars[j].Smu.getelem(2,0)+1.5707);
	  corners[0] = tempx - cars[j].Smu.getelem(0,0)*cos(cars[j].Smu.getelem(2,0)+PI/2);
	  corners[1] = tempy - cars[j].Smu.getelem(0,0)*sin(cars[j].Smu.getelem(2,0)+PI/2);
	  debugingfile << "bl " << tempx << " " << tempy << " " << atan2(cars[j].length.y,cars[j].length.x) << endl;
	  */
	  break;
	  
	case FRONT_RIGHT:
	  // need to reverse the length terms
	  corners[2] = tempx;
	  corners[3] = tempy;
	  corners[0] = tempx-cars[j].length.x;
	    corners[1] = tempy-cars[j].length.y;
	    corners[6] = tempx-cars[j].length.x+cars[j].width.x;
	    corners[7]=tempy-cars[j].length.y+cars[j].width.y;
	    corners[4] = tempx+cars[j].width.x;
	    corners[5] = tempy+cars[j].width.y;
	    /*
	  corners[0] = tempx - cars[j].Smu.getelem(1,0)*cos(cars[j].Smu.getelem(2,0));
	  corners[1] = tempy - cars[j].Smu.getelem(1,0)*sin(cars[j].Smu.getelem(2,0));
	  corners[6] = tempx-cars[j].Smu.getelem(1,0)*cos(cars[j].Smu.getelem(2,0))+cars[j].Smu.getelem(0,0)*cos(cars[j].Smu.getelem(2,0)+PI/2);
	  corners[7] = tempy - cars[j].Smu.getelem(1,0)*sin(cars[j].Smu.getelem(2,0))+cars[j].Smu.getelem(0,0)*sin(cars[j].Smu.getelem(2,0)+1.5707);
	  corners[4] = tempx + cars[j].Smu.getelem(0,0)*cos(cars[j].Smu.getelem(2,0)+PI/2);
	  corners[5] = tempy + cars[j].Smu.getelem(0,0)*sin(cars[j].Smu.getelem(2,0)+PI/2);
	  debugingfile << "fr " << tempx << " " << tempy  << " " << atan2(cars[j].length.y,cars[j].length.x) << endl;
	  */  
	  break;
	  
	case FRONT_LEFT:
	  // need to reverse all d terms
	  corners[4] = tempx;
	  corners[5] = tempy;
	  corners[6] = tempx-cars[j].length.x;
		corners[7] = tempy-cars[j].length.y;
		corners[0] = tempx-cars[j].length.x-cars[j].width.x;
		corners[1]=tempy-cars[j].length.y-cars[j].width.y;
		corners[2] = tempx-cars[j].width.x;
		corners[3] = tempy-cars[j].width.y;
		/*
	  corners[6] = tempx - cars[j].Smu.getelem(1,0)*cos(cars[j].Smu.getelem(2,0));
	  corners[7] = tempy - cars[j].Smu.getelem(1,0)*sin(cars[j].Smu.getelem(2,0));
	  corners[0] = tempx - cars[j].Smu.getelem(1,0)*cos(cars[j].Smu.getelem(2,0))-cars[j].Smu.getelem(0,0)*cos(cars[j].Smu.getelem(2,0)+PI/2);
	  corners[1] = tempy - cars[j].Smu.getelem(1,0)*sin(cars[j].Smu.getelem(2,0))-cars[j].Smu.getelem(0,0)*sin(cars[j].Smu.getelem(2,0)+1.5707);
	  corners[2] = tempx - cars[j].Smu.getelem(0,0)*cos(cars[j].Smu.getelem(2,0)+PI/2);
	  corners[3] = tempy - cars[j].Smu.getelem(0,0)*sin(cars[j].Smu.getelem(2,0)+PI/2);
	  debugingfile << "fl " << tempx << " " << tempy << " " << atan2(cars[j].length.y,cars[j].length.x) << endl;
	  */
	  break;
	  
	default:
	  ERROR("shouldn't ever reach this point");
	}
	/*	
	corners[0] = cars[j].corners[BACK_RIGHT].x;
	corners[1] = cars[j].corners[BACK_RIGHT].y;
	corners[2] = cars[j].corners[FRONT_RIGHT].x;
	corners[3] = cars[j].corners[FRONT_RIGHT].y;
	corners[4] = cars[j].corners[FRONT_LEFT].x;
	corners[5] = cars[j].corners[FRONT_LEFT].y;
	corners[6] = cars[j].corners[BACK_LEFT].x;
	corners[7] = cars[j].corners[BACK_LEFT].y;
*/	
	//      debugingfile << cars[j].Smu.getelem(2,0) << " " << atan2(cars[j].length.y,cars[j].length.x) << " " << atan2(cars[j].Ymu.getelem(1,0),cars[j].Xmu.getelem(1,0)) << endl;
	tmppt = point2(corners[0], corners[1]);
	objectPoints.push_back(tmppt);
	tmppt = point2(corners[2], corners[3]);
	objectPoints.push_back(tmppt);
	tmppt = point2(corners[4], corners[5]);
	objectPoints.push_back(tmppt);
	tmppt = point2(corners[6], corners[7]);
	objectPoints.push_back(tmppt);
      }

      //set the originating module and blob ID numbers

      double xSigma = cars[j].Xsigma.getelem(0,0);
      double ySigma = cars[j].Ysigma.getelem(0,0);
      for(int w = 0; w < objectPoints.size(); w++){
	if(fabs(xSigma)>fabs(ySigma)){
	  objectPoints.arr.at(w).max_var = xSigma;
	  objectPoints.arr.at(w).min_var = ySigma;
	  objectPoints.arr.at(w).axis = 0;
	}else{
	  objectPoints.arr.at(w).max_var = ySigma;
	  objectPoints.arr.at(w).min_var = xSigma;
	  objectPoints.arr.at(w).axis = PI/2;
	}
      }

      el.setId(mapCarID,cars[j].mapID);
      el.setGeometry(objectPoints);
      el.setTypeVehicle();
      el.height=2;
      el.plotColor = MAP_COLOR_GREY;

      if(cars[j].isStopped) {
	DGCgettime(currTime);
	el.timeStopped = (currTime - cars[j].lastTimeMoving)/100000;
      } else {
	el.timeStopped = 0.0;
      }

      //TODO: this is totally arbitrary, just wanting to give some idea to the map
      el.conf = min(1.0, cars[j].timesSeen/100);

      //Xsigma is the KF variances for X position. the 1,1 element is the velocity's squared variance
      //TODO: check units
      double velocityUncertainty = sqrt(sqrt(cars[j].Xsigma.getelem(1,1)) + sqrt(cars[j].Ysigma.getelem(1,1)));
      el.velocity = point2_uncertain(cars[j].Xmu.getelem(1,0), cars[j].Ymu.getelem(1,0), velocityUncertainty);

      //TODO: Add state (something about map centering itself?)
      el.state = rawState;
    
      bytesSent = sendMapElement(&el, outputSubgroup);
    } //end checking for recent data
  }// end cycling through cars


  //possibly useful code for sending non-car pts
  /**

  int startIndex, endIndex;

  for(int i=0; i<numNewObjects; i++) {
  if((0 == matchedCar[i]) && (0 == matchedObj[i])) {

  objectPoints.clear();
  startIndex = newObjects[i].startIndex;
  endIndex = newObjects[i].endIndex;

  for(int j=startIndex; j <= endIndex; j++) {
  double temprange = rawData[j][RANGE];
  if((MAXRANGE > temprange) && (MINRANGE < temprange)) {
  tmppt = point2(xyzData[j][0],xyzData[j][1]);
  objectPoints.push_back(tmppt);
  }
  }
  for(int j = endIndex; j>= startIndex; j--) {
  double temprange = rawData[j][RANGE];
  if((MAXRANGE > temprange) && (MINRANGE < temprange)) {
  tmppt = point2(depthData[j][0],depthData[j][1]);
  objectPoints.push_back(tmppt);
  }
  }

  el.setId(mapCarID,currSensor,numSent);
  el.setTypeObstacle();
  el.setGeometry(objectPoints);
  //       fprintf(stderr, "current sensor = %d \n", currSensor);
  if(currSensor == 0) {
  el.plotColor = MAP_COLOR_GREEN;
  } else if(currSensor == 1) {
  el.plotColor = MAP_COLOR_BLUE;
  } else if(currSensor == 2) {
  el.plotColor = MAP_COLOR_CYAN;
  } else {
  el.plotColor = MAP_COLOR_GREY;
  }
  el.state = rawState;
  bytesSent = sendMapElement(&el, debugSubgroup);
  numSent++;
  }
  }
  **/



  return;
}











void LadarCarPerceptor::sendDebug() 
{
  MapElement el;
  point2 tmppt;
  vector <point2> objectPoints;
  int bytesSent;

  //********* sending raw scan *************
  objectPoints.clear();
  for(int i=0; i<NUMSCANPOINTS; i++) {
    objectPoints.push_back(point2(xyzData[i][0],xyzData[i][1]));
  }
  el.clear();
  el.setId(mapCarID,modulePointsID,1);
  el.setTypePoints();
  el.setGeometry(objectPoints);
  if(currSensor == 0) {
    el.plotColor = MAP_COLOR_GREEN;
  } else if(currSensor == 1) {
    el.plotColor = MAP_COLOR_BLUE;
  } else if(currSensor == 2) {
    el.plotColor = MAP_COLOR_CYAN;
  } else {
    el.plotColor = MAP_COLOR_PINK;
  }
  el.state = rawState;

  bytesSent = sendMapElement(&el, debugSubgroup);

  //for debugging segmentation
  double x1[numSegments];
  double x2[numSegments];
  double y1[numSegments];
  double y2[numSegments];
  double x3[numSegments];
  double y3[numSegments];
  int tmpindex;

  vector <point2> segmentStartPoints, segmentEndPoints;
  segmentStartPoints.clear();
  segmentEndPoints.clear();

  for(int i=0; i<numSegments;i++) {

    x1[i]=ladarState.X;
    y1[i]=ladarState.Y;
    tmpindex = posSegments[i][0];
    x2[i]=xyzData[tmpindex][0];
    y2[i]=xyzData[tmpindex][1];
    segmentStartPoints.push_back(point2(x1[i],y1[i]));
    segmentStartPoints.push_back(point2(x2[i],y2[i]));
    tmpindex = posSegments[i][1];
    x3[i]=xyzData[tmpindex][0];
    y3[i]=xyzData[tmpindex][1];
    segmentEndPoints.push_back(point2(x1[i],y1[i]));
    segmentEndPoints.push_back(point2(x3[i],y3[i]));
  }


  el.clear();

  el.setTypeObstacle();
  el.height=2;
  el.setId(mapCarID,moduleSegmentsID);
  el.setGeometry(segmentStartPoints);
  el.plotColor = MAP_COLOR_BLUE;
  el.height=2;

  //    bytesSent = sendMapElement(&el, debugSubgroup);

  el.clear();

  el.setTypeObstacle();
  el.height=2;
  el.setId(mapCarID,moduleSegmentsID2);
  el.setGeometry(segmentEndPoints);
  el.plotColor = MAP_COLOR_CYAN;
  el.height=2;
  //    bytesSent = sendMapElement(&el, debugSubgroup);




  // ********cycling through cars **********
  double KFvelX, KFvelY;
  point2 cornerPt, width, length;
  double tempX, tempY;
  for(unsigned int k = 0; k < usedCars.size(); k++) { 
    int j = usedCars.at(k);

    //sending predicted position
    width = cars[j].width;
    length = cars[j].length;
    tempX = cars[j].Xmu.getelem(0,0);
    tempY = cars[j].Ymu.getelem(0,0);
    tmppt = point2(tempX, tempY);
    switch(cars[j].trackedCorner) {
    case BACK_RIGHT:
      cornerPt = tmppt;
      break;

    case BACK_LEFT:
      cornerPt = tmppt - width;
      break;

    case FRONT_RIGHT:
      cornerPt = tmppt - length;
      break;

    case FRONT_LEFT:
      cornerPt = tmppt - width - length;
      break;

    default:
      ERROR("shouldn't reach this branch of switch statement");
    }

    objectPoints.clear();
    objectPoints.push_back(cornerPt);
    objectPoints.push_back(cornerPt + length);
    objectPoints.push_back(cornerPt + length + width);
    objectPoints.push_back(cornerPt + width);

    el.clear();
    el.setGeometry(objectPoints);
    el.setTypeObstacle();

    el.height=2;
    el.setId(mapCarID,predictedCarID,cars[j].ID);
    el.plotColor = MAP_COLOR_GREY;
    el.state = rawState;
    bytesSent = sendMapElement(&el, debugSubgroup);


    //sending results of fit car, with sides in different colors
 
    objectPoints.clear();
    objectPoints.push_back(cars[j].corners[BACK_RIGHT]);
    objectPoints.push_back(cars[j].corners[FRONT_RIGHT]);

    el.clear();
    el.setTypeObstacle();
    el.height=2;
    el.setId(mapCarID,moduleCarSeg1ID,cars[j].ID);
    el.setGeometry(objectPoints);
    el.plotColor = MAP_COLOR_YELLOW;
    el.state = rawState;
    bytesSent = sendMapElement(&el, debugSubgroup);
  
    objectPoints.clear();
    objectPoints.push_back(cars[j].corners[BACK_RIGHT]);
    objectPoints.push_back(cars[j].corners[BACK_LEFT]);

    el.clear();
    el.setTypeObstacle();
    el.height=2;
    el.setId(mapCarID,moduleCarSeg2ID,cars[j].ID);
    el.setGeometry(objectPoints);
    el.plotColor = MAP_COLOR_MAGENTA;
    el.state = rawState;
    bytesSent = sendMapElement(&el, debugSubgroup);


    //Sending KF velocity
    KFvelX = cars[j].Xmu.getelem(1,0);
    KFvelY = cars[j].Ymu.getelem(1,0);
    tmppt = point2(KFvelX, KFvelY);

    objectPoints.clear();
    objectPoints.push_back(cornerPt + .5*width + .5*length);
    objectPoints.push_back(cornerPt + .5*width + .5*length + tmppt);

    el.clear();
    el.setTypeObstacle();
    el.height=2;
    el.setGeometry(objectPoints);
    el.setId(mapCarID,carKFVelocityID,cars[j].ID);
    el.plotColor = MAP_COLOR_GREEN;
    el.state = rawState;
    bytesSent = sendMapElement(&el, debugSubgroup);

  } // end cycling through each car


  // ********* cycling through objects **************
  for(unsigned int k=0; k<usedObjs.size(); k++) {
    int j = usedObjs.at(k);

    // sending object velocity 

    objectPoints.clear();
    objectPoints.push_back(objects[j].el.center);
    point2 velocity = point2(objects[j].Xmu.getelem(1,0),objects[j].Ymu.getelem(1,0));
    //    point2 velocity = point2(objects[j].velocity);
    objectPoints.push_back(objects[j].el.center + 75*velocity);

    el.clear();
    el.setTypeObstacle();
    el.height=2;
    el.setGeometry(objectPoints);
    el.setId(mapCarID,objectVelocityID,objects[j].ID);
    el.plotColor = MAP_COLOR_RED;
    el.state = rawState;
    bytesSent = sendMapElement(&el, debugSubgroup);


    //sending the outline of the ellipse
    point2arr objel = objects[j].el.border(MARGIN);

    el.clear();
    el.setId(mapCarID,moduleEllipseID,objects[j].ID);
    el.setTypePoints();
    el.setGeometry(objel);
    el.plotColor = MAP_COLOR_YELLOW;
    el.state = rawState;
    bytesSent = sendMapElement(&el, debugSubgroup);


    //sending this object's history
    point2arr tmparr = point2arr(objects[j].history);
    el.clear();
    el.setId(mapCarID,moduleHistoryID,objects[j].ID);
    el.setTypePoints();
    el.setGeometry(objects[j].history);
    el.plotColor = MAP_COLOR_MAGENTA;
    el.state = rawState;
    bytesSent = sendMapElement(&el, debugSubgroup);




  } // end cycling through each object

}

  
