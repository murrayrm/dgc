/**
 * File: LadarCarPerceptor.hh
 * Description:
 *   Uses bumper ladar to track objects.
 *   This file has the functions handling all
 *   LADAR interfaces, as well as initializing
 *   everything and running the main loop
 * Based on the Obsperceptor template by Andrew
 * Last Changed: Jul 12
 **/


#include <assert.h>
#include <ctype.h>
#include <math.h>
#include <ncurses.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <time.h>
#include <unistd.h>
#include <vector>
//#include <GL/glut.h>

#undef border //this is defined in ncurses and conflicts with some functions.


#include <alice/AliceConstants.h>
#include <cotk/cotk.h>
#include <dgcutils/DGCutils.hh>
#include <emap/emap.hh>
//#include "../emap/emapViewer.cc"
#include <frames/pose3.h>
#include <frames/coords.hh>
#include <frames/ellipse.hh>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/LadarRangeBlob.h>
#include <interfaces/MapElementMsg.h>
#include <interfaces/ProcessState.h>
#include <map/MapElementTalker.hh>
#include <map/MapElement.hh>
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_replay.h>


// Cmd-line handling
#include "cmdline.h"
#include "Matrix.hh"

using namespace std;

#define min(a,b) (((a)<(b))?(a):(b))
#define max(a,b) (((a)>(b))?(a):(b))

#define PI 3.14159

#define MAXTRAVEL 1 //max dist obj can travel btwn frames
#define MARGIN 1 //dist outside ellipse that's allowable

#define MAXNUMCARS 50 //max num cars we can track
#define MAXNUMOBJS 300

#define MINNUMPOINTS 3 //min # pts to consider object a 'car'
#define NUMSCANPOINTS 181

#define GAPDISTANCE 1.5 //used in determining segmentation
//chosen because want at least 3 points on target to track object
//and cars aren't going to be much longer than 4 m long, so a 2m
//gap distance shouldn't be too short. Additionally, a car won't be 
//coming out of a gap less than 2m wide, so we can ignore them

#define SINTHETA .017452   //sin(1 deg) used for determining segmentation

#define MAXRANGE 80.0 //any range greater than this is no-return point
#define MINRANGE .1 //used in case we get returns off the ladar box

#define CARMARGIN .5 //Only associate seg w/ car if it's closer than this

//FIXME - these are tuned for realistic driving situation
#define MINSPEED 3.0   //min velocity to call something a car
#define MAXSPEED 30.0 //max velocity 
#define MINDIMENSION 1.5 //we're assuming this is the minimum car dimension 
                       //used when we can only see 1 side...
#define MAXCARLENGTH 6.0  //testing to see if this solves the splitting problem
#define MAXCARWIDTH  2.5 //using alice's dimensions =)

#warning "arbitrary parameter!!"
#define MAXCARELLIPSEDIM 2.0 //if ellipse larger than this, can't be car

#define OBJECTTIMEOUT 50 //how many loops before we delete an object
#define CARTIMEOUT 50 //how many loops before we delete a cara
#define STATIONARYVELOCITY .2 //velocity to consider something stationary for planner

#define MAPRADIUS 128//(new) //how far from alice map stretches
#define MAPCELLDIMENSION .5   //dimensions of each cell (in m)
#define MAPOBJECTINCREMENT 5
#define MAPFREESPACEINCREMENT 1
#define MAPOCCUPIEDTHRESHOLD .8 //probability of static 
#define MAPCLEARVALUE 0xC0C0

#define MAPVISIBLE 35.0
//object that we require to rule out moving vehicle


/** 
 * defines for changing the algorithm's behaviour
 * only one of each group should be used
 */

#define USE_KALMAN_FILTER_VELOCITY // - Not tuned yet!!
//#define USE_AVERAGE_VELOCITY

#define USE_ELLIPSE_FOR_CHECKMOTION
//#define USE_VELOCITY_FOR_CHECKMOTION - will give false 
// positives, esp combined w/ using average velocity

//#define BOTH_ENDS_IN_FOREGROUND
//#define ONE_END_IN_FOREGROUND
#define NO_FOREGROUND_REQUIREMENT

//not ready yet 
//#define USE_PREDICTION_FOR_ASSOCIATION 


//for accessing the rawData array
enum {
  ANGLE,
  RANGE
};

enum {
  MINANGLE_35,
  MAXANGLE_35,
  MINANGLE_MINRANGE,
  MINANGLE_MAXRANGE,
  MAXANGLE_MINRANGE,
  MAXANGLE_MAXRANGE
};

enum carCorner {
  FRONT_LEFT,
  FRONT_RIGHT,
  BACK_LEFT,
  BACK_RIGHT,
  UNDEFINED_CORNER
};

enum {
  moduleObjectID, 
  moduleCarID, 
  moduleEllipseID,
  moduleHistoryID,
  carVelocityID, 
  carKFVelocityID,
  objectVelocityID,
  modulePointsID, 
  moduleSegmentsID, 
  moduleSegmentsID2,
  moduleCarBoundID, 
  moduleCarSeg1ID,
  moduleCarSeg2ID,
  predictedCarID, 
  moduleCarHistoryID, 
  moduleNonCarPtsID
};

/**
 * This struct is how we store all the information about a car that we ever use
 **/
struct carRepresentation{

  //unique ID # for this car (used by mapping...)
  int ID;
  int mapID; //repeating ID, 0 <= id < 500 

  unsigned long long lastUpdated;

  point2 dTrackedCorner;
  carCorner trackedCorner; //can change, depending on which edge of car is occluded
  point2 corners[4];

  point2 length;
  point2 width;

  //vars for non-KF velocity calculations; units are m/scan
  point2 velocity;
  int ptsinvelocity;

  //for planner, keeping track of how long car has been stopped
  bool isStopped;
  unsigned long long lastTimeMoving;

  //for storing history...
  //FIXME: make into array
  vector<point2> history;

  //histories of the points for this car
  point2arr lastScan; //used for all internal calculations
  point2arr lastScanDepth;  //polygon w/ depth, sent to map
  //the last scan we saw the car at, and how many times
  int lastSeen;
  int timesSeen;

  //whether this end is in foreground or background
  // >0 means in fg, < 0 means in bg, =0 means scan edge
  double dStart, dEnd;

  //whether we've actually seen front/back of car 
  //(useful for updating cars)
  //FIXME: these should be bool. 
  int seenFront, seenBack, seenCorner;
  //whether we actually fit a corner to the data, 
  //or added one based on where it should be


  //matrices for KF estimates.
  // note that N and E are treated independently
  Matrix Xmu;
  Matrix Xmu_hat;
  Matrix Xsigma;
  Matrix Xsigma_hat;

  Matrix Ymu;
  Matrix Ymu_hat;
  Matrix Ysigma;
  Matrix Ysigma_hat;

};

struct logDataPoint {
  double el_x, el_y, el_a, el_b;
  double alice_x, alice_y, alice_theta;
  double vel_x, vel_y;
  double obj_length; //dist from first point to last point
  double std_linefit;

};

/**
 * If we're tracking objects rather than cars, this struct is how we keep
 * track of all relevant information
 **/
struct objectRepresentation {

  int ID;  //non-repeating ID, for internal use
  int mapID; //repeating ID, 0 <= id < 500

  unsigned long long lastUpdated;

  logDataPoint logData[30];

  //the ellipse describing the obj
  ellipse el;

  //vars for non-KF velocity calculation
  point2 velocity;
  int ptsinvelocity;

  //history of where the object has been
#warning "remove this vector!"
  vector<point2> history;

  //histories of the points for this car
  point2arr lastScan; //used for all internal calculations
  point2arr lastScanDepth;  //polygon w/ depth, sent to map

  //how many times we've seen it, and when the last one was
  int timesSeen;
  int lastSeen;

  //for the planner, keeping track of how long object has been stopped
  bool isStopped;
  unsigned long long lastTimeMoving;

  //start and end distances (+ means car is in foreground, 
  //0 means at edge of scan, - means car is in background)
  double dStart, dEnd; 

  //indices of start/end point -> used only for turning new
  //objects into cars
  int startIndex, endIndex;

  //how to know when to update object position
  int seenStart, seenEnd;

  //last seen end points
  point2 startPoint, endPoint;

  //matrices for KF estimates
  Matrix Xmu;
  Matrix Xmu_hat;
  Matrix Xsigma;
  Matrix Xsigma_hat;

  Matrix Ymu;
  Matrix Ymu_hat;
  Matrix Ysigma;
  Matrix Ysigma_hat;


};

struct lineSegmentFit {
  double a;
  double b;
  point2 pt1;
  point2 pt2;
};

struct myState {
  double X;
  double Y;
  double Z;
  double Theta;
};

// LADAR blob perceptor class
class LadarCarPerceptor : public CMapElementTalker
{
public:

  // Constructor
  LadarCarPerceptor();

  // Destructor
  ~LadarCarPerceptor();
  
  // Parse the command line
  int parseCmdLine(int argc, char **argv);
  
  // Initialize sensnet
  int initSensnet();

  // Clean up sensnet
  int finiSensnet();

  // Update the map
  int update();

  //send heartbeat to process control
  int updateProcessState();

  // Initialize console display
  int initConsole();

  // Finalize console display
  int finiConsole();
  
  // Console button callback
  static int onUserQuit(cotk_t *console, LadarCarPerceptor *self, const char *token);

  // Console button callback
  static int onUserPause(cotk_t *console, LadarCarPerceptor *self, const char *token);


  //useful function to calculate dist between 2 pts
  double dist(double x1, double y1, double x2, double y2) {
    return sqrt(pow(x1-x2,2)+pow(y1-y2,2));
  }


  /*************TRACKING FUNCTIONS****************/


  /**
   * Initializes all tracking variables:
   *   matrices for KF calculations
   *   stack of indices for objects & cars
   *   sets up logfile
   *   sets counters to 0
   **/
  int trackerInit();

  /**
   * deals with incoming range data from feeder
   * (called by main loop when new scan arrives)
   * calls the sequence of fxns that perform 
   *   operations on the new data
   **/
  void processScan(int ladarID);

  /**
   * segments a single scan, using the discontinuity 
   * requirement does not yet impose convex requirement.
   * TODO: add Convex requirement, or better threshold
   **/
  void segmentScan();

  /**
   * We represent objects by keeping track of their 
   * 'center of mass'. This function turns segmented 
   * objects into a point, and keeps track of their 
   * corresponding points/ranges/angles
   * (uses the objectRepresentation struct)
   **/
  void createObjects();

  /**
   * Updates old object with data from new object:
   *  - changes center, updates occlusion characteristics,
   *  - adds scan/range/angle points to the vectors
   *  - updates timesSeen and lastSeen
   **/
  void updateObject(objectRepresentation* oldObj, point2arr points, point2arr depthpoints, double dStart, double dEnd);

  /**
   * Adds new object to array
   *  - updates numObjects
   *  - gets index of next free space in object array
   *  - copies all data from oldObject to that free space
   **/
  void addObject(objectRepresentation* newObj);


  /**
   * tracks objects through frames (using their centers)
   **/
  void classifyObjects();

  /**
   * Checks if objects appear to be moving - if so,
   * turn them into a car
   **/
  void checkMotion();

  /**
   * Sends all tracked objects to mapper 
   **/
  void sendToMap();

  /**
   * sends all debugging mapelements
   **/
  void sendDebug();

  /**
   * removes any objects that haven't been updated recently
   *
   * checks objects for potential duplicates, and calls
   * mergeObjects when necessary
   **/
  void cleanObjects();

  // merges two objects (newobj into oldobj)
  void mergeObjects(objectRepresentation* oldObj, objectRepresentation* newObj);

  /**
   * removes cars that haven't been seen recently
   *
   * TODO: add free space
   **/
  void cleanCars();

  /**
   * This function takes the points stored in the vector 
   * points, and uses them to update the representation 
   * Car. 
   **/
  void updateCar(carRepresentation* Car, point2arr points, point2arr depthpoints, double dStart, double dEnd);

  /**
   * we have an obj and checkMotion says that it's moving,
   * so we need to turn it into a car
   * 
   * Removes object at given array index, fits a car 
   *  shape to most recently observed scan, and adds 
   *  the new car to the car array
   **/
  void obj2car(int index); 

  /**
   * takes in a list of points, and fits a rectangle 
   * to them. 
   **/
  int fitCar(point2arr points, carCorner currentCorner, point2 currentCornerCoords, int seenStart, int seenEnd, carRepresentation* fit, double vN, double vE, double mind1, double mind2);

  /**
   * removes object at given index, where index
   * is index to the usedObjs array (NOT the index
   * to the array of objects)
   **/
  void removeObject(int index);

  /**
   * removes car at given index
   **/
  void removeCar(int index);

  /**
   * uses the most recent measurements (if they exist) 
   * to update the KF
   * */
  void KFupdate();

  /**
   * calculates mu_hat and sigma_hat, to be used in 
   * matching objects w/ new scans.
   **/
  void KFpredict(double dt);

  void writeLogs();


  /***************MAPPING FUNCTIONS****************/
  void mapInit();
  void mapUpdate();

public:
  
  // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;
  modulename moduleId;

  FILE * logfile;

  // Sensnet module
  sensnet_t *sensnet;

  // Sensnet replay module
  sensnet_replay_t *replay;

  // Operation mode
  enum {modeLive, modeReplay} mode;

  // Console interface
  cotk_t *console;

  int useDisplay;
  int useMap;
  int useMapDisplay;
  int useLogging;
  int onlyLeft, onlyRight, onlyCenter, onlyRear, onlyRoofRight, onlyRoofLeft, onlyRiegl;

  unsigned long long startTime, origStartTime;
  unsigned long long endTime;
  unsigned long long currTime;
  double diffTime;



  // Should we quit?
  bool quit;
  
  // Should we pause?
  bool pause;

  //depth to add to objects along ladar's beam
  double objectDepth;

  //how many times we've looped in the main program
  int loopCount;

  // Individual ladar data
  struct Ladar
  {
    // Sensor id
    sensnet_id_t sensorId;

    // Latest blob id
    int blobId;
  };

  // List of currently subscribed ladars
  int numLadars;
  Ladar ladars[16];


  /************ defines from trackMO ***************/

  //angle, range data
  double rawData[NUMSCANPOINTS][2];
  double occupiedData[NUMSCANPOINTS][6][3];
  //differences in RANGES between consecutive scans
  double diffs[NUMSCANPOINTS-1];
  double diffs2[NUMSCANPOINTS-2];
  double diffs3[NUMSCANPOINTS-3];

  //x,y,z data (in local frame)
  double xyzData[NUMSCANPOINTS+1][3];
  //x,y,z data in local frame, for points 1m radially
  //behind the actual return
  double depthData[NUMSCANPOINTS+1][3];

  //vehicle state at time of scan, used for figuring 
  //which corner to add
  myState currState;
  myState ladarState;
  VehicleState rawState;

  // Matrices for Kalman Filter calculations
  Matrix A;
  Matrix C;
  Matrix I;
  Matrix R;
  Matrix Q;

  //variables for the maptalker stuff
  int subgroup; 
  int mapCarID, mapObjID;
  int outputSubgroup;
  int debugSubgroup;
  int outputRate;

  //timestamp to keep track of how often to send cars to map
  unsigned long long lastSentCars;
  unsigned long long lastSentObjs;

  //timestamp to keep track for KF
  unsigned long long lastKFupdate;

  //first and last index of each object, as found 
  //by segmentScan()
  int posSegments[NUMSCANPOINTS][2]; 

  //number of pts in ith object, again as found 
  //by segmentScan()
  int sizeSegments[NUMSCANPOINTS]; 

  int numSegments; //number of objects found by segmentScan
  int numFrames; //how many frames we've processed
  int numObjects;
  int numNonCarObjects; //how many objs we saw in the last frame with BOTH
                     //edges occluded

   //the stack to keep track of used/unused object indices
  vector<int> openObjs; 
  vector<int> usedObjs;

  vector<int>::iterator objIterator;
  objectRepresentation newObjects[MAXNUMOBJS];
  objectRepresentation objects[MAXNUMOBJS];

  //have to send to mapper index between 0 and 499, inclusive, w/o repeating
  //keep track of which ones are available here
  vector<int> mapIndices;

  //how many objects we've turned into cars
  // used to give each car unique ID
  int numCars; 

  //the stack to keep track of used/unused car indices
  vector<int> openCars;
  vector<int> usedCars;
  vector<int>::iterator carIterator;
  carRepresentation cars[MAXNUMCARS]; //Trying to define an array of car structs

  int numNewObjects;
  vector<int> carsToUpdate[MAXNUMCARS];
  vector<int> objsToUpdate[MAXNUMOBJS];

  /************** VARS FOR MAPPING ******************/
  
  EMap *tamasMap;
  //vars for keeping track of where the map is centered (x,y coords of center of
  //cell w/ index (centerindex, centerindex)
  double mapCenterX, mapCenterY;

  //vars for sending current scan's obstacles
  int numSensorIds;  
  int currSensor;

#warning "magic numbers here..."
  int matchedCar[61];
  int matchedObj[61];

  int numSentSegments[4];

};


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


