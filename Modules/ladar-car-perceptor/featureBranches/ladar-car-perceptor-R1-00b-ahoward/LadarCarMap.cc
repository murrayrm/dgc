/**
 * File: LadarCarMap.cc
 * Description: (see p. 110)
 *   A simple grid-map that tracks freespace
 *   (Ambrus convinced me to give this a try)
 *   The purpose is to add another check before deciding that an object is
 *   actualy moving (if it's in what this map thinks is free space, it's moving
 *   otherwise, it's probably just poor association/wall partially occluded
 *   
 *   All cells start out with value 0, value is decremented if cell is
 *   observed to be free, incremented if observed to be occupied
 *
 * Last Changed: May 5
 **/

#include "LadarCarPerceptor.hh"
using namespace std;

void LadarCarPerceptor::mapInit()
{
  vector<double> mapRow(MAPSIZE,0.0);
  //map starts out as square grid of 0's
  for(int i=0; i < MAPSIZE; i++) 
    {
       occupancyMap.push_back(mapRow);
    }
  return;
}

/**
 * This function cycles through each cell in the map, updating them if
 * necessary. 
 **/
void LadarCarPerceptor::mapUpdate()
{
  double cellX, cellY;
  for(int j=0; j<MAPSIZE; j++) 
    {

      for(int j = 0; j < MAPSIZE; j++)
	{


	}
    }

}

/**
 * This function increments the cell at index x,y by delta
 **/
void LadarCarPerceptor::mapUpdateElement(int x, int y, int delta)
{
  occupancyMap[y][x] += delta;
}

/**
 * This function shifts the center of the map MAPCELLSIZE in the positive
 * X direction
 *
 * In each row, remove element at start and push new one at end
 **/
void LadarCarPerceptor::mapShiftPosX()
{
  for(int i=0; i < MAPSIZE; i++) 
    {
      occupancyMap.at(i).push_back(0.0);
      vector<double>::iterator tempIterator = occupancyMap.at(i).begin();
      occupancyMap.at(i).erase(tempIterator);
    }
}


/**
 * This function shifts the center of the map MAPCELLSIZE in the negative
 * X direction.
 *
 * In each row, remove element at end, and push new one at beginning
 **/
void LadarCarPerceptor::mapShiftNegX()
{
  for(int i=0; i < MAPSIZE; i++) 
    {
      occupancyMap.at(i).pop_back();
      vector<double>::iterator tempIterator = occupancyMap.at(i).begin();
      occupancyMap.at(i).insert(tempIterator, 0.0);
    }
}

/**
 * This function shifts the center of the map mapcellsize in the 
 * positive Y direction.
 *
 * removes row at start, inserts new one at end
 **/
void LadarCarPerceptor::mapShiftPosY()
{
  vector<double> mapRow(MAPSIZE,0.0);
  occupancyMap.push_back(mapRow);

  vector<vector<double> >::iterator tempIterator = occupancyMap.begin();
  occupancyMap.erase(tempIterator);
}


/**
 * Shifts the center of the map mapcellsize m in the negative Y direction
 *
 * removes row at end, adds new one at start
 **/
void LadarCarPerceptor::mapShiftNegY()
{
  vector<double> mapRow(MAPSIZE,0.0);
  occupancyMap.pop_back();

  vector<vector<double> >::iterator tempIterator = occupancyMap.begin();
  occupancyMap.insert(tempIterator,1,mapRow);
}

/**
 * This function is used to test the above operations on the map
 **/
void LadarCarPerceptor::mapTest() 
{
  mapUpdateElement(2,2,2);
  fprintf(stderr, "after incrementing element at (2,2), map is: \n");
  mapPrintTest();

  mapShiftNegX();
  fprintf(stderr, "after shift to neg x, map is: \n");
  mapPrintTest();

  mapShiftNegY();
  fprintf(stderr, "after shift to neg Y, map is: \n");
  mapPrintTest();

  mapShiftPosX();
  fprintf(stderr, "after shift to pos X, map is: \n");
  mapPrintTest();

  mapShiftPosY();
  fprintf(stderr, "after shift to pos Y, map is: \n");
  mapPrintTest();

}

void LadarCarPerceptor::mapPrintTest()
{
  fprintf(stderr, "    %f, %f, %f, %f, %f \n", occupancyMap[4][0], occupancyMap[4][1], occupancyMap[4][2], occupancyMap[4][3], occupancyMap[4][4]);
  fprintf(stderr, "    %f, %f, %f, %f, %f \n", occupancyMap[3][0], occupancyMap[3][1], occupancyMap[3][2], occupancyMap[3][3], occupancyMap[3][4]);
  fprintf(stderr, "    %f, %f, %f, %f, %f \n", occupancyMap[2][0], occupancyMap[2][1], occupancyMap[2][2], occupancyMap[2][3], occupancyMap[2][4]);
  fprintf(stderr, "    %f, %f, %f, %f, %f \n", occupancyMap[1][0], occupancyMap[1][1], occupancyMap[1][2], occupancyMap[1][3], occupancyMap[1][4]);
  fprintf(stderr, "    %f, %f, %f, %f, %f \n", occupancyMap[0][0], occupancyMap[0][1], occupancyMap[0][2], occupancyMap[0][3], occupancyMap[0][4]);
}
