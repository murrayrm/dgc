/**
 * File: LadarCarPerceptor.cc
 * Description:
 *   Uses bumper ladar to track objects.
 *   This file has the functions handling all
 *   LADAR interfaces, as well as initializing
 *   everything and running the main loop
 * Based on the Obsperceptor template by Andrew
 * Last Changed: March 9
 **/

#include "LadarCarPerceptor.hh"

// Default constructor
LadarCarPerceptor::LadarCarPerceptor()
{
  memset(this, 0, sizeof(*this));

 
  return;
}


// Default destructor
LadarCarPerceptor::~LadarCarPerceptor()
{
  fclose(logfile);
}


// Parse the command line
int LadarCarPerceptor::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // See which mode we are running in (sensnet or log)
  if (this->options.inputs_num == 0)
    this->mode = modeLive;
  else
    this->mode = modeReplay;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);

  string logfilename = this->options.log_name_arg;
 logfile = fopen(this->options.log_name_arg,"w");
  if(logfile==NULL) {
    fprintf(stderr, "logfile open failed \n");
  } else {
    fprintf(logfile, "testing! \n");
  }

      
  trackerInit();
  return 0;
}



// Initialize sensnet
int LadarCarPerceptor::initSensnet()
{
  int i;
  int numSensorIds;  
  sensnet_id_t sensorIds[16];
  Ladar *ladar;
    

  if (this->mode == modeLive)
    {
      // Create sensnet interface
      this->sensnet = sensnet_alloc();
      assert(this->sensnet);
      if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
        return -1;
    }
  else
    {
      // Create replay interface
      this->replay = sensnet_replay_alloc();
      assert(this->replay);    
      if (sensnet_replay_open(this->replay, this->options.inputs_num, this->options.inputs) != 0)
        return ERROR("unable to open log");
    }

  // Default ladar set
  numSensorIds = 0;
  sensorIds[numSensorIds++] = SENSNET_LF_ROOF_LADAR;
  sensorIds[numSensorIds++] = SENSNET_RIEGL;
  sensorIds[numSensorIds++] = SENSNET_RF_ROOF_LADAR;
  sensorIds[numSensorIds++] = SENSNET_LF_BUMPER_LADAR;
  sensorIds[numSensorIds++] = SENSNET_MF_BUMPER_LADAR;
  sensorIds[numSensorIds++] = SENSNET_RF_BUMPER_LADAR;

  // Initialize ladar list
  for (i = 0; i < numSensorIds; i++)
    {
      assert((size_t) this->numLadars < sizeof(this->ladars) / sizeof(this->ladars[0]));
      ladar = this->ladars + this->numLadars++;

      // Initialize ladar data
      ladar->sensorId = sensorIds[i];

      // Join the ladar data group
      if (this->sensnet)
        {
          if (sensnet_join(this->sensnet, ladar->sensorId,
                           SENSNET_LADAR_BLOB, sizeof(LadarRangeBlob)) != 0)
            return ERROR("unable to join %d", ladar->sensorId);
        }
    }
  
  return 0;
}


// Clean up sensnet
int LadarCarPerceptor::finiSensnet()
{
  if (this->sensnet)
    {
      int i;
      Ladar *ladar;
      for (i = 0; i < this->numLadars; i++)
        {
          ladar = this->ladars + i;
          sensnet_leave(this->sensnet, ladar->sensorId, SENSNET_LADAR_BLOB);
        }
      sensnet_free(this->sensnet);
      this->sensnet = NULL;
    }
  else if (this->replay)
    {
      sensnet_replay_close(this->replay);
      sensnet_replay_free(this->replay);
      this->replay = NULL;
    }


  return 0;
}


// Update the map with new range data
int LadarCarPerceptor::update()
{
  //  fprintf(stderr, "entering update() \n");

  int i;
  Ladar *ladar;
  int blobId, blobLen;
  LadarRangeBlob blob;

 if (this->sensnet)
  {
    // In live mode wait for new data, but timeout if
    // we dont get anything new for a while.
    if (sensnet_wait(this->sensnet, 200) != 0)
      return 0;
  }
  else if (this->replay)
  {
    // In replay mode, advance the log to the next record.
    if (sensnet_replay_next(this->replay, 0) != 0)
      return 0;
  }

  for (i = 0; i < this->numLadars; i++)
    {
      ladar = this->ladars + i;


  if (this->sensnet)
    {
      // Check the latest blob id
      if (sensnet_peek(this->sensnet, ladar->sensorId,
                       SENSNET_LADAR_BLOB, &blobId, &blobLen) != 0)
        continue;

      // Is this a new blob?
      if (blobId == ladar->blobId)
        continue;

      // If this is a new blob, read it    
      if (sensnet_read(this->sensnet, ladar->sensorId,
                       SENSNET_LADAR_BLOB, &ladar->blobId, sizeof(blob), &blob) != 0)
        break;
    }
    else if (this->replay)
    {
      // Advance log one step
      if (sensnet_replay_next(this->replay, 0) != 0)
        return -1;
 
      // Read new blob
      if (sensnet_replay_read(this->replay, ladar->sensorId,
                              SENSNET_LADAR_BLOB, &ladar->blobId, sizeof(blob), &blob) != 0)
        break;
    }

   

      //testing - checking that I can access the data and creating template for future usage =)
      //sensor id
      //    fprintf(stderr,"sensorID: %d \n", blob.sensor_id);

      //scan id
      //    fprintf(stderr, "scanID: %d \n", blob.scanid);

      //timestamp
      //    fprintf(stderr, "timestamp: %f \n", blob.timestamp);

      //state
      //    fprintf(stderr, "Vehicle state (N, E, timestamp): %f, %f, %f \n", blob.state.utmNorthing, blob.state.utmEasting, blob.state.timestamp);

      //number of points
      //    fprintf(stderr, "num Points: %d \n", blob.num_points);

      //raw ranges
      //    fprintf(stderr, "first and last raw ranges: %f, %f \n", blob.points[0][1], blob.points[180][1]);

      //transformed to sensor frame
      float sfx, sfy, sfz; //sensor frame vars
      LadarRangeBlobScanToSensor(&blob, blob.points[45][0], blob.points[45][1],&sfx, &sfy, &sfz);
      //    fprintf(stderr, "middle range transformed to sensor frame (x,y,z): %f, %f, %f \n", sfx, sfy, sfz);

      //transformed to vehicle frame
      float vfx, vfy, vfz; //vehicle frame vars
      LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
      //    fprintf(stderr, "middle range transformed to vehicle frame (x,y,z): %f, %f, %f \n", vfx, vfy, vfz);

      //transformed to local frame
      float lfx, lfy, lfz; //local frame vars
      LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
      //    fprintf(stderr, "middle range transformed to local frame (x,y,z): %f, %f, %f \n", lfx, lfy, lfz);

      //coyping the data to a arrays accessible by any 
      //function in the program. Seems inefficient....
      double xpts[NUMSCANPOINTS];
      double ypts[NUMSCANPOINTS];
      double zpts[NUMSCANPOINTS];

      int numReturns = 0;
      for(int j=0; j < NUMSCANPOINTS; j++) 
        {
          rawData[j][ANGLE] = blob.points[j][ANGLE];
          rawData[j][RANGE] = blob.points[j][RANGE];
          LadarRangeBlobScanToSensor(&blob, blob.points[j][ANGLE], blob.points[j][RANGE],&sfx, &sfy, &sfz);
          LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
          LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
          xyzData[j][0] = lfx;
          xyzData[j][1] = lfy;
          xyzData[j][2] = lfz;

          //if this isn't a no-return point, send to display
          if(rawData[j][1] < 80) {
            xpts[numReturns]=lfx;
            ypts[numReturns]=lfy;
            zpts[numReturns]=lfz;
            numReturns++;
          }

        }

      for(int j=0; j < NUMSCANPOINTS; j++) 
        {
          LadarRangeBlobScanToSensor(&blob, blob.points[j][ANGLE], blob.points[j][RANGE] + RADIALOFFSET,&sfx, &sfy, &sfz);
          LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
          LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
          depthData[j][0] = lfx;
          depthData[j][1] = lfy;
          depthData[j][2] = lfz;

        }




      //for plotting the segmentation
      LadarRangeBlobScanToSensor(&blob, 0.0, 0.0, &sfx, &sfy, &sfz);
      LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
      LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
      ladarState.X = lfx;
      ladarState.Y = lfy;
      ladarState.Z = lfz;
      ladarState.Theta = atan2(xyzData[90][1] - lfy, xyzData[90][0] - lfx); 

      currState.X = blob.state.localX;
      currState.Y = blob.state.localY;
      currState.Z = blob.state.localZ;
      //TODO: check if this runs fast enough to not be limiting factor
      //    fprintf(stderr, "calling processScan() \n");
      if(useDisplay) {
        // myDisplay->sendScan(numReturns, xpts, ypts, zpts);
      }

      //call all the tracking functions
      processScan(blob.sensorId);

      if (this->console)
        {
          char token[64];
          snprintf(token, sizeof(token), "%%ladar%d%%", i);
          cotk_printf(this->console, token, A_NORMAL, "%s %d %8.3f",
                      sensnet_id_to_name(ladar->sensorId),
                      blob.scanId, fmod((double) blob.timestamp * 1e-6, 10000));
        }
    } //end cycling through ladars

  return 0;
} //end update()


// Template for console
/*
  01234567890123456789012345678901234567890123456789012345678901234567890123456789
*/
static char *consoleTemplate =
"LadarCarPerceptor $Revision$                                                    \n"
"                                                                           \n"
"Skynet: %spread%                                                           \n"
"                                                                           \n"
"Ladar[0]: %ladar0%                                                         \n"
"Ladar[1]: %ladar1%                                                         \n"
"Ladar[2]: %ladar2%                                                         \n"
"Ladar[3]: %ladar3%                                                         \n"
"Ladar[4]: %ladar4%                                                         \n"
"Ladar[5]: %ladar5%                                                         \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%]                                                           \n";


// Initialize console display
int LadarCarPerceptor::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
    
  // Initialize the display
  cotk_open(this->console,NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));

  return 0;
}


// Finalize sparrow display
int LadarCarPerceptor::finiConsole()
{
  // Clean up the CLI
  if (this->console)
    {
      cotk_close(this->console);
      cotk_free(this->console);
      this->console = NULL;
    }

  return 0;
}


// Handle button callbacks
int LadarCarPerceptor::onUserQuit(cotk_t *console, LadarCarPerceptor *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int LadarCarPerceptor::onUserPause(cotk_t *console, LadarCarPerceptor *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}


// Main program thread
int main(int argc, char **argv)
{
  LadarCarPerceptor *percept;
  
  percept = new LadarCarPerceptor();
  assert(percept);

  // Parse command line options
  if (percept->parseCmdLine(argc, argv) != 0)
    return -1;
  
  // Initialize sensnet
  if (percept->initSensnet() != 0)
    return -1;

  // Initialize cotk display
  if (!percept->options.disable_console_flag)
    if (percept->initConsole() != 0)
      return -1;

  // If I'm only sending cars, or sending debug info
  //as well to the mapviewer
  if (percept->options.disable_display_flag)
    percept->useDisplay = 0;
  else
    percept->useDisplay = 1;

  if (percept->options.disable_map_flag)
    percept->useMap = 0;
  else
    percept->useMap = 1;


  fprintf(stderr, "entering main thread of LadarCarPerceptor \n");

  percept->loopCount =0;
  unsigned long long loopStart, loopEnd;  
  while (!percept->quit)
    {
      DGCgettime(loopStart);
      int tempCount = percept->loopCount;
      percept->loopCount = tempCount+1;

      // Update the console
      if (percept->console)
        cotk_update(percept->console);

      // If we are paused, dont do anything
      if (percept->pause)
        {
          usleep(0);
          continue;
        }

      // Wait for new data
      //  if (sensnet_step(percept->sensnet, 100) != 0)
      //  {
      //     break;
      //  } else {
          //      fprintf(stderr, "should have new data! \n");
      //  }

      // Update the map
      if (percept->update() != 0)
        {	
          break;
        } else {
          //      fprintf(stderr, "should be updating the map! \n");
        }
      DGCgettime(loopEnd);
      //     fprintf(stderr, "time elapsed in ladar-car-perceptor: %llu \n", loopEnd - loopStart);
    }

  if (percept->console)
    percept->finiConsole();
  percept->finiSensnet();

  MSG("exited cleanly");
  
  return 0;
}



