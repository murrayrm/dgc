Sat Oct 20 19:40:25 2007	Sam Pfister (sam)

	* version R1-02d
	BUGS:  
	FILES: Makefile.yam(45884), MapViewer.cc(45884),
		MapWindow.cc(45884), MapWindow.hh(45884),
		cmdline.ggo(45884)
	merged in new changes

	FILES: Makefile.yam(45875), MapViewer.cc(45875),
		MapWindow.cc(45875), MapWindow.hh(45875),
		cmdline.ggo(45875)
	added obstacle velocity visualization.	Updated makefile to include
	the rndf library needed by map

Sat Oct 20 14:01:29 2007	Jessica Gonzalez (jengo)

	* version R1-02c
	BUGS:  
	FILES: MapWindow.cc(45746)
	map window now shows element velocity

Sat Oct  6 14:00:39 2007	Sam Pfister (sam)

	* version R1-02b
	BUGS:  
	FILES: MapViewer.cc(43158)
	Streamlined some drawing commands for remote operation

	FILES: MapViewer.cc(43207), MapWindow.cc(43207),
		MapWindow.hh(43207), cmdline.ggo(43207)
	Added visualization for Alice in local frame and site frame.  Alice
	in local frame is shown with dotted outline, Alice in site frame is
	shown as before.  Local frame visualization can be turned off at
	command line using --hide-local-state option

Fri Oct  5 20:02:23 2007	Sam Pfister (sam)

	* version R1-02a
	BUGS:  
	FILES: MapWindow.cc(43129)
	added back the cost painting option in main loop. 

Sun Sep 30 10:14:55 2007	Sam Pfister (sam)

	* version R1-02
	BUGS:  
	FILES: MapViewer.cc(41814), MapWindow.cc(41814),
		MapWindow.hh(41814), cmdline.ggo(41814)
	Mapviewer now uses new mapElementTalker function which recovers
	from spread overflow errors.  In the case of an overflow the map
	will be cleared and the Overflow count on the display will be
	incremented.  

	Also the receive rate of map elements is now showm in
	elements/second on the display.  

	There is a new option to turn off rndf visualization which can
	help as a stopgap to test with very large rndfs on a slow machine
	or remotely.  The 'h' key will toggle the hiding of the rndf and
	the --hide-rndf option will hide the rndf at startup.

Sat Sep 29 15:43:58 2007	Sam Pfister (sam)

	* version R1-01z
	BUGS:  
	FILES: MapWindow.cc(41564)
	Added functionality to view debugging text contained in map
	elements

Sat Sep 29  8:22:06 2007	murray (murray)

	* version R1-01y
	BUGS:  
	FILES: MapViewer.cc(41313), MapWindow.cc(41313), cmdline.ggo(41313)
	added ability to rotate plots

Fri Sep 28  6:48:31 2007	Sam Pfister (sam)

	* version R1-01x
	BUGS:  
	FILES: MapViewer.cc(40958), MapWindow.cc(40958),
		MapWindow.hh(40958)
	added functionality to visualize debugging information stored in
	received map elements of type ELEMENT_DEBUG

	FILES: MapViewer.cc(40958), MapWindow.cc(40958),
		MapWindow.hh(40958)
	added functionality to visualize debugging information stored in
	received map elements of type ELEMENT_DEBUG

Tue Sep 18 19:30:12 2007	datamino (datamino)

	* version R1-01w
	BUGS:  
	FILES: Makefile.yam(38690)
	Stopped fltk-config to enable compiler optimization thus making
	debugging really difficult.

	FILES: Makefile.yam(39402)
	Added option to compile with optimizations, default disabled. Use
	'ymk all OPTIMIZE=1' to enable compiler optimizations.

Tue Aug 28 15:19:10 2007	Jessica Gonzalez (jengo)

	* version R1-01v
	BUGS:  
	FILES: MapViewer.cc(36002), MapWindow.cc(36002),
		MapWindow.hh(36002), cmdline.ggo(36002)
	Added flag for showing time stopped on map elements, and turned it
	off by default

Tue Aug 28 13:36:33 2007	datamino (datamino)

	* version R1-01u
	BUGS:  
	FILES: Makefile.yam(35912)
	Added -lbitmap_gl to compile with the latest bitmap release

Mon Aug 27 13:10:47 2007	Jessica Gonzalez (jengo)

	* version R1-01t
	BUGS:  
	FILES: MapWindow.cc(35664)
	Added timestopped output next to stopped objects

Thu Aug 23 11:28:28 2007	Jessica Gonzalez (jengo)

	* version R1-01s
	BUGS:  
	FILES: MapWindow.cc(35133)
	adding new color for traversed path

Wed Aug 22  6:28:30 2007	Sam Pfister (sam)

	* version R1-01r
	BUGS:  
	FILES: MapViewer.cc(34878), MapWindow.cc(34878),
		MapWindow.hh(34878), cmdline.ggo(34878)
	Removed plotting of uncertainty ellipses by default.  Can be
	reenabled from command line.  Added display of total number of
	elements plotted.

Mon Aug 20 23:50:11 2007	Laura Lindzey (lindzey)

	* version R1-01q
	BUGS:  
	FILES: MapWindow.cc(34514)
	now centers on Alice by default (one less thing for person running
	startup scripts to have to worry about)

Mon Aug 13 15:58:47 2007	Jessica Gonzalez (jengo)

	* version R1-01p
	BUGS:  
	FILES: MapWindow.cc(33160), MapWindow.hh(33160)
	Added opt to hide traversed path

Sat Jul 28  2:14:19 2007	Laura Lindzey (lindzey)

	* version R1-01o
	BUGS:  
	FILES: MapViewer.cc(30798), MapWindow.cc(30798),
		MapWindow.hh(30798), cmdline.ggo(30798)
	adding command line option "--show-ids" This causes the mapelement
	ids to be printed all the time, not just when that particular
	element is highlighted (I'm using this for working on fusion) In
	this mode, only the last element of the id vector is actually shown
	- this is the one that modules use to identify discrete elements -
	the others show originating module, etc. 

Mon Jun 18 22:39:49 2007	murray (murray)

	* version R1-01n
	BUGS:  
	FILES: Makefile.yam(28109), MapWindow.hh(28109)
	MACOSX mods

Thu Jun  7 11:26:50 2007	Sam Pfister (sam)

	* version R1-01m
	BUGS:  
	FILES: MapViewer.cc(27113), MapWindow.cc(27113), cmdline.ggo(27113)
	Updated mapviewer to not display cost map by default.  The option
	--show-costmap or -c will turn on cost map viewing functionality. 
	Updated element selection so that the map element label is
	displayed along with the id if the label has been set.	Note that
	the label is limitted to 3 lines and 50 characters per line and
	will be clipped when sent.  The label is a vector of strings in a
	map element and can be set directly.

Wed Jun  6 19:50:52 2007	datamino (datamino)

	* version R1-01l
	BUGS:  
	FILES: MapViewer.cc(26907), MapWindow.cc(26907),
		MapWindow.hh(26907), cmdline.ggo(26907)
	Optimization: changed the size of the image buffer used to paint
	the costmap, now it's always power-of-two sized, so
	BitmapBase::toGlTexture() can avoid copying it to a temp buffer
	(need a new release of bitmap to take advantage of this). Added a
	command line option to select the colormap, --colormap=...

Wed Jun  6  0:20:08 2007	datamino (datamino)

	* version R1-01k
	BUGS:  
	FILES: MapWindow.cc(26770), MapWindow.hh(26770)
	Implemented showing the costmap using a rainbow color shade (red =
	high cost, blue = low cost)

Tue Jun  5 22:40:49 2007	datamino (datamino)

	* version R1-01j
	BUGS:  
	FILES: Makefile.yam(26378), MapViewer.cc(26378),
		MapWindow.cc(26378), MapWindow.hh(26378)
	Adding visualization of cost map (work in progress).

	FILES: Makefile.yam(26739), MapViewer.cc(26739),
		MapWindow.cc(26739), cmdline.ggo(26739)
	- Now using CostMapEstimator to receive the polygons and draw the
	costmap - Added a command line option --hide-costmap to disable
	costmap display - Now using DGCgettime for timings - Added timings
	to the opengl costmap drawing code, which turns out to be the real
	bottleneck. I'll try to make it faster in another release.

	FILES: MapViewer.cc(26433), MapWindow.cc(26433)
	Added code to show the CostMap in the viewer. At the moment it's
	really slow, need to think of a faster way to do this.

	FILES: MapViewer.cc(26668), MapWindow.cc(26668),
		MapWindow.hh(26668)
	Showing the costmap was very slow, now is better, but can still be
	improved I think.

	FILES: MapViewer.cc(26754), MapWindow.cc(26754),
		MapWindow.hh(26754), cmdline.ggo(26754)
	Added a command line option for the debug level, defaults to zero
	(no messages at all).

Sat Jun  2 15:43:00 2007	 (ahoward)

	* version R1-01i
	BUGS:  
	FILES: MapWindow.cc(26203)
	Tweaked log calls (again) for gcc 3.3 compatability

Fri Jun  1  4:51:35 2007	Sam Pfister (sam)

	* version R1-01h
	BUGS:  
	FILES: MapViewer.cc(25969)
	minor changes to the main loop.  Should reduce spread errors

Tue May 29 23:39:28 2007	Jessica Gonzalez (jengo)

	* version R1-01g
	BUGS:  
	FILES: MapWindow.cc(25475)
	added more colors

Mon May 28 13:03:27 2007	Jessica Gonzalez (jengo)

	* version R1-01f
	BUGS:  
	FILES: MapWindow.cc(25231)
	added dark green color for polytopes

Tue May 22 14:56:27 2007	Sam Pfister (sam)

	* version R1-01e
	BUGS:  
	FILES: MapWindow.cc(24438), MapWindow.hh(24438)
	Updated drawing functions in mapviewer. Now hit the d key to turn
	on and off state visualization.  State will now be visualized by
	default if the received data holds state info

Tue May 15 23:37:44 2007	Sam Pfister (sam)

	* version R1-01d
	BUGS:  
	FILES: MapWindow.cc(23257)
	cleaned up some element selection code to work more predictably.

Mon May 14 23:29:45 2007	Jessica Gonzalez (jengo)

	* version R1-01c
	BUGS:  
	FILES: MapWindow.cc(23124)
	added some new colors for visualization

Mon May 14 11:32:14 2007	Sam Pfister (sam)

	* version R1-01b
	BUGS:  
	FILES: MapViewer.cc(22982), MapWindow.cc(22982),
		MapWindow.hh(22982)
	Switched ymult back to -1.  Updated drawing functions to ignore
	elements outside of the window to speed up remote operation. 
	Implemented new interface with map module

Thu May 10 13:55:18 2007	Jessica Gonzalez (jengo)

	* version R1-01a
	BUGS:  
	FILES: MapWindow.cc(22692), MapWindow.hh(22692)
	added keybinding ('a') to auto center on alice; changed ymult to 1

Mon Apr 30 12:42:35 2007	Sam Pfister (sam)

	* version R1-01
	BUGS:  
	FILES: MapViewer.cc(21704), MapWindow.cc(21704)
	Updated mapviewer to work with new map module.	Implements sub
	channel communication and variable length messages

Fri Mar 30 12:30:52 2007	Sam Pfister (sam)

	* version R1-00h
	BUGS: 
	FILES: MapWindow.cc(19029)
	fixed compile bug in MapWindow.cc

Sat Mar 17 11:44:35 2007	Sam Pfister (sam)

	* version R1-00g
	BUGS: 
	FILES: MapWindow.cc(18267)
	updated MapWindow.cc to plot Alice's position given by an element
	when an element is selected.  Updated COLOR_ prefix to MAP_COLOR_
	prefix labels to work with new MapElement enum definitions.  
	Selected element now draws in RED and label in GREEN instead of
	bold.

Tue Mar 13 16:04:28 2007	Sam Pfister (sam)

	* version R1-00f
	BUGS: 
	FILES: MapWindow.cc(17473), MapWindow.hh(17473)
	Fixed mirroring due to local frame bug in mapviewer.

Mon Mar 12 15:39:29 2007	Sam Pfister (sam)

	* version R1-00e
	BUGS: 
	FILES: MapViewer.cc(17269), MapWindow.cc(17269),
		MapWindow.hh(17269)
	Updated mapviewer to send out map elements corresponding to map
	view center point when 'o' is pressed.	If an element is selected
	in the mapviewer 'o' will send that element.  This is used for map
	debugging in testMap in the map module

Sun Mar 11 11:35:37 2007	Sam Pfister (sam)

	* version R1-00d
	BUGS: 
	FILES: MapViewer.cc(17099), MapWindow.cc(17099),
		MapWindow.hh(17099)
	updated viewer to draw alice when the alice state message is
	received.  Also clears objects from a clear message.  Pressing r in
	the window now resets the map and clears all objects.

Fri Mar  9 17:10:14 2007	Sam Pfister (sam)

	* version R1-00c
	BUGS: 
	FILES: Makefile.yam(16892), MapViewer.cc(16892),
		MapWindow.hh(16892)
	updated mapviewer to work with new map talker placement

Sun Mar  4 17:42:14 2007	Sam Pfister (sam)

	* version R1-00b
	BUGS: 
	FILES: Makefile.yam(16526)
	fixed library ordering bug in Makefile.yam

Thu Mar  1 19:27:34 2007	Sam Pfister (sam)

	* version R1-00a
	BUGS: 
	New files: MapViewer.cc MapWindow.cc MapWindow.hh cmdline.ggo
	FILES: Makefile.yam(15078)
	Updated functionality of mapviewer. Accepts MapElement messages and
	plots them.  Still needs command line args and options.

	FILES: Makefile.yam(15119)
	added commandline parsing

	FILES: Makefile.yam(15313)
	updated mapviewer to work with new skynet version

Tue Feb  6 14:49:53 2007	Sam Pfister (sam)

	* version R1-00
	Created mapviewer module.











































