/**********************************************************
 **
 **  MAPVIEWER.CC
 **
 **    Time-stamp: <2007-02-27 10:01:48 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Thu Feb  8 23:03:09 2007
 **
 **
 **********************************************************
 **
 **  2D map viewer
 **
 **********************************************************/

#include "MapWindow.hh"
#include "skynettalker/MapElementTalker.hh"
#include "interfaces/sn_types.h"

// Cmd-line handling
#include "cmdline.h"
using namespace std;

class MapViewer : public CMapElementTalker
{
public:
	MapViewer() {recvflag = false;}


	~MapViewer() {}
	
	int init();
	int add_element(MapElement& el);
	int parseCmdLine(int argc, char**argv);
	
	Fl_Window *mainwin;
	MapWindow *mapwin;
	bool ispaused;
	bool recvflag;
	gengetopt_args_info options;

	int skynetKey;
	int sendSubGroup;
	int recvSubGroup;
	
};

int MapViewer::parseCmdLine(int argc, char**argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;
 
 // Fill out the send subgroup number
  if (this->options.send_subgroup_given)
    this->sendSubGroup = this->options.send_subgroup_arg;
  else
    this->sendSubGroup = -1;

 // Fill out the recv subgroup number
  if (this->options.recv_subgroup_given)
    this->recvSubGroup = this->options.recv_subgroup_arg;
  else
    this->recvSubGroup = 0;

  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
	
	return 0;
}

int MapViewer::init()
{
	initRecvMapElement(this->skynetKey);
	this->mainwin = new Fl_Window(500,500,"DGC Map Viewer");
	this->mapwin = new MapWindow(10,10, 
															 this->mainwin->w()-20, 
															 this->mainwin->h()-20);
	ispaused = false;
	return 0;
}

int MapViewer::add_element(MapElement& el)
{

	vector<int> thisid = el.id;
	vector<int> tmpid;

	for (int i = 0; i < (int)mapwin->localmap.size(); ++i){
		tmpid = mapwin->localmap[i].id;
		if (tmpid == thisid){
			mapwin->localmap[i] = el;
			return 0;
		}
	}
	mapwin->localmap.push_back(el);
	return 0;
}

// Handle idle callbacks
void main_idle(MapViewer *self)
{
	int bytesRecv = 0;
	MapElement el;
	bytesRecv = self->recvMapElementNoBlock(&el,self->recvSubGroup);
	if (bytesRecv && !self->ispaused){
		self->add_element(el);
		self->recvflag = true;
	}
	else{
		if (self->recvflag){
			self->mapwin->redraw();
			self->recvflag = false;
		}
		usleep(10000);
	}

	return;
}


int main(int argc, char *argv[]) {
 
	//	Fl_Window win(500, 500);
	//MapWindow  map_window(10, 10, win.w()-20, win.h()-20);
	glutInit(&argc, argv);

	MapViewer mapviewer;
	mapviewer.parseCmdLine(argc,argv);
	mapviewer.init();

	mapviewer.mainwin->resizable(mapviewer.mapwin);

	Fl::add_idle((void (*) (void*)) main_idle, &mapviewer);
	mapviewer.mainwin->show();
	return(Fl::run());
}
    
