              Release Notes for "mapviewer" module

Release R1-02d (Sat Oct 20 19:33:25 2007):
	added obstacle velocity visualization.	

Release R1-02c (Sat Oct 20 14:01:33 2007):
	Map window now shows element velocity next to the ID.

Release R1-02b (Sat Oct  6 14:00:46 2007):
	Added visualization for Alice in local frame and site frame.  Alice
	in local frame is shown with dotted outline, Alice in site frame is
	shown as before.  Local frame visualization can be turned off at
	command line using --hide-local-state option

	Streamlined some drawing commands for remote operation


Release R1-02a (Fri Oct  5 20:02:26 2007):
	added back the cost painting option in main loop. 


Release R1-02 (Sun Sep 30 10:15:01 2007):
	Mapviewer now uses new mapElementTalker function which recovers
	from spread overflow errors.  In the case of an overflow the map
	will be cleared and the Overflow count on the display will be
	incremented.  

	Also the receive rate of map elements is now showm in
	elements/second on the display.  

	There is a new option to turn off rndf visualization which can
	help as a stopgap to test with very large rndfs on a slow machine
	or remotely.  The 'h' key will toggle the hiding of the rndf and
	the --hide-rndf option will hide the rndf at startup.


Release R1-01z (Sat Sep 29 15:44:01 2007):
Added functionality to view debugging text contained in map
	elements

Release R1-01y (Sat Sep 29  8:22:11 2007):
	Added --necoord option to rotate the view to the usual NE
	coordinates (north is up).  The default is the old XY coordinates
	(north is right).  In addition to the command line option, you can
	set the environment variable DGC_MAPVIEWER_NECOORD (useful if you
	are running the standard startup scripts).

	No changes in functionality and the default version should behave
	exactly as before.

Release R1-01x (Fri Sep 28  6:43:31 2007):
	added functionality to visualize debugging information stored in
	received map elements of type ELEMENT_DEBUG.  Build release is also 
	with optimizations.

Release R1-01w (Tue Sep 18 19:30:15 2007):
	Stopped fltk-config to enable compiler optimization thus making
	debugging really difficult.
	Added option to compile with optimizations, default disabled. Use
	'ymk all OPTIMIZE=1' to enable compiler optimizations.
        NOTE: This build release is compiled with optimizations, so mapviewer
        won't be slower than in the past.

Release R1-01v (Tue Aug 28 15:19:19 2007):
	Added flag for showing time stopped on map elements, and turned it off by default (--show-time-stopped)

Release R1-01u (Tue Aug 28 13:36:36 2007):
	Added -lbitmap_gl to compile with the latest bitmap release

Release R1-01t (Mon Aug 27 13:10:51 2007):
	If an object's timeStopped variable is greater than zero, mapviewer will show the time stopped next to the element. Should be useful for debugging

Release R1-01s (Thu Aug 23 11:28:34 2007):
	Added new color for traversed path

Release R1-01r (Wed Aug 22  6:28:37 2007):
	Removed plotting of uncertainty ellipses by default.  Can be
	reenabled from command line.  Added display of total number of
	elements plotted.

Release R1-01q (Mon Aug 20 23:50:14 2007):
	now centers on Alice by default (one less thing for person running
	startup scripts to have to worry about)

Release R1-01p (Mon Aug 13 15:58:52 2007):
	Added option to hide traversed path (red spline). Press 't' with window selected to turn the path on or off.

Release R1-01o (Sat Jul 28  2:14:28 2007):
		adding command line option "--show-ids" This causes the mapelement
	ids to be printed all the time, not just when that particular
	element is highlighted (I'm using this for working on fusion) In
	this mode, only the last element of the id vector is actually shown
	- this is the one that modules use to identify discrete elements -
	the others show originating module, etc. 


Release R1-01n (Mon Jun 18 22:39:53 2007):
	Minor modifications to allow compilation under MAC OS X.

Release R1-01m (Thu Jun  7 11:26:55 2007):
	Updated mapviewer to not display cost map by default.  The option
	--show-costmap or -c will turn on cost map viewing functionality. 
	Updated element selection so that the map element label is
	displayed along with the id if the label has been set.	Note that
	the label is limitted to 3 lines and 50 characters per line and
	will be clipped when sent.  The label is a vector of strings in a
	map element and can be set directly.

Release R1-01l (Wed Jun  6 19:51:00 2007):
	Optimization: changed the size of the image buffer used to paint
	the costmap, now it's always power-of-two sized. The latest release
        of bitmap (R1-00j or greater) can take advantage of this, but the
        previous one will still work, though.
        Added a command line option to select the colormap, --colormap=...
        and another "secret" option.

Release R1-01k (Wed Jun  6  0:20:13 2007):
	Implemented showing the costmap using a rainbow color shade (red =
	high cost, blue = low cost)

Release R1-01j (Tue Jun  5 22:40:59 2007):
	Adding visualization of cost map. It's somewhat slow, so it
        can be disabled with  --hide-costmap. I plan to optimize it
        soon, though.
        With --debug=1 you get some timings of the costmap drawing
        functions, and --debug=2 you get all the polygons on cout too.
        NOTE: it needs the latest version of bitmap and ocpspecs.

Release R1-01i (Sat Jun  2 15:43:16 2007):
  Tweaked log() calls to enable building under gcc 3.3.

Release R1-01h (Fri Jun  1  4:51:37 2007):
	minor changes to the main loop.  Should reduce spread errors

Release R1-01g (Tue May 29 23:39:31 2007):
	Added more colors... can't ever have enough

Release R1-01f (Mon May 28 13:03:31 2007):
	Added dark green color

Release R1-01e (Tue May 22 14:56:31 2007):
	Updated drawing functions in mapviewer. Now hit the d key to turn
	on and off state visualization.  State will now be visualized by
	default if the received data holds state info

Release R1-01d (Tue May 15 23:37:47 2007):
	cleaned up some element selection code to work more predictably.

Release R1-01c (Mon May 14 23:29:48 2007):
	<* Pleaase insert a release notes entry here. If none, then please delete these lines *>

Release R1-01b (Mon May 14 11:32:20 2007):
	Switched ymult back to -1.  Updated drawing functions to ignore
	elements outside of the window to speed up remote operation. 
	Implemented new interface with map module

Release R1-01a (Thu May 10 13:55:22 2007):
	Added option to auto center on alice (keybinding = 'a'). Also changed ymult to 1 because of frames issues.

Release R1-01 (Mon Apr 30 12:42:40 2007):
	Updated mapviewer to work with new map module.	Implements sub
	channel communication and variable length messages

Release R1-00h (Fri Mar 30 12:30:55 2007):
	fixed compile bug in MapWindow.cc

Release R1-00g (Sat Mar 17 11:44:39 2007):
	updated MapWindow.cc to plot Alice's position given by an element
	when an element is selected.  Updated COLOR_ prefix to MAP_COLOR_
	prefix labels to work with new MapElement enum definitions.  
	Selected element now draws in RED and label in GREEN instead of
	bold.

Release R1-00f (Tue Mar 13 16:04:35 2007):
	Fixed mirroring due to local frame bug in mapviewer.

Release R1-00e (Mon Mar 12 15:39:34 2007):
	Updated mapviewer to send out map elements corresponding to map
	view center point when 'o' is pressed.	If an element is selected
	in the mapviewer 'o' will send that element.  This is used for map
	debugging in testMap in the map module


Release R1-00d (Sun Mar 11 11:35:42 2007):
	updated viewer to draw alice when the alice state message is
	received.  Also clears objects from a clear message.  Pressing r in
	the window now resets the map and clears all objects.


Release R1-00c (Fri Mar  9 17:10:19 2007):
	updated mapviewer to work with new map talker placement

Release R1-00b (Sun Mar  4 17:42:18 2007):
	fixed library ordering bug in Makefile.yam

Release R1-00a (Thu Mar  1 19:27:37 2007):
	Added command line args.  

Release R1-00 (Tue Feb  6 14:49:53 2007):
	Created.









































