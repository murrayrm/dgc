/**********************************************************
 **
 **  MAPVIEWER.CC
 **
 **    Time-stamp: <2007-09-23 18:14:23 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Thu Feb  8 23:03:09 2007
 **
 **
 **********************************************************
 **  
 **  2D map viewer
 **
 **********************************************************/

#include <iomanip>

#include "MapWindow.hh"
#include "map/MapElementTalker.hh"
#include "skynettalker/SkynetTalker.hh"
#include "bitmap/BitmapParams.hh"
#include "interfaces/sn_types.h"
#include "ocpspecs/CostMapEstimator.hh"
#include <boost/serialization/vector.hpp>

// Cmd-line handling
#include "cmdline.h"
using namespace std;

using namespace bitmap;

class MapViewer : public CMapElementTalker
{
public:
  MapViewer() {
    cmapEst=NULL;
    recvflag = false;
  }


  ~MapViewer() {
    if (cmapEst != NULL)
      delete cmapEst;
  }
   
  int init();
  int add_element(MapElement& el);
  int parseCmdLine(int argc, char**argv);
  
  Fl_Window *mainwin;
  MapWindow *mapwin;
  bool ispaused;
  bool recvflag;
  gengetopt_args_info options;

  int skynetKey;
  int sendSubGroup;
  int recvSubGroup;
  
  CostMapEstimator* cmapEst;
};

int MapViewer::parseCmdLine(int argc, char**argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;
 
 // Fill out the recv subgroup number
  if (this->options.recv_subgroup_given)
    this->recvSubGroup = this->options.recv_subgroup_arg;
  else
    this->recvSubGroup = 0;

 // Fill out the send subgroup number
  if (this->options.send_subgroup_given)
    this->sendSubGroup = this->options.send_subgroup_arg;
  else
    this->sendSubGroup = -1;


  if (this->sendSubGroup == this->recvSubGroup){
    cout << "Error in MapViewer.  Can't run with send and receive subgroups set to same value." << endl;
    exit(1);
  }


   // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  return 0;
}

int MapViewer::init()
{
  initRecvMapElement(this->skynetKey,this->recvSubGroup);
  initSendMapElement(this->skynetKey);
  cmapEst = new CostMapEstimator(this->skynetKey, false/*GUI*/,
                  false/*thread*/, this->options.debug_arg);

  this->mainwin = new Fl_Window(500,500,"DGC Map Viewer");
  this->mapwin = new MapWindow(10,10, 
                               this->mainwin->w()-20, 
                               this->mainwin->h()-20);
  this->mapwin->debugLevel = this->options.debug_arg;

  this->mapwin->showIDs = this->options.show_ids_given;

this->mapwin->showUncertainty = this->options.show_uncertainty_given;

 this->mapwin->showTimeStopped = this->options.show_time_stopped_flag;

  // select colormap
  if (this->options.show_costmap_flag) {
      if (this->options.make_nok_happy_flag)
      {
          this->mapwin->initColormap(MapWindow::CMAP_PINK);
      }
      else if (this->options.colormap_given)
      {
          MapWindow::colormap_t colormap = MapWindow::CMAP_RAINBOW;
          if (string("gray") == this->options.colormap_arg) {
               colormap = MapWindow::CMAP_GRAYSCALE;
          } else if (string("bluered") == this->options.colormap_arg) {
              colormap = MapWindow::CMAP_BLUERED;
          } else if (string("rainbow") == this->options.colormap_arg) {
              colormap = MapWindow::CMAP_RAINBOW;
          } else if (string("pink") == this->options.colormap_arg) {
              colormap = MapWindow::CMAP_PINK;
          } else {
              cerr << "Unknown colormap " << this->options.colormap_arg
                   << ", using default" << endl;
          }

          this->mapwin->initColormap(colormap);
      }
  }

  ispaused = false;
  return 0;
}

int MapViewer::add_element(MapElement& el)
{

  MapId thisid = el.id;
  MapId tmpid;
	//	mapwin->elrate++;
  for (unsigned int i = 0; i < mapwin->localmap.size(); ++i){
    tmpid = mapwin->localmap[i].id;
    if (tmpid == thisid){
      if (el.type==ELEMENT_CLEAR){
        mapwin->localmap.erase(mapwin->localmap.begin()+i);
      }else{
        mapwin->localmap[i] = el;
      }
      return 0;
    }
  }
  if(el.type != ELEMENT_CLEAR)
    mapwin->localmap.push_back(el);
  return 0;
}

// Handle idle callbacks
void main_idle(MapViewer *self)
{
  int bytesRecv = 0;
  int bytesSent = 0;
  MapElement recvEl,sendEl;
  bytesRecv = self->recvMapElementTimedBlock(&recvEl,1,self->recvSubGroup);
  if (bytesRecv && !self->ispaused){
    self->add_element(recvEl);
    self->recvflag = true;
	}
  else if (self->options.show_costmap_flag &&
           self->cmapEst->updateMap(&self->mapwin->costMap,
                                    &self->mapwin->costMapMin,
                                    &self->mapwin->costMapMax)){
    self->mapwin->newCostMap = true;
    self->recvflag = true;
  }
  else{
    if (self->recvflag){
      self->mapwin->redraw();
      self->recvflag = false;
    }
    else if (self->mapwin->get_sendElement(sendEl)){
      bytesSent = self->sendMapElement(&sendEl,self->sendSubGroup);
    }
  }

  return;
}


int main(int argc, char *argv[]) {
 
  //  Fl_Window win(500, 500);
  //MapWindow  map_window(10, 10, win.w()-20, win.h()-20);
  glutInit(&argc, argv);

  MapViewer mapviewer;
  mapviewer.parseCmdLine(argc,argv);
  mapviewer.init();

  mapviewer.mainwin->resizable(mapviewer.mapwin);

  Fl::add_idle((void (*) (void*)) main_idle, &mapviewer);
  mapviewer.mainwin->show();
  return(Fl::run());
}
    
