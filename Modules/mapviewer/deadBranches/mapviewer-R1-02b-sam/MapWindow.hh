/**********************************************************
 **
 **  MAPWINDOW.HH
 **
 **    Time-stamp: <2007-10-19 10:35:07 team> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Feb 13 23:54:12 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#ifndef MAPWINDOW_H
#define MAPWINDOW_H

#include <FL/Fl.H>
#include <FL/Fl_Gl_Window.H>
#include <FL/gl.h>
#include <GL/glu.h>
#ifdef MACOSX
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <math.h>
#include <iostream>
#include <string>
#include <sstream>

#include "map/MapElement.hh"
#include "frames/point2_uncertain.hh"
#include "alice/AliceConstants.h"
#include "bitmap/Bitmap.hh"

using namespace std;

using namespace bitmap;

class MapWindow : public Fl_Gl_Window
{
  public:
  MapWindow(int X,int Y,int W,int H,const char*L=0);


  void draw();
  void reshape();

  void DrawGrid();
  void DrawEllipse(point2_uncertain pt);
  void DrawText(string txt, double x, double y , double width);
  void DrawElement(int index);

  void DrawVehicle(VehicleState &state, bool drawlocal = false);

  void DrawElements();
	void DrawDebugElements();
  void DrawShape();

  void DrawCostMap(const CostMap& map);

  bool isElementVisible(const MapElement & el);
  void select_element(double x, double y);

  void set_color(MapElementColorType color, int value =100);

  double scale_to_mult(double s);

  int set_view_screen_delta(double dx, double dy);

  int set_view(double x, double y);

  int set_scale_delta(double ds, double cx, double cy);

  int set_scale(double s);
  
  bool get_sendElement(MapElement &el);

  int handle(int event) ;
  string id_to_string(MapId& id);
  
  void reset_map();

  enum colormap_t {
    CMAP_GRAYSCALE,
    CMAP_BLUERED,
    CMAP_RAINBOW,
    CMAP_PINK
  };
  void initColormap(colormap_t colormap = CMAP_RAINBOW);
 
  double mouse_local_x;
  double mouse_local_y;
  double mouse_x;
  double mouse_y;
  double mouse_x_press;
  double mouse_y_press;
  double mouse_button;
  double center_x;
  double center_y;
  double scale;
    
  int elrate;
  
  int overflowcount;

  vector<MapElement> localmap; 
  vector<bool> updateflag;

  static const int COLORMAP_SIZE = 0x10000;
  vector<uint32_t> colormap;

  bool newCostMap; // set to true when a new costmap has been received but not yet drawn
  cost_t costMapMin;
  cost_t costMapMax;
	CostMap costMap;
  Bitmap<uint8_t, 4> mapImg; // color-coded image representing the costmap
  GLuint mapTxtId; // texture id for the map
  float mapScaleX, mapScaleY;

  int selected;
  vector<int> selectedarr;
  int selectedarr_index;
  point2arr selectedptarr;
  point2 selectedpt;


  bool sendFlag;
  MapElement sendEl;
  //static void Timer_CB(void *userdata) {
  //  Map_Window *pb = (Map_Window*)userdata;
  //  pb->redraw();
  // Fl::repeat_timeout(1.0/24.0, Timer_CB, userdata);
  //}

  int ymult;
  bool centerOnAlice;
  bool xAxisRight;

  // used to represent the most recent element drawn in the map which has state info
  VehicleState latestState;
  bool drawStateFlag;
  bool hideLocalStateFlag;
  bool centerLocalFlag;

  bool hideRndfFlag;

  int debugLevel;
  bool showIDs; //whether to print the mapelement IDs on the display
  bool showUncertainty;
  bool showTrav; //whether or not to show the trav path
  bool showTimeStopped;

};

template<class T>
inline string to_string(const T& x)
{
  ostringstream o;
  o << x;
  return o.str();
}

#endif
