/**********************************************************
 **
 **  MAPWINDOW.CC
 **
 **    Time-stamp: <2007-02-21 14:37:31 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Feb 13 23:51:20 2007
 **
 **
 **********************************************************
 **
 **  2D FLTK OpenGL Map Window
 **
 **********************************************************/


#include "MapWindow.hh"

using namespace std;
const float PI=3.141592653589793;


MapWindow::MapWindow(int X,int Y,int W,int H,const char*L) 
	: Fl_Gl_Window(X,Y,W,H,L) {
	center_x = 0;
	center_y = 0;
	scale = 0;
	selected = -1;
}



void MapWindow::draw() {
	
	if (!valid()) {
		gl_font(FL_HELVETICA ,12);
		valid(1);
	}
	reshape();			
	glClear(GL_COLOR_BUFFER_BIT);

	DrawGrid();	
	//        glPushMatrix();
	//	DrawShape();

	DrawElements();
	
	//	glPopMatrix();
	


 
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluOrtho2D(0,w(),0,h());
	

	string status_str = "Position x = " 
		+ to_string(mouse_local_x) + 
		" y = " + to_string(mouse_local_y) + "     ";

	if (selected <0)
		status_str = status_str + "  None selected";
	else{
		status_str = status_str + "  Element " + 
			id_to_string(localmap[selected].id) + " selected";
	}	
//sprintf(buf, "x=%f, y=%f ", mouse_local_x, mouse_local_y);
	
	glColor3f(1.0f, 1.0f, 0.0f);
  gl_font(FL_HELVETICA, 12);
	
	gl_draw(status_str.c_str() , 20,10);



}
string MapWindow::id_to_string(vector<int>& id)
{
	string out = "";
	int thisid;
	for (int i = 0; i < (int)id.size(); ++i){
		if (i>0)
			out = out + ".";
		thisid = id[i];
		out = out + to_string(thisid);
	}
	return out;
}

void MapWindow::reshape(){
	glViewport(0,0,w(),h());
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	double tmpscale = scale_to_mult(scale);
	
	double x1 = center_x-tmpscale*w()/2;
	double x2 = center_x+tmpscale*w()/2;
	double y1 = center_y-tmpscale*h()/2;
	double y2 = center_y+tmpscale*h()/2;
	

	
	gluOrtho2D(x1,x2,y1,y2);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

}

void MapWindow::DrawEllipse(point2_uncertain pt){

	double major = pt.max_var;
	double minor = pt.min_var;
	double ang = pt.axis;
	point2 center(pt.x,pt.y);
	int numpts = 100;
	point2 ptarr[numpts];
	point2 thispt;
	double dang = 2*PI/(numpts);
	double thisR = 0;
	double thisAng = 0;
	double smallval = .000000001;

	if (major<=0)
		return;
	if (minor<=0)
		minor = smallval;
	
			
	for (int i= 0; i<numpts; ++i){
		thisR = sqrt(1/ (pow((cos(thisAng)/major),2) 
										 + pow((sin(thisAng)/minor),2)) );
		thispt.x = thisR*cos(thisAng+ang);
		thispt.y = thisR*sin(thisAng+ang);
		thispt = thispt+center;
		ptarr[i] = thispt;
		
		thisAng = thisAng+dang;
		
	}
	
	glLineWidth(1);
	glPointSize(3);			
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_LINE_LOOP);
	for (int i =0 ; i<numpts; ++i){
		glVertex2f(ptarr[i].x,ptarr[i].y);
	}
	glEnd();

}

void MapWindow::DrawText(string txt, double x, double y , double width)
{
	int len = (int)txt.size();
	double tmpscale = width/104.76;

	float widthtot =0;
	for (int i =0 ; i < len ; ++i){
		widthtot = widthtot + glutStrokeWidth(GLUT_STROKE_ROMAN,txt[i]);
	}
	widthtot = widthtot*tmpscale;

	glPushMatrix();
	glTranslatef(x-(double)widthtot/2, y+width/2, 0);
	glScalef(tmpscale,tmpscale,tmpscale);
	for (int i =0 ; i < len ; ++i){
		glutStrokeCharacter(GLUT_STROKE_ROMAN, txt[i]);
	}
	glPopMatrix();

}

void MapWindow::DrawGrid()
{
	double minx, maxx, miny, maxy;
	double tmpscale = scale_to_mult(scale);
	minx = center_x-tmpscale*w()/2;
	maxx = center_x+tmpscale*w()/2;
	miny = center_y-tmpscale*h()/2;
	maxy = center_y+tmpscale*h()/2;
	
	//cout << "minx = " << minx 
	//		 << "maxx = " << maxx 
	//		 << "miny = " << miny 
	//		 << "maxy = " << maxy << endl;
	
	double exponent = log10(tmpscale);
	double rem = exponent-floor(exponent);
	exponent = floor(exponent);
	double delta = pow(10,exponent+2);
	if (rem > log10(5))
		delta = 5*delta;
	else if (rem > log10(2))
		delta = 2*delta;


	double minx_grid = floor((minx)/delta)*delta;
	double maxx_grid = ceil((maxx)/delta)*delta;

	double miny_grid = floor((miny)/delta)*delta;
	double maxy_grid = ceil((maxy)/delta)*delta;
	

	glLineWidth(1);
	glColor3f(0.3, 0.3, 0.3);
	glBegin(GL_LINE_STRIP);
	for (double x =minx_grid ; x<=maxx_grid ; x+=delta){
		glVertex2f(x,miny_grid);
		glVertex2f(x,maxy_grid);
		glVertex2f(x+delta,maxy_grid);
	}
	glEnd();
	


	glBegin(GL_LINE_STRIP);
	for (double y =miny_grid ; y<=maxy_grid ; y+=delta){
		glVertex2f(minx_grid,y);
		glVertex2f(maxx_grid,y);
		glVertex2f(maxx_grid,y+delta);
	}
	glEnd();

	glColor3f(0.5, 0.5, 0.5);

	
	string axis_str;
	
	gl_font(FL_HELVETICA,12);
	axis_str = "(m)" ;

	double xoffset = tmpscale*10;
	double yoffset = tmpscale*20;

	gl_draw(axis_str.c_str(),(float)(minx+xoffset),
					(float)(maxy-yoffset));
	
	for (double x =minx_grid ; x<=maxx_grid ; x+=delta){
		axis_str =  to_string(x) ;
		gl_font(FL_HELVETICA, 12);
		if (x> minx+3*xoffset)
			gl_draw(axis_str.c_str() , (float)x, (float)(maxy-yoffset));
	}

	for (double y =miny_grid ; y<=maxy_grid ; y+=delta){
		axis_str =  to_string(y) ;
		gl_font(FL_HELVETICA, 12);
		if (y < maxy-2*yoffset)
			gl_draw(axis_str.c_str() , (float)(minx+xoffset), (float)y);
	}

	double csze = tmpscale*5;
	glBegin(GL_LINE_STRIP);
	glVertex2f(center_x+csze,center_y+csze);
	glVertex2f(center_x-csze,center_y-csze);
	glEnd();

	glBegin(GL_LINE_STRIP);
	glVertex2f(center_x-csze,center_y+csze);
	glVertex2f(center_x+csze,center_y-csze);
	glEnd();

	
	//	cout << "delta "<< delta << "  mingrid " << minx_grid << endl;
	
	
}



void MapWindow::DrawElement(int index) {
	if ((index<0) | (index>= (int)this->localmap.size()))
		return;

	MapElement el = this->localmap[index];

	glPointSize(3);
		glColor3f(0.5, 0.5, 0.5);
	if (index == selected){
		//glLineWidth(2);
		glPointSize(5);	
		glColor3f(1.0, 1.0, 1.0);
		//	textsize = 2*textsize;
	}


	glBegin(GL_POINTS);
	glVertex2f(el.center.x,el.center.y);
	glEnd();



	if (el.geometry.size()>0){
			
		//--------------------------------------------------
		// draw geometry
		//--------------------------------------------------
		glLineWidth(1);
		glPointSize(2);				
		if (index == selected){
			glLineWidth(3);
			glPointSize(4);		
		}
		
		glColor3f(0.0, 1.0, 1.0);

		switch(el.geometry_type){
		case GEOMETRY_UNDEF:			
		case GEOMETRY_POINTS:
			glBegin(GL_POINTS);
			for (int i =0 ; i< (int) el.geometry.size(); ++i){
				glVertex2f(el.geometry[i].x, el.geometry[i].y);
			}
			glEnd();
			break;
		case GEOMETRY_ORDERED_POINTS:
	glColor3f(0.0, .50, .50);
			glBegin(GL_LINE_STRIP);
			for (int i =0 ; i< (int)el.geometry.size() ; ++i){
				glVertex2f(el.geometry[i].x, el.geometry[i].y);
			}
			glEnd();
	glColor3f(0.0, 1.0, 1.0);
			glBegin(GL_POINTS);
			for (int i =0 ; i< (int) el.geometry.size(); ++i){
				glVertex2f(el.geometry[i].x, el.geometry[i].y);
			}
			glEnd();
			break;
		case GEOMETRY_LINE:
			glBegin(GL_LINE_STRIP);
			for (int i =0 ; i< (int)el.geometry.size() ; ++i){
				glVertex2f(el.geometry[i].x, el.geometry[i].y);
			}
			glEnd();
			glBegin(GL_POINTS);
			for (int i =0 ; i< (int) el.geometry.size(); ++i){
				glVertex2f(el.geometry[i].x, el.geometry[i].y);
			}
			glEnd();
			break;
		case GEOMETRY_EDGE:
			glBegin(GL_LINE_STRIP);
			for (int i =0 ; i< (int)el.geometry.size() ; ++i){
				glVertex2f(el.geometry[i].x, el.geometry[i].y);
			}
			glEnd();
			glBegin(GL_POINTS);
			for (int i =0 ; i< (int) el.geometry.size(); ++i){
				glVertex2f(el.geometry[i].x, el.geometry[i].y);
			}
			glEnd();
			break;
		case GEOMETRY_POLY:
			glColor3f(0.0, 1.0, 1.0);
			glBegin(GL_LINE_LOOP);
			for (int i =0 ; i< (int)el.geometry.size() ; ++i){
				glVertex2f(el.geometry[i].x, el.geometry[i].y);
			}
			glEnd();
			glBegin(GL_POINTS);
			for (int i =0 ; i< (int) el.geometry.size(); ++i){
				glVertex2f(el.geometry[i].x, el.geometry[i].y);
			}
			glEnd();
			break;
		}
			
	}


	if (el.length>0){
		int i;
		double tmpx[4];
		double tmpy[4];
		double len,wid,orient;
		len = el.length/2;
		wid = el.width/2;
		orient = el.orientation;
		double co = cos(orient);
		double so = sin(orient);
	glLineWidth(1);
		glPointSize(3);				
		if (index == selected){
			glLineWidth(3);
			glPointSize(5);		
		}
		glColor3f(1.0, 0.0, 1.0);

		switch(el.center_type){
		case CENTER_UNDEF:	
		case CENTER_POINT:
		case CENTER_POSE:
			break;
		case CENTER_BOUND_RADIUS:
			
			glBegin(GL_LINE_LOOP);
		for (double ang =0 ; ang<2*PI; ang = ang+PI/50){
			glVertex2f(el.center.x+len*cos(ang),el.center.y+len*sin(ang));
		}
		glEnd();
		break;
		
		case CENTER_BOUND_BOX:
			tmpx[0] = el.center.x+(len*co -wid*(so));
			tmpy[0] = el.center.y+(len*so +wid*(co));
			
			tmpx[1] = el.center.x+(-len*co -wid*(so));
			tmpy[1] = el.center.y+(-len*so +wid*(co));
			
			tmpx[2] = el.center.x+(-len*co +wid*(so));
			tmpy[2] = el.center.y+(-len*so -wid*(co));
			
			tmpx[3] = el.center.x+(len*co +wid*(so));
		tmpy[3] = el.center.y+(len*so -wid*(co));
		
		glBegin(GL_LINE_LOOP);
		for (i =0 ; i<4; ++i){
			glVertex2f(tmpx[i],tmpy[i]);
		}
		glEnd();
		break;

		case CENTER_BOUND_LINES:
			break;
		}


		
		
	

	}
	

	//if (el.label.size()>0){
	//	for (int i=0; i <el.label.size(); ++i){
	//		gl_draw(el->label[i].c_str(),(int)(el->center.x), (int)(el->center.y + i*12));
	//	}
	//}

	if (el.center.max_var>0 && el.center.min_var>0){
		DrawEllipse(el.center);
	}
	
	//DrawVehicle(index);

	glLineWidth(1);
	glPointSize(3);
	double textsize = 1;
		
	glColor3f(.50, .50, .50);
	if (index == selected){
	glColor3f(1.0, 1.0, 1.0);		
		}
		

	DrawText(id_to_string(el.id),el.center.x, el.center.y, textsize);
}

void MapWindow::DrawVehicle(int index) {
	if ((index<0) | (index>= (int)this->localmap.size()))
		return;
		
	MapElement el = this->localmap[index];
	double cx = el.state.localX;
	double cy = el.state.localY;
	double orient = el.state.localYaw;

	double xvel = el.state.localXVel;
	double yvel = el.state.localYVel;

	double dfront = DIST_REAR_AXLE_TO_FRONT;
	double drear = DIST_REAR_TO_REAR_AXLE;
	double dside =VEHICLE_WIDTH/2;

	double tmpx[4];
	double tmpy[4];
	
	double co = cos(orient);
	double so = sin(orient);
	
	tmpx[0] = cx+(dfront*co -dside*so);
	tmpy[0] = cy+(dfront*so +dside*co);

	tmpx[1] = cx+(-drear*co -dside*so);
	tmpy[1] = cy+(-drear*so +dside*co);

	tmpx[2] = cx+(-drear*co +dside*so);
	tmpy[2] = cy+(-drear*so -dside*co);

	tmpx[3] = cx+(dfront*co +dside*so);
	tmpy[3] = cy+(dfront*so -dside*co);

	glLineWidth(1);
	glPointSize(3);			
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_POLYGON);
	for (int i =0 ; i<4; ++i){
		glVertex2f(tmpx[i],tmpy[i]);
	}
	glEnd();
	
	glBegin(GL_LINE_STRIP);
	glVertex2f(cx,cy);
	glVertex2f(cx+xvel,cy+yvel);
	glEnd();
	

}

void MapWindow::DrawElements(){
	for (int i=0; i< (int)this->localmap.size(); ++i){
		DrawElement(i);
	}
	if (selected >=0){
	
		DrawElement(selected);

	}
}
	
void MapWindow::DrawShape() {
       
	// Draw shape
	glColor3f(1.0, 1.0, 1.0);
      
	glBegin(GL_LINE_LOOP);
	glVertex2f(0.0, 0.0);
	glVertex2f(.20, .30);
	glVertex2f(.30, .20);
	glVertex2f(0.0, 0.0);
	glVertex2f(20, 30);
	glVertex2f(30, 20);
		
	glEnd();
			
        
}



void MapWindow::select_element(double x, double y)
{
	double dx, dy, orient, length, width;
	double newdx, newdy;
	vector<int> tmpselected;
	double minboundthresh = 2; 
	
	for (int i = 0 ; i < (int)localmap.size() ; ++i){
		
		dx = x-localmap[i].center.x;
		dy = y-localmap[i].center.y;
		orient = localmap[i].orientation;
		length = localmap[i].length;
		if (length<minboundthresh)
			length = minboundthresh;//max of those values
		width = localmap[i].width;
		if (width<minboundthresh)
			width = minboundthresh; // max of those values
		
		newdx = fabs(dx*cos(-orient)-dy*sin(-orient));
		newdy = fabs(dx*sin(-orient)+dy*cos(-orient));

		if (newdx<=(length/2) && newdy<=(width/2)){
			tmpselected.push_back(i);
		}
	
	}
	if (tmpselected == selectedarr){
		if (selectedarr.size()){
			selectedarr_index++;
			if (selectedarr_index>=(int)selectedarr.size())
				selectedarr_index = -1;
		}
	}
	else{
		selectedarr = tmpselected;
		selectedarr_index = 0;
	}
	if (selectedarr.size() && selectedarr_index>=0)
		selected = selectedarr[selectedarr_index];
	else
		selected = -1;

	if (selected >=0){
		localmap[selected].print();
		if (localmap[selected].state.timestamp!=0)
			localmap[selected].print_state();
	}
	reshape();
	redraw();
}

double MapWindow::scale_to_mult(double s){
	
	return (pow(2,s-3));
	//	if (s>=0)
	//		return(.1*(1+s));
	//	else
	//		return(.1/(1-s));
}

int MapWindow::set_view_screen_delta(double dx, double dy){
	center_x = center_x + scale_to_mult(scale)*(dx);
	center_y = center_y - scale_to_mult(scale)*(dy);
	reshape();
	redraw();
	return 0;
}

int MapWindow::set_view(double x, double y){
	center_x = x;
	center_y = y;
	reshape();
	redraw();
	return 0;

}
int MapWindow::set_scale_delta(double ds, double cx, double cy){

	double scalerat = scale_to_mult(scale)-scale_to_mult(scale+ds);
	double dx = scalerat*(cx-0.5*w())/scale_to_mult(scale+ds);
	double dy = scalerat*(cy-0.5*h())/scale_to_mult(scale+ds);
	
	scale  = scale+ds;
	return(set_view_screen_delta(dx, dy));
}
int MapWindow::set_scale(double s){
	scale = s;
	reshape();
	redraw();
	return 0;
}


int MapWindow::handle(int event) {
	switch(event) {
	case FL_PUSH:
		mouse_button = Fl::event_button();
		mouse_x = Fl::event_x();
		mouse_y = Fl::event_y();
		mouse_x_press = Fl::event_x();
		mouse_y_press = Fl::event_y();
		//if (mouse_button==2){
		//  set_view_screen_delta(Fl::event_x()-0.5*w(),Fl::event_y()-0.5*h());
		//}
		mouse_local_x = center_x + scale_to_mult(scale)*(mouse_x-0.5*w());
		mouse_local_y = center_y - scale_to_mult(scale)*(mouse_y-0.5*h());; 
		
		//		cout << "mlx = " << mouse_local_x << " mly = " << mouse_local_y <<endl;

		if (mouse_button==2){
			select_element(mouse_local_x, mouse_local_y);
		
		}

		reshape();
		redraw();
		return 1;

		
	case FL_DRAG:
				
		if (mouse_button == 1){
			set_view_screen_delta(mouse_x-Fl::event_x(),mouse_y-Fl::event_y());
		}
		if (mouse_button == 3){
				
			double dscale_x = (mouse_x-Fl::event_x())/20;
			double dscale_y = (-mouse_y+Fl::event_y())/20;
				
			if (fabs(dscale_x) > fabs(dscale_y))
				set_scale_delta(dscale_x,mouse_x_press,mouse_y_press);
			else
				set_scale_delta(dscale_y,mouse_x_press,mouse_y_press);
				
		}
		mouse_x = Fl::event_x();
		mouse_y = Fl::event_y();
	

		return 1;
	case FL_MOUSEWHEEL:
			
		if (Fl::event_key()==65261){
			set_scale(scale+.5);
		}
		else if (Fl::event_key()==65260){
			set_scale(scale-.5);
		}
		return 1;
			
	case FL_RELEASE:

		
		if ((mouse_x_press == Fl::event_x()) 
				&& (mouse_y_press == Fl::event_y())){
				
			if (mouse_button==1){
				select_element(mouse_local_x, mouse_local_y);

			}

			reshape();
			redraw();
		}
   
		return 1;
	case FL_FOCUS :
	case FL_UNFOCUS :
		
		return 1;
	case FL_KEYBOARD:
		switch(Fl::event_key()) {
		case 'q':
			exit(1);
			break;
		case 'z':
			set_scale(0);
			return 1;
		case 'c':
			set_view(0,0);
			return 1;
		case 's':
			mouse_local_x = center_x;
			mouse_local_y = center_y;
			select_element(center_x,center_y);
			return 1;

		case 65365: // pgup, zoom in
			set_scale(scale-0.1);
			return 1;
		case 65366: // pgdn, zoom out
			set_scale(scale+0.1);
			return 1;
		case 65364: // down arrow, scroll down
			set_view_screen_delta(0,10);
			return 1;
		case 65362: // up arrow, scroll up
			set_view_screen_delta(0,-10);
			return 1;
		case 65361: // left arrow, scroll left
			set_view_screen_delta(-10,0);
			return 1;
		case 65363: // right arrow, scroll right
			set_view_screen_delta(10,0);
			return 1;
		default:
			return 1;
		}
		return 1;
	case FL_SHORTCUT:
		return 1;
	default:
		// pass other events to the base class...
		return Fl_Gl_Window::handle(event);
	}
}



