/**********************************************************
 **
 **  MAPWINDOW.HH
 **
 **    Time-stamp: <2007-05-22 14:08:36 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Feb 13 23:54:12 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#ifndef MAPWINDOW_H
#define MAPWINDOW_H

#include <FL/Fl.H>
#include <FL/Fl_Gl_Window.H>
#include <FL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <math.h>
#include <iostream>
#include <string>
#include <sstream>

#include "map/MapElement.hh"
#include "frames/point2_uncertain.hh"
#include "alice/AliceConstants.h"
#include "bitmap/Bitmap.hh"

using namespace std;

using namespace bitmap;

class MapWindow : public Fl_Gl_Window
{
  public:
  MapWindow(int X,int Y,int W,int H,const char*L=0);



  void draw();
  void reshape();

  void DrawGrid();
  void DrawEllipse(point2_uncertain pt);
  void DrawText(string txt, double x, double y , double width);
  void DrawElement(int index);

  void DrawVehicle(VehicleState &state);

  void DrawElements();
  void DrawShape();

  void DrawCostMap(const CostMap& map);

  bool isElementVisible(const MapElement & el);
  void select_element(double x, double y);

  void set_color(MapElementColorType color, int value =100);

  double scale_to_mult(double s);

  int set_view_screen_delta(double dx, double dy);

  int set_view(double x, double y);

  int set_scale_delta(double ds, double cx, double cy);

  int set_scale(double s);
  
  bool get_sendElement(MapElement &el);

  int handle(int event) ;
  string id_to_string(MapId& id);
  
  void reset_map();
 
  double mouse_local_x;
  double mouse_local_y;
  double mouse_x;
  double mouse_y;
  double mouse_x_press;
  double mouse_y_press;
  double mouse_button;
  double center_x;
  double center_y;
  double scale;
  
  
  vector<MapElement> localmap; 

  bool newCostMap; // set to true when a new costmap has been received but not yet drawn
  cost_t costMapMin;
  cost_t costMapMax;
	CostMap costMap;
  GLuint mapTxtId; // texture id for the map
  float mapScaleX, mapScaleY;

  int selected;
  vector<int> selectedarr;
  int selectedarr_index;
  point2arr selectedptarr;
  point2 selectedpt;


  bool sendFlag;
  MapElement sendEl;
  //static void Timer_CB(void *userdata) {
  //  Map_Window *pb = (Map_Window*)userdata;
  //  pb->redraw();
  // Fl::repeat_timeout(1.0/24.0, Timer_CB, userdata);
  //}

  int ymult;
  bool centerOnAlice;
  bool xAxisRight;

  // used to represent the most recent element drawn in the map which has state info
  VehicleState latestState;
  bool drawStateFlag;

  int debugLevel;
};

template<class T>
inline string to_string(const T& x)
{
  ostringstream o;
  o << x;
  return o.str();
}

#endif
