/**
 * \file gcplc.c
 * \brief Code for interfacing between gcdrive and the PLC library.
 *
 * \author Joshua Oreman
 * \date 16 July 2007
 *
 * This file provides a "glue" layer of sorts between gcdrive and
 * the Programmable Logic Controller that handles most of the
 * actuators. The PLC takes a rather long time (~50ms) to respond
 * to commands, so we want to send *all* our outputs at once.
 * In addition, you can read status in a separate thread so as to avoid
 * unnecessary delays in the main loop. Finally, when speed is at
 * a premium, it's possible to blast write requests onto the wire
 * without waiting for a response; this gives almost instantaneous
 * update of the actuators, at the expense of error conditions
 * not being flagged.
 *
 * This interface may well be useful to more than just gcdrive.
 */

#include <pthread.h>
/* This is apparently not declared as it should be: */
extern int pthread_mutexattr_settype (pthread_mutexattr_t *attr, int type);
#ifdef MACOSX
/* OS X uses a different name for this attribute */
#define PTHREAD_MUTEX_RECURSIVE_NP PTHREAD_MUTEX_RECURSIVE
#endif

#include <errno.h>
#include <sys/time.h>
#include <stdlib.h>
#include "gcplc.h"

/** Modbus object used for all reads from the PLC. */
static modbus_t *mb_read;
/** Modbus object used for all writes to the PLC. */
static modbus_t *mb_write;

/**
 * Read-write lock object used for handling reads.
 *
 * It is read-locked by all the read accessor functions
 * (gcplc_read_*, gcplc_peek_*) and write-locked by
 * gcplc_do_read().
 *
 * The semantics of a read-write lock are such that
 * at any time, it may either be locked exactly once
 * for writing and no times for reading, or locked
 * no times for writing and an arbitrary number of
 * times for reading. This allows multiple
 * accessor functions to execute simultaneously,
 * but still guards against race conditions.
 */
static pthread_rwlock_t rwl_reading;

/** Mutex object used for handling writes. */
static pthread_mutex_t mtx_writing;

/** Condition object used by gcplc_do_cond_write(). Harmless if not used. */
static pthread_cond_t cond_write_ready;

/** The IP address we were originally asked to connect to. */
static const char *plc_ip;

/** Are we write-blasting? */
static int write_blasting;

/** Default timeout, in milliseconds, to wait between sending a READ query and
    getting the reply back. */
#define READ_TIMEOUT    500

/** Default timeout, in milliseconds, to wait between sending a WRITE comman d and
    getting the confirmation back. Has no effect if write-blasting mode is ON. */
#define WRITE_TIMEOUT   500

/** The register to write to when doing a "ping". */
#define PING_WRITE_REG  2028

/** The register to read from when doing a "ping". The ping is valid if the value written to
    PING_WRITE_REG can be read from PING_READ_REG before READ_TIMEOUT ms have passed. */
#define PING_READ_REG   2029

/** The first register to cache. */
#define CACHE_BASE      2000

/** The number of registers to cache. */
#define NR_REGS_CACHED  27

/** Our read cache. */
static modbus_reg_t read_cache[NR_REGS_CACHED];

/** Number of 2ms increments that may pass between writing commands to the PLC without the PLC putting
    the vehicle in DISABLE mode. */
#define WATCHDOG_TIMER_INTERVAL   0xFFFF

/** Write cache structure. This is structured to be able to be written byte-for-byte to the PLC. MUST 
    be used on a little-endian machine! */
struct write_cache_struct {
    float analog[NR_ANALOG_OUT];  /**< The analog outputs. */
    modbus_reg_t digital;        /**< The digital outputs, packed into one register. */
    modbus_reg_t watchdog;       /**< The watchdog timer; should ALWAYS be set to WATCHDOG_TIMER_INTERVAL. */
} __attribute__((packed));

/** Our write cache. */
static struct write_cache_struct write_cache;

/** Write cache dirty? Bitwise OR of DIRTY_* flags. */
static int write_cache_dirty;

/** Something has been modified in write_cache_digital but not yet written to the device. */
#define DIRTY_DIGITAL 1

/** Something has been modified in write_cache_analog but not yet written to the device. */
#define DIRTY_ANALOG  2

/** Write blasting default. (0 to turn off, anything else to turn on) */
#define DEFAULT_WRITE_BLASTING  0

int gcplc_init (const char *ip_addr) 
{
    static int inited = 0;
    if (!inited) {
        /* Initialize locks and Modbus objects if it's the first time we were called. */
        pthread_mutexattr_t attr;
        pthread_mutexattr_init (&attr);
        pthread_mutexattr_settype (&attr, PTHREAD_MUTEX_RECURSIVE_NP);
        pthread_mutex_init (&mtx_writing, &attr); // This must be a recursive mutex!
        pthread_rwlock_init (&rwl_reading, NULL);
        pthread_cond_init (&cond_write_ready, NULL);
        mb_read = mb_write = NULL;
        write_blasting = DEFAULT_WRITE_BLASTING;
        write_cache.watchdog = WATCHDOG_TIMER_INTERVAL;  /* don't change this field later! */
        
        inited = 1;
    }

    /* gcplc_reconnect() doesn't mind if the modbus objects are NULL, so just call it now. */
    plc_ip = ip_addr;
    return gcplc_reconnect();
}

void gcplc_set_wblast (int wb) 
{
    write_blasting = wb;
    if (mb_write) modbus_set_timeout (mb_write, WRITE_TIMEOUT, write_blasting ? 0 : WRITE_TIMEOUT);
}

int gcplc_ping() 
{
    modbus_reg_t magic = rand() & 0xFFFF; /* clamp it to 16-bits */
    modbus_reg_t more_magic;
    struct timeval abracadabra, alakazam; /* well, we *are* dealing with magic numbers here */

    if (!mb_write || !mb_read) {
        errno = EFAULT;
        return -1;
    }

    gettimeofday (&abracadabra, NULL);

    modbus_write_registers (mb_write, PING_WRITE_REG, 1, &magic);

    while (1) {
        int res = modbus_read_registers (mb_read, PING_READ_REG, 1, &more_magic);
        if (res == 0)
            return -1;
        if (more_magic == magic)
            break;

        gettimeofday (&alakazam, NULL);
        int diff = ((alakazam.tv_sec - abracadabra.tv_sec) * 1000) +
                   ((alakazam.tv_usec - abracadabra.tv_usec) / 1000);
        if (diff > READ_TIMEOUT) {
            errno = ETIMEDOUT;
            return -1;
        }
    }

    return 0;
}

int gcplc_reconnect() 
{
    int i;

    pthread_rwlock_wrlock (&rwl_reading);

    if (mb_read) modbus_destroy (mb_read);
    mb_read = modbus_create (plc_ip);
    if (mb_read) modbus_set_timeout (mb_read, READ_TIMEOUT, READ_TIMEOUT);

    pthread_rwlock_unlock (&rwl_reading);
    

    pthread_mutex_lock (&mtx_writing);
    
    if (mb_write) modbus_destroy (mb_write);
    mb_write = modbus_create (plc_ip);
    if (mb_write) modbus_set_timeout (mb_write, WRITE_TIMEOUT, write_blasting ? 0 : WRITE_TIMEOUT);
    
    pthread_mutex_unlock (&mtx_writing);

    
    if (!mb_read || !mb_write) return -1;

    /* Fill in the write cache so we don't write zeroes for things not modified - this fixe
       bugs like the fct that the transmission likes to change to Park. */
    if (gcplc_do_read() < 0)
        return -1;
    for (i = 0; i < NR_ANALOG_OUT; i++) {
        write_cache.analog[i] = gcplc_peek_analog (i);
    }
    write_cache.digital = read_cache[MODBUS_REG_DO - CACHE_BASE];

    return 0;
}

int gcplc_do_read() 
{
    int res;

    if (!mb_read) {
        errno = EINVAL;
        return -1;
    }
    
    pthread_rwlock_wrlock (&rwl_reading);

    /* Read all the I/O registers - one contiguous block. */
    res = modbus_read_registers (mb_read, CACHE_BASE, NR_REGS_CACHED, read_cache);

    pthread_rwlock_unlock (&rwl_reading);

    if (!res)
        return -1;
    
    return 0;
}

int gcplc_do_write() 
{
    int res;

    if (!mb_write || !mb_read) {
        errno = EINVAL;
        return -1;
    }

    pthread_mutex_lock (&mtx_writing);

    /* This relies on a specific register ordering: analogs, then digital, then
       watchdog. And you must run it on a LITTLE ENDIAN MACHINE! */

    res = modbus_write_registers (mb_write, MODBUS_REG_AO(0), 2*NR_ANALOG_OUT + 2 /* DO + watchdog */, (modbus_reg_t *)&write_cache);

    /* Function returns nonzero for success. If both were successful, res will be
       nonzero now. If that's the case, mark everything non-dirty. */
    if (res != 0)
        write_cache_dirty = 0;

    pthread_mutex_unlock (&mtx_writing);
    
    if (!res)
        return -1;

    return 0;
}

int gcplc_do_cond_write() 
{
    int res;
    
    pthread_mutex_lock (&mtx_writing);
    while (!write_cache_dirty) {
        pthread_cond_wait (&cond_write_ready, &mtx_writing);
    }
    res = gcplc_do_write(); // will deadlock if mtx_writing isn't recursive (which it is)
    pthread_mutex_unlock (&mtx_writing);

    return res;
}

int gcplc_read_digital (int nr) 
{
    int res;
    pthread_rwlock_rdlock (&rwl_reading);
    res = ((read_cache[MODBUS_REG_DI - CACHE_BASE] & (1 << nr)) >> nr);
    pthread_rwlock_unlock (&rwl_reading);
    return res;
}

int gcplc_peek_digital (int nr) 
{
    int res;
    pthread_rwlock_rdlock (&rwl_reading);
    res = ((read_cache[MODBUS_REG_DO - CACHE_BASE] & (1 << nr)) >> nr);
    pthread_rwlock_unlock (&rwl_reading);
    return res;
}

void gcplc_write_digital (int nr, unsigned char val) 
{
    pthread_mutex_lock (&mtx_writing);

    if (val)
        write_cache.digital |= (1 << nr);
    else
        write_cache.digital &= ~(1 << nr);

    write_cache_dirty |= DIRTY_DIGITAL;
    pthread_cond_broadcast (&cond_write_ready);
    
    pthread_mutex_unlock (&mtx_writing);
}

int gcplc_read_digitals (int base, int nbits) 
{
    int res;
    pthread_rwlock_rdlock (&rwl_reading);
    res = ((read_cache[MODBUS_REG_DI - CACHE_BASE] & (((1 << nbits) - 1) << base)) >> base);
    pthread_rwlock_unlock (&rwl_reading);
    return res;
}

int gcplc_peek_digitals (int base, int nbits) 
{
    int res;
    pthread_rwlock_rdlock (&rwl_reading);
    res = ((read_cache[MODBUS_REG_DO - CACHE_BASE] & (((1 << nbits) - 1) << base)) >> base);
    pthread_rwlock_unlock (&rwl_reading);
    return res;
}

void gcplc_write_digitals (int base, int nbits, int val) 
{
    pthread_mutex_lock (&mtx_writing);

    /* The bits we're modifying: */
    int mask = (((1 << nbits) - 1) << base);

    write_cache.digital &= ~mask;  /* clear the old value */
    write_cache.digital |= ((val << base) & mask);  /* set the new one, make sure it doesn't "leak" */

    write_cache_dirty |= DIRTY_DIGITAL;
    pthread_cond_broadcast (&cond_write_ready);

    pthread_mutex_unlock (&mtx_writing);
}

float gcplc_read_analog (int nr)
{
    if (nr < 0 || nr >= NR_ANALOG_IN) return 0.0;

    float res;
    pthread_rwlock_rdlock (&rwl_reading);
    res = *(float *)&read_cache[MODBUS_REG_AI(nr) - CACHE_BASE];
    pthread_rwlock_unlock (&rwl_reading);
    return res;
}

float gcplc_peek_analog (int nr)
{
    if (nr < 0 || nr >= NR_ANALOG_OUT) return 0.0;

    float res;
    pthread_rwlock_rdlock (&rwl_reading);
    res = *(float *)&read_cache[MODBUS_REG_AO(nr) - CACHE_BASE];
    pthread_rwlock_unlock (&rwl_reading);
    return res;
}

void gcplc_write_analog (int nr, float val) 
{
    if (nr < 0 || nr >= NR_ANALOG_OUT) return;

    pthread_mutex_lock (&mtx_writing);

    write_cache.analog[nr] = val;

    write_cache_dirty |= DIRTY_ANALOG;    
    pthread_cond_broadcast (&cond_write_ready);

    pthread_mutex_unlock (&mtx_writing);
}

int gcplc_read_estop() 
{
    return read_cache[MODBUS_REG_ESTOP - CACHE_BASE];
}
