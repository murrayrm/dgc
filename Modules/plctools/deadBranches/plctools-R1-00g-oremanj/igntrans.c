/*!
 *\file igntrans.c
 *\low level driver for the transmission actuator
 * 
 *\author Noele Norris
 *\date 11 July 2007
 *
 *uses PLC, communicating through Modubs TCP/IP protoc
 */
  

#include <stdlib.h>
#include <errno.h>
#include <pthread.h>

#include "plc.h"

/* sanity checks transmission status */
int plc_trans_check()
{
    if(plc_trans_getposition() == -1)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

/* not... */
int plc_ign_check() 
{
    errno = ENOSYS;
    return -1;
}
/* ...yet... */
int plc_ign_setposition (int position)
{
    errno = ENOSYS;
    return -1;
}

/* ...implemented... */
int plc_ign_getposition (void)
{
    errno = ENOSYS;
    return -1;
}


/* Sets the position of the transmission.
 * The ERC that controls the transmission has four positions based on the signals
 * from four wires.
 */
int plc_trans_setposition (int position)
{
    int relays = 0;
                                          /* set bit E 1 0 */
    if (position == T_PARK)    relays = 4;    /*  to 1 0 0 - position 1 on ERC */
    if (position == T_REVERSE) relays = 5;    /*  to 1 0 1 - position 2 on ERC */
    if (position == T_NEUTRAL) relays = 6;    /*  to 1 1 0 - position 3 on ERC */
    if (position == T_DRIVE)   relays = 7;    /*  to 1 1 1 - position 4 on ERC */
    if (position == T_OFF)     relays = 0;    /*  to 0 x x - ERC off */

    if (relays == 0) return -1; /* anything else - invalid */

    gcplc_write_digitals (TRANS_POS_DO /* base */, TRANS_POS_BITS /* bits */, relays);
    
    return 0;
}



/*Returns current position of the transmission*/
int plc_trans_getposition()
{
    int relays = gcplc_peek_digitals (TRANS_POS_DO /* base */, TRANS_POS_BITS /* bits */);

                                /* when bit     E 1 0 */
    if (relays == 4) return T_PARK;      /*  is 1 0 0 */
    if (relays == 5) return T_REVERSE;   /*  is 1 0 1 */
    if (relays == 6) return T_NEUTRAL;   /*  is 1 1 0 */
    if (relays == 7) return T_DRIVE;     /*  is 1 1 1 */
    if (relays == 0) return T_OFF;       /*  is 0 x x */
    return -1;                  /* anything else - invalid */
}
