/**
 * \file plc.h
 * \brief Stub header file for including all PLC-related functionality.
 *
 * Joshua Oreman
 * 2 July 2007
 */

#ifndef __PLC_H__
#define __PLC_H__

#include "modbus.h"
#include "gcplc.h"
#include "brake.h"
#include "throttle.h"
#include "igntrans.h"

/** The IP address of the PLC in the lab. */
#define PLC_ADDR_LAB "192.168.1.75"
/** The IP address of the PLC in Alice. */
#define PLC_ADDR_ALICE "192.168.0.75"

/* Configuration of the PLC in Alice. CHANGE THIS! when you modify things. */

#define BRAKE_PRESSURE_AI   0   /**< Analog input 0: brake pressure sensor, 0.0 to 5.0 V. */
#define UNALLOCATED_AI_1    1   /**< Analog input 1: unassigned. */
#define UNALLOCATED_AI_2    2   /**< Analog input 2: unassigned. */
#define UNALLOCATED_AI_3    3   /**< Analog input 3: unassigned. */
#define UNALLOCATED_AI_4    4   /**< Analog input 4: unassigned. */
#define UNALLOCATED_AI_5    5   /**< Analog input 5: unassigned. */
#define UNALLOCATED_AI_6    6   /**< Analog input 6: unassigned. */
#define UNALLOCATED_AI_7    7   /**< Analog input 7: unassigned. */

#define THROTTLE_APPS1_AO   0   /**< Analog output 0: accelerator pedal position, wire 1. */
#define THROTTLE_APPS2_AO   1   /**< Analog output 1: accelerator pedal position, wire 2. */
#define THROTTLE_APPS3_AO   2   /**< Analog output 2: accelerator pedal position, wire 3. */
#define UNALLOCATED_AO_1    3   /**< Analog output 3: unassigned. */

#define UNALLOCATE_DI_0     0   /**< Digital input 0: unassigned. */
#define UNALLOCATE_DI_1     1   /**< Digital input 1: unassigned. */
#define UNALLOCATE_DI_2     2   /**< Digital input 2: unassigned. */
#define UNALLOCATE_DI_3     3   /**< Digital input 3: unassigned. */
#define UNALLOCATE_DI_4     4   /**< Digital input 4: unassigned. */
#define UNALLOCATE_DI_5     5   /**< Digital input 5: unassigned. */
#define UNALLOCATE_DI_6     6   /**< Digital input 6: unassigned. */
#define UNALLOCATE_DI_7     7   /**< Digital input 7: unassigned. */

#define BRAKE_POS_DO        0   /**< Digital relay outputs 0 through 3: brake pedal position. */
#define BRAKE_POS_BITS      4   /**< Number of bits in brake pedal position output. */
#define TRANS_POS_DO        4   /**< Digital relay outputs 4 and 5: transmission position. */
#define TRANS_POS_BITS      2   /**< Number of bits in transmission position output. */
#define UNALLOCATED_RDO_1   6   /**< Digital relay output 6: unassigned. */
#define UNALLOCATED_RDO_2   7   /**< Digital relay output 7: unassigned. */
#define UNALLOCATED_DO_1    8   /**< Digital output 8: unassigned. */
#define UNALLOCATED_DO_2    9   /**< Digital output 9: unassigned. */
#define UNALLOCATED_DO_3    10  /**< Digital output 10: unassigned. */
#define UNALLOCATED_DO_4    11  /**< Digital output 11: unassigned. */
#define UNALLOCATED_DO_5    12  /**< Digital output 12: unassigned. */
#define UNALLOCATED_DO_6    13  /**< Digital output 13: unassigned. */
#define UNALLOCATED_DO_7    14  /**< Digital output 14: unassigned. */
#define UNALLOCATED_DO_8    15  /**< Digital output 15: unassigned. */

#endif
