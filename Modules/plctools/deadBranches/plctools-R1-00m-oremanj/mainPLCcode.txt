(* In all cases, "input" and "output" refer to the MODULES. An "input" value will
   actually be output to Modbus, and vice versa. *)

(* Initial initialization, so as not to freak out the vehicle. *)
IF INITIALIZED = FALSE THEN
	(* Set the watchdog to disabled *)
	MB_WATCHDOG := 16#FFFF;
	(* Turn brake ON for safety *)
	MB_DO_0 := TRUE;
	MB_DO_1 := TRUE;
	MB_DO_2 := TRUE;
	MB_DO_3 := TRUE;
	(* Pause ON, disable OFF *)
	ESTOP_PAUSE := TRUE;
	ESTOP_DISABLE := FALSE;
	ESTOP_RUN := FALSE;
	(* Mark us as initialized so this only happens once. *)
	INITIALIZED := TRUE;
END_IF;

(* Read input registers from the AI and DIO modules and copy to Modbus. *)
MB_AI := ANALOG_IN;
MB_DI := DIGITAL_IO_IN;

IF NOT MB_DI_5 THEN
	(* Estop Reset switch - active low *)
	ESTOP_DISABLE := FALSE;
END_IF;

(* Handle Estop inputs. Note, all except DI_0 (the in-Alice switches, which are NC
   and break the circuit when pressed -> floats the input HIGH) are active LOW,
   hence the inversion. *)
IF NOT ESTOP_DISABLE THEN
	(* Only allow changing state if not in DISABLE mode. *)
	(* The run/pause switch [DI_1] is closed when set to RUN. That connects it to ground
	   -> FALSE when running. The remote pause [DI_2] is +5V when running. *)
	ESTOP_PAUSE   :=      MB_DI_1  OR (NOT MB_DI_2);
	(* The disable buttons [DI_0] are normally closed, and connected to ground.
	   They will read as TRUE when pressed, and FALSE when not. *)
	ESTOP_DISABLE :=      MB_DI_0  OR (NOT MB_DI_3);
	ESTOP_RUN     := NOT (ESTOP_PAUSE OR ESTOP_DISABLE);
END_IF;

(* Throttle is off if ESTOP_PAUSE or ESTOP_DISABLE. *)
IF ESTOP_DISABLE OR ESTOP_PAUSE THEN
	(* APPS sensors - these are idle values *)
	MB_AO_0 := 4.0375;
	MB_AO_1 := 1.485;
	MB_AO_2 := 0.935;
END_IF;

(* Things to do when DISABLED. *)
IF ESTOP_DISABLE THEN
	(* Open the Estop relay to fire the emergency brake. *)
	MB_DO_7 := FALSE;
	(* Ignition off? *)
	(* ... *)
	(* Full braking *)
	MB_DO_0 := TRUE;
	MB_DO_1 := TRUE;
	MB_DO_2 := TRUE;
	MB_DO_3 := TRUE;
	(* Lights and tone OFF. *)
	RDO_LIGHTS := FALSE;
	RDO_TONE   := FALSE;
	(* LEDs*)
	LED_DISABLE := TRUE;
	LED_PAUSE   := FALSE;
	LED_RUN     := FALSE;
	(* State = 0 *)
	MB_ESTOP := 0;
ELSIF ESTOP_PAUSE THEN
	(* Normal operation - E-brake relay is CLOSED (ON) *)
	MB_DO_7 := TRUE;
	(* Pause => more comfortable braking (at least 0x8, ~50% - gcdrive will apply more) *)
	MB_DO_3 := TRUE;    (* Bit 3: 1    gets 0b1xxx = at least 0x8 *)
	(* Lights and tone OFF. *)
	RDO_LIGHTS := FALSE;
	RDO_TONE   := FALSE;
	(* LEDs *)
	LED_DISABLE := FALSE;
	LED_PAUSE   := TRUE;
	LED_RUN     := FALSE;
	(* State = 1 *)
	MB_ESTOP := 1;
ELSE
	(* E-brake relay closed (don't fire). *)
	MB_DO_7 := TRUE;
	(* Lights and tone. *)
	RDO_LIGHTS := TRUE;    (* Lights always when in run mode *)
	RDO_TONE   := MB_DI_4; (* Tone only if the switch is on *)
	(* LEDs *)
	LED_DISABLE := FALSE;
	LED_PAUSE   := FALSE;
	LED_RUN     := TRUE;
	(* State = 2 *)
	MB_ESTOP := 2;
END_IF;

(* Throttle and trans - manual/auto, auto low *)
IF MB_DI_6 = FALSE THEN
	(* Automatic throttle *)
	RDO_THR1 := TRUE;
	RDO_THR2 := TRUE;
	RDO_THR3 := TRUE;
	RDO_THRV1 := TRUE;
	RDO_THRV2 := FALSE;
ELSE
	(* Manual throttle *)
	RDO_THR1 := FALSE;
	RDO_THR2 := FALSE;
	RDO_THR3 := FALSE;
	RDO_THRV1 := FALSE;
	RDO_THRV2 := FALSE;
END_IF;

IF MB_DI_7 = FALSE THEN
	(* Automatic transmission control *)
	MB_DO_6 := TRUE;
ELSE
	(* Manual transmission control *)
	MB_DO_6 := FALSE;
END_IF;

IF MB_DI_8 = TRUE THEN
	(* Manual brake control - turn off ze brake *)
	MB_DO_0 := FALSE;
	MB_DO_1 := FALSE;
	MB_DO_2 := FALSE;
	MB_DO_3 := FALSE;
END_IF;

(* Read output registers from Modbus and write to the AO and DO modules. *)
ANALOG_OUT      := MB_AO;
DIGITAL_R1_OUT  := MB_DO_LOW;
DIGITAL_R2_OUT  := MB_DO_HIGH;

(* Watchdog enabled? *)
IF MB_WATCHDOG <> 16#FFFF THEN
	(* Decrement it and DISABLE if it hits zero. *)
	MB_WATCHDOG := MB_WATCHDOG - 1;
	IF MB_WATCHDOG = 0 THEN
		ESTOP_DISABLE := TRUE;
	END_IF;
END_IF;

(* Used for checking the device is up-and-running etc. *)
MB_TEST_W := MB_TEST_R;