              Release Notes for "plctools" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "plctools" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "plctools" module can be found in
the ChangeLog file.

Release R1-00b-oremanj (Tue Jul 17 21:13:02 2007):
	Added gcplc_do_cond_write() for use in a dedicated writer thread - it
	blocks, using a condition variable, until a write is actually necessary.

Release R1-00b (Tue Jul 17 12:47:18 2007):
	I'm moving all the PLC actuator drivers from gcdrive to this module.
	(I never released gcdrive with them in it, but that's where I had
	been developing them previously.) I believe it makes more sense:
	they're pretty tightly coupled to the PLC, and this allows me to
	develop them without stepping on the toes of anyone working on
	the rest of gcdrive. The "old" drivers will remain in gcdrive,
	and can be compiled in; these should not clash with them.
	
	The actuator API is now:  (xxx = throttle, brake, ign, trans)
	    plc_xxx_check() : does a sanity check of some sort, returns 1
			      if it passed and 0 if it failed
	    plc_xxx_setposition(...) : sets the actuator's position
				       ... is a double for brake and throttle,
					      a #defined int for ign and trans
	    plc_xxx_getposition() : returns the last validly set position,
				    the same type that setposition sets
	More functions may be defined if they make sense (for instance,
	plc_brake_getpressure() returns the current value of the brake
	pressure sensor scaled onto [0.0, 1.0]).
	
	I've also added code in gcplc.c (functions starting with gcplc_)
	to handle multiple components using the PLC in one program, possibly
	even running simultaneously to each other, and marshal the requests
	into *one* connection (well, two, for thread-related reasons) that
	grabs all new status information at once instead of getting it
	piecemeal when requested - as each request takes a not insignificant
	amount of time.

Release R1-00a (Fri Jul  6 15:22:05 2007):
	Added all the code.

Release R1-00 (Fri Jul  6 11:48:59 2007):
	Created.


