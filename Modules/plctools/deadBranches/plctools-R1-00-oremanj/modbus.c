/**
 * \file modbus.c
 * \brief Implementation file for various functions having to do with Modbus communication.
 *
 * Joshua Oreman
 * 3 July 2007
 *
 * This file defines functions to be used for interfacing with the Programmable
 * Logic Controller in Alice via Modbus/TCP.
 */

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <poll.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>

#include "modbus.h"

/** Size of the internal packet buffer. This should be more than sufficient; the PLC won't
 * accept packets over 256B anyway. */
#define PKT_BUFSIZE    512

/** Size of the extended error data buffer. See documentation for PKT_BUFSIZE. */
#define EERR_BUFSIZE   256

/**
 * The Modbus object structure. It's an opaque type, so don't concern yourself with its innards, please.
 *
 * Because of the internal buffers and cache, this structure takes up about 1K in memory - but no
 * dynamic allocation is done in any of the Modbus API functions except modbus_create().
 */
struct modbus_t {
    int fd;                     /**< The socket descriptor used for communicating with the device. */
    int err;                    /**< Last error code, or 0 if nothing bad happened. */
    int exterr_len;             /**< Length of the last extended error. */
    int last_tid;               /**< Last used transaction ID (initially 0, so first tid is 1). */
    int timeout;                /**< Timeout to use for ops with the PLC (in ms) */

    /* Cache stuff: */
    int c_valid;                /**< What parts of the cache are valid? Uses the same flags as modbus_update(). */
    modbus_reg_t c_dig_in;      /**< Digital inputs register. */
    modbus_reg_t c_dig_out;     /**< Digital outputs register. */
    float c_analog_in[NR_ANALOG_IN]; /**< Analog inputs cache. */
    float c_analog_out[NR_ANALOG_OUT]; /**< Analog outputs cache. */

    /* Buffer stuff: */
    unsigned char b_packet[512];  /**< Packet-receiving and packet-making buffer. */
    unsigned char b_exterr[256];  /**< Buffer for extended error information. */
};

/** The length of the packet header. */
#define HEADER_LEN 8

/**
 * The packet header structure.
 *
 * This header can be found at the beginning of every packet, outgoing and incoming.
 *
 * \note The \a len member actually contains the length of the <i>rest of the packet</i>;
 * this includes the \a unit and \a func members. Also, Modbus documentation usually refers
 * to the \a func byte as being part of the payload, not the header. It is in the header
 * here for convenience and alignment reasons.
 *
 * All two-byte values in this header must be transmitted in network byte order.
 */
struct packet_header {
    unsigned short tid;     /**< Transaction ID. Serves to link a request and its reply. */
    unsigned short proto;   /**< Protocol ID. Always set to zero. */
    unsigned short len;     /**< Length of payload + function byte + unit ID. */
    unsigned char unit;     /**< Unit ID. Always set to zero. */
    unsigned char func;     /**< Function code. Indicates the operation requested / operation performed. */
} __attribute__((packed));

/** Convert a packet_header structure into network byte order. */
static void endianize (struct packet_header *phdr) 
{
    phdr->tid = htons (phdr->tid);
    phdr->proto = htons (phdr->proto);
    phdr->len = htons (phdr->len);
}

/** Convert a packet_header structure back into host byte order. */
static void unendianize (struct packet_header *phdr) 
{
    phdr->tid = ntohs (phdr->tid);
    phdr->proto = ntohs (phdr->proto);
    phdr->len = ntohs (phdr->len);
}


/**
 * The payload for use with Modbus functions 1 (Read Coils) and 3 (Read Holding Registers).
 */
struct read_payload {
    unsigned short start;       /**< The first coil/register index to read. */
    unsigned short len;         /**< The number of coils/registers to read. */
} __attribute__ ((packed));

/**
 * The payload header for use with Modbus functions 15 (Force Multiple Coils) and 16 (Preset Multiple
 * Registers). It is followed by the data to write, in bitstring form for coils or as a sequence of
 * big-endian words for registers.
 */
struct write_payload_hdr {
    unsigned short start;       /**< The first coil/register index to write. */
    unsigned short regs;        /**< The number of coils/registers to write. */
    unsigned char bytes;        /**< The number of bytes of data in the rest of the packet. Yes, it's redundant. */
} __attribute__ ((packed));

/**
 * The payload for use with Modbus functions 5 (Force Single Coil) and 6 (Preset Single Register).
 *
 * For forcing a coil, val must contain either \c 0xFF00 to set the coil or \c 0x0000 to reset it.
 * Other values are erroneous.
 */
struct single_write_payload {
    unsigned short reg;         /**< The coil/register index to write. */
    unsigned short val;         /**< The value to write to it. */
} __attribute__ ((packed));


modbus_t *modbus_create (unsigned int ip_addr, int port) 
{
    struct sockaddr_in addr;
    modbus_t *ret = (modbus_t *)malloc (sizeof(modbus_t));
    if (!ret) {
        errno = ENOMEM;
        return NULL;
    }

    memset (ret, 0, sizeof(modbus_t));
    
    ret->fd = socket (PF_INET, SOCK_STREAM, 0);
    if (ret->fd == -1) goto err;

    memset (&addr, 0, sizeof(struct sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_port = 0;
    addr.sin_addr.s_addr = INADDR_ANY;

    if (bind (ret->fd, (struct sockaddr *)&addr, sizeof(addr)) < 0) goto err;
    
    if (port <= 0) port = MODBUS_DEFAULT_PORT;
    addr.sin_family = AF_INET;
    addr.sin_port = htons (port);
    addr.sin_addr.s_addr = ip_addr;

    if (connect (ret->fd, (struct sockaddr *)&addr, sizeof(addr)) < 0) goto err;
    
    return ret;

 err:
    if (ret->fd != -1) close (ret->fd);
    free (ret);
    return NULL;
}


void modbus_destroy (modbus_t *self) 
{
    if (self == NULL) return;
    
    close (self->fd);
    free (self);
}


void modbus_set_timeout (modbus_t *self, int timeout) 
{
    self->timeout = timeout;
}


int modbus_get_error (modbus_t *self)
{
    return self->err;
}

/**
 * \brief Sets the last-error field in the Modbus object \a self to \a err.
 *
 * The global variable \c errno is automatically set to a corresponding error code.
 *
 * \param self The Modbus object to operate on.
 * \param err The error code to set (one of MODBUS_ERR_*).
 */
static void set_err (modbus_t *self, int err) 
{
    static int errno_table[16] = {
        EINVAL,
        [MODBUS_ERR_ILLEGAL_FUNCTION] = EPERM,
        [MODBUS_ERR_ILLEGAL_DATA_ADDRESS] = EFAULT,
        [MODBUS_ERR_ILLEGAL_DATA_VALUE] = EINVAL,
        [MODBUS_ERR_DEVICE_FAILURE] = EIO,
        [MODBUS_ERR_TIMEOUT] = ETIMEDOUT,
        [MODBUS_ERR_DEVICE_BUSY] = EAGAIN,
        [MODBUS_ERR_UNABLE_TO_PERFORM_FUNCTION] = ENOSYS,
        [MODBUS_ERR_MEMORY_PARITY] = ENOMEM,
        [MODBUS_ERR_GW_PATH_NOT_AVAILABLE] = EHOSTUNREACH,
        [MODBUS_ERR_GW_NO_TARGET_RESPONSE] = EHOSTDOWN,
        EINVAL
    };
    
    self->err = err;
    if (err < 16) errno = errno_table[err];
}

unsigned char *modbus_get_extra_error (modbus_t *self, unsigned char *buf, int *len) 
{
    if (self->err != MODBUS_ERR_EXTD)
        return NULL;

    if (buf != NULL) {
        memcpy (buf, self->b_exterr, (self->exterr_len < *len)? self->exterr_len : *len);
    } else {
        buf = self->b_exterr;
    }

    *len = self->exterr_len;
    return buf;
}

/**
 * \brief Reliable read with timeout.
 *
 * This function tries to read exactly \a buflen bytes from file descriptor
 * \a fd into the buffer pointed to by \a buf. If more than \a *timeout
 * milliseconds pass before all \a buflen bytes are read, a short read
 * is returned.
 *
 * \param fd The file descriptor to read from.
 * \param buf The buffer into which the received data will be written.
 * \param buflen The number of bytes to read.
 * \param timeout A pointer to a timeout value, in milliseconds.
 *                A timeout of -1 results in no timeout, 0 in a non-blocking read.
 *                The variable you specify will be rewritten to contain
 *                the number of milliseconds left after the read.
 * \return The number of bytes read, 0 if nothing came before the timeout,
 *         -1 and sets errno on error.
 */
static int read_timeout (int fd, void *buf, int buflen, int *timeout) 
{
    struct pollfd fds = { fd, POLLIN, 0 };
    struct timeval tvs, tve;
    int res;
    int readlen = 0;

    while (readlen < buflen) {
        gettimeofday (&tvs, NULL);
        
        if ((res = poll (&fds, 1, *timeout)) <= 0) {
            if (res < 0) return res;
            return readlen;
        }
        
        res = read (fd, buf, buflen - readlen);

        if (*timeout >= 0) {
            gettimeofday (&tve, NULL);
            *timeout -= (tve.tv_sec - tvs.tv_sec) * 1000;
            *timeout -= (tve.tv_usec - tvs.tv_usec) / 1000;
            if (*timeout < 0) *timeout = 0;
        }

        if (res <= 0) break;
        readlen += res;
    }

    if (res <= 0) return res;
    return readlen;
}

/**
 * Waits not more than \a timeout milliseconds to read
 * a packet into the internal buffer of \a self.
 *
 * The packet header will be converted back into host
 * byte order before this function returns.
 *
 * \param self The Modbus object to operate on.
 * \param pktlen Written with the length of the actual payload that was received,
 *               NOT including the eight bytes of header.
 * \param timeout Pointer to a timeout variable, in milliseconds.
 *                It will be rewritten with the number of milliseconds
 *                not elapsed when the function returns.
 * \return *pktlen on success, 0 on timeout, -1 and sets errno on error.
 */
static int read_packet (modbus_t *self, int *pktlen, int *timeout)
{
    struct packet_header *hdr;
    int res;

    if ((res = read_timeout (self->fd, self->b_packet, HEADER_LEN, timeout)) != HEADER_LEN)
        return (res < 0)? res : 0;

    hdr = (struct packet_header *)self->b_packet;
    unendianize (hdr);
    *pktlen = hdr->len - 2;
    if (*pktlen > PKT_BUFSIZE - HEADER_LEN) {
        set_err (self, MODBUS_ERR_ILLEGAL_DATA_VALUE);
        return -1;
    }

    if ((res = read_timeout (self->fd, self->b_packet + HEADER_LEN, *pktlen, timeout)) != *pktlen)
        return (res < 0)? res : 0;

    return *pktlen;
}

int modbus_command (modbus_t *self, int func, const void *datav, int datalen,
                    void *respv, int resplen, int timeout) 
{
    struct packet_header pkt_hdr;
    const unsigned char *data = datav;

    if (self == NULL || self->fd == -1) return 0;

    if (datalen > PKT_BUFSIZE - HEADER_LEN || resplen > PKT_BUFSIZE - HEADER_LEN) {
        set_err (self, MODBUS_ERR_ILLEGAL_DATA_ADDRESS);
        return 0;
    }

    assert (sizeof(pkt_hdr) == HEADER_LEN);

    // Construct a packet and send it out.
    pkt_hdr.tid = ++self->last_tid;
    pkt_hdr.proto = 0;
    pkt_hdr.len = 2 + datalen;
    pkt_hdr.unit = 0;
    pkt_hdr.func = func;
    
    // Order is important here - this lets us use self->b_packet as an *input* pkt buffer too.
    // And yes, it must be memmove().
    memmove (self->b_packet + HEADER_LEN, data, datalen);
    memcpy (self->b_packet, &pkt_hdr, HEADER_LEN);

    endianize ((struct packet_header *)self->b_packet);
    if (write (self->fd, self->b_packet, HEADER_LEN + datalen) != HEADER_LEN + datalen) {
        set_err (self, MODBUS_ERR_DEVICE_FAILURE);
        return 0;
    }

    if (timeout == 0) {
        self->err = 0;
        return self->last_tid;
    }

    return modbus_wait (self, self->last_tid, respv, resplen, timeout);
}

int modbus_wait (modbus_t *self, int tid, void *respv, int resplen, int timeout) 
{
    struct packet_header *hdrp = (struct packet_header *)self->b_packet;
    int res;
    int pktlen;
    unsigned char *resp = respv;
    int orig_timeout = timeout;
        
    while ((res = read_packet (self, &pktlen, &timeout)) > 0) {
        if (hdrp->tid == tid)
            break;
    }

    if (res <= 0) {
        if (res == 0) set_err (self, MODBUS_ERR_TIMEOUT);
        return 0;
    }

    if (hdrp->func & 0x80) {
        // Modbus error indication
        unsigned char errcode = self->b_packet[HEADER_LEN];
        if (errcode == 0xFF) {
            int eelen = ntohs (*(unsigned short *)(self->b_packet + HEADER_LEN + 1));
            self->err = 0xFF;
            if (eelen > pktlen - 3) {
                const char *s = "Corrupt extended error packet";
                self->exterr_len = strlen (s);
                strcpy ((char *)self->b_exterr, s);
            } else {
                self->exterr_len = eelen;
                if (self->exterr_len > EERR_BUFSIZE) self->exterr_len = EERR_BUFSIZE;
                memcpy (self->b_exterr, self->b_packet + HEADER_LEN + 3, self->exterr_len);
            }
        } else if (errcode == 0x05) { // "acknowledge"
            // Try again.
            return modbus_wait (self, tid, resp, resplen, orig_timeout);
        } else {
            set_err (self, errcode);
        }
        return 0;
    }

    memcpy (resp, self->b_packet + HEADER_LEN,
            (pktlen > resplen)? resplen : pktlen);
    
    self->err = 0;
    return 1;
}

int modbus_read_coils (modbus_t *self, int start, int ncoils, unsigned char *outputs) 
{
    int res, coil = 0, curbyte, bitlen;
    unsigned char *bitsp;
    struct read_payload p = { htons (start), htons (ncoils) };

    if ((res = modbus_command (self, 0x01 /* Read Coils */, &p, sizeof(p), NULL, 0, -1)) <= 0)
        return res;

    // Unpack the bit string:
    bitlen = self->b_packet[HEADER_LEN];
    bitsp = self->b_packet + HEADER_LEN + 1;

    while (coil < ncoils) {
        if ((coil % 8) == 0) {
            curbyte = *bitsp++;
            if (bitsp > self->b_packet + HEADER_LEN + bitlen + 1)
                return 1;
        }
        
        *outputs++ = curbyte & 1;
        curbyte >>= 1;
        coil++;
    }

    return 1;
}

int modbus_write_coils (modbus_t *self, int start, int ncoils, const unsigned char *inputs) 
{
    int coil = 0, curbyte = 0;
    struct write_payload_hdr *wph = (struct write_payload_hdr *)self->b_packet;
    unsigned char *bitsp = self->b_packet + sizeof(*wph);
    
    // Pack *inputs into a bit string.
    while (coil < ncoils) {
        if (coil != 0 && ((coil % 8) == 0)) {
            *bitsp++ = curbyte;
            curbyte = 0;
        }
        curbyte >>= 1;
        if (*inputs++)
            curbyte |= 0x80;
        coil++;
    }
    // Last byte...
    if ((ncoils % 8) != 0)
        curbyte >>= (8 - (ncoils % 8));
    *bitsp++ = curbyte;

    wph->bytes = bitsp - sizeof(*wph) - self->b_packet;
    wph->regs = htons (ncoils);
    wph->start = htons (start);
    
    return modbus_command (self, 0x0F /* Force Multiple Coils */, self->b_packet,
                           sizeof(*wph) + wph->bytes, NULL, 0, -1);
}

int modbus_read_registers (modbus_t *self, int start, int nregs, modbus_reg_t *output) 
{
    int res, i;
    struct read_payload p = { htons (start), htons (nregs) };
    modbus_reg_t *regs = (modbus_reg_t *)(self->b_packet + HEADER_LEN + 1);

    if ((res = modbus_command (self, 0x03 /* Read Holding Registers */, &p, sizeof(p), NULL, 0, -1)) <= 0)
        return res;
    
    if (self->b_packet[HEADER_LEN] != nregs*2) {
        // got the wrong number of bytes in response
        set_err (self, MODBUS_ERR_ILLEGAL_DATA_VALUE);
        return 0;
    }

    for (i = 0; i < nregs; i++) {
        output[i] = ntohs (regs[i]);
    }
    return 1;
}

int modbus_write_registers (modbus_t *self, int start, int nregs, const modbus_reg_t *input) 
{
    int i;
    struct write_payload_hdr *wph = (struct write_payload_hdr *)self->b_packet;
    modbus_reg_t *regs = (modbus_reg_t *)(self->b_packet + sizeof(*wph));

    if (nregs > 200) {
        set_err (self, MODBUS_ERR_ILLEGAL_DATA_ADDRESS);
        return 0;
    }

    wph->start = htons (start);
    wph->regs = htons (nregs);
    wph->bytes = 2 * nregs;

    for (i = 0; i < nregs; i++) {
        regs[i] = htons (input[i]);
    }

    return modbus_command (self, 0x10 /* Preset Multiple Registers */, self->b_packet,
                           sizeof(*wph) + wph->bytes, NULL, 0, -1);
}


int modbus_update (modbus_t *self, int flags) 
{
    int ok = 1;
    
    if (flags & MODBUS_UPDATE_DI) {
        if (modbus_read_registers (self, MODBUS_REG_DI, 1, &self->c_dig_in))
            self->c_valid |= MODBUS_UPDATE_DI;
        else
            ok = 0;
    }

    if (flags & MODBUS_UPDATE_DO) {
        if (modbus_read_registers (self, MODBUS_REG_DO, 1, &self->c_dig_out))
            self->c_valid |= MODBUS_UPDATE_DO;
        else
            ok = 0;
    }
    
    if (flags & MODBUS_UPDATE_AI) {
        // Yes, by some miracle or another, as long as we're both little-endian (which we are)
        // you can read the registers into an array of floats and things will Just Work.
        if (modbus_read_registers (self, MODBUS_REG_AI(0), NR_ANALOG_IN * 2, (modbus_reg_t *)self->c_analog_in))
            self->c_valid |= MODBUS_UPDATE_AI;
        else
            ok = 0;
    }

    if (flags & MODBUS_UPDATE_AO) {
        if (modbus_read_registers (self, MODBUS_REG_AO(0), NR_ANALOG_OUT * 2, (modbus_reg_t *)self->c_analog_out))
            self->c_valid |= MODBUS_UPDATE_AO;
        else
            ok = 0;
    }

    return ok;
}


int modbus_read_digital (modbus_t *self, int nr) 
{
    if (nr < 0 || nr >= NR_DIGITAL_IN) {
        set_err (self, MODBUS_ERR_ILLEGAL_DATA_ADDRESS);
        return -1;
    }

    if (!(self->c_valid & MODBUS_UPDATE_DI)) {
        if (!modbus_update (self, MODBUS_UPDATE_DI)) {
            return -1;
        }
    }
    
    return !!(self->c_dig_in & (1 << nr));
}

int modbus_read_all_digital (modbus_t *self, modbus_reg_t *regp) 
{
    int res = modbus_update (self, MODBUS_UPDATE_DI);
    if (res == 0) return 0;

    *regp = self->c_dig_in;
    return 1;
}

int modbus_peek_digital (modbus_t *self, int nr) 
{
    if (nr < 0 || nr >= NR_DIGITAL_OUT) {
        set_err (self, MODBUS_ERR_ILLEGAL_DATA_ADDRESS);
        return -1;
    }

    if (!(self->c_valid & MODBUS_UPDATE_DO)) {
        if (!modbus_update (self, MODBUS_UPDATE_DO)) {
            return -1;
        }
    }
    
    return !!(self->c_dig_out & (1 << nr));
}

int modbus_peek_all_digital (modbus_t *self, modbus_reg_t *regp) 
{
    int res = modbus_update (self, MODBUS_UPDATE_DO);
    if (res == 0) return 0;

    *regp = self->c_dig_out;
    return 1;
}

int modbus_write_digital (modbus_t *self, int nr, unsigned char val) 
{
    struct single_write_payload p;

    if (nr < 0 || nr >= NR_DIGITAL_OUT) {
        set_err (self, MODBUS_ERR_ILLEGAL_DATA_ADDRESS);
        return -1;
    }

    p.reg = htons (MODBUS_COIL_DO(nr));
    p.val = htons (val? 0xFF00 : 0x000);

    self->c_valid = 0;

    return modbus_command (self, 0x05 /* Force Single Coil */, &p, sizeof(p), NULL, 0, -1);
}

int modbus_write_all_digital (modbus_t *self, modbus_reg_t val) 
{
    struct single_write_payload p;

    p.reg = htons (MODBUS_REG_DO);
    p.val = htons (val);

    self->c_valid = 0;
    
    return modbus_command (self, 0x06 /* Preset Single Register */, &p, sizeof(p), NULL, 0, -1);
}

int modbus_read_analog (modbus_t *self, int nr, float *valp) 
{
    if (nr < 0 || nr >= NR_ANALOG_IN) {
        set_err (self, MODBUS_ERR_ILLEGAL_DATA_ADDRESS);
        return 0;
    }

    if (self->c_valid & MODBUS_UPDATE_AI) {
        *valp = self->c_analog_in[nr];
        return 1;
    }

    return modbus_read_registers (self, MODBUS_REG_AI(nr), 2, (modbus_reg_t *)valp);
}

int modbus_peek_analog (modbus_t *self, int nr, float *valp) 
{
    if (nr < 0 || nr >= NR_ANALOG_OUT) {
        set_err (self, MODBUS_ERR_ILLEGAL_DATA_ADDRESS);
        return 0;
    }

    if (self->c_valid & MODBUS_UPDATE_AO) {
        *valp = self->c_analog_out[nr];
        return 1;
    }

    return modbus_read_registers (self, MODBUS_REG_AO(nr), 2, (modbus_reg_t *)valp);
}

int modbus_write_analog (modbus_t *self, int nr, float val) 
{
    if (nr < 0 || nr >= NR_ANALOG_OUT) {
        set_err (self, MODBUS_ERR_ILLEGAL_DATA_ADDRESS);
        return -1;
    }

    self->c_valid = 0;

    return modbus_write_registers (self, MODBUS_REG_AO(nr), 2, (const modbus_reg_t *)&val);
}

#ifdef TEST
int main() 
{
    modbus_t *mb;
    modbus_reg_t mbregs[256];
    unsigned char mbcoils[256];
    __asm__ ("int $0x3"); // breakpoint
    return 0;
}
#endif
