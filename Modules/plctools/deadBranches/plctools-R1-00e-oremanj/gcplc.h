/**
 * \file gcplc.h
 * \brief Code for interfacing between gcdrive and the PLC library.
 *
 * \author Joshua Oreman
 * \date 16 July 2007
 *
 * This file provides a "glue" layer of sorts between gcdrive and
 * the Programmable Logic Controller that handles most of the
 * actuators. The PLC takes a rather long time (~50ms) to respond
 * to commands, so we want to send *all* our outputs at once.
 * In addition, you can read status in a separate thread so as to avoid
 * unnecessary delays in the main loop. Finally, when speed is at
 * a premium, it's possible to blast write requests onto the wire
 * without waiting for a response; this gives almost instantaneous
 * update of the actuators, at the expense of error conditions
 * not being flagged.
 *
 * This interface may well be useful to more than just gcdrive.
 */

#ifndef __GCPLC_H__
#define __GCPLC_H__

#include "modbus.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \brief Initializes the Modbus connections. We use two - one for writing,
 * one for reading - so as not to slow things down overmuch.
 *
 * \param ip_addr The IP address to connect to, in dotted decimal format
 *                (e.g. "192.168.0.75").
 * \return 0 on success, -1 and sets \c errno on failure.
 */
int gcplc_init (const char *ip_addr);

/**
 * \brief Changes the current write-blasting status for the PLC.
 *
 * When write-blasting is ON, gcplc_do_write() returns almost instantaneously,
 * and the actuator outputs should be updated within 5ms. However, this speed
 * comes at the expense of a slight unreliability: if something goes wrong and
 * the write fails, you won't know except by checking whether the read values
 * match up with your written ones. Error returns are ignored.
 *
 * When write-blasting is OFF, gcplc_do_write() waits for a response from the
 * PLC before returning, and so can indicate error conditions when the PLC does.
 *
 * It should be noted that the PLC is notoriously poor at returning errors;
 * in case of failure it's much more likely to just hang indefinitely or
 * even not notice the failure at all. The gcplc_ping() function provides
 * a much more reliable means of verifying the PLC's integrity.
 *
 * Write-blasting is OFF by default.
 *
 * \param wb The new value for write-blasting. Zero turns it off, and any nonzero
 *           value turns it on.
 */
void gcplc_set_wblast (int wb);

/**
 * \brief Checks whether the PLC is on and responding to commands.
 *
 * This is not a "ping" in the ICMP/networking sense; it writes a random
 * value to Modbus register 26, which is copied to register 27 by the program
 * running on the PLC each time through its loop. If the value does not transfer
 * over within about a second, or if any networking or Modbus error is encountered,
 * failure is returned.
 *
 * \return 0 on success, -1 and sets \c errno on failure.
 */
int gcplc_ping();

/**
 * \brief Reconnects to the PLC.
 *
 * This is not likely to do much, but it may be useful as a grasping-at-straws effort
 * if something gets wedged.
 *
 * \return 0 on success, -1 and sets \c errno on failure.
 */
int gcplc_reconnect();

/**
 * \brief Reads all registers used by Alice's PLC into internal caches that may
 * be accessed by gcplc_read_* and gcplc_peek_*.
 *
 * This function will take about 50ms to return. You may wish to run it in a
 * separate thread; proper rwlock support is handled internally, so it is
 * thread-safe with the accessor functions below.
 *
 * \return 0 on success, -1 and sets \c errno on failure.
 */
int gcplc_do_read();

/**
 * \brief Writes to the PLC all values that have been modified with gcplc_write_*
 * since this function was last called.
 *
 * If write-blasting is ON, returns 0 immediately after writing the packet to the
 * network, except in case of an error on send(), in which case it returns -1.
 *
 * If write-blasting is OFF, awaits the PLC's reply, and returns 0 for success or
 * -1 for failure as usual.
 *
 * \return 0 or -1; see above.
 */
int gcplc_do_write();

/**
 * \brief Waits for some value to be ready to write to the PLC, and then writes it.
 *
 * This is intended for use in a writer thread; just call gcplc_do_cond_write()
 * over and over again. It \e blocks until the write cache is dirtied in some way,
 * then calls gcplc_do_write() with appropriate mutices held.
 *
 * \return The return value of gcplc_do_write().
 */
int gcplc_do_cond_write();

/**
 * \defgroup plcwrappers Wrappers for the PLC functions.
 *
 * Scant documentation is provided for most of these, because their
 * functionality is nearly identical to the functions of the same name
 * in the PLC library (replace gcplc_ with modbus_). The wrappers are
 * necessary so values are read and written in the all-at-once
 * contiguous bursts that are needed to preserve reasonable speed.
 *
 * These functions will never fail.
 */
/*@{*/

/** Reads the current value of one digital input. */
int gcplc_read_digital (int nr);

/** Reads the current value of one digital output. */
int gcplc_peek_digital (int nr);

/** Sets a new value for one digital output. */
void gcplc_write_digital (int nr, unsigned char val);

/** Reads the current value of one analog input. */
float gcplc_read_analog (int nr);

/** Reads the current value of one analog output. */
float gcplc_peek_analog (int nr);

/** Sets a new value for one analog output. */
void gcplc_write_analog (int nr, float val);

/**
 * \brief Reads a multiple-bit value from digital inputs.
 *
 * The wires should be connected so that the \a base input
 * specified contains the <i>least significant bit</i> of
 * the value you wish to read. (Yes, I know input 0 is at the top of each
 * module. Yes, I know this may seem backwards. Deal with it.)
 *
 * You must specify the proper number of bits to read, or
 * all hell will break loose.
 *
 * \param base The input number containing the most-significant
 * bit of those you wish to read.
 * \param nbits The number of bits to read.
 * \return The read value.
 */
int gcplc_read_digitals (int base, int nbits);

/** Read the current value of several bits' worth of digital output. See gcplc_read_digitals(). */
int gcplc_peek_digitals (int base, int nbits);

/** Write several bits' worth of digital output. See gcplc_read_digitals(). */
void gcplc_write_digitals (int base, int nbits, int val);

/*@}*/

/**
 * \brief Read the current status of the E-stop unit and return it.
 *
 * This function obeys the same caching properties as all the other gcplc_read_* functions.
 *
 * \return GCPLC_ESTOP_DISABLE, GCPLC_ESTOP_PAUSE, or GCPLC_ESTOP_RUN.
 */
int gcplc_read_estop();

/** E-stop disable value. */
#define GCPLC_ESTOP_DISABLE 0
/** E-stop pause value. */
#define GCPLC_ESTOP_PAUSE 1
/** E-stop run value. */
#define GCPLC_ESTOP_RUN 2

#ifdef __cplusplus
}
#endif

#endif /* __GCPLC_H__ */
