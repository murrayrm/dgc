/**
 * \file brake.c
 * \brief Low-level brake code for Alice.
 *
 * \author Joshua Oreman
 * \date 10 July 2007
 *
 * This file defines functions used by gcdrive to access and manipulate
 * Alice's brake (gas pedal).
 */

#include <stdlib.h>
#include <math.h>
#include <errno.h>
#include <pthread.h>
#include <unistd.h>

#include "plc.h"

/** Brake position corresponding to no braking. */
#define BRAKE_MIN   0

/** Brake position corresponding to full braking. */
#define BRAKE_MAX   15

/** The number of brake positions defined. */
#define BRAKE_RANGE 16

#ifndef BRAKE_FULL_QUANTIZE
/** If TRUE, the quantization will be over BRAKE_RANGE and any
    braking values between (BRAKE_RANGE-1)/BRAKE_RANGE and 1.0 will
    correspond to full braking. If FALSE, the quantization will
    be over BRAKE_RANGE-1 and only 1.0 will correspond to full braking. */
#define BRAKE_FULL_QUANTIZE 1
#endif

int plc_brake_check()
{
  double pos;
  
  /* Set the position to 1. */
  if (plc_brake_setposition (1.0) < 0)
    return 0;
  
  gcplc_do_write();
  
  usleep (500000);              /* give time for the brake to settle */

  gcplc_do_read();

  /* Read the position back. */
  if ((pos = plc_brake_getposition()) < 0)
    return 0;
  
  /* Close enough? (brake only has 16 gradations) */
  if ((int)(fabs (pos) * BRAKE_RANGE) != BRAKE_MAX)
    return 0;

  /* Pressure all right? */
  if (plc_brake_getpressure() < 0.5)
    return 0;
  
  /* Looks good. */
  return 1;
}

int plc_brake_setposition (double pos) 
{
  /*
   * Brake uses four digital outputs, encoding pos into a
   * 16-gradation thing.
   */
  int ipos;

  if (pos < 0.0 || pos > 1.0) {
    errno = EINVAL;
    return -1;
  }

#if BRAKE_FULL_QUANTIZE
  /*
   * Note that this causes anything between 0.0 and 0.0625 to be "no braking",
   * and anything between 0.9375 and 1.0 to be "full".
   */
  ipos = (int)(pos * BRAKE_RANGE);
#else
  ipos = (int)(pos * (BRAKE_RANGE - 1));
#endif
  if (ipos < BRAKE_MIN) ipos = BRAKE_MIN;
  if (ipos > BRAKE_MAX) ipos = BRAKE_MAX;

  gcplc_write_digitals (BRAKE_POS_DO /* base */, BRAKE_POS_BITS /* bits */, ipos);
  
  return 0;
}

double plc_brake_getposition() 
{
  int ipos;
  
  /* Read 'em in, convert four bits to one four-bit value. */
  ipos = gcplc_peek_digitals (BRAKE_POS_DO /* base */, BRAKE_POS_BITS /* bits */);

#if BRAKE_FULL_QUANTIZE
  /* And convert *that* to a float. Note that because of the quantization
     method used, a set value of 1.0 will be returned as 15/16 (0.9375). */
  return (double)ipos / (double)BRAKE_RANGE;
#else
  /* This one returns a set value of 1.0 as 1.0. */
  return (double)ipos / (double)(BRAKE_RANGE - 1);
#endif
}

double plc_brake_getpressure()
{
  /* Read it. */
  float val = gcplc_read_analog (BRAKE_PRESSURE_AI);
  
  val /= 5.0;
  if (val < 0.0 || val > 1.0)
    return -1;

  return val;
}

/* Local Variables: */
/* mode: c */
/* indent-tabs-mode: nil */
/* c-basic-offset: 2 */
/* End: */
