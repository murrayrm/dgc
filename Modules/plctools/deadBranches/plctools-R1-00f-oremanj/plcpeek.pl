#!/usr/bin/perl -w
use strict;
use Socket;
use Term::ReadLine;

my $Term = new Term::ReadLine 'Command-line PLC modifier';
my $OUT = $Term->OUT || \*STDOUT;

my $Help = <<EOF;
--- Modbus PLC Peeker ---
help
  this screen

quit, exit
  quit this program

connect IP [PORT]
  connect to the Modbus server running on the given IP and PORT.
  PORT defaults to 502.
  if repeated, this command will disconnect from current server
  and connect to new one.

read REG [...]
   where REG has the form TS[:E]
   where T is a type: d(igital), h(olding), i(nput), or di# or ai#
     and S is the starting register
     and E is the ending register
   if no REG is specified, reads 42001:42026
   if REG is just "d" or "digital", reads 02001:02032

write REG [...]
   where REG has the form TS[:E]=V
   where T is a type: d(igital), h(olding), or do# or ao#
     and S is the starting register and E is the ending register
     and V is a comma-separated list of values
        each of which is [for digital] 0, 1, on, off, yes, no
        [for analog] 123 (decimal), 0644 (octal), 0x1234 (hex), 0b0101 (binary),
          1.529 (float; uses TWO REGISTERS)
        if there are less values than registers, the last is repeated
        if there are more values than registers, the extra values are ignored
          except if it's only one off and the last value is a float, in which case
          an extra register is used

commands may be abbreviated to one letter

--- end of help ---
EOF

my $Connected = undef;
# socket is CONN

sub mess {
    my($out) = join '', @_;
    $out .= "\n" unless $out =~ /\s$/;
    print $OUT $out;
}

sub err {
    my($err) = join '', @_;
    $err .= "\n" unless $err =~ /\s$/;
    die $err;
}

sub regconv {
    my($reg, $type, $end) = @_;
    my $ret;

    # Specific:
    if ($type eq "di") {
        $reg =~ /^0*[0-7]$/ or err "$reg: digital inputs must be numbers 0-7";
        $ret = 2001 + int $reg;
    }
    if ($type eq "do") {
        $reg =~ /^0*([0-9]|1[0-5])$/ or err "$reg: digital outputs must be numbers 0-15";
        $ret = 2417 + int $reg;
    }
    if ($type eq "ai") {
        $reg =~ /^0*[0-7]$/ or err "$reg: analog inputs must be numbers 0-7";
        $ret = 2003 + 2 * int($reg) + defined $end;
    }
    if ($type eq "ao") {
        $reg =~ /^0*[0-3]$/ or err "$reg: analog outputs must be numbers 0-3";
        $ret = 2019 + 2 * int($reg) + defined $end;
    }

    # General:
    if ($type eq "d") {
        $reg =~ /^0(\d\d\d\d)$/ or err "$reg: digital registers must look like 0xxxx";
        $ret = $1;
    }
    if ($type eq "h") {
        $reg =~ /^4(\d\d\d\d)$/ or err "holding registers must look like 4xxxx";
        $ret = $1;
    }
    if ($type eq "i") {
        $reg =~ /^3(\d\d\d\d)$/ or err "input registers must look like 3xxxx";
        $ret = $1;
    }

    if (defined $ret) {
        err "$reg: there is no register zero" if $ret <= 0;
        return $ret - 1;
    }

    return $reg;
}

sub regunconv {
    my($reg, $type) = @_;
    return sprintf "0%04d (DI %02d)", $reg + 1, $reg - 2000 if $type eq "d" and $reg >= 2000 and $reg <= 2007;
    return sprintf "0%04d (DO %02d)", $reg + 1, $reg - 2416 if $type eq "d" and $reg >= 2416 and $reg <= 2431;
    return         "42001 (DI **)"    if $reg == 2000 and $type eq "h";
    return         "42002 (ESTOP)"    if $reg == 2001 and $type eq "h";
    return         "42027 (DO **)"    if $reg == 2026 and $type eq "h";
    return         "42028 (W DOG)"    if $reg == 2027 and $type eq "h";
    return         "42029 (<PING)"    if $reg == 2028 and $type eq "h";
    return         "42030 (PING>)"    if $reg == 2029 and $type eq "h";
    return sprintf "4%04d (AI %d%s)", $reg + 1, int(($reg - 2002)/2), ('l','h')[$reg % 2] if $type eq "h" and $reg >= 2002 and $reg <= 2017;
    return sprintf "4%04d (AO %d%s)", $reg + 1, int(($reg - 2018)/2), ('l','h')[$reg % 2] if $type eq "h" and $reg >= 2018 and $reg <= 2025;
    return sprintf "        0%04d", $reg + 1 if $type eq "d";
    return sprintf "        3%04d", $reg + 1 if $type eq "i";
    return sprintf "        4%04d", $reg + 1 if $type eq "h";
    return $reg;
}

sub regparse {
    my($reg, $writable) = @_;
    my($type, $start, $end, $vals);
    if ($writable) {
        $reg =~ /^(?:([DdHhAaIi]|[DdAa][OoIi])#?(?:igital|olding|nalog|nput)?)?(\d+)(?::(\d+))?=\(?((.+,)*(.+?))\)?$/
             or err "invalid register format for `$reg'";
        ($type, $start, $end, $vals) = ($1? lc $1 : undef, $2, $3 || $2, $4);
    } else {
        $reg =~ /^(?:([DdHhAaIi]|[DdAa][IiOo])#?(?:igital|olding|nalog|nput)?)?(\d+)(?::(\d+))?$/ or err "invalid register format for `$reg'";
        ($type, $start, $end) = ($1? lc $1 : undef, $2, $3 || $2);
    }
    my @vals;
    if (!defined $type) {
        $start =~ /^([034])\d\d\d\d$/ or err "you must either specify an explicit type (d, h, i) or use a register number that determines the type (0xxxx, 4xxxx, 3xxxx)";
        $type = "d" if $1 eq "0";
        $type = "i" if $1 eq "3";
        $type = "h" if $1 eq "4";
    } else {
        $type = "h" if $type eq "a"; # analog is an alias for holding
    }
    $start = regconv $start, $type if $start =~ /^\d\d\d\d\d$/ or $type =~ /../;
    $end = regconv $end, $type, 1 if $end =~ /^\d\d\d\d\d$/ or $type =~ /../;
    $type = "d" if $type =~ /d./; # di, do
    $type = "h" if $type =~ /a./; # ai, ao

    my $lastfp = 0;
    if (defined $vals and $vals =~ /\S/) {
        for (split /,/, $vals) {
            if ($type eq "d") {
                if (/on|yes/i) { push @vals, 1; next; }
                if (/off|no/i) { push @vals, 0; next; }
                if (/^0*1+$/i) { push @vals, 1; next; }
                if (/^0+$/i)   { push @vals, 0; next; }
                err "Couldn't get a Boolean from `$_'; try using on,off, yes,no, or 1,0.";
            }
            if (/^0[\dXxBb]*[^0][\dA-Fa-f]*$/) {
                $_ = oct;
                err "Modbus registers are 16 bits wide; $_ is too large" unless $_ < 0x10000;
                $lastfp = 0;
                push @vals, $_;
            }
            if (/^[+-]?\d*\.\d*([Ee][+-]?\d+)?$/) {
                $lastfp = 1;
                push @vals, unpack("vv", pack("f", $_));
            }
        }

        if (@vals < ($end - $start + 1)) { # repeat last value
            while (@vals < $end - $start + 1) {
                if ($lastfp) {
                    push @vals, @vals[-2,-1];
                } else {
                    push @vals, $vals[-1];
                }
            }
        }

        if (@vals == ($end - $start + 2) and $lastfp) { # one too few regs
            $end++; # try to write to one more
        }

        if (@vals > ($end - $start + 1)) {
            mess "warning: truncating value list: ", scalar(@vals), " values given, ",
                 ($end - $start + 1), " expected - values removed: @{[ splice @vals, $end-$start+1 ]}";
        }
    }

    return ($type, $start, $end, @vals);
}

sub decode_bitstring {
    my($string, $nregs) = @_;
    $string = reverse $string; # lets us use chop()
    my($reg, $curbyte) = (0, '');
    my @ret;

    while ($reg < $nregs) {
        $curbyte = ord chop $string if ($reg % 8) == 0;
        push @ret, $curbyte & 1;
        $curbyte >>= 1;
        $reg++;
    }
    return @ret;
}

sub encode_bitstring {
    my @regs = @_;
    my($reg, $nregs) = (0, scalar @regs);
    my($ret, $curbyte) = ('', 0);

    while ($reg < $nregs) {
        if ($reg % 8 == 0 and $reg != 0) {
            $ret .= chr $curbyte;
            $curbyte = 0;
        }
        $curbyte >>= 1;
        $curbyte |= 0x80 if shift @regs;
        $reg++;
    }
    $curbyte >>= (8 - ($nregs % 8)) unless ($nregs % 8) == 0;
    $ret .= chr $curbyte;

    return $ret;
}

my $TransactionID = 1;

my @ModbusErrors = ("???", # 00
                    "illegal function", # 01
                    "illegal data address", # 02
                    "illegal data value", # 03
                    "device failure (unrecoverable)", # 04
                    "timeout", # 05
                    "device busy (retry later)", # 06
                    "unable to perform function", # 07
                    "memory parity error (retry with caution)", # 08
                    "???", # 09
                    "gateway problem (path(s) not available)", # 0A
                    "gateway problem (target failed to respond)", # 0B
                   );
sub modbus {
    my($func, $resplen, $data) = @_;
    my $sendtid = $TransactionID++;
    my $bytes = pack ("nxxnxCa*",
                      $sendtid, # Transaction ID, 2 bytes, network b.o.
                      # Protocol identifier, 2 bytes, always zero
                      length($data) + 1, # Length of payload, (function byte + data)
                      # Unit Identifier, 1 byte, always zero
                      $func, # Function code, 1 byte
                      $data, # Data, arbitrary length
                      );
    syswrite CONN, $bytes;

    sysread CONN, $bytes, 8 + $resplen;
    my $recvtid;
    ($recvtid, $resplen, $func, $data) = unpack ("nxxnxCa*", $bytes . ("\xff" x 12));
    err "Transaction ID of reply didn't match request (got $recvtid, expected $sendtid)"
         unless $sendtid == $recvtid;
    if ($func & 0x80) { # error!
        my($err, $exterr) = unpack "Cn/a*", $data;
        if ($err < scalar @ModbusErrors) {
            err sprintf ("Modbus returned error code 0x%02x (%s) [func=0x%02x]", $err,
                         $ModbusErrors[$err], $func);
        } elsif ($err == 0xFF) {
            err "Modbus returned extended error data:",
                 map { sprintf " %02x", ord $_ } split(//, $exterr);
        } else {
            err sprintf ("Modbus returned unrecognized error code 0x%02x [func=0x%02x]", $err, $func);
        }
    } else {
        return $data;
    }
}

sub request {
    my($request, $start, $end, $regtype) = splice @_, 0, 4;
    my $ret;
    my $nregs = $end - $start + 1;
    if ($request eq "READ") {
        if ($regtype eq "d") {
            $ret = modbus 0x01, $nregs/8 + 2, pack "nn", $start, $nregs;
            my $bytes = unpack "C/a*", $ret;
            return decode_bitstring ($bytes, $nregs);
        }
        elsif ($regtype eq "h" or $regtype eq "i") {
            my $code = ($regtype eq "i"? 0x04 : 0x03);
            $ret = modbus $code, $nregs*2 + 1, pack "nn", $start, $nregs;
            my($length, $bytes) = unpack "Ca*", $ret;
            err "Didn't receive the right number of registers (expected $nregs, got ".($length/2).")"
                 unless $length/2 == $nregs;
            return unpack "n" x ($length/2), $bytes;
        }
        else {
            err "Type for read is not one of [d h i]";
        }
    } elsif ($request eq "WRITE") {
        err "Argument count mismatch (want $nregs, got ".scalar(@_).")"
             if $nregs > scalar(@_);
        if ($regtype eq "d") {
            if ($nregs == 1) {
                modbus 0x05, 4, pack "nn", $start, ($_[0]? 0xFF00 : 0x0000);
            } else {
                my $bytes = encode_bitstring (@_);
                modbus 0x0F, 4, pack "nnCa*", $start, $nregs, length($bytes), $bytes;
            }
        }
        elsif ($regtype eq "h") {
            if ($nregs == 1) {
                modbus 0x06, 4, pack "nn", $start, $_[0];
            } else {
                modbus 0x10, 4, pack "nnCn*", $start, $nregs, $nregs*2, @_;
            }
        }
        else {
            err "Type for write is not one of [d h]";
        }
    } else {
        err "Request type is not one of [READ WRITE]";
    }
}

sub cmd_help {
    mess $Help;
}

sub cmd_connect {
    my($ip, $port) = @_;
    $port = 502 unless defined $port;
    err "usage: connect IP [PORT]" unless defined $ip;

    if ($Connected) {
        mess "disconnecting from $Connected";
        close CONN;
    }

    socket(CONN, PF_INET, SOCK_STREAM, getprotobyname('tcp')) or err "socket: $!";
    connect(CONN, sockaddr_in($port, inet_aton($ip))) or err "connect: $!";
    $Connected = $ip;
    mess "connected to $ip";
}

#my @colors = qw(0 1;30);
my @colors = qw(0 0);

sub cmd_read {
    my(@regs) = @_;
    @regs = ("42001:42028") if !@regs or $regs[0] =~ /^a[a-z]*$/i;
    @regs = ("02001:02008", "02417:02432") if $regs[0] =~ /^d[a-z]*$/i;
    for (@regs) {
        my($type, $start, $end) = regparse($_, 0);
        my(@vals) = request READ => $start, $end, $type;
        for ($start .. $end) {
            my $v = $vals[$_ - $start];
            printf $OUT "\e[%sm", $colors[$_ % 2] if @vals > 8;
            if ($type eq "d") {
                printf $OUT "%s ==> %3s (%d)\n",
                     regunconv($_, $type), ($v? "ON" : "OFF"), $v;
            } else {
                my $fp = "";
                if (($_ - $start) % 2 == 0 and defined $vals[$_ - $start + 1]) {
                    $fp = sprintf("%6.3f", unpack("f", pack("vv", $v, $vals[$_ - $start + 1])));
                }
                printf $OUT "%s ==> %8d  0x%04x  %08b_%08b  $fp\n",
                     regunconv($_, $type), $v, $v, ($v >> 8), ($v & 0xff);
            }
        }
        printf $OUT "\e[0m";
    }
}

sub cmd_write {
    my(@regs) = @_;
    for (@regs) {
        my($type, $start, $end, @vals) = regparse($_, 1);
        request WRITE => $start, $end, $type, @vals;
        for ($start .. $end) {
            my $v = $vals[$_ - $start];
            printf $OUT "\e[%dm", $colors[$_ % @colors] if @vals > 8;
            if ($type eq "d") {
                printf $OUT "%s <== %3s (%d)\n",
                     regunconv($_, $type), ($v? "ON" : "OFF"), $v;
            } else {
                my $fp = "";
                if (($_ - $start) % 2 == 0 and defined $vals[$_ - $start + 1]) {
                    $fp = sprintf("%6.3f", unpack("f", pack("vv", $v, $vals[$_ - $start + 1])));
                }
                printf $OUT "%s <== %8d  0x%04x  %08b_%08b  $fp\n",
                     regunconv($_, $type), $v, $v, ($v >> 8), ($v & 0xff);
            }
        }
        printf $OUT "\e[0m";
    }
}

sub handle_command {
    my($command, @words) = @_;
    return unless defined $command;
    local $_ = $command;
    s/^\s+//; s/\s+$//;
    eval {
        if (/^[Hh]/) { cmd_help(); }
        elsif (/^[QqXxEe]/) { exit(0); }
        elsif (/^[Cc]/) { cmd_connect (@words); }
        elsif (/^[Rr]/) { cmd_read (@words); }
        elsif (/^[Ww]/) { cmd_write (@words); }
        else { die "unrecognized command, use h for help\n"; }
    };
    print STDERR "error: $@" if $@;
}

if (defined $ARGV[0]) {
    cmd_connect (@ARGV);
}

while (defined ($_ = $Term->readline("PLC> "))) {
    chomp;
    my(@words) = split;
    my($command) = shift @words;
    handle_command($command, @words);
    $Term->addhistory($_) if /\S/;
}
