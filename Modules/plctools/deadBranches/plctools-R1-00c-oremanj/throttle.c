/**
 * \file throttle.c
 * \brief Low-level throttle code for Alice.
 *
 * \author Joshua Oreman
 * \date 10 July 2007
 *
 * This file defines functions used by gcdrive to access and manipulate
 * Alice's throttle (gas pedal).
 */

#include <stdlib.h>
#include <math.h>
#include <errno.h>
#include <pthread.h>
#include <unistd.h>

#include "plc.h"

int plc_throttle_check() 
{
  double pos;
  
  /* Set the position to 0. */
  if (plc_throttle_setposition (0.0) < 0)
    return 0;
  
  gcplc_do_write();
  
  usleep (100000);

  gcplc_do_read();

  /* Read the position back. */
  if ((pos = plc_throttle_getposition()) < 0)
    return 0;
  
  /* Close enough? (these are floats, we can't test equality) */
  if (fabs (pos) > 0.01)
    return 0;
  
  /* Looks good. */
  return 1;
}

/** Reference voltage (Vref). Everything else is a percentage of this. */
static float VREF = 5.0;              /* used to be a #define */

/**@{*/
/** Min/max voltages for APPS sensors 1/2/3. These are at the *edges* of the
    acceptability curve, not the centers. Don't use these for setposition! */
#define APPS1_MIN (.1497*VREF)  /**< APPS1 min voltage. */
#define APPS1_MAX (.8285*VREF)  /**< APPS1 max voltage. Note that APPS1 is reversed - it gets its max at idle, not full. */
#define APPS2_MIN (.2760*VREF)  /**< APPS2 min voltage. */
#define APPS2_MAX (.8012*VREF)  /**< APPS2 max voltage. */
#define APPS3_MIN (.1660*VREF)  /**< APPS3 min voltage. */
#define APPS3_MAX (.6912*VREF)  /**< APPS3 max voltage. */
/**@}*/
/* Idle voltages for APPS sensors 1/2/3. */
#define APPS1_IDLE (.8075*VREF) /**< APPS1 idle (no acceleration) voltage. */
#define APPS2_IDLE (.2970*VREF) /**< APPS2 idle voltage. */
#define APPS3_IDLE (.1870*VREF) /**< APPS3 idle voltage. */
/* Slope of each APPS sensor's input, in volts per degree of pedal depression. */
#define APPS1_SLOPE (-.0398*VREF)  /**< APPS1 voltage curve slope (volts per degree of pedal depression). */
#define APPS2_SLOPE (.0302*VREF)  /**< APPS2 voltage curve slope. */
#define APPS3_SLOPE (.0302*VREF)  /**< APPS3 voltage curve slope. */
/* Degrees of pedal depression corresponding to full throttle. */
#define PEDAL_MAXANGLE  15.25   /**< Maximum pedal depression angle (degrees), corresponding to full throttle. */
/* Voltage range of APPS sensors 1/2/3. */
#define APPS1_RANGE (APPS1_SLOPE * PEDAL_MAXANGLE)  /**< Range of APPS1 sensor (negative). */
#define APPS2_RANGE (APPS2_SLOPE * PEDAL_MAXANGLE)  /**< Range of APPS2 sensor. */
#define APPS3_RANGE (APPS3_SLOPE * PEDAL_MAXANGLE)  /**< Range of APPS3 sensor. */
/* Full throttle voltages for APPS sensors 1/2/3. */
#define APPS1_FULL (APPS1_IDLE + APPS1_RANGE)  /**< Full throttle voltage for APPS1 sensor. */
#define APPS2_FULL (APPS2_IDLE + APPS2_RANGE)  /**< Full throttle voltage for APPS2 sensor. */
#define APPS3_FULL (APPS3_IDLE + APPS3_RANGE)  /**< Full throttle voltage for APPS3 sensor. */

int plc_throttle_setposition (double pos) 
{
  /* Update VREF. */
  VREF = gcplc_read_analog (THROTTLE_VREF_AI);

  /*
   * Throttle uses three voltages; they have to be synchronized
   * properly for the commanded acceleration to take effect.
   */
  float apps1, apps2, apps3;
  
  apps1 = APPS1_IDLE + APPS1_RANGE * pos;
  
  apps2 = APPS2_IDLE + APPS2_RANGE * pos;
  
  apps3 = APPS3_IDLE + APPS3_RANGE * pos;
  
  gcplc_write_analog (THROTTLE_APPS1_AO, apps1);
  gcplc_write_analog (THROTTLE_APPS2_AO, apps2);
  gcplc_write_analog (THROTTLE_APPS3_AO, apps3);
  
  return 0;
}

double plc_throttle_getposition() 
{
  /* Values read from peeking the APPS output voltages on the PLC. */
  float apps[3];
  /* The average of those values. */
  double apps_avg;
  /* Those values scaled to the [0, 1] range. */
  double pos[3];
  /* Average of the component_pos[i] values. */
  double pos_avg;
  /* Misc. */
  int i;
  
  /* Update VREF. */
  VREF = gcplc_read_analog (THROTTLE_VREF_AI);

  /* Read 'em in. */
  apps[0] = gcplc_peek_analog (THROTTLE_APPS1_AO);
  apps[1] = gcplc_peek_analog (THROTTLE_APPS2_AO);
  apps[2] = gcplc_peek_analog (THROTTLE_APPS3_AO);
  
  /* Various sanity checks. */
  for (i = 0; i < 3; i++) {
    double max = (i == 0)? APPS1_MAX : (i == 1)? APPS2_MAX : APPS3_MAX;
    double min = (i == 0)? APPS1_MIN : (i == 1)? APPS2_MIN : APPS3_MIN;
    double idle = (i == 0)? APPS1_IDLE : (i == 1)? APPS2_IDLE : APPS3_IDLE;
    double range = (i == 0)? APPS1_RANGE : (i == 1)? APPS2_RANGE : APPS3_RANGE;

    /* Can't be greater than maximum or less than minimum. */
    if (apps[i] > max || apps[i] < min)
      return PLC_THROTTLE_POS_INVALID;
    
    /* Scale it to [0, 1]. */
    pos[i] = (apps[i] - idle) / range;
  }
  
  /* Average apps and double-check it. */
  apps_avg = (apps[0] + apps[1] + apps[2]) / 3.0;
  for (i = 0; i < 3; i++) {
    /* The difference between apps[i] and apps_avg should not
       exceed 3.2% of Vref. */
    if (fabs ((double)apps[i] - apps_avg) > .032*VREF)
      return PLC_THROTTLE_POS_INVALID;
  }
  
  /* Looks good. Average the positions and return them. */
  pos_avg = (pos[0] + pos[1] + pos[2]) / 3.0;
  
  /* Normalize return value to [0, 1]. */
  if (pos_avg < 0.0) pos_avg = 0.0;
  if (pos_avg > 1.0) pos_avg = 1.0;
  
  return pos_avg;
}

/* Local Variables: */
/* mode: c */
/* indent-tabs-mode: nil */
/* c-basic-offset: 2 */
/* End: */
