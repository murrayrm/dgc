/**
 * \file plc.h
 * \brief Stub header file for including all PLC-related functionality.
 *
 * Joshua Oreman
 * 2 July 2007
 */

#ifndef __PLC_H__
#define __PLC_H__

#include "modbus.h"
#include "gcplc.h"
#include "brake.h"
#include "throttle.h"
#include "igntrans.h"

/** The IP address of the PLC in the lab. */
#define PLC_ADDR_LAB "192.168.1.75"
/** The IP address of the PLC in Alice. */
#define PLC_ADDR_ALICE "192.168.0.75"

/* Configuration of the PLC in Alice. CHANGE THIS! when you modify things. */

#define THROTTLE_VREF_AI    0   /**< Analog input 0: voltage reference for throttle interface */
#define THROTTLE_VREF2_AI   1   /**< Analog input 1: other voltage reference (currently unused) */
#define BRAKE_PRESSURE_AI   2   /**< Analog input 2: brake pressure sensor (currently not connected) */
#define UNALLOCATED_AI_3    3   /**< Analog input 3: unassigned. */
#define UNALLOCATED_AI_4    4   /**< Analog input 4: unassigned. */
#define UNALLOCATED_AI_5    5   /**< Analog input 5: unassigned. */
#define UNALLOCATED_AI_6    6   /**< Analog input 6: unassigned. */
#define UNALLOCATED_AI_7    7   /**< Analog input 7: unassigned. */

#define THROTTLE_APPS1_AO   0   /**< Analog output 0: accelerator pedal position, wire 1. */
#define THROTTLE_APPS2_AO   1   /**< Analog output 1: accelerator pedal position, wire 2. */
#define THROTTLE_APPS3_AO   2   /**< Analog output 2: accelerator pedal position, wire 3. */
#define UNALLOCATED_AO_1    3   /**< Analog output 3: unassigned. */

#define ESTOP_DISABLE_DI    0   /**< Digital input 0: estop disable switches (active when FALSE) */
#define ESTOP_RUN_DI        1   /**< Digital input 1: estop in run mode (running when FALSE) */
#define ESTOP_RPAUSE_DI     2   /**< Digital input 2: DARPA/remote pause requested? (pause when FALSE) */
#define ESTOP_RDISABLE_DI   3   /**< Digital input 3: DARPA/remote disable requested? (disable when FALSE) */
#define TONE_ENABLE_DI      4   /**< Digital input 4: warning tone on? (tone enabled when FALSE) */
#define ESTOP_RESET_DI      5   /**< Digital input 5: estop reset (reset when FALSE) */
#define UNALLOCATE_DI_6     6   /**< Digital input 6: unassigned. */
#define UNALLOCATE_DI_7     7   /**< Digital input 7: unassigned. */

/* The output bits on the DIO module are not controllable by software, but here's their correspondence. */
/* Output 0: DISABLE LED (red) */
/* Output 1: PAUSE LED (yellow) */
/* Output 2: RUN LED (green) */
/* Output 3..7: unassinged */

#define BRAKE_POS_DO        0   /**< Digital relay outputs 0 through 3: brake pedal position. */
#define BRAKE_POS_BITS      4   /**< Number of bits in brake pedal position output. */
#define TRANS_POS_DO        4   /**< Digital relay outputs 4 through 6: transmission position. */
#define TRANS_POS_BITS      3   /**< Number of bits in transmission position output. */
#define EBRAKE_DO           7   /**< Digital relay output 7: emergency brake (activated when OPEN = FALSE) */

#endif
