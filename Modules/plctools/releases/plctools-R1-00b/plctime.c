#include "plc.h"
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>

unsigned long long gettime() 
{
    struct timeval tv;
    gettimeofday (&tv, NULL);
    return (unsigned long long)tv.tv_usec + (unsigned long long)tv.tv_sec * 1000000ULL;
}

#define PLC_ADDR "192.168.1.75"

int sum_wrlat, sum_rdlat, sum_setlat;
int nr_lat;

void sigint_handler (int sig) 
{
    printf ("Averages:\n");
    printf (" Write latency: %9.2f\n", (double)sum_wrlat / (double)nr_lat);
    printf ("  Read latency: %9.2f\n", (double)sum_rdlat / (double)nr_lat);
    printf ("Settle latency: %9.2f\n", (double)sum_setlat / (double)nr_lat);
    exit (0);
}

int main (int argc, char **argv) 
{
    int SLEEP = 0;
    if (argv[1]) SLEEP = atoi (argv[1]);

    signal (SIGINT, sigint_handler);

    unsigned long long t[16];
    modbus_t *mb = modbus_create (PLC_ADDR);

    sum_wrlat = sum_rdlat = sum_setlat = nr_lat = 0;

    for(;;) {
        modbus_reg_t magic, more_magic;
        magic = rand() & 0xFFFF;
        
        t[0] = gettime();
        usleep (SLEEP);
        t[1] = gettime();
        modbus_write_registers (mb, 2026, 1, &magic);
        t[2] = gettime();
        usleep (SLEEP);
        t[3] = gettime();
        modbus_read_registers (mb, 2027, 1, &more_magic);
        t[4] = t[5] = gettime();
        while (more_magic != magic) {
            usleep (SLEEP);
            modbus_read_registers (mb, 2027, 1, &more_magic);
            t[5] = gettime();
        }

        int wrlat, rdlat, setlat;
        wrlat = t[2] - t[1];
        rdlat = t[4] - t[3];
        setlat = t[5] - t[4];
        printf ("%lld: write +%d, read +%d, settle +%d\n",
                t[0], wrlat, rdlat, setlat);

        sum_wrlat += wrlat;
        sum_rdlat += rdlat;
        sum_setlat += setlat;
        nr_lat++;
    }
}
