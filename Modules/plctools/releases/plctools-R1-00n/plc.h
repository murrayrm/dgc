/**
 * \file plc.h
 * \brief Stub header file for including all PLC-related functionality.
 *
 * Joshua Oreman
 * 2 July 2007
 */

#ifndef __PLC_H__
#define __PLC_H__

#include "modbus.h"
#include "gcplc.h"
#include "brake.h"
#include "throttle.h"
#include "igntrans.h"

/** The IP address of the PLC in the lab. */
#define PLC_ADDR_LAB "192.168.1.75"
/** The IP address of the PLC in Alice. */
#define PLC_ADDR_ALICE "192.168.0.75"

/* Configuration of the PLC in Alice. CHANGE THIS! when you modify things. */

#define THROTTLE_VREF_AI    0   /**< Analog input 0: voltage reference for throttle interface */
#define THROTTLE_VREF2_AI   1   /**< Analog input 1: other voltage reference (currently unused) */
#define BRAKE_PRESSURE_AI   2   /**< Analog input 2: brake pressure sensor (currently not connected) */
#define UNALLOCATED_AI_3    3   /**< Analog input 3: unassigned. */
#define UNALLOCATED_AI_4    4   /**< Analog input 4: unassigned. */
#define UNALLOCATED_AI_5    5   /**< Analog input 5: unassigned. */
#define UNALLOCATED_AI_6    6   /**< Analog input 6: unassigned. */
#define UNALLOCATED_AI_7    7   /**< Analog input 7: unassigned. */

#define THROTTLE_APPS1_AO   0   /**< Analog output 0: accelerator pedal position, wire 1. */
#define THROTTLE_APPS2_AO   1   /**< Analog output 1: accelerator pedal position, wire 2. */
#define THROTTLE_APPS3_AO   2   /**< Analog output 2: accelerator pedal position, wire 3. */
#define UNALLOCATED_AO_1    3   /**< Analog output 3: unassigned. */

#define ESTOP_DISABLE_DI    0   /**< Digital input 0: estop disable switches (active when FALSE) */
#define ESTOP_RUN_DI        1   /**< Digital input 1: estop in run mode (running when FALSE) */
#define ESTOP_RPAUSE_DI     2   /**< Digital input 2: DARPA/remote pause requested? (pause when FALSE) */
#define ESTOP_RDISABLE_DI   3   /**< Digital input 3: DARPA/remote disable requested? (disable when FALSE) */
#define TONE_LOUD_DI        4   /**< Digital input 4: warning tone strength? (tone loud when TRUE) */
#define ESTOP_RESET_DI      5   /**< Digital input 5: estop reset (reset when FALSE) */
#define THROTTLE_MAN_DI     6   /**< Digital input 6: throttle manual/auto (auto when FALSE) */
#define TRANS_MAN_DI        7   /**< Digital input 7: transmission manual/auto (auto when FALSE) */
#define BRAKE_MAN_DI        8   /**< Digital input 8: brake manual/auto (auto when FALSE) */
#define LIGHTS_ON_DI        9   /**< Digital input 9: lights on/off (on when TRUE) */
#define TONE_ON_DI         10   /**< Digital inpt 10: tone on/off (on when TRUE) */
#define UNALLOCATED_DI_11  11   /**< Digital inpt 11: unallocated */
#define DISABLE_LED_DIO    12   /**< Digital out  12: disable LED (on when FALSE) */
#define PAUSE_LED_DIO      13   /**< Digital out  13: pause LED (on when FALSE) */
#define RUN_LED_DIO        14   /**< Digital out  14: run LED (on when FALSE) */
#define UNALLOCATED_DIO_15 15   /**< Digital out  15: unallocated */

#define BRAKE_POS_DO        0   /**< Digital relay outputs 0 through 3: brake pedal position. */
#define BRAKE_POS_BITS      4   /**< Number of bits in brake pedal position output. */
#define TRANS_POS_DO        4   /**< Digital relay outputs 4 through 6: transmission position. */
#define TRANS_POS_BITS      3   /**< Number of bits in transmission position output. */
#define EBRAKE_DO           7   /**< Digital relay output 7: emergency brake (activated when OPEN = FALSE) */

#endif
