/**
 * \file throttle.h
 * \brief Low-level throttle interface for Alice.
 *
 * \author Joshua Oreman
 * \date 10 July 2007
 *
 * This file declares functions that access the PLC via Modbus to
 * control Alice's throttle. The API is largely based on the old
 * throttle code.
 */

#ifndef __PLC_THROTTLE_H__
#define __PLC_THROTTLE_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \brief Does a brief sanity check on the throttle.
 *
 * This currently just sets the throttle position to 0 and does a sanity check
 * by attempting to read it back. It does \e not verify whether the throttle
 * actually can be actuated; such a test would by its nature be rather dangerous.
 * (Short version: if a wire is cut or something, this still might "pass".)
 *
 * \return 1 if the sanity check passed, 0 if it failed.
 */
int plc_throttle_check (void);

/**
 * \brief Sets the throttle position, akin to changing the angle of the gas pedal.
 *
 * \param pos The position to set. This should be a floating-point value between 0.0
 *            and 1.0; 0.0 is no positive acceleration, while 1.0 is full throttle.
 * \return 0 on success, -1 and sets \c errno on failure.
 */
int plc_throttle_setposition (double pos);

/**
 * \brief Gets the last good throttle position.
 *
 * This retrieves the position commanded by the <i>last voltages sent to the PLC</i> - it
 * does \e not necessarily indicate whether the acceleration succeeded (though the only
 * exceptions are cases of grievous mechanical error).
 *
 * If the three voltages read are not properly synchronized with each other, returns
 * PLC_THROTTLE_POS_INVALID.
 *
 * If the device cannot be accessed, returns PLC_THROTTLE_POS_ERROR and sets \c errno.
 *
 * It is safe to test the return value for equality against these values.
 */
double plc_throttle_getposition (void);

/** Value returned from plc_throttle_getposition() when an error occurs reading the voltages. */
#define PLC_THROTTLE_POS_ERROR   -1.0
/** Value returned from plc_throttle_getposition() when the voltages read do not agree with each other. */
#define PLC_THROTTLE_POS_INVALID -2.0

#ifdef __cplusplus
}
#endif

#endif /* __THROTTLE_H__ */
