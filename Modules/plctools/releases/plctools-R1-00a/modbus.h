/**
 * \file modbus.h
 * \brief Header file for various functions having to do with Modbus communication.
 *
 * Joshua Oreman
 * 2 July 2007
 *
 * This file declares functions to be used for interfacing with the Programmable
 * Logic Controller in Alice via Modbus/TCP.
 */

#ifndef __MODBUS_H__
#define __MODBUS_H__

struct modbus_t;

/**
 * The Modbus object. It's opaque for a reason; don't mess with it.
 */
typedef struct modbus_t modbus_t;

/**
 * A Modbus register (unsigned short).
 */
typedef unsigned short modbus_reg_t;

/** The default port for a Modbus connection (502). */
#define MODBUS_DEFAULT_PORT 502

/**
 * \defgroup general Device-independent functions.
 */
/*@{*/

/**
 * \brief Creates a new modbus structure connected to the device at a given address.
 * \param ip_addr The IP address of the device to connect to. Use inet_ntoa() to get
 *                a dotted-decimal address into proper form, or gethostbyname() to do
 *                the same for a hostname.
 * \param port The port to connect to. Usually this should be MODBUS_DEFAULT_PORT (502).
 *             Supplying 0 means this default port as well.
 * \return A newly allocated modbus_t connected to the given address, or NULL if the
 *         allocation or connection failed. In case of failure, \c errno contains
 *         the error code returned by the socket function that failed, or \c ENOMEM
 *         if allocation failed.
 */
modbus_t *modbus_create (unsigned int ip_addr, int port);

/**
 * \brief Disconnects and deallocates a Modbus structure.
 * \param self The Modbus structure to destroy.
 */
void modbus_destroy (modbus_t *self);

/**
 * \brief Sets the timeout used for network operations with the
 * connected device.
 *
 * \param self The Modbus object to operate on.
 * \param timeout The timeout to use, in milliseconds. 0 means
 * not to wait for any replies at all, which is probably \e not what
 * you want. Use -1 for no timeout (the default).
 */
void modbus_set_timeout (modbus_t *self, int timeout);

/*@}*/

/**
 * \defgroup error Error codes and error-reporting functions.
 */
/*@{*/

/**
 * \brief Returns the error code of the most recently called
 * function for the given Modbus object.
 *
 * Whenever an error condition occurs, \c errno is also set;
 * see the documentation for the individual error codes to
 * determine the mapping. It's more-or-less sensible.
 *
 * If a function succeeds, \c errno is \e not cleared.
 *
 * \return 0 if the function succeeded, or one of the MODBUS_ERR_* codes
 * otherwise. If the code is MODBUS_ERR_EXTD, call modbus_get_extra_error()
 * for specifics.
 */
int modbus_get_error (modbus_t *self);

/** Illegal function. The function code in the Modbus query was disallowed or invalid. Maps to EPERM. */
#define MODBUS_ERR_ILLEGAL_FUNCTION           0x01

/** Illegal data address. A data address (e.g. a register number) was invalid. Maps to EFAULT. */
#define MODBUS_ERR_ILLEGAL_DATA_ADDRESS       0x02

/** Illegal data value. A data value (e.g. something to be written to a register) was invalid.
 * Maps to EINVAL. */
#define MODBUS_ERR_ILLEGAL_DATA_VALUE         0x03

/** Server device failure. The device failed during execution, with an unrecoverable error. Maps to EIO. */
#define MODBUS_ERR_DEVICE_FAILURE             0x04

/** Timeout. No message (including code 5 "acknowledge" messages) was received within five seconds.
 * Maps to ETIMEDOUT. */
#define MODBUS_ERR_TIMEOUT                    0x05

/** Server device busy. The device is busy processing a long-duration command; try again later.
 * Maps to EAGAIN. */
#define MODBUS_ERR_DEVICE_BUSY                0x06

/** Negative acknowledge. The device can't perform the function code requested, despite its being
 * a valid function code. Maps to ENOSYS. */
#define MODBUS_ERR_UNABLE_TO_PERFORM_FUNCTION 0x07

/** Memory parity error. The device detected a parity error while reading extended memory.
 * A retry may succeed, but service on the device is probably warranted. Maps to ENOMEM. */
#define MODBUS_ERR_MEMORY_PARITY              0x08

/** Gateway problem: gateway path(s) not available. Maps to EHOSTUNREACH. */
#define MODBUS_ERR_GW_PATH_NOT_AVAILABLE      0x0A

/** Gateway problem: the target device failed to respond. Maps to EHOSTDOWN. */
#define MODBUS_ERR_GW_NO_TARGET_RESPONSE      0x0B

/** Extended error information is available. Use modbus_get_extra_error() for the raw bytes. */
#define MODBUS_ERR_EXTD                       0xFF

/**
 * \brief Fetches any extra error information that's available.
 *
 * Extended errors in Modbus have no defined format; they're just a short string of
 * bytes with a length attached. They may or may not be human-readable.
 *
 * \param self The Modbus object to operate on.
 * \param buf A buffer in which to store the extended error information. If you pass
 *            NULL, an internal buffer will be used which is only valid until the next
 *            extended error occurs on this device.
 * \param len Pointer to an \c int where the length of \a buf is stored. The value will
 *            be updated to hold the actual length of the error information. If you pass
 *            NULL for \a buf, the pointee's initial value is ignored.
 * \return \a buf if it was non-NULL, a pointer to an internal buffer if not, or NULL if
 *         no extended error information is available.
 */
unsigned char *modbus_get_extra_error (modbus_t *self, unsigned char *buf, int *len);

/*@}*/

/**
 * \defgroup lowlevel Low-level (command-based) API.
 *
 * This API is suitable for use with any Modbus device, even one with esoteric command
 * support, but you'll have to marshal and unmarshal packet payloads yourself.
 * Not recommended unless the higher-level APIs do not support your application.
 */
/*@{*/

/**
 * \brief Executes a raw Modbus command.
 *
 * \param self The Modbus object to operate on.
 * \param func The function code to send.
 * \param data The payload to send.
 * \param datalen Length of \a data.
 * \param resp Where to store the response payload (can be NULL to use internal buffer).
 * \param resplen Length of \a resp (ignored if \a resp is NULL).
 * \param timeout Number of milliseconds to await a response. If zero, returns immediately
 *                after sending the packet, ignoring the response. (You can use modbus_wait() to
 *                get the response later.) For no timeout, pass a negative value.
 * \return Nonzero on success, 0 and sets modbus_get_error() on failure. If \a timeout was
 *         zero, the value returned is the transaction ID that was used; you may pass this
 *         to modbus_wait().
 */
int modbus_command (modbus_t *self, int func,
                    const void *data, int datalen,
                    void *resp, int resplen,
                    int timeout);

/**
 * \brief Awaits a reply from the Modbus controller.
 *
 * \param self The Modbus object to operate on.
 * \param tid The transaction ID to await a reply on; use the return code of modbus_command().
 * \param resp A buffer in which to store the response; must have an allocation of at least \a resplen bytes.
 * \param resplen The length of the \a resp buffer, in bytes.
 * \param timeout The amount of time to wait for a response. Note that the wait could possibly
 * be longer if the device sends error code 5 packets ("acknowledge long job"); when such a packet
 * is received, the timeout is reset.
 */
int modbus_wait (modbus_t *self, int tid,
                 void *resp, int resplen,
                 int timeout);

/*@}*/

/**
 * \defgroup midlevel Register-level interface.
 *
 * These functions allow you to read and write arbitrary sequences of registers and coils.
 * They should suffice for any ordinary Modbus device, not just the PLC used in Alice.
 */
/*@{*/

/**
 * \brief Reads a contiguous sequence of coils from the connected device.
 *
 * Coils are one-bit values, used for digital inputs and outputs. For
 * use with Alice, it is recommended that you use modbus_read_digital();
 * this function is lower-level.
 *
 * Coils are written in an unpacked form, one per byte: \c 0xFF if the
 * coil is set, \c 0x00 if it is not set.
 *
 * \param self The Modbus object to operate on.
 * \param start The first coil to read; must be between 0 and 9999 decimal.
 * \param ncoils The number of coils to read; must be less than 1984.
 * \param outputs A buffer to write the read values to. It must be allocated
 *                to hold at least \c ncoils bytes.
 * \return 1 on success, 0 on error. The error code may be read using either modbus_get_error()
 *         or from the \c errno variable; see the documentation for modbus_get_error()
 *         for the translation between Modbus errors and errno codes.
 */
int modbus_read_coils (modbus_t *self, int start, int ncoils, unsigned char *outputs);

/**
 * \brief Writes a contiguous sequence of coils to the connected device.
 *
 * Coils are one-bit values, used for digital inputs and outputs. For
 * use with Alice, it is recommended that you use modbus_write_digital();
 * this function is lower-level.
 *
 * Coils are read in an unpacked form, one per byte: a coil is set 
 * if its byte is nonzero, cleared if its byte is zero.
 *
 * \param self The Modbus object to operate on.
 * \param start The first coil to write; must be between 0 and 9999 decimal.
 * \param ncoils The number of coils to write; must be less than 1984.
 * \param inputs A buffer to read the coils from. It must have at least \c ncoils bytes.
 * \return 1 on success, 0 on error. The error code may be read using either modbus_get_error()
 *         or from the \c errno variable; see the documentation for modbus_get_error()
 *         for the translation between Modbus errors and errno codes.
 */
int modbus_write_coils (modbus_t *self, int start, int ncoils, const unsigned char *inputs);



/**
 * \brief Reads a contiguous sequence of registers from the connected device.
 *
 * This function uses the Modbus command 0x03 (Read Holding Registers)
 * to read a sequence of registers from the device. Registers as
 * defined on Alice's PLC may be given using the \c MODBUS_* defines,
 * but it is recommended that applications use the higher-level
 * modbus_read_analog() and modbus_read_digital() functions instead.
 * Those functions use this one internally.
 *
 * Modbus registers are 16 bits wide.
 *
 * This function does not cache its results in any way.
 *
 * \param self The Modbus object to operate on.
 * \param start The first register to read; should be between 0 and 9999 decimal.
 *              (Modbus registers are sometimes specified as 4xxxx; to convert from
 *              that form to this one, subtract 40001.)
 * \param nregs The number of registers to read; must be less than 124.
 * \param output A buffer the register values will be written to, in numerical order.
 *               Must have space for at least \a nregs * 2 bytes.
 * \return 1 on success, 0 on error. The error code may be read using either modbus_get_error()
 *         or from the \c errno variable; see the documentation for modbus_get_error()
 *         for the translation between Modbus errors and errno codes.
 */
int modbus_read_registers (modbus_t *self, int start, int nregs, modbus_reg_t *output);

/**
 * \brief Writes a contiguous set of registers to the connected device.
 *
 * As with modbus_read_registers(), this command is not recommended for high-level
 * use of Alice's PLC. Use modbus_write_analog() or modbus_write_digital() instead.
 *
 * The notes attached to modbus_read_registers() apply to this function as well.
 *
 * \param self The Modbus object to operate on.
 * \param start The first register to write; should be between 40001 and 49999 decimal.
 * \param nregs The number of registers to write; must be less than 124.
 * \param input A buffer the register values will be read from, in numerical order.
 *              Must have at least \a nregs * 2 bytes of data.
 * \return 1 on success, 0 on error. The error code may be read using either modbus_get_error()
 *         or from the \c errno variable; see the documentation for modbus_get_error()
 *         for the translation between Modbus errors and errno codes.
 */
int modbus_write_registers (modbus_t *self, int start, int nregs, const modbus_reg_t *input);

/*@}*/

/**
 * \defgroup alicedefs Definitions for the PLC in Alice.
 *
 * Some of these will need to be changed if we add new inputs, etc.
 *
 * There are architectural issues with having more 16 digital inputs or
 * more than 16 digital outputs (each one is a bit in a single 16-bit word),
 * but I doubt that'll be a problem.
 */
/*@{*/

/** Coil containing digital input \a x. \a x should be between 0 and 7. */
#define MODBUS_COIL_DI(x) (2000+(x))

/** Coil containing digital output \a x. \a x should be between 0 and 15. */
#define MODBUS_COIL_DO(x) (2016+(x))

/** Register containing digital inputs. */
#define MODBUS_REG_DI   2000

/** Register containing digital outputs. */
#define MODBUS_REG_DO   2001

/** \brief Returns the first register for the given analog input.
 * \a x should be between 0 and 7.
 *
 * (Each analog input is a four-byte floating point value that
 * takes up two registers.)
 */
#define MODBUS_REG_AI(x) (2002+2*(x))

/** Returns the first register for the given analog output. \a x should
 * be between 0 and 3. */
#define MODBUS_REG_AO(x) (2018+2*(x))

/** The number of digital inputs configured. Must be <= 16. */
#define NR_DIGITAL_IN    8
/** The number of digital outputs configured. Must be <= 16. */
#define NR_DIGITAL_OUT  16
/** The number of digital inputs configured. No limit, but remember
 * to change the definition of MODBUS_REG_AO() above if you modify. */
#define NR_ANALOG_IN     8
/** The number of digital inputs configured. No limit. */
#define NR_ANALOG_OUT    4

/*@}*/

/** \defgroup updatedefs Flag definitions for modbus_update(). */
/*@{*/
/** Reread the digital inputs register. */
#define MODBUS_UPDATE_DI       0x01
/** Reread the digital outputs register. */
#define MODBUS_UPDATE_DO       0x02
/** Reread the analog input registers, and use the cache for future analog reads. */
#define MODBUS_UPDATE_AI       0x04
/** Reread the analog output registers, and use the cache for future analog peeks. */
#define MODBUS_UPDATE_AO       0x08
/** Combination of MODBUS_UPDATE_DI and MODBUS_UPDATE_DO. */
#define MODBUS_UPDATE_DIGITAL  0x03
/** Combination of MODBUS_UPDATE_AI and MODBUS_UPDATE_AO. */
#define MODBUS_UPDATE_ANALOG   0x0C
/** Combination of MODBUS_UPDATE_DI and MODBUS_UPDATE_AI. */
#define MODBUS_UPDATE_INPUTS   0x05
/** Combination of MODBUS_UPDATE_DO and MODBUS_UPDATE_AO. */
#define MODBUS_UPDATE_OUTPUTS  0x0A
/** Combination of MODBUS_UPDATE_INPUTS and MODBUS_UPDATE_OUTPUTS. */
#define MODBUS_UPDATE_ALL      0x0F
/*@}*/

/**
 * \defgroup highlevel High-level interface
 * The following functions are intended for use by applications controlling the PLC as
 * it is configured in Alice specifically. They all cache their results, and read from
 * that cache when possible; you must call modbus_update() whenever you want changes to
 * be visible.
 *
 * Digital values are always read in a block (all at once), so you must call modbus_update()
 * each time through your main loop when working with them. Analog values are read
 * when requested, or you can read all of them at once by passing flags to
 * to modbus_update(). In all cases, written values cause their own command to go
 * out over the wire, and cause a complete invalidation of the cache - so interleaving
 * writes and reads might result in a slight performance decline.
 *
 * You can see the values of output registers by passing appropriate flags to
 * modbus_update(), and using modbus_peek_digital() or modbus_peek_analog() to
 * fetch the values so read.
 */
/*@{*/

/**
 * \brief Refills parts of the read cache for the given Modbus object based on the registers of
 * the device it is connected to.
 *
 * This function updates the connected device based on the given \a flags, bitwise ORed together:
 * - \c MODBUS_UPDATE_DI causes the digital inputs register (42001) to be reread.
 *   This is necessary whenever you are using digital inputs.
 * - \c MODBUS_UPDATE_DO causes the digital outputs register (42002) to be reread.
 *   This is only necessary if you're using modbus_peek_digital().
 * - \c MODBUS_UPDATE_AI causes the analog input regsiters (42003-42018) to be reread.
 *   This will speed things up if you are doing a lot of analog register reads.
 * - \c MODBUS_UPDATE_AO causes the analog output registers (42019-42026) to be reread.
 *   This will speed things up if you're doing a lot of modbus_peek_analog().
 * 
 * There are also some shortcuts available:
 * - \c MODBUS_UPDATE_DIGITAL is equivalent to both _DI and _DO.
 * - \c MODBUS_UPDATE_ANALOG is equivalent to both _AI and _AO.
 * - \c MODBUS_UPDATE_INPUTS is equivalent to both _DI and _AI.
 * - \c MODBUS_UPDATE_OUTPUTS is equivalent to both _DO and _AO.
 * - \c MODBUS_UPDATE_ALL is the logical OR of all four.
 *
 * \param self The Modbus object to operate on.
 * \param flags Specifies what to update; see above.
 * \return 1 on success, 0 on error. The error code may be read using either modbus_get_error()
 *         or from the \c errno variable; see the documentation for modbus_get_error()
 *         for the translation between Modbus errors and errno codes.
 */
int modbus_update (modbus_t *self, int flags);

/**
 * \brief Reads a digital input from the connected device.
 * You must have called modbus_update(MODBUS_UPDATE_DI) recently,
 * or written an output, for the value returned to be timely,
 * because it is read from the Modbus object's read cache.
 *
 * If the cache is not valid, this function calls modbus_update with
 * \c MODBUS_UPDATE_DI for \a flags.
 *
 * \param self The Modbus object to operate on.
 * \param nr The input number to read; should be between 0 and 7.
 * \return 1 if the input is set, 0 if not, -1 and sets modbus_get_error()
 *         on failure.
 */
int modbus_read_digital (modbus_t *self, int nr);

/**
 * \brief Reads all digital inputs from the connected device.
 * This will call modbus_update(MODBUS_UPDATE_DI) automatically.
 *
 * \param self The Modbus object to operate on.
 * \param regp Pointer to a 16-bit variable that will be written with
 *             the current state of the inputs (coil 0 in bit 0,
 *             coil 1 in bit 1, etc.)
 * \return 1 on success, 0 and sets modbus_get_error() on failure.
 */
int modbus_read_all_digital (modbus_t *self, modbus_reg_t *regp);

/**
 * \brief Peeks at a digital output on the connected device.
 * You must have called modbus_update(MODBUS_UPDATE_DO) recently,
 * or written an output, for the value returned to be timely,
 * because it is read from the Modbus object's read cache.
 *
 * If the cache is not valid, this function calls modbus_update with
 * \c MODBUS_UPDATE_DO for \a flags.
 *
 * \param self The Modbus object to operate on.
 * \param nr The output number to read; should be between 0 and 15.
 * \return 1 if the output is set, 0 if not, -1 and sets modbus_get_error()
 *         on failure.
 */
int modbus_peek_digital (modbus_t *self, int nr);

/**
 * \brief Peeks at all digital outputs on the connected device.
 * This will call modbus_update(MODBUS_UPDATE_DO) automatically.
 *
 * \param self The Modbus object to operate on.
 * \param regp Pointer to a 16-bit variable that will be written with
 *             the current state of the outputs (coil 0 in bit 0,
 *             coil 1 in bit 1, etc.)
 * \return 1 on success, 0 and sets modbus_get_error() on failure.
 */
int modbus_peek_all_digital (modbus_t *self, modbus_reg_t *regp);

/**
 * \brief Writes a digital output to the connected device.
 * This invalidates all read caches, analog and digital.
 *
 * \param self The Modbus object to operate on.
 * \param nr The output number to write; should be between 0 and 15.
 * \param val The value to write. Nonzero values set the output, zero clears it.
 * \return 1 on success, 0 and sets modbus_get_error() on failure.
 */
int modbus_write_digital (modbus_t *self, int nr, unsigned char val);

/**
 * \brief Writes all the digital outputs at once on the connected device.
 * This invalidates all read caches.
 *
 * \warning This functions creates definite potential for various sorts of
 * race conditions if more than one thread/process is accessing the device
 * at once. <i>Do not use it unless you know what you are doing.</i>
 *
 * \param self The Modbus object to operate on.
 * \param val The value to write to the digital outputs register; digital output
 * \a n is turned ON if bit \a n of \a val is set, OFF otherwise.
 * \return 1 on success, 0 and sets modbus_get_error() on failure.
 */
int modbus_write_all_digital (modbus_t *self, modbus_reg_t val);

/**
 * \brief Reads an analog input from the connected device.
 * 
 * If the cache of analog inputs is valid, as when set by modbus_update(),
 * the value is returned from this cache; otherwise it is read from
 * the device and returned. 
 *
 * If the cache is not valid, it is <i>left unchanged</i>.
 *
 * \param self The Modbus object to operate on.
 * \param nr The input number to read; should be between 0 and 7.
 * \param valp Pointer to a \c float where the input value will be stored on success.
 *             On failure, set to NaN.
 * \return 1 on success, 0 and sets modbus_get_error() on failure.
 */
int modbus_read_analog (modbus_t *self, int nr, float *valp);

/**
 * Peeks at an analog output on the connected device.
 *
 * If the cache of analog outputs is valid, as when set by modbus_update(),
 * the value is returned from this cache; otherwise it is read from
 * the device and returned. 
 *
 * If the cache is not valid, it is <i>left unchanged</i>.
 *
 * \param self The Modbus object to operate on.
 * \param nr The output number to read; should be between 0 and 3.
 * \param valp Pointer to a \c float where the output value will be stored on success.
 *             On failure, set to NaN.
 * \return 1 on success, 0 and sets modbus_get_error() on failure.
 */
int modbus_peek_analog (modbus_t *self, int nr, float *valp);

/**
 * \brief Writes an analog output to the connected device.
 * This invalidates all read caches, analog and digital.
 *
 * \param self The Modbus object to operate on.
 * \param nr The output number to write; should be between 0 and 3.
 * \param val The value to write, as a floating-point number indicating
 *            voltage.
 * \return 1 on success, 0 and sets modbus_get_error() on failure.
 */
int modbus_write_analog (modbus_t *self, int nr, float val);

/*@}*/

#endif
