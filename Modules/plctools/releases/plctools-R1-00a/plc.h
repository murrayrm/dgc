/**
 * \file plc.h
 * \brief Stub header file for including all PLC-related functionality, just in case there's ever more of it.
 *
 * Joshua Oreman
 * 2 July 2007
 */

#ifndef __PLC_H__
#define __PLC_H__

#include "modbus.h"

#endif
