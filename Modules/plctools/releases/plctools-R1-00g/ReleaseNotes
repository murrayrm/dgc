              Release Notes for "plctools" module

Release R1-00g (Mon Jul 30 19:49:34 2007):
	Changed the layout of the PLC a little. (trans has 3 bits of output, etc)

Release R1-00f (Wed Jul 25 11:40:49 2007):
	Add support for E-stop. Rearrange registers to work with new,
	saner layout on the PLC - now complete reads and writes can
	both be done in one command. Added support for watchdog timer
	on PLC.

Release R1-00e (Sat Jul 21 16:50:56 2007):
	Updated recursive mutex attribute to work under MACOSX.  No
	change in functionality.

Release R1-00d (Sat Jul 21 15:25:16 2007):
	Added support for reading Vref and basing throttle outputs on that.

Release R1-00c (Tue Jul 17 21:13:02 2007):
	Added gcplc_do_cond_write() for use in a dedicated writer thread - it
	blocks, using a condition variable, until a write is actually necessary.

Release R1-00b (Tue Jul 17 12:47:18 2007):
	I'm moving all the PLC actuator drivers from gcdrive to this module.
	(I never released gcdrive with them in it, but that's where I had
	been developing them previously.) I believe it makes more sense:
	they're pretty tightly coupled to the PLC, and this allows me to
	develop them without stepping on the toes of anyone working on
	the rest of gcdrive. The "old" drivers will remain in gcdrive,
	and can be compiled in; these should not clash with them.
	
	The actuator API is now:  (xxx = throttle, brake, ign, trans)
	    plc_xxx_check() : does a sanity check of some sort, returns 1
			      if it passed and 0 if it failed
	    plc_xxx_setposition(...) : sets the actuator's position
				       ... is a double for brake and throttle,
					      a #defined int for ign and trans
	    plc_xxx_getposition() : returns the last validly set position,
				    the same type that setposition sets
	More functions may be defined if they make sense (for instance,
	plc_brake_getpressure() returns the current value of the brake
	pressure sensor scaled onto [0.0, 1.0]).
	
	I've also added code in gcplc.c (functions starting with gcplc_)
	to handle multiple components using the PLC in one program, possibly
	even running simultaneously to each other, and marshal the requests
	into *one* connection (well, two, for thread-related reasons) that
	grabs all new status information at once instead of getting it
	piecemeal when requested - as each request takes a not insignificant
	amount of time.

Release R1-00a (Fri Jul  6 15:22:05 2007):
	Added all the code.

Release R1-00 (Fri Jul  6 11:48:59 2007):
	Created.







