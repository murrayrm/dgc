/**
 * \file igntrans.h
 * \brief interface for low level driver for transmission and ignition
 *
 * \author Noele Norris
 * \date 11 July 2007
 *
 * The functions use the Modbus protocol to access the PLC, which controls
 * Alice's transmission.  The interface follows that of igntrans.hh, old
 * transmission code.
 */

#ifndef __PLC_IGNTRANS_H__
#define __PLC_IGNTRANS_H__

#ifdef __cplusplus
extern "C" {
#endif

/** An error occurred while getting transmission position. -1 is a
    valid transmission position (T_REVERSE), so another value must
    be used. */
#define TRANS_ERROR -2

/** Transmission position PARK (throttle has no effect, wheels are locked). */
#define T_PARK 0

/** Transmission position REVERSE (vehicle moves backwards). */
#define T_REVERSE -1

/** Transmission position NEUTRAL (throttle has no effect, wheels not locked). */
#define T_NEUTRAL 2

/** Transmission position DRIVE (vehicle moves forwards). */
#define T_DRIVE 1

/**
 * \brief Sanity-checks transmission status.
 *
 * This function does a very brief sanity check by reading the current position of the
 * transmission and making sure it's a sensible value (corresponding to
 * one of the defined transmission states).
 *
 * \return 1 for success, 0 for failure
 */
int plc_trans_check();

/**
 * Sets the transmission position to given position (reverse, park, neutral, drive).
 *
 *
 * \note This function returns immediately after commanding the transmission actuator to
 * move; it may be several seconds before the desired gear is actually achieved.
 *
 * \param position One of T_PARK, T_REVERSE, T_NEUTRAL, or T_DRIVE.
 *
 * \return 0 on success, -1 and sets \c errno on failure.
 */ 
int plc_trans_setposition (int position);

/**
 * Gets the transmission position using outputs from PLC.
 *
 * \return TRANS_ERR on failure to receive good position, otherwise returns
 *         T_PARK, T_REVERSE, T_NEUTRAL, or T_DRIVE
 */
int plc_trans_getposition (void);

/** An error occurred while getting ignition position. */
#define IGN_ERR -1

/** Ignition position OFF (vehicle is off, throttle and steering will have no effect). */
#define I_OFF 0

/** Ignition position RUN (vehicle is on, engine is running unless it stalled). */
#define I_RUN 1

/** Ignition position START (vehicle is on, actively starting the engine). Only stay
    in this position for a few seconds. */
#define I_START 2

/**
 * \brief Sanity-checks ignition status.
 *
 * \unimplemented
 *
 * \return 0 (failure) for now.
 */
int plc_ign_check();

/**
 * Sets the current ignition position to \a position. Valid values are 
 * \c I_OFF, \c I_RUN, and \c I_START.
 *
 * \param position The position to set.
 * \return 0 on success, -1 and sets \c errno on failure.
 */
int plc_ign_setposition (int position);

/**
 * Gets the current ignition position, as one of the \c I_* values.
 * \return I_OFF, I_RUN, I_START, or on error, I_ERR and sets \c errno.
 */
int plc_ign_getposition (void);

#ifdef __cplusplus
}
#endif

#endif /* __PLC_IGNTRANS_H__ */
