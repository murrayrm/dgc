              Release Notes for "astate" module

Release R1-01n-field (Mon Nov 12  8:38:19 2007):
	* Added offset option for UTM/site coordinates

Release R1-01n (Thu Oct 18 23:17:42 2007):
	Added program read-imu to version control. This can be used to plot and log IMU data from the Applanix unit.

Release R1-01m (Thu Oct 18 21:47:06 2007):
	Repaired functionality to get Applanix state data from log files. This enables testing of state filters without being connected to the POS-LV 
unit.

Release R1-01l (Tue Oct 16 22:02:56 2007):
	Changed tcp timeout period from 50 ms to 100 ms. If astate has received no state message in this time it reconnects to the POS-LV.

Release R1-01k (Sat Oct 13 22:59:41 2007):
	Added option to make the local frame to be identical to the site frame at startup. This shouldn't be needed, but can be useful while the 
system doesn't fully handle the different frames.

	Moved logging output from /tmp/logs to /logs

Release R1-01j (Sat Oct 13 16:34:15 2007):
	Added site frame pose to sparrow display.

Release R1-01i (Thu Oct 11 11:00:18 2007):
  Added a command line option to read and RNDF file, thus establishing
  the site frame from waypoint 1.1.1.  This has not been tested on
  Alice yet.

Release R1-01h (Thu Oct  4 15:52:20 2007):
	Added binary read-dmi that can be used to get the raw output of 
the DMI. A plot is output to mapviewer on receive subgroup -18 and a 
log-file is written to /tmp/logs/astate_raw/[dateAndTime]DMI.raw

Release R1-01g (Wed Oct  3 10:54:16 2007):
  Added support for site coordinates.

Release R1-01f (Tue Sep 25  0:46:08 2007):
	Added command line option --fix-xy, that keeps x- and 
	y-coordinates fixed wrt the global frame and lets the altitude 
	drift.

	This gives the possibility to have a continuous altitude 
	estimate while accounting for that the system doesn't fully
	handle drifting x- and y-coordinates yet.

Release R1-01e (Tue Aug 21 23:09:47 2007):
	Implemented simple filter that should remove the observed state 
	jumps in the local frame.

	Local and global frame still always have the same orientation, 
	but how they are translated with respect to eachother is 
	continuously updated.

	When AState is started, the local coordinates are initialized to
	localX = 0;
	localY = 0;
	localZ = utmAltitude;
	
	To run AState the old way with local frame fixed to the global 
	frame, use --fix-local-frame

	IMPORTANT NOTE:
	Since lots of code has been developed with the local frame fixed 
	to the global frame things may start to break with this update 
	of AState. If things start behaving strange, you might want to try 
	running AState with --fix-local-frame. If this helps, look through
	your code and fix it so it works with the drifting local frame.

Release R1-01d (Tue Aug 14 22:05:40 2007):
	adding command-line option to control how fast 
	AState actually broadcasts state data. The
	Applanix is sending at 200Hz - new default for
	AState is to send at this full rate (previously
	sent at 25Hz). Since the sensors run at 75Hz,
	we really don't want to run state slower than that.

	The AState struct is pretty small, but keep this
	change in mind if we start having network 
	problems tomorrow.

Release R1-01c (Fri Apr 27 22:56:38 2007):
  Added vehicle speed field.

Release R1-01b (Fri Apr 27 13:27:22 2007):
        Tuning on the time parameters of the health monitor. Angular rates are correctly set in the vehicle frame now. 

Release R1-01a (Tue Apr 24 10:00:14 2007):
        Astate status information moved to interfaces/StatePrecision.hh . Switching release number becuse of the (large) set of modifications done in the previous two releases (I forgot to do that before).
		
Release R1-00i (Fri Apr 20 14:05:17 2007):
	    Health monitor and connection failure recovery is now implemented. In case the applanix does not send message in 0.5 sec we assume it is dead, we disconnect, and we try to estabilish a new connection. The information about astate health is sent on the skynet group SNastateHealth. The message is an integer in the range [0,10] consistent with enum{FULL_NAV,...} which is defined in AState.hh.
	    Preliminary tests have been done with Alice stationary, by phisically unplagging the ethernet cable from Applanix. The tests were successful.

Release R1-00h (Sun Apr 15 13:03:46 2007):
        Astate has been extended to receive estimate precision information. A new message is avilable containing state precision information.

Release R1-00g (Wed Mar 28 18:01:23 2007):
        The Heading angle is now in [-\Pi,\Pi] with 0 at North, increasing toward East and discontinuity at South.

Release R1-00f (Tue Feb 20 15:51:26 2007):
	Fixed include statements so this revision of astate compiles against revision R2-00a of the interfaces and the skynet modules

Release R1-00e (Fri Feb  2 16:02:20 2007):
        Astate and data replay tested on Alice. Modifications made to sparrow display. 
(Files modified: AState_Apx.cc Astate.cc Astate.hh Astate_Sparrow.cc asdisp.hh)

Release R1-00c (Sat Jan 27 18:27:05 2007):
	This is a minor release to get rid of an installation problem on
	YaM (bad symbolic link).  No change to the interface.  Also testing
	(again) the ability to post bugzilla messages.

Release R1-00a (Sun Jan 14 20:21:22 2007):
	This is the initial release of astate under YaM.  It has been
	converted to use the YaM makefile structure as well as the
	YaM versions of various modules and interfaces.  This version should
	behave identically to the dgc/trunk version.  Stefano is going to
	test it in the next few days (it runs, but we don't know for sure
	if it talks to the Applanix correctly).

Release R1-00 (Sun Jan 14 19:15:35 2007):
	Created.


























