/* \file AState.hh 
 * \brief Astate class declaration
 * \author Stefano Di Cairano,
 * \date dec-06
 */

#ifndef ASTATE_HH
#define ASTATE_HH

#include <fstream>

#include "applanix.hh" //Applanix header file. Define here msgs structure and applanix constants

#include "skynet/sn_msg.hh"
#include "interfaces/sn_types.h"
#include "skynet/SkynetContainer.hh"
#include "interfaces/VehicleState.h"
#include "interfaces/StatePrecision.hh"
#include "dgcutils/DGCutils.hh"
#include "skynettalker/SkynetTalker.hh"

#define APX_NAV_BUFFER_SIZE 1500
#define APX_PREC_BUFFER_SIZE 500
#define DEBUG_LEVEL 5 



/*! Struct containing the Applanix Navigation data
 * /param time: long long containing the data timestamp
 * /param data: apxNavData struct containing navigation data  
 */
 
struct rawApx {	
	unsigned long long time;
	apxNavData data;
};


struct rawPrec {	
	unsigned long long time;
	apxPrecData data;
};

/*! Struct containing the user defined options
 *  /param useSparrow: integer, set to 0 to disable sparrow display
 *  /param logRaw: integer, set to 1 to do logging
 *  /param stateReplay: integer, set to 1 to do replay from state data
 */

struct astateOpts {
	int useSparrow;
	int logRaw;
	int stateReplay;
        int dataReplay;
        int frequencyDivisor;
        int fixLocalFrame;
        int fixXY;
};




/*! Astate class declaration
 */

class AState : public CSkynetContainer
{

    SkynetTalker<int> m_healthTalker;
	int m_astateStatus;
    pthread_mutex_t m_healthMutex;
    pthread_mutex_t m_socketMutex;
    DGCcondition m_apxReconnected;
	pthread_mutex_t m_VehicleStateMutex;
    // Lock these for the m_VehicleStateMutex

    /*! VehicleState struct containing Alice current state estimate */ 
    VehicleState m_vehicleState;
    VehiclePrecisionPlus m_vehiclePrecision;
  
    SkynetTalker <VehiclePrecisionPlus> m_precisionTalker; //talker for the precision data

  //what to subsample applanix data rate by
  int frequency_divisor;

    /*! Socket to read from POS-LV, */
    int m_apxDataSocket;      // set in the constructor and never changed
  
   /*! Socket to broadcast state estimate*/
    int m_broadcastStateSock;  // Should be set in constructor and never changed.
  
  
    pthread_mutex_t m_ApxNavDataMutex;
    //lock list of m_ApxNavDataMutex
    DGCcondition m_apxNavBufferFree;
    /*! \param apxNavBuffer: rawApx array containing navigation data received from POS-LV*/
    rawApx m_apxNavBuffer[APX_NAV_BUFFER_SIZE]; 
    int m_apxNavBufferReadInd;
    int m_apxNavBufferLastInd;
    // end lock list 


    pthread_mutex_t m_ApxPrecDataMutex;
    // lock list of  m_ApxPrecDataMutex
    DGCcondition m_apxPrecBufferFree;
   /*! \param apxNavBuffer: rawApx array containing navigation data received from POS-LV  */
    rawPrec m_apxPrecBuffer[APX_PREC_BUFFER_SIZE]; 
    int m_apxPrecBufferReadInd;
    int m_apxPrecBufferLastInd;
    // end lock list 



/*\param stateMode: state estimate status
 */
  //int m_stateMode;
 
  //These variables are ONLY here to give sparrow access to them...

  /*! Status variables :
   * \param apxCount: integer, number of navigation data packets received from POS-LV
   * \param apxDead: integer, 1 if POS_LV is not sending messages 
   * \param stateStatus: integer, navigation solution status
   * \param apxConnected: integer, 1 if POS-LV is connected
   */ 
    int m_apxCount;
    int m_apxPrecisionCount;
    int m_stateStatus;
    int m_apxConnected;
    int m_apxDead;
    double m_spTime;
  
    DGCcondition m_newNavData;

    // Variables to remain unlocked
    int m_quitPressed;

/*! \param snKey: integer, the skynet key
 */
    int m_snKey;  // Should be set in AState_Main and never changed.

/*! _options: astateOpts, internal copy of user defined options
 */  
    astateOpts m_options; 

/*! startTime: long long, initial time
 */  
    unsigned long long m_startTime; // Should be set in constructor and never changed.

  // Initial offset for bogus local pose
    bool m_haveInitPose;
    double m_initN, m_initE;

  // Variables used to show drift in sparrow
    double m_driftN, m_driftE, m_driftD;

    fstream m_apxLogStream; // These four are opened & closed in constructor and destructor
    fstream m_stateLogStream;   
    char m_apxLogFile[100];
    char m_stateLogFile[100];
    fstream m_replayStateStream;  
    char m_replayStateFile[100];
    fstream m_replayDataStream;  
    char m_replayDataFile[100];
  /*****************************************************************
   *  The following are the private methods of the AState class.
   *****************************************************************/

    
  /*! Function that updates the vehicle_state struct */
    void updateStateThread();

  /*! Function to broadcast state data */
    void broadcast();

  /*! Thread to read from the POS-LV. */
    void apxReadThread(); 
  
  /*! Thread to control POS-LV. */
//  void apxControlThread();

  /*! Thread to update sparrow variables*/
    void sparrowThread();

//  void playback();
    void supervisoryThread();


public:
  /*! AState Constructor */
    AState(int skynetKey, astateOpts options);

 /*! Astate main loop  */
    void active();
  
/*! Astate desctrutor */
    ~AState();
};

#endif
