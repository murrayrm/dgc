/* \file Applanix.hh
 * \brief Applanix POS-LV drivers declaration.
 * \author Stefano Di Cairano,
 * \date dec-06
 */

#ifndef APPLANIX_HH
#define APPLANIX_HH

#include <string.h>
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <time.h>

#define APX_CONTROL_PORT 5601
#define APX_DATA_PORT 5602
#define APX_IP_ADDR "192.168.0.100" 
#define LENGTH_BYTE 6
#define LENGTH_OFFSET 8
#define SEL_MSG_LENGTH 20
#define NAVMOD_MSG_LENGTH 16
#define CONTROL_MSG_LENGTH 16
#define NAV_GRP_LENGTH 140
#define ACK_MSG_LENGTH 52
#define APX_BUFFER_SIZE 4096 //for now we can also set it to 140, this is for test

/*!This file contains the definition of the interfaces with the Applanix POS-LV system.
 * Data-structures and functions fto send/receive data are defined here
 */

/* ************************************
 * ********** APX_DATA STRUCT *********
 * ************************************/
 
/*! Structure contains navigation solution data. POS-LV datagroup 1
 */
 
typedef struct apxNavData
{
  char groupStart[5];      //increased by 1 because of null termination
  unsigned short ID;
  unsigned short byteCount;
	
	// time and distance -> maybe another struct
  double time1;
  double time2;
  double distanceTag;
  char timeTypes;
  char distanceType;
	
  double latitude;
  double longitude;
  double altitude;	
	
  float northVelocity;
  float eastVelocity;
  float downVelocity;
	
  double roll;
  double pitch;
  double heading;
  double wander;
	
  float trackAngle;
  float speed;
	
	
  float rollRate;
  float pitchRate;
  float yawRate;
		
  float longitudinalAcc;
  float transverseAcc;
  float downAcc;
	
  char alignStatus;
  char pad;
  unsigned short checksum;
  char groupEnd[3]; //increased by 1 because of null termination		
};



/*! Function to parse navigation data.
 * \param inBuffer: the pointer to the buffer to be parsed
 * \param size: length of such a buffer
 * \param msg: pointer to the the apxNavData struct in which the parsed data are stored
 */
 void parseNavData(char* inBuffer, int size, apxNavData* msg);




/* *****************************************
 * ************* Navigation Mode ***********
 * *****************************************/


/*! Structure defining the message to switch to Navigation mode. POS-LV message 50 */
typedef struct apxNavModeMsg
{ 
  apxNavModeMsg()    //this is the constructor, it fills the standard fields
  {  
    char init[5]="$MSG";
	char end[3]="$#";
	strcpy(msgStart,init);
	strcpy(msgEnd,end);
	ID=50;
	byteCount=8;
	pad = '\0';
	checksum=0;
  }
  char msgStart[5]; //increased by 1 because of null termination-> don' t send the add byte!!!
  unsigned short ID;
  unsigned short byteCount;
  unsigned short transNumber;
  char navMode;
  char pad;
  unsigned short checksum;
  char msgEnd[3]; //increased by 1 because of null termination-> don' t send the add byte!!!
};


/*! Function to build the Navigation Mode message
 * \param msg: pointer to apxNavModeMsg struct, where the data will be stored in 
 * \param transaction: short int, the transaction number
 */
 void buildNavModeMsg(apxNavModeMsg* msg, unsigned short transaction);



/* ! Function to serialize Navigation Mode message.
 * \param msg: pointer to apxNavModeMsg struct, the data to be serialized 
 * \param inBuffer: pointer to the char buffer the data is going to be stored in
 */
 void serialNavModeMsg(apxNavModeMsg* msg, char* inBuffer);







/* *****************************************
 * ************** SELECT DATA GROUPS *******
 * *****************************************/

/*! Structure defining the message to select the data to be received from POS-LV. POS-LV message 52 */
typedef struct apxSelGroupsMsg
{
  apxSelGroupsMsg()    //this is the constructor, it fills the standard fields
  {  
    char init[5]="$MSG";
	char end[3]="$#";
	strcpy(msgStart,init);
	strcpy(msgEnd,end);
	ID=52;
	checksum=0;
  } 	
	
  char msgStart[5]; //increased by 1 because of null termination-> don' t send the add byte!!!
  unsigned short ID;
  unsigned short byteCount;
  unsigned short transNumber;
  unsigned short numberOfGroups;
  unsigned short groupID;  //I set to 10 the max number of groups we can get. This can be changed in the future
  unsigned short dataRate;
  unsigned short pad;
  unsigned short checksum;
  char msgEnd[3]; //increased by 1 because of null termination-> don' t send the add byte!!!	
};


/*! Function to build the Navigation Mode message
 * \param msg: pointer to apxSelGroupsMsg struc, where the data will be stored 
 * \param transaction: short int,  the transaction number
 */
void buildSelGroupsMsg(apxSelGroupsMsg* msg, unsigned short transaction);


/*! Function to serialize Group Selection Control message.
 * \param msg: pointer to apxSelGroupsMsg struct, the data to be serialized 
 * \param inBuffer: pointer to the char buffer the data is going to be stored in
 * \param size: integer, size of the data to be searilized (this message has variable length) 
 */ 
void serialSelGroupsMsg(apxSelGroupsMsg* msg, char* inBuffer, int size);


/* *****************************************
 * ***** ACKNOWLEDGE MESSAGE ****************
 * *****************************************/
 
/*! Structure defining the Acknowledge message to be received from POS-LV as replay to a message. POS-LV message 0 */ 
 typedef struct apxAckMsg
{ 
  char msgStart[5]; //increased by 1 because of null termination
  unsigned short ID;
  unsigned short byteCount;
  unsigned short transNumber;
  unsigned short recMsgID;
  unsigned short responseCode;
  char newParamStatus;
  char rejectedParamName[32];
  unsigned short pad;
  unsigned short checksum;
  char msgEnd[3]; //increased by 1 because of null termination			
};



/*! Function to parse Ack message data.
 * \param inBuffer: pointer to the buffer to be parsed
 * \param size: length of such a buffer
 * \param msg: pointer to the the apxAckMsg struct in which the parsed data are stored
 */
void parseAckMsg(char* inBuffer, int size, apxAckMsg* msg);



/* *****************************************
 * ******** CONTROL MESSAGE ****************
 * *****************************************/

/*! Structure defining the General Control Message to keep control on POS-LV. Should every at most 30 secs. POS-LV message 90 */
 typedef struct apxControlMsg
{
  apxControlMsg()    //this is the constructor, it fills the standard fields
  {  
    char init[5]="$MSG";
	char end[3]="$#";
	strcpy(msgStart,init);
	strcpy(msgEnd,end);
	byteCount = 8;
	ID = 90;
	checksum=0;
  } 	
	
  char msgStart[5]; //increased by 1 because of null termination-> don' t send the add byte!!!
  unsigned short ID;
  unsigned short byteCount;
  unsigned short transNumber;
  unsigned short control;
  unsigned short checksum;
  char msgEnd[3]; //increased by 1 because of null termination-> don' t send the add byte!!!	
};



/*! Function to build the General Control Message
 * \param msg: pointer to apxNavModeMsg struct, where the data will be stored in 
 * \param transaction: short int, the transaction number
 * \param control: short int, the action to be taken (see Applanix Messaging Manual)
 */
 void buildControlMsg(apxControlMsg* , unsigned short transaction, unsigned short control);

/* ! Function to serialize the General Control Message
 * \param msg: pointer to apxControlMsg struct, the data to be serialized 
 * \param inBuffer: pointer to the char buffer the data is going to be stored in
 */
void serialControlMsg(apxControlMsg* msg, char* inBuffer);



/* ****************************************************
 * ****************** CHECKSUM ************************
 * ****************************************************/

/*! Function to compute the checksum to be added at every message
 * \param inBuffer: pointer to the char buffer containing the message
 * \param size: integer, the length of the message
 */
unsigned short apxCompChecksum(char* inBuffer, int size);



/*! Function to verify the checksum received with every message/datagroup
 * \param inBuffer: pointer to the char buffer containing the message
 * \param size: integer, the length of the message
 */
unsigned short apxVerChecksum(char* inBuffer, int size);





/* ****************************************************
*  ************* TCP-IP functions *********************
*  ****************************************************/

/*! Function to send data through TCP-IP connection.
 * \param socket: pointer to the socket identifier (an int)
 * \param message: pointer to the char buffer containing the message to be sent
 * \param msgLength: short int, the size of the data to be sent
 */

short apxTcpSend(int* socket, char* message, short msgLength);
// **************** RECEIVE ******************


/*! Function to receive data through TCP-IP connection.
 * \param socket: pointer to the socket identifier (an int)
 * \param message: pointer to the char buffer where the message will be stored
 * \param sizeMsg: short int, the size of the data to be received
 */
short apxTcpReceive(int* socket, char* message, short sizeMsg);

#endif //APPLANIX_HH
