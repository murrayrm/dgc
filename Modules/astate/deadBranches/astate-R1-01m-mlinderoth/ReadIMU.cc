/*! \file ReadIMU.cc
 * \brief Reads IMU data from POS-LV
 * \author Magnus Linderoth,
 * \date 2007-10-02
 */


#include <stdlib.h>
#include "skynet/skynet.hh"
//#include "applanix.cc"
#include "ReadIMU.hh"

#include <sys/types.h>   //these are the libraries needed for sockets
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <errno.h>



ReadIMU::ReadIMU(int sn_key, bool log, bool plot)
{
  m_sn_key = sn_key;
  m_log = log;
  m_plot = plot;

  m_initOffset = 2;

  DGCcreateMutex(&m_bufferMutex);
  DGCcreateCondition(&m_dataAvailable);
  DGCSetConditionFalse(m_dataAvailable);
  m_bufferReadIndex = m_bufferWriteIndex = 0;
  memset(&m_dataBuffer, 0, sizeof(m_dataBuffer));

  if (m_log) {
    mkdir("/tmp/logs/astate_raw/",
          S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH
          | S_IXOTH);

    char fileName[256];
    time_t t = time(NULL);
    tm *local;
    local = localtime(&t);
    sprintf(fileName,
            "/tmp/logs/astate_raw/%04d%02d%02d_%02d%02d%02d_IMU.raw",
            local->tm_year + 1900, local->tm_mon + 1, local->tm_mday,
            local->tm_hour, local->tm_min, local->tm_sec);

    m_logStream.open(fileName, fstream::out);
    cerr << "Logging to " << fileName << endl;
  }

  if (m_plot) {
    m_meTalker.initSendMapElement(m_sn_key);
    cerr << "Plotting on sendSubGroup " << SKYNET_SEND_SUBGROUP << endl;
  }
}

ReadIMU::~ReadIMU()
{

}

void ReadIMU::run()
{
  char buffer[2 * IMU_GRP_LENGTH]; // Double length just to be safe

  DGCstartMemberFunctionThread(this, &ReadIMU::plotLogThread); 

  int msgCount = 0;

  while (true) {

    while (!m_apxConnected) {
      inetConnect();
    }

    
    int comStatus = 0;
    memset(buffer,'\0',sizeof(buffer)); //reset the buffer
    int msgLength;
           
    while (comStatus < 1 )  {   
      comStatus = apxTcpReceive(&m_apxDataSocket, buffer, &msgLength);
    }

    // we have a new message hence the Applanix is working
    if (comStatus==2)  {
      //failure
      m_apxConnected = 0;
    } else {
      if (msgLength == IMU_GRP_LENGTH) {
	//we have received a IMU data message
	apxIMUData IMUData;
	parseIMUData(buffer, IMU_GRP_LENGTH, &IMUData);

	DGClockMutex(&m_bufferMutex);
	m_dataBuffer[m_bufferWriteIndex++] = IMUData;
	DGCunlockMutex(&m_bufferMutex);
	
	m_bufferWriteIndex %= BUF_SIZE;

	if (m_bufferWriteIndex % SAMPS_PER_LOG_CYCLE == 0) {
	  DGCSetConditionTrue(m_dataAvailable);
	}
      }
      if (++msgCount % 200 == 0) {
        cout << "Received " << msgCount << " messages" << endl;
      }
    }
  }
}

void ReadIMU::plotLogThread()
{
  while (true) {
    DGCWaitForConditionTrue(m_dataAvailable);

    /*if (m_initOffset) {
      m_initOffset--;
      if (m_initOffset == 0) {
	m_offset = m_dataBuffer[0].upDownPulseCount;
      }
      }*/

    DGCSetConditionFalse(m_dataAvailable);

    if (m_log) {
      for (int i = m_bufferReadIndex; i < m_bufferReadIndex + SAMPS_PER_LOG_CYCLE; i++) {
        char str[256];
        sprintf(str, "%f\t%li\t%li\t%li\t%li\t%li\t%li\n",
                m_dataBuffer[i % BUF_SIZE].time2,
                m_dataBuffer[i % BUF_SIZE].xIncVel,
                m_dataBuffer[i % BUF_SIZE].yIncVel,
                m_dataBuffer[i % BUF_SIZE].zIncVel,
                m_dataBuffer[i % BUF_SIZE].xIncAng,
                m_dataBuffer[i % BUF_SIZE].yIncAng,
                m_dataBuffer[i % BUF_SIZE].zIncAng);
        m_logStream.write(str, strlen(str));
      }
      m_logStream.flush();
    }

    if (m_plot) {
      MapElement me;
      point2 point;
      vector<point2> points;

      double startTime = m_dataBuffer[(m_bufferReadIndex + SAMPS_PER_LOG_CYCLE - 1) % BUF_SIZE].time2;
      
      // xIncVel
      for (int i = m_bufferReadIndex + SAMPS_PER_LOG_CYCLE - 1;
           i >= m_bufferReadIndex - BUF_SIZE + 2*SAMPS_PER_LOG_CYCLE;
           i--) {
        point.set(m_dataBuffer[i % BUF_SIZE].time2 - startTime,
                  (m_dataBuffer[i % BUF_SIZE].xIncVel /*- m_offset*/) * VERT_SCALE);
        points.push_back(point);
      }
      me.setColor(MAP_COLOR_RED);
      me.setGeometry(points);
      me.setId(1);
      me.setTypeLine();
      m_meTalker.sendMapElement(&me, SKYNET_SEND_SUBGROUP);
      me.setId(2);
      me.setTypePoints();
      m_meTalker.sendMapElement(&me, SKYNET_SEND_SUBGROUP);
      
      // yIncVel
      points.clear();
      for (int i = m_bufferReadIndex + SAMPS_PER_LOG_CYCLE - 1;
           i >= m_bufferReadIndex - BUF_SIZE + 2*SAMPS_PER_LOG_CYCLE;
           i--) {
        point.set(m_dataBuffer[i % BUF_SIZE].time2 - startTime,
                  m_dataBuffer[i % BUF_SIZE].yIncVel * VERT_SCALE);
        points.push_back(point);
      }
      me.setColor(MAP_COLOR_GREEN);
      me.setGeometry(points);
      me.setId(3);
      me.setTypeLine();
      m_meTalker.sendMapElement(&me, SKYNET_SEND_SUBGROUP);
      me.setId(4);
      me.setTypePoints();
      m_meTalker.sendMapElement(&me, SKYNET_SEND_SUBGROUP);
      
      // zIncVel
      points.clear();
      for (int i = m_bufferReadIndex + SAMPS_PER_LOG_CYCLE - 1;
           i >= m_bufferReadIndex - BUF_SIZE + 2*SAMPS_PER_LOG_CYCLE;
           i--) {
        point.set(m_dataBuffer[i % BUF_SIZE].time2 - startTime,
                  m_dataBuffer[i % BUF_SIZE].zIncVel * VERT_SCALE);
        points.push_back(point);
      }
      me.setColor(MAP_COLOR_BLUE);
      me.setGeometry(points);
      me.setId(5);
      me.setTypeLine();
      m_meTalker.sendMapElement(&me, SKYNET_SEND_SUBGROUP);
      me.setId(6);
      me.setTypePoints();
      m_meTalker.sendMapElement(&me, SKYNET_SEND_SUBGROUP);
      
      // xIncAng
      points.clear();
      for (int i = m_bufferReadIndex + SAMPS_PER_LOG_CYCLE - 1;
           i >= m_bufferReadIndex - BUF_SIZE + 2*SAMPS_PER_LOG_CYCLE;
           i--) {
        point.set(m_dataBuffer[i % BUF_SIZE].time2 - startTime,
                  m_dataBuffer[i % BUF_SIZE].xIncAng * VERT_SCALE);
        points.push_back(point);
      }
      me.setColor(MAP_COLOR_YELLOW);
      me.setGeometry(points);
      me.setId(7);
      me.setTypeLine();
      m_meTalker.sendMapElement(&me, SKYNET_SEND_SUBGROUP);
      me.setId(8);
      me.setTypePoints();
      m_meTalker.sendMapElement(&me, SKYNET_SEND_SUBGROUP);
      
      // yIncAng
      points.clear();
      for (int i = m_bufferReadIndex + SAMPS_PER_LOG_CYCLE - 1;
           i >= m_bufferReadIndex - BUF_SIZE + 2*SAMPS_PER_LOG_CYCLE;
           i--) {
        point.set(m_dataBuffer[i % BUF_SIZE].time2 - startTime,
                  m_dataBuffer[i % BUF_SIZE].yIncAng * VERT_SCALE);
        points.push_back(point);
      }
      me.setColor(MAP_COLOR_MAGENTA);
      me.setGeometry(points);
      me.setId(9);
      me.setTypeLine();
      m_meTalker.sendMapElement(&me, SKYNET_SEND_SUBGROUP);
      me.setId(10);
      me.setTypePoints();
      m_meTalker.sendMapElement(&me, SKYNET_SEND_SUBGROUP);
      
      // zIncAng
      points.clear();
      for (int i = m_bufferReadIndex + SAMPS_PER_LOG_CYCLE - 1;
           i >= m_bufferReadIndex - BUF_SIZE + 2*SAMPS_PER_LOG_CYCLE;
           i--) {
        point.set(m_dataBuffer[i % BUF_SIZE].time2 - startTime,
                  m_dataBuffer[i % BUF_SIZE].zIncAng * VERT_SCALE);
        points.push_back(point);
      }
      me.setColor(MAP_COLOR_CYAN);
      me.setGeometry(points);
      me.setId(11);
      me.setTypeLine();
      m_meTalker.sendMapElement(&me, SKYNET_SEND_SUBGROUP);
      me.setId(12);
      me.setTypePoints();
      m_meTalker.sendMapElement(&me, SKYNET_SEND_SUBGROUP);
      
    }
    
    m_bufferReadIndex += SAMPS_PER_LOG_CYCLE;
  }
}


void ReadIMU::inetConnect()
{
  close(m_apxDataSocket);                             // closing the old socket as it may be pointing nowhere
  usleep(RECONNECTION_PERIOD);                                     // it seems that after closing the socket the POS-LV needs some time before open 
  m_apxDataSocket=socket(PF_INET, SOCK_STREAM, 0); 	// create the data socket
  if(m_apxDataSocket < 0) {
    cout << "ERROR while creating data socket" << endl;
    exit(1);
  }
  struct sockaddr_in apxData_addr;					// these structs contains the addresses 
  apxData_addr.sin_addr.s_addr = inet_addr(APX_IP_ADDR);  
  apxData_addr.sin_family = AF_INET;
  apxData_addr.sin_port = htons(APX_DATA_PORT);  					
  
  cout<<" Attempting a connection" << endl; 
  if (connect(m_apxDataSocket,(struct sockaddr *)&apxData_addr,sizeof(apxData_addr)) < 0) {
    cerr << ("ERROR connecting to Applanix Data Port") << endl;
    cerr << "Error: " << strerror(errno) << endl;
    m_apxConnected = 0;
  } else { 
    m_apxConnected = 1;
    cerr << "Connected to Data Port" << endl; 
  }
}


// **************** RECEIVE ******************

//I am assuming to receive the IMU data group in a message.
//We throw away everything else

short ReadIMU::apxTcpReceive(int* socket, char* message, int* msgLength)
{       
  short comStatus = 1;                            
  short recBytes = 0;
  fd_set readfds;
  struct timeval tv;

  char buffer[APX_BUFFER_SIZE];
  FD_ZERO(&readfds);
  FD_SET(*socket,&readfds);
  tv.tv_sec=0;
  tv.tv_usec=TIMEOUT_PERIOD;
  int isReady=select(*socket+1,&readfds,NULL,NULL,&tv);
  int test = 0;
  if( FD_ISSET(*socket,&readfds)){
    test = 1;
  }
  if(isReady>0) {
    recBytes=recv(*socket, buffer, APX_BUFFER_SIZE, 0);
    if(recBytes < 1) {
      comStatus = 2;
    } else {
      if(recBytes != IMU_GRP_LENGTH) {
        comStatus = 0;     
        //   cout << "Received bytes: " << recBytes << endl; // use this to get infor about misformed packets
      } else {
        char isGroup[5]="$GRP";
        char isMine[5]="nnnn";       //initialization to have a null-terminated string...
        memcpy(&isMine[0],&buffer[0],4);
        
        if( strcmp(isMine,isGroup)!=0 ) {  //is not a group message
          comStatus = 0;           //this packet is out of synchrony
        } else {
          *msgLength = recBytes;
        }
      }	                  
      
      if(comStatus > 0) {
        memcpy(message, buffer, recBytes);                //copy the temp buffer into message buffer
      }
    }
  } else {
    comStatus = 2;
  }
  return(comStatus);
}


void ReadIMU::parseIMUData(char* inBuffer, int size, apxIMUData* msg)
{
  char buffer[size];
  memcpy(buffer, inBuffer, size);
  memset(&(msg->groupStart[0]),'\0',5);
  memcpy(&(msg->groupStart[0]),&buffer[0],4);  
  
  
  memcpy(&(msg->ID),&buffer[4],2);  
  memcpy(&(msg->byteCount),&buffer[6],2);
  memcpy(&(msg->time1),&buffer[8],8);
  memcpy(&(msg->time2),&buffer[16],8);
  memcpy(&(msg->distanceTag),&buffer[24],8);
  memcpy(&(msg->timeTypes),&buffer[32],1);
  memcpy(&(msg->distanceType),&buffer[33],1);

  memcpy(&(msg->xIncVel),&buffer[34],4);
  memcpy(&(msg->yIncVel),&buffer[38],4);
  memcpy(&(msg->zIncVel),&buffer[42],4);
  memcpy(&(msg->xIncAng),&buffer[46],4);
  memcpy(&(msg->yIncAng),&buffer[50],4);
  memcpy(&(msg->zIncAng),&buffer[54],4);
  memcpy(&(msg->dataStatus),&buffer[58],1);
  memcpy(&(msg->IMUType),&buffer[59],1);
  memcpy(&(msg->dataRate),&buffer[60],1);
  memcpy(&(msg->dataRate),&buffer[61],2);

  memcpy(&(msg->pad),&buffer[63],1);
  memcpy(&(msg->checksum),&buffer[64],2);
  memset(&(msg->groupEnd[0]),'\0',3);
  memcpy(&(msg->groupEnd[0]),&buffer[66],2);
 
}



int main(int argc, char **argv)
{
  int sn_key  = skynet_findkey(argc, argv); //getting skynet key
  
  bool log = true;
  bool plot = true;

  for (int i = 0; i < argc; i ++) {
    if (strcmp(argv[i], "--no-log") == 0) {
      log = false;
    }
    if (strcmp(argv[i], "--no-plot") == 0) {
      plot = false;
    }
  }

  ReadIMU readIMU(sn_key, log, plot);

  readIMU.run();
}





