#ifndef READ_IMU_H_LJFIYTKDCFUYKGLYJUFCK
#define READ_IMU_H_LJFIYTKDCFUYKGLYJUFCK


#include "dgcutils/DGCutils.hh"
#include <fstream>
//#include <sys/stat.h>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>


#define IMU_GRP_LENGTH 68

#define SAMPS_PER_LOG_CYCLE 50
#define BUF_SIZE 2000  // Must be a multiple of SAMPS_PER_LOG_CYCLE and at least 2*SAMPS_PER_LOG_CYCLE

#define SKYNET_SEND_SUBGROUP -18
#define VERT_SCALE 0.1

#define TIMEOUT_PERIOD 50000
#define RECONNECTION_PERIOD 500000
#define APX_DATA_PORT 5602
#define APX_IP_ADDR "192.168.0.100" 
#define APX_BUFFER_SIZE 4096 //for now we can also set it to 140, this is for test


typedef struct apxIMUData
{
  char groupStart[5];      //increased by 1 because of null termination
  unsigned short ID;
  unsigned short byteCount;
	
	// time and distance -> maybe another struct
  double time1;
  double time2;
  double distanceTag;
  char timeTypes;
  char distanceType;
  
  long xIncVel;
  long yIncVel;
  long zIncVel;
  long xIncAng;
  long yIncAng;
  long zIncAng;
  char dataStatus;
  char IMUType;
  char dataRate;
  unsigned short IMUStatus;
  
  char pad;
  unsigned short checksum;
  char groupEnd[3]; //increased by 1 because of null termination		
};


class ReadIMU {
public:
  ReadIMU(int sn_key, bool log, bool plot);
  ~ReadIMU();

  void run();

private:
  void plotLogThread();

  void inetConnect();

  short apxTcpReceive(int* socket, char* message, int* msgLength);

  void parseIMUData(char* inBuffer, int size, apxIMUData* msg);

  //! Socket to read from POS-LV
  int m_apxDataSocket;      // set in the constructor and never changed

  // apxConnected: integer, 1 if POS-LV is connected
  int m_apxConnected;

  // Skynet key
  int m_sn_key;
  
  // Do logging
  bool m_log;

  // Send plot
  bool m_plot;

  // Data is available for logging or plotting
  DGCcondition m_dataAvailable;
  pthread_mutex_t m_bufferMutex;
  int m_bufferReadIndex;
  int m_bufferWriteIndex;
  apxIMUData m_dataBuffer[BUF_SIZE];

  fstream  m_logStream;

  CMapElementTalker m_meTalker;

  int m_initOffset;
  long m_offset;
};


#endif  //READ_IMU_H_LJFIYTKDCFUYKGLYJUFCK
