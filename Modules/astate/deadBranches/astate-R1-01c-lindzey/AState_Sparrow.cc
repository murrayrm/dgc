/* \file Astate_Sparrow.cc
 * \brief Astate class functions to generate/update Astate Sparrow display
 * \author Stefano Di Cairano,
 * \date dec-06
 */

#include "AState.hh"

//Struct containing all possible pieces of data being outputted

#include "sparrowhawk/SparrowHawk.hh"
#include "asdisp.h"		// display table

//Populate displayVars struct

void AState::sparrowThread() 
{
  CSparrowHawk &sh = SparrowHawk();

  sh.add_page(asdisp, "Main");

  sh.rebind("snkey", &m_snKey);
  
  sh.rebind("stateStatus", &m_stateStatus);
  sh.rebind("apxCount", &m_apxCount);
  sh.rebind("apxConnected", &m_apxConnected);
  sh.rebind("logEn", &m_options.logRaw);
  sh.rebind("repEn", &m_options.stateReplay);
  sh.rebind("drepEn", &m_options.dataReplay);
  sh.rebind("mytime", &m_spTime);
  sh.rebind("status", &m_astateStatus);
  sh.rebind("apxDead", &m_apxDead);
  
  sh.rebind("vsNorthing", &m_vehicleState.utmNorthing);
  sh.rebind("vsEasting", &m_vehicleState.utmEasting);
  sh.rebind("vsAltitude", &m_vehicleState.utmAltitude);
  sh.rebind("vsVel_N",  &m_vehicleState.utmNorthVel);
  sh.rebind("vsVel_E",  &m_vehicleState.utmEastVel);
  sh.rebind("vsVel_D",  &m_vehicleState.utmAltitudeVel);
  sh.rebind("vsRoll", &m_vehicleState.utmRoll);
  sh.rebind("vsPitch", &m_vehicleState.utmPitch);
  sh.rebind("vsYaw", &m_vehicleState.utmYaw);
  sh.rebind("vsRollRate", &m_vehicleState.utmRollRate);
  sh.rebind("vsPitchRate", &m_vehicleState.utmPitchRate);
  sh.rebind("vsYawRate", &m_vehicleState.utmYawRate);
  sh.rebind("vsNorthConf", &m_vehiclePrecision.utmNorthConfidence);
  sh.rebind("vsEastConf", &m_vehiclePrecision.utmEastConfidence);
  sh.rebind("vsHeightConf", &m_vehiclePrecision.utmAltitudeConfidence);
  sh.rebind("vsRollConf", &m_vehiclePrecision.rollConfidence);
  sh.rebind("vsPitchConf", &m_vehiclePrecision.pitchConfidence);
  sh.rebind("vsYawConf", &m_vehiclePrecision.yawConfidence);

  #warning "Fix this random collection of display values"
  sh.rebind("lpNorthing", &m_vehicleState.localX);
  sh.rebind("lpEasting", &m_vehicleState.localY);
  sh.rebind("lpAltitude", &m_vehicleState.localZ);
  sh.rebind("lpVel_N",  &m_vehicleState.localXVel);
  sh.rebind("lpVel_E",  &m_vehicleState.localYVel);
  sh.rebind("lpVel_D",  &m_vehicleState.localZVel);
  sh.rebind("lpRoll", &m_vehicleState.localRoll);
  sh.rebind("lpPitch", &m_vehicleState.localPitch);
  sh.rebind("lpYaw", &m_vehicleState.localYaw);
  sh.rebind("lpRollRate", &m_vehicleState.vehRollRate);
  sh.rebind("lpPitchRate", &m_vehicleState.vehPitchRate);

  sh.rebind("velNorthConf", &m_vehiclePrecision.velNorthRMS);
  sh.rebind("velEastConf", &m_vehiclePrecision.velEastRMS);
  sh.rebind("velHeightConf", &m_vehiclePrecision.velDownRMS);
  sh.rebind("errEllMaj", &m_vehiclePrecision.errEllipMajor);
  sh.rebind("errEllMin", &m_vehiclePrecision.errEllipMinor);
  sh.rebind("errEllAngle", &m_vehiclePrecision.errEllipAngle);

//  sh.set_notify("RESTART", this, &AState::restart);
  
  //Sparrow bind goes here

  // Run sparrow
  sh.run();

  while(sh.running()) 
  { 
    
    usleep(100000);
  }

  m_quitPressed = 1;

}

