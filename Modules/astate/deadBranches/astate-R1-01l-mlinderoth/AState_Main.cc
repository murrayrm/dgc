/*! \file AState_Main.cc
 * \brief Main function of Astate
 * \author Stefano Di Cairano,
 * \date jan-07
 */


#include <stdlib.h>
#include "AState.hh"
#include "dgcutils/DGCutils.hh"
#include "skynet/skynet.hh"
#include "cmdline.h"

AState *ss;


int main(int argc, char **argv)
{
  int sn_key  = skynet_findkey(argc, argv); //getting skynet key
  
  astateOpts options;
  options.useSparrow=1;
  options.logRaw=0;
  options.stateReplay=0;
  options.dataReplay=0;
  options.fastReplay=0;
  options.frequencyDivisor=1;
  options.fixLocalFrame = 0;
  options.fixXY = 0;
  options.initLocalToSite = 0;

  //parsing user defined arguments
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0)
  { 
    exit(1);
  }
  
  if (cmdline.enable_logging_given)
  {
  	options.logRaw = 1;
  }  
 
  if (cmdline.enable_statereplay_given)
  {
  	options.stateReplay = 1;
  }  
  
  if (cmdline.enable_datareplay_given)
  {
  	options.dataReplay = 1; 
  }  
  
  if (cmdline.fast_replay_given)
  {
  	options.fastReplay = 1; 
  }  
  
  if (cmdline.disable_sparrow_given)
  {
  	options.useSparrow = 0;
  }
  
  if (cmdline.frequency_divisor_given) {
    options.frequencyDivisor = cmdline.frequency_divisor_arg;
  }

  if (cmdline.fix_local_frame_given) {
    options.fixLocalFrame = 1;
  }
  
  if (cmdline.fix_xy_given) {
    options.fixXY = 1;
  }

  if (cmdline.init_local_to_site_given) {
    options.initLocalToSite = 1;
  }
         
  // few checks to avoid stupid inconsistencies

  if ((options.dataReplay==1) && (options.stateReplay==1))
  {
  	cout << "You should decide: EITHER replay from state OR replay from DATA !!"<< endl;
  	exit(1);
  }
  
  if ( (options.stateReplay==1) && (options.logRaw==1) )
  {
  	cout << "Why do you want to log replay data? This makes no sense"<< endl;
  	exit(1);
  }
  
 
  // Create AState server:
  ss = new AState(sn_key,options);

  // If the RNDF was given, load the site frame
  if (cmdline.rndf_given)
    ss->loadRNDF(cmdline.rndf_arg);

  // Initialize connection, start threads, etc
  ss->start();

  // Loop until done
  ss->active();

  delete ss;

  return 0;
}
