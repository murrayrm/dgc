/* asdisp.dd (repAstate) 
 *
 * Astate display table
 *
 * Stefano Di Cairano, DEC-06
 *
 */

extern long long sparrowhawk;
#define D(x) (sparrowhawk)

%%
ASTATE -- SNKEY: %snkey
--------------------------+----------------------------------------------------+
APX: %a      %apxCount    | Time:      %dTime                                  |
Status:      %status      | Estimate:  %stateStatus    DataReplay: %Drep       |
APX Dead:    %apxDead     | Logging:   %Log           StateReplay: %Rep        |
--------------------------+----------------------------------------------------+
VEHICLESTATE	                                                               |
 N: %northing     | N': %n_dot       | N'': %n_dot_dot    | NC: %northConf     |
 E: %easting      | E': %e_dot       | E'': %e_dot_dot    | EC: %eastConf      |
 D: %altitude	  | D': %d_dot       | D'': %d_dot_dot    | HC: %heightConf    |
 R: %roll         | R': %r_dot       | R'': %r_dot_dot    | RC: %rollConf      |
 P: %pitch        | P': %p_dot       | P'': %p_dot_dot    | PC: %pitchConf     |
 Y: %yaw          | Y': %y_dot       | Y'': %y_dot_dot    | YC: %yawConf       |
-------------------------------------------------------------------------------+
LOCAL POSE          SITE POSE                                                  |
 N: %lp_northing  | N: %sp_n         |                    | VNC: %vNorthConf   |
 E: %lp_easting   | E: %sp_e         |                    | VEC: %vEastConf    |
 D: %lp_altitude  | D: %sp_d         |                    | VDC: %vDownConf    |
 R: %lp_roll      | R: %sp_r         |                    | EMj: %ellAxMaj     |
 P: %lp_pitch     | P: %sp_p         |                    | Emn: %ellAxMin     |
 Y: %lp_yaw       | Y: %sp_y         |                    | EOr: %ellOrient    |
-------------------------------------------------------------------------------+
DRIFT:            | N:  %dr_n        | E:   %dr_e         | D:   %dr_d         |
-------------------------------------------------------------------------------+

%RESTART          %EXIT
%%

tblname: asdisp;
bufname: asdisp_buf;


# Variables 
int:	%snkey		D(snkey)			"%8d";
double:	%dTime          D(mytime)                       "%12.3f"; 
int: 	%a	  	D(apxConnected)        		"%2d";
int:	%apxCount	D(apxCount)			"%2d";	
int:	%apxDead        D(apxDead)			"%2d";	
int: 	%stateStatus    D(stateStatus) 		        "%2d";
int:    %Log  	        D(logEn)      		        "%2d";
int:    %Rep            D(repEn)                        "%2d";
int:    %Drep           D(drepEn)                       "%2d";
int:    %status         D(status)                       "%2d";

double: %northing	D(vsNorthing)		"%12.3f";
double: %easting	D(vsEasting)		"%12.3f";
double: %altitude	D(vsAltitude)		"%12.3f";
double: %n_dot		D(vsVel_N)			"%12.3f";
double: %e_dot		D(vsVel_E)			"%12.3f";
double: %d_dot		D(vsVel_D)			"%12.3f";
double: %n_dot_dot	D(vsAcc_N)			"%12.3f";
double: %e_dot_dot	D(vsAcc_E)			"%12.3f";
double: %d_dot_dot	D(vsAcc_D)			"%12.3f";
double: %roll		D(vsRoll)			"%12.5f";
double: %pitch		D(vsPitch)			"%12.5f";
double: %yaw		D(vsYaw)			"%12.5f";
double: %r_dot		D(vsRollRate)		"%12.5f";
double: %p_dot		D(vsPitchRate)		"%12.5f";
double: %y_dot		D(vsYawRate)		"%12.5f";
double: %r_dot_dot	D(vsRollAcc)		"%12.5f";
double: %p_dot_dot	D(vsPitchAcc)		"%12.5f";
double: %y_dot_dot	D(vsYawAcc)			"%12.5f";
double: %northConf	D(vsNorthConf)		"%12.3f";
double: %eastConf	D(vsEastConf)		"%12.3f";
double: %heightConf	D(vsHeightConf)		"%12.3f";
double: %rollConf	D(vsRollConf)		"%12.3f";
double: %pitchConf	D(vsPitchConf)		"%12.3f";
double: %yawConf	D(vsYawConf)		"%12.3f";

double: %lp_northing	D(lpNorthing)		"%12.3f";
double: %lp_easting	    D(lpEasting)		"%12.3f";
double: %lp_altitude	D(lpAltitude)		"%12.3f";
# double: %lp_n_dot		D(lpVel_N)			"%12.3f";
# double: %lp_e_dot		D(lpVel_E)			"%12.3f";
# double: %lp_d_dot		D(lpVel_D)			"%12.3f";

double: %sp_n	D(spNorthing)		"%12.3f";
double: %sp_e	    D(spEasting)		"%12.3f";
double: %sp_d	D(spAltitude)		"%12.3f";
double: %sp_r	D(spRoll)		"%12.5f";
double: %sp_p	D(spPitch)		"%12.5f";
double: %sp_y	D(spYaw)		"%12.5f";

# double: %lp_n_dot_dot	D(lpAcc_N)			"%12.3f";
# double: %lp_e_dot_dot	D(lpAcc_E)			"%12.3f";
# double: %lp_d_dot_dot	D(lpAcc_D)			"%12.3f";
double: %lp_roll		D(lpRoll)			"%12.5f";
double: %lp_pitch		D(lpPitch)			"%12.5f";
double: %lp_yaw		    D(lpYaw)			"%12.5f";
# double: %lp_r_dot		D(lpRollRate)		"%12.5f";
# double: %lp_p_dot		D(lpPitchRate)		"%12.5f";
# double: %lp_y_dot		D(lpYawRate)		"%12.5f";
# double: %lp_r_dot_dot	D(lpRollAcc)		"%12.5f";
# double: %lp_p_dot_dot	D(lpPitchAcc)		"%12.5f";
# double: %lp_y_dot_dot	D(lpYawAcc)		"%12.5f";
double:  %vNorthConf 	D(velNorthConf)		"%12.3f";
double:  %vEastConf     D(velEastConf)		"%12.3f";
double:  %vDownConf  	D(velHeightConf)	"%12.3f";
double:  %ellAxMaj      D(errEllMaj)		"%12.3f";
double:  %ellAxMin   	D(errEllMin)		"%12.3f";
double:  %ellOrient     D(errEllAngle)		"%12.3f";

double:  %dr_n          D(driftN)		"%12.6f";
double:  %dr_e  	D(driftE)		"%12.6f";
double:  %dr_d          D(driftD)		"%12.6f";

 button: %EXIT "EXIT" dd_exit_loop;
 button: %RESTART "RESTART" NULL;
