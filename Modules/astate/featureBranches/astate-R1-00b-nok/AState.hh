/* \file AState.hh 
 * \brief Astate class declaration
 * \author Stefano Di Cairano,
 * \date dec-06
 */

#ifndef ASTATE_HH
#define ASTATE_HH

#include <fstream>

#include "applanix.hh" //Applanix header file. Define here msgs structure and applnix constants

#include "skynet/sn_msg.hh"
#include "skynet/sn_types.h"
#include "interfaces/SkynetContainer.hh"
#include "interfaces/VehicleState.h"
// #include "interfaces/ActuatorState.hh"
#include "dgcutils/DGCutils.hh"

#define APX_DATA_BUFFER_SIZE 1500
#define DEBUG_LEVEL 5 




/*! Struct containing the Applanix Navigation data
 * /param time: long long containing the data timestamp
 * /param data: apxNavData struct containing navigation data  
 */
 
struct rawApx {	
	unsigned long long time;
	apxNavData data;
};

/*! Struct containing the user defined options
 *  /param useSparrow: integer, set to 0 to disable sparrow display
 *  /param logRaw: integer, set to 1 to do logging
 *  /param stateReplay: integer, set to 1 to do replay from state data
 */

struct astateOpts {
	int useSparrow;
	int logRaw;
	int stateReplay;
        int dataReplay;
};

/*! Status of state estimation
 */ 

enum {
  PREINIT,
  INIT,
  NAV,
  FALLBACK,
  OH_SHIT,
  NUM_MODES
};

/*! Applanix navigation solution status
 */
 
enum {
  FULL_NAV,   //this refers to table 5 in applanix Port Interface Document
  FINE_ALIGN,
  ALIGN_GPS,
  ALIGN_NOGPS,
  NOT_ALIGN_GPS,
  NOT_ALIGN_NOGPS,
  COARSE_LEVELING,
  INITIAL,
  NO_SOLUTION
};

/*! Astate class declaration
 */

class AState : public CSkynetContainer
{


  pthread_mutex_t m_VehicleStateMutex;
  // Lock these for the m_VehicleStateMutex

  /*! VehicleState struct containing Alice current state estimate
   */ 
  struct VehicleState _vehicleState;

  // end lock list 

  pthread_mutex_t m_MetaStateMutex;

  // Lock these for the m_MetaStateMutex


  /*! Socket to read from POS-LV, 
   */
  int apxDataSocket;      // set in the constructor and never changed
  
  /*! Socket to broadcast state estimate
   */
  int broadcastStateSock;  // Should be set in constructor and never changed.
  

  pthread_mutex_t m_ApxDataMutex;

  // Lock these for the m_ApxDataMutex

  DGCcondition apxBufferFree;

 /*! \param apxBuffer: rawApx array containing navigation data received from POS-LV
  */
  rawApx apxBuffer[APX_DATA_BUFFER_SIZE]; 
  
  
  int apxBufferReadInd;
  int apxBufferLastInd;


  // end lock list 

/*\param stateMode: state estimate status
 */
  int stateMode;
 
  //These variables are ONLY here to give sparrow access to them...

  /*! Values relevent to sparrow:
   * \param apxCount: integer, number of navigation data packets received from POS-LV
   * \param apxDead: integer, 1 if POS_LV is not working
   * \param apxStatus: integer, navigation solution status
   * \param apxEnabled: integer, 0 if POS-LV is disabled
   * \param apxActive: integer, 1 if POS-LV is sending data
   * \param timberEnabled: integer, 1 if Timber is enabled (not implemented yet)
   */ 
  int apxCount;
  int apxStatus;
  int apxEnabled;
  int apxActive;
  int apxDead;

  int timberEnabled;
  
 /*! Navigation data obtained from POS-LV
  */
  
  double apxHeight;
  double apxNorth;
  double apxEast;
  double apxVelN;
  double apxVelE;
  double apxVelD;
  double apxAccN;
  double apxAccE;
  double apxAccD;
  double apxRoll;
  double apxPitch;
  double apxHeading;
  double apxRollRate;
  double apxPitchRate;
  double apxHeadingRate;

/*! Navigation solution accuracy. Not implemented yet
 */
  double apxHeight_accuracy;
  double apxNorth_accuracy;
  double apxEast_accuracy;
  double apxVelN_accuracy;
  double apxVelE_accuracy;
  double apxVelD_accuracy;
  double apxAccN_accuracy;
  double apxAccE_accuracy;
  double apxAccD_accuracy;
  double apxRoll_accuracy;
  double apxPitch_accuracy;
  double apxHeading_accuracy;
  double apxRollRate_accuracy;
  double apxPitchRate_accuracy;
  double apxHeadingRate_accuracy;


  DGCcondition newData;


  // Lock these for the m_HeartbeatMutex


  // end lock list


  // Variables to remain unlocked
  int quitPressed;

/*! \param snKey: integer, the skynet key
 */
    int snKey;  // Should be set in AState_Main and never changed.

/*! _options: astateOpts, internal copy of user defined options
 */  
  astateOpts _options; 

/*! startTime: long long, initial time
 */  
  unsigned long long startTime; // Should be set in constructor and never changed.

  // Initial offset for bogus local pose
  bool haveInitPose;
  double initE, initN;

  fstream apxLogStream; // These four are opened & closed in constructor and destructor
  fstream stateLogStream;   
  char apxLogFile[100];
  char stateLogFile[100];
  fstream replayStateStream;  
  char replayStateFile[100];

  /*****************************************************************
   *  The following are the private methods of the AState class.
   *****************************************************************/

    
  /*! Function that updates the vehicle_state struct */
  void updateStateThread();

  /*! Function to broadcast state data */
  void broadcast();

  /*! Thread to read from the POS-LV. */
  void apxReadThread(); 
  
  /*! Thread to control POS-LV. */
//  void apxControlThread();

  /*! Thread to update sparrow variables*/
  void sparrowThread();

//  void playback();



public:
  /*! AState Constructor */
  AState(int skynetKey, astateOpts options);

 /*! Astate main loop
  */
  void active();
  
/*! Astate desctrutor
 */
  ~AState();
};

#endif
