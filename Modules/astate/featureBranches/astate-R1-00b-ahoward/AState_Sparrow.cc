/* \file Astate_Sparrow.cc
 * \brief Astate class functions to generate/update Astate Sparrow display
 * \author Stefano Di Cairano,
 * \date dec-06
 */

#include "AState.hh"

//Struct containing all possible pieces of data being outputted

#include "sparrowhawk/SparrowHawk.hh"
#include "asdisp.h"		// display table

//Populate displayVars struct

void AState::sparrowThread() 
{
  CSparrowHawk &sh = SparrowHawk();

  sh.add_page(asdisp, "Main");

  sh.rebind("snkey", &snKey);
  
//  sh.rebind("apxActive", &apxActive);
  sh.rebind("apxStatus", &apxStatus);
  sh.rebind("apxEnabled", &apxEnabled);
  sh.rebind("apxCount", &apxCount);
  sh.rebind("stateMode", &stateMode);
  sh.rebind("timberEn", &timberEnabled);
  sh.rebind("apxActive", &apxActive);
  sh.rebind("logEn", &_options.logRaw);
  sh.rebind("repEn", &_options.stateReplay);
  
  sh.rebind("vsNorthing", &_vehicleState.northing);
  sh.rebind("vsEasting", &_vehicleState.easting);
  sh.rebind("vsAltitude", &_vehicleState.altitude);
  sh.rebind("vsVel_N",  &_vehicleState.velN);
  sh.rebind("vsVel_E",  &_vehicleState.velE);
  sh.rebind("vsVel_D",  &_vehicleState.velD);
  sh.rebind("vsRoll", &_vehicleState.roll);
  sh.rebind("vsPitch", &_vehicleState.pitch);
  sh.rebind("vsYaw", &_vehicleState.yaw);
  sh.rebind("vsRollRate", &_vehicleState.rollRate);
  sh.rebind("vsPitchRate", &_vehicleState.pitchRate);
  sh.rebind("vsYawRate", &_vehicleState.yawRate);
  sh.rebind("vsNorthConf", &_vehicleState.northConfidence);
  sh.rebind("vsEastConf", &_vehicleState.eastConfidence);
  sh.rebind("vsHeightConf", &_vehicleState.altitudeConfidence);
  sh.rebind("vsRollConf", &_vehicleState.rollConfidence);
  sh.rebind("vsPitchConf", &_vehicleState.pitchConfidence);
  sh.rebind("vsYawConf", &_vehicleState.yawConfidence);
  sh.rebind("lpNorthing", &_vehicleState.localX);
  sh.rebind("lpEasting", &_vehicleState.localY);
  sh.rebind("lpAltitude", &_vehicleState.localZ);
  sh.rebind("lpVel_N",  &_vehicleState.vehicleVelX);
  sh.rebind("lpVel_E",  &_vehicleState.vehicleVelY);
  sh.rebind("lpVel_D",  &_vehicleState.vehicleVelZ);
  sh.rebind("lpRoll", &_vehicleState.localRoll);
  sh.rebind("lpPitch", &_vehicleState.localPitch);
  sh.rebind("lpYaw", &_vehicleState.localYaw);
  sh.rebind("lpRollRate", &_vehicleState.vehicleVelRoll);
  sh.rebind("lpPitchRate", &_vehicleState.vehicleVelPitch);
  
//  sh.set_notify("RESTART", this, &AState::restart);
  
  //Sparrow bind goes here

  // Run sparrow
  sh.run();

  while(sh.running()) 
  { 
    usleep(100000);
  }

  quitPressed = 1;

}

