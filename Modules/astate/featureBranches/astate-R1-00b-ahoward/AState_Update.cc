/* \file AState_Update.cc
 * \brief Astate class function that updates the state estimate
 * \author Stefano Di Cairano,
 * \date dec-06
 */

#include <math.h>

#include "AState.hh"
#include "sparrow/display.h"
#include "sparrow/flag.h"
#include "dgcutils/ggis.h"

#define FREQUENCY_DIVISOR 8
#define square(x) ((x)*(x))

extern unsigned short setStateMode(unsigned short);
GisCoordLatLon latsForVehState;
GisCoordUTM utmForVehState;


/*! Thread that updates the state estimate
 */
 
void AState::updateStateThread()
{
  unsigned long long stateTime = 0;
  unsigned long long replayStateLogStart = 0;
  unsigned long long replayDataTime = 0;
  unsigned long long nowTime = 0;
  
  unsigned long long apxTime = 0;
  rawApx apxEstimate;
  int newState = 0;
  int count = 0;   //Counter that increments every loop (used to limit broadcast rate)
  int haveInitTime = 0;
  VehicleState inState; //used for replay as temp var
  
  while (!quitPressed)
  {
    newState = 0;
         
    if(_options.stateReplay==1)
    {    
      replayStateStream.read((char*)&inState, sizeof(VehicleState));
      
      if(haveInitTime == 0) 
      {
        replayStateLogStart = inState.timestamp; //this is the initial instant of the logging
        haveInitTime = 1;
      }
      if(replayStateStream.eof()==1)
      {
        quitPressed=1;
      }

      replayDataTime = inState.timestamp - replayStateLogStart + startTime;
      DGCgettime(nowTime);
      while( replayDataTime > nowTime )
      {
        usleep(10);
        DGCgettime(nowTime);
      }
      	  
      _vehicleState = inState;
      newState = 1;
    }
    else
    { 
      DGClockMutex(&m_ApxDataMutex);
      if (apxBufferReadInd > apxBufferLastInd)
      { 
        
        DGCSetConditionTrue(apxBufferFree);
        apxBufferLastInd++;

        int ind = apxBufferLastInd % APX_DATA_BUFFER_SIZE;

        apxEstimate=apxBuffer[ind];

        apxTime = apxBuffer[ind].time;

        if(stateTime < apxTime)
        {
          stateTime = apxTime;
        }

        newState = 1;
        apxCount++;
      }
       
      DGCunlockMutex(&m_ApxDataMutex); 
      if( (DEBUG_LEVEL>4) && (apxActive == 0) ) //I this is for test when Applanix is not connected
      {
        usleep(5000);
        apxEstimate.time = 100;
        apxEstimate.data.latitude = 100;
        apxEstimate.data.longitude = 100;
        apxEstimate.data.altitude = 100;
        apxEstimate.data.heading = 100;
        apxEstimate.data.northVelocity = 100;
        apxEstimate.data.eastVelocity = 100;
        apxEstimate.data.downVelocity = 100;
        apxEstimate.data.longitudinalAcc = 100;
        apxEstimate.data.transverseAcc = 100;
        apxEstimate.data.downAcc = 100;
        apxEstimate.data.roll = 100;
        apxEstimate.data.pitch = 100;
        apxEstimate.data.heading = 100;
        apxEstimate.data.rollRate = 100;
        apxEstimate.data.pitchRate = 100;
        apxEstimate.data.yawRate = 100;
        apxEstimate.data.alignStatus = NO_SOLUTION;
        newState=1;
      }
     
    }

    if (newState == 0)
    { 
      newData.bCond = false;
      DGCWaitForConditionTrue(newData);
    }
    else
    { 
      DGClockMutex(&m_VehicleStateMutex);
      if(_options.stateReplay==0)
      {
        latsForVehState.latitude = apxEstimate.data.latitude;
        latsForVehState.longitude = apxEstimate.data.longitude;
        gis_coord_latlon_to_utm(&latsForVehState, &utmForVehState, GIS_GEODETIC_MODEL_WGS_84);
     
        _vehicleState.timestamp = apxEstimate.time;
        _vehicleState.northing = utmForVehState.n; 
        _vehicleState.easting = utmForVehState.e;
        _vehicleState.altitude = -apxEstimate.data.altitude; //POS-LV gives positive altitude
        _vehicleState.velN = apxEstimate.data.northVelocity;
        _vehicleState.velE = apxEstimate.data.eastVelocity;
        _vehicleState.velD = apxEstimate.data.downVelocity;
        _vehicleState.rollRate = apxEstimate.data.rollRate*M_PI/180;
        _vehicleState.pitchRate = apxEstimate.data.pitchRate*M_PI/180;
        _vehicleState.yawRate = apxEstimate.data.yawRate*M_PI/180;
        _vehicleState.roll = apxEstimate.data.roll*M_PI/180;
        _vehicleState.pitch =  apxEstimate.data.pitch*M_PI/180;
        _vehicleState.yaw =  apxEstimate.data.heading*M_PI/180;
        // Update local pose
        if ((!this->haveInitPose) && (_vehicleState.easting >0) && (_vehicleState.northing > 0 ))
        {
          this->haveInitPose = true;
          this->initE = _vehicleState.easting;
          this->initN = _vehicleState.northing;
        }
        _vehicleState.localX = _vehicleState.northing - this->initN;
        _vehicleState.localY = _vehicleState.easting - this->initE;
        _vehicleState.localZ = _vehicleState.altitude;
        _vehicleState.localRoll = _vehicleState.roll;
        _vehicleState.localPitch = _vehicleState.pitch;
        _vehicleState.localYaw = _vehicleState.yaw;
        _vehicleState.vehicleVelX =   cos(_vehicleState.yaw) * _vehicleState.velN;
        _vehicleState.vehicleVelX +=  sin(_vehicleState.yaw) * _vehicleState.velE;
        _vehicleState.vehicleVelY =  -sin(_vehicleState.yaw) * _vehicleState.velN;
        _vehicleState.vehicleVelY +=  cos(_vehicleState.yaw) * _vehicleState.velE;
        _vehicleState.vehicleVelRoll = _vehicleState.rollRate;
        _vehicleState.vehicleVelPitch = _vehicleState.pitchRate;
        _vehicleState.vehicleVelYaw = _vehicleState.yawRate;
  	      
        //We don't get confidences yet

        _vehicleState.northConfidence = 1;
        _vehicleState.eastConfidence = 1;
        _vehicleState.altitudeConfidence = 1;
        _vehicleState.rollConfidence = 1;
        _vehicleState.pitchConfidence = 1;
        _vehicleState.yawConfidence = 1;
        apxStatus = apxEstimate.data.alignStatus;
        stateMode = setStateMode((unsigned short)apxEstimate.data.alignStatus);
      }
      else
      {
      	_vehicleState=inState;
      } 
       
      DGCunlockMutex(&m_VehicleStateMutex);
      
      if(count++ % FREQUENCY_DIVISOR == 0) 
      {
        broadcast();
      }
      
      if (_options.logRaw==1)
      {
        stateLogStream.write((char*)&_vehicleState, sizeof(_vehicleState));      
        if(apxBufferReadInd % 1300 == 1)
        {
          apxLogStream.flush();
        }      	    
      }            
    } //end of the ELSE (update if got a newState)
    
  }//end of the main loop
}//end of the Thread


/*! Converts the apxStatus into AState statemode.
 * preinit stands for the initial solution
 * init stands for a large error (coarse leveling or heading error > 15 )
 * nav stands for for full navigation or heading error < 15 deg (fine alignement)
 * oh_shit stands for a missing initial solution/ system errors
 */
  
unsigned short setStateMode(unsigned short status)
{
  unsigned short mode = OH_SHIT;
  
  if(status<8)
  {
	mode = PREINIT;
  }
  
  if(status<7)
  {
	mode = INIT;
  }
  
  if(status<1)
  {
	mode = NAV;
  }
  
  return(mode);
}
