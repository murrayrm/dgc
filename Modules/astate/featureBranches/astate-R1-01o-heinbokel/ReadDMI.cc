/*! \file ReadDMI.cc
 * \brief Reads DMI data from POS-LV
 * \author Magnus Linderoth,
 * \date 2007-10-02
 */


#include <stdlib.h>
#include "skynet/skynet.hh"
//#include "applanix.cc"
#include "ReadDMI.hh"

#include <sys/types.h>   //these are the libraries needed for sockets
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <errno.h>



ReadDMI::ReadDMI(int sn_key, bool log, bool plot)
{
  m_sn_key = sn_key;
  m_log = log;
  m_plot = plot;

  m_initOffset = 2;

  DGCcreateMutex(&m_bufferMutex);
  DGCcreateCondition(&m_dataAvailable);
  DGCSetConditionFalse(m_dataAvailable);
  m_bufferReadIndex = m_bufferWriteIndex = 0;
  memset(&m_dataBuffer, 0, sizeof(m_dataBuffer));

  if (m_log) {
    mkdir("/tmp/logs/astate_raw/",
          S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH
          | S_IXOTH);

    char fileName[256];
    time_t t = time(NULL);
    tm *local;
    local = localtime(&t);
    sprintf(fileName,
            "/tmp/logs/astate_raw/%04d%02d%02d_%02d%02d%02d_DMI.raw",
            local->tm_year + 1900, local->tm_mon + 1, local->tm_mday,
            local->tm_hour, local->tm_min, local->tm_sec);

    m_logStream.open(fileName, fstream::out);
    cerr << "Logging to " << fileName << endl;
  }

  if (m_plot) {
    m_meTalker.initSendMapElement(m_sn_key);
    cerr << "Plotting on sendSubGroup " << SKYNET_SEND_SUBGROUP << endl;
  }
}

ReadDMI::~ReadDMI()
{

}

void ReadDMI::run()
{
  char buffer[2 * RAW_DMI_GRP_LENGTH]; // Double length just to be safe

  DGCstartMemberFunctionThread(this, &ReadDMI::plotLogThread); 

  int msgCount = 0;

  while (true) {

    while (!m_apxConnected) {
      inetConnect();
    }

    
    int comStatus = 0;
    memset(buffer,'\0',sizeof(buffer)); //reset the buffer
    int msgLength;
           
    while (comStatus < 1 )  {   
      comStatus = apxTcpReceive(&m_apxDataSocket, buffer, &msgLength);
    }

    // we have a new message hence the Applanix is working
    if (comStatus==2)  {
      //failure
      m_apxConnected = 0;
    } else {
      if (msgLength == RAW_DMI_GRP_LENGTH) {
	//we have received a raw DMI data message
	apxRawDMIData rawDMIData;
	parseRawDMIData(buffer, RAW_DMI_GRP_LENGTH, &rawDMIData);

	DGClockMutex(&m_bufferMutex);
	m_dataBuffer[m_bufferWriteIndex++] = rawDMIData;
	DGCunlockMutex(&m_bufferMutex);
	
	m_bufferWriteIndex %= BUF_SIZE;

	if (m_bufferWriteIndex % SAMPS_PER_LOG_CYCLE == 0) {
	  DGCSetConditionTrue(m_dataAvailable);
	}
      }
      if (++msgCount % 200 == 0) {
        cout << "Received " << msgCount << " messages" << endl;
      }
    }
  }
}

void ReadDMI::plotLogThread()
{
  while (true) {
    DGCWaitForConditionTrue(m_dataAvailable);

    if (m_initOffset) {
      m_initOffset--;
      if (m_initOffset == 0) {
	m_offset = m_dataBuffer[0].upDownPulseCount;
      }
    }

    DGCSetConditionFalse(m_dataAvailable);

    if (m_log) {
      for (int i = m_bufferReadIndex; i < m_bufferReadIndex + SAMPS_PER_LOG_CYCLE; i++) {
        char str[256];
        sprintf(str, "%f\t%li\t%lu\t%li\n",
                m_dataBuffer[i % BUF_SIZE].time2,
                m_dataBuffer[i % BUF_SIZE].upDownPulseCount,
                m_dataBuffer[i % BUF_SIZE].rectifiedPulseCount,
                m_dataBuffer[i % BUF_SIZE].eventCount);
        m_logStream.write(str, strlen(str));
      }
      m_logStream.flush();
    }

    if (m_plot) {
      MapElement me;
      point2 point;
      vector<point2> points;

      double startTime = m_dataBuffer[(m_bufferReadIndex + SAMPS_PER_LOG_CYCLE - 1) % BUF_SIZE].time2;
      
      // UpDownPulseCount
      for (int i = m_bufferReadIndex + SAMPS_PER_LOG_CYCLE - 1;
           i >= m_bufferReadIndex - BUF_SIZE + 2*SAMPS_PER_LOG_CYCLE;
           i--) {
        point.set(m_dataBuffer[i % BUF_SIZE].time2 - startTime,
                  (m_dataBuffer[i % BUF_SIZE].upDownPulseCount - m_offset) * VERT_SCALE);
        points.push_back(point);
      }
      me.setColor(MAP_COLOR_RED);
      me.setGeometry(points);
      me.setId(1);
      me.setTypeLine();
      m_meTalker.sendMapElement(&me, SKYNET_SEND_SUBGROUP);
      me.setId(2);
      me.setTypePoints();
      m_meTalker.sendMapElement(&me, SKYNET_SEND_SUBGROUP);
      
      // RectifiedPulseCount
      points.clear();
      for (int i = m_bufferReadIndex + SAMPS_PER_LOG_CYCLE - 1;
           i >= m_bufferReadIndex - BUF_SIZE + 2*SAMPS_PER_LOG_CYCLE;
           i--) {
        point.set(m_dataBuffer[i % BUF_SIZE].time2 - startTime,
                  m_dataBuffer[i % BUF_SIZE].rectifiedPulseCount * VERT_SCALE);
        points.push_back(point);
      }
      me.setColor(MAP_COLOR_BLUE);
      me.setGeometry(points);
      me.setId(3);
      me.setTypeLine();
      m_meTalker.sendMapElement(&me, SKYNET_SEND_SUBGROUP);
      me.setId(4);
      me.setTypePoints();
      m_meTalker.sendMapElement(&me, SKYNET_SEND_SUBGROUP);
      
      /*// EventCount
      points.clear();
      for (int i = m_bufferReadIndex + SAMPS_PER_LOG_CYCLE - 1;
           i >= m_bufferReadIndex - BUF_SIZE + 2*SAMPS_PER_LOG_CYCLE;
           i--) {
        point.set(m_dataBuffer[i % BUF_SIZE].time2 - startTime,
                  m_dataBuffer[i % BUF_SIZE].eventCount);
        points.push_back(point);
      }
      me.setColor(MAP_COLOR_CYAN);
      me.setGeometry(points);
      me.setId(5);
      me.setTypeLine();
      m_meTalker.sendMapElement(&me, SKYNET_SEND_SUBGROUP);
      me.setId(6);
      me.setTypePoints();
      m_meTalker.sendMapElement(&me, SKYNET_SEND_SUBGROUP);
      */
    }
    
    m_bufferReadIndex += SAMPS_PER_LOG_CYCLE;
  }
}


void ReadDMI::inetConnect()
{
  close(m_apxDataSocket);                             // closing the old socket as it may be pointing nowhere
  usleep(RECONNECTION_PERIOD);                                     // it seems that after closing the socket the POS-LV needs some time before open 
  m_apxDataSocket=socket(PF_INET, SOCK_STREAM, 0); 	// create the data socket
  if(m_apxDataSocket < 0) {
    cout << "ERROR while creating data socket" << endl;
    exit(1);
  }
  struct sockaddr_in apxData_addr;					// these structs contains the addresses 
  apxData_addr.sin_addr.s_addr = inet_addr(APX_IP_ADDR);  
  apxData_addr.sin_family = AF_INET;
  apxData_addr.sin_port = htons(APX_DATA_PORT);  					
  
  cout<<" Attempting a connection" << endl; 
  if (connect(m_apxDataSocket,(struct sockaddr *)&apxData_addr,sizeof(apxData_addr)) < 0) {
    cerr << ("ERROR connecting to Applanix Data Port") << endl;
    cerr << "Error: " << strerror(errno) << endl;
    m_apxConnected = 0;
  } else { 
    m_apxConnected = 1;
    cerr << "Connected to Data Port" << endl; 
  }
}


// **************** RECEIVE ******************

//I am assuming to receive the raw DMI data group in a message.
//We throw away everything else

short ReadDMI::apxTcpReceive(int* socket, char* message, int* msgLength)
{       
  short comStatus = 1;                            
  short recBytes = 0;
  fd_set readfds;
  struct timeval tv;

  char buffer[APX_BUFFER_SIZE];
  FD_ZERO(&readfds);
  FD_SET(*socket,&readfds);
  tv.tv_sec=0;
  tv.tv_usec=TIMEOUT_PERIOD;
  int isReady=select(*socket+1,&readfds,NULL,NULL,&tv);
  int test = 0;
  if( FD_ISSET(*socket,&readfds)){
    test = 1;
  }
  if(isReady>0) {
    recBytes=recv(*socket, buffer, APX_BUFFER_SIZE, 0);
    if(recBytes < 1) {
      comStatus = 2;
    } else {
      if(recBytes != RAW_DMI_GRP_LENGTH) {
        comStatus = 0;     
        //   cout << "Received bytes: " << recBytes << endl; // use this to get infor about misformed packets
      } else {
        char isGroup[5]="$GRP";
        char isMine[5]="nnnn";       //initialization to have a null-terminated string...
        memcpy(&isMine[0],&buffer[0],4);
        
        if( strcmp(isMine,isGroup)!=0 ) {  //is not a group message
          comStatus = 0;           //this packet is out of synchrony
        } else {
          *msgLength = recBytes;
        }
      }	                  
      
      if(comStatus > 0) {
        memcpy(message, buffer, recBytes);                //copy the temp buffer into message buffer
      }
    }
  } else {
    comStatus = 2;
  }
  return(comStatus);
}


void ReadDMI::parseRawDMIData(char* inBuffer, int size, apxRawDMIData* msg)
{
  char buffer[size];
  memcpy(buffer, inBuffer, size);
  memset(&(msg->groupStart[0]),'\0',5);
  memcpy(&(msg->groupStart[0]),&buffer[0],4);  
  
  
  memcpy(&(msg->ID),&buffer[4],2);  
  memcpy(&(msg->byteCount),&buffer[6],2);
  memcpy(&(msg->time1),&buffer[8],8);
  memcpy(&(msg->time2),&buffer[16],8);
  memcpy(&(msg->distanceTag),&buffer[24],8);
  memcpy(&(msg->timeTypes),&buffer[32],1);
  memcpy(&(msg->distanceType),&buffer[33],1);

  memcpy(&(msg->upDownPulseCount),&buffer[34],4);
  memcpy(&(msg->rectifiedPulseCount),&buffer[38],4);
  memcpy(&(msg->eventCount),&buffer[42],4);
  memcpy(&(msg->reservedCount),&buffer[46],4);

  memcpy(&(msg->pad),&buffer[50],2);
  memcpy(&(msg->checksum),&buffer[52],2);
  memset(&(msg->groupEnd[0]),'\0',3);
  memcpy(&(msg->groupEnd[0]),&buffer[54],2);
 
}



int main(int argc, char **argv)
{
  int sn_key  = skynet_findkey(argc, argv); //getting skynet key
  
  bool log = true;
  bool plot = true;

  for (int i = 0; i < argc; i ++) {
    if (strcmp(argv[i], "--no-log") == 0) {
      log = false;
    }
    if (strcmp(argv[i], "--no-plot") == 0) {
      plot = false;
    }
  }

  ReadDMI readDMI(sn_key, log, plot);

  readDMI.run();
}





