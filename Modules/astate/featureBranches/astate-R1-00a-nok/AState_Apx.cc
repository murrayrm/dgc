/*! \file AState_APX.cc
 *  \brief AState class functions to interface with Applanix POS-LV.
 *  \author Stefano Di Cairano,
 *  \date dec-06
 */

#include "AState.hh"

/*! Thread that reads from Applanix POS-LV
 */
 
void AState::apxReadThread()
{
  unsigned long long nowTime;
  unsigned long long rawApxTime;

  rawApx apxIn;
  apxNavData navData;
  
  int readError = 0;
  char buffer[NAV_GRP_LENGTH];
  
  while (!quitPressed) 
  { 
    int comStatus = 0;
    memset(buffer,'\0',sizeof(buffer)); //reset the buffer
    while (comStatus < 1)  
    {   
      comStatus = apxTcpReceive(&apxDataSocket, buffer, sizeof(buffer));
    }
    parseNavData(buffer, NAV_GRP_LENGTH, &navData);
    DGCgettime(rawApxTime); // time stamp as soon as data read.
    if(_options.logRaw==1)
    {
      apxLogStream.write((char*)&apxIn, sizeof(rawApx));
      if(apxBufferReadInd % 1500 == 1)
      {
        apxLogStream.flush();
      }
    }
    DGClockMutex(&m_ApxDataMutex);
    if (((apxBufferReadInd + 1) % APX_DATA_BUFFER_SIZE) == (apxBufferLastInd % APX_DATA_BUFFER_SIZE))
    {
      apxBufferFree.bCond = false;
      DGCunlockMutex(&m_ApxDataMutex);
      DGCWaitForConditionTrue(apxBufferFree);
      DGClockMutex(&m_ApxDataMutex);
    }
    ++apxBufferReadInd;
    memcpy(&(apxIn.data), &navData, sizeof(apxNavData));
    apxIn.time = rawApxTime;
    memcpy(&apxBuffer[apxBufferReadInd % APX_DATA_BUFFER_SIZE], &apxIn, sizeof(rawApx));
    DGCunlockMutex(&m_ApxDataMutex);

    DGCSetConditionTrue(newData);
  }   
}
  