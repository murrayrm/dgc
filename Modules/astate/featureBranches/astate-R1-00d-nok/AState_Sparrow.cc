/* \file Astate_Sparrow.cc
 * \brief Astate class functions to generate/update Astate Sparrow display
 * \author Stefano Di Cairano,
 * \date dec-06
 */

#include "AState.hh"

//Struct containing all possible pieces of data being outputted

#include "sparrowhawk/SparrowHawk.hh"
#include "asdisp.h"		// display table

//Populate displayVars struct

void AState::sparrowThread() 
{
  CSparrowHawk &sh = SparrowHawk();

  sh.add_page(asdisp, "Main");

  sh.rebind("snkey", &snKey);
  
//  sh.rebind("apxActive", &apxActive);
  sh.rebind("apxStatus", &apxStatus);
  sh.rebind("apxEnabled", &apxEnabled);
  sh.rebind("apxCount", &apxCount);
  sh.rebind("stateMode", &stateMode);
  sh.rebind("timberEn", &timberEnabled);
  sh.rebind("apxActive", &apxActive);
  sh.rebind("logEn", &_options.logRaw);
  sh.rebind("repEn", &_options.stateReplay);
  
  sh.rebind("vsNorthing", &_vehicleState.utmNorthing);
  sh.rebind("vsEasting", &_vehicleState.utmEasting);
  sh.rebind("vsAltitude", &_vehicleState.utmAltitude);
  sh.rebind("vsVel_N",  &_vehicleState.utmNorthVel);
  sh.rebind("vsVel_E",  &_vehicleState.utmEastVel);
  sh.rebind("vsVel_D",  &_vehicleState.utmAltitudeVel);
  sh.rebind("vsRoll", &_vehicleState.utmRoll);
  sh.rebind("vsPitch", &_vehicleState.utmPitch);
  sh.rebind("vsYaw", &_vehicleState.utmYaw);
  sh.rebind("vsRollRate", &_vehicleState.utmRollRate);
  sh.rebind("vsPitchRate", &_vehicleState.utmPitchRate);
  sh.rebind("vsYawRate", &_vehicleState.utmYawRate);
  sh.rebind("vsNorthConf", &_vehicleState.utmNorthConfidence);
  sh.rebind("vsEastConf", &_vehicleState.utmEastConfidence);
  sh.rebind("vsHeightConf", &_vehicleState.utmAltitudeConfidence);
  //sh.rebind("vsRollConf", &_vehicleState.rollConfidence);
  //sh.rebind("vsPitchConf", &_vehicleState.pitchConfidence);
  //sh.rebind("vsYawConf", &_vehicleState.yawConfidence);

  #warning "Fix this random collection of display values"
  sh.rebind("lpNorthing", &_vehicleState.localX);
  sh.rebind("lpEasting", &_vehicleState.localY);
  sh.rebind("lpAltitude", &_vehicleState.localZ);
  sh.rebind("lpVel_N",  &_vehicleState.localXVel);
  sh.rebind("lpVel_E",  &_vehicleState.localYVel);
  sh.rebind("lpVel_D",  &_vehicleState.localZVel);
  sh.rebind("lpRoll", &_vehicleState.localRoll);
  sh.rebind("lpPitch", &_vehicleState.localPitch);
  sh.rebind("lpYaw", &_vehicleState.localYaw);
  sh.rebind("lpRollRate", &_vehicleState.vehRollRate);
  sh.rebind("lpPitchRate", &_vehicleState.vehPitchRate);
  
//  sh.set_notify("RESTART", this, &AState::restart);
  
  //Sparrow bind goes here

  // Run sparrow
  sh.run();

  while(sh.running()) 
  { 
    usleep(100000);
  }

  quitPressed = 1;

}

