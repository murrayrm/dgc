/* \file applanix.c
 * \brief Applanix POS-LV drivers definition.
 * \author Stefano Di Cairano,
 * \date dec-06
 */
 
// #include "apxTest.hh"

#include <stdio.h>
#include <string.h>
#include "applanix.hh"

using namespace std;
#define MAX_MSG_SIZE 200
#define DEBUG_LEVEL 5
#define TIMEOUT_PERIOD 100000


/* ****************************************************
 * ****************** CHECKSUM ************************
 * ****************************************************/

// *************COMPUTE CHECKSUM **********************

unsigned short apxCompChecksum(char* inBuffer,int size)
{
	
  char buffer[size];
  memcpy(buffer, inBuffer, size);	
  unsigned short checksum=0;
  unsigned short len=sizeof(buffer);
  if(len%2>0)
  {
    printf("There is an error here");	
  }
  else
  {
  	
    unsigned short nTerms=len/2;
    unsigned short total=0;
    unsigned short elem=0;  
    for(short i=0; i<nTerms; i++)
    {
      memcpy(&elem,&buffer[2*i],2);
      total+=elem; //here we implicitly get the modulo
      elem=0;
    }  
  checksum=65536-total;
  
  }
  return(checksum);
}


// *************VERIFY CHECKSUM **********************

unsigned short apxVerChecksum(char* inBuffer,int size)
{
	
  char buffer[size];
  memcpy(buffer, inBuffer, size);
  unsigned short len=sizeof(buffer);	
  unsigned short total=0;
  if(len%2>0)
  {
    printf("There is an error here");	
  }
  else
  {
  	
    unsigned short nTerms=len/2;
    unsigned short elem=0;  
    for(short i=0; i<nTerms; i++)
    {
      memcpy(&elem,&buffer[2*i],2);
      total+=elem; //here we implicitly get the modulo
      elem=0;
    }    
  }
  return(total); //this must be 0
}




/* ************************************
 * ********** APX_DATA STRUCT *********
 * ************************************/


void parseNavData(char* inBuffer, int size, apxNavData* msg)
{
  char buffer[size];
  memcpy(buffer, inBuffer, size);
  memset(&(msg->groupStart[0]),'\0',5);
  memcpy(&(msg->groupStart[0]),&buffer[0],4);  
  
  
  memcpy(&(msg->ID),&buffer[4],2);  
  memcpy(&(msg->byteCount),&buffer[6],2);
  memcpy(&(msg->time1),&buffer[8],8);
  memcpy(&(msg->time2),&buffer[16],8);
  memcpy(&(msg->distanceTag),&buffer[24],8);
  memcpy(&(msg->timeTypes),&buffer[32],1);
  memcpy(&(msg->distanceType),&buffer[33],1);
  memcpy(&(msg->latitude),&buffer[34],8);
  memcpy(&(msg->longitude),&buffer[42],8);
  memcpy(&(msg->altitude),&buffer[50],8);
  memcpy(&(msg->northVelocity),&buffer[58],4);
  memcpy(&(msg->eastVelocity),&buffer[62],4);
  memcpy(&(msg->downVelocity),&buffer[66],4);
  memcpy(&(msg->roll),&buffer[70],8);
  memcpy(&(msg->pitch),&buffer[78],8);
  memcpy(&(msg->heading),&buffer[86],8);
  memcpy(&(msg->wander),&buffer[94],8);
  memcpy(&(msg->trackAngle),&buffer[102],4);
  memcpy(&(msg->speed),&buffer[106],4);
  memcpy(&(msg->rollRate),&buffer[110],4);
  memcpy(&(msg->pitchRate),&buffer[114],4);
  memcpy(&(msg->yawRate),&buffer[118],4);
  memcpy(&(msg->longitudinalAcc),&buffer[122],4);
  memcpy(&(msg->transverseAcc),&buffer[126],4);
  memcpy(&(msg->downAcc),&buffer[130],4);
  memcpy(&(msg->alignStatus),&buffer[134],1);
  memcpy(&(msg->pad),&buffer[135],1);
  memcpy(&(msg->checksum),&buffer[136],2);
  memset(&(msg->groupEnd[0]),'\0',3);
  memcpy(&(msg->groupEnd[0]),&buffer[138],2);
 
}



void parsePrecData(char* inBuffer, int size, apxPrecData* msg)
{
  char buffer[size];
  memcpy(buffer, inBuffer, size);
  memset(&(msg->groupStart[0]),'\0',5);
  memcpy(&(msg->groupStart[0]),&buffer[0],4);  
  
  memcpy(&(msg->ID),&buffer[4],2);  
  memcpy(&(msg->byteCount),&buffer[6],2);
  memcpy(&(msg->time1),&buffer[8],8);
  memcpy(&(msg->time2),&buffer[16],8);
  memcpy(&(msg->distanceTag),&buffer[24],8);
  memcpy(&(msg->timeTypes),&buffer[32],1);
  memcpy(&(msg->distanceType),&buffer[33],1);
  memcpy(&(msg->northRMS),&buffer[34],4);
  memcpy(&(msg->eastRMS),&buffer[38],4);
  memcpy(&(msg->altitudeRMS),&buffer[42],4);
  memcpy(&(msg->northVelocityRMS),&buffer[46],4);
  memcpy(&(msg->eastVelocityRMS),&buffer[50],4);
  memcpy(&(msg->downVelocityRMS),&buffer[54],4);
  memcpy(&(msg->rollRMS),&buffer[58],4);
  memcpy(&(msg->pitchRMS),&buffer[62],4);
  memcpy(&(msg->headingRMS),&buffer[66],4);
  memcpy(&(msg->errorEllipsAxMajor),&buffer[70],4);
  memcpy(&(msg->errorEllipsAxMinor),&buffer[74],4);
  memcpy(&(msg->errorEllipsOrientation),&buffer[78],4);
  
  memcpy(&(msg->pad),&buffer[82],2);
  memcpy(&(msg->checksum),&buffer[84],2);
  memset(&(msg->groupEnd[0]),'\0',3);
  memcpy(&(msg->groupEnd[0]),&buffer[86],2);
 
}


/* *****************************************
 * ************* Navigation Mode ***********
 * *****************************************/




// ********* BUILD MESSAGE ***************

void buildNavModeMsg(apxNavModeMsg* msg, unsigned short transaction) //this will be made more flexible later on
{ 
  msg->transNumber=transaction;
}

// ******** SERIALIZE NAV MODE MESSAGE *******

void serialNavModeMsg(apxNavModeMsg* msg, char* inBuffer)
{ 
  char buffer[16];
  memcpy(&buffer[0],msg->msgStart,4);
  memcpy(&buffer[4],&(msg->ID),2);
  memcpy(&buffer[6],&(msg->byteCount),2);
  memcpy(&buffer[8],&(msg->transNumber),2);
  memcpy(&buffer[10],&(msg->navMode),1);\
  memcpy(&buffer[11],&(msg->pad),1);  	
  memcpy(&buffer[12],&(msg->checksum),2);  	 
  memcpy(&buffer[14],(msg->msgEnd),2);
  unsigned short checksum=apxCompChecksum(buffer,16);
  memcpy(&buffer[12],&checksum,2);  	   
  memcpy(inBuffer, buffer, 16);
}



/* *****************************************
 * ************** SELECT DATA GROUPS *******
 * *****************************************/




// ********* BUILD MESSAGE ***************

void buildSelGroupsMsg(apxSelGroupsMsg* msg, unsigned short transaction) //this will be made more flexible
{ 
  msg->transNumber=transaction;
  msg->numberOfGroups=1;
  msg->groupID=60;  //I set to 10 the max number of groups we can get. This can be changed in the future
  msg->dataRate=200;
  msg->pad=0;
  msg->byteCount=10+2*(msg->numberOfGroups);
}


// ******** SERIALIZE MESSAGE *******

void serialSelGroupsMsg(apxSelGroupsMsg* msg, char* inBuffer, int size)
{ 
  char buffer[size];
  memcpy(&buffer[0],msg->msgStart,4);
  memcpy(&buffer[4],&(msg->ID),2);
  memcpy(&buffer[6],&(msg->transNumber),2);
  memcpy(&buffer[8],&(msg->numberOfGroups),2);
  memcpy(&buffer[10],&(msg->groupID),2);
  memcpy(&buffer[12],&(msg->dataRate),2);
  memcpy(&buffer[14],&(msg->checksum),2);
  memcpy(&buffer[16],&(msg->pad),2);  	
  memcpy(&buffer[18],(msg->msgEnd),2);
  unsigned short checksum=apxCompChecksum(buffer,size);
  memcpy(&buffer[14],&checksum,2); 
  memcpy(inBuffer, buffer, 20);
}





/* *****************************************
 * ***** ACKNOWLEDGE MESSAGE ****************
 * *****************************************/
 
void parseAckMsg(char* inBuffer, int size, apxAckMsg* msg)
{
  char buffer[size];
  memcpy(&(buffer[0]), inBuffer, size);
  memset(&(msg->msgStart[0]),'\0',5);
  memcpy(&(msg->msgStart[0]),&buffer[0],2);  
  memcpy(&(msg->ID),&buffer[4],2);  
  memcpy(&(msg->byteCount),&buffer[6],2);
  memcpy(&(msg->transNumber),&buffer[8],2);
  memcpy(&(msg->recMsgID),&buffer[10],2);
  memcpy(&(msg->responseCode),&buffer[12],2);
  memcpy(&(msg->newParamStatus),&buffer[14],1);
  memcpy(&(msg->rejectedParamName),&buffer[15],32);
  memcpy(&(msg->pad),&buffer[47],1);
  memcpy(&(msg->checksum),&buffer[48],2);
  memset(&(msg->msgEnd[0]),'\0',3);
  memcpy(&(msg->msgEnd[0]),&buffer[50],2);  	
} 



/* *****************************************
 * *********** CONTROL MESSAGE  ************
 * *****************************************/






// ********* BUILD MESSAGE ***************

void buildControlMsg(apxControlMsg* msg, unsigned short transaction, unsigned short control) //this will be made more flexible later on
{ 
  msg->transNumber = transaction;
  msg->control = control;
}

// ******** SERIALIZE MESSAGE *******

void serialControlMsg(apxControlMsg* msg, char* inBuffer)
{ 
  char buffer[16];
  memcpy(&buffer[0],msg->msgStart,4);
  memcpy(&buffer[4],&(msg->ID),2);
  memcpy(&buffer[6],&(msg->byteCount),2);
  memcpy(&buffer[8],&(msg->transNumber),2);
  memcpy(&buffer[10],&(msg->control),2);
  memcpy(&buffer[12],&(msg->checksum),2);  	 
  memcpy(&buffer[14],(msg->msgEnd),2);
  unsigned short checksum=apxCompChecksum(buffer,16);
  memcpy(&buffer[12],&checksum,2);  	   
  memcpy(inBuffer, buffer, 16);
}




/* ****************************************************
*  ************* TCP-IP functions *********************
*  ****************************************************/

// **************** SEND ******************************

short apxTcpSend(int* socket, char* message, short sizeMsg)
{
  short totBytes = 0;            // total bytes we have sent
  short remBytes = sizeMsg;  // how many we have left to send
  short sentBytes = 0;           // how many bytes we have sent this round 
  short comStatus = 1;		   // status of the communication, -1 = failure
      
      //send the message: as a cycle in case of packet splitting
      
  while( (totBytes< remBytes) && (comStatus > 0) )
  {
    sentBytes = send(*socket, &message[totBytes], remBytes, 0);
    if (sentBytes < 1)  //0 = socket closed; -1 = transfer error;
    { 
      comStatus = 0; //error!!!
    } 
    else
    {
      totBytes += sentBytes;
      remBytes -= sentBytes;
    }
  }
  return(comStatus);
}


// **************** RECEIVE ******************

//I am assuming to receive the NAV group / PREC group /NAV+PREC group on a single message.
//We throw away everything is not in this combinations
//The assumption has been validated by looking at the real message size from POS-LV

short apxTcpReceive(int* socket, char* message, int* msgLength)
{       
    short comStatus = 1;                            
    short recBytes = 0;
    fd_set readfds;
	struct timeval tv;

	char buffer[APX_BUFFER_SIZE];
    FD_ZERO(&readfds);
	FD_SET(*socket,&readfds);
    tv.tv_sec=0;
	tv.tv_usec=TIMEOUT_PERIOD;
    int isReady=select(*socket+1,&readfds,NULL,NULL,&tv);
	int test = 0;
	if( FD_ISSET(*socket,&readfds))
	  test =1;
	if(isReady>0)
	{
        recBytes=recv(*socket, buffer, APX_BUFFER_SIZE, 0);
        if(recBytes <1)
		{
		  comStatus = 2;
		}
		else
		{
	        if( (recBytes != NAV_GRP_LENGTH) && (recBytes != PREC_GRP_LENGTH) && (recBytes != NAV_GRP_LENGTH + PREC_GRP_LENGTH)) //these are the combinations we are interested so far
            {
                comStatus = 0;     
            //   cout << "Received bytes: " << recBytes << endl; // use this to get infor about misformed packets
            }
            else
            {
                char isGroup[5]="$GRP";
                char isMine[5]="nnnn";       //initialization to have a null-terminated string...
                memcpy(&isMine[0],&buffer[0],4);
  
                if( strcmp(isMine,isGroup)!=0 )  //is not a group message
                {
                    comStatus = 0;           //this packet is out of synchrony
                }	     
	            else
	            {
                    *msgLength = recBytes;
	            }
            }	                  
    
            if(comStatus>0)
            {
               memcpy(message, buffer, recBytes);                //copy the temp buffer into message buffer
            }
        }
	}
	else
	{
	  comStatus = 2;
	}
    return(comStatus);
}



