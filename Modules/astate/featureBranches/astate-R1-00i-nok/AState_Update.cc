/* \file AState_Update.cc
 * \brief Astate class function that updates the state estimate
 * \author Stefano Di Cairano,
 * \date dec-06
 */

#include <math.h>

#include "AState.hh"
#include "sparrow/display.h"
#include "sparrow/flag.h"
#include "dgcutils/ggis.h"

#define FREQUENCY_DIVISOR 8
#define square(x) ((x)*(x))

GisCoordLatLon latsForVehState;
GisCoordUTM utmForVehState;


/*! Thread that updates the state estimate
 */
 
void AState::updateStateThread()
{
    unsigned long long stateTime = 0;
    unsigned long long replayStateLogStart = 0;
    unsigned long long replayDataTime = 0;
    unsigned long long nowTime = 0;
  
    unsigned long long apxTime = 0;
    rawApx apxEstimate;
    unsigned long long apxPrecisionTime = 0;
    rawPrec apxPrecision;
    
    int newState = 0;
    int newPrecision = 0;
    int count = 0;   //Counter that increments every loop (used to limit broadcast rate)
    int haveInitTime = 0;
    VehicleState inState; //used for replay as temp var
  
    while (!m_quitPressed)
    {  
        newState = 0;
        newPrecision = 0;
         
        if(m_options.stateReplay==1)
        {    
            m_replayStateStream.read((char*)&inState, sizeof(VehicleState));
      
            if(haveInitTime == 0) 
            {
                replayStateLogStart = inState.timestamp; //this is the initial instant of the logging
                haveInitTime = 1;
            }
            if(m_replayStateStream.eof()==1)
            {
                m_quitPressed=1;
            }

            replayDataTime = inState.timestamp - replayStateLogStart + m_startTime;
            DGCgettime(nowTime);
            while( replayDataTime > nowTime )
            {
                usleep(10);
                DGCgettime(nowTime);
            }
      	  
            m_vehicleState = inState;
            newState = 1;
        }
        else
        { 
            DGClockMutex(&m_ApxNavDataMutex);
            if (m_apxNavBufferReadInd > m_apxNavBufferLastInd)
            {       
                DGCSetConditionTrue(m_apxNavBufferFree);
               m_apxNavBufferLastInd++;
                int ind = m_apxNavBufferLastInd % APX_NAV_BUFFER_SIZE;
                apxEstimate=m_apxNavBuffer[ind];
                apxTime = m_apxNavBuffer[ind].time;

                if(stateTime < apxTime)
                {
                    stateTime = apxTime;
                }

                newState = 1;  //new state data avilable
                m_apxCount++;
            }
       
            DGCunlockMutex(&m_ApxNavDataMutex); 
         /*   if( (DEBUG_LEVEL>4) && (m_apxConnected == 0) ) //I this is for test when Applanix is not connected
            {
                DGCgettime(apxEstimate.time);
                m_apxCount++;
                apxEstimate.time = 100;
                apxEstimate.data.latitude = 100;
                apxEstimate.data.longitude = 100;
                apxEstimate.data.altitude = 100;
                apxEstimate.data.heading = 100;
                apxEstimate.data.northVelocity = 100;
                apxEstimate.data.eastVelocity = 100;
                apxEstimate.data.downVelocity = 100;
                apxEstimate.data.longitudinalAcc = 100;
                apxEstimate.data.transverseAcc = 100;
                apxEstimate.data.downAcc = 100;
                apxEstimate.data.roll = 100;
                apxEstimate.data.pitch = 100;
                apxEstimate.data.heading = 100;
                apxEstimate.data.rollRate = 100;
                apxEstimate.data.pitchRate = 100;
                apxEstimate.data.yawRate = 100;
                apxEstimate.data.alignStatus = NO_SOLUTION;
                newState=1;  //new state data available
		usleep(5000);
            }
     */
        }

        if (newState == 0)
        { 
            m_newNavData.bCond = false;
            DGCWaitForConditionTrue(m_newNavData); //we block here... not sure it is correct but state data are 200Hz
        }
        else
        {
	    DGClockMutex(&m_VehicleStateMutex);
            if(m_options.stateReplay==0)
            {
                latsForVehState.latitude = apxEstimate.data.latitude;
                latsForVehState.longitude = apxEstimate.data.longitude;
                gis_coord_latlon_to_utm(&latsForVehState, &utmForVehState, GIS_GEODETIC_MODEL_WGS_84); 
                m_vehicleState.timestamp = apxEstimate.time;
                m_vehicleState.utmZone = utmForVehState.zone;
                m_vehicleState.utmLetter = utmForVehState.letter;
                m_vehicleState.utmNorthing = utmForVehState.n; 
                m_vehicleState.utmEasting = utmForVehState.e;
                m_vehicleState.utmAltitude = -apxEstimate.data.altitude; //POS-LV gives positive altitude
                m_vehicleState.utmRoll = apxEstimate.data.roll*M_PI/180;
                m_vehicleState.utmPitch =  apxEstimate.data.pitch*M_PI/180;
                if(apxEstimate.data.heading < 180)
                    m_vehicleState.utmYaw =  (apxEstimate.data.heading)*M_PI/180;
                else
		    m_vehicleState.utmYaw =  (apxEstimate.data.heading)*M_PI/180-2*M_PI; //shifting according to the convention used
		m_vehicleState.utmNorthVel = apxEstimate.data.northVelocity;
                m_vehicleState.utmEastVel =apxEstimate.data.eastVelocity;
                m_vehicleState.utmAltitudeVel = apxEstimate.data.downVelocity; 
                m_vehicleState.utmRollRate = apxEstimate.data.rollRate*M_PI/180;
                m_vehicleState.utmPitchRate = apxEstimate.data.pitchRate*M_PI/180;
                m_vehicleState.utmYawRate = apxEstimate.data.yawRate*M_PI/180;

                 // Update local pose
                if ((!this->m_haveInitPose) &&
                       (m_vehicleState.utmEasting >0) && (m_vehicleState.utmNorthing > 0 ))
                {
                    this->m_haveInitPose = true;
                    this->m_initE = m_vehicleState.utmEasting;
                    this->m_initN = m_vehicleState.utmNorthing;
                }

                #warning "Using global pose for local pose"
                m_vehicleState.localX = m_vehicleState.utmNorthing - this->m_initN;
                m_vehicleState.localY = m_vehicleState.utmEasting - this->m_initE;
                m_vehicleState.localZ = m_vehicleState.utmAltitude;
                m_vehicleState.localRoll = m_vehicleState.utmRoll;
                m_vehicleState.localPitch = m_vehicleState.utmPitch;
                m_vehicleState.localYaw = m_vehicleState.utmYaw;
                m_vehicleState.localXVel = m_vehicleState.utmNorthVel;
                m_vehicleState.localYVel = m_vehicleState.utmEastVel;
                m_vehicleState.localZVel = m_vehicleState.utmAltitudeVel;
                m_vehicleState.localRollRate = m_vehicleState.utmRollRate;
                m_vehicleState.localPitchRate = m_vehicleState.utmPitchRate;
                m_vehicleState.localYawRate = m_vehicleState.utmYawRate;

                m_vehicleState.vehXVel =   cos(m_vehicleState.utmYaw) * m_vehicleState.utmNorthVel;
                m_vehicleState.vehXVel +=  sin(m_vehicleState.utmYaw) * m_vehicleState.utmEastVel;
                m_vehicleState.vehYVel =  -sin(m_vehicleState.utmYaw) * m_vehicleState.utmNorthVel;
                m_vehicleState.vehYVel +=  cos(m_vehicleState.utmYaw) * m_vehicleState.utmEastVel;
        
        #warning "Vehicle angular rates are not set"
                m_vehicleState.vehRollRate = 0;
                m_vehicleState.vehPitchRate = 0;
                m_vehicleState.vehYawRate = 0;
  	              
                m_stateStatus = apxEstimate.data.alignStatus;
            }
            else   //it is a replay
            {
      	        m_vehicleState=inState;
            } 
       
            DGCunlockMutex(&m_VehicleStateMutex);
      
            //update the precision, without locking

            DGClockMutex(&m_ApxPrecDataMutex);
            if (m_apxPrecBufferReadInd > m_apxPrecBufferLastInd)
            { 
                DGCSetConditionTrue(m_apxPrecBufferFree);
                m_apxPrecBufferLastInd++;
                int ind = m_apxPrecBufferLastInd % APX_PREC_BUFFER_SIZE;
                apxPrecision=m_apxPrecBuffer[ind];
		
		apxPrecisionTime = m_apxPrecBuffer[ind].time;

                if(stateTime < apxPrecisionTime)
                {
                    stateTime = apxPrecisionTime;
                }

                newPrecision = 1;
                m_apxPrecisionCount++;
            }
            DGCunlockMutex(&m_ApxPrecDataMutex); 
      
            if(newPrecision == 1)
            {
                DGClockMutex(&m_VehicleStateMutex);
                if(m_options.stateReplay==0)
                {
                    m_vehicleState.utmNorthConfidence = apxPrecision.data.northRMS;
                    m_vehicleState.utmEastConfidence = apxPrecision.data.eastRMS;
                    m_vehicleState.utmAltitudeConfidence = apxPrecision.data.altitudeRMS;
		    m_vehicleState.rollConfidence = apxPrecision.data.rollRMS;
		    m_vehicleState.pitchConfidence = apxPrecision.data.pitchRMS;
		    m_vehicleState.yawConfidence = apxPrecision.data.headingRMS;
                    m_vehiclePrecision.timestamp = apxPrecision.time;
                    m_vehiclePrecision.utmNorthConfidence = apxPrecision.data.northRMS;
                    m_vehiclePrecision.utmEastConfidence = apxPrecision.data.eastRMS;
                    m_vehiclePrecision.utmAltitudeConfidence = apxPrecision.data.altitudeRMS;
		    m_vehiclePrecision.rollConfidence = apxPrecision.data.rollRMS;
		    m_vehiclePrecision.pitchConfidence = apxPrecision.data.pitchRMS;
		    m_vehiclePrecision.yawConfidence = apxPrecision.data.headingRMS;
		    m_vehiclePrecision.velNorthRMS  = apxPrecision.data.northVelocityRMS ;
		    m_vehiclePrecision.velEastRMS = apxPrecision.data.eastVelocityRMS ;
		    m_vehiclePrecision.velDownRMS = apxPrecision.data.downVelocityRMS ;
		    m_vehiclePrecision.errEllipMajor = apxPrecision.data.errorEllipsAxMajor ;
		    m_vehiclePrecision.errEllipMinor = apxPrecision.data.errorEllipsAxMinor ;
		    m_vehiclePrecision.errEllipAngle = apxPrecision.data.errorEllipsOrientation ;
                    m_precisionTalker.send(&m_vehiclePrecision);
                }
       
                DGCunlockMutex(&m_VehicleStateMutex);
            }

            if(count++ % FREQUENCY_DIVISOR == 0) 
            {
                broadcast();
            }
      
            if (m_options.logRaw==1)
            {
                m_stateLogStream.write((char*)&m_vehicleState, sizeof(m_vehicleState));      
                if(m_apxNavBufferReadInd % 1300 == 1)
                {
                    m_apxLogStream.flush();
                }      	    
            }            
            m_spTime = (double) m_vehicleState.timestamp;
        } //end of the ELSE (update if got a newState)
   
    }//end of the main loop

}//end of the Thread


