/* \file AState.cc 
 * \brief Astate class definition
 * \author Stefano Di Cairano,
 * \date dec-06
 */
 
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>

#include <sys/types.h>   //these are the libraries needed for sockets
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include "AState.hh"
#include "dgcutils/DGCutils.hh"
#include "dgcutils/ggis.h"

#define HEALTH_MONITOR_PERIOD 100000
#define RECONNECTION_PERIOD 500000

/*! Astate Constructor. 
 * /param skynetKey: integer defining the skynet key to be used
 * /param options: astateOpts struct defining user options  */
 
AState::AState(int skynetKey, astateOpts options):
CSkynetContainer(SNastate, skynetKey),
m_precisionTalker(skynetKey,SNstatePrecision,SNastate),
m_healthTalker(skynetKey, SNastateHealth,SNastate)
{
 /*! \param snKey: integer, the skynet key  */
    m_snKey = skynetKey;
    m_options = options;
    m_quitPressed = 0;
    m_astateStatus = NOT_CONNECTED;
    m_apxCount = 0;
    m_apxDead = 1;
    m_stateStatus = NO_SOLUTION; 
    m_apxConnected = 0;
    m_spTime = 0;  
  
  //Log Files Setup
  
    if(m_options.logRaw == 1 )
    {
        cout << "Activating logging" << endl;	  
        time_t t = time(NULL);
        tm *local;
        local = localtime(&t);
  	
        mkdir("/tmp/logs/astate_raw/",
                S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH
                | S_IXOTH);
    
        sprintf(m_apxLogFile,
                "/tmp/logs/astate_raw/%04d%02d%02d_%02d%02d%02d_APX.raw",
                local->tm_year + 1900, local->tm_mon + 1, local->tm_mday,
                local->tm_hour, local->tm_min, local->tm_sec);

        sprintf(m_stateLogFile,
                "/tmp/logs/astate_raw/%04d%02d%02d_%02d%02d%02d_State.raw",
                local->tm_year + 1900, local->tm_mon + 1, local->tm_mday,
                local->tm_hour, local->tm_min, local->tm_sec);
    
        m_apxLogStream.open(m_apxLogFile, fstream::out | fstream::binary);
        m_stateLogStream.open(m_stateLogFile, fstream::out | fstream::binary);  
    }
    
    if(m_options.stateReplay == 1)
    {
  	    // name is hard-coded for now
  	    cout << "Ready for  state replay: wait 3 seconds..." << endl;
	    sleep(3);
  	    m_replayStateStream.open("/tmp/logs/astate_raw/stateLog.raw", fstream::in | fstream::binary);
        if(!m_replayStateStream)
        {
            cout << "error: replay file is missing" << endl;
            exit(1);
        }
  	    m_apxConnected = 0;
    }
    else
    {
        if(m_options.dataReplay == 1)
        {
  	    // name is hard-coded for now
  	        cout << "Ready for data replay: wait 3 seconds..." << endl;
	        sleep(3);
  	        m_replayDataStream.open("/tmp/logs/astate_raw/dataLog.raw", fstream::in | fstream::binary);
            if(!m_replayDataStream)
            {
                cout << "error: replay file is missing" << endl;
                exit(1);
            }
  	        m_apxConnected = 1; //this is to start the APX thread
        }
        else	
  	    {
            m_apxDataSocket=socket(PF_INET, SOCK_STREAM, 0); 	// create the data socket
            if(m_apxDataSocket<0)
            {
  	            cout << "ERROR while creating data socket" << endl;
	            exit(1);
            }
            struct sockaddr_in apxControl_addr, apxData_addr;					// these structs contains the addresses 
            apxData_addr.sin_addr.s_addr = inet_addr(APX_IP_ADDR);  
            apxData_addr.sin_family = AF_INET;
            apxData_addr.sin_port = htons(APX_DATA_PORT);  					
 	
            memset(&(apxControl_addr.sin_zero), '\0', 8);
            cout<<" Attempting a connection" << endl; 
            if (connect(m_apxDataSocket,(struct sockaddr *)&apxData_addr,sizeof(apxData_addr)) < 0)
            {  
                cerr << ("ERROR connecting to Applanix Data Port") << endl;
                cerr<< "Error: "<<strerror(errno) << endl;
                m_apxConnected = 0;
            }
            else 
            { 
                m_apxConnected = 1;
                cerr<< "Connected to Data Port" << endl; 
            }
        } 
    }    
  
    //Get starting time;
    DGCgettime(m_startTime);

    //Initialize EVERYTHING to be zeroe.
    memset(&m_vehicleState, 0, sizeof(m_vehicleState));
    m_vehicleState.timestamp = m_startTime;

    memset(&m_vehiclePrecision, 0, sizeof(m_vehiclePrecision));
    m_vehiclePrecision.timestamp = m_startTime;
    // We dont yet know the initial pose
    m_haveInitPose = false;

 
  
    DGCcreateMutex(&m_VehicleStateMutex);
    DGCcreateMutex(&m_ApxNavDataMutex);
    DGCcreateMutex(&m_healthMutex);
    DGCcreateMutex(&m_socketMutex);

	DGCcreateCondition(&m_apxReconnected);
    DGCcreateCondition(&m_newNavData);
    DGCcreateCondition(&m_apxNavBufferFree);

    DGCSetConditionTrue(m_apxNavBufferFree);
    DGCSetConditionFalse(m_apxReconnected);

    DGCcreateMutex(&m_ApxPrecDataMutex);
  
    //  DGCcreateCondition(&m_newPrecData);
    DGCcreateCondition(&m_apxPrecBufferFree);

    DGCSetConditionTrue(m_apxPrecBufferFree);
  
    //Set up socket for skynet broadcasting:
    m_broadcastStateSock = m_skynet.get_send_sock(SNstate);
    if (m_broadcastStateSock < 0)
    {
        cerr << "AState::AState(): skynet get_send_sock returned error" << endl;
        exit(1);
    }

    m_apxNavBufferLastInd = 0;
    m_apxNavBufferReadInd = 0;
  
    m_apxPrecBufferLastInd = 0;
    m_apxPrecBufferReadInd = 0;
    #warning forcing threads to start for testing
    if(m_apxConnected == 1 || true)
    {
        m_apxDead = 0;
        DGCstartMemberFunctionThread(this, &AState::apxReadThread); 
        DGCstartMemberFunctionThread(this, &AState::supervisoryThread);
    } 
    DGCstartMemberFunctionThread(this, &AState::updateStateThread);

    if(m_options.useSparrow)    
        DGCstartMemberFunctionThread(this, &AState::sparrowThread);
} 

/*! Astate destructor */
 
AState::~AState()
{
    if(m_options.logRaw)
    {
        m_apxLogStream.close();
        m_stateLogStream.close();
    }
    if(m_options.stateReplay);
    {
        m_replayStateStream.close();
    }
    if(m_options.dataReplay);
    {
        m_replayDataStream.close();
    }
    if(m_apxConnected)
    {
        close(m_apxDataSocket);
    }
} 


/*! Function that broadcasts VehicleState. */
void AState::broadcast()
{
    pthread_mutex_t *pMutex = &m_VehicleStateMutex;
    int c=m_skynet.send_msg(m_broadcastStateSock,
                        &m_vehicleState,
                        sizeof(VehicleState),
                        0, &pMutex);
    if (c!= sizeof(VehicleState))
    {
        cerr << "AState::Broadcast(): didn't send right size state message" << endl;
        sleep(1);
        exit(1);
    }
} 



/*! Astate main loop */

void AState::active()
{
    while (!m_quitPressed)
    { 
        sleep(1);
    }
}

void AState::supervisoryThread()
{  
    sleep(1); //give some time to the system to start
    bool apxRecovered;
    while(!m_quitPressed)
    { 
	    
        DGClockMutex(&m_healthMutex);
		if(m_apxDead == 1)
		{
	        m_apxConnected = 0;
	    }
		else
            m_apxConnected = 1;
	    m_astateStatus = m_stateStatus;
		if(m_apxDead)
		   m_astateStatus = NO_MESSAGES;
		if(m_apxConnected == 0)
		   m_astateStatus = NOT_CONNECTED;
        m_healthTalker.send(&m_astateStatus);
	    //cout << "m_apxDead: " << m_apxDead << endl;  
        if(m_apxDead == 1)
        {   
            DGCunlockMutex(&m_healthMutex);
	        apxRecovered = false;
            DGClockMutex(&m_socketMutex); 
            close(m_apxDataSocket);                             // closing the old socket as it may be pointing nowhere
            usleep(RECONNECTION_PERIOD);                                     // it seems that after closing the socket the POS-LV needs some time before open 
			                                                    // a new connection this time interval has been found empirically empirical
            m_apxDataSocket=socket(PF_INET, SOCK_STREAM, 0); 	// create the new socket
            if(m_apxDataSocket<0)
            {
  	            cerr << "ERROR while creating data socket" << endl;
	            exit(1);
            }

            struct sockaddr_in apxControl_addr, apxData_addr;					// these structs contains the addresses 
            apxData_addr.sin_addr.s_addr = inet_addr(APX_IP_ADDR);  
            apxData_addr.sin_family = AF_INET;
            apxData_addr.sin_port = htons(APX_DATA_PORT);  					
            memset(&(apxControl_addr.sin_zero), '\0', 8);
            cerr<<" Attempting to reconnect" << endl; 
            if (connect(m_apxDataSocket,(struct sockaddr *)&apxData_addr,sizeof(apxData_addr)) < 0)
            {  
                  // close(m_apxDataSocket);   //commented out because the socket is closed at the beginning of the next cycle
                  cerr << ("ERROR reconnecting to Applanix Data Port") << endl;
                  cerr<< "Error: "<<strerror(errno) << endl;
            }
            else 
            { 
				apxRecovered = true;
                cerr<< "Reconnected to Data Port" << endl; 
            }
            DGCunlockMutex(&m_socketMutex);
            if(apxRecovered)
            {
                DGClockMutex(&m_healthMutex);
                m_apxConnected = 1;
                m_apxDead = 0;                //this are the variable polled by the healt monitor
                DGCunlockMutex(&m_healthMutex);
                DGCSetConditionTrue(m_apxReconnected);
            }
        }  		
        else
        {   
            DGCunlockMutex(&m_healthMutex);
        }
        usleep(HEALTH_MONITOR_PERIOD);      //this leave time to a new message to be received. Max frequency 10Hz
    }	
}

