/* \file AState_Update.cc
 * \brief Astate class function that updates the state estimate
 * \author Stefano Di Cairano,
 * \date dec-06
 */

#include <math.h>

#include "AState.hh"
#include "sparrow/display.h"
#include "sparrow/flag.h"
#include "dgcutils/ggis.h"

#define FREQUENCY_DIVISOR 8
#define square(x) ((x)*(x))

extern unsigned short setStateMode(unsigned short);
GisCoordLatLon latsForVehState;
GisCoordUTM utmForVehState;


/*! Thread that updates the state estimate
 */
 
void AState::updateStateThread()
{
  unsigned long long stateTime = 0;
  unsigned long long replayStateLogStart = 0;
  unsigned long long replayDataTime = 0;
  unsigned long long nowTime = 0;
  
  unsigned long long apxTime = 0;
  rawApx apxEstimate;
  int newState = 0;
  int count = 0;   //Counter that increments every loop (used to limit broadcast rate)
  int haveInitTime = 0;
  VehicleState inState; //used for replay as temp var
  
  while (!quitPressed)
  {
    newState = 0;
         
    if(_options.stateReplay==1)
    {    
      replayStateStream.read((char*)&inState, sizeof(VehicleState));
      
      if(haveInitTime == 0) 
      {
        replayStateLogStart = inState.timestamp; //this is the initial instant of the logging
        haveInitTime = 1;
      }
      if(replayStateStream.eof()==1)
      {
        quitPressed=1;
      }

      replayDataTime = inState.timestamp - replayStateLogStart + startTime;
      DGCgettime(nowTime);
      while( replayDataTime > nowTime )
      {
        usleep(10);
        DGCgettime(nowTime);
      }
      	  
      _vehicleState = inState;
      newState = 1;
    }
    else
    { 
      DGClockMutex(&m_ApxDataMutex);
      if (apxBufferReadInd > apxBufferLastInd)
      { 
        
        DGCSetConditionTrue(apxBufferFree);
        apxBufferLastInd++;

        int ind = apxBufferLastInd % APX_DATA_BUFFER_SIZE;

        apxEstimate=apxBuffer[ind];

        apxTime = apxBuffer[ind].time;

        if(stateTime < apxTime)
        {
          stateTime = apxTime;
        }

        newState = 1;
        apxCount++;
      }
       
      DGCunlockMutex(&m_ApxDataMutex); 
      if( (DEBUG_LEVEL>4) && (apxActive == 0) ) //I this is for test when Applanix is not connected
      {
        DGCgettime(apxEstimate.time);
        apxCount++;
        apxEstimate.time = 100;
        apxEstimate.data.latitude = 100;
        apxEstimate.data.longitude = 100;
        apxEstimate.data.altitude = 100;
        apxEstimate.data.heading = 180.0001;
        apxEstimate.data.northVelocity = 100;
        apxEstimate.data.eastVelocity = 100;
        apxEstimate.data.downVelocity = 100;
        apxEstimate.data.longitudinalAcc = 100;
        apxEstimate.data.transverseAcc = 100;
        apxEstimate.data.downAcc = 100;
        apxEstimate.data.roll = 100;
        apxEstimate.data.pitch = 100;
        apxEstimate.data.rollRate = 100;
        apxEstimate.data.pitchRate = 100;
        apxEstimate.data.yawRate = 100;
        apxEstimate.data.alignStatus = NO_SOLUTION;
        newState=1;
	usleep(5000);
      }
     
    }

    if (newState == 0)
    { 
      newData.bCond = false;
      DGCWaitForConditionTrue(newData);
    }
    else
    { 
      DGClockMutex(&m_VehicleStateMutex);
      if(_options.stateReplay==0)
      {
        latsForVehState.latitude = apxEstimate.data.latitude;
        latsForVehState.longitude = apxEstimate.data.longitude;
        gis_coord_latlon_to_utm(&latsForVehState, &utmForVehState, GIS_GEODETIC_MODEL_WGS_84);
        
        _vehicleState.timestamp = apxEstimate.time;
        _vehicleState.utmZone = utmForVehState.zone;
        _vehicleState.utmLetter = utmForVehState.letter;
        _vehicleState.utmNorthing = utmForVehState.n; 
        _vehicleState.utmEasting = utmForVehState.e;
        _vehicleState.utmAltitude = -apxEstimate.data.altitude; //POS-LV gives positive altitude
        _vehicleState.utmRoll = apxEstimate.data.roll*M_PI/180;
        _vehicleState.utmPitch =  apxEstimate.data.pitch*M_PI/180;
       // _vehicleState.utmYaw =  apxEstimate.data.heading*M_PI/180;
        if(apxEstimate.data.heading < 180)
	    _vehicleState.utmYaw =  apxEstimate.data.heading*M_PI/180;
        else
	    _vehicleState.utmYaw =  apxEstimate.data.heading*M_PI/180-2*M_PI; //shifting according to the convention used
       _vehicleState.utmNorthVel = apxEstimate.data.northVelocity;
        _vehicleState.utmEastVel = apxEstimate.data.eastVelocity;
        _vehicleState.utmAltitudeVel = apxEstimate.data.downVelocity; 
        _vehicleState.utmRollRate = apxEstimate.data.rollRate*M_PI/180;
        _vehicleState.utmPitchRate = apxEstimate.data.pitchRate*M_PI/180;
        _vehicleState.utmYawRate = apxEstimate.data.yawRate*M_PI/180;

        //We don't get confidences yet
        _vehicleState.utmNorthConfidence = 1;
        _vehicleState.utmEastConfidence = 1;
        _vehicleState.utmAltitudeConfidence = 1;

        // REMOVE (useless)
        //_vehicleState.rollConfidence = 1;
        //_vehicleState.pitchConfidence = 1;
        //_vehicleState.yawConfidence = 1;

        // Update local pose
        if ((!this->haveInitPose) &&
            (_vehicleState.utmEasting >0) && (_vehicleState.utmNorthing > 0 ))
        {
          this->haveInitPose = true;
          this->initE = _vehicleState.utmEasting;
          this->initN = _vehicleState.utmNorthing;
        }

        #warning "Using global pose for local pose"
        _vehicleState.localX = _vehicleState.utmNorthing - this->initN;
        _vehicleState.localY = _vehicleState.utmEasting - this->initE;
        _vehicleState.localZ = _vehicleState.utmAltitude;
        _vehicleState.localRoll = _vehicleState.utmRoll;
        _vehicleState.localPitch = _vehicleState.utmPitch;
        _vehicleState.localYaw = _vehicleState.utmYaw;
        _vehicleState.localXVel = _vehicleState.utmNorthVel;
        _vehicleState.localYVel = _vehicleState.utmEastVel;
        _vehicleState.localZVel = _vehicleState.utmAltitudeVel;
        _vehicleState.localRollRate = _vehicleState.utmRollRate;
        _vehicleState.localPitchRate = _vehicleState.utmPitchRate;
        _vehicleState.localYawRate = _vehicleState.utmYawRate;

        _vehicleState.vehXVel =   cos(_vehicleState.utmYaw) * _vehicleState.utmNorthVel;
        _vehicleState.vehXVel +=  sin(_vehicleState.utmYaw) * _vehicleState.utmEastVel;
        _vehicleState.vehYVel =  -sin(_vehicleState.utmYaw) * _vehicleState.utmNorthVel;
        _vehicleState.vehYVel +=  cos(_vehicleState.utmYaw) * _vehicleState.utmEastVel;
        
        #warning "Vehicle angular rates are not set"
        _vehicleState.vehRollRate = 0;
        _vehicleState.vehPitchRate = 0;
        _vehicleState.vehYawRate = 0;
  	              
        apxStatus = apxEstimate.data.alignStatus;
        stateMode = setStateMode((unsigned short)apxEstimate.data.alignStatus);
      }
      else
      {
      	_vehicleState=inState;
      } 
       
      DGCunlockMutex(&m_VehicleStateMutex);
      
      if(count++ % FREQUENCY_DIVISOR == 0) 
      {
        broadcast();
      }
      
      if (_options.logRaw==1)
      {
        stateLogStream.write((char*)&_vehicleState, sizeof(_vehicleState));      
        if(apxBufferReadInd % 1300 == 1)
        {
          apxLogStream.flush();
        }      	    
      }            
      spTime = (double) _vehicleState.timestamp;\
     // cout << spTime <<endl;
    } //end of the ELSE (update if got a newState)
   
   //cout << "sparrow time: " << spTime << endl;
  // sleep(1);
  }//end of the main loop
}//end of the Thread


/*! Converts the apxStatus into AState statemode.
 * preinit stands for the initial solution
 * init stands for a large error (coarse leveling or heading error > 15 )
 * nav stands for for full navigation or heading error < 15 deg (fine alignement)
 * oh_shit stands for a missing initial solution/ system errors
 */
  
unsigned short setStateMode(unsigned short status)
{
  unsigned short mode = OH_SHIT;
  
  if(status<8)
  {
	mode = PREINIT;
  }
  
  if(status<7)
  {
	mode = INIT;
  }
  
  if(status<1)
  {
	mode = NAV;
  }
  
  return(mode);
}
