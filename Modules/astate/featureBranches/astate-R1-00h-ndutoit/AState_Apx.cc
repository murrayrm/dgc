/*! \file AState_APX.cc
 *  \brief AState class functions to interface with Applanix POS-LV.
 *  \author Stefano Di Cairano,
 *  \date dec-06
 */

#include "AState.hh"
#include "applanix.hh"

/*! Thread that reads from Applanix POS-LV
 */
 
void AState::apxReadThread()
{
    unsigned long long nowTime;
    unsigned long long rawApxTime;

    rawApx apxNavIn;
    rawPrec apxPrecIn;
    apxNavData navData;
    apxPrecData precData;
    
    //int readError = 0; not used!!
    char buffer[NAV_GRP_LENGTH + PREC_GRP_LENGTH];  //this is the max size we can correctly receive so far
    int haveInitTime = 0;
    unsigned long long replayDataTime;
    unsigned long long replayDataLogStart;
    bool newNavData = false;
    bool newPrecData = false;
  
    while (!m_quitPressed) 
    {  
        if(m_options.dataReplay == 1)
        {
            m_replayDataStream.read((char*)&apxNavIn, sizeof(rawApx));
      
            if(haveInitTime == 0)
            {
	        replayDataLogStart = apxNavIn.time; //this is the initial instant of the logging
                haveInitTime = 1;
            }
      
            if(m_replayDataStream.eof()==1)
            {
                m_quitPressed=1;
            }

            replayDataTime = apxNavIn.time - replayDataLogStart + m_startTime;
            DGCgettime(nowTime);
      
            while( replayDataTime > nowTime ) //replay timing
            {
                usleep(10);
	        DGCgettime(nowTime);
            }	
        }
        else // not a data replay
        {
            int comStatus = 0;
            memset(buffer,'\0',sizeof(buffer)); //reset the buffer
            int msgLength;

            while (comStatus < 1)  
            {   
                comStatus = apxTcpReceive(&m_apxDataSocket, buffer, &msgLength);
            }
      
            if(msgLength == NAV_GRP_LENGTH) //we have received a Navigation data message
            {
                parseNavData(buffer, NAV_GRP_LENGTH, &navData);
	        newNavData = true;
            }
      
            if(msgLength == PREC_GRP_LENGTH) //we have received a precision data message
            {  
                parsePrecData(buffer, PREC_GRP_LENGTH, &precData);
                newPrecData = true;
            }


            if(msgLength == (PREC_GRP_LENGTH + NAV_GRP_LENGTH) ) //we have received a Navigation data AND a precision data message
            {
                unsigned short msgID = 0;
		memcpy(&msgID,&buffer[4],2);                   //we need to check which one is the first

                if(msgID == NAV_GRP_ID)
		{
                    parseNavData(buffer, NAV_GRP_LENGTH, &navData);
	            newNavData = true;
                    parsePrecData(&buffer[NAV_GRP_LENGTH], PREC_GRP_LENGTH, &precData);
                    newPrecData = true;
	        }
                if(msgID == PREC_GRP_ID)
		{
                    parsePrecData(buffer, PREC_GRP_LENGTH, &precData);
                    newPrecData = true;
                    parseNavData(&buffer[PREC_GRP_LENGTH], NAV_GRP_LENGTH, &navData);
	            newNavData = true;
	        }
		
            }
      
            if (newNavData)
            {
                DGCgettime(rawApxTime); // time stamp as soon as data read.
                memcpy(&(apxNavIn.data), &navData, sizeof(apxNavData));
                apxNavIn.time = rawApxTime;

                if(m_options.logRaw==1)
                {
                    m_apxLogStream.write((char*)&apxNavIn, sizeof(rawApx));
                    if(m_apxNavBufferReadInd % 1500 == 1)
                    {
                        m_apxLogStream.flush();
                    }
                }
            
                DGClockMutex(&m_ApxNavDataMutex);
                if (((m_apxNavBufferReadInd + 1) % APX_NAV_BUFFER_SIZE) == (m_apxNavBufferLastInd % APX_NAV_BUFFER_SIZE))
                {
                    m_apxNavBufferFree.bCond = false;
                    DGCunlockMutex(&m_ApxNavDataMutex);
                    DGCWaitForConditionTrue(m_apxNavBufferFree);
                    DGClockMutex(&m_ApxNavDataMutex);
                }
                ++m_apxNavBufferReadInd;
                memcpy(&m_apxNavBuffer[m_apxNavBufferReadInd % APX_NAV_BUFFER_SIZE], &apxNavIn, sizeof(rawApx));
                newNavData = false;
		DGCunlockMutex(&m_ApxNavDataMutex);
                DGCSetConditionTrue(m_newNavData);
            }  // end of if (newNavData)

            if (newPrecData)
            {
                DGCgettime(rawApxTime); // time stamp as soon as data read.
                memcpy(&(apxPrecIn.data), &precData, sizeof(apxPrecData));
                newPrecData = false;
		apxPrecIn.time = rawApxTime;

              /*  if(m_options.logRaw==1)
                {
                    m_apxLogStream.write((char*)&apxNavIn, sizeof(rawApx));
                    if(m_apxNavBufferReadInd % 1500 == 1)
                    {
                        m_apxLogStream.flush();
                    }
                }*/
            
                DGClockMutex(&m_ApxPrecDataMutex);
                if (((m_apxPrecBufferReadInd + 1) % APX_PREC_BUFFER_SIZE) == (m_apxPrecBufferLastInd % APX_PREC_BUFFER_SIZE))
                {
                    m_apxPrecBufferFree.bCond = false;
                    DGCunlockMutex(&m_ApxPrecDataMutex);
                    DGCWaitForConditionTrue(m_apxPrecBufferFree);
                    DGClockMutex(&m_ApxPrecDataMutex);
                }
                ++m_apxPrecBufferReadInd;
                memcpy(&m_apxPrecBuffer[m_apxPrecBufferReadInd % APX_PREC_BUFFER_SIZE], &apxPrecIn, sizeof(rawPrec));
                DGCunlockMutex(&m_ApxPrecDataMutex);
               // DGCSetConditionTrue(m_newPrecData);
            }  // end of if (newNavData)
        } //end of else (not a replay)

    } // end of while loop	

}
