/* \file AState.cc 
 * \brief Astate class definition
 * \author Stefano Di Cairano,
 * \date dec-06
 */
 
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>

#include <sys/types.h>   //these are the libraries needed for sockets
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include "AState.hh"
// #include "SensorConstants.h"
#include "dgcutils/DGCutils.hh"
#include "dgcutils/ggis.h"

/*! Astate Constructor. 
 * /param skynetKey: integer defining the skynet key to be used
 * /param options: astateOpts struct defining user options
 */
 
AState::AState(int skynetKey, astateOpts options):
CSkynetContainer(SNastate, skynetKey),
m_precisionTalker(skynetKey,SNstatePrecision,SNastate)
{
 /*! \param snKey: integer, the skynet key
  */
  m_snKey = skynetKey;
 m_options = options;
  m_quitPressed = 0;


  m_apxCount = 0;
  m_apxDead = 1;
  m_apxStatus = NO_SOLUTION; // check the table
  m_timberEnabled = 0;
  m_apxEnabled = 1;
  m_apxActive = 1;
  m_spTime = 0;  
  
  //Log Files Setup
  
  if(m_options.logRaw == 1 )
  {
    cout << "Activating logging" << endl;	  
    time_t t = time(NULL);
    tm *local;
    local = localtime(&t);
  	
    mkdir("/tmp/logs/astate_raw/",
          S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH
          | S_IXOTH);
    
    sprintf(m_apxLogFile,
            "/tmp/logs/astate_raw/%04d%02d%02d_%02d%02d%02d_APX.raw",
            local->tm_year + 1900, local->tm_mon + 1, local->tm_mday,
            local->tm_hour, local->tm_min, local->tm_sec);

    sprintf(m_stateLogFile,
            "/tmp/logs/astate_raw/%04d%02d%02d_%02d%02d%02d_State.raw",
            local->tm_year + 1900, local->tm_mon + 1, local->tm_mday,
            local->tm_hour, local->tm_min, local->tm_sec);
    
    m_apxLogStream.open(m_apxLogFile, fstream::out | fstream::binary);
    m_stateLogStream.open(m_stateLogFile, fstream::out | fstream::binary);
    
  }
  
 /* 
  m_apxHeight = -1;
  m_apxNorth = -1;
  m_apxEast = -1;
  m_apxVelN = -1;
  m_apxVelE = -1;
  m_apxVelD = -1;
  m_apxAccN = -1;
  m_apxAccE = -1;
  m_apxAccD = -1;
  m_apxRoll = -1;
  m_apxPitch = -1;
  m_apxHeading = -1;
  m_apxRollRate = -1;
  m_apxPitchRate = -1;
  m_apxHeadingRate = -1;
  
  m_apxHeight_accuracy = -1;
  m_apxNorth_accuracy = -1;
  m_apxEast_accuracy = -1;
  m_apxVelN_accuracy = -1;
  m_apxVelE_accuracy = -1;
  m_apxVelD_accuracy = -1;
  m_apxAccN_accuracy = -1;
  m_apxAccE_accuracy = -1;
  m_apxAccD_accuracy = -1;
  m_apxRoll_accuracy = -1;
  m_apxPitch_accuracy = -1;
  m_apxHeading_accuracy = -1;
  m_apxRollRate_accuracy = -1;
  m_apxPitchRate_accuracy = -1;
  m_apxHeadingRate_accuracy = -1;
  */
  
  //Initialize the sockets
  if(m_options.stateReplay == 1)
  {
  	// name is hard-coded for now
  	cout << "Ready for  state replay: wait 3 seconds..." << endl;
	sleep(3);
  	m_replayStateStream.open("/tmp/logs/astate_raw/stateLog.raw", fstream::in | fstream::binary);
    if(!m_replayStateStream)
    {
      cout << "error: replay file is missing" << endl;
      exit(1);
    }
  	m_apxActive = 0;
  }
  else
  {
  	if(m_options.dataReplay == 1)
  	{
  	// name is hard-coded for now
  	  cout << "Ready for data replay: wait 3 seconds..." << endl;
	  sleep(3);
  	  m_replayDataStream.open("/tmp/logs/astate_raw/dataLog.raw", fstream::in | fstream::binary);
      if(!m_replayDataStream)
      {
        cout << "error: replay file is missing" << endl;
        exit(1);
      }
  	  m_apxActive = 1; //this is to start the APX thread
    }
    else	
  	{
      m_apxDataSocket=socket(PF_INET, SOCK_STREAM, 0); 	// create the data socket
      if(m_apxDataSocket<0)
      {
  	    cout << "ERROR while creating data socket" << endl;
	    exit(1);
      }
      struct sockaddr_in apxControl_addr, apxData_addr;					// these structs contains the addresses 
      apxData_addr.sin_addr.s_addr = inet_addr(APX_IP_ADDR);  
      apxData_addr.sin_family = AF_INET;
      apxData_addr.sin_port = htons(APX_DATA_PORT);  					
 	
      memset(&(apxControl_addr.sin_zero), '\0', 8);
      cout<<" Attempting a connection" << endl; 
      if (connect(m_apxDataSocket,(struct sockaddr *)&apxData_addr,sizeof(apxData_addr)) < 0)
      {  
        cout << ("ERROR connecting to Applanix Data Port") << endl;
        cout<< "Error: "<<strerror(errno) << endl;
        m_apxActive = 0;
      }
      else 
      { 
        m_apxActive = 1;
        cout<< "Connected to Data Port" << endl; 
      }
  	}
   
  }
  
  m_stateMode = PREINIT;
  //Get starting time;
  DGCgettime(m_startTime);

  //Initialize EVERYTHING to be zeroed for good measure.
  memset(&m_vehicleState, 0, sizeof(m_vehicleState));
  m_vehicleState.timestamp = m_startTime;

  memset(&m_vehiclePrecision, 0, sizeof(m_vehiclePrecision));
  m_vehiclePrecision.timestamp = m_startTime;
  // We dont yet know the initial pose
  m_haveInitPose = false;

  //Initialize metastate struct 

  //Create mutexes:
  DGCcreateMutex(&m_VehicleStateMutex);
  DGCcreateMutex(&m_ApxNavDataMutex);
  
  DGCcreateCondition(&m_newNavData);
  DGCcreateCondition(&m_apxNavBufferFree);

  DGCSetConditionTrue(m_apxNavBufferFree);
  
  DGCcreateMutex(&m_ApxPrecDataMutex);
  
//  DGCcreateCondition(&m_newPrecData);
  DGCcreateCondition(&m_apxPrecBufferFree);

  DGCSetConditionTrue(m_apxPrecBufferFree);
  
  //Set up socket for skynet broadcasting:
  m_broadcastStateSock = m_skynet.get_send_sock(SNstate);
  if (m_broadcastStateSock < 0)
  {
    cerr << "AState::AState(): skynet get_send_sock returned error" << endl;
  }


  m_apxNavBufferLastInd = 0;
  m_apxNavBufferReadInd = 0;
  
  m_apxPrecBufferLastInd = 0;
  m_apxPrecBufferReadInd = 0;
  if(m_apxActive == 1)
  {
    DGCstartMemberFunctionThread(this, &AState::apxReadThread); //this will be the Thread to read from apx
  } 
  DGCstartMemberFunctionThread(this, &AState::updateStateThread); //this will be the Thread to read from apx

  //Start sparrow threads
  if (m_options.useSparrow)
  {
    DGCstartMemberFunctionThread(this, &AState::sparrowThread);
  }
} // end AState::AState() constructor

/*! Astate destructor
 */
 
AState::~AState()
{
  if(m_options.logRaw)
  {
    m_apxLogStream.close();
    m_stateLogStream.close();
  }
  if(m_options.stateReplay);
  {
    m_replayStateStream.close();
  }
  if(m_options.dataReplay);
  {
    m_replayDataStream.close();
  }
  if(m_apxActive)
  {
    close(m_apxDataSocket);
  }
} // end AState::~AState() destructor


/*! Function that broadcasts VehicleState.
 */
void AState::broadcast()
{
  pthread_mutex_t *pMutex = &m_VehicleStateMutex;
  int c=m_skynet.send_msg(m_broadcastStateSock,
                        &m_vehicleState,
                        sizeof(VehicleState),
                        0, &pMutex);
  if (c!= sizeof(VehicleState))
  {
    cerr << "AState::Broadcast(): didn't send right size state message" << endl;
    sleep(1);
    exit(1);
  }
 
} // end AState::broadcast()



/*! Astate main loop
 */

void AState::active()
{
  while (!m_quitPressed)
  { 
    sleep(1);
  }
}
