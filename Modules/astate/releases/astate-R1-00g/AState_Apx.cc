/*! \file AState_APX.cc
 *  \brief AState class functions to interface with Applanix POS-LV.
 *  \author Stefano Di Cairano,
 *  \date dec-06
 */

#include "AState.hh"

/*! Thread that reads from Applanix POS-LV
 */
 
void AState::apxReadThread()
{
  unsigned long long nowTime;
  unsigned long long rawApxTime;

  rawApx apxIn;
  apxNavData navData;
  
  int readError = 0;
  char buffer[NAV_GRP_LENGTH];
  int haveInitTime = 0;
  unsigned long long replayDataTime;
  unsigned long long replayDataLogStart;
  
  while (!quitPressed) 
  { 
    if(_options.dataReplay == 1)
    {
      replayDataStream.read((char*)&apxIn, sizeof(rawApx));
      
      if(haveInitTime == 0)
      {
	    replayDataLogStart = apxIn.time; //this is the initial instant of the logging
        haveInitTime = 1;
      }
      if(replayDataStream.eof()==1)
      {
        quitPressed=1;
      }

      replayDataTime = apxIn.time - replayDataLogStart + startTime;
      DGCgettime(nowTime);
      while( replayDataTime > nowTime ) //replay timing
      {
        usleep(10);
	    DGCgettime(nowTime);
      }	
    }
    else // not a data replay
    {
      int comStatus = 0;
      memset(buffer,'\0',sizeof(buffer)); //reset the buffer
    
      while (comStatus < 1)  
      {   
        comStatus = apxTcpReceive(&apxDataSocket, buffer, sizeof(buffer));
      }
      parseNavData(buffer, NAV_GRP_LENGTH, &navData);
      DGCgettime(rawApxTime); // time stamp as soon as data read.
      memcpy(&(apxIn.data), &navData, sizeof(apxNavData));
      apxIn.time = rawApxTime;

      if(_options.logRaw==1)
      {
        apxLogStream.write((char*)&apxIn, sizeof(rawApx));
        if(apxBufferReadInd % 1500 == 1)
        {
          apxLogStream.flush();
        }
      }
    }
    DGClockMutex(&m_ApxDataMutex);
    if (((apxBufferReadInd + 1) % APX_DATA_BUFFER_SIZE) == (apxBufferLastInd % APX_DATA_BUFFER_SIZE))
    {
      apxBufferFree.bCond = false;
      DGCunlockMutex(&m_ApxDataMutex);
      DGCWaitForConditionTrue(apxBufferFree);
      DGClockMutex(&m_ApxDataMutex);
    }
    ++apxBufferReadInd;
    memcpy(&apxBuffer[apxBufferReadInd % APX_DATA_BUFFER_SIZE], &apxIn, sizeof(rawApx));
    DGCunlockMutex(&m_ApxDataMutex);
    DGCSetConditionTrue(newData);
  }   
}

