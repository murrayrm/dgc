/*
 Definition of the OCPTSPECS class that provides the interface
 between Tplanner and Dplanner

 Stefano Di Cairano mar-07
 */

#include "OCPtSpecs.hh"
#include "interfaces/TpDpInterface.hh"
#define MAX_DELTA_SIZE 1000000  //taken from plannerModule
#define center_N 0.0           //taken from map.default.dat
#define center_E 0.0


OCPtSpecs::OCPtSpecs(int sn_key, bool useAstate, int verbose, bool useScmap, bool useObst, bool useParams, bool useRDDF, bool usePolyCorridor)
    : CSkynetContainer(MODdynamicplanner, sn_key),
      CStateClient(false),
      CRDDFTalker(false),
      m_paramsTalker(sn_key,SNocpParams,MODdynamicplanner),
      m_statusTalker(sn_key,SNdplStatus,MODdynamicplanner),
      m_obstTalker(sn_key,SNocpObstacles,MODdynamicplanner),
      m_verbose(verbose)
{
        
    m_skynetKey = sn_key;
    cout << "Skynet key: " << sn_key << endl; 
    m_quit = 0;	
	//setting member vars that control threads
    m_useParams = useParams;
    m_useObstacles = useObst;
    m_useRDDF = useRDDF;
    m_useScmap = useScmap;
    m_usePolyCorridor =usePolyCorridor;

    DGCcreateMutex(&m_dplannerStatusMutex);
    DGCcreateCondition(&m_notPausedCond);  //condition on deltas reception
    DGCstartMemberFunctionThread(this, &OCPtSpecs::supervisoryThread); //start the thread to catch the map deltas 
	//create the mutex for specs data: only one mutex to have better data consistency
    DGCcreateMutex(&m_specsDataMutex);
    //initialize costmap and related mutexes and sockets
    m_useAstate = useAstate;
    if(m_useAstate)
    {
        cerr << "Warning: using State from AState" << endl;
    }
		
    //parameters
    if(m_useParams)
    {
        DGCstartMemberFunctionThread(this, &OCPtSpecs::getOCPparamsThread) ;
    }

    //obstacles
    if(m_useObstacles)
    {
        DGCcreateCondition(&m_obstRecvdCond);  //condition on deltas reception
        DGCstartMemberFunctionThread(this, &OCPtSpecs::getOCPobstThread) ;
    }
    
    //RDDF receiver
    if(m_useRDDF)
    {
        m_RDDFSocket =  m_skynet.listen(SNrddf,MODtrafficplanner); //put skynet interface here   
        DGCcreateCondition(&m_RDDFRecvdCond);  //condition on deltas reception
		DGCstartMemberFunctionThread(this, &OCPtSpecs::getRDDFThread) ;
    }
    
    if(m_usePolyCorridor)
    {
        DGCcreateCondition(&m_polyCorridorRecvdCond);  //condition on deltas reception
        DGCstartMemberFunctionThread(this, &OCPtSpecs::getPolyCorridorThread); //start the thread to catch the map deltas 
    }
    
	//DGCstartMemberFunctionThread(this, &OCPtSpecs::supervisoryThread);
    if(m_useScmap)
    {
        m_pScmapDelta = new char[MAX_DELTA_SIZE];   //this is the buffer for the deltas
        m_pStaticCostMap = new CMapPlus();
        m_pStaticCostMap->initMap(center_N, center_E, SCM_NROWS, SCM_NCOLS, SCM_RESROWS, SCM_RESCOLS, VERBOSITY); // initialize the map qith appropriate size
        m_scmapLayer = m_pStaticCostMap->addLayer<double>(SCM_NODATA,SCM_OUTSIDEMAP,SCM_USEDELTA); //add the staic cost layer

        DGCcreateCondition(&m_scmapDeltaRecvdCond);  //condition on deltas reception
	    m_mapCenter[0]=0;
		m_mapCenter[1]=0;
        m_scmapRequestSocket = m_skynet.get_send_sock(SNtplannerStaticCostMapRequest);  //put skynet interface here
        m_scmapDeltaSocket =  m_skynet.listen(SNtplannerStaticCostMap,MODtrafficplanner); //put skynet interface here   
        DGCstartMemberFunctionThread(this, &OCPtSpecs::getScmapDeltasThread); //start the thread to catch the map deltas 
	    scmapRequest(); // send a request for the full map    
    }
}

void OCPtSpecs::scmapRequest()
{
    bool bRequestScmap = true;
    m_skynet.send_msg(m_scmapRequestSocket,&bRequestScmap, sizeof(bool) , 0);
}

void OCPtSpecs::getScmapDeltasThread()
{
    while(!m_quit)
    {
        int deltasize;		
        if (m_verbose) cout << "Waiting for SCM Deltas " << endl;
        if(m_useAstate)
        {
            m_pStaticCostMap->updateVehicleLoc(m_state.localX, m_state.localY);
            m_mapCenter[0] = m_state.localX;
            m_mapCenter[1] = m_state.localY;
        }
        else
        {
            DGClockMutex(&m_specsDataMutex);
            m_pStaticCostMap->updateVehicleLoc(m_OCPparams.initialConditionLB[NORTHING_IDX_C], m_OCPparams.initialConditionLB[EASTING_IDX_C]);
            m_mapCenter[0] = m_OCPparams.initialConditionLB[EASTING_IDX_C];
            m_mapCenter[1] = m_OCPparams.initialConditionLB[NORTHING_IDX_C];
			DGCunlockMutex(&m_specsDataMutex); 
        }

        deltasize = m_skynet.get_msg(m_scmapDeltaSocket, m_pScmapDelta, MAX_DELTA_SIZE, 0);
        if(deltasize>0)
        {
            DGClockMutex(&m_specsDataMutex);
	    if (m_verbose) cout << "Applying SCM Deltas " << endl;
            m_pStaticCostMap->applyDelta<double>(m_scmapLayer, m_pScmapDelta, deltasize);	
		  
        if(!m_scmapDeltaRecvdCond.bCond)
        {
            DGCSetConditionTrue(m_scmapDeltaRecvdCond);
        }
		   DGCunlockMutex(&m_specsDataMutex); 
        }
	}
}


void OCPtSpecs::getOCPparamsThread()
{
    bool paramsCorrect;
    OCPparams tempParams;
	while(!m_quit)
	{	
		if (m_verbose) 
		    cout << "Waiting for Params" << endl;
        paramsCorrect = false;
        paramsCorrect = m_paramsTalker.receive(&tempParams);
		if(! paramsCorrect)
		{
		    cerr << "error in Parameters message" << endl;
        }
		else
		{
		    DGClockMutex(&m_specsDataMutex);
		    m_OCPparams = tempParams;
		    if(m_verbose) 
		        cout << "I am applying params. g: " << m_OCPparams.parameters[11] <<endl;     
		    if(!m_paramsRecvdCond.bCond)
			{
		       DGCSetConditionTrue(m_paramsRecvdCond);   
		       // DGCSetConditionTrue(m_paramsRecvdOne, m_paramsRecvdCond, m_paramsRecvdMutex);   
            }
			DGCunlockMutex(&m_specsDataMutex);
		}
	}
}

void OCPtSpecs::getRDDFThread()
{
    bool RDDFcorrect;
    RDDF tempRDDF;
    while(!m_quit)
	{
		// Reset the tempRDDF so that we always load full RDDF
        tempRDDF.clear();

	    // Now read in the data from the socket
        RDDFcorrect = RecvRDDF(m_RDDFSocket, &tempRDDF);

	    if (m_verbose > 1)
	        cout << "OCPtSpecs::getRDDFThread received RDDF" << endl;
	
	    if(!RDDFcorrect)
	    {
	        cerr <<"Error while recv RDDF" << endl;
	    }
	    else
	    {
            DGClockMutex(&m_specsDataMutex);
	        m_RDDF = tempRDDF;
	        if (m_verbose > 1)
	          cout << "OCPtSpecs::getRDDFThread RDDF object copied"<< endl;
	        if (m_verbose > 2) tempRDDF.print();
	        DGCunlockMutex(&m_specsDataMutex);
        }
        if(!m_RDDFRecvdCond.bCond)
        {
            DGCSetConditionTrue( m_RDDFRecvdCond);
	    }   				
    }
}

void OCPtSpecs::getOCPobstThread()
{
    bool obstCorrect;
    OCPobstacles tempObst;
	while(!m_quit)
	{		
	    if(m_verbose) 
	        cout << "Waiting for obstacles" << endl;
        obstCorrect = false;
        obstCorrect = m_obstTalker.receive(&tempObst);
		if(!obstCorrect)
		{
		    cerr << "OCPtSpecs:getOCPobstThread error" << endl;
        }
		else
		{
		    if (m_verbose) 
		        cout << "Applying obstacles: " << tempObst.ObstNumber <<  endl;
                    
		    //DGClockMutex(&m_obstMutex);
            DGClockMutex(&m_specsDataMutex);
            m_Qconstr.Nconstr=tempObst.ObstNumber;            
            // Obstacles are sent by mapper as (x-xc)H(x_xc)<=1
            // We store them as   xHx+Sx+Kx <= 1
            // Hence here we compute the H, S, K components and store them
	        for(int i=0;i<tempObst.ObstNumber;i++)
		    {
                m_Qconstr.S[i][0] = -2*( tempObst.ObstHessian[i][0][0]*tempObst.ObstCenter[i][0] + 
			                tempObst.ObstHessian[i][0][1]*tempObst.ObstCenter[i][1]) ;
	            m_Qconstr.S[i][1] = -2*( tempObst.ObstHessian[i][1][0]*tempObst.ObstCenter[i][0] + 
			                tempObst.ObstHessian[i][1][1]*tempObst.ObstCenter[i][1]) ;
                m_Qconstr.H[i][0][0] = tempObst.ObstHessian[i][0][0];
                            m_Qconstr.H[i][0][1] = tempObst.ObstHessian[i][0][1]; 
	            m_Qconstr.H[i][1][1] = tempObst.ObstHessian[i][1][1]; 
	            m_Qconstr.H[i][1][0] = tempObst.ObstHessian[i][1][0];
                m_Qconstr.K[i] = tempObst.ObstCenter[i][0] * tempObst.ObstHessian[i][0][0] * tempObst.ObstCenter[i][0]  +
			                2 * tempObst.ObstCenter[i][0] * tempObst.ObstHessian[i][0][1] * tempObst.ObstCenter[i][1] +
						    tempObst.ObstCenter[i][1] * tempObst.ObstHessian[i][1][1] * tempObst.ObstCenter[i][1];                         
		    }
            //zeroing the unapplied constraints -> this is not necessary, but for now ....
	        for(int i=tempObst.ObstNumber;i<MAX_OBSTACLES;i++)
	        {
                m_Qconstr.S[i][0] = 0;
	            m_Qconstr.S[i][1] = 0;
                m_Qconstr.H[i][0][0] = 0;
                m_Qconstr.H[i][0][1] = 0; 
	            m_Qconstr.H[i][1][1] = 0; 
	            m_Qconstr.H[i][1][0] = 0;
                m_Qconstr.K[i] = 0;
	        }
		    
		    if(!m_obstRecvdCond.bCond)
		    {    DGCSetConditionTrue( m_obstRecvdCond);  
            }//DGCunlockMutex(&m_obstMutex);
            DGCunlockMutex(&m_specsDataMutex);
        }
    }
}


OCPtSpecs::~OCPtSpecs(void)
{
	//check: I don't think this is enough!
    m_quit = 1;
	usleep(100000);
	delete[] m_pScmapDelta ;  
    delete m_pStaticCostMap ;
	DGCdeleteMutex(&m_specsDataMutex);
	DGCdeleteCondition(&m_paramsRecvdCond);
	DGCdeleteCondition(&m_obstRecvdCond);
	DGCdeleteCondition(&m_scmapDeltaRecvdCond);
	DGCdeleteCondition(&m_RDDFRecvdCond);
	DGCdeleteCondition(&m_polyCorridorRecvdCond);
//	~m_RDDFTalker();
 //   ~m_paramsTalker();
 //   ~m_obstTalker();
//	~m_scmapDeltaTalker();
}

void OCPtSpecs::getRDDFcorridor(RDDF* pNewRDDF)
{
    *pNewRDDF =  m_RDDF;
    return ;
}

void OCPtSpecs::getOCPComponentSizes(OCPComponentSizes* const ocpComponentSizes)
{
	//this are fixed for now, will be stored in a class member in the future
    ocpComponentSizes->IsActive             = true;
    ocpComponentSizes->HasICostFunction     = false;
    ocpComponentSizes->HasTCostFunction     = true;
    ocpComponentSizes->HasFCostFunction     = false;
    ocpComponentSizes->NofILinConstraints   = 0;
    ocpComponentSizes->NofTLinConstraints   = 0;
    ocpComponentSizes->NofFLinConstraints   = 0;
    ocpComponentSizes->NofINLinConstraints  = 0;
    ocpComponentSizes->NofTNLinConstraints  = m_Qconstr.Nconstr;
    ocpComponentSizes->NofFNLinConstraints  = 0;
}


void OCPtSpecs::InitialCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Fi, double* const DFi)
{
    // no initial cost function for the moment
    return ; 
}

void OCPtSpecs::TrajectoryCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ft, double* const DFt)
{
    if(*mode == 0 || *mode == 2)
    {   
       *Ft = m_pStaticCostMap->getDataUTM<double>(m_scmapLayer, z2, z1);
	   //cout << "Cost at (" << z1 << "," <<z2 << ") : "  <<*Ft <<endl;
    }

    if(*mode == 1 || *mode == 2)
    {
        for(m_i=0;m_i<NZ;m_i++)  //initialize the df/dz-vector to 0 
        {
    	    DFt[m_i] = 0;
        }
        double grad[2];
        m_pStaticCostMap->getUTMGradient(m_scmapLayer, z2 , z1, grad);
        DFt[I_z1]=grad[0];
        DFt[I_z2]=grad[1];
     }  
       
 /*  sparsity:  not implemented yet  
     
     if(*mode == 99)
     {
 
     }
 */
}

void OCPtSpecs::FinalCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ff, double* const DFf)
{
    return ;  //not used yet
}

void OCPtSpecs::getLinearConstraints(const ConstraintType* const ctype, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A, const int* const Offset)
{
/*	
	if(*ctype == ct_LinInitial)
	{
        for(m_i=0; m_i<NofLConstraints; m_i++)
        {
            for(m_j=0; m_j<NofVariables; m_j++)
            {
                A[m_i][m_j] = 0.0;
            }
        }

 	}	
	else if(*ctype == ct_LinTrajectory)
	{
          
	}
	else if(*ctype == ct_LinFinal)
	{
         
	}	
*/
    return ;  //not used yet -> maybe we should use it for final constraints

}

void OCPtSpecs::InitialNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli, const int* const Offset) 
{
    return ;  //not used yet
}

void OCPtSpecs::TrajectoryNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt, const int* const Offset)
{
    double tempX[2]; //temporary position vector
    tempX[0]=z1;
    tempX[1]=z2;
    for(m_i=0;m_i<*NofConstraints; m_i++)
    {
        for(m_j=0;m_j<*NofVariables; m_j++)
        {
            DCnlt[m_i+*Offset][m_j]=0;  //initialize the jacobian to 0
        }
        Cnlt[*Offset+m_i]=0;  //this should not be necessary, because the value of each constraint is computed.
    }

     //Value
    if(*mode == 0 | *mode == 2)
    {
        for(m_i=0;m_i<*NofConstraints;m_i++)
        {
  	        Cnlt[*Offset+m_i] = tempX[0]*m_Qconstr.H[m_i][0][0]*tempX[0]+ 2 * tempX[0]*m_Qconstr.H[m_i][0][1]*tempX[1] + 
	                          tempX[1]*m_Qconstr.H[m_i][1][1]*tempX[1] + m_Qconstr.S[m_i][0]*tempX[0] + m_Qconstr.S[m_i][1]*tempX[1];
        }
    } 
    
    //Jacobian
    if(*mode == 1 | *mode == 2)
    {
        for(m_i=0;m_i<*NofConstraints;m_i++)
        {
            DCnlt[m_i+*Offset][I_z1]=m_Qconstr.S[m_i][0]+2*(tempX[0]*m_Qconstr.H[m_i][0][0]+tempX[1]*m_Qconstr.H[m_i][1][0]) ;
	        DCnlt[m_i+*Offset][I_z2]=m_Qconstr.S[m_i][1]+2*(tempX[0]*m_Qconstr.H[m_i][0][1]+tempX[1]*m_Qconstr.H[m_i][1][1]) ;
        }
    }
   
   //compute the Sparsity
    if(*mode == 99)
    {   
        for(m_i=0;m_i<*NofConstraints;m_i++)
        {   
            DCnlt[m_i+*Offset][I_z1]= 1;
            DCnlt[m_i+*Offset][I_z2]= 1;  
        }
    }
   
    return ;
}

/*! use to implement the nonlinear constraints applicable at the end of the time interval.*/
void OCPtSpecs::FinalNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf, const int* const Offset)
{
    return ;  //not used yet
}

/*! Use to implement the bounds for all the constraints: linear and nonlinear.*/
void OCPtSpecs::getLUBounds(const ConstraintType* const ctype, const BoundsType* const ubtype, const int* const i, const double* const t, const int* const NofConstraints, double* const LUBounds, const int* const Offset)
{
    if(*ctype == ct_NLinTrajectory)
	{  
	    double temp =0;
	    	//we assume quadratic constraints in the form xQx+Sx+K >= 1
	        for(m_j=0; m_j<(*NofConstraints); m_j++)
	        {
	            if( *ubtype == ubt_LowerBound )   //lower bound is 1-K
		            temp = 1 - m_Qconstr.K[m_j];    		
		        else
		        temp = DBL_MAX;                  //upper bound is \infty
	            LUBounds[*Offset+(*i)*(*NofConstraints)+m_j]=temp;
	        } 
	}
    return ;
}
    
    
void  OCPtSpecs::getInitialCondition(const BoundsType* const ubtype, double* const initCond)
{
    bool valuecheck = false;
    if( *ubtype == ubt_LowerBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          initCond[m_i]=m_OCPparams.initialConditionLB[m_i];	  
        }
	initCond[HEADING_IDX_C]=M_PI/2-initCond[HEADING_IDX_C];
	if( initCond[HEADING_IDX_C]> M_PI)
	   initCond[HEADING_IDX_C]=initCond[HEADING_IDX_C]-2*M_PI;
	valuecheck = true;
    }
    
    if( *ubtype == ubt_UpperBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          initCond[m_i]=m_OCPparams.initialConditionUB[m_i];	  
        }
	initCond[HEADING_IDX_C]=M_PI/2-initCond[HEADING_IDX_C];
	if( initCond[HEADING_IDX_C]> M_PI)
	   initCond[HEADING_IDX_C]=initCond[HEADING_IDX_C]-2*M_PI;
	valuecheck = true;
    }
    if(valuecheck==false)
    {
        cout << "wrong value for initial bounds type" <<endl;
        exit(1);
    }

    return ;
}

void  OCPtSpecs::getFinalCondition(const BoundsType* const ubtype, double* const finalCond)
{
    bool valuecheck = false;
    if( *ubtype == ubt_LowerBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          finalCond[m_i]=m_OCPparams.finalConditionLB[m_i];	  
        }
	finalCond[HEADING_IDX_C]=M_PI/2-finalCond[HEADING_IDX_C];
	if( finalCond[HEADING_IDX_C]> M_PI)
	   finalCond[HEADING_IDX_C]=finalCond[HEADING_IDX_C]-2*M_PI;
	valuecheck = true;
    }
    
    if( *ubtype == ubt_UpperBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          finalCond[m_i]=m_OCPparams.finalConditionUB[m_i];	  
        }
	finalCond[HEADING_IDX_C]=M_PI/2-finalCond[HEADING_IDX_C];
	if( finalCond[HEADING_IDX_C]> M_PI)
	   finalCond[HEADING_IDX_C]=finalCond[HEADING_IDX_C]-2*M_PI;
	valuecheck = true;
    }
    if(valuecheck==false)
    {
        cout << "wrong value for final bounds type" <<endl;
        exit(1);
    }

    return ;
}



void  OCPtSpecs::getParams(double* const params)
{


    for(m_i=0;m_i<PARAMS_NUMBER;m_i++)
    {
        params[m_i]=m_OCPparams.parameters[m_i];	  
    }
    params[12] = m_OCPparams.initialConditionLB[VELOCITY_IDX_C];
    params[13] = m_OCPparams.finalConditionLB[VELOCITY_IDX_C];
    params[14] = m_OCPparams.initialConditionLB[ACCELERATION_IDX_C];
    params[15] = m_OCPparams.finalConditionLB[ACCELERATION_IDX_C];
   
    return ;
}

void  OCPtSpecs::getMode(int* mode)
{     
    *mode = m_OCPparams.mode; 
    return ;
}

void OCPtSpecs::startSolve()
{
    if(m_verbose)
        cout << "Request to solve. RDDF: "<< m_useRDDF << " Obst: " << m_useObstacles<< " Map: "<< m_useScmap << " Params: " << m_useParams <<endl;
	
    if(m_useRDDF)
        DGCWaitForConditionTrue(m_RDDFRecvdCond);
    
    if(m_useParams)
        DGCWaitForConditionTrue(m_paramsRecvdCond);
    if(m_useObstacles)
        DGCWaitForConditionTrue( m_obstRecvdCond);
    
   // if(m_useScmap)
     //   DGCWaitForConditionTrue(m_scmapDeltaRecvdCond);
    

    DGClockMutex(&m_specsDataMutex);    
    if(m_verbose)
        cout << "Ready to solve."<< endl;    
    return ;
}

void OCPtSpecs::endSolve()
{

    
   // bool result = m_statusTalker.send(&m_dplannerStatus);    
    DGCunlockMutex(&m_specsDataMutex);    
    
    if(m_verbose)
        cout << "Finished solving" << endl; 
    return ;
}

int OCPtSpecs::setStatus(int index, int value)
{
    if(index<= 0 || index >= N_STATUS)
        return 0;
    else
    {
        switch (index)
	{
	    case 1 : 
	        m_dplannerStatus.OCPsolved = (bool)value;
		break;
	    case 2 : 
	        m_dplannerStatus.solverFlag = value;
		break;
	    case 3 : 
	        m_dplannerStatus.corridorGenerated = (bool)value;
		break;
	    case 4 : 
	        m_dplannerStatus.corridorFlag = value;
	        break;
	    default : 
	        return 0;

	 }  	      

        return 1;
    }

}


void OCPtSpecs::printPolyCorridor()
{
   cout << endl<< endl << endl <<  "Printing corridor information " << endl << endl << endl; 
   cout << "Number of polytopes: " << m_nPolytopes << endl; 
   for(int i=0;i<m_nPolytopes;i++)
      cout <<&m_polyCorridor[i] << endl;

}
void OCPtSpecs::supervisoryThread()
{
    SkynetTalker<bool> supervisoryTalker= SkynetTalker<bool>(m_skynetKey,SNtrajPause,MODdynamicplanner);
    bool supervisoryMsg;
	while(!m_quit)
	{
        //receive pause message
        supervisoryTalker.receive(&supervisoryMsg);
		//the following lines set the conditions to false. Hence before to start a new optimization we wait for a new set of specs
		if(supervisoryMsg)
		{
		    DGClockMutex(&m_dplannerStatusMutex);
            //replying to tplanner
            #warning this is a temporary hack. We will move to the real CSS soon, I promise!
            m_dplannerStatus.OCPsolved = false;
			m_dplannerStatus.solverFlag = -1;
            m_statusTalker.send(&m_dplannerStatus);    
		    DGCSetConditionFalse(m_notPausedCond);
			DGClockMutex(&m_dplannerStatusMutex);
			//propagating the message to trajfollow
			// to be done
             #warning we may have consistency problem if we don't check the status before sending out a new trajectory.
            //blocking the computation  
			DGClockMutex(&m_specsDataMutex);
            DGCSetConditionFalse( m_scmapDeltaRecvdCond);
            DGCSetConditionFalse(m_paramsRecvdCond);
            DGCSetConditionFalse(m_obstRecvdCond);
            DGCSetConditionFalse( m_scmapDeltaRecvdCond);
		    DGCunlockMutex(&m_specsDataMutex);

	    }
    }
}
void OCPtSpecs::getPolyCorridorThread()
{
    
    CAlgebraicGeometry algGeom = CAlgebraicGeometry();
    double** vertices;
	int NofDim = 2;
    SkynetTalker<sendCorr> recCorTalker= SkynetTalker<sendCorr>(m_skynetKey, SNpolyCorridor,MODdynamicplanner);
	sendCorr corridor;
    while(!m_quit)
	{
	
        recCorTalker.receive(&corridor);
		DGClockMutex(&m_specsDataMutex);
        m_nPolytopes = corridor.size();
        delete[] m_polyCorridor;
        m_polyCorridor = new CPolytope[m_nPolytopes];
        for(int i=0; i<m_nPolytopes; i++)
        {
            int polySize = corridor[i].size();
	        vertices = new double*[NofDim];
	        for(int j=0;j<NofDim;j++)
                vertices[j] = new double[polySize];
            for(int j=0;j<polySize;j++)
		    {
                vertices[0][j] = corridor[i][j].x ;
                vertices[1][j] = corridor[i][j].y ;
            }    
        
            m_polyCorridor[i] = new CPolytope(&NofDim, &polySize, (const double**) vertices);
            algGeom.FacetEnumeration(&m_polyCorridor[i]);
            algGeom.VertexEnumeration(&m_polyCorridor[i]); //vertex reduction
            
	        for(int j=0;j<NofDim;j++)
                delete[]  vertices[j];
            delete vertices;		   
        }
        if(!m_polyCorridorRecvdCond.bCond)
        {
            DGCSetConditionTrue(m_polyCorridorRecvdCond);
        }
		DGCunlockMutex(&m_specsDataMutex);
    }
}

void OCPtSpecs::getSurfaceControlPoints(int windowSize,TimeInterval* xInterval, TimeInterval* yInterval, double** controlPoints)
{
   // int Nrows = m_pStaticCostMap->getNumRows();
//	int Ncols = m_pStaticCostMap->getNumCols();
	double resRows = m_pStaticCostMap->getResRows();
	double resCols = m_pStaticCostMap->getResCols();
   // cout << "Res: " << resRows << " " << resCols << endl;
//	cout<< "Center" << m_mapCenter[0] << m_mapCenter[1] << endl;
	int modWin = windowSize % 2;
    int halfWindow = (int)((windowSize - modWin) / 2) ;
    int initIndex = -halfWindow + (1-modWin) ; 
	int finalIndex = halfWindow;
/*	yInterval->t0 =  m_mapCenter[1]+initIndex*resRows; 
    yInterval->tf = m_mapCenter[1]+finalIndex*resRows; 
    xInterval->t0 = m_mapCenter[0]+initIndex*resCols; 
    xInterval->tf = m_mapCenter[0]+finalIndex*resCols; 
    cout << "X interval: [" << xInterval->t0 <<"," <<  xInterval->tf<<  "]     Y interval: [" << yInterval->t0 <<"," <<  yInterval->tf<<"]"<< endl;
    sleep(1); 
	for(int i=initIndex; i<=finalIndex; i++ )
	{
	   for(int j = initIndex; j<=finalIndex; j++)
	   {
	       controlPoints[i-initIndex][j-initIndex]=m_pStaticCostMap->getDataUTM<double>(m_scmapLayer,m_mapCenter[1]+resRows*j, m_mapCenter[0]+resCols*i); // i is the index on the X axis (easting) and j is the index on the Y axis (northing)
            cout << fixed << setprecision(2) <<  "  N,E : (" << m_mapCenter[1]+resRows*j<<"," <<   m_mapCenter[0]+resCols*i <<  ")=  "<<  controlPoints[i-initIndex][j-initIndex] ;
	   }
	   cout << endl;
    }
*/	
	//version 2 : if coordinate are switched
	
	xInterval->t0 =  m_mapCenter[1]+initIndex*resRows; 
    xInterval->tf = m_mapCenter[1]+finalIndex*resRows; 
    yInterval->t0 = m_mapCenter[0]+initIndex*resCols; 
    yInterval->tf = m_mapCenter[0]+finalIndex*resCols; 
    cout << "X interval: [" << xInterval->t0 <<"," <<  xInterval->tf<<  "]     Y interval: [" << yInterval->t0 <<"," <<  yInterval->tf<<"]"<< endl;
/*	cout <<"printing non-trivial data" << endl;
	bool isPrinted = false;
	for(int i=initIndex; i<=finalIndex; i++ )
	{
	   for(int j = initIndex; j<=finalIndex; j++)
	   {
	       controlPoints[i-initIndex][j-initIndex]=m_pStaticCostMap->getDataUTM<double>(m_scmapLayer, m_mapCenter[0]+resCols*i,m_mapCenter[1]+resRows*j); // i is the index on the X axis (easting) and j is the index on the Y axis (northing)
		   if(controlPoints[i-initIndex][j-initIndex]<90.0)
		   {
		       cout << fixed << setprecision(2) <<  " X,Y : (" << m_mapCenter[1]+resRows*j<<"," <<   m_mapCenter[0]+resCols*i <<  ")=  "<<  controlPoints[i-initIndex][j-initIndex] ;
	           isPrinted = true;
		   }
	   }
	   if(isPrinted)
	   {
	       cout << endl;
	       isPrinted = false;
	   }
    }*/
}






