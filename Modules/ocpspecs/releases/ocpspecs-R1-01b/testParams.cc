/*!
 * \file testParams.cc
 * \author Stefano Di Cairano
 *
 * \brief Test sending out the OCPParams structure
 *
 */

#include <iostream.h>
#include "interfaces/TpDpInterface.hh"
#include "interfaces/sn_types.h"
#include "skynettalker/SkynetTalker.hh"
#include "skynettalker/RDDFTalker.hh"
#include "dgcutils/RDDF.hh"
#include "testParams_cmdline.h"
#include <math.h>

class CRDDFTalkerTestSend : public CRDDFTalker
{
public:
  CRDDFTalkerTestSend(int sn_key, RDDF &rddf)
    : CSkynetContainer(MODrddftalkertestsend, sn_key)
  {
    RDDFVector waypoints = rddf.getTargetPoints();

    cout << "about to get_send_sock...";
    int rddfSocket = m_skynet.get_send_sock(SNrddf);
    cout << " get_send_sock returned" << endl;
		
    cout << "about to send rddf with "
	 << rddf.getNumTargetPoints() << " points" << endl;
    SendRDDF(rddfSocket, &rddf);
    cout << " sent an rddf!" << endl;
    flush(cout);
  }
};

int main(int argc, char** argv)
{

  // Parse command line options
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);

  /* Make sure that we got an input */
  if (cmdline.inputs_num != 1) {
    fprintf(stderr, "Usage: %s [-S key] rddf-file\n", argv[0]);
    exit(1);
  }

  // Figure out the skynet key
  int skynet_key = skynet_findkey(argc, argv);

  // Load the RDDF file that we will use
  RDDF rddf(cmdline.inputs[0]);	// read from an RDDF file
  rddf.print();		// print out the RDDF file
  RDDFVector waypoints = rddf.getTargetPoints();

  // Set up the parameters based on the waypoint file
  OCPparams myparams;

  // Initial point = first waypoint, with same upper and lower bounds
  myparams.initialConditionLB[EASTING_IDX_C] = 0;// waypoints[0].Northing;
  myparams.initialConditionLB[NORTHING_IDX_C] = 0; // waypoints[0].Easting;
  myparams.initialConditionLB[VELOCITY_IDX_C] = 0;
  myparams.initialConditionLB[HEADING_IDX_C] = 0;
  myparams.initialConditionLB[ACCELERATION_IDX_C] = 0;
  myparams.initialConditionLB[STEERING_IDX_C] = 0;
  for (int i = 0; i < COND_NUMBER; ++i) 
    myparams.initialConditionUB[i] = myparams.initialConditionLB[i];

  // Final point = last waypoint, with same upper and lower bounds
  const int N = rddf.getNumTargetPoints()-1;
  myparams.finalConditionLB[EASTING_IDX_C] = 30; //waypoints[N].Northing;
  myparams.finalConditionLB[NORTHING_IDX_C] = 25; // waypoints[N].Easting;
  myparams.finalConditionLB[VELOCITY_IDX_C] = 0;
  myparams.finalConditionLB[HEADING_IDX_C] = M_PI/2;
  myparams.finalConditionLB[ACCELERATION_IDX_C] = 0;
  myparams.finalConditionLB[STEERING_IDX_C] = 0;
  for (int i = 0; i < COND_NUMBER; ++i) 
    myparams.finalConditionUB[i] = myparams.finalConditionLB[i];

  myparams.parameters[VMIN_IDX_P] = 0.1;
  myparams.parameters[VMAX_IDX_P] = 26.8;
  myparams.parameters[AMIN_IDX_P] = -3.0;
  myparams.parameters[AMAX_IDX_P] = 0.981;
  myparams.parameters[PHIMIN_IDX_P] = -0.45 ;
  myparams.parameters[PHIMAX_IDX_P] = +0.45;
  myparams.parameters[PHIDMIN_IDX_P] = -1.31 ;
  myparams.parameters[PHIDMAX_IDX_P] = +1.31 ;

  // Let the user know what we are sending
  cout << "Sending initial = (" 
	<< myparams.initialConditionLB[EASTING_IDX_C] << ", "
	<< myparams.initialConditionLB[NORTHING_IDX_C] << "), "
	<< "final = ("
	<< myparams.finalConditionLB[EASTING_IDX_C] << ", "
	<< myparams.finalConditionLB[NORTHING_IDX_C] << ")" << endl; 

  SkynetTalker<OCPparams> paramsSender(skynet_key, SNocpParams,
				       MODdynamicplanner);
  bool result = paramsSender.send(&myparams);
  cout << " sent: " << result << endl;

  /* Now send out the RDDF as a corridor */
  CRDDFTalkerTestSend test(skynet_key, rddf);
}
