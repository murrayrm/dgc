
/*
 Definition of the OCPTSPECS class that provides the interface
 between Tplanner and Dplanner

 Stefano Di Cairano mar-07
 */

#include "OCPtSpecs.hh"
#include "interfaces/TpDpInterface.hh"
#define MAX_DELTA_SIZE 1000000  //taken from plannerModule
#define center_N 0.0           //taken from map.default.dat
#define center_E 0.0


OCPtSpecs::OCPtSpecs(int sn_key, bool useAstate)
    : CSkynetContainer(MODdynamicplanner, sn_key),
      CStateClient(false),
      CRDDFTalker(false),
      m_scmapRecvdOneDelta(false),
      m_paramsRecvdOne(false),
      m_obstRecvdOne(false),
      m_RDDFRecvdOne(false),
      m_paramsTalker(sn_key,SNocpParams,MODdynamicplanner),
      m_obstTalker(sn_key,SNocpObstacles,MODdynamicplanner)
{
	 
        //initialize costmap and related mutexes and sockets
        m_skynetKey = sn_key;
        m_useAstate = useAstate;
	if(m_useAstate)
	 cout << "Warning: using State from AState" << endl;
	m_pScmapDelta = new char[MAX_DELTA_SIZE];   //this is the buffer for the deltas
    
        m_pStaticCostMap = new CMapPlus();
	m_pStaticCostMap->initMap(center_N, center_E, SCM_NROWS, SCM_NCOLS, SCM_RESROWS, SCM_RESCOLS, VERBOSITY); // initialize the map qith appropriate size
	m_scmapLayer = m_pStaticCostMap->addLayer<double>(SCM_NODATA,SCM_OUTSIDEMAP,SCM_USEDELTA); //add the staic cost layer

	DGCcreateMutex(&m_scmapMutex);                  //mutex for the static cost map
	DGCcreateMutex(&m_scmapDeltaRecvdMutex);     //mutex for the deltas
	DGCcreateCondition(&m_scmapDeltaRecvdCond);  //condition on deltas reception
	
        m_scmapRequestSocket = m_skynet.get_send_sock(SNtplannerStaticCostMapRequest);  //put skynet interface here
	m_scmapDeltaSocket =  m_skynet.listen(SNtplannerStaticCostMap,MODtrafficplanner); //put skynet interface here   

        //params mutexes
	
        DGCcreateMutex(&m_paramsMutex);                  //mutex for the static cost map
	DGCcreateMutex(&m_paramsRecvdMutex);     //mutex for the deltas
	DGCcreateCondition(&m_paramsRecvdCond);  //condition on deltas reception
	
       	m_paramsSocket =  m_skynet.listen(SNocpParams,MODtrafficplanner); //put skynet interface here   


        //obstacles
	
        DGCcreateMutex(&m_obstMutex);                  //mutex for the static cost map
	DGCcreateMutex(&m_obstRecvdMutex);     //mutex for the deltas
	DGCcreateCondition(&m_obstRecvdCond);  //condition on deltas reception
	
       	m_obstSocket =  m_skynet.listen(SNocpObstacles,MODtrafficplanner); //put skynet interface here   

        //RDDF receiver
	
       	m_RDDFSocket =  m_skynet.listen(SNrddf,MODtrafficplanner); //put skynet interface here   
	DGCcreateMutex(&m_RDDFMutex);                  //mutex for the static cost map
	DGCcreateMutex(&m_RDDFRecvdMutex);     //mutex for the deltas
	DGCcreateCondition(&m_RDDFRecvdCond);  //condition on deltas reception

        //start the threads maybe in the future we should avoid threads and make them synchrounous functions.
         
       DGCstartMemberFunctionThread(this, &OCPtSpecs::getRDDFThread) ;
       DGCstartMemberFunctionThread(this, &OCPtSpecs::getOCPparamsThread) ;
	DGCstartMemberFunctionThread(this, &OCPtSpecs::getOCPobstThread) ;
	DGCstartMemberFunctionThread(this, &OCPtSpecs::getScmapDeltasThread); //start the thread to catch the map deltas 
//	scmapRequest(); // send a request for the full map
    
}

void OCPtSpecs::scmapRequest()
{
	bool bRequestScmap = true;
	m_skynet.send_msg(m_scmapRequestSocket,&bRequestScmap, sizeof(bool) , 0);
}

void OCPtSpecs::getScmapDeltasThread()
{
  // The skynet socket for receiving map deltas (from fusionmapper)
//	int scMapDeltaSocket = m_skynet.listen( <Put here the communication names>,  <Put here the communication names>);
//	if(scMapDeltaSocket < 0)
//		cerr << "OCPtSpecs::getMapDeltasThread(): skynet listen returned error" << endl;

	while(true)
	{
		int deltasize;
		
		cout << "Waiting for SCM Deltas " << endl;

		// I don't think the RecvMapdelta function works like it should --JM
		//RecvMapdelta(m_scmapDeltaSocket, m_pScmapDelta, &deltasize);  

//		WaitForNewState();

		//LOCK MUTEX!!!
                
		// we update the map from the problems parameters
		if(m_useAstate)
		{
		    m_pStaticCostMap->updateVehicleLoc(m_state.localX, m_state.localY);
		}
		else
		{
		    
		    DGClockMutex(&m_paramsMutex);
		    m_pStaticCostMap->updateVehicleLoc(m_OCPparams.initialConditionLB[NORTHING_IDX_C], m_OCPparams.initialConditionLB[EASTING_IDX_C]);
		    DGCunlockMutex(&m_paramsMutex);
		}

                deltasize = m_skynet.get_msg(m_scmapDeltaSocket, m_pScmapDelta, MAX_DELTA_SIZE, 0);
	       if(deltasize>0)
	       {
	
	           DGClockMutex(&m_scmapMutex);
	           cout << "Applying SCM Deltas " << endl;
		   m_pStaticCostMap->applyDelta<double>(m_scmapLayer, m_pScmapDelta, deltasize);	
		  
		   if(!m_scmapRecvdOneDelta)
		   {
		       DGCSetConditionTrue(m_scmapRecvdOneDelta, m_scmapDeltaRecvdCond, m_scmapDeltaRecvdMutex);
		       DGCSetConditionFalse(m_scmapRecvdOneDelta, m_scmapDeltaRecvdCond, m_scmapDeltaRecvdMutex);
		   } 

		   //UNLOCK MUTEX!!!
		   DGCunlockMutex(&m_scmapMutex);
               }
//		cout << "Receive Delta Thread is running"  <<endl;
				
//		sleep(1);
	}

}


void OCPtSpecs::getOCPparamsThread()
{
       bool paramsCorrect;
       OCPparams tempParams;
	while(true)
	{
		
		cout << "Waiting for Params" << endl;

		// I don't think the RecvMapdelta function works like it should --JM
		//RecvMapdelta(m_scmapDeltaSocket, m_pScmapDelta, &deltasize);  
                paramsCorrect = false;
		

                paramsCorrect = m_paramsTalker.receive(&tempParams);
		if(! paramsCorrect)
		{
		    cout << "error" << endl;
		//UNLOCK MUTEX!!!
                }
		else
		{
		    DGClockMutex(&m_paramsMutex); 
		    m_OCPparams = tempParams;
                    cout << "I am applying params. g: " << m_OCPparams.parameters[11] <<endl;     
		    if(!m_paramsRecvdOne)
		    {
		        DGCSetConditionTrue(m_paramsRecvdOne, m_paramsRecvdCond, m_paramsRecvdMutex);
	            }   
                    DGCunlockMutex(&m_paramsMutex);
		
		}
//		cout << "Receive Params Thread is running"  <<endl;
				
	//	sleep(1);
	}

}

void OCPtSpecs::getRDDFThread()
{
    bool RDDFcorrect;
    RDDF tempRDDF;
    while(true)
    {
		
		
//	cout << "about to receive an rddf ..."<< endl;
//	WaitForRDDF();
//	UpdateRDDF();
//	cout << "done updating ..." << endl;
        RDDFcorrect = false;
	RDDFcorrect = RecvRDDF(m_RDDFSocket,&tempRDDF);
	if(!RDDFcorrect)
	{
	    cout <<"Error while recv RDDF" << endl;
	}
	else
	{
            DGClockMutex(&m_RDDFMutex);
	    m_RDDF = tempRDDF;
//	    cout << "Applied RDDF object ..."<< endl;
	    DGCunlockMutex(&m_RDDFMutex);
        }
        if(!m_RDDFRecvdOne)
        {
            DGCSetConditionTrue(m_RDDFRecvdOne, m_RDDFRecvdCond, m_RDDFRecvdMutex);
	}   
				
    }

}

void OCPtSpecs::getOCPobstThread()
{
       bool obstCorrect;
       OCPobstacles tempObst;
	while(true)
	{
		
		cout << "Waiting for obstacles" << endl;

		// I don't think the RecvMapdelta function works like it should --JM
		//RecvMapdelta(m_scmapDeltaSocket, m_pScmapDelta, &deltasize);  
                obstCorrect = false;
		

		//LOCK MUTEX!!!

                
	//	obstCorrect =  m_skynet.get_msg(m_obstSocket, &tempObst, sizeof(OCPobstacles), 0); 
                obstCorrect = m_obstTalker.receive(&tempObst);

		if(!obstCorrect)
		{
		    cout << "error" << endl;
		//UNLOCK MUTEX!!!
                }
		else
		{
                    cout << "Applying obstacles: " << tempObst.ObstNumber <<  endl;
                    
		    DGClockMutex(&m_obstMutex);
                    m_Qconstr.Nconstr=tempObst.ObstNumber;
                    //building the H, S, K components
	            for(int i=0;i<tempObst.ObstNumber;i++)
		    {
                        m_Qconstr.S[i][0] = -2*( tempObst.ObstHessian[i][0][0]*tempObst.ObstCenter[i][0] + 
			                                    tempObst.ObstHessian[i][0][1]*tempObst.ObstCenter[i][1]) ;
	                m_Qconstr.S[i][1] = -2*( tempObst.ObstHessian[i][1][0]*tempObst.ObstCenter[i][0] + 
			                                    tempObst.ObstHessian[i][1][1]*tempObst.ObstCenter[i][1]) ;
                        m_Qconstr.H[i][0][0] = tempObst.ObstHessian[i][0][0];
                        m_Qconstr.H[i][0][1] = tempObst.ObstHessian[i][0][1]; 
	                m_Qconstr.H[i][1][1] = tempObst.ObstHessian[i][1][1]; 
	                m_Qconstr.H[i][1][0] = tempObst.ObstHessian[i][1][0];
                        m_Qconstr.K[i] = tempObst.ObstCenter[i][0] * tempObst.ObstHessian[i][0][0] * tempObst.ObstCenter[i][0]  +
			                         2 * tempObst.ObstCenter[i][0] * tempObst.ObstHessian[i][0][1] * tempObst.ObstCenter[i][1] +
						          tempObst.ObstCenter[i][1] * tempObst.ObstHessian[i][1][1] * tempObst.ObstCenter[i][1]; 
                        
		    }
                   //zeroing the unapplied constraints -> this is not necessary, but for now ....
	            for(int i=tempObst.ObstNumber;i<MAX_OBSTACLES;i++)
	            {
                        m_Qconstr.S[i][0] = 0;
	                m_Qconstr.S[i][1] = 0;
                        m_Qconstr.H[i][0][0] = 0;
                        m_Qconstr.H[i][0][1] = 0; 
	                m_Qconstr.H[i][1][1] = 0; 
	                m_Qconstr.H[i][1][0] = 0;
                        m_Qconstr.K[i] = 0;
	            }


		
		    
	          //  m_OCPobstacles = tempObst;
		    
		    if(!m_obstRecvdOne)
		    {
		        DGCSetConditionTrue(m_obstRecvdOne, m_obstRecvdCond, m_obstRecvdMutex);
	            }  
                    DGCunlockMutex(&m_paramsMutex);
		
		}
				
	//	sleep(1);
	    }

    }


OCPtSpecs::~OCPtSpecs(void)
{
//not doing anything for now
}

void OCPtSpecs::getRDDFcorridor(RDDF* pNewRDDF)
{
    *pNewRDDF =  m_RDDF;
    return ;
}

/* ! Use to specify all the dimensions and flags related to the coded OC Problem. */
void OCPtSpecs::getOCPComponentSizes(OCPComponentSizes* const ocpComponentSizes)
{
	//this are fixed for now, will be stored in a class member in the future
    ocpComponentSizes->IsActive             = true;
    ocpComponentSizes->HasICostFunction     = false;
    ocpComponentSizes->HasTCostFunction     = true;
    ocpComponentSizes->HasFCostFunction     = false;
    ocpComponentSizes->NofILinConstraints   = 0;
    ocpComponentSizes->NofTLinConstraints   = 0;
    ocpComponentSizes->NofFLinConstraints   = 0;
    ocpComponentSizes->NofINLinConstraints  = 0;
    ocpComponentSizes->NofTNLinConstraints  = m_Qconstr.Nconstr;
    ocpComponentSizes->NofFNLinConstraints  = 0;
}


/*! Use to implement the cost function applicable for the begining of the time interval.*/
void OCPtSpecs::InitialCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Fi, double* const DFi)
{
    return ; // no initial cost function for the moment
}

/*! Use to implement the cost function for the whole time interval.*/
void OCPtSpecs::TrajectoryCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ft, double* const DFt)
{
    if(*mode == 0 || *mode == 2)
    {   
       *Ft = m_pStaticCostMap->getDataUTM<double>(m_scmapLayer, z2, z1);
    }

    if(*mode == 1 || *mode == 2)
    {
        for(m_i=0;m_i<NZ;m_i++)  //initialize the df/dz-vector to 0 
        {
    	    DFt[m_i] = 0;
        }
     double grad[2];
     m_pStaticCostMap->getUTMGradient(m_scmapLayer, z2 , z1, grad);
     DFt[I_z1]=grad[0];
     DFt[I_z2]=grad[1];
     }  
       
//       if(*mode == 99)  not implemented yet
//       {
//          // sparsity
//       }
}

/*! Use to implement the cost function applicable at the end of the time interval.*/
void OCPtSpecs::FinalCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ff, double* const DFf)
{
    return ;  //not used yet
}

/*! Use to implement the linear constraints - those applicable at the beginning of the time interval, for the whole time interval and for those at the end of the time interval.*/
void OCPtSpecs::getLinearConstraints(const ConstraintType* const ctype, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A, const int* const Offset)
{
//	if(*ctype == ct_LinInitial)
//	{
//        for(m_i=0; m_i<NofLConstraints; m_i++)
//        {
//            for(m_j=0; m_j<NofVariables; m_j++)
//            {
//                A[m_i][m_j] = 0.0;
//            }
//        }

// 	}	
//	else if(*ctype == ct_LinTrajectory)
//	{
//          
//	}
//	else /*if(*ctype == ct_LinFinal)*/
//	{
//         
//	}	

    return ;  //not used yet

}

/*! Use to imOCPtSpecs::plement the nonlinear constraints applicable at the beginning of the time interval.*/
 void OCPtSpecs::InitialNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli, const int* const Offset) 
{
    return ;  //not used yet
}

/*! Use to implement the nonlinear constraints applicable during the whole time interval.*/
void OCPtSpecs::TrajectoryNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt, const int* const Offset)
{
    double tempX[2]; //temporary position vector
    tempX[0]=z1;
    tempX[1]=z2;
    for(m_i=0;m_i<*NofConstraints; m_i++)
    {
        for(m_j=0;m_j<*NofVariables; m_j++)
        {
            DCnlt[m_i+*Offset][m_j]=0;  //initialize the jacobian to 0
        }
        Cnlt[*Offset+m_i]=0;  //this should not be necessary, because the value of each constraint is computed.
    }

 /* the following is a debug code, left here in case I need it 
   double newDCnlt[*Offset+*NofConstraints][*NofVariables];
    double newCnlt[*Offset+*NofConstraints];
 
    for(m_i=0;m_i<*NofConstraints+*Offset; m_i++)
    {
        for(m_j=0;m_j<*NofVariables; m_j++)
        {
           newDCnlt[m_i][m_j]=0;  //initialize the jacobian to 0
        }
        newCnlt[m_i]=0;  //this should not be necessary, because the value of each constraint is computed.
    }

    for(m_i=0;m_i<*NofConstraints; m_i++)
    {
        for(m_j=0;m_j<*NofVariables; m_j++)
        {
          cout <<" " <<  newDCnlt[m_i+*Offset][m_j];  //initialize the jacobian to 0
        }
 //       newCnlt[*Offset+m_i]=0;  //this should not be necessary, because the value of each constraint is computed.
   cout << endl;
   }

cout << "init value" << endl;
    for(int i=0;i<*Offset+*NofConstraints;i++)
    {
        for (int j=0;j<*NofVariables;j++)
        cout << "  " << newDCnlt[i][j] ;
 
        cout << endl;
    }

 cout << endl;

*/
    if(*mode == 0 | *mode == 2)
    {
        for(m_i=0;m_i<*NofConstraints;m_i++)
        {

// computation of (x-c)H(x-c)-c'Hc = x'Hx-2x'Hc , since SNOPT does not like the constant term....
// new code with higher performance below
/*            Cnlt[*Offset+m_i] =  tempX[0] * m_OCPobstacles.ObstHessian[m_i][0][0] * tempX[0] +
                                   tempX[0] * m_OCPobstacles.ObstHessian[m_i][0][1] * tempX[1] +
                                      tempX[1] * m_OCPobstacles.ObstHessian[m_i][1][0] * tempX[0] +
                                         tempX[1] * m_OCPobstacles.ObstHessian[m_i][1][1] * tempX[1] -
				     	    2 *  tempX[0] * m_OCPobstacles.ObstHessian[m_i][0][0] * m_OCPobstacles.ObstCenter[m_i][0] -
                                              2 *  tempX[0] * m_OCPobstacles.ObstHessian[m_i][0][1] * m_OCPobstacles.ObstCenter[m_i][1] - 
                                                 2 *  tempX[1] * m_OCPobstacles.ObstHessian[m_i][1][0] * m_OCPobstacles.ObstCenter[m_i][0 ]- 
                                                    2 *  tempX[1] * m_OCPobstacles.ObstHessian[m_i][1][1] * m_OCPobstacles.ObstCenter[m_i][1] ;
*/

  	  Cnlt[*Offset+m_i] = tempX[0]*m_Qconstr.H[m_i][0][0]*tempX[0]+ 2 * tempX[0]*m_Qconstr.H[m_i][0][1]*tempX[1] + 
	                          tempX[1]*m_Qconstr.H[m_i][1][1]*tempX[1] + m_Qconstr.S[m_i][0]*tempX[0] + m_Qconstr.S[m_i][1]*tempX[1];
 
 
        }
    } 
 //the gradient of (x-c)'H(x-c) is 2(x-c)H for H simmetric, which is the case for ellipsis
   //N.B. the gradient of x'Hx-2x'Hc=(x-c)'H(x-c)-c'Hc is computed in the same way, since they differ for a constant term that goes away when differentiating
        /*    DCnlt[m_i+*Offset][I_z1]=2*( (tempX[0]-m_OCPobstacles.ObstCenter[m_i][0])*m_OCPobstacles.ObstHessian[m_i][0][0]+(tempX[1]-m_OCPobstacles.ObstCenter[m_i][1])*m_OCPobstacles.ObstHessian[m_i][1][0]);
            DCnlt[m_i+*Offset][I_z2]=2*( (tempX[0]-m_OCPobstacles.ObstCenter[m_i][0])*m_OCPobstacles.ObstHessian[m_i][0][1]+(tempX[1]-m_OCPobstacles.ObstCenter[m_i][1])*m_OCPobstacles.ObstHessian[m_i][1][1]);
  */



    if(*mode == 1 | *mode == 2)
    {
        for(m_i=0;m_i<*NofConstraints;m_i++)
        {

            DCnlt[m_i+*Offset][I_z1]=m_Qconstr.S[m_i][0]+2*(tempX[0]*m_Qconstr.H[m_i][0][0]+tempX[1]*m_Qconstr.H[m_i][1][0]) ;
	    DCnlt[m_i+*Offset][I_z2]=m_Qconstr.S[m_i][1]+2*(tempX[0]*m_Qconstr.H[m_i][0][1]+tempX[1]*m_Qconstr.H[m_i][1][1]) ;

        }
    }
   
   //compute the Sparsity
   //
    if(*mode == 99)
    {   
      //  cout << "Sparsity :"  <<endl; 
        for(m_i=0;m_i<*NofConstraints;m_i++)
        {
             
            DCnlt[m_i+*Offset][I_z1]= 1;
            DCnlt[m_i+*Offset][I_z2]= 1;  
        }
    }
   
       return ;  //not used yet
}

/*! Use to implement the nonlinear constraints applicable at the end of the time interval.*/
void OCPtSpecs::FinalNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf, const int* const Offset)
{
    return ;  //not used yet
}

/*! Use to implement the bounds for all the constraints: linear and nonlinear.*/
void OCPtSpecs::getLUBounds(const ConstraintType* const ctype, const BoundsType* const ubtype, const int* const NofConstraintCollocPoints, const int* const NofConstraints, double* const LUBounds, const int* const Offset)
{
//	if(*ctype == ct_LinInitial)
//	{
//        
//        if(*ubtype == ubt_LowerBound)
//        {
//            for(m_i=0; m_i<m_NofConstraints; m_i++)
//            {
//                LUBounds[m_i] = 0;
//            }                
//        }
//        else
//        {
//            for(m_i=0; m_i<m_NofILinConstraints; m_i++)
//            {
//                LUBounds[m_i] = 0;
//            }                
//        }
//        
//	}
//
//	if(*ctype == ct_LinTrajectory)
//	{
//          
//
//	}
//
//	if(*ctype == ct_LinFinal)
//	{
//
//	}
//
//	if(*ctype == ct_NLinInitial)
//	{
//
//	}
//
        if(*ctype == ct_NLinTrajectory)
	{  
	    double temp =0;
            for(m_i=0; m_i<(*NofConstraintCollocPoints); m_i++)
	    {
	        for(m_j=0; m_j<(*NofConstraints); m_j++)
	        {
	            if( *ubtype == ubt_LowerBound )
		    {   
		        //subtracting the constant term removed from the ellipse (c'Hc)
		     /*   temp = 1 - m_OCPobstacles.ObstCenter[m_j][0] * m_OCPobstacles.ObstHessian[m_j][0][0] * m_OCPobstacles.ObstCenter[m_j][0] - 
                                   m_OCPobstacles.ObstCenter[m_j][0] * m_OCPobstacles.ObstHessian[m_j][0][1] * m_OCPobstacles.ObstCenter[m_j][1] -
                                      m_OCPobstacles.ObstCenter[m_j][1] * m_OCPobstacles.ObstHessian[m_j][1][0] * m_OCPobstacles.ObstCenter[m_j][0]  -
                                         m_OCPobstacles.ObstCenter[m_j][1] * m_OCPobstacles.ObstHessian[m_j][1][1] * m_OCPobstacles.ObstCenter[m_j][1]; 
		*/
		     temp = 1 - m_Qconstr.K[m_j];
		        // temp = 1;		  
		    }
		    else
		    {
		        temp = DBL_MAX;
		    }
	        LUBounds[*Offset+m_i*(*NofConstraints)+m_j]=temp;
	        } 
	    }

	}
//
//	if(*ctype == ct_NLinFinal)
//	{
//       
//	}   
return ;  //not used yet
}
    
    
void  OCPtSpecs::getInitialCondition(const BoundsType* const ubtype, double* const initCond)
{
    bool valuecheck = false;
    if( *ubtype == ubt_LowerBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          initCond[m_i]=m_OCPparams.initialConditionLB[m_i];	  
        }
	valuecheck = true;
    }
    
    if( *ubtype == ubt_UpperBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          initCond[m_i]=m_OCPparams.initialConditionUB[m_i];	  
        }
	valuecheck = true;
    }
    if(valuecheck==false)
    {
        cout << "wrong value for bounds type" <<endl;
        exit(1);
    }

    return ;
}

void  OCPtSpecs::getFinalCondition(const BoundsType* const ubtype, double* const finalCond)
{
    bool valuecheck = false;
    if( *ubtype == ubt_LowerBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          finalCond[m_i]=m_OCPparams.finalConditionLB[m_i];	  
        }
	valuecheck = true;
    }
    
    if( *ubtype == ubt_UpperBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          finalCond[m_i]=m_OCPparams.finalConditionUB[m_i];	  
        }
	valuecheck = true;
    }
    if(valuecheck==false)
    {
        cout << "wrong value for bounds type" <<endl;
        exit(1);
    }

    return ;
}



void  OCPtSpecs::getParams(double* const params)
{

    for(m_i=0;m_i<PARAMS_NUMBER;m_i++)
    {
        params[m_i]=m_OCPparams.parameters[m_i];	  
    }
    params[12] = m_OCPparams.initialConditionLB[VELOCITY_IDX_C];
    params[13] = m_OCPparams.finalConditionLB[VELOCITY_IDX_C];
    params[14] = m_OCPparams.initialConditionLB[ACCELERATION_IDX_C];
    params[15] = m_OCPparams.finalConditionLB[ACCELERATION_IDX_C];
   
return ;
}
void  OCPtSpecs::getMode(int* mode)
{     
    *mode = m_OCPparams.mode; 
    return ;
}

void OCPtSpecs::startSolve()
{
    
    //this function waits the data to be filled in
    //then lock all the mutexes to keep consistency
    DGCWaitForConditionTrue(m_RDDFRecvdOne, m_RDDFRecvdCond, m_paramsRecvdMutex);
    DGCWaitForConditionTrue(m_paramsRecvdOne, m_paramsRecvdCond, m_paramsRecvdMutex);
    DGCWaitForConditionTrue(m_obstRecvdOne, m_obstRecvdCond, m_obstRecvdMutex);
    DGCWaitForConditionTrue(m_scmapRecvdOneDelta, m_scmapDeltaRecvdCond, m_scmapDeltaRecvdMutex);
   
    DGClockMutex(&m_RDDFMutex);
    DGClockMutex(&m_obstMutex);
    DGClockMutex(&m_paramsMutex);
    DGClockMutex(&m_scmapMutex);
    
    return ;
}

void OCPtSpecs::endSolve()
{
   //this function set all the conditions to false
   //then unlock all the mutexes to let new data come in
/*
    DGCSetConditionFalse(m_scmapRecvdOneDelta, m_scmapDeltaRecvdCond, m_scmapDeltaRecvdMutex);
    DGCSetConditionFalse(m_paramsRecvdOne, m_paramsRecvdCond, m_paramsRecvdMutex);
    DGCSetConditionFalse(m_obstRecvdOne, m_obstRecvdCond, m_obstRecvdMutex);
  */  
    DGCunlockMutex(&m_RDDFMutex);
    DGCunlockMutex(&m_scmapMutex);
    DGCunlockMutex(&m_obstMutex);
    DGCunlockMutex(&m_paramsMutex);
   return ;


}
