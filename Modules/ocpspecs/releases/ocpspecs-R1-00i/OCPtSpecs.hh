#ifndef OCPTSPECS_HH_
#define OCPTSPECS_HH_

#endif /*OCPTSPECS_HH_*/
/*
 Declaration of the OCPTSPECS class that provides the interface
 between Tplanner and Dplanner

 Stefano Di Cairano mar-07
 */

#ifdef OTG_HOME
#include "NURBSBasedOTG/OTG_Interfaces.hh"
#else
#warning using OTG_InterfacesX.hh as placeholder
#include "OTG_InterfacesX.hh"
#endif
#include "cmap/CMapPlus.hh"
#include "cmap/MapdeltaTalker.hh"
#include "interfaces/sn_types.h" 
//#include "skynet/sn_msg.hh"
#include "skynet/skynet.hh" 
#include "skynet/SkynetContainer.hh"
#include "dgcutils/DGCutils.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/SkynetTalker.hh"
#include "skynettalker/SkynetTalker.cc"
#include "pthread.h"
#include <iostream>
#include <unistd.h>

#define PARAMS_NUMBER 12
#define COND_NUMBER 6

#define LENGTH_IDX_P 0
#define WEELB_IDX_P 1
#define HCG_IDX_P 2
#define VMIN_IDX_P 3
#define VMAX_IDX_P 4
#define AMIN_IDX_P 5
#define AMAX_IDX_P 6
#define PHIMIN_IDX_P 7
#define PHIMAX_IDX_P 8
#define PHIDMIN_IDX_P 9
#define PHIDMAX_IDX_P 10
#define G_IDX_P 11 
#define EASTING_IDX_C 0
#define NORTHING_IDX_C 1
#define VELOCITY_IDX_C 2
#define HEADING_IDX_C 3
#define ACCELERATION_IDX_C 4
#define STEEERING_IDX_C 5

/* Flat outputs: */
#define z1		 z[0]
#define z1d		 z[1]
#define z1dd	 z[2]
#define z2		 z[3]
#define z2d		 z[4]
#define z2dd	 z[5]
#define z3		 z[6]
#define z3d		 z[7]
#define z3dd	 z[8]
#define z4		 z[9]
#define z4d		 z[10]
#define z4dd	 z[11]
#define z5		 z[12]
#define z6		 z[13]
#define z6d		 z[14]
#define z6dd	 z[15]
#define z7		 z[16]
#define z7d		 z[17]
#define z7dd	 z[18]

/* z1 = x, z2 = y, z3 = v, z4 = theta, z5 = t, z6 = a, z7 = phi */

#define NZ 19
#define SCM_NROWS 500          // taken from map.default.dat
#define SCM_NCOLS 500 
#define SCM_RESROWS 0.4
#define SCM_RESCOLS 0.4
#define VERBOSITY 0
#define SCM_NODATA 100       //these have to be tuned depending on the generated cost function
#define SCM_OUTSIDEMAP 90
#define SCM_USEDELTA 1

#define MAX_OBSTACLES 5

struct OCPobstacles 
{
    int ObstNumber ;
    double ObstCenters [MAX_OBSTACLES][2];
    double ObstHessian [MAX_OBSTACLES][2][2];

    OCPobstacles()
    {
        ObstNumber=0;
	for(int i=0;i<MAX_OBSTACLES;i++)
	{
            ObstCenters[i][1] = 0;
	    ObstCenters[i][2] = 0;
            ObstHessian[i][1][1] = 0;
            ObstHessian[i][1][2] = 0; 
	    ObstHessian[i][2][2] = 0; 
	    ObstHessian[i][2][1] = 0;

	}

    }

};


/*! -  This enum is used to encapsulate the valid flags that specify the system mode.*/
enum Mode{md_FWD, md_REV};



struct OCPparams
{
    int mode;
    double parameters [PARAMS_NUMBER];
    double initialConditionLB [COND_NUMBER];
    double initialConditionUB [COND_NUMBER];
    
    /*
    same structure as finalCondition
    */
       
    double finalConditionLB [COND_NUMBER];
    double finalConditionUB [COND_NUMBER];
 /*
     finalCondition[0] = Northing_min
     finalCondition[1] = Northing_max
     finalCondition[2] = Easting_min
     finalCondition[3] = Easting_max
     finalCondition[4]  = Heading_min
     finalCondition[5]  = Heading_max 
     finalCondition[6]  = Velocity_min (norm)
     finalCondition[7]  = Velocity_max (norm)
     finalCondition[8]  = Acceleration_min (norm)
     finalCondition[9]  = Acceleration_max (norm)
     finalCondition[10]  = SteeringAngle_min 
     finalCondition[11]  = SteeringAngle_max
      */
  
    OCPparams()
    {
    	//this is the fastest way to set everything to 0
        memset(parameters,'\0',PARAMS_NUMBER*sizeof(double));    
        memset(initialConditionLB,'\0',COND_NUMBER*sizeof(double));    
        memset(finalConditionLB,'\0',COND_NUMBER*sizeof(double));    
        memset(initialConditionUB,'\0',COND_NUMBER*sizeof(double));    
        memset(finalConditionUB,'\0',COND_NUMBER*sizeof(double));    
        //by default we drive fwd
        mode = md_FWD ;
        
        //setting parasmeters to the default values defined For Alice dynamics in dplanner
	    parameters[0]  = 5.43560 ;         // L
	    parameters[1]  = 2.13360 ;         // W
	    parameters[2]  = 1.06680 ;         // hcg
	    parameters[3]  = 0.00000 ;         // v_min
	    parameters[4]  = 1.40800 ;         // v_max
	    parameters[5]  =-3.00000 ;         // a_min
	    parameters[6]  = 0.98100 ;         // a_max
	    parameters[7]  =-0.44942 ;         // phi_min
	    parameters[8]  = 0.44942 ;         // phi_max
	    parameters[9]  =-1.30900 ;         // phid_min
	    parameters[10] = 1.30900 ;         // phid_max
	    parameters[11] = 9.81000 ;         // g
	    
    }
    template <class Archive>
    void serialize(Archive &ar,const unsigned int version)
    {
    
    ar & mode;
    ar & parameters;
    ar & initialConditionLB;
    ar & initialConditionUB;
    
    /*
    same structure as finalCondition
    */
       
    ar & finalConditionLB;
    ar & finalConditionUB;


    }
};


class OCPtSpecs : public IOCPComponent,  public CMapdeltaTalker, public CStateClient
{ 
  
private:
  int m_i;
  int m_j;
  int m_skynetKey;
  int m_useAstate;
 //OCP parameters
  OCPparams m_OCPparams;

 //Obstacles
  OCPobstacles m_OCPobstacles;
 
 //static cost map data
  CMapPlus* m_pStaticCostMap;
  
  int m_scmapLayer;
  char* m_pScmapDelta;
  
  //map access mutex
  pthread_mutex_t	m_scmapMutex;
  
  //mutex+cond on received deltas
  bool m_scMapRecvdOneDelta;
  pthread_mutex_t	m_scmapDeltaRecvdMutex;
  pthread_cond_t m_scmapDeltaRecvdCond;
  
  //socket to request the full map
  int m_scmapRequestSocket;
  //socket to receive deltas
  int m_scmapDeltaSocket;


  //socket to receive params
  int m_paramsSocket;
  pthread_mutex_t m_paramsMutex;
  pthread_mutex_t m_paramsRecvdMutex;
  pthread_cond_t m_paramsRecvdCond;
  bool m_paramsRecvdOne;

  //skynetTalker Object to Receive Params
  
  SkynetTalker <OCPparams> m_paramsTalker;

 
  
  //private methods to deal with the static cost map
  void getScmapDeltasThread();  //thread to read deltas
  void scmapRequest();   //function that request the full map
 
  //thread to read parameters
  void getOCPparamsThread();
  
  //socket to receive obstacles
  int m_obstSocket;
  pthread_mutex_t m_obstMutex;
//  pthread_mutex_t m_obstaclesRecvdMutex;
//  pthread_cond_t m_obstaclesRecvdCond;
//  bool m_obstaclesRecvdOne;


//thread to read obstacles
  void getOCPobstThread();
  
  //private methods to deal with the static cost map
 /* void getScmapDeltasThread();  //thread to read deltas
  void scmapRequest();   //function that request the full map
 
  //thread to read parameters
  void getOCPparamsThread();
*/

public:
  
    OCPtSpecs(int sn_key, bool useAstate=false);
    virtual  ~OCPtSpecs(void);
  
  /* ! Use to specify all the dimensions and flags related to the coded OC Problem. */
    virtual  void getOCPComponentSizes(OCPComponentSizes* const ocpComponentSizes);
  /*! Use to implement the cost function applicable for the begining of the time interval.*/
    virtual   void InitialCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Fi, double* const DFi);
  
  /*! Use to implement the cost function for the whole time interval.*/
    virtual   void TrajectoryCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ft, double* const DFt);
  
  /*! Use to implement the cost function applicable at the end of the time interval.*/
    virtual      void FinalCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ff, double* const DFf);
  
  /*! Use to implement the linear constraints - those applicable at the beginning of the time interval, for the whole time interval and for those at the end of the time interval.*/
    virtual void getLinearConstraints(const ConstraintType* const ctype, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A, const int* const Offset);
  
  /*! Use to implement the nonlinear constraints applicable at the beginning of the time interval.*/
    virtual void InitialNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli, const int* const Offset) ;
  
  
  /*! Use to implement the nonlinear constraints applicable during the whole time interval.*/
    virtual void TrajectoryNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt, const int* const Offset);
  
  /*! Use to implement the nonlinear constraints applicable at the end of the time interval.*/
    virtual void FinalNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf, const int* const Offset);
  
  /*! Use to implement the bounds for all the constraints: linear and nonlinear.*/
    virtual void getLUBounds(const ConstraintType* const ctype, const BoundsType* const ubtype, const int* const NofConstraintCollocPoints, const int* const NofConstraints, double* const LUBounds, const int* const Offset);
    
    virtual void getInitialCondition(const BoundsType* const ubtype, double* const initCond);
    virtual void getFinalCondition(const BoundsType* const ubtype, double* const finalCond);
    virtual void getParams(double* const params);
    virtual void getMode(int* mode); 
};
