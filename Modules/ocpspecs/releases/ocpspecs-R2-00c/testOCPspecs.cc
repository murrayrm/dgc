#include <iostream.h>
#include "ocpspecs/OCPtSpecs.hh"
int main(int argc, char** argv)
{
    int skynet_key;
    cout << "Please enter Skynet Key:" << endl;
    cin >> skynet_key;
    int Npoints1 = 3 ;
    int Npoints2 = 4 ;
    int NofDim = 2 ;
	double** vertices1;
	double** vertices2;
    OCPtSpecs* pOCPspecs = new OCPtSpecs(skynet_key,true,3,false,false,false,false,false);
    CAlgebraicGeometry* algGeom = new CAlgebraicGeometry(); 
	vertices1 = new double*[NofDim];
    vertices2 = new double*[NofDim];
	for(int k=0; k<NofDim;k++)
	{
	   vertices1[k] = new double[Npoints1];
	}
	vertices1[0][0] = 0;
	vertices1[1][0] = 0;
	vertices1[0][1] = 5;
	vertices1[1][1] = 0;
	vertices1[0][2] = 0;
	vertices1[1][2] = 5;
  cout << " vtx" << endl; sleep(1);
    CPolytope* p1;
	CPolytope* p2;
    p1 = new CPolytope(&NofDim, &Npoints1,  (const double** const)  vertices1 );
    algGeom->VertexEnumeration(p1);
	algGeom->FacetEnumeration(p1);
    cout << p1 << endl;
	sleep(1);
	
    for(int k=0; k<NofDim;k++)
	{
	   vertices2[k] = new double[Npoints2];
	}
	vertices2[0][0] = -1;
	vertices2[1][0] = -1;
	vertices2[0][1] = -1;
	vertices2[1][1] = +1;
	vertices2[0][2] = +1;
	vertices2[1][2] = +1;
	vertices2[0][3] = +1;
	vertices2[1][3] = -1;
    p2 = new CPolytope(&NofDim, &Npoints1,  (const double** const) vertices2 );
    algGeom->VertexEnumeration(p2);
	algGeom->FacetEnumeration(p2);
    cout << p2 << endl;
	int counter = 0;
    double vInt = pOCPspecs->polyIntersectionVolume(p1,p2);
    cout << " " << vInt << endl;
	while(true)
	{
        cout << "sleeping: "<< ++counter << endl;
        sleep(1);
       // float cost = pOCPspecs->getCostMapValue(2,1);
//		cout << "Cost : " << cost << endl;
    //    point2 grad =  pOCPspecs->getCostMapGradient(2,1);
//		cout << "Gradient : " << grad.x<< " , "<< grad.y << endl;
    }
  
}
