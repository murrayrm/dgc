/*
 Declaration of the OCPTSPECS class that provides the 
 specifications for the trajectory generation problem
 

 Stefano Di Cairano mar-07
 */

#ifndef OCPTSPECS_HH_
#define OCPTSPECS_HH_

#include "NURBSBasedOTG/OTG_Interfaces.hh"
#include "AlgGeom/AlgebraicGeometry.hh"
#include "AlgGeom/Polytope.hh"
#include "interfaces/sn_types.h" 
#include "skynet/skynet.hh" 
#include "dgcutils/DGCutils.hh"
#include "skynettalker/SkynetTalker.hh"
#include "skynettalker/RDDFTalker.hh"
#include "pthread.h"
#include <iostream>
#include <unistd.h>
#include "ocpspecs/OTG_AliceInterface.hh"
#include "interfaces/TpDpInterface.hh"
#include <bitmap/Polygon.hh>
#include "CostMapEstimator.hh"
#include <boost/serialization/vector.hpp>
#include <highgui.h>

#define MAP_EXPANSION 5.0
#define GRAD_SMOOTHING 2


/*! flat output indices, in case we need to change something in the future */

#define I_z1    0
#define I_z1d   1
#define I_z1dd  2
#define I_z1ddd 3

#define I_z2    4
#define I_z2d   5
#define I_z2dd  6
#define I_z2ddd 7

#define I_z3    8

/*! 
 * constants used in the problem specification
*/

#define NZ 9
#define SCM_NROWS 250          // taken from map.default.dat
#define SCM_NCOLS 250 
#define SCM_RESROWS 0.40
#define SCM_RESCOLS 0.40
#define VERBOSITY 0
#define SCM_NODATA 0       //these have to be tuned depending on the generated cost function
#define SCM_OUTSIDEMAP 0
#define SCM_USEDELTA 1
#define N_STATUS 4
using namespace bitmap; 

/*! Structure to store quadratic (obstacle) constraints.
 * Constraints are stored by three arrays H, S, K
 * x'H[i]x+S[i]'x+K[i]
 */

struct OCPqConstraints 
{
    int Nconstr ;
    double H [MAX_OBSTACLES][2][2] ;
    double S [MAX_OBSTACLES][2] ;
    double K [MAX_OBSTACLES] ;

    OCPqConstraints()
    {
        Nconstr=0;
        for(int i=0;i<MAX_OBSTACLES;i++)
        {
            S[i][0] = 0;
            S[i][1] = 0;
            H[i][0][0] = 0;
            H[i][0][1] = 0; 
            H[i][1][1] = 0; 
            H[i][1][0] = 0;
            K[i] = 0;
        }
    }
    /*! 
     * default constructor. Set all the matrices/vectors to 0 
     * 
     * */
    OCPqConstraints(const OCPqConstraints& p)
    {        
        Nconstr = p.Nconstr;
        for(int i=0;i<MAX_OBSTACLES;i++)
        {
            S[i][0] =  p.S[i][0]; 
            S[i][1] =  p.S[i][1]; 
            H[i][0][0] =  p.H[i][0][0]; 
            H[i][0][1] =  p.H[i][0][1]; 
            H[i][1][1] =  p.H[i][1][1]; 
            H[i][1][0] =  p.H[i][1][0];
            K[i] = p.K[i];
        }
    }

/*! 
 * Serialization method
 */
 
    template <class Archive>
    void serialize(Archive &ar,const unsigned int version)
    {
    
        ar & Nconstr;
        ar & H;
        ar & S;
        ar & K;
    }
};

/*! The OCPSPECSFLAGS structure embeds all the OCPtSpecs constructor options.
 *  The variables starting with USE enable/disable the corresponding threads.
 *  The variables starting with SHOW enable/disable the visualization of the corresponding maps.
 *  the variable VERBOSE [0,3] defines verbosity level when printing debug messages
 */

struct OCPspecsFlags
{
	int skynetKey;
    bool useCostmap;
	bool useParams;
	bool useObstacles;
	bool useRDDF;
	bool usePolyCorridor;
	bool showCostmap;
	bool computeGradient;
	bool showGradientmap;
	int verbose;
  
    OCPspecsFlags()
    {
	    useCostmap = true;
		useParams = true;
		useObstacles = false;
		useRDDF = false;
		usePolyCorridor = true;
		verbose = 0;
		skynetKey = 0;
        showCostmap = true;
		computeGradient = false;
		showGradientmap = false;
	}
};



/*!   
 * OCPTSPECS class, main class that receives, stores, and provides the specification of the trajectory generation problem
 *
 */ 

class OCPtSpecs : public IOCPComponent, public IOCPparameters,  public CRDDFTalker
{ 
  
private:
    int m_i;
    int m_j;
    int m_skynetKey;
    //int m_useAstate;
    int m_quit;
    
    int m_nWaypoints;
    double** m_waypoints;
    
    SkynetTalker <DplannerStatus> m_statusTalker;
    DplannerStatus m_dplannerStatus; 
    pthread_mutex_t m_dplannerStatusMutex;
	DGCcondition m_notPausedCond;
	/*!
     * Mutex to access and modify specification Data, i.e., all the data used in the OCP
     */
//    pthread_mutex_t m_DataMutex;
  
    /*!
     * Member object to store the problem parameters
     */
    OCPparams m_OCPparams;    
    SkynetTalker <OCPparams> m_paramsTalker;  //skynetTalker Object to Receive Params
	DGCcondition m_paramsRecvdCond;
    pthread_mutex_t m_paramsDataMutex;
    bool m_useParams;
 

    /*!
     * Member object to store the quadratic constraints
     */
    OCPqConstraints m_Qconstr;
	DGCcondition m_obstRecvdCond;
    SkynetTalker <OCPobstacles> m_obstTalker;  //skynetTalker Object to Receive Obstacles
    pthread_mutex_t m_obstDataMutex;
    bool m_useObstacles;

    /*!
     * Member object to store the RDDF that specifies the corridor
     */
    RDDF m_RDDF;
    int m_RDDFSocket; 
    DGCcondition m_RDDFRecvdCond;
    pthread_mutex_t m_rddfDataMutex;
    bool m_useRDDF;
     
	/*  Polytopic corridor */ 

    CPolytope** m_polyCorridor;
    int m_nPolytopes;
	DGCcondition m_polyCorridorRecvdCond;
    pthread_mutex_t m_polyCorridorDataMutex;
    bool m_usePolyCorridor;
	/*!
     * Member object that defines the static cost map
     */
    CostMapEstimator m_costMapEst;
	CostMap m_costMap;
	bool m_useCostMap;                          // activating the static cost map
    CostMap m_costDx;
	CostMap m_costDy;
	DGCcondition m_costMapRecvdCond;
    pthread_mutex_t m_costmapDataMutex;
    bool m_showCostmap;
	bool m_computeGradient;
	bool m_showGradientmap;
    /*! 
     * Flag to turn on verbose messages
     */
    int m_verbose;		

    /*! 
     * Thread to receive the obstacles
     */
    void getOCPobstThread();

    void getCostMapThread();
    
    /*! 
     * Thread to receive the parameters
     */
    void getOCPparamsThread();

    /*! 
     * Thread to receive the RDDF that defines the corridor
     */
    void getRDDFThread();

    void getPolyCorridorThread();

    void supervisoryThread(void);
public:
    
    /*! 
     * Constructor
     */
    OCPtSpecs(int sn_key, bool useAstate=false, int verbose = 0, bool useCostMap=true, bool useObst=true, bool useParams=true, bool useRDDF=true, bool usePolyCorridor = true, bool showCostmap = true, bool computeGradient = false, bool showGradient = false);
    
    OCPtSpecs(OCPspecsFlags ocpFlags);
        
    /*! 
     * Destructor 
     */
    virtual  ~OCPtSpecs(void);
   
    int  setStatus(int index, int value);
    
	void printPolyCorridor(void);
    /*!
     * Specifies all the dimensions and flags related to the coded OC Problem. 
     */
    virtual  void getOCPComponentSizes(OCPComponentSizes* const ocpComponentSizes);
    /*! Implement the cost function applicable for the begining of the time interval.*/
    virtual   void InitialCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Fi, double* const DFi);
  
    /*! Implement the cost function for the whole time interval.*/
    virtual   void TrajectoryCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ft, double* const DFt);
  
    /*! Implement the cost function applicable at the end of the time interval.*/
    virtual      void FinalCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ff, double* const DFf);
  
    /*! Implement the linear constraints - those applicable at the beginning of the time interval, for the whole time interval and for those at the end of the time interval.*/
    virtual void getLinearConstraints(const ConstraintType* const ctype, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A, const int* const Offset);
  
    /*! Implement the nonlinear constraints applicable at the beginning of the time interval.*/
    virtual void InitialNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli, const int* const Offset) ;
  
  
    /*! Implement the nonlinear constraints applicable during the whole time interval.*/
    virtual void TrajectoryNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt, const int* const Offset);
  
    /*! Implement the nonlinear constraints applicable at the end of the time interval.*/
    virtual void FinalNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf, const int* const Offset);
  
    /*! Implement the bounds for all the constraints: linear and nonlinear.*/
    virtual void getLUBounds(const ConstraintType* const ctype, const BoundsType* const ubtype, const int* const i,const double* const t, const int* const NofConstraints, double* const LUBounds, const int* const Offset);

    /* Implement functions of IOCPparameters */
    /*!
     * returns the initial condition
     */
    virtual void getInitialCondition(const BoundsType* const ubtype, double* const initCond);
    
    /*!
     * returns the final condition
     */
    virtual void getFinalCondition(const BoundsType* const ubtype, double* const finalCond);
    
    /*! 
     * returns the parameters
     */
    virtual void getParams(double* const params);
    
    /*!
     * returns the mode (FWD/REV so far)
     */
    virtual void getMode(int* mode);
    
    /*! 
     * call this when start the solution of tarjectory generation problem. 
     * This locks all the mutices to ensure data consistency along the problem solution
     */
    virtual void startSolve(void);

    /*! 
     * call this when finished the solution of tarjectory generation problem. 
     * This unlocks all the mutices to ensure data consistency along the problem solution
     */
    virtual void endSolve(void);
    
    /*!
     * this returns the RDDF specifying the corridor
     */    
    virtual void getRDDFcorridor(RDDF* pNewRDDF);

    void getSurfaceControlPoints(int windowsSize, TimeInterval* yInterval, TimeInterval* xInterval, double* dx, double* dy, double** controlPoints );

    double getCostMapValue(double x, double y);
    
    double getGradientMapValue(double x, double y, int component);
    
	point2 getCostMapGradient(double x, double y);
    
    void getGradientMap();
	
	int isInsideCorridor(double* x, int dim);

    int isInsideCorridor(double* x, int dim, int initIdx);

	int getNpolytopes();
    
	void getPolytopes(CPolytope* const polytopes);
     
	double polyIntersectionVolume(CPolytope* p1, CPolytope* p2);
    
	void getLegalEndPoint(int NofDim, double* illegalEndPoint, double* legalEndPoint);

    void getLegalInitialPoint(int NofDim, double* illegalInitialPoint, double* legalInitialPoint);

    double corridorCostToGo(double* initialPoint, double* finalPoint);
    
    int getActivePolytopes(CPolytope* const polytopes);
    
    int getNofActivePolytopes();
    
    int getNofActiveWaypoints();
    
    int getActiveWaypoints(double** const waypoints);

};

#endif /*OCPTSPECS_HH_*/
