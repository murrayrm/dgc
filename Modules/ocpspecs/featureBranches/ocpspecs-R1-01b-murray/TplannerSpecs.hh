/*
 Declaration of the TplannerSpecs class that provides the 
 specifications for the trajectory generation problem
 

 Stefano Di Cairano mar-07
 */

#ifndef TplannerSpecs_HH_
#define TplannerSpecs_HH_

#include "cmap/CMapPlus.hh"
#include "cmap/MapdeltaTalker.hh"
#include "interfaces/sn_types.h" 
#include "skynet/skynet.hh" 
#include "skynet/SkynetContainer.hh"
#include "dgcutils/DGCutils.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/SkynetTalker.hh"
#include "skynettalker/RDDFTalker.hh"
#include "pthread.h"
#include <iostream>
#include <unistd.h>
#include "interfaces/TpDpInterface.hh"

/*! 
 * constants used in the problem specification
*/

#define NZ 9
#define SCM_NROWS 250          // taken from map.default.dat
#define SCM_NCOLS 250 
#define SCM_RESROWS 0.40
#define SCM_RESCOLS 0.40
#define VERBOSITY 0
#define SCM_NODATA 100       //these have to be tuned depending on the generated cost function
#define SCM_OUTSIDEMAP 90
#define SCM_USEDELTA 1
#define N_STATUS 4
/*! Structure to store quadratic (obstacle) constraints.
 * Constraints are stored by three arrays H, S, K
 * x'H[i]x+S[i]'x+K[i]
 */
 
struct OCPqConstraints 
{
    int Nconstr ;
    double H [MAX_OBSTACLES][2][2] ;
    double S [MAX_OBSTACLES][2] ;
    double K [MAX_OBSTACLES] ;

    OCPqConstraints()
    {
        Nconstr=0;
        for(int i=0;i<MAX_OBSTACLES;i++)
	{
            S[i][0] = 0;
	    S[i][1] = 0;
            H[i][0][0] = 0;
            H[i][0][1] = 0; 
	    H[i][1][1] = 0; 
	    H[i][1][0] = 0;
            K[i] = 0;
        }
    }
    /*! 
     * default constructor. Set all the matrices/vectors to 0 
     * 
     * */
    OCPqConstraints(const OCPqConstraints& p)
    {        
        Nconstr = p.Nconstr;
        for(int i=0;i<MAX_OBSTACLES;i++)
        {
            S[i][0] =  p.S[i][0]; 
            S[i][1] =  p.S[i][1]; 
            H[i][0][0] =  p.H[i][0][0]; 
            H[i][0][1] =  p.H[i][0][1]; 
            H[i][1][1] =  p.H[i][1][1]; 
            H[i][1][0] =  p.H[i][1][0];
            K[i] = p.K[i];
	}
    }

/*! 
 * Serialization method
 */
 
    template <class Archive>
    void serialize(Archive &ar,const unsigned int version)
    {
        ar & Nconstr;
        ar & H;
        ar & S;
        ar & K;
    }
};

enum BoundsType{ubt_LowerBound, ubt_UpperBound};

/*!   
 * OCPTSPCES class, main class that receives, stores, and provides the specification of the trajectory generation problem
 *
 */ 

class TplannerSpecs : public CMapdeltaTalker, public CStateClient, 
			public CRDDFTalker
{ 
  
private:
    int m_i;
    int m_j;
    int m_skynetKey;
    int m_useAstate;
   
    SkynetTalker <DplannerStatus> m_statusTalker;
    DplannerStatus m_dplannerStatus; 
    /*!
     * Mutex to access and modify specification Data, i.e., all the data used in the OCP
     */
    pthread_mutex_t m_specsDataMutex;
  
    /*!
     * Member object to store the problem parameters
     */
    OCPparams m_OCPparams;    
    SkynetTalker <OCPparams> m_paramsTalker;  //skynetTalker Object to Receive Params
    pthread_mutex_t m_paramsRecvdMutex;
    pthread_cond_t m_paramsRecvdCond;
    bool m_paramsRecvdOne;
    bool m_useParams;
 

    /*!
     * Member object to store the quadratic constraints
     */
    OCPqConstraints m_Qconstr;
    pthread_mutex_t m_obstRecvdMutex;
    pthread_cond_t m_obstRecvdCond;
    bool m_obstRecvdOne;
    bool m_useObstacles;
    SkynetTalker <OCPobstacles> m_obstTalker;  //skynetTalker Object to Receive Obstacles
 
    /*!
     * Member object to store the RDDF that specifies the corridor
     */
    RDDF m_RDDF;
    int m_RDDFSocket; 
    pthread_mutex_t m_RDDFRecvdMutex;
    pthread_cond_t m_RDDFRecvdCond;
    bool m_RDDFRecvdOne;
    bool m_useRDDF;
 
    /*!
     * Member object that defines the static cost map
     */
    CMapPlus* m_pStaticCostMap;
    int m_scmapLayer;
    char* m_pScmapDelta;
    bool m_scmapRecvdOneDelta;                // conditional mutex on received deltas
    pthread_mutex_t	m_scmapDeltaRecvdMutex;   // mutex to receive static cost map deltas
    pthread_cond_t m_scmapDeltaRecvdCond;     // condition on scmap deltas
    bool m_useScmap;                          // activating the static cost map
    int m_scmapRequestSocket;                 // socket to request the full map
    int m_scmapDeltaSocket;                   // socket to receive deltas

    /*! 
     * Flag to turn on verbose messages
     */
    int m_verbose;		

    /*! 
     * Thread to receive the obstacles
     */
    void getOCPobstThread();

    /*! 
     * Thread to receive the static cost map deltas
     */  
    void getScmapDeltasThread();  //thread to read deltas
    
    /*! 
     * Thread to request the map
     */
    void scmapRequest();   //function that request the full map
 
    /*! 
     * Thread to receive the parameters
     */
    void getOCPparamsThread();

    /*! 
     * Thread to receive the RDDF that defines the corridor
     */
    void getRDDFThread();


public:
    
    /*! 
     * Constructor
     */
    TplannerSpecs(int sn_key, bool useAstate=false, int verbose = 0, bool useScmap=true, bool useObst=true, bool useParams=true, bool useRDDF=true);
    
    /*! 
     * Destructor 
     */
    virtual  ~TplannerSpecs(void);
   
    int  setStatus(int index, int value);

    /*! 
     * returns the parameters
     */
    virtual void getParams(double* const params);
    
    /*!
     * returns the mode (FWD/REV so far)
     */
    virtual void getMode(int* mode);
    
    /*! 
     * call this when start the solution of tarjectory generation problem. 
     * This locks all the mutices to ensure data consistency along the problem solution
     */
    virtual void startSolve(void);

    /*! 
     * call this when finished the solution of tarjectory generation problem. 
     * This unlocks all the mutices to ensure data consistency along the problem solution
     */
    virtual void endSolve(void);
    
    /*!
     * this returns the RDDF specifying the corridor
     */    
    virtual void getRDDFcorridor(RDDF* pNewRDDF);

    /*!
     * returns the initial condition
     */
    virtual void getInitialCondition(const BoundsType* const ubtype, double* const initCond);
    
    /*!
     * returns the final condition
     */
    virtual void getFinalCondition(const BoundsType* const ubtype, double* const finalCond);
    
};

#endif /*OCPTSPECS_HH_*/
