/*
 Declaration of the OCPTSPECS class that provides the 
 specifications for the trajectory generation problem
 

 Stefano Di Cairano mar-07
 */

#ifndef OCPTSPECS_HH_
#define OCPTSPECS_HH_

#include "NURBSBasedOTG/OTG_Interfaces.hh"
#include "cmap/CMapPlus.hh"
#include "cmap/MapdeltaTalker.hh"
#include "interfaces/sn_types.h" 
#include "skynet/skynet.hh" 
#include "skynet/SkynetContainer.hh"
#include "dgcutils/DGCutils.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/SkynetTalker.hh"
#include "skynettalker/RDDFTalker.hh"
#include <pthread.h>
#include <iostream>
#include <unistd.h>
#include "OTG_AliceInterface.hh"
#include "interfaces/TpDpInterface.hh"
#include "interfaces/AliceFlat_Definitions.hh"   //here the indices are defined

#include "TplannerSpecs.hh"

/* Flat outputs: Defined in dplanner/AliceFlat_Definitions.hh
 *  z1 = x, z2 = y, z3 = t 
 */

/*! flat output indices, in case we need to change something in the future */

#define I_z1    0
#define I_z1d   1
#define I_z1dd  2
#define I_z1ddd 3

#define I_z2    4
#define I_z2d   5
#define I_z2dd  6
#define I_z2ddd 7

#define I_z3    8

/*!   
 * OCPTSPCES class, main class that receives, stores, and provides the specification of the trajectory generation problem
 *
 */ 

class OCPtSpecs : public IOCPComponent, public IOCPparameters, 
		  public TplannerSpecs
{ 
private:

public:
    
    /*! 
     * Constructor
     */
    OCPtSpecs(int sn_key, bool useAstate=false, int verbose = 0, bool useScmap=true, bool useObst=true, bool useParams=true, bool useRDDF=true);
    
    /*! 
     * Destructor 
     */
    virtual  ~OCPtSpecs(void);
   
    /*!
     * Specifies all the dimensions and flags related to the coded OC Problem. 
     */
    virtual  void getOCPComponentSizes(OCPComponentSizes* const ocpComponentSizes);
    /*! Implement the cost function applicable for the begining of the time interval.*/
    virtual   void InitialCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Fi, double* const DFi);
  
    /*! Implement the cost function for the whole time interval.*/
    virtual   void TrajectoryCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ft, double* const DFt);
  
    /*! Implement the cost function applicable at the end of the time interval.*/
    virtual      void FinalCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ff, double* const DFf);
  
    /*! Implement the linear constraints - those applicable at the beginning of the time interval, for the whole time interval and for those at the end of the time interval.*/
    virtual void getLinearConstraints(const ConstraintType* const ctype, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A, const int* const Offset);
  
    /*! Implement the nonlinear constraints applicable at the beginning of the time interval.*/
    virtual void InitialNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli, const int* const Offset) ;
  
  
    /*! Implement the nonlinear constraints applicable during the whole time interval.*/
    virtual void TrajectoryNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt, const int* const Offset);
  
    /*! Implement the nonlinear constraints applicable at the end of the time interval.*/
    virtual void FinalNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf, const int* const Offset);
  
    /*! Implement the bounds for all the constraints: linear and nonlinear.*/
    virtual void getLUBounds(const ConstraintType* const ctype, const BoundsType* const ubtype, const int* const i,const double* const t, const int* const NofConstraints, double* const LUBounds, const int* const Offset);

    /* Implement functions of IOCPparameters */
};

#endif /*OCPTSPECS_HH_*/
