/*
 Definition of the TplannerSpecs class that provides the interface
 between Tplanner and Dplanner

 Stefano Di Cairano mar-07
 */

#include "TplannerSpecs.hh"
#include "interfaces/TpDpInterface.hh"
#define MAX_DELTA_SIZE 1000000  //taken from plannerModule
#define center_N 0.0           //taken from map.default.dat
#define center_E 0.0

TplannerSpecs::TplannerSpecs(int sn_key, bool useAstate, int verbose, bool useScmap, bool useObst, bool useParams, bool useRDDF)
    : CSkynetContainer(MODdynamicplanner, sn_key),
      CStateClient(false),
      CRDDFTalker(false),
      m_scmapRecvdOneDelta(false),
      m_paramsRecvdOne(false),
      m_paramsTalker(sn_key,SNocpParams,MODdynamicplanner),
      m_statusTalker(sn_key,SNdplStatus,MODdynamicplanner),
      m_obstRecvdOne(false),
      m_obstTalker(sn_key,SNocpObstacles,MODdynamicplanner),
      m_RDDFRecvdOne(false),
      m_verbose(verbose)
{
        
    m_skynetKey = sn_key;
	
	//setting member vars that control threads
    m_useParams = useParams;
    m_useObstacles = useObst;
    m_useRDDF = useRDDF;
    m_useScmap = useScmap;


	//create the mutex for specs data: only one mutex to have better data consistency
    DGCcreateMutex(&m_specsDataMutex);

    //initialize costmap and related mutexes and sockets
    m_useAstate = useAstate;
    if(m_useAstate)
    {
        cerr << "Warning: using State from AState" << endl;
    }
    if(m_useScmap)
    {
        m_pScmapDelta = new char[MAX_DELTA_SIZE];   //this is the buffer for the deltas
        m_pStaticCostMap = new CMapPlus();
        m_pStaticCostMap->initMap(center_N, center_E, SCM_NROWS, SCM_NCOLS, SCM_RESROWS, SCM_RESCOLS, VERBOSITY); // initialize the map qith appropriate size
        m_scmapLayer = m_pStaticCostMap->addLayer<double>(SCM_NODATA,SCM_OUTSIDEMAP,SCM_USEDELTA); //add the staic cost layer

        DGCcreateMutex(&m_scmapDeltaRecvdMutex);     //mutex for the deltas
        DGCcreateCondition(&m_scmapDeltaRecvdCond);  //condition on deltas reception
	
        m_scmapRequestSocket = m_skynet.get_send_sock(SNtplannerStaticCostMapRequest);  //put skynet interface here
        m_scmapDeltaSocket =  m_skynet.listen(SNtplannerStaticCostMap,MODtrafficplanner); //put skynet interface here   
    }
		
    //parameters
    if(m_useParams)
    {
        DGCcreateMutex(&m_paramsRecvdMutex);     //mutex for the deltas
        DGCcreateCondition(&m_paramsRecvdCond);  //condition on deltas reception
    }

    //obstacles
    if(m_useObstacles)
    {
        DGCcreateMutex(&m_obstRecvdMutex);     //mutex for the deltas
        DGCcreateCondition(&m_obstRecvdCond);  //condition on deltas reception
    }
    
    //RDDF receiver
    if(m_useRDDF)
    {
        m_RDDFSocket =  m_skynet.listen(SNrddf,MODtrafficplanner); //put skynet interface here   
        DGCcreateMutex(&m_RDDFRecvdMutex);     //mutex for the deltas
        DGCcreateCondition(&m_RDDFRecvdCond);  //condition on deltas reception
    }
    
    //start the threads that listen for data
    if(m_useRDDF)  
        DGCstartMemberFunctionThread(this, &TplannerSpecs::getRDDFThread) ;
       
    if(m_useParams)  
        DGCstartMemberFunctionThread(this, &TplannerSpecs::getOCPparamsThread) ;
        
    if(m_useObstacles)  
        DGCstartMemberFunctionThread(this, &TplannerSpecs::getOCPobstThread) ;
        
    if(m_useScmap)  
        DGCstartMemberFunctionThread(this, &TplannerSpecs::getScmapDeltasThread); //start the thread to catch the map deltas 

    //	scmapRequest(); // send a request for the full map    
}

void TplannerSpecs::scmapRequest()
{
    bool bRequestScmap = true;
    m_skynet.send_msg(m_scmapRequestSocket,&bRequestScmap, sizeof(bool) , 0);
}

void TplannerSpecs::getScmapDeltasThread()
{
    while(true)
    {
        int deltasize;		
        if (m_verbose) cout << "Waiting for SCM Deltas " << endl;
        if(m_useAstate)
        {
            m_pStaticCostMap->updateVehicleLoc(m_state.localX, m_state.localY);
        }
        else
        {
            DGClockMutex(&m_specsDataMutex);
            m_pStaticCostMap->updateVehicleLoc(m_OCPparams.initialConditionLB[NORTHING_IDX_C], m_OCPparams.initialConditionLB[EASTING_IDX_C]);
            DGCunlockMutex(&m_specsDataMutex); 
        }

        deltasize = m_skynet.get_msg(m_scmapDeltaSocket, m_pScmapDelta, MAX_DELTA_SIZE, 0);
        if(deltasize>0)
        {
            DGClockMutex(&m_specsDataMutex);
	if (m_verbose) cout << "Applying SCM Deltas " << endl;
            m_pStaticCostMap->applyDelta<double>(m_scmapLayer, m_pScmapDelta, deltasize);	
		  
        if(!m_scmapRecvdOneDelta)
        {
            DGCSetConditionTrue(m_scmapRecvdOneDelta, m_scmapDeltaRecvdCond, m_scmapDeltaRecvdMutex);
        }
		   DGCunlockMutex(&m_specsDataMutex); 
        }
	}
}


void TplannerSpecs::getOCPparamsThread()
{
    bool paramsCorrect;
    OCPparams tempParams;
	while(true)
	{	
		if (m_verbose) 
		    cout << "Waiting for Params" << endl;
        paramsCorrect = false;
        paramsCorrect = m_paramsTalker.receive(&tempParams);
		if(! paramsCorrect)
		{
		    cerr << "error in Parameters message" << endl;
        }
		else
		{
		    DGClockMutex(&m_specsDataMutex);
		    m_OCPparams = tempParams;
		    if(m_verbose) 
		        cout << "I am applying params. g: " << m_OCPparams.parameters[11] <<endl;     
		    if(!m_paramsRecvdOne)
		        DGCSetConditionTrue(m_paramsRecvdOne, m_paramsRecvdCond, m_paramsRecvdMutex);   
            DGCunlockMutex(&m_specsDataMutex);
		}
	}
}

void TplannerSpecs::getRDDFThread()
{
    bool RDDFcorrect;
    RDDF tempRDDF;
    while(true)
    {
        // Reset the tempRDDF so that we always load full RDDF
        tempRDDF.clear();

	    // Now read in the data from the socket
        RDDFcorrect = RecvRDDF(m_RDDFSocket, &tempRDDF);

	    if (m_verbose > 1)
	        cout << "TplannerSpecs::getRDDFThread recieved RDDF" << endl;
	
	    if(!RDDFcorrect)
	    {
	        cerr <<"Error while recv RDDF" << endl;
	    }
	    else
	    {
            DGClockMutex(&m_specsDataMutex);
	        m_RDDF = tempRDDF;
	        if (m_verbose > 1)
	          cout << "TplannerSpecs::getRDDFThread RDDF object copied"<< endl;
	        if (m_verbose > 2) tempRDDF.print();
	        DGCunlockMutex(&m_specsDataMutex);
        }
        if(!m_RDDFRecvdOne)
        {
            DGCSetConditionTrue(m_RDDFRecvdOne, m_RDDFRecvdCond, m_RDDFRecvdMutex);
	    }   				
    }
}

void TplannerSpecs::getOCPobstThread()
{
    bool obstCorrect;
    OCPobstacles tempObst;
	while(true)
	{		
	    if(m_verbose) 
	        cout << "Waiting for obstacles" << endl;
        obstCorrect = false;
        obstCorrect = m_obstTalker.receive(&tempObst);
		if(!obstCorrect)
		{
		    cerr << "TplannerSpecs:getOCPobstThread error" << endl;
        }
		else
		{
		    if (m_verbose) 
		        cout << "Applying obstacles: " << tempObst.ObstNumber <<  endl;
                    
		    //DGClockMutex(&m_obstMutex);
            DGClockMutex(&m_specsDataMutex);
            m_Qconstr.Nconstr=tempObst.ObstNumber;            
            // Obstacles are sent by mapper as (x-xc)H(x_xc)<=1
            // We store them as   xHx+Sx+Kx <= 1
            // Hence here we compute the H, S, K components and store them
	        for(int i=0;i<tempObst.ObstNumber;i++)
		    {
                m_Qconstr.S[i][0] = -2*( tempObst.ObstHessian[i][0][0]*tempObst.ObstCenter[i][0] + 
			                tempObst.ObstHessian[i][0][1]*tempObst.ObstCenter[i][1]) ;
	            m_Qconstr.S[i][1] = -2*( tempObst.ObstHessian[i][1][0]*tempObst.ObstCenter[i][0] + 
			                tempObst.ObstHessian[i][1][1]*tempObst.ObstCenter[i][1]) ;
                m_Qconstr.H[i][0][0] = tempObst.ObstHessian[i][0][0];
                            m_Qconstr.H[i][0][1] = tempObst.ObstHessian[i][0][1]; 
	            m_Qconstr.H[i][1][1] = tempObst.ObstHessian[i][1][1]; 
	            m_Qconstr.H[i][1][0] = tempObst.ObstHessian[i][1][0];
                m_Qconstr.K[i] = tempObst.ObstCenter[i][0] * tempObst.ObstHessian[i][0][0] * tempObst.ObstCenter[i][0]  +
			                2 * tempObst.ObstCenter[i][0] * tempObst.ObstHessian[i][0][1] * tempObst.ObstCenter[i][1] +
						    tempObst.ObstCenter[i][1] * tempObst.ObstHessian[i][1][1] * tempObst.ObstCenter[i][1];                         
		    }
            //zeroing the unapplied constraints -> this is not necessary, but for now ....
	        for(int i=tempObst.ObstNumber;i<MAX_OBSTACLES;i++)
	        {
                m_Qconstr.S[i][0] = 0;
	            m_Qconstr.S[i][1] = 0;
                m_Qconstr.H[i][0][0] = 0;
                m_Qconstr.H[i][0][1] = 0; 
	            m_Qconstr.H[i][1][1] = 0; 
	            m_Qconstr.H[i][1][0] = 0;
                m_Qconstr.K[i] = 0;
	        }
		    
		    if(!m_obstRecvdOne)
		        DGCSetConditionTrue(m_obstRecvdOne, m_obstRecvdCond, m_obstRecvdMutex);  
            //DGCunlockMutex(&m_obstMutex);
            DGCunlockMutex(&m_specsDataMutex);
        }
    }
}


TplannerSpecs::~TplannerSpecs(void)
{
	//check: I don't think this is enough!
    delete m_pScmapDelta ;  
    delete m_pStaticCostMap ;
}

void TplannerSpecs::getRDDFcorridor(RDDF* pNewRDDF)
{
    *pNewRDDF =  m_RDDF;
    return ;
}

void  TplannerSpecs::getParams(double* const params)
{


    for(m_i=0;m_i<PARAMS_NUMBER;m_i++)
    {
        params[m_i]=m_OCPparams.parameters[m_i];	  
    }
    params[12] = m_OCPparams.initialConditionLB[VELOCITY_IDX_C];
    params[13] = m_OCPparams.finalConditionLB[VELOCITY_IDX_C];
    params[14] = m_OCPparams.initialConditionLB[ACCELERATION_IDX_C];
    params[15] = m_OCPparams.finalConditionLB[ACCELERATION_IDX_C];
   
    return ;
}

void  TplannerSpecs::getMode(int* mode)
{     
    *mode = m_OCPparams.mode; 
    return ;
}

void TplannerSpecs::startSolve()
{
    if(m_verbose)
        cout << "Request to solve" <<endl; 
    if(m_useRDDF)
        DGCWaitForConditionTrue(m_RDDFRecvdOne, m_RDDFRecvdCond, m_RDDFRecvdMutex);
    
    if(m_useParams)
        DGCWaitForConditionTrue(m_paramsRecvdOne, m_paramsRecvdCond, m_paramsRecvdMutex);
    
    if(m_useObstacles)
        DGCWaitForConditionTrue(m_obstRecvdOne, m_obstRecvdCond, m_obstRecvdMutex);
    
    if(m_useScmap)
        DGCWaitForConditionTrue(m_scmapRecvdOneDelta, m_scmapDeltaRecvdCond, m_scmapDeltaRecvdMutex);
    

    DGClockMutex(&m_specsDataMutex);    
    if(m_verbose)
        cout << "Ready to solve."<< endl;    
    return ;
}

void TplannerSpecs::endSolve()
{

/*
    DGCSetConditionFalse(m_scmapRecvdOneDelta, m_scmapDeltaRecvdCond, m_scmapDeltaRecvdMutex);
    DGCSetConditionFalse(m_paramsRecvdOne, m_paramsRecvdCond, m_paramsRecvdMutex);
    DGCSetConditionFalse(m_obstRecvdOne, m_obstRecvdCond, m_obstRecvdMutex);
  */  
    
    bool result = m_statusTalker.send(&m_dplannerStatus);    
    DGCunlockMutex(&m_specsDataMutex);    
    
    if(m_verbose)
        cout << "Finished solving" << endl; 
    return ;
}

int TplannerSpecs::setStatus(int index, int value)
{
    if(index<= 0 || index >= N_STATUS)
        return 0;
    else
    {
        switch (index)
	{
	    case 1 : 
	        m_dplannerStatus.OCPsolved = (bool)value;
		break;
	    case 2 : 
	        m_dplannerStatus.solverFlag = value;
		break;
	    case 3 : 
	        m_dplannerStatus.corridorGenerated = (bool)value;
		break;
	    case 4 : 
	        m_dplannerStatus.corridorFlag = value;
	        break;
	    default : 
	        return 0;

	 }  	      

        return 1;
    }

}

void  TplannerSpecs::getInitialCondition(const BoundsType* const ubtype, double* const initCond)
{
    bool valuecheck = false;
    if( *ubtype == ubt_LowerBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          initCond[m_i]=m_OCPparams.initialConditionLB[m_i];	  
        }
	valuecheck = true;
    }
    
    if( *ubtype == ubt_UpperBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          initCond[m_i]=m_OCPparams.initialConditionUB[m_i];	  
        }
	valuecheck = true;
    }
    if(valuecheck==false)
    {
        cout << "wrong value for initial bounds type" <<endl;
        exit(1);
    }

    return ;
}

void  TplannerSpecs::getFinalCondition(const BoundsType* const ubtype, double* const finalCond)
{
    bool valuecheck = false;
    if( *ubtype == ubt_LowerBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          finalCond[m_i]=m_OCPparams.finalConditionLB[m_i];	  
        }
	valuecheck = true;
    }
    
    if( *ubtype == ubt_UpperBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          finalCond[m_i]=m_OCPparams.finalConditionUB[m_i];	  
        }
	valuecheck = true;
    }
    if(valuecheck==false)
    {
        cout << "wrong value for final bounds type" <<endl;
        exit(1);
    }

    return ;
}
