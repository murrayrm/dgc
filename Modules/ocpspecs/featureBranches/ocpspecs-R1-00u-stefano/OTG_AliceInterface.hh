/*
	OTG_AliceInterface.hh
	OTG Interface specific for UGC Alice.
	Addendum to IOCP components.

	S. Di Cairano mar-07.
*/
#ifndef OTG_ALICEINTERFACE_HH
#define OTG_ALICEINTERFACE_HH

#include "NURBSBasedOTG/OTG_Interfaces.hh"
#include "dgcutils/RDDF.hh"

/*! This virtual class defines the methods that are used by OTG and that are specific to Alice/Urban Challenge */
class IOCPparameters
{
    public:

        /*! This returns the initial condition */
        virtual void getInitialCondition(const BoundsType* const ubtype, double* const initCond) = 0;
        /*! This returns the final condition */
        virtual void getFinalCondition(const BoundsType* const ubtype, double* const finalCond) = 0;
        /*! This returns the parameters */
        virtual void getParams(double* const params) = 0;
        /*! This returns the mode (FWD/REV) */
	    virtual void getMode(int* mode) = 0; 
        /*! This MUST be called before starting the OCP: it locks all the data for consistency */
        virtual void startSolve() = 0;
        /*! This MUST be called when the OCP is solved: it unlocks all the data for consistency */
	    virtual void endSolve() = 0;
        /*! This returns the RDDF-based corridor */
	    virtual void getRDDFcorridor(RDDF* pNewRDDF) = 0;
};


#endif /* OTG_ALICEINTERFACE_HH */

