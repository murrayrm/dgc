#include <iostream.h>
#include "ocpspecs/OCPtSpecs.hh"
//#include "OTG_Interfaces.hh"
int main(int argc, char** argv)
{

  int skynet_key;
  cout << "Please enter Skynet Key:" << endl;
  cin >> skynet_key;

//OCPtSpecs* pOCPspecs=new OCPtSpecs(skynet_key,true);

OCPtSpecs* pOCPspecs=new OCPtSpecs(skynet_key,true,3,true,true,true,true);

cout << "Ciao mondo!!"  << endl;
 int counter=0;
 while(true){
 sleep(2);
 pOCPspecs->startSolve();
 cout << "Solving slot: "<< counter << endl;
 counter++;
 pOCPspecs->endSolve();
 int NcPts =3;
 int Nconstr =2;
 int offset = 2;
 double bounds[Nconstr*NcPts+offset+1];
 ConstraintType cType = ct_NLinTrajectory;
 BoundsType bType = ubt_LowerBound;
 for(int i=0;i<Nconstr*NcPts+offset+1;i++)
 {
     bounds[i] = 0 ;
 }
 pOCPspecs->getLUBounds(&cType,&bType,&NcPts,&Nconstr,bounds,&offset);  
 cout << " Bounds :"; 
 for(int i=0;i<Nconstr*NcPts+offset+1;i++)
 {
     cout << "  " << bounds[i] ;
 }
 cout << endl;

 int mode = 2;
 int state = 1;
 int myI = 0; 
 double t = 0;
 int Nvars = NZ;
 double z[Nvars];
 int Nparams = 10;
 double params[Nparams];
 for(int i=0;i<Nparams;i++)
 {
     params[i]=0;
 }
 double Cnlt[Nconstr];
 double** DCnlt=new double*[offset+Nconstr];
 for(int i=0;i<offset+Nconstr;i++)
 {
    DCnlt[i]=new double[Nvars];
 }
 for(int i=0; i<Nconstr+offset; i++)
 {
   for(int j=0; j<Nvars; j++)
     DCnlt[i][j]=1;
 }
 //cout << DCnlt[Nconstr-1][Nvars-1] << endl;

 for(int i=0;i<Nvars;i++)
 {
     z[i]=0;
     
 }
 z[0]=-1;
 z[3]=3;
 //double** dbl;
 for(int i=0;i<Nconstr+offset;i++)
 {
     Cnlt[i]=1;
 }
 //cout << " matrix set" << endl;
// sleep(1);
cout << "My mode: " << mode << endl;
 pOCPspecs->TrajectoryNLConstraintFunction(&mode, &state, &myI, &t, &Nvars, z, &Nparams, params, &Nconstr, Cnlt, DCnlt, &offset);
 
 cout << "Constraints value: " << endl;
    for(int i=0;i<offset+Nconstr;i++)
    {
        cout << "  " << Cnlt[i] ;
    }
    cout << endl;

    cout << "Constraints Jacobian: " << endl;

    for(int i=0;i<offset+Nconstr;i++)
    {
        for (int j=0;j<Nvars;j++)
        cout << "  " << DCnlt[i][j] ;
 
        cout << endl;
    }

 cout << endl;
 
 }

}
