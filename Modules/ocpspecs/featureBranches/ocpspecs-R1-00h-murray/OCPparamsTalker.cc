
/*
 * Definition of the talker to send Optimal Control Problem parameters from 
 * Tplanner to Dplanner.
 *
 * S. Di Cairano mar 07
 *
 */
#include "OCPparamsTalker.hh"

using namespace std;

COCPparamsTalker::COCPparamsTalker() {
}

COCPparamsTalker::~COCPparamsTalker() {
}

bool COCPparamsTalker::RecvOCPparams(int OCPparamsSocket, OCPparams* params, int* paramsSize) {
	// receive a larger-than-possible message
	*paramsSize = m_skynet.get_msg(OCPparamsSocket, params, sizeof(OCPparams), 0);
	if(*paramsSize > 0) {
	  return true;
	}

	cerr << "CMapdeltaTalker::RecvMapdelta: Error!" << endl;
	return false;
}


bool COCPparamsTalker::SendOCPparams(int OCPparamsSocket, OCPparams* params )  {

    m_skynet.send_msg(OCPparamsSocket, params, 0);
  return true;
}
