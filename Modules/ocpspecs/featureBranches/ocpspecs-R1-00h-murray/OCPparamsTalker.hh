/*
 * Declaration of the talker to send Optimal Control Problem parameters from 
 * Tplanner to Dplanner.
 *
 * S. Di Cairano mar 07
 *
 */

#ifndef __OCPPARAMSTALKER_HH__
#define __OCPPARAMSTALKER_HH__

#include "pthread.h"
#include <iostream>
#include <unistd.h>
#include "skynet/SkynetContainer.hh"
#include "dgcutils/DGCutils.hh"
#include "OTG_InterfacesX.hh" 

#define PARAMS_NUMBER 12
#define COND_NUMBER 6

#define LENGTH_IDX_P 0
#define WEELB_IDX_P 1
#define HCG_IDX_P 2
#define VMIN_IDX_P 3
#define VMAX_IDX_P 4
#define AMIN_IDX_P 5
#define AMAX_IDX_P 6
#define PHIMIN_IDX_P 7
#define PHIMAX_IDX_P 8
#define PHIDMIN_IDX_P 9
#define PHIDMAX_IDX_P 10
#define G_IDX_P 11 
#define EASTING_IDX_C 0
#define NORTING_IDX_C 1
#define VELOCITY_IDX_C 2
#define HEADING_IDX_C 3
#define ACCELERATION_IDX_C 4
#define STEEERING_IDX_C 5


struct OCPparams
{
    int mode;
    double parameters [PARAMS_NUMBER];
    double initialConditionLB [COND_NUMBER];
    double initialConditionUB [COND_NUMBER];
    
    /*
    same structure as finalCondition
    */
       
    double finalConditionLB [COND_NUMBER];
    double finalConditionUB [COND_NUMBER];
 /*
     finalCondition[0] = Northing_min
     finalCondition[1] = Northing_max
     finalCondition[2] = Easting_min
     finalCondition[3] = Easting_max
     finalCondition[4]  = Heading_min
     finalCondition[5]  = Heading_max 
     finalCondition[6]  = Velocity_min (norm)
     finalCondition[7]  = Velocity_max (norm)
     finalCondition[8]  = Acceleration_min (norm)
     finalCondition[9]  = Acceleration_max (norm)
     finalCondition[10]  = SteeringAngle_min 
     finalCondition[11]  = SteeringAngle_max
      */
  
    OCPparams()
    {
    	//this is the fastest way to set everything to 0
        memset(parameters,'\0',PARAMS_NUMBER*sizeof(double));    
        memset(initialConditionLB,'\0',COND_NUMBER*sizeof(double));    
        memset(finalConditionLB,'\0',COND_NUMBER*sizeof(double));    
        memset(initialConditionUB,'\0',COND_NUMBER*sizeof(double));    
        memset(finalConditionUB,'\0',COND_NUMBER*sizeof(double));    
        //by default we drive fwd
        mode = md_FWD ;
        
        //setting parasmeters to the default values defined For Alice dynamics in dplanner
	    parameters[0]  = 5.43560 ;         // L
	    parameters[1]  = 2.13360 ;         // W
	    parameters[2]  = 1.06680 ;         // hcg
	    parameters[3]  = 0.00000 ;         // v_min
	    parameters[4]  = 1.40800 ;         // v_max
	    parameters[5]  =-3.00000 ;         // a_min
	    parameters[6]  = 0.98100 ;         // a_max
	    parameters[7]  =-0.44942 ;         // phi_min
	    parameters[8]  = 0.44942 ;         // phi_max
	    parameters[9]  =-1.30900 ;         // phid_min
	    parameters[10] = 1.30900 ;         // phid_max
	    parameters[11] = 9.81000 ;         // g
	    
    }
};


class COCPparamsTalker : virtual public CSkynetContainer {
public:
	COCPparamsTalker();
	~COCPparamsTalker();

	bool SendOCPparams(int OCPparamsSocket, OCPparams* params);
	bool RecvOCPparams(int OCPparamsSocket, OCPparams* params, int* paramsSize);
};

#endif // _OCPPARAMSTALKER_H_
