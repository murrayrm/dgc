/*
  Definition of the OCPTSPECS class that provides the interface
  between Tplanner and Dplanner

  Stefano Di Cairano mar-07
*/

#include "OCPtSpecs.hh"

//////////////////////////////////////////
///// CONSTRUCTORS/DESTRUCTORS  //////////
//////////////////////////////////////////

OCPtSpecs::OCPtSpecs(int sn_key, bool useAstate, int verbose, bool useCostMap, bool useObst, bool useParams, bool useRDDF, bool usePolyCorridor, bool showCostmap, bool computeGradient, bool showGradient)
  : CSkynetContainer(MODdynamicplanner, sn_key),
    CRDDFTalker(false),
    m_paramsTalker(sn_key,SNocpParams,MODdynamicplanner),
    m_statusTalker(sn_key,SNdplStatus,MODdynamicplanner),
    m_obstTalker(sn_key,SNocpObstacles,MODdynamicplanner),
    m_verbose(verbose),
    m_costMapEst(sn_key, showCostmap, false),
    m_useThreads(true)
{
  m_polyCorridor = NULL; //otherwise valgrind complains     
  m_skynetKey = sn_key;
  cout << "Skynet key: " << sn_key << endl; 
  m_quit = 0;	
  m_useParams = useParams;
  m_useObstacles = useObst;
  m_useRDDF = useRDDF;
  m_useCostMap = useCostMap;
  m_usePolyCorridor =usePolyCorridor;
  m_nPolytopes = 0;
  DGCcreateMutex(&m_dplannerStatusMutex);
  DGCcreateCondition(&m_notPausedCond);  
  //DGCstartMemberFunctionThread(this, &OCPtSpecs::supervisoryThread);  
  DGCcreateMutex(&m_paramsDataMutex);
  DGCcreateMutex(&m_obstDataMutex);
  DGCcreateMutex(&m_rddfDataMutex);
  DGCcreateMutex(&m_polyCorridorDataMutex);
  DGCcreateMutex(&m_costmapDataMutex);
  if(m_useParams)
    {
      DGCcreateCondition(&m_paramsRecvdCond);  
      DGCstartMemberFunctionThread(this, &OCPtSpecs::getOCPparamsThread) ;
    }
  if(m_useObstacles)
    {
      DGCcreateCondition(&m_obstRecvdCond); 
      DGCstartMemberFunctionThread(this, &OCPtSpecs::getOCPobstThread) ;
    }
  if(m_useRDDF)
    {
      m_RDDFSocket =  m_skynet.listen(SNrddf,MODtrafficplanner);   
      DGCcreateCondition(&m_RDDFRecvdCond); 
      DGCstartMemberFunctionThread(this, &OCPtSpecs::getRDDFThread) ;
    }    
  if(m_usePolyCorridor)
    {
      DGCcreateCondition(&m_polyCorridorRecvdCond);
      DGCstartMemberFunctionThread(this, &OCPtSpecs::getPolyCorridorThread);
    }
  if(m_useCostMap)
    {   
	    m_costMapEst.initMap(&m_costMap);
      m_costDx = m_costMap;   
      m_costDy = m_costMap;
      DGCcreateCondition(&m_costMapRecvdCond);  
      DGCstartMemberFunctionThread(this, &OCPtSpecs::getCostMapThread); 
    }
}

OCPtSpecs::OCPtSpecs(int sn_key, int verbose, bool useAstate, bool showCostmap, bool useThreads)
  : CSkynetContainer(MODdynamicplanner, sn_key),
    CRDDFTalker(false),
    m_paramsTalker(sn_key,SNocpParams,MODdynamicplanner),
    m_statusTalker(sn_key,SNdplStatus,MODdynamicplanner),
    m_obstTalker(sn_key,SNocpObstacles,MODdynamicplanner),
    m_verbose(verbose),
    m_costMapEst(sn_key, showCostmap, false),
    m_useThreads(useThreads)
{
  m_polyCorridor = NULL; //otherwise valgrind complains     
  m_skynetKey = sn_key;
  m_quit = 0;	
  m_useParams = true;
  m_useObstacles = false;
  m_useRDDF = false;
  m_useCostMap = true;
  m_usePolyCorridor = true;
  m_nPolytopes = 0;
  m_costMapEst.initMap(&m_costMap);
  m_costDx = m_costMap;   
  m_costDy = m_costMap;
}


OCPtSpecs::~OCPtSpecs(void)
{
	//check: I don't think this is enough!
  m_quit = 1;
	usleep(100000);
  if (m_useThreads) {
    DGCdeleteMutex(&m_rddfDataMutex);
    DGCdeleteMutex(&m_paramsDataMutex);
    DGCdeleteMutex(&m_obstDataMutex);
    DGCdeleteMutex(&m_costmapDataMutex);
    DGCdeleteMutex(&m_polyCorridorDataMutex);
    DGCdeleteCondition(&m_paramsRecvdCond);
    DGCdeleteCondition(&m_obstRecvdCond);
    DGCdeleteCondition(&m_costMapRecvdCond);
    DGCdeleteCondition(&m_RDDFRecvdCond);
    DGCdeleteCondition(&m_polyCorridorRecvdCond);
  }
}

///////////////////////////////////////
///////   RECEIVE THREADS    //////////
///////////////////////////////////////


void OCPtSpecs::getCostMapThread()
{
  bool mapRec = false;
  while(!m_quit)
    {
      DGClockMutex(&m_costmapDataMutex);
      mapRec = m_costMapEst.updateMap(&m_costMap);
      if(mapRec && m_computeGradient)
        {  
          cout << "Costmap received" << endl;
          getGradientMap();  
        }
      if( (mapRec) && (!m_costMapRecvdCond.bCond) )
        {
          DGCSetConditionTrue(m_costMapRecvdCond);
        }
      DGCunlockMutex(&m_costmapDataMutex);
      usleep(50);
    }
}

void OCPtSpecs::getOCPparamsThread()
{
  bool paramsCorrect = false;
  OCPparams tempParams;
	bool haveNewParams = false; 
	while(!m_quit)
    {	
      paramsCorrect = false;
      DGClockMutex(&m_paramsDataMutex);
      while(m_paramsTalker.hasNewMessage())
        {
          paramsCorrect = m_paramsTalker.receive(&tempParams);
          haveNewParams = true;
        }
      if( paramsCorrect && haveNewParams)
        {
          cout << "New final condition received: " << m_OCPparamsTmp.finalConditionLB[NORTHING_IDX_C]<< " " << m_OCPparamsTmp.finalConditionLB[EASTING_IDX_C] <<  " " << m_OCPparamsTmp.finalConditionLB[HEADING_IDX_C]<<endl;
          m_OCPparamsTmp = tempParams;
          if( (!m_paramsRecvdCond.bCond) && paramsCorrect)
            DGCSetConditionTrue(m_paramsRecvdCond);   
        }
      DGCunlockMutex(&m_paramsDataMutex);
      haveNewParams = false;
      usleep(50000);
    }
}

void OCPtSpecs::getRDDFThread()
{
  bool RDDFcorrect;
  RDDF tempRDDF;
  while(!m_quit)
    {   
	    
      RDDFcorrect = false;
      DGClockMutex(&m_rddfDataMutex);
      while(NewRDDF())
        {
          tempRDDF.clear();
          RDDFcorrect = RecvRDDF(m_RDDFSocket, &tempRDDF);
        }
	    if (m_verbose > 1)
        cout << "OCPtSpecs::getRDDFThread received RDDF" << endl;
	
	    if(!RDDFcorrect)
        {
	        cerr <<"Error while recv RDDF" << endl;
        }
	    else
        {
	        m_RDDF = tempRDDF;
	        if (m_verbose > 1)
            cout << "OCPtSpecs::getRDDFThread RDDF object copied"<< endl;
	        if (m_verbose > 2) tempRDDF.print();
        }
      if((!m_RDDFRecvdCond.bCond) && RDDFcorrect)
        DGCSetConditionTrue(m_RDDFRecvdCond);   				
	    DGCunlockMutex(&m_rddfDataMutex);
      usleep(50000);
    }
}

void OCPtSpecs::getOCPobstThread()
{
  bool obstCorrect;
  OCPobstacles tempObst;
	while(!m_quit)
    {		
      obstCorrect = false;
      DGClockMutex(&m_obstDataMutex);
      while(m_obstTalker.hasNewMessage())
        obstCorrect = m_obstTalker.receive(&tempObst);
      if(!obstCorrect)
        {
          cerr << "OCPtSpecs:getOCPobstThread error" << endl;
        }
      else
        {
          if (m_verbose) 
		        cout << "Applying obstacles: " << tempObst.ObstNumber <<  endl;
                    
          m_Qconstr.Nconstr=tempObst.ObstNumber;            

          // Obstacles are sent by mapper as (x-xc)H(x_xc)<=1
          // We store them as   xHx+Sx+Kx <= 1
          // Hence here we compute the H, S, K components and store them
	        for(int i=0;i<tempObst.ObstNumber;i++)
            {
              m_Qconstr.S[i][0] = -2*( tempObst.ObstHessian[i][0][0]*tempObst.ObstCenter[i][0] + 
                                       tempObst.ObstHessian[i][0][1]*tempObst.ObstCenter[i][1]) ;
	            m_Qconstr.S[i][1] = -2*( tempObst.ObstHessian[i][1][0]*tempObst.ObstCenter[i][0] + 
                                       tempObst.ObstHessian[i][1][1]*tempObst.ObstCenter[i][1]) ;
              m_Qconstr.H[i][0][0] = tempObst.ObstHessian[i][0][0];
              m_Qconstr.H[i][0][1] = tempObst.ObstHessian[i][0][1]; 
	            m_Qconstr.H[i][1][1] = tempObst.ObstHessian[i][1][1]; 
	            m_Qconstr.H[i][1][0] = tempObst.ObstHessian[i][1][0];
              m_Qconstr.K[i] = tempObst.ObstCenter[i][0] * tempObst.ObstHessian[i][0][0] * tempObst.ObstCenter[i][0]  +
                2 * tempObst.ObstCenter[i][0] * tempObst.ObstHessian[i][0][1] * tempObst.ObstCenter[i][1] +
						    tempObst.ObstCenter[i][1] * tempObst.ObstHessian[i][1][1] * tempObst.ObstCenter[i][1];                         
            }
          //zeroing the unapplied constraints -> this is not necessary, but for now ....
	        for(int i=tempObst.ObstNumber;i<MAX_OBSTACLES;i++)
            {
              m_Qconstr.S[i][0] = 0;
	            m_Qconstr.S[i][1] = 0;
              m_Qconstr.H[i][0][0] = 0;
              m_Qconstr.H[i][0][1] = 0; 
	            m_Qconstr.H[i][1][1] = 0; 
	            m_Qconstr.H[i][1][0] = 0;
              m_Qconstr.K[i] = 0;
            }
		    
        }
      if( (!m_obstRecvdCond.bCond) && obstCorrect)
		    DGCSetConditionTrue( m_obstRecvdCond);  
      DGCunlockMutex(&m_obstDataMutex);
      usleep(50000);
    }
}


void OCPtSpecs::getPolyCorridorThread()
{
    
  CAlgebraicGeometry* algGeom = new CAlgebraicGeometry();
  double** vertices;
	int NofDim = 2;
  SkynetTalker<sendCorr> recCorTalker= SkynetTalker<sendCorr>(m_skynetKey, SNpolyCorridor,MODdynamicplanner);
	sendCorr corridor;
  bool haveNewCorridor = false;
	bool corridorMsgCorrect;
	while(!m_quit)
    {
        
      haveNewCorridor = false;
      corridorMsgCorrect = true;
      DGClockMutex(&m_polyCorridorDataMutex);

      for(unsigned int i=0;i<corridor.size();i++)
        corridor[i].clear();
      corridor.clear();
      while(recCorTalker.hasNewMessage())
        {
          corridorMsgCorrect = recCorTalker.receive(&corridor);
          haveNewCorridor = true;
        }
      if( haveNewCorridor && corridorMsgCorrect )
        {
          if(m_polyCorridor != NULL)
            {
              for(int i=0;i<m_nPolytopes;i++)
                {
                  delete m_polyCorridor[i];
                  m_polyCorridor[i]= NULL; 
                }
              delete[] m_polyCorridor;
              m_polyCorridor = NULL;
            }
          //deleting waypoints
          if(m_waypoints != NULL)
            {
              for(int i=0;i<NofDim;i++)
                {
                  delete[] m_waypoints[i];
                  m_waypoints[i] = NULL;
                }
              delete[] m_waypoints;
              m_waypoints= NULL;
            }

          m_nPolytopes = corridor.size();
          m_polyCorridor=new CPolytope*[m_nPolytopes];
          bool isCorrect = true ;
          int centroidCorrect = -1;
          for(int i=0; i<m_nPolytopes; i++)
            {
              int polySize = corridor[i].size();
	            vertices = new double*[NofDim];
	            for(int j=0;j<NofDim;j++)
                vertices[j] = new double[polySize];
              for(int j=0;j<polySize;j++)
                {
                  vertices[0][j] = corridor[i][j].x ;
                  vertices[1][j] = corridor[i][j].y ;
                }    
              m_polyCorridor[i] = new CPolytope(&NofDim, &polySize, (const double**) vertices);
              ALGErrorType algErr = algGeom->VertexAndFacetEnumeration(m_polyCorridor[i]);
            
              if(algErr == alg_NoError)
                {
                  centroidCorrect = algGeom->ComputeCentroid(m_polyCorridor[i]);
                  if(centroidCorrect != 0)
                    {
                      cout << " Problems while extracting the centroid " << endl;
                      algErr = alg_HasRays;
                    }
                }
              if(algErr != alg_NoError)
                {
                  cout << "Corridor invalid. AlgGeom Error type (AlgGeom_Interfaces.hh) : " << algErr << endl;
                  cout << "Dimension : " << NofDim << " N of Vertices: "<< polySize <<  " vertices : " << endl;
                  for(int j = 0 ; j  < NofDim ; j++)
                    {
                      for(int k = 0; k < polySize ; k ++)
                        cout << " " << vertices[j][k] ;
                      cout << endl;
                
                    }
                  isCorrect = false ;                
                }
	            for(int j=0;j<NofDim;j++)
                delete[]  vertices[j];
              delete[] vertices;
            }
          if(isCorrect && (m_nPolytopes > 0))
            {
              if(!m_polyCorridorRecvdCond.bCond)
                {
                  cout << " a new valid corridor has been received" << endl;
                  /*
                    for(int k = 0; k< m_nPolytopes; k++)
                    cout << k << endl << m_polyCorridor[k] << endl;
                  */
                  DGCSetConditionTrue(m_polyCorridorRecvdCond);
						
                }
            }
          else
            {
              if(!isCorrect)
                {
                  cout << "Waiting for a new valid corridor on a mutex locked condition."  << endl;
                  DGCSetConditionFalse(m_polyCorridorRecvdCond);
                }
            }
        }	
      else
        {
          if(!corridorMsgCorrect)
            cout << "Bad corridor message received through skynet" << endl;
        }
      DGCunlockMutex(&m_polyCorridorDataMutex);
      usleep(50000);
    
    }//end of while (active loop)

  delete algGeom;		   
}

void OCPtSpecs::setOCPspecs(OCPparams ocpParams, CPolytope* polyCorridor, BitmapParams bmparams)
{
  m_OCPparams = ocpParams;

  //  cout << "Init Pos = " << m_OCPparams.initialConditionLB[NORTHING_IDX_C] << m_OCPparams.initialConditionLB[EASTING_IDX_C] << endl;
  // cout << "Fin Pos = " << m_OCPparams.finalConditionLB[NORTHING_IDX_C] <<  m_OCPparams.finalConditionLB[EASTING_IDX_C] << endl;
  
  if (m_polyCorridor) {
    cout << "deleting previous corridor" << endl;
    if (*m_polyCorridor) free((void *)*m_polyCorridor);
    free((void *)m_polyCorridor);
  }
  m_polyCorridor = (CPolytope **)malloc(sizeof(CPolytope *));
  *m_polyCorridor = (CPolytope *)malloc(sizeof(CPolytope));
  **m_polyCorridor = *polyCorridor;
  
  //cout << *m_polyCorridor << endl;
  
  m_nPolytopes = 1;
  
  //m_costMapEst.initMap(&m_costMap);
  m_costMapEst.updateMapWithBitmapParams(&m_costMap, NULL, NULL, bmparams);
}


/////////////////////////////////////////
//////// TRANSMISSION THREAD   //////////
/////////////////////////////////////////

int OCPtSpecs::setStatus(int index, int value)
{
  if(index<= 0 || index >= N_STATUS)
    return 0;
  else
    {
      switch (index)
        {
        case 1 : 
	        m_dplannerStatus.OCPsolved = (bool)value;
          break;
        case 2 : 
	        m_dplannerStatus.solverFlag = value;
          break;
        case 3 : 
	        m_dplannerStatus.corridorGenerated = (bool)value;
          break;
        case 4 : 
	        m_dplannerStatus.corridorFlag = value;
	        break;
        default : 
	        return 0;

        }  	      
      return 1;
    }
}

void OCPtSpecs::supervisoryThread()
{
  SkynetTalker<bool> supervisoryTalker= SkynetTalker<bool>(m_skynetKey,SNtrajPause,MODdynamicplanner);
  bool supervisoryMsg;
	/*
    while(!m_quit)
    {
    //receive pause message
    supervisoryTalker.receive(&supervisoryMsg);
		//the following lines set the conditions to false. Hence before to start a new optimization we wait for a new set of specs
		if(supervisoryMsg)
		{
    DGClockMutex(&m_dplannerStatusMutex);
    //replying to tplanner
    #warning this is a temporary hack. We will move to the real CSS soon, I promise!
    m_dplannerStatus.OCPsolved = false;
    m_dplannerStatus.solverFlag = -1;
    m_statusTalker.send(&m_dplannerStatus);    
    DGCSetConditionFalse(m_notPausedCond);
    DGClockMutex(&m_dplannerStatusMutex);
    //propagating the message to trajfollow
    // to be done
    #warning we may have consistency problem if we don't check the status before sending out a new trajectory.
    //blocking the computation  
    DGClockMutex(&m_rddfDataMutex);
    DGClockMutex(&m_paramsDataMutex);
    DGClockMutex(&m_obstDataMutex);
    DGClockMutex(&m_costmapDataMutex);
    DGClockMutex(&m_polyCorridorDataMutex);
    DGCSetConditionFalse( m_costMapRecvdCond);
    DGCSetConditionFalse(m_paramsRecvdCond);
    DGCSetConditionFalse(m_obstRecvdCond);
    DGCSetConditionFalse( m_polyCorridorRecvdCond);
    DGCSetConditionFalse(m_rddfRecvdCond);
    DGCunlockMutex(&m_rddfDataMutex);
    DGCunlockMutex(&m_paramsDataMutex);
    DGCunlockMutex(&m_obstDataMutex);
    DGCunlockMutex(&m_costmapDataMutex);
    DGCunlockMutex(&m_polyCorridorDataMutex);

    }
    }
  */
}




