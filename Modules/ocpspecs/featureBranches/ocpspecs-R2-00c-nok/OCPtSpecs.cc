/*
 Definition of the OCPTSPECS class that provides the interface
 between Tplanner and Dplanner

 Stefano Di Cairano mar-07
 */

#include "OCPtSpecs.hh"
#include "interfaces/TpDpInterface.hh"
#include "interfaces/AliceFlat_Definitions.hh"
#include <highgui.h>
#define MAX_DELTA_SIZE 1000000  //taken from plannerModule
#define MAP_EXPANSION 5.0
#define GRAD_SMOOTHING 2

OCPtSpecs::OCPtSpecs(int sn_key, bool useAstate, int verbose, bool useCostMap, bool useObst, bool useParams, bool useRDDF, bool usePolyCorridor)
    : CSkynetContainer(MODdynamicplanner, sn_key),
	  //CStateClient(false),
      CRDDFTalker(false),
      m_paramsTalker(sn_key,SNocpParams,MODdynamicplanner),
      m_statusTalker(sn_key,SNdplStatus,MODdynamicplanner),
      m_obstTalker(sn_key,SNocpObstacles,MODdynamicplanner),
      m_verbose(verbose),
	  m_costMapEst(sn_key,true, false)
{
        
    m_skynetKey = sn_key;
    cout << "Skynet key: " << sn_key << endl; 
    m_quit = 0;	
	//setting member vars that control threads
    m_useParams = useParams;
    m_useObstacles = useObst;
    m_useRDDF = useRDDF;
    m_useCostMap = useCostMap;
    m_usePolyCorridor =usePolyCorridor;
    m_nPolytopes = 0;
    DGCcreateMutex(&m_dplannerStatusMutex);
    DGCcreateCondition(&m_notPausedCond);  //condition on deltas reception
    DGCstartMemberFunctionThread(this, &OCPtSpecs::supervisoryThread); //start the thread to catch the map deltas 
	//create the mutex for specs data: only one mutex to have better data consistency
    DGCcreateMutex(&m_specsDataMutex);
    //initialize costmap and related mutexes and sockets
   // m_useAstate = useAstate;
   // if(m_useAstate)
   // {
     //   cerr << "Warning: using State from AState" << endl;
   // }
		
    //parameters
    if(m_useParams)
    {
        DGCcreateCondition(&m_paramsRecvdCond);  //condition on deltas reception
        DGCstartMemberFunctionThread(this, &OCPtSpecs::getOCPparamsThread) ;
    }

    //obstacles
    if(m_useObstacles)
    {
        DGCcreateCondition(&m_obstRecvdCond);  //condition on deltas reception
        DGCstartMemberFunctionThread(this, &OCPtSpecs::getOCPobstThread) ;
    }
    
    //RDDF receiver
    if(m_useRDDF)
    {
        m_RDDFSocket =  m_skynet.listen(SNrddf,MODtrafficplanner); //put skynet interface here   
        DGCcreateCondition(&m_RDDFRecvdCond);  //condition on deltas reception
		DGCstartMemberFunctionThread(this, &OCPtSpecs::getRDDFThread) ;
    }
    
    if(m_usePolyCorridor)
    {
        DGCcreateCondition(&m_polyCorridorRecvdCond);  //condition on deltas reception
        DGCstartMemberFunctionThread(this, &OCPtSpecs::getPolyCorridorThread); //start the thread to catch the map deltas 
    }
    
	//DGCstartMemberFunctionThread(this, &OCPtSpecs::supervisoryThread);
    if(m_useCostMap)
    {   
	    m_costMapEst.initMap(&m_costMap);
        m_costDx = m_costMap;   //to initialize the correct dimensions we copy the cost map
		m_costDy = m_costMap;
		DGCcreateCondition(&m_costMapRecvdCond);  //condition on deltas reception
        DGCstartMemberFunctionThread(this, &OCPtSpecs::getCostMapThread); //start the thread to catch the map deltas 
    }
}

void OCPtSpecs::getCostMapThread()
{
    bool mapRec = false;
    while(!m_quit)
    {
        DGClockMutex(&m_specsDataMutex);
		mapRec = m_costMapEst.updateMap(&m_costMap);
		if(mapRec)
		{   cout << "computing gradient!" << endl;
		    getGradientMap(); //if the map was updated, compute the gradient map!
            cout << "gradient computed!" <<endl;
	    }
		DGCunlockMutex(&m_specsDataMutex);
        cout << mapRec << endl;  
		if( (mapRec) && (!m_costMapRecvdCond.bCond) )
        {
            DGCSetConditionTrue(m_costMapRecvdCond);
        }
		cvWaitKey(50);
	}
}



void OCPtSpecs::getOCPparamsThread()
{
    bool paramsCorrect;
    OCPparams tempParams;
	while(!m_quit)
	{	
		if (m_verbose) 
		    cout << "Waiting for Params" << endl;
        paramsCorrect = false;
        paramsCorrect = m_paramsTalker.receive(&tempParams);
		if(! paramsCorrect)
		{
		    cerr << "error in Parameters message" << endl;
        }
		else
		{
		    DGClockMutex(&m_specsDataMutex);
		    m_OCPparams = tempParams;
		    if(m_verbose) 
		        cout << "I am applying params. g: " << m_OCPparams.parameters[11] <<endl;     
		    if(!m_paramsRecvdCond.bCond)
			{
		       DGCSetConditionTrue(m_paramsRecvdCond);   
            }
			DGCunlockMutex(&m_specsDataMutex);
		}
	}
}

void OCPtSpecs::getRDDFThread()
{
    bool RDDFcorrect;
    RDDF tempRDDF;
    while(!m_quit)
	{
		// Reset the tempRDDF so that we always load full RDDF
        tempRDDF.clear();

	    // Now read in the data from the socket
        RDDFcorrect = RecvRDDF(m_RDDFSocket, &tempRDDF);

	    if (m_verbose > 1)
	        cout << "OCPtSpecs::getRDDFThread received RDDF" << endl;
	
	    if(!RDDFcorrect)
	    {
	        cerr <<"Error while recv RDDF" << endl;
	    }
	    else
	    {
            DGClockMutex(&m_specsDataMutex);
	        m_RDDF = tempRDDF;
	        if (m_verbose > 1)
	          cout << "OCPtSpecs::getRDDFThread RDDF object copied"<< endl;
	        if (m_verbose > 2) tempRDDF.print();
	        DGCunlockMutex(&m_specsDataMutex);
        }
        if(!m_RDDFRecvdCond.bCond)
        {
            DGCSetConditionTrue( m_RDDFRecvdCond);
	    }   				
    }
}

void OCPtSpecs::getOCPobstThread()
{
    bool obstCorrect;
    OCPobstacles tempObst;
	while(!m_quit)
	{		
	    if(m_verbose) 
	        cout << "Waiting for obstacles" << endl;
        obstCorrect = false;
        obstCorrect = m_obstTalker.receive(&tempObst);
		if(!obstCorrect)
		{
		    cerr << "OCPtSpecs:getOCPobstThread error" << endl;
        }
		else
		{
		    if (m_verbose) 
		        cout << "Applying obstacles: " << tempObst.ObstNumber <<  endl;
                    
            DGClockMutex(&m_specsDataMutex);
            m_Qconstr.Nconstr=tempObst.ObstNumber;            
            // Obstacles are sent by mapper as (x-xc)H(x_xc)<=1
            // We store them as   xHx+Sx+Kx <= 1
            // Hence here we compute the H, S, K components and store them
	        for(int i=0;i<tempObst.ObstNumber;i++)
		    {
                m_Qconstr.S[i][0] = -2*( tempObst.ObstHessian[i][0][0]*tempObst.ObstCenter[i][0] + 
			                tempObst.ObstHessian[i][0][1]*tempObst.ObstCenter[i][1]) ;
	            m_Qconstr.S[i][1] = -2*( tempObst.ObstHessian[i][1][0]*tempObst.ObstCenter[i][0] + 
			                tempObst.ObstHessian[i][1][1]*tempObst.ObstCenter[i][1]) ;
                m_Qconstr.H[i][0][0] = tempObst.ObstHessian[i][0][0];
                            m_Qconstr.H[i][0][1] = tempObst.ObstHessian[i][0][1]; 
	            m_Qconstr.H[i][1][1] = tempObst.ObstHessian[i][1][1]; 
	            m_Qconstr.H[i][1][0] = tempObst.ObstHessian[i][1][0];
                m_Qconstr.K[i] = tempObst.ObstCenter[i][0] * tempObst.ObstHessian[i][0][0] * tempObst.ObstCenter[i][0]  +
			                2 * tempObst.ObstCenter[i][0] * tempObst.ObstHessian[i][0][1] * tempObst.ObstCenter[i][1] +
						    tempObst.ObstCenter[i][1] * tempObst.ObstHessian[i][1][1] * tempObst.ObstCenter[i][1];                         
		    }
            //zeroing the unapplied constraints -> this is not necessary, but for now ....
	        for(int i=tempObst.ObstNumber;i<MAX_OBSTACLES;i++)
	        {
                m_Qconstr.S[i][0] = 0;
	            m_Qconstr.S[i][1] = 0;
                m_Qconstr.H[i][0][0] = 0;
                m_Qconstr.H[i][0][1] = 0; 
	            m_Qconstr.H[i][1][1] = 0; 
	            m_Qconstr.H[i][1][0] = 0;
                m_Qconstr.K[i] = 0;
	        }
		    
		    if(!m_obstRecvdCond.bCond)
		    {    DGCSetConditionTrue( m_obstRecvdCond);  
            }
            DGCunlockMutex(&m_specsDataMutex);
        }
    }
}


OCPtSpecs::~OCPtSpecs(void)
{
	//check: I don't think this is enough!
    m_quit = 1;
	usleep(100000);
	DGCdeleteMutex(&m_specsDataMutex);
	DGCdeleteCondition(&m_paramsRecvdCond);
	DGCdeleteCondition(&m_obstRecvdCond);
	DGCdeleteCondition(&m_costMapRecvdCond);
	DGCdeleteCondition(&m_RDDFRecvdCond);
	DGCdeleteCondition(&m_polyCorridorRecvdCond);
}

void OCPtSpecs::getRDDFcorridor(RDDF* pNewRDDF)
{
    *pNewRDDF =  m_RDDF;
    return ;
}

void OCPtSpecs::getOCPComponentSizes(OCPComponentSizes* const ocpComponentSizes)
{
	//this are fixed for now, will be stored in a class member in the future
    ocpComponentSizes->IsActive             = true;
    ocpComponentSizes->HasICostFunction     = false;
    ocpComponentSizes->HasTCostFunction     = true;
    ocpComponentSizes->HasFCostFunction     = false;
    ocpComponentSizes->NofILinConstraints   = 0;
    ocpComponentSizes->NofTLinConstraints   = 0;
    ocpComponentSizes->NofFLinConstraints   = 0;
    ocpComponentSizes->NofINLinConstraints  = 0;
    ocpComponentSizes->NofTNLinConstraints  = m_Qconstr.Nconstr;
    ocpComponentSizes->NofFNLinConstraints  = 0;
}


void OCPtSpecs::InitialCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Fi, double* const DFi)
{
    // no initial cost function for the moment
    return ; 
}

void OCPtSpecs::TrajectoryCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ft, double* const DFt)
{
    if(*mode == 0 || *mode == 2)
    {   
    //   *Ft = m_pStaticCostMap->getDataUTM<double>(m_scmapLayer, z2, z1);
	   //cout << "Cost at (" << z1 << "," <<z2 << ") : "  <<*Ft <<endl;
    }

    if(*mode == 1 || *mode == 2)
    {
      /*for(m_i=0;m_i<NZ;m_i++)  //initialize the df/dz-vector to 0 
        {
    	    DFt[m_i] = 0;
        }
        double grad[2];
        m_pStaticCostMap->getUTMGradient(m_scmapLayer, z2 , z1, grad);
        DFt[I_z1]=grad[0];
        DFt[I_z2]=grad[1];*/
     }  
       
 /*  sparsity:  not implemented yet  
     
     if(*mode == 99)
     {
 
     }
 */
}

void OCPtSpecs::FinalCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ff, double* const DFf)
{
    return ;  //not used yet
}

void OCPtSpecs::getLinearConstraints(const ConstraintType* const ctype, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A, const int* const Offset)
{
/*	
	if(*ctype == ct_LinInitial)
	{
        for(m_i=0; m_i<NofLConstraints; m_i++)
        {
            for(m_j=0; m_j<NofVariables; m_j++)
            {
                A[m_i][m_j] = 0.0;
            }
        }

 	}	
	else if(*ctype == ct_LinTrajectory)
	{
          
	}
	else if(*ctype == ct_LinFinal)
	{
         
	}	
*/
    return ;  //not used yet -> maybe we should use it for final constraints

}

void OCPtSpecs::InitialNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli, const int* const Offset) 
{
    return ;  //not used yet
}

void OCPtSpecs::TrajectoryNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt, const int* const Offset)
{
    double tempX[2]; //temporary position vector
    tempX[0]=z1;
    tempX[1]=z2;
    for(m_i=0;m_i<*NofConstraints; m_i++)
    {
        for(m_j=0;m_j<*NofVariables; m_j++)
        {
            DCnlt[m_i+*Offset][m_j]=0;  //initialize the jacobian to 0
        }
        Cnlt[*Offset+m_i]=0;  //this should not be necessary, because the value of each constraint is computed.
    }

     //Value
    if(*mode == 0 | *mode == 2)
    {
        for(m_i=0;m_i<*NofConstraints;m_i++)
        {
  	        Cnlt[*Offset+m_i] = tempX[0]*m_Qconstr.H[m_i][0][0]*tempX[0]+ 2 * tempX[0]*m_Qconstr.H[m_i][0][1]*tempX[1] + 
	                          tempX[1]*m_Qconstr.H[m_i][1][1]*tempX[1] + m_Qconstr.S[m_i][0]*tempX[0] + m_Qconstr.S[m_i][1]*tempX[1];
        }
    } 
    
    //Jacobian
    if(*mode == 1 | *mode == 2)
    {
        for(m_i=0;m_i<*NofConstraints;m_i++)
        {
            DCnlt[m_i+*Offset][I_z1]=m_Qconstr.S[m_i][0]+2*(tempX[0]*m_Qconstr.H[m_i][0][0]+tempX[1]*m_Qconstr.H[m_i][1][0]) ;
	        DCnlt[m_i+*Offset][I_z2]=m_Qconstr.S[m_i][1]+2*(tempX[0]*m_Qconstr.H[m_i][0][1]+tempX[1]*m_Qconstr.H[m_i][1][1]) ;
        }
    }
   
   //compute the Sparsity
    if(*mode == 99)
    {   
        for(m_i=0;m_i<*NofConstraints;m_i++)
        {   
            DCnlt[m_i+*Offset][I_z1]= 1;
            DCnlt[m_i+*Offset][I_z2]= 1;  
        }
    }
   
    return ;
}

/*! use to implement the nonlinear constraints applicable at the end of the time interval.*/
void OCPtSpecs::FinalNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf, const int* const Offset)
{
    return ;  //not used yet
}

/*! Use to implement the bounds for all the constraints: linear and nonlinear.*/
void OCPtSpecs::getLUBounds(const ConstraintType* const ctype, const BoundsType* const ubtype, const int* const i, const double* const t, const int* const NofConstraints, double* const LUBounds, const int* const Offset)
{
    if(*ctype == ct_NLinTrajectory)
	{  
	    double temp =0;
	    	//we assume quadratic constraints in the form xQx+Sx+K >= 1
	        for(m_j=0; m_j<(*NofConstraints); m_j++)
	        {
	            if( *ubtype == ubt_LowerBound )   //lower bound is 1-K
		            temp = 1 - m_Qconstr.K[m_j];    		
		        else
		        temp = DBL_MAX;                  //upper bound is \infty
	            LUBounds[*Offset+(*i)*(*NofConstraints)+m_j]=temp;
	        } 
	}
    return ;
}
    
    
void  OCPtSpecs::getInitialCondition(const BoundsType* const ubtype, double* const initCond)
{
    bool valuecheck = false;
    if( *ubtype == ubt_LowerBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          initCond[m_i]=m_OCPparams.initialConditionLB[m_i];	  
        }
/*	initCond[HEADING_IDX_C]=M_PI/2-initCond[HEADING_IDX_C];
	if( initCond[HEADING_IDX_C]> M_PI)
	   initCond[HEADING_IDX_C]=initCond[HEADING_IDX_C]-2*M_PI;
	*/   
	valuecheck = true;
    }
    
    if( *ubtype == ubt_UpperBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          initCond[m_i]=m_OCPparams.initialConditionUB[m_i];	  
        }
    /*    
	initCond[HEADING_IDX_C]=M_PI/2-initCond[HEADING_IDX_C];
	if( initCond[HEADING_IDX_C]> M_PI)
	   initCond[HEADING_IDX_C]=initCond[HEADING_IDX_C]-2*M_PI;
	*/
	valuecheck = true;
    }
    if(valuecheck==false)
    {
        cout << "wrong value for initial bounds type" <<endl;
        exit(1);
    }

    return ;
}

void  OCPtSpecs::getFinalCondition(const BoundsType* const ubtype, double* const finalCond)
{
    bool valuecheck = false;
    if( *ubtype == ubt_LowerBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          finalCond[m_i]=m_OCPparams.finalConditionLB[m_i];	  
        }
        /*
	finalCond[HEADING_IDX_C]=M_PI/2-finalCond[HEADING_IDX_C];
	if( finalCond[HEADING_IDX_C]> M_PI)
	   finalCond[HEADING_IDX_C]=finalCond[HEADING_IDX_C]-2*M_PI;
	*/
	valuecheck = true;
    }
    
    if( *ubtype == ubt_UpperBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          finalCond[m_i]=m_OCPparams.finalConditionUB[m_i];	  
        }
        /*
	finalCond[HEADING_IDX_C]=M_PI/2-finalCond[HEADING_IDX_C];
	if( finalCond[HEADING_IDX_C]> M_PI)
	   finalCond[HEADING_IDX_C]=finalCond[HEADING_IDX_C]-2*M_PI;
	   */
	valuecheck = true;
    }
    if(valuecheck==false)
    {
        cout << "wrong value for final bounds type" <<endl;
        exit(1);
    }

    return ;
}



void  OCPtSpecs::getParams(double* const params)
{


    for(m_i=0;m_i<PARAMS_NUMBER;m_i++)
    {
        params[m_i]=m_OCPparams.parameters[m_i];	  
    }
    params[12] = m_OCPparams.initialConditionLB[VELOCITY_IDX_C];
    params[13] = m_OCPparams.finalConditionLB[VELOCITY_IDX_C];
    params[14] = m_OCPparams.initialConditionLB[ACCELERATION_IDX_C];
    params[15] = m_OCPparams.finalConditionLB[ACCELERATION_IDX_C];
   
    return ;
}

void  OCPtSpecs::getMode(int* mode)
{     
    *mode = m_OCPparams.mode; 
    return ;
}

void OCPtSpecs::startSolve()
{
    if(m_verbose)
        cout << "Request to solve. RDDF: "<< m_useRDDF << " Obst: " << m_useObstacles<< " Map: "<< m_useCostMap << " Params: " << m_useParams <<endl;
	
    if(m_useRDDF)
  //      DGCWaitForConditionTrue(m_RDDFRecvdCond);
    
    if(m_useParams)
        DGCWaitForConditionTrue(m_paramsRecvdCond);
    if(m_useObstacles)
     //   DGCWaitForConditionTrue( m_obstRecvdCond);
    
    if(m_useCostMap)
      //  DGCWaitForConditionTrue( m_costMapRecvdCond);
    //m_costMapEst.updateMap(&m_costMap); 
   
    if(m_usePolyCorridor)
        DGCWaitForConditionTrue(m_polyCorridorRecvdCond);
   
    DGClockMutex(&m_specsDataMutex);    
    if(m_verbose)
        cout << "Ready to solve."<< endl;    
    return ;
}

void OCPtSpecs::endSolve()
{

    
   // bool result = m_statusTalker.send(&m_dplannerStatus);    
    DGCunlockMutex(&m_specsDataMutex);    
    
    if(m_verbose)
        cout << "Finished solving" << endl; 
    return ;
}

int OCPtSpecs::setStatus(int index, int value)
{
    if(index<= 0 || index >= N_STATUS)
        return 0;
    else
    {
        switch (index)
	{
	    case 1 : 
	        m_dplannerStatus.OCPsolved = (bool)value;
		break;
	    case 2 : 
	        m_dplannerStatus.solverFlag = value;
		break;
	    case 3 : 
	        m_dplannerStatus.corridorGenerated = (bool)value;
		break;
	    case 4 : 
	        m_dplannerStatus.corridorFlag = value;
	        break;
	    default : 
	        return 0;

	 }  	      

        return 1;
    }

}


void OCPtSpecs::printPolyCorridor()
{
   cout << endl<< endl << endl <<  "Printing corridor information " << endl << endl << endl; 
   cout << "Number of polytopes: " << m_nPolytopes << endl; 
   for(int i=0;i<m_nPolytopes;i++)
      cout <<&m_polyCorridor[i] << endl;

}
void OCPtSpecs::supervisoryThread()
{
    SkynetTalker<bool> supervisoryTalker= SkynetTalker<bool>(m_skynetKey,SNtrajPause,MODdynamicplanner);
    bool supervisoryMsg;
	while(!m_quit)
	{
        //receive pause message
        supervisoryTalker.receive(&supervisoryMsg);
		//the following lines set the conditions to false. Hence before to start a new optimization we wait for a new set of specs
		if(supervisoryMsg)
		{
		    DGClockMutex(&m_dplannerStatusMutex);
            //replying to tplanner
            #warning this is a temporary hack. We will move to the real CSS soon, I promise!
            m_dplannerStatus.OCPsolved = false;
			m_dplannerStatus.solverFlag = -1;
            m_statusTalker.send(&m_dplannerStatus);    
		    DGCSetConditionFalse(m_notPausedCond);
			DGClockMutex(&m_dplannerStatusMutex);
			//propagating the message to trajfollow
			// to be done
             #warning we may have consistency problem if we don't check the status before sending out a new trajectory.
            //blocking the computation  
			DGClockMutex(&m_specsDataMutex);
            DGCSetConditionFalse( m_costMapRecvdCond);
            DGCSetConditionFalse(m_paramsRecvdCond);
            DGCSetConditionFalse(m_obstRecvdCond);
		    DGCunlockMutex(&m_specsDataMutex);

	    }
    }
}
void OCPtSpecs::getPolyCorridorThread()
{
    
    CAlgebraicGeometry algGeom = CAlgebraicGeometry();
    double** vertices;
	int NofDim = 2;
    SkynetTalker<sendCorr> recCorTalker= SkynetTalker<sendCorr>(m_skynetKey, SNpolyCorridor,MODdynamicplanner);
	sendCorr corridor;
    while(!m_quit)
	{
	
        recCorTalker.receive(&corridor);
		DGClockMutex(&m_specsDataMutex);
        m_nPolytopes = corridor.size();
        delete[] m_polyCorridor;
        m_polyCorridor = new CPolytope[m_nPolytopes];
        for(int i=0; i<m_nPolytopes; i++)
        {
            int polySize = corridor[i].size();
	        vertices = new double*[NofDim];
	        for(int j=0;j<NofDim;j++)
                vertices[j] = new double[polySize];
            for(int j=0;j<polySize;j++)
		    {
                vertices[0][j] = corridor[i][j].x ;
                vertices[1][j] = corridor[i][j].y ;
            }    
        
            m_polyCorridor[i] = new CPolytope(&NofDim, &polySize, (const double**) vertices);
            algGeom.FacetEnumeration(&m_polyCorridor[i]);
            algGeom.VertexEnumeration(&m_polyCorridor[i]); //vertex reduction
            
	        for(int j=0;j<NofDim;j++)
                delete[]  vertices[j];
            delete vertices;		   
        }
        if(!m_polyCorridorRecvdCond.bCond)
        {
            DGCSetConditionTrue(m_polyCorridorRecvdCond);
        }
		DGCunlockMutex(&m_specsDataMutex);
    }
}

void OCPtSpecs::getSurfaceControlPoints(int windowSize,TimeInterval* xInterval, TimeInterval* yInterval, double* dx, double* dy, double** controlPoints)
{
    double xi = m_OCPparams.initialConditionLB[EASTING_IDX_C];
    double xf = m_OCPparams.finalConditionLB[EASTING_IDX_C];
    double yi = m_OCPparams.initialConditionLB[NORTHING_IDX_C];
    double yf = m_OCPparams.finalConditionLB[NORTHING_IDX_C];
    
    if(xi<xf)
    {
      xInterval->t0 = xi - MAP_EXPANSION;
      xInterval->tf = xf + MAP_EXPANSION;
    }
    else
    {
      xInterval->t0 = xf - MAP_EXPANSION;
      xInterval->tf = xi + MAP_EXPANSION;   	
    }
    if(yi<yf)
    {
      yInterval->t0 = yi - MAP_EXPANSION;
      yInterval->tf = yf + MAP_EXPANSION;
    }
    else
    {
      yInterval->t0 = yf - MAP_EXPANSION;
      yInterval->tf = yi + MAP_EXPANSION;   	
    }
    
    *dy = (yInterval->tf-yInterval->t0)/windowSize;
    *dx = (xInterval->tf-xInterval->t0)/windowSize;
    
    
    for(int i = 0; i<windowSize; i++)
    {
        for(int j = 0; j<windowSize; j++)
        {
            controlPoints[i][j]=getCostMapValue(xInterval->t0+(*dx)*i,yInterval->t0+(*dy)*i);	
        }
    }  
    
    
}

float OCPtSpecs::getCostMapValue(double x, double y)
{
   float res=-10;
   point2 p = point2(x,y);
   res = m_costMap.getPixelLoc(p);
   return res;


}

point2 OCPtSpecs::getCostMapGradient(double x, double y)
{
   point2 upperLeft =  m_costMap.getUpperLeft();
   point2 lowerRight = m_costMap.getLowerRight();
   
   float dx = (lowerRight.x-upperLeft.x)/m_costMap.getWidth() ;
   float dy = (lowerRight.y-upperLeft.y)/m_costMap.getHeight() ;
   float derX = 0.0 ;
   float derY = 0.0 ;
   cout << "Using " << GRAD_SMOOTHING << " steps in gradient " << endl; 
   if(GRAD_SMOOTHING  < 1)
   {
       derX += ( (getCostMapValue(x+dx,y)-getCostMapValue(x,y))/dx );
       derY += ( (getCostMapValue(x,y+dy)-getCostMapValue(x,y))/dy );   
   }
   else
   {  

       for(int i=1;i<=GRAD_SMOOTHING ;i++) //smooth the gradient by loking at the numerical gradients with increments +/- 
       {   
           derX +=   ( (getCostMapValue(x+i*dx,y)-getCostMapValue(x,y))/(i*dx)) - ( (getCostMapValue(x-i*dx,y)-getCostMapValue(x,y))/(i*dx) );
           derY +=   ( (getCostMapValue(x,y+i*dy)-getCostMapValue(x,y))/(i*dy)) - ( (getCostMapValue(x,y-i*dy)-getCostMapValue(x,y))/(i*dy) );  
       }
       derX = derX/(2*GRAD_SMOOTHING);
       derY = derY/(2*GRAD_SMOOTHING);      
   }
   point2 res = point2(derX,derY);
   return res;
}

void OCPtSpecs::getGradientMap()
{
    IplImage* p_img=m_costMap.getImage();
    if( (m_costMap.getWidth() != m_costDx.getWidth() ) || (m_costMap.getHeight() != m_costDx.getHeight() ) )
    {
        m_costDx = m_costMap;
        m_costDy = m_costMap;

    }
    cvSobel(p_img, m_costDx.getImage(), 1, 0, 5 );
    cvSobel(p_img, m_costDy.getImage(), 0, 1, 5 );
    
	IplImage* img = cvCloneImage(m_costDx.getImage());
    double minVal, maxVal;
    cvMinMaxLoc(img, &minVal, &maxVal);
    cvSubS(img, cvRealScalar(minVal), img);
    if (maxVal != minVal)
      cvScale(img, img, 1/(maxVal - minVal));
    cvNamedWindow("Dx",CV_WINDOW_AUTOSIZE);
    cvShowImage("Dx",img);
    cvReleaseImage(&img);
    
	img = cvCloneImage(m_costDy.getImage());
    cvMinMaxLoc(img, &minVal, &maxVal);
    cvSubS(img, cvRealScalar(minVal), img);
    if (maxVal != minVal)
      cvScale(img, img, 1/(maxVal - minVal));
    cvNamedWindow("Dy",CV_WINDOW_AUTOSIZE);
    cvShowImage("Dy",img);
    cvReleaseImage(&img);
 //  cvWaitKey(100);
}

double OCPtSpecs::polyIntersectionVolume(CPolytope* poly1, CPolytope* poly2)
{
    int Nrows1 = poly1->getNofRows(); 
    int Ncols1 = poly1->getNofColumns();
	int NofDim = Ncols1;
	CAlgebraicGeometry* algGeom = new CAlgebraicGeometry();
	double** A1;
    double* b1;
	b1 = new double[Nrows1];
	A1 = new double*[Nrows1];
	for(int j=0;j<Nrows1;j++)
        A1[j] = new double[Ncols1];
        
    poly1->getA(&Nrows1,&Ncols1,A1);
    poly1->getb(&Nrows1,b1);
        
	int Nrows2=poly2->getNofRows(); 
    int Ncols2=poly2->getNofColumns();
	double** A2;
    double* b2;
	b2 = new double[Nrows2];
	A2 = new double*[Nrows2];
	for(int j=0;j<Nrows2;j++)
        A2[j] = new double[Ncols2];
        
    poly2->getA(&Nrows2,&Ncols2,A2);
    poly2->getb(&Nrows2,b2);

	double** Aint;
    double* bint;
	bint = new double[Nrows1+Nrows2];
	Aint = new double*[Nrows1+Nrows2];
    for(int j=0;j<Nrows1;j++)
	{
	    Aint[j] = A1[j];   //I use the memory which has been already allocated
	    bint[j] = b1[j];
    }
    for(int j=Nrows1;j<(Nrows1+Nrows2);j++)
	{
	    Aint[j] = A2[j-Nrows1];   //I reuse the memory which has been already allocated
	    bint[j] = b2[j-Nrows1];
    }
	int NrowsInt = Nrows1+Nrows2;
	int NcolsInt = Ncols1;
			
	int NofVtxInt= 4;
	double** verticesInt = new double*[NofDim];
	for(int j=0;j<NofDim;j++)
	    verticesInt[j] = new double[NofVtxInt];
	cout << "data prepared " <<  endl;
    algGeom->VertexEnumeration(&NrowsInt,&NcolsInt,(const double**)Aint, (const double*)bint, &NofDim, &NofVtxInt, verticesInt);
	for(int j=0;j<NofDim;j++)
        delete[] verticesInt[j];
	delete[] verticesInt;

	for(int i =0; i<Nrows1+Nrows2; i++)
	  cout << Aint[i][0]<< " "<< Aint[i][1] <<  " <= " << bint[i] <<  endl;
	sleep(1);
	verticesInt = new double*[NofDim];
	for(int j=0;j<NofDim;j++)
	    verticesInt[j] = new double[NofVtxInt];
    cout << "n vertices" << NofVtxInt << endl;
	sleep(1);
    algGeom->VertexEnumeration(&NrowsInt,&NcolsInt,(const double**)Aint, (const double*)bint, &NofDim, &NofVtxInt, verticesInt);
    cout << "vertices computed" << endl;
	for(int i =0; i<NofVtxInt; i++)
	  cout << verticesInt[0][i]<< " "<<  verticesInt[1][i] << endl;
	sleep(1);
    delete[] b1;
    delete[] b2;
    delete[] Aint;         //note the second level pointers point to memory shared with other pointers. I deallocate from those.
	for(int j=0;j<Nrows1;j++)
	    delete[] A1[j];
    delete[] A1;
    for(int j=0;j<Nrows2;j++)
        delete[] A2[j];
    delete[] A2;

    double volumeInt = 0.0;
	CPolytope polyInt  =  new CPolytope( &NofDim, &NofVtxInt, (const double**)  verticesInt ); //Ask Melvin: is this Correct?
	algGeom->FacetEnumeration(&polyInt); //vertex reduction
	ALGErrorType algErr = algGeom->VertexEnumeration(&polyInt); //vertex reduction
	cout << &polyInt << endl;
	if(algErr == alg_NoError)
	{
	    algGeom->FacetEnumeration(&polyInt);
        algGeom->ComputeVolume(&polyInt);
        volumeInt =  polyInt.getVolume();
	}
	else
	{
	    if(algErr != alg_EmptyVrepresentation)
            volumeInt =  -1.0;
	}
    cout << volumeInt  << endl;
	//delete polyInt;
	delete algGeom;
	return volumeInt;



}

int OCPtSpecs::isInsideCorridor(double* x, int dim)
{
    int Nrows = 0 ;
    int Ncols = 0 ;
    bool next = true ;
	int i = 0 ;
	bool error = false ;
    double temp = 0 ;
    int result = -2 ;

    while( (next) && (i<m_nPolytopes) )
    { 
	    Nrows=m_polyCorridor[i].getNofRows(); 
        Ncols=m_polyCorridor[i].getNofColumns();
		double** A;
        double* b;
		b = new double[Nrows];
		A = new double*[Nrows];
		for(int j=0;j<Nrows;j++)
            A[j] = new double[Ncols];
        
        m_polyCorridor[i].getA(&Nrows,&Ncols,A);
        m_polyCorridor[i].getb(&Nrows,b);
        bool feasible = true;
        if(dim != Ncols)
		{
		  cerr << "Wrong dimension specified! Should be: "<< dim << " while it is: " << Ncols  << endl; 
		  error = true;
		  break;
		}
		
		int j = 0;
		temp = 0;

		while( (feasible) && (j<Nrows))
		{
            for(int h=0;h<dim;h++)
			    temp+=A[j][h]*x[h];
            
//			cout << "row " << j << " " << temp << " " << b[j] << endl; 
			feasible = (temp<=b[j]);
            j++;
			temp = 0;
		}
		//sleep(1);
		if(feasible)
		    next = false;
        else
		{
		    i++;
            delete[] b;
		    for(int j=0;j<Nrows;j++)
			    delete[] A[j];
            delete[] A;
	    }
 
    }
	
	if(next)
	   result = -1;
	else
	   result = i;
    if(error)
	   result = -2;

    return result;

}


int OCPtSpecs::getNpolytopes()
{
    return m_nPolytopes;
}



