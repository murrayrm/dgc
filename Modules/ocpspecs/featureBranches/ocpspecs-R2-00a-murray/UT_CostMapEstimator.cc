#include <highgui.h>
#include <bitmap/Polygon.hh>
#include <skynet/skynet.hh>
#include <skynettalker/SkynetTalker.hh>
#include <boost/serialization/vector.hpp>
#include <map/MapElement.hh>
#include "CostMapEstimator.hh"

using namespace std;
using namespace bitmap;

int main(int argc, char  **argv)
{
  int skynetKey = skynet_findkey(argc, argv);
  cout << "Skynet key: " << skynetKey << endl;
  /* Receiver */
  CostMapEstimator cmapEst(skynetKey, true, true);

  /* Sender */
  SkynetTalker< BitmapParams > polyTalker(skynetKey, SNbitmapParams, MODtrafficplanner);

  int seed = time(0);
  //int seed = 1178578092;
  cout << "random seed: " << seed << endl;
  srand(seed);

  BitmapParams bmparams;
  bmparams.width = 800;
  bmparams.height = 600;
  bmparams.resX = 0.5;
  bmparams.resY = 0.5;
  bmparams.centerX = 200;
  bmparams.centerY = 150;
  bmparams.baseVal = 0;

  for (unsigned i=0; i<20; i++) {
    vector<point2> points;
    point2 tmpPoint;
    for (unsigned j=0; j<3; j++) {
      float tmpPointX = (rand()/double(RAND_MAX)) * 400;
      float tmpPointY = (rand()/double(RAND_MAX)) * 300;
      tmpPoint.set(tmpPointX, tmpPointY);
      cout << "tmpPoint = (" << tmpPointX << ", " << tmpPointY << ")" << endl;
      points.push_back(tmpPoint);
    }

    cout << endl;
    
    MapElement mapElement;
    mapElement.frame_type = FRAME_LOCAL;
    mapElement.type = ELEMENT_OBSTACLE;
    mapElement.geometry_type = GEOMETRY_POLY;
    mapElement.set_id(1);
    mapElement.set_geometry(points);
    mapElement.height = 10*(rand()/double(RAND_MAX));

    Polygon tmpPoly;
    vector<float> cost(3, mapElement.height);
    tmpPoly.setVertices(mapElement.geometry, cost);
    bmparams.polygons.push_back(tmpPoly);
    
    polyTalker.send(&bmparams);
    usleep(100000);
  }

  cvWaitKey(0);

  return 0;
}

