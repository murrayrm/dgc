#include <bitmap/Polygon.hh>
#include <skynet/skynet.hh>
#include <skynettalker/SkynetTalker.hh>
#include <boost/serialization/vector.hpp>
#include <map/MapElement.hh>
#include "CostMapEstimator.hh"

using namespace std;
using namespace bitmap;

int main(int argc, char  **argv)
{
  int skynetKey = skynet_findkey(argc, argv);
  cout << "Skynet key: " << skynetKey << endl;
  /* Receiver */
  CostMap cmap;
  CostMapEstimator cmapEst(skynetKey, true, false);

  while (true) {
    bool cmapRecv = cmapEst.updateMap(&cmap);
    if (cmapRecv)
      cout << "Recv cost map" << endl;
    usleep(100000);
  }  

  return 0;
}

