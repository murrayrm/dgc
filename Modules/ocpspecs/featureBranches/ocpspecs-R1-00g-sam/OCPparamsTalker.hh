#ifndef _OCPPARAMSTALKER_HH_
#define _OCPPARAMSTALKER_HH_

#include "pthread.h"
#include <iostream>
#include <unistd.h>
#include "skynet/SkynetContainer.hh"
#include "dgcutils/DGCutils.hh"
 

#define DRIVE_FWD 1
#define DRIVE_REV 0
#define PARAMS_NUMBER 12
#define COND_NUMBER 12
#define LENGTH_IDX 0
#define WEELB_IDX 1
#define HCG_IDX 2
#define VMIN_IDX 3
#define VMAX_IDX 4
#define AMIN_IDX 5
#define AMAX_IDX 6
#define PHIMIN_IDX 7
#define PHIMAX_IDX 8
#define PHIDMIN_IDX 9
#define PHIDMAX_IDX 10
#define G_IDX 11 
#define NORTING_MIN_IDX 0
#define NORTING_MAX_IDX 1
#define EASTING_MIN_IDX 2
#define EASTING_MAX_IDX 3
#define HEADING_MIN_IDX 4
#define HEADING_MAX_IDX 5
#define VELOCITY_MIN_IDX 6
#define VELOCITY_MAX_IDX 7
#define ACCELERATION_MIN_IDX 8
#define ACCELERATION_MAX_IDX 9
#define STEERING_MIN_IDX 10
#define STEEERING_MAX_IDX 11


struct OCPparams
{
	int driveDirection;
    double parameters [PARAMS_NUMBER];
    
    double initialCondition [COND_NUMBER];
    
    /*
    same structure as finalCondition
    */
       
    double finalCondition [COND_NUMBER];
      /*
     finalCondition[0] = Northing_min
     finalCondition[1] = Northing_max
     finalCondition[2] = Easting_min
     finalCondition[3] = Easting_max
     finalCondition[4]  = Heading_min
     finalCondition[5]  = Heading_max 
     finalCondition[6]  = Velocity_min (norm)
     finalCondition[7]  = Velocity_max (norm)
     finalCondition[8]  = Acceleration_min (norm)
     finalCondition[9]  = Acceleration_max (norm)
     finalCondition[10]  = SteeringAngle_min 
     finalCondition[11]  = SteeringAngle_max
      */
  
    OCPparams()
    {
    	//this is the fastest way to set everything to 0
        memset(parameters,'\0',PARAMS_NUMBER*sizeof(double));    
        memset(initialCondition,'\0',COND_NUMBER*sizeof(double));    
        memset(finalCondition,'\0',COND_NUMBER*sizeof(double));    
        //by default we drive fwd
        driveDirection = DRIVE_FWD ;
        
        //setting parasmeters to the default values defined For Alice dynamics in dplanner
	    parameters[0]  = 5.43560 ;         // L
	    parameters[1]  = 2.13360 ;         // W
	    parameters[2]  = 1.06680 ;         // hcg
	    parameters[3]  = 0.00000 ;         // v_min
	    parameters[4]  = 1.40800 ;         // v_max
	    parameters[5]  =-3.00000 ;         // a_min
	    parameters[6]  = 0.98100 ;         // a_max
	    parameters[7]  =-0.44942 ;         // phi_min
	    parameters[8]  = 0.44942 ;         // phi_max
	    parameters[9]  =-1.30900 ;         // phid_min
	    parameters[10] = 1.30900 ;         // phid_max
	    parameters[11] = 9.81000 ;         // g
	    
    }
};


class COCPparamsTalker : virtual public CSkynetContainer {
public:
	COCPparamsTalker();
	~COCPparamsTalker();

	bool SendOCPparams(int OCPparamsSocket, OCPparams* params);
	bool RecvOCPparams(int OCPparamsSocket, OCPparams* params, int* paramsSize);
};

#endif // _OCPPARAMSTALKER_H_
