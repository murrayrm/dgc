Tue Aug 14 18:16:44 2007	Noel duToit (ndutoit)

	* version R2-00s
	BUGS:  
	FILES: CostMapEstimator.cc(31556), CostMapEstimator.hh(31556),
		OCPtSpecs.cc(31556), OCPtSpecs.hh(31556)
	Added option to populate the ocp problem with function calls,
	instead of sending the info from the planner.

	FILES: OCPtSpecs.cc(31780), OCPtSpecs.hh(31780)
	fixed interface

Mon Jul  2 18:20:03 2007	Nok Wongpiromsarn (nok)

	* version R2-00r
	BUGS:  
	FILES: OCPtSpecs.cc(28480), testOCPspecs.cc(28480)
	Removed cvWaitKey from OCPtSpec so it works on mac

Sun Jun 10 13:47:32 2007	Stefano Di Cairano (stefano)

	* version R2-00q
	BUGS:  
	FILES: OCPtSpecs_stage1.cc(27727)
	Robustified getLegalInitialPoint, in case we want to use it.

Fri Jun  8 12:44:10 2007	Stefano Di Cairano (stefano)

	* version R2-00p
	BUGS:  
	FILES: OCPtSpecs.cc(27019), OCPtSpecs_stage1.cc(27019)
	robustified corridor check against badly formed polyhedra

	FILES: OCPtSpecs_common.cc(27122), OCPtSpecs_stage1.cc(27122)
	robustified corridor checking

Tue Jun  5 22:15:51 2007	datamino (datamino)

	* version R2-00o
	BUGS:  
	FILES: CostMapEstimator.cc(26734), CostMapEstimator.hh(26734)
	Added two optional output parameters to updateMap, that are used to
	return the maximum and minimum cost of the vertices. Added code to
	show timings in updateMap when debugLevel >= 1.

Sun Jun  3 14:04:01 2007	Nok Wongpiromsarn (nok)

	* version R2-00n
	BUGS:  
	FILES: OCPtSpecs.cc(26240)
	Fixed the argument to CostmapEstimator so it does not always pop up
	the cost map

Fri Jun  1 18:00:03 2007	Stefano Di Cairano (stefano)

	* version R2-00m
	BUGS:  
	FILES: OCPtSpecs_common.cc(26101), OCPtSpecs_stage1.cc(26101)
	fixed getLegalEndPoint and tuned some timing params.

Thu May 31 17:36:54 2007	Stefano Di Cairano (stefano)

	* version R2-00l
	BUGS:  
	FILES: OCPtSpecs.hh(25800), OCPtSpecs_common.cc(25800)
	added accessor function to get the maximum allowed velocity

Thu May 31 15:35:53 2007	Stefano Di Cairano (stefano)

	* version R2-00k
	BUGS:  
	FILES: OCPtSpecs.cc(25707), OCPtSpecs_stage1.cc(25707)
	fixed a memory leak in isInsideCorridor

	FILES: OCPtSpecs.cc(25708)
	fixed small bug

	FILES: OCPtSpecs.cc(25726), OCPtSpecs.hh(25726),
		OCPtSpecs_common.cc(25726)
	Further speedup to final condition. The thread is always active and
	the condition is copied over when the solver starts. Furthermore we
	force the solver to wait for at least a new final condition before
	to solve a new problem (to force the update).

	FILES: OCPtSpecs.cc(25751), OCPtSpecs_common.cc(25751)
	added more debug info

	FILES: OCPtSpecs_stage1.cc(25768)
	introduced a boundary tolerance on the corridor checks

Tue May 29 18:43:25 2007	Stefano Di Cairano (stefano)

	* version R2-00j
	BUGS:  
	New files: OCPtSpecs_common.cc OCPtSpecs_dplanner.cc
		OCPtSpecs_stage1.cc
	FILES: CostMapEstimator.cc(25418), OCPtSpecs.cc(25418),
		OCPtSpecs.hh(25418)
	We use now separate mutices for each message. Heuristic function
	for s1planner updsated. Added a function to do a local search of
	feasibility of a point in the s1planner trajectory. Constructor
	added: it uses a structure of type OCPspecsFlags, which is defined
	in OCPtSpecs.hh, and that has a default constructor. The
	construcotrs have the possibility of turning on and off the
	visualization of costmap and gradientmaps. Backward compatibility
	ensured by default values. 

	FILES: Makefile.yam(25342), OCPtSpecs.cc(25342),
		OCPtSpecs.hh(25342)
	 changes in the files and in the constructor have been debugged

	FILES: OCPtSpecs.cc(25339), OCPtSpecs.hh(25339)
	files have been splitted to make the module more readable and easy
	to manage

	FILES: OCPtSpecs.cc(25345)
	moving to flush the whole queues of messages before to unlock the
	mutices

	FILES: OCPtSpecs.hh(25340)
	added structure based constructor

	FILES: OCPtSpecs.hh(25347)
	initial debug and test of the new version of s1planner. Tested on
	santaanita big loop. seems ok so far

Mon May 28 22:03:57 2007	Nok Wongpiromsarn (nok)

	* version R2-00i
	BUGS:  
	FILES: CostMapEstimator.cc(25298)
	Set out of bounds to be large number (as set by tplanner)

Fri May 25  0:10:16 2007	Stefano Di Cairano (stefano)

	* version R2-00h
	BUGS:  
	FILES: OCPtSpecs.cc(24637, 24638), OCPtSpecs.hh(24637),
		testOCPspecs.cc(24637)
	debugging

	FILES: OCPtSpecs.cc(24818), OCPtSpecs.hh(24818)
	added accessors functions for Waypoints. Added waypoints
	computation as needed by Dplanner.

	FILES: OCPtSpecs.cc(24831), OCPtSpecs.hh(24831)
	Waypoints and accessors syntactically debugged

	FILES: OCPtSpecs.cc(24841)
	fixed a bug

	FILES: OCPtSpecs.cc(24842)
	fixed a couple of logic bugs

	FILES: OCPtSpecs.cc(24864)
	soving a segfault

	FILES: OCPtSpecs.cc(24911)
	 extensively tested. It does not segfault

Tue May 22 17:30:13 2007	Stefano Di Cairano (stefano)

	* version R2-00g
	BUGS:  
	FILES: OCPtSpecs.cc(23859), OCPtSpecs.hh(23859),
		testOCPspecs.cc(23859), testParams.cc(23859)
	added function to replace the endPoint of s1planner when that is
	not legal

	FILES: OCPtSpecs.cc(24385)
	implemented refined A* heuristic

	FILES: OCPtSpecs.cc(24386), OCPtSpecs.hh(24386)
	Corridor based A* heuristic: syntax debugged, now it compiles.
	Ready for runtime debug (I hate SegFaults, I really hate them)

	FILES: OCPtSpecs.cc(24417), testOCPspecs.cc(24417),
		testParams.cc(24417)
	heuristic cost debugged against unit test program

	FILES: testOCPspecs.cc(24489)
	Heurstic inbtegrated and tested against s1planner. Works Ok, we may
	do better in the near future.

Thu May 17 23:37:31 2007	Stefano Di Cairano (stefano)

	* version R2-00f
	BUGS:  
	FILES: OCPtSpecs.cc(23499)
	previous release was not up to date

	FILES: OCPtSpecs.cc(23672), testOCPspecs.cc(23672)
	debugging with s1planner

Thu May 17 12:54:46 2007	Stefano Di Cairano (stefano)

	* version R2-00e
	BUGS:  
	FILES: OCPtSpecs.cc(23491), OCPtSpecs.hh(23491),
		testOCPspecs.cc(23491)
	Modified functions that return trajectory cost function and
	gradient to dplanner. Added accessor function to get polytopic
	corridor. Added accessor function to get Costmap gradient
	component.

Wed May 16 23:02:03 2007	Stefano Di Cairano (stefano)

	* version R2-00d
	BUGS:  
	FILES: OCPtSpecs.cc(23248)
	discovered memory leaks in CPolytope class. We have to use it only
	by pointers

	FILES: OCPtSpecs.cc(23273), OCPtSpecs.hh(23273),
		testOCPspecs.cc(23273)
	More memory leaks removal in OCPspecs. Still smaller leaks to be
	fixed. Those appear to be in CAlgebraicGeometry and in the talkers
	(SkynetTalker + RDDFTalker/SkynetContainer)

	FILES: OCPtSpecs.cc(23350), testOCPspecs.cc(23350)
	Fixed a memory leak in isInsideCorridor

	FILES: OCPtSpecs.cc(23392), testOCPspecs.cc(23392)
	more debugging

Tue May 15 18:17:58 2007	Stefano Di Cairano (stefano)

	* version R2-00c
	BUGS:  
	FILES: OCPtSpecs.cc(23224), OCPtSpecs.hh(23224),
		testOCPspecs.cc(23224)
	Debugging against S1planner. Wait conditions disabled

	FILES: OCPtSpecs.cc(23107), testOCPspecs.cc(23107)
	Removed the shifting on the angle. All the planners should agree
	with the current convention on their code. Test program updated.

	FILES: OCPtSpecs.cc(23158), OCPtSpecs.hh(23158),
		testOCPspecs.cc(23158)
	debugging on the intersection function done

Mon May 14 13:26:41 2007	Sam Pfister (sam)

	* version R2-00b
	BUGS:  
	FILES: UT_CostMapEstimator.cc(23030),
		UT_CostMapEstimatorNoThread.cc(23030)
	Updated map element functions to work with the new map version

Fri May 11  0:57:17 2007	Stefano Di Cairano (stefano)

	* version R2-00a
	BUGS:  
	FILES: CostMapEstimator.cc(22706), Makefile.yam(22706),
		OCPtSpecs.cc(22706), OCPtSpecs.hh(22706),
		testOCPspecs.cc(22706)
	initial integration of the new costmap

	FILES: CostMapEstimator.cc(22718), CostMapEstimator.hh(22718),
		OCPtSpecs.cc(22718), OCPtSpecs.hh(22718),
		UT_CostMapEstimator.cc(22718),
		UT_CostMapEstimatorNoThread.cc(22718),
		testOCPspecs.cc(22718)
	implemented gradient map computation

	FILES: Makefile.yam(22724), OCPtSpecs.cc(22724),
		OCPtSpecs.hh(22724)
	Removed state client, integrated gradient maps, removed unused
	talkers

	FILES: OCPtSpecs.cc(22708), OCPtSpecs.hh(22708)
	rebuilt getSurfaceControlPoints function

	FILES: OCPtSpecs.cc(22709)
	debugging getSurface

	FILES: OCPtSpecs.cc(22710)
	more debugging

	FILES: OCPtSpecs.cc(22713), OCPtSpecs.hh(22713)
	implemented gradient computation on the new costmap

	FILES: OCPtSpecs.cc(22714)
	gradient computation with smoothing.

	FILES: OCPtSpecs.cc(22715)
	debugging on gradient computation with smoothing

	FILES: OCPtSpecs.cc(22716)
	tuning the gradient smoothing

Wed May  9  1:34:22 2007	Nok Wongpiromsarn (nok)

	* version R1-01e
	BUGS:  
	New files: CostMapEstimator.cc CostMapEstimator.hh
		UT_CostMapEstimator.cc UT_CostMapEstimatorNoThread.cc
	FILES: Makefile.yam(22569)
	Added CostMapEstimator which paints cost map based on parameters
	received over skynet.

Tue May  8 10:50:51 2007	Stefano Di Cairano (stefano)

	* version R1-01d
	BUGS:  
	FILES: OCPtSpecs.cc(21485), OCPtSpecs.hh(21485),
		testParams.cc(21485)
	theta offset to account for the shift in theta.

	FILES: OCPtSpecs.cc(21682), OCPtSpecs.hh(21682),
		testOCPspecs.cc(21682), testParams.cc(21682)
	changes in the integration test. surface control points coordinates
	have been switched due to the frame is used into the map.

	FILES: OCPtSpecs.cc(22469), OCPtSpecs.hh(22469),
		testParams.cc(22469)
	small changes for dplanner testing

Sat Apr 28 11:23:23 2007	Stefano Di Cairano (stefano)

	* version R1-01c
	BUGS:  
	FILES: Makefile.yam(20905), OCPtSpecs.cc(20905),
		OCPtSpecs.hh(20905), testOCPspecs.cc(20905)
	inserted supervisory thread. Removed unused variables

	FILES: OCPtSpecs.cc(20976), OCPtSpecs.hh(20976),
		testOCPspecs.cc(20976)
	Surface control points function implemented

	FILES: OCPtSpecs.cc(20992), testParams.cc(20992)
	Debugging with dplanner

	FILES: OCPtSpecs.cc(21034), OCPtSpecs.hh(21034)
	modified destructor to solve a spread problem. Don't think it has
	been solvevd, need a better test.

	FILES: OCPtSpecs.cc(21187), OCPtSpecs.hh(21187)
	supervisory thread implemented. It is a hack waiting for the full
	CSA 

	FILES: OCPtSpecs.cc(21318), OCPtSpecs.hh(21318)
	more on the supervisory thread

	FILES: testOCPspecs.cc(20977)
	 small fix in the test program

Mon Apr 23 23:17:32 2007	Stefano Di Cairano (stefano)

	* version R1-01b
	BUGS:  
	FILES: OCPtSpecs.cc(20375), OCPtSpecs.hh(20375),
		testOCPspecs.cc(20375)
	conforming to the new library interface

	FILES: OCPtSpecs.cc(20378), testParams.cc(20378)
	checking params

	FILES: OCPtSpecs.cc(20380), testParams.cc(20380)
	debuggin cost function

	FILES: OCPtSpecs.cc(20399), testOCPspecs.cc(20399)
	changed interface according to the new functions in the NGC library

Sun Apr 22 10:54:08 2007	Stefano Di Cairano (stefano)

	* version R1-01a
	BUGS:  
	FILES: OCPtSpecs.cc(20234), OCPtSpecs.hh(20234)
	Minor changes

Tue Apr 17 15:11:18 2007	Stefano Di Cairano (stefano)

	* version R1-00w
	BUGS:  
	FILES: OCPtSpecs.cc(19846), OCPtSpecs.hh(19846)
	Changed to deal with the Flat model of Alice

Tue Apr 10 16:47:47 2007	murray (murray)

	* version R1-00v
	BUGS: 
	New files: test.rddf testParams.ggo
	FILES: Makefile.yam(19259), testParams.cc(19259)
	updated testParams to use gengetopt and send initial/final point
	from rddf

	FILES: testParams.cc(19260)
	added more verbose output

	FILES: testParams.cc(19278)
	added functionality for sending out RDDF (in addition to params)

Fri Mar 23 17:44:55 2007	Stefano Di Cairano (stefano)

	* version R1-00u
	BUGS: 
	FILES: OCPtSpecs.cc(18885), OCPtSpecs.hh(18885),
		testOCPspecs.cc(18885)
	removed unused sockets

Fri Mar 16 12:10:50 2007	Stefano Di Cairano (stefano)

	* version R1-00t
	BUGS: 
	FILES: OCPtSpecs.cc(18005), OCPtSpecs.hh(18005),
		testOCPspecs.cc(18005)
	Changed the constructor to enable/disable threads. By default all
	the threads are enabled.

	FILES: OCPtSpecs.cc(18006), testOCPspecs.cc(18006)
	Added outputs to check the threads are not running.

Fri Mar 16 10:12:31 2007	murray (murray)

	* version R1-00s
	BUGS: 
	FILES: OCPtSpecs.cc(17957)
	verbose flag + reset RDDF before receiving

	FILES: OCPtSpecs.hh(17956)
	added verbose flag

2007-03-16  murray  <murray@gclab.dgc.caltech.edu>

	* OCPtSpecs.cc (getRDDFThread): cleared out tempRDDF before using,
	so that trajectory gets properly stored.  Turns out that RecvRDDF
	will not overwrite existing points in the RDDF that it is passed.

	* OCPtSpecs.cc (getRDDFThread): added debug prints (m_verbose > 1
	=> print program flow, m_verbose > 2 => print data objects)

	* OCPtSpecs.hh (class OCPtSpecs): Added m_verbose to control error
	messages.  Initialized to zero in constructor.

Thu Mar 15 21:31:58 2007	Stefano Di Cairano (stefano)

	* version R1-00r
	BUGS: 
	FILES: OCPtSpecs.cc(17873)
	Changed getRDDFcorridor function

	FILES: OCPtSpecs.cc(17876)
	Fixed small bug into getInitial function

	FILES: OCPtSpecs.cc(17881)
	 Another small bug fixed in get final condition

Thu Mar 15 13:32:30 2007	Stefano Di Cairano (stefano)

	* version R1-00q
	BUGS: 
	FILES: Makefile.yam(17782), OCPtSpecs.cc(17782),
		OCPtSpecs.hh(17782), testOCPspecs.cc(17782),
		testObstacles.cc(17782)
	Conic form of nonlinear constraint tested. Changed implementation
	of the solver mode (reduced n_of_ops. Interface with tplanner moved
	to interfaces module

	FILES: OCPtSpecs.cc(17749), OCPtSpecs.hh(17749),
		testOCPspecs.cc(17749), testObstacles.cc(17749)
	Computation of the Quadratic constrainits is now performed in Conic
	Form. This reduces the number of operations required by the solver

Wed Mar 14 19:41:33 2007	Stefano Di Cairano (stefano)

	* version R1-00p
	BUGS: 
	FILES: OCPtSpecs.cc(17683), testOCPspecs.cc(17683),
		testObstacles.cc(17683)
	Modified handling of the nonllinear constraints to comply with
	SNOPT specs (no constant terms in the constraints function

Wed Mar 14 17:46:00 2007	Stefano Di Cairano (stefano)

	* version R1-00o
	BUGS: 
	FILES: OCPtSpecs.cc(17648), testOCPspecs.cc(17648),
		testObstacles.cc(17648)
	Constraint evaluation and Jacobians tested wrt spoof-data. Few
	errors corrected. Results checked analitically.

Tue Mar 13 19:14:32 2007	Stefano Di Cairano (stefano)

	* version R1-00n
	BUGS: 
	FILES: OCPtSpecs.cc(17515), OCPtSpecs.hh(17515)
	Modified RDDF receiver to avoid generating a dummy thread

Tue Mar 13 17:20:38 2007	Stefano Di Cairano (stefano)

	* version R1-00m
	BUGS: 
	FILES: OCPtSpecs.cc(17503)
	Tested wrt test_RDDFTalkerRecv 

Tue Mar 13 14:16:15 2007	Stefano Di Cairano (stefano)

	* version R1-00l
	BUGS: 
	FILES: OCPtSpecs.cc(17444), OCPtSpecs.hh(17444),
		OTG_AliceInterface.hh(17444)
	moved RDDF talker here. Ready for integration with dplanner

Mon Mar 12 19:20:39 2007	Stefano Di Cairano (stefano)

	* version R1-00k
	BUGS: 
	New files: OTG_AliceInterface.hh testObstacles.cc
	FILES: Makefile.yam(17277), OCPtSpecs.cc(17277),
		OCPtSpecs.hh(17277)
	Added obstacles computation and communication using skynettalker.
	Added virtual interface for parameters and other Alice-related
	functions. Added threads synchronization.

	FILES: Makefile.yam(17282), OCPtSpecs.cc(17282),
		OCPtSpecs.hh(17282)
	Test of the obstacles reception succeeded. Few canges in the names.

Sun Mar 11 20:13:35 2007	murray (murray)

	* version R1-00j
	BUGS: 
	Deleted files: OTG_InterfacesX.hh
	FILES: Makefile.yam(17169), OCPtSpecs.hh(17169)
	uses OTG interfaces

Sun Mar 11 19:34:31 2007	Stefano Di Cairano (stefano)

	* version R1-00i
	BUGS: 
	New files: testParams.cc
	Deleted files: OCPparamsTalker.cc OCPparamsTalker.hh
	FILES: Makefile.yam(17156), OCPtSpecs.cc(17156),
		OCPtSpecs.hh(17156), OTG_InterfacesX.hh(17156),
		testOCPspecs.cc(17156)
	Removed OCPparamsTalker, using SkynetTalker now. Added test program
	to check Params receive. Locking consistency corrected. Mode
	definition moved to OCPtSpecs.hh 

Sat Mar 10 20:13:16 2007	Stefano Di Cairano (stefano)

	* version R1-00h
	BUGS: 
	FILES: OCPparamsTalker.cc(17051), OCPparamsTalker.hh(17051),
		OCPtSpecs.cc(17051), OCPtSpecs.hh(17051),
		OTG_InterfacesX.hh(17051)
	Added accessors for OCP parameters, initial condition and final
	condition.

	FILES: OCPtSpecs.cc(17008), OCPtSpecs.hh(17008)
	introducing static obstacles in OCP. Interface is ok.

Thu Mar  8 15:47:04 2007	Stefano Di Cairano (stefano)

	* version R1-00g
	BUGS: 
	New files: OCPparamsTalker.cc OCPparamsTalker.hh
	FILES: Makefile.yam(16795), OCPtSpecs.cc(16795),
		OCPtSpecs.hh(16795)
	Added interface to receive OCP parameters, including initial and
	final conditions. Added a Talker to send/receive OCP parameters,
	which can be possibly moved to interfaces in the future.

Sat Mar  3 16:21:30 2007	Jeremy Ma (jerma)

	* version R1-00f
	BUGS: 
	FILES: Makefile.yam(16404), OCPtSpecs.cc(16404),
		OCPtSpecs.hh(16404), testOCPspecs.cc(16404)
	modified OCPtSpecs to inherit StateClient since the maintained map
	needs to be updated to the vehicle location each time through the
	loop; this required modified changes to the makefile and
	testOCPspecs.cc 

	FILES: testOCPspecs.cc(16405)
	modified the testOCPspecs.cc file so that it doesn't use a
	hard-coded SNkye but rather asks you for it. minor change.

Sat Mar  3 13:41:19 2007	Jeremy Ma (jerma)

	* version R1-00e
	BUGS: 
	FILES: OCPtSpecs.cc(16341), OCPtSpecs.hh(16341)
	now that we can send deltas through the CMap class, there is no
	longer any need for the CostMapMsg.h interface; these changes
	reflect that

Fri Mar  2 17:47:09 2007	Jeremy Ma (jerma)

	* version R1-00d
	BUGS: 
	FILES: OCPtSpecs.cc(16170), OCPtSpecs.hh(16170)
	modified ocpspecs to handle sending deltas since cmap send deltas
	function didn't seem to be working

Thu Mar  1 19:37:40 2007	Melvin Flores (mflores)

	* version R1-00c
	BUGS: 
	FILES: Makefile.yam(16066), OCPtSpecs.hh(16066)
	set up to use proprietary NGC code if OTG_HOME is defined

Wed Feb 28 13:42:50 2007	Stefano Di Cairano (stefano)

	* version R1-00b
	BUGS: 
	FILES: Makefile.yam(15963), OCPtSpecs.cc(15963),
		OCPtSpecs.hh(15963), OTG_InterfacesX.hh(15963),
		testOCPspecs.cc(15963)
	Debugging ocpspec module. Tested on a simple test program. The
	objects is instanciated and the threads are started corrretly.
	Nobody is sending delta for now.

Tue Feb 27 23:37:29 2007	Stefano Di Cairano (stefano)

	* version R1-00a
	BUGS: 
	New files: OCPtSpecs.cc OCPtSpecs.hh OTG_InterfacesX.hh
		testOCPspecs.cc
	FILES: Makefile.yam(15933)
	initial release of OCPSPECS module. It compiles, but it has not
	been tested yet

Tue Feb 27 22:03:09 2007	Stefano Di Cairano (stefano)

	* version R1-00
	Created ocpspecs module.



















































