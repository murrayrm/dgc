#include <iostream.h>
#include "ocpspecs/OCPtSpecs.hh"
int main(int argc, char** argv)
{
    int skynet_key;
    cout << "Please enter Skynet Key:" << endl;
    cin >> skynet_key;
    int Npoints1 = 3 ;
    int Npoints2 = 4 ;
    int NofDim = 2 ;
	double** vertices1;
	double** vertices2;
    OCPtSpecs* pOCPspecs = new OCPtSpecs(skynet_key,false,1,true,false,false,false,false);
    CAlgebraicGeometry* algGeom = new CAlgebraicGeometry(); 
	vertices1 = new double*[NofDim];
    vertices2 = new double*[NofDim];
	for(int k=0; k<NofDim;k++)
	{
	   vertices1[k] = new double[Npoints1];
	}
	/*
	vertices1[0][0] = -2.5215;
	vertices1[1][0] = -44.6711;
	vertices1[0][1] = 1.13610;  
	vertices1[1][1] = -44.67957;
	vertices1[0][2] = -2.5299;
	vertices1[1][2] = -48.3088;
	vertices1[0][3] = 1.127740;
	vertices1[1][3] = -48.3142; 
    */
	
	vertices1[0][0] = 2;
	vertices1[1][0] = 5;
	vertices1[0][1] = 5;  
	vertices1[1][1] = 2;
	vertices1[0][2] = 2;
	vertices1[1][2] = 2;
	
	CPolytope* p1;
	CPolytope* p2;
    p1 = new CPolytope(&NofDim, &Npoints1,  (const double** const)  vertices1 );
	bool myFlag=true;
	algGeom->setDebugFlag(&myFlag);
	algGeom->FacetEnumeration(p1);
    algGeom->VertexEnumeration(p1);
    cout << p1 << endl;
    for(int k=0; k<NofDim;k++)
	{
	   vertices2[k] = new double[Npoints2];
	}
	vertices2[0][0] = -1;
	vertices2[1][0] = -1;
	vertices2[0][1] = -1;
	vertices2[1][1] = +1;
	vertices2[0][2] = +1;
	vertices2[1][2] = +1;
	vertices2[0][3] = +1;
	vertices2[1][3] = -1;
    p2 = new CPolytope(&NofDim, &Npoints2,  (const double** const) vertices2 );
    algGeom->VertexEnumeration(p2);
	algGeom->FacetEnumeration(p2);
	cout << p2 << endl;
	int counter = 0;
    double vInt = pOCPspecs->polyIntersectionVolume(p1,p2);
	cout <<vInt << endl << endl;
	double myPoint[2];
	myPoint[0] = 4;
	myPoint[1]=2;
    //cout << " " << vInt << endl;
	while(true)
	{
        cout << "sleeping: "<< ++counter << endl;
        pOCPspecs->startSolve();
		cout<< endl << pOCPspecs->isInsideCorridor(myPoint ,2) << endl<< endl;
        cout<< "Cost : " << pOCPspecs->getCostMapValue(0,0) << endl;
		cout<< "Gradient : (" << pOCPspecs->getGradientMapValue(0,0,0)<< "," << pOCPspecs->getGradientMapValue(0,0,1)<<") " <<  endl;
		//pOCPspecs->printPolyCorridor();
		pOCPspecs->endSolve();
		sleep(1);
	    //pOCPspecs->printPolyCorridor();
       // float cost = pOCPspecs->getCostMapValue(2,1);
//		cout << "Cost : " << cost << endl;
    //    point2 grad =  pOCPspecs->getCostMapGradient(2,1);
//		cout << "Gradient : " << grad.x<< " , "<< grad.y << endl;
    }
  
}
