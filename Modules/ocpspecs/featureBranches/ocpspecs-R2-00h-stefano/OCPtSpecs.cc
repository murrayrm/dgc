/*
 Definition of the OCPTSPECS class that provides the interface
 between Tplanner and Dplanner

 Stefano Di Cairano mar-07
 */

#include "OCPtSpecs.hh"
#include "interfaces/TpDpInterface.hh"
#include "interfaces/AliceFlat_Definitions.hh"
#include <highgui.h>
#define MAX_DELTA_SIZE 1000000  //taken from plannerModule
#define MAP_EXPANSION 5.0
#define GRAD_SMOOTHING 2

OCPtSpecs::OCPtSpecs(int sn_key, bool useAstate, int verbose, bool useCostMap, bool useObst, bool useParams, bool useRDDF, bool usePolyCorridor)
    : CSkynetContainer(MODdynamicplanner, sn_key),
	  //CStateClient(false),
      CRDDFTalker(false),
      m_paramsTalker(sn_key,SNocpParams,MODdynamicplanner),
      m_statusTalker(sn_key,SNdplStatus,MODdynamicplanner),
      m_obstTalker(sn_key,SNocpObstacles,MODdynamicplanner),
      m_verbose(verbose),
	  m_costMapEst(sn_key,true, false)
{
    m_polyCorridor = NULL; //otherwise valgrind complains     
    m_skynetKey = sn_key;
    cout << "Skynet key: " << sn_key << endl; 
    m_quit = 0;	

    m_useParams = useParams;
    m_useObstacles = useObst;
    m_useRDDF = useRDDF;
    m_useCostMap = useCostMap;
    m_usePolyCorridor =usePolyCorridor;
    m_nPolytopes = 0;

    DGCcreateMutex(&m_dplannerStatusMutex);
    DGCcreateCondition(&m_notPausedCond); 

    DGCstartMemberFunctionThread(this, &OCPtSpecs::supervisoryThread); 
    DGCcreateMutex(&m_specsDataMutex);
    if(m_useParams)
    {
        DGCcreateCondition(&m_paramsRecvdCond);
        DGCstartMemberFunctionThread(this, &OCPtSpecs::getOCPparamsThread) ;
    }
    if(m_useObstacles)
    {
        DGCcreateCondition(&m_obstRecvdCond);  
        DGCstartMemberFunctionThread(this, &OCPtSpecs::getOCPobstThread) ;
    }
    if(m_useRDDF)
    {
        m_RDDFSocket =  m_skynet.listen(SNrddf,MODtrafficplanner);   
        DGCcreateCondition(&m_RDDFRecvdCond);  
		DGCstartMemberFunctionThread(this, &OCPtSpecs::getRDDFThread) ;
    }    
    if(m_usePolyCorridor)
    {
        DGCcreateCondition(&m_polyCorridorRecvdCond);  
        DGCstartMemberFunctionThread(this, &OCPtSpecs::getPolyCorridorThread); 
    }
    if(m_useCostMap)
    {   
	    m_costMapEst.initMap(&m_costMap);
        m_costDx = m_costMap;   
		m_costDy = m_costMap;
		DGCcreateCondition(&m_costMapRecvdCond); 
        DGCstartMemberFunctionThread(this, &OCPtSpecs::getCostMapThread);  
    }
}


OCPtSpecs::~OCPtSpecs(void)
{
	//check: I don't think this is enough!
    m_quit = 1;
	usleep(100000);
	DGCdeleteMutex(&m_specsDataMutex);
	DGCdeleteCondition(&m_paramsRecvdCond);
	DGCdeleteCondition(&m_obstRecvdCond);
	DGCdeleteCondition(&m_costMapRecvdCond);
	DGCdeleteCondition(&m_RDDFRecvdCond);
	DGCdeleteCondition(&m_polyCorridorRecvdCond);
}

//////////////////////////////////////////
////////// Receiving Threads /////////////
//////////////////////////////////////////


void OCPtSpecs::getPolyCorridorThread()
{
    
    CAlgebraicGeometry* algGeom = new CAlgebraicGeometry();
    double** vertices;
	int NofDim = 2;
    SkynetTalker<sendCorr> recCorTalker= SkynetTalker<sendCorr>(m_skynetKey, SNpolyCorridor,MODdynamicplanner);
	sendCorr corridor;
    while(!m_quit)
	{
        
		for(int i=0;i<corridor.size();i++)
            corridor[i].clear();
		corridor.clear();
        recCorTalker.receive(&corridor);
		//cout << "corridor recvd " << endl;
		DGClockMutex(&m_specsDataMutex);
		if(m_polyCorridor != NULL)
		{
		    for(int i=0;i<m_nPolytopes;i++)
			{
			    if(m_polyCorridor[i] !=NULL)
		            delete m_polyCorridor[i];
		    }
            delete[] m_polyCorridor;
		}
		//deleting waypoints
		if(m_waypoints != NULL)
		{
		    for(int i=0;i<NofDim;i++)
		        delete[] m_waypoints[i];
            delete[] m_waypoints;
		}

        m_nPolytopes = corridor.size();
	    cout << "Receiving "<< m_nPolytopes <<" polytopes " <<  endl;
		usleep(10000);
		m_polyCorridor=new CPolytope*[m_nPolytopes];
        //for(int i=0; i<m_nPolytopes; i++)
        int i = 0 ;
        bool isCorrect = true ;
		int centroidCorrect = -1;
        while( (i<m_nPolytopes) )
        {
			int polySize = corridor[i].size();
	        vertices = new double*[NofDim];
	        for(int j=0;j<NofDim;j++)
                vertices[j] = new double[polySize];
            for(int j=0;j<polySize;j++)
		    {
                vertices[0][j] = corridor[i][j].x ;
                vertices[1][j] = corridor[i][j].y ;
            }    
            m_polyCorridor[i] = new CPolytope(&NofDim, &polySize, (const double**) vertices);
			ALGErrorType algErr = algGeom->VertexAndFacetEnumeration(m_polyCorridor[i]);
            
			if(algErr == alg_NoError)
            {
                centroidCorrect = algGeom->ComputeCentroid(m_polyCorridor[i]);
				if(centroidCorrect != 0)
				{
				    cout << " Problems while extracting the centroid " << endl;
				    algErr = alg_HasRays;
			    }
            }
			if(algErr == alg_NoError)
			{
            }
			else
            {
                cout << "Corridor invalid. AlgGeom Error type (AlgGeom_Interfaces.hh) : " << algErr << endl;
				cout << "Dimension : " << NofDim << " N of Vertices: "<< polySize <<  " vertices : " << endl;
				for(int j = 0 ; j  < NofDim ; j++)
				{
				    for(int k = 0; k < polySize ; k ++)
					    cout << " " << vertices[j][k] ;
				    cout << endl;
                
				}
                isCorrect = false ;                
            }
	        for(int j=0;j<NofDim;j++)
                delete[]  vertices[j];
            delete[] vertices;
            i++;
        }
        if(isCorrect)
		{
                if(!m_polyCorridorRecvdCond.bCond)
				{
//                    cout << " a new valid corridor has been received" << endl;
					for(int k = 0; k< m_nPolytopes; k++)
					     cout << k << endl << m_polyCorridor[k] << endl;
					DGCSetConditionTrue(m_polyCorridorRecvdCond);
			    }
	    }
		else
		{
//				cout << "Waiting for a new valid corridor on a mutex locked condition."  << endl;
                m_polyCorridorRecvdCond.bCond = false;
	    }
	    
/* 
        if(isCorrect)
        {       
        //allocating the waypoints
            m_nWaypoints = m_nPolytopes - 1;
            m_waypoints = new double*[NofDim];
            for(int i = 0; i<NofDim; i++)
                m_waypoints[i] = new double[m_nWaypoints];       
        
        //computing the waypoints ...
        
            double tmpWP[2];
        
	        for(int i = 0 ; i < m_nPolytopes-1; i++)
	        {
	            int Nrows1 = m_polyCorridor[i]->getNofRows(); 
	            int Ncols1 = m_polyCorridor[i]->getNofColumns();
			    double** A1;
			    double* b1;
			    b1 = new double[Nrows1];
			    A1= new double*[Nrows1];
			    for(int j=0;j<Nrows1;j++)
			        A1[j] = new double[Ncols1];
	        
			    m_polyCorridor[i]->getA(&Nrows1,&Ncols1,A1);
			    m_polyCorridor[i]->getb(&Nrows1,b1);
	
	            int Nrows2 = m_polyCorridor[i+1]->getNofRows(); 
	            int Ncols2 = m_polyCorridor[i+1]->getNofColumns();
			    double** A2;
			    double* b2;
			    b2 = new double[Nrows2];
			    A2 = new double*[Nrows2];
			    for(int j=0;j<Nrows2;j++)
			        A2[j] = new double[Ncols2];
			    m_polyCorridor[i+1]->getA(&Nrows2,&Ncols2,A2);
			    m_polyCorridor[i+1]->getb(&Nrows2,b2);
	            
				double** Aint;
	            double* bint;
				bint = new double[Nrows2+Nrows1];
				Aint = new double*[Nrows2+Nrows1];
	            for(int j=0;j<Nrows1;j++)
				{
				    Aint[j] = A1[j];   //I use the memory which has been already allocated
				    bint[j] = b1[j];
	            }
	            for(int j=Nrows1;j<(Nrows1+Nrows2);j++)
				{
				    Aint[j] = A2[j-Nrows1];   //I reuse the memory which has been already allocated
				    bint[j] = b2[j-Nrows1];
	            }
				int NrowsInt = Nrows1+Nrows2 ;
				int NcolsInt = Ncols1 ;
				
				int NofVtxInt = 4;
		        double** verticesInt;
				verticesInt = new double*[NofDim];
		        for(int j=0;j<NofDim;j++)
		            verticesInt[j] = new double[NofVtxInt];
	            algGeom->VertexEnumeration(&NrowsInt,&NcolsInt,(const double**)Aint, (const double*)bint, &NofDim, &NofVtxInt, verticesInt);
				for(int j=0;j<NofDim;j++)
	                delete[] verticesInt[j];
		        delete[] verticesInt;
	
		        verticesInt = new double*[NofDim];
		        for(int j=0;j<NofDim;j++)
		            verticesInt[j] = new double[NofVtxInt];
			
	            algGeom->VertexEnumeration(&NrowsInt,&NcolsInt,(const double**)Aint, (const double*)bint, &NofDim, &NofVtxInt, verticesInt);
	            CPolytope* poly = new CPolytope(&NofDim, &NofVtxInt, (const double** const) verticesInt);
				algGeom->VertexAndFacetEnumeration(poly);
	           	algGeom->ComputeCentroid(poly); 
	            poly->getCentroid(&NofDim, tmpWP );
	            for(int j = 0; j<NofDim; j++)
	                m_waypoints[j][i] = tmpWP[j];            
	            
				delete poly;
				for(int j=0;j<NofDim;j++)
	                delete[] verticesInt[j];
		        delete[] verticesInt;

	            delete[] bint;   
	            delete[] b2;
	            delete[] b1;
	            delete[] Aint;         //note the second level pointers point to memory shared with other pointers. I deallocate from those.
			    for(int j=0;j<Nrows1;j++)
				    delete[] A1[j];
	            delete[] A1;
	            for(int j=0;j<Nrows2;j++)
                    delete[] A2[j];
                delete[] A2;
            
            } //end of for (all WP)
        
        } //end IF (computation of waypoints)             
*/		
		DGCunlockMutex(&m_specsDataMutex);
    
    }//end of while (active loop)

    delete algGeom;		   
}



void OCPtSpecs::getCostMapThread()
{
    bool mapRec = false;
    while(!m_quit)
    {
	   // cout << "Receiving maps " << endl;
        DGClockMutex(&m_specsDataMutex);
		mapRec = m_costMapEst.updateMap(&m_costMap);
		if(mapRec)
		{  // cout << "computing gradient!" << endl;
		    getGradientMap(); //if the map was updated, compute the gradient map!
           // cout << "gradient computed!" <<endl;
	    }
		DGCunlockMutex(&m_specsDataMutex);
		if( (mapRec) && (!m_costMapRecvdCond.bCond) )
        {
            DGCSetConditionTrue(m_costMapRecvdCond);
        }
		cvWaitKey(50);
	}
}



void OCPtSpecs::getOCPparamsThread()
{
    bool paramsCorrect;
    OCPparams tempParams;
	while(!m_quit)
	{	
        paramsCorrect = false;
        paramsCorrect = m_paramsTalker.receive(&tempParams);
		if(! paramsCorrect)
		{
		    cerr << "error in Parameters message" << endl;
        }
		else
		{
		    DGClockMutex(&m_specsDataMutex);
		    m_OCPparams = tempParams;
		    if(!m_paramsRecvdCond.bCond)
			{
		       DGCSetConditionTrue(m_paramsRecvdCond);   
            }
			DGCunlockMutex(&m_specsDataMutex);
		    if (m_verbose) 
	           cout << "Received new Params" << endl;			
		}
	}
}

void OCPtSpecs::getRDDFThread()
{
    bool RDDFcorrect;
    RDDF tempRDDF;
    while(!m_quit)
	{
		// Reset the tempRDDF so that we always load full RDDF
        tempRDDF.clear();

	    // Now read in the data from the socket
        RDDFcorrect = RecvRDDF(m_RDDFSocket, &tempRDDF);

	    if (m_verbose)
	        cout << "OCPtSpecs::getRDDFThread received RDDF" << endl;
	
	    if(!RDDFcorrect)
	    {
	        cerr <<"Error while recv RDDF" << endl;
	    }
	    else
	    {
            DGClockMutex(&m_specsDataMutex);
	        m_RDDF = tempRDDF;
	        if (m_verbose > 1)
	          cout << "OCPtSpecs::getRDDFThread RDDF object copied"<< endl;
	        if (m_verbose > 2) tempRDDF.print();
	        DGCunlockMutex(&m_specsDataMutex);
        }
        if(!m_RDDFRecvdCond.bCond)
        {
            DGCSetConditionTrue( m_RDDFRecvdCond);
	    }   				
    }
}

void OCPtSpecs::getOCPobstThread()
{
    bool obstCorrect;
    OCPobstacles tempObst;
	while(!m_quit)
	{		
        obstCorrect = false;
        obstCorrect = m_obstTalker.receive(&tempObst);
		if(!obstCorrect)
		{
		    cerr << "OCPtSpecs:getOCPobstThread error" << endl;
        }
		else
		{
		    if (m_verbose) 
		        cout << "Received obstacles: " << tempObst.ObstNumber <<  endl;
                    
            DGClockMutex(&m_specsDataMutex);
            m_Qconstr.Nconstr=tempObst.ObstNumber;            
            // Obstacles are sent by mapper as (x-xc)H(x_xc)<=1
            // We store them as   xHx+Sx+Kx <= 1
            // Hence here we compute the H, S, K components and store them
	        for(int i=0;i<tempObst.ObstNumber;i++)
		    {
                m_Qconstr.S[i][0] = -2*( tempObst.ObstHessian[i][0][0]*tempObst.ObstCenter[i][0] + 
			                tempObst.ObstHessian[i][0][1]*tempObst.ObstCenter[i][1]) ;
	            m_Qconstr.S[i][1] = -2*( tempObst.ObstHessian[i][1][0]*tempObst.ObstCenter[i][0] + 
			                tempObst.ObstHessian[i][1][1]*tempObst.ObstCenter[i][1]) ;
                m_Qconstr.H[i][0][0] = tempObst.ObstHessian[i][0][0];
                            m_Qconstr.H[i][0][1] = tempObst.ObstHessian[i][0][1]; 
	            m_Qconstr.H[i][1][1] = tempObst.ObstHessian[i][1][1]; 
	            m_Qconstr.H[i][1][0] = tempObst.ObstHessian[i][1][0];
                m_Qconstr.K[i] = tempObst.ObstCenter[i][0] * tempObst.ObstHessian[i][0][0] * tempObst.ObstCenter[i][0]  +
			                2 * tempObst.ObstCenter[i][0] * tempObst.ObstHessian[i][0][1] * tempObst.ObstCenter[i][1] +
						    tempObst.ObstCenter[i][1] * tempObst.ObstHessian[i][1][1] * tempObst.ObstCenter[i][1];                         
		    }
            //zeroing the unapplied constraints -> this is not necessary, but for now ....
	        for(int i=tempObst.ObstNumber;i<MAX_OBSTACLES;i++)
	        {
                m_Qconstr.S[i][0] = 0;
	            m_Qconstr.S[i][1] = 0;
                m_Qconstr.H[i][0][0] = 0;
                m_Qconstr.H[i][0][1] = 0; 
	            m_Qconstr.H[i][1][1] = 0; 
	            m_Qconstr.H[i][1][0] = 0;
                m_Qconstr.K[i] = 0;
	        }
		    
		    if(!m_obstRecvdCond.bCond)
		    {    DGCSetConditionTrue( m_obstRecvdCond);  
            }
            DGCunlockMutex(&m_specsDataMutex);
        }
    }
}


///////////////////////////////////////
/////   Transmission thread      //////
///////////////////////////////////////

void OCPtSpecs::supervisoryThread()
{
    SkynetTalker<bool> supervisoryTalker= SkynetTalker<bool>(m_skynetKey,SNtrajPause,MODdynamicplanner);
    bool supervisoryMsg;
	while(!m_quit)
	{
        //receive pause message
        supervisoryTalker.receive(&supervisoryMsg);
		//the following lines set the conditions to false. Hence before to start a new optimization we wait for a new set of specs
		if(supervisoryMsg)
		{
		    DGClockMutex(&m_dplannerStatusMutex);
            //replying to tplanner
            #warning this is a temporary hack. We will move to the real CSS soon, I promise!
            m_dplannerStatus.OCPsolved = false;
			m_dplannerStatus.solverFlag = -1;
            m_statusTalker.send(&m_dplannerStatus);    
		    DGCSetConditionFalse(m_notPausedCond);
			DGClockMutex(&m_dplannerStatusMutex);
			//propagating the message to trajfollow
			// to be done
             #warning we may have consistency problem if we don't check the status before sending out a new trajectory.
            //blocking the computation  
			DGClockMutex(&m_specsDataMutex);
            DGCSetConditionFalse( m_costMapRecvdCond);
            DGCSetConditionFalse(m_paramsRecvdCond);
            DGCSetConditionFalse(m_obstRecvdCond);
		    DGCunlockMutex(&m_specsDataMutex);

	    }
    }
}



int OCPtSpecs::setStatus(int index, int value)
{
    if(index<= 0 || index >= N_STATUS)
        return 0;
    else
    {
        switch (index)
	{
	    case 1 : 
	        m_dplannerStatus.OCPsolved = (bool)value;
		break;
	    case 2 : 
	        m_dplannerStatus.solverFlag = value;
		break;
	    case 3 : 
	        m_dplannerStatus.corridorGenerated = (bool)value;
		break;
	    case 4 : 
	        m_dplannerStatus.corridorFlag = value;
	        break;
	    default : 
	        return 0;

	 }  	      

        return 1;
    }

}


////////////////////////////////////////////
///////  Common Accessors         //////////
////////////////////////////////////////////
    
    
void  OCPtSpecs::getInitialCondition(const BoundsType* const ubtype, double* const initCond)
{
    bool valuecheck = false;
    if( *ubtype == ubt_LowerBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          initCond[m_i]=m_OCPparams.initialConditionLB[m_i];	  
        }
	valuecheck = true;
    }
    
    if( *ubtype == ubt_UpperBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          initCond[m_i]=m_OCPparams.initialConditionUB[m_i];	  
        }
	valuecheck = true;
    }
    if(valuecheck==false)
    {
        cout << "wrong value for initial bounds type" <<endl;
        exit(1);
    }

    return ;
}


void  OCPtSpecs::getFinalCondition(const BoundsType* const ubtype, double* const finalCond)
{
    bool valuecheck = false;
    if( *ubtype == ubt_LowerBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          finalCond[m_i]=m_OCPparams.finalConditionLB[m_i];	  
        }
	valuecheck = true;
    }
    
    if( *ubtype == ubt_UpperBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          finalCond[m_i]=m_OCPparams.finalConditionUB[m_i];	  
        }
	valuecheck = true;
    }
    if(valuecheck==false)
    {
        cout << "wrong value for final bounds type" <<endl;
        exit(1);
    }

    return ;
}

void  OCPtSpecs::getParams(double* const params)
{


    for(m_i=0;m_i<PARAMS_NUMBER;m_i++)
    {
        params[m_i]=m_OCPparams.parameters[m_i];	  
    }
    params[12] = m_OCPparams.initialConditionLB[VELOCITY_IDX_C];
    params[13] = m_OCPparams.finalConditionLB[VELOCITY_IDX_C];
    params[14] = m_OCPparams.initialConditionLB[ACCELERATION_IDX_C];
    params[15] = m_OCPparams.finalConditionLB[ACCELERATION_IDX_C];
   
    return ;
}

void  OCPtSpecs::getMode(int* mode)
{     
    *mode = m_OCPparams.mode; 
    return ;
}


double OCPtSpecs::getCostMapValue(double x, double y)
{
   double res=-10;
   point2 p = point2(x,y);
   res = m_costMap.getPixelLoc(p);
   return (double)res;
}


//////////////////////////////////
//// START-END Computation  //////
//////////////////////////////////


void OCPtSpecs::startSolve()
{
    if(m_verbose)
	{
        cout << "Request to solve. RDDF: "<< m_useRDDF << " Obst: " << m_useObstacles<< " Map: "<< m_useCostMap << " Params: " << m_useParams <<" Poly: " << m_usePolyCorridor <<endl;
	    cout <<"Have RDDF: " << m_RDDFRecvdCond.bCond << " Have Obst: " <<m_obstRecvdCond.bCond << " Have Params: " << m_paramsRecvdCond.bCond << " Have Cost: " <<m_costMapRecvdCond.bCond << " Have Poly: " << m_polyCorridorRecvdCond.bCond <<endl;
	}
    if(m_useRDDF)
        DGCWaitForConditionTrue(m_RDDFRecvdCond);
    
    if(m_useParams)
        DGCWaitForConditionTrue(m_paramsRecvdCond);
    if(m_useObstacles)
        DGCWaitForConditionTrue( m_obstRecvdCond);
    
    if(m_useCostMap)
       DGCWaitForConditionTrue( m_costMapRecvdCond);
   
    if(m_usePolyCorridor)
        DGCWaitForConditionTrue(m_polyCorridorRecvdCond);
   
    DGClockMutex(&m_specsDataMutex);    
    if(m_verbose)
        cout << "Ready to solve."<< endl;    
    return ;
}

void OCPtSpecs::endSolve()
{
   // bool result = m_statusTalker.send(&m_dplannerStatus);    
    DGCunlockMutex(&m_specsDataMutex);    
    
    if(m_verbose)
        cout << "Finished solving" << endl; 
    return ;
}


////////////////////////////////
/////// GRADIENT MAPS    ///////
////////////////////////////////

double OCPtSpecs::getGradientMapValue(double x, double y, int component)
{
   double res=0;
   point2 p = point2(x,y);
   if(component == 0)
   {
       res = m_costDx.getPixelLoc(p);
   }
   if(component == 1)
   {
       res = m_costDy.getPixelLoc(p);
   }
   
   return (double)res;
}

void OCPtSpecs::getGradientMap()
{
    IplImage* p_img=m_costMap.getImage();
    if( (m_costMap.getWidth() != m_costDx.getWidth() ) || (m_costMap.getHeight() != m_costDx.getHeight() ) )
    {
        m_costDx = m_costMap;
        m_costDy = m_costMap;

    }
    cvSobel(p_img, m_costDx.getImage(), 1, 0, 5 );
    cvSobel(p_img, m_costDy.getImage(), 0, 1, 5 );
    
	IplImage* img = cvCloneImage(m_costDx.getImage());
    double minVal, maxVal;
    cvMinMaxLoc(img, &minVal, &maxVal);
    cvSubS(img, cvRealScalar(minVal), img);
    if (maxVal != minVal)
      cvScale(img, img, 1/(maxVal - minVal));
    cvNamedWindow("Dx",CV_WINDOW_AUTOSIZE);
    cvShowImage("Dx",img);
    cvReleaseImage(&img);
    
	img = cvCloneImage(m_costDy.getImage());
    cvMinMaxLoc(img, &minVal, &maxVal);
    cvSubS(img, cvRealScalar(minVal), img);
    if (maxVal != minVal)
      cvScale(img, img, 1/(maxVal - minVal));
    cvNamedWindow("Dy",CV_WINDOW_AUTOSIZE);
    cvShowImage("Dy",img);
    cvReleaseImage(&img);
}


point2 OCPtSpecs::getCostMapGradient(double x, double y)
{
   point2 upperLeft =  m_costMap.getUpperLeft();
   point2 lowerRight = m_costMap.getLowerRight();
   
   float dx = (lowerRight.x-upperLeft.x)/m_costMap.getWidth() ;
   float dy = (lowerRight.y-upperLeft.y)/m_costMap.getHeight() ;
   float derX = 0.0 ;
   float derY = 0.0 ;
   if(GRAD_SMOOTHING  < 1)
   {
       derX += ( (getCostMapValue(x+dx,y)-getCostMapValue(x,y))/dx );
       derY += ( (getCostMapValue(x,y+dy)-getCostMapValue(x,y))/dy );   
   }
   else
   {  

       for(int i=1;i<=GRAD_SMOOTHING ;i++) //smooth the gradient by loking at the numerical gradients with increments +/- 
       {   
           derX +=   ( (getCostMapValue(x+i*dx,y)-getCostMapValue(x,y))/(i*dx)) - ( (getCostMapValue(x-i*dx,y)-getCostMapValue(x,y))/(i*dx) );
           derY +=   ( (getCostMapValue(x,y+i*dy)-getCostMapValue(x,y))/(i*dy)) - ( (getCostMapValue(x,y-i*dy)-getCostMapValue(x,y))/(i*dy) );  
       }
       derX = derX/(2*GRAD_SMOOTHING);
       derY = derY/(2*GRAD_SMOOTHING);      
   }
   point2 res = point2(derX,derY);
   return res;
}


///////////////////////////////////
///// Printing Function    ////////
///////////////////////////////////


void OCPtSpecs::printPolyCorridor()
{
   cout << endl<< endl << endl <<  "Printing corridor information " << endl << endl << endl; 
   cout << "Number of polytopes: " << m_nPolytopes << endl; 
   for(int i=0;i<m_nPolytopes;i++)
      cout <<m_polyCorridor[i] << endl;

}
