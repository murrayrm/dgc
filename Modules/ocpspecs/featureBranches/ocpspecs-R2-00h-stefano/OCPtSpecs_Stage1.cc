#include "OCPtSpecs.hh"

/** this hfunction has been introduced for s1planner
 * it overcomes the problem of the end point not being legal
 * At the moment is very rough, it returns the centroid of the last polytope.
 * For the moment the current endpoint is not even used so that the pointer can be NULL 
*/
void OCPtSpecs::getLegalEndPoint(int NofDim, double* illegalEndPoint, double* legalEndPoint)
{
    m_polyCorridor[m_nPolytopes-1]->getCentroid(&NofDim, legalEndPoint); 
    
}




void OCPtSpecs::getLegalInitialPoint(int NofDim, double* illegalInitialPoint, double* legalInitialPoint)
{
    
	double currPoint[2] ;
    double dist2cent = DBL_MAX ;
    double minDist = DBL_MAX ;
	int minIdx = 0;
    for(int i = 0 ; i < m_nPolytopes ; i++)
    {
        m_polyCorridor[i] -> getCentroid(&NofDim, currPoint) ;           
        dist2cent = sqrt( pow(currPoint[0]-illegalInitialPoint[0],2) + pow(currPoint[1]-illegalInitialPoint[1],2) ) ;
        if(dist2cent < minDist)
        {
            minDist = dist2cent ;
            minIdx = i ;	
        }        
    }    	
    
    m_polyCorridor[minIdx] -> getCentroid(&NofDim, legalInitialPoint) ;           
}

/* This function computes a better heuristic cost to go used by s1planner 
 * So far it computes the length of the path connecting the polytopes centroids
 * in the future can be modified to use the centroids of the intersection areas
 * 
 * 
 * */

double OCPtSpecs::corridorCostToGo(double* initialPoint, double* finalPoint)
{
    double hCost = 0 ;
	double DhCost = 0 ;
	int NofDim = 2 ;
	double currPoint[2] ;
    double prevPoint[2] ;
	double p1Cost = 0;
	double p2Cost = 0;
	double p12dist = 0;
    prevPoint[0] = initialPoint[0] ; 
    prevPoint[1] = initialPoint[1] ;
	if(m_polyCorridorRecvdCond.bCond)
    {
	   int initialPolytope = isInsideCorridor(initialPoint, NofDim) ;
    
        if( initialPolytope < 0) //bad initial condition, we search for the closer centroid and use that polytope as the initial one
        {
    	    double dist2cent = DBL_MAX ;
        	double minDist = DBL_MAX ;
	    	int minIdx = 0;
        	for(int i = 0 ; i < m_nPolytopes ; i++)
            {
                m_polyCorridor[i] -> getCentroid(&NofDim, currPoint) ;           
                dist2cent =  pow(currPoint[0]-initialPoint[0],2) + pow(currPoint[1]-initialPoint[1],2)  ; //removed sqrt since it is useless
                if(dist2cent < minDist)
                {
                   minDist = dist2cent ;
                   minIdx = i ;	
                }        
            }    	
            initialPolytope = minIdx ;
        }
        int finalPolytope   = isInsideCorridor(finalPoint, NofDim) ;
        if( finalPolytope < 0) // bad final condition. we use the last polytope as final polytope-> CHANGE THIS
            finalPolytope = m_nPolytopes - 1 ;
    
    /* we compute the heuristic as the cumulative distance between centroids starting from the one that follows the initial polytope
     * up to the one that preceeds the final polytope. Then we add the distance between the initial point and the first centroid 
     * and the distance between the last centroid and the final point. The distance is now weighted by the average of the cost
	 * at the two extrema.
     */
 //     cout << " computing cost" << endl; 
        for(int i = initialPolytope+1 ; i < finalPolytope ; i++)
        {
            //ALGErrorType algErr = m_polyCorridor[i]->getCentroid(&NofDim, currPoint);
            m_polyCorridor[i]->getCentroid(&NofDim, currPoint);
//		if(algErr != alg_NoError)
//		   cout << "Centroid cannot be recovered: What can we do now??" << endl;
            
		    p1Cost = getCostMapValue(prevPoint[0],prevPoint[1]);
            p2Cost = getCostMapValue(currPoint[0],currPoint[1]) ;
			p12dist = sqrt( pow(currPoint[0]-prevPoint[0],2) + pow(currPoint[1]-prevPoint[1],2) );

			DhCost = (p1Cost+p2Cost)/2*p12dist ;
			hCost+=DhCost;
           
            if((p1Cost < 0)|| (p2Cost<0))
			{
		    cout << "from (" << prevPoint[0] << "," << prevPoint[1] << ") to (" <<currPoint[0]<<  "," << currPoint[1]<<")" << endl;
            cout << "P1 : " << p1Cost <<  " P2 : " << p2Cost << " dist :  " << p12dist << " Dcost :  " << DhCost  << " Cost :  " << hCost << endl;
		    prevPoint[0] = currPoint[0] ;
            prevPoint[1] = currPoint[1] ;       
			} 
        }
    } 
    currPoint[0] = finalPoint[0] ;
    currPoint[1] = finalPoint[1] ;
		    p1Cost = getCostMapValue(prevPoint[0],prevPoint[1]);
            p2Cost = getCostMapValue(currPoint[0],currPoint[1]) ;
			p12dist = sqrt( pow(currPoint[0]-prevPoint[0],2) + pow(currPoint[1]-prevPoint[1],2) );

			DhCost = (p1Cost+p2Cost)/2*p12dist ;
			hCost+=DhCost;
            if((p1Cost < 0)|| (p2Cost<0))
			{
		    cout << "from (" << prevPoint[0] << "," << prevPoint[1] << ") to (" <<currPoint[0]<<  "," << currPoint[1]<<")" << endl;
            cout << "P1 : " << p1Cost <<  " P2 : " << p2Cost << " dist :  " << p12dist << " Dcost :  " << DhCost  << " Cost :  " << hCost << endl;
    //hCost += 0.5*( getCostMapValue(prevPoint[0],prevPoint[1]) + getCostMapValue(currPoint[0],currPoint[1]) ) *
     //                              sqrt( pow(currPoint[0]-prevPoint[0],2) + pow(currPoint[1]-prevPoint[1],2) ) ;
    cout <<"cost computed" << endl;								   
}
    return hCost ;
    
}    
    
int OCPtSpecs::isInsideCorridor(double* x, int dim)
{
    int Nrows = 0 ;
    int Ncols = 0 ;
    bool next = true ;
	int i = 0 ;
	bool error = false ;
    double temp = 0 ;
    int result = -2 ;
    
    while( (next) && (i<m_nPolytopes) )
    { 
	    //cout << "accessing polytope "<<i<< " out of " << m_nPolytopes << endl; 
		Nrows=m_polyCorridor[i]->getNofRows(); 
        Ncols=m_polyCorridor[i]->getNofColumns();
		double** A;
        double* b;
		b = new double[Nrows];
		A = new double*[Nrows];
		for(int j=0;j<Nrows;j++)
            A[j] = new double[Ncols];
        
        m_polyCorridor[i]->getA(&Nrows,&Ncols,A);
        m_polyCorridor[i]->getb(&Nrows,b);
        bool feasible = true;
        if(dim != Ncols)
		{
	//	  cerr << "Wrong dimension specified! Should be: "<< dim << " while it is: " << Ncols  << endl; 
		  error = true;
		  //cout << m_polyCorridor[i] << endl;
		  break;
		}
		
		int j = 0;
		temp = 0;

		while( (feasible) && (j<Nrows))
		{
            for(int h=0;h<dim;h++)
			    temp+=A[j][h]*x[h];
            
			feasible = (temp<=b[j]);
            j++;
			temp = 0;
		}
		if(feasible)
		{
		    next = false;
            delete[] b;
		    for(int j=0;j<Nrows;j++)
			    delete[] A[j];
            delete[] A;
        }
		else
		{
		    i++;
            delete[] b;
		    for(int j=0;j<Nrows;j++)
			    delete[] A[j];
            delete[] A;
	    }
 
    }
	
	if(next)
	   result = -1;
	else
	   result = i;
    if(error)
	   result = -2;

    return result;

}


double OCPtSpecs::polyIntersectionVolume(CPolytope* poly1, CPolytope* poly2)
{
    int Nrows1 = poly1->getNofRows(); 
    int Ncols1 = poly1->getNofColumns();
	int NofDim = Ncols1;
	CAlgebraicGeometry* algGeom = new CAlgebraicGeometry();
	double** A1;
    double* b1;
	b1 = new double[Nrows1];
	A1 = new double*[Nrows1];
	for(int j=0;j<Nrows1;j++)
        A1[j] = new double[Ncols1];
        
    poly1->getA(&Nrows1,&Ncols1,A1);
    poly1->getb(&Nrows1,b1);
        
	int Nrows2=poly2->getNofRows(); 
    int Ncols2=poly2->getNofColumns();
	double** A2;
    double* b2;
	b2 = new double[Nrows2];
	A2 = new double*[Nrows2];
	for(int j=0;j<Nrows2;j++)
        A2[j] = new double[Ncols2];
        
    poly2->getA(&Nrows2,&Ncols2,A2);
    poly2->getb(&Nrows2,b2);

	double** Aint;
    double* bint;
	bint = new double[Nrows1+Nrows2];
	Aint = new double*[Nrows1+Nrows2];
    for(int j=0;j<Nrows1;j++)
	{
	    Aint[j] = A1[j];   //I use the memory which has been already allocated
	    bint[j] = b1[j];
    }
    for(int j=Nrows1;j<(Nrows1+Nrows2);j++)
	{
	    Aint[j] = A2[j-Nrows1];   //I reuse the memory which has been already allocated
	    bint[j] = b2[j-Nrows1];
    }
	int NrowsInt = Nrows1+Nrows2;
	int NcolsInt = Ncols1;
    
    cout << "Data extracted" << endl;

	int NofVtxInt= 4;
	double** verticesInt = new double*[NofDim];
	for(int j=0;j<NofDim;j++)
	    verticesInt[j] = new double[NofVtxInt];
    algGeom->VertexEnumeration(&NrowsInt,&NcolsInt,(const double**)Aint, (const double*)bint, &NofDim, &NofVtxInt, verticesInt);
	for(int j=0;j<NofDim;j++)
        delete[] verticesInt[j];
	delete[] verticesInt;

	verticesInt = new double*[NofDim];
	for(int j=0;j<NofDim;j++)
	    verticesInt[j] = new double[NofVtxInt];
    algGeom->VertexEnumeration(&NrowsInt,&NcolsInt,(const double**)Aint, (const double*)bint, &NofDim, &NofVtxInt, verticesInt);
    cout << "intersection vertices computed" << endl;
    delete[] b1;
    delete[] b2;
	delete[] bint;
    delete[] Aint;         //note the second level pointers point to memory shared with other pointers. I deallocate from those.
	for(int j=0;j<Nrows1;j++)
	    delete[] A1[j];
    delete[] A1;
    for(int j=0;j<Nrows2;j++)
        delete[] A2[j];
    delete[] A2;

    double volumeInt = 0.0;
	CPolytope* polyInt  =  new CPolytope( &NofDim, &NofVtxInt, (const double**)  verticesInt ); //Ask Melvin: is this Correct?
	ALGErrorType algErr = algGeom->FacetEnumeration(polyInt); //vertex reduction
	cout << algErr << endl << endl;
	if(algErr == alg_NoError)
	{        
	    algErr = algGeom->VertexEnumeration(polyInt); //vertex reduction
	    cout << algErr << endl << endl;
	    if(algErr == alg_NoError)
	    {
	        algGeom->FacetEnumeration(polyInt);
            algGeom->ComputeVolume(polyInt);
            volumeInt =  polyInt->getVolume();
	    }
	    else
	    {
	        if( (algErr != alg_EmptyVrepresentation) && (algErr != alg_EmptyHrepresentation))
                volumeInt =  -1.0;
        }
    }
	else
	{   
	    if( ! ((algErr == alg_EmptyVrepresentation) || (algErr == alg_EmptyHrepresentation)) )
            volumeInt =  -1.0;
    }


	for(int j=0;j<NofDim;j++)
	    delete[]  verticesInt[j] ;
	delete[] verticesInt;
	delete polyInt;
	delete algGeom;
	return volumeInt;

}


