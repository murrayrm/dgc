#include "OCPtSpecs.hh"
#include "interfaces/AliceFlat_Definitions.hh"

void OCPtSpecs::getOCPComponentSizes(OCPComponentSizes* const ocpComponentSizes)
{
	//this are fixed for now, will be stored in a class member in the future
    ocpComponentSizes->IsActive             = true;
    ocpComponentSizes->HasICostFunction     = false;
    ocpComponentSizes->HasTCostFunction     = true;
    ocpComponentSizes->HasFCostFunction     = false;
    ocpComponentSizes->NofILinConstraints   = 0;
    ocpComponentSizes->NofTLinConstraints   = 0;
    ocpComponentSizes->NofFLinConstraints   = 0;
    ocpComponentSizes->NofINLinConstraints  = 0;
    ocpComponentSizes->NofTNLinConstraints  = m_Qconstr.Nconstr;
    ocpComponentSizes->NofFNLinConstraints  = 0;
}
/////////////////////////////////////
////////// COST FUNCTION  ///////////
/////////////////////////////////////


void OCPtSpecs::InitialCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Fi, double* const DFi)
{
    return ; 
}

void OCPtSpecs::TrajectoryCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ft, double* const DFt)
{
    if(*mode == 0 || *mode == 2)
    {   
       *Ft = *Ft+( getCostMapValue(z1,z2)*sqrt(pow(z1d,2)+pow(z2d,2))*z3);
    }

    if(*mode == 1 || *mode == 2)
    {
        DFt[I_z1] = DFt[I_z1]+( getGradientMapValue(z1,z2,0) * sqrt( pow(z1d,2)+pow(z2d,2) ) * z3 );
        DFt[I_z2] = DFt[I_z2]+( getGradientMapValue(z1,z2,1) * sqrt(pow(z1d,2)+pow(z2d,2)) * z3 );
        DFt[I_z1d] = DFt[I_z1d]+( getCostMapValue(z1,z2) * ( z1d/sqrt( pow(z1d,2)+pow(z2d,2) ) ) * z3 );
        DFt[I_z2d] = DFt[I_z2d]+( getCostMapValue(z1,z2) * ( z2d/sqrt( pow(z1d,2)+pow(z2d,2) ) ) * z3 );
        DFt[I_z3] = DFt[I_z3]+ ( getCostMapValue(z1,z2) * sqrt(pow(z1d,2)+pow(z2d,2)) );
    }  
}

void OCPtSpecs::FinalCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ff, double* const DFf)
{
    return ;  //not used yet
}


///////////////////////////////////////////
//////////  CONSTRAINTS   /////////////////
///////////////////////////////////////////


void OCPtSpecs::getLinearConstraints(const ConstraintType* const ctype, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A, const int* const Offset)
{
    return ;  //not used yet -> maybe we should use it for final constraints
}

void OCPtSpecs::InitialNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli, const int* const Offset) 
{
    return ;  //not used yet
}

void OCPtSpecs::TrajectoryNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt, const int* const Offset)
{
    double tempX[2]; //temporary position vector
    tempX[0]=z1;
    tempX[1]=z2;
    for(m_i=0;m_i<*NofConstraints; m_i++)
    {
        for(m_j=0;m_j<*NofVariables; m_j++)
        {
            DCnlt[m_i+*Offset][m_j]=0;  //initialize the jacobian to 0
        }
        Cnlt[*Offset+m_i]=0;  //this should not be necessary, because the value of each constraint is computed.
    }

     //Value
    if(*mode == 0 | *mode == 2)
    {
        for(m_i=0;m_i<*NofConstraints;m_i++)
        {
  	        Cnlt[*Offset+m_i] = tempX[0]*m_Qconstr.H[m_i][0][0]*tempX[0]+ 2 * tempX[0]*m_Qconstr.H[m_i][0][1]*tempX[1] + 
	                          tempX[1]*m_Qconstr.H[m_i][1][1]*tempX[1] + m_Qconstr.S[m_i][0]*tempX[0] + m_Qconstr.S[m_i][1]*tempX[1];
        }
    } 
    
    //Jacobian
    if(*mode == 1 | *mode == 2)
    {
        for(m_i=0;m_i<*NofConstraints;m_i++)
        {
            DCnlt[m_i+*Offset][I_z1]=m_Qconstr.S[m_i][0]+2*(tempX[0]*m_Qconstr.H[m_i][0][0]+tempX[1]*m_Qconstr.H[m_i][1][0]) ;
	        DCnlt[m_i+*Offset][I_z2]=m_Qconstr.S[m_i][1]+2*(tempX[0]*m_Qconstr.H[m_i][0][1]+tempX[1]*m_Qconstr.H[m_i][1][1]) ;
        }
    }
   
   //compute the Sparsity
    if(*mode == 99)
    {   
        for(m_i=0;m_i<*NofConstraints;m_i++)
        {   
            DCnlt[m_i+*Offset][I_z1]= 1;
            DCnlt[m_i+*Offset][I_z2]= 1;  
        }
    }
   
    return ;
}

/*! use to implement the nonlinear constraints applicable at the end of the time interval.*/
void OCPtSpecs::FinalNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf, const int* const Offset)
{
    return ;  //not used yet
}

/*! Use to implement the bounds for all the constraints: linear and nonlinear.*/
void OCPtSpecs::getLUBounds(const ConstraintType* const ctype, const BoundsType* const ubtype, const int* const i, const double* const t, const int* const NofConstraints, double* const LUBounds, const int* const Offset)
{
    if(*ctype == ct_NLinTrajectory)
	{  
	    double temp =0;
	    	//we assume quadratic constraints in the form xQx+Sx+K >= 1
	        for(m_j=0; m_j<(*NofConstraints); m_j++)
	        {
	            if( *ubtype == ubt_LowerBound )   //lower bound is 1-K
		            temp = 1 - m_Qconstr.K[m_j];    		
		        else
		        temp = DBL_MAX;                  //upper bound is \infty
	            LUBounds[*Offset+(*i)*(*NofConstraints)+m_j]=temp;
	        } 
	}
    return ;
}

////////////////////////////////
//////    WAYPOINTS    /////////
////////////////////////////////

int OCPtSpecs::getNofActiveWaypoints()
{
	double initPt[2];
	double finalPt[2];
    initPt[0] = m_OCPparams.initialConditionLB[NORTHING_IDX_C];
    initPt[1] = m_OCPparams.initialConditionLB[EASTING_IDX_C];
    finalPt[1] = m_OCPparams.finalConditionLB[EASTING_IDX_C];
    finalPt[0] = m_OCPparams.finalConditionLB[NORTHING_IDX_C];
    
    int initPolytope   =  isInsideCorridor(initPt,2);    
    if(initPolytope<0)
        return -1; //error in the initial point
    int finalPolytope  =  isInsideCorridor(finalPt,2);
    if(finalPolytope<0)
        return -2; //error in the final point
    
    int nActWP = finalPolytope-initPolytope;
    if(nActWP<0)
        return -3; //error in the initial final condition ordering 
    else
        return (nActWP+2); //summing initial and final condition;

}

int OCPtSpecs::getActiveWaypoints(double** const waypoints)
{
	double initPt[2];
	double finalPt[2];
    initPt[0] = m_OCPparams.initialConditionLB[NORTHING_IDX_C];
    initPt[1] = m_OCPparams.initialConditionLB[EASTING_IDX_C];
    finalPt[1] = m_OCPparams.finalConditionLB[EASTING_IDX_C];
    finalPt[0] = m_OCPparams.finalConditionLB[NORTHING_IDX_C];
    
    int initPolytope   =  isInsideCorridor(initPt,2);    
    if(initPolytope<0)
        return -1; //error in the initial point
    int finalPolytope  =  isInsideCorridor(finalPt,2);
    if(finalPolytope<0)
        return -2; //error in the final point
    
    if( (finalPolytope-initPolytope) <0)
        return -3; //error in the initial final condition ordering 
    
    waypoints[0][0]=initPt[0];
    waypoints[1][0]=initPt[1];
    for(int i = initPolytope; i<finalPolytope;i++)
    {
        waypoints[0][i - initPolytope + 1] = m_waypoints[0][i];
        waypoints[0][i - initPolytope + 1] = m_waypoints[0][i];
    }
    waypoints[0][finalPolytope - initPolytope + 1]=finalPt[0];
    waypoints[1][finalPolytope - initPolytope + 1]=finalPt[1];
	
	return 1;
}


/*
void OCPtSpecs::getSurfaceControlPoints(int windowSize,TimeInterval* xInterval, TimeInterval* yInterval, double* dx, double* dy, double** controlPoints)
{
    double xi = m_OCPparams.initialConditionLB[EASTING_IDX_C];
    double xf = m_OCPparams.finalConditionLB[EASTING_IDX_C];
    double yi = m_OCPparams.initialConditionLB[NORTHING_IDX_C];
    double yf = m_OCPparams.finalConditionLB[NORTHING_IDX_C];
    
    if(xi<xf)
    {
      xInterval->t0 = xi - MAP_EXPANSION;
      xInterval->tf = xf + MAP_EXPANSION;
    }
    else
    {
      xInterval->t0 = xf - MAP_EXPANSION;
      xInterval->tf = xi + MAP_EXPANSION;   	
    }
    if(yi<yf)
    {
      yInterval->t0 = yi - MAP_EXPANSION;
      yInterval->tf = yf + MAP_EXPANSION;
    }
    else
    {
      yInterval->t0 = yf - MAP_EXPANSION;
      yInterval->tf = yi + MAP_EXPANSION;   	
    }
    
    *dy = (yInterval->tf-yInterval->t0)/windowSize;
    *dx = (xInterval->tf-xInterval->t0)/windowSize;
    
    
    for(int i = 0; i<windowSize; i++)
    {
        for(int j = 0; j<windowSize; j++)
        {
            controlPoints[i][j]=getCostMapValue(xInterval->t0+(*dx)*i,yInterval->t0+(*dy)*i);	
        }
    }  
    
    
}
*/

///////////////////////////////
//// POLYTOPIC CORRIDOR  //////
///////////////////////////////    

int OCPtSpecs::getNpolytopes()
{
    return m_nPolytopes;
}

void OCPtSpecs::getPolytopes(CPolytope* const polytopes)
{
    for(m_i=0;m_i<m_nPolytopes;m_i++)
        polytopes[m_i] = m_polyCorridor[m_i];

}


int OCPtSpecs::getNofActivePolytopes()
{
    double initPt[2];
	double finalPt[2];
    initPt[0] = m_OCPparams.initialConditionLB[NORTHING_IDX_C];
    initPt[1] = m_OCPparams.initialConditionLB[EASTING_IDX_C];
    finalPt[1] = m_OCPparams.finalConditionLB[EASTING_IDX_C];
    finalPt[0] = m_OCPparams.finalConditionLB[NORTHING_IDX_C];
    
    int initPolytope   =  isInsideCorridor(initPt,2);    
    if(initPolytope<0)
        return -1; //error in the initial point
    int finalPolytope  =  isInsideCorridor(finalPt,2);
    if(finalPolytope<0)
        return -2; //error in the final point
    
    int nActPoly = finalPolytope-initPolytope;
    if(nActPoly<0)
        return -3; //error in the initial final condition ordering 
    else
        return nActPoly+1; //summing initial and final condition;
}

int OCPtSpecs::getActivePolytopes(CPolytope* const polytopes)
{
    double initPt[2];
	double finalPt[2];
    initPt[0] = m_OCPparams.initialConditionLB[NORTHING_IDX_C];
    initPt[1] = m_OCPparams.initialConditionLB[EASTING_IDX_C];
    finalPt[1] = m_OCPparams.finalConditionLB[EASTING_IDX_C];
    finalPt[0] = m_OCPparams.finalConditionLB[NORTHING_IDX_C];
    
    int initPolytope   =  isInsideCorridor(initPt,2);    
    if(initPolytope<0)
        return -1; //error in the initial point
    int finalPolytope  =  isInsideCorridor(finalPt,2);
    if(finalPolytope<0)
        return -2; //error in the final point
    

    if((finalPolytope-initPolytope)<0)
        return -3; //error in the initial final condition ordering 
    else
    {
        for(int i=initPolytope; i<=finalPolytope; i++)
            polytopes[i-initPolytope] = m_polyCorridor[i];
    
    return 1;
	}
}


void OCPtSpecs::getRDDFcorridor(RDDF* pNewRDDF)
{
    *pNewRDDF =  m_RDDF;
    return ;
}


/////////////////////////////////////////
////////     COSTMAP        /////////////
/////////////////////////////////////////


double OCPtSpecs::getCostMapValue(double x, double y)
{
   double res=-10;
   point2 p = point2(x,y);
   res = m_costMap.getPixelLoc(p);
   return (double)res;
}


double OCPtSpecs::getGradientMapValue(double x, double y, int component)
{
   double res=0;
   point2 p = point2(x,y);
   if(component == 0)
   {
       res = m_costDx.getPixelLoc(p);
   }
   if(component == 1)
   {
       res = m_costDy.getPixelLoc(p);
   }
   
   return (double)res;
}

point2 OCPtSpecs::getCostMapGradient(double x, double y)
{
   point2 upperLeft =  m_costMap.getUpperLeft();
   point2 lowerRight = m_costMap.getLowerRight();
   
   float dx = (lowerRight.x-upperLeft.x)/m_costMap.getWidth() ;
   float dy = (lowerRight.y-upperLeft.y)/m_costMap.getHeight() ;
   float derX = 0.0 ;
   float derY = 0.0 ;
   if(GRAD_SMOOTHING  < 1)
   {
       derX += ( (getCostMapValue(x+dx,y)-getCostMapValue(x,y))/dx );
       derY += ( (getCostMapValue(x,y+dy)-getCostMapValue(x,y))/dy );   
   }
   else
   {  

       for(int i=1;i<=GRAD_SMOOTHING ;i++) //smooth the gradient by loking at the numerical gradients with increments +/- 
       {   
           derX +=   ( (getCostMapValue(x+i*dx,y)-getCostMapValue(x,y))/(i*dx)) - ( (getCostMapValue(x-i*dx,y)-getCostMapValue(x,y))/(i*dx) );
           derY +=   ( (getCostMapValue(x,y+i*dy)-getCostMapValue(x,y))/(i*dy)) - ( (getCostMapValue(x,y-i*dy)-getCostMapValue(x,y))/(i*dy) );  
       }
       derX = derX/(2*GRAD_SMOOTHING);
       derY = derY/(2*GRAD_SMOOTHING);      
   }
   point2 res = point2(derX,derY);
   return res;
}

void OCPtSpecs::getGradientMap()
{
    IplImage* p_img=m_costMap.getImage();
    if( (m_costMap.getWidth() != m_costDx.getWidth() ) || (m_costMap.getHeight() != m_costDx.getHeight() ) )
    {
        m_costDx = m_costMap;
        m_costDy = m_costMap;

    }
    cvSobel(p_img, m_costDx.getImage(), 1, 0, 5 );
    cvSobel(p_img, m_costDy.getImage(), 0, 1, 5 );
    
    if(m_showGradientmap)
    {
	    IplImage* img = cvCloneImage(m_costDx.getImage());
        double minVal, maxVal;
        cvMinMaxLoc(img, &minVal, &maxVal);
        cvSubS(img, cvRealScalar(minVal), img);
        if (maxVal != minVal)
            cvScale(img, img, 1/(maxVal - minVal));
        cvNamedWindow("Dx",CV_WINDOW_AUTOSIZE);
        cvShowImage("Dx",img);
        cvReleaseImage(&img);
    
	    img = cvCloneImage(m_costDy.getImage());
        cvMinMaxLoc(img, &minVal, &maxVal);
        cvSubS(img, cvRealScalar(minVal), img);
        if (maxVal != minVal)
            cvScale(img, img, 1/(maxVal - minVal));
        cvNamedWindow("Dy",CV_WINDOW_AUTOSIZE);
        cvShowImage("Dy",img);
        cvReleaseImage(&img);
    }
}

