
#include "OCPtSpecs.hh"
#define MAX_DELTA_SIZE 1000000  //taken from plannerModule
#define center_N 0.0           //taken from map.default.dat
#define center_E 0.0



OCPtSpecs::OCPtSpecs(int sn_key)
    : CSkynetContainer(MODdynamicplanner, sn_key),
      m_scMapRecvOneDelta(false),
      CStateClient(true)
{
	 
//	readspecs();      //not sure if I need this

	m_pScmapDelta = new char[MAX_DELTA_SIZE];   //this is the buffer for the deltas
	m_scmapRequestSocket = m_skynet.get_send_sock(SNtplannerStaticCostMapRequest);  //socket to request the fullmap-> CHECK
    
        m_pStaticCostMap = new CMapPlus();
	m_pStaticCostMap->initMap(center_N, center_E, SCM_NROWS, SCM_NCOLS, SCM_RESROWS, SCM_RESCOLS, VERBOSITY); // initialize the map qith appropriate size
//	m_pStaticCostMap->initMap(1,1,1,1,1,1,1); // initialize the map qith appropriate size
	m_scmapLayer = m_pStaticCostMap->addLayer<double>(SCM_NODATA,SCM_OUTSIDEMAP,SCM_USEDELTA); //add the staic cost layer

	DGCcreateMutex(&m_scmapMutex);                  //mutex for the static cost map
	DGCcreateMutex(&m_scmapDeltaRecvMutex);     //mutex for the deltas
	DGCcreateCondition(&m_scmapDeltaRecvCond);  //condition on deltas reception
	
        m_scmapRequestSocket = m_skynet.get_send_sock(SNtplannerStaticCostMapRequest);  //put skynet interface here
	m_scmapDeltaSocket =  m_skynet.listen(SNtplannerStaticCostMap,MODtrafficplanner); //put skynet interface here   
	
	DGCstartMemberFunctionThread(this, &OCPtSpecs::getScmapDeltasThread); //start the thread to catch the map deltas 
	scmapRequest(); // send a request for the full map
    
}

void OCPtSpecs::scmapRequest()
{
	bool bRequestScmap = true;
	m_skynet.send_msg(m_scmapRequestSocket,&bRequestScmap, sizeof(bool) , 0);
}

void OCPtSpecs::getScmapDeltasThread()
{
  // The skynet socket for receiving map deltas (from fusionmapper)
//	int scMapDeltaSocket = m_skynet.listen( <Put here the communication names>,  <Put here the communication names>);
//	if(scMapDeltaSocket < 0)
//		cerr << "OCPtSpecs::getMapDeltasThread(): skynet listen returned error" << endl;

	while(true)
	{
		int deltasize;
		
		cout << "Waiting for SCM Deltas " << endl;

		// I don't think the RecvMapdelta function works like it should --JM
		//RecvMapdelta(m_scmapDeltaSocket, m_pScmapDelta, &deltasize);  

		WaitForNewState();

		DGClockMutex(&m_scmapMutex);
		//LOCK MUTEX!!!

		m_pStaticCostMap->updateVehicleLoc(m_state.localX, m_state.localY);

		deltasize = m_skynet.get_msg(m_scmapDeltaSocket, m_pScmapDelta, MAX_DELTA_SIZE, 0);
		if(deltasize>0)
		  {
	
		    m_pStaticCostMap->applyDelta<double>(m_scmapLayer, m_pScmapDelta, deltasize);	
		  }
	
		if(!m_scMapRecvOneDelta)
		{
			DGCSetConditionTrue(m_scMapRecvOneDelta, m_scmapDeltaRecvCond, m_scmapDeltaRecvMutex);
		}

		//UNLOCK MUTEX!!!
		DGCunlockMutex(&m_scmapMutex);

		cout << "Receive Delta Thread is running"  <<endl;
				
		//sleep(1);
	}

}



OCPtSpecs::~OCPtSpecs(void)
{
//not doing anything for now
}


/* ! Use to specify all the dimensions and flags related to the coded OC Problem. */
void OCPtSpecs::getOCPComponentSizes(OCPComponentSizes* const ocpComponentSizes)
{
	//this are fixed for now, will be stored in a class member in the future
	ocpComponentSizes->IsActive         = true;
    ocpComponentSizes->HasICostFunction     = false;
    ocpComponentSizes->HasTCostFunction     = true;
    ocpComponentSizes->HasFCostFunction     = false;
    ocpComponentSizes->NofILinConstraints   = 0;
    ocpComponentSizes->NofTLinConstraints   = 0;
    ocpComponentSizes->NofFLinConstraints   = 0;
    ocpComponentSizes->NofINLinConstraints  = 0;
    ocpComponentSizes->NofTNLinConstraints  = 0;
    ocpComponentSizes->NofFNLinConstraints  = 0;
}


/*! Use to implement the cost function applicable for the begining of the time interval.*/
void OCPtSpecs::InitialCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Fi, double* const DFi)
{
    return ; // no initial cost function for the moment
}

/*! Use to implement the cost function for the whole time interval.*/
void OCPtSpecs::TrajectoryCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ft, double* const DFt)
{
    if(*mode == 0 || *mode == 2)
    {   
       *Ft = m_pStaticCostMap->getDataUTM<double>(m_scmapLayer, z[3], z[0]);
    }

    if(*mode == 1 || *mode == 2)
    {
        for(m_i=0;m_i<NZ;m_i++)  //initialize the df/dz-vector to 0 
        {
    	    DFt[m_i] = 0;
        }
     double grad[2];
     m_pStaticCostMap->getUTMGradient(m_scmapLayer, z[3], z[0], grad);
     DFt[0]=grad[0];
     DFt[3]=grad[1];
     }  
       
//       if(*mode == 99)  not implemented yet
//       {
//          // sparsity
//       }
}

/*! Use to implement the cost function applicable at the end of the time interval.*/
void OCPtSpecs::FinalCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ff, double* const DFf)
{
    return ;  //not used yet
}

/*! Use to implement the linear constraints - those applicable at the beginning of the time interval, for the whole time interval and for those at the end of the time interval.*/
void OCPtSpecs::getLinearConstraints(const ConstraintType* const ctype, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A, const int* const Offset)
{
//	if(*ctype == ct_LinInitial)
//	{
//        for(m_i=0; m_i<NofLConstraints; m_i++)
//        {
//            for(m_j=0; m_j<NofVariables; m_j++)
//            {
//                A[m_i][m_j] = 0.0;
//            }
//        }
//        
// 	}	
//	else if(*ctype == ct_LinTrajectory)
//	{
//          
//	}
//	else /*if(*ctype == ct_LinFinal)*/
//	{
//         
//	}	

    return ;  //not used yet

}

/*! Use to imOCPtSpecs::plement the nonlinear constraints applicable at the beginning of the time interval.*/
 void OCPtSpecs::InitialNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli, const int* const Offset) 
{
    return ;  //not used yet
}

/*! Use to implement the nonlinear constraints applicable during the whole time interval.*/
void OCPtSpecs::TrajectoryNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt, const int* const Offset)
{
    return ;  //not used yet
}

/*! Use to implement the nonlinear constraints applicable at the end of the time interval.*/
void OCPtSpecs::FinalNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf, const int* const Offset)
{
    return ;  //not used yet
}

/*! Use to implement the bounds for all the constraints: linear and nonlinear.*/
void OCPtSpecs::getLUBounds(const ConstraintType* const ctype, const BoundsType* const ubtype, const int* const NofConstraintCollocPoints, const int* const NofConstraints, double* const LUBounds, const int* const Offset)
{
//	if(*ctype == ct_LinInitial)
//	{
//        
//        if(*ubtype == ubt_LowerBound)
//        {
//            for(m_i=0; m_i<m_NofConstraints; m_i++)
//            {
//                LUBounds[m_i] = 0;
//            }                
//        }
//        else
//        {
//            for(m_i=0; m_i<m_NofILinConstraints; m_i++)
//            {
//                LUBounds[m_i] = 0;
//            }                
//        }
//        
//	}
//
//	if(*ctype == ct_LinTrajectory)
//	{
//          
//
//	}
//
//	if(*ctype == ct_LinFinal)
//	{
//
//	}
//
//	if(*ctype == ct_NLinInitial)
//	{
//
//	}
//
//	if(*ctype == ct_NLinTrajectory)
//	{   
//            
//	}
//
//	if(*ctype == ct_NLinFinal)
//	{
//       
//	}   
return ;  //not used yet
}
