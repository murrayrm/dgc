#include <highgui.h>
#include <dgcutils/AutoMutex.hh>
#include "CostMapEstimator.hh"
#include <boost/serialization/vector.hpp>

bool CostMapEstimator::updateMap(CostMap* map)
{
  if (!polyTalker.hasNewMessage()) {
    return false;
  }

  BitmapParams bmparams;

  while (polyTalker.hasNewMessage()) {
    polyTalker.receive(&bmparams);
  }
  if (m_debugLevel > 1) {
    cout << "center = (" << bmparams.centerX << ", " << bmparams.centerY 
	 << ")" << endl;
    cout << "res = (" << bmparams.resX << ", " << bmparams.resY 
	 << ")" << endl;
    cout << "size = (" << bmparams.width << ", " << bmparams.height 
	 << ")" << endl;
    cout << "baseVal = " << bmparams.baseVal << endl;
  }
  map->init(bmparams.width, bmparams.height, bmparams.resX, bmparams.resY, 
	    point2(bmparams.centerX, bmparams.centerY));
  map->clear(bmparams.baseVal);

  for (unsigned i=0; i<bmparams.polygons.size(); i++) {
    bmparams.polygons[i].draw(map);
  }

  if (m_guiEnabled) {
    IplImage* img = cvCloneImage(map->getImage());
    double minVal, maxVal;
    cvMinMaxLoc(img, &minVal, &maxVal);
    cvSubS(img, cvRealScalar(minVal), img);
    if (maxVal != minVal)
      cvScale(img, img, 1/(maxVal - minVal));

    cvNamedWindow( "dplanner cost map", 1 );
    cvShowImage( "dplanner cost map", img);
    cvReleaseImage(&img);
    cvWaitKey(1000);
  }

  return true;

}

CostMap CostMapEstimator::getLatestMap()
{
  AutoMutex lock(m_cmapMutex);
  return m_cmap;
}

void CostMapEstimator::getMapThread()
{
  while (true) {
    DGClockMutex(&m_cmapMutex);
    if (updateMap(&m_cmap) && m_debugLevel > 0)
      cout << "Received map" << endl;
    DGCunlockMutex(&m_cmapMutex);
    usleep(m_usleepTime);
  }
}
