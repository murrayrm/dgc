/*
	OTG_InterfacesX.hh
	Melvin E. Flores
	Northrop Grumman Space Technology
	Proprietary Level 1
	Redondo Beach, California
	Last Updated: Jan 11, 2006.
*/

#ifndef OTG_INTERFACESX_HH
#define OTG_INTERFACESX_HH

/*! -  This enum is used to encapsulate the valid flags that specify the types of cost functions available.*/
enum CostType{ct_Initial, ct_Trajectory, ct_Final};

/*! -  This enum is used to encapsulate the valid flags that specify the system mode.*/
enum Mode{md_FWD, md_REV};


/*! -  This enum is used to encapsulate the valid flags that specify the types of constraint functions available.*/
enum ConstraintType{ct_LinInitial, ct_LinTrajectory, ct_LinFinal, ct_NLinInitial, ct_NLinTrajectory, ct_NLinFinal};

/*! -  This enum is used to encapsulate the valid flags that specify the types of bounds available.*/
enum BoundsType{ubt_LowerBound, ubt_UpperBound};

/*! -  This enum is used to encapsulate the valid flags that specify the types of functions available.*/
enum FunctionType{ft_Cost, ft_Constraint};

/*! This structure is used to specify a time interval. */
struct TimeInterval
{
	double t0;
	double tf;
};

/*! - This structure is used to represent the dimensions of the various functions specified in the OCPComponent. */
struct OCPComponentSizes
{
    bool IsActive;
    bool HasICostFunction;
    bool HasTCostFunction;
    bool HasFCostFunction;
    int  NofILinConstraints;
    int  NofTLinConstraints;
    int  NofFLinConstraints;
    int  NofINLinConstraints;
    int  NofTNLinConstraints;
    int  NofFNLinConstraints;
};


/*! - In order to plug and play with the optimal control problem, the user is required to implement the IOCPComponent interface. */
class IOCPComponent
{
    public:
        /* ! Use to specify all the dimensions and flags related to the coded OC Problem. */
        virtual void getOCPComponentSizes(OCPComponentSizes* const ocpComponentSizes) = 0;
        
        // IOCPComponent();
//	virtual ~IOCPComponent();

        /*! Use to implement the cost function applicable for the begining of the time interval.*/
        virtual void InitialCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Fi, double* const DFi) = 0;
        
        /*! Use to implement the cost function for the whole time interval.*/
        virtual void TrajectoryCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ft, double* const DFt) = 0;

       /*! Use to implement the cost function applicable at the end of the time interval.*/
        virtual void FinalCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ff, double* const DFf) = 0;

		/*! Use to implement the linear constraints - those applicable at the beginning of the time interval, for the whole time interval and for those at the end of the time interval.*/
        virtual void getLinearConstraints(const ConstraintType* const ctype, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A, const int* const Offset) = 0;

		/*! Use to implement the nonlinear constraints applicable at the beginning of the time interval.*/
        virtual void InitialNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli, const int* const Offset) = 0;
        
        /*! Use to implement the nonlinear constraints applicable during the whole time interval.*/
        virtual void TrajectoryNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt, const int* const Offset) = 0;

		/*! Use to implement the nonlinear constraints applicable at the end of the time interval.*/
        virtual void FinalNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf, const int* const Offset) = 0;

		/*! Use to implement the bounds for all the constraints: linear and nonlinear.*/
        virtual void getLUBounds(const ConstraintType* const ctype, const BoundsType* const ubtype, const int* const NofConstraintCollocPoints, const int* const NofConstraints, double* const LUBounds, const int* const Offset) = 0;
        virtual void getInitialCondition(const BoundsType* const ubtype, double* const initCond) = 0;
        virtual void getFinalCondition(const BoundsType* const ubtype, double* const finalCond) = 0;
        virtual void getParams(double* const params) = 0;
	virtual void getMode(int* mode) = 0; 
};


#endif /* OTG_INTERFACESX_HH */

