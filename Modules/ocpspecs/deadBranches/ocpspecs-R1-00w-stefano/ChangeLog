Sun Apr 22 10:54:08 2007	Stefano Di Cairano (stefano)

	* version R1-00w-stefano
	BUGS:  
	FILES: OCPtSpecs.cc(20234), OCPtSpecs.hh(20234)
	Minor changes

Tue Apr 17 15:11:18 2007	Stefano Di Cairano (stefano)

	* version R1-00w
	BUGS:  
	FILES: OCPtSpecs.cc(19846), OCPtSpecs.hh(19846)
	Changed to deal with the Flat model of Alice

Tue Apr 10 16:47:47 2007	murray (murray)

	* version R1-00v
	BUGS: 
	New files: test.rddf testParams.ggo
	FILES: Makefile.yam(19259), testParams.cc(19259)
	updated testParams to use gengetopt and send initial/final point
	from rddf

	FILES: testParams.cc(19260)
	added more verbose output

	FILES: testParams.cc(19278)
	added functionality for sending out RDDF (in addition to params)

Fri Mar 23 17:44:55 2007	Stefano Di Cairano (stefano)

	* version R1-00u
	BUGS: 
	FILES: OCPtSpecs.cc(18885), OCPtSpecs.hh(18885),
		testOCPspecs.cc(18885)
	removed unused sockets

Fri Mar 16 12:10:50 2007	Stefano Di Cairano (stefano)

	* version R1-00t
	BUGS: 
	FILES: OCPtSpecs.cc(18005), OCPtSpecs.hh(18005),
		testOCPspecs.cc(18005)
	Changed the constructor to enable/disable threads. By default all
	the threads are enabled.

	FILES: OCPtSpecs.cc(18006), testOCPspecs.cc(18006)
	Added outputs to check the threads are not running.

Fri Mar 16 10:12:31 2007	murray (murray)

	* version R1-00s
	BUGS: 
	FILES: OCPtSpecs.cc(17957)
	verbose flag + reset RDDF before receiving

	FILES: OCPtSpecs.hh(17956)
	added verbose flag

2007-03-16  murray  <murray@gclab.dgc.caltech.edu>

	* OCPtSpecs.cc (getRDDFThread): cleared out tempRDDF before using,
	so that trajectory gets properly stored.  Turns out that RecvRDDF
	will not overwrite existing points in the RDDF that it is passed.

	* OCPtSpecs.cc (getRDDFThread): added debug prints (m_verbose > 1
	=> print program flow, m_verbose > 2 => print data objects)

	* OCPtSpecs.hh (class OCPtSpecs): Added m_verbose to control error
	messages.  Initialized to zero in constructor.

Thu Mar 15 21:31:58 2007	Stefano Di Cairano (stefano)

	* version R1-00r
	BUGS: 
	FILES: OCPtSpecs.cc(17873)
	Changed getRDDFcorridor function

	FILES: OCPtSpecs.cc(17876)
	Fixed small bug into getInitial function

	FILES: OCPtSpecs.cc(17881)
	 Another small bug fixed in get final condition

Thu Mar 15 13:32:30 2007	Stefano Di Cairano (stefano)

	* version R1-00q
	BUGS: 
	FILES: Makefile.yam(17782), OCPtSpecs.cc(17782),
		OCPtSpecs.hh(17782), testOCPspecs.cc(17782),
		testObstacles.cc(17782)
	Conic form of nonlinear constraint tested. Changed implementation
	of the solver mode (reduced n_of_ops. Interface with tplanner moved
	to interfaces module

	FILES: OCPtSpecs.cc(17749), OCPtSpecs.hh(17749),
		testOCPspecs.cc(17749), testObstacles.cc(17749)
	Computation of the Quadratic constrainits is now performed in Conic
	Form. This reduces the number of operations required by the solver

Wed Mar 14 19:41:33 2007	Stefano Di Cairano (stefano)

	* version R1-00p
	BUGS: 
	FILES: OCPtSpecs.cc(17683), testOCPspecs.cc(17683),
		testObstacles.cc(17683)
	Modified handling of the nonllinear constraints to comply with
	SNOPT specs (no constant terms in the constraints function

Wed Mar 14 17:46:00 2007	Stefano Di Cairano (stefano)

	* version R1-00o
	BUGS: 
	FILES: OCPtSpecs.cc(17648), testOCPspecs.cc(17648),
		testObstacles.cc(17648)
	Constraint evaluation and Jacobians tested wrt spoof-data. Few
	errors corrected. Results checked analitically.

Tue Mar 13 19:14:32 2007	Stefano Di Cairano (stefano)

	* version R1-00n
	BUGS: 
	FILES: OCPtSpecs.cc(17515), OCPtSpecs.hh(17515)
	Modified RDDF receiver to avoid generating a dummy thread

Tue Mar 13 17:20:38 2007	Stefano Di Cairano (stefano)

	* version R1-00m
	BUGS: 
	FILES: OCPtSpecs.cc(17503)
	Tested wrt test_RDDFTalkerRecv 

Tue Mar 13 14:16:15 2007	Stefano Di Cairano (stefano)

	* version R1-00l
	BUGS: 
	FILES: OCPtSpecs.cc(17444), OCPtSpecs.hh(17444),
		OTG_AliceInterface.hh(17444)
	moved RDDF talker here. Ready for integration with dplanner

Mon Mar 12 19:20:39 2007	Stefano Di Cairano (stefano)

	* version R1-00k
	BUGS: 
	New files: OTG_AliceInterface.hh testObstacles.cc
	FILES: Makefile.yam(17277), OCPtSpecs.cc(17277),
		OCPtSpecs.hh(17277)
	Added obstacles computation and communication using skynettalker.
	Added virtual interface for parameters and other Alice-related
	functions. Added threads synchronization.

	FILES: Makefile.yam(17282), OCPtSpecs.cc(17282),
		OCPtSpecs.hh(17282)
	Test of the obstacles reception succeeded. Few canges in the names.

Sun Mar 11 20:13:35 2007	murray (murray)

	* version R1-00j
	BUGS: 
	Deleted files: OTG_InterfacesX.hh
	FILES: Makefile.yam(17169), OCPtSpecs.hh(17169)
	uses OTG interfaces

Sun Mar 11 19:34:31 2007	Stefano Di Cairano (stefano)

	* version R1-00i
	BUGS: 
	New files: testParams.cc
	Deleted files: OCPparamsTalker.cc OCPparamsTalker.hh
	FILES: Makefile.yam(17156), OCPtSpecs.cc(17156),
		OCPtSpecs.hh(17156), OTG_InterfacesX.hh(17156),
		testOCPspecs.cc(17156)
	Removed OCPparamsTalker, using SkynetTalker now. Added test program
	to check Params receive. Locking consistency corrected. Mode
	definition moved to OCPtSpecs.hh 

Sat Mar 10 20:13:16 2007	Stefano Di Cairano (stefano)

	* version R1-00h
	BUGS: 
	FILES: OCPparamsTalker.cc(17051), OCPparamsTalker.hh(17051),
		OCPtSpecs.cc(17051), OCPtSpecs.hh(17051),
		OTG_InterfacesX.hh(17051)
	Added accessors for OCP parameters, initial condition and final
	condition.

	FILES: OCPtSpecs.cc(17008), OCPtSpecs.hh(17008)
	introducing static obstacles in OCP. Interface is ok.

Thu Mar  8 15:47:04 2007	Stefano Di Cairano (stefano)

	* version R1-00g
	BUGS: 
	New files: OCPparamsTalker.cc OCPparamsTalker.hh
	FILES: Makefile.yam(16795), OCPtSpecs.cc(16795),
		OCPtSpecs.hh(16795)
	Added interface to receive OCP parameters, including initial and
	final conditions. Added a Talker to send/receive OCP parameters,
	which can be possibly moved to interfaces in the future.

Sat Mar  3 16:21:30 2007	Jeremy Ma (jerma)

	* version R1-00f
	BUGS: 
	FILES: Makefile.yam(16404), OCPtSpecs.cc(16404),
		OCPtSpecs.hh(16404), testOCPspecs.cc(16404)
	modified OCPtSpecs to inherit StateClient since the maintained map
	needs to be updated to the vehicle location each time through the
	loop; this required modified changes to the makefile and
	testOCPspecs.cc 

	FILES: testOCPspecs.cc(16405)
	modified the testOCPspecs.cc file so that it doesn't use a
	hard-coded SNkye but rather asks you for it. minor change.

Sat Mar  3 13:41:19 2007	Jeremy Ma (jerma)

	* version R1-00e
	BUGS: 
	FILES: OCPtSpecs.cc(16341), OCPtSpecs.hh(16341)
	now that we can send deltas through the CMap class, there is no
	longer any need for the CostMapMsg.h interface; these changes
	reflect that

Fri Mar  2 17:47:09 2007	Jeremy Ma (jerma)

	* version R1-00d
	BUGS: 
	FILES: OCPtSpecs.cc(16170), OCPtSpecs.hh(16170)
	modified ocpspecs to handle sending deltas since cmap send deltas
	function didn't seem to be working

Thu Mar  1 19:37:40 2007	Melvin Flores (mflores)

	* version R1-00c
	BUGS: 
	FILES: Makefile.yam(16066), OCPtSpecs.hh(16066)
	set up to use proprietary NGC code if OTG_HOME is defined

Wed Feb 28 13:42:50 2007	Stefano Di Cairano (stefano)

	* version R1-00b
	BUGS: 
	FILES: Makefile.yam(15963), OCPtSpecs.cc(15963),
		OCPtSpecs.hh(15963), OTG_InterfacesX.hh(15963),
		testOCPspecs.cc(15963)
	Debugging ocpspec module. Tested on a simple test program. The
	objects is instanciated and the threads are started corrretly.
	Nobody is sending delta for now.

Tue Feb 27 23:37:29 2007	Stefano Di Cairano (stefano)

	* version R1-00a
	BUGS: 
	New files: OCPtSpecs.cc OCPtSpecs.hh OTG_InterfacesX.hh
		testOCPspecs.cc
	FILES: Makefile.yam(15933)
	initial release of OCPSPECS module. It compiles, but it has not
	been tested yet

Tue Feb 27 22:03:09 2007	Stefano Di Cairano (stefano)

	* version R1-00
	Created ocpspecs module.

























