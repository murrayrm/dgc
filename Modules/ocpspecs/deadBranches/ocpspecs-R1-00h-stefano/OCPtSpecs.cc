
/*
 Definition of the OCPTSPECS class that provides the interface
 between Tplanner and Dplanner

 Stefano Di Cairano mar-07
 */

#include "OCPtSpecs.hh"
#define MAX_DELTA_SIZE 1000000  //taken from plannerModule
#define center_N 0.0           //taken from map.default.dat
#define center_E 0.0


OCPtSpecs::OCPtSpecs(int sn_key, bool useAstate)
    : CSkynetContainer(MODdynamicplanner, sn_key),
      CStateClient(false),
      m_scMapRecvdOneDelta(false),
      m_paramsRecvdOne(false),
      m_paramsTalker(sn_key,SNocpParams,MODdynamicplanner)
{
	 
        //initialize costmap and related mutexes and sockets
        m_skynetKey = sn_key;
        m_useAstate = useAstate;
	if(m_useAstate)
	 cout << "Warning: using State from AState" << endl;
	m_pScmapDelta = new char[MAX_DELTA_SIZE];   //this is the buffer for the deltas
    
        m_pStaticCostMap = new CMapPlus();
	m_pStaticCostMap->initMap(center_N, center_E, SCM_NROWS, SCM_NCOLS, SCM_RESROWS, SCM_RESCOLS, VERBOSITY); // initialize the map qith appropriate size
	m_scmapLayer = m_pStaticCostMap->addLayer<double>(SCM_NODATA,SCM_OUTSIDEMAP,SCM_USEDELTA); //add the staic cost layer

	DGCcreateMutex(&m_scmapMutex);                  //mutex for the static cost map
	DGCcreateMutex(&m_scmapDeltaRecvdMutex);     //mutex for the deltas
	DGCcreateCondition(&m_scmapDeltaRecvdCond);  //condition on deltas reception
	
        m_scmapRequestSocket = m_skynet.get_send_sock(SNtplannerStaticCostMapRequest);  //put skynet interface here
	m_scmapDeltaSocket =  m_skynet.listen(SNtplannerStaticCostMap,MODtrafficplanner); //put skynet interface here   

        //params mutexes
	
        DGCcreateMutex(&m_paramsMutex);                  //mutex for the static cost map
	DGCcreateMutex(&m_paramsRecvdMutex);     //mutex for the deltas
	DGCcreateCondition(&m_paramsRecvdCond);  //condition on deltas reception
	
       	m_paramsSocket =  m_skynet.listen(SNocpParams,MODtrafficplanner); //put skynet interface here   


        //obstacles
	
        DGCcreateMutex(&m_obstMutex);                  //mutex for the static cost map
//	DGCcreateMutex(&m_obstRecvdMutex);     //mutex for the deltas
//	DGCcreateCondition(&m_obstRecvdCond);  //condition on deltas reception
	
       	m_obstSocket =  m_skynet.listen(SNocpObstacles,MODtrafficplanner); //put skynet interface here   



        //start the threads maybe in the future we should avoid threads and make them synchrounous functions.
         
        DGCstartMemberFunctionThread(this, &OCPtSpecs::getOCPparamsThread) ;
	DGCstartMemberFunctionThread(this, &OCPtSpecs::getOCPobstThread) ;
	DGCstartMemberFunctionThread(this, &OCPtSpecs::getScmapDeltasThread); //start the thread to catch the map deltas 
	scmapRequest(); // send a request for the full map
    
}

void OCPtSpecs::scmapRequest()
{
	bool bRequestScmap = true;
	m_skynet.send_msg(m_scmapRequestSocket,&bRequestScmap, sizeof(bool) , 0);
}

void OCPtSpecs::getScmapDeltasThread()
{
  // The skynet socket for receiving map deltas (from fusionmapper)
//	int scMapDeltaSocket = m_skynet.listen( <Put here the communication names>,  <Put here the communication names>);
//	if(scMapDeltaSocket < 0)
//		cerr << "OCPtSpecs::getMapDeltasThread(): skynet listen returned error" << endl;

	while(true)
	{
		int deltasize;
		
		cout << "Waiting for SCM Deltas " << endl;

		// I don't think the RecvMapdelta function works like it should --JM
		//RecvMapdelta(m_scmapDeltaSocket, m_pScmapDelta, &deltasize);  

//		WaitForNewState();

		//LOCK MUTEX!!!
                
		// we update the map from the problems parameters
		if(m_useAstate)
		{
		    m_pStaticCostMap->updateVehicleLoc(m_state.localX, m_state.localY);
		}
		else
		{
		    
		    DGClockMutex(&m_paramsMutex);
		    m_pStaticCostMap->updateVehicleLoc(m_OCPparams.initialConditionLB[NORTHING_IDX_C], m_OCPparams.initialConditionLB[EASTING_IDX_C]);
		    DGCunlockMutex(&m_paramsMutex);
		}

                deltasize = m_skynet.get_msg(m_scmapDeltaSocket, m_pScmapDelta, MAX_DELTA_SIZE, 0);
	       if(deltasize>0)
	       {
	
	           DGClockMutex(&m_scmapMutex);
	           cout << "Applying SCM Deltas " << endl;
		   m_pStaticCostMap->applyDelta<double>(m_scmapLayer, m_pScmapDelta, deltasize);	
		  
		   if(!m_scMapRecvdOneDelta)
		   {
		       DGCSetConditionTrue(m_scMapRecvdOneDelta, m_scmapDeltaRecvdCond, m_scmapDeltaRecvdMutex);
		   } 

		   //UNLOCK MUTEX!!!
		   DGCunlockMutex(&m_scmapMutex);
               }
//		cout << "Receive Delta Thread is running"  <<endl;
				
//		sleep(1);
	}

}

void OCPtSpecs::getOCPparamsThread()
{
       bool paramsCorrect;
       OCPparams tempParams;
	while(true)
	{
		
		cout << "Waiting for Params" << endl;

		// I don't think the RecvMapdelta function works like it should --JM
		//RecvMapdelta(m_scmapDeltaSocket, m_pScmapDelta, &deltasize);  
                paramsCorrect = false;
		

                paramsCorrect = m_paramsTalker.receive(&tempParams);
		if(! paramsCorrect)
		{
		    cout << "error" << endl;
		//UNLOCK MUTEX!!!
                }
		else
		{
		    DGClockMutex(&m_paramsMutex); 
		    m_OCPparams = tempParams;
                    cout << "I sould apply params. g: " << m_OCPparams.parameters[11] <<endl;     
		    if(!m_paramsRecvdOne)
		    {
		        DGCSetConditionTrue(m_paramsRecvdOne, m_paramsRecvdCond, m_paramsRecvdMutex);
	            }   
                    DGCunlockMutex(&m_paramsMutex);
		
		}
//		cout << "Receive Params Thread is running"  <<endl;
				
	//	sleep(1);
	}

}

void OCPtSpecs::getOCPobstThread()
{
       int obstSize;
	while(true)
	{
		
		cout << "Waiting for obstacles" << endl;

		// I don't think the RecvMapdelta function works like it should --JM
		//RecvMapdelta(m_scmapDeltaSocket, m_pScmapDelta, &deltasize);  
                obstSize = 0;
		
		DGClockMutex(&m_obstMutex); 

		//LOCK MUTEX!!!

                
		obstSize =  m_skynet.get_msg(m_obstSocket, &m_OCPobstacles, sizeof(OCPobstacles), 0); 


		if(obstSize!=sizeof(OCPobstacles))
		{
		    cout << "error" << endl;
		//UNLOCK MUTEX!!!
		    DGCunlockMutex(&m_obstMutex);
                }
		else
		{
                    cout << "I sould apply obstacles" << endl;
                    
		/*    if(!m_obstRecvdOne)
		    {
		        DGCSetConditionTrue(m_obstRecvdOne, m_obstRecvdCond, m_obstRecvdMutex);
	            } */  
                    DGCunlockMutex(&m_paramsMutex);
		
		}
		cout << "Receive Params Thread is running"  <<endl;
				
	//	sleep(1);
	}

}


OCPtSpecs::~OCPtSpecs(void)
{
//not doing anything for now
}


/* ! Use to specify all the dimensions and flags related to the coded OC Problem. */
void OCPtSpecs::getOCPComponentSizes(OCPComponentSizes* const ocpComponentSizes)
{
	//this are fixed for now, will be stored in a class member in the future
	ocpComponentSizes->IsActive         = true;
    ocpComponentSizes->HasICostFunction     = false;
    ocpComponentSizes->HasTCostFunction     = true;
    ocpComponentSizes->HasFCostFunction     = false;
    ocpComponentSizes->NofILinConstraints   = 0;
    ocpComponentSizes->NofTLinConstraints   = 0;
    ocpComponentSizes->NofFLinConstraints   = 0;
    ocpComponentSizes->NofINLinConstraints  = 0;
    ocpComponentSizes->NofTNLinConstraints  = 0;
    ocpComponentSizes->NofFNLinConstraints  = 0;
}


/*! Use to implement the cost function applicable for the begining of the time interval.*/
void OCPtSpecs::InitialCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Fi, double* const DFi)
{
    return ; // no initial cost function for the moment
}

/*! Use to implement the cost function for the whole time interval.*/
void OCPtSpecs::TrajectoryCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ft, double* const DFt)
{
    if(*mode == 0 || *mode == 2)
    {   
       *Ft = m_pStaticCostMap->getDataUTM<double>(m_scmapLayer, z[3], z[0]);
    }

    if(*mode == 1 || *mode == 2)
    {
        for(m_i=0;m_i<NZ;m_i++)  //initialize the df/dz-vector to 0 
        {
    	    DFt[m_i] = 0;
        }
     double grad[2];
     m_pStaticCostMap->getUTMGradient(m_scmapLayer, z[3], z[0], grad);
     DFt[0]=grad[0];
     DFt[3]=grad[1];
     }  
       
//       if(*mode == 99)  not implemented yet
//       {
//          // sparsity
//       }
}

/*! Use to implement the cost function applicable at the end of the time interval.*/
void OCPtSpecs::FinalCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ff, double* const DFf)
{
    return ;  //not used yet
}

/*! Use to implement the linear constraints - those applicable at the beginning of the time interval, for the whole time interval and for those at the end of the time interval.*/
void OCPtSpecs::getLinearConstraints(const ConstraintType* const ctype, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A, const int* const Offset)
{
//	if(*ctype == ct_LinInitial)
//	{
//        for(m_i=0; m_i<NofLConstraints; m_i++)
//        {
//            for(m_j=0; m_j<NofVariables; m_j++)
//            {
//                A[m_i][m_j] = 0.0;
//            }
//        }
//        
// 	}	
//	else if(*ctype == ct_LinTrajectory)
//	{
//          
//	}
//	else /*if(*ctype == ct_LinFinal)*/
//	{
//         
//	}	

    return ;  //not used yet

}

/*! Use to imOCPtSpecs::plement the nonlinear constraints applicable at the beginning of the time interval.*/
 void OCPtSpecs::InitialNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli, const int* const Offset) 
{
    return ;  //not used yet
}

/*! Use to implement the nonlinear constraints applicable during the whole time interval.*/
void OCPtSpecs::TrajectoryNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt, const int* const Offset)
{
   //compute the function
   //
   //compute the  Jacobian
   //
   //compute the Sparsity
    


return ;  //not used yet
}

/*! Use to implement the nonlinear constraints applicable at the end of the time interval.*/
void OCPtSpecs::FinalNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf, const int* const Offset)
{
    return ;  //not used yet
}

/*! Use to implement the bounds for all the constraints: linear and nonlinear.*/
void OCPtSpecs::getLUBounds(const ConstraintType* const ctype, const BoundsType* const ubtype, const int* const NofConstraintCollocPoints, const int* const NofConstraints, double* const LUBounds, const int* const Offset)
{
//	if(*ctype == ct_LinInitial)
//	{
//        
//        if(*ubtype == ubt_LowerBound)
//        {
//            for(m_i=0; m_i<m_NofConstraints; m_i++)
//            {
//                LUBounds[m_i] = 0;
//            }                
//        }
//        else
//        {
//            for(m_i=0; m_i<m_NofILinConstraints; m_i++)
//            {
//                LUBounds[m_i] = 0;
//            }                
//        }
//        
//	}
//
//	if(*ctype == ct_LinTrajectory)
//	{
//          
//
//	}
//
//	if(*ctype == ct_LinFinal)
//	{
//
//	}
//
//	if(*ctype == ct_NLinInitial)
//	{
//
//	}
//
        if(*ctype == ct_NLinTrajectory)
	{  
	    double temp =0;
            for(m_i=0; m_i<(*NofConstraintCollocPoints); m_i++)
	    {
	        for(m_j=0; m_j<(*NofConstraints); m_j++)
	        {
	            if( *ubtype == ubt_LowerBound )
		    {
		        temp = 1;
		    }
		    else
		    {
		        temp = DBL_MAX;
		    }
	        LUBounds[*Offset+m_i*(*NofConstraints)+m_j]=temp;
	        } 
	    }

	}
//
//	if(*ctype == ct_NLinFinal)
//	{
//       
//	}   
return ;  //not used yet
}
    
    
void  OCPtSpecs::getInitialCondition(const BoundsType* const ubtype, double* const initCond)
{
    bool valuecheck = false;
    if( *ubtype == ubt_LowerBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          initCond[m_i]=m_OCPparams.initialConditionLB[m_i];	  
        }
	valuecheck = true;
    }
    
    if( *ubtype == ubt_LowerBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          initCond[m_i]=m_OCPparams.initialConditionUB[m_i];	  
        }
	valuecheck = true;
    }
    if(valuecheck==false)
    {
        cout << "wrong value for bounds type" <<endl;
        exit(1);
    }

    return ;
}

void  OCPtSpecs::getFinalCondition(const BoundsType* const ubtype, double* const finalCond)
{
    bool valuecheck = false;
    if( *ubtype == ubt_LowerBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          finalCond[m_i]=m_OCPparams.finalConditionLB[m_i];	  
        }
	valuecheck = true;
    }
    
    if( *ubtype == ubt_LowerBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          finalCond[m_i]=m_OCPparams.finalConditionUB[m_i];	  
        }
	valuecheck = true;
    }
    if(valuecheck==false)
    {
        cout << "wrong value for bounds type" <<endl;
        exit(1);
    }

    return ;
}



void  OCPtSpecs::getParams(double* const params)
{

    for(m_i=0;m_i<PARAMS_NUMBER;m_i++)
    {
        params[m_i]=m_OCPparams.parameters[m_i];	  
    }
    params[12] = m_OCPparams.initialConditionLB[VELOCITY_IDX_C];
    params[13] = m_OCPparams.finalConditionLB[VELOCITY_IDX_C];
    params[14] = m_OCPparams.initialConditionLB[ACCELERATION_IDX_C];
    params[15] = m_OCPparams.finalConditionLB[ACCELERATION_IDX_C];
    
return ;
}
void  OCPtSpecs::getMode(int* mode)
{     
    *mode = m_OCPparams.mode; 
    return ;
}
    
