#include <sstream>
#include <highgui.h>
#include <dgcutils/AutoMutex.hh>
#include "CostMapEstimator.hh"
#include <boost/serialization/vector.hpp>

void CostMapEstimator::initMap(CostMap* map)
{
  float centerX = 0;
  float centerY = 0;
  int width = 800;
  int height = 800;
  float resX = 0.1;
  float resY = 0.1;
  map->init(width, height, resX, resY, point2(centerX, centerY));
  map->clear(100);
  map->setOutOfBounds(200);
}

void CostMapEstimator::mouseCallback(int event, int x, int y, int flags, void* param)
{
  CostMap* map = (CostMap*) param;
  if (event == CV_EVENT_LBUTTONDOWN)
  {
    float xLoc, yLoc;
    map->toLocalFrame(y, x, &xLoc, &yLoc);
    cout << "x = " << xLoc << ", y = " << yLoc << ", cost = " << map->getPixel(y, x) << endl;
  }
}


bool CostMapEstimator::updateMap(CostMap* map)
{
  if (!polyTalker.hasNewMessage()) {
    return false;
  }

  BitmapParams bmparams;

  while (polyTalker.hasNewMessage()) {
    polyTalker.receive(&bmparams);
  }
  if (m_debugLevel > 1) {
    cout << "center = (" << bmparams.centerX << ", " << bmparams.centerY 
	 << ")" << endl;
    cout << "res = (" << bmparams.resX << ", " << bmparams.resY 
	 << ")" << endl;
    cout << "size = (" << bmparams.width << ", " << bmparams.height 
	 << ")" << endl;
    cout << "baseVal = " << bmparams.baseVal << endl;
  }
  map->init(bmparams.width, bmparams.height, bmparams.resX, bmparams.resY, 
	    point2(bmparams.centerX, bmparams.centerY));
  map->clear(bmparams.baseVal);
  map->setOutOfBounds(bmparams.outOfBounds);

  if (m_debugLevel > 1) {
    cout << endl << "Number of polygons: " << bmparams.polygons.size() << endl;
  }
  for (unsigned i=0; i<bmparams.polygons.size(); i++) {
    if (m_debugLevel > 1) {
      ostringstream os;
      os << "poly{" << i << "}";
      bmparams.polygons[i].saveToMatlabScript(cout, os.str());
    }
    bmparams.polygons[i].draw(map);
  }

  if (m_guiEnabled) {
    IplImage* img = cvCloneImage(map->getImage());
    double minVal, maxVal;
    cvMinMaxLoc(img, &minVal, &maxVal);
    cvSubS(img, cvRealScalar(minVal), img);
    if (maxVal != minVal)
      cvScale(img, img, 1/(maxVal - minVal));

    cvNamedWindow( "dplanner cost map", CV_WINDOW_AUTOSIZE );
    cvShowImage( "dplanner cost map", img);
#warning "If the map pointer gets deleted, clicking on the windows will segfault."
    cvSetMouseCallback( "dplanner cost map", mouseCallback, map);
    cvReleaseImage(&img);
    cvWaitKey(1000);
  }

  return true;

}

CostMap CostMapEstimator::getLatestMap()
{
  AutoMutex lock(m_cmapMutex);
  return m_cmap;
}

void CostMapEstimator::getMapThread()
{
  while (true) {
    DGClockMutex(&m_cmapMutex);
    if (updateMap(&m_cmap) && m_debugLevel > 0)
      cout << "Received map" << endl;
    DGCunlockMutex(&m_cmapMutex);
    usleep(m_usleepTime);
  }
}
