#include "OCPtSpecs.hh"

///////////////////////////////////
//////   COMMON ACCESSORS  ////////
///////////////////////////////////

    
void  OCPtSpecs::getInitialCondition(const BoundsType* const ubtype, double* const initCond)
{
    bool valuecheck = false;
    if( *ubtype == ubt_LowerBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          initCond[m_i]=m_OCPparams.initialConditionLB[m_i];	  
        }
	    valuecheck = true;
    }
    
    if( *ubtype == ubt_UpperBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          initCond[m_i]=m_OCPparams.initialConditionUB[m_i];	  
        }
	    valuecheck = true;
    }
    if(valuecheck==false)
    {
        cout << "wrong value for initial bounds type" <<endl;
        exit(1);
    }
    return ;
}


void  OCPtSpecs::getFinalCondition(const BoundsType* const ubtype, double* const finalCond)
{
    bool valuecheck = false;
    if( *ubtype == ubt_LowerBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          finalCond[m_i]=m_OCPparams.finalConditionLB[m_i];	  
        }
	valuecheck = true;
    }
    
    if( *ubtype == ubt_UpperBound)
    {
        for(m_i=0;m_i<COND_NUMBER;m_i++)
        {
          finalCond[m_i]=m_OCPparams.finalConditionUB[m_i];	  
        }
	valuecheck = true;
    }
    if(valuecheck==false)
    {
        cout << "wrong value for final bounds type" <<endl;
        exit(1);
    }
    return ;
}


void  OCPtSpecs::getParams(double* const params)
{
    for(m_i=0;m_i<PARAMS_NUMBER;m_i++)
    {
        params[m_i]=m_OCPparams.parameters[m_i];	  
    }
    params[12] = m_OCPparams.initialConditionLB[VELOCITY_IDX_C];
    params[13] = m_OCPparams.finalConditionLB[VELOCITY_IDX_C];
    params[14] = m_OCPparams.initialConditionLB[ACCELERATION_IDX_C];
    params[15] = m_OCPparams.finalConditionLB[ACCELERATION_IDX_C];
   
    return ;
}


void  OCPtSpecs::getMode(int* mode)
{     
    *mode = m_OCPparams.mode; 
    return ;
}

double OCPtSpecs::getMaxVelocity()
{
   return m_OCPparams.parameters[VMAX_IDX_P];

}
//////////////////////////////////////
////// START END SOLUTION     ////////
//////////////////////////////////////

void OCPtSpecs::startSolve()
{
    if(m_verbose)
	{
        cout << "Request to solve. RDDF: "<< m_useRDDF << " Obst: " << m_useObstacles<< " Map: "<< m_useCostMap << " Params: " << m_useParams <<" Poly: " << m_usePolyCorridor <<endl;
	    cout <<"Have RDDF: " << m_RDDFRecvdCond.bCond << " Have Obst: " <<m_obstRecvdCond.bCond << " Have Params: " << m_paramsRecvdCond.bCond << " Have Cost: " <<m_costMapRecvdCond.bCond << " Have Poly: " << m_polyCorridorRecvdCond.bCond <<endl;
	}
    if(m_useRDDF)
        DGCWaitForConditionTrue(m_RDDFRecvdCond);
    
    if(m_useParams)
        DGCWaitForConditionTrue(m_paramsRecvdCond);
    if(m_useObstacles)
        DGCWaitForConditionTrue( m_obstRecvdCond);
    
    if(m_useCostMap)
       DGCWaitForConditionTrue( m_costMapRecvdCond);
   
    if(m_usePolyCorridor)
        DGCWaitForConditionTrue(m_polyCorridorRecvdCond);
   
    DGClockMutex(&m_rddfDataMutex);    
    DGClockMutex(&m_obstDataMutex);    
    DGClockMutex(&m_paramsDataMutex);
	m_OCPparams = m_OCPparamsTmp ; 
    cout << "Providing final condition " << m_OCPparams.finalConditionLB[NORTHING_IDX_C]<< " " << m_OCPparams.finalConditionLB[EASTING_IDX_C] << " " << m_OCPparams.finalConditionLB[HEADING_IDX_C]<< endl;
    DGCunlockMutex(&m_paramsDataMutex);
    DGClockMutex(&m_costmapDataMutex);    
    DGClockMutex(&m_polyCorridorDataMutex);    
    if(m_verbose)
        cout << "Ready to solve."<< endl;    
    return ;
}


void OCPtSpecs::endSolve()
{
   // bool result = m_statusTalker.send(&m_dplannerStatus);    
    DGCunlockMutex(&m_rddfDataMutex);    
    DGCunlockMutex(&m_obstDataMutex);    
  //  DGCunlockMutex(&m_paramsDataMutex); 
  //  DGCSetConditionFalse(m_paramsRecvdCond);
    DGCunlockMutex(&m_costmapDataMutex);    
    DGCunlockMutex(&m_polyCorridorDataMutex);    
    
    if(m_verbose)
        cout << "Finished solving" << endl; 
    return ;
}

/////////////////////////////////////
//////   DISPLAY FUNCTION    ////////
/////////////////////////////////////

void OCPtSpecs::printPolyCorridor()
{
   cout << endl<< endl << endl <<  "Printing corridor information " << endl << endl << endl; 
   cout << "Number of polytopes: " << m_nPolytopes << endl; 
   for(int i=0;i<m_nPolytopes;i++)
      cout <<m_polyCorridor[i] << endl;
}




