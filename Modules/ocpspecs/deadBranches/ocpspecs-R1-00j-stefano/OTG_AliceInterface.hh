/*
	OTG_AliceInterface.hh
	OTG Interface specific for UGC Alice.
	Addendum to IOCP components.

	S. Di Cairano mar-07.
*/
//#ifdef OTG_HOME
//#include "NURBSBasedOTG/OTG_Interfaces.hh"
//#include "OTG_InterfacesX.hh"
//#else
//#warning using OTG_InterfacesX.hh as placeholder
//#include "OTG_InterfacesX.hh"
#include "NURBSBasedOTG/OTG_Interfaces.hh"
//#endif
#ifndef OTG_ALICEINTERFACE_HH
#define OTG_ALICEINTERFACE_HH

/*! -  This enum is used to encapsulate the valid flags that specify the types of bounds available.*/
//enum BoundsType{ubt_LowerBound, ubt_UpperBound};


/*! - In order to plug and play with the optimal control problem, the user is required to implement the IOCPComponent interface. */
class IOCPparameters
{
    public:
        /* ! Use to specify all the dimensions and flags related to the coded OC Problem. */

		/*! Use to implement the bounds for all the constraints: linear and nonlinear.*/
        virtual void getInitialCondition(const BoundsType* const ubtype, double* const initCond) = 0;
        virtual void getFinalCondition(const BoundsType* const ubtype, double* const finalCond) = 0;
        virtual void getParams(double* const params) = 0;
	virtual void getMode(int* mode) = 0; 
        virtual void startSolve() = 0;
	virtual void endSolve() = 0;
};


#endif /* OTG_ALICEINTERFACE_HH */

