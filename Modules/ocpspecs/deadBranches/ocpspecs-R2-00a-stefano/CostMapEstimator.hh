#ifndef COSTMAPESTIMATOR_HH_2345678954345368956478
#define COSTMAPESTIMATOR_HH_2345678954345368956478

#include <bitmap/Polygon.hh>
#include <bitmap/Bitmap.hh>
#include <bitmap/BitmapParams.hh>
#include <dgcutils/DGCutils.hh>
#include <skynettalker/SkynetTalker.hh>

using namespace bitmap;

class CostMapEstimator
{
private:
  /*! talker for receiving parameters for painting cost map from tplanner */
  SkynetTalker< BitmapParams > polyTalker;

  /*! whether we want to run the gui */
  bool m_guiEnabled;

  /*!m_cmap will only be used when the thread is enabled */
  CostMap m_cmap;

  /*! mutex for m_cmap */
  pthread_mutex_t m_cmapMutex;

  int m_debugLevel;

  /*! sleep in getMapThread */
  int m_usleepTime;

  /*! specify whether we want to start getMapThread */
  bool m_startThread;

  /*! a thread that keeps listening for BitmapParams */
  void getMapThread();

  static void mouseCallback(int event, int x, int y, int flags, void* param);

public:
  /*! Default constructor */
  CostMapEstimator(int skynetKey, bool guiEnabled = false, 
		   bool startThread = false, int debugLevel = 0,
		   long usleepTime=100000) 
    : polyTalker(skynetKey, SNbitmapParams, MODdynamicplanner, true),
      m_guiEnabled(guiEnabled), m_debugLevel(debugLevel), 
      m_usleepTime(usleepTime), m_startThread(startThread)
  { 
    if (m_startThread) {
      DGCcreateMutex(&m_cmapMutex);
      DGCstartMemberFunctionThread(this, &CostMapEstimator::getMapThread);
    }
  }

  /*! Destructor */
  virtual ~CostMapEstimator()
  {
    if (m_startThread)
      DGCdeleteMutex(&m_cmapMutex);
  }

  /*! Initialize the cost map */
  void initMap(CostMap* map);

  /*! Update the cost map. Use this function when you don't run the thread. */
  bool updateMap(CostMap* map);

  /*! Return the latest cost map. Use this function when you run the thread. */
  CostMap getLatestMap();
  
};

#endif // COSTMAPESTIMATOR_HH_2345678954345368956478
