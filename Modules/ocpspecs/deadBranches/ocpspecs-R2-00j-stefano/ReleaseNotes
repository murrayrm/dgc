              Release Notes for "ocpspecs" module

Release R2-00j-stefano (Thu May 31 15:35:59 2007):
    Fised a couple of potentially dangerous bugs. Now the initial-final conditions are always listened, and copied over when the solver starts. We also force to wait for a new tplanner message before to solve a new problem. 

Release R2-00j (Tue May 29 18:43:32 2007):
    Major update. The message queues are flushed entirely to provide only the most recent message. To do that properly we also had to add separate mutices for each message type and sleep of 150 msec in each receive thread. Added a new constructor based on a struct. Updated Heuristic function for s1planner and added a function that check for point trajectory legality only locally. Map visualization can be switched on and off, as well as gradient computation. 

Release R2-00i (Mon May 28 22:04:06 2007):
	Set out of bounds to be large number (as set by tplanner).

Release R2-00h (Fri May 25  0:10:21 2007):
    Many modifications. Introduced waypoints, computed from polytopes intersections. Corridor robustified, checks added. It worked ok in many tests, completing 75% and never segfaulting (in those).

Release R2-00g (Tue May 22 17:30:19 2007):
    Corridor reconstruction robustified. A* heuristic implemented.

Release R2-00f (Thu May 17 23:37:37 2007):
    Committing again because something in the previous release was not saved. Also modified the intersection volume computation to be more robust.

Release R2-00e (Thu May 17 12:54:52 2007):
    Modified accessor functions that return the cost and gradient to tplanner. Added accessor functions that returns the polytopic corridor and the cost gradient components.

Release R2-00d (Wed May 16 23:02:09 2007):
    Fixed memory leaks in the corridor and in general in the polyhedral computations. 

Release R2-00c (Tue May 15 15:51:50 2007):
    Initial and Final conditions changed according the convention. Function that computes the volume of the intersection of polyhedra added (possibly to be moved where we need). Waiting conditions arer now disabled. 

Release R2-00b (Mon May 14 13:26:46 2007):
	Updated map element functions to work with the new map version

Release R2-00a (Fri May 11  0:57:28 2007):
    New costmap is integrated. The module now depend on BITMAP module. Dependency on CMAP removed. The module does not use anymore state information.
	Basic computations (costmap value and gradient) are in place. Gradient maps are generated even though not extensively tested.
	Computation of corridor is in place together with accessor functions that check if a point is inside the corridor.
	Need extensive integration testing.

Release R1-01e (Wed May  9  1:34:28 2007):
	* Added CostMapEstimator which paints cost map based on parameters
	received over skynet.
	* Two unit tests, one starts the thread, the other doesn't.

Release R1-01d (Tue May  8 10:50:58 2007):
    Minor changes while testing with dplanner

Release R1-01c (Sat Apr 28 11:23:30 2007):
    Inserted supervisory thread to send/receive status messages and pause messages.

Release R1-01b (Mon Apr 23 23:17:38 2007):
    Minor changes to comply with the new function declarations in the OTG library

Release R1-01a (Sun Apr 22 10:54:12 2007):
    Minor changes in the code. Switching release number because I forgot to do that before, when moving to the flat model of Alice (major change).

Release R1-00w (Tue Apr 17 15:11:22 2007):
    Code has been modified to deal with the flat model of Alice.

Release R1-00v (Tue Apr 10 16:47:53 2007):
	Added some functionality to the testParams program.  This program
	now accepts command line options (-S) and reads an RDDF file to
	determine what initial and final points to send.  In addition to
	sending the OCPparams information, the RDDF is also sent using the
	RDDFTalker.  This allows the testParams program to be used to
	generate the minimal input required by dplanner for generating a
	trajectory.

Release R1-00u (Fri Mar 23 17:45:00 2007):
        Removed unused sockets. They are commented in the code.
	
Release R1-00t (Fri Mar 16 12:10:55 2007):

Release R1-00s (Fri Mar 16 10:12:36 2007):
	Added ability to specify whether error messages are printed.  In
	OCPtSpecs constructor, this can be specified as the last argument
	(defaults to verbose = 0 => no messages).  Setting to 1 will print
	out basic operations, 2 will print out functional flow and 3 will
	print out data details.

	Also fixed a bug in the way that RDDFs are received.  Turns out that
	RDDFTalker will not overwrite the first part of an RDDF that is read
	from a stream => we need to clear the RDDF before sending the
	pointer to RDDFRecv.  This is now corrected using the new RDDF.clear
	function and is working with rddfplanner/tplanner.

Release R1-00r (Thu Mar 15 21:32:00 2007):
        Fixed bugs from testing with tplanner and trajfollow

Release R1-00q (Thu Mar 15 13:32:41 2007):
        Modified the internal representation of nonlinear constraint, now in
        conic form. The Declarations requried by tplanner are now on the
        interfaces module, (file TpDpInterface.hh). Improved the evaluation
        of quadratic functions.	 

Release R1-00p (Wed Mar 14 19:41:38 2007):
        Modified handling of the nonlinear constraints, since SNOPT does not
        want constant terms in the nonlinear constraint functions. The
        interface is still the same. Tested wrt Matlab evaluation. 

Release R1-00o (Wed Mar 14 17:46:05 2007):
        Constraint evaluation and Jacobian computation tested with
        spoof-data. Few corrections (seg.faults). 

Release R1-00n (Tue Mar 13 19:14:36 2007):
        RDDF reception modified to avoid creating a dummy thread	

Release R1-00m (Tue Mar 13 17:20:40 2007):
        RDDF reception tested. Few small corrections.	

Release R1-00l (Tue Mar 13 14:16:20 2007):
	Inserted sparsity and RDDF talker to receive corridor.

Release R1-00k (Mon Mar 12 19:20:49 2007):
        Inserted static obstacles and thread synchronization. Using
        skynettalker for parameters and obstacles.	 

Release R1-00j (Sun Mar 11 20:13:39 2007):
	Reverted to using the OTG version of the interface library (we had
	been using a copy).  This version should compile against dplanner
	but will not compile on Alice (OTG libraries not installed there
	yet).

Release R1-00i (Sun Mar 11 19:34:38 2007):
         Included test program to send OCPparams. Using skynetTalker
         now. State can be read from astate or from OCPparams. Unit testing
         done." 

Release R1-00h (Sat Mar 10 20:13:24 2007):
	Added accessors for OCP parameters initial conditions and final
	conditions. Message groups are correctly set. 

Release R1-00g (Thu Mar  8 15:47:09 2007):
	Added interface to catch OCP parameters including initial and final
	conditions. Added the appropriate talker, which can be possibly
	moved into interfaces in the future. 

Release R1-00f (Sat Mar  3 16:21:36 2007):
	Added CStateClient inheritance to the OCPtSpecs class since in order
	to maintain its local copy of the cost map, it needs to update the
	map to the vehicle location each time through the loop. made
	appropriate changes to the makefile and the test script that Stefano
	wrote. 

Release R1-00e (Sat Mar  3 13:41:27 2007):
	Now that we can send deltas through the CMap class, the CostMapMsg.h
	interface is obsolete. Changing OCPspecs to use the SendMapdelta
	function instead of the CostMapMsg interface.	 

Release R1-00d (Fri Mar  2 17:47:13 2007):
	Since the RecvMapDeltas function didn't seem to be working in the
	CMapDeltaTalker class, modified ocpspecs to handle deltas sent using
	the CostMapMsg.h interface

Release R1-00c (Thu Mar  1 19:37:44 2007):
	This release of ocpspecs checks to see if the OTG_HOME environment
	variable is defined.  If so, then it includes header files from
	that directory (which are NGC proprietary).  This change allows
	authorized OTG users (currently Melvin and Richard) to link against
	the NGC code.  If OTG_HOME is not defined, a substitute header
	is used (which will let the module compile).

Release R1-00b (Wed Feb 28 13:43:01 2007):
        Modified Makefile and class to work correctly on the test
        program. The class object is correctly instanciated and the thread
        that receives deltas runs. Nobody is sending delta for now, so that
        map population has not been tested yet. 	 

Release R1-00a (Tue Feb 27 23:37:32 2007):
	This is the initial release of OCPSPECS module. The module contains
	a virtual class that is the interface to the OCP specs and a class
	that implements it. Code compiles but it has not been tested
	yet. The compiled library that depends on modules: interfaces,
	cmap. The compiled test program is just an empty shell for now. 

Release R1-00 (Tue Feb 27 22:03:09 2007):
	Created.









































