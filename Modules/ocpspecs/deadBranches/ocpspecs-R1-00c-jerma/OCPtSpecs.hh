#ifndef OCPTSPECS_HH_
#define OCPTSPECS_HH_

#endif /*OCPTSPECS_HH_*/
/*
 *  tdinterface.hh
 *  dplanner
 *
 *  Created by mflores on 2/15/07.
 *  Copyright 2007 __MyCompanyName__. All rights reserved.
 *
 */

#ifdef OTG_HOME
#include "NURBSBasedOTG/OTG_Interfaces.hh"
#else
#warning using OTG_InterfacesX.hh as placeholder
#include "OTG_InterfacesX.hh"
#endif
#include "cmap/CMapPlus.hh"
#include "cmap/MapdeltaTalker.hh"
#include "interfaces/sn_types.h" 
//#include "skynet/sn_msg.hh"
#include "skynet/skynet.hh" 
#include "skynet/SkynetContainer.hh"
#include "dgcutils/DGCutils.hh"
#include "interfaces/CostMapMsg.h"

/* Flat outputs: */
#define z1		 z[0]
#define z1d		 z[1]
#define z1dd	 z[2]
#define z2		 z[3]
#define z2d		 z[4]
#define z2dd	 z[5]
#define z3		 z[6]
#define z3d		 z[7]
#define z3dd	 z[8]
#define z4		 z[9]
#define z4d		 z[10]
#define z4dd	 z[11]
#define z5		 z[12]
#define z6		 z[13]
#define z6d		 z[14]
#define z6dd	 z[15]
#define z7		 z[16]
#define z7d		 z[17]
#define z7dd	 z[18]

/* z1 = x, z2 = y, z3 = v, z4 = theta, z5 = t, z6 = a, z7 = phi */

#define NZ 19
#define SCM_NROWS 200          // taken from map.default.dat
#define SCM_NCOLS 200 
#define SCM_RESROWS 0.4
#define SCM_RESCOLS 0.4
#define VERBOSITY 0
#define SCM_NODATA 100       //these have to be tuned depending on the generated cost function
#define SCM_OUTSIDEMAP 90
#define SCM_USEDELTA 1

class OCPtSpecs : public IOCPComponent,  public CMapdeltaTalker
{ 
  
private:
  int m_i;
  int m_j;
  //static cost map data
  CMapPlus* m_pStaticCostMap;
  
  //for receiving map deltas
  CostMapMsg* costMsg;
  
  int m_scmapLayer;
  char* m_pScmapDelta;
  
  //map access mutex
  pthread_mutex_t		m_scmapMutex;
  
  //mutex+cond on received deltas
  bool m_scMapRecvOneDelta;
  pthread_mutex_t	m_scmapDeltaRecvMutex;
  pthread_cond_t m_scmapDeltaRecvCond;
  
  //socket to request the full map
  int m_scmapRequestSocket;
  //socket to receive deltas
  int m_scmapDeltaSocket;
  
  //private methods to deal with the static cost map
  void getScmapDeltasThread();  //thread to read deltas
  void scmapRequest();   //function that request the full map
  
public:
  
  OCPtSpecs(int sn_key);
  virtual  ~OCPtSpecs(void);
  
  /* ! Use to specify all the dimensions and flags related to the coded OC Problem. */
  virtual  void getOCPComponentSizes(OCPComponentSizes* const ocpComponentSizes);
  /*! Use to implement the cost function applicable for the begining of the time interval.*/
  virtual   void InitialCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Fi, double* const DFi);
  
  /*! Use to implement the cost function for the whole time interval.*/
  virtual   void TrajectoryCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ft, double* const DFt);
  
  /*! Use to implement the cost function applicable at the end of the time interval.*/
  virtual      void FinalCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ff, double* const DFf);
  
  /*! Use to implement the linear constraints - those applicable at the beginning of the time interval, for the whole time interval and for those at the end of the time interval.*/
  virtual void getLinearConstraints(const ConstraintType* const ctype, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A, const int* const Offset);
  
  /*! Use to implement the nonlinear constraints applicable at the beginning of the time interval.*/
  virtual void InitialNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli, const int* const Offset) ;
  
  
  /*! Use to implement the nonlinear constraints applicable during the whole time interval.*/
  virtual void TrajectoryNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt, const int* const Offset);
  
  /*! Use to implement the nonlinear constraints applicable at the end of the time interval.*/
  virtual void FinalNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf, const int* const Offset);
  
  /*! Use to implement the bounds for all the constraints: linear and nonlinear.*/
  virtual void getLUBounds(const ConstraintType* const ctype, const BoundsType* const ubtype, const int* const NofConstraintCollocPoints, const int* const NofConstraints, double* const LUBounds, const int* const Offset);
    
};
