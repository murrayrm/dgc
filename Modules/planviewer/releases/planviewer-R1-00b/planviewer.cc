/*!
 * \file planviewer.cc
 * \brief Simple viewer for planner stack
 *
 * \author Richard M. Murray
 * \date 4 March 2007
 *
 * This program is a simple viewer that displays information sent
 * between the various planners.  It is a "quick fix" to allow us to
 * see what is going on until we can develop a better graphical
 * interface.
 *
 */

#include "VehiclePlan.hh"
#include "mapdisplay/MapDisplay.hh"
#include "mapdisplay/MapConfig.hh"
#include "dgcutils/DGCutils.hh"
#include "dgcutils/cfgfile.h"
#include "cmdline.h"

VehiclePlan *veh;		///< Vehicle plan (and state)
MapDisplayThread *mdt;		///< Map display thread

/* 
 * Define the trajectories that we want to read 
 *
 * This list can be modified to include other trajectories that are
 * sent out by various modules.  To add a new trajectory, create an
 * entry at the bottom of the trajData initialization listing the
 * trajectory type, source (which I don't think is actually used) and
 * the color for the trajectory.
 */
struct trajEntry {
  int type;			// trajectory type
  int source;			// trajectory source
  RGBA_COLOR color;		// color to use
} trajData[] = {
  {0, 0, RGBA_COLOR(1.0, 0.0, 0.0)},		   // path history
  {SNtraj, SNplanner, RGBA_COLOR(1.0, 0.0, 1.0)}   // commanded path
};
#define NUMPATHS (sizeof(trajData)/sizeof(trajEntry))
#define HISTORY_PATH 0		// path to use for storing history
PATH paths[NUMPATHS];		// Space for storing path information

/* 
 * Define the list of cmaps that we want to read
 *
 * This list can be modified to include additional cmaps that we would
 * like to display.  This functionality is probably less useful now
 * that we have the mapviewer, but can basically be used to overlay
 * information onto the the map.
 */
struct mapEntry {
  int type;			// map type
  int source;			// map source
  const char *label;		// name of the layer
  int layer;			// layer for the map (uninitialized)
} mapData[] = {
  {SNfusiondeltamap, SNfusionmapper, "Final Cost"}
};
#define NUMMAPS (sizeof(mapData)/sizeof(mapEntry))
#define MAP_SIZE 500		// default number of rows and columns
#define MAP_RES	0.4		// default height and width of a row
char *mapDeltas[NUMMAPS];	// Space for storing map deltas

int main(int argc, char **argv)
{
  /* Process command line options */
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);

  /* Parse command line objects */
  int snkey = skynet_findkey(argc, argv);

  /* Create the vehicle plan object (includes state) */
  veh = new VehiclePlan(snkey);

  /* Create the map display thread object */
  mdt = new MapDisplayThread("PlanViewer", new MapConfig);

  /* Set up pointers to the vehicle information the display needs */
  mdt->get_map_config()->set_state_struct(veh->p_vehicleState());
  mdt->get_map_config()->set_actuator_state_struct(veh->p_actuatorState());

  /* Set up the cmap that we will use */
  if (cmdline.cmap_config_given) {
    // Read the map configuration from a file
    char *cfgfile = dgcFindConfigFile(cmdline.cmap_config_arg, "cmap");
    veh->cmap.initMap(cfgfile);

  } else {
    // Use the default map resolution
    if (cmdline.verbose_flag) {
      cerr << "planviewer: using default map: " << MAP_SIZE << " cells @ "
	   << MAP_RES << " m/cell" << endl;
    }
    veh->cmap.initMap(0, 0, MAP_SIZE, MAP_SIZE, MAP_RES, MAP_RES, 0);
  }

  // Now instantiate all of the layers
  for (unsigned i = 0; i < NUMMAPS; ++i) {
    /* Create the layer in the plan viewer cmap */
    mapData[i].layer = veh->cmap.addLayer<double>(0.0, -1.0);
    veh->cmap.setLayerLabel(mapData[i].layer, mapData[i].label);

    /* Allocate space for storing the map deltas */
    mapDeltas[i] = new char[MAX_DELTA_SIZE];
    if (mapDeltas[i] == NULL) {
      cerr << "couldn't allocate memory for map delta" << endl;
      exit(1);
    }

    /* Set up the arguments for starting a thread to read the deltas */
    struct mapArgument arg;
    arg.mapType = mapData[i].type;		
    arg.mapSource = mapData[i].source;
    arg.layer = mapData[i].layer;
    arg.delta = mapDeltas[i];

    /* Create a thread to listen for map deltas */
    DGCstartMemberFunctionThreadWithArg(veh, &VehiclePlan::getMapDeltaThread, 
					(void *) &arg);
  }
  DGCcreateMutex(&veh->cmap_mutex);
  mdt->get_map_config()->set_cmap(&veh->cmap, &veh->cmap_mutex);

  /* Start the thread that displays the gui */
  mdt->guithread_start_thread();

  /* Set up the RDDF for viewing */
  RDDFVector rddf_vector;
  mdt->get_map_config()->set_rddf(rddf_vector);
  DGCstartMemberFunctionThreadWithArg(veh, &VehiclePlan::getRDDFThread,
				      (void *) &rddf_vector);

  /* Start up the threads for trajectories; path 0 is history */
  for (unsigned i = 0; i < NUMPATHS; ++i) {
    /* Initialize the paths data structure */
    paths[i].color = trajData[i].color;
    DGCcreateMutex(&paths[i].pathMutex);

    /* Now call trajThread to read in the data */
    if (i > 0) {
      struct trajArgument arg;
      arg.pathType = trajData[i].type;		
      arg.pathSource = trajData[i].source;
      arg.points = &paths[i].points;
      arg.pathMutex = &paths[i].pathMutex;
      DGCstartMemberFunctionThreadWithArg(veh, &VehiclePlan::trajThread, 
					  (void *) &arg);
    }
  }
  mdt->get_map_config()->set_paths(paths, NUMPATHS);

  /* The main loop reads the vehicle state and displays the position */
  while (1) {
    veh->WaitForNewState();
    veh->UpdateState();
    veh->cmap.updateVehicleLoc(veh->p_vehicleState()->utmNorthing, 
			       veh->p_vehicleState()->utmNorthing);

    /* Update the path history */
    int num_points;
    if((num_points = paths[HISTORY_PATH].points.getNumPoints()) > 
       TRAJ_MAX_LEN-2 ) {
      paths[HISTORY_PATH].points.shiftNoDiffs(1);
    }
    paths[HISTORY_PATH].points.inputNoDiffs(veh->p_vehicleState()->utmNorthing, 
					    veh->p_vehicleState()->utmEasting);

    /* Check to see if we got a new RDDF */
    if (veh->newRDDF)  {
      mdt->get_map_config()->set_rddf(rddf_vector);
      veh->newRDDF = 0;
    }
    
    /* Update the display */
    mdt->notify_update();
  }
  return 0;
}
