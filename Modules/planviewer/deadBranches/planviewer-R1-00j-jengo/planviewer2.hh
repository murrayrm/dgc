#ifndef PLANVIEWER2_H_
#define PLANVIEWER2_H_

/*!
 * \file planviewer2.hh
 * \brief Converts planner stack data to MapElements and sends to mapviewer
 * for visualization
 *
 */

#include "VehiclePlan.hh"
#include "map/MapElement.hh"
#include "map/MapElementTalker.hh"
#include "cmdline.h"
#include "alice/AliceConstants.h"
#include "skynettalker/SkynetTalker.hh"
#include "interfaces/TpDpInterface.hh"
#include <boost/serialization/vector.hpp>


void genSteerSpline(point2arr &steer);

void genSteerCmdSpline(point2arr &steerCmd);

void RDDFtoMapElement(RDDFVector &vect, point2arr &rddf);

void polytopeToMapElement(sendCorr &corr, vector< point2arr > &corrPts);


/* stuff for sort.cc

double isLeft( point2 P0, point2 P1, point2 P2 );

#define NONE  (-1)

typedef struct range_bin Bin;
struct range_bin {
    int    min;    // index of min point P[] in bin (>=0 or NONE)
    int    max;    // index of max point P[] in bin (>=0 or NONE)
};

int nearHull_2D( point2arr &P, int n, int k, point2arr &H );
*/
#endif
