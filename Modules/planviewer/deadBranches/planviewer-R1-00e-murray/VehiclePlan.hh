/*!
 * \file VehiclePlan.hh
 * \brief Class for planner stack viewer
 *
 * \author Richard M. Murray
 * \date 4 March 2007
 *
 * The PlanViewer class is used to recieve various types of data
 * messages and update the map display.  This has to be implemented as
 * a class for now because all of the talkers want to derive from
 * SkynetContainer.  This needs to be fixed, but will be done later.
 *
 */

#include "skynet/skynet.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/RDDFTalker.hh"
#include "interfaces/sn_types.h"
#include "trajutils/TrajTalker.hh"
#include "cmap/CMapPlus.hh"
#include "cmap/MapdeltaTalker.hh"

class VehiclePlan : public CStateClient, public CTrajTalker, 
		    public CMapdeltaTalker, public CRDDFTalker
{
public:
  int newRDDF;			// set when we receive new RDDF
  CMapPlus cmap; 		// cmap to store layers
  pthread_mutex_t cmap_mutex; 

  VehiclePlan(int sn_key);
  ~VehiclePlan();

  /* Accessor functions */
  VehicleState *p_vehicleState() { return &m_state; }
  ActuatorState *p_actuatorState() { return &m_actuatorState; }

  /* Threads for reading data */
  void trajThread(void *);
  void getMapDeltaThread(void *);
  void getRDDFThread(void *);
};

/* Data structure for passing information about trajectory threads */
struct trajArgument {
  int pathType;			// skynet type
  int pathSource;		// skynet source

  /* The entries below are initialized at startup */
  CTraj *points;		// path to be displayed
  pthread_mutex_t *pathMutex;	// mutex for protecting access
};

/* Data structure for passing information about map layer threads */
struct mapArgument {
  int mapType;			// skynet type
  int mapSource;		// skynet source
  int layer;			// layer for this map
  char *delta;			// map delta storage
};
#define MAX_DELTA_SIZE 1000000	// max delta we can send
#warning MAX_DELTA_SIZE should be a global define??
