#include "planviewer2.hh"

VehiclePlan *veh;	///< Vehicle plan (and state)
point2arr rddfBound;
point2arr steerCmdSpline;
point2arr steerSpline;
point2arr planTraj;
vector< point2arr > polyCorr;
point2arr initCond;
point2arr finalCond;
int numPolys;
point2 emptyPoint;
bool useLocal;
bool useTransform;
double xTranslate, yTranslate;
int subgroup;
bool verbose;

void genSteerSpline(point2arr &steer)
{
  steer.clear();
  double phi, trans;

  if( veh->p_actuatorState() == NULL )
    {
      phi = 0.0;
    }
  else
    {
      phi = veh->p_actuatorState()->m_steerpos * VEHICLE_MAX_AVG_STEER;
      trans = veh->p_actuatorState()->m_transcmd;
      if (trans == -1) 
        phi *= -1;
    }
  if(fabs(phi) < 0.0001)
    {
      phi = 0.0001;
    }

  double turningRadius = VEHICLE_WHEELBASE / sin(phi);
  double cn, ce, angle, dangle;
  double lengthArc = VEHICLE_WHEELBASE * 0.5;
  double localYaw = veh->p_vehicleState()->localYaw;
  double screenyaw = localYaw;//M_PI/2.0 - localYaw;
  point2 frontWheels;
  //  frontWheels.x = veh->p_vehicleState()->utmNorthing + cos(screenyaw) *VEHICLE_WHEELBASE;
  //frontWheels.y = veh->p_vehicleState()->utmEasting + sin(screenyaw) *VEHICLE_WHEELBASE;
  frontWheels.x = veh->p_vehicleState()->localX + cos(screenyaw) *VEHICLE_WHEELBASE;
  frontWheels.y = veh->p_vehicleState()->localY + sin(screenyaw) *VEHICLE_WHEELBASE;
  
  // turningRadius has a sign and will make sure the following are correct
  cn = frontWheels.y + turningRadius*sin(M_PI/2.0 -localYaw - phi);
  ce = frontWheels.x  - turningRadius*cos(M_PI/2.0 -localYaw - phi);
  dangle = 2.0 * M_PI * lengthArc / turningRadius / 20.0;
  angle = atan2(frontWheels.x - ce, frontWheels.y - cn);
  
  //  if (useLocal)
  //  steer.push_back (point2(frontWheels.x-xTranslate, frontWheels.y-yTranslate));
  //else
  steer.push_back (point2(frontWheels.x, frontWheels.y));
  
  for(int i=0; i<20; i++)
    {
      angle -= dangle;
      
      //     if (useLocal) {
      //steer.push_back(point2(ce + fabs(turningRadius)*sin(angle)-xTranslate,
			//       cn + fabs(turningRadius)*cos(angle)-yTranslate));
      // }
      //  else {
      steer.push_back(point2(ce + fabs(turningRadius)*sin(angle),
                             cn + fabs(turningRadius)*cos(angle)));
      // }
    }
}

void genSteerCmdSpline(point2arr &steerCmd)
{

  steerCmd.clear();
  double phi;
  
  if( veh->p_actuatorState() == NULL )
    {
      phi = 0.0;
    }
  else
    {
      phi = veh->p_actuatorState()->m_steercmd * VEHICLE_MAX_AVG_STEER;
    }
  if(fabs(phi) < 0.0001)
    {
      phi = 0.0001;
    }
  
  double turningRadius = VEHICLE_WHEELBASE / sin(phi);
  double cn, ce, angle, dangle;
  double lengthArc = VEHICLE_WHEELBASE * 0.5;
  double localYaw = veh->p_vehicleState()->localYaw;
  double screenyaw = localYaw;//M_PI/2.0 - localYaw;
  point2 frontWheels;
  // frontWheels.x = veh->p_vehicleState()->utmNorthing + cos(screenyaw) *VEHICLE_WHEELBASE;
  // frontWheels.y = veh->p_vehicleState()->utmEasting + sin(screenyaw) *VEHICLE_WHEELBASE;
  frontWheels.x = veh->p_vehicleState()->localX + cos(screenyaw) *VEHICLE_WHEELBASE;
  frontWheels.y = veh->p_vehicleState()->localY + sin(screenyaw) *VEHICLE_WHEELBASE;
  
  // turningRadius has a sign and will make sure the following are correct
  cn = frontWheels.y + turningRadius*sin(M_PI/2.0 -localYaw - phi);
  ce = frontWheels.x  - turningRadius*cos(M_PI/2.0 -localYaw - phi);
  dangle = 2.0 * M_PI * lengthArc / turningRadius / 20.0;
  angle = atan2(frontWheels.x - ce, frontWheels.y - cn);
  
  //  if (useLocal)
  //  steerCmd.push_back(point2(frontWheels.x-xTranslate, frontWheels.y-yTranslate));
  // else
  steerCmd.push_back(point2(frontWheels.x, frontWheels.y));
  
  for(int i=0; i<20; i++)
    {
      angle -= dangle;
      
      //   if (useLocal) {
      //steerCmd.push_back(point2(ce + fabs(turningRadius)*sin(angle)-xTranslate,
			//	  cn + fabs(turningRadius)*cos(angle)-yTranslate));
      // }
      // else {
      steerCmd.push_back(point2(ce + fabs(turningRadius)*sin(angle),
                                cn + fabs(turningRadius)*cos(angle)));
      // }
      
    }
}

void RDDFtoMapElement(RDDFVector &vect, point2arr &rddf)
{
  rddf.clear();

  point2 basept;
  point2 circlept;
  point2 delta(xTranslate,yTranslate);
  int i;
  int circlenumpts = 16;
  // store each waypoint
  for (vector<RDDFData>::iterator it = vect.begin();
       it != vect.end(); it++) {
    
    // store the waypoint info 
    double a = cos(M_PI/4);
    double r = it->radius;
    double x,y;
    x = it->Northing;
    y = it->Easting;

    basept.set(x,y);
    if (useLocal)
      basept = basept-delta;
    
    
    for (i=0; i < circlenumpts; ++i){
      a = i*2*M_PI/(circlenumpts-1);
      circlept.set(r*cos(a),r*sin(a));
      rddf.push_back(basept+circlept);
    }


    // draw a circle around the waypoint
    //    rddf.push_back(point2(r+x,0+y));
    // rddf.push_back(point2(r*a+x,r*a+y));
    // rddf.push_back(point2(0+x,r+y));
    // rddf.push_back(point2(-r*a+x,r*a+y));
    // rddf.push_back(point2(-r+x,0+y));
    // rddf.push_back(point2(-r*a+x,-r*a+y));
    // rddf.push_back(point2(0+x,-r+y));
    // rddf.push_back(point2(r*a+x,-r*a+y));
    // rddf.push_back(point2(r+x,0+y));
  }
}

void polytopeToMapElement(sendCorr &corr, vector< point2arr > &corrPts)
{

  point2arr tempCorr;
  //tempCorr.push_back(point2(0,0));

  for (vector<sendPoly>::iterator it = corr.begin();
       it != corr.end(); it++) {

    corrPts.push_back(tempCorr);
    tempCorr.clear();
    if (verbose)    cout<<"next poly:"<<endl;

    for (vector<sendPoint>::iterator it2 = it->begin();
         it2 != it->end(); it2++) {

      corrPts.back().push_back(point2(it2->x,it2->y));
      if (verbose)      cout<<it2->x<<" "<<it2->y<<endl;
    }

    // add 1st point to complete poly
    corrPts.back().push_back(point2(it->front().x,it->front().y));

  }

  // sort the points
  for (unsigned int i=0;i<corrPts.size();++i){
    point2arr tempArr = corrPts.at(i);
    nearHull_2D(tempArr,corrPts.at(i).size(),100,corrPts.at(i));
  }
  
  if (verbose) {
    cout<<"stored pts:"<<endl;
    for (unsigned int i=0;i<corrPts.size();++i){
      cout<<"next poly:"<<endl;
      cout << corrPts[i] <<endl;
    }
  }
}

void paramsToMapElement(OCPparams &params, point2arr &init, point2arr &final)
{

  double n,e;
  double heading;
  int offset;
  double a = cos(M_PI/4);
  double r = 1;
  point2 basept;
  point2 circlept;
  point2 delta(xTranslate,yTranslate);
  int circlenumpts = 16;

  // clear the old vectors
  init.clear();
  final.clear();

  // initial condition: a bar (parallel to heading) with a circle around it
  n = params.initialConditionLB[NORTHING_IDX_C];
  e = params.initialConditionLB[EASTING_IDX_C];
  // heading from -pi to pi, 0 at x axis (north)
  heading = params.initialConditionLB[HEADING_IDX_C];
  basept.set(n,e);
  if (useLocal)
    basept = basept-delta;
  
  // offset to start the circle so we can see the heading
  offset = heading/(2*M_PI)*(circlenumpts-1);

  // draw a circle
  for (int i=offset; i < circlenumpts+offset; i++){
    a = i*2*M_PI/(circlenumpts-1);
    circlept.set(r*cos(a),r*sin(a));
    init.push_back(basept+circlept);
  }

  // draw a bar parallel to heading
  circlept.set(r*cos(heading),r*sin(heading));
  init.push_back(basept+circlept);
  circlept.set(r*cos(heading+M_PI),r*sin(heading+M_PI));
  init.push_back(basept+circlept);
  

  // final condition: an arrow (heading) with a circle around it
  n = params.finalConditionLB[NORTHING_IDX_C];
  e = params.finalConditionLB[EASTING_IDX_C];
  // heading from -pi to pi, 0 at x axis (north)
  heading = params.finalConditionLB[HEADING_IDX_C];
  basept.set(n,e);
  if (useLocal)
    basept = basept-delta;

  // offset to start the circle so we can see the heading
  offset = heading/(2*M_PI)*(circlenumpts-1);

  // draw a circle
  for (int i=offset; i < circlenumpts + offset; i++){
    a = i*2*M_PI/(circlenumpts-1);
    circlept.set(r*cos(a),r*sin(a));
    final.push_back(basept+circlept);
  }
    
  // draw an arrow
  circlept.set(r*cos(heading),r*sin(heading));
  final.push_back(basept+circlept);
  circlept.set(r*cos(heading+M_PI/2),r*sin(heading+M_PI/2));
  final.push_back(basept+circlept);
  circlept.set(r*cos(M_PI/2-heading),r*sin(-M_PI/2-heading));
  final.push_back(basept+circlept);
  circlept.set(r*cos(heading),r*sin(heading));
  final.push_back(basept+circlept);
  circlept.set(r*cos(heading+M_PI),r*sin(heading+M_PI));
  final.push_back(basept+circlept);

}

/* **************************
**** MAIN *******************
****************************/

int main(int argc, char **argv)
{

  /* ***************************
 *** GET COMMAND LINE OPTS ***
 * ***************************/

  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);

  /* Parse command line objects */
  int snkey = skynet_findkey(argc, argv);

  if (cmdline.send_subgroup_given)
    subgroup = cmdline.send_subgroup_arg;
  else
    subgroup = -2;
  cout<<"Sending mapviewer messages on subgroup "<<subgroup<<endl;
  cout<<"To visualize run 'mapviewer --recv-subgroup="<<subgroup << "'"<<endl;
  // set local/global settings
  useLocal = true;
  useLocal = cmdline.use_local_flag;
  
  useTransform = true;
  useTransform = !cmdline.disable_site_transform_flag;
  //  if (useLocal)
  cout<<"Using local coords"<<endl;
  //  else
  //    cout<<"Using global coords"<<endl;

  bool showRDDF = !(cmdline.no_rddf_flag);
  bool showPolys = !(cmdline.no_polytopes_flag);
  if (showRDDF)
    cout<<"Showing RDDFs"<<endl;
  else cout<<"Not showing RDDFs"<<endl;
  if (showPolys)
    cout<<"Showing tplanner->dplanner polytopes"<<endl;
  else cout<<"Not showing polytopes"<<endl;

  bool showTrav = !(cmdline.no_trav_flag);
  if (showTrav)
    cout<<"Showing traversed path"<<endl;
  else cout<<"Not showing traversed path"<<endl;

  bool limitTravPath = cmdline.limit_trav_flag;
  int TRAV_LENGTH = cmdline.trav_length_arg;  
  if (limitTravPath)
    cout<<"Limiting trav to length "<<TRAV_LENGTH<<endl;
  int travCounter = 0;
  vector<point2>::iterator travIter;

  verbose = cmdline.verbose_flag;
  if (verbose) cout<<"verbose output"<<endl;

  // ID for sending mapelements
  int baseId = -2;

  /* **********************
**** INITIALIZATION ****
***********************/

  /* Create the vehicle plan object (includes state) */
  veh = new VehiclePlan(snkey);

  /* Create and initialize map talker */
  CMapElementTalker mapTalker;
  mapTalker.initSendMapElement(snkey);

  // map element for rddf
  MapElement rddfObj;
  rddfBound.push_back(point2(0,0));
  rddfObj.setTypeRDDF();
  rddfObj.setId(baseId,2);

  /* Set up the RDDF for viewing */
  RDDFVector rddf_vector;
  DGCstartMemberFunctionThreadWithArg(veh, &VehiclePlan::getRDDFThread,
                                      (void *) &rddf_vector);

  /* Create map element of ocpspecs init and final conditions */
  MapElement initCondObj, finalCondObj;
  initCondObj.setTypeInitCond();
  finalCondObj.setTypeFinalCond();
  initCondObj.setId(baseId,20,1);
  finalCondObj.setId(baseId,20,2);
  initCond.push_back(emptyPoint);
  finalCond.push_back(emptyPoint);

  /* Sett up skynet talker for init and final conds */
  OCPparams m_OCPparams;    
  DGCstartMemberFunctionThreadWithArg(veh, &VehiclePlan::getOCPparamsThread,
                                      (void *) &m_OCPparams);

  // map element for planning traj
  MapElement planTrajObj;
  planTraj.push_back(point2(0,0));
  planTrajObj.setTypePlanningTraj();
  planTrajObj.setId(baseId,5);

  /* Set up the planning traj for viewing */
  CTraj *planTrajData = new CTraj(3);
  pthread_mutex_t path;
  DGCcreateMutex(&path);
  struct trajArgument arg;
  arg.pathType = SNtraj;		
  arg.pathSource = SNplanner;
  arg.points = planTrajData;
  arg.pathMutex = &path;
  DGCstartMemberFunctionThreadWithArg(veh, &VehiclePlan::trajThread, 
                                      (void *) &arg);

  // map element for dplanner polytope
  MapElement polyCorrObj;
  polyCorrObj.setTypePolytope();
  polyCorrObj.setId(baseId,10);

  /* Set up the planning polytope  */
  SkynetTalker<sendCorr> polyCorrTalker 
    = SkynetTalker<sendCorr>(snkey, SNpolyCorridor, MODdynamicplanner);
  sendCorr dplanCorridor;
  numPolys = 0;
  //  pthread_mutex_t m_specsDataMutex;
  //  DGCcreateMutex(&m_specsDataMutex);

  /* Create map element for steering spline */
  MapElement steerObj;
  steerObj.setTypeSteerCmd();
  steerObj.setId(baseId,3);
  steerSpline.push_back(point2(0,0));

  /* Create map element for commanded steering spline */
  MapElement steerCmdObj;
  steerCmdObj.setTypeSteer();
  steerCmdObj.setId(baseId,4);
  steerCmdSpline.push_back(point2(0,0));

  

  /* Create map element for alice */
  MapElement alice;
  alice.setTypeAlice();

  /* Get the first state data */
  veh->UpdateState();
  point2 alicePos = point2(veh->p_vehicleState()->localX,
                           veh->p_vehicleState()->localY);
  xTranslate = veh->p_vehicleState()->utmNorthing - veh->p_vehicleState()->localX;
  yTranslate = veh->p_vehicleState()->utmEasting - veh->p_vehicleState()->localY;
  point2 delta(xTranslate,yTranslate);

  /* Create a traversed path(s) */
  point2arr travPath, travPathOld;
  MapElement travPathObj, travPathOldObj;
  int travPathObjId = 0;
  point2 lastpoint;

  travPathObj.setTypeTravPath();
  travPathOldObj.setTypeTravPath();

  if (limitTravPath) {
    
    for (int i = 0; i <= TRAV_LENGTH/2; i++)
      travPathOld.push_back(alicePos);
    
    travPathObj.setId(baseId,1,0);
    travPathOldObj.setId(baseId,1,1);
    
  }
  else {
    
    travPathObj.setId(baseId,1,travPathObjId);
  }


  point2 loc2site;
  double loc2siteYaw;
  /* *********************
***** UPDATE LOOP *****
**********************/

  /* The main loop reads the vehicle state and displays the position */
  while (1) {
    //Adding delay to reduce computation time and network traffic.  
    //Won't need to visualize at more than 20Hz.
    usleep(50000);
    veh->WaitForNewState();
    veh->UpdateState();
    veh->UpdateActuatorState();
   
    // send alice object to mapviewer
    alice.setState(*(veh->p_vehicleState()));
    mapTalker.sendMapElement(&alice,subgroup);

    // update the traversed path
    alicePos.x = veh->p_vehicleState()->localX;
    alicePos.y = veh->p_vehicleState()->localY;
    // want to space out the points so the vector isn't huge
    
    loc2site.x = veh->p_vehicleState()->siteNorthing-alicePos.x;
    loc2site.y = veh->p_vehicleState()->siteEasting-alicePos.y;

    
    // store the traversed path
    if ((alicePos - lastpoint).norm() >= 1) {

      lastpoint = alicePos;

      if (limitTravPath) {

        if (travCounter > TRAV_LENGTH/2) {

          // set new traj to old
          travCounter = 0;
          travPathOld.clear();
          travPathOld.push_back(lastpoint);	  
          for (int i = travPath.size()-1; i >=0; i--)
            travPathOld.push_back(travPath.arr.at(i));	  
          travPath.clear();

        }
        // store new point
        travPath.push_back(lastpoint);
        travCounter++;
        travPathOld.pop_back();

        // send the traversed path
        travPathObj.setGeometry(travPath);
        travPathOldObj.setGeometry(travPathOld);
        if (showTrav) {
          if (useTransform){
            travPathObj.translate(loc2site);
            travPathOldObj.translate(loc2site);
          }
          mapTalker.sendMapElement(&travPathObj,subgroup);
          mapTalker.sendMapElement(&travPathOldObj,subgroup);
        }

      }
      
      // not limiting trav
      else  {
	
        if (travPath.size()>200) {
          travPathObjId++;
          travPathObjId = travPathObjId%250; //limit total number of path segments
          travPathObj.setId(baseId,1,travPathObjId);
          lastpoint = travPath.back();
          travPath.clear();
          travPath.push_back(lastpoint);
        }
	
        travPath.push_back(alicePos);
        // send the traversed path
        travPathObj.setGeometry(travPath);
        if (showTrav){
          if (useTransform){
            travPathObj.translate(loc2site);
          }
          mapTalker.sendMapElement(&travPathObj,subgroup);
        }
      }
    }
    /*
      else {
      if (travPath.size()>0){
      travPath[travPath.size()-1]=alicePos;    //sets end of travPath to current point
      travPathObj.setGeometry(travPath);
      if (showTrav)
      mapTalker.sendMapElement(&travPathObj,subgroup);
      }
      }
    */
    // store the translation
    xTranslate = veh->p_vehicleState()->utmNorthing - veh->p_vehicleState()->localX;
    yTranslate = veh->p_vehicleState()->utmEasting - veh->p_vehicleState()->localY;

    
    // mapviewer can only handle vectors of a certain size
    if (travPath.size() > 2000) {
      cout<<"Traversed path is too long, resetting."<<endl;
      travPath.clear();
    }

    // update the steering spline
    genSteerSpline(steerSpline);
    steerObj.setGeometry(steerSpline);

    if (useTransform){
      steerObj.translate(loc2site);
    }
 
    mapTalker.sendMapElement(&steerObj,subgroup);

    // update the commanded steering spline
    genSteerCmdSpline(steerCmdSpline);
 
    steerCmdObj.setGeometry(steerCmdSpline);
    if (useTransform){
      steerCmdObj.translate(loc2site);
    }
    mapTalker.sendMapElement(&steerCmdObj,subgroup);


    // update the planning traj
    if (planTrajData->getNumPoints()!=0) {
      planTraj.clear();
    
      for (int i=0; i<planTrajData->getNumPoints(); i++) {
        planTraj.push_back(point2(planTrajData->getNorthing(i),
                                  planTrajData->getEasting(i)));
      }
    }
    
    if (useLocal)
      planTraj = planTraj-delta;
    

    planTrajObj.setGeometry(planTraj);
    if (useTransform){
      planTrajObj.translate(loc2site);
    }
    mapTalker.sendMapElement(&planTrajObj,subgroup);


    // Check for new polytope 
    if (polyCorrTalker.hasNewMessage() && showPolys) {
      if (verbose) 
        cout<<"New polytope"<<endl; 

      numPolys = polyCorr.size();
      
      // clear the old polytopes
      //      for(unsigned int i = 0; i<dplanCorridor.size(); i++)
      //      	dplanCorridor[i].clear();
      dplanCorridor.clear();
      //      for (vector< point2arr >::iterator it = polyCorr.begin();
      //	   it != polyCorr.end(); it++) 
      //	it->clear();
      polyCorr.clear();

      // get the new corridor
      polyCorrTalker.receive(&dplanCorridor);
      //      DGClockMutex(&m_specsDataMutex);
      
      // clear old polys
      if (dplanCorridor.size() < numPolys) {
        if (verbose) {
          cout<<"dplanncorridor size: "<<dplanCorridor.size()<<endl;
          cout<<"numpolys: "<<numPolys<<endl;
        }
        for (int i = 0; i<numPolys; i++) {
          polyCorrObj.setGeometry(emptyPoint);
          polyCorrObj.setId(baseId,10+i);
          if (useTransform){
            polyCorrObj.translate(loc2site);
          }

          mapTalker.sendMapElement(&polyCorrObj,subgroup);
        }
      }
      
      // convert polytopes
      polytopeToMapElement(dplanCorridor,polyCorr);
      // send the polytopes to mapviewer
      for (unsigned int i = 0; i< polyCorr.size(); i++) {
        polyCorrObj.setGeometry(polyCorr.at(i));
        polyCorrObj.setId(baseId,10+i);
        if (useTransform){
          polyCorrObj.translate(loc2site);
        }
        mapTalker.sendMapElement(&polyCorrObj,subgroup);
      }
      
      //      DGCunlockMutex(&m_specsDataMutex);
    }
    
    /* Check for new init and final conds */
    if (veh->newOCPparams) {
      
      if (verbose) cout<<"new ocpparams"<<endl;

      // convert the params
      paramsToMapElement(m_OCPparams,initCond,finalCond);
      initCondObj.setGeometry(initCond);
      finalCondObj.setGeometry(finalCond);
      if (useTransform){
        initCondObj.translate(loc2site);
        finalCondObj.translate(loc2site);
      }
      mapTalker.sendMapElement(&initCondObj,subgroup);
      mapTalker.sendMapElement(&finalCondObj,subgroup);

      veh->newOCPparams = 0;

    }
  

    /* Check to see if we got a new RDDF */
    if (veh->newRDDF && showRDDF)  {
      if (verbose) {
        cout<< "new rddf"<<endl;
        cout<<"xtranslate "<<xTranslate<<endl;
        cout<<"ytranslate "<<yTranslate<<endl;
      }

      // send the RDDF to mapviewer
      RDDFtoMapElement(rddf_vector,rddfBound);
      rddfObj.setGeometry(rddfBound);
      if (useTransform){
        rddfObj.translate(loc2site);
      }
      mapTalker.sendMapElement(&rddfObj,subgroup);
      //      mdt->get_map_config()->set_rddf(rddf_vector);
      veh->newRDDF = 0;
    }


  }

  // note: these lines are never reached because this program is
  // always killed. need to figure out a way to exit cleanly
  delete veh;
  delete planTrajData;
  cout<<"done"<<endl;
  return 0;
}
