/*!
 * \file planviewer.cc
 * \brief Simple viewer for planner stack
 *
 * \author Richard M. Murray
 * \date 4 March 2007
 *
 * This program is a simple viewer that displays information sent
 * between the various planners.  It is a "quick fix" to allow us to
 * see what is going on until we can develop a better graphical
 * interface.
 *
 */

#include "VehiclePlan.hh"
#include "mapdisplay/MapDisplay.hh"
#include "mapdisplay/MapConfig.hh"
#include "dgcutils/DGCutils.hh"

VehiclePlan *veh;		///< Vehicle plan (and state)
MapDisplayThread *mdt;		///< Map display thread

/* Define the trajectories that we want to read */
struct trajEntry {
  int type;			// trajectory type
  int source;			// trajectory source
  RGBA_COLOR color;		// color to use
} trajData[] = {
  {0, 0, RGBA_COLOR(1.0, 0.0, 0.0)},		   // path history
  {SNtraj, SNplanner, RGBA_COLOR(1.0, 0.0, 1.0)}   // commanded path
};
#define NUMPATHS (sizeof(trajData)/sizeof(trajEntry))
#define HISTORY_PATH 0		// path to use for storing history

/* Allocate space for storing the paths */
PATH paths[NUMPATHS];

int main(int argc, char **argv)
{
  /* Parse command line objects */
  int snkey = skynet_findkey(argc, argv);

  /* Create the vehicle plan object (includes state) */
  veh = new VehiclePlan(snkey);

  /* Create the map display thread object */
  mdt = new MapDisplayThread("PlanViewer", new MapConfig);

  /* Set up pointers to the vehicle information the display needs */
  mdt->get_map_config()->set_state_struct(veh->p_vehicleState());
  mdt->get_map_config()->set_actuator_state_struct(veh->p_actuatorState());

  /* Set up the cmap that we will use */
  pthread_mutex_t pv_cmap_mutex; DGCcreateMutex(&pv_cmap_mutex);
  CMapPlus pv_cmap; pv_cmap.initMap(0, 0, 10, 10, 1.0, 1.0, 0);
  int costID = pv_cmap.addLayer<double>(-11, -12);
  mdt->get_map_config()->set_cmap(&pv_cmap, &pv_cmap_mutex);

  /* Start the thread that displays the gui */
  mdt->guithread_start_thread();

  /* Set up the RDDF for viewing */
  RDDFVector rddf_vector;
  mdt->get_map_config()->set_rddf(rddf_vector);
  DGCstartMemberFunctionThreadWithArg(veh, &VehiclePlan::getRDDFThread,
				      (void *) &rddf_vector);

  /* Start up the threads for trajectories; path 0 is history */
  for (unsigned i = 0; i < NUMPATHS; ++i) {
    /* Initialize the paths data structure */
    paths[i].color = trajData[i].color;
    DGCcreateMutex(&paths[i].pathMutex);

    /* Now call trajThread to read in the data */
    if (i > 0) {
      struct trajArgument arg;
      arg.pathType = trajData[i].type;		
      arg.pathSource = trajData[i].source;
      arg.points = &paths[i].points;
      arg.pathMutex = &paths[i].pathMutex;
      DGCstartMemberFunctionThreadWithArg(veh, &VehiclePlan::trajThread, 
					  (void *) &arg);
    }
  }
  mdt->get_map_config()->set_paths(paths, NUMPATHS);

  /* The main loop reads the vehicle state and displays the position */
  while (1) {
    veh->WaitForNewState();
    veh->UpdateState();
    pv_cmap.updateVehicleLoc(veh->p_vehicleState()->utmNorthing, 
			     veh->p_vehicleState()->utmNorthing);

    /* Update the path history */
    int num_points;
    if((num_points = paths[HISTORY_PATH].points.getNumPoints()) > 
       TRAJ_MAX_LEN-2 ) {
      paths[HISTORY_PATH].points.shiftNoDiffs(1);
    }
    paths[HISTORY_PATH].points.inputNoDiffs(veh->p_vehicleState()->utmNorthing, 
					    veh->p_vehicleState()->utmEasting);

    /* Check to see if we got a new RDDF */
    if (veh->newRDDF)  {
      mdt->get_map_config()->set_rddf(rddf_vector);
      veh->newRDDF = 0;
    }
    
    /* Update the display */
    mdt->notify_update();
  }
  return 0;
}
