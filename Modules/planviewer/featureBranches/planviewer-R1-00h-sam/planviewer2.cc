/*!
 * \file planviewer2.cc
 * \brief Converts planner stack data to MapElements and sends to mapviewer
 * for visualization
 *
 */

#include "VehiclePlan.hh"
#include "map/MapElement.hh"
#include "map/MapElementTalker.hh"
#include "cmdline.h"
#include "alice/AliceConstants.h"

VehiclePlan *veh;	///< Vehicle plan (and state)
vector<point2> rddfBound;
vector<point2> steerCmdSpline;
vector<point2> steerSpline;
bool useLocal;
double xTranslate, yTranslate;
int subgroup;

void genSteerSpline(vector<point2> &steer)
{
  steer.clear();
  double phi;

 if( veh->p_actuatorState() == NULL )
    {
      phi = 0.0;
    }
  else
    {
      phi = veh->p_actuatorState()->m_steerpos * VEHICLE_MAX_AVG_STEER;
    }
  if(fabs(phi) < 0.0001)
    {
      phi = 0.0001;
    }
  
  double turningRadius = VEHICLE_WHEELBASE / sin(phi);
  double cn, ce, angle, dangle;
  double lengthArc = VEHICLE_WHEELBASE * 0.5;
  double localYaw = veh->p_vehicleState()->localYaw;
  double screenyaw = localYaw;//M_PI/2.0 - localYaw;
  point2 frontWheels;
  frontWheels.x = veh->p_vehicleState()->utmNorthing + cos(screenyaw) *VEHICLE_WHEELBASE;
  frontWheels.y = veh->p_vehicleState()->utmEasting + sin(screenyaw) *VEHICLE_WHEELBASE;
  
  // turningRadius has a sign and will make sure the following are correct
  cn = frontWheels.y + turningRadius*sin(M_PI/2.0 -localYaw - phi);
  ce = frontWheels.x  - turningRadius*cos(M_PI/2.0 -localYaw - phi);
  dangle = 2.0 * M_PI * lengthArc / turningRadius / 20.0;
  angle = atan2(frontWheels.x - ce, frontWheels.y - cn);
  
  if (useLocal)
    steer.push_back (point2(frontWheels.x-xTranslate, frontWheels.y-yTranslate));
  else
    steer.push_back (point2(frontWheels.x, frontWheels.y));
  
  for(int i=0; i<20; i++)
    {
      angle -= dangle;
      
      if (useLocal) {
	steer.push_back(point2(ce + fabs(turningRadius)*sin(angle)-xTranslate,
			       cn + fabs(turningRadius)*cos(angle)-yTranslate));
      }
      else {
	steer.push_back(point2(ce + fabs(turningRadius)*sin(angle),
			       cn + fabs(turningRadius)*cos(angle)));
      }
    }
}

void genSteerCmdSpline(vector<point2> &steerCmd)
{

  steerCmd.clear();
  double phi;
  
  if( veh->p_actuatorState() == NULL )
    {
      phi = 0.0;
    }
  else
    {
      phi = veh->p_actuatorState()->m_steercmd * VEHICLE_MAX_AVG_STEER;
    }
  if(fabs(phi) < 0.0001)
    {
      phi = 0.0001;
    }
  
  double turningRadius = VEHICLE_WHEELBASE / sin(phi);
  double cn, ce, angle, dangle;
  double lengthArc = VEHICLE_WHEELBASE * 0.5;
  double localYaw = veh->p_vehicleState()->localYaw;
  double screenyaw = localYaw;//M_PI/2.0 - localYaw;
  point2 frontWheels;
  frontWheels.x = veh->p_vehicleState()->utmNorthing + cos(screenyaw) *VEHICLE_WHEELBASE;
  frontWheels.y = veh->p_vehicleState()->utmEasting + sin(screenyaw) *VEHICLE_WHEELBASE;
  
  // turningRadius has a sign and will make sure the following are correct
  cn = frontWheels.y + turningRadius*sin(M_PI/2.0 -localYaw - phi);
  ce = frontWheels.x  - turningRadius*cos(M_PI/2.0 -localYaw - phi);
  dangle = 2.0 * M_PI * lengthArc / turningRadius / 20.0;
  angle = atan2(frontWheels.x - ce, frontWheels.y - cn);
  
  if (useLocal)
    steerCmd.push_back(point2(frontWheels.x-xTranslate, frontWheels.y-yTranslate));
  else
    steerCmd.push_back(point2(frontWheels.x, frontWheels.y));
  
  for(int i=0; i<20; i++)
    {
      angle -= dangle;
      
      if (useLocal) {
	steerCmd.push_back(point2(ce + fabs(turningRadius)*sin(angle)-xTranslate,
				  cn + fabs(turningRadius)*cos(angle)-yTranslate));
      }
      else {
	steerCmd.push_back(point2(ce + fabs(turningRadius)*sin(angle),
				  cn + fabs(turningRadius)*cos(angle)));
      }
      
    }
}

void RDDFtoMapElement(RDDFVector &vect, vector<point2> &rddf)
{
  rddf.clear();

  // store each waypoint
  for (vector<RDDFData>::iterator it = vect.begin();
       it != vect.end(); it++) {
    
    // store the waypoint info 
    double a = cos(M_PI/4);
    double r = it->radius;
    double x,y;
    if (useLocal) {
      x = it->Northing;
      y = it->Easting;
    }
    else {
      x = it->Northing - xTranslate;
      y = it->Easting - yTranslate;
    }
    
    // draw a circle around the waypoint
    rddf.push_back(point2(r+x,0+y));
    rddf.push_back(point2(r*a+x,r*a+y));
    rddf.push_back(point2(0+x,r+y));
    rddf.push_back(point2(-r*a+x,r*a+y));
    rddf.push_back(point2(-r+x,0+y));
    rddf.push_back(point2(-r*a+x,-r*a+y));
    rddf.push_back(point2(0+x,-r+y));
    rddf.push_back(point2(r*a+x,-r*a+y));
    rddf.push_back(point2(r+x,0+y));
  }
}

int main(int argc, char **argv)
{
  /* Process command line options */
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);

  /* Parse command line objects */
  int snkey = skynet_findkey(argc, argv);

  if (cmdline.send_subgroup_given)
    subgroup = cmdline.send_subgroup_arg;
  else
    subgroup = 0;
  cout<<"Sending messages on subgroup "<<subgroup<<endl;

  if (cmdline.use_local_given)
    useLocal = cmdline.use_local_flag;
  else
    useLocal = true;
  if (useLocal)
    cout<<"Using local coords (default)"<<endl;
  else
    cout<<"Using global coords"<<endl;
  

  /* Create the vehicle plan object (includes state) */
  veh = new VehiclePlan(snkey);

  /* Create map talker */
  CMapElementTalker mapTalker;

  /* Decide whether to use local or global coordinates */
  useLocal = cmdline.use_local_flag;

  /* Set up the RDDF for viewing */
  RDDFVector rddf_vector;
  DGCstartMemberFunctionThreadWithArg(veh, &VehiclePlan::getRDDFThread,
				      (void *) &rddf_vector);
  // map element for rddf
  MapElement rddfObj;
  rddfBound.push_back(point2(0,0));
  rddfObj.setTypeRDDF();
  rddfObj.setId(2);

  /* Create map element for steering spline */
  MapElement steerObj;
  steerObj.setTypeSteerCmd();
  steerObj.setId(3);
  steerSpline.push_back(point2(0,0));

  /* Create map element for commanded steering spline */
  MapElement steerCmdObj;
  steerCmdObj.setTypeSteer();
  steerCmdObj.setId(4);
  steerCmdSpline.push_back(point2(0,0));

  /* Create map element for alice */
  MapElement alice;
  alice.setTypeAlice();

  /* Get the first state data */
  veh->UpdateState();
  point2 alicePos = point2(veh->p_vehicleState()->localX,
			   veh->p_vehicleState()->localY);
  xTranslate = veh->p_vehicleState()->utmNorthing - veh->p_vehicleState()->localX;
  yTranslate = veh->p_vehicleState()->utmEasting - veh->p_vehicleState()->localY;

  /* Create a traversed path(s) */
  vector<point2> travPath;
  travPath.push_back(alicePos);
  // map element for traversed path
  MapElement travPathObj;
  travPathObj.setTypeTravPath();
  travPathObj.setId(1);

  /* The main loop reads the vehicle state and displays the position */
  while (1) {
    veh->WaitForNewState();
    veh->UpdateState();
    veh->UpdateActuatorState();

    // send alice object to mapviewer
    alice.setState(*(veh->p_vehicleState()));
    mapTalker.initSendMapElement(snkey);
    mapTalker.sendMapElement(&alice,subgroup);

    // update the traversed path
    alicePos.x = veh->p_vehicleState()->localX;
    alicePos.y = veh->p_vehicleState()->localY;
    // want to space out the points so the vector isn't huge
    if ((alicePos - travPath.back()).norm() >= 1) {
      travPath.push_back(alicePos);
      // send the traversed path
      travPathObj.setGeometry(travPath);
      mapTalker.initSendMapElement(snkey);
      mapTalker.sendMapElement(&travPathObj,subgroup);
      
      xTranslate = veh->p_vehicleState()->utmNorthing - veh->p_vehicleState()->localX;
      yTranslate = veh->p_vehicleState()->utmEasting - veh->p_vehicleState()->localY;
    }
    // mapviewer can only handle vectors of a certain size
    if (travPath.size() > 2000) {
      cout<<"Traversed path is too long, resetting."<<endl;
      travPath.clear();
    }

    // update the steering spline
    genSteerSpline(steerSpline);
    steerObj.setGeometry(steerSpline);
    mapTalker.initSendMapElement(snkey);
    mapTalker.sendMapElement(&steerObj,subgroup);

    // update the commanded steering spline
    genSteerCmdSpline(steerCmdSpline);
    steerCmdObj.setGeometry(steerCmdSpline);
    mapTalker.initSendMapElement(snkey);
    mapTalker.sendMapElement(&steerCmdObj,subgroup);

    /* Check to see if we got a new RDDF */
    if (veh->newRDDF)  {
      cout<< "new rddf (planviewer)"<<endl;
      cout<<"xtranslate "<<xTranslate<<endl;
      cout<<"ytranslate "<<yTranslate<<endl;

      // send the RDDF to mapviewer
      RDDFtoMapElement(rddf_vector,rddfBound);
      rddfObj.setGeometry(rddfBound);
      mapTalker.initSendMapElement(snkey);
      mapTalker.sendMapElement(&rddfObj,subgroup);
      //      mdt->get_map_config()->set_rddf(rddf_vector);
      veh->newRDDF = 0;
    }
    


  }
  return 0;
}
