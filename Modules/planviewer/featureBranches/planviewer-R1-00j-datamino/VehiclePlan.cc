/*!
 * \file VehiclePlan.cc
 * \brief Code for VehiclePlan class
 *
 * \author Richard M. Murray
 * \date 4 March 2007
 *
 */

#include "VehiclePlan.hh"
#include "dgcutils/RDDF.hh"

/* Constructor */
VehiclePlan::VehiclePlan(int snkey)
  : CSkynetContainer(SNGui, snkey, NULL),
    CStateClient(true), CRDDFTalker(true)
{
  newRDDF = 0;			// start with no RDDF
}

void VehiclePlan::trajThread(void *arg)
{
  struct trajArgument *src = (struct trajArgument *) arg;

  /* Figure out which skynet channel to listen on */
  int trajSocket = m_skynet.listen(src->pathType, src->pathSource);

  cerr << "trajThread started: type = " << src->pathType << endl;
  while (1) {
    /* Wait until we get a traj and then read it */
    WaitForTrajData(trajSocket);
    RecvTraj(trajSocket, src->points, src->pathMutex);

    /* Display will be updated when we get a new state */
  }
}

void VehiclePlan::getRDDFThread(void *arg)
{
  /* Our argument tells us where to look */
  RDDFVector *rddf_vector = (RDDFVector *) arg;

  /* Loop and read in the RDDF as it arrives */
  while (true) {
    WaitForRDDF();

#   warning should use mutex here
    *rddf_vector = m_newRddf.getTargetPoints();

    /* Set a flag so that the display program knows we have a new RDDF */
    newRDDF = 1;
  }
}

void VehiclePlan::getMapDeltaThread(void *arg)
{
  struct mapArgument *src = (struct mapArgument *) arg;

  /* Figure out which skynet channel to listen on */
  cerr << "VehiclePlan: listening for type " << src->mapType << endl;
  int mapDeltaSocket = m_skynet.listen(src->mapType, src->mapSource);
  if (mapDeltaSocket < 0) {
    cerr << "VehiclePlan::getMapDeltaThread(): skynet listen return error" 
	 << endl;
  }

  cerr << "getMapDeltaThread started: type = " << src->mapType << endl;
  while (1) {
    int deltasize;

    if (RecvMapdelta(mapDeltaSocket, src->delta, &deltasize)) {
      DGClockMutex(&cmap_mutex);
      cmap.applyDelta<double>(src->layer, src->delta, deltasize);
      DGCunlockMutex(&cmap_mutex);
    }

    /* Display will be updated when we get a new state */
  }
}

