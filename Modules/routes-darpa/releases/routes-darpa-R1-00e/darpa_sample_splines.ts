Environment( 
bottomLeftBound --> Vector(0, 0)
topRightBound --> Vector(100, 100)
xTranslation --> 308883.7755338631
yTranslation --> 4304762.739057675
xInit --> 4305261.988270575
yInit --> 308716.7682095252
nameDirectory --> collection(28) {
Spline : Spline( cacheResolution --> 100
points --> collection(6) {
Vector(-217.0016479492188, 503.6443786621094) ,
Vector(-209.9703979492188, 503.2020568847656) ,
Vector(-92.78848266601562, 506.8853454589844) ,
Vector(18.62957572937012, 513.2578735351562) ,
Vector(101.6264495849609, 520.2835083007812) ,
Vector(109.257698059082, 519.680908203125) ,
 }
numForwardLanes --> 2
numReverseLanes --> 0
cached --> 0
 ) ,
Spline___10 : Spline( cacheResolution --> 100
points --> collection(6) {
Vector(-7.880066394805908, 113.4556655883789) ,
Vector(-7.900295257568359, 108.4557037353516) ,
Vector(-8.000032424926758, 83.80384826660156) ,
Vector(-4.739121913909912, 52.94998168945312) ,
Vector(-6.586532592773438, 37.55850601196289) ,
Vector(-7.182396411895752, 32.59413909912109) ,
 }
numForwardLanes --> 1
numReverseLanes --> 0
cached --> 0
 ) ,
Spline___14 : Spline( cacheResolution --> 100
points --> collection(7) {
Vector(-345.3753662109375, 162.2291107177734) ,
Vector(-334.6503601074219, 162.2291107177734) ,
Vector(-259.21875, 166.3554992675781) ,
Vector(-226.40625, 256.20166015625) ,
Vector(-218.75, 348.8554992675781) ,
Vector(-228.3297729492188, 483.7405395507812) ,
Vector(-228.7003021240234, 495.6925354003906) ,
 }
numForwardLanes --> 1
numReverseLanes --> 0
cached --> 0
 ) ,
Spline___15 : Spline( cacheResolution --> 100
points --> collection(4) {
Vector(-530.46875, 181.095947265625) ,
Vector(-520.625, 181.095947265625) ,
Vector(-369.1003723144531, 163.9595794677734) ,
Vector(-363.671875, 162.8466186523438) ,
 }
numForwardLanes --> 1
numReverseLanes --> 0
cached --> 0
 ) ,
Spline___17 : Spline( cacheResolution --> 100
points --> collection(4) {
Vector(-508.046875, 499.76953125) ,
Vector(-496.015625, 499.76953125) ,
Vector(-239.1002960205078, 502.9605712890625) ,
Vector(-226.4253082275391, 503.6527404785156) ,
 }
numForwardLanes --> 2
numReverseLanes --> 0
cached --> 0
 ) ,
Spline___18 : Spline( cacheResolution --> 100
points --> collection(7) {
Vector(-103.90625, -199.3456726074219) ,
Vector(-118.125, -200.7495269775391) ,
Vector(-219.84375, -192.3264465332031) ,
Vector(-297.5, -71.59567260742188) ,
Vector(-355.46875, 33.69278717041016) ,
Vector(-357.0753479003906, 147.0008697509766) ,
Vector(-354.375, 158.6351013183594) ,
 }
numForwardLanes --> 1
numReverseLanes --> 0
cached --> 0
 ) ,
Spline___19 : Spline( cacheResolution --> 100
points --> collection(4) {
Vector(-249.7109375, -518.3455200195312) ,
Vector(-238.2890625, -518.3455200195312) ,
Vector(-70.21875, -518.90966796875) ,
Vector(-59.375, -520.7918701171875) ,
 }
numForwardLanes --> 1
numReverseLanes --> 1
cached --> 0
 ) ,
Spline___20 : Spline( cacheResolution --> 100
points --> collection(4) {
Vector(104.3671875, -627.4993286132812) ,
Vector(106.171875, -617.5763549804688) ,
Vector(117.5867156982422, -530.136474609375) ,
Vector(118.8780670166016, -526.4259033203125) ,
 }
numForwardLanes --> 1
numReverseLanes --> 1
cached --> 0
 ) ,
Spline___21 : Spline( cacheResolution --> 100
points --> collection(4) {
Vector(10.70619201660156, -193.5892639160156) ,
Vector(18.46251106262207, -196.8456268310547) ,
Vector(103.6119613647461, -196.3270721435547) ,
Vector(110.0546875, -195.5765686035156) ,
 }
numForwardLanes --> 1
numReverseLanes --> 1
cached --> 0
 ) ,
Spline___22 : Spline( cacheResolution --> 100
points --> collection(4) {
Vector(125.5859375, -190.2432250976562) ,
Vector(137.4119567871094, -186.6363830566406) ,
Vector(223.5370483398438, -121.2241744995117) ,
Vector(226.81640625, -118.2431793212891) ,
 }
numForwardLanes --> 1
numReverseLanes --> 1
cached --> 0
 ) ,
Spline___25 : Spline( cacheResolution --> 100
points --> collection(12) {
Vector(241.23828125, -105.9098129272461) ,
Vector(246.78515625, -101.9098129272461) ,
Vector(307.24609375, -61.90980911254883) ,
Vector(348.84765625, -29.90981101989746) ,
Vector(353.28515625, -0.57647705078125) ,
Vector(351.06640625, 29.42352294921875) ,
Vector(349.95703125, 118.7568435668945) ,
Vector(348.84765625, 206.7568511962891) ,
Vector(334.98046875, 231.0901641845703) ,
Vector(303.91796875, 233.7568359375) ,
Vector(233.6121215820312, 232.6592712402344) ,
Vector(225.15234375, 233.7568359375) ,
 }
numForwardLanes --> 1
numReverseLanes --> 1
cached --> 0
 ) ,
Spline___26 : Spline( cacheResolution --> 100
points --> collection(4) {
Vector(211.83984375, 233.7568359375) ,
Vector(202.4121246337891, 234.0436553955078) ,
Vector(133.6745452880859, 233.0054016113281) ,
Vector(128.08203125, 235.0901641845703) ,
 }
numForwardLanes --> 1
numReverseLanes --> 1
cached --> 0
 ) ,
Spline___27 : Spline( cacheResolution --> 100
points --> collection(8) {
Vector(113.3828125, 233.4235534667969) ,
Vector(104.7495422363281, 234.0436859130859) ,
Vector(46.265625, 235.4235534667969) ,
Vector(31.2890625, 234.7568817138672) ,
Vector(22.4140625, 218.7568817138672) ,
Vector(22.4140625, 188.0902099609375) ,
Vector(22.68655967712402, 149.7694549560547) ,
Vector(22.03656005859375, 144.5780181884766) ,
 }
numForwardLanes --> 1
numReverseLanes --> 0
cached --> 0
 ) ,
Spline___28 : Spline( cacheResolution --> 100
points --> collection(4) {
Vector(126.4545745849609, 523.7963256835938) ,
Vector(134.2577056884766, 524.6744995117188) ,
Vector(193.1358337402344, 528.1873168945312) ,
Vector(202.3577728271484, 526.4307861328125) ,
 }
numForwardLanes --> 2
numReverseLanes --> 0
cached --> 0
 ) ,
Spline___29 : Spline( cacheResolution --> 100
points --> collection(4) {
Vector(216.5452728271484, 530.8218383789062) ,
Vector(225.0577697753906, 532.5782470703125) ,
Vector(323.660888671875, 542.2384643554688) ,
Vector(337.848388671875, 544.8731079101562) ,
 }
numForwardLanes --> 2
numReverseLanes --> 0
cached --> 0
 ) ,
Spline___30 : Spline( cacheResolution --> 100
points --> collection(4) {
Vector(118.6513900756836, 509.3058776855469) ,
Vector(118.3997039794922, 504.3448181152344) ,
Vector(119.6995391845703, 251.694580078125) ,
Vector(118.6513900756836, 242.3315277099609) ,
 }
numForwardLanes --> 1
numReverseLanes --> 1
cached --> 0
 ) ,
Spline___31 : Spline( cacheResolution --> 100
points --> collection(6) {
Vector(118.6513824462891, 228.7193908691406) ,
Vector(117.0995407104492, 219.5076446533203) ,
Vector(118.6513977050781, -23.76448440551758) ,
Vector(117.9420318603516, -146.2740936279297) ,
Vector(118.2061920166016, -179.1183166503906) ,
Vector(117.2326583862305, -192.8189697265625) ,
 }
numForwardLanes --> 1
numReverseLanes --> 1
cached --> 0
 ) ,
Spline___32 : Spline( cacheResolution --> 100
points --> collection(8) {
Vector(117.2326583862305, -202.9183044433594) ,
Vector(117.5343170166016, -210.5413970947266) ,
Vector(116.5232849121094, -304.7901000976562) ,
Vector(135.6764068603516, -340.7965087890625) ,
Vector(137.0951538085938, -380.3157348632812) ,
Vector(128.5826873779297, -447.4985656738281) ,
Vector(121.1617202758789, -506.9480285644531) ,
Vector(120.2218170166016, -515.6759033203125) ,
 }
numForwardLanes --> 1
numReverseLanes --> 1
cached --> 0
 ) ,
Spline___33 : Spline( cacheResolution --> 100
points --> collection(6) {
Vector(208.387451171875, 517.6487426757812) ,
Vector(209.07470703125, 505.7292175292969) ,
Vector(211.9342651367188, 405.2381896972656) ,
Vector(216.8998870849609, 271.7510375976562) ,
Vector(218.3186340332031, 248.0394897460938) ,
Vector(217.6092681884766, 232.2317962646484) ,
 }
numForwardLanes --> 1
numReverseLanes --> 1
cached --> 0
 ) ,
Spline___34 : Spline( cacheResolution --> 100
points --> collection(6) {
Vector(218.3186340332031, 226.96240234375) ,
Vector(218.3371276855469, 218.1232452392578) ,
Vector(221.8656158447266, 131.6772155761719) ,
Vector(226.8312683105469, 6.532963275909424) ,
Vector(230.3781433105469, -98.85165405273438) ,
Vector(230.3781433105469, -109.3901138305664) ,
 }
numForwardLanes --> 1
numReverseLanes --> 1
cached --> 0
 ) ,
Spline___35 : Spline( cacheResolution --> 100
points --> collection(6) {
Vector(-47.41099548339844, -457.3774108886719) ,
Vector(-37.50946044921875, -456.6056518554688) ,
Vector(-7.098495483398438, -435.8774108886719) ,
Vector(0.2921295166015625, -381.3004760742188) ,
Vector(3.187510967254639, -207.5746154785156) ,
Vector(2.979629516601562, -196.4831695556641) ,
 }
numForwardLanes --> 1
numReverseLanes --> 0
cached --> 0
 ) ,
Spline___36 : Spline( cacheResolution --> 100
points --> collection(6) {
Vector(2.979629516601562, -191.1082153320312) ,
Vector(2.21251106262207, -184.0400848388672) ,
Vector(3.315567016601562, -39.78131484985352) ,
Vector(1.971817016601562, 6.52637767791748) ,
Vector(-4.612504005432129, 21.88719749450684) ,
Vector(-12.13755798339844, 26.37253189086914) ,
 }
numForwardLanes --> 1
numReverseLanes --> 0
cached --> 0
 ) ,
Spline___37 : Spline( cacheResolution --> 100
points --> collection(4) {
Vector(-12.80943298339844, 27.6128101348877) ,
Vector(-18.18443298339844, 30.92050170898438) ,
Vector(-49.4625129699707, 39.71109771728516) ,
Vector(-55.63751220703125, 39.71109771728516) ,
 }
numForwardLanes --> 1
numReverseLanes --> 0
cached --> 0
 ) ,
Spline___38 : Spline( cacheResolution --> 100
points --> collection(4) {
Vector(-101.4969329833984, 93.7666015625) ,
Vector(-87.72349548339844, 83.43010711669922) ,
Vector(-67.33750915527344, 48.36350250244141) ,
Vector(-62.19224548339844, 41.25703048706055) ,
 }
numForwardLanes --> 0
numReverseLanes --> 1
cached --> 0
 ) ,
Spline___39 : Spline( cacheResolution --> 100
points --> collection(6) {
Vector(-58.88751220703125, 34.51965713500977) ,
Vector(-70.58750915527344, 37.28842544555664) ,
Vector(-97.46568298339844, 7.766604423522949) ,
Vector(-100.1531829833984, -58.38724136352539) ,
Vector(-99.83750915527344, -188.5394287109375) ,
Vector(-99.48130798339844, -194.8295440673828) ,
 }
numForwardLanes --> 2
numReverseLanes --> 0
cached --> 0
 ) ,
Spline___40 : Spline( cacheResolution --> 100
points --> collection(7) {
Vector(-100.1531829833984, -201.8584899902344) ,
Vector(-100.1625061035156, -205.1520385742188) ,
Vector(-100.8250579833984, -356.9064636230469) ,
Vector(-100.1531829833984, -418.9256896972656) ,
Vector(-85.37193298339844, -440.4256896972656) ,
Vector(-66.02508544921875, -452.1825866699219) ,
Vector(-49.76255798339844, -455.310302734375) ,
 }
numForwardLanes --> 2
numReverseLanes --> 0
cached --> 0
 ) ,
Spline___7 : Spline( cacheResolution --> 100
points --> collection(4) {
Vector(-51.38298034667969, -468.0736389160156) ,
Vector(-49.95000076293945, -476.4915466308594) ,
Vector(-53.91571044921875, -506.58642578125) ,
Vector(-51.72111892700195, -514.2140502929688) ,
 }
numForwardLanes --> 1
numReverseLanes --> 1
cached --> 0
 ) ,
Spline___8 : Spline( cacheResolution --> 100
points --> collection(4) {
Vector(115.5186920166016, -520.637451171875) ,
Vector(107.51171875, -516.6387329101562) ,
Vector(-34.34999847412109, -519.7535400390625) ,
Vector(-40.0859375, -519.1724243164062) ,
 }
numForwardLanes --> 1
numReverseLanes --> 1
cached --> 0
 ) ,
 }
nameList --> collection(1) {
Spline : 41 ,
 }
 )