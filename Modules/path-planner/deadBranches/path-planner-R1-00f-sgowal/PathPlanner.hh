#ifndef PATHPLANNER_HH_
#define PATHPLANNER_HH_

#include <frames/pose3.h>
#include <frames/point2.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ActuatorState.h>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <assert.h>
#include <alice/AliceConstants.h>
#include <math.h>
#include <temp-planner-interfaces/AliceStateHelper.hh>
#include <temp-planner-interfaces/Console.hh>


// Maximum value for the plan cost
#define GRAPH_PLAN_COST_MAX 100000000


/// @brief Plan constraints.
///
/// These are the weights used to construct plans.  A negative value
/// inidicates a hard constraint.
struct PathPlannerConstraints
{
  /// Enable driving in reverse.
  bool enableReverse;

  /// Cost multiplier for driving off the lane center-line.
  int centerCost;
  
  /// Cost for driving in an on-coming lane (and risking a head-on
  /// crash). This value should be much larger than laneCost.
  int headOnCost;

  /// Cost for changing lanes.
  int changeCost;  

  /// Cost for drivig off-road
  int offRoadCost;

  /// Cost for driving in reverse.
  int reverseCost;

  /// Cost for driving through a static obstacle.  Can be set to -1 to
  /// denote a hard constraint.
  int obsCost;

  /// Cost for driving through another vehicle. This can be set
  /// to zero to produce queuing behavior at intersections (the
  /// trajectory generation step will set the velocity profile
  /// such that we dont crash into other vehicles).
  int carCost;
};


class PathPlanner {

public: 

  static int init();
  static void destroy();
  static Err_t planPath(Path_t* path, Cost_t& totCost, Graph_t* graph, VehicleState vehState, pose3_t finalPose, Path_params_t pathParams);

  static void display(int sendSubgroup, Path_t* path);
  
private :

  /// @brief Construct a plan for reaching the given checkpoint
  static Err_t makePlan(int checkId, Graph_t* graph);
  /// @brief Construct a plan for reaching the given pose
  static Err_t makePlan(pose3_t pose, Graph_t* graph);
  /// @brief Construct a plan for reaching the given node
  static Err_t makePlan(GraphNode *node, Graph_t* graph);

  /// @brief Construct path.
  /// @param[out] path The planned path, including some meta-data.
  static Err_t makePath(GraphPath *path, Graph_t* graph);

  /// @brief Get the plan constraints
  ///
  /// @param[out] cons Current plan constraints. 
  //  static int getConstraints(PathPlannerConstraints *cons);

  /// @brief Set the plan constraints
  ///
  /// @param[in] cons Current plan constraints. 
  //  static int setConstraints(const PathPlannerConstraints *cons);

  // Push node onto the priority queue
  static Err_t pushNode(GraphNode *node, int planCost);

  static bool isInsideHiResBox(GraphNode *alice, GraphNode *node);
  static bool isAnySrcInsideHiResBox(GraphNode *alice, GraphNode *node);
    
  // Pop node from the priority queue
  static GraphNode *popNode();



  static CMapElementTalker meTalker;
  // Priority queue for Dijkstra
  static int queueLen, queueMax;
  static GraphNode **queue;
  // Plan constraints
  static PathPlannerConstraints cons;
};

#endif /*PATHPLANNER_HH_*/




