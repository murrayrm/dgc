
/* 
 * Desc: Test rail planner
 * Date: 7 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <dgcutils/DGCutils.hh>
#include "RailPlanner.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)

// Common macros
#define MIN(a, b) ((a) <= (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))


// Construct random plan 
int testRandomWaypoints(PlanGraph *pgraph, RailPlanner *planner, int samples)
{
  uint64_t time;
  int i, na, nb;
  RNDFGraph *rndf;
  RNDFGraphWaypoint *wpA, *wpB;
  PlanGraphNode *nodeA, *nodeB;
  PlanGraphPath path;
  
  float numRNDF[2] = {0};
  float meanRNDF[2] = {0};
  float maxRNDF[2] = {0};
  float meanStates[2] = {0};

  rndf = &pgraph->rndf;

  while ((samples--) > 0)
  {
    // Pick an initial point that is not in a zone
    while (true)
    {
      na = (rand() % rndf->numWaypoints);
      wpA = rndf->waypoints + na;
      if (wpA->laneId == 0)
        continue;
      if (wpA->flags.isZoneParking || wpA->flags.isZonePerimeter)
        continue;
      break;
    }
    nodeA = pgraph->getNearestPos(wpA->px, wpA->py, 1.0, PLAN_GRAPH_NODE_LANE);
    assert(nodeA);

    // Update the ROI
    pgraph->updateROI(wpA->px, wpA->py, 128);
    
    for (i = 0; i < 100; i++)
    {
      // Pick a goal that is not a zone perimeter
      while (true)
      {
        nb = (rand() % rndf->numWaypoints);
        if (na == nb)
          continue;    
        wpB = rndf->waypoints + nb;
        if (wpB->flags.isZonePerimeter)
          continue;
        break;
      }
      nodeB = pgraph->getNearestPos(wpB->px, wpB->py, 1,
                                    PLAN_GRAPH_NODE_LANE | PLAN_GRAPH_NODE_ZONE_PARKING);
      assert(nodeB);

      //MSG("start %d.%d.%d goal %d.%d.%d",
      //    wpA->segmentId, wpA->laneId, wpA->waypointId,
      //    wpB->segmentId, wpB->laneId, wpB->waypointId);

      // Run the path-planner with reversing
      time = DGCgettime();
      planner->setMode(true, true);
      //planner->setHeuristic(HEURISTIC_RNDF);
      //planner->setHeuristic(HEURISTIC_L2);      
      planner->planPath(nodeA, PLAN_GRAPH_PATH_FWD, nodeB, &path);
      time = DGCgettime() - time;
      
      numRNDF[planner->goalFound] += 1;
      meanRNDF[planner->goalFound] += time*1e-3;
      maxRNDF[planner->goalFound] = MAX(time*1e-3, maxRNDF[planner->goalFound]);
      meanStates[planner->goalFound] += planner->numSearched;
      
      if (!planner->goalFound)
      {
        MSG("RNDF failed: start %d.%d.%d goal %d.%d.%d",
            wpA->segmentId, wpA->laneId, wpA->waypointId,
            wpB->segmentId, wpB->laneId, wpB->waypointId);
      }
    
      //MSG("success %d nodes %d time %.3fms",
      //    planner->goalFound, planner->numSearched, (double) time * 1e-3);
    }
  }

  MSG("summary stats");
  MSG("RNDF fail %.0f mean %5.1fms max %5.1fms states %5.1f",
      numRNDF[0], meanRNDF[0]/numRNDF[0], maxRNDF[0], meanStates[0]/numRNDF[0]);
  MSG("RNDF pass %.0f mean %5.1fms max %5.1fms states %5.1f",
      numRNDF[1], meanRNDF[1]/numRNDF[1], maxRNDF[1], meanStates[1]/numRNDF[1]);
  
  return 0;
}


// Test some specific cases
int testHardCases(PlanGraph *pgraph, RailPlanner *planner)
{

  // TODO
  
  return 0;
}


int main(int argc, char **argv)
{
  char *filename;
  int samples;
  PlanGraph graph;
  RailPlanner planner(&graph);
  
  // Parse command line
  filename = NULL;
  samples = 1;
  if (argc > 1)
    filename = argv[1];
  if (!filename)
    return fprintf(stderr, "usage: %s <FILE.RNDF.PG>\n", argv[0]);
  if (argc > 2)
    samples = atoi(argv[2]);
  
  // Load compiled plan graph
  MSG("loading %s", filename);
  if (graph.load(filename) != 0)
    return -1;

  MSG("graph nodes %d", graph.nodeCount);

  // Tricky cases
  testHardCases(&graph, &planner);

  // Allow reverse
  planner.setMode(false, true);
  
  // Run some random goal tests
  testRandomWaypoints(&graph, &planner, samples);
  
  return 0;
}
