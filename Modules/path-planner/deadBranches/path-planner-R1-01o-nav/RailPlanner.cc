/* 
 * Desc: Plan paths on a graph.
 * Date: 07 October 2007
 * Author: Sven Gowal, Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <errno.h>
#include <float.h>
#include <stdio.h>

#include <temp-planner-interfaces/ConfigFile.hh>

#include "RailPlanner.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)

// Common macros
#ifndef MIN
#define MIN(a, b) ((a) <= (b) ? (a) : (b))
#endif
#ifndef MAX
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#endif

// Distance between two nodes
#define DISTANCE(a, b) \
  sqrtf( ((a)->pose.pos.x - (b)->pose.pos.x) * ((a)->pose.pos.x - (b)->pose.pos.x) + \
         ((a)->pose.pos.y - (b)->pose.pos.y) * ((a)->pose.pos.y - (b)->pose.pos.y) )


// Default constructor
RailPlanner::RailPlanner(PlanGraph *graph)
{
  this->graph = graph;

  // Initialize weights
  memset(&this->weights, 0, sizeof(this->weights));

  this->heuristic = HEURISTIC_RNDF;

  this->speed = 0;
  
  this->statusFunc = NULL;
  this->statusData = NULL;

  this->corridor = NULL;
  this->laneChangeHardConstraint = true;
  this->allowLaneChange = false;
  this->allowReverse = false;
  this->allowVehicle = true;

  this->goalRadius = 2.0;
  this->goalDistOnly = false;
  
  return;
}


// Destructor
RailPlanner::~RailPlanner()
{
  // TODO Clean up
  
  return;
}


// Load the weights from a file
int RailPlanner::loadWeights(const char *filename)
{
  FILE * file;

  if (!filename)
    return -1;
  
  // Check for file existance before opening 
  file = fopen(filename, "r");
  if (file == NULL)
    return MSG("unable to load config file %s : %s", filename, strerror(errno));
  fclose(file);
  
  ConfigFile config(filename);
  config.readInto(this->weights.infeasibleCost,"infeasibleCost");
  config.readInto(this->weights.centerCost,"centerCost");
  config.readInto(this->weights.headOnCost_soft,"headOnCost_soft");
  config.readInto(this->weights.headOnCost_hard,"headOnCost_hard");
  config.readInto(this->weights.offRoadCost,"offRoadCost");
  config.readInto(this->weights.gearChangeCost,"gearChangeCost");
  config.readInto(this->weights.reverseCost,"reverseCost");
  config.readInto(this->weights.railCost,"railCost");
  config.readInto(this->weights.sideCost,"sideCost");
  config.readInto(this->weights.sideDist,"sideDist");
  config.readInto(this->weights.vehicleCost,"vehicleCost");
  config.readInto(this->weights.changeCost_nopass,"changeCost_nopass");
  config.readInto(this->weights.changeCost_pass,"changeCost_pass");
  config.readInto(this->weights.obsCost_nopass,"obsCost_nopass");
  config.readInto(this->weights.obsCost_pass,"obsCost_pass");
  config.readInto(this->weights.carCost_nopass,"carCost_nopass");
  config.readInto(this->weights.carCost_pass,"carCost_pass");
  config.readInto(this->laneChangeHardConstraint,"laneChangeHardConstraint");
  config.readInto(this->weights.accelScale,"accelScale");
  config.readInto(this->weights.accelExp,"accelExp");

  // Set some defaults
  this->weights.changeCost   = this->weights.changeCost_nopass;
  this->weights.obsCost      = this->weights.obsCost_nopass;
  this->weights.carCost      = this->weights.carCost_nopass;

  if (this->laneChangeHardConstraint)
    this->weights.headOnCost = weights.headOnCost_hard;
  else
    this->weights.headOnCost = weights.headOnCost_soft;
  
  return 0;
}


// Set the heuristic method
int RailPlanner::setHeuristic(RailPlannerHeuristic heuristic)
{
  this->heuristic = heuristic;
  
  return 0;
}


// Set the corridor (entry/exit points)
int RailPlanner::setCorridor(RNDFGraphWaypointList *corridor)
{
  this->corridor = corridor;
  return 0;
}


// Set the flags that modulate weights
int RailPlanner::setMode(bool allowPass, bool allowReverse, bool allowVehicle)
{
  this->allowLaneChange = allowPass;
  this->allowReverse = allowReverse;
  this->allowVehicle = allowVehicle;

  if (allowPass)
  {
    //this->weights.headOnCost   = this->weights.headOnCost_pass;
    this->weights.changeCost   = this->weights.changeCost_pass;
    this->weights.obsCost      = this->weights.obsCost_pass;
    this->weights.carCost      = this->weights.carCost_pass;    
  }
  else
  {
    //this->weights.headOnCost   = this->weights.headOnCost_nopass;
    this->weights.changeCost   = this->weights.changeCost_nopass;
    this->weights.obsCost      = this->weights.obsCost_nopass;
    this->weights.carCost      = this->weights.carCost_nopass;
  }

  if (this->laneChangeHardConstraint)
    this->weights.headOnCost = weights.headOnCost_hard;
  else
    this->weights.headOnCost = weights.headOnCost_soft;
  
  return 0;
}


// Set the distance at which we accept goal completion.
int RailPlanner::setGoalRadius(float goalRadius, bool goalDistOnly)
{
  this->goalRadius = goalRadius;
  this->goalDistOnly = goalDistOnly;
  return 0;
}


// Set the current speed (m/s) for curvature costs.
int RailPlanner::setSpeed(float speed)
{
  this->speed = speed;
  
  return 0;
}

      
// Check for hard constraints.  
bool RailPlanner::checkConstraints(PlanState *stateA, PlanState *stateB)
{  
  PlanGraphNode *nodeA, *nodeB;

  nodeA = stateA->node;
  nodeB = stateB->node;

  // Check for changes into oncoming lanes
  if (nodeB->flags.isOncoming == 1)
  {
    if (!this->allowLaneChange && this->laneChangeHardConstraint)
      return false;
  }
  
  // Only reverse in-lane or along the vehicle sub-graph
  if (stateB->inReverse)
  {
    if (!(nodeB->flags.isLane || nodeB->flags.isVehicle))
      return false;
  }

  // See if we are allowed to use the vehicle sub-graph
  if (nodeB->flags.isVehicle)
  {
    if (!this->allowVehicle)
      return false;
  }
  
  // If we start in collision, dont accept states that make things
  // worse.  We assume that if the collision state is fresh, the
  // distances must also be fresh.
  if (stateA->inCollision && this->graph->isObsCollision(nodeB))
  {
    if (!stateB->inReverse && nodeB->status.obsFrontDist < nodeA->status.obsFrontDist)
      return false;
    if (stateB->inReverse && nodeB->status.obsRearDist < nodeA->status.obsRearDist)
      return false;
  }

  // Check for deviations from the corridor
  if (this->corridor && !this->checkCorridor(stateB))
  {
    //MSG("stateA rev %d ex %d en %d %d.%d.%d %d.%d.%d %d.%d.%d",
    //stateA->inReverse, nodeA->flags.isExit, nodeA->flags.isEntry,
    //nodeA->segmentId, nodeA->laneId, nodeA->waypointId,
    //nodeA->nextWaypoint->segmentId, nodeA->nextWaypoint->laneId, nodeA->nextWaypoint->waypointId,
    //nodeA->prevWaypoint->segmentId, nodeA->prevWaypoint->laneId, nodeA->prevWaypoint->waypointId);
    //MSG("stateB rev %d ex %d en %d %d.%d.%d %d.%d.%d %d.%d.%d",
    //stateB->inReverse, nodeB->flags.isExit, nodeB->flags.isEntry,
    //nodeB->segmentId, nodeB->laneId, nodeB->waypointId,
    //nodeB->nextWaypoint->segmentId, nodeB->nextWaypoint->laneId, nodeB->nextWaypoint->waypointId,
    //nodeB->prevWaypoint->segmentId, nodeB->prevWaypoint->laneId, nodeB->prevWaypoint->waypointId);
    return false;
  }

  return true;
}


// Check for corridor constraints
bool RailPlanner::checkCorridor(PlanState *state)
{
  int i;
  PlanGraphNode *node;
  RNDFGraphWaypoint *wp;

  node = state->node;
  
  if (!(node->flags.isEntry || node->flags.isExit))
    return true;
  
  // If the node is an entry/exit point, search for a corresponding
  // entry/exit in the corridor.
  assert(this->corridor);
  for (i = 0; i < (int) this->corridor->size(); i++)
  {
    wp = (*this->corridor)[i];
    if (node->flags.isExit && node->nextWaypoint == wp)
      return true;
    if (node->flags.isEntry && node->prevWaypoint == wp)
      return true;
  }

  // HACK to allow us to reverse through exit points;
  // needed for some particularly tight turns.
  if (node->flags.isExit && state->inReverse)
    return true;
  
  //MSG("node ex %d en %d %d.%d.%d %d.%d.%d %d.%d.%d",
  //    node->flags.isExit, node->flags.isEntry,
  //    node->segmentId, node->laneId, node->waypointId,
  //    node->nextWaypoint->segmentId, node->nextWaypoint->laneId, node->nextWaypoint->waypointId,
  //    node->prevWaypoint->segmentId, node->prevWaypoint->laneId, node->prevWaypoint->waypointId);

  return false;
}


// Calculate the actual cost for getting between two adjacent nodes.
float RailPlanner::calcG(PlanState *stateA, PlanState *stateB)
{
  float cost;
  PlanGraphNode *nodeA, *nodeB;

  nodeA = stateA->node;
  nodeB = stateB->node;

  // Start with cost for node A
  cost = stateA->costG;

  // Add cost for the Euclidean distance.
  cost += DISTANCE(nodeB, nodeA);

  // Discourage infeasible turns
  if (nodeB->flags.isInfeasible)
    cost += this->weights.infeasibleCost;
  
  // Discourage driving in oncoming lanes
  if (nodeB->flags.isOncoming)
    cost += this->weights.headOnCost;

  // Discourage driving on vehicle nodes
  // This is a field HACK; should not be necessary.
  if (nodeB->flags.isVehicle)
    cost += this->weights.vehicleCost;
  
  // If the state is in collision...
  if (this->graph->isObsCollision(nodeB))
  {
    // TESTING
    // HACK for off-road; may not be necessary.
    if (nodeB->flags.isVehicle)
    {
      float dx, dy;
      dx = nodeB->pose.pos.x - this->startPose.pos.x;
      dy = nodeB->pose.pos.y - this->startPose.pos.y;
      if (dx*dx + dy*dy < 30*30)  // MAGIC
        cost += this->weights.obsCost;
    }
    else
    {
      // Discourage collisions.
      if (nodeB->status.obsCollision)
        cost += this->weights.obsCost;
    }
  }
  else if (this->graph->isStatusFresh(nodeB->status.obsDistTime))
  {
    // If the obstacle distance information is current, apply cost
    // for glancing collisions.
    float s;
    s = this->weights.sideDist - nodeB->status.obsSideDist;
    if (s > 0)
      cost += s * this->weights.sideCost;
  }

  // If the sensned lane information is current...
  if (this->graph->isStatusFresh(nodeB->status.centerTime))
  {
    // Stay in the center of the lane.
    cost += nodeB->status.centerLineDist * this->weights.centerCost;
    cost += nodeB->status.centerRoughDist * this->weights.centerCost;
  }
  else
  {
    // If there is no sensed information, discourage driving off the
    // center rail.
    cost += this->weights.railCost * abs(nodeB->railId);
  }

  // Apply a one-off cost for changing gears
  if (stateA->inReverse != stateB->inReverse)
    cost += this->weights.gearChangeCost;

  // Apply a cost for driving in reverse
  if (stateB->inReverse)
    cost += this->weights.reverseCost;

  // Give up here if we are outside the graph ROI
  if (!this->graph->hasPointROI(nodeB->pose.pos.x, nodeB->pose.pos.y))
    return cost;
  
  // When making rail or lane changes, check the curvature to minimize
  // swerving.  For turning, we assume the vehicle will slow down
  // first.
  if (this->weights.accelScale > 0 && this->speed > 0 && !stateB->inReverse &&
      (nodeB->flags.isRailChange || nodeB->flags.isLaneChange || nodeB->flags.isVehicle))
  {
    pose2f_t pose;
    float cx, cy, mx, my, ra, rb, radius, accel;

    // TODO optimize using site-to-node transform
    // Final pose in the initial frame
    pose = pose2f_mul(pose2f_inv(nodeA->pose), nodeB->pose);

    // Compute equation of line at right angles to current pose.
    // x = t mx + cx.
    // y = t my + cy.
    cx = pose.pos.x;
    cy = pose.pos.y;
    mx = -sin(pose.rot);
    my = +cos(pose.rot);

    // Trap straight-line case to avoid div-by-zero.
    if (fabsf(mx) > 1e-6)
    {
      // Find intercept with x = 0; the values t and s specify the radii
      // of two turning circles.
      rb = -cx / mx;
      ra = +rb * my + cy;
      radius = MIN(fabsf(ra), fabsf(rb));

      // Compute the maximum centripetal acceleration on this turn.
      accel = this->speed * this->speed / radius;
      
      // Apply a velocity-dependent cost
      cost += powf(accel/this->weights.accelScale, this->weights.accelExp);

      //MSG("radius %f accel %f cost %f", radius, accel, cost);
    }
  }

  // Check for overflow
  if (!finite(cost))
    return FLT_MAX;

  return cost;
}



// Calculate the heuristic (guess for the cost) between the given nodes.
// This must strictly under-estimate the true cost.
float RailPlanner::calcH(PlanState *state)
{
  float dx, dy, cost;
  PlanGraphNode *node;

  node = state->node;

  if (this->heuristic == HEURISTIC_RNDF)
  {
    float costNext = FLT_MAX;
    float costPrev = FLT_MAX;
    
    if (node->nextWaypoint)
    {
      // Add the euclidean distance from this node to the waypoint
      dx = node->pose.pos.x - node->nextWaypoint->px;
      dy = node->pose.pos.y - node->nextWaypoint->py;
      costNext = node->nextWaypoint->planCost + sqrtf(dx*dx + dy*dy);
    }
    if (node->prevWaypoint)
    {
      // Add the euclidean distance from this node to the waypoint
      dx = node->pose.pos.x - node->prevWaypoint->px;
      dy = node->pose.pos.y - node->prevWaypoint->py;
      costPrev = node->prevWaypoint->planCost + sqrtf(dx*dx + dy*dy);
    }

    if (node->nextWaypoint || node->prevWaypoint)
    {
      cost = MIN(costNext, costPrev);
    }
    else
    {
      // Try Euclidean distance to the goal
      dx = node->pose.pos.x - this->goalPos.x;
      dy = node->pose.pos.y - this->goalPos.y;
      cost = sqrtf(dx*dx + dy*dy);      
    }
  }
  else if (this->heuristic == HEURISTIC_L1)
  {
    // Use Manhatten distance to the goal
    dx = node->pose.pos.x - this->goalPos.x;
    dy = node->pose.pos.y - this->goalPos.y;
    cost = dx + dy;
  }
  else 
  {
    // Use Euclidean distance to the goal
    dx = node->pose.pos.x - this->goalPos.x;
    dy = node->pose.pos.y - this->goalPos.y;
    cost = sqrtf(dx*dx + dy*dy);
  }

  return cost;
}


// Plan path from the start node to the goal node, given the current
// planner weights.
int RailPlanner::planPath(PlanGraphNode *nodeStart, PlanGraphPathDirection dirStart, 
                          PlanGraphNode *nodeGoal, PlanGraphPath *path)
{
  PlanState *stateFinal;
  PlanStateList states;
  RailPlannerHeuristic defaultHeuristic;

  // Reset path
  path->valid = false;
  path->collideObs = 0;
  path->collideCar = 0;
  path->dist = 0;
  path->pathLen = 0;
  
  // Reset stats
  this->numSearched = 0;
  this->goalFound = false;
  this->forwardDist = 0;
  this->reverseDist = 0;

  stateFinal = NULL;

  // Check for valid start/end conditions
  if (!nodeStart)
    return -1;
  if (!nodeGoal)
    return -1;

  // Record the default heuristic; we may change the one we use
  defaultHeuristic = this->heuristic;

  // Generate the heuristic; if it fails, fall back to L2
  if (this->heuristic == HEURISTIC_RNDF)
  {
    if (this->genHeuristic(nodeGoal->segmentId, nodeGoal->laneId, nodeGoal->waypointId) != 0)
    {
      MSG("falling back to L2 Euclidean heuristic (failure 1).");
      this->heuristic = HEURISTIC_L2;
    }
  }
   
  // Record start pose so we can discount distant obstacles
  this->startPose = nodeStart->pose;

  // Record the goal position for the Heursistics
  this->goalPos = nodeGoal->pose.pos;
    
  // Run A* search.  If successful, finalState will contain the last
  // state in the path.
  if (this->genPlan(nodeStart, dirStart, nodeGoal, &stateFinal) == 0)
  {
    this->goalFound = true;
    this->forwardDist = stateFinal->forwardDist;
    this->reverseDist = stateFinal->reverseDist;
  }
  
  // Print diagnostics
  //this->dumpGraph();

  // Backtrack to get the list of states on this path
  while (stateFinal)
  {
    states.push_front(stateFinal);
    stateFinal = stateFinal->prior;
  }

  // Sanity check: the first state in the path must correspond to the
  // starting node
  if (stateFinal)
    assert(states.front()->node == nodeStart);

  // Print diagnostics
  //this->dumpPlanStates(&states);

  // Construct path from the state list
  this->genPath(&states, path);
  
  // Clearn up, ready for next time.
  this->clearOpen();
  this->freeStates();

  // Restore the default heuristic
  this->heuristic = defaultHeuristic;

  // Signal failure if we could not get a path to the goal.
  if (!this->goalFound)
    return -1;
  
  return 0;
}


// Pre-compute the search heuristic on the RNDF (Djikstra).
int RailPlanner::genHeuristic(int segmentId, int laneId, int waypointId)
{
  int i;
  float dx, dy, cost;
  RNDFGraph *rndf;
  RNDFGraphWaypoint *wp, *wpA, *wpB;
  PlanState *state, *stateA, *stateB;
  
  // Use the underlying graph RNDF
  rndf = &this->graph->rndf;
  
  // TODO: remove the plan cost from the RNDF graph; instead, just
  // leave the state nodes in place (i.e., don't free the states), and
  // run the RNDF planner on every cycle (for a small additional
  // overhead).
  
  // Reset plan costs; this is slow, but necessary if we are going to
  // use the cost values stored in the graph.
  for (i = 0; i < rndf->numWaypoints; i++)
  {
    wp = rndf->waypoints + i;
    wp->planCost = FLT_MAX;
  }

  // Get the goal point
  wp = rndf->getWaypoint(segmentId, laneId, waypointId);
  if (!wp)
    return ERROR("waypoint %d.%d.%d not found in RNDF", segmentId, laneId, waypointId);

  // Push the goal.  Note that we use the cost field in the RNDF,
  // since this is the final product of the pre-planning step.
  state = this->getState(wp);
  state->isOpen = true;
  state->isClosed = false;
  wp->planCost = 0;
  this->pushOpen(state);
  
  // Run Djikstra on the open list; search backwards from goal to
  // assign a cost to each waypoint.
  while (this->popOpen(&stateB))
  {
    stateB->isOpen = false;
    stateB->isClosed = true;
    wpB = stateB->wp;
    
    //MSG("B %d.%d.%d %f",
    //    wpB->segmentId, wpB->laneId, wpB->waypointId, wpB->planCost);
          
    // Look at our incoming waypoints
    for (i = 0; i < wpB->numPrev; i++)
    {
      wpA = wpB->prev[i];
      stateA = this->getState(wpA);

      //MSG("A %d.%d.%d %f",
      //      wpA->segmentId, wpA->laneId, wpA->waypointId, wpA->planCost);

      // Compute the cumulative cost to reach the goal from this node
      cost = wpB->planCost;
      dx = wpB->px - wpA->px;
      dy = wpB->py - wpA->py;
      cost += sqrtf(dx*dx + dy*dy);

      // If it is better than the current cost...
      if (cost < wpA->planCost)
      {
        // Push onto queue
        stateA->isOpen = true;
        wpA->planCost = cost;
        this->pushOpen(stateA);
      }
    }
    
    // Look at our outgoing waypoints
    for (i = 0; i < wpB->numNext; i++)
    {
      wpA = wpB->next[i];
      stateA = this->getState(wpA);

      //MSG("A %d.%d.%d %f",
      //      wpA->segmentId, wpA->laneId, wpA->waypointId, wpA->planCost);

      // Compute the cumulative cost to reach the goal from this node
      cost = wpB->planCost;
      dx = wpB->px - wpA->px;
      dy = wpB->py - wpA->py;
      cost += sqrtf(dx*dx + dy*dy);

      // If it is better than the current cost...
      if (cost < wpA->planCost)
      {
        // Push onto queue
        stateA->isOpen = true;
        wpA->planCost = cost;
        this->pushOpen(stateA);
      }
    }
  }

  // Clean up after ourselves
  this->freeStates();
  
  return 0;
}


// Find the optimial path using (A*)
int RailPlanner::genPlan(PlanGraphNode *nodeStart, PlanGraphPathDirection dirStart, 
                         PlanGraphNode *nodeGoal, PlanState **stateFinal)
{
  int i;
  bool found, useReverse;
  float costG, costH;
  PlanState *state, *stateA, *stateB;
  PlanGraphNode *nodeA, *nodeB;

  // Initilize stats
  found = false;
  this->numSearched = 0;
  
  // Push the start node onto the open list.
  // The total cost is just the heuristic.
  state = this->getState(nodeStart, (dirStart == PLAN_GRAPH_PATH_REV));
  state->inReverse = (dirStart == PLAN_GRAPH_PATH_REV);
  state->costG = 0;
  state->costH = this->calcH(state);
  state->cost = state->costG + state->costH;
  state->isOpen = true;

  // Is the initial state already in collision?  If so, we will apply
  // a hard obstacle constraints until we are out of collision; after that,
  // we revert to soft constraints.
  if (this->graph->isObsCollision(nodeStart))
    state->inCollision = true;

  state->forwardDist = 0;
  state->reverseDist = 0;
  
  this->pushOpen(state);
  
  // Get the open nodes one by one and run A*
  while (this->popOpen(&stateA))
  {
    // Dont re-open duplicates
    if (!stateA->isOpen)
      continue;
    this->numSearched++;
    
    // Get the corresponding node in the plan graph
    nodeA = stateA->node;

    // Mark it is closed for now (although it may get re-opened later)
    stateA->isClosed = true;

    // Reset the initial collision flag, if warranted.
    if (stateA->inCollision && !this->graph->isObsCollision(nodeA))
      stateA->inCollision = false;

    // Do a check for the total distance we have reversed.
    // If it is too large, this path is no good.
    // TESTING
    //if (stateA->reverseDist > 20.0) // MAGIC
    //  continue;
    
    //MSG("expanding %d.%d.%d.%d %f %f = %f",
    //    nodeA->segmentId, nodeA->laneId, nodeA->waypointId, nodeA->interId,
    //    stateA->costG, stateA->costH, stateA->cost);
    
    // Check for goal achieved.  If distCheck is enabled, we require
    // only that we are close to the goal; otherwise, we must arrive
    // at a node in the right part of the RNDF, and be also close to
    // the goal.
    if (this->goalDistOnly ||
        (nodeA->prevWaypoint &&
         nodeA->prevWaypoint->segmentId == nodeGoal->segmentId &&
         nodeA->prevWaypoint->laneId == nodeGoal->laneId &&
         nodeA->prevWaypoint->waypointId == nodeGoal->waypointId))
    {
      float dx, dy, dist;
      dx = nodeA->pose.pos.x - nodeGoal->pose.pos.x;
      dy = nodeA->pose.pos.y - nodeGoal->pose.pos.y;
      dist = sqrtf(dx*dx + dy*dy);
      if (dist < this->goalRadius)
      {
        found = true;
        *stateFinal = stateA;       
        break;
      }
    }

    // Look at all the outgoing arcs for this node.
    for (i = 0; i < nodeA->numNext; i++)
    {
      nodeB = nodeA->next[i];
      stateB = this->getState(nodeB, false);
      stateB->inReverse = false;

      assert(nodeA != nodeB);
      assert(stateA != stateB);
            
      // Check for hard constraints
      if (!this->checkConstraints(stateA, stateB))
        continue;

      // Compute cost so far plus the cost still to go.
      costG = this->calcG(stateA, stateB);
      costH = this->calcH(stateB);

      // Check if this next node is already is open or closed; if the
      // copy there is as good or better, dont bother adding this one.
      // Note that this needs to be a greater-than-or-equal to test to
      // ensure termination of the search (possibly should add some
      // epsilon just to make sure).
      if (stateB->isOpen || stateB->isClosed)
      {
        if (costG + costH >= stateB->costG + stateB->costH)
          continue;
      }
      
      // The node is no longer closed, so push it back onto the open
      // list with the new costs.
      stateB->prior = stateA;      
      stateB->isClosed = false;
      stateB->isOpen = true;
      stateB->costG = costG;
      stateB->costH = costH;
      stateB->cost = costG + costH;
      stateB->inCollision = stateA->inCollision;
      stateB->forwardDist = stateA->forwardDist + DISTANCE(nodeB, nodeA);
      stateB->reverseDist = stateA->reverseDist;
      
      this->pushOpen(stateB);
    }
    
    useReverse = this->allowReverse;

    // If we are in the goal segment/lane, and close to the goal,
    // allow reversing so we can hit the goal point.
    if (nodeA->segmentId == nodeGoal->segmentId && nodeA->laneId == nodeGoal->laneId)
    {
      float dx, dy, dist;
      dx = nodeA->pose.pos.x - nodeGoal->pose.pos.x;
      dy = nodeA->pose.pos.y - nodeGoal->pose.pos.y;
      dist = sqrtf(dx*dx + dy*dy);
      useReverse = (dist < 20); // MAGIC
    }

    // See if reversing should be used
    if (!useReverse)
      continue;
    
    // Look at all the incoming arcs for this node (driving in reverse)
    for (i = 0; i < nodeA->numPrev; i++)
    {
      nodeB = nodeA->prev[i];
      stateB = this->getState(nodeB, true);
      stateB->inReverse = true;
      
      assert(nodeA != nodeB);
      assert(stateA != stateB);
            
      // Check for hard constraints
      if (!this->checkConstraints(stateA, stateB))
        continue;
            
      // Compute cost so far plus the cost still to go.
      costG = this->calcG(stateA, stateB);
      costH = this->calcH(stateB);

      // Check if this prev node is already is open or closed; if the
      // copy there is already better, dont bother adding this one.
      if (stateB->isOpen || stateB->isClosed)
      {
        if (costG + costH >= stateB->costG + stateB->costH)
          continue;
      }
      
      // The node is no longer closed, so push it back onto the open
      // list with the new costs.
      stateB->prior = stateA;      
      stateB->isClosed = false;
      stateB->isOpen = true;
      stateB->costG = costG;
      stateB->costH = costH;
      stateB->cost = costG + costH;
      stateB->inCollision = stateA->inCollision;
      stateB->forwardDist = stateA->forwardDist;
      stateB->reverseDist = stateA->reverseDist + DISTANCE(nodeB, nodeA);

      this->pushOpen(stateB);
    }    
  }
  
  if (!found)
    return -1;
    
  return 0;
}


// Construct path from the state list
int RailPlanner::genPath(PlanStateList *states, PlanGraphPath *path)
{
  int i;
  PlanState *state;
  PlanGraphNode *node;

  // Initialize path
  path->valid = true;
  path->collideObs = 0;
  path->collideCar = 0;
  path->dist = 0;
  path->pathLen = 0;

  // Walk along the plan states
  for (i = 0; i < (int) states->size(); i++)
  {
    state = (*states)[i];
    node = state->node;

    // Update the total path distance
    if (i > 0)
    {
      PlanState *statePrev;
      PlanGraphNode *nodePrev;
      statePrev = (*states)[i - 1];
      nodePrev = statePrev->node;
      path->dist += vec2f_mag(vec2f_sub(node->pose.pos, nodePrev->pose.pos));      
    }    

    // Append node to the path list
    assert((size_t) path->pathLen < sizeof(path->nodes)/sizeof(path->nodes[0]));
    path->nodes[path->pathLen] = node;
    path->dists[path->pathLen] = path->dist;
    if (state->inReverse)
      path->directions[path->pathLen] = PLAN_GRAPH_PATH_REV;
    else
      path->directions[path->pathLen] = PLAN_GRAPH_PATH_FWD;
    path->pathLen++;

    // Update collision fields
    if (node->status.obsCollision && this->graph->isStatusFresh(node->status.obsTime))
      path->collideObs = 1;
    if (node->status.carCollision && this->graph->isStatusFresh(node->status.carTime))
      path->collideCar = 1;

    // If we didnt get to the goal, stop here.
    if (!this->goalFound)
      break;
  }
  
  return 0;
}


// Get the state information for a waypoint.  The state is stored in
// the user data pointer in the waypoint.
RailPlanner::PlanState *RailPlanner::getState(RNDFGraphWaypoint *wp)
{
  if (wp->data == NULL)
  {
    PlanState *state = (PlanState*) calloc(1, sizeof(PlanState));
    state->wp = wp;
    wp->data = state;
    this->dirtyList.push(state);
  }
  return (PlanState*) (wp->data);
}


// Get the state information for a PlanGraph node.  Note that we only
// store one state per node, regardless of transmission position,
// since the optimal path cannot go both forwards and reverse through a node.
RailPlanner::PlanState *RailPlanner::getState(PlanGraphNode *node, bool inReverse)
{
  void **data;

  if (!inReverse)
    data = &node->data1;
  else
    data = &node->data2;
  
  if ((*data) == NULL)
  {
    PlanState *state = (PlanState*) calloc(1, sizeof(PlanState));
    state->node = node;
    (*data) = state;
    this->dirtyList.push(state);
  }
  
  return (PlanState*) (*data);  
}


// Free the state information.  The state data is freed and removed
// from the user data pointers in the RNDF or plan graphs.
void RailPlanner::freeStates()
{
  PlanState *state;
  while (!this->dirtyList.empty())
  {
    state = this->dirtyList.front();
    this->dirtyList.pop();
    if (state->wp)
      state->wp->data = NULL;
    if (state->node)
    {
      state->node->data1 = NULL;
      state->node->data2 = NULL;
    }
    free(state);
  }  
  return;
}


// Push a state onto the open list
void RailPlanner::pushOpen(PlanState *state)
{
  this->openList.push(state);  
  return;
}


// Pop a state from the open list
bool RailPlanner::popOpen(PlanState **state)
{
  if (this->openList.empty())
    return false;
  *state = this->openList.top();
  this->openList.pop();
  return true;
}


// Clear the open list
void RailPlanner::clearOpen()
{
  while (!this->openList.empty())
    this->openList.pop();
  return;
}


// Dianostic: dump the entire graph with costs
int RailPlanner::dumpGraph()
{
  int i;
  PlanGraphNodeList nodes;
  PlanGraphNode *node;
  PlanState *state;

  this->graph->getRegion(&nodes, 0, 0, 8192, 8192);

  for (i = 0; i < (int) nodes.size(); i++)
  {
    node = nodes[i];
    state = (PlanState*) node->data1;
    if (!state)
      continue;    
    fprintf(stdout, "graph %d.%d.%d.%d  %f %f  %f %f %f  %f\n",
            node->segmentId, node->laneId, node->waypointId, node->interId,
            node->pose.pos.x, node->pose.pos.y,
            state->costG, state->costH, state->cost, this->calcH(state));
  }
  fprintf(stdout, "\n\n");
  fflush(stdout);
  
  return 0;
}


// Diagnostics: print the states in a path
int RailPlanner::dumpStates(PlanStateList *states)
{
  int i;
  PlanState *state;
  PlanGraphNode *node;    

  for (i = 0; i < (int) states->size(); i++)
  {
    state = (*states)[i];
    node = state->node;
    fprintf(stdout, "states %d.%d.%d.%d  %f %f  %f %f %f\n",
            node->segmentId, node->laneId, node->waypointId, node->interId,
            node->pose.pos.x, node->pose.pos.y,
            state->costG, state->costH, state->cost);
  }
  fprintf(stdout, "\n\n");

  return 0;
}


// Diagnostics: print the path
int RailPlanner::dumpPath(PlanGraphPath *path)
{
  int i;
  PlanGraphNode *node;    

  for (i = 0; i < path->pathLen; i++)
  {
    node = path->nodes[i];
    fprintf(stdout, "path %d.%d.%d.%d  %f %f\n",
            node->segmentId, node->laneId, node->waypointId, node->interId,
            node->pose.pos.x, node->pose.pos.y);
  }
  fprintf(stdout, "\n\n");

  return 0;
}

