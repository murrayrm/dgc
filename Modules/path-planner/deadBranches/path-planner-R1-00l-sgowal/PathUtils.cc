/**********************************************************
 **
 **  PATHUTILS.CC
 **
 **    Time-stamp: <2007-08-03 18:35:00 ndutoit> 
 **
 **    Author: Noel du Toit
 **    Created: Thu Jul 26 08:59:31 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "PathUtils.hh"

#define EPS_X 0.0001
#define EPS_T 0.001
// TODO: talk to follower people and figure out what the right value for this parameter is
#define MAX_PATH_DIST 10.0

Err_t PathUtils::planFromNearestNodeOnPath(Path_t* path, Graph_t* graph, VehicleState vehState)
{
  double heading, m1, c1, m2, c2, x, y;
  point2 currPos;

  // Get Alice current pos
  heading = AliceStateHelper::getHeading(vehState);
  currPos.set(AliceStateHelper::getPositionRearAxle(vehState));
  
  // project this point into the previous path we had
  int i;
  for (i=0; i+1< path->pathLen; i++) {
    // get params for line perp to alice's heading, through rear axle
    m1 = tan(getAngleInRange(heading+M_PI/2));
    c1 = currPos.y - m1*currPos.x;
    
    // get params for line between 2 nodes
    if (fabs(path->path[i+1]->pose.pos.x - path->path[i]->pose.pos.x)<EPS_X)
      m2 = (path->path[i+1]->pose.pos.y - path->path[i]->pose.pos.y)/EPS_X;
    else
      m2 = (path->path[i+1]->pose.pos.y - path->path[i]->pose.pos.y)/(path->path[i+1]->pose.pos.x - path->path[i]->pose.pos.x);
    c2 = path->path[i]->pose.pos.y - m2*path->path[i]->pose.pos.x;

    // get the intersection of these lines
    x = -(c1-c2)/(m1-m2);
    y = m1*x + c1;

    // check to see if this intersection is between these nodes. 
    // If so, use the first node in the path as the vehicle node. 
    // if not, move on to the next node
    double angle1, angle2, diff_angles;
    angle1 = atan2(path->path[i]->pose.pos.y - y, path->path[i]->pose.pos.x - x);
    angle2 = atan2(path->path[i+1]->pose.pos.y - y, path->path[i+1]->pose.pos.x - x);
    diff_angles = angle1 - angle2;
    diff_angles = PathUtils::getAngleInRange(diff_angles);
    if ( fabs(diff_angles) > EPS_T ) {
      // If the node on the path is too far away, we disregard it.
      if (pow(path->path[i]->pose.pos.x-currPos.x,2) + pow(path->path[i]->pose.pos.y-currPos.y,2) < MAX_PATH_DIST*MAX_PATH_DIST) {
        graph->vehicleNode = path->path[i];
        return PP_OK;
      } else
        return PP_NONODEFOUND;
    }
  }
  // if we get here we did not find any intersections within the ranges
  // so something went wrong
  return PP_NONODEFOUND;
}

GraphNode* PathUtils::getNearestNode(Graph* graph, point2 point, int typeMask, double dist)
{
  vec3_t pos;
  pos.x = point.x;
  pos.y = point.y;
  pos.z = 0;
  return graph->getNearestNode(pos, typeMask, dist);
}


/*!
 * return the node on the path that is closest to Alice using euclidean dist
 */
GraphNode* PathUtils::getNearestNodeOnPath(Path_t* path, point2 point)
{
  GraphNode* minNode = NULL;
  point2 tmpPt;
  double minDist = 100000000;
  for (int i=0; i<path->pathLen; i++) {
    if (!path->path[i]) break;
    tmpPt.set(path->path[i]->pose.pos.x,path->path[i]->pose.pos.y);
    if (tmpPt.dist(point)<minDist) {
      minDist = tmpPt.dist(point);
      minNode = path->path[i];
    }
  }
  return minNode;
}


/**
 * @brief Returns an -PI, PI bounded angle\
 */
double PathUtils::getAngleInRange(double angle)
{
  double angleInRange = angle;

  while (angleInRange > M_PI) 
    angleInRange -= 2*M_PI;

  while (angleInRange <= -M_PI)
    angleInRange += 2*M_PI;

  return angleInRange;
}
