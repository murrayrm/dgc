#ifndef PATHUTILS_HH_
#define PATHUTILS_HH_

#include <frames/pose2.h>
#include <temp-planner-interfaces/PlanGraphPath.hh>

class PathUtils {

public: 

  static PlanGraphNode *getNearestNodeEuclidean(PlanGraphPath* path, vec2f_t pos, float maxDist);
  static PlanGraphNode *getNearestNodeProjection(PlanGraphPath* path, pose2f_t pose, float maxDist, float extended = 1.0);
  static double distanceToObsOnPath(PlanGraph *graph, PlanGraphPath* path);
  static float getAngleInRange(float angle);
};

#endif /*PATHUTILS_HH_*/




