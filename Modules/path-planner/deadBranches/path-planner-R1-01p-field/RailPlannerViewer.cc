/* 
 * Desc: Rail planner viewer utility
 * Date: 15 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <float.h>

#include <GL/glut.h>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>
#include <frames/mat44.h>
#include <dgcutils/DGCutils.hh>

#include "PathUtils.hh"
#include "RailPlannerViewer.hh"

 
// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, "%s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)

// Useful macros
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))


// Default constructor
RailPlannerViewer::RailPlannerViewer()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Destructor
RailPlannerViewer::~RailPlannerViewer()
{
  return;
}


// Initialize stuff
int RailPlannerViewer::initGUI(int cols, int rows)
{
  // The menu
  Fl_Menu_Item menuitems[] =
    {
      {"&File", 0, 0, 0, FL_SUBMENU},    
      {"E&xit", FL_CTRL + 'q', (Fl_Callback*) RailPlannerViewer::onExit},
      {0},

      {"&Action", 0, 0, 0, FL_SUBMENU},    
      {"Pause", ' ', (Fl_Callback*) RailPlannerViewer::onAction,
       (void*) CMD_ACTION_PAUSE, FL_MENU_TOGGLE},
      {"Reload", 'u', (Fl_Callback*) RailPlannerViewer::onAction,
       (void*) CMD_ACTION_RELOAD},
      {"Allow reverse", FL_ALT + 'r', (Fl_Callback*) RailPlannerViewer::onAction,
       (void*) CMD_ACTION_REVERSE, FL_MENU_TOGGLE},
      {"Allow oncoming", FL_ALT + 'o', (Fl_Callback*) RailPlannerViewer::onAction,
       (void*) CMD_ACTION_ONCOMING, FL_MENU_TOGGLE},
      {0},
      
      {"&View", 0, 0, 0, FL_SUBMENU},

      {"Aerial", 'a', (Fl_Callback*) RailPlannerViewer::onAction,
       (void*) (CMD_VIEW_AERIAL + CMD_VIEW_FIRST), FL_MENU_TOGGLE | FL_MENU_VALUE},      
      {"RNDF", 'r', (Fl_Callback*) RailPlannerViewer::onAction,
       (void*) (CMD_VIEW_RNDF + CMD_VIEW_FIRST), FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"Graph", 'g', (Fl_Callback*) RailPlannerViewer::onAction,
       (void*) (CMD_VIEW_GRAPH + CMD_VIEW_FIRST), FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"Obstacles", 'o', (Fl_Callback*) RailPlannerViewer::onAction,
       (void*) (CMD_VIEW_OBS + CMD_VIEW_FIRST), FL_MENU_TOGGLE | FL_MENU_VALUE},
      
      // Graph display properties
      {"Plan graph", 0, 0, 0, 0},
      {"Node info", FL_CTRL + 'n', (Fl_Callback*) RailPlannerViewer::onAction,
       (void*) (CMD_GRAPH_NODES + CMD_GRAPH_FIRST), FL_MENU_TOGGLE},
      {"Vehicle", FL_CTRL + 'v', (Fl_Callback*) RailPlannerViewer::onAction,
       (void*) (CMD_GRAPH_VEHICLE + CMD_GRAPH_FIRST), FL_MENU_TOGGLE},
      {"Lane changes", FL_CTRL + 'l', (Fl_Callback*) RailPlannerViewer::onAction,
       (void*) (CMD_GRAPH_LANE_CHANGES + CMD_GRAPH_FIRST), FL_MENU_TOGGLE},
      {"Rail changes", FL_CTRL + 'r', (Fl_Callback*) RailPlannerViewer::onAction,
       (void*) (CMD_GRAPH_RAIL_CHANGES + CMD_GRAPH_FIRST), FL_MENU_TOGGLE},
      {"Node status", FL_CTRL + 's', (Fl_Callback*) RailPlannerViewer::onAction,
       (void*) (CMD_GRAPH_NODE_STATUS + CMD_GRAPH_FIRST),
       FL_MENU_TOGGLE | FL_MENU_VALUE | FL_MENU_DIVIDER},
      
      {0},

      {0},
    };

  // Create top-level window
  this->mainwin = new Fl_Window(cols, rows + 30, "DGC Planner Viewer");
  this->mainwin->user_data(this);

  this->mainwin->begin();

  // Crate the menu bar
  this->menubar = new Fl_Menu_Bar(0, 0, cols, 30);
  this->menubar->user_data(this);
  this->menubar->copy(menuitems);

  // Create world window
  this->worldwin = new Fl_Glv_Window(0, 30, cols, rows, this, (Fl_Callback*) onDraw);

  this->mainwin->end();

  // Make world window resizable  
  this->mainwin->resizable(this->worldwin);

  // Set the initial POV
  this->worldwin->set_hfov(40.0);  
  this->worldwin->set_clip(4, 2000);
  this->worldwin->set_lookat(-0.1, 0, -80, 0, 0, 0, 0, 0, -1);

  // Set consistent menu state
  this->viewLayers = (1 << CMD_VIEW_AERIAL) | (1 << CMD_VIEW_RNDF);
  this->viewLayers |= (1 << CMD_VIEW_GRAPH) | (1 << CMD_VIEW_OBS);
  this->graphProps = (1  << CMD_GRAPH_NODE_STATUS);

  this->rndfList = 0;
  
  this->dirty = true;
  
  return 0;
}


// Finalize stuff
int RailPlannerViewer::finiGUI()
{  
  return 0;
}


// Handle menu callbacks
void RailPlannerViewer::onExit(Fl_Widget *w, int option)
{
  RailPlannerViewer *self;

  self = (RailPlannerViewer*) w->user_data();
  self->quit = true;

  return;
}


// Handle menu callbacks
void RailPlannerViewer::onAction(Fl_Widget *w, int option)
{
  RailPlannerViewer *self;  

  self = (RailPlannerViewer*) w->user_data();
  if (option == CMD_ACTION_PAUSE)
    self->pause = !self->pause;

  if (option == CMD_ACTION_RELOAD)
  {
    self->reload();
    self->dirty = true;
  }

  if (option == CMD_ACTION_REVERSE)
  {
    self->allowReverse = !self->allowReverse;
    self->dirty = true;
  }
  
  if (option == CMD_ACTION_ONCOMING)
  {
    self->allowOncoming = !self->allowOncoming;
    self->dirty = true;
  }
  
  // Check for changes in the selected display viewLayers
  if (option >= CMD_VIEW_FIRST && option < CMD_VIEW_LAST)
  {
    self->viewLayers = (self->viewLayers ^ (1 << (option - CMD_VIEW_FIRST)));
    self->dirty = true;
    self->worldwin->redraw();
  }

  // Check for changes in the selected graph properties
  if (option >= CMD_GRAPH_FIRST && option < CMD_GRAPH_LAST)
  {
    self->graphProps = (self->graphProps ^ (1 << (option - CMD_GRAPH_FIRST)));
    self->dirty = true;
    self->worldwin->redraw();
  }

  return;
}


// Handle draw callbacks
void RailPlannerViewer::onDraw(Fl_Glv_Window *win, RailPlannerViewer *self)
{
  VehicleState *vehState;
  ActuatorState *actState;
  PlanGraph *graph;

  // Get the state.
  vehState = &self->vehState;
  actState = &self->actState;

  // Update the state based on the mouse motion
  if (win->mouse_button == 1 && win->mouse_shift)
  {
    vehState->siteNorthing = win->at_x - self->at_x + self->at_n;
    vehState->siteEasting = win->at_y - self->at_y + self->at_e;
    self->dirty = true;
  }
  else
  {
    self->at_x = win->at_x;
    self->at_y = win->at_y;
    self->at_n = vehState->siteNorthing;
    self->at_e = vehState->siteEasting;
  }

  // Get the graph so we know the global to graph offset
  graph = self->graph;
  assert(graph);

  // Predraw the static RNDF if necessary
  if (self->rndfList == 0)
    self->rndfList = self->graph->rndf.predraw();
      
  // Predraw if necessary
  if (self->dirty)
  {
    // Draw the grid
    self->drawMisc.predrawGrid(vehState->siteNorthing, vehState->siteEasting, 10.0);
    
    // Draw the graph
    if (self->viewLayers & (1 << CMD_VIEW_GRAPH))
      self->predrawGraph(graph, vehState, self->graphProps);

    // Predraw the obsacles
    if (self->viewLayers & (1 << CMD_VIEW_OBS))
      self->predrawObs();
      
    self->dirty = false;
  }    
  
  // Switch to site frame
  glPushMatrix();

  // Draw the grid
  glCallList(self->drawMisc.gridList);
   
  // Do a translation to center on the vehicle
  glTranslatef(-vehState->siteNorthing, -vehState->siteEasting, -vehState->siteAltitude);

  if (true)
  {
    glPushMatrix();
     
    // Translate z so that the maps are directly beneath the vehicle
    glTranslatef(0, 0, vehState->siteAltitude + VEHICLE_TIRE_RADIUS + 0.2);

    // Draw the aerial image
    if (self->viewLayers & (1 << CMD_VIEW_AERIAL))
      self->drawAerial.draw(vehState->siteNorthing, vehState->siteEasting);

    glPopMatrix();

    glPushMatrix();

    // Translate z so that the maps are directly beneath the vehicle
    glTranslatef(0, 0, vehState->siteAltitude + VEHICLE_TIRE_RADIUS);

    // Draw the RNDF 
    if (self->viewLayers & (1 << CMD_VIEW_RNDF))
      glCallList(self->rndfList);  

    // Draw the graph
    if (self->viewLayers & (1 << CMD_VIEW_GRAPH))
    {
      glCallList(self->graphList);
      if (self->graphProps & (1  << CMD_GRAPH_NODE_STATUS))
        glCallList(self->statusList);
    }

    // Draw obstacles
    if (self->viewLayers & (1 << CMD_VIEW_OBS))
      glCallList(self->obsList);

    // Draw the start and goal
    if (true)
    {
      glColor3f(0.7, 0.7, 0.7);
      glPointSize(30);
      glBegin(GL_POINTS);
      if (self->startNode)
        glVertex2f(self->startNode->pose.pos.x, self->startNode->pose.pos.y);
      if (self->goalNode)
        glVertex2f(self->goalNode->pose.pos.x, self->goalNode->pose.pos.y);
      glEnd();

      if (self->startNode && self->goalNode)
      {
        glLineWidth(1);
        glBegin(GL_LINES);
        glVertex2f(self->startNode->pose.pos.x, self->startNode->pose.pos.y);
        glVertex2f(self->goalNode->pose.pos.x, self->goalNode->pose.pos.y);
        glEnd();
      }
    }
    
    // Draw the planned path
    if (true)
      self->drawPath(&self->path);

    glPopMatrix();
  }

  if (true)
  {
    // Switch to vehicle frame
    self->pushFrameVehicle(vehState);
    
    // Draw Alice
    self->drawMisc.drawAxes(1.0);
    self->drawMisc.drawAlice(actState->m_steerpos * VEHICLE_MAX_AVG_STEER);

    // Draw safety boxes
    self->drawSafety();
  
    self->popFrame();
  }

  glPopMatrix();
  
  return;
}


// Switch to the vehicle frame 
void RailPlannerViewer::pushFrameVehicle(const VehicleState *state)
{
  PlanGraph *graph;
  pose3_t pose;
  float m[4][4];
  
  graph = this->graph;
  
  // Transform from vehicle to site frame
  pose.pos = vec3_set(state->siteNorthing, state->siteEasting, state->siteAltitude);
  pose.rot = quat_from_rpy(state->siteRoll, state->sitePitch, state->siteYaw);
  pose3_to_mat44f(pose, m);

  // Transpose to column-major order for GL
  mat44f_trans(m, m);
  
  glPushMatrix();
  glMultMatrixf((GLfloat*) m);
  
  return;
}


// Revert to previous frame
void RailPlannerViewer::popFrame()
{
  glPopMatrix();  
  return;
}


// Handle idle callbacks
void RailPlannerViewer::onIdle(RailPlannerViewer *self)
{
  if (!self->pause)
  {  
    // Update perceptor
    if (self->update() != 0)
      self->quit = true;

    // Redraw the display
    self->worldwin->redraw();
  }
  else
  {
    // Sleepy bye-bye
    usleep(0);
  }
  
  return;
}


// Parse command-line options
int RailPlannerViewer::parseCmdLine(int argc, char **argv)
{
  rail_planner_cmdline_init(&this->cmdline);
      
  // Run parser
  if (rail_planner_cmdline(argc, argv, &this->cmdline) != 0)
  {
    rail_planner_cmdline_print_help();
    return -1;
  }

  // Load configuration file, if given; settings in the config file do not
  // override the command line options.
  if (this->cmdline.inputs_num > 0)
  {
    if (rail_planner_cmdline_configfile(this->cmdline.inputs[0], &this->cmdline,
                                        false, false, false) != 0)
    {
      rail_planner_cmdline_print_help();
      return -1;
    }
  }

  // Do some checks
  if (!this->cmdline.rndf_given)
    return ERROR("rndf must be specified");
  
  return 0;
}


// Initialize the planner
int RailPlannerViewer::init()
{
  char filename [1024];
  
  // Create the graph
  this->graph = new PlanGraph();

  // Load the graph
  snprintf(filename, sizeof(filename), "%s.pg", this->cmdline.rndf_arg);  
  if (this->graph->load(filename) != 0)
    return ERROR("unable to load %s", filename);

  // Load up imagery
  this->drawAerial.load("/dgc/aerial-images/", this->graph->rndf.siteN, this->graph->rndf.siteE);
  
  // Create updater
  this->updater = new PlanGraphUpdater(this->graph);

  // Create the status updater
  this->status = new PlanGraphStatus(this->graph);
  
  // Create planner
  this->planner = new RailPlanner(this->graph);

  // MAGIC
  this->roiSize = 128;

  // Load setting
  this->reload();
  
  return 0;
}


// Finalize the planner
int RailPlannerViewer::fini()
{  
  // Clean up
  free(this->obs);
  this->obs = NULL;
  delete this->planner;
  this->planner = NULL;
  delete this->status;
  this->status = NULL;
  delete this->updater;
  this->updater = NULL;
  delete this->graph;
  this->graph = NULL;
  
  return 0;  
}


// Re-load config file
int RailPlannerViewer::reload()
{
  int i;
  Obstacle *obs;
  pose2f_t pose;

  MSG("reload");
  
  // Re-load the config file
  if (this->cmdline.inputs_num > 0)
  {
    char *filename;
    filename = strdup(this->cmdline.inputs[0]);
    rail_planner_cmdline_free(&this->cmdline);
    rail_planner_cmdline_init(&this->cmdline);
    rail_planner_cmdline_configfile(filename, &this->cmdline, false, false, false);
    free(filename);
  }

  // Get the intial pose
  if (this->cmdline.start_given)
  {
    int numArgs;
    int segmentId, laneId, waypointId;
    float px, py, ph;
    PlanGraphNode *wp;

    // Load the waypoint; the pose fields are optional
    px = py = ph = 0;
    numArgs = sscanf(this->cmdline.start_arg, "%d.%d.%d %f%f%f", 
                     &segmentId, &laneId, &waypointId, &px, &py, &ph);
    if (numArgs < 3)
      return ERROR("syntax error in [%s]", this->cmdline.start_arg);

    // Get the waypoint
    wp = this->graph->getWaypoint(segmentId, laneId, waypointId);
    if (!wp)
      return MSG("unknow waypoint %d.%d.%d", segmentId, laneId, waypointId);

    // Convert from waypoint frame to site frame
    pose.pos = vec2f_set(px, py);
    pose.rot = ph * M_PI/180;
    pose = pose2f_mul(wp->pose, pose);

    // Set the initial state value
    this->vehState.siteNorthing = pose.pos.x;
    this->vehState.siteEasting = pose.pos.y;
    this->vehState.siteYaw = pose.rot;

    MSG("start %s", this->cmdline.start_arg);
  }
  
  // Get the goal pose
  if (this->cmdline.goal_given)
  {
    int numArgs;
    int segmentId, laneId, waypointId;

    // Load the waypoint; the pose fields are optional
    numArgs = sscanf(this->cmdline.goal_arg, "%d.%d.%d", 
                     &segmentId, &laneId, &waypointId);
    if (numArgs < 3)
      return ERROR("syntax error in [%s]", this->cmdline.start_arg);

    // Get the goal node
    this->goalNode = this->graph->getWaypoint(segmentId, laneId, waypointId);
    if (!this->goalNode)
      return MSG("unknow waypoint %d.%d.%d", segmentId, laneId, waypointId);
  }

  // Load the corridor
  this->corridor.clear();
  for (i = 0; i < (int) this->cmdline.corridor_given; i++)
  {
    int numArgs;
    int segmentId, laneId, waypointId;
    RNDFGraphWaypoint *wp;

    numArgs = sscanf(this->cmdline.corridor_arg[i], "%d.%d.%d", &segmentId, &laneId, &waypointId);
    if (numArgs < 3)
    {
      MSG("syntax error in [%s]", this->cmdline.corridor_arg[i]);
      continue;
    }
    MSG("corridor %d.%d.%d", segmentId, laneId, waypointId);

    wp = this->graph->rndf.getWaypoint(segmentId, laneId, waypointId);
    if (!wp)
    {
      MSG("unknown waypoint %d.%d.%d", segmentId, laneId, waypointId);
      continue;
    }

    this->corridor.push_back(wp);
  }
  
  // Make space for obstacles
  this->numObs = 0;
  if (this->maxObs < (int) this->cmdline.obs_given)
  {
    this->maxObs = this->cmdline.obs_given + this->cmdline.car_given;
    this->obs = (Obstacle*) calloc(this->maxObs, sizeof(this->obs[0]));
  }
  
  // Load obstacles
  for (i = 0; i < (int) this->cmdline.obs_given; i++)
  {
    int numArgs;
    int segmentId, laneId, waypointId;
    float px, py, ph, sx, sy;
    PlanGraphNode *wp;

    px = py = ph = 0;

    // Load the waypoint, size and pose; the pose fields are optional
    numArgs = sscanf(this->cmdline.obs_arg[i], "%d.%d.%d %fx%f %f%f%f",
                     &segmentId, &laneId, &waypointId, &sx, &sy, &px, &py, &ph);
    if (numArgs < 5)
    {
      MSG("syntax error in [%s]", this->cmdline.obs_arg[i]);
      continue;
    }

    MSG("obs %d.%d.%d  %f %f %f  %f x %f",
        segmentId, laneId, waypointId, px, py, ph, sx, sy);

    // Get the waypoint
    wp = this->graph->getWaypoint(segmentId, laneId, waypointId);
    if (!wp)
    {
      MSG("unknown waypoint %d.%d.%d", segmentId, laneId, waypointId);
      continue;
    }

    // Convert from waypoint frame to site frame
    pose.pos = vec2f_set(px, py);
    pose.rot = ph * M_PI/180;
    pose.pos = vec2f_add(wp->pose.pos, pose.pos);

    // Construct the obstacle
    obs = this->obs + this->numObs++;
    obs->isCar = false;
    obs->pose.pos.x = pose.pos.x;
    obs->pose.pos.y = pose.pos.y;
    obs->pose.rot = pose.rot;
    obs->size.x = sx;
    obs->size.y = sy;
  }

  // Load cars
  for (i = 0; i < (int) this->cmdline.car_given; i++)
  {
    int numArgs;
    int segmentId, laneId, waypointId;
    float px, py, ph, sx, sy;
    PlanGraphNode *wp;

    px = py = ph = 0;

    // Load the waypoint, size and pose; the pose fields are optional
    numArgs = sscanf(this->cmdline.car_arg[i], "%d.%d.%d %fx%f %f%f%f",
                     &segmentId, &laneId, &waypointId, &sx, &sy, &px, &py, &ph);
    if (numArgs < 5)
    {
      MSG("syntax error in [%s]", this->cmdline.car_arg[i]);
      continue;
    }

    MSG("car %d.%d.%d  %f %f %f  %f x %f",
        segmentId, laneId, waypointId, px, py, ph, sx, sy);

    // Get the waypoint
    wp = this->graph->getWaypoint(segmentId, laneId, waypointId);
    if (!wp)
    {
      MSG("unknown waypoint %d.%d.%d", segmentId, laneId, waypointId);
      continue;
    }

    // Convert from waypoint frame to site frame
    pose.pos = vec2f_set(px, py);
    pose.rot = ph * M_PI/180;
    pose = pose2f_mul(wp->pose, pose);

    // Construct the obstacle
    obs = this->obs + this->numObs++;
    obs->isCar = true;
    obs->pose.pos.x = pose.pos.x;
    obs->pose.pos.y = pose.pos.y;
    obs->pose.rot = pose.rot;
    obs->size.x = sx;
    obs->size.y = sy;
  }

  return 0;
}


// Update the planner
int RailPlannerViewer::update()
{
  uint64_t time;
  pose2f_t pose;
  float steering;
  VehicleState *state;

  state = &this->vehState;
  
  // Get the pose
  pose.pos = vec2f_set(state->siteNorthing, state->siteEasting);
  pose.rot = state->siteYaw;

  // Current steering angle
  steering = this->actState.m_steerpos * VEHICLE_MAX_AVG_STEER;

  // Update the ROI
  time = DGCgettime();
  this->graph->updateROI(pose.pos.x, pose.pos.y, this->roiSize);
  time = DGCgettime() - time;
  MSG("roi time %.3fms", (double) time / 1e3);

  // Pick a starting node
  this->startNode = NULL;

  // TESTING
  //if (this->path.pathLen > 1)
  //  this->startNode = PathUtils::getNearestNodeProjection(&this->path, pose, 1.0, 1.0);

  if (this->startNode == NULL)
  {
    // Set the updater mode
    this->updater->setMode(this->allowOncoming, this->allowReverse, false);
  
    // Update the vehicle subgraph
    time = DGCgettime();
    this->startNode = this->updater->update(pose, steering, this->goalNode);
    time = DGCgettime() - time;
    MSG("updater time %.3fms", (double) time / 1e3);
  }

  bool evasive = false;
  if (evasive)
  {
    pose2f_t evasionPose;
    evasionPose.pos.x = this->startNode->pose.pos.x + 25.0*cos(this->startNode->pose.rot) - 8.0*sin(this->startNode->pose.rot);
    evasionPose.pos.y = this->startNode->pose.pos.y + 25.0*sin(this->startNode->pose.rot) + 8.0*cos(this->startNode->pose.rot);
    evasionPose.rot = this->startNode->pose.rot;
    this->startNode = this->updater->genEvasive(pose, steering, evasionPose, 8.0, 4.0);
    uint16_t include = PLAN_GRAPH_NODE_VOLATILE;
    uint16_t exclude = PLAN_GRAPH_NODE_ONCOMING | PLAN_GRAPH_NODE_LANE | PLAN_GRAPH_NODE_ZONE;
    this->goalNode = this->graph->getNearestPos(evasionPose.pos.x, evasionPose.pos.y, 1.0, include, exclude);
  }

  assert(this->startNode);
   
  // Get a node from the graph; this is for testing only
  if (this->startNode == NULL)
  {
    uint16_t include, exclude;
    include = PLAN_GRAPH_NODE_LANE | PLAN_GRAPH_NODE_TURN | PLAN_GRAPH_NODE_VEHICLE;
    exclude = PLAN_GRAPH_NODE_ONCOMING;
    this->startNode = this->graph->getNearestPos(pose.pos.x, pose.pos.y, 1.0, include, exclude);
  } 
  
  // Update the graph status values (collisions)
  time = DGCgettime();
  this->updateStatus();
  time = DGCgettime() - time;
  MSG("state time %.3fms", (double) time / 1e3);

  // Load the planner weights
  if (this->planner->loadWeights(this->cmdline.weights_arg))
    MSG("unable to load %s", this->cmdline.weights_arg);

  // Set the corridor
  if (this->corridor.size() > 0)
    this->planner->setCorridor(&this->corridor);
  else
    this->planner->setCorridor(NULL);
  
  // Set the planner mode
  this->planner->setMode(allowOncoming, allowReverse);

  // Set the vehicle speed
  if (this->cmdline.speed_given)
    this->planner->setSpeed(this->cmdline.speed_arg);

  // Set evasive mode
  this->planner->setGoalRadius(2.0, evasive);

  // Unfix the current path (which will set the path length to zero
  // and may delete nodes).  We are about to re-generate it anyway.
  this->path.unfix(this->graph);

  time = DGCgettime();
  this->planner->planPath(this->startNode, PLAN_GRAPH_PATH_FWD, this->goalNode, &this->path);
  time = DGCgettime() - time;
  MSG("plan %s time %.3fms dist %.3f %.3f",
      (this->planner->goalFound ? "success" : "failure"), (double) time / 1e3,
      this->planner->forwardDist, this->planner->reverseDist);

  this->path.fix(this->graph);

  this->dirty = true; 
  
  return 0;
}


// Update the graph state
int RailPlannerViewer::updateStatus()
{
  int i;
  Obstacle *obs;
  float ox, oy, sx, sy;
  VehicleState *state;
  vec2f_t pa, pb;

  state = &this->vehState;

  // Set the age of the data we about to set
  this->graph->setStatusTime(DGCgettime());

  // Set the bounding box dimensions
  this->status->setOuterBuffer(this->cmdline.front_dist_arg,
                               this->cmdline.rear_dist_arg,
                               this->cmdline.side_dist_arg);

  // Project each obstacle into the ROI.
  for (i = 0; i < this->numObs; i++)
  {
    obs = this->obs + i;

    // Get the bounding box for the obstacle (site frame)
    this->getSiteBox(obs->pose, obs->size, &ox, &oy, &sx, &sy);
    
    // Check the bounding box against the graph; if there are no nodes
    // that could possible collide with this bounding box, ignore the
    // obstacle.
    if (!this->status->checkBox(ox, oy, sx, sy))
      continue;

    //MSG("testing obstacle %d/%d", i, this->numObs);
    
    // Test each line in the polygonal obstacle
    pa = vec2f_transform(obs->pose, vec2f_set(-obs->size.x/2, -obs->size.y/2));
    pb = vec2f_transform(obs->pose, vec2f_set(+obs->size.x/2, -obs->size.y/2));
    this->status->updateLine(pa, pb, !obs->isCar, obs->isCar);

    pa = vec2f_transform(obs->pose, vec2f_set(+obs->size.x/2, -obs->size.y/2));
    pb = vec2f_transform(obs->pose, vec2f_set(+obs->size.x/2, +obs->size.y/2));
    this->status->updateLine(pa, pb, !obs->isCar, obs->isCar);

    pa = vec2f_transform(obs->pose, vec2f_set(+obs->size.x/2, +obs->size.y/2));
    pb = vec2f_transform(obs->pose, vec2f_set(-obs->size.x/2, +obs->size.y/2));
    this->status->updateLine(pa, pb, !obs->isCar, obs->isCar);

    pa = vec2f_transform(obs->pose, vec2f_set(-obs->size.x/2, +obs->size.y/2));
    pb = vec2f_transform(obs->pose, vec2f_set(-obs->size.x/2, -obs->size.y/2));
    this->status->updateLine(pa, pb, !obs->isCar, obs->isCar);
  }
  
  return 0;
}


// Get the bounding box for the obstacle (site frame)
void RailPlannerViewer::getSiteBox(pose2f_t pose, vec2f_t size,
                                   float *ox, float *oy, float *sx, float *sy)
{
  vec2f_t p;
  float ax, ay, bx, by;

  ax = +FLT_MAX;
  bx = -FLT_MAX;
  ay = +FLT_MAX;
  by = -FLT_MAX;
    
  p = vec2f_set(-size.x/2, -size.y/2);
  p = vec2f_transform(pose, p);    
  ax = MIN(ax, p.x);
  bx = MAX(bx, p.x);
  ay = MIN(ay, p.y);
  by = MAX(by, p.y);

  p = vec2f_set(+size.x/2, -size.y/2);
  p = vec2f_transform(pose, p);    
  ax = MIN(ax, p.x);
  bx = MAX(bx, p.x);
  ay = MIN(ay, p.y);
  by = MAX(by, p.y);

  p = vec2f_set(+size.x/2, +size.y/2);
  p = vec2f_transform(pose, p);    
  ax = MIN(ax, p.x);
  bx = MAX(bx, p.x);
  ay = MIN(ay, p.y);
  by = MAX(by, p.y);

  p = vec2f_set(-size.x/2, +size.y/2);
  p = vec2f_transform(pose, p);    
  ax = MIN(ax, p.x);
  bx = MAX(bx, p.x);
  ay = MIN(ay, p.y);
  by = MAX(by, p.y);

  *ox = (ax + bx) / 2;
  *oy = (ay + by) / 2;
  *sx = (bx - ax);
  *sy = (by - ay);

  return;
}


/* REMOVE
// Check for any nodes in the obstacle bounding box (site frame)
bool RailPlannerViewer::checkBox(PlanGraphQuad *quad, float ox, float oy, float sx, float sy)
{
  int i;
  PlanGraphQuad *leaf;
  PlanGraphNode *node;
  
  // Check for intersection between the quad and the box.
  if (!quad->hasIntersection(ox, oy, sx, sy))
    return false;

  // Check our children
  for (i = 0; i < 4; i++)
  {
    leaf = quad->leaves[i];
    if (!leaf)
      continue;    
    if (this->checkBox(leaf, ox, oy, sx, sy))
      return true;
  }

  // Check our static nodes to see if any fall inside the bounding box
  for (i = 0; i < quad->numNodes; i++)
  {
    node = quad->nodes[i];
    if (node->pose.pos.x < ox - sx/2)
      continue;
    if (node->pose.pos.x > ox + sx/2)
      continue;
    if (node->pose.pos.y < oy - sy/2)
      continue;
    if (node->pose.pos.y > oy + sy/2)
      continue;
    return true;
  }

  return false;
}


// Update the status values along a line
void RailPlannerViewer::updateLineStatus(vec2f_t pa, vec2f_t pb, bool obs, bool car)
{
  int i, numSteps;
  float length, spacing;
  float px, py, dx, dy;

  // MAGIC
  // Spacing of the test points
  spacing = 0.5;
  
  length = vec2f_mag(vec2f_sub(pb, pa));
  numSteps = (int) (ceil(length / spacing));
  assert(numSteps > 0);

  // Compute line parameters
  px = pa.x;
  py = pa.y;
  dx = (pb.x - pa.x) / numSteps;
  dy = (pb.y - pa.y) / numSteps;

  // Walk the line
  for (i = 0; i < numSteps; i++)
  {
    //printf("line %d %f %f\n", i, px, py);    
    this->updateQuadStatus(this->graph->root, px, py, obs, car);
    px += dx;
    py += dy;
  }
  
  return;
}


// Update the status values for intersecting nodes
void RailPlannerViewer::updateQuadStatus(PlanGraphQuad *quad,
                                         float ox, float oy, bool obs, bool car)
{
  int i;
  PlanGraphQuad *leaf;
  PlanGraphNode *node;
  float px, py;
  float m[3][3];
        
  // See of the quad can contain any node that overlaps this point.
  if (!quad->hasIntersection(ox, oy, 2*this->aliceOuterDiam, 2*this->aliceOuterDiam))
    return;

  // Check our children
  for (i = 0; i < 4; i++)
  {
    leaf = quad->leaves[i];
    if (!leaf)
      continue;    
    this->updateQuadStatus(leaf, ox, oy, obs, car);
  }

  // Check our nodes.
  for (i = 0; i < quad->numNodes; i++)
  {
    node = quad->nodes[i];

    // Compute transform from site frame to node frame.  If the value
    // has never been set, do it now.
    if (node->status.transTime == 0)
    {
      pose2f_to_mat33f(pose2f_inv(node->pose), m);
      memcpy(node->status.trans[0], m[0], sizeof(m[0]));
      memcpy(node->status.trans[1], m[1], sizeof(m[1]));
      node->status.transTime = this->graph->getStatusTime();
    }

    // Transform point into the node frame.
    px = node->status.trans[0][0]*ox + node->status.trans[0][1]*oy + node->status.trans[0][2];
    py = node->status.trans[1][0]*ox + node->status.trans[1][1]*oy + node->status.trans[1][2];

    // Update obstacle distances
    if (!this->graph->isStatusFresh(node->status.obsDistTime))
    {
      node->status.obsSideDist = fabsf(py) - this->aliceInnerSide;
      node->status.obsFrontDist = this->aliceOuterFront - this->aliceInnerFront;
      node->status.obsRearDist = this->aliceOuterRear - this->aliceInnerRear;
      node->status.obsDistTime = this->graph->getStatusTime();
    }
    if (py > -this->aliceInnerSide && py < +this->aliceInnerSide)
    {
      if (px > 0)
        node->status.obsFrontDist = MIN(node->status.obsFrontDist, +px - this->aliceInnerFront);
      else
        node->status.obsRearDist = MIN(node->status.obsRearDist, -px - this->aliceInnerRear);
    }
    node->status.obsSideDist = MIN(node->status.obsSideDist, fabsf(py) - this->aliceInnerSide);
      
    // Ignore points outside the Alice box
    if (px > +this->aliceOuterFront)
      continue;
    if (px < -this->aliceOuterRear)
      continue;
    if (py > +this->aliceOuterSide)
      continue;
    if (py < -this->aliceOuterSide)
      continue;

    // Update the node
    if (obs)
    {
      node->status.obsCollision = true;
      node->status.obsTime = this->graph->getStatusTime();
    }
    if (car)
    {
      node->status.carCollision = true;
      node->status.carTime = this->graph->getStatusTime();
    }
  }    
  
  return;
}
*/


int main(int argc, char *argv[])
{
  RailPlannerViewer *app;
 
  // Initialize GLUT calls
  glutInit(&argc, argv);

  app = new RailPlannerViewer();

  // Parse cmd line
  if (app->parseCmdLine(argc, argv) != 0)
    return -1;

  // Initialize gui
  if (app->initGUI(1024, 768) != 0)
    return -1;

  // Initilize the app
  if (app->init() != 0)
    return -1;

  // Idle callback
  Fl::add_idle((void (*) (void*)) RailPlannerViewer::onIdle, app);
    
  // Run
  app->mainwin->show();
  while (!app->quit)
    Fl::wait();

  MSG("exiting");
  
  // Clean up
  app->fini();
  app->finiGUI();
  delete app;
 
  MSG("exited cleanly");
  
  return 0;
}
