
/* 
 * Desc: Planner viewer drawing functions
 * Date: 29 Sep 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <GL/glut.h>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>
#include <frames/mat44.h>

#include "RailPlannerViewer.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, "%s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Pre-draw the graph to a display list
void RailPlannerViewer::predrawGraph(PlanGraph *graph, VehicleState *state, int props)
{
  float px, py, size;
  uint16_t include, exclude;

  px = state->siteNorthing;
  py = state->siteEasting;
  size = 128; // MAGIC

  // Select the type of nodes to display
  include = PLAN_GRAPH_NODE_LANE | PLAN_GRAPH_NODE_TURN;
  exclude = PLAN_GRAPH_NODE_ONCOMING;

  if (graphProps & (1 << CMD_GRAPH_VEHICLE))
    include |= PLAN_GRAPH_NODE_VEHICLE;

  if (graphProps & (1 << CMD_GRAPH_LANE_CHANGES))
  {
    include |= PLAN_GRAPH_NODE_LANE_CHANGE | PLAN_GRAPH_NODE_RAIL_CHANGE;
    exclude &= ~PLAN_GRAPH_NODE_ONCOMING;
  }

  // Draw the graph structure
  this->graphList = this->graph->predrawStruct(px, py, size, include, exclude);

  // Draw the graph status
  this->statusList = this->graph->predrawStatus(px, py, size, include, exclude);

  return;
}


// Pre-draw obstacles
void RailPlannerViewer::predrawObs()
{
  int i;
  Obstacle *obs;

  // Create display list
  if (this->obsList == 0)
    this->obsList = glGenLists(1);
  glNewList(this->obsList, GL_COMPILE);
  
  glColor3f(1, 0, 0);
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  for (i = 0; i < this->numObs; i++)
  {
    obs = this->obs + i;

    glPushMatrix();
    glTranslatef(obs->pose.pos.x, obs->pose.pos.y, 0);
    glRotatef(obs->pose.rot * 180/M_PI, 0, 0, 1);

    glBegin(GL_QUADS);
    glVertex2f(-obs->size.x/2, -obs->size.y/2);
    glVertex2f(+obs->size.x/2, -obs->size.y/2);
    glVertex2f(+obs->size.x/2, +obs->size.y/2);
    glVertex2f(-obs->size.x/2, +obs->size.y/2);
    glEnd();
    
    glPopMatrix();
  }

  glEndList();
  
  return;
}


// Draw the path (site frame)
void RailPlannerViewer::drawPath(PlanGraphPath *path)
{
  int i;
  PlanGraphNode *nodeA, *nodeB;

  glLineWidth(2);
  
  glBegin(GL_LINES);
  for (i = 0; i < path->pathLen - 1; i++)
  {
    nodeA = path->nodes[i];
    nodeB = path->nodes[i + 1];

    //if (path->collideObs)
    //  glColor3f(1, 1, 0);
    //else if (path->collideCar)
    //  glColor3f(1, 0.5, 0);
    if (path->directions[i + 1] == PLAN_GRAPH_PATH_REV)
      glColor3f(1, 0, 0);
    else
      glColor3f(0, 1, 1);
    
    glVertex3f(nodeA->pose.pos.x, nodeA->pose.pos.y, -0.5);
    glVertex3f(nodeB->pose.pos.x, nodeB->pose.pos.y, -0.5);
  }
  glEnd();

  return;
}
