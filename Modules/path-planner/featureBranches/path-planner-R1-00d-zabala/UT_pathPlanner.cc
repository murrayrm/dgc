/**********************************************************
 **
 **  UT_PLANNER.CC
 **
 **    Author: Noel du Toit
 **    Created: Mon Jul 16 16:20
 **
 **
 **********************************************************
 **
 **  Unit test for the planner stack
 **
 **********************************************************/

#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <graph-updater/GraphUpdater.hh>
#include <interfaces/VehicleState.h>
#include <interfaces/ActuatorState.h>
#include <frames/pose3.h>
#include <planner/CmdArgs.hh>
#include <iostream>
#include <sstream>
#include <fstream>
#include <trajutils/traj.hh>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>
#include <skynet/skynet.hh>

#include "PathPlanner.hh"


using namespace std;

int main(int argc, char **args)
{
  // INITIALIZE THE MAP
  Map* map = new Map();
  map->loadRNDF(args[1]);
  map->prior.delta = point2(3778410.5, 403942.3);

  // INITIALIZE THE GRAPH
  Graph_t* graph;
  //  GraphNode* node;
    
  CmdArgs::RNDF_file = args[1];
  CmdArgs::sn_key = skynet_findkey(argc, args);
  GraphUpdater::init(&graph, map);

  // PLAN PATH
  Path_t path;
  memset(&path, 0 ,sizeof(path));
  VehicleState vehState;
  memset(&vehState, 0, sizeof(vehState));
  ActuatorState actState;
  memset(&actState, 0, sizeof(actState));
  Cost_t cost;
  pose3_t finPose;
  double roll, pitch, yaw;

  // set the vehicle state to the first waypoint
  GraphNode* initNode = graph->getNodeFromRndfId(1,1,1);
  vehState.localX = initNode->pose.pos.x;
  vehState.localY = initNode->pose.pos.y;
  vehState.localZ = initNode->pose.pos.z;
  quat_to_rpy(initNode->pose.rot, &roll, &pitch, &yaw);
  vehState.localYaw = yaw;
  //  vehState.localX = 0;
  //  vehState.localY = .05;
  //  vehState.localZ = 0;
  //  quat_to_rpy(initNode->pose.rot, &roll, &pitch, &yaw);
  //  vehState.localYaw = -0.79;
  
  // set final pose
  GraphNode* finalNode = graph->getNodeFromRndfId(2,2,28);
  finPose = finalNode->pose;
  //  finPose.pos.x = 107.2;
  // finPose.pos.y = -32.4;
  // finPose.pos.z = 0;
  // finPose.rot = quat_from_rpy(0, 0, -0.79);

  // plan the path
  Path_params_t pathParams;
  PathPlanner::init();
  GraphUpdater::genVehicleSubGraph(graph, vehState, actState);
  PathPlanner::planPath(&path, cost, graph, finPose, pathParams);
  
  // DISPLAY INFORMATION IN MAPVIEWER
  if (1) {
    // DISPLAY GRAPH
    GraphUpdater::display(10, graph, vehState);
    
    // DISPLAY PATH
    PathPlanner::display(10, &path);
  }
}
