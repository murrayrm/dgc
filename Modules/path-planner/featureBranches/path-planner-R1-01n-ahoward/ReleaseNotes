	Release Notes for "path-planner" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "path-planner" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "path-planner" module can be found in
the ChangeLog file.

Release R1-01n (Sat Oct 20 12:26:35 2007):
	* removed reversing the heading when we go in reverse- it is now always yaw rather than the direction we're going  some logging added
	* changed set of clothoids used in road region

Release R1-01m (Tue Oct 16 21:31:09 2007):
	* Added support for driving in reverse
	* Added cost for driving off-road
	* Only allow reverse to connect to vehicle nodes.
	* Added optimized graph status update
	* Support for off-road

Release R1-01l (Mon Oct 15 23:39:08 2007):
	* Added viewer utility for railplanner
	* implemented hard/soft constraint switching from 
path-plan.config file. Both are working, hard constraint is the 
default.

Release R1-01k (Mon Oct 15  7:59:01 2007):
	* Implemented logic for gen veh subgraph when planFromCurrPos=true.
	* updated getNearestNode fcns.
	* Added freshness check for obstacle along the path
	* Taking into accound the feasibility og man's in cost
	* Set distance along path correctly
	* Implemented lane changes as hard constraints (currently disabled).

Release R1-01j (Thu Oct 11  0:30:15 2007):
  Major release, fully integrated with the planner module.

Release R1-01i (Tue Oct  9 16:55:09 2007):
  Added a new RailPlanner class (compatible with PlanGraph) to replace the 
  existing PathPlanner class.  

Release R1-01h (Sat Oct  6 17:18:04 2007):
	* Updated our handling of not planning from the current position to make it more robust (while I fix the genVehSubgraph).
	* fixed a bug in astar that stopped us from sometimes finding a path
        when using the volatile nodes. Also fixed a bug in the
        multiresolution graph searches.

Release R1-01g (Tue Oct  2 16:00:08 2007):
	Fixed the heuristic in A* again.
	A* is now even faster (not using a vector anymore for the closed list).
	Using AliceStateHelper (for the site coordinates).

Release R1-01f (Tue Oct  2 11:57:43 2007):
	Moved display and print functions to Utils.
	Updated function calls for above change.
	Commented out the UT in the PROJ_BINS to speed up compiling. 
	To use UT, just uncomment (easier than maintaining deprecated UT's)

Release R1-01e (Sun Sep 30 14:40:27 2007):
	Fixed the heuristic in A* (still have issues of exploring more than 10'000 nodes sometimes)

Release R1-01d (Sat Sep 29  9:46:09 2007):
	Updated the heuristic and arc_cost function in A*

Release R1-01c (Fri Sep 28 16:38:13 2007):
	Now reads path-planner params (costs) from a file. This can be re-read when requested from
	the console (by hitting "u").

Release R1-01b (Thu Sep 27 21:17:01 2007):
	Centerdist cost adjustments based on field test.

Release R1-01a (Thu Sep 27 16:57:53 2007):
	Implemented A*: search time on the first segment of victorvilleAllSegs is less than 1 ms.
	Cycle time of planner is now less than 0.1 second.

Release R1-01 (Thu Sep 27 10:45:39 2007):
	Added skeleton for the A* algorithm

Release R1-00z (Wed Sep 26 20:49:10 2007):
	Tweaked the cost associated with the dist from centerline a little.

Release R1-00y (Wed Sep 26 11:10:50 2007):
	Propagating changes from temp-planner-interfaces.

Release R1-00x (Thu Aug 30 18:10:46 2007):
	Changed the display and print functions a little. No new functionality.

Release R1-00w (Wed Aug 29 16:53:25 2007):
	Added a print function to assist with debugging.
	MAde the display function more robust.

Release R1-00v (Wed Aug 29 14:18:57 2007):
	Increased cost of navigating on a left or right rail

Release R1-00u (Wed Aug 29  2:40:39 2007):
	Updated the path display function to take an optional arg specifying which color to display in MapViewer.
	Changed the mapId being sent.

Release R1-00t (Mon Aug 27 10:35:34 2007):
	Removed link to planner.  

Release R1-00s (Fri Aug 24 11:25:32 2007):
	Added left and right node to the goals to reach

Release R1-00r (Thu Aug 23 16:59:09 2007):
	Fixed bug when searching destination of an intersection

Release R1-00q (Wed Aug 22 18:19:29 2007):
	Not returning PP_COLLISION if collision occurs too far away

Release R1-00p (Tue Aug 21 17:42:32 2007):
	Changed cost of going through cars when passing
	Ability to search for multiple destination (useful at intersections)

Release R1-00o (Tue Aug 21 11:21:17 2007):
	Fixed compile issues from previous release. 

Release R1-00n (Mon Aug 20 19:26:09 2007):
	Resolved connectivity issues experienced at St. Luke

Release R1-00m (Wed Aug 15 17:57:17 2007):
	Accelerated the search algorithm by correcting the termination condition

Release R1-00l (Tue Aug 14 13:17:33 2007):
	Added multiple rails support

Release R1-00k (Tue Aug 14 12:10:02 2007):

Release R1-00j (Mon Aug 13 12:58:27 2007):
	Adjusted Makefile to work with the new console

Release R1-00i (Mon Aug  6 20:27:51 2007):
	Fixed bug creating a loop path when in pause

Release R1-00h (Fri Aug  3 20:39:18 2007):
	Changed the way we find the node on the path the plan from: added a
        max dist that this node can be away. Also, when we do not find this
        node, simply find the node on the path closest to Alice to plan
        from.
	Fixed multi-resolution bug

Release R1-00g (Fri Aug  3  0:45:56 2007):
	Increased maximum cost
	Fixed return values

Release R1-00f (Tue Jul 31 19:56:19 2007):
	Propagating changes to temp-planner-interfaces

Release R1-00e (Fri Jul 27 16:35:40 2007):
	Implemented path search continuity to search from a node on 

the previous path (if requested).
	Implemented multi-resolution graph search.

Release R1-00d (Thu Jul 19 18:43:13 2007):
	Modified display function

Release R1-00c (Mon Jul 16 20:56:58 2007):
	Ported the path planner functions from the graph-planner. Path 
planner is now independent of graph-planner. 
	Created a unit test that creates a graph from a RNDF and then 
searches the graph.

Release R1-00b (Mon Jul 16 13:45:50 2007):
	Now using Graph as Graph_t

Release R1-00a (Fri Jul 13 16:32:17 2007):
	Initial implementation of the path planner. Currently this is 
only wrapper functions around the graph-planner functions. These 
functions live in temp-planner-interfaces (for the time being).

Release R1-00 (Tue Jul 10 17:00:42 2007):
	Created.














































