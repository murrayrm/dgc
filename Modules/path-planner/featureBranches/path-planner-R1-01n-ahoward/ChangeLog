Sat Oct 20 12:26:22 2007	Noel duToit (ndutoit)

	* version R1-01n
	BUGS:  
	New files: eltoro_speed_test.cfg
	FILES: Makefile.yam(45540), RailPlanner.cc(45540),
		RailPlannerViewer.cc(45540), RailPlannerViewer.hh(45540),
		RailPlannerViewerDraw.cc(45540)
	Switched drawing functions to use library

	FILES: RailPlanner.cc(45202), RailPlanner.hh(45202)
	Field hacks to termination conditions; test and tidy

	FILES: RailPlanner.cc(45362)
	To check whether the current explored node is the goal, we check
	that node->waypoint == waypoint we want to go to

	FILES: RailPlanner.cc(45381), RailPlannerViewer.cc(45381),
		RailPlannerViewer.hh(45381),
		RailPlannerViewerDraw.cc(45381), UT_RailPlanner.cc(45381)
	Some hacks for testing tomorrow; the RNDF heuristic needs to be
	fixed for driving in reverse

	FILES: RailPlanner.cc(45403)
	Using previous waypoint test for termination

	FILES: RailPlanner.cc(45472), RailPlanner.hh(45472),
		RailPlannerViewer.cc(45472),
		RailPlannerViewerDraw.cc(45472), UT_RailPlanner.cc(45472),
		rail_planner_viewer_cmdline.ggo(45472)
	Added speed-dependent curvature cost

	FILES: RailPlanner.cc(45477)
	Doc changes

	FILES: RailPlanner.cc(45596), RailPlanner.hh(45596),
		RailPlannerViewer.cc(45596), UT_RailPlanner.cc(45596)
	Added direction for initial state

	FILES: RailPlannerViewer.cc(45100), RailPlannerViewer.hh(45100),
		RailPlannerViewerDraw.cc(45100)
	Display tweaks

Tue Oct 16 21:31:00 2007	Noel duToit (ndutoit)

	* version R1-01m
	BUGS:  
	FILES: RailPlanner.cc(44997), RailPlanner.hh(44997),
		RailPlannerViewer.cc(44997)
	Added support for driving in reverse

	FILES: RailPlanner.cc(45007), RailPlanner.hh(45007),
		RailPlannerViewer.cc(45007)
	Added cost for driving off-road

	FILES: RailPlanner.cc(45025)
	Only allow reverse to connect to vehicle nodes.

	FILES: RailPlannerViewer.cc(44961), RailPlannerViewer.hh(44961),
		RailPlannerViewerDraw.cc(44961),
		rail_planner_viewer_cmdline.ggo(44961)
	Added optimized graph status update

	FILES: RailPlannerViewer.cc(44985), RailPlannerViewer.hh(44985),
		RailPlannerViewerDraw.cc(44985),
		rail_planner_viewer_cmdline.ggo(44985)
	Support for off-road

Mon Oct 15 23:39:02 2007	Noel duToit (ndutoit)

	* version R1-01l
	BUGS:  
	New files: RailPlannerViewer.cc RailPlannerViewer.hh
		RailPlannerViewerDraw.cc rail_planner_viewer_cmdline.ggo
	FILES: Makefile.yam(44773)
	Added viewer utility for the rail planner

	FILES: Makefile.yam(44837)
	Added viewer utility for rail planner

	FILES: RailPlanner.cc(44814), RailPlanner.hh(44814)
	changing lanes can now be hard or soft constraints. This is set
	from the path_plan.config file in planner. The weights have been
	adjusted for the two cases. Both seem to work reasobly well right
	now.

	FILES: RailPlanner.cc(44815), RailPlanner.hh(44815)
	added some more functionality for switching between hard and soft
	constraints.

Mon Oct 15  7:58:27 2007	Noel duToit (ndutoit)

	* version R1-01k
	BUGS:  
	FILES: Makefile.yam(44235), RailPlanner.cc(44235)
	Implemented the logic for replanning from current pos. Implemented
	error reporting from rail planner.

	FILES: Makefile.yam(44261), RailPlanner.cc(44261),
		RailPlanner.hh(44261), UT_RailPlanner.cc(44261)
	Moved path replan code to planner

	FILES: PathUtils.cc(44371), PathUtils.hh(44371)
	added function to calc dist along path to first obstacle.

	FILES: PathUtils.cc(44372)
	fixed a bug in previous commit.

	FILES: PathUtils.cc(44374), PathUtils.hh(44374)
	Added distance checks

	FILES: PathUtils.cc(44407), PathUtils.hh(44407)
	changed the getNearestProjectedPos fcn to extend the path at the
	beginning for the check along the path - got rid of the occasional
	planFromCurrPos.

	FILES: PathUtils.cc(44595), PathUtils.hh(44595)
	Added freshness check for obstacle distance calculation

	FILES: PathUtils.cc(44599), RailPlanner.cc(44599)
	Updated freshness

	FILES: RailPlanner.cc(44201)
	Fixed assert when the next waypoint field is not set

	FILES: RailPlanner.cc(44309), RailPlanner.hh(44309)
	Taking into account the feasibility of the maneuvers in the cost

	FILES: RailPlanner.cc(44318), RailPlanner.hh(44318)
	set the directions in the path correctly.

	FILES: RailPlanner.cc(44321)
	No changes

	FILES: RailPlanner.cc(44324)
	Fixed cost for obstacles

	FILES: RailPlanner.cc(44326, 44327)
	Added oncoming cost

	FILES: RailPlanner.cc(44342), RailPlanner.hh(44342)
	Reverted to RNDF heuristic

	FILES: RailPlanner.cc(44382)
	Added test for freshness

	FILES: RailPlanner.cc(44384)
	Added freshness check

	FILES: RailPlanner.cc(44429), RailPlanner.hh(44429)
	Added cost for sensed lane centers

	FILES: RailPlanner.cc(44435), RailPlanner.hh(44435)
	changed handling of lane changes to be hard constraints.

	FILES: RailPlanner.cc(44501), RailPlanner.hh(44501)
	Added experimental lazy status evaluation

	FILES: RailPlanner.cc(44603)
	Fixed path obstacle flags

Thu Oct 11  0:30:06 2007	Andrew Howard (ahoward)

	* version R1-01j
	BUGS:  
	FILES: Makefile.yam(43859), RailPlanner.cc(43859, 43888),
		RailPlanner.hh(43859), UT_RailPlanner.cc(43859)
	Tweaks

	FILES: Makefile.yam(43883), PathUtils.cc(43883),
		PathUtils.hh(43883), RailPlanner.cc(43883),
		RailPlanner.hh(43883), UT_RailPlanner.cc(43883)
	Port to new graph structure

	FILES: RailPlanner.cc(43932)
	Commented out assert; needs fixing

Tue Oct  9 16:55:06 2007	 (abhoward)

	* version R1-01i
	BUGS:  
	New files: RailPlanner.cc RailPlanner.hh UT_RailPlanner.cc
	FILES: Makefile.yam(43520)
	Experimental version of the A* rail planner

	FILES: Makefile.yam(43805)
	Build tweaks

Sat Oct  6 17:18:00 2007	Christian Looman (clooman)

	* version R1-01h
	BUGS:  
	FILES: PathPlanner.cc(43084)
	Updated our handling of not planning from the current position to
	make it more robust (while I fix the genVehSubgraph).

	FILES: PathPlanner.cc(43149)
	minor changes.

	FILES: PathPlanner.cc(43199), PathPlanner.hh(43199)
	fixed a bug in astar that stopped us from sometimes finding a path
	when using the volatile nodes. Also fixed a bug in the
	multiresolution graph searches.

Tue Oct  2 16:03:26 2007	Sven Gowal (sgowal)

	* version R1-01g
	BUGS:  
	FILES: PathPlanner.cc(42321), PathPlanner.hh(42321),
		UT_pathPlanner.cc(42321)
	Merged

	FILES: PathPlanner.cc(42189), PathPlanner.hh(42189)
	Made A* faster (can now explore 150'000+ nodes in 1 second)

	FILES: PathPlanner.cc(42314), UT_pathPlanner.cc(42314)
	A* is now faster. Uses AliceStateHelper now.

Tue Oct  2 11:57:35 2007	Noel duToit (ndutoit)

	* version R1-01f
	BUGS:  
	FILES: Makefile.yam(42219), PathPlanner.cc(42219),
		PathPlanner.hh(42219), UT_pathPlanner.cc(42219)
	Moved the display and print functions to the tpi-Utils object.
	Commented out deprecated unit test in makefile to speed up
	compilation. To use, uncomment the UT in the PROJ_BINS line.

Sun Sep 30 14:40:24 2007	Sven Gowal (sgowal)

	* version R1-01e
	BUGS:  
	FILES: PathPlanner.cc(41845)
	Added debug info

	FILES: PathPlanner.cc(41898)
	Fixed heuristic

Sat Sep 29  9:52:20 2007	Sven Gowal (sgowal)

	* version R1-01d
	BUGS:  
	FILES: PathPlanner.cc(41383)
	Merged

	FILES: PathPlanner.cc(41373)
	Updated heuristic and arc_cost functions for A*

Fri Sep 28 16:38:04 2007	Noel duToit (ndutoit)

	* version R1-01c
	BUGS:  
	FILES: Makefile.yam(41153), PathPlanner.cc(41153),
		PathPlanner.hh(41153)
	Reads the path planning costs from a file upon request.

Thu Sep 27 21:16:58 2007	Noel duToit (ndutoit)

	* version R1-01b
	BUGS:  
	FILES: PathPlanner.cc(40917)
	Field changes: addjusted costs a little.

Thu Sep 27 16:57:48 2007	Sven Gowal (sgowal)

	* version R1-01a
	BUGS:  
	FILES: PathPlanner.cc(40895), PathPlanner.hh(40895)
	Implemented A*

Thu Sep 27 10:49:40 2007	Sven Gowal (sgowal)

	* version R1-01
	BUGS:  
	FILES: PathPlanner.cc(40825), PathPlanner.hh(40825)
	Merged

	FILES: PathPlanner.cc(40818), PathPlanner.hh(40818)
	Added skeleton functions for A*

Wed Sep 26 20:49:07 2007	Noel duToit (ndutoit)

	* version R1-00z
	BUGS:  
	FILES: PathPlanner.cc(40610)
	Changed the cost for lane-updating based on distance from the
	centerline.

Wed Sep 26 11:10:45 2007	Sven Gowal (sgowal)

	* version R1-00y
	BUGS:  
	FILES: PathPlanner.cc(40484), PathPlanner.hh(40484)
	Propagating changes from temp-planner-interfaces.

Thu Aug 30 18:10:42 2007	Noel duToit (ndutoit)

	* version R1-00x
	BUGS:  
	FILES: PathPlanner.cc(36501), PathPlanner.hh(36501)
	Changed the display function a little

Wed Aug 29 20:34:12 2007	Noel duToit (ndutoit)

	* version R1-00w
	BUGS:  
	FILES: PathPlanner.cc(36400), PathPlanner.hh(36400)
	merged with the latest release of path-planner.

	FILES: PathPlanner.cc(36244), PathPlanner.hh(36244)
	Added a print function to be able to output a path to the log file

Wed Aug 29 14:18:53 2007	Sven Gowal (sgowal)

	* version R1-00v
	BUGS:  
	FILES: PathPlanner.cc(36278)
	Increased cost of navigating on a rail

Wed Aug 29  2:40:34 2007	Noel duToit (ndutoit)

	* version R1-00u
	BUGS:  
	FILES: PathPlanner.cc(36127), PathPlanner.hh(36127),
		UT_pathPlanner.cc(36127)
	updated the display function to allow you to specify the color of
	the path on MapViewer.

Mon Aug 27 10:35:32 2007	vcarson (vcarson)

	* version R1-00t
	BUGS:  
	FILES: Makefile.yam(35641)
	Removed planner link from Makefile.yam 

Fri Aug 24 11:25:29 2007	Sven Gowal (sgowal)

	* version R1-00s
	BUGS:  
	FILES: PathPlanner.cc(35416)
	Minor fix

Thu Aug 23 16:59:06 2007	Sven Gowal (sgowal)

	* version R1-00r
	BUGS:  
	FILES: PathPlanner.cc(35295)
	Fix search to multiple destination

Wed Aug 22 18:19:24 2007	Sven Gowal (sgowal)

	* version R1-00q
	BUGS:  
	FILES: PathPlanner.cc(35013), PathPlanner.hh(35013)
	No returning PP_COLLISION if the collision is occuring too far away

Tue Aug 21 17:42:28 2007	Sven Gowal (sgowal)

	* version R1-00p
	BUGS:  
	FILES: PathPlanner.cc(34682)
	Increased cost of going through cars when passing

	FILES: PathPlanner.cc(34720)
	Searching for multiple destination now

Tue Aug 21 11:21:12 2007	vcarson (vcarson)

	* version R1-00o
	BUGS:  
	FILES: PathPlanner.cc(34626), PathUtils.cc(34626),
		PathUtils.hh(34626)
	Fixed compile errors from last release. 

Mon Aug 20 19:26:04 2007	Sven Gowal (sgowal)

	* version R1-00n
	BUGS:  
	FILES: PathPlanner.cc(34378), PathPlanner.hh(34378)
	Resolved connectivity issues experienced at St. Luke

Wed Aug 15 17:57:13 2007	Sven Gowal (sgowal)

	* version R1-00m
	BUGS:  
	FILES: PathPlanner.cc(33771)
	Accelerated the backward search algorithm

Tue Aug 14 17:49:34 2007	Sven Gowal (sgowal)

	* version R1-00l
	BUGS:  
	FILES: PathPlanner.cc(33501), PathPlanner.hh(33501),
		PathUtils.cc(33501)
	merged

	FILES: PathPlanner.cc(32582)
	Fixed segfault

	FILES: PathPlanner.cc(33427), PathPlanner.hh(33427),
		PathUtils.cc(33427)
	Added multi-rail support

Tue Aug 14 12:09:52 2007	Noel duToit (ndutoit)

	* version R1-00k
	BUGS:  
	FILES: Makefile.yam(33359)
	Changed the makefile for the new cSpecs interface.

Tue Aug 14  9:27:18 2007	Christian Looman (clooman)

	* version R1-00j
	BUGS:  
	FILES: Makefile.yam(33112)
	changed makefile for new console

	FILES: Makefile.yam(33112)
	changed makefile for new console

Mon Aug  6 20:27:47 2007	Sven Gowal (sgowal)

	* version R1-00i
	BUGS:  
	FILES: PathPlanner.cc(32383), PathUtils.cc(32383)
	bug fix

Fri Aug  3 20:39:11 2007	Sven Gowal (sgowal)

	* version R1-00h
	BUGS:  
	FILES: PathPlanner.cc(31822), PathUtils.cc(31822),
		PathUtils.hh(31822)
	Changed the way we find the node on the path the plan from: added a
	max dist that this node can be away. Also, when we do not find this
	node, simply find the node on the path closest to Alice to plan
	from.

	FILES: PathPlanner.cc(31870), PathPlanner.hh(31870)
	Multi-resolution bug fix

Fri Aug  3  0:45:51 2007	Sven Gowal (sgowal)

	* version R1-00g
	BUGS:  
	FILES: PathPlanner.cc(31653), PathPlanner.hh(31653)
	Minor fixes

Tue Jul 31 19:56:16 2007	Sven Gowal (sgowal)

	* version R1-00f
	BUGS:  
	FILES: PathPlanner.cc(30855)
	Nothing

	FILES: PathPlanner.cc(30900)
	Reflected changes in temp-planner-interfaces

	FILES: PathPlanner.cc(31191)
	reverted GRAPH_ARC type

	FILES: PathPlanner.cc(31228)
	Removed output

Fri Jul 27 16:35:33 2007	Noel duToit (ndutoit)

	* version R1-00e
	BUGS:  
	New files: PathUtils.cc PathUtils.hh
	FILES: Makefile.yam(30388), PathPlanner.cc(30388),
		PathPlanner.hh(30388)
	trickling through changes in temp-planner.

	FILES: Makefile.yam(30474), PathPlanner.cc(30474),
		PathPlanner.hh(30474), UT_pathPlanner.cc(30474)
	added continuity to the path planning. Now we project our current
	location onto the previous path and use that node to plan from,
	instead of generating a path from our current pos all the time. Can
	still generate path from curr pos if necessary.

	FILES: PathPlanner.cc(30291), PathPlanner.hh(30291)
	started enforcing continuity between plans. Not done yet.

	FILES: PathPlanner.cc(30309), UT_pathPlanner.cc(30309)
	Fixed code to enable compilation

	FILES: PathPlanner.cc(30397, 30398)
	Account for arc type (basic)

	FILES: PathPlanner.cc(30411), PathPlanner.hh(30411)
	Added isInsideHiResBox function

	FILES: PathPlanner.cc(30440), PathPlanner.hh(30440)
	Added high resolution box pruning of the graph

	FILES: PathPlanner.cc(30476)
	Added some fault handling for when we do not find a node on the
	path. This still needs some work/refinement.

	FILES: PathPlanner.cc(30482), PathPlanner.hh(30482)
	changed the way the pathutils are included.

	FILES: PathPlanner.cc(30500)
	merging

	FILES: PathPlanner.cc(30501)
	merging path continuity changes with multi-res changes.

	FILES: UT_pathPlanner.cc(30392)
	trickling through changes in temp-planner-interfaces.

Thu Jul 19 18:43:07 2007	Sven Gowal (sgowal)

	* version R1-00d
	BUGS:  
	FILES: Makefile.yam(29598), PathPlanner.cc(29598)
	commented out the console function call in PathPlanner.cc line 318
	for now

	FILES: PathPlanner.cc(29545), PathPlanner.hh(29545)
	Updated cost

	FILES: PathPlanner.cc(29548)
	Updated costs for lane change and obstacles

	FILES: PathPlanner.cc(29606)
	Added some threshold before deciding that a path is colliding with
	an obstacle

	FILES: PathPlanner.cc(29615)
	Merged

	FILES: PathPlanner.cc(29621)
	Added more error flags in path planner.

	FILES: PathPlanner.cc(29708)
	Updated display

Mon Jul 16 20:56:53 2007	Noel duToit (ndutoit)

	* version R1-00c
	BUGS:  
	New files: UT_pathPlanner.cc
	FILES: Makefile.yam(29205), PathPlanner.cc(29205),
		PathPlanner.hh(29205)
	Moved graph-planner functions associated with path planning to the
	Path Planner module.

Mon Jul 16 13:45:46 2007	Francisco zabala (zabala)

	* version R1-00b
	BUGS:  
	FILES: PathPlanner.cc(29134)
	Changed Graph_t to be Graph (instead of GraphPlanner)

Fri Jul 13 16:32:15 2007	Noel duToit (ndutoit)

	* version R1-00a
	BUGS:  
	New files: PathPlanner.cc PathPlanner.hh
	FILES: Makefile.yam(28738)
	Updated Makefile

	FILES: Makefile.yam(28761)
	Moved old graph-planner files to the temp-planner-interfaces
	library and correctly link to that now. Removed the redundant
	files.

	FILES: Makefile.yam(28906)
	Added display function to now display the path on MapViewer.

Tue Jul 10 17:00:42 2007	Noel duToit (ndutoit)

	* version R1-00
	Created path-planner module.




















































