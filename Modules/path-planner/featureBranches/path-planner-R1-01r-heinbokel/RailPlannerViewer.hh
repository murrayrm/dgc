
/* 
 * Desc: Rail planner viewer utility
 * Date: 15 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef RAIL_PLANNER_VIEWER_HH
#define RAIL_PLANNER_VIEWER_HH


#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Menu_Bar.H>
#include <dgcutils/Fl_Glv_Window.H>

#include <interfaces/VehicleState.h>
#include <interfaces/ActuatorState.h>
#include <rndf/DrawMisc.hh>
#include <rndf/DrawAerial.hh>
#include <temp-planner-interfaces/PlanGraphUpdater.hh>
#include <temp-planner-interfaces/PlanGraphStatus.hh>

#include "RailPlanner.hh"
#include "rail_planner_viewer_cmdline.h"


class RailPlannerViewer
{
  public:

  // Default constructor
  RailPlannerViewer();

  // Destructor
  ~RailPlannerViewer();

  public:
  
  // Parse the command line
  int parseCmdLine(int argc, char **argv);

  // Initialize GUI
  int initGUI(int cols, int rows);

  // Finalize GUI
  int finiGUI();
 
  public:
    
  // Exit callback
  static void onExit(Fl_Widget *w, int option);

  // Action callback
  static void onAction(Fl_Widget *w, int option);

  // Handle draw callbacks
  static void onDraw(Fl_Glv_Window *win, RailPlannerViewer *self);

  // Handle idle callbacks
  static void onIdle(RailPlannerViewer *self);

  private:

  // Switch to the vehicle frame
  void pushFrameVehicle(const VehicleState *state);

  // Revert to previous frame
  void popFrame();

  private:

  // Pre-draw the graph to a display list
  void predrawGraph(PlanGraph *graph, VehicleState *state, int props);

  // Pre-draw obstacles
  void predrawObs();

  // Draw the safety boxes (vehicle frame)
  void drawSafety();

  // Draw the path (site frame)
  void drawPath(PlanGraphPath *path);
  
  public:

  // Initialize the viewer
  int init();

  // Finalize the viewer
  int fini();

  // Re-load config file
  int reload();

  // Update the viewer
  int update();

  // Update the graph status
  int updateStatus();

  private:

  // Get the bounding box for the obstacle (site frame)
  void getSiteBox(pose2f_t pose, vec2f_t size, float *ox, float *oy, float *sx, float *sy);

  public:

  // MENU options
  enum
  {
    CMD_ACTION_PAUSE = 0x1000,
    CMD_ACTION_RELOAD = 0x1001,
    CMD_ACTION_ONCOMING = 0x1002,
    CMD_ACTION_REVERSE = 0x1003,

    CMD_VIEW_FIRST  = 0x2000,
    CMD_VIEW_AERIAL = 0x00,
    CMD_VIEW_RNDF   = 0x01,
    CMD_VIEW_GRAPH  = 0x02,
    CMD_VIEW_OBS    = 0x03,
    CMD_VIEW_LAST   = 0x20FF,
    
    CMD_GRAPH_FIRST         = 0x2100,
    CMD_GRAPH_NODES         = 0x00,
    CMD_GRAPH_VEHICLE       = 0x01,
    CMD_GRAPH_LANE_CHANGES  = 0x02,
    CMD_GRAPH_RAIL_CHANGES  = 0x03,
    CMD_GRAPH_NODE_STATUS   = 0x04,
    CMD_GRAPH_LAST          = 0x21FF,
  };
  
  // Top-level window
  Fl_Window *mainwin;

  // Top menu bar
  Fl_Menu_Bar *menubar;
  
  // 3D window
  Fl_Glv_Window *worldwin;

  // Should we pause?
  bool pause;

  // Should we advance a single step?
  bool step;
  
  // Should we quit?
  bool quit;
  
  // Command-line options
  struct rail_planner_viewer_cmdline cmdline;

  // Canonical vehicle state
  VehicleState vehState;
  ActuatorState actState;

  // The corridor
  RNDFGraphWaypointList corridor;
  
  // Current start node
  PlanGraphNode *startNode;
  
  // Current goal node
  PlanGraphNode *goalNode;

  // The graph
  PlanGraph *graph;

  // The path
  PlanGraphPath path;

  // Graph generator
  PlanGraphUpdater *updater;

  // Graph status updater
  PlanGraphStatus *status;
  
  // Rail planner object
  RailPlanner *planner;

  // Obstacle data
  struct Obstacle
  {
    bool isCar;
    pose2f_t pose;
    vec2f_t size;
  };
  
  // List of obstacles
  int numObs, maxObs;
  Obstacle *obs;

  // Size of the ROI
  float roiSize;

  // Driving modes
  bool allowReverse, allowOncoming;
  
  // Are the display lists dirty?
  bool dirty;

  // Workspace for mousing around
  float at_x, at_y, at_n, at_e;
  
  // Which layers are we viewing?  This is a bit-mask, with each bit
  // position denoting a seperate layer that is on or off.
  int viewLayers;

  // Which graph properties are enabled?  A bit-mask.
  int graphProps;

  // Drawing tools
  DrawMisc drawMisc;
  DrawAerial drawAerial;
  
  // Display lists 
  GLuint rndfList, graphList, statusList, obsList;
};

#endif
