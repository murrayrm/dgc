/**********************************************************
 **
 **  PATHUTILS.CC
 **
 **    Time-stamp: <2007-10-13 17:11:52 ndutoit> 
 **
 **    Author: Noel du Toit
 **    Created: Thu Jul 26 08:59:31 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/
#include <math.h>

#include "PathUtils.hh"
#include <temp-planner-interfaces/Log.hh>

#define EPS_X 0.0001
#define EPS_T 0.001


/*!
 * return the node on the path that is closest to Alice using euclidean dist
 */
PlanGraphNode* PathUtils::getNearestNodeEuclidean(PlanGraphPath* path, vec2f_t pos, float maxDist)
{
  PlanGraphNode* minNode = NULL;
  float dist, minDist = 100000000;
  
  for (int i=0; i<path->pathLen; i++) {
    if (!path->nodes[i]) break;
    dist = vec2f_mag(vec2f_sub(pos,path->nodes[i]->pose.pos));    
    if (dist < minDist && dist < maxDist) {
      minDist = dist;
      minNode = path->nodes[i];
    }
  }
  
  return minNode;
}

/*!
 * return the node on the path that is closest to Alice using projection.
 */
PlanGraphNode* PathUtils::getNearestNodeProjection(PlanGraphPath* path, pose2f_t pose, float maxDist, float extended)
{
  float heading, m1, c1, m2, c2;
  vec2f_t currPos, projPt;
  float dist;
  float angle1, angle2, diff_angles;

  // Get Alice current pos
  heading = pose.rot;
  currPos = pose.pos;
  
  // project this point into the previous path we had
  int i;
  for (i=0; i+1< path->pathLen; i++) {
    // get params for line perp to alice's heading, through rear axle
    m1 = tan(getAngleInRange(heading+M_PI/2));
    c1 = currPos.y - m1*currPos.x;
    
    // get params for line between 2 nodes
    if (fabs(path->nodes[i+1]->pose.pos.x - path->nodes[i]->pose.pos.x)<EPS_X)
      m2 = (path->nodes[i+1]->pose.pos.y - path->nodes[i]->pose.pos.y)/EPS_X;
    else
      m2 = (path->nodes[i+1]->pose.pos.y - path->nodes[i]->pose.pos.y)/(path->nodes[i+1]->pose.pos.x - path->nodes[i]->pose.pos.x);
    c2 = path->nodes[i]->pose.pos.y - m2*path->nodes[i]->pose.pos.x;

    // get the intersection of these lines
    projPt.x = -(c1-c2)/(m1-m2);
    projPt.y = m1*projPt.x + c1;
    
    // check to see if this intersection is between these nodes. 
    // If so, use the first node in the path as the vehicle node. 
    // if not, move on to the next node
    angle1 = atan2(path->nodes[i]->pose.pos.y - projPt.y, path->nodes[i]->pose.pos.x - projPt.x);
    angle2 = atan2(path->nodes[i+1]->pose.pos.y - projPt.y, path->nodes[i+1]->pose.pos.x - projPt.x);
    diff_angles = angle1 - angle2;
    diff_angles = PathUtils::getAngleInRange(diff_angles);
    if ( fabs(diff_angles) > EPS_T ) {
      // If the projected pt on the path is too far away, we disregard it.
      dist = vec2f_mag(vec2f_sub(currPos, projPt));
      if (dist < maxDist) {
        return path->nodes[i];
      } else
        return NULL;
    }

    if ( (i==0) && (path->pathLen>1) ) {
      // the intersection was not in the first segment, so try to extend the path before the first node and check there too
      pose2f_t tmpPose;
      tmpPose.rot = atan2(path->nodes[1]->pose.pos.y - path->nodes[0]->pose.pos.y, 
                          path->nodes[1]->pose.pos.x - path->nodes[0]->pose.pos.x);
      tmpPose.pos.x = path->nodes[0]->pose.pos.x - extended*cos(tmpPose.rot);
      tmpPose.pos.y = path->nodes[0]->pose.pos.y - extended*sin(tmpPose.rot);
      angle1 = atan2(tmpPose.pos.y - projPt.y, tmpPose.pos.x - projPt.x);
      angle2 = atan2(path->nodes[0]->pose.pos.y - projPt.y, path->nodes[0]->pose.pos.x - projPt.x);
      diff_angles = angle1 - angle2;
      diff_angles = PathUtils::getAngleInRange(diff_angles);
      if ( fabs(diff_angles) > EPS_T ) {
        // If the node on the path is too far away, we disregard it.
        dist = vec2f_mag(vec2f_sub(currPos, projPt));
        if (dist < maxDist) {
          return path->nodes[0];
        } else
          return NULL;
      }
      
    }

  }
  // if we get here we did not find any intersections within the ranges
  // so something went wrong
  return NULL;
}


/**
 * @brief Returns an -PI, PI bounded angle\
 */
float PathUtils::getAngleInRange(float angle)
{
  float angleInRange = angle;

  while (angleInRange > M_PI) 
    angleInRange -= 2*M_PI;

  while (angleInRange <= -M_PI)
    angleInRange += 2*M_PI;

  return angleInRange;
}


/**
 * @brief Returns distance along path to the first obstacle
 */
double PathUtils::distanceToObsOnPath(PlanGraph *graph, PlanGraphPath* path)
{
  double distance = 0.0;
  if (path != NULL) {
    for (int i=0; i<path->pathLen; i++) {
      if (i!=0)
        distance += vec2f_mag(vec2f_sub(path->nodes[i-1]->pose.pos, path->nodes[i]->pose.pos));
      if (graph->isStatusFresh(path->nodes[i]->status.obsTime) &&
          path->nodes[i]->status.obsCollision == 1)
        break;
    }
    return distance;
  }

  return 0.0;
}
