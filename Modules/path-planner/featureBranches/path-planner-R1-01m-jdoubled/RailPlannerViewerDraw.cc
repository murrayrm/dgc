
/* 
 * Desc: Planner viewer drawing functions
 * Date: 29 Sep 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <GL/glut.h>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>
#include <frames/mat44.h>

#include "RailPlannerViewer.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, "%s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)



// Draw a set of axes
void RailPlannerViewer::drawAxes(float size)
{
  // Show camera origin
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(size, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, size, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, size);
  glEnd();

  return;
}


// Draw a text box
void RailPlannerViewer::drawText(float size, const char *text)
{
  int i, count;
  float sx, sy;

  // Compute the scale factors in x/y directions, based on the 'M'
  // character.
  sx = glutStrokeWidth(GLUT_STROKE_MONO_ROMAN, 'M');
  sy = sx * 1.3;
    
  glPushMatrix();
  glScalef(size / sx, size / sy, 1);
  glTranslatef(0, -sy, 0);

  glPushMatrix();
    
  for (i = 0, count = 0; i < (int) strlen(text); i++, count++)
  {
    if (text[i] == '\n')
    {
      glPopMatrix();
      glTranslatef(0, -sy, 0);
      glPushMatrix();
      count = 0;      
    }
    else
    {
      glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, text[i]);
    }
  }

  glPopMatrix();
  glPopMatrix();

  return;
}


// Draw a grid
void RailPlannerViewer::drawGrid(float px, float py, float spacing)
{
  int i, j, n;

  n = (int) (100 / spacing);

  // Pick an offset that will line us up with the grid spacing
  px = -fmod(px + 1e6, spacing);
  py = -fmod(py + 1e6, spacing);
  
  glLineWidth(1.0);
  glColor3f(0.5, 0.5, 0.5);

  glPushMatrix();
  glTranslatef(px, py, 0);
  
  // Draw horizontal lines
  for (j = -n; j < +n; j++)
  {
    glBegin(GL_LINE_STRIP);
    for (i = -n; i < +n; i++)
      glVertex2f(i * spacing, j * spacing);
    glEnd();
  }  
  
  // Draw vertical lines
  for (j = -n; j < +n; j++)
  {
    glBegin(GL_LINE_STRIP);
    for (i = -n; i < +n; i++)
      glVertex2f(j * spacing, i * spacing);    
    glEnd();
  }  

  glPopMatrix();


  // Switch to a viewport frame
  // TODO
  
	return;
}


// Draw Alice (vehicle frame)
void RailPlannerViewer::drawAlice(float steerAngle)
{
  glLineWidth(1);
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  
  // Draw frame axes
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(1, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 1, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, 1);
  glEnd();
  
  // Draw outer vehicle dimensions
  glPushMatrix();
  glTranslatef(+(VEHICLE_LENGTH/2-DIST_REAR_TO_REAR_AXLE), 0, -VEHICLE_HEIGHT/2);
  glScalef(VEHICLE_LENGTH, VEHICLE_WIDTH, VEHICLE_HEIGHT - VEHICLE_TIRE_RADIUS);
  glColor3f(0, 0.70, 0);
  glutWireCube(1.0);
  glPopMatrix();

  // Rear left wheel
  glPushMatrix();
  glTranslatef(0, -VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Rear right wheel
  glPushMatrix();
  glTranslatef(0, +VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Front left wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, -VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(steerAngle * 180/M_PI, 0, 0, 1);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Front right wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, +VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(steerAngle * 180/M_PI, 0, 0, 1);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();
    
  return;
}


// Pre-draw the graph to a display list
void RailPlannerViewer::predrawGraph(PlanGraph *graph, VehicleState *state, int props)
{
  float px, py, size;
  uint16_t include, exclude;

  px = state->siteNorthing;
  py = state->siteEasting;
  size = 128; // MAGIC

  // Select the type of nodes to display
  include = PLAN_GRAPH_NODE_LANE | PLAN_GRAPH_NODE_TURN | PLAN_GRAPH_NODE_VEHICLE;
  exclude = PLAN_GRAPH_NODE_ONCOMING;
  
  if (graphProps & (1 << CMD_GRAPH_LANE_CHANGES))
  {
    include |= PLAN_GRAPH_NODE_LANE_CHANGE | PLAN_GRAPH_NODE_RAIL_CHANGE;
    exclude &= ~PLAN_GRAPH_NODE_ONCOMING;
  }

  // Draw the graph structure
  this->graphList = this->graph->predrawStruct(px, py, size, include, exclude);

  // Draw the graph status
  this->statusList = this->graph->predrawStatus(px, py, size, include, exclude);

  return;
}


// Pre-draw obstacles
void RailPlannerViewer::predrawObs()
{
  int i;
  Obstacle *obs;

  // Create display list
  if (this->obsList == 0)
    this->obsList = glGenLists(1);
  glNewList(this->obsList, GL_COMPILE);
  
  glColor3f(1, 0, 0);
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  for (i = 0; i < this->numObs; i++)
  {
    obs = this->obs + i;

    glPushMatrix();
    glTranslatef(obs->pose.pos.x, obs->pose.pos.y, 0);
    glRotatef(obs->pose.rot * 180/M_PI, 0, 0, 1);

    glBegin(GL_QUADS);
    glVertex2f(-obs->size.x/2, -obs->size.y/2);
    glVertex2f(+obs->size.x/2, -obs->size.y/2);
    glVertex2f(+obs->size.x/2, +obs->size.y/2);
    glVertex2f(-obs->size.x/2, +obs->size.y/2);
    glEnd();
    
    glPopMatrix();
  }

  glEndList();
  
  return;
}



// Draw the path (site frame)
void RailPlannerViewer::drawPath(PlanGraphPath *path)
{
  int i;
  PlanGraphNode *nodeA, *nodeB;

  glLineWidth(2);
  if (path->collideObs)
    glColor3f(1, 1, 0);
  else if (path->collideCar)
    glColor3f(1, 0.5, 0);
  else
    glColor3f(0, 1, 1);
  glBegin(GL_LINES);
  for (i = 0; i < path->pathLen - 1; i++)
  {
    nodeA = path->nodes[i];
    nodeB = path->nodes[i + 1];
    glVertex3f(nodeA->pose.pos.x, nodeA->pose.pos.y, -0.5);
    glVertex3f(nodeB->pose.pos.x, nodeB->pose.pos.y, -0.5);
  }
  glEnd();

  return;
}
