/* 
 * Desc: Plan paths on a graph.
 * Date: 07 October 2007
 * Author: Sven Gowal, Andrew Howard
 * CVS: $Id$
*/

#ifndef RAIL_PLANNER_HH
#define RAIL_PLANNER_HH

#include <vector>
#include <queue>

// Dependencies
#include <temp-planner-interfaces/PlanGraph.hh>
#include <temp-planner-interfaces/PlanGraphPath.hh>


/// Maximum value for the plan cost
#define RAIL_PLANNER_COST_MAX 0x3FFFFFFF


/// @brief Plan constraints.
///
/// These are the weights used to construct plans.  A negative value
/// inidicates a hard constraint.
struct RailPlannerWeights
{
  /// Cost for taking infeasiable paths.
  float infeasibleCost;

  /// Cost multiplier for driving off the lane center-line.
  float centerCost;
  
  /// Cost for driving in an on-coming lane (and risking a head-on
  /// crash). This value should be much larger than laneCost.
  float headOnCost;

  /// head on cost if lane changes are hard constraints. This is only to push us back into 
  /// our lane asap
  float headOnCost_hard;

  /// head on cost if lane changes are soft constraints. This is to keep us from doing lane
  /// changed without consent
  float headOnCost_soft;

  /// Cost for changing lanes.
  float changeCost;  

  /// Cost for drivig off-road
  float offRoadCost;

  /// Cost for changing gears
  float gearChangeCost;
  
  /// Cost for driving in reverse.
  float reverseCost;

  /// Cost for driving through a static obstacle.  Can be set to -1 to
  /// denote a hard constraint.
  float obsCost;

  /// Cost for driving through another vehicle. This can be set
  /// to zero to produce queuing behavior at intersections (the
  /// trajectory generation step will set the velocity profile
  /// such that we dont crash into other vehicles).
  float carCost;

  /// Cost for not being on the center rail
  float railCost;

  /// Cost for being on a volatile node
  float volatileCost;

  // Some useful storage fields
  
  /// Cost for changing lanes with the no-pass flag
  float changeCost_nopass;

  /// Cost for changing lanes with the pass flag
  float changeCost_pass;

  /// Cost for driving through an obstacle with no-pass flag
  float obsCost_nopass;

  /// Cost for driving through an obstacle with pass flag
  float obsCost_pass;

  /// Cost for driving through a car with no-pass flag
  float carCost_nopass;

  /// Cost for driving through a car with pass flag
  float carCost_pass;
};


/// @brief Methods for calculating the A* heuristic
enum RailPlannerHeuristic
{
  HEURISTIC_L1,   // Manhatten distance
  HEURISTIC_L2,   // Euclidean distance
  HEURISTIC_RNDF, // RNDF distance
};


/// @brief Function prototype for status callback.
typedef int (*RailPlannerStatusFunc) (void *data, PlanGraph *graph, PlanGraphNode *node);


/// @brief Basic "rail" planner for searching the graph.
class RailPlanner
{
  public:

  /// @brief Default constructor
  RailPlanner(PlanGraph *graph);

  /// @brief Destructor
  ~RailPlanner();

  public:

  /// @brief Load the planner weights from a file.
  int loadWeights(const char *filename);

  /// @brief Set the weights to the given values
  int setWeights(const RailPlannerWeights *weights);

  /// @brief Set the heuristic method
  int setHeuristic(RailPlannerHeuristic heuristic);

  /// @brief Set the flags that modulate weights
  int setMode(bool enablePass, bool enableReverse = false);

  /// @brief Set the callback function for evaluating node status.
  int setStatusCallback(RailPlannerStatusFunc func, void *data);
  
  public:

  /// @brief Plan path from node A to node B.  
  ///
  /// @param[in] nodeStart Starting node in the graph.
  /// @param[in] nodeGoal  Goal node in the graph.
  /// @param[out] path The planned path.
  int planPath(PlanGraphNode *nodeStart, PlanGraphNode *nodeGoal, PlanGraphPath *path);
  
  private:
  
  // Planning state data
  struct PlanState
  {
    // Is this state in the open list?
    uint8_t isOpen;

    // Is this state in the closed list?
    uint8_t isClosed;

    // Is this state driving in reverse?
    uint8_t isReverse;
    
    // Plan cost
    float cost;

    // A* costs: actual and heuristic
    float costG, costH;

    // Prior state in plan
    PlanState *prior;

    // Pointer to corresponding waypoint in RNDF
    RNDFGraphWaypoint *wp;

    // Pointer to the corresponding node in the plan graph
    PlanGraphNode *node;

    // Comparison operator for maintaining priority queue
    int operator() (const PlanState *stateA, const PlanState *stateB)
      {
        return (stateA->cost > stateB->cost);
      }
  };

  // Class for a list of plan states
  typedef std::deque<PlanState*> PlanStateList;

  private:
    
  // Pre-plan on the RNDF; this pre-computes the heuristic for
  // the A* search.  
  int genHeuristic(int segmentId, int laneId, int waypointId);

  // Generate a path from node A to node B and return the
  // final state in the path (A*)
  int genPlan(PlanGraphNode *nodeStart, PlanGraphNode *nodeGoal, PlanState **stateFinal);

  // Construct path from the state list
  int genPath(PlanStateList *states, PlanGraphPath *path);

  private:
  
  // Get the state information for a waypoint
  PlanState *getState(RNDFGraphWaypoint *wp);

  // Get the state information for a PlanGraph node.  The state is
  // stored in the user data pointer in the node.
  PlanState *getState(PlanGraphNode *node);

  // Free the state information
  void freeStates();

  // Push a state onto the open list
  void pushOpen(PlanState *state);

  // Pop a state from the open list
  bool popOpen(PlanState **state);

  // Clear the open list
  void clearOpen();

  // Priority queue for open plan states
  std::priority_queue<PlanState*, std::vector<PlanState*>, PlanState> openList;

  // Standard queue for the dirty states
  std::queue<PlanState*> dirtyList;  

  private:
  
  // Calculate the heuristic (guess for the cost) between the given
  // nodes.  This must strictly under-estimate the true cost.
  float calcH(PlanState *state);

  // Calculate the actual cummulative cost of getting to state B via state A.
  float calcG(PlanState *stateA, PlanState *stateB);

  public:

  // Dianostic: dump the entire graph with costs
  int dumpGraph();

  // Diagnostics: print the states
  int dumpStates(PlanStateList *states);

  // Diagnostics: print the path
  int dumpPath(PlanGraphPath *path);

  public:

  // Useful search statistics.
  int numSearched;
  bool goalFound;
  
  private:
  
  // The graph we are using
  PlanGraph *graph;

  // Plan weights
  RailPlannerWeights weights;

  // Method for calculating the A* heuristic
  RailPlannerHeuristic heuristic;

  // Status callback function pointer
  RailPlannerStatusFunc statusFunc;
  void *statusData;
  
  // Nominal goal position (may be used for heurstic calculation).
  vec2f_t goalPos;

  // Are we allowing lane changes?
  bool allowLaneChange;
  
  // Are lane changes hard or soft constraints?
  bool laneChangeHardConstraint;

  // Are we allowing reversing?
  bool allowReverse;

};

#endif




