
/* 
 * Desc: Test rail planner
 * Date: 7 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <dgcutils/DGCutils.hh>
#include "RailPlanner.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)

// Common macros
#define MIN(a, b) ((a) <= (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))


// Construct random plan 
int testRandomWaypoints(PlanGraph *pgraph, RailPlanner *planner)
{
  uint64_t time;
  int i, j, na, nb;
  RNDFGraph *rndf;
  RNDFGraphWaypoint *wpA, *wpB;
  PlanGraphNode *nodeA, *nodeB;
  PlanGraphPath path;
  
  float numL2[2] = {0};
  float meanL2[2] = {0};
  float maxL2[2] = {0};
  float numRNDF[2] = {0};
  float meanRNDF[2] = {0};
  float maxRNDF[2] = {0};

  rndf = &pgraph->rndf;
    
  for (j = 0; j < 10; j++)
  {
    // Pick a goal that is not on a perimeter
    while (true)
    {
      nb = (rand() % rndf->numWaypoints);
      wpB = rndf->waypoints + nb;
      if (wpB->laneId == 0)
        continue;
      break;
    }
    nodeB = pgraph->getNearestPos(wpB->px, wpB->py, 1, PLAN_GRAPH_NODE_LANE);
    assert(nodeB);
  
    MSG("goal %d.%d.%d",
        wpB->segmentId, wpB->laneId, wpB->waypointId);

    for (i = 0; i < 10; i++)
    {
      // Pick an initial point that is not on a perimeter
      while (true)
      {
        na = (rand() % rndf->numWaypoints);
        if (na == nb)
          continue;    
        wpA = rndf->waypoints + na;
        if (wpA->laneId == 0)
          continue;
        break;
      }
      nodeA = pgraph->getNearestPos(wpA->px, wpA->py, 1.0, PLAN_GRAPH_NODE_LANE);
      assert(nodeA);
    
      MSG("  start %d.%d.%d",
          wpA->segmentId, wpA->laneId, wpA->waypointId);

      // Run the path-planner with L2 heuristic
      time = DGCgettime();
      planner->setHeuristic(HEURISTIC_L2);
      planner->planPath(nodeA, nodeB, &path);
      time = DGCgettime() - time;
      MSG("    path-plan goal %d nodes %d time %.3fms",
          planner->goalFound, planner->numSearched, (double) time * 1e-3);

      numL2[planner->goalFound] += 1;
      meanL2[planner->goalFound] += time*1e-3;
      maxL2[planner->goalFound] = MAX(time*1e-3, maxL2[planner->goalFound]);

      // Run the path-planner with pre-planning
      time = DGCgettime();
      planner->setHeuristic(HEURISTIC_RNDF);
      planner->planPath(nodeA, nodeB, &path);
      time = DGCgettime() - time;
      MSG("    path-plan goal %d nodes %d time %.3fms",
          planner->goalFound, planner->numSearched, (double) time * 1e-3);

      numRNDF[planner->goalFound] += 1;
      meanRNDF[planner->goalFound] += time*1e-3;
      maxRNDF[planner->goalFound] = MAX(time*1e-3, maxRNDF[planner->goalFound]);
    }
  }

  MSG("summary stats");
  MSG("L2   fail %.0f mean %5.1fms max %5.1fms", numL2[0], meanL2[0]/numL2[0], maxL2[0]);
  MSG("L2   pass %.0f mean %5.1fms max %5.1fms", numL2[1], meanL2[1]/numL2[1], maxL2[1]);
  MSG("RNDF fail %.0f mean %5.1fms max %5.1fms", numRNDF[0], meanRNDF[0]/numRNDF[0], maxRNDF[0]);
  MSG("RNDF pass %.0f mean %5.1fms max %5.1fms", numRNDF[1], meanRNDF[1]/numRNDF[1], maxRNDF[1]);
  
  return 0;
}


int main(int argc, char **argv)
{
  char *filename;
  PlanGraph graph;
  RailPlanner planner(&graph);
  
  // Parse command line
  filename = NULL;  
  if (argc > 1)
    filename = argv[1];
  if (!filename)
    return fprintf(stderr, "usage: %s <FILE.RNDF.PG>\n", argv[0]);
  
  // Load compiled plan graph
  MSG("loading %s", filename);
  if (graph.load(filename) != 0)
    return -1;

  MSG("graph nodes %d", graph.nodeCount);
  
  // Run some random goal tests
  testRandomWaypoints(&graph, &planner);
  
  return 0;
}
