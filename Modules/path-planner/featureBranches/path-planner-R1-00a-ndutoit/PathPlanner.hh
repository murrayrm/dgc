#ifndef PATHPLANNER_HH_
#define PATHPLANNER_HH_

#include "frames/pose3.h"
#include "frames/point2.hh"

#include "temp-planner-interfaces/PlannerInterfaces.h"
#include "interfaces/VehicleState.h"
#include "interfaces/ActuatorState.h"
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>

#include "planner/CmdArgs.hh"

class PathPlanner {

public: 

  static int init();
  static void destroy();
  static Err_t planPath(Path_t* path, Cost_t& maxCost, Graph_t* graph, pose3_t finalPose, Path_params_t pathParams);

  static void display(int sendSubgroup, Path_t* paph);
  
private :
  static CMapElementTalker meTalker;
};

#endif /*PATHPLANNER_HH_*/




