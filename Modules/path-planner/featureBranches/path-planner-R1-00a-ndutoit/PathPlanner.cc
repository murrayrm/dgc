#include "PathPlanner.hh"
//#include "planner/Console.hh"
#include "graph-updater/GraphUpdater.hh"

#include <alice/AliceConstants.h>
#include <math.h>

CMapElementTalker PathPlanner::meTalker;

int PathPlanner::init()
{
  // Initialize map talker
  meTalker.initSendMapElement(CmdArgs::sn_key);

  return 0;
}

void PathPlanner::destroy()
{
  return;
}

Err_t PathPlanner::planPath(Path_t* path, Cost_t& maxCost, Graph_t* graph, 
                            pose3_t finalPose, Path_params_t pathParams)
{
  // Steps through nodes backwards to set the cost to the departure node
  graph->makePlan(finalPose);

  // Search fwd from departure node to the destination node to find lowest cost solution
  graph->makePath(path);

  // Set cost of the path
  maxCost = 0;

  return PP_OK;
}

void PathPlanner::display(int sendSubgroup, Path_t* path)
{
  int counter=12000;
  point2 point;
  vector<point2> points;
  MapId mapId;
  GraphNode* node;
  MapElement me;
  
  mapId = counter;
  for (int i=0; i<path->pathLen; i++) {
    node = path->path[i];
    point.set(node->pose.pos.x,node->pose.pos.y);
    points.push_back(point);
  }
  me.setId(mapId);
  me.setTypeTravPath();
  me.setGeometry(points);
  meTalker.sendMapElement(&me,sendSubgroup);    
}
