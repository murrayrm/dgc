
/* 
 * Desc: Rail planner viewer utility
 * Date: 15 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <limits.h>

#include <GL/glut.h>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>
#include <frames/mat44.h>
#include <dgcutils/DGCutils.hh>

#include "RailPlannerViewer.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, "%s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Default constructor
RailPlannerViewer::RailPlannerViewer()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Destructor
RailPlannerViewer::~RailPlannerViewer()
{
  return;
}


// Initialize stuff
int RailPlannerViewer::initGUI(int cols, int rows)
{
  // The menu
  Fl_Menu_Item menuitems[] =
    {
      {"&File", 0, 0, 0, FL_SUBMENU},    
      {"E&xit", FL_CTRL + 'q', (Fl_Callback*) RailPlannerViewer::onExit},
      {0},

      {"&Action", 0, 0, 0, FL_SUBMENU},    
      {"Pause", ' ', (Fl_Callback*) RailPlannerViewer::onAction,
       (void*) CMD_ACTION_PAUSE, FL_MENU_TOGGLE},
      {0},
      
      {"&View", 0, 0, 0, FL_SUBMENU},

      {"Alice", 'a', (Fl_Callback*) RailPlannerViewer::onAction,
       (void*) (CMD_VIEW_ALICE + CMD_VIEW_FIRST), FL_MENU_TOGGLE | FL_MENU_VALUE},      
      {"RNDF", 'r', (Fl_Callback*) RailPlannerViewer::onAction,
       (void*) (CMD_VIEW_RNDF + CMD_VIEW_FIRST), FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"Graph", 'g', (Fl_Callback*) RailPlannerViewer::onAction,
       (void*) (CMD_VIEW_GRAPH + CMD_VIEW_FIRST), FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"Map", 'm', (Fl_Callback*) RailPlannerViewer::onAction,
       (void*) (CMD_VIEW_OBS + CMD_VIEW_FIRST), FL_MENU_TOGGLE | FL_MENU_VALUE},
      
      // Graph display properties
      {"Plan graph", 0, 0, 0, 0},
      {"Lane changes", FL_CTRL + 'l', (Fl_Callback*) RailPlannerViewer::onAction,
       (void*) (CMD_GRAPH_LANE_CHANGES + CMD_GRAPH_FIRST), FL_MENU_TOGGLE},
      {"Node status", FL_CTRL + 's', (Fl_Callback*) RailPlannerViewer::onAction,
       (void*) (CMD_GRAPH_NODE_STATUS + CMD_GRAPH_FIRST),
       FL_MENU_TOGGLE | FL_MENU_VALUE | FL_MENU_DIVIDER},
      
      {0},

      {0},
    };

  // Create top-level window
  this->mainwin = new Fl_Window(cols, rows + 30, "DGC Planner Viewer");
  this->mainwin->user_data(this);

  this->mainwin->begin();

  // Crate the menu bar
  this->menubar = new Fl_Menu_Bar(0, 0, cols, 30);
  this->menubar->user_data(this);
  this->menubar->copy(menuitems);

  // Create world window
  this->worldwin = new Fl_Glv_Window(0, 30, cols, rows, this, (Fl_Callback*) onDraw);

  this->mainwin->end();

  // Make world window resizable 
  this->mainwin->resizable(this->worldwin);

  // Set the initial POV
  this->worldwin->set_hfov(40.0);
  this->worldwin->set_clip(4, 1000);
  this->worldwin->set_lookat(-20, -20, -20, 0, 0, 0, 0, 0, -1);

  // Set consistent menu state
  this->viewLayers = (1 << CMD_VIEW_ALICE) | (1 << CMD_VIEW_RNDF);
  this->viewLayers |= (1 << CMD_VIEW_GRAPH) | (1 << CMD_VIEW_OBS);
  this->graphProps = (1  << CMD_GRAPH_NODE_STATUS);

  this->rndfList = 0;
  this->dirty = true;
  
  return 0;
}


// Finalize stuff
int RailPlannerViewer::finiGUI()
{  
  return 0;
}


// Handle menu callbacks
void RailPlannerViewer::onExit(Fl_Widget *w, int option)
{
  RailPlannerViewer *self;

  self = (RailPlannerViewer*) w->user_data();
  self->quit = true;

  return;
}


// Handle menu callbacks
void RailPlannerViewer::onAction(Fl_Widget *w, int option)
{
  RailPlannerViewer *self;  

  self = (RailPlannerViewer*) w->user_data();
  if (option == CMD_ACTION_PAUSE)
    self->pause = !self->pause;
  
  // Check for changes in the selected display viewLayers
  if (option >= CMD_VIEW_FIRST && option < CMD_VIEW_LAST)
  {
    self->viewLayers = (self->viewLayers ^ (1 << (option - CMD_VIEW_FIRST)));
    self->dirty = true;
    self->worldwin->redraw();
  }

  // Check for changes in the selected graph properties
  if (option >= CMD_GRAPH_FIRST && option < CMD_GRAPH_LAST)
  {
    self->graphProps = (self->graphProps ^ (1 << (option - CMD_GRAPH_FIRST)));
    self->dirty = true;
    self->worldwin->redraw();
  }

  return;
}


// Handle draw callbacks
void RailPlannerViewer::onDraw(Fl_Glv_Window *win, RailPlannerViewer *self)
{
  VehicleState *vehState;
  ActuatorState *actState;
  PlanGraph *graph;

  // Get the state.
  vehState = &self->vehState;
  actState = &self->actState;

  /*
  // TODO
  // Update the state based on the mouse motion
  if (win->mouse_state != 0)
  {
    MSG("%f %f", win->at_x, win->at_y);
    vehState->siteNorthing = win->at_x;
    vehState->siteEasting = win->at_y;
  }
  */
  
  // Get the graph so we know the global to graph offset
  graph = self->graph;
  assert(graph);

  // Predraw the static RNDF if necessary
  if (self->rndfList == 0)
    self->rndfList = self->graph->rndf.predraw();
      
  // Predraw if necessary
  if (self->dirty)
  {
    // Draw the graph
    if (self->viewLayers & (1 << CMD_VIEW_GRAPH))
      self->predrawGraph(graph, vehState, self->graphProps);

    // Predraw the obsacles
    if (self->viewLayers & (1 << CMD_VIEW_OBS))
      self->predrawObs();
      
    self->dirty = false;
  }    
  
  // Draw the grid
  self->drawGrid(vehState->siteNorthing, vehState->siteEasting, 10.0);

  // Switch to site frame
  glPushMatrix();
  
  // Do a translation to center on the vehicle
  glTranslatef(-vehState->siteNorthing, -vehState->siteEasting, -vehState->siteAltitude);

  if (true)
  {
    glPushMatrix();
    
    // Translate z so that the maps are directly beneath the vehicle
    glTranslatef(0, 0, vehState->siteAltitude + VEHICLE_TIRE_RADIUS);

    // Draw the RNDF 
    if (self->viewLayers & (1 << CMD_VIEW_RNDF))
      glCallList(self->rndfList);  

    // Draw the graph
    if (self->viewLayers & (1 << CMD_VIEW_GRAPH))
    {
      glCallList(self->graphList);
      if (self->graphProps & (1  << CMD_GRAPH_NODE_STATUS))
        glCallList(self->statusList);
    }

    // Draw obstacles
    if (self->viewLayers & (1 << CMD_VIEW_OBS))
      glCallList(self->obsList);
    
    // Draw the planned path
    if (true)
      self->drawPath(&self->path);

    glPopMatrix();
  }

  if (true)
  {
    // Switch to vehicle frame
    self->pushFrameVehicle(vehState);
    
    // Draw Alice
    if (self->viewLayers & (1 << CMD_VIEW_ALICE))
    {
      self->drawAxes(1.0);
      self->drawAlice(actState->m_steerpos * VEHICLE_MAX_AVG_STEER);
    }
  
    self->popFrame();
  }

  glPopMatrix();
  
  return;
}


// Switch to the vehicle frame 
void RailPlannerViewer::pushFrameVehicle(const VehicleState *state)
{
  PlanGraph *graph;
  pose3_t pose;
  float m[4][4];
  
  graph = this->graph;
  
  // Transform from vehicle to site frame
  pose.pos = vec3_set(state->siteNorthing, state->siteEasting, state->siteAltitude);
  pose.rot = quat_from_rpy(state->siteRoll, state->sitePitch, state->siteYaw);
  pose3_to_mat44f(pose, m);

  // Transpose to column-major order for GL
  mat44f_trans(m, m);
  
  glPushMatrix();
  glMultMatrixf((GLfloat*) m);
  
  return;
}


// Revert to previous frame
void RailPlannerViewer::popFrame()
{
  glPopMatrix();  
  return;
}


// Handle idle callbacks
void RailPlannerViewer::onIdle(RailPlannerViewer *self)
{
  if (!self->pause)
  {  
    // Update perceptor
    if (self->update() != 0)
      self->quit = true;

    // Redraw the display
    self->worldwin->redraw();
  }
  else
  {
    // Sleepy bye-bye
    usleep(0);
  }
  
  return;
}


// Parse command-line options
int RailPlannerViewer::parseCmdLine(int argc, char **argv)
{  
  // Run parser
  if (rail_planner_cmdline(argc, argv, &this->cmdline) != 0)
  {
    rail_planner_cmdline_print_help();
    return -1;
  }

  // Load configuration file, if given; settings in the config file do not
  // override the command line options.
  if (this->cmdline.inputs_num > 0)
  {
    if (rail_planner_cmdline_configfile(this->cmdline.inputs[0], &this->cmdline,
                                        false, false, false) != 0)
    {
      rail_planner_cmdline_print_help();
      return -1;
    }
  }
  
  return 0;
}


// Initialize the planner
int RailPlannerViewer::init()
{
  int i;
  Obstacle *obs;
  char filename [1024];
  
  // Create the graph
  this->graph = new PlanGraph();

  // Load the graph
  snprintf(filename, sizeof(filename), "%s.pg", this->cmdline.rndf_arg);  
  if (this->graph->load(filename) != 0)
    return ERROR("unable to load %s", filename);
  
  // Create planner
  this->planner = new RailPlanner(this->graph);

  // MAGIC
  this->roiSize = 128;

  // Get the intial pose
  if (this->cmdline.start_given)
  {
    int numArgs;
    int segmentId, laneId, waypointId;
    float px, py, ph;
    PlanGraphNode *wp;

    // Load the waypoint; the pose fields are optional
    px = py = ph = 0;
    numArgs = sscanf(this->cmdline.start_arg, "%d.%d.%d %f%f%f", 
                     &segmentId, &laneId, &waypointId, &px, &py, &ph);
    if (numArgs < 3)
      return ERROR("syntax error in [%s]", this->cmdline.start_arg);

    // Get the waypoint
    wp = this->graph->getWaypoint(segmentId, laneId, waypointId);
    if (!wp)
      return MSG("unknow waypoint %d.%d.%d", segmentId, laneId, waypointId);

    // Set the initial state value
    this->vehState.siteNorthing = wp->pose.pos.x + px;
    this->vehState.siteEasting = wp->pose.pos.y + py;
    this->vehState.siteYaw = wp->pose.rot + ph * M_PI/180;
  }

  // Get the goal pose
  if (this->cmdline.goal_given)
  {
    int numArgs;
    int segmentId, laneId, waypointId;

    // Load the waypoint; the pose fields are optional
    numArgs = sscanf(this->cmdline.goal_arg, "%d.%d.%d", 
                     &segmentId, &laneId, &waypointId);
    if (numArgs < 3)
      return ERROR("syntax error in [%s]", this->cmdline.start_arg);

    // Get the goal node
    this->goal = this->graph->getWaypoint(segmentId, laneId, waypointId);
    if (!goal)
      return MSG("unknow waypoint %d.%d.%d", segmentId, laneId, waypointId);
  }

  // Load obstacles
  this->numObs = 0;
  this->maxObs = this->cmdline.obs_given;
  this->obs = (Obstacle*) calloc(this->maxObs, sizeof(this->obs[0]));
  for (i = 0; i < (int) this->cmdline.obs_given; i++)
  {
    int numArgs;
    int segmentId, laneId, waypointId;
    float px, py, ph, sx, sy;
    PlanGraphNode *wp;

    px = py = ph = 0;

    // Load the waypoint, size and pose; the pose fields are optional
    numArgs = sscanf(this->cmdline.obs_arg[i], "%d.%d.%d %fx%f %f%f%f",
                     &segmentId, &laneId, &waypointId, &sx, &sy, &px, &py, &ph);
    if (numArgs < 5)
    {
      MSG("syntax error in [%s]", this->cmdline.obs_arg[i]);
      continue;
    }

    MSG("obs %d.%d.%d  %f %f %f  %f x %f",
        segmentId, laneId, waypointId, px, py, ph, sx, sy);

    // Get the waypoint
    wp = this->graph->getWaypoint(segmentId, laneId, waypointId);
    if (!wp)
    {
      MSG("unknow waypoint %d.%d.%d", segmentId, laneId, waypointId);
      continue;
    }

    // Construct the obstacle
    obs = this->obs + this->numObs++;
    obs->pose.pos.x = wp->pose.pos.x + px;
    obs->pose.pos.y = wp->pose.pos.y + py;
    obs->pose.rot = ph * M_PI/180;
    obs->size.x = sx;
    obs->size.y = sy;
  }

  return 0;
}


// Finalize the planner
int RailPlannerViewer::fini()
{  
  // Clean up
  free(this->obs);
  this->obs = NULL;
  delete this->planner;
  this->planner = NULL;
  delete this->graph;
  this->graph = NULL;
  
  return 0;  
}


// Update the planner
int RailPlannerViewer::update()
{
  uint64_t time;
  VehicleState *state;
  PlanGraphNode *start;

  state = &this->vehState;
  
  // TODO: something more intelligent with paths.
  // Update the ROI
  if (this->graph->updateROI(state->siteNorthing, state->siteEasting, this->roiSize) != 0)
    this->path.pathLen = 0;

  // TODO (SLOW)
  time = DGCgettime();
  this->updateState();
  time = DGCgettime() - time;
  MSG("state time %.3fms", (double) time / 1e3);
  
  // TODO
  uint16_t include, exclude;
  include = PLAN_GRAPH_NODE_LANE;
  exclude = PLAN_GRAPH_NODE_ONCOMING;
  start = this->graph->getNearestPos(state->siteNorthing, state->siteEasting,
                                     1.0, include, exclude);

  // Run the planner
  time = DGCgettime();
  this->planner->planPath(start, this->goal, &this->path);
  time = DGCgettime() - time;
  MSG("plan time %.3fms", (double) time / 1e3);

  this->dirty = true;
  
  return 0;
}


// Update the graph state
int RailPlannerViewer::updateState()
{
  int i, j;
  Obstacle *obs;
  PlanGraphNode *node;
  PlanGraphNodeList nodes;
  vec2f_t pos;
  float bbFront, bbRear, bbLeft, bbRight;
  VehicleState *state;
  
  // TODO
  bbFront = 4.0;
  bbRear = -1.0;
  bbLeft = +2.0;
  bbRight = -2.0;

  state = &this->vehState;
  
  // Get all the nodes in the ROI
  this->graph->getRegion(&nodes, state->siteNorthing, state->siteEasting,
                         this->roiSize, this->roiSize);
  
  // Project each obstacle into the ROI; wildly inefficient.
  for (i = 0; i < this->numObs; i++)
  {
    obs = this->obs + i;

    for (j = 0; j < (int) nodes.size(); j++)
    {
      node = nodes[j];
      
      pos = vec2f_transform(pose2f_inv(node->pose), obs->pose.pos);

      if (pos.x > bbFront || pos.x < bbRear)
        continue;
      if (pos.y > bbLeft || pos.y < bbRight)
        continue;

      node->status.obsCollision = true;
    }
    
  }
  
  return 0;
}


int main(int argc, char *argv[])
{
  RailPlannerViewer *app;

  // Initialize GLUT calls
  glutInit(&argc, argv);

  app = new RailPlannerViewer();

  // Parse cmd line
  if (app->parseCmdLine(argc, argv) != 0)
    return -1;

  // Initialize gui
  if (app->initGUI(1024, 768) != 0)
    return -1;

  // Initilize the app
  if (app->init() != 0)
    return -1;

  // Idle callback
  Fl::add_idle((void (*) (void*)) RailPlannerViewer::onIdle, app);
    
  // Run
  app->mainwin->show();
  while (!app->quit)
    Fl::wait();

  MSG("exiting");
  
  // Clean up
  app->fini();
  app->finiGUI();
  delete app;
 
  MSG("exited cleanly");
  
  return 0;
}
