/* 
 * Desc: Plan paths on a graph.
 * Date: 07 October 2007
 * Author: Sven Gowal, Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <float.h>
#include <stdio.h>

#include <temp-planner-interfaces/ConfigFile.hh>

#include "RailPlanner.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)

// Common macros
#ifndef MIN
#define MIN(a, b) ((a) <= (b) ? (a) : (b))
#endif
#ifndef MAX
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#endif


// Default constructor
RailPlanner::RailPlanner(PlanGraph *graph)
{
  this->graph = graph;

  // Initialize weights
  memset(&this->weights, 0, sizeof(this->weights));

  this->heuristic = HEURISTIC_RNDF;

  this->statusFunc = NULL;
  this->statusData = NULL;

  this->laneChangeHardConstraint = true;
  this->restrictLaneChange = true;
  
  return;
}


// Destructor
RailPlanner::~RailPlanner()
{
  // TODO Clean up
  
  return;
}


// Load the weights from a file
int RailPlanner::loadWeights(const char *filename)
{
  FILE * file;

  if (!filename)
    return -1;
  
  // Check for file existance before opening 
  file = fopen(filename, "r");
  if (file == NULL)
    return -1;
  
  ConfigFile config(filename);
  config.readInto(this->weights.centerCost,"centerCost");
  config.readInto(this->weights.headOnCost_soft,"headOnCost_soft");
  config.readInto(this->weights.headOnCost_hard,"headOnCost_hard");
  config.readInto(this->weights.offRoadCost,"offRoadCost");
  config.readInto(this->weights.reverseCost,"reverseCost");
  config.readInto(this->weights.railCost,"railCost");
  config.readInto(this->weights.volatileCost,"volatileCost");
  config.readInto(this->weights.changeCost_nopass,"changeCost_nopass");
  config.readInto(this->weights.changeCost_pass,"changeCost_pass");
  config.readInto(this->weights.obsCost_nopass,"obsCost_nopass");
  config.readInto(this->weights.obsCost_pass,"obsCost_pass");
  config.readInto(this->weights.carCost_nopass,"carCost_nopass");
  config.readInto(this->weights.carCost_pass,"carCost_pass");
  config.readInto(this->laneChangeHardConstraint,"laneChangeHardConstraint");
  fclose(file);

  // Set some defaults
  this->weights.changeCost   = this->weights.changeCost_nopass;
  this->weights.obsCost      = this->weights.obsCost_nopass;
  this->weights.carCost      = this->weights.carCost_nopass;

  if (this->laneChangeHardConstraint)
    this->weights.headOnCost = weights.headOnCost_hard;
  else
    this->weights.headOnCost = weights.headOnCost_soft;
  
  return 0;
}


// Set the heuristic method
int RailPlanner::setHeuristic(RailPlannerHeuristic heuristic)
{
  this->heuristic = heuristic;
  
  return 0;
}


// Set the flags that modulate weights
int RailPlanner::setMode(bool enablePass)
{
  this->restrictLaneChange = !enablePass;

  if (enablePass)
  {
    //this->weights.headOnCost   = this->weights.headOnCost_pass;
    this->weights.changeCost   = this->weights.changeCost_pass;
    this->weights.obsCost      = this->weights.obsCost_pass;
    this->weights.carCost      = this->weights.carCost_pass;    
  }
  else
  {
    //this->weights.headOnCost   = this->weights.headOnCost_nopass;
    this->weights.changeCost   = this->weights.changeCost_nopass;
    this->weights.obsCost      = this->weights.obsCost_nopass;
    this->weights.carCost      = this->weights.carCost_nopass;
  }

  if (this->laneChangeHardConstraint)
    this->weights.headOnCost = weights.headOnCost_hard;
  else
    this->weights.headOnCost = weights.headOnCost_soft;
  
  return 0;
}


// Set the callback function for evaluating node status.
int RailPlanner::setStatusCallback(RailPlannerStatusFunc func, void *data)
{
  this->statusFunc = func;
  this->statusData = data;
    
  return 0;
}


// Calculate the actual cost for getting between two adjacent nodes.
float RailPlanner::calcG(PlanState *stateA, PlanState *stateB)
{
  float cost;
  PlanGraphNode *nodeA, *nodeB;

  nodeA = stateA->node;
  nodeB = stateB->node;

  // Start with cost for node A
  cost = stateA->costG;

  // Add cost for the Euclidean distance.
  cost += 1000 * vec2f_mag(vec2f_sub(nodeB->pose.pos, nodeA->pose.pos));

  // Discourange driving in oncoming lanes
  if (nodeB->flags.isOncoming)
    cost += this->weights.headOnCost;

  /*
  // TESTING
  if (this->statusFunc && !this->graph->isStatusFresh(nodeB))
  {
    this->statusFunc(this->statusData, this->graph, nodeB);
    this->graph->setStatusFresh(nodeB);
  }
  */

  // If the status information is current, apply some additional costs
  if (this->graph->isStatusFresh(nodeB->status.obsTime))
  {
    // Discourage collisions.
    if (nodeB->status.obsCollision)
      cost += this->weights.obsCost;

    // Stay in the center of the lane.
    cost += nodeB->status.centerDist * this->weights.centerCost;
  }
  else
  {
    // If there is no sensed lane, discourage driving off the center rail.
    if (nodeB->railId != 0)
      cost += this->weights.railCost * abs(nodeB->railId);
  }

  // TODO: other conditions
  
  return cost;
}


// Calculate the heuristic (guess for the cost) between the given nodes.
// This must strictly under-estimate the true cost.
float RailPlanner::calcH(PlanState *state)
{
  float dx, dy, cost;
  PlanGraphNode *node;

  node = state->node;

  if (this->heuristic == HEURISTIC_RNDF && node->nextWaypoint)
  {
    // Get the next RNDF waypoint and use the pre-computed cost as the
    // basis for the heurstic.
    assert(node->nextWaypoint);
    cost = node->nextWaypoint->planCost;
    if (cost == FLT_MAX)
      return cost;

    // Add the euclidean distance from this node to the waypoint
    dx = node->pose.pos.x - node->nextWaypoint->px;
    dy = node->pose.pos.y - node->nextWaypoint->py;
    cost += sqrtf(dx*dx + dy*dy);
  }
  else if (this->heuristic == HEURISTIC_L1)
  {
    // Use Manhatten distance to the goal
    dx = node->pose.pos.x - this->goalPos.x;
    dy = node->pose.pos.y - this->goalPos.y;
    cost = dx + dy;
  }
  else 
  {
    // Use Euclidea distance to the goal
    dx = node->pose.pos.x - this->goalPos.x;
    dy = node->pose.pos.y - this->goalPos.y;
    cost = sqrtf(dx*dx + dy*dy);
  }

  return 1000 * cost;
}


// Plan path from the start node to the goal node, given the current
// planner weights.
int RailPlanner::planPath(PlanGraphNode *nodeStart, PlanGraphNode *nodeGoal, PlanGraphPath *path)
{
  PlanState *stateFinal;
  PlanStateList states;
  RailPlannerHeuristic defaultHeuristic;
  
  // Reset stats
  this->numSearched = 0;
  this->goalFound = false;
  stateFinal = NULL;

  // Check for valid start/end conditions
  if (!nodeStart)
    return -1;
  if (!nodeGoal)
    return -1;

  // Record the default heuristic; we may change the one we use
  defaultHeuristic = this->heuristic;
        
  if (this->heuristic == HEURISTIC_RNDF && nodeStart->nextWaypoint)
  {
    // Generate the heuristic
    if (this->genHeuristic(nodeGoal->segmentId, nodeGoal->laneId, nodeGoal->waypointId) == 0)
    {
      // Do a check to see if this plan looks even vaguely possible,
      // based on the heuristic.
      assert(nodeStart->nextWaypoint);
      if (nodeStart->nextWaypoint->planCost == FLT_MAX)
        return -1;
    }
    else
    {
      MSG("falling back to L2 Euclidean heuristic.");
      this->heuristic = HEURISTIC_L2;
    }
  }
   
  // Record the goal position for the Heursistics
  this->goalPos = nodeGoal->pose.pos;

  // Run A* search.  If successful, finalState will contain the last
  // state in the path.
  if (this->genPlan(nodeStart, nodeGoal, &stateFinal) == 0)
    this->goalFound = true;
  
  // Print diagnostics
  //this->dumpGraph();

  // Backtrack to get the list of states on this path
  while (stateFinal)
  {
    states.push_front(stateFinal);
    stateFinal = stateFinal->prior;
  }

  // Sanity check: the first state in the path must correspond to the
  // starting node
  if (stateFinal)
    assert(states.front()->node == nodeStart);

  // Print diagnostics
  //this->dumpPlanStates(&states);

  // Construct path from the state list
  this->genPath(&states, path);
  
  // Reset flags in the nodes we have visited to ensure that the
  // next search starts with a clean slate.
  this->freeStates();
  this->clearOpen();

  // Restore the default heuristic
  this->heuristic = defaultHeuristic;

  return 0;
}


// Pre-compute the search heuristic on the RNDF (Djikstra).
int RailPlanner::genHeuristic(int segmentId, int laneId, int waypointId)
{
  int i;
  float dx, dy, cost;
  RNDFGraph *rndf;
  RNDFGraphWaypoint *wp, *wpA, *wpB;
  PlanState *state, *stateA, *stateB;
  
  // Use the underlying graph RNDF
  rndf = &this->graph->rndf;

  // TODO: remove the plan cost from the RNDF graph; instead, just
  // leave the state nodes in place (i.e., don't free the states), and
  // run the RNDF planner on every cycle (for a small additional
  // overhead).
  
  // Reset plan costs; this is slow, but necessary if we are going to
  // use the cost values stored in the graph.
  for (i = 0; i < rndf->numWaypoints; i++)
  {
    wp = rndf->waypoints + i;
    wp->planCost = FLT_MAX;
  }

  // Get the goal point
  wp = rndf->getWaypoint(segmentId, laneId, waypointId);
  if (!wp)
    return ERROR("waypoint %d.%d.%d not found in RNDF", segmentId, laneId, waypointId);

  // Push the goal.  Note that we use the cost field in the RNDF,
  // since this is the final product of the pre-planning step.
  state = this->getState(wp);
  state->isOpen = true;
  state->isClosed = false;
  wp->planCost = 0;
  this->pushOpen(state);
  
  // Run Djikstra on the open list; search backwards from goal to
  // assign a cost to each waypoint.
  while (this->popOpen(&stateB))
  {
    stateB->isOpen = false;
    stateB->isClosed = true;
    wpB = stateB->wp;
    
    //MSG("B %d.%d.%d %f",
    //    wpB->segmentId, wpB->laneId, wpB->waypointId, wpB->planCost);
          
    // Look at our incoming waypoints
    for (i = 0; i < wpB->numPrev; i++)
    {
      wpA = wpB->prev[i];
      stateA = this->getState(wpA);

      //MSG("A %d.%d.%d %f",
      //      wpA->segmentId, wpA->laneId, wpA->waypointId, wpA->planCost);

      // Compute the cumulative cost to reach the goal from this node
      cost = wpB->planCost;
      dx = wpB->px - wpA->px;
      dy = wpB->py - wpA->py;
      cost += sqrtf(dx*dx + dy*dy);

      // If it is better than the current cost...
      if (cost < wpA->planCost)
      {
        // Push onto queue
        stateA->isOpen = true;
        wpA->planCost = cost;
        this->pushOpen(stateA);
      }
    }
  }

  // Clean up after ourselves
  this->freeStates();
  
  return 0;
}


// Find the optimial path using (A*)
int RailPlanner::genPlan(PlanGraphNode *nodeStart, PlanGraphNode *nodeGoal, PlanState **stateFinal)
{
  int i;
  bool found;
  float costG, costH;
  PlanState *state, *stateA, *stateB;
  PlanGraphNode *nodeA, *nodeB;
  float dx, dy, dist;

  // Initilize stats
  found = false;
  this->numSearched = 0;
  
  // Push the start node onto the open list.
  // The total cost is just the heuristic.
  state = this->getState(nodeStart);
  state->costG = 0;
  state->costH = this->calcH(state);
  state->cost = state->costG + state->costH;
  state->isOpen = true;
  this->pushOpen(state);
  
  // Get the open nodes one by one and run A*
  while (this->popOpen(&stateA))
  {
    // Dont re-open duplicates
    if (!stateA->isOpen)
      continue;
    this->numSearched++;
    
    // Get the corresponding node in the plan graph
    nodeA = stateA->node;

    // Mark it is closed for now (although it may get re-opened later)
    stateA->isClosed = true;

    //MSG("expanding %d.%d.%d.%d %f %f = %f",
    //    nodeA->segmentId, nodeA->laneId, nodeA->waypointId, nodeA->interId,
    //    stateA->costG, stateA->costH, stateA->cost);
        
    // See if we have arrived at the goal using Euclidean distance.
    // TODO: allow selectable termination conditions.
    dx = nodeA->pose.pos.x - nodeGoal->pose.pos.x;
    dy = nodeA->pose.pos.y - nodeGoal->pose.pos.y;
    dist = sqrtf(dx*dx + dy*dy);
    if (nodeA == nodeGoal || dist < 2.0) // MAGIC
    {
      *stateFinal = stateA;
      found = true;
      break;
    }
    // TESTING
    if (false)
    {
      // See if the node is outside the ROI
      dx = nodeA->pose.pos.x - nodeStart->pose.pos.x;
      dy = nodeA->pose.pos.y - nodeStart->pose.pos.y;
      dist = sqrtf(dx*dx + dy*dy);
      if (stateA->costH < FLT_MAX && dist > 128) // MAGIC
      {
        *stateFinal = stateA;
        found = true;
        break;
      }
    }

    // Look at all the outgoing arcs for this node.
    for (i = 0; i < nodeA->numNext; i++)
    {
      nodeB = nodeA->next[i];
      stateB = this->getState(nodeB);

      assert(nodeA != nodeB);
      assert(stateA != stateB);

      // lane change as a hard constraint
      if ( (this->laneChangeHardConstraint) && (this->restrictLaneChange) )
        if (nodeB->flags.isOncoming == 1) continue;
            
      // Compute cost so far plus the cost still to go.
      costG = this->calcG(stateA, stateB);
      costH = this->calcH(stateB);

      // Check if this next node is already is open or closed; if the
      // copy there is already better, dont bother adding this one.
      if (stateB->isOpen || stateB->isClosed)
      {
        if (costG + costH > stateB->costG + stateB->costH)
          continue;
      }
      
      // The node is no longer closed, so push it back onto the open
      // list with the new costs.
      stateB->costG = costG;
      stateB->costH = costH;
      stateB->cost = costG + costH;
      stateB->prior = stateA;      
      stateB->isClosed = false;
      stateB->isOpen = true;
      this->pushOpen(stateB);
    }
  }
  
  if (!found)
    return -1;
    
  return 0;
}


// Construct path from the state list
int RailPlanner::genPath(PlanStateList *states, PlanGraphPath *path)
{
  int i;
  PlanState *state;
  PlanGraphNode *node;

  // Initialize path
  path->valid = true;
  path->collideObs = 0;
  path->collideCar = 0;
  path->dist = 0;
  path->pathLen = 0;

  // Walk along the plan states
  for (i = 0; i < (int) states->size(); i++)
  {
    state = (*states)[i];
    node = state->node;

    // Update the total path distance
    if (i > 0)
    {
      PlanState *statePrev;
      PlanGraphNode *nodePrev;
      statePrev = (*states)[i - 1];
      nodePrev = statePrev->node;
      path->dist += vec2f_mag(vec2f_sub(node->pose.pos, nodePrev->pose.pos));      
    }    

    // Append node to the path list
    assert((size_t) path->pathLen < sizeof(path->nodes)/sizeof(path->nodes[0]));
    path->nodes[path->pathLen] = node;
    path->dists[path->pathLen] = path->dist;
    path->directions[path->pathLen] = PLAN_GRAPH_PATH_FWD;
    path->pathLen++;

    // Update collision fields
    if (node->status.obsCollision && this->graph->isStatusFresh(node->status.obsTime))
      path->collideObs = 1;
    if (node->status.carCollision && this->graph->isStatusFresh(node->status.carTime))
      path->collideCar = 1;

    /* REMOVE
    if (this->graph->isStatusFresh(node))
    {
      if (node->status.collideCar)
        path->collideCar = 1;
      if (node->status.collideObs)
        path->collideObs = 1;
    }
    */
  }
  
  return 0;
}


// Get the state information for a waypoint.  The state is stored in
// the user data pointer in the waypoint.
RailPlanner::PlanState *RailPlanner::getState(RNDFGraphWaypoint *wp)
{
  if (wp->data == NULL)
  {
    PlanState *state = (PlanState*) calloc(1, sizeof(PlanState));
    state->wp = wp;
    wp->data = state;
    this->dirtyList.push(state);
  }
  return (PlanState*) (wp->data);
}


// Get the state information for a PlanGraph node.  The state is
// stored in the user data pointer in the node.
RailPlanner::PlanState *RailPlanner::getState(PlanGraphNode *node)
{
  if (node->data == NULL)
  {
    PlanState *state = (PlanState*) calloc(1, sizeof(PlanState));
    state->node = node;
    node->data = state;
    this->dirtyList.push(state);
  }
  return (PlanState*) (node->data);
}


// Free the state information.  The state data is freed and removed
// from the user data pointers in the RNDF or plan graphs.
void RailPlanner::freeStates()
{
  PlanState *state;
  while (!this->dirtyList.empty())
  {
    state = this->dirtyList.front();
    this->dirtyList.pop();
    if (state->wp)
      state->wp->data = NULL;
    if (state->node)
      state->node->data = NULL;
    free(state);
  }  
  return;
}


// Push a state onto the open list
void RailPlanner::pushOpen(PlanState *state)
{
  this->openList.push(state);  
  return;
}


// Pop a state from the open list
bool RailPlanner::popOpen(PlanState **state)
{
  if (this->openList.empty())
    return false;
  *state = this->openList.top();
  this->openList.pop();
  return true;
}


// Clear the open list
void RailPlanner::clearOpen()
{
  while (!this->openList.empty())
    this->openList.pop();
  return;
}


// Dianostic: dump the entire graph with costs
int RailPlanner::dumpGraph()
{
  int i;
  PlanGraphNodeList nodes;
  PlanGraphNode *node;
  PlanState *state;

  this->graph->getRegion(&nodes, 0, 0, 8192, 8192);

  for (i = 0; i < (int) nodes.size(); i++)
  {
    node = nodes[i];
    state = (PlanState*) node->data;
    if (!state)
      continue;    
    fprintf(stdout, "graph %d.%d.%d.%d  %f %f  %f %f %f  %f\n",
            node->segmentId, node->laneId, node->waypointId, node->interId,
            node->pose.pos.x, node->pose.pos.y,
            state->costG, state->costH, state->cost, this->calcH(state));
  }
  fprintf(stdout, "\n\n");
  fflush(stdout);
  
  return 0;
}


// Diagnostics: print the states in a path
int RailPlanner::dumpStates(PlanStateList *states)
{
  int i;
  PlanState *state;
  PlanGraphNode *node;    

  for (i = 0; i < (int) states->size(); i++)
  {
    state = (*states)[i];
    node = state->node;
    fprintf(stdout, "states %d.%d.%d.%d  %f %f  %f %f %f\n",
            node->segmentId, node->laneId, node->waypointId, node->interId,
            node->pose.pos.x, node->pose.pos.y,
            state->costG, state->costH, state->cost);
  }
  fprintf(stdout, "\n\n");

  return 0;
}


// Diagnostics: print the path
int RailPlanner::dumpPath(PlanGraphPath *path)
{
  int i;
  PlanGraphNode *node;    

  for (i = 0; i < path->pathLen; i++)
  {
    node = path->nodes[i];
    fprintf(stdout, "path %d.%d.%d.%d  %f %f\n",
            node->segmentId, node->laneId, node->waypointId, node->interId,
            node->pose.pos.x, node->pose.pos.y);
  }
  fprintf(stdout, "\n\n");

  return 0;
}

