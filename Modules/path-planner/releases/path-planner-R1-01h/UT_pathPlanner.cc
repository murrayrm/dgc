/**********************************************************
 **
 **  UT_PLANNER.CC
 **
 **    Author: Noel du Toit
 **    Created: Mon Jul 16 16:20
 **
 **
 **********************************************************
 **
 **  Unit test for the planner stack
 **
 **********************************************************/

#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <graph-updater/GraphUpdater.hh>
#include <interfaces/VehicleState.h>
#include <interfaces/ActuatorState.h>
#include <frames/pose3.h>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <temp-planner-interfaces/Utils.hh>
#include <iostream>
#include <sstream>
#include <fstream>
#include <trajutils/traj.hh>
//#include <map/MapElement.hh>
//#include <map/MapElementTalker.hh>
#include <skynet/skynet.hh>

#include "PathPlanner.hh"


using namespace std;

int main(int argc, char **args)
{
  CmdArgs::RNDF_file = args[1];
  CmdArgs::sn_key = skynet_findkey(argc, args);

  // INITIALIZE THE MAP
  Map* map = new Map();
  map->loadRNDF(CmdArgs::RNDF_file);
  map->prior.delta = point2(3778410.5, 403942.3);
  vec3_t offset = { 3778410.5, 403942.3, 0 };

  // INITIALIZE THE GRAPH
  Graph_t* graph;
  //  GraphNode* node;
    
  GraphUpdater::init(&graph, offset, map);

  // PLAN PATH
  Path_t path;
  memset(&path, 0 ,sizeof(path));
  VehicleState vehState;
  memset(&vehState, 0, sizeof(vehState));
  ActuatorState actState;
  memset(&actState, 0, sizeof(actState));
  Cost_t cost;
  pose3_t finPose;
  double roll, pitch, yaw;

  // set the vehicle state to the first waypoint
  GraphNode* initNode = graph->getNodeFromRndfId(1,1,1);
  vehState.localX = initNode->pose.pos.x + 1;
  vehState.localY = initNode->pose.pos.y + 1;
  vehState.localZ = initNode->pose.pos.z;
  quat_to_rpy(initNode->pose.rot, &roll, &pitch, &yaw);
  vehState.localYaw = yaw;
  //  vehState.localX = 0;
  //  vehState.localY = .05;
  //  vehState.localZ = 0;
  //  quat_to_rpy(initNode->pose.rot, &roll, &pitch, &yaw);
  //  vehState.localYaw = -0.79;
  
  // set final pose
  GraphNode* finalNode = graph->getNodeFromRndfId(1,1,6);
  finPose = finalNode->pose;
  //  finPose.pos.x = 107.2;
  // finPose.pos.y = -32.4;
  // finPose.pos.z = 0;
  // finPose.rot = quat_from_rpy(0, 0, -0.79);

  // plan the path
  Path_params_t pathParams;
  PathPlanner::init();

  pathParams.flag = NO_PASS;
  pathParams.planFromCurrPos = true;
  if (pathParams.planFromCurrPos) {
    /* Update graph to account for the current car position */
    GraphUpdater::genVehicleSubGraph(graph, vehState, actState);
  }

  PathPlanner::planPath(&path, cost, graph, vehState, finPose, pathParams);
  
  // DISPLAY INFORMATION IN MAPVIEWER
  // DISPLAY GRAPH
  Utils::init();
  Utils::displayGraph(10, graph, vehState);
  // DISPLAY PATH
  Utils::displayPath(10, &path);
  
  
  // cycle 2
  sleep(5);
  // set the vehicle state to the first waypoint
  vehState.localX = initNode->pose.pos.x + 2;
  vehState.localY = initNode->pose.pos.y + 2;
  vehState.localZ = initNode->pose.pos.z;
  vehState.localYaw = yaw;
  
  pathParams.planFromCurrPos = false;
  if (pathParams.planFromCurrPos) {
    /* Update graph to account for the current car position */
    GraphUpdater::genVehicleSubGraph(graph, vehState, actState);
  }

  PathPlanner::planPath(&path, cost, graph, vehState, finPose, pathParams);
  
  // DISPLAY PATH
   Utils::displayPath(10, &path, MAP_COLOR_BLUE);
   //Utils::displayPath(10, &path);
  
}

