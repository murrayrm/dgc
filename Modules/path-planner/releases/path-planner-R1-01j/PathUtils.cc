/**********************************************************
 **
 **  PATHUTILS.CC
 **
 **    Time-stamp: <2007-08-03 18:35:00 ndutoit> 
 **
 **    Author: Noel du Toit
 **    Created: Thu Jul 26 08:59:31 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/
#include <math.h>

#include "PathUtils.hh"

#define EPS_X 0.0001
#define EPS_T 0.001
// TODO: talk to follower people and figure out what the right value for this parameter is
#define MAX_PATH_DIST 10.0


/*!
 * return the node on the path that is closest to Alice using euclidean dist
 */
PlanGraphNode* PathUtils::getNearestNodeEuclidean(PlanGraphPath* path, vec2f_t pos)
{
  PlanGraphNode* minNode = NULL;
  float dist, minDist = 100000000;
  
  for (int i=0; i<path->pathLen; i++) {
    if (!path->nodes[i]) break;
    dist = vec2f_mag(vec2f_sub(pos,path->nodes[i]->pose.pos));    
    if (dist < minDist) {
      minDist = dist;
      minNode = path->nodes[i];
    }
  }
  
  return minNode;
}

/*!
 * return the node on the path that is closest to Alice using projection.
 */
PlanGraphNode* PathUtils::getNearestNodeProjection(PlanGraphPath* path, pose2f_t pose)
{
  float heading, m1, c1, m2, c2, x, y;
  vec2f_t currPos;

  // Get Alice current pos
  heading = pose.rot;
  currPos = pose.pos;
  
  // project this point into the previous path we had
  int i;
  for (i=0; i+1< path->pathLen; i++) {
    // get params for line perp to alice's heading, through rear axle
    m1 = tan(getAngleInRange(heading+M_PI/2));
    c1 = currPos.y - m1*currPos.x;
    
    // get params for line between 2 nodes
    if (fabs(path->nodes[i+1]->pose.pos.x - path->nodes[i]->pose.pos.x)<EPS_X)
      m2 = (path->nodes[i+1]->pose.pos.y - path->nodes[i]->pose.pos.y)/EPS_X;
    else
      m2 = (path->nodes[i+1]->pose.pos.y - path->nodes[i]->pose.pos.y)/(path->nodes[i+1]->pose.pos.x - path->nodes[i]->pose.pos.x);
    c2 = path->nodes[i]->pose.pos.y - m2*path->nodes[i]->pose.pos.x;

    // get the intersection of these lines
    x = -(c1-c2)/(m1-m2);
    y = m1*x + c1;

    // check to see if this intersection is between these nodes. 
    // If so, use the first node in the path as the vehicle node. 
    // if not, move on to the next node
    float dist;
    float angle1, angle2, diff_angles;
    angle1 = atan2(path->nodes[i]->pose.pos.y - y, path->nodes[i]->pose.pos.x - x);
    angle2 = atan2(path->nodes[i+1]->pose.pos.y - y, path->nodes[i+1]->pose.pos.x - x);
    diff_angles = angle1 - angle2;
    diff_angles = PathUtils::getAngleInRange(diff_angles);
    if ( fabs(diff_angles) > EPS_T ) {
      // If the node on the path is too far away, we disregard it.
      dist = vec2f_mag(vec2f_sub(currPos, path->nodes[i]->pose.pos));
      if (dist < MAX_PATH_DIST) {
        return path->nodes[i];
      } else
        return NULL;
    }
  }
  // if we get here we did not find any intersections within the ranges
  // so something went wrong
  return NULL;
}


/**
 * @brief Returns an -PI, PI bounded angle\
 */
float PathUtils::getAngleInRange(float angle)
{
  float angleInRange = angle;

  while (angleInRange > M_PI) 
    angleInRange -= 2*M_PI;

  while (angleInRange <= -M_PI)
    angleInRange += 2*M_PI;

  return angleInRange;
}
