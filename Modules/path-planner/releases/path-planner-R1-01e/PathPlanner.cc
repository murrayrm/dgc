#include "PathPlanner.hh"
#include "PathUtils.hh"

#include <dgcutils/DGCutils.hh>
#include <temp-planner-interfaces/Log.hh>

CMapElementTalker PathPlanner::meTalker;
int PathPlanner::queueLen;
int PathPlanner::queueMax;
GraphNode** PathPlanner::queue;
PathPlannerConstraints PathPlanner::m_cons;
priority_queue<astar_list_entry, vector<astar_list_entry>, AStarCompare> PathPlanner::open_list;
vector<astar_list_entry> PathPlanner::closed_list;

// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)

// Common macros
#define MIN(a, b) ((a) <= (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

#define HI_RES_BOX_HALF_WIDTH  (50.0)
#define HI_RES_BOX_HALF_HEIGHT (50.0)

#define A_STAR_SEARCH 1

int PathPlanner::init()
{
  // Initialize map talker
  meTalker.initSendMapElement(CmdArgs::sn_key);

  //  initialize constraints/costs
  memset(&m_cons, 0, sizeof(m_cons));
  // Read the parameters in from a file
  updateCostsFromFile();

  // Set some defaults
  m_cons.enableReverse = 0;
  m_cons.changeCost   = m_cons.changeCost_nopass;
  m_cons.obsCost      = m_cons.obsCost_nopass;
  m_cons.carCost      = m_cons.carCost_nopass;
  
  // set queue parameters
  queueMax = 8192;
  queueLen = 0;
  queue = new GraphNode*[queueMax];
  
  return 0;
}

void PathPlanner::destroy()
{
  return;
}

Err_t PathPlanner::planPath(Path_t* path, Cost_t& totCost, Graph_t* graph, 
                            VehicleState &vehState, pose3_t finalPose, Path_params_t pathParams)
{
  if (pathParams.readPathPlanParams) {
    if (updateCostsFromFile() == 0)
      Console::addMessage("Read path planner parameter from file");
    else
      Console::addMessage("Error reading the parameter file");
  }

  if (pathParams.flag == NO_PASS) {
    m_cons.changeCost = m_cons.changeCost_nopass; // very high value
    m_cons.obsCost    = m_cons.obsCost_nopass; // lower value
    m_cons.carCost    = m_cons.carCost_nopass;
  } else {
    m_cons.changeCost = m_cons.changeCost_pass; // very high value
    m_cons.obsCost    = m_cons.obsCost_pass; // lower value
    m_cons.carCost    = m_cons.carCost_pass;
  }

  Err_t status = PP_OK;

  if (!pathParams.planFromCurrPos) {
    status |= PathUtils::planFromNearestNodeOnPath(path, graph, vehState);
    if (status & PP_NONODEFOUND) {
      // Console::addMessage("No node found on path, trying to find the closest node on graph");
      point2 currPos;
      currPos.set(AliceStateHelper::getPositionRearAxle(vehState)); 
      graph->vehicleNode = PathUtils::getNearestNodeOnPath(path, currPos);
    }
  }

  if (vehState.localX != finalPose.pos.x || vehState.localY != finalPose.pos.y) {
#if A_STAR_SEARCH == 0
    // Steps through nodes backwards to set the cost to the departure node
    status |= makePlan(finalPose, graph);
    // Search fwd from departure node to the destination node to find lowest cost solution
    status |= makePath(path, graph, vehState);
#else
    GraphNode *goalNode = graph->getNearestNode(finalPose.pos, GRAPH_NODE_LANE | GRAPH_NODE_TURN | GRAPH_NODE_ZONE, 10.0, 1);
    status |= astar_search(path, graph, vehState, graph->vehicleNode, goalNode);
#endif

    if (!graph->vehicleNode) {
      path->pathLen = 1;
      path->path[0] = NULL;
    }
  } else {
    if (graph->vehicleNode) {
      point2 currPos = AliceStateHelper::getPositionRearAxle(vehState);
      vec3_t vec = { currPos.x, currPos.y, 0 };
      graph->vehicleNode = graph->getNearestNode(vec, GRAPH_NODE_LANE | GRAPH_NODE_TURN, 10, 1);
    }
    path->pathLen = 1;
    path->path[0] = graph->vehicleNode;
  }

  // Set cost of the path
  // TODO: return the path cost
  totCost = 0;

  return status;
}

void PathPlanner::display(int sendSubgroup, Path_t* path, MapElementColorType colorType, MapId mapId)
{
  if (path->pathLen == 0 || path->path[0] == NULL) return;

  point2 point;
  vector<point2> points;
  point2 start, end;
  GraphNode* node;
  MapElement me;
  
  if (path->pathLen > 0) {
    for (int i=0; i<path->pathLen; i++) {
      node = path->path[i];
      if (!node) break;
      point.set(node->pose.pos.x,node->pose.pos.y);
      points.push_back(point);
    }
    me.setId(mapId);
    me.setTypeLine();
    if (path->path[0] && path->path[0]->pathDir == GRAPH_PATH_REV)
      me.setColor(MAP_COLOR_YELLOW,100);
    else 
      me.setColor(colorType, 100);
    me.setGeometry(points);
    meTalker.sendMapElement(&me,sendSubgroup);
  }
}

void PathPlanner::print(Path_t* path)
{
  Log::getStream(1) << "PathPlanner::print - Printing the path" << endl;
  for (int i=0; i<path->pathLen; i++) {
    if (path->path[i]) {
      Log::getStream(1) << "Path node " << i << " = (" << path->path[i]->pose.pos.x << "," << path->path[i]->pose.pos.y << ") and direction " << path->path[i]->pathDir << endl;
    }
  }
}

Err_t PathPlanner::astar_search(Path_t *path, Graph_t *graph, VehicleState &vehState, GraphNode *srcNode, GraphNode *dstNode)
{
  if (!srcNode) return PP_OK;
  if (!dstNode) return PP_OK;

  path->valid = true;
  path->collideObs = 0;
  path->collideCar = 0;
  path->goalDist = 0.0;
  path->pathLen = 0;

  /************************************************
   * Populate the vector of possible destinations *
   ************************************************/

  vector<GraphNode*> goals;
  // Find the node for the goal checkpoint
  // if the node is an entry point, we want to increase the destination area
  if (dstNode->isEntry) {
    // Get nodes further down the lane as well
    GraphNode *laneNode = dstNode;
    for (int i=0; i<3; i++) {
      // Try left and right nodes
      for (int rail = -1; rail < 2; rail++) {
        GraphNode *tmpNode = (rail == -1)?laneNode->leftNode:(rail == 0)?laneNode:laneNode->rightNode;
        if (!tmpNode) continue;
        goals.push_back(tmpNode);
      }
      laneNode = graph->getNextLaneNode(laneNode);
      if (!laneNode) break;
    }
  } else {
    goals.push_back(dstNode);
    if (dstNode->leftNode) goals.push_back(dstNode->leftNode);
    if (dstNode->rightNode) goals.push_back(dstNode->rightNode);
  }

  /***********************************
   * Initialize closed and open list *
   ***********************************/
  closed_list.clear();
  while (!open_list.empty()) open_list.pop();
  astar_list_entry connectingVertex;
  connectingVertex.node = srcNode;
  connectingVertex.pred = NULL;
  connectingVertex.heuristic_cost = heuristic(srcNode, dstNode);
  connectingVertex.goto_cost = 0;
  connectingVertex.cost = connectingVertex.heuristic_cost + connectingVertex.goto_cost;
  open_list.push(connectingVertex);

  /*****************
   * The algorithm *
   *****************/

  int index;
  GraphArc *arc;
  astar_list_entry closingVertex;
  unsigned int nodes_explored = 0;
  unsigned long long time1, time2;
  double t1, t2, t3, s1, s2;
  t1 = t2 = t3 = 0.0;
  while (!open_list.empty()) {
    nodes_explored++;
    s1 += open_list.size();
    s2 += closed_list.size();

    // Get open vertex with minimum cost
    closingVertex = open_list.top();
    open_list.pop();

    // If that vertex is part of the goals we are done
    bool found = false;
    for (unsigned int i=0; i<goals.size(); i++) {
      if (goals[i] == closingVertex.node) found = true;
    }
    if (found) {
      // Populate the path
      
      // First, backtrack the path (in the closed set)
      unsigned int size = 0;
      GraphNode *node_array[closed_list.size()+1];

      // Add final node to the reversed node array representing the path
      node_array[size++] = closingVertex.node;

      // While we didn't reach the source, add the predecessor to the node array
      while (closingVertex.node != srcNode) {
        index = contains(closed_list, closingVertex.pred);
        if (index == -1) return PP_NOPATH_LEN;

        closingVertex = closed_list[index];
        node_array[size++] = closingVertex.node;
      }

      // Second, create the path by reversing the array
      double stop_distance = pow(AliceStateHelper::getVelocityMag(vehState),2)/(2.0*0.5) + 30.0;
      int obs_cnt = 0;
      int status = PP_OK;
      GraphNode *prevNode = node_array[size-1];
      for (int i = size-1; i>=0; i--) {
        path->dists[path->pathLen] = path->goalDist;
        path->path[path->pathLen++] = node_array[i];

        path->collideCar = MAX(node_array[i]->collideCar, path->collideCar);
        if (node_array[i]->collideObs) obs_cnt++;
        if (obs_cnt > 1) {
          path->collideObs = MAX(path->collideObs, node_array[i]->collideObs);
          if (path->goalDist < stop_distance) status = PP_COLLISION;
        }
        path->goalDist += (float) vec3_mag(vec3_sub(prevNode->pose.pos, node_array[i]->pose.pos));
      }

      Log::getStream(1) << "  Contains execution time = " << t1 << " ms (" << s2/nodes_explored << ")" << endl;
      Log::getStream(1) << "  Push execution time = " << t2 << " ms (" << s1/nodes_explored << ")" << endl;
      Log::getStream(1) << "  Main execution time = " << t3 << " ms" << endl;
      Log::getStream(1) << "  Nodes explored = " << nodes_explored << endl;
      return status;
    }

    // If the closingVertex has already been closed, ignore
    DGCgettime(time1);
    if (contains(closed_list, closingVertex.node) != -1) continue;
    DGCgettime(time2);
    t1 += (time2-time1)/1000.0;

    closed_list.push_back(closingVertex);
    
    DGCgettime(time1);
    for (arc = closingVertex.node->outFirst; arc != NULL; arc = arc->outNext) {
      // Use multi-resolution
      if (arc->nodeB->isWaypoint) {
        if (isInsideHiResBox(graph->vehicleNode, arc->nodeB) || isAnySrcInsideHiResBox(graph, graph->vehicleNode, arc->nodeB)) {
          if (arc->type == GRAPH_ARC_WAYPOINT) {
            arc->ignored = true;
            continue;
          }
        } else {
          if (arc->type == GRAPH_ARC_NODE) {
            arc->ignored = true;
            continue;
          }
        }
      }
      arc->ignored = false;

      connectingVertex.node = arc->nodeB;
      int goto_cost = closingVertex.goto_cost + node_cost(connectingVertex.node) + arc_cost(arc);

      DGCgettime(time1);
      index = contains(closed_list, connectingVertex.node);
      DGCgettime(time2);
      t1 += (time2-time1)/1000.0;

      if (index == -1) {
        // connectingVertex is not in the closed list
        connectingVertex.heuristic_cost = heuristic(connectingVertex.node, dstNode);
        connectingVertex.goto_cost = goto_cost;
        connectingVertex.cost = connectingVertex.heuristic_cost + connectingVertex.goto_cost;
        connectingVertex.pred = closingVertex.node;

        DGCgettime(time1);
        open_list.push(connectingVertex);
        DGCgettime(time2);
        t2 += (time2-time1)/1000.0;

      } else if (goto_cost < closed_list[index].goto_cost) {
        // the new cost of going to that node is lower
        closed_list.erase(closed_list.begin()+index);
        connectingVertex.heuristic_cost = heuristic(connectingVertex.node, dstNode);
        connectingVertex.goto_cost = goto_cost;
        connectingVertex.cost = connectingVertex.heuristic_cost + connectingVertex.goto_cost;
        connectingVertex.pred = closingVertex.node;

        DGCgettime(time1);
        open_list.push(connectingVertex);
        DGCgettime(time2);
        t2 += (time2-time1)/1000.0;
      }
      DGCgettime(time2);
      t3 += (time2-time1)/1000.0;
    }
  }

  return PP_NOPATH_LEN;
}

int PathPlanner::node_cost(GraphNode *node)
{
  int cost = 0;

  // Discourage driving on the wrong side of the road.
  if (node->direction < 0)
    cost += m_cons.headOnCost;

  // Discourage driving on the left or right rail
  if (node->railId != 0)
    cost += m_cons.railCost;
  
  // Discourage using volatile graph
  if (node->type & GRAPH_NODE_VOLATILE)
    cost += m_cons.volatileCost;

  // Discourage collisions; we use the collision value to scale the
  // cost, because some collisions are worse than others.
  if (node->collideObs > 0)
    cost += m_cons.obsCost;

  // Discourage straying from the centerline of the lane
  cost += (int)(node->centerDist * m_cons.centerCost);

  return cost;
}

int PathPlanner::arc_cost(GraphArc *arc)
{
  int cost = 0;

  // Factor in length of the arc
  cost += arc->planDist;

  // Discourage lane changes
  if (arc->nodeA->segmentId == arc->nodeB->segmentId && arc->nodeA->laneId != arc->nodeB->laneId && !(arc->nodeA->type & GRAPH_NODE_LOCAL_CHANGE))
    cost += m_cons.changeCost;

  return cost;
}

int PathPlanner::heuristic(GraphNode *src, GraphNode *dst)
{
  int cost = 0;

  // We have to change lanes to go to the destination (at least two nodes in a change maneuver)
  if (src->direction == -1)
    cost += (m_cons.changeCost)*2;

  // We add least need to come back on the center rail (~10m long maneuver)
  cost += ((int)(src->centerDist * m_cons.centerCost))*5;

  // Distance to destination (use manhattan distance)
  vec3_t vec = vec3_sub(src->pose.pos, dst->pose.pos);
  double manhattan = ((int)(fabs(vec.x) + fabs(vec.y)))*100;
  double straight  = ((int)sqrt((vec.x*vec.x + vec.y*vec.y)))*100;
  cost += (int)(0.5*manhattan + 0.5*straight);

  return cost;
}

int PathPlanner::contains(vector<astar_list_entry> &list, GraphNode *node)
{
  for (unsigned int i = 0; i<list.size(); i++) {
    if (list[i].node == node) {
      return (int)i;
    }
  }

  return -1;
}

// Construct a plan for reaching the given pose
Err_t PathPlanner::makePlan(pose3_t pose, Graph_t* graph)
{
  GraphNode *node = graph->getNearestNode(pose.pos, GRAPH_NODE_LANE | GRAPH_NODE_TURN | GRAPH_NODE_ZONE, 10, 1);
  return makePlan(node, graph);
}

// Construct a plan for reaching the given checkpoint
Err_t PathPlanner::makePlan(GraphNode *node, Graph_t* graph)
{
  int i;
  int planCost, nplanCost;
  // Any node in goals will be reached
  vector<GraphNode*> goals;
  GraphNode *src, *dst;
  GraphArc *arc;

  if (!node) return PP_OK;

  // Find the node for the goal checkpoint
  // if the node is an entry point, we want to increase the destination area
  if (node->isEntry) {
    // Get nodes further down the lane as well
    GraphNode *laneNode = node;
    for (int i=0; i<3; i++) {
      // Try left and right nodes
      for (int rail = -1; rail < 2; rail++) {
        GraphNode *tmpNode = (rail == -1)?laneNode->leftNode:(rail == 0)?laneNode:laneNode->rightNode;
        if (!tmpNode) continue;
        goals.push_back(tmpNode);
      }
      laneNode = graph->getNextLaneNode(laneNode);
      if (!laneNode) break;
    }
  } else {
    goals.push_back(node);
    if (node->leftNode) goals.push_back(node->leftNode);
    if (node->rightNode) goals.push_back(node->rightNode);
  }

  // Reset the queue
  queueLen = 0;
  
  // Initialize costs and priority queue
  for (i = 0; i < graph->getNodeCount(); i++)
  {
    dst = graph->getNode(i);
    dst->planCost = GRAPH_PLAN_COST_MAX;
  }

  // Initialize the goal cost
  for (unsigned int i=0; i<goals.size(); i++) {
    pushNode(goals[i], 0);
  }

  while (true)
  {
    // Get the waiting node from the queue
    dst = popNode();
    if (!dst) {
      break;
    }

    // Hmmm, looks like there is no plan to this node, so
    // we must be done.
    if (dst->planCost >= GRAPH_PLAN_COST_MAX) {
      break;
    }
    // Got the vehicle node, so we are done.
    // Comment this out to test worst-case planning performance.
    if (dst == graph->vehicleNode)
      break;

    planCost = dst->planCost;

    // Discourage driving on the wrong side of the road.
    if (dst->direction < 0)
    {
      if (m_cons.headOnCost < 0)
        continue;
      planCost += m_cons.headOnCost;
    }

    // Discourage driving on the left or right rail
    if (dst->railId != 0)
    {
      if (m_cons.railCost < 0)
        continue;
      planCost += m_cons.railCost;
    }

    // Discourage using volatile graph
    if (dst->type & GRAPH_NODE_VOLATILE)
    {
      if (m_cons.volatileCost < 0)
        continue;
      planCost += m_cons.volatileCost;
    }

    // Discourage collisions; we use the collision value to scale the
    // cost, because some collisions are worse than others.
    if (dst->collideObs > 0)
    {
      if (m_cons.obsCost < 0)
        continue;
      planCost += m_cons.obsCost;
    }

    // Discourage straying from the centerline of the lane
    planCost += (int) (dst->centerDist * m_cons.centerCost);

    // Consider all incoming arcs for this node
    for (arc = dst->inFirst; arc != NULL; arc = arc->inNext)
    {
      assert(arc->nodeB == dst);

      // If outside hi res box, ignore intermediate points
      if (dst->isWaypoint) {
        if (isInsideHiResBox(graph->vehicleNode, dst) || isAnySrcInsideHiResBox(graph, graph->vehicleNode, dst)) {
          if (arc->type == GRAPH_ARC_WAYPOINT) {
            arc->ignored = true;
            continue;
          }
        } else {
          if (arc->type == GRAPH_ARC_NODE) {
            arc->ignored = true;
            continue;
          }
        }
      }
      arc->ignored = false;

      src = arc->nodeA;
      // MSG("src %d %d %f %d", i, src->id, src->planCost, src->direction);

      // Add distance cost for the arc
      nplanCost = planCost + arc->planDist;
      assert(nplanCost >= 0);
      assert(nplanCost > dst->planCost);

      // Discourage lane changes
      if (src->segmentId == dst->segmentId && src->laneId != dst->laneId && !(src->type & GRAPH_NODE_LOCAL_CHANGE))
        nplanCost += m_cons.changeCost;

      if (nplanCost < src->planCost) {
        pushNode(src, nplanCost);
      }
    }

    if (m_cons.enableReverse)
    {
      // Discourage driving in reverse
      planCost += m_cons.reverseCost;

      // Consider all outgoing arcs for this node; this allows for driving
      // in reverse
      for (arc = dst->outFirst; arc != NULL; arc = arc->outNext)
      {
        assert(arc->nodeA == dst);
        src = arc->nodeB;
        // MSG("src %d %d %f %d", i, src->id, src->planCost, src->direction);

        // Add distance cost for the arc
        nplanCost = planCost + arc->planDist;
        assert(nplanCost >= 0);
        assert(nplanCost > dst->planCost);
       
        if (nplanCost < src->planCost) {
          pushNode(src, nplanCost);
        }
      }
    }
  }
  
  return PP_OK;
}


// Construct path
Err_t PathPlanner::makePath(GraphPath *path, Graph_t* graph, VehicleState &vehState)
{
  // Temporary: Do not assume obstacle for only one node colliding
  int obsCnt = 0;

  Err_t status = PP_OK;
  int minCost;
  double stop_distance = pow(AliceStateHelper::getVelocityMag(vehState),2)/(2.0*0.5) + 30.0;
  GraphArc *arc;
  GraphNode *srcNode, *dstNode, *minNode;

  if (!graph->vehicleNode)
    return PP_OK;

  // Get the starting node
  assert(graph->vehicleNode);
  srcNode = graph->vehicleNode;

  path->valid = true;
  path->collideObs = 0;
  path->collideCar = 0;
  path->goalDist = 0;
  path->pathLen = 0;
 
  while (true)
  {
    assert((size_t) path->pathLen < sizeof(path->path)/sizeof(path->path[0]));
    path->dists[path->pathLen] = path->goalDist;
    path->path[path->pathLen++] = srcNode;

    // Does this path contains a collision?
    // Temporary!
    if (srcNode->collideObs) {
      obsCnt++;
    }
    if (obsCnt > 1) {
      path->collideObs = MAX(path->collideObs, srcNode->collideObs);
      if (path->goalDist < stop_distance)
        status = PP_COLLISION;
    }

    if (srcNode->collideCar) {
      path->collideCar = MAX(path->collideCar, srcNode->collideCar);
    }

    // No plan for this node
    if (srcNode->planCost == GRAPH_PLAN_COST_MAX)
      break;

    minCost = srcNode->planCost;
    minNode = NULL;
    
    // Consider all outgoing arcs for this node 
    for (arc = srcNode->outFirst; arc != NULL; arc = arc->outNext)
    {
      assert(arc->nodeA == srcNode);
      if (arc->ignored) continue;

      dstNode = arc->nodeB;
      if (dstNode->planCost < minCost)
      {
        minCost = dstNode->planCost;
        minNode = dstNode;
      }
    }

    if (m_cons.enableReverse)
    {
      // Consider all incoming arcs for this node (we may
      // have to drive in reverse).
      for (arc = srcNode->inFirst; arc != NULL; arc = arc->inNext)
      {
        assert(arc->nodeB == srcNode);
        if (arc->ignored) continue;

        dstNode = arc->nodeA;
        if (dstNode->planCost < minCost)
        {
          minCost = dstNode->planCost;
          minNode = dstNode;
        }
      }
    }

    // Found nothing, we must be done
    if (!minNode)
      break;

    // Keep track of the path length
    path->goalDist += (float) vec3_mag(vec3_sub(minNode->pose.pos, srcNode->pose.pos));
    srcNode = minNode;
  }

  // See if we reached to goal
  if (srcNode->planCost > 0)
  {
    path->valid = false;
    return PP_NOPATH_COST;
  }

  // See if we have a path
  if (path->pathLen < 2)
  {
    path->valid = false;
    return PP_NOPATH_LEN;
  }
  
  // MSG("path to goal (cost %d)", path->path[0]->planCost);
  // Console::addMessage("Plan cost = %d", path->path[0]->planCost);
  
  return status;
}

// Push node onto the priority queue
Err_t PathPlanner::pushNode(GraphNode *node, int planCost)
{
  int i;
  GraphNode *a, *b;
  
  assert(queueLen < queueMax);
  
  if (node->planCost == GRAPH_PLAN_COST_MAX)
  {
    // If the node is not already in the queue, insert it at the beginning.
    memmove(queue + 1, queue, queueLen * sizeof(queue[0]));
    queue[0] = node;
    queueLen++;
  }

  // Set the new plan cost
  node->planCost = planCost;

  // Bubble sort the queue.  This can work in one pass, since at most
  // one node has changed, and the value of that node must be less
  // than it's previous value.  Hence we only need to move that one
  // node towards the end of the queue.
  for (i = 0; i < queueLen - 1; i++)
  {
    a = queue[i + 0];
    b = queue[i + 1];
    if (a->planCost < b->planCost)
    {
      queue[i + 0] = b;
      queue[i + 1] = a;
    }
  }

  //MSG("queuelen %d", queueLen);

  return PP_OK;
}


// Pop node from the priority queue
GraphNode *PathPlanner::popNode()
{
  // Check for empty queue
  if (queueLen == 0)
    return NULL;
  return queue[--queueLen];  
}

bool PathPlanner::isInsideHiResBox(GraphNode *alice, GraphNode *node)
{
  double veh_x = alice->pose.pos.x;
  double veh_y = alice->pose.pos.y;
  double x = node->pose.pos.x;
  double y = node->pose.pos.y;

  if (x > veh_x + HI_RES_BOX_HALF_WIDTH || y > veh_y + HI_RES_BOX_HALF_HEIGHT ||
      x < veh_x - HI_RES_BOX_HALF_WIDTH || y < veh_y - HI_RES_BOX_HALF_HEIGHT) return false;
  return true;
}

bool PathPlanner::isAnySrcInsideHiResBox(Graph_t *graph, GraphNode *alice, GraphNode *node)
{
  GraphArc *arc;

  // for all incoming arc check whether any src is inside the high resolution box
  for (arc = node->inFirst; arc != NULL; arc = arc->inNext) {
    if (isInsideHiResBox(alice, arc->nodeA)) return true;
  }

  GraphNode *tmp = graph->getPrevLaneNode(node);
  if (!tmp) return false;
  while (!tmp->isWaypoint) {
    if (isInsideHiResBox(alice, tmp)) return true;
    tmp = graph->getPrevLaneNode(tmp);
    if (!tmp) return false;
  }

  return false;
}

int PathPlanner::updateCostsFromFile()
{
  FILE * pFile;
  pFile = fopen(CmdArgs::path_plan_config,"r");
  if (pFile != NULL) {
    ConfigFile config(CmdArgs::path_plan_config);
    config.readInto(m_cons.centerCost,"centerCost");
    config.readInto(m_cons.headOnCost,"headOnCost");
    config.readInto(m_cons.offRoadCost,"offRoadCost");
    config.readInto(m_cons.reverseCost,"reverseCost");
    config.readInto(m_cons.railCost,"railCost");
    config.readInto(m_cons.volatileCost,"volatileCost");
    config.readInto(m_cons.changeCost_nopass,"changeCost_nopass");
    config.readInto(m_cons.changeCost_pass,"changeCost_pass");
    config.readInto(m_cons.obsCost_nopass,"obsCost_nopass");
    config.readInto(m_cons.obsCost_pass,"obsCost_pass");
    config.readInto(m_cons.carCost_nopass,"carCost_nopass");
    config.readInto(m_cons.carCost_pass,"carCost_pass");
    fclose(pFile);
    return 0;
  }
    
  return -1;
}
