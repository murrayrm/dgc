#ifndef PATHUTILS_HH_
#define PATHUTILS_HH_

#include <math.h>

#include <frames/point2.hh>
#include <frames/pose3.h>
#include <interfaces/VehicleState.h>
#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <temp-planner-interfaces/AliceStateHelper.hh>


class PathUtils {

public: 
  static Err_t planFromNearestNodeOnPath(Path_t* path, Graph_t* graph, VehicleState vehState);
  static double getAngleInRange(double angle);
  static GraphNode* getNearestNode(Graph* graph, point2 point, int typeMask, double dist);

private :
};

#endif /*PATHUTILS_HH_*/




