#include "PathPlanner.hh"

CMapElementTalker PathPlanner::meTalker;
int PathPlanner::queueLen;
int PathPlanner::queueMax;
GraphNode** PathPlanner::queue;
PathPlannerConstraints PathPlanner::cons;

// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)

// Common macros
#define MIN(a, b) ((a) <= (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

int PathPlanner::init()
{
  // Initialize map talker
  meTalker.initSendMapElement(CmdArgs::sn_key);

  //  initialize constraints/costs
  memset(&cons, 0, sizeof(cons));
  cons.enableReverse = 0;
  cons.centerCost = 1000;
  cons.headOnCost =   100000;
  cons.offRoadCost =   10000;
  cons.reverseCost =   10000;
  cons.changeCost =    50000;
  cons.obsCost =     1000000;
  cons.carCost = 0;
  
  // set queue parameters
  queueMax = 8192;
  queueLen = 0;
  queue = new GraphNode*[queueMax];
  
  return 0;
}

void PathPlanner::destroy()
{
  return;
}

Err_t PathPlanner::planPath(Path_t* path, Cost_t& maxCost, Graph_t* graph, 
                            pose3_t finalPose, Path_params_t pathParams)
{
  // Use PathPlanner from GraphUpdater (to be removed)
  //PathPlanner *graphPlanner = GraphUpdater::getPathPlanner();

  // Steps through nodes backwards to set the cost to the departure node
  //  graphPlanner->makePlan(finalPose);
  makePlan(finalPose, graph);

  // Search fwd from departure node to the destination node to find lowest cost solution
  //  graphPlanner->makePath(path);
  makePath(path, graph);

  // Set cost of the path
  maxCost = 0;

  return PP_OK;
}

void PathPlanner::display(int sendSubgroup, Path_t* path)
{
  int counter=12000;
  point2 point;
  vector<point2> points;
  MapId mapId;
  GraphNode* node;
  MapElement me;
  
  mapId = counter;
  for (int i=0; i<path->pathLen; i++) {
    node = path->path[i];
    point.set(node->pose.pos.x,node->pose.pos.y);
    points.push_back(point);
  }
  me.setId(mapId);
  me.setTypeTravPath();
  me.setGeometry(points);
  meTalker.sendMapElement(&me,sendSubgroup);    
}

// Construct a plan for reaching the given pose
int PathPlanner::makePlan(pose3_t pose, Graph* graph)
{
  GraphNode *node = graph->getNearestNode(pose.pos, GRAPH_NODE_LANE | GRAPH_NODE_TURN, 10);
  return makePlan(node, graph);
}

// Construct a plan for reaching the given node
int PathPlanner::makePlan(int checkpointId, Graph* graph)
{
  GraphNode *node = graph->getNodeFromCheckpointId(checkpointId);
  return makePlan(node, graph);
}

// Construct a plan for reaching the given checkpoint
int PathPlanner::makePlan(GraphNode *node, Graph* graph)
{
  int i;
  int planCost, nplanCost;
  GraphNode *goal, *src, *dst;
  GraphArc *arc;

  // Find the node for the goal checkpoint
  goal = node;

  // Reset the queue
  queueLen = 0;
  
  // Initialize costs and priority queue
  for (i = 0; i < graph->getNodeCount(); i++)
  {
    dst = graph->getNode(i);
    dst->planCost = GRAPH_PLAN_COST_MAX;
  }

  // Initialize the goal cost
  pushNode(goal, 0);
  
  while (true)
  {
    // Get the waiting node from the queue
    dst = popNode();
    if (!dst)
      break;

    // Hmmm, looks like there is no plan to this node, so
    // we must be done.
    if (dst->planCost >= GRAPH_PLAN_COST_MAX)
      break;

    // Got the vehicle node, so we are done.
    // Comment this out to test worst-case planning performance.
    //if (dst->type == GRAPH_NODE_VEHICLE)
    //  break;

    planCost = dst->planCost;

    // Discourage driving on the wrong side of the road.
    if (dst->direction < 0)
    {
      if (cons.headOnCost < 0)
        continue;
      planCost += cons.headOnCost;
    }

    // Discourage collisions; we use the collision value to scale the
    // cost, because some collisions are worse than others.
    if (dst->collideObs > 0)
    {
      if (cons.obsCost < 0)
        continue;
      planCost += dst->collideObs * cons.obsCost;
    }

    // Discourage straying from the centerline of the lane
    planCost += (int) (dst->centerDist * cons.centerCost);

    // Consider all incoming arcs for this node
    for (arc = dst->inFirst; arc != NULL; arc = arc->inNext)
    {
      assert(arc->nodeB == dst);
      src = arc->nodeA;
      //MSG("src %d %d %f %d", i, src->id, src->planCost, src->direction);

      // Add distance cost for the arc
      nplanCost = planCost + arc->planDist;
      assert(nplanCost >= 0);
      assert(nplanCost > dst->planCost);

      // TESTING
      // Discourage lane changes
      if (src->segmentId == dst->segmentId && src->laneId != dst->laneId)
        nplanCost += cons.changeCost;

      if (nplanCost < src->planCost)
        pushNode(src, nplanCost);
    }

    if (cons.enableReverse)
    {
      // Discourage driving in reverse
      planCost += cons.reverseCost;

      // Consider all outgoing arcs for this node; this allows for driving
      // in reverse
      for (arc = dst->outFirst; arc != NULL; arc = arc->outNext)
      {
        assert(arc->nodeA == dst);
        src = arc->nodeB;
        //MSG("src %d %d %f %d", i, src->id, src->planCost, src->direction);

        // Add distance cost for the arc
        nplanCost = planCost + arc->planDist;
        assert(nplanCost >= 0);
        assert(nplanCost > dst->planCost);
       
        if (nplanCost < src->planCost)
          pushNode(src, nplanCost);
      }
    }
  }
  
  return 0;
}


// Construct path
int PathPlanner::makePath(GraphPath *path, Graph* graph)
{
  int minCost;
  GraphArc *arc;
  GraphNode *srcNode, *dstNode, *minNode;

  if (!graph->vehicleNode)
    return ERROR("no vehicle node");

  // Get the starting node
  assert(graph->vehicleNode);
  srcNode = graph->vehicleNode;

  path->valid = true;
  path->collideObs = 0;
  path->collideCar = 0;
  path->goalDist = 0;
  path->pathLen = 0;
  
  while (true)
  {
    //MSG("path %d %d %f", path->pathLen, srcNode->index, srcNode->planCost);
            
    assert((size_t) path->pathLen < sizeof(path->path)/sizeof(path->path[0]));
    path->path[path->pathLen++] = srcNode;
    
    // Does this path contains a collision?
    if (srcNode->collideObs)
      path->collideObs = MAX(path->collideObs, srcNode->collideObs);
    if (srcNode->collideCar)
      path->collideCar = MAX(path->collideCar, srcNode->collideCar);

    // No plan for this node
    if (srcNode->planCost == GRAPH_PLAN_COST_MAX)
      break;

    minCost = srcNode->planCost;
    minNode = NULL;
    
    // Consider all outgoing arcs for this node 
    for (arc = srcNode->outFirst; arc != NULL; arc = arc->outNext)
    {
      assert(arc->nodeA == srcNode);
      dstNode = arc->nodeB;
      if (dstNode->planCost < minCost)
      {
        minCost = dstNode->planCost;
        minNode = dstNode;
      }
    }

    if (cons.enableReverse)
    {
      // Consider all incoming arcs for this node (we may
      // have to drive in reverse).
      for (arc = srcNode->inFirst; arc != NULL; arc = arc->inNext)
      {
        assert(arc->nodeB == srcNode);
        dstNode = arc->nodeA;
        if (dstNode->planCost < minCost)
        {
          minCost = dstNode->planCost;
          minNode = dstNode;
        }
      }
    }

    // Found nothing, we must be done
    if (!minNode)
      break;

    // Keep track of the path length
    path->goalDist += (float) vec3_mag(vec3_sub(minNode->pose.pos, srcNode->pose.pos));
    
    srcNode = minNode;
  }

  // See if we have a path
  if (path->pathLen < 2)
  {
    path->valid = false;
    MSG("no path to goal (plan len %d)", path->pathLen);
    return -1;
  }
  
  // See if we reached to goal
  if (srcNode->planCost > 0)
  {
    path->valid = false;
    MSG("no path to goal (cost %d)", srcNode->planCost);
    return -1;
  }

  MSG("path to goal (cost %d)", path->path[0]->planCost);  
  
  return 0;
}



// Push node onto the priority queue
int PathPlanner::pushNode(GraphNode *node, int planCost)
{
  int i;
  GraphNode *a, *b;
  
  assert(queueLen < queueMax);
  
  if (node->planCost == GRAPH_PLAN_COST_MAX)
  {
    // If the node is not already in the queue, insert it at the beginning.
    memmove(queue + 1, queue, queueLen * sizeof(queue[0]));
    queue[0] = node;
    queueLen++;
  }

  // Set the new plan cost
  node->planCost = planCost;

  // Bubble sort the queue.  This can work in one pass, since at most
  // one node has changed, and the value of that node must be less
  // than it's previous value.  Hence we only need to move that one
  // node towards the end of the queue.
  for (i = 0; i < queueLen - 1; i++)
  {
    a = queue[i + 0];
    b = queue[i + 1];
    if (a->planCost < b->planCost)
    {
      queue[i + 0] = b;
      queue[i + 1] = a;
    }
  }

  //MSG("queuelen %d", queueLen);

  return 0;
}


// Pop node from the priority queue
GraphNode *PathPlanner::popNode()
{
  // Check for empty queue
  if (queueLen == 0)
    return NULL;
  return queue[--queueLen];  
}
