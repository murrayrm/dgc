/*
 * Copyright 2005, by the California Institute of Technology. ALL
 * RIGHTS RESERVED. United States Government Sponsorship
 * acknowledged. Any commercial use must be negotiated with the Office
 * of Technology Transfer at the California Institute of Technology.
 *
 * This software may be subject to U.S. export control laws. By
 * accepting this software, the user agrees to comply with all
 * applicable U.S. export laws and regulations. User has the
 * responsibility to obtain export licenses, or other export authority
 * as may be required before exporting such information to foreign
 * countries or providing access to foreign persons.
 */

/* Desc: Structures and operators for 3D poses and transforms
 * Author: Andrew Howard
 * Date: 5 Oct 2006
 * CVS: $Id: pose3.h,v 1.11 2006/12/06 16:43:11 abhoward Exp $
 */

/** @file 

@brief Structures and operators for 3D transformations.

@sa vec3.h quat.h

A pose @f$ p = (p_t, p_r) @f$ to consists of a 3-vector @f$ p_t @f$
describing the position of a body and a quaternion @f$ p_r @f$
describing its attitude.  The pose also describes a rigid-body
transformation between coordinate systems such that:

@f[ v' = p_r v p_r^\dagger + p_t @f]

where @f$ v @f$ is a vector measured the frame attached to the body
and @f$ v' @f$ corresponding vector in world coordinates.  For
compactness, we write the above expression as:

@f[ v' = p \otimes v @f]

Poses can be inverted: if @f$ p @f$ is the pose of the body in the
world frame, then @f$ p^{-1} @f$ is the pose of the world in the body
frame.  Equivalently, if @f$ p @f$ transforms points from the body
frame to the world frame, @f$ p^{-1} @f$ will transform points from
the world frame to the body frame:

@f[ v = p^{-1} \otimes v' @f]

Poses can be composed: if @f$ a @f$ is the pose of a camera measured
in the robot frame, and @f$ b @f$ is the pose of the robot in the
world frame, the pose @f$ c @f$ of the camera in the world frame is
given by @f$ c = b \otimes a @f$.  Equivalently, if @f$ a @f$
transforms points from the camera frame to the robot frame, and @f$ b
@f$ transforms points from the robot frame to the world frame, the
combined transform @f$ c = b \otimes a @f$ will transform points from
the camera frame to the world frame:

@f[ v' = c \otimes v = (b \otimes a) \otimes v = b \otimes (a \otimes v) @f]

Poses can also be converted to/from a 4x4 homogenous transform matrix @f$ M
@f$ such that:

@f[
\left[
\begin{array}{l}
v_x' \\ v_y' \\ v_z' \\ 1
\end{array}
\right]
=
\left[
\begin{array}{llll}
R_{00} & R_{01} & R_{02} & T_x \\
R_{10} & R_{11} & R_{12} & T_y \\
R_{20} & R_{21} & R_{22} & T_z \\
0 & 0 & 0 & 1 \\
\end{array}
\right]
\left[
\begin{array}{l}
v_x \\ v_y \\ v_z \\ 1
\end{array}
\right]
@f]

*/

#ifndef POSE3_H
#define POSE3_H

#include <math.h>
#include "quat.h"
#include "vec3.h"


/// @brief 3D pose data.
///
/// Represents coordinate transformation such that:
/// @f[ v' = r v r^\dagger + t @f]
/// where @f$ r @f$ is the rotation (unit quaterion) and @f$ t @f$ is
/// the translation (3-vector).
typedef struct
{
  vec3_t pos;
  quat_t rot;
} pose3_t;



/// @brief Returns the identity pose.
///
/// @returns Returns the identity pose.
static __inline__ pose3_t pose3_ident()
{
  pose3_t c = {{0, 0, 0}, {1, 0, 0, 0}};
  return c;
}


/// @brief Set the pose.
///
/// @param[in] pos Translation vector.
/// @param[in] rot Rotation quaternion.
/// @returns New pose.
static __inline__ pose3_t pose3_set(vec3_t pos, quat_t rot)
{
  pose3_t c = {pos, rot};
  return c;
}


/// @brief Test for finite pose.
///
/// Tests that all the components are valid and finite.
/// @param[in] p Pose to test.
/// @return Returns non-zero if the pose is finite.
static __inline__ int pose3_finite(pose3_t p)
{
  return quat_finite(p.rot) && vec3_finite(p.pos);
}


// Internal helper: multiply quatern by vector and return quatern
static __inline__ quat_t quat_mul_vec3(quat_t q, vec3_t v)
{
  quat_t u = {      0 - q.x*v.x - q.y*v.y - q.z*v.z,
              q.u*v.x +       0 + q.y*v.z - q.z*v.y,
              q.u*v.y - q.x*v.z +       0 + q.z*v.x,
              q.u*v.z + q.x*v.y - q.y*v.x +       0};
  return u;
}


// Internal helper: multiply vector by quatern and return vector
static __inline__ vec3_t vec3_mul_quat(quat_t v, quat_t q)
{
  vec3_t u = {-v.u*q.x + v.x*q.u - v.y*q.z + v.z*q.y,
              -v.u*q.y + v.x*q.z + v.y*q.u - v.z*q.x,
              -v.u*q.z - v.x*q.y + v.y*q.x + v.z*q.u};
  return u;
}


/// @brief Rotate vector by a quaternion.
///
/// Given a unit quaternion $q$ and vector $v$, compute the rotated
/// vector @f$ v' = q v q^\dagger @f$.
///
/// @param[in] q Unit quaterion representing the rotation.
/// @param[in] v 3-vector to rotate.
/// @returns Returns the rotated vector.
static __inline__ vec3_t vec3_rotate(quat_t q, vec3_t v)
{
  return vec3_mul_quat(quat_mul_vec3(q, v), q);
}


/// @brief Transform a vector.
///
/// Given pose @f$ p @f$ and vector @f$ v @f$, compute
/// the transformed vector @f$ v' @f$ such that:
/// @f[ v' = p \otimes v = p_r v p_r^\dagger + p_t @f]
static __inline__ vec3_t vec3_transform(pose3_t p, vec3_t v)
{
  return vec3_add(vec3_rotate(p.rot, v), p.pos);
}


/// @brief Compute the inverse pose.
///
/// Given some pose @f$ p @f$ such that:
/// @f[ v' = p \otimes v @f]
/// this function computes the inverse transform @f$ p^{-1} @f$:
/// @f[ v  = p^{-1} \otimes v' @f]
///
/// @param[in] p Pose to invert.
/// @return Returns the inverse pose.
static __inline__ pose3_t pose3_inv(pose3_t p)
{
  pose3_t q;
  q.rot = quat_conj(p.rot);
  q.pos = vec3_scale(-1, vec3_rotate(q.rot, p.pos));
  return q;
}


/// @brief Combine two transforms.
///
/// Given transforms @f$ b, a @f$ such that
/// @f[ v' = b \otimes a \otimes v @f]
/// compute the combined transform @f$ c = b \otimes a @f$:
/// @f[ v' = c \otimes v @f]
///
/// @param[in] b Transform to apply last.
/// @param[in] a Transform to apply first.
/// @returns Combined transform.
static __inline__ pose3_t pose3_mul(pose3_t b, pose3_t a)
{
  pose3_t c;
  c.rot = quat_unit(quat_mul(b.rot, a.rot));
  c.pos = vec3_add(vec3_rotate(b.rot, a.pos), b.pos);
  return c;
}


/// @brief Combine two transforms, with inverse.
///
/// Given transforms @f$ b, a @f$ such that
/// @f[ v' = b^{-1} \otimes a \otimes v @f]
/// compute the combined transform @f$ c = b^{-1} \otimes a @f$:
/// @f[ v' = c \otimes v @f]
///
/// @param[in] b Inverse of transform to apply last.
/// @param[in] a Transform to apply first.
/// @returns Combined transform.
static __inline__ pose3_t pose3_div(pose3_t b, pose3_t a)
{
  pose3_t c;
  c = pose3_mul(pose3_inv(b), a);
  return c;
}


/// @brief Sum two transforms.
/// @deprecated
static __inline__ pose3_t pose3_add(pose3_t b, pose3_t a)
{
  return pose3_mul(a, b);
}


/// @brief Subtract two transforms.
/// @deprecated
static __inline__ pose3_t pose3_sub(pose3_t b, pose3_t a)
{
  return pose3_div(a, b);
}


/// @brief Create a pose from a 4x4 rotation matrix of floats.
static __inline__ pose3_t pose3_from_mat44f(float m[4][4])
{
  vec3_t a;
  quat_t b;
  float t, s;
  a.x = m[0][3];
  a.y = m[1][3];
  a.z = m[2][3];
  t = 1 + m[0][0] + m[1][1] + m[2][2];
  if (t > 1e-8)
  {
    s = 0.5 / sqrtf(t);
    b.u = 0.25 / s;
    b.x = (m[2][1] - m[1][2]) * s;
    b.y = (m[0][2] - m[2][0]) * s;
    b.z = (m[1][0] - m[0][1]) * s;
  }
  else if (m[0][0] > m[1][1] && m[0][0] > m[2][2])
  {
    s = sqrtf(1 + m[0][0] - m[1][1] - m[2][2]) * 2;
    b.x = 0.25 * s;
    b.y = (m[0][1] + m[1][0]) / s;
    b.z = (m[0][2] + m[2][0]) / s;
    b.u = (m[2][1] - m[1][2]) / s;    
  }
  else if (m[1][1] > m[2][2])
  {
    s = sqrtf(1 + m[1][1] - m[0][0] - m[2][2]) * 2;
    b.x = (m[0][1] + m[1][0]) / s;
    b.y = 0.25 * s;
    b.z = (m[1][2] + m[2][1]) / s;
    b.u = (m[0][2] - m[2][0]) / s;    
  }
  else
  {
    s = sqrtf(1 + m[2][2] - m[0][0] - m[1][1]) * 2;
    b.x = (m[0][2] + m[2][0]) / s;
    b.y = (m[1][2] + m[2][1]) / s;
    b.z = 0.25 * s;
    b.u = (m[1][0] - m[0][1]) / s;    
  }
  return pose3_set(a, quat_unit(b));
}


/// @brief Convert to homogeneous 4x4 matrix of doubles.
///
/// @param[in] p Transform.
/// @param[out] m Homogenous transform matrix.
static __inline__ void pose3_to_mat44d(pose3_t p, double m[4][4])
{
  double tmp1, tmp2;
  double squ, sqx, sqy, sqz;

  squ = p.rot.u*p.rot.u;
  sqx = p.rot.x*p.rot.x;
  sqy = p.rot.y*p.rot.y;
  sqz = p.rot.z*p.rot.z;
  
  m[0][0] =  sqx - sqy - sqz + squ;
  m[1][1] = -sqx + sqy - sqz + squ;
  m[2][2] = -sqx - sqy + sqz + squ;
  
  tmp1 = p.rot.x*p.rot.y;
  tmp2 = p.rot.z*p.rot.u;
  m[1][0] = 2.0 * (tmp1 + tmp2);
  m[0][1] = 2.0 * (tmp1 - tmp2);
  tmp1 = p.rot.x*p.rot.z;
  tmp2 = p.rot.y*p.rot.u;
  m[2][0] = 2.0 * (tmp1 - tmp2);
  m[0][2] = 2.0 * (tmp1 + tmp2);
  tmp1 = p.rot.y*p.rot.z;
  tmp2 = p.rot.x*p.rot.u;
  m[2][1] = 2.0 * (tmp1 + tmp2);
  m[1][2] = 2.0 * (tmp1 - tmp2);
  
  m[0][3] = p.pos.x;
  m[1][3] = p.pos.y;
  m[2][3] = p.pos.z;

  m[3][0] = 0;
  m[3][1] = 0;
  m[3][2] = 0;
  m[3][3] = 1;

  return;
}


/// @brief Convert to homogeneous 4x4 matrix of floats.
static __inline__ void pose3_to_mat44f(pose3_t p, float m[4][4])
{
  double tmp1, tmp2;
  double squ, sqx, sqy, sqz;

  squ = p.rot.u*p.rot.u;
  sqx = p.rot.x*p.rot.x;
  sqy = p.rot.y*p.rot.y;
  sqz = p.rot.z*p.rot.z;
  
  m[0][0] =  sqx - sqy - sqz + squ;
  m[1][1] = -sqx + sqy - sqz + squ;
  m[2][2] = -sqx - sqy + sqz + squ;
  
  tmp1 = p.rot.x*p.rot.y;
  tmp2 = p.rot.z*p.rot.u;
  m[1][0] = 2.0 * (tmp1 + tmp2);
  m[0][1] = 2.0 * (tmp1 - tmp2);
  tmp1 = p.rot.x*p.rot.z;
  tmp2 = p.rot.y*p.rot.u;
  m[2][0] = 2.0 * (tmp1 - tmp2);
  m[0][2] = 2.0 * (tmp1 + tmp2);
  tmp1 = p.rot.y*p.rot.z;
  tmp2 = p.rot.x*p.rot.u;
  m[2][1] = 2.0 * (tmp1 + tmp2);
  m[1][2] = 2.0 * (tmp1 - tmp2);
  
  m[0][3] = p.pos.x;
  m[1][3] = p.pos.y;
  m[2][3] = p.pos.z;

  m[3][0] = 0;
  m[3][1] = 0;
  m[3][2] = 0;
  m[3][3] = 1;

  return;
}


#endif
