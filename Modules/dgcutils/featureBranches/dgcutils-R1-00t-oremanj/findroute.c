/*!
 * \file findroute.c
 * \brief Find path to configuration files
 *
 * \author Richard M. Murray
 * \date 10 February 2007
 *
 * This file contains a small utility function for finding a route
 * file (mdf, fddf, rndf) by looking in standard places.  This
 * function returns a string, so you can say:
 *
 *   rddf.loadFile(dgcFindRouteFile(cmdline.rddf_arg));
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>

#include "findroute.h"
int findroute_verbose = 0;

/* Look for configuration file and return path */
char *dgcFindRouteFile(char *file)
{
  int found = 0;			/* have we found the file yet? */
  struct stat buf;			/* status buffer for fstat */

  /* Allocate space for the resulting path name */
  char *cfgfile = malloc(PATH_MAX+1);

  /* First see if the file exists in the current directory */
  snprintf(cfgfile, PATH_MAX, "./%s", file);
  if (stat(cfgfile, &buf) == 0) found = 1;  

  /* Parse the string into pieces */
  char *routedir, *routename, *routebuf = NULL;
  if (!found) {
    char *routebuf = malloc(PATH_MAX+1);
    strcpy(routebuf, file);		/* make a copy of the string */
    routename = routebuf;		/* store pointer for strsep */
    routedir = strsep(&routename, "_");	/* split out the route name */
  }
  if (findroute_verbose) {
    fprintf(stderr, "findroute: dir = %s, name = %s\n", routedir, routename);
  }

  /* If not, see if DGC_CONFIG_PATH is set and look there */
  char *dgc_config_path = getenv("DGC_CONFIG_PATH");
  if (!found && dgc_config_path != NULL) {
    snprintf(cfgfile, PATH_MAX, "%s/routes-%s/%s_%s", dgc_config_path, 
             routedir, routedir, routename);
    if (stat(cfgfile, &buf) == 0) found = 1;  
  }

  /* If DGC_CONFIG_PATH is not set, try guessing from the current
   * directory */
  if (!found && getenv("PWD")) {
    char *end;

    /* See if we are in one of the binary or source directories */
    strcpy(cfgfile, getenv("PWD"));
    if ((end = strstr(cfgfile, "/bin")) || (end = strstr(cfgfile, "/src"))) {
      /* strstr() returns a pointer to the start of the search string */
      *end = '\0';
    }
    strcat(cfgfile, "/etc/");

    /* Append the name of the file */
    if (routedir != NULL) {
      strcat(cfgfile, "routes-"); strcat(cfgfile, routedir); 
      strcat(cfgfile, "/");
    }
    strcat(cfgfile, routedir); strcat(cfgfile, "_"); strcat(cfgfile, routename);
    if (stat(cfgfile, &buf) == 0) found = 1;  
  }
  
  /* Finally, try looking in /dgc/etc for the file */
  if (!found) {
    snprintf(cfgfile, PATH_MAX, "/dgc/etc/routes-%s/%s_%s",
             routedir, routedir, file);
    if (stat(cfgfile, &buf) == 0) found = 1;  
  }

  /* If we didn't find anything, just return the name we were passed */
  if (!found) {
    strncpy(cfgfile, file, PATH_MAX);
    fprintf(stderr, "Warning: couldn't find config file %s\n", cfgfile);
  }

  /* Return whatever we found */
  if (routebuf) free(routebuf);
  return (cfgfile);
}
