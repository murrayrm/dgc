/*!
 * \file UT_findroute.c
 * \brief Unit test for DGCFindRouteFile()
 *
 * \author Richard M. Murray
 * \date 18 May 2007
 *
 * This file can be used to test the DGCFindRouteFile() routine.  It
 * takes a command line argument indicating the file you want to find
 * and returns the file that is found.
 *
 */

#include <stdio.h>
#include "findroute.h"

int main(int argc, char **argv)
{
  puts(dgcFindRouteFile(argv[1]));
  return 0;
}
