/*
 * Copyright 2005, by the California Institute of Technology. ALL
 * RIGHTS RESERVED. United States Government Sponsorship
 * acknowledged. Any commercial use must be negotiated with the Office
 * of Technology Transfer at the California Institute of Technology.
 *
 * This software may be subject to U.S. export control laws. By
 * accepting this software, the user agrees to comply with all
 * applicable U.S. export laws and regulations. User has the
 * responsibility to obtain export licenses, or other export authority
 * as may be required before exporting such information to foreign
 * countries or providing access to foreign persons.
 */

/* 
 * Desc: An FLTK widget for doing GL stuff
 * Date: 19 October 2005
 * Author: Andrew Howard
 * CVS: $Id: Fl_Glv_Window.H,v 1.10 2007/05/03 07:03:10 abhoward Exp $
*/

#ifndef FL_GLV_WINDOW_H
#define FL_GLV_WINDOW_H

#include <FL/Fl.H>
#include <FL/gl.h>
#include <GL/glu.h>
#include <FL/Fl_Gl_Window.H>


// OpenGL window
class Fl_Glv_Window : public Fl_Gl_Window
{
  public:
  
  // Constructor
  Fl_Glv_Window(int x, int y, int w, int h, void *data, Fl_Callback *draw);

  // Constructor
  Fl_Glv_Window(int x, int y, int w, int h, void *data, Fl_Callback *predraw, Fl_Callback *draw);

  // Set the horizontal field-of-view
  void set_hfov(float hfov);
  
  // Set the "up" vector using a string descriptor: +x -x +y -y +z -z.
  void set_up(const char *up);

  // Set the clipping planes
  void set_clip(float near, float far);

  // Set where we are looking
  void set_lookat(float eye_x, float eye_y, float eye_z,
                  float at_x, float at_y, float at_z,
                  float up_x, float up_y, float up_z);

  protected:

  // Draw the window
  virtual void draw();
  
  // Handle events
  virtual int handle(int event);

  // Process mouse events
  void handle_mouse(int x, int y);

  private:

  // Callback functions
  void *data;
  Fl_Callback *predraw_callback;
  Fl_Callback *draw_callback;

  // Drag settings
  float mouse_pos_x, mouse_pos_y, mouse_pos_z;
  float mouse_eye_x, mouse_eye_y, mouse_eye_z;
  float mouse_at_x, mouse_at_y, mouse_at_z;

  // Field of view
  float hfov;
  
  // Near and far clipping distances
  float near_clip, far_clip;

  public:

  // Camera pose
  float eye_x, eye_y, eye_z;
  float at_x, at_y, at_z;
  float up_x, up_y, up_z;

  // Mouse settings
  int mouse_state, mouse_button, mouse_shift;
  float mouse_x, mouse_y;
};


#endif
