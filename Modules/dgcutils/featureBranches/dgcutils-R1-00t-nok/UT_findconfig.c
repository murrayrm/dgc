/*!
 * \file UT_findconfig.c
 * \brief Unit test for DGCFindConfigFile()
 *
 * \author Richard M. Murray
 * \date 18 May 2007
 *
 * This file can be used to test the DGCFindConfigFile() routine.  It
 * takes a command line argument indicating the file you want to find
 * and returns the file that is found.
 *
 */

#include <stdio.h>
#include "cfgfile.h"

int main(int argc, char **argv)
{
  if (argc < 3) 
    fprintf(stderr, "Usage: UT_findconfig file module\n");
  else
    puts(dgcFindConfigFile(argv[1], argv[2]));

  return 0;
}
