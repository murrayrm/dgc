/* 
 * Desc: An FLTK widget for doing GL stuff
 * Date: 19 October 2005
 * Author: Andrew Howard
 * CVS: $Id: Fl_Glv_Window.cc,v 1.16 2007/05/05 00:57:59 abhoward Exp $
*/

#include <math.h>
#include <stdio.h>
#include <string.h>
#include "Fl_Glv_Window.H"


// Constructor
Fl_Glv_Window::Fl_Glv_Window(int x, int y, int w, int h, void *data, Fl_Callback *draw)
    : Fl_Gl_Window(x, y, w, h, NULL)
{
  this->data = data;
  this->predraw_callback = NULL;
  this->draw_callback = draw;

  // Use double-buffered window
  // REMOVE? this->mode(FL_RGB | FL_ALPHA | FL_DEPTH | FL_DOUBLE);

  // Initialize GL camera pose
  this->eye_x = -2;
  this->eye_y = -2;
  this->eye_z = -2;
  this->at_x = 0;
  this->at_y = 0;
  this->at_z = 0;
  this->up_x = 0;
  this->up_y = 0;
  this->up_z = -1;

  this->mouse_x = 0;
  this->mouse_y = 0;
  this->mouse_button = -1;

  this->hfov = 70;
  this->near_clip = 0.2;
  this->far_clip = 50;
  
  return;
}


// Constructor
Fl_Glv_Window::Fl_Glv_Window(int x, int y, int w, int h, void *data,
                             Fl_Callback *predraw, Fl_Callback *draw)
    : Fl_Gl_Window(x, y, w, h, NULL)
{
  this->data = data;
  this->predraw_callback = predraw;
  this->draw_callback = draw;

  // Initialize GL camera pose
  this->eye_x = -2;
  this->eye_y = -2;
  this->eye_z = -2;
  this->at_x = 0;
  this->at_y = 0;
  this->at_z = 0;
  this->up_x = 0;
  this->up_y = 0;
  this->up_z = -1;

  this->mouse_x = 0;
  this->mouse_y = 0;
  this->mouse_button = -1;

  this->hfov = 70;
  this->near_clip = 0.2;
  this->far_clip = 50;
  
  return;
}


// Set the "up" vector using a string descriptor: +x -x +y -y +z -z.
void Fl_Glv_Window::set_up(const char *up)
{
  this->up_x = 0;
  this->up_y = 0;
  this->up_z = 0;
  
  // Set the up direction
  if (strcmp(up, "+x") == 0)
    this->up_x = +1;
  else if (strcmp(up, "-x") == 0)
    this->up_x = -1;
  else if (strcmp(up, "+y") == 0)
    this->up_y = +1;
  else if (strcmp(up, "-y") == 0)
    this->up_y = -1;
  else if (strcmp(up, "+z") == 0)
    this->up_z = +1;
  else if (strcmp(up, "-z") == 0)
    this->up_z = -1;
  else
    this->up_z = -1;

  return;
}


// Set the horizontal field-of-view
void Fl_Glv_Window::set_hfov(float hfov)
{
  this->hfov = hfov;
  return;
}


// Set the clipping planes
void Fl_Glv_Window::set_clip(float near, float far)
{
  this->near_clip = near;
  this->far_clip = far;
  return;
}

// Set where we are looking
void Fl_Glv_Window::set_lookat(float eye_x, float eye_y, float eye_z,
                               float at_x, float at_y, float at_z,
                               float up_x, float up_y, float up_z)
{
  this->eye_x = eye_x;
  this->eye_y = eye_y;
  this->eye_z = eye_z;
  
  this->at_x = at_x;
  this->at_y = at_y;
  this->at_z = at_z;

  this->up_x = up_x;
  this->up_y = up_y;
  this->up_z = up_z;

  return;
}


// Draw the window
void Fl_Glv_Window::draw()
{
  // Make sure we are using the correct context
  this->make_current();

  // Use the whole window as the context
  glViewport(0, 0, this->w(), this->h());
 
  glClearColor(0, 0, 0, 0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_POINT_SMOOTH);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(this->hfov,
                 (float) this->w() / (float) this->h(), this->near_clip, this->far_clip);

  if (this->predraw_callback)
    (*this->predraw_callback) (this, this->data);
 
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();    
  gluLookAt(this->eye_x, this->eye_y, this->eye_z,
            this->at_x, this->at_y, this->at_z,
            this->up_x, this->up_y, this->up_z);

  if (this->draw_callback)
  {
    (*this->draw_callback) (this, this->data);
    return;
  }
      
  // Show the origin of the world coordinate system
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(1, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 1, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, 1);
  glEnd();
  
  return;
};


// Handle events
int Fl_Glv_Window::handle(int event)
{  
  switch (event)
  {
    case FL_PUSH:
    {
      // Get mouse button; we expect 3-buttons
      this->mouse_button = Fl::event_button();
            
      // Check modifier buttons to emulate 3-button mouse on Mac.
      if (Fl::event_state() & FL_ALT)
        this->mouse_button = FL_MIDDLE_MOUSE;
      else if (Fl::event_state() & FL_META)
        this->mouse_button = FL_RIGHT_MOUSE;
      
      this->mouse_state = 1;
      this->mouse_shift = Fl::event_shift();
      this->mouse_x = Fl::event_x();
      this->mouse_y = Fl::event_y();  
      this->mouse_pos_x = Fl::event_x();
      this->mouse_pos_y = Fl::event_y();
      this->mouse_eye_x = this->eye_x;
      this->mouse_eye_y = this->eye_y;
      this->mouse_eye_z = this->eye_z;
      this->mouse_at_x = this->at_x;
      this->mouse_at_y = this->at_y;
      this->mouse_at_z = this->at_z;
      redraw();
      return 1;
    }

    case FL_DRAG:
    {
      this->mouse_state = 2;
      this->mouse_x = Fl::event_x();
      this->mouse_y = Fl::event_y();  
      handle_mouse(Fl::event_x(), Fl::event_y());
      redraw();
      return 1;
    }

    case FL_RELEASE:
    {
      this->mouse_state = 0;
      this->mouse_button = -1;
      this->mouse_shift = false;
      this->mouse_x = Fl::event_x();
      this->mouse_y = Fl::event_y();  
      redraw();
      return 1;
    }
  }
  return Fl_Gl_Window::handle(event);
}


// Process mouse events
void Fl_Glv_Window::handle_mouse(int x, int y)
{
  double u_x, u_y, u_z;
  double v_x, v_y, v_z;
  double w_x, w_y, w_z;
  double dx, dy, ra, rb;
  double m[16];
  
  if (this->mouse_button == FL_MIDDLE_MOUSE)
  {
    dx = -360 * (x - this->mouse_pos_x) / this->w();
    dy = -360 * (y - this->mouse_pos_y) / this->h(); 
    
    v_x = this->mouse_eye_x - this->at_x;
    v_y = this->mouse_eye_y - this->at_y;
    v_z = this->mouse_eye_z - this->at_z;

    u_x = this->up_x;
    u_y = this->up_y;
    u_z = this->up_z;

    // Construct vector orthogonal to the up vector and the vector
    // joining eye and at
    w_x = +u_y * v_z - u_z * v_y;
    w_y = -u_x * v_z + u_z * v_x;
    w_z = +u_x * v_y - u_y * v_x;

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();

    // Construct a rotation matrix around the current up and the orthogonal vector
    glLoadIdentity();
    glRotatef(dx, u_x, u_y, u_z);
    glRotatef(dy, w_x, w_y, w_z);
    glGetDoublev(GL_MODELVIEW_MATRIX, m);
    glPopMatrix();

    // Shift the eye around the at point
    this->eye_x = this->at_x + m[0] * v_x + m[4] * v_y + m[8] * v_z;
    this->eye_y = this->at_y + m[1] * v_x + m[5] * v_y + m[9] * v_z;
    this->eye_z = this->at_z + m[2] * v_x + m[6] * v_y + m[10] * v_z;
  }

  if (this->mouse_button == FL_RIGHT_MOUSE)
  {
    dx = (double) x / this->w();
    dy = (double) y / this->h();
    ra = sqrt(dx * dx + dy * dy);
    
    dx = (double) this->mouse_pos_x / this->w();
    dy = (double) this->mouse_pos_y / this->h();
    rb = sqrt(dx * dx + dy * dy);
    
    v_x = this->mouse_eye_x - this->at_x;
    v_y = this->mouse_eye_y - this->at_y;
    v_z = this->mouse_eye_z - this->at_z;

    // Move the eye point towards or away from the at point
    v_x *= exp(rb - ra);
    v_y *= exp(rb - ra);
    v_z *= exp(rb - ra);

    this->eye_x = this->at_x + v_x;
    this->eye_y = this->at_y + v_y;
    this->eye_z = this->at_z + v_z;
  }

  if (this->mouse_button == FL_LEFT_MOUSE)
  {
    u_x = this->up_x;
    u_y = this->up_y;
    u_z = this->up_z;
        
    v_x = this->mouse_eye_x - this->mouse_at_x;
    v_y = this->mouse_eye_y - this->mouse_at_y;
    v_z = this->mouse_eye_z - this->mouse_at_z;

    ra = sqrt(v_x * v_x + v_y * v_y + v_z * v_z);
    v_x /= ra;
    v_y /= ra;
    v_z /= ra;

    // Scale the translation according to the distance from the eye/at
    // distance.  Should probably modify this to acccount for the
    // field of view.
    dx = -ra * (x - this->mouse_pos_x) / this->w();
    dy = +ra * (y - this->mouse_pos_y) / this->h(); 
    
    // Construct vector orthogonal to the up vector and the vector
    // joining eye and at
    w_x = +u_y * v_z - u_z * v_y;
    w_y = -u_x * v_z + u_z * v_x;
    w_z = +u_x * v_y - u_y * v_x;

    ra = sqrt(w_x * w_x + w_y * w_y + w_z * w_z);
    w_x /= ra;
    w_y /= ra;
    w_z /= ra;

    // Construct vector orthogonal to both vectors
    v_x = +u_y * w_z - u_z * w_y;
    v_y = -u_x * w_z + u_z * w_x;
    v_z = +u_x * w_y - u_y * w_x;

    ra = sqrt(v_x * v_x + v_y * v_y + v_z * v_z);
    v_x /= ra;
    v_y /= ra;
    v_z /= ra;

    // Translate the at and eye points together
    this->at_x = this->mouse_at_x + dx * w_x + dy * v_x;
    this->at_y = this->mouse_at_y + dx * w_y + dy * v_y;
    this->at_z = this->mouse_at_z + dx * w_z + dy * v_z;
    
    this->eye_x = this->mouse_eye_x + dx * w_x + dy * v_x;
    this->eye_y = this->mouse_eye_y + dx * w_y + dy * v_y;
    this->eye_z = this->mouse_eye_z + dx * w_z + dy * v_z;
  }

  return;
}
