
/*
 Desc: Useful image functions
 Author: Andrew Howard
 Date: 3 Feb 2005
 SVN: $Id: dgc_image.c,v 1.28 2007/09/14 07:03:00 abhoward Exp $
*/

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <netinet/in.h>
#include <unistd.h>

#include "dgc_image.h"


// RAN pixels get manipulated for endianness; this union helps.
typedef union {uint32_t i; float f;} ran_pixel_t;

// Local functions
static void dgc_image_write_caption(const dgc_image_t *self, FILE *file, const char *caption);

// Read header from a PNM file.
static int dgc_read_pnm_header(const char *filename,
                                       char *caption, size_t caption_size,
                                       int *cols, int *rows, int *channels, int *bits,
                                       size_t *header_size);

#define DGC_ERROR_SET(err, ...) \
  (fprintf(stderr, __VA_ARGS__) ? err : err)


// Create a new image object.
dgc_image_t *dgc_image_alloc(int cols, int rows, int channels,
                             int bits_per_channel, int bits_per_pixel, uint8_t *data)
{
  dgc_image_t *self;

  if (bits_per_pixel < channels * bits_per_channel)
    bits_per_pixel = channels * bits_per_channel;
  
  self = (dgc_image_t*) calloc(1, sizeof(dgc_image_t));

  self->cols = cols;
  self->rows = rows;
  self->channels = channels;
  self->bits = bits_per_channel;
  self->pixel_size = bits_per_pixel / 8;
  self->row_size = self->pixel_size * self->cols;
  self->data_size = self->row_size * self->rows;

  if (data == NULL)
  {
    self->data = calloc(1, self->data_size);
    self->own_data = true;
  }
  else
  {
    self->data = data;
    self->own_data = false;
  }
  
  return self;
}


// Create a new image object from an image file.
dgc_image_t *dgc_image_alloc_file(const char *filename)
{
  dgc_image_t *self;
  int cols, rows, channels, bits;
  
  // Get size and depth
  if (dgc_image_query_any(filename, &cols, &rows, &channels, &bits) != DGC_OK)
    return NULL;

  // Allocate image
  self = dgc_image_alloc(cols, rows, channels, bits, 0, NULL);
  assert(self);

  // Load file
  if (dgc_image_read_any(self, filename) != 0)
  {
    dgc_image_free(self);
    return NULL;
  }
  
  return self;
}


// Create a new image object by cloning another.
dgc_image_t *dgc_image_clone(const dgc_image_t *image)
{
  dgc_image_t *self;

  self = (dgc_image_t*) calloc(1, sizeof(dgc_image_t));

  self->cols = image->cols;
  self->rows = image->rows;
  self->channels = image->channels;
  self->bits = image->bits;
  self->pixel_size = image->pixel_size;
  self->row_size = image->row_size;
  self->data_size = image->data_size;

  self->data = malloc(image->data_size);
  memcpy(self->data, image->data, self->data_size);
  self->own_data = true;
  
  return self;
}


// Free the image object.
void dgc_image_free(dgc_image_t *self)
{
  if (self->data && self->own_data)
    free(self->data);
  free(self);
  
  return;
}


// See if images have the same format (size, depth, etc).
bool dgc_image_compare(dgc_image_t *self, dgc_image_t *image)
{
  return (self->cols == image->cols && self->rows == image->rows &&
          self->channels == image->channels && self->bits == image->bits &&
          self->data_size == image->data_size);
}


// Get the pointer to a given pixel.
uint8_t *dgc_image_pixel(dgc_image_t *self, int col, int row)
{
  if (col < 0 || col >= self->cols)
    return NULL;
  if (row < 0 || row >= self->rows)
    return NULL;
  return self->data + row * self->row_size + col * self->pixel_size;
}


// Copy data from another image.
int dgc_image_copy(dgc_image_t *self, const dgc_image_t *src)
{
  if (self->cols != src->cols || self->rows != src->rows)
    return DGC_ERROR_SET(DGC_ERR_INVALID, "mis-matched image formats");
  if (self->channels != src->channels || self->bits != src->bits)
    return DGC_ERROR_SET(DGC_ERR_INVALID, "mis-matched image formats");
  if (self->pixel_size != src->pixel_size || self->row_size != src->row_size)
    return DGC_ERROR_SET(DGC_ERR_INVALID, "mis-matched image formats");
  if (self->data_size != src->data_size)
    return DGC_ERROR_SET(DGC_ERR_INVALID, "mis-matched image formats");
  
  memcpy(self->data, src->data, self->data_size);
    
  return DGC_OK;
}


// Copy one channel from another image.
int dgc_image_copy_channel(dgc_image_t *self, const dgc_image_t *src, int channel)
{
  int i, j;
  uint8_t *sp, *dp;
  
  if (self->channels != 1 || self->bits != 8)
    return DGC_ERROR_SET(DGC_ERR_INVALID, "invalide image format");
  if (self->cols != src->cols || self->rows != src->rows)
    return DGC_ERROR_SET(DGC_ERR_INVALID, "mis-matched image formats");
  if (channel < 0 || channel >= src->channels)
    return DGC_ERROR_SET(DGC_ERR_INVALID, "invalid channel request");

  sp = src->data;
  dp = self->data;
  for (j = 0; j < self->rows; j++)
  {
    for (i = 0; i < self->cols; i++)
    {
      dp[0] = sp[channel];
      dp += 1;
      sp += src->channels;
    }
  }
    
  return DGC_OK;
}


// Write an image caption
void dgc_image_write_caption(const dgc_image_t *self, FILE *file, const char *caption)
{
  int i;

  // Break line at newlines
  fprintf(file, "# ");
  for (i = 0; i < strlen(caption); i++)
  {
    fputc(caption[i], file);
    if (caption[i] == '\n' && i + 1 < strlen(caption))
      fprintf(file, "# ");
  }

  // Make sure we have exactly one trailing newlines
  if (caption[i - 1] != '\n')
    fprintf(file, "\n");
  
  return;
}


// Utility function to read PNM headers
int dgc_read_pnm_header(const char *filename,
                                  char *caption, size_t caption_size,
                                  int *cols, int *rows, int *channels, int *bits,
                                  size_t *header_size)
{
  FILE *file;
  char line[4096];
  int i, num_tokens, c;
  int max;
  
  file = fopen(filename, "r");
  if (!file)
    return DGC_ERROR_SET(DGC_ERR_SYSTEM, "error opening %s : %s", filename, strerror(errno));

  // Get the first line; must contain the magic number
  if (fgets(line, sizeof(line), file) == NULL)
  {
    fclose(file);
    return DGC_ERROR_SET(DGC_ERR_FAILED, "%s: unable to read image header", filename);
  }

  // RGB or mono
  if (strncmp(line, "P5", 2) == 0)
    *channels = 1;
  else if (strncmp(line, "P6", 2) == 0)
    *channels = 3;
  else
  {
    fclose(file);
    return DGC_ERROR_SET(DGC_ERR_FAILED, "%s: unknown image type [%.2s]", filename, line);
  }

  // Reset the caption
  if (caption && caption_size > 0)
    caption[0] = 0;
  
  // Get some number of comment lines and read out the caption
  while (true)
  {
    c = fgetc(file);
    if (c != '#')
    {
      ungetc(c, file);
      break;
    }
    
    if (fgets(line, sizeof(line), file) == NULL)
    {
      fclose(file);
      return DGC_ERROR_SET(DGC_ERR_FAILED, "unexpected end of file");
    }

    // Append line to caption string
    if (caption && strlen(line) >= 2)
      strncat(caption, line + 1, caption_size);
  }
  
  // Read two numbers (image dimensions)
  num_tokens = 0;
  for (i = 0; i < sizeof(line) - 1 && num_tokens < 2; i++)
  {
    line[i] = fgetc(file);
    line[i + 1] = 0;
    if (i > 0 && !isdigit(line[i]))
    { 
      if (isdigit(line[i - 1]))
        num_tokens++;
    }
  }
  assert(num_tokens == 2);

  // Read dimensions
  assert(sscanf(line, "%d %d", cols, rows) == 2);

  // Read more comments
  // Get some number of comment lines and read out the caption
  while (true)
  {
    c = fgetc(file);
    if (c != '#')
    {
      ungetc(c, file);
      break;
    }
    
    if (fgets(line, sizeof(line), file) == NULL)
    {
      fclose(file);
      return DGC_ERROR_SET(DGC_ERR_FAILED, "unexpected end of file");
    }

    // Append line to caption string
    if (caption && strlen(line) >= 2)
      strncat(caption, line + 1, caption_size);
  }
  // Read two numbers (image dimensions)
  num_tokens = 0;
  for (i = 0; i < sizeof(line) && num_tokens < 1; i++)
  {
    line[i] = fgetc(file);
    if (i > 0 && !isdigit(line[i]))
    { 
      if (isdigit(line[i - 1]))
        num_tokens++;
    }
  }
  assert(num_tokens == 1);  

  // Read max pixel value
  assert(sscanf(line, "%d", &max) == 1);

  if (max <= 255)
    *bits = 8;
  else
    *bits = 16;

  // Compute size of header
  if (header_size)
    *header_size = (size_t) ftell(file);
  
  fclose(file);

  return DGC_OK;
}


// Write an image to a PNM file 
int dgc_image_write_pnm(const dgc_image_t *self,
                                  const char *filename, const char *caption)
{
  FILE *file;
  int i, j, id;
  uint8_t *pix;
  uint16_t p;
  
  file = fopen(filename, "wb");
  if (!file)  
    return DGC_ERROR_SET(DGC_ERR_SYSTEM, "error opening %s : %s", filename, strerror(errno));

  id = fileno(file);

  if (self->channels == 1)
  {
    fprintf(file, "P5\n");
  }
  else if (self->channels == 3 || self->channels == 4)
  {
    fprintf(file, "P6\n");
  }
  else
  {
    fclose(file);
    return DGC_ERROR_SET(DGC_ERR_INVALID, "unsupported image type");
  }

  if (caption)
    dgc_image_write_caption(self, file, caption);

  if (self->bits <= 16)
    fprintf(file, "%d %d\n%d\n", self->cols, self->rows, (1 << self->bits) - 1);
  else
    fprintf(file, "%d %d\n%d\n", self->cols, self->rows, 0xFFFF);

  // TODO: do other optimizations
  // Optimize for 1-channel PGM
  if (self->channels == 1 && self->bits == 8)
  {
    fwrite(self->data, 1, self->data_size, file);
  }
  else if (self->channels == 3 && self->bits == 8)
  {
    fwrite(self->data, 1, self->data_size, file);
  }
  else
  {
    for (j = 0; j < self->rows; j++)
    {
      for (i = 0; i < self->cols; i++)
      {
        pix = self->data + j * self->row_size + i * self->pixel_size;

        if (self->channels == 1 && self->bits == 16)
        {
          p = htons(((uint16_t*) pix)[0]);
          fwrite(&p, sizeof(p), 1, file);
        }
        else if (self->channels == 1 && self->bits == 32)
        {
          // Assume this is a float and convert to int
          p = htons((uint16_t) (int16_t) (fabs(((float*) pix)[0]) * 0xFFFF));
          fwrite(&p, sizeof(p), 1, file);
        }
        else if (self->channels == 4 && self->bits == 8)
        {
          fputc(pix[0], file);
          fputc(pix[1], file);
          fputc(pix[2], file);       
          // Discard the 4th channel
        }
        else if (self->channels == 3 && self->bits == 16)
        {
          p = htons(((uint16_t*) pix)[0]);
          fwrite(&p, sizeof(p), 1, file);
          p = htons(((uint16_t*) pix)[1]);
          fwrite(&p, sizeof(p), 1, file);
          p = htons(((uint16_t*) pix)[2]);
          fwrite(&p, sizeof(p), 1, file);
        }
        else
        {
          fclose(file);
          return DGC_ERROR_SET(DGC_ERR_INVALID, "unsupported image type");
        }
      }
    }
  }

  // Flush to disk
//  fsync(id);  
  fclose(file);

  return DGC_OK;
}


// Query a PNM file to determine image dimensions
int dgc_image_query_pnm(const char *filename,
                                  int *cols, int *rows, int *channels, int *bits)
{
  size_t size;    
  return dgc_read_pnm_header(filename, NULL, 0, cols, rows, channels, bits, &size);
}


// Read an image from a PNM file 
int dgc_image_read_pnm(dgc_image_t *self, const char *filename,
                                 char *caption, size_t caption_size)
{
  FILE *file;
  int i, j;
  int rows, cols, channels, bits;
  size_t header_size;
  uint8_t *pix;
  uint16_t p;
  
  // Read the header
  if (dgc_read_pnm_header(filename, caption, caption_size,
                           &cols, &rows, &channels, &bits, &header_size) != DGC_OK)
    return DGC_ERR_INVALID;

  // Make sure the image format is correct
  if (!(rows == self->rows && cols == self->cols &&
        channels == self->channels && bits == self->bits))
    return DGC_ERROR_SET(DGC_ERR_INVALID,
                          "file format (%dx%dx%d:%d) does not match image (%dx%dx%d:%d)",
                          cols, rows, channels, bits,
                          self->cols, self->rows, self->channels, self->bits);

  // Open the file
  file = fopen(filename, "r");
  if (!file)
    return DGC_ERROR_SET(DGC_ERR_SYSTEM, "error opening %s : %s", filename, strerror(errno));

  // Skip the header
  fseek(file, header_size, SEEK_SET);

  // TODO: do other optimizations
  // Optimize for 1-channel PGM
  if (self->channels == 1 && self->bits == 8)
  {
    fread(self->data, 1, self->data_size, file);
  }
  else if (self->channels == 3 && self->bits == 8)
  {
    fread(self->data, 1, self->data_size, file);
  }
  else
  {
    for (j = 0; j < self->rows; j++)
    {
      for (i = 0; i < self->cols; i++)
      {
        pix = self->data + j * self->row_size + i * self->pixel_size;

        if (self->channels == 1 && self->bits == 16)
        {
          fread(&p, sizeof(p), 1, file);
          ((uint16_t*) pix)[0] = ntohs(p);
        }
        else if (self->channels == 3 && self->bits == 8)
        {
          pix[0] = fgetc(file);
          pix[1] = fgetc(file);
          pix[2] = fgetc(file); 
        }
        else if (self->channels == 3 && self->bits == 16)
        {
          fread(&p, sizeof(p), 1, file);
          ((uint16_t*) pix)[0] = ntohs(p);
          fread(&p, sizeof(p), 1, file);
          ((uint16_t*) pix)[1] = ntohs(p);
          fread(&p, sizeof(p), 1, file);
          ((uint16_t*) pix)[2] = ntohs(p);
        }
      }
    }
  }
  
  fclose(file);

  return DGC_OK;
}


// Query the dimensions of a RGB file
int dgc_image_query_rgb(const char *filename,
                                  int *cols, int *rows, int *channels, int *bits)
{
  FILE *file;
  int32_t ncols, nsize;
  double size, test;
  
  // Open the file
  file = fopen(filename, "r");
  if (!file)
    return DGC_ERROR_SET(DGC_ERR_SYSTEM, "error opening %s : %s", filename, strerror(errno));

  // Test to see the file length matches
  if (fseek(file, 0, SEEK_END) == -1)
    return DGC_ERROR_SET(DGC_ERR_SYSTEM, "error fseek %s : %s", filename, strerror(errno));
  if ((nsize = ftell(file)) == -1)
    return DGC_ERROR_SET(DGC_ERR_SYSTEM, "error ftell %s : %s", filename, strerror(errno));
  fclose(file);

  size = nsize * 4 / 9.0;
  test = sqrt(size);

  ncols = (uint32_t)(test);
  if (test != ncols)
    return DGC_ERROR_SET(DGC_ERR_INVALID,
                          "rgb image %s not 3/4 aspect ratio", filename);

  *cols = ncols;
  *rows = ncols*3/4;
  *channels = 3;
  *bits = 8;

  return DGC_OK;
}


// Read an image from a RGB file 
int dgc_image_read_rgb(dgc_image_t *self, const char *filename)
{
  FILE *file;

  // Check the image format
  if (self->channels != 3 && self->bits != 8)
  {
    return DGC_ERROR_SET(DGC_ERR_INVALID,
                          "mis-matched image format (should be 3 channel, 8 bits/channel)");
  }
  
  // Open the file
  file = fopen(filename, "r");
  if (!file)
    return DGC_ERROR_SET(DGC_ERR_SYSTEM, "error opening %s : %s", filename, strerror(errno));

  if (fread(self->data, self->data_size, 1, file) < 1)
  {
    fclose(file);
    return DGC_ERROR_SET(DGC_ERR_SYSTEM, "incomplete read from %s : %s",
                          filename, strerror(errno));
  }
  
  fclose(file);

  return DGC_OK;
}


// Write an image to a RGB file 
int dgc_image_write_rgb(dgc_image_t *self, const char *filename)
{
  FILE *file;

  // Check the image format
  if (self->channels != 3 && self->bits != 8)
  {
    return DGC_ERROR_SET(DGC_ERR_INVALID,
                          "mis-matched image format (should be 3 channel, 8 bits/channel)");
  }
  
  // Open the file
  file = fopen(filename, "w");
  if (!file)
    return DGC_ERROR_SET(DGC_ERR_SYSTEM, "error opening %s : %s", filename, strerror(errno));

  // Read dimensions (first 8 bytes)
  fwrite(self->data, self->data_size, 1, file);
  
  fclose(file);

  return DGC_OK;
}

// Query the dimensions of a PIC file
int dgc_image_query_pic(const char *filename,
                                  int *cols, int *rows, int *channels, int *bits)
{
  FILE *file;
  uint32_t ncols, nrows, size;
  
  // Open the file
  file = fopen(filename, "r");
  if (!file)
    return DGC_ERROR_SET(DGC_ERR_SYSTEM, "error opening %s : %s", filename, strerror(errno));

  // Read dimensions (first 8 bytes)
  fread(&nrows, sizeof(nrows), 1, file);
  fread(&ncols, sizeof(ncols), 1, file);

  *cols = ntohl(ncols);
  *rows = ntohl(nrows);
  *channels = 1;
  *bits = 8;

  // Test to see the file length matches
  size = *cols * *rows * *channels * *bits / 8 + 8;
  fseek(file, 0, SEEK_END);
  if (ftell(file) != size)
  {
    fclose(file);
    return DGC_ERROR_SET(DGC_ERR_INVALID, "file has incorrect size: %s", filename);
  }

  fclose(file);

  return DGC_OK;
}


// Read an image from a PIC file 
int dgc_image_read_pic(dgc_image_t *self, const char *filename)
{
  FILE *file;
  uint32_t cols, rows;

  // Check the image format
  if (self->channels != 1 && self->bits != 8)
  {
    return DGC_ERROR_SET(DGC_ERR_INVALID,
                          "mis-matched image format (should be 1 channel, 8 bits/channel)");
  }
  
  // Open the file
  file = fopen(filename, "r");
  if (!file)
    return DGC_ERROR_SET(DGC_ERR_SYSTEM, "error opening %s : %s", filename, strerror(errno));

  // Read dimensions (first 8 bytes)
  fread(&rows, sizeof(rows), 1, file);
  fread(&cols, sizeof(cols), 1, file);

  cols = ntohl(cols);
  rows = ntohl(rows);

  if (cols != self->cols || rows != self->rows)
  {
    fclose(file);
    return DGC_ERROR_SET(DGC_ERR_INVALID,
                          "mis-matched dimensions: file is %dx%d, image is %dx%d",
                          cols, rows, self->cols, self->rows);
  }
  
  if (fread(self->data, self->data_size, 1, file) < 1)
  {
    fclose(file);
    return DGC_ERROR_SET(DGC_ERR_SYSTEM, "incomplete read from %s : %s",
                          filename, strerror(errno));
  }
  
  fclose(file);

  return DGC_OK;
}


// Write an image to a PIC file 
int dgc_image_write_pic(dgc_image_t *self, const char *filename)
{
  FILE *file;
  uint32_t cols, rows;

  // Check the image format
  if (self->channels != 1 && self->bits != 8)
  {
    return DGC_ERROR_SET(DGC_ERR_INVALID,
                          "mis-matched image format (should be 1 channel, 8 bits/channel)");
  }
  
  // Open the file
  file = fopen(filename, "w");
  if (!file)
    return DGC_ERROR_SET(DGC_ERR_SYSTEM, "error opening %s : %s", filename, strerror(errno));

  // Read dimensions (first 8 bytes)
  cols = htonl(self->cols);
  rows = htonl(self->rows);
  fwrite(&rows, sizeof(rows), 1, file);
  fwrite(&cols, sizeof(cols), 1, file);
  fwrite(self->data, self->data_size, 1, file);
  
  fclose(file);

  return DGC_OK;
}


// Query the dimensions of a RAN file
int dgc_image_query_ran(const char *filename,
                                  int *cols, int *rows, int *channels, int *bits)
{
  FILE *file;
  uint32_t ncols, nrows, size;
  
  // Open the file
  file = fopen(filename, "r");
  if (!file)
    return DGC_ERROR_SET(DGC_ERR_SYSTEM, "error opening %s : %s", filename, strerror(errno));

  // Read dimensions (first 8 bytes)
  fread(&nrows, sizeof(nrows), 1, file);
  fread(&ncols, sizeof(ncols), 1, file);

  *cols = ntohl(ncols);
  *rows = ntohl(nrows);
  *channels = 1;
  *bits = 8;

  // Test to see the file length matches
  size = *cols * *rows * *channels * *bits / 8 + 8;
  fseek(file, 0, SEEK_END);
  if (ftell(file) != size)
  {
    fclose(file);
    return DGC_ERROR_SET(DGC_ERR_INVALID, "file has incorrect size: %s", filename);
  }

  fclose(file);

  return DGC_OK;
}


// Read an image from a RAN file 
int dgc_image_read_ran(dgc_image_t *self, const char *filename)
{
  FILE *file;
  int i, j, k;
  uint32_t cols, rows;
  float *pix;
  ran_pixel_t s;

  // Check the image format
  if (self->channels != 3 && self->bits != 8 * sizeof(float))
  {
    return DGC_ERROR_SET(DGC_ERR_INVALID,
                          "mis-matched image format (should be 3 channels, 32 bits/channel)");
  }
  
  // Open the file
  file = fopen(filename, "r");
  if (!file)
    return DGC_ERROR_SET(DGC_ERR_SYSTEM, "error opening %s : %s", filename, strerror(errno));

  // Read dimensions (first 8 bytes)
  fread(&rows, sizeof(rows), 1, file);
  fread(&cols, sizeof(cols), 1, file);

  cols = ntohl(cols);
  rows = ntohl(rows);

  if (cols != self->cols || rows != self->rows)
  {
    fclose(file);
    return DGC_ERROR_SET(DGC_ERR_INVALID,
                          "mis-matched dimensions: file is %dx%d, image is %dx%d",
                          cols, rows, self->cols, self->rows);
  }

  for (j = 0; j < self->rows; j++)
  {
    for (i = 0; i < self->cols; i++)
    {
      pix = (float*) (self->data + j * self->row_size + i * self->pixel_size);
      for (k = 0; k < self->channels; k++)
      {
        if (fread(&s, sizeof(s), 1, file)  < 1)
        {
          fclose(file);
          return DGC_ERROR_SET(DGC_ERR_SYSTEM, "incomplete read from %s : %s",
                                filename, strerror(errno));
        }

        // Convert to host byte order
        s.i = ntohl(s.i);
        pix[k] = s.f;
      }
    }
  }
  
  fclose(file);

  return DGC_OK;
}


// Write an image to a RAN file 
int dgc_image_write_ran(dgc_image_t *self, const char *filename)
{
  FILE *file;
  int i, j, k;
  uint32_t cols, rows;
  float *pix;
  ran_pixel_t s;

  // Check the image format
  if (self->channels != 3 && self->bits != 8 * sizeof(float))
  {
    return DGC_ERROR_SET(DGC_ERR_INVALID,
                          "mis-matched image format (should be 3 channels, 32 bits/channel)");
  }
  
  // Open the file
  file = fopen(filename, "w");
  if (!file)
    return DGC_ERROR_SET(DGC_ERR_SYSTEM, "error opening %s : %s", filename, strerror(errno));

  // Read dimensions (first 8 bytes)
  cols = htonl(self->cols);
  rows = htonl(self->rows);
  fwrite(&rows, sizeof(rows), 1, file);
  fwrite(&cols, sizeof(cols), 1, file);

  for (j = 0; j < self->rows; j++)
  {
    for (i = 0; i < self->cols; i++)
    {
      pix = (float*) (self->data + j * self->row_size + i * self->pixel_size);
      for (k = 0; k < self->channels; k++)
      {
        // Convert to network byte-order
        s.f = pix[k];
        s.i = htonl(s.i);
        fwrite(&s, sizeof(s), 1, file);
      }
    }
  }

  fsync(fileno(file));  
  fclose(file);

  return DGC_OK;
}


// Query all supported file types to determine image dimensions
int dgc_image_query_any(const char *filename,
                                  int *cols, int *rows, int *channels, int *bits)
{
  int err;

  err = dgc_image_query_pnm(filename, cols, rows, channels, bits);
  if (err == DGC_OK)
    return DGC_OK;
  if (err == DGC_ERR_SYSTEM)
    return err;

  err = dgc_image_query_rgb(filename, cols, rows, channels, bits);
  if (err == DGC_OK)
    return DGC_OK;
  if (err == DGC_ERR_SYSTEM)
    return err;

  err = dgc_image_query_pic(filename, cols, rows, channels, bits);
  if (err == DGC_OK)
    return DGC_OK;
  if (err == DGC_ERR_SYSTEM)
    return err;
  
  err = dgc_image_query_ran(filename, cols, rows, channels, bits);
  if (err == DGC_OK)
    return DGC_OK;
  if (err == DGC_ERR_SYSTEM)
    return err;

  return DGC_ERROR_SET(DGC_ERR_INVALID, "file type not recognized");
}

  
// Read an image from any supported file type.
int dgc_image_read_any(dgc_image_t *self, const char *filename)
{
  int err;
  int cols, rows, channels, bits;

  err = dgc_image_query_pnm(filename, &cols, &rows, &channels, &bits);
  if (err == DGC_OK)
    return dgc_image_read_pnm(self, filename, NULL, 0);
  if (err == DGC_ERR_SYSTEM)
    return err;

  err = dgc_image_query_rgb(filename, &cols, &rows, &channels, &bits);
  if (err == DGC_OK)
    return dgc_image_read_rgb(self, filename);
  if (err == DGC_ERR_SYSTEM)
    return err;

  err = dgc_image_query_pic(filename, &cols, &rows, &channels, &bits);
  if (err == DGC_OK)
    return dgc_image_read_pic(self, filename);
  if (err == DGC_ERR_SYSTEM)
    return err;

  err = dgc_image_query_ran(filename, &cols, &rows, &channels, &bits);
  if (err == DGC_OK)
    return dgc_image_read_ran(self, filename);
  if (err == DGC_ERR_SYSTEM)
    return err;

  return DGC_ERROR_SET(DGC_ERR_INVALID, "file type not recognized");
}
