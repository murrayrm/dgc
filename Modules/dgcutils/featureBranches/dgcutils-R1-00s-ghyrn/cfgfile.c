/*!
 * \file cfgfile.c
 * \brief Find path to configuration files
 *
 * \author Richard M. Murray
 * \date 10 February 2007
 *
 * This file contains a small utility function for finding a
 * configuration file by looking in standard places.  It is written as
 * a wrapper so that you can invoke it as
 *
 *   ifstream cfgfile(dgcConfigFile(filename))
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>


/* Look for configuration file and return path */
char *dgcFindConfigFile(char *file, char *module)
{
  int found = 0;			/* have we found the file yet? */
  struct stat buf;			/* status buffer for fstat */

  /* Allocate space for the resulting path name */
  char *cfgfile = malloc(PATH_MAX+1);

  /* First see if the file exists in the current directory */
  snprintf(cfgfile, PATH_MAX, "./%s", file);
  if (stat(cfgfile, &buf) == 0) found = 1;  

  /* If not, see if DGC_CONFIG_PATH is set and look there */
  char *dgc_config_path = getenv("DGC_CONFIG_PATH");
  if (!found && dgc_config_path != NULL) {
    snprintf(cfgfile, PATH_MAX, "%s/%s/%s", dgc_config_path, 
             module == NULL ? "" : module, file);
    if (stat(cfgfile, &buf) == 0) found = 1;  
  }

  /* If DGC_CONFIG_PATH is not set, try guessing from the current
   * directory */
  if (!found && getenv("PWD")) {
    const char *pwd;
    pwd = getenv("PWD");

    if (strstr(pwd, "bin/i486-gentoo-linux"))
      strncpy(cfgfile, pwd, strlen(pwd) - 21);
    else if (strstr(pwd, "bin/i386-darwin"))
      strncpy(cfgfile, pwd, strlen(pwd) - 15);
    else
      strcpy(cfgfile, ".");        
    strcat(cfgfile, "/etc/");
    if (module != NULL)
    {
      strcat(cfgfile, module);
      strcat(cfgfile, "/");
    }
    strcat(cfgfile, file);
    printf("searching %s", cfgfile);
    if (stat(cfgfile, &buf) == 0) found = 1;  
  }
  
  /* Finally, try looking in /dgc/etc for the file */
  if (!found) {
    snprintf(cfgfile, PATH_MAX, "/dgc/etc/%s/%s",
             module == NULL ? "" : module, file);
    if (stat(cfgfile, &buf) == 0) found = 1;  
  }

  /* If we didn't find anything, just return the name we were passed */
  if (!found) {
    strncpy(cfgfile, file, PATH_MAX);
    fprintf(stderr, "Warning: couldn't find config file %s\n", cfgfile);
  }

  /* Return whatever we found */
  return (cfgfile);
}


/* Look for configuration directy */
char *dgcFindConfigDir(char *module)
{
  int found = 0;			/* have we found the file yet? */
  struct stat buf;			/* status buffer for fstat */

  /* Allocate space for the resulting path name */
  char *cfgfile = malloc(PATH_MAX+1);

  /* If not, see if DGC_CONFIG_PATH is set and look there */
  char *dgc_config_path = getenv("DGC_CONFIG_PATH");
  if (!found && dgc_config_path != NULL) {
    snprintf(cfgfile, PATH_MAX, "%s/%s", dgc_config_path, module);
    if (stat(cfgfile, &buf) == 0) found = 1;  
  }

  /* If DGC_CONFIG_PATH is not set, try guessing from the current
   * directory */
  if (!found && getenv("PWD")) {
    const char *pwd;
    pwd = getenv("PWD");

    if (strstr(pwd, "bin/i486-gentoo-linux"))
      strncpy(cfgfile, pwd, strlen(pwd) - 21);
    else if (strstr(pwd, "bin/i386-darwin"))
      strncpy(cfgfile, pwd, strlen(pwd) - 15);
    else
      strcpy(cfgfile, ".");        
    strcat(cfgfile, "etc/");
    strcat(cfgfile, module);
    printf("searching %s", cfgfile); // TESTING
    if (stat(cfgfile, &buf) == 0) found = 1;  
  }
  
  /* Finally, try looking in /dgc/etc for the file */
  if (!found) {
    snprintf(cfgfile, PATH_MAX, "/dgc/etc/%s", module);
    if (stat(cfgfile, &buf) == 0) found = 1;  
  }

  /* If we didn't find anything, just return the name we were passed */
  if (!found) {
    strncpy(cfgfile, module, PATH_MAX);
    fprintf(stderr, "Warning: couldn't find config dir %s\n", cfgfile);
  }

  /* Return whatever we found */
  return (cfgfile);
}
