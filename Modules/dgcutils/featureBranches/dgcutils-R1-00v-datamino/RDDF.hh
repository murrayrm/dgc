/*!
 * \file RDDF.hh 
 * \brief class to read the rddf file and return the waypoints
 *
 * \author Thyago Consort 
 * \date 2003-04
 *
 * The RDDF class allows manipulation of RDDF files.
 *
 * This file has been simplifed to just include the type of RDDF
 * processing we need for backward compatibility in the Urban
 * Challenge.  See the full RDDF library if you need the old code.
 *
 */

#ifndef __RDDF_HH__
#define __RDDF_HH__

#include <vector>
#include <iostream>
using namespace std;

#define RDDF_DELIMITER ','
#define RDDF_METERS_PER_FOOT	0.3048
#define RDDF_MPS_PER_MPH	0.44704
#define RDDF_UTM_ZONE		11
#define RDDF_UTM_LETTER		'S'

/*!
 * \struct RDDFData
 *
 * Waypoints are defined by the RDDF, and translated to a vector of
 * RDDFData structs when RDDF::loadFile() is called.  Even though the
 * RDDF is defined in terms of latitude, longitude, mph, and feed, SI
 * units are used from this point on in all of our code.
 */
typedef struct RDDFData
{
  /*
   * The corridor segment defined by waypoint i goes from waypoint i
   * to waypoint i+1.  DARPA RDDFs are indexed from waypoint 1, which
   * corresponds to index 0 in our code.
   */
  int number;			///< waypoint number (starting at 1)
  double latitude;		///< waypoint lattitude (degrees)
  double longitude;		///< waypoint longitude (degrees)

  /*
   * As soon as we read a waypoint in to memory, we convert to UTM and
   * coordinates, so that we can add and subtract.  The elements below
   * are the converted values.
   */
  double Northing, Easting;

  double maxSpeed;		///< speed limit (in m/s) 
  double radius;		///< corridor radius (in meters)
};

/// RDDFVector is a STL vector of waypoint data defined by RDDFData.
typedef std::vector<RDDFData> RDDFVector;

/*!
 * \class RDDF
 * \brief Simple RDDF class
 *
 * The RDDF class can be used to access RDDFs in a simple way.
 */
class RDDF 
{
protected:
  int numTargetPoints;
  RDDFVector targetPoints;

public:
  RDDF();			///< Generic constructor
  RDDF(char*pFileName);		///< Create an RDDF from a file
  RDDF(const RDDF &other) 	///< Create an RDDF from another RDDF
  {
    numTargetPoints = other.getNumTargetPoints();
    targetPoints = other.getTargetPoints();
  }

  ~RDDF();			///< Generic destructor

  /// Assignment works by copying data
  RDDF &operator=(const RDDF &other);

  // Accessors

  /** Accessor function to get the number of waypoints. */
  int getNumTargetPoints() const { return numTargetPoints; }

  /** Accessor function to get a vector of all the waypoints.  RDDFVector is a 
   * standard template vector of RDDFData structs, which contain all essential 
   * data for a waypoint (number, Northing, Easting, maxSpeed, offset). */
  RDDFVector getTargetPoints() const { return targetPoints; }

  /** Get the Northing value of a given waypoint. */
  double getWaypointNorthing(int waypointNum) 
  { 
    return targetPoints[waypointNum].Northing; 
  };

  /** Get the Easting value of a given waypoint. */ 
  double getWaypointEasting(int waypointNum) 
  { 
    return targetPoints[waypointNum].Easting;
  };

  double getWaypointSpeed(int waypointNum)
  {
    return targetPoints[waypointNum].maxSpeed;
  }

  /** Load data from file (RDDF format file). */
  int loadFile(char* fileName);

  /** Add a new point to the end of the RDDF */
  void addDataPoint(RDDFData &data_point);

  /** prints rddf to screen or file */
  void print(ostream & out = cout);

  /** Get the angle (in radians) from the specified waypoint to the next, 
   * defined as positive clockwise from north.  Waypoints are indexed from 0.  
   * */
  double getTrackLineYaw( int i );
  
  /** Get the distance (in meters) from the specified waypoint to the next. */
  double getTrackLineDist( int i );

  /** Save the file as the rddf format (returns 0 if OK) */
  int createRDDFFile(char* fileName);

  void clear() { targetPoints.clear(); numTargetPoints = 0; }
};

#endif //__RDDF_HH__
