
/*
 Desc: JPL images
 Author: Andrew Howard
 Date: 3 Feb 2005
 SVN: $Id: dgc_image.h,v 1.14 2007/03/21 00:36:38 abhoward Exp $
*/

/** @file

@brief Basic image handling.

The dgc_image_t structure is used for basic image handling; it
describes the image size and layout, and carries a pointer to the
pixel data.  To address an individual pixel, use the following
formula:

@code pixel_pointer = image->data + row * image->row_size + col * image->pixel_size @endcode

Equivalently, use the provided inline function:

@code pixel_pointer = dgc_image_pixel(image, col, row) @endcode

A number of functions are provided for serializing images to/from files.

*/

#ifndef DGC_IMAGE_H
#define DGC_IMAGE_H

#if defined __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stdint.h>

  
/// @brief Error codes.
typedef enum
{
  /// Operation completed successfully.
  DGC_OK = 0,

  /// Error: invalid argument.
  DGC_ERR_INVALID = -1,

  /// Error: missing key/value.
  DGC_ERR_MISSING = -2,

  /// Error: out-of-bounds array access.
  DGC_ERR_BOUNDS = -3,

  /// Error: system error (e.g. from libc).
  DGC_ERR_SYSTEM = -4,
  
  /// Error: operation failed for unknown reasons
  DGC_ERR_FAILED = -5,

} dgc_error_t;

  
/// @brief Basic image structure.
///
/// @sa dgc_image.h
typedef struct
{
  /// Image dimensions (pixels).
  int cols, rows;

  /// Number of color channels (e.g., monochrome = 1, RGB = 3).
  int channels;

  /// Number of bits per channel (must be a multiple of 8).
  int bits;

  /// @brief Size of each pixel, in bytes (e.g., 8-bit color = 1,
  /// 16-bit disparity = 2, 24-bit RGB = 3, 32-bit RGB = 4).
  int pixel_size;

  /// Size of each row, in bytes.
  int row_size;

  /// Size of the image buffer, in bytes.
  int data_size;

  /// Pointer to image buffer.
  uint8_t *data;

  /// @brief This flag is set to true if the image "owns" the data
  /// buffer, and should therefore de-allocate it when the image is
  /// freed.
  bool own_data;
  
} dgc_image_t;


/// @brief Create a new image object.
///
/// Images can be created in one of two ways:
///   - Call ::dgc_image_alloc with @c data set to NULL: a new buffer
///   containing the pixel data will be allocated on the heap; this
///   buffer will be de-allocated by ::dgc_image_free.
///   - Call ::dgc_image_alloc with @c data set to a previously allocated
///   block of memory.  It is the caller's responsibility to ensure
///   that this block is large enough to contain all of the pixel
///   data, and to deallocate the block when it is no longer in use.
///
/// @param[in] cols,rows Image dimensions (pixels).
/// @param[in] channels Number of channels (1 for monochrome, 3 for RGB).
/// @param[in] bits_per_channel Number of bits per channel (must be a multiple of 8).
/// @param[in] bits_per_pixel Number of bits per pixel (must be a multiple of 8).
///            This argument is used to add extra packing to pixels; e.g., 32-bit RGB.
///            If zero, bits_per_pixel will be calculated from channels and bits_per_channel.
/// @param[in] data Pointer to image data; if this is NULL, a new
///            image buffer will be allocated on the heap.
/// @returns On success, returns a pointer to the new image; on
///          failure returns NULL and sets the error code.
dgc_image_t *dgc_image_alloc(int cols, int rows, int channels,
                               int bits_per_channel, int bits_per_pixel, uint8_t *data);

/// @brief Create a new image object from an image file.
///
/// @param[in] filename Image filename.
/// @returns On success, returns a pointer to the new image; on
///          failure returns NULL and sets the error code.
dgc_image_t *dgc_image_alloc_file(const char *filename);

/// @brief Create a new image object by cloning another.
///
/// This function creates a deep copy of the original image; i.e.,
/// it copies all image parameters and pixel data into a entirely new image.
///
/// @param[in] image Image to clone.
/// @returns On success, returns a pointer to the new image; on
///          failure returns NULL and sets the error code.
dgc_image_t *dgc_image_clone(const dgc_image_t *image);

/// @brief Free the image object.
///
/// Frees the image along with any internal image buffers.
void dgc_image_free(dgc_image_t *self);

/// @brief See if images have the same format (size, depth, etc).
///
/// @param[in] self Source image.
/// @param[in] image Test image.  
/// @returns Returns true if the images have the same size, channels and bits.
bool dgc_image_compare(dgc_image_t *self, dgc_image_t *image);
  
/// @brief Get the pointer to a given pixel.
///
/// Note that this function does not do any range-checking on its
/// arguments; providing an invalid column and/or row value will
/// return an invalid address.
///  
/// @param[in] self Pointer to image.
/// @param[in] col,row Image coordinates.
/// @returns Returns a pointer to the selected pixel.  
uint8_t *dgc_image_pixel(dgc_image_t *self, int col, int row);

/// @brief Copy data from another image.
///
/// This function copies pixel data from the source image;
/// the source and destination image formats must be identical.
///
/// @param[in] self Pointer to image.
/// @param[in] src Image to copy.
int dgc_image_copy(dgc_image_t *self, const dgc_image_t *src);

/// @brief Copy one channel from another image.
///
/// This function copies pixel data from a multi-channel source image
/// to a single channel destination image.  The image sizes must be
/// identical.
///
/// @param[in] self Pointer to image.
/// @param[in] src Image to copy.
/// @param[in] channel Channel to copy (e.g., 1 to copy G from an RGB image).
int dgc_image_copy_channel(dgc_image_t *self, const dgc_image_t *src, int channel);

/// @brief Query a PNM file to determine image dimensions
///
/// Note that this function does not operate on a image object;
/// we include it here for name-spacing purposes only.
///  
/// @param[in] filename Image filename.
/// @param[out] cols, rows Image dimensions.
/// @param[out] channels Number of channels (e.g., 3 for RGB).
/// @param[out] bits Bits per channel.  
int dgc_image_query_pnm(const char *filename,
                                  int *cols, int *rows, int *channels, int *bits);
  
/// @brief Write an image to a PNM file.
///
/// Write an image to a PNM file.  The format of the file is dictated
/// by the image format: RGB images are written as PPM files;
/// monochrome images are written as PGM files.
///
/// @param[in] self Pointer to image.
/// @param[in] filename Image filename.
/// @param[in] caption Image caption; this will be written to the
/// image header as a comment field, and may contain multiple lines.
int dgc_image_write_pnm(const dgc_image_t *self,
                                  const char *filename, const char *caption);
  
/// @brief Read an image from a PNM file.
///
/// Read an image from a PNM file.
///  
/// @param[in] self Pointer to image.
/// @param[in] filename Image filename.
/// @param[out] caption Image caption; this will be read from the
/// comment fields in the image header.
/// @param[in] caption_size Size of the image caption buffer.
int dgc_image_read_pnm(dgc_image_t *self, const char *filename,
                                 char *caption, size_t caption_size);
  
/// @brief Query a RGB file to determine image dimensions
///
/// Note only 3/4 apsect ratio images are support
/// This function does not operate on a image object;
/// we include it here for name-spacing purposes only.
///  
/// @param[in] filename Image filename.
/// @param[out] cols, rows Image dimensions.
/// @param[out] channels Number of channels; always 3 for RGB.
/// @param[out] bits Bits per channel; always 8 for RGB.  
int dgc_image_query_rgb(const char *filename,
                                  int *cols, int *rows, 
                                  int *channels, int *bits);
  
/// @brief Read an image from a JPL RGB file.
///
/// Read an image from a JPL RGB (8-bit RGB/BGR) file.  Note that the
/// image must have 3 channels, with 8 bits/channel, and that the
/// image dimensions must be of 3/4 aspect ratio
///  
/// @sa @ref format_rgb
///  
/// @param[in] self Pointer to image.
/// @param[in] filename Image filename.
/// @returns Returns DGC_ERR_INVALID if the image has the wrong format.
int dgc_image_read_rgb(dgc_image_t *self, const char *filename);

/// @brief Write an image to a JPL RGB file.
///
/// Write an image to a JPL RGB (8-bit RGB/BGR) file. Note that the image
/// must have 3 channels, with 8 bits/channel.
///  
/// @sa @ref format_rgb
///  
/// @param[in] self Pointer to image.
/// @param[in] filename Image filename.
/// @returns Returns DGC_ERR_INVALID if the image has the wrong format.
int dgc_image_write_rgb(dgc_image_t *self, const char *filename);


/// @brief Query a PIC file to determine image dimensions
///
/// Note that this function does not operate on a image object;
/// we include it here for name-spacing purposes only.
///  
/// @param[in] filename Image filename.
/// @param[out] cols, rows Image dimensions.
/// @param[out] channels Number of channels; always 1 for PIC.
/// @param[out] bits Bits per channel; always 8 for PIC.  
int dgc_image_query_pic(const char *filename,
                                  int *cols, int *rows, int *channels, int *bits);
  
/// @brief Read an image from a JPL PIC file.
///
/// Read an image from a JPL PIC (8-bit mono) file.  Note that the
/// image must have 1 channels, with 8 bits/channel, and that the
/// image dimensions must match the file dimensions.
///  
/// @sa @ref format_pic
///  
/// @param[in] self Pointer to image.
/// @param[in] filename Image filename.
/// @returns Returns DGC_ERR_INVALID if the image has the wrong format.
int dgc_image_read_pic(dgc_image_t *self, const char *filename);

/// @brief Write an image to a JPL PIC file.
///
/// Write an image to a JPL PIC (8-bit mono) file. Note that the image
/// must have 1 channels, with 8 bits/channel.
///  
/// @sa @ref format_pic
///  
/// @param[in] self Pointer to image.
/// @param[in] filename Image filename.
/// @returns Returns DGC_ERR_INVALID if the image has the wrong format.
int dgc_image_write_pic(dgc_image_t *self, const char *filename);
  
/// @brief Query a JPL RAN (range points) file to determine image
/// dimensions.
///
/// Note that this function does not operate on a image object;
/// we include it here for name-spacing purposes only.
///  
/// @param[in] filename Image filename.
/// @param[out] cols, rows Image dimensions.
/// @param[out] channels Number of channels; always 1 for PIC.
/// @param[out] bits Bits per channel; always 8 for PIC.  
int dgc_image_query_ran(const char *filename,
                                  int *cols, int *rows, int *channels, int *bits);

/// @brief Read an image from a JPL RAN (range points) file.
///
/// Read an image from a JPL RAN (range points) file.  Each pixel
/// gives the xyz location of a point.  Note that the image must have
/// 3 channels, with 32 bits/channel, and that the image dimensions
/// must match the file dimensions.
///  
/// @sa @ref format_ran
///  
/// @param[in] self Pointer to image.
/// @param[in] filename Image filename.
/// @returns Returns DGC_ERR_INVALID if the image has the wrong format.
int dgc_image_read_ran(dgc_image_t *self, const char *filename);

/// @brief Write an image to a JPL RAN (range points) file.
///
/// Write an image to a JPL RAN (range points) file.  Each pixel
/// gives the xyz location of a point.  Note that the image must have
/// 3 channels, with 32 bits/channel.
///  
/// @sa @ref format_ran
///  
/// @param[in] self Pointer to image.
/// @param[in] filename Image filename.
/// @returns Returns DGC_ERR_INVALID if the image has the wrong format.
int dgc_image_write_ran(dgc_image_t *self, const char *filename);
  
/// @brief Query all supported file types to determine image dimensions
///
/// Note that this function does not operate on a image object;
/// we include it here for name-spacing purposes only.
///  
/// @param[in] filename Image filename.
/// @param[out] cols, rows Image dimensions.
/// @param[out] channels Number of channels; always 1 for PIC.
/// @param[out] bits Bits per channel; always 8 for PIC.  
int dgc_image_query_any(const char *filename,
                                  int *cols, int *rows, int *channels, int *bits);
  
/// @brief Read an image from any supported file type.
///
/// @param[in] self Pointer to image.
/// @param[in] filename Image filename.
/// @returns Returns DGC_ERR_INVALID if the image has the wrong format.
int dgc_image_read_any(dgc_image_t *self, const char *filename);

  

#if defined __cplusplus
}
#endif

#endif
