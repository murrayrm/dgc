#include "ggis.h"
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>
#include <assert.h>

#define boolean int
#define TRUE	1
#define FALSE	0

/* Note: much of this was cribbed from usenet posts
 * with the filename LatLong-UTMconversion.cpp
 * by Chuck Gantz- chuck.gantz@globalstar.com.
 *
 * However, this code has been extensively modified
 * by me, daveb@ffem.org.
 * Please contact me for support first if you are having
 * problems with this software.
 */

/* --- Helper functions --- */
/* evaluate a polynomial by horner's rule. */
static double
poly (double variable,
      unsigned   degree,
      double highest_order_coeff,
      ...)
{
  unsigned i;
  double cur = highest_order_coeff;
  va_list args;
  va_start (args, highest_order_coeff);
  for (i = 0; i < degree; i++)
    {
      cur *= variable;
      cur += va_arg (args, double);
    }
  va_end (args);
  return cur;
}

static inline double
square (double v)
{
  return v * v;
}

#define DEGREES_TO_RADIANS		(M_PI / 180.0)
#define RADIANS_TO_DEGREES		(180.0 / M_PI)

/* --- various conversion functions --- */

/* Based on:
 * LatLong-UTMconversion.cpp by Chuck Gantz- chuck.gantz@globalstar.com
 *
 * Equations from USGS Bulletin 1532
 *
 *  East Longitudes are positive, West longitudes are negative.
 *  North latitudes are positive, South latitudes are negative.
 *  Lat and Long are in degrees.
 */

/* This routine determines the correct UTM letter
 * designator for the given latitude, returning
 * FALSE for out-of-range latitudes.
 *
 * Written by Chuck Gantz- chuck.gantz@globalstar.com.
 */
static boolean 
find_utm_letter_designator(double latitude,
			   char   *letter_out)
{
  if (84.0 >= latitude && latitude >= 72)
    {
      *letter_out = 'X';
      return TRUE;
    }
  else if (latitude > 84.0 || latitude < -80.0)
    {
      /* user needs to use Universal Polar Stereographic */
      return FALSE;
    }
  else
    {
      int lat = (int)(latitude + 80.0);
      lat /= 8;
      assert (lat < 20);
      *letter_out = "CDEFGHJKLMNPQRSTUVW"[lat];
      return TRUE;
    }
}


boolean
gis_coord_latlon_to_utm(const GisCoordLatLon   *input,
			GisCoordUTM      *output,
			GisGeodeticModel  model)
{
  const GisGeodeticEllipsoid *ellipsoid = gis_geodetic_ellipsoid_lookup (model);
  double equatorial_radius;
  double eccentricity_squared;
  double k0 = 0.9996;

  double ecc_prime_squared;
  double N, T, C, A, M;

  /* Make sure the longitude is in [-180, +180) */
  double longitude = fmod (input->longitude + 180, 360) - 180;

  double latitude = input->latitude;
  double latitude_radians = latitude * DEGREES_TO_RADIANS;
  double longitude_radians = longitude * DEGREES_TO_RADIANS;
  double longitude_zone_origin;
  double longitude_origin_radians;
  int zone_number;
  char letter;

  if (ellipsoid == NULL)
    return FALSE;

  equatorial_radius = ellipsoid->equatorial_radius;
  eccentricity_squared = ellipsoid->eccentricity_squared;
  
  zone_number = (int) ((longitude + 180.0) / 6.0) + 1;
  
  if (latitude >= 56.0 && latitude < 64.0
      && longitude >= 3.0 && longitude < 12.0)
    zone_number = 32;
  
  /* Special zones for Svalbard */
  if (latitude >= 72.0 && latitude < 84.0)
  {
    if (longitude >= 0.0 && longitude < 9.0)
      zone_number = 31;
    else if (longitude >= 9.0 && longitude < 21.0)
      zone_number = 33;
    else if (longitude >= 21.0 && longitude < 33.0)
      zone_number = 35;
    else if (longitude >= 33.0 && longitude < 42.0)
      zone_number = 37;
  }

  longitude_zone_origin = (zone_number - 1) * 6 - 180 + 3;

  /* +3 puts origin in middle of zone */
  longitude_origin_radians = longitude_zone_origin * DEGREES_TO_RADIANS;

  /* compute the UTM Zone from the latitude and longitude */
  if (!find_utm_letter_designator (latitude, &letter))
    return FALSE;
  assert (0 <= zone_number && zone_number < 100);
  output->letter = letter;
  output->zone = zone_number;

  ecc_prime_squared = (eccentricity_squared) / (1.0 - eccentricity_squared);

  N = equatorial_radius / sqrt (1.0 - eccentricity_squared * square (sin(latitude_radians)));
  T = square (tan(latitude_radians));
  C = ecc_prime_squared * square (cos(latitude_radians));
  A = cos(latitude_radians) * (longitude_radians - longitude_origin_radians);

  M = equatorial_radius
    * (poly (eccentricity_squared,
             3, -5.0 / 256.0, -3.0 / 64.0, -1.0 / 4.0, 1.0)
       * latitude_radians
       - poly (eccentricity_squared,
               3, 45.0 / 1024.0, 3.0 / 32.0, 3.0 / 8.0, 0.0)
       * sin (2.0 * latitude_radians)
       + poly (eccentricity_squared,
               3, 45.0 / 1024.0, 15.0 / 256.0, 0.0, 0.0)
       * sin (4.0 * latitude_radians)
       - poly (eccentricity_squared,
               3, 35.0 / 3072.0, 0.0, 0.0, 0.0)
       * sin (6.0 * latitude_radians));


  output->e = (k0 * N * (A + (1 - T + C) * A * A * A / 6
                         + (poly (T, 2, 5.0, -18.0, 1.0) + 72 * C - 58 * ecc_prime_squared)
                         * A * A * A * A * A / 120)
               + 500000.0);

  output->n = k0
    * (M
       + N * tan (latitude_radians)
       * poly (A * A, 3,
               (61.0 - 58.0 * T + T * T + 600.0 * C - 330 * ecc_prime_squared) / 720.0,
               (5.0 - T + 9.0 * C + 4.0 * C * C) / 24.0,
               0.5,
               0.0
               ));

  /* 10000000 meter offset for southern hemisphere */
  if (output->n < 0)
    output->n += 10000000.0;
  return TRUE;
}

/* converts UTM coords to lat/long.  Equations from USGS Bulletin 1532 
 * Written by Chuck Gantz- chuck.gantz@globalstar.com
 */
boolean
gis_coord_utm_to_latlon (const GisCoordUTM      *input,
			 GisCoordLatLon   *output,
	                 GisGeodeticModel  model)
{
  const GisGeodeticEllipsoid *ellipsoid = gis_geodetic_ellipsoid_lookup (model);
  double k0 = 0.9996;
  double equatorial_radius;
  double eccentricity_squared;
  double ecc_prime_squared;
  double e1;
  double N1, T1, C1, R1, D, M;
  double longitude_zone_origin;
  double mu, phi1, phi1_radians;
  double x, y;
  int zone_number;
  //char *after_number;
  char zone_letter;
  boolean northern_hemisphere;

  equatorial_radius = ellipsoid->equatorial_radius;
  eccentricity_squared = ellipsoid->eccentricity_squared;

  e1 = (1.0 - sqrt (1.0 - eccentricity_squared))
     / (1.0 + sqrt (1.0 - eccentricity_squared));

  x = input->e - 500000.0; /* remove 500,000 meter offset for longitude */
  y = input->n;

  zone_number = input->zone;
  zone_letter = input->letter;
  if (zone_letter != 0 && ('C' > zone_letter || 'X' < zone_letter))
    return FALSE;

  if (zone_letter == 0 || zone_letter - 'N' >= 0)
    northern_hemisphere = TRUE;
  else
    {
      northern_hemisphere = FALSE;

      /* remove 10,000,000 meter offset used for southern hemisphere */
      y -= 10000000.0;
    }

  /* + 3 puts origin in middle of zone */
  longitude_zone_origin = (zone_number - 1) * 6 - 180 + 3;

  ecc_prime_squared = (eccentricity_squared) / (1.0 - eccentricity_squared);


  M = y / k0;
  mu = M / equatorial_radius
     / poly (eccentricity_squared, 3, -5.0 / 256, -3.0 / 64, -1.0 / 4, 1.0);

  {
    double sin_2mu = sin (2.0 * mu);
    double sin_4mu = sin (4.0 * mu);
    double sin_6mu = sin (6.0 * mu);
    phi1_radians = poly (e1, 4, -55.0 * sin_4mu / 32.0,
		    151.0 * sin_6mu / 96.0 - 27.0 * sin_2mu / 32.0,
		    21.0 * sin_4mu / 16.0,
		    3.0 * sin_2mu / 2.0,
		    mu);
  }

  phi1 = phi1_radians * RADIANS_TO_DEGREES;

  N1 = equatorial_radius / sqrt(1.0 - eccentricity_squared * square (sin(phi1_radians)));
  T1 = square (tan(phi1_radians));
  C1 = ecc_prime_squared * square (cos(phi1_radians));
  R1 = equatorial_radius * (1.0 - eccentricity_squared)
     / pow (1.0 - eccentricity_squared * square (sin(phi1_radians)), 1.5);
  D = x / (N1 * k0);

  output->latitude = RADIANS_TO_DEGREES *
        (phi1_radians
       - (N1 * tan (phi1_radians) / R1)
       * poly (D * D,
	       3,
	       (61.0+90*T1+298*C1+45*T1*T1-252*ecc_prime_squared-3*C1*C1)/720.0,
		-(5.0+3*T1+10*C1-4*C1*C1-9*ecc_prime_squared)/24.0,
	       0.5,
	       0.0));
  output->longitude = poly (D,
		 5,
		 (5.0-2*C1+28*T1-3*C1*C1+8*ecc_prime_squared+24*T1*T1)/120.0,
		 0.0,
		 - (1.0 + 2.0 * T1 + C1) / 6.0,
		 0.0,
		 1.0,
		 0.0) / cos (phi1_radians) * RADIANS_TO_DEGREES
		  + longitude_zone_origin;
  return TRUE;
}

/* --- Geodetic models --- */

/* NOTE:  these must match the order in gisgeodeticmodel.h!!! */
/* Reference ellipsoids derived from Peter H. Dana's website- 
       http://www.utexas.edu/depts/grg/gcraft/notes/datum/elist.html
Department of Geography, University of Texas at Austin
Internet: pdana@mail.utexas.edu
3/22/95

Source
Defense Mapping Agency. 1987b. DMA Technical Report: Supplement to Department of Defense World Geodetic System
1984 Technical Report. Part I and II. Washington, DC: Defense Mapping Agency
*/

static GisGeodeticEllipsoid known_models[] =
{
  { GIS_GEODETIC_MODEL_AIRY, "Airy", 6377563, 0.00667054 },
  { GIS_GEODETIC_MODEL_AUSTRALIAN_NATIONAL,
      "Australian National", 6378160, 0.006694542 },
  { GIS_GEODETIC_MODEL_BESSEL_1841, "Bessel 1841", 6377397, 0.006674372 },
  { GIS_GEODETIC_MODEL_BESSEL_1841_NAMBIA,
      "Bessel 1841 (Nambia) ", 6377484, 0.006674372 },
  { GIS_GEODETIC_MODEL_CLARKE_1866, "Clarke 1866", 6378206, 0.006768658 },
  { GIS_GEODETIC_MODEL_CLARKE_1880, "Clarke 1880", 6378249, 0.006803511 },
  { GIS_GEODETIC_MODEL_EVEREST, "Everest", 6377276, 0.006637847 },
  { GIS_GEODETIC_MODEL_FISCHER_1960_MERCURY,
      "Fischer 1960 (Mercury) ", 6378166, 0.006693422 },
  { GIS_GEODETIC_MODEL_FISCHER_1968, "Fischer 1968", 6378150, 0.006693422 },
  { GIS_GEODETIC_MODEL_GRS_1967, "GRS 1967", 6378160, 0.006694605 },
  { GIS_GEODETIC_MODEL_GRS_1980, "GRS 1980", 6378137, 0.00669438 },
  { GIS_GEODETIC_MODEL_HELMERT_1906, "Helmert 1906", 6378200, 0.006693422 },
  { GIS_GEODETIC_MODEL_HOUGH, "Hough", 6378270, 0.00672267 },
  { GIS_GEODETIC_MODEL_INTERNATIONAL, "International", 6378388, 0.00672267 },
  { GIS_GEODETIC_MODEL_KRASSOVSKY, "Krassovsky", 6378245, 0.006693422 },
  { GIS_GEODETIC_MODEL_MODIFIED_AIRY, "Modified Airy", 6377340, 0.00667054 },
  { GIS_GEODETIC_MODEL_MODIFIED_EVEREST, "Modified Everest", 6377304, 0.006637847 },
  { GIS_GEODETIC_MODEL_MODIFIED_FISCHER_1960,
      "Modified Fischer 1960", 6378155, 0.006693422 },
  { GIS_GEODETIC_MODEL_SOUTH_AMERICAN_1969,
      "South American 1969", 6378160, 0.006694542 },
  { GIS_GEODETIC_MODEL_WGS_60, "WGS-60", 6378165, 0.006693422 },
  { GIS_GEODETIC_MODEL_WGS_66, "WGS-66", 6378145, 0.006694542 },
  { GIS_GEODETIC_MODEL_WGS_72, "WGS-72", 6378135, 0.006694318 },
  { GIS_GEODETIC_MODEL_WGS_84, "WGS-84", 6378137, 0.00669438 }
};

const GisGeodeticEllipsoid *
gis_geodetic_ellipsoid_lookup (GisGeodeticModel model)
{
  return (model > GIS_GEODETIC_MODEL_WGS_84) ? NULL : (known_models + model);
}


