#include <iostream>
#include <vector>
using namespace std;

#include <math.h>

// useful vector operations



// magnitude of vector
double vecmag(const vector<double> & vec)
{
  double temp = 0;
  for(unsigned int i = 0; i < vec.size(); i++)
    {
      temp += pow(vec[i], 2);
    }
  return sqrt(temp);
}



// (magnitude of vector) ^ 2
double vecmagsquared(const vector<double> & vec)
{
  double temp = 0;
  for(unsigned int i = 0; i < vec.size(); i++)
    {
      temp += pow(vec[i], 2);
    }
  return temp;
}



// dot product of two vectors
double vecdot(const vector<double> & vec1, const vector<double> & vec2)
{
  double temp = 0;
  for(unsigned int i = 0; i < vec1.size(); i++)
    {
      temp += vec1[i] * vec2[i];
    }
  return temp;
}



// store array2vector
vector<double> array2vector(unsigned int size, double* array)
{
  vector<double> ret;
  for(unsigned int i = 0; i < size; i++)
    cout << array[i] << ", ";
  //  for(unsigned int i = size-1; i != 0; i--)
  //  ret.push_back(array[i]);
  for(unsigned int i = 0; i < size; i++)
    ret.push_back(array[i]);
  return ret;
}



// print vector
void vecprint(vector<double> vec)
{
  if (vec.size() == 0)
    cout << "{}";
  else
    {	  
      cout << "{";
      for(unsigned int i = 0; i < vec.size()-1; i++)
	{
	  cout << vec[i] << ", ";
	}
      cout << vec[vec.size()-1] << "}";
    }
}



// + operator
vector<double> operator + (const vector<double> & lhs, const vector<double> & rhs)
{
  vector<double> ret = lhs;
  for (unsigned int i = 0; i < ret.size(); i++)
    ret[i] += rhs[i];
  return ret;
}

vector<double> operator + (const vector<double> & lhs, const double & rhs)
{
  vector<double> ret = lhs;
  for (unsigned int i = 0; i < ret.size(); i++)
    ret[i] += rhs;
  return ret;
}

vector<double> operator + (const double & lhs, const vector<double> & rhs)
{
  vector<double> ret = rhs;
  for (unsigned int i = 0; i < ret.size(); i++)
    ret[i] += lhs;
  return ret;
}



// - operator
vector<double> operator - (const vector<double> & lhs, const vector<double> & rhs)
{
  vector<double> ret = lhs;
  for (unsigned int i = 0; i < ret.size(); i++)
    ret[i] -= rhs[i];
  return ret;
}

vector<double> operator - (const vector<double> & lhs, const double & rhs)
{
  vector<double> ret = lhs;
  for (unsigned int i = 0; i < ret.size(); i++)
    ret[i] -= rhs;
  return ret;
}

vector<double> operator - (const double & lhs, const vector<double> & rhs)
{
  vector<double> ret = rhs;
  for (unsigned int i = 0; i < ret.size(); i++)
    ret[i] = lhs - rhs[i];
  return ret;
}



// * operator
vector<double> operator * (const vector<double> & lhs, const vector<double> & rhs)
{
  vector<double> ret = lhs;
  for (unsigned int i = 0; i < ret.size(); i++)
    ret[i] *= rhs[i];
  return ret;
}

vector<double> operator * (const vector<double> & lhs, const double & rhs)
{
  vector<double> ret = lhs;
  for (unsigned int i = 0; i < ret.size(); i++)
    ret[i] *= rhs;
  return ret;
}

vector<double> operator * (const double & lhs, const vector<double> & rhs)
{
  vector<double> ret = rhs;
  for (unsigned int i = 0; i < ret.size(); i++)
    ret[i] *= lhs;
  return ret;
}



// / operator
vector<double> operator / (const vector<double> & lhs, const vector<double> & rhs)
{
  vector<double> ret = lhs;
  for (unsigned int i = 0; i < ret.size(); i++)
    ret[i] /= rhs[i];
  return ret;
}

vector<double> operator / (const vector<double> & lhs, const double & rhs)
{
  vector<double> ret = lhs;
  for (unsigned int i = 0; i < ret.size(); i++)
    ret[i] /= rhs;
  return ret;
}

vector<double> operator / (const double & lhs, const vector<double> & rhs)
{
  vector<double> ret = rhs;
  for (unsigned int i = 0; i < ret.size(); i++)
    ret[i] = lhs / rhs[i];
  return ret;
}



// ^ operator
// didn't get this working...prolly don't need it though
/*
vector<double> operator^ (const vector<double> & lhs, const vector<double> & rhs)
{
  vector<double> ret = lhs;
  for (unsigned int i = 0; i < ret.size(); i++)
    ret[i] = pow(ret[i], rhs[i]);
  return ret;
}

vector<double> operator^ (const vector<double> & lhs, double & rhs)
{
  vector<double> ret = lhs;
  for (unsigned int i = 0; i < ret.size(); i++)
    ret[i] = pow(ret[i], rhs);
  return ret;
}
*/
