/**
// Vector Operator Library
//
// Changes:
//   12-28-2004, Jason Yosinski, created
//
// Function:
//   defines +, -, *, /, vecmag, vecdot, array2vector,
//   and vecprint for the vector<double> class.
//
// Usage:
//   put #include <vectorOps.hh> and #include <vector.h> in the
//   include section of your .cc file
//
*/

#include <vector>

/* finds the  magnitude of a vector. */
double vecmag(const vector<double> & vec);

/* finds the square of the magnitude of a vector (faster than finding
* the actual magnitude. */
double vecmagsquared(const vector<double> & vec);

/* takes the dot product of two vectors of the same length */
double vecdot(const vector<double> & vec1, const vector<double> & vec2);

/* stores an array in vector form */
vector<double> array2vector(unsigned int size, double* array);

/** prints a vector to cout in the format {a, b, ..., d} */
void vecprint(vector<double> vec);

/* + operator for (vector + vector) */
vector<double> operator + (const vector<double> & lhs, const vector<double> & rhs);
/* + operator for (vector + double) */
vector<double> operator + (const vector<double> & lhs, const double & rhs);
/* + operator for (double + vector) */
vector<double> operator + (const double & lhs, const vector<double> & rhs);

/* - operator for (vector - vector) */
vector<double> operator - (const vector<double> & lhs, const vector<double> & rhs);
/* - operator for (vector - double) */
vector<double> operator - (const vector<double> & lhs, const double & rhs);
/* - operator for (double - vector) */
vector<double> operator - (const double & lhs, const vector<double> & rhs);

/* * operator for (vector * vector) */
vector<double> operator * (const vector<double> & lhs, const vector<double> & rhs);
/* * operator for (vector * double) */
vector<double> operator * (const vector<double> & lhs, const double & rhs);
/* * operator for (double * vector) */
vector<double> operator * (const double & lhs, const vector<double> & rhs);

/* / operator for (vector / vector) */
vector<double> operator / (const vector<double> & lhs, const vector<double> & rhs);
/* / operator for (vector / double) */
vector<double> operator / (const vector<double> & lhs, const double & rhs);
/* / operator for (double / vector) */
vector<double> operator / (const double & lhs, const vector<double> & rhs);
