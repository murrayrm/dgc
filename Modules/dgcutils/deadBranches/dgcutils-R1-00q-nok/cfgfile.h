/*!
 * \file cfgfile.h
 * \brief Header file for dgcutils configuration file utilities
 */
#ifndef __CFGFILE_H__
#define __CFGFILE_H__

#ifdef __cplusplus
extern "C"
{
#endif

/*!
 * \fn dgcFindConfigFile(char *name, char *module)
 * \param name Basename of file
 * \param module Name of the module for the configuration file
 *
 * \brief Location a configuration file and return path
 *
 * The dgcConfigFile function is used to look for a configuration file
 * in a number of standard location and return its path.  The function
 * searches in the following locations and returns the first file found
 *
 *   1. <file> 
 *   2. $DGC_CONFIG_PATH/<module>/<file>
 *   3. ../../etc/<module>/<file>
 *   4. /dgc/etc/<module>/<file>
 *
 * If none of the above files is found, the function prints a warning
 * message to stderr and returns a copy of <file>.
 *
 * The return pointer for the function is a newly allocated string with
 * the path.  The string can be freed with free().
 */
char *dgcFindConfigFile(char *, char *);


/*!
 * \fn dgcFindConfigDir(char *module)
 * \param module Name of the module for the configuration file
 *
 * \brief Locate a configuration directory and return path
 *
 * The dgcConfigDir function is used to look for a configuration directory
 * in a number of standard location and return its path.  The function
 * searches in the following locations and returns the first directory found
 *
 *   1. $DGC_CONFIG_PATH/<module>/
 *   2. ../../etc/<module>/
 *   3. /dgc/etc/<module>/
 *
 * If none of the above direcries is found, the function prints a warning
 * message to stderr and returns a copy of <file>.
 *
 * The return pointer for the function is a newly allocated string with
 * the path.  The string can be freed with free().
 */
char *dgcFindConfigDir(char *module);
  

#ifdef __cplusplus
}
#endif

#endif
