/**********************************************************************
 **********************************************************************
 * +FILE:        rddf.cc
 *    
 * +DESCRIPTION: rddf class methods
 *
 * 
 **********************************************************************
 *********************************************************************/

#include <fstream>
#include <math.h>

#include "RDDF.hh"
#include "ggis.h"

// Constructor
RDDF::RDDF()
{
  numTargetPoints = 0;
}

RDDF::RDDF(char *pFileName)
{
  if (pFileName != NULL) {
    loadFile(pFileName);
  } else {
    numTargetPoints = 0;
  }
}

// Destructor
RDDF::~RDDF()
{
}

RDDF &RDDF::operator=(const RDDF &other)
{
  numTargetPoints = other.getNumTargetPoints();
  targetPoints = other.getTargetPoints();
  return *this;
}

// API comments in header
int RDDF::loadFile(char* fileName)
{
  ifstream file;
  string line;
  RDDFData eachWaypoint;
  bool on_first_line = true;

  numTargetPoints = 0;
  
  file.open(fileName,ios::in);
  if(!file)
  {
    cout << "UNABLE TO OPEN THE RDDF FILE: " << fileName << endl;
    exit(1);
  }

  while(!file.eof())
  {
    GisCoordLatLon latlon;
    GisCoordUTM utm;

    // Number
    getline(file,line,RDDF_DELIMITER);    
    // First input on a line should be checked for empty lines.
    if(line.empty()) {
      continue;
    }

    // -1 lets us stick things in arrays while
    // keeping them with DARPA standards.  (i.e.  starting w/ 
    // waypoint #1)
    eachWaypoint.number = atoi(line.c_str()) - 1;  

    if(eachWaypoint.number < 0)
      {
	cerr << "RDDF:loadFile - Input RDDF is not in correct DARPA format" 
	     << endl;
	    exit(1);
      }
    on_first_line = false;
    eachWaypoint.number++;

    // Latitude
    getline(file,line,RDDF_DELIMITER);
    latlon.latitude = atof(line.c_str());
    
    // Longitude
    getline(file,line,RDDF_DELIMITER);
    latlon.longitude = atof(line.c_str());

    // Converting to UTM
    if(!gis_coord_latlon_to_utm(&latlon, &utm, GEODETIC_MODEL))
    {
      cerr << "Error converting coordinate " << eachWaypoint.number 
	   << " to utm\n";
      exit(1);
    }
    eachWaypoint.Northing = utm.n;
    eachWaypoint.Easting = utm.e;

    // Preserve the location in original coordinates, so that 
    // we don't have to calculate it later.
    eachWaypoint.latitude = latlon.latitude;
    eachWaypoint.longitude = latlon.longitude;

    // Boundary offset and radius
    getline(file,line,RDDF_DELIMITER);
    eachWaypoint.radius = atof(line.c_str()) * RDDF_METERS_PER_FOOT;

    // Speed limit
    getline(file,line,'\n');
    eachWaypoint.maxSpeed = atof(line.c_str()) * RDDF_MPS_PER_MPH;

    if(numTargetPoints <= eachWaypoint.number)
    {
      targetPoints.push_back(eachWaypoint);
      numTargetPoints++;
    }
    else
    {
      break;
    }
  }

  return(0);
}

/** Add a new point to the end of the RDDF */
void RDDF::addDataPoint(RDDFData &data_point)
{
  ++numTargetPoints;
  targetPoints.push_back(data_point);
}


// API comments in header file.
void RDDF::print(ostream & out)
{
  for (int i = 0; i < numTargetPoints; i++)
  {
    printf("N=%d North=%10.3f East=%10.3f VEL=%.3f R=%.2f\n",
	   targetPoints[i].number, targetPoints[i].Northing,
	   targetPoints[i].Easting, targetPoints[i].maxSpeed,
	   targetPoints[i].radius);
  }
}

// API comments in header file.
double RDDF::getTrackLineYaw( int i )
{
  // Return the Yaw of waypoint i's trackline.
  return atan2( targetPoints[i+1].Easting - targetPoints[i].Easting, 
                targetPoints[i+1].Northing - targetPoints[i].Northing );
}

// API comments in header file.
double RDDF::getTrackLineDist( int i )
{
  // Return the distance between waypoints i and i+1
  return sqrt( pow(targetPoints[i+1].Easting - targetPoints[i].Easting, 2) + 
               pow(targetPoints[i+1].Northing - targetPoints[i].Northing, 2) );
}

int RDDF::createRDDFFile(char* fileName)
{
  ofstream file;
  file.open(fileName);
  file.precision(10);
  if(!file)
    {
      cout << "UNABLE TO OPEN THE FILE" << endl;
      return(-1);
    }

  int point_num;
  GisCoordUTM utm;
  GisCoordLatLon latlon;
  for(int i = 0; i < numTargetPoints; i++)
    {
      point_num = i + 1;
      utm.zone = RDDF_UTM_ZONE;
      utm.letter = RDDF_UTM_LETTER;
      utm.n = targetPoints[i].Northing;
      utm.e = targetPoints[i].Easting;

      if (!gis_coord_utm_to_latlon (&utm, &latlon, GEODETIC_MODEL))
	{
	  cerr << "Error converting coordinate to latlon." << endl;
	  exit (1);
	}

      file << point_num << RDDF_DELIMITER
	   << latlon.latitude << RDDF_DELIMITER
	   << latlon.longitude << RDDF_DELIMITER
	   << targetPoints[i].radius / RDDF_METERS_PER_FOOT << RDDF_DELIMITER
	   << targetPoints[i].maxSpeed / RDDF_MPS_PER_MPH << endl;
    }
  return(0);
}

