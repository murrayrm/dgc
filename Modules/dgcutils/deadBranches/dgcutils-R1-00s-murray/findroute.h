/*!
 * \file findroute.h
 * \brief Header file for dgcutils configuration file utilities
 */
#ifndef __FINDROUTE_H__
#define __FINDROUTE_H__

#ifdef __cplusplus
extern "C"
{
#endif

/*!
 * \fn dgcFindRouteFile(char *name)
 * \param name Name of the route file
 *
 * \brief Location a configuration file and return path
 *
 * The dgcConfigFile function is used to look for a route file in a
 * number of standard location and return its path.  The file name
 * should be of the form location_name.  The function searches in the
 * following locations and returns the first file found
 *
 *   1. <file> 
 *   2. $DGC_CONFIG_PATH/routes-<location>/<location>_<name>
 *   3. ../../etc/routes-<location>/<location>_<name>
 *   4. /dgc/etc/routes-<location>/<location>_<name>
 *
 * If none of the above files is found, the function prints a warning
 * message to stderr and returns a copy of <file>.
 *
 * The return pointer for the function is a newly allocated string with
 * the path.  The string can be freed with free().
 */
char *dgcFindRouteFile(char *);
extern int findroute_verbose;

#ifdef __cplusplus
}
#endif

#endif
