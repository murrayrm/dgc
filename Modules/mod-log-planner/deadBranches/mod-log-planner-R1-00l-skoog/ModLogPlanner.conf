
// Nominal maximum distance to closest obstacle before stopping [double, meters] 
// This isn't really beeing used in this module, refer to Planner
NOMINAL_SAFETYBUFFER = 1.0

// Print logicData each execution? [int, 0=no]
PRINT_LOGIC_DATA = 1

// Minimum distance to obstacle in front of alice when driving on road? [float, meters] 
ROAD_OBS_DISTANCE = 16

// Maximum time Alice is allowed to pause wating for an obstacle to disappear [int, seconds]  
MAX_YIELD_TIME = 12

// Desired deceleration (for stopping at intersections) [float, m/(s^2)]
DESIRED_DECELERATION = 0.25

// The minimum time to spend with one mission goal (prevents state oscillation and frequent mission planner replanning requests) [int, seconds]
// This should be higher than 'MAX_YIELD_TIME'
GOAL_MIN_TIME = 15

// The maximum time a maneuver is allowed to take [int, seconds]
ASTERN_MAX_TIME = 29
UTURN_MAX_TIME = 300

// How long to stay in passingreverse (road) mode and be stuck, until switching to offroad mode [int, seconds]
ROAD_TO_OFFROAD_TIME = 90

// Maximum distance to move in examine mode before swithcing to default segment strategy [int, meters]
EXAMINE_MAX_DISTANCE = 10
// Minimum distance to consider a state re-enter to be caused by the same obstacle [int, meters]
EXAMINE_MIN_DISTANCE = 5

// Lower average velocity limit that must be held to be considered moving at all in drive mode ('magic' filtered average). [float, m/s] 
DRIVE_VEL_REQUIREMENT = 0.25

// Lower average velocity limit that must be held to be considered moving at all in intersections ('magic' filtered average). [float, m/s]
INTERSECT_VEL_REQUIREMENT = 0.2 
// Have stop this close to a stopline for a while to pass the 'stop_at_line' test [float, meters]
INTERSECT_STOPLINE_DIST = 1.5
// Minimum/maximum time to stay stopped at stopline before driving into intersection [int, seconds] 
INTERSECT_MIN_STOP_TIME = 2
INTERSECT_MAX_STOP_TIME = 55
// Minimum time between exiting intersection mode and entering it again. To prevent oscillations near intersections [int, seconds]
INTERSECT_DEADTIME = 4
// Minimum distance between stoplines. 'Stop at line'-mode will not be entered if two stoplines are within this distance. 
// This prevents Alice from getting stuck at a stopline when backing up in intersections due to obstacles [float, meters]
INTERSECT_MIN_DISTANCE = 10
// The longest distance allowed to move in intersections before force query of default segment handler [float, meters]
INTERSECT_MAX_DISTANCE = 20
// Distance before intersection (e.g. stopline) to start running IntersectionHandler [int, meter]
INTERSECT_START_HANDLER = 0   // Don't increase this, it seems like IntersectionHandler always says go if Alice is still moving...

// Lower average velocity limit that must be held to be considered moving at all in (parking)zones ('magic' filtered average). [float, m/s]
ZONE_VEL_REQUIREMENT = 0.25
// Timeout for safe->agressive transition [int, seconds]
ZONE_SAFE_TIMEOUT = 58
// Timeout for agressive->bare transition [int, seconds]
ZONE_AGRESSIVE_TIMEOUT = 68
// Timeout bare->offroad transition [int, seconds]
ZONE_BARE_TIMEOUT = 110

// Maximum time offroad per try [int, seconds]
OFFROAD_MAX_TIME = 70
// Maximum (Euclidian) distance do use offroad planner [int, meters] 
OFRROAD_MAX_DISTANCE = 20



// Magic velocity filter cutoff frequencies. 
// Range 0..1 where 0 means infinit update time and 1 means dead-beat update (no filter at all). This is simply the IIR filter constant.
// Fs for acceleration [float, units/execution]
VEL_FILTER_FS_HIGH = 0.4
// Fs for deceleration [float, units/execution]
VEL_FILTER_FS_LOW = 0.1
// Default (fake) speed to reset filter with
VEL_FILTER_RESET = 2.0 

