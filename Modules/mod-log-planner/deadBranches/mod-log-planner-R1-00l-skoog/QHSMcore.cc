/*!
 * \file QHSMcore.cc
 * \brief Quantum Hierarchical State Machine
 *
 * \author Miro Samek, modified by Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */



/*********************************
 * Include header
 *********************************/
#include "QHSMcore.hh"
DEFINE_THIS_FILE; // Assertion debug helping trick (see Qassert)


/*********************************
* Initiate (static) class variables
**********************************/


/*********************************
 * Methods
 *********************************/

/**
 * @brief 	Quantum Hierarchical State Machine constructor
 *			Creates class and defines initial state transition (but does not execute it!)
 * @return  None
 */
QHSMcore::QHSMcore(QPseudoState initial) : myState(&QHSMcore::top), mySource( Q_STATE_CAST(initial) )
{
	// Nothing
}

// Destructor
QHSMcore::~QHSMcore() 
{
	// Nothing
}

// Return version number
char const *QHSMcore::getVersion() { 
   return "QHSMcore 2.2.4.skoog";
}


/**
 * @brief 	Quantum Hierarchical State Machine initialization
 *			Initializes the QHSMcore by executing the transition to the defined initial state.
 * @return  None
 */
void QHSMcore::init(QEvent* e) 
{
	// Watch out for the empty QEvent signal!! (it's killing the advance methods in the states...)
	REQUIRE(myState == &QHSMcore::top &&        /* HSM not executed yet */
			mySource != 0);    				// we are about to dereference mySource
	register QState s = myState;        	// save myState in a temporary
	(this->*(QPseudoState)mySource)(e); 	// top-most initial transition
	// initial transition must go *one* level deep
	// "The initial transition can target only a direct substate of a given state"
	ASSERT(s == TRIGGER(myState, Q_EMPTY_SIG)); // Send EMPTY_SIG to the function assigned from initial()
	s = myState;                               // update the temporary
	TRIGGER(s, Q_ENTRY_SIG);                   // enter the state
	while (TRIGGER(s, Q_INIT_SIG) == 0) {      // init handled?
		// initial transition must go *one* level deep
		ASSERT(s == TRIGGER(myState, Q_EMPTY_SIG));
		s = myState;
		TRIGGER(s, Q_ENTRY_SIG);                  // enter the substate
	}
}


/**
 * @brief 	Quantum Hierarchical State Machine mass initialization
 *			Saves a record of all logic levels created within state machine 
 * 			and initializes them sequetially upon call to init.   
 * 			
 * @return  None
 */
void QHSMcore::record(QPseudoState initial)
{
	/* TODO */
}


/**
 * @brief 	Quantum Hierarchical State Machine assertion
 *			Defines how to handle an assertion error at runtime. 
 * 			Assertions can be futher configured in Qassert.hh
 * @return  None
 */
void QHSMcore::onAssert__(char const *file, unsigned line)
{
	QLOG(2,"\nAssertion error at '%s': %u",file,line);
}


/**
 * @brief 	Quantum Hierarchical State Machine dispatcher
 *			Main method to be called every time you want to execute the current state
 * 			Important to run init() one time before dispatch() is called!
 * @return  None
 */
void QHSMcore::dispatch(QEvent* e) 
{	
	QLOG(9,"\n Current state: %p", Q_UGLY_STATE_CAST(myState) );
	QLOG(9,". Last state: %p", Q_UGLY_STATE_CAST(myLastState) );
	
	// Keep executing member-of-function pointers until 0 is returned.
	for (mySource = myState; mySource != 0;
		mySource = Q_STATE_CAST((this->*mySource)(e))) { }
	
	QLOG(10,"\n Current state: %p", Q_UGLY_STATE_CAST(myState) );
	QLOG(10,". Last state: %p", Q_UGLY_STATE_CAST(myLastState) );
}


/**
 * @brief 	Quantum Hierarchical State Machine tansition
 *			Manage the internal transitions between states.
 * @return  None
 */
void QHSMcore::tranStat(Tran *tran, QState target) 
{
	REQUIRE(target != &QHSMcore::top);        // cannot target "top" state

	QLOG(9,"\n MAKING A TRANSITION from %p",Q_UGLY_STATE_CAST(myState) );
	QLOG(9," to %p", Q_UGLY_STATE_CAST(target) );
	
	myLastState = myState;		// Save history
	register QState s;
	for (s = myState; s != mySource; ) {
		ASSERT(s != 0);                // we are about to dereference s
		
		QState t = TRIGGER(s, Q_EXIT_SIG);
		if (t != 0) { 	// exit action unhandled, t points to superstate
		   s = t;
		}	
		else {  		// exit action handled, elicit superstate
			s = TRIGGER(s, Q_EMPTY_SIG);
		}
	}
	if (tran->myActions == 0) {  	// is the tran object initialized?
		tranSetup(tran, target);  	// setup the transition object
   	}
   	else {  // transition object initialized, execute transition chain
   		register QState *c = &tran->myChain[0];
   		register unsigned short a;
   		for (a = tran->myActions >> 1; a != 0; a >>= 2, ++c) {
   			(this->*(*c))(&pkgStdQEvent[a & 3]);
   		}
   		myState = *c;
   	}
}		


/**
 * @brief 	Quantum Hierarchical State Machine 
 *			Compare argument QState with the current active state
 * @return  (int)0 if mismatch, !0 if match
 */
int QHSMcore::isIn(QState state) 
{
   register QState s;
   for (s = myState; s != 0; s = TRIGGER(mySource, Q_EMPTY_SIG)) {
      if (s == state) {		// do the states match?
         return !0;			// match found, return true
      }
   }
   return 0;                          // no match found, return false
} 

/**
 * @brief 	Quantum Hierarchical State Machine 
 *			Check if the input QState was the last one we visited (issued the state transiotion) 
 * @return  (int)0 if mismatch, !0 if match
 */
int QHSMcore::cameFrom(QState state)
{
	return (myLastState == state);
}

/**
 * @brief 	Quantum Hierarchical State Machine transition setup tool
 *			Initialize transitions between states.
 * @return  None
 */
void QHSMcore::tranSetup(Tran *tran, QState target) {
   QState entry[DIM(tran->myChain)], p, q, s, *c, *e, *lca;
   unsigned short a = 0;

   #define RECORD(state_, sig_) \
      if (TRIGGER(state_, sig_) == 0) {\
         a |= ((sig_) << 14);   \
         a >>= 2;               \
         *c++ = (state_);       \
      } else ((void)0)

   c = &tran->myChain[0];
   *(e = &entry[0]) = 0;
   *(++e) = target;                         // assume entry to target

   // (a) check mySource == target (transition to self)
   if (mySource == target) {
      RECORD(mySource, Q_EXIT_SIG);                    // exit source
      goto inLCA;
   }
   // (b) check mySource == target->super
   p = TRIGGER(target, Q_EMPTY_SIG);
   if (mySource == p) {
      goto inLCA;
   }
   // (c) check mySource->super == target->super (most common)
   q = TRIGGER(mySource, Q_EMPTY_SIG);
   if (q == p) {
      RECORD(mySource, Q_EXIT_SIG);                    // exit source
      goto inLCA;
   }
   // (d) check mySource->super == target
   if (q == target) {
      RECORD(mySource, Q_EXIT_SIG);                    // exit source
      --e;                                    // do not enter the LCA
      goto inLCA;
   }
   // (e) check rest of mySource == target->super->super... hierarchy
   *(++e) = p;
   for (s = TRIGGER(p, Q_EMPTY_SIG); s != 0; 
        s = TRIGGER(s, Q_EMPTY_SIG)) 
   {
      if (mySource == s) {
         goto inLCA;
      }
      *(++e) = s;
   }
   RECORD(mySource, Q_EXIT_SIG);                       // exit source
   // (f) check rest of mySource->super == target->super->super...
   for (lca = e; *lca != 0; --lca) {
      if (q == *lca) {
         e = lca - 1;                         // do not enter the LCA
         goto inLCA;
      }
   }
   // (g) check each mySource->super->super..for each target...
   for (s = q; s != 0; s = TRIGGER(s, Q_EMPTY_SIG)) {
      for (lca = e; *lca != 0; --lca) {
         if (s == *lca) {
            e = lca - 1;                      // do not enter the LCA
            goto inLCA;
         }
      }
      RECORD(s, Q_EXIT_SIG);                                // exit s
   }   
   ASSERT(0);                                        // malformed HSM
inLCA:                // now we are in the LCA of mySource and target
   ASSERT(e < &entry[DIM(entry)]);         // new entry e must fit in
   while ((s = *e--) != 0) {//retrace the entry path in reverse order
      RECORD(s, Q_ENTRY_SIG);                              // enter s
   }
   myState = target;                          // update current state
   while (TRIGGER(target, Q_INIT_SIG) == 0) {
                       // initial transition must go *one* level deep
      ASSERT(target == TRIGGER(myState, Q_EMPTY_SIG));
      a |= (Q_INIT_SIG << 14);
      a >>= 2;
      *c++ = target;
      target = myState;
      RECORD(target, Q_ENTRY_SIG);                    // enter target
   }
   #undef RECORD
   *c = target;
   tran->myActions = (a >> (13 - (c - &tran->myChain[0])*2)) & 0x1;
   ENSURE(tran->myChain[0] != 0 &&       /* transition initialized */
          c < &tran->myChain[DIM(tran->myChain)]);  // check overflow
}



// EOF 'QHSMcore.cc'
