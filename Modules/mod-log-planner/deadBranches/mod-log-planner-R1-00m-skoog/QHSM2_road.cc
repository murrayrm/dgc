/*!
 * \file QHSM2_road.cc
 * \brief Quantum Hierarchical State Machine Level 2 - Road
 *
 * \author Stefan Skoog
 * \date September 2008
 *
 * \ingroup mod-log-planner
 *
 */



/**
 * @brief 	Logic Level 2 State 
 *			Alice drives
 * @return  QSTATE
 */
QSTATE QHSM::S21_drive(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		PRINT_ENTRY;
	 	LVL2_ENTRY;
		// Change state problem rules
		ld->problem->state = DRIVE;
		ld->problem->planner = RAIL_PLANNER;
		ld->problem->obstacle = OBSTACLE_SAFETY;
		return 0;
	 }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG: {
    	 PRINT_INIT;
    	 if (ld->params->seg_goal.illegalPassingAllowed) {
    		 QLOG(7,"Passing allowed ");
    		 Q_INIT(&QHSM::S211_passing); return 0;
    		 return 0;
    	 } else {    	 
    		 QLOG(7,"Passing NOT allowed ");
    		 Q_INIT(&QHSM::S212_nopassing);
    		 return 0;
    	 }
     }
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM::S2_road;
}


/**
 * @brief 	Logic Level 2 State 
 *			Alice is stopped because of some obstacle in the way
 * @return  QSTATE
 */
QSTATE QHSM::S22_stop(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		PRINT_ENTRY; 
	 	LVL2_ENTRY;
		ld->problem->state = STOP_OBS;
		return 0;
	 }
	 case Q_USER_SIG: { 
		 // Do 'ordinary' stuff
		 float stop_duration = since( ld->last_lvl2_tran );
		 
		 /* ToDo:
		  * Why did we stop? (read error signals)
		  * What can we do about it? (replan path, switch path strategy/planner)
		  * Watch out for limit cycles
		  */
		 
		 if ( ld->currentVelocity < ld->DRIVE_VEL_REQUIREMENT ) {
			 // Alice has stopped
			 QLOG(7,"Alice has stopped. ");
			 
			 // Path still blocked?
			 if ( (ld->prevErr & PP_COLLISION_BARE ) || 
				  (ld->prevErr & PP_NOPATH_LEN)
			 	 ) {
				 QLOG(7,"Still stuff in the way to drive, switch to examine mode\t");
				 Q_TRAN(&QHSM::S24_examine); 
				 return 0;
			 }
			 
		 } else {
			 QLOG(7," Alice is stopping...");
		 }
		 
		 // Stopped for obstacle too long?
		 if ( stop_duration > ld->MAX_YIELD_TIME ) {
			 // Back up!
			 QLOG(7,"StopObs timeout (%d seconds), backing up.", ld->MAX_YIELD_TIME );
			 Q_TRAN(&QHSM::S23_astern); 
			 return 0;
		 }
		 
		 // Default printout
		 QLOG(7,"Alice has stopped for obstacle, since %f seconds", stop_duration );
		 return 0;	
	 }
	 
		 
     case Q_EXIT_SIG: {
 		ld->last_stopobs = GET_TIME;
 		PRINT_EXIT; 
 		return 0;
     }
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM::S2_road;
}


/**
 * @brief 	Logic Level 2 State 
 *			Alice reverses (to get away from obstacle)
 * @return  QSTATE
 */
QSTATE QHSM::S23_astern(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY;  
		 LVL2_ENTRY;
		 ld->problem->state = BACKUP;
		 ld->problem->planner = S1PLANNER;
		 // Save the position we startet to back up from
		 QLOG(7,"This is %f meters from the last position we backed up from",distance(ld, ld->last_astern_pos) );
		 // ToDo: If we end up trying to back up from same place as last time, do something else!

		 return 0;
	 }
	 case Q_USER_SIG: { 

    	 // Measure for how long we have been backing up. IN SECONDS (Don't change this to milliseconds!)
		 float astern_duration = since( ld->last_lvl2_tran );
    	 
    	 // We are either done with backup (signal from pPlanner), or timed out. Switch back to drive.
    	 if ( (ld->prevErr & VP_BACKUP_FINISHED) ||
    		  (ld->prevErr & VP_UTURN_FINISHED)  ||
    	      (astern_duration > ld->ASTERN_MAX_TIME)
    	      ) {
    		 QLOG(6,"Backup ");
    		 if ( (ld->prevErr == VP_BACKUP_FINISHED) ||
    		      (ld->prevErr & VP_UTURN_FINISHED)
    			 ) {
    			 QLOG(6,"finished");
    		 } 
    		 if (astern_duration > ld->ASTERN_MAX_TIME) {
    			 QLOG(6,"timeout");
    		 }
    		 // Tell pPlanner to replan
    		 ld->params->planFromCurrPos = true;
    		 
    		 QLOG(6,", swithing to "); 
    		 
    		 // Switch back to drive if we're backed up for too long
	    	 // ...depending what we are allowed to do...
			 if (ld->params->seg_goal.illegalPassingAllowed) {
				 QLOG(6,"passingreverse");
				 Q_TRAN(&QHSM::S213_passingreverse);
	    		 return 0;
	    	 } else {    	 
	    		 QLOG(6,"nopassing");
	    		 Q_TRAN(&QHSM::S212_nopassing);
	    		 return 0;
	    	 }
    	 } else if (  (ld->prevErr & PP_NOPATH_LEN) &&
    			 	  (astern_duration > 3)
    			 	  ) {
    		 /*   a) Serious path planning errors
    		  *   b) Tried to back up for at least 3 seconds 
    		  *   Problem here could be that path is blocked behind us, and we are obviously trying to back up anyway!   
    		  */

    		 QLOG(6,"S1planner is stupid, it can't find a way out of here. Switch to offroad agressive mode! ");
    		 // May need this to get out of here
    		 ld->problem->obstacle = OBSTACLE_AGGRESSIVE;
    		 ld->params->planFromCurrPos = true;
    		 ld->replanInZone = true;
    		 Q_TRAN(&QHSM::S4_offroad);
    		 return 0;
    	 }
	   	 
    	 QLOG(6,"Alice is backing up, since %f seconds", astern_duration); 
    	 return 0;
	 }
     case Q_EXIT_SIG: {
		 // Save time we last backed up
		 ld->last_astern = GET_TIME; 
    	 PRINT_EXIT; 
    	 return 0;
     }
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM::S2_road;
}




/**
 * @brief 	Logic Level 2 State 
 *			Alice is stopped because of some obstacle in the way
 * @return  QSTATE
 */
QSTATE QHSM::S24_examine(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		PRINT_ENTRY; 
	 	LVL2_ENTRY;
		
	 	// Which planner and strategy is best to use here?
	 	ld->problem->state = DRIVE;
	 	ld->problem->planner = RAIL_PLANNER;	
	 	ld->problem->flag = PASS_REV;	// Well, reversing may not be allowed from mission planner, but it's the best way to ger rail planner to really find new paths around obstacles!
	 	ld->resetFilter(1.5);	// Reset filter to have some headroom in time before a new strategy is selected
	 	
		 // Did we just re-enter this state to try to pass the same obstacle?
		 // If Alice hasn't move far enought since last time, apply a more aggressive strategy
		float dist = distance(ld, ld->last_examine_pos);	// The distance Alice has moved since last time we entered this state 
	 	if (dist < ld->EXAMINE_MIN_DISTANCE) {
	 		PRINT_INIT;
	 		float aggr = since(ld->last_aggressive);	// Time since last used aggressive mode
	 		float bare = since(ld->last_bare);	// Time since last used aggressive mode
			 QLOG(7,"We just used this state (ds = %.2f), same obstacle!? ",dist);
			 QLOG(7,"Aggressive %.1f s ago, bare %.1f s ago.\t",aggr,bare);
			 if (aggr < 30) {
				 // We were in aggressive mode last time, try BARE this time!
				 ld->problem->obstacle = OBSTACLE_BARE;  // Get intimate with obstacles!
				 QLOG(7,"Using bare mode\t");
	
			 } else {
				 // We were in safe mode last time, try aggressive now.
				 ld->problem->obstacle = OBSTACLE_AGGRESSIVE;
				 QLOG(7,"Using aggressive mode\t");
			 }
	 	} 
	 	
	 	
		// Replan every time we enter
		ld->params->planFromCurrPos = true;
		ld->replanInZone = true;
		return 0;
	 }
	 case Q_USER_SIG: { 
		 // Do 'ordinary' stuff
		 float duration = since( ld->last_lvl2_tran ); 	// Time consumed IN this state
		 float dist = distance(ld, ld->last_lvl2_pos);	// The distance moved IN this state 
		 /* ToDo:
		  * Make Alice look around to eliminate groundstrikes, spurious obstacles and that kind of stuff
		  */

		 if (since(ld->last_bare) < 30) { 		// Time since last use of bare mode
			 // We were in bare mode recently, use offroad this time! Pretty radical, huh?
			 QLOG(7,"Tried bare last time, GOING OFFROAD... WEEHOOO!");
			 QCONSOLE("\tGOING OFFROAD!");
			 Q_TRAN(&QHSM::S4_offroad);
			 return 0;
		 }
		 
		 // Replan path if it's blocked
		 if ( (ld->prevErr & PP_COLLISION_BARE) ||
			  (ld->prevErr & PP_NOPATH_LEN)
			   ) {
			 // Be a little bit more gentle on the replan, at least some few executions on each to make Alice move just a little bit before reconsidering the path
			 if ( distance(ld, ld->last_lvl2_pos) < 2 ) {
				 /*    a)  Path planner have serious trouble finding a path
				  *    b)  We haven't moved more than 2 meters since we entered this state
				  *	Force path planner to replan
				  */
				 QLOG(7,"No progress, request for path replan");
				 ld->params->planFromCurrPos = true;
				 ld->replanInZone = true;				 
			 }

		 } else if ( !(ld->prevErr & PP_COLLISION_SAFETY) &&
				      (ld->currentVelocity > 3.0)
				        ) {
			 /*   a) Path is not blocked for safe mode (any more)
			  *   b) Driving pretty fast already
			  *   There is nothing to examine anymore, switch to default segment handler.
			  */
			 QLOG(7,"We're driving fast and problems are gone, switching to default segment handler. ");
			 return segment_type(ld->params);
		 }
		 
		 
		 // Difference between initial waypoint distance (at state entry) and the current measured
		 float closer = distance(ld->last_lvl2_pos, ld->exitPoint) - distance(ld, ld->exitPoint);
		 QLOG(9,"Init dist=%02.2f, curr dist=%02.2f, diff=%02.2f\t", distance(ld->last_lvl2_pos, ld->exitPoint), distance(ld, ld->exitPoint), closer);
		 if ( closer > ld->EXAMINE_MAX_DISTANCE ) {
			 /*	 The difference between the distance to the goal at entry of this state, and the current distance to the goal must be over [config] meters 
			  *  Then, enter default segment handler again.
			  */
			 QLOG(7,"Far enough (%02.2f) with this state, switching to default segment handler. ",closer);
			 return segment_type(ld->params);	// Switch to mission planner specified road segment
		 }
			 
			 
		 if ( distance(ld, ld->last_lvl2_pos) > 2*ld->EXAMINE_MAX_DISTANCE ) {
			 /*   We have moved a considerable distance in this state, dunno what is going on, but we better change to default segment strategy (state)
			  */
			 QLOG(9,"Moved %f meters.\t",distance(ld, ld->last_lvl2_pos));
			 QLOG(7,"Moving way too far (%d m), switching to default segment. ",ld->EXAMINE_MAX_DISTANCE);
			 return segment_type(ld->params);	// Switch to mission planner specified road segment
		 }
		 
		 
		 // No progress at all?
		 if ( ld->filteredVelocity < ld->DRIVE_VEL_REQUIREMENT ) {
			 if (dist < 2) {
				 // Goush! No movements at all! Alice has just standing still and starring
				 // Changing to more aggressive won't help, so instead back up! If we recently tried to back up, ask for a replan instead
				 if ( since(ld->last_astern) < 5*ld->MAX_YIELD_TIME ) {
					 // Backed up less than a minute ago. Replan instead!
					 if ( since(ld->last_goal) > ld->GOAL_MIN_TIME ) {
			    		 QLOG(6,"Recently backed up, asking for mission replan.");
			    		 QCONSOLE("\nExamine consider the road blocked.");
						 ld->newErr |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
						 return 0;
					 } else {
						 // We recently requested a new goal, go offroad to brute-force obstacle instead of requesting another (worthless) mission replan
						 QLOG(6,"Recently request of mission replan, didn't help, go offroad!");
						 QCONSOLE("\nRecently request of mReplan, didn't help, OFFROAD!");
						 Q_TRAN(&QHSM::S4_offroad);
						 return 0;
					 }
					 
				 } else {
					 // Haven't tried to back up yet. Do it now.
					 QCONSOLE("\nBacking away from obstacle.");
					 QLOG(6,"Haven't moved at all, back away from obstacle and try again!");	 
					 Q_TRAN(&QHSM::S23_astern);
					 return 0;
				 }

			 } else {
				 // Movements are present, but be a little more aggressive
				 // Make a self-transistion! This will rise the aggressiveness.
				 QLOG(6,"Low progress, more brute force!");
				 Q_TRAN(&QHSM::S24_examine);	
				 return 0;
			 }
 			 
		 } else if ( duration > ld->MAX_YIELD_TIME ) {
			 // Been in this state too long without any other actions to get out of here
			 // Make a self-transistion! This will rise the aggressiveness.
			 QLOG(6,"State time out, more brute force!");
			 Q_TRAN(&QHSM::S24_examine);	
			 return 0;
		 }
		 
		 // Default printout
		 QLOG(7,"Alice has been examining obstacles for %.02f seconds, got %.2f meters closer to the waypoint", duration, closer );
		 return 0;	
	 }
	 
		 
     case Q_EXIT_SIG: {
    	 PRINT_EXIT;
    	 ld->last_examine_pos = getPos(ld);		// Save the position Alice last had when working in this state
    	 // Explicitly save time stamps for mood state (obstacle strategy). Needed for self-transitions
    	 QLOG(8,"Saving obstacle time stamps");
    	 if (ld->problem->obstacle == OBSTACLE_AGGRESSIVE) {
    		 ld->last_aggressive = GET_TIME;
    	 } else if (ld->problem->obstacle == OBSTACLE_BARE) {
    		 ld->last_bare = GET_TIME;
    	 }
    	 return 0;
     }
     case Q_INIT_SIG: NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM::S2_road;
}


// Import all supstates of this logic level. Yes, this is not a beautiful way to do it, but it works perfectly for small state machines
#include "QHSM21_drive.cc"


// EOF 'QHSM2_road.cc'
