/*!
 * \file QHSM5_intersect.cc
 * \brief Quantum Hierarchical State Machine Level 2 - Intersection
 *
 * \author Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */





/******************************************************************
 * 						State helping methods 
 ******************************************************************/
bool QHSM::intersectionJustVisited(Logic_data_t* ld)
{
	float last_time = since( ld->last_intersection_exit); 
	float last_distance = distance(ld, ld->last_intersection_pos);
	if ( last_time < ld->INTERSECT_DEADTIME) {
		QLOG(6,"\nIntersection signal IGNORED due to deadtime (%02.2f < %02.2f)",last_time, ld->INTERSECT_DEADTIME);
		return true;
	} else if ( last_distance < ld->INTERSECT_MIN_DISTANCE) {
		if ( last_time > 60) {
			// We can actually visit the same intersection if we are driving in a loop.
			// It must be more than a minute between each of these visits then.
			QLOG(6,"\nIntersection visited before, but that was %02.2f seconds ago", since( ld->last_intersection_exit) );	
			return false;			
		} else {
			// Same intersection without actually moving out from it first
			QLOG(6,"\nIntersection signal IGNORED due to spatial hysteresis (%02.2f < %02.2f meters)", 
					last_distance, ld->INTERSECT_MIN_DISTANCE);
			return true;
		}
	}
	return false;
}




/******************************************************************
 * 						State methods 
 ******************************************************************/

/**
 * @brief 	Logic Level 2 State 
 *			Alice drives throug an intersection
 * @return  QSTATE
 */
QSTATE QHSM::S51_intersect_go(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY;
		 LVL2_ENTRY;
		 
		 // Change state problem rules
		 ld->problem->state = DRIVE;
		 ld->problem->planner = RAIL_PLANNER;
		 // 'Reset' velocity filter
		 ld->resetFilter(1.1);	// Reset filter to have some headroom in time before a new strategy is selected
		 // Force path planner to replan, makes nicer arcs...
		 ld->params->planFromCurrPos = true;
		 
		 return 0;
	 }
	 //case Q_STOP_INTERSECTION: // Allow super super state to change state due to new segments now!
	 case Q_STOP_INTERSECTION:	// Treat 'new' intersection signal as user signal
     case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 float duration = since(ld->last_lvl2_tran);	// Time consumed since entering this state
    	 float dist = distance(ld,ld->last_lvl2_pos);	// Distance moved since entering this state
    	 
    	 // If low passed velocity is under ~0.2 m/s, Alice is considered stopped, stalled, jammed.
     	 if ( ld->filteredVelocity < ld->INTERSECT_VEL_REQUIREMENT ) {
     		 QLOG(7,"Progress is too low, switching to examine");
     		 Q_TRAN(&QHSM::S24_examine); 
     		 return 0;
     		 
     	 } else if ( !(ld->params->seg_goal.intersection_type == SegGoals::INTERSECTION_STRAIGHT) &&
     			 	 (ld->filteredVelocity > 3.5) &&
     			  	 (duration > 4)
     			  	  ) {
     		 /*   a) We are not merging, i.e. we are not in an (logical) straight intersection.
     		  *   b) We are driving pretty fast, meaning we can NOT still be in an intersection with this speed!?
     		  *   c) We ave been in this state for at least 2 seconds
     		  *      Switch to default segment handler.
     		  */
     		 QLOG(7,"Progress is too HIGH to be in an intersection, switching to default segment handler.  ");
     		 return segment_type(ld->params);	// Switch to mission planner specified road segment

     	 } else if (dist > ld->INTERSECT_MAX_DISTANCE) {
     		 // We have moved more than 15 meters in this state, we must be out of the intersection now!
     		 QLOG(7,"Have moved more than %02.2f meters in this state, switching to default segment handler. ",ld->INTERSECT_MAX_DISTANCE);
     		 return segment_type(ld->params);	// Switch to mission planner specified road segment
     		 
     	 } else if ( (since(ld->last_intersection_enter) > ld->MAX_YIELD_TIME*3) &&
     	 			 (ld->currentVelocity < 1.5)
     	 			 ) {
     		 /*   a) Very long time ago we stopped for this intersection. Change method to get out.
     		  *   b) We doesn't move particularly fast
     		  */
     		 if (ld->currentVelocity < ld->INTERSECT_VEL_REQUIREMENT) {
     			 // Not moving. Probably stuck in the middle of the intersection. Examine!
     			 QLOG(7,"Timeout and standstill, using examine to get out of here. ");
     			 Q_TRAN(&QHSM::S24_examine);
     			 return 0;
     		 } else {     		 
     			 QLOG(7,"Timeout and moving, switching to default segment handler.  ");
     			 return segment_type(ld->params);	// Switch to mission planner specified road segment
     		 }
     	 }

    	 QLOG(7,"\tAlice is driving THROUGH an intersection, since %02.2f seconds\t",since(ld->last_lvl2_tran) );
    	 return 0;
     }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM::S5_intersect;
}



/**
 * @brief 	Logic Level 2 State 
 *			Alice is merging to change road in an intersection, running IntersectionHandler
 * @return  QSTATE
 */
QSTATE QHSM::S52_merge(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY; 
		 LVL2_ENTRY;
		 ld->problem->state = STOP_INT;		// Just tell velocity planner to stop by the stopline...
		 
		 QLOG(7,"This is %.2f meters from the last stopline we visited.",distance(ld, ld->last_intersection_pos) );
		 ld->last_intersection_pos = getPos(ld);
		 
		 // Tell path planner to replan since we are in a new traffic situation now, probably stopped with a sharp turn ahead.
		 ld->params->planFromCurrPos = true;
		 
		 // Run intsHandler first time for this intersection. Ignore the result, just get it executing...
		 QLOG(3,"\t");
		 ld->intHandler->checkIntersection( *(ld->vehState), ld->params->seg_goal, ld->params->mergingWaypoints, ld->params->readConfig);
		 
		 return 0;
	 }
	 case Q_NEW_SEGMENT_TYPE:	// Treat new segments as user signal, we are probably standing right at the change between two segments
	 case Q_STOP_INTERSECTION:	// Treat 'new' intersection signal as user signal
	 case Q_USER_SIG: { 
		 // Do 'ordinary' stuff
		 float stop_duration = since( ld->last_lvl2_tran );
		  
		 // Run fancy intersection handler
		 CIntersectionHandling::IntersectionReturn inter_ret;  // Return value
		 QLOG(3,"\n\t");
		 inter_ret = ld->intHandler->checkIntersection( *(ld->vehState), 
				 ld->params->seg_goal, ld->params->mergingWaypoints, ld->params->readConfig);
		 
		 // Interpret the answer from the fancy IntersectionHandler
		 switch(inter_ret){
		   case CIntersectionHandling::RESET: {
			   QLOG(6,"Intersection handler reset requested");
			   break;
		   }
		   case CIntersectionHandling::WAIT_PRECEDENCE: {
			   QLOG(6,"Intersection handler is waiting for precedence");
			   break;
		   }
		   case CIntersectionHandling::WAIT_MERGING: {
			   QLOG(6,"Intersection handler is waiting for merging");
			   break;
		   }
		   case CIntersectionHandling::WAIT_CLEAR: {
			   QLOG(6,"Intersection handler is waiting for clear intersection");
			   break;
		   }
		   case CIntersectionHandling::GO: {
			   // Finally, go!
			   QLOG(6,"Intersection handler says: GO!");
			   if (stop_duration > ld->INTERSECT_MIN_STOP_TIME) {
				   // Must have stopped for a minimum time before driving...
				   Q_TRAN(&QHSM::S51_intersect_go);				   
			   } else {
				   QLOG(7," ...waiting for minimum time (%02.2f/%d)",stop_duration,ld->INTERSECT_MIN_STOP_TIME);   
			   }
			   return 0;
			   break;
		   }
		   case CIntersectionHandling::JAMMED: {
			   QLOG(6,"Intersection handler consider us jammed");
			   break;
		   }
		   default: {
			   QLOG(6,"Intersection handler return message undefined");
		   }
		 }
		 
		 
		 // Timeout
		 if ( stop_duration > ld->INTERSECT_MAX_STOP_TIME ) {
			 // Boring, just drive straight out in the intersection using default planner
			 QLOG(7,"Intersection state time out (%d s)! Switching to drive",ld->INTERSECT_MAX_STOP_TIME);
			 Q_TRAN(&QHSM::S51_intersect_go); 
			 return 0;
		 }
	    	
		 QLOG(7,"\n\tAlice has waited in intersection for %02.2f seconds\n", stop_duration);
		 return 0;	
	 }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM::S5_intersect;
}



/**
 * @brief 	Logic Level 2 State 
 *			Alice is stopping at the stopline. Not authorized to go on until correctly stopped at the line!
 * @return  QSTATE
 */
QSTATE QHSM::S53_stop_at_line(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY; 
		 LVL2_ENTRY;
		 // Just tell velocity planner to stop by the stopline...
		 ld->problem->state = STOP_INT;		
		 
		 if (ld->params->seg_goal.stopAtExit) {
			 QLOG(6,"Intersection has stopline, stop.\t");
		 } else {
			 QLOG(6,"Intersection has NO stopline, slow down to merge.\t");
		 }
		 // This info might help...
		 QLOG(7,"This is %02.2f meters from the last stopline we topped at",distance(ld, ld->last_intersection_pos) );
		 
		 return 0;
	 }
	 case Q_NEW_SEGMENT_TYPE:	// Treat new segments as user signal, we are probably standing right at the change between two segments
	 case Q_STOP_INTERSECTION:	// Treat 'new' intersection signal as user signal
	 case Q_USER_SIG: { 
		 // Do 'ordinary' stuff
		 float duration = since( ld->last_intersection_enter);

		 if ( MIN(ld->stoplineDistance,ld->intersectDistance) < ld->INTERSECT_START_HANDLER ) {
			 /* When getting close to intersection [config], start running intersection handler so it can have a decition when stopping
			  * Don't care about it's result until we stand completely still, that's in 'merge' state
			  */
			 QLOG(3,"\n\t");
			 ld->intHandler->checkIntersection( *(ld->vehState), ld->params->seg_goal, ld->params->mergingWaypoints, ld->params->readConfig);
		 }
		 
		 if ( (!ld->params->seg_goal.stopAtExit)  &&
			  (ld->intersectDistance < 1.0)
			   ) {
			 /*   a) No stopline to stop at
			  *   b) We are very close or in intersection area
			  * Switch to merge right away!
			  */
			 QLOG(7,"In intersection, no stopline to stop at, switch to merge.");
			 Q_TRAN(&QHSM::S52_merge);
			 return 0;
		 }
		 
		 if (  (ld->currentVelocity < ld->INTERSECT_VEL_REQUIREMENT) &&
			   (fabs(ld->stoplineDistance) < ld->INTERSECT_STOPLINE_DIST) &&
			   (duration > 3) ) {
			 /*  a) Actually stopped?  (Use real velocity, not filtered...) 
			  *  b) Near the stopline? 
			  *  c) For three seconds?
			  *  We have found and stopped at the stopline!
			  */  
			 QLOG(6,"Alice successfully stopped at the stopline (v < %02.2f)", ld->INTERSECT_VEL_REQUIREMENT);
			 QCONSOLE("\nAlice successfully stopped at the stopline.");
			 Q_TRAN(&QHSM::S52_merge); 
			 return 0;
			 
		 } else if ((ld->currentVelocity < ld->INTERSECT_VEL_REQUIREMENT) &&
		   			(ld->INTERSECT_STOPLINE_DIST == INFINITY) &&
		   			(duration > 3) ) {
			 /*  a) Stopline disappeared
			  *  b) We are standing pretty still
			  *  c) For at least three seconds
			  *  Switch to merge instead of trying to stop at the stop line (we won't find it anyway)
			  */ 
		   	 QLOG(6,"Stopline disappeared, switching to merge!");
		   	 Q_TRAN(&QHSM::S52_merge);
		   	 return 0;
		   	 
		 } else if ( (ld->stoppingDistance < ld->stoplineDistance) &&
				 	 (ld->prevErr & PP_COLLISION_BARE)
				     ) {
			 /*  a) Deceleration is higher than needed, obstacle in the way?
			  *  b) Path planner error, obstacle IN the way!
			  * 
			  *  // Switch to default segment handler
			  */
			 QLOG(7,"Found obstacle when trying to stop!  ");
			 return segment_type(ld->params);
		 
		 } else if ( (ld->currentVelocity < ld->INTERSECT_VEL_REQUIREMENT) &&
				 	 (ld->prevErr & PP_COLLISION_BARE) ) {
			 /*  a) Standing still?
			  *  b) Path planner error!
			  *  There is most likely an obstacle in the way, 
			  *  OR the upcoming intersection is jammed before we even stopped at the line
			  *  Ask mission planner for a replan (hopefully, we can take another way through the intersection that is not jammed.
			  *  Backing up here is not a good idea?
			  */
			 QLOG(6,"Intersection ahead is jammed, requesting mission replan!");
			 QCONSOLE("\nIntersection jammed, mission failed.");
			 ld->newErr |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
			 // Note: Since Alice has stopped, a new segment will most likely make us transition to drive state next execution!
			 return 0;
		 }
		 
		 
		 // Timeout in this mode
		 if ( duration > ld->MAX_YIELD_TIME*2 ) {
			 // Time is up! Go through intersection.
			 QLOG(7,"\tWaited in intersection long enough (%d s), switching to drive",(int)duration);
			 QCONSOLE("\nStop at line timeout!");
			 Q_TRAN(&QHSM::S51_intersect_go); return 0;
		 }
	 
		 QLOG(7,"\tAlice has been looking for the stop line for %02.2f seconds", duration);
		 return 0;	
	 }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM::S5_intersect;
}



// EOF 'QHSM5_intersect.cc'
