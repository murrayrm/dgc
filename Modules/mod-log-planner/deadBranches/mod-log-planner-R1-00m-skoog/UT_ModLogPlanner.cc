/*!
 * \file UT_ModLogPlanner.cc
 * \brief Unit Test Source Code for Modular Logic Planner
 *
 * \author Stefan Skoog
 * \date June 2008
 *
 * \ingroup mod-log-planner
 *
 */







/*********************************
* Initiate (static) class variables
**********************************/
using namespace std;

#include "SM.cc"


/*********************************
 * Main Function
 *********************************/

class Aggregator;

class NO_VTABLE State:public IState_base<Aggregator,State>
{
public:
  virtual State* event1(Aggregator*,const boost::any&){return this;}
  virtual State* event2(Aggregator*,const boost::any&){return this;}
};
/*****************************************************************************/
*   IState_base<Aggregator>
*           |
*         State     <<-app-specific, prototypes every event handled by the system
*         / | \
*        /  |  \
*       /   |   \
* state3  state4 state1 <<each state implements events that it cares about
*                 |
*                state2
******************************************************************************/
class Aggregator:public Fsm<Aggregator,State>{
    class state1:public State{
        virtual State* event1 (Aggregator* p,const boost::any&)
        {return &p->m_state2;}
        virtual State* event2 (Aggregator* p,const boost::any&){return this;}
    } m_state1;

    class state2:public state1{
        virtual State* event1(Aggregator* p,const boost::any&)
        {return &p->m_state1;}
    } m_state2;
};

Aggregator::Event get_event(boost::any& arg){
   return (random)? State::event1 : State::event2;      
}

int main()
{
 for(;;){
   boost::any arg;
   Aggregator::Event e=get_event(arg);
   State* next_state=a.dispatch(e,arg);
   if(0==next_state) break;
 }
}


// EOF 'UT_ModLogPlanner.cc'
