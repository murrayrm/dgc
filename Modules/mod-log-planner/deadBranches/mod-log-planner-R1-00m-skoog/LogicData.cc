/*!
 * \file LogicData.cc
 * \brief 	Quantum Hierarchical State Machine Event Definitions
 * 			Maybe, some day, a bunch of methods to parse discrete events will show up here
 *
 * \author Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */



/*********************************
 * Includes
 *********************************/
//#include <temp-planner-interfaces/ConfigFile.hh>
//#include <temp-planner-interfaces/Console.hh>
//#include <temp-planner-interfaces/Log.hh>
#include "LogicData.hh"
#include "Qassert.hh"
DEFINE_THIS_FILE; // Assertion debug helping trick (see Qassert)

/*********************************
* Initiate (static) class variables
**********************************/

// Store names of road segments
/*
const char* road_segment_names[] = { 	"ROAD_SEGMENT", "PARKING_ZONE", "INTERSECTION", "PREZONE", 
										"UTURN", "RESET", "DRIVE_AROUND", "BACK_UP", "END_OF_MISSION", 
										"EMERGENCY_STOP", "UNKNOWN", "START_CHUTE"};
*/

// This pre-defined array is needed AND reserved for state transitions only! 
QEvent pkgStdQEvent[] = {
	{Q_EMPTY_SIG}, 
	{Q_INIT_SIG}, 
	{Q_ENTRY_SIG}, 
	{Q_EXIT_SIG}, 
	{Q_USER_SIG} 
};



/*********************************
 * Methods
 *********************************/



// EOF 'LogicData.cc'
