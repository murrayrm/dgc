/*!
 * \file QHSM1.cc
 * \brief Quantum Hierarchical State Machine Level 1
 *
 * \author Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */



/*********************************
 * Include header 
 *********************************/
#include "QHSM1.hh"
DEFINE_THIS_FILE; // Assertion debug helping trick (see Qassert)


/*********************************
 * Basic class functions 
 *********************************/

// 1 st constructor
QHSM1::QHSM1(Logic_data_t& logicData): QHSM( (QPseudoState)&QHSM1::initial ), ld(&logicData) 
{ 
	qprint("\n[%s]:    Got logicData pointer %p upon creation.",__FUNCTION__,ld);
	qprint("\n[%s]:    first logicData problem pointer: %p.",__FUNCTION__,ld->problem);
}

// 2nd constructor
QHSM1::QHSM1(QPseudoState initial): QHSM( (QPseudoState)&QHSM1::initial )
{
	// Nothing
}

/**
 * @brief 	Logic Level 1 initializer
 *			Defines and executes initial state transition.
 * 			Initializes internal state variables.
 * @return  None
 */
void QHSM1::initial(QEvent const *e) // <--- DO NOT TRY TO DEREFERENCE THIS QEvent! 
{
	PRINT_INFO; qprint(" \tTOP-%s;",__FUNCTION__); 
	
	/***********  Init any internal variables here  ***********/
	
	// This is first state transition, save time
	last_trans_time = Utils::getTime();
	last_paused = Utils::getTime();
	last_astern = Utils::getTime();
	
	// Create default rules for all level 1 states
	static StateProblem_t S1 = 	{PAUSE, 0.99, NO_PASS, ROAD_REGION,  RAIL_PLANNER, OBSTACLE_SAFETY };
	S1_pause_problem = 		&S1;
	static StateProblem_t S2 = 	{DRIVE, 0.99, NO_PASS, ROAD_REGION,  RAIL_PLANNER, OBSTACLE_SAFETY };
	S2_road_problem = 		&S2;
	static StateProblem_t S3 = 	{DRIVE, 0.99, PASS,	   ZONE_REGION,  S1PLANNER,    OBSTACLE_SAFETY };
	S3_zone_problem = 		&S3;     	
	static StateProblem_t S4 = 	{DRIVE, 0.99, OFFROAD, ROAD_REGION,  S1PLANNER,    OBSTACLE_SAFETY };
	S4_offroad_problem = 	&S4; 
	//static StateProblem_t S5 = 	{DRIVE, 0.99, NO_PASS, INTERSECTION, RAIL_PLANNER, OBSTACLE_SAFETY }; // Alternative problem
	static StateProblem_t S5 = 	{DRIVE, 0.99, NO_PASS, ROAD_REGION, RAIL_PLANNER,  OBSTACLE_SAFETY };
	S5_intersect_problem = 	&S5;
	static StateProblem_t S6 = 	{DRIVE, 0.99, NO_PASS, ROAD_REGION,  S1PLANNER,    OBSTACLE_SAFETY };
	S6_uturn_problem = 		&S6;
	static StateProblem_t S7 =	{PAUSE, 0.99, NO_PASS, ROAD_REGION,  RAIL_PLANNER, OBSTACLE_SAFETY };
	S7_fail_problem = 		&S7;
	
	
	Q_INIT(&QHSM1::S0_top);  		// Initial transition, make transition to road
}

/**
 * @brief 	Quantum Hierarchical State Machine, Logic Level 1 destructor
 *			Performs a transition to the highest known state and exits.
 * @return  None
 */
void QHSM1::destroy()
{
	qprint("QHSM1 GOT A TERMINATE SIGNAL!\nNow transistioning to S0_top and exit");
	// Transition to S0_top
	Q_TRAN(&QHSM1::S0_top);
	// Now it's safe to exit
	return;
}


/******************************************************************
 * 						State helping methods 
 ******************************************************************/

QSTATE QHSM1::segment_type(Logic_params_t const *p)
{
 	switch (p->segment_type) {
 	  case SegGoals::ROAD_SEGMENT:	qprint("On Road, switch to road state.");	
 	  									Q_TRAN(&QHSM1::S2_road); return 0; 
 	  case SegGoals::PARKING_ZONE:	qprint("In parking zone, switch to zone state.");	
 	  									Q_TRAN(&QHSM1::S3_zone); return 0;
 	  case SegGoals::PREZONE:		qprint("Prezone segment type, swith to zone state"); 	
 	  									Q_TRAN(&QHSM1::S3_zone); return 0;
 	  case SegGoals::INTERSECTION:	qprint("In intersection, switch to intersection state.");	
 	  									Q_TRAN(&QHSM1::S5_intersect); return 0;
 	  case SegGoals::UTURN:			qprint("U-turn requested, switch to U-turn state.");	
 	  									Q_TRAN(&QHSM1::S6_uturn); return 0;
 	  case SegGoals::DRIVE_AROUND:	qprint("Just drive around, use offroad planning"); 	
 	  									Q_TRAN(&QHSM1::S4_offroad); return 0;
 	  case SegGoals::RESET:			qprint("RESET SIGNAL FROM mPLANNER, pause state"); 	
 	  									Q_TRAN(&QHSM1::S1_pause); return 0;
 	  case SegGoals::END_OF_MISSION: qprint("End of mission, pause state"); 	
 	  									Q_TRAN(&QHSM1::S1_pause); return 0;
 	  case SegGoals::EMERGENCY_STOP: qprint("EM STOP!, switch to pause state"); 	
 	  									Q_TRAN(&QHSM1::S1_pause); return 0;
 	  case SegGoals::START_CHUTE: 	qprint("Start Chute!?, switch to pause state"); 	
 	  									Q_TRAN(&QHSM1::S1_pause); return 0;
 	  case SegGoals::BACK_UP:		qprint("Reversing requested by MP, swith to ???"); 	return 0; 	  										 	  
 	  case SegGoals::UNKNOWN:		qprint("Unknown segment type, switch to pause state"); 	
 	  									Q_TRAN(&QHSM1::S1_pause); return 0;
 	  default: qprint("Mission planner supplied a strange road segment type!\n"); 	 		  

 	}  
  	return (QSTATE)&QHSM1::top;
}


void QHSM1::defaultStateError()
{
	qprint(" \tUnrecognized signal, returning super state.")	
}


// Copy state problems from  source to target
void QHSM1::copyStateProblem(StateProblem_t* target, const StateProblem_t* source)
{
	//qprint("\n[%s]: Got StateProblem_t target pointer: %p",__FUNCTION__,target);

	// Debug a few pointer from logicData struct
	//qprint("\n[%s]: logicData*: %p\n\t\t.problem:\t%p\n\t\t.params:\t%p", __FUNCTION__,	ld, ld->problem, ld->params);

	// Cath evil null pointers
	if (target==NULL || target == (void*)0xFFFFFFFF ) {
		qprint("\n%s: Nughty target! NULL pointer, aborting copy...",__FUNCTION__);
		return;
	}
	if ( source==NULL ) {
		qprint("\n%s: Source was null, using default road problem. \n",__FUNCTION__);
		// Let source be an preset "Road" StateEvent and recursively return
		copyStateProblem( target, S1_pause_problem );
		return;
	}

	memcpy( target, source, sizeof(StateProblem_t) );

	// Backup solution when/if memcpy fails
	//target->state = source->state;
	//target->probability = source->probability;
	//target->flag = source->flag;
	//target->region = source->region;
	//target->planner = source->planner;
	//target->obstacle = source->obstacle;

	
	//qprint("[%s]: Done with ",__FUNCTION__); printStateProblem( target );

	return;
}


// Quick complete print of a StateProblem_t, mostly used for debugging
void QHSM1::printStateProblem(const StateProblem_t* s)
{
	if (s==NULL) {
		qprint("STATE PROBLEM WAS NULL POINTER!\n")
	} else {
		qprint("state problem %p: { %s, %s, %s, %s, %s }\n", s,
				stateString[s->state], flagString[s->flag],
				regionString[s->region], plannerString[s->planner],
				obstacleString[s->obstacle] );
	}
}




/******************************************************************
 * 						State methods 
 ******************************************************************/

/**
 * @brief 	Logic Level 0 State: S0_top 
 *			Absolute superstate
 * @return  QSTATE
 */
QSTATE QHSM1::S0_top(QEvent* e)
{
	qprint("\nLast transition: %llu ms ago", (Utils::getTime()-last_trans_time)/1000 );
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_EMPTY_SIG: break;
	 case Q_ENTRY_SIG: qprint("%s-ENTRY;",__FUNCTION__); return 0;
     case Q_EXIT_SIG:  qprint("%s-EXIT;", __FUNCTION__); return 0;
     case Q_INIT_SIG:  qprint("%s-INIT;", __FUNCTION__); Q_INIT(&QHSM1::S1_pause); return 0;
     case Q_USER_SIG: 			return segment_type(ld->params);
     case Q_NEW_SEGMENT_TYPE:  	return segment_type(ld->params);
     case Q_STOP_INTERSECTION: {
    	 // Intersection if very close, stop!
    	 qprint("Intersection ahead, deal with it immediately!");
    	 Q_TRAN(&QHSM1::S5_intersect); return 0;
     }
     default: qprint("Dunno what to do with this signal...");  // NO RETURN VALUE ALLOWED on default!
	}
	// This is the superstate: This (only this) state should return the top state (inherited from QHSM core)
    return (QSTATE)&QHSM1::top; 
}


/**
 * @brief 	Logic Level 1 State: S1_pause 
 *			Alice is paused
 * @return  QSTATE
 */
QSTATE QHSM1::S1_pause(QEvent* e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {	qprint("%s-ENTRY;",__FUNCTION__); 
		 copyStateProblem( ld->problem, S1_pause_problem ); 
		 last_paused = Utils::getTime();
    	 return 0;
     }
     case Q_USER_SIG: {
    	 qprint("\t Alice has been paused for %llu ms", (Utils::getTime()-last_paused)/1000 );
    	 return 0;
     }
     case Q_EXIT_SIG:  qprint("%s-EXIT;", __FUNCTION__); return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
     case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
	
    return (QSTATE)&QHSM1::S0_top;
}


/**
 * @brief 	Logic Level 1 State: S2_road 
 *			Alice is driving on the road with a set of rules defined by level >=2 substates
 * @return  QSTATE
 */
QSTATE QHSM1::S2_road(QEvent* e)
{
	PRINT_STATE(e); 
	switch (e->sig) {
	 case Q_ENTRY_SIG: { qprint("%s-ENTRY;",__FUNCTION__); 
	 	copyStateProblem( ld->problem, S2_road_problem ); 
    	return 0;
     } 
     case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 qprint("\tAlice is driving...");
    	 
    	 return 0;
     }
     case Q_INIT_SIG:  qprint("%s-INIT;", __FUNCTION__); Q_INIT(&QHSM1::S21_drive); return 0;
     case Q_EXIT_SIG: qprint("%s-EXIT;", __FUNCTION__); return 0;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S0_top;
}


/**
 * @brief 	Logic Level 1 State: S3_zone 
 *			Alice is driving on an open-zone area with a set of rules specified by level >=2 substates
 * @return  QSTATE
 */
QSTATE QHSM1::S3_zone(QEvent* e) 
{
	PRINT_STATE(e); 
	switch (e->sig) {
	 case Q_ENTRY_SIG: { qprint("%s-ENTRY;",__FUNCTION__); 
	 	 copyStateProblem( ld->problem, S3_zone_problem); 
    	 return 0;
     }
     case Q_USER_SIG: {  
    	 // Do 'ordinary' stuff
    	 qprint("\tAlice is in a parking zone...");
    	 
    	 return 0;
     }
     case Q_EXIT_SIG:  qprint("%s-EXIT;", __FUNCTION__); return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S0_top;
}


/**
 * @brief 	Logic Level 1 State: S4_road 
 *			Alice is driving off-road with a set of rules defined by level >=2 substates
 * @return  QSTATE
 */
QSTATE QHSM1::S4_offroad(QEvent* e)
{
	PRINT_STATE(e); 
	switch (e->sig) {
	 case Q_ENTRY_SIG: { qprint("%s-ENTRY;",__FUNCTION__); 
	 	 copyStateProblem( ld->problem, S4_offroad_problem); 
    	 return 0;
     }
     case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 qprint("\tAlice is OFFROAD!...");
    	 
    	 return 0;
     }
     case Q_EXIT_SIG:  qprint("%s-EXIT;", __FUNCTION__); return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S0_top;
}


/**
 * @brief 	Logic Level 1 State: S5_intersect 
 *			Alice is in an intersection with a set of rules defined by level >=2 substates
 * @return  QSTATE
 */
QSTATE QHSM1::S5_intersect(QEvent* e)
{
	PRINT_STATE(e); 
	switch (e->sig) {
	 case Q_ENTRY_SIG: { qprint("%s-ENTRY;",__FUNCTION__); 
	 	 copyStateProblem( ld->problem, S5_intersect_problem); 
    	 return 0;
     }
     case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 qprint("\tAlice is in an intersection...");
    	 return 0;
     }
     case Q_EXIT_SIG:  qprint("%s-EXIT;", __FUNCTION__); return 0;
     case Q_INIT_SIG:  qprint("%s-INIT;", __FUNCTION__); Q_INIT(&QHSM1::S52_wait); return 0;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S0_top;
}


/**
 * @brief 	Logic Level 1 State: S6_uturn 
 *			Alice is performing an U-turn with a set of rules defined by level >=2 substates
 * @return  QSTATE
 */
QSTATE QHSM1::S6_uturn(QEvent* e)
{
	PRINT_STATE(e); 
	switch (e->sig) {
	 case Q_ENTRY_SIG: { qprint("%s-ENTRY;",__FUNCTION__); 
	 	 copyStateProblem( ld->problem, S6_uturn_problem); 
    	 return 0;
     }
     case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 qprint("\tAlice is doing a U-turn...");
    	 return 0;
     }
     case Q_EXIT_SIG:  qprint("%s-EXIT;", __FUNCTION__); return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S0_top;
}


/**
 * @brief 	Logic Level 1 State: S7_fail 
 *			Alice has failed with every attempt to make progress
 * @return  QSTATE
 */
QSTATE QHSM1::S7_fail(QEvent* e)
{
	PRINT_STATE(e); 
	switch (e->sig) {
	 case Q_ENTRY_SIG: { qprint("%s-ENTRY;",__FUNCTION__); 
	 	 copyStateProblem( ld->problem, S7_fail_problem); 
    	 return 0;
     }
     case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 qprint("\tAlice has failed :(");
    	 return 0;
     }
     case Q_EXIT_SIG:  qprint("%s-EXIT;", __FUNCTION__); return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S0_top;
}
	

// This is just a temporary fix to include other states!
// It will not work like this when the design pattern is all worked out.
#include "QHSM2_road.cc"
#include "QHSM5_intersect.cc"


// EOF 'QHSM1.cc'
