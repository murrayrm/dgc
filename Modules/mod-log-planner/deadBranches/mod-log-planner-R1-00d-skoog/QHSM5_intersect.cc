/*!
 * \file QHSM5_intersect.cc
 * \brief Quantum Hierarchical State Machine Level 2 - Intersection
 *
 * \author Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */



/**
 * @brief 	Logic Level 2 State 
 *			Alice drives throug an intersection
 * @return  QSTATE
 */
QSTATE QHSM1::S51_drive(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 qprint("%s-ENTRY;",__FUNCTION__);
		 
		 // Change state problem rules
		 ld->problem->state = DRIVE;
		 ld->problem->planner = RAIL_PLANNER;
		 // 'Reset' velocity filter
		 ld->resetFilter(1.1);	// Reset filter to have some headroom in time before a new strategy is selected


		 return 0;
	 }
	 case Q_NEW_SEGMENT_TYPE:	// Treat new segments as user signal, we are probably standing right at the change between two segments
	 case Q_STOP_INTERSECTION:	// Treat 'new' intersection signal as user signal
     case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 qprint("\tAlice is driving THROUGH an intersection");
   	 
    	 // You can do a lot of stuff here, if you like... 
    	 // Alice is just driving and waiting for a change in segment type.
    	 
    	 // No passing not successful? Try passing...
    	 // If low passed velocity is under 0.2 m/s, Alice is stopped.
     	 if ( ld->filteredVelocity < 0.19 ) {
     		 ld->resetFilter();	// Reset filter to give astern a chance...
     		 Q_TRAN(&QHSM1::S23_astern); return 0;
     	 } else if (ld->filteredVelocity > 1.3) {
     		 // Driving fast, we can NOT still be in an intersection!?
             Q_TRAN(&QHSM1::S2_road); return 0;
     	 } else if ( (Utils::getTime()-last_intersection)/((int)1E6) > 20) {
     		 // Very long time ago we stopped for this intersection. Change method to get out.
     		 Q_TRAN(&QHSM1::S2_road); return 0;
     	 }

    	 return 0;
     }
     case Q_EXIT_SIG:  qprint("%s-EXIT;\n", __FUNCTION__); return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S5_intersect;
}


/**
 * @brief 	Logic Level 2 State 
 *			Alice is stopped and is waiting in/outside an intersection
 * @return  QSTATE
 */
QSTATE QHSM1::S52_wait(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 qprint("%s-ENTRY;",__FUNCTION__); 
		 ld->problem->state = STOP_INT;		// Just make Alice stop...
		 
		 last_intersection = Utils::getTime(); // Save this timestamp
		 return 0;
	 }
	 case Q_NEW_SEGMENT_TYPE:	// Treat new segments as user signal, we are probably standing right at the change between two segments
	 case Q_STOP_INTERSECTION:	// Treat 'new' intersection signal as user signal
	 case Q_USER_SIG: { 
		 // Do 'ordinary' stuff
		 int stop_duration = (Utils::getTime()-last_intersection)/((int)1E6);
		 qprint("\tAlice has stopped in intersection for %d seconds", stop_duration);
		 
		 // Stopped in intersection enough?
		 if ( stop_duration > 7 ) {			// This is soooo temporary. Just wait 10 seconds and go again...
			 if ( stop_duration > 300 ) {	// If waited for more than 5 minutes, there is something wrong with the timestamp!	
				 last_intersection = Utils::getTime();	// Get a new timestamp
			 } else {
				 // Time is up! Go through intersection.
				 Q_TRAN(&QHSM1::S51_drive); return 0;
			 }
		 }
	    	 
		 return 0;	
	 }
     case Q_EXIT_SIG:  qprint("%s-EXIT;\n", __FUNCTION__); return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S5_intersect;
}



// EOF 'QHSM5_intersect.cc'
