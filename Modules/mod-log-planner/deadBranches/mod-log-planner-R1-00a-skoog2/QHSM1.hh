/*!
 * \file QHSM1.hh
 * \brief Quantum Hierarchical State Machine Level 1
 *
 * \author Miro Samek, modified by Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */


#ifndef QHSM1_hh
#define QHSM1_hh


/*********************************
 * Includes
 *********************************/
//#include <temp-planner-interfaces/ConfigFile.hh>
//#include <temp-planner-interfaces/Console.hh>
#include <temp-planner-interfaces/Log.hh>
#include <dgcutils/DGCutils.hh>	// Uses pause/sleep from here

#include <stdlib.h>
#include <stdio.h>

#include "QHSM.hh"
#include "QEvent.hh"
#include "Qassert.hh"




/*********************************
* Initiate class variables
**********************************/
/*
enum QHSM1Signals {
   A_SIG = Q_USER_SIG, B_SIG, C_SIG, 
   D_SIG, E_SIG, F_SIG, G_SIG, H_SIG
};

static const QEvent testQEvt[] = { 
   {A_SIG}, {B_SIG}, {C_SIG}, {D_SIG}, 
   {E_SIG}, {F_SIG}, {G_SIG}, {H_SIG}
};
*/


/*********************************
 * Class define
 *********************************/
class QHSM1 : public QHSM {
public:
	/* 	Constructor, takes no arguments, but calls base class 
	 	with member function pointer to initial state */ 
	QHSM1(): QHSM( (QPseudoState)&QHSM1::initial ) {}	
	virtual ~QHSM1() {QHSM1::destroy(); }
	
protected:
	void destroy();
	
	void initial(QEvent const *e); 	// Initial (startup) state
		// The list of different available level 0/1-states
		QSTATE S0(QEvent const *e);		// Superstate
		  QSTATE S1_pause(QEvent const *e);	
		  QSTATE S2_road(QEvent const *e);		
		  QSTATE S3_zone(QEvent const *e);		
		  QSTATE S4_offroad(QEvent const *e);	
		  QSTATE S5_intersect(QEvent const *e);	
		  QSTATE S6_uturn(QEvent const *e);				
		  QSTATE S7_fail(QEvent const *e);		
		
private:                               
	// Extended state variables goes here
};





#endif
// EOF 'QHSM1.hh'
