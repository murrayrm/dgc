/*!
 * \file ModLogPlanner.cc
 * \brief Source code for Modular Logic Planner
 *
 * \author Stefan Skoog
 * \date Created June 2008
 *
 * \ingroup mod-log-planner
 *
 */



/*********************************
 * Include header
 *********************************/
#include "ModLogPlanner.hh"
DEFINE_THIS_FILE; // Assertion debug helping trick (see Qassert)


/*********************************
* Initiate (static) class variables
**********************************/




/*********************************
 * Methods
 *********************************/

// Constructor
ModLogPlanner::ModLogPlanner() 
{
	// Call alternative init function
	init();
	return;
}

// Alternative constructor stuff
int ModLogPlanner::init() 
{
	qprint("\nInitializing 'ModLogPlanner'...\n");
	
	// Read config file and save into class variables
	//readConfig();
	
	// Cycle counter, begin at zero
	cycle_counter = 0;
	
	// Init current status, complete state representation
	current_status.state       = DRIVE;
	current_status.probability = 0.99;
	current_status.flag        = PASS;
	current_status.region      = ROAD_REGION;
	current_status.planner     = RAIL_PLANNER;
	current_status.obstacle    = OBSTACLE_SAFETY;
	
	
	// Create the quantum hierarchical state machine
	qhsm = new QHSM1();
	// Init state machine; VERY IMPORTANT!
	qhsm->init();
	
	// Init a new QEvent to feed to qhsm
	qe.sig = Q_USER_SIG;	// Load signal with first user definable signal 
	qe.problem = &current_status; // Let qe.problem point to surrent_status
	
	
	// Create a new IntersectionHandler
	//IntersectionHandling = new CIntersectionHandling();
	
	
	qprint("\nModule 'ModLogPlanner' initiated successfully.\n\n");
	return 0;
}

// Destructor
ModLogPlanner::~ModLogPlanner()
{
	// Get rid of all created objects and class references
	destroy();  // Call alternate destructor
	return;
}

// Alternative destructor
void ModLogPlanner::destroy() 
{
	// Deallocate all claimed objects...
	return;
}

// Read config file
void ModLogPlanner::readConfig() 
{
	// Read from file into workspace
	//... insert code here...
	return;
}



// * * * * * * * Plan logic * * * * * * * //
Err_t ModLogPlanner::planLogic(vector<StateProblem_t> &problems, PlanGraph *graph, Err_t prevErr, 
			 VehicleState &vehState, Map *map, 
			 Logic_params_t &params, int& currentSegmentId, int& currentLaneId,
			 bool& replanInZone) 
{
	
	
	// DEBUGGING: Print message every 10th time call here
	if ( !(++cycle_counter % 10) ) {
		qprint("-->\tModLogPlanner call # %0ld\n", cycle_counter);
	}

	
/*
	// Load QEvent with exactly the same state problems as current problems
	qe.problem->state = 		current_status.state;
    qe.problem->probability = 	current_status.probability;
    qe.problem->flag = 			current_status.flag;
    qe.problem->region = 		current_status.region;
    qe.problem->planner = 		current_status.planner;
    qe.problem->obstacle = 		current_status.obstacle;

    // Fabricate a signal to the state machine
	qe->sig = Q_USER_SIG;	// User sig is first free signal to use
	//qe->params = &params; 
*/
	
	// Print segment type received from planner and/or mission planner
	qprint("Segment type: %s",params.seg_goal.toString().c_str() )
	
	// Run state machine dispatcher
	qhsm->dispatch( &qe );

    // Add new problem to problem queue
    problems.push_back( *(qe.problem) );
	

    // Change return data when code is starting to work
	return LP_OK;
}








// EOF 'ModLogPlanner.cc'
