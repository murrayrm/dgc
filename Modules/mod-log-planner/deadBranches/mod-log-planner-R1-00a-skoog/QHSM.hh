/**********************************
 * Header for QHSM.cc
 * Author: Miro Samek, modified by Stefan Skoog
 * Date:   July 2008
 * 
 **********************************/

#ifndef QHSM_hh
#define QHSM_hh

/*********************************
 * Includes
 *********************************/
#include "QEvent.hh"	// Inport event definition
#include "Qassert.hh"
//#include <temp-planner-interfaces/ConfigFile.hh>
#include <temp-planner-interfaces/Console.hh>
#include <temp-planner-interfaces/Log.hh>



/*********************************
* MACROS
**********************************/

// Choose from {DEBUG_PRINTF, DEBUG_CONSOLE, DEBUG_LOG}
#define DEBUG_LOG

#ifdef DEBUG_PRINTF
  #define qprint(...) printf(__VA_ARGS__);
#endif
#ifdef DEBUG_CONSOLE
  //#define qprint(...) Console::addMessage(__VA_ARGS__);
  static char qprintbuff[255];	// Define char buffer to use with sprintf
  #define qprint(...) sprintf(qprintbuff,__VA_ARGS__);Console::addMessage(qprintbuff);
#endif
#ifdef DEBUG_LOG
  static char qprintbuff[255];	// Define char buffer to use with sprintf
  #define qprint(...) sprintf(qprintbuff,__VA_ARGS__);Log::getStream(1) << qprintbuff;
#endif
  
#ifndef qprint
  #define qprint(...)  	// Empty function
#endif

// Print info in functions at breakpoints
#ifndef PRINT_INFO
#define PRINT_INFO	 	qprint("\nCurrent state: \"%s\": ",__FUNCTION__);
#endif

// Print extensive info in functions at breakpoints
#ifndef PRINT_INFO_EXT
#define PRINT_INFO_EXT 	cout <<"Time: "<<__TIME__<<" "<<__DATE__<<" in file "<<__FILE__<<" in line "<<__LINE__<<" in function \""<< __PRETTY_FUNCTION__ <<"\""<<endl;
#endif



/*********************************
 * Quantum Hierarchical State Machine class define
 *********************************/
class QHSM {           

public:
   
	/* 	Define object QPseudoState as a function member of QHSM 
	   	with QEvent as argument and void return data */ 
	typedef void (QHSM::*QPseudoState)(QEvent const*); 
	
	/* 	Define object QState as a function member of QHSM 
		with QEvent as argument and QPseudoState return data */
	typedef QPseudoState (QHSM::*QState)(QEvent const*);

	// Virtual function to refer to initial stata member function pointer
	//virtual void initial(QEvent const *e) =0;
	
	void init(QEvent const *e = 0);		// Execute initial transition
	void dispatch(QEvent const *e);  	// Dispatch event
	int isIn(QState state);	        	// "is-in-state" query
	char const *getVersion();	// Return framework version
	
	// Assertion error handling. Needed by Qassert.hh to safe debugging
	void onAssert__(char const *file, unsigned line); 
   
protected:
	struct Tran {            		// Protected inner class Tran
      QState myChain[7];
      unsigned short myActions; 	// Action mask (2-bits for action)
	};
	
	/* 	Constructor 
	  	Needs a member function (of QHSM) pointer to initial state (refer to typedef above) */
	QHSM(QPseudoState initial);   	
	
	// Virtual destructor
	virtual ~QHSM();				
	
	// The "top" state (pointer to function member)
	QPseudoState top(QEvent const*) { return 0; } 
	// The current state (pointer to function member)
   	QState getState() const { return myState; }
   	
   	// Dynamic state transition
   	void tran(QState target);              
   	// Static state transition
   	void tranStat(Tran *t, QState target);  
   	// 
   	void init_(QState target) { myState = target; }
   	#define Q_INIT(target_) init_(Q_STATIC_CAST(QState, target_))
   	#define Q_TRAN(target_) if (1) { \
      static Tran t_; \
      tranStat(&t_, Q_STATIC_CAST(QState, target_));\
   	} else ((void)0)
   	#define Q_TRAN_DYN(target_) tran(Q_STATIC_CAST(QState, target_))

private:
	void tranSetup(Tran *t, QState target);

private:
	QState myState;		// The current active state (QHSM member function pointer)
	QState mySource;	// Source state during a transition (QHSM member function pointer)
};

// QSTATE is another name for QPseudoState-functions; member function pointers within QHSM
typedef QHSM::QPseudoState QSTATE;       

// helper macro to calculate static dimension of a 1-dim array
#define DIM(array_) (sizeof(array_)/sizeof(*(array_)))

// Helper macro to present one of the reserved signals to a given statehandler
#define TRIGGER(state_,sig_) \
	Q_STATE_CAST((this->*(state_))(&pkgStdEvt[sig_]))
// Compiler-specific cast
#define Q_STATE_CAST(x_)      reinterpret_cast<QState>(x_)
// Platform-specific cast
#define Q_STATIC_CAST(type_, expr_) static_cast<type_>(expr_)

#endif
// EOF 'QHSM.hh'
