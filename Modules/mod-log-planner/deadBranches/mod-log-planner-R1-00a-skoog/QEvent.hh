/**********************************
 * Header for QEvent.cc
 * Author: Miro Samek, modified by Stefan Skoog
 * Date:   July 2008
 * 
 **********************************/

#ifndef QEvent_hh
#define QEvent_hh

// Includes
#include <temp-planner-interfaces/PlannerInterfaces.h>	// State problem definition from here
#include <gcinterfaces/SegGoals.hh>

typedef unsigned int QSignal;  // Define QSignal

enum {	// Standard QSignals  
	Q_EMPTY_SIG,
	Q_INIT_SIG,
	Q_ENTRY_SIG,
	Q_EXIT_SIG,
	Q_USER_SIG
};

struct QEvent {
	QSignal sig;		// Signal of the event instance
	StateProblem_t *problem;	// Definition of state problems 
	Logic_params_t *params;		// A lot of data from planner and missionPlanner
};

// This pre-defined array is needed AND reserved for state transitions only! 
const QEvent pkgStdEvt[] = {
	{Q_EMPTY_SIG, NULL, NULL}, 
	{Q_INIT_SIG,  NULL, NULL}, 
	{Q_ENTRY_SIG, NULL, NULL}, 
	{Q_EXIT_SIG,  NULL, NULL}, 
	{Q_USER_SIG,  NULL, NULL} 
};





#endif

// EOF 'QEvent.hh'
