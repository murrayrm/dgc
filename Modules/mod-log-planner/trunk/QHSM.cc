/*!
 * \file QHSM.cc
 * \brief Quantum Hierarchical State Machine Level 1
 *
 * \author Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */


/*********************************
 * Include header 
 *********************************/
#include "QHSM.hh"
DEFINE_THIS_FILE; // Assertion debug helping trick (see Qassert)


/*********************************
 * Basic class functions 
 *********************************/

// 1st constructor
QHSM::QHSM(Logic_data_t& logicData): QHSMcore( (QPseudoState)&QHSM::initial ), ld(&logicData) 
{ 
	QLOG(9,"\n[%s]:    Got logicData pointer %p upon creation.",__FUNCTION__,ld);
	QLOG(9,"\n[%s]:    first logicData problem pointer: %p.\n",__FUNCTION__,ld->problem);
}

// 2nd constructor
QHSM::QHSM(QPseudoState initial): QHSMcore( (QPseudoState)&QHSM::initial )
{
	// Nothing
}

/**
 * @brief 	Logic Level 1 initializer
 *			Defines and executes initial state transition.
 * 			Initializes internal state variables.
 * @return  None
 */
void QHSM::initial(QEvent const *e) // <--- DO NOT TRY TO DEREFERENCE THIS QEvent! 
{
	QLOG(2,"\nInitializing Quantum Hierarchical State Machine");
	QLOG(9,"\nDrilling my way down to the initial state...");
	QLOG(3,"\n  @ Initial pseudostate"); 
	
	/***********  Init any internal variables here  ***********/
		
	// Create default srules for all level 1 states
	static StateProblem_t S1 = 	{PAUSE, 0.99, NO_PASS, ROAD_REGION,  RAIL_PLANNER, OBSTACLE_SAFETY };
	S1_pause_problem = 		&S1;
	static StateProblem_t S2 = 	{DRIVE, 0.99, NO_PASS, ROAD_REGION,  RAIL_PLANNER, OBSTACLE_SAFETY };
	S2_road_problem = 		&S2;
	static StateProblem_t S3 = 	{DRIVE, 0.99, PASS,	   ZONE_REGION,  S1PLANNER,    OBSTACLE_SAFETY };
	S3_zone_problem = 		&S3;     	
	static StateProblem_t S4 = 	{DRIVE, 0.99, OFFROAD, ROAD_REGION,  S1PLANNER,    OBSTACLE_SAFETY };
	S4_offroad_problem = 	&S4; 
	//static StateProblem_t S5 = 	{DRIVE, 0.99, NO_PASS, INTERSECTION, RAIL_PLANNER, OBSTACLE_SAFETY }; // Alternative problem
	static StateProblem_t S5 = 	{DRIVE, 0.99, NO_PASS, ROAD_REGION, RAIL_PLANNER,  OBSTACLE_SAFETY };
	S5_intersect_problem = 	&S5;
	static StateProblem_t S6 = 	{UTURN, 0.99, NO_PASS, ROAD_REGION,  S1PLANNER,    OBSTACLE_SAFETY };
	S6_uturn_problem = 		&S6;
	static StateProblem_t S7 =	{PAUSE, 0.99, NO_PASS, ROAD_REGION,  RAIL_PLANNER, OBSTACLE_SAFETY };
	S7_fail_problem = 		&S7;
	
	
	Q_INIT(&QHSM::S0_top);  		// Initial transition, make transition to road
}

/**
 * @brief 	Quantum Hierarchical State Machine, Logic Level 1 destructor
 *			Performs a transition to the highest known state and exits.
 * @return  None
 */
void QHSM::destroy()
{
	QLOG(6,"QHSM GOT A TERMINATE SIGNAL!\nNow transistioning to S0_top and exit");
	// Transition to S0_top
	Q_TRAN(&QHSM::S0_top);
	// Now it's safe to exit
	return;
}


/******************************************************************
 * 						State helping methods 
 ******************************************************************/

// Default way to handle new segment types sent from from mission planner
QSTATE QHSM::segment_type(Logic_params_t const *p)
{
 	switch (p->segment_type) {
 	  case SegGoals::ROAD_SEGMENT:	{
 		  QLOG(6,"On Road, switch to road state.");	
 		  Q_TRAN(&QHSM::S2_road);
 		  return 0; 
 	  }
 	  case SegGoals::PARKING_ZONE:	{
 		  QLOG(6,"In parking zone, switch to zone state.");	
 	  	  Q_TRAN(&QHSM::S3_zone); 
 	  	  return 0; 
 	  }
 	  case SegGoals::PREZONE: {
 		  QLOG(6,"Prezone segment type, swith to zone state"); 	
 		  Q_TRAN(&QHSM::S3_zone); 
 		  return 0;
 	  }
 	  case SegGoals::INTERSECTION: {
 		  QLOG(6,"In intersection, switch to intersection state.");	
 	  	  Q_TRAN(&QHSM::S5_intersect); 
 	  	  return 0;
 	  }
 	  case SegGoals::UTURN: {
 		  QLOG(6,"U-turn requested, switch to U-turn state.");
		  Q_TRAN(&QHSM::S6_uturn); 
		  return 0;
 	  }
 	  case SegGoals::DRIVE_AROUND: {
 		  QLOG(6,"Just drive around, use offroad planning"); 	
		  Q_TRAN(&QHSM::S4_offroad); 
		  return 0;
 	  }
 	  case SegGoals::RESET: {
 		  QLOG(6,"RESET SIGNAL FROM mPLANNER, pause state");
		  QCONSOLE(" !! RESET FROM MPLANNER !!");
		  Q_TRAN(&QHSM::S1_pause); 
		  return 0;
 	  }
 	  case SegGoals::END_OF_MISSION: {
 		  QLOG(6,"End of mission, pause state");
          QCONSOLE("End of mission, WOHOO! (pause state)");
		  Q_TRAN(&QHSM::S1_pause); 
		  return 0;
 	  }
 	  case SegGoals::EMERGENCY_STOP: {
 		  QLOG(6,"EM-STOP SEGMENT! Switch to pause state");
 		  Q_TRAN(&QHSM::S1_pause);
 		  return 0;
 	  }
 	  case SegGoals::START_CHUTE: {
 		  QLOG(6,"Start Chute, zone mode...");
		  Q_TRAN(&QHSM::S3_zone); 
		  return 0;
 	  }
 	  case SegGoals::BACK_UP: {
 		  QLOG(6,"Reversing requested by mPlanner, swith to ???"); 	
 		  return 0; 	  										 	  
 	  }
 	  case SegGoals::UNKNOWN: {
 		  QLOG(6,"Unknown segment type, switch to pause state");
		  Q_TRAN(&QHSM::S1_pause); 
		  return 0;
 	  }
 	  default: {
 		  QLOG(6,"Mission planner supplied a strange road segment type!\n"); 	 		  
 	  }

 	}  
  	return (QSTATE)&QHSM::top;
}


// Default way to handle unrecognized signals
void QHSM::defaultStateError()
{
	QLOG(6,"Unrecognized signal, returning super state.");
}


// Copy state problems from  source to target
void QHSM::copyStateProblem(StateProblem_t* target, const StateProblem_t* source)
{
	QLOG(9,"\n[%s]: Got StateProblem_t target pointer: %p",__FUNCTION__,target);

	// Cath evil null pointers
	if (target==NULL || target == (void*)0xFFFFFFFF ) {
		QLOG(2,"\n%s: Nughty target! NULL pointer, aborting copy...",__FUNCTION__);
		return;
	}
	if ( source==NULL ) {
		QLOG(2,"\n%s: Source was null, using default road problem. \n",__FUNCTION__);
		// Let source be an preset "Road" StateEvent and recursively return
		copyStateProblem( target, S1_pause_problem );
		return;
	}

	// 'raw' copy state problem
	memcpy( target, source, sizeof(StateProblem_t) );

	return;
}


// Quick complete print of a StateProblem_t, only used for debugging
void QHSM::printStateProblem(const StateProblem_t* s)
{
	if (s==NULL) {
		QLOG(7,"STATE PROBLEM WAS NULL POINTER!\n");
	} else {
		QLOG(7,"state problem %p: { %s, %s, %s, %s, %s }\n", s,
				stateString[s->state], flagString[s->flag],
				regionString[s->region], plannerString[s->planner],
				obstacleString[s->obstacle] );
	}
}


/**
 * @brief 	Helping function 
 *			Derive the time difference between input time stamp and the current time
 * @return  (float) [seconds]
 */
float QHSM::since(uint64_t timestamp)
{
	if ( timestamp == 0 ) return 0;
	// Double precision is actually needed here (due to divide by 10^6)
	return (float)( ((double)(GET_TIME - timestamp)) / 1.0E6 );
}

/**
 * @brief 	Helping function 
 *			Derive the euclidian distance between input x/y-coordinate and the current position
 * @return  float [meters]
 */
float QHSM::distance(Logic_data_t* ld, point2 from)
{
	// Get current position
	point2 here = getPos(ld);
	// Calc 2-norm (Euclidian) of distances
	float diffx = here.x-from.x;
	float diffy = here.y-from.y;
	return sqrt( diffx*diffx + diffy*diffy );
}

/**
 * @brief 	Helping function 
 *			Derive the euclidian distance between input x/y-coordinate 1 and 2
 * @return  float [meters]
 */
float QHSM::distance(point2 one, point2 two)
{
	// Calc 2-norm (Euclidian) of distances
	float diffx = one.x-two.x;
	float diffy = one.y-two.y;
	return sqrt( diffx*diffx + diffy*diffy );
}


/**
 * @brief 	Helping function 
 *			Just a facede to AliceStateHelper to get current position
 * @return  point2 [x-y-z coords]
 */
point2 QHSM::getPos(Logic_data_t* ld) 
{	
	if (ld->vehState==NULL) return point2();	// A new x=y=0 point
	return AliceStateHelper::getPositionFrontBumper( *(ld->vehState) );
}
 

/******************************************************************
 * 						State funcions
 ******************************************************************/

/**
 * @brief 	Logic Level 0 State: S0_top 
 *			Absolute superstate. This only takes care of all rejected signals from higher states
 * @return  QSTATE
 */
QSTATE QHSM::S0_top(QEvent* e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_EMPTY_SIG: break;
	 case Q_ENTRY_SIG: PRINT_ENTRY; return 0;
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  PRINT_INIT; Q_INIT(&QHSM::S1_pause); return 0;
     case Q_NEW_SEGMENT_TYPE:  	return segment_type(ld->params);
     case Q_ESTOP: {
		 if ( CAME_FROM(&QHSM::S1_pause) ) {
			 // We are definately already in state pause
			 // Don't bother to switch state at all!
			 QLOG(7,"Already paused by eSTOP.");
			 return 0;
		 } else {
			 // We are not in pause, make transition!
			 QLOG(6,"eSTOP TRIGGED, pausing...");
			 QCONSOLE("\nGot an E-STOP. Pausing.");
	 		 Q_TRAN(&QHSM::S1_pause);
	 		 return 0;
		 }
     }
     case Q_NEW_GOAL:  {
    	 QLOG(6,"New goal, reset progress filter and save timestamp.");
    	 QLOG(7,"\tLast goal was given %f seconds ago", since( ld->last_goal) );
    	 ld->last_goal = GET_TIME;
    	 ld->resetFilter(ld->VEL_FILTER_RESET);
    	 if (ld->params->segment_type == SegGoals::END_OF_MISSION) {
    		 QLOG(2,"\n\nEND OF MISSION! WOHOO!!\n\n");
    		 QCONSOLE("\nEND OF MISSION! WOHOO!!");
    		 Q_TRAN(&QHSM::S1_pause);
    		 return 0;
    	 } else if (ld->params->segment_type == SegGoals::EMERGENCY_STOP) {
    		 QLOG(2,"\nEMERGENCY STOP GOAL!\n\n");
    		 QCONSOLE("\nEMERGENCY STOP GOAL!");
    		 Q_TRAN(&QHSM::S1_pause);
    		 return 0;    		 
    	 }
    	 
    	 return 0;
     }
     case Q_STOP_INTERSECTION: {
    	 // Intersection is very close, tell vel-planner to stop close to it!
    	 QLOG(6,"Intersection ahead!");
    	 Q_TRAN(&QHSM::S5_intersect); 
    	 return 0;
     }
     case Q_USER_SIG: {
    	 QLOG(2,"USER ERROR. Got 'USER_SIG' @ superstate, you have implemented a state the wrong way!");
    	 return segment_type(ld->params);;
     }
     default: QLOG(6,"Dunno what to do with this signal...");  // NO RETURN VALUE ALLOWED on default!
	}
	// This is the superstate: This (only this) state should return the top state (inherited from QHSM core)
    return (QSTATE)&QHSM::top; 
}


/**
 * @brief 	Logic Level 1 State: S1_pause 
 *			Alice is paused
 * @return  QSTATE
 */
QSTATE QHSM::S1_pause(QEvent* e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {	
		 PRINT_ENTRY; 
	 	 LVL1_ENTRY;
		 copyStateProblem( ld->problem, S1_pause_problem ); 
    	 return 0;
     }
     case Q_USER_SIG: {
    	 QLOG(6,"\t Alice has been paused for %f seconds", since( ld->last_lvl1_tran ) );
    	 return 0;
     }
     case Q_EXIT_SIG: {
		 ld->last_paused = GET_TIME;
    	 PRINT_EXIT; 
    	 return 0;
     }
     case Q_INIT_SIG:  NO_SUBSTATES;
     case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
	
    return (QSTATE)&QHSM::S0_top;
}


/**
 * @brief 	Logic Level 1 State: S2_road 
 *			Alice is driving on the road with a set of rules defined by level >=2 substates
 * @return  QSTATE
 */
QSTATE QHSM::S2_road(QEvent* e)
{
	PRINT_STATE(e); 
	switch (e->sig) {
	 case Q_ENTRY_SIG: { 
		PRINT_ENTRY; 
	 	LVL1_ENTRY;
	 	copyStateProblem( ld->problem, S2_road_problem ); 
    	return 0;
     } 
     case Q_INIT_SIG: {
    	 PRINT_INIT;
    	 QLOG(7,"On road, drive..."); 
    	 Q_INIT(&QHSM::S21_drive); 
    	 return 0;
     }
     case Q_EXIT_SIG: PRINT_EXIT; return 0;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM::S0_top;
}


/**
 * @brief 	Logic Level 1 State: S3_zone 
 *			Alice is driving on an open-zone area with a set of rules specified by level >=2 substates
 * @return  QSTATE
 */
QSTATE QHSM::S3_zone(QEvent* e) 
{
	PRINT_STATE(e); 
	switch (e->sig) {
	 case Q_ENTRY_SIG: { 
		PRINT_ENTRY; 
	 	LVL1_ENTRY;	
	 	copyStateProblem( ld->problem, S3_zone_problem); 
    	ld->last_zone_enter = GET_TIME;
	 	return 0;
     }
     case Q_EXIT_SIG: {
    	 PRINT_EXIT; 
    	 ld->last_zone_exit = GET_TIME;
    	 return 0;
     }
     case Q_INIT_SIG:  PRINT_INIT; Q_INIT(&QHSM::S31_safe); return 0;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM::S0_top;
}


/**
 * @brief 	Logic Level 1 State: S4_offroad 
 *			Alice is driving off-road.
 * @return  QSTATE
 */
QSTATE QHSM::S4_offroad(QEvent* e)
{
	PRINT_STATE(e); 
	switch (e->sig) {
	 case Q_ENTRY_SIG: {  
		PRINT_ENTRY; 
	 	LVL1_ENTRY;
	 	copyStateProblem( ld->problem, S4_offroad_problem); 
    	return 0;
     }
     case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 float duration = since( ld->last_lvl1_tran );
    	 
    	 if ( distance(ld, ld->last_lvl1_pos) > ld->OFFROAD_MAX_DISTANCE ) {
    		 // We have moved far enough (config parameter) from the point where we entered zone mode
    		 QLOG(6,"OFFROAD distance limit (%d meters). Switching to default segment handler",ld->OFFROAD_MAX_DISTANCE);
    		 QCONSOLE("OFFROAD distance limit. Switching to default segment handler");
    		 return segment_type(ld->params);	// Switch to missionplanner specified road segment    		 
    	 }
    	 
    	 
    	 if (duration > ld->OFFROAD_MAX_TIME) {
    		 // Been offroad too long now. Switch to default segment handler!
    		 QLOG(6,"OFFROAD time limit (%d). Switching to default segment handler",ld->OFFROAD_MAX_TIME);
    		 QCONSOLE("OFFROAD time limit. Switching to default segment handler");
    		 return segment_type(ld->params);	// Switch to missionplanner specified road segment
    	 }
    	 
    	 
    	 QLOG(6,"Alice is OFFROAD, since %f seconds", duration);
    	 return 0;
     }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM::S0_top;
}


/**
 * @brief 	Logic Level 1 State: S5_intersect 
 *			Alice is in an intersection with a set of rules defined by level >=2 substates
 * @return  QSTATE
 */
QSTATE QHSM::S5_intersect(QEvent* e)
{
	PRINT_STATE(e); 
	switch (e->sig) {
	 case Q_ENTRY_SIG: { 
		PRINT_ENTRY; 
	 	LVL1_ENTRY;
	 	copyStateProblem( ld->problem, S5_intersect_problem); 
		ld->last_intersection_enter = GET_TIME; // Save this timestamp
		
		// Reset intsHandler - load it with new map position
		QLOG(7,"Resetting intersectionHandler");
		ld->intHandler->reset(ld->graph, ld->map);
		
    	return 0;
     }
     case Q_EXIT_SIG: {
    	 PRINT_EXIT; 
    	 ld->last_intersection_exit = GET_TIME;
    	 
    	 // Reset intsHandler - clears drawings on map
		 QLOG(7,"Resetting intersectionHandler");
		 ld->intHandler->reset(ld->graph, ld->map);
		 
    	 return 0;
     }
     case Q_INIT_SIG:  {
    	 PRINT_INIT;
    	 // Intersection with stopline?
    	 if (ld->stoplineDistance == INFINITY ) {
    		 // No defined/found stopline
    		 QLOG(6,"Intersection without stopline, don't stop: merge!");
    		 Q_INIT(&QHSM::S53_stop_at_line); 
    		 return 0;
    		 
    	 } else {
    		 // There is an stopline, but...
    		 if ( intersectionJustVisited(ld) ) {
    			 /* This is a re-entry in an intersection we just visited;
    			  * Don't try to stop at the stopline since it's most likely blocked
    			  * by obstacles or we have already passed it
    			  * 
    			  * ToDo:
    			  * If we are here because of a jumping stopline (in mapper),
    			  * it's probably better to try to backup to the stopline rather than 
    			  * to just drive straight through the intersection
    			  */
    			 QLOG(6,"Re-visiting the same intersection, ignore stopline!");
	     		 Q_INIT(&QHSM::S51_intersect_go); 
	     		 return 0;
	     		     			 
    		 } else {
	     		// The stopline is new, stop at it.
				 QLOG(6,"New intersection with stopline, stop!");
	     		 Q_INIT(&QHSM::S53_stop_at_line); 
	     		 return 0;
    		 }
    		 
    	 }
     }
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM::S0_top;
}


/**
 * @brief 	Logic Level 1 State: S6_uturn 
 *			Alice is performing an U-turn with a set of rules defined by level >=2 substates
 * @return  QSTATE
 */
QSTATE QHSM::S6_uturn(QEvent* e)
{
	PRINT_STATE(e); 
	switch (e->sig) {
	 case Q_ENTRY_SIG: { 
		PRINT_ENTRY; 
	 	LVL1_ENTRY;
	 	copyStateProblem( ld->problem, S6_uturn_problem); 
    	return 0;
     }
     case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 float duration = since( ld->last_lvl1_tran);	// Time since entered S2 (seconds)
    	 
    	 // Velocity planner is supposed to signal when U-turn is done
    	 if (ld->prevErr & VP_UTURN_FINISHED) {
    		 // Switch state to... uhm... whatever mission planer recommends.
    		 QLOG(7,"vPlanner thinks the U-turn is done, swithing to default segment handler");
    		 return segment_type(ld->params);	// Switch to missionplanner specified road segment
    	 }
    	 
    	 // For some reason, if U-turn takes toooo long...
		 if (duration > ld->UTURN_MAX_TIME) {
    		 // Switch state to... uhm... whatever mission planer recommends.
    		 QLOG(7,"Time limit (%d s) for U-turn passed! Switching to default segment handler",ld->UTURN_MAX_TIME);
    		 return segment_type(ld->params);	// Switch to missionplanner specified road segment
		 }
    	 
    	 QLOG(7,"\tAlice is doing a U-turn since %f seconds ago",duration);
    	 return 0;
     }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM::S0_top;
}


/**
 * @brief 	Logic Level 1 State: S7_fail 
 *			Alice has failed with every attempt to make progress
 * @return  QSTATE
 */
QSTATE QHSM::S7_fail(QEvent* e)
{
	PRINT_STATE(e); 
	switch (e->sig) {
	 case Q_ENTRY_SIG: { 
		PRINT_ENTRY; 
	 	LVL1_ENTRY;
	 	copyStateProblem( ld->problem, S7_fail_problem);
	 	QCONSOLE("\nAlice has failed =(");
    	return 0;
     }
     case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 QLOG(2,"Alice has failed =(");
    	 return 0;
     }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM::S0_top;
}
	

// Import all supstates of this logic level. Yes, this is not a beautiful way to do it, but it works perfectly for small state machines
#include "QHSM2_road.cc"
#include "QHSM3_zone.cc"
#include "QHSM5_intersect.cc"


// EOF 'QHSM.cc'
