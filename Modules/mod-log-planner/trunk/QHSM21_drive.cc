/*!
 * \file QHSM21_drive.cc
 * \brief Quantum Hierarchical State Machine, Logic Level 3 - (Road)Drive
 *
 * \author Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */



/**
 * @brief 	Logic Level 3 State 
 *			Alice drives with passing allowed
 * @return  QSTATE
 */
QSTATE QHSM::S211_passing(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY;
		 LVL3_ENTRY;
		 // Change state problem rules
		 ld->problem->flag = PASS;
		 ld->resetFilter();	// Reset filter to have some headroom in time before a new strategy is selected
		 return 0;
	 }
     case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 
    	 if (ld->currentLane.lane != ld->desiredLane.lane) {
    		 // We're merging to the "other" lane
    		 QLOG(7,"\tMerging to the other lane (%d -> %d)",ld->desiredLane.lane, ld->currentLane.lane);
    		 QCONSOLE(" Merging  lane. ");
    	 }    	 
    	 
    	 if (ld->currentLane.lane > 1) {
    		 // In the 'other' lane!
    		 // Make sure we leave passign mode when the obstacle disappears!
    		 if ( (ld->obstacleInLane == INFINITY) &&
    			  ( distance(ld, ld->last_lvl3_pos) > ld->NOMINAL_SAFETYBUFFER)		// ToDo: Change this config parameter some time later
    			 ) {
    			 /*  a) Obstacle gone! We probably passed it 
    			  *  b) We tried to pass it for at least some few (config parameter) meters
    			  *  Switch bak to driving in the right/correct lane
    			  */
    			 QLOG(6," Passed the obstacle, switch back to right lane!");
    			 Q_TRAN(&QHSM::S212_nopassing); 
    			 return 0;
    		 }
    		 QLOG(7,"Using the other lane\t");
    	 }
    	 
    	 
	   	 // Passing mode not successful?
       	 // If filtered velocity is under [config parameter], Alice is considered stopped.
    	 if ( ld->filteredVelocity < ld->DRIVE_VEL_REQUIREMENT ) {
    		 QLOG(6,"Progress is too low (v < %f), examine environment",ld->DRIVE_VEL_REQUIREMENT);
    		 Q_TRAN(&QHSM::S24_examine); 
    		 return 0;
    	 }

    	 // Default printout
    	 QLOG(7,"Alice is driving with passing allowed...");
    	 return 0;
     }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
	 case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM::S21_drive;
}



/**
 * @brief 	Logic Level 3 State 
 *			Alice is driving, passing is NOT allowed
 * @return  QSTATE
 */
QSTATE QHSM::S212_nopassing(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY; 
		 LVL3_ENTRY;
		 
		 ld->problem->flag = NO_PASS;
		 ld->resetFilter();	// Reset filter to have some headroom in time before a new strategy is selected
		 return 0;
	 }
	 case Q_USER_SIG: { 
		 // Do 'ordinary' stuff

		 // Try this, cool?
		 // If level1 duration is big, and level2 duration small, we must have stopped and/or backed up already
		 // Alt. take: If difference between these times are substantial, we have a potential cycle
		 int diff = (ld->last_lvl2_tran - ld->last_lvl1_tran)/(int)(1E6); 
		 if ( diff > 2 ) {
			 QLOG(8,"lvl1-lvl2 difference is %d seconds. ", diff);
			 QLOG(6,"We're back in this mode from stopobs, reverse or examine. \t");

			 /* We entered this state from another level 2 state... possible cycle!
			  * Behave different!
			  * 
			  * We are most likely in this scenario:
			  * Alice stopped for obstacle, waited, backed up, and switched into drive again (here).
			  * If sensing modules have not changed their mind about the obstacle (i.e. no spurious obstacles),
			  * it's probably better to drive around it in some other way, 
			  * which implies a replan request through mission planner. 
			  */

			 /* First, check if path planner still think it's a bad idea to drive forward.
			  * If no errors show up, the obstacle is probably gone.  
			  */ 

			 /*  NOT VERY SUCCESSFUL STRATEGIES @ field test. We have to at least low pass path planner error codes if this is going work
			 if ( ld->prevErr & PP_COLLISION_SAFETY ) {
				 // Obvious blocked path!
				 if (ld->params->seg_goal.illegalPassingAllowed) {
					 // Pasing is allowed according to mPlanner, switch to passing mode
					 QLOG(7,"\nLane is blocked, passing allowed, switching to passing!");	 
					 Q_TRAN(&S211_passing);
					 
				 } else {
					 // Lane blocked, not allowed to pass... but... 
					 if ( since(ld->last_goal) < ld->GOAL_MIN_TIME) {
						 // ...have we even tried to get closer the object?
						 // We have to try to tackle this problem for a little while to not drive the mission planner crazy
						 QLOG(7,"\nLane blocked, but I have to try to explore it; agressive mode.");
						 ld->problem->obstacle = OBSTACLE_AGGRESSIVE;
						 
					 } else {
						 // ...ok, ask mission planner for a replan (failure error)
						 QLOG(6,"\nLane blocked, not allowed to pass; mission failure!");
						 ld->newErr |= LP_FAIL_MPLANNER_LANE_BLOCKED;
					 }
					 
				 }
				 return 0;
				 
			 }
			 */
			 
		 } else if ( (ld->currentVelocity > 4.0) &&   // 4 m/s ~ 9 mph
				 	 (ld->obstacleOnPath < 1.2*ld->ROAD_OBS_DISTANCE) &&
				 	 (ld->last_goal > (uint64_t)ld->GOAL_MIN_TIME)
				 	 ) {
			 /*  a) We are driving with good flow down the road
			  *  b) An obstacle is in our way in our desired path (=lane?)
			  *  c) We did not just ask for a new goal from mPlanner
			  *  Ask mission planner if we could be allowed to pass it, without stopping first! 
			  */  
			 QCONSOLE("\nDriving fast, obstacle in our lane, want to pass it with high speed: mPlan:'lane blocked'");
			 QLOG(6,"I want to overtake obstacle with high speed; mPlanner error: 'lane blocked'");
			 ld->newErr |= LP_FAIL_MPLANNER_LANE_BLOCKED;
			 return 0;
			 
		 } else if ( ld->obstacleOnPath < ld->ROAD_OBS_DISTANCE ) {
			 // Is there a new obstacle on our path, too close?
			 QCONSOLE("\nObstacle in our path, stopping!");
			 QLOG(6,"Obstacle in our path, stopping!\t");
			 // We're not allowed to pass. 
			 // Pause and see if obstacle disappear
			 Q_TRAN(&QHSM::S22_stop); 
			 return 0;
		 }
		 
		 
		 // Timeout; low progress
    	 if ( ld->filteredVelocity < ld->DRIVE_VEL_REQUIREMENT ) {
    	   	 /*  a) Low passed velocity is under [parameter], Alice is considered stopped.
    	   	  *  --> Mode 'nopassing' not successful to make progress? Try mode 'passing' if it's allowed
           	  */
    		 QLOG(6,"Progress is too low (v < %f), examine environment",ld->DRIVE_VEL_REQUIREMENT);
    		 // Are we allowed to pass according to mPlanner?
    		 if (ld->params->seg_goal.illegalPassingAllowed) {
	    		 QLOG(6,", switching to pass mode (allowed)");
	    		 Q_TRAN(&QHSM::S211_passing); 
	    		 return 0;
	    		 
	    	 } else {
	      		 // Switch to examine mode
    			 QLOG(7,", switching to examine mode (passing not allowed)");
    			 Q_TRAN(&QHSM::S24_examine);
    			 return 0;
	    	 }
    	 }
		 
    	 // Default printout
		 QLOG(7,"Alice is driving, but is not allowed to pass");
		 return 0;	
	 }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM::S21_drive;
}



/**
 * @brief 	Logic Level 3 State 
 *			Alice is driving and is allowed to both pass and reverse
 * @return  QSTATE
 */
QSTATE QHSM::S213_passingreverse(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY;  
		 LVL3_ENTRY;
		 
		 ld->problem->flag = PASS_REV;
		 ld->resetFilter(3.0);	// Reset filter to have some headroom in time before a new strategy is selected
		 return 0;
	 }
	 case Q_USER_SIG: { 
	     // Do 'ordinary' stuff
		 
		 /*
		 // We transitioned here from astern  
		 if ( CAME_FROM(&S23_astern) ) {
			 QLOG(7,"\nEnded up here from astern!");

			 // Path planner can still not find a legal path
			 if ( ld->prevErr & PP_NOPATH_LEN ) {
				 if ( since(ld->last_goal) > ld->GOAL_MIN_TIME) {
					 // Ask mission planner for a replan (failure error)
					 QLOG(6,"\nThis path is not applicable, mission failure: road blocked!");
					 QCONSOLE("\nMission failure: Road blocked!");
					 ld->newErr |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
					 return 0;
					 
				 } else {
					 // We have to try on a new goal long enough
					 QLOG(6,"\nThis path is not applicable, but I have to try some more; bare mode!");
					 ld->problem->obstacle = OBSTACLE_BARE;
					 return 0;
				 }
			     
			 }
		 }
		 */
	 
    	 if (ld->currentLane.lane != ld->desiredLane.lane) {
    		 // We're merging to the "other" lane
    		 QLOG(7,"Merging to the other lane (%d -> %d)\t",ld->desiredLane.lane, ld->currentLane.lane);
    	 } 
    	 
    	 if (ld->currentLane.lane > 1) {
    		 // In the 'other' lane!
    		 QLOG(7,"Passing an obstacle!?");
    		 // Make sure we leave passign mode when the obstacle disappears!
    		 if (ld->obstacleInLane == INFINITY) {
    			 // Obstacle gone! Switch bak to driving in the right/correct lane
    			 QLOG(7," It's done, switch back to right lane");
    			 Q_TRAN(&QHSM::S212_nopassing); 
    			 return 0;
    		 }
    	 }
    	 
    	 
	   	 // Passingreverse not successful?
       	 // If low passed velocity is under ~0.2 m/s, Alice is considered stopped.
    	 if ( ld->filteredVelocity < ld->DRIVE_VEL_REQUIREMENT ) {
    		 QLOG(7,"Progress is too low, use examine mode. ");
    		 Q_TRAN(&QHSM::S24_examine);
    		 return 0;

    		 /*
    		 // We just visited astern mode, so tell mPlaner we've failed to get around this obstacle
	    	 if ( since(ld->last_astern) > (2*ld->ASTERN_MAX_TIME) ) {
	    		 QLOG(6,"\nThis path is not applicable, mission failure: 'road blocked'");
	    		 QCONSOLE("\nPath is not applicable, mission failure: 'road blocked'");
	    		 // Set error message (to mPLanner?)
	    		 ld->newErr |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
	    		 return 0;
	    		 
	    	 } else {
	    		 QLOG(6,", trying to back up...");
	    		 Q_TRAN(&S23_astern); 
	    		 return 0;
	    	 }
	    	 */
    	 }
    	 
    	 
    	 // Tried this too many times? 
    	 // If we have been stuck in 2_road states too long, switch to offroad!
    	 if ( since(ld->last_lvl1_tran) > ld->ROAD_TO_OFFROAD_TIME ) {  	// Compare time since entered S2* (seconds)
    		 QLOG(6,"State 'road' has not made enough progress for %d seconds, use OFFROAD!",ld->ROAD_TO_OFFROAD_TIME);
    		 QCONSOLE("\nLow progress, using OFFROAD!");
    		 Q_TRAN(&QHSM::S4_offroad); 
    		 return 0;
    	 }
    	 
    	 // Print what's happening if no special events occored
    	 QLOG(7,"Alice is driving with passing reverse allowed");
    	 return 0;
	 }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM::S21_drive;
}




// EOF 'QHSM2_road.cc'
