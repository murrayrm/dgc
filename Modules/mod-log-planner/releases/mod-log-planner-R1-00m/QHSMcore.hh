/**********************************
 * Header for QHSMcore.cc
 * Author: Miro Samek, modified by Stefan Skoog
 * Date:   September 2008
 * 
 **********************************/

#ifndef QHSMcore_hh
#define QHSMcore_hh

/*********************************
 * Includes
 *********************************/

#include "Qassert.hh"
#include "QMacros.hh"
#include "LogicData.hh"	// Inport event definition
#include <temp-planner-interfaces/Console.hh>
#include <temp-planner-interfaces/Utils.hh>
#include <temp-planner-interfaces/Log.hh>


/*********************************
* MACROS
**********************************/
// Have a look in QMacros.hh


/*********************************
 * Quantum Hierarchical State Machine class define
 *********************************/
class QHSMcore {           

public:
   
	/* 	Define object QPseudoState as a function member of QHSMcore 
	   	with QEvent as argument and void return data */ 
	typedef void (QHSMcore::*QPseudoState)(QEvent*); 
	
	/* 	Define object QState as a function member of QHSMcore 
		with QEvent as argument and QPseudoState return data */
	typedef QPseudoState (QHSMcore::*QState)(QEvent*);

	// Virtual function to refer to initial stata member function pointer
	// This must be overridden by any inheriting classes
	//virtual void initial(QEvent const *e) =0;
	
	void init(QEvent* e = &pkgStdQEvent[0] );		// Execute initial transition
	void dispatch(QEvent* e);  	// Dispatch event
	void record(QPseudoState initial);
	int isIn(QState state);	    // Test if argument state is the current executing state
	int cameFrom(QState state);	// Test if argument state was the last executed state before the current
	char const *getVersion();	// Return framework version
	
	// Assertion error handling. Needed by Qassert.hh to safe debugging
	void onAssert__(char const *file, unsigned line); 
   
protected:
	struct Tran {            		// Protected inner class Tran
      QState myChain[10];			// This one defines the maximum number of jumps in one transition. It's now at least 8! 
      unsigned short myActions; 	// Action mask (2-bits for action)
	};
	
	/* 	Constructor 
	  	Needs a member function (of QHSMcore) pointer to initial state (refer to typedef above) */
	QHSMcore(QPseudoState initial);   	
	
	// Virtual destructor
	virtual ~QHSMcore();				
	
	// The "top" state (pointer to function member)
	QPseudoState top(QEvent* ) { return 0; } 
	// The current state (pointer to function member)
   	QState getState() const { return myState; }
   	
   	// Dynamic state transition
   	void tran(QState target);              
   	// Static state transition
   	void tranStat(Tran *t, QState target);  
   	// 
   	void init_(QState target) { myState = target; }
   	#define Q_INIT(target_) init_(Q_STATIC_CAST(QState, target_))

   	#define Q_TRAN(target_) if (1) { \
      static Tran t_; \
      tranStat(&t_, Q_STATIC_CAST(QState, target_));\
   	} else ((void)0)
   	
   	#define Q_TRAN_DYN(target_) tran(Q_STATIC_CAST(QState, target_))
   	// Magic (it is, I promise!) macro to fool compiler that calls to cameFrom is legal outise this class
	#define CAME_FROM(target_) ( cameFrom( Q_STATIC_CAST(QState, target_) ) )
   	
private:
	void tranSetup(Tran *t, QState target);

private:
	QState myState;		// The current active state (QHSMcore member function pointer)
	QState myLastState;	// The last state we visited (history)
	QState mySource;	// Source state during a transition (QHSMcore member function pointer)

protected:

};

// QSTATE is another name for QPseudoState-functions; member function pointers within QHSMcore
typedef QHSMcore::QPseudoState QSTATE;       

// helper macro to calculate static dimension of a 1-dim array
#define DIM(array_) (sizeof(array_)/sizeof(*(array_)))

// Helper macro to present one of the reserved signals to a given statehandler
#define TRIGGER(state_,sig_) \
	Q_STATE_CAST((this->*(state_))(&pkgStdQEvent[sig_]))
// Compiler-specific cast
#define Q_STATE_CAST(x_)      reinterpret_cast<QState>(x_)
// Platform-specific cast
#define Q_STATIC_CAST(type_, expr_) static_cast<type_>(expr_)
#define Q_UGLY_STATE_CAST(QState_) (*reinterpret_cast<void**>(&QState_))
#endif
// EOF 'QHSMcore.hh'
