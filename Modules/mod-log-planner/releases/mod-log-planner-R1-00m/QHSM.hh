/*!
 * \file QHSM.hh
 * \brief Quantum Hierarchical State Machine Level 1
 *
 * \author Miro Samek, modified by Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */


#ifndef QHSM_hh
#define QHSM_hh


/*********************************
 * Includes
 *********************************/
//#include <temp-planner-interfaces/ConfigFile.hh>
//#include <temp-planner-interfaces/Console.hh>
#include <temp-planner-interfaces/Log.hh>
#include <temp-planner-interfaces/Utils.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <temp-planner-interfaces/AliceStateHelper.hh>
//#include <dgcutils/DGCutils.hh>	// Uses pause/sleep from here
#include "IntersectionHandling.hh"
#include "Qassert.hh"
#include "QMacros.hh"
#include "LogicData.hh"
#include "QHSMcore.hh"
#include <stdlib.h>
#include <stdio.h>


/*********************************
* MACROS
**********************************/
// Have a look in QMacros.hh




/*********************************
 * Class define
 *********************************/
class QHSM:public QHSMcore {
public:
	/* 	Constructor, takes no arguments, but calls base class 
	 	with member function pointer to initial state.
	 	Two different constructors are more convenient, one for creating class and one for inheriting */ 
	QHSM(Logic_data_t& logicData); 
	QHSM(QPseudoState initial);
	virtual ~QHSM() { QHSM::destroy(); }
	
	// Send initializer to QHSM core
	void record(QPseudoState initial) { QHSMcore::record(initial);}
	
	void printStateProblem(const StateProblem_t* s);
	void copyStateProblem(StateProblem_t* target, const StateProblem_t* source);
	float since(uint64_t timestamp);
	float distance(Logic_data_t* ld, point2 from);
	float distance(point2 one, point2 two);

	
protected:
	void destroy(); // Alternative destructor 
		
	void initial(QEvent const *e); 	// Initial (startup) state
		// The list of different available level 0/1-states
		QSTATE S0_top(QEvent *e);		// Superstate
		  QSTATE S1_pause(QEvent *e);	
		  QSTATE S2_road(QEvent *e);	
		     // Import definitions of all supstates of this logic level. 
		     // Yes, this is not a beautiful way to do it, but it works perfectly for small state machines
		     #include "QHSM2_road.hh"
			   #include "QHSM21_drive.hh"
		  QSTATE S3_zone(QEvent *e);	
			 #include "QHSM3_zone.hh" 
		  QSTATE S4_offroad(QEvent *e);	
		  QSTATE S5_intersect(QEvent *e);
             #include "QHSM5_intersect.hh"
		  QSTATE S6_uturn(QEvent *e);				
		  QSTATE S7_fail(QEvent *e);		


	// Helping methods
	void defaultStateError(void);
	point2 getPos(Logic_data_t* ld);
	QSTATE segment_type(Logic_params_t const *p);

public:
	// Pointer to data structure that contains all we need
	Logic_data_t* ld;
	bool intersectionJustVisited(Logic_data_t* ld);
	
protected:	
	// Define provisional state problems for each level 1 state
	StateProblem_t* S1_pause_problem;
	StateProblem_t* S2_road_problem;
    StateProblem_t* S3_zone_problem;
    StateProblem_t* S4_offroad_problem;
    StateProblem_t* S5_intersect_problem;
    StateProblem_t* S6_uturn_problem;
    StateProblem_t* S7_fail_problem;

		  
private:           
	

};





#endif
// EOF 'QHSM.hh'
