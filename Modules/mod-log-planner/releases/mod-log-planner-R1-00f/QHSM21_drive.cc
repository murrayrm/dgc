/*!
 * \file QHSM21_drive.cc
 * \brief Quantum Hierarchical State Machine Level 3 - Road-Drive
 *
 * \author Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */


/**
 * @brief 	Logic Level 3 State 
 *			Alice drives with passing allowed
 * @return  QSTATE
 */
QSTATE QHSM1::S211_passing(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 qprint("%s-ENTRY;",__FUNCTION__);
		 // Change state problem rules
		 ld->problem->flag = PASS;
		 ld->resetFilter();	// Reset filter to have some headroom in time before a new strategy is selected
		 return 0;
	 }
     case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 qprint("\tAlice is driving with passing allowed...");
    	 
    	 // Passing not successful? Try passing reverse...
       	 // If low passed velocity is under ~0.2 m/s, Alice is considered stopped.
    	 if ( ld->filteredVelocity < ld->DRIVE_VEL_REQUIREMENT ) {
    		 Q_TRAN(&QHSM1::S213_passingreverse); return 0;
    	 }

    	 
    	 return 0;
     }
     case Q_EXIT_SIG:  qprint("%s-EXIT;\n", __FUNCTION__); return 0;
	 case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S21_drive;
}


/**
 * @brief 	Logic Level 3 State 
 *			Alice is driving, passing is NOT allowed
 * @return  QSTATE
 */
QSTATE QHSM1::S212_nopassing(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 qprint("%s-ENTRY;",__FUNCTION__); 
		 
		 ld->problem->flag = NO_PASS;
		 ld->resetFilter();	// Reset filter to have some headroom in time before a new strategy is selected
		 return 0;
	 }
	 case Q_USER_SIG: { 
		 // Do 'ordinary' stuff
		 
		 qprint("\tAlice is driving, but is not allowed to pass");

	   	 // No passing not successful? Try passing...
       	 // If low passed velocity is under ~0.2 m/s, Alice is considered stopped.
    	 if ( ld->filteredVelocity < ld->DRIVE_VEL_REQUIREMENT ) {
    		 Q_TRAN(&QHSM1::S211_passing); return 0;
    	 }
		 
		 return 0;	
	 }
     case Q_EXIT_SIG:  qprint("%s-EXIT;\n", __FUNCTION__); return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S21_drive;
}


/**
 * @brief 	Logic Level 3 State 
 *			Alice is driving and is allowed to both pass and reverse
 * @return  QSTATE
 */
QSTATE QHSM1::S213_passingreverse(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 qprint("%s-ENTRY;",__FUNCTION__);  
		 
		 ld->problem->flag = PASS_REV;
		 ld->resetFilter();	// Reset filter to have some headroom in time before a new strategy is selected
		 return 0;
	 }
	 case Q_USER_SIG: { 
	    	 // Do 'ordinary' stuff
	    	 qprint("\tAlice is driving with passing reverse allowed");
	    	 
		   	 // Passingreverse not successful? Try force astern!
	       	 // If low passed velocity is under ~0.2 m/s, Alice is considered stopped.
	    	 if ( ld->filteredVelocity < ld->DRIVE_VEL_REQUIREMENT ) {
	    		 Q_TRAN(&QHSM1::S23_astern); return 0;
	    	 }
	    	 
	    	 return 0;
	 }
     case Q_EXIT_SIG:  qprint("%s-EXIT;\n", __FUNCTION__); return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S21_drive;
}




// EOF 'QHSM2_road.cc'
