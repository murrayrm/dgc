/*!
 * \file QHSM3_zone.cc
 * \brief Quantum Hierarchical State Machine Level 2 - Zone
 *
 * \author Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */



/**
 * @brief 	Logic Level 2 State 
 *			Alice is safe with high buffer
 * @return  QSTATE
 */
QSTATE QHSM1::S31_safe(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY;
		 LVL2_ENTRY;
		 ld->problem->obstacle = OBSTACLE_SAFETY;

		 return 0;
	 }
     case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 double duration = since( (double)ld->last_zone);
    	 qprint("\tAlice is driving in zone, safe, since %f seconds ago",duration);
   	 
    	 // Are we driving fast enough (any direction)?
    	 if ( ld->filteredVelocity < ld->ZONE_VEL_REQUIREMENT ) {
     		 Q_TRAN(&QHSM1::S32_agressive); return 0;
     	 }
    	 
    	 return 0;
     }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S3_zone;
}


/**
 * @brief 	Logic Level 2 State 
 *			Alice is agressive with small safety buffer
 * @return  QSTATE
 */
QSTATE QHSM1::S32_agressive(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY; 
		 LVL2_ENTRY;
		 ld->problem->obstacle = OBSTACLE_AGGRESSIVE;
		 ld->last_agressive = Utils::getTime();
		 ld->resetFilter();	
		 return 0;
	 }
	 case Q_USER_SIG: { 
		 // Do 'ordinary' stuff
		 double duration = since( (double)ld->last_zone);
    	 qprint("\tAlice is driving AGRESSIVLY in zone since %f seconds ago",duration);
		 
		 // No progress either?
    	 // If tried for one minute AND no (filtered) velocity, switch to bare
		 if ( duration > 60 &&
			  ld->filteredVelocity < ld->ZONE_VEL_REQUIREMENT) {
			 // Switch to BARE mode
			 Q_TRAN(&QHSM1::S33_bare); return 0;
		 }
	
	    	 
		 return 0;	
	 }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S3_zone;
}


/**
 * @brief 	Logic Level 2 State 
 *			Alice is crazy and has nearly NO SAFETYBUFFER
 * @return  QSTATE
 */
QSTATE QHSM1::S33_bare(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY; 
		 LVL2_ENTRY;
		 ld->problem->obstacle = OBSTACLE_BARE;
		 ld->last_bare = Utils::getTime();
		 ld->resetFilter();	
		 return 0;
	 }
	 case Q_USER_SIG: { 	    	 
		 // Measure for how long we have been backing up. IN SECONDS (Don't change this to milliseconds!)
	 	 double duration = since( (double)ld->last_zone);
		 qprint("\tAlice is driving BARE in zone since %f seconds ago",duration);
		 
		 // No progress either?
		 // If tried for two minutes AND no (filtered) velocity, switch to offroad!
		 if ( duration > 120 &&
			  ld->filteredVelocity < ld->ZONE_VEL_REQUIREMENT) {
			 // Switch to OFFROAD mode!
			 Q_TRAN(&QHSM1::S4_offroad); return 0;
		 }
		 
		 return 0;
	 }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S3_zone;
}



// EOF 'QHSM3_zone.cc'
