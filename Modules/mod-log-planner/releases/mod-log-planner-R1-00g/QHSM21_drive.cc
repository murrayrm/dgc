/*!
 * \file QHSM21_drive.cc
 * \brief Quantum Hierarchical State Machine Level 3 - Road-Drive
 *
 * \author Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */


/**
 * @brief 	Logic Level 3 State 
 *			Alice drives with passing allowed
 * @return  QSTATE
 */
QSTATE QHSM1::S211_passing(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY;
		 LVL3_ENTRY;
		 // Change state problem rules
		 ld->problem->flag = PASS;
		 ld->resetFilter();	// Reset filter to have some headroom in time before a new strategy is selected
		 return 0;
	 }
     case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 qprint("\tAlice is driving with passing allowed...");
    	 
	   	 // Passing not successful?
       	 // If filtered velocity is under ~0.2 m/s, Alice is considered stopped.
    	 if ( ld->filteredVelocity < ld->DRIVE_VEL_REQUIREMENT ) {
    		 qprint("Progress is too low, trying to back up...");
    		 Q_TRAN(&QHSM1::S23_astern); return 0;
    	 }

    	 
    	 return 0;
     }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
	 case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S21_drive;
}


/**
 * @brief 	Logic Level 3 State 
 *			Alice is driving, passing is NOT allowed
 * @return  QSTATE
 */
QSTATE QHSM1::S212_nopassing(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY; 
		 LVL3_ENTRY;
		 
		 ld->problem->flag = NO_PASS;
		 ld->resetFilter();	// Reset filter to have some headroom in time before a new strategy is selected
		 return 0;
	 }
	 case Q_USER_SIG: { 
		 // Do 'ordinary' stuff
		 
		 qprint("\tAlice is driving, but is not allowed to pass");

	   	 // No passing not successful? Try passing if it's allowed
       	 // If low passed velocity is under ~0.2 m/s, Alice is considered stopped.
    	 if ( ld->filteredVelocity < ld->DRIVE_VEL_REQUIREMENT ) {
    		 qprint("Progress is to low");
    		 // Are we allowed to pass according to mPlanner?
    		 if (ld->params->seg_goal.illegalPassingAllowed) {
	    		 qprint(", switching to pass mode");
	    		 Q_TRAN(&QHSM1::S211_passing); return 0;	    		 
	    	 } else {
	    		 qprint(", not allowed to pass, sending failed to mPlanner.");
	      		 // Set error message to mPlanner
	       		 ld->newErr |= LP_FAIL_MPLANNER_ROAD_BLOCKED;	
	    	 }
    	 }
		 
		 return 0;	
	 }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S21_drive;
}


/**
 * @brief 	Logic Level 3 State 
 *			Alice is driving and is allowed to both pass and reverse
 * @return  QSTATE
 */
QSTATE QHSM1::S213_passingreverse(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY;  
		 LVL3_ENTRY;
		 
		 ld->problem->flag = PASS_REV;
		 ld->resetFilter(3.0);	// Reset filter to have some headroom in time before a new strategy is selected
		 return 0;
	 }
	 case Q_USER_SIG: { 
	    	 // Do 'ordinary' stuff
	    	 qprint("\tAlice is driving with passing reverse allowed");
	    	 
		   	 // Passingreverse not successful?
	       	 // If low passed velocity is under ~0.2 m/s, Alice is considered stopped.
	    	 if ( ld->filteredVelocity < ld->DRIVE_VEL_REQUIREMENT ) {
	    		 qprint("Progress is too low");

	    		 // We just visited astern mode, so tell mPlaner we've failed to get around this obstacle
		    	 if ( since(ld->last_astern) > (2*ld->ASTERN_MAX_TIME) ) {
		    		 qprint(", sending failed to mPlanner.");
		    		 // Set error message (to mPLanner?)
		    		 ld->newErr |= LP_FAIL_MPLANNER_ROAD_BLOCKED;	
		    	 } else {
		    		 qprint(", trying to back up...");
		    		 Q_TRAN(&QHSM1::S23_astern); return 0;
		    	 }
	    	 }
	    	 
	    	 
	    	 // Tried this too many times? 
	    	 // If we have been stuck in 2_road states too long, switch to offroad!
	    	 if ( since(ld->last_lvl1_tran) > ld->ROAD_TO_OFFROAD_TIME ) {  	// Compare time since entered S2 (seconds)
	    		 qprint("State 'road' has not made enough progress for %d seconds, use OFFROAD!",ld->ROAD_TO_OFFROAD_TIME);
	    		 Q_TRAN(&QHSM1::S4_offroad); return 0;
	    	 }
	    	 return 0;
	 }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S21_drive;
}




// EOF 'QHSM2_road.cc'
