/*!
 * \file QHSMtest.hh
 * \brief Quantum Hierarchical State Machine test example
 *
 * \author Miro Samek, modified by Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */
#ifndef QHSMtest_hh
#define QHSMtest_hh


/*********************************
 * Includes
 *********************************/
//#include <temp-planner-interfaces/ConfigFile.hh>
#include <temp-planner-interfaces/Console.hh>
#include <temp-planner-interfaces/Log.hh>
#include <dgcutils/DGCutils.hh>	// Uses pause/sleep from here

#include <stdlib.h>
#include <stdio.h>

#include "QHSM.hh"
#include "QEvent.hh"
#include "Qassert.hh"

/*********************************
 * Macros
 *********************************/
#define PRINT_INFO	 	cout <<"Function name: \"" <<__FUNCTION__ << "\"" << endl;
#define PRINT_INFO_EXT 	cout <<"Time: "<<__TIME__<<" "<<__DATE__<<" in file "<<__FILE__<<" in line "<<__LINE__<<" in function \""<< __PRETTY_FUNCTION__ <<"\""<<endl;

	
/*********************************
* Initiate class variables
**********************************/
enum QHSMtestSignals {
   A_SIG = Q_USER_SIG, B_SIG, C_SIG, 
   D_SIG, E_SIG, F_SIG, G_SIG, H_SIG
};

static const QEvent testQEvt[] = { 
   {A_SIG}, {B_SIG}, {C_SIG}, {D_SIG}, 
   {E_SIG}, {F_SIG}, {G_SIG}, {H_SIG}
};


/*********************************
 * Class define
 *********************************/
class QHSMtest : public QHSM {
public:
	/* 	Constructor, takes no arguments, but calls base class 
	 	with member function pointer to initial state */ 
	QHSMtest(): QHSM( (QPseudoState)&QHSMtest::initial ) {}	
	int getFoo();
	
protected:

	void initial(QEvent const *e);
		QSTATE s0(QEvent const *e);			// State-handler...                  
	      QSTATE s1(QEvent const *e);                   
	        QSTATE s11(QEvent const *e);               
	      QSTATE s2(QEvent const *e);                    
	        QSTATE s21(QEvent const *e);                
	          QSTATE s211(QEvent const *e);            

	
private:                               
	// Extended state variables goes here
	int myFoo;
};


// Other functions
int main(); 





#endif
// EOF 'QHSMtest.hh'
