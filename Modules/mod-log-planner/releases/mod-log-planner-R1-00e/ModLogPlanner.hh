/**********************************
 * Header for ModLogPlanner.cc
 * Author: Stefan Skoog
 * Date: Created June 2008
 * 
 **********************************/

#ifndef MOD_LOG_PLANNER_HH_
#define MOD_LOG_PLANNER_HH_

/** @file

@brief Perform modular logic planning and generate state problem for path- & velocity-planners to solve. 

*/


 /********************************
 * Includes
 *********************************/
#include <temp-planner-interfaces/AliceStateHelper.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <temp-planner-interfaces/ConfigFile.hh>
#include <temp-planner-interfaces/Console.hh>
#include <temp-planner-interfaces/Utils.hh>
#include <temp-planner-interfaces/Log.hh>
#include <interfaces/ActuatorState.h>	// Em-stop signal
#include <dgcutils/cfgfile.h>
#include "QMacros.hh"
#include "QHSM1.hh"		// THE state machine


 /********************************
 * Class definitions
 *********************************/

/// @brief Main class handling the logics
class ModLogPlanner 
{
	
  public:

	/*! Constructor */
	ModLogPlanner();

	/*! Destructor */
	~ModLogPlanner();
	
	/*! Inittialization, just for backward compatibleness */	
	int init();

	/*! Alternative destructor, just for backward compatibleness */
	void destroy();
	
	/*! Read input config file and store in class variables  */
	int readConfig();
	
	/*! Do the actual logic planning!  */
	Err_t planLogic(vector<StateProblem_t*> &problems, PlanGraph *graph, Err_t prevErr, 
			 VehicleState &vehState, Map *map, 
			 Logic_params_t &params, int& currentSegmentId, int& currentLaneId,
			 bool& replanInZone);	
	
	void resetState(void);
	void updateIntersectionHandlingConsole(void);
	
	void printLogicData(Logic_data_t& ld);
	
  protected:
	  // Just call som eternal functions to update important information in LogicData structure
	int updateLogicData(Logic_data_t& ld);
    void velLowPass(double& filterVel, double newVel);
	  

    // - - - - - - - - VARIABLES - - - - - - - - - //
  public:
	// An event to feed to state machine
	QEvent qe;
	// All data passed to ModLogPlanner
	Logic_data_t logicData;
	
  protected:
    // Config parameters
	double NOMINAL_SAFETYBUFFER;
	uint64_t MAX_YIELD_TIME;
	double DESIRED_DECELERATION;
	  
	  
  private:
	// Finite State Machine current status, specified in temp-planner-interfaces/PlannerInterfaces.h
	// The struct name contains 'problem', but this time it's not used for a problem representation
	StateProblem_t current_status;
	SegGoals::SegmentType last_seg_type;
	
	// Reference to the state machine framework
	QHSM1 *qhsm;

	
	// Intersection Handler
	//CIntersectionHandling* IntersectionHandling;
	
	// This one is only needed for development, remove it when things start to work
	long cycle_counter;

};


#endif /*MOD_LOG_PLANNER_HH_*/
// EOF 'ModLogPlanner.hh'
