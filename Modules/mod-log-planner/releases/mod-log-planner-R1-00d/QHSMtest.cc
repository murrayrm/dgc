/*!
 * \file QHSMtest.cc
 * \brief Quantum Hierarchical State Machine test example
 *
 * \author Miro Samek, modified by Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */


/*********************************
 * Include header 
 *********************************/
#include "QHSMtest.hh"
DEFINE_THIS_FILE; // Assertion debug helping trick (see Qassert)


/*********************************
 * Class functions 
 *********************************/
/*
QHSMtest::QHSMtest() : QHSM( (QPseudoState)(initial) ) 
	// Nothing. Super class does the job.
}*/

// Return inner state
int QHSMtest::getFoo()
{
	return myFoo;
}


// Initial state (top state)
void QHSMtest::initial(QEvent const *) 
{
   cout << "top-INIT;"<<endl; 
   myFoo = 0;               	// Initialize extended state variable
   Q_INIT(&QHSMtest::s0);  		// Initial transition, make transition to s0
}


QSTATE QHSMtest::s0(QEvent const *e) 
{
   switch (e->sig) {
   case Q_ENTRY_SIG: cout << "s0-ENTRY;"<<endl; return 0;
   case Q_EXIT_SIG:  cout << "s0-EXIT;"<<endl;  return 0;
   case Q_INIT_SIG:  cout << "s0-INIT;"<<endl;  Q_INIT(&QHSMtest::s1);   
      return 0;
   }
   return (QSTATE)&QHSMtest::top;
}


QSTATE QHSMtest::s1(QEvent const *e) 
{
   switch (e->sig) {
   case Q_ENTRY_SIG: cout << "s1-ENTRY;"<<endl; return 0;
   case Q_EXIT_SIG:  cout << "s1-EXIT;"<<endl;  return 0;
   case Q_INIT_SIG:  cout << "s1-INIT;"<<endl;  Q_INIT(&QHSMtest::s11);
      return 0;
   case A_SIG: cout << "s1-A;"<<endl; Q_TRAN(&QHSMtest::s1);   return 0;
   case B_SIG: cout << "s1-B;"<<endl; Q_TRAN(&QHSMtest::s11);  return 0;
   case C_SIG: cout << "s1-C;"<<endl; Q_TRAN(&QHSMtest::s2);   return 0;
   case D_SIG: cout << "s1-D;"<<endl; Q_TRAN(&QHSMtest::s0);   return 0;
   case F_SIG: cout << "s1-F;"<<endl; Q_TRAN(&QHSMtest::s21);  return 0;
   } 
   return (QSTATE)&QHSMtest::s0;
}


QSTATE QHSMtest::s11(QEvent const *e) 
{
   switch (e->sig) {
   case Q_ENTRY_SIG: cout << "s11-ENTRY;"<<endl; return 0;
   case Q_EXIT_SIG:  cout << "s11-EXIT;"<<endl;  return 0;
   case G_SIG: cout << "s11-G;"<<endl; Q_TRAN(&QHSMtest::s211); return 0;
   case H_SIG:                    // internal transition with a guard
      if (myFoo) {                // test the guard condition
         cout << "s11-H;"<<endl;
         myFoo = 0;
         return 0;
      }
      break;
   } 
   return (QSTATE)&QHSMtest::s1;
}


QSTATE QHSMtest::s2(QEvent const *e) 
{
   switch (e->sig) {
   case Q_ENTRY_SIG: cout << "s2-ENTRY;"<<endl; return 0;
   case Q_EXIT_SIG:  cout << "s2-EXIT;"<<endl;  return 0;
   case Q_INIT_SIG:  cout << "s2-INIT;"<<endl;  Q_INIT(&QHSMtest::s21); 
      return 0;
   case C_SIG: cout << "s2-C;"<<endl; Q_TRAN(&QHSMtest::s1);  return 0;
   case E_SIG: cout << "s2-E;"<<endl; Q_TRAN(&QHSMtest::s211);return 0;
   case F_SIG: cout << "s2-F;"<<endl; Q_TRAN(&QHSMtest::s11); return 0;
   } 
   return (QSTATE)&QHSMtest::s0;
}


QSTATE QHSMtest::s21(QEvent const *e) 
{
   switch (e->sig) {
   case Q_ENTRY_SIG: cout << "s21-ENTRY;"<<endl; return 0;
   case Q_EXIT_SIG:  cout << "s21-EXIT;"<<endl;  return 0;
   case Q_INIT_SIG:  cout << "s21-INIT;"<<endl;  Q_INIT(&QHSMtest::s211); 
      return 0;
   case B_SIG: cout << "s21-B;"<<endl; Q_TRAN(&QHSMtest::s211); return 0;
   case H_SIG:                        // self transition with a guard
      if (!myFoo) {                       // test the guard condition
         cout << "s21-H;"<<endl;
         myFoo = 1;
         Q_TRAN(&QHSMtest::s21);                    // self transition
         return 0;
      }
      break;                        // break to return the superstate
   } 
   return (QSTATE)&QHSMtest::s2;              // return the superstate
}


QSTATE QHSMtest::s211(QEvent const *e) 
{
   switch (e->sig) {
   case Q_ENTRY_SIG: cout << "s211-ENTRY;"<<endl; return 0;
   case Q_EXIT_SIG:  cout << "s211-EXIT;"<<endl;  return 0;
   case D_SIG: cout << "s211-D;"<<endl; Q_TRAN(&QHSMtest::s21); return 0;
   case G_SIG: cout << "s211-G;"<<endl; Q_TRAN(&QHSMtest::s0);  return 0;
   } 
   return (QSTATE)&QHSMtest::s21;
}

         
	

/*********************************
 * Main function 
 *********************************/	

int main() 
{
	cout << "QHSMtest example, version 0.09"<<endl;

	PRINT_INFO;
	// Create a test object
   QHSMtest test;
   cout << "QHSM version: " << test.getVersion() << endl;
   
   // Take the initial transition
   cout << endl << "Initializing QHSM..." << endl;
   test.init();     

   cout << endl << "Running main loop..." << endl;
   
   for (;;) {
      cout << "\nGive a signal (a-h): ";
      char c = getc(stdin); 
      getc(stdin);                                    // discard '\n'
      if (c < 'a' || 'h' < c) {                          // in range?
    	  cout << "\nUnspecified signal, exiting..."<<endl<<"Bye!";
    	  return 0; 
      }
      test.dispatch(&testQEvt[c - 'a']);            // dispatch event
      
      // Display inner state
      cout << "Foo = " << test.getFoo() << endl;
      
      // Pause some ms
      //DGCusleep( (int)(1E3*1000) );	// mikroseconds to milliseconds
   }
   
   return 0;
}










// EOF 'QHSMtest.cc'
