/**********************************
 * Header for QEvent.cc
 * Author: Miro Samek, modified by Stefan Skoog
 * Date:   July 2008
 * 
 **********************************/

#ifndef QEvent_hh
#define QEvent_hh

// Includes
#include <temp-planner-interfaces/PlannerInterfaces.h>	// State problem definition from here
#include <gcinterfaces/SegGoals.hh>
#include <map/MapPrior.hh>
#include "IntersectionHandling.hh"


/******************************************************************
 * Macros for QEvent enum->string conversion
 ******************************************************************/
#define FOR_ALL_QSIGNALS(QS)\
		QS(Q_EMPTY_SIG) QS(Q_INIT_SIG) QS(Q_ENTRY_SIG) QS(Q_EXIT_SIG)\
		QS(Q_USER_SIG) QS(Q_NEW_SEGMENT_TYPE) QS(Q_STOP_INTERSECTION) \
		QS(Q_ESTOP) QS(Q_NEW_GOAL)

#define DECLARE_QSIGNAL(A) A,
enum QSignal {
	FOR_ALL_QSIGNALS(DECLARE_QSIGNAL) QSIGNAL_COUNT
};

#define MAP_QSIGNAL_TO_STRING(A) #A,
const char *const QSignalStr[QSIGNAL_COUNT] = {
	FOR_ALL_QSIGNALS(MAP_QSIGNAL_TO_STRING)
};


/******************************************************************
 * Definition of QEvent
 ******************************************************************/
struct QEvent {
	QSignal sig;		// Signal of the event instance
	// QState source;		
	// StateProblem_t *problem;		// Definition of state problems 
	// Logic_params_t params;		// A lot of data from planner and missionPlanner
};

// This pre-defined array is needed AND reserved for state transitions only! 
extern QEvent pkgStdQEvent[]; 


/******************************************************************
 * Definition of LogicData structure
 ******************************************************************/
typedef struct {
	
	// - - - - - - - - Some basic logic data passed from planner every execution cycle - - - - - - - - //
	//vector<StateProblem_t*> problems;	// Vector of pointers to StateProblems_t. Since I didn't get this to work due to a lot of segmentation faults, I'm going with a single problem instead,
	StateProblem_t* problem;	// Pointer to StateProblems_t
	PlanGraph* graph;
	Err_t newErr; 
	Err_t prevErr;
	VehicleState* vehState; 
	Map* map; 
	Logic_params_t* params;
	int currentSegmentId; 
	int currentLaneId;
	bool replanInZone;

	// Cool objects
	CIntersectionHandling* intHandler;
	
	// - - - - - - - - Time stamps - - - - - - - - //
	// Logic level 1
	uint64_t last_lvl1_tran;			// The last transition between lvl 1 states
	uint64_t last_intersection_enter;	// Last time any intersection state was started (entered)
	uint64_t last_intersection_exit;	// Last time any intersection state was started (entered)
	uint64_t last_zone_enter;			// Last time zone state was entered
	uint64_t last_zone_exit;			// Last time zone state was exited
	uint64_t last_paused;				// Last time state paused was used
	
	// Logic level 2
	uint64_t last_lvl2_tran;		// The last transition between lvl 2 states
	uint64_t last_stopobs;			// Last time Alice was stopped due to obstacle
	uint64_t last_astern;			// Last time Alice tried to back up
	uint64_t last_agressive;		// Last time Alice tried to run in agressive mode
	uint64_t last_bare;				// Last time Alice tried to run in bare mode
	
	// Higher logic levels
	uint64_t last_lvl3_tran;		// The last transition between lvl 3 states

	
	// - - - - - - - - Derived spatial data - - - - - - - - // 
	double currentVelocity;
	double filteredVelocity;
	double stoplineDistance;
	double obstacleDistance;
	double exitDistance;
	double obstacleOnPath;
	double obstacleInLane;
	double stoppingDistance;
	
	double obsSide;
	double obsRear;
	double obsFront;
	
	LaneLabel currentLane;
	LaneLabel desiredLane;
	
	// - - - - - - - - mod-log-planner config data - - - - - - - - //
	// Please see config file for documentation for these
	double NOMINAL_SAFETYBUFFER;
	double ROAD_OBS_DISTANCE;
	double DESIRED_DECELERATION;
	int ASTERN_MAX_TIME;
	int UTURN_MAX_TIME;
	int MAX_YIELD_TIME;
	int ROAD_TO_OFFROAD_TIME;
	double DRIVE_VEL_REQUIREMENT;
	double INTERSECT_VEL_REQUIREMENT;
	double INTERSECT_STOPLINE_DIST;
	int INTERSECT_MIN_STOP_TIME;
	double INTERSECT_DEADTIME;
	double ZONE_VEL_REQUIREMENT;
	int ZONE_SAFE_TIMEOUT;
	int ZONE_AGRESSIVE_TIMEOUT;
	int ZONE_BARE_TIMEOUT;
	int OFFROAD_MAX_TIME;
	double VEL_FILTER_FS_HIGH;
	double VEL_FILTER_FS_LOW;
	double VEL_FILTER_RESET;
	
	
	// - - - - - - - - Some functions - - - - - - - - //
	
	// Do funky funky filtering with velocity
	void velLowPass(double newVel)
	{
		// Do funky, magic, non-linear filtering
		if (newVel > filteredVelocity) {
			// New speed is higher than filter speed, update with rather short time constant
			filteredVelocity = filteredVelocity*(1-VEL_FILTER_FS_LOW) + newVel*(VEL_FILTER_FS_LOW);
		} else {
			// New speed is lower than filter speed, update with slooow time constant
			filteredVelocity = filteredVelocity*(1.0-VEL_FILTER_FS_LOW) + newVel*(VEL_FILTER_FS_LOW);	
		}
	}
	// Do reset of funky funky velocity filter
	void resetFilter(void)
	{
		filteredVelocity = VEL_FILTER_RESET;
	}
	// Another way to reset, with free reset value
	void resetFilter(double value)
	{
		filteredVelocity = value;
	}
	
} Logic_data_t;



#endif

// EOF 'QEvent.hh'
