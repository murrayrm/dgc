/*!
 * \file QHSM2_road.cc
 * \brief Quantum Hierarchical State Machine Level 2 - Road
 *
 * \author Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */



/**
 * @brief 	Logic Level 2 State 
 *			Alice drives
 * @return  QSTATE
 */
QSTATE QHSM1::S21_drive(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		PRINT_ENTRY;
	 	LVL2_ENTRY;
		// Change state problem rules
		ld->problem->state = DRIVE;
		ld->problem->planner = RAIL_PLANNER;
		return 0;
	 }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG: {
    	 PRINT_INIT;
    	 if (ld->params->seg_goal.illegalPassingAllowed) {
    		 qprint(" Passing allowed ");
    		 Q_INIT(&QHSM1::S211_passing); return 0;
    		 return 0;
    	 } else {    	 
    		 qprint(" Passing not allowed ");
    		 Q_INIT(&QHSM1::S212_nopassing);
    		 return 0;
    	 }
     }
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S2_road;
}


/**
 * @brief 	Logic Level 2 State 
 *			Alice is stopped because of some obstacle in the way
 * @return  QSTATE
 */
QSTATE QHSM1::S22_stop(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		PRINT_ENTRY; 
	 	LVL2_ENTRY;
		ld->problem->state = STOP_OBS;
		ld->last_stopobs = Utils::getTime();
		return 0;
	 }
	 case Q_USER_SIG: { 
		 // Do 'ordinary' stuff
		 int stop_duration = since(ld->last_stopobs);
		 QLOG(7,"\tAlice has stopped for obstacle, since %d seconds", stop_duration );
		 
	 
	//  ONLY TRY NEW STRATEGY IF PROPER ERROR SIGNAL IS DETECTED!
		 // Stopped for obstacle too long?
		 if ( stop_duration > ld->MAX_YIELD_TIME ) {
			 if ( stop_duration > 200 ) {
				 // Silly big, some time measure errors. Reset it!
				 ld->last_stopobs = Utils::getTime();
			 } else {
				 // Back up!
				 QLOG(7,"\tStopObs timeout (%d seconds), backing up.", stop_duration );
				 Q_TRAN(&QHSM1::S23_astern); return 0;
			 }
		 }
		 return 0;	

	 }
	 
		 
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S2_road;
}


/**
 * @brief 	Logic Level 2 State 
 *			Alice reverses (to get away from obstacle)
 * @return  QSTATE
 */
QSTATE QHSM1::S23_astern(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY;  
		 LVL2_ENTRY;
		 ld->problem->state = BACKUP;
		 ld->problem->planner = S1PLANNER;
		 // Save time to start backing up
		 ld->last_astern = Utils::getTime(); 
		 
		 return 0;
	 }
	 case Q_USER_SIG: { 

    	 // Measure for how long we have been backing up. IN SECONDS (Don't change this to milliseconds!)
		 int astern_duration = since(ld->last_astern);
    	 
    	 // We are either done with backup (signal from pPlanner), or timed out. Switch back to drive.
    	 if ( (ld->prevErr & VP_BACKUP_FINISHED) ||
    		  (ld->prevErr & VP_UTURN_FINISHED)  ||
    	      (astern_duration > ld->ASTERN_MAX_TIME)
    	      ) {
    		 QLOG(6,"\tBackup ");
    		 if (ld->prevErr == VP_BACKUP_FINISHED) {
    			 QLOG(6,"finished");
    		 }
    		 if (astern_duration > ld->ASTERN_MAX_TIME) {
    			 QLOG(6,"timeout");
    		 }
    		 // Tell pPlanner to replan
    		 ld->params->planFromCurrPos = true;
    		 
    		 QLOG(6,", swithing to "); 
    		 
    		 // Switch back to drive if we're backed up for too long
	    	 // ...depending what we are allowed to do...
			 if (ld->params->seg_goal.illegalPassingAllowed) {
				 QLOG(6,"passingreverse");
				 Q_TRAN(&QHSM1::S213_passingreverse);
	    		 return 0;
	    	 } else {    	 
	    		 QLOG(6,"nopassing");
	    		 Q_TRAN(&QHSM1::S212_nopassing);
	    		 return 0;
	    	 }
    	 } else if ( ld->prevErr & PP_NOPATH_LEN ) {
    		 // We have a problem with the path planner!
    		 QLOG(6,"\tS1planner is stupid, it can't find a way out of here. Switch to offroad");
    		 // May need this to get out of here
    		 ld->problem->flag = OFFROAD;
    		 ld->problem->obstacle = OBSTACLE_AGGRESSIVE;
    		 ld->params->planFromCurrPos = true;
    		 ld->replanInZone = true;
    	 }
	   	 
    	 QLOG(6,"\tAlice is backing up, since %d seconds", astern_duration); 
    	 return 0;
	 }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S2_road;
}


// This is just a temporary fix to include other states!
// It will not work like this when the design pattern is all worked out.
#include "QHSM21_drive.cc"


// EOF 'QHSM2_road.cc'
