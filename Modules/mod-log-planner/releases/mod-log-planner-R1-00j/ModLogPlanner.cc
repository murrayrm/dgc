/*!
 * \file ModLogPlanner.cc
 * \brief Source code for Modular Logic Planner
 *
 * \author Stefan Skoog
 * \date Created June 2008
 *
 * \ingroup mod-log-planner
 *
 */



/*********************************
 * Include header
 *********************************/
#include "ModLogPlanner.hh"
DEFINE_THIS_FILE; // Assertion debug helping trick (see Qassert)


/*********************************
* Initiate (static) class variables
**********************************/




/*********************************
 * Methods
 *********************************/

/**
 * @brief 	Modular Logic PLanner Constructor
 *			Creates and initiates this logical planner.
 * @return  None
 */
ModLogPlanner::ModLogPlanner() 
{
	// Call alternative init function
	init();
	return;
}

/**
 * @brief 	Modular Logic PLanner initializer
 *			Reads config file, resets internal variables, creates Quantum Hierarchical State Machine.
 * @return  (int)0 if good
 */
int ModLogPlanner::init() 
{
	QLOG(2,"\nInitializing 'ModLogPlanner'...\n");
	
	// Create a new StateProblem_t with a copy of current_status
	logicData.problem = new StateProblem_t;
	memcpy( logicData.problem, &current_status, sizeof(StateProblem_t) );
	
	// Create the quantum hierarchical state machine
	// Creating with 'new' avoids putting the state machine on the stack...
	qhsm = new QHSM1(logicData);
	// Init state machine; VERY IMPORTANT!

	// Create a new IntersectionHandler
	logicData.intHandler = new CIntersectionHandling();
	
	// Resetting all (imporant) variables
	resetState();
	
	QLOG(2,"Module 'ModLogPlanner' initialized successfully.\n\n");
	
	return 0;
}

// Destructor
ModLogPlanner::~ModLogPlanner()
{
	// Get rid of all created objects and class references
	destroy();  // Call alternate destructor
	return;
}

// Alternative destructor
void ModLogPlanner::destroy() 
{
	// Deallocate all claimed objects...
	delete qhsm;
	delete logicData.problem;
	
	return;
}

/**
 * @brief 	Modular Logic PLanner config file reader
 *			Reads config file and store data in internal variables. Prints to log.
 * @return  (int)0 if good
 */
int ModLogPlanner::readConfig() 
{
	QLOG(2,"\nReading configuration file... ");
	// Read from file into workspace
	char *path;

	path=dgcFindConfigFile("ModLogPlanner.conf","mod-log-planner");
	FILE* configFile = fopen(path,"r");
	if (configFile == NULL) {
		qprint("Warning: Couldn't read ModLogPlanner configuration file!\n");
    	QLOG(1,"\nWarning: Couldn't read ModLogPlanner configuration file!\n");
    	cerr << "ModLogPlanner couldn't read config file" << endl;
	} else {
		ConfigFile config(path);
	    config.readInto( logicData.NOMINAL_SAFETYBUFFER, 	"NOMINAL_SAFETYBUFFER");
	    config.readInto( logicData.ROAD_OBS_DISTANCE, 		"ROAD_OBS_DISTANCE");
	    config.readInto( logicData.MAX_YIELD_TIME, 			"MAX_YIELD_TIME");
	    config.readInto( logicData.DESIRED_DECELERATION, 	"DESIRED_DECELERATION");
	    config.readInto( logicData.ASTERN_MAX_TIME, 		"ASTERN_MAX_TIME");
	    config.readInto( logicData.UTURN_MAX_TIME, 			"UTURN_MAX_TIME");
	    config.readInto( logicData.ROAD_TO_OFFROAD_TIME,	"ROAD_TO_OFFROAD_TIME");
	    config.readInto( logicData.DRIVE_VEL_REQUIREMENT, 	"DRIVE_VEL_REQUIREMENT");
	    config.readInto( logicData.INTERSECT_VEL_REQUIREMENT,"INTERSECT_VEL_REQUIREMENT");
	    config.readInto( logicData.INTERSECT_STOPLINE_DIST, "INTERSECT_STOPLINE_DIST");
	    config.readInto( logicData.INTERSECT_MIN_STOP_TIME,	"INTERSECT_MIN_STOP_TIME");
	    config.readInto( logicData.INTERSECT_DEADTIME, 		"INTERSECT_DEADTIME");
	    config.readInto( logicData.INTERSECT_MIN_DISTANCE,	"INTERSECT_MIN_DISTANCE");
	    config.readInto( logicData.ZONE_VEL_REQUIREMENT,	"ZONE_VEL_REQUIREMENT");
	    config.readInto( logicData.ZONE_SAFE_TIMEOUT,		"ZONE_SAFE_TIMEOUT");
	    config.readInto( logicData.ZONE_AGRESSIVE_TIMEOUT,	"ZONE_AGRESSIVE_TIMEOUT");
	    config.readInto( logicData.ZONE_BARE_TIMEOUT,		"ZONE_BARE_TIMEOUT");
	    
	    config.readInto( logicData.OFFROAD_MAX_TIME,		"OFFROAD_MAX_TIME");
	    
	    config.readInto( logicData.VEL_FILTER_FS_HIGH, 		"VEL_FILTER_FS_HIGH");
	    config.readInto( logicData.VEL_FILTER_FS_LOW, 		"VEL_FILTER_FS_LOW");
	    config.readInto( logicData.VEL_FILTER_RESET, 		"VEL_FILTER_RESET");
	    
	    
	    fclose(configFile);
	    QLOG(2,"done!\n")
	    
	    // Log all the parameters
	    QLOG(4,"ModLogPlanner config parameters:\n");
	    QLOG(4,"NOMINAL_SAFETYBUFFER: %f meters\n",			logicData.NOMINAL_SAFETYBUFFER);
	    QLOG(4,"ROAD_OBS_DISTANCE: %f meters\n",			logicData.ROAD_OBS_DISTANCE);
	    QLOG(4,"MAX_YIELD_TIME: %u seconds\n",				logicData.MAX_YIELD_TIME);
	    QLOG(4,"DESIRED_DECELERATION: %f m/s^2\n",			logicData.DESIRED_DECELERATION);
	    QLOG(4,"ASTERN_MAX_TIME: %u s\n",					logicData.ASTERN_MAX_TIME);
	    QLOG(4,"UTURN_MAX_TIME: %u s\n",					logicData.UTURN_MAX_TIME);
	    QLOG(4,"ROAD_TO_OFFROAD_TIME: %u s\n",				logicData.ROAD_TO_OFFROAD_TIME);
	    
	    QLOG(4,"DRIVE_VEL_REQUIREMENT %f m/s\n",			logicData.DRIVE_VEL_REQUIREMENT);
		QLOG(4,"INTERSECT_VEL_REQUIREMENT %f m/s\n",		logicData.INTERSECT_VEL_REQUIREMENT);
		QLOG(4,"INTERSECT_STOPLINE_DIST %f m\n",			logicData.INTERSECT_STOPLINE_DIST);
		QLOG(4,"INTERSECT_MIN_TOP_TIME %d s\n",				logicData.INTERSECT_MIN_STOP_TIME);
		QLOG(4,"INTERSECT_DEADTIME %f s\n",					logicData.INTERSECT_DEADTIME);
		QLOG(4,"INTERSECT_MIN_DISTANCE %f s\n",				logicData.INTERSECT_MIN_DISTANCE);
		
	    QLOG(4,"ZONE_VEL_REQUIREMENT %f m/s\n",	 			logicData.ZONE_VEL_REQUIREMENT);
	    QLOG(4,"ZONE_SAFE_TIMEOUT %d s\n",	 				logicData.ZONE_SAFE_TIMEOUT);
	    QLOG(4,"ZONE_AGRESSIVE_TIMEOUT %d s\n",				logicData.ZONE_AGRESSIVE_TIMEOUT);
	    QLOG(4,"ZONE_BARE_TIMEOUT %d s\n",	 				logicData.ZONE_BARE_TIMEOUT);
	    
	    QLOG(4,"OFFROAD_MAX_TIME %d s\n",	 				logicData.OFFROAD_MAX_TIME);
	    
	    QLOG(4,"VEL_FILTER_FS_HIGH %f units/execution\n",	logicData.VEL_FILTER_FS_HIGH);
	    QLOG(4,"VEL_FILTER_FS_LOW %f units/execution\n", 	logicData.VEL_FILTER_FS_LOW);
	    QLOG(4,"VEL_FILTER_RESET %f m/s\n", 				logicData.VEL_FILTER_RESET);
	    
	    QLOG(4,"\nConfig file read successfully.\n");
	    return 0;
	}
	return -1;
}



/**
 * @brief 	Modular Logic PLanner main method - plan logic
 *			Requires a lot of data from caller. 		
 * 
 * @return  An Err_t object (temp-planner-interfaces/PlannerInterfaces.h)
 * 			Also, the main result is returned as a StateProblem_t pushed into the vector (first argument)
 */
Err_t ModLogPlanner::planLogic(vector<StateProblem_t*> &problems, PlanGraph *graph, Err_t prevErr, 
			 VehicleState &vehState, Map *map, Logic_params_t &params, 
			 int& currentSegmentId, int& currentLaneId, bool& replanInZone ) 
{
	
	QLOG(2,"\n-->\tModLogPlanner call # %0ld", ++cycle_counter);
	
	// ** ** Now here's some few 'MAGIC' things to do before actual planning starts ** ** //
	
	// Update MapElements. If a car has stopped for long enough, it's considered as a static obstacle
	Utils::updateStoppedMapElements(map);
	
	
	// ** ** End of magic ** ** //
	
	
	// Save all incoming data into public struct
	logicData.problem = new StateProblem_t;	// Create new (empty) stateProblem to work with this cycle
	logicData.graph = graph;
	logicData.prevErr = prevErr;
	logicData.newErr  = LP_OK;		// Default respond is 'OK'
	logicData.vehState = &vehState;
	logicData.map = map;
	logicData.params = &params;
	logicData.currentSegmentId = currentSegmentId;
	logicData.currentLaneId = currentLaneId;
	logicData.replanInZone = false;

	// Update other important parameters
	updateLogicData(logicData);
	
	// Load new state problem with current data
	qhsm->copyStateProblem( logicData.problem, &current_status);
	//memcpy(logicData.problem, &current_status, sizeof(StateProblem_t) );
	
	
    // - - - - Fabricate a signal to the state machine - - - - - //
    if (params.m_estop != EstopRun ) { 
    	
    	// Is this a (new) transition to eStop?
    	if (last_seg_type == SegGoals::EMERGENCY_STOP) {
    		// No, it's old. Just prevent all other signals from generation 
    		QLOG(7,"\n[%s]: Still in eStop...",__FUNCTION__);
    	} else {
    		// Yep, transition to eStop
	    	QLOG(7,"\n[%s]: Got Estop, sending Q_ESTOP event",__FUNCTION__);
	    	// Paused, disabled or E-stopped
			qe.sig = Q_ESTOP;
			// Set last seg type to pause so that we can resume from disable signal (work?)
			last_seg_type = SegGoals::EMERGENCY_STOP;  
    	}
		
    } else if ( (int)last_seg_type != (int)params.segment_type) {
    	// New segment type is not the same as the old one
    	QLOG(7,"\n[%s]: New segment, sending Q_NEW_SEGMENT event",__FUNCTION__);
    	qe.sig = Q_NEW_SEGMENT_TYPE;
    	last_seg_type = params.segment_type;
    	
    } else if (params.seg_goal.goalID != last_goalID) {
    	// We have a new goal from missionPLanner!
    	QLOG(7,"\n[%s]: New goal from mPlanner!",__FUNCTION__);
    	qe.sig = Q_NEW_GOAL;
    	last_goalID = params.seg_goal.goalID;  // Save as old goalID
    	
    } else if ( logicData.stoppingDistance > logicData.stoplineDistance && 
    			!qhsm->intersectionJustVisited(&logicData) ) {
    	// Switch to intersection mode only if:
    	// a) close to intersection
    	// b) not stopped for this stopline already recently
		
    	// Tell velocity planner to stop at the stop line
		qe.sig = Q_STOP_INTERSECTION;
    	
	} else if (prevErr) {
    	// We have got a problem! Figure out which one and what to do!
    	if (prevErr != last_err) {	// Print only if error is new
			QLOG(7,"\n[%s]: New planning error: %d (old: %d)", __FUNCTION__, prevErr, last_err );
			QCONSOLE("Err: %s", plannerErrorsToString(prevErr).c_str() );
			
    	} else if ( (prevErr & PP_COLLISION_BARE) &&
    				(qhsm->since( logicData.last_goal ) > 5 )
    				) {
    		/* a) We have received serious collision error
    		 * b) Last goad was evalueated for at least five seconds
    		 * 
    		 *  Tell mission planner that this is not a good way to travel 
    		 */
    		
    		QLOG(7,"\n[%s]: Path is seriously blocked, new mission please!",__FUNCTION__);
    		QCONSOLE("\nPath really jammed, replan mission!");
    		logicData.newErr |= LP_FAIL_MPLANNER_ROAD_BLOCKED;  // Replan, please!
    	}
    	qe.sig = Q_USER_SIG;  	// Send ordinary signal for now
    	last_err = prevErr;		// Save this as old error
    	
	} else {
		QLOG(7,"\n[%s]: No special signals, sending Q_USER_SIG event",__FUNCTION__);
		qe.sig = Q_USER_SIG;	//  Nothing happend since last execution, send standard signal to state machine
		
	}

	

	// Print logic data, tabbed and nice
	printLogicData(logicData, true);
	
	
	// Run state machine dispatcher with new signal in QEvent
	qhsm->dispatch( &qe );	
	
	// Update intersection handler consol
	logicData.intHandler->updateConsole();
	
	// Save result in internal StateProblem_t
	memcpy( &current_status, logicData.problem, sizeof(StateProblem_t) );
	
	// Copy derived stateProblems to planner's vector
	problems.push_back( logicData.problem );
	
	if(replanInZone = logicData.replanInZone) {
		QLOG(7,"- - - - replanInZone! - - - -\n");
	}
	
    // Change return data when code is starting to work
	return logicData.newErr;
}


/**
 * @brief 	Modular Logic PLanner reset
 *			Re-initiates the state machine 		
 * 
 * @return  void
 */
void ModLogPlanner::resetState()
{
	QLOG(2,"\nRESET 'ModLogPlanner'...");
	
	// Re-read config file
	readConfig();
	
	// Reset cycle counter
	cycle_counter = 0;
	
	// Re-init state problem
	current_status.state       = DRIVE;
	current_status.probability = 0.99;
	current_status.flag        = PASS;
	current_status.region      = ROAD_REGION;
	current_status.planner     = RAIL_PLANNER;
	current_status.obstacle    = OBSTACLE_SAFETY;

	// Init LogicData parameters
	logicData.filteredVelocity = 0;		// Good to initialize!
	logicData.resetTimeStamps( GET_TIME );	// Reset to current time
	logicData.resetPositionStamps( point2() );	// Zero on all coordinates
	
	// Re-init state machine. Hopefully, it will not crash
	qhsm->init();
	
}

// Blaha
void ModLogPlanner::updateIntersectionHandlingConsole()
{
	return;
}


/**
 * @brief 	Modular Logic PLanner update logic data
 *			Get info from peripheral classes, modify, and save in Logic_data structure 
 * 
 * @return  non-0 for success
 */
int ModLogPlanner::updateLogicData(Logic_data_t& ld)
{
	ld.currentVelocity   = 	AliceStateHelper::getVelocityMag( *(ld.vehState) );
	ld.stoplineDistance = 	Utils::getDistToStopline( ld.params->m_path );
	if ( abs( (long long)ld.stoplineDistance ) > 1000) {	// Stoplines further than 1 km away is considered non-existent
		ld.stoplineDistance = INFINITY;
	}
	ld.currentLane =		Utils::getCurrentLaneAnyDir( *(ld.vehState), ld.graph );
	ld.desiredLane = 		Utils::getDesiredLane( *(ld.vehState), ld.graph );
	ld.obstacleDistance = 	Utils::getNearestObsDist( *(ld.vehState), ld.map, ld.currentLane);
	ld.obstacleOnPath = 	Utils::getNearestObsOnPath( ld.params->m_path, ld.graph, *(ld.vehState) );
	ld.obstacleInLane = 	Utils::getNearestObsInLane( *(ld.vehState), ld.map, ld.currentLane);
	ld.exitDistance = 		Utils::getDistToExit( *(ld.vehState), ld.map, ld.params->seg_goal );
	ld.stoppingDistance = 	(ld.currentVelocity * ld.currentVelocity)/(2.0*ld.DESIRED_DECELERATION);
	
	// Clean up two of them
	if (ld.obstacleDistance == -1) ld.obstacleDistance = INFINITY;
	if (ld.obstacleInLane   == -1) ld.obstacleInLane   = INFINITY;
	
	// Prevent bad pointers from messing up this data collection
	if ( (ld.params->m_path==NULL) || 
		 (ld.params->m_path==(void*)0xFFFFFFFF) 
		 ) {
		ld.obsSide =	 		ld.params->m_path->nodes[0]->status.obsSideDist;
		ld.obsRear =	 		ld.params->m_path->nodes[0]->status.obsRearDist;
		ld.obsFront =	 		ld.params->m_path->nodes[0]->status.obsFrontDist;	
	} else {
		QLOG(9,"\n[%s]: PlanGraphPath not yet established",__FUNCTION__);
		ld.obsSide  = INFINITY;
		ld.obsRear  = INFINITY;
		ld.obsFront = INFINITY;
	}
	
	// Low pass filter Alice's speed and store in LogicData
	ld.velLowPass(ld.currentVelocity);

	return 1;	// Succeeded!
}


/**
 * @brief 	Modular Logic PLanner print logic data
 *			Print all logicData in a nice tabbed way 
 * 
 * @return  none
 */
void ModLogPlanner::printLogicData(Logic_data_t& ld, bool extended)
{
	if(extended) {
		QLOG(7,	"\n[%s]: ", __FUNCTION__);
		QLOG(7, "\n\tsegGoals: %s", ld.params->seg_goal.toString().c_str() );
		QLOG(7, "\n\tEstopStatus:  %s", 		estopString[ld.params->m_estop] );
		QLOG(7,	"\n\tSegExit:      %f meters", 	ld.params->seg_goal.distance );
		QLOG(7,	"\n\tExit:         %f meters", 	ld.exitDistance );
		QLOG(7,	"\n\tPrev Error:   %d = %s", 	ld.prevErr, plannerErrorsToString(ld.prevErr).c_str() );
		QLOG(7,	"\n\tVelocity:     %f m/s", 	ld.currentVelocity );
		QLOG(7,	"\n\tFilteredVel:  %f m/s", 	ld.filteredVelocity );
		QLOG(7,	"\n\tStopDistance: %f meters", 	ld.stoppingDistance );
		QLOG(7,	"\n\tStopline:     %f meters", 	ld.stoplineDistance );
		QLOG(7,	"\n\tObstacle:     %f meters", 	ld.obstacleDistance );
		QLOG(7,	"\n\tObstOnPath:   %f meters", 	ld.obstacleOnPath );
		QLOG(7,	"\n\tObstInLane:   %f meters", 	ld.obstacleInLane );
		QLOG(7,	"\n\tLane/Segment: %d/%d -> %d/%d", ld.currentLane.lane, ld.currentLane.segment, ld.desiredLane.lane, ld.desiredLane.segment );
		if ( (ld.params->m_path!=NULL) && (ld.params->m_path=!(void*)0xFFFFFFFF) ) {
			QLOG(7,	"\n\tObsFront:     %f meters", 	ld.obsFront );
			QLOG(7,	"\n\tObsSide:      %f meters", 	ld.obsSide );
			QLOG(7,	"\n\tObsRear:      %f meters", 	ld.obsRear );
		}
		QLOG(7,	"\n");
		
	} else {
		QLOG(7,	"\n[%s]: "
				"\n\tPrev Error:   %d = %s"
				"\n\tVelocity:     %f m/s"
				"\n\tFilteredVel:  %f m/s"
				"\n\tStopline:     %f meters"
				"\n\tStopDistance: %f meters"
				"\n\tObstacle:     %f meters"
				"\n\tObstOnPath:   %f meters"
				"\n\tObsFront:     %f meters"
				"\n\tObsSide:      %f meters"
				"\n\tObsRear:      %f meters"
				"\n\tExit:         %f meters"
				"\n\tSegExit:      %f meters"
				"\n\tLane/Segment: %d/%d",
				__FUNCTION__, ld.prevErr, plannerErrorsToString(ld.prevErr).c_str(), ld.currentVelocity, ld.filteredVelocity, 
				ld.stoplineDistance, ld.stoppingDistance, ld.obstacleDistance, ld.obstacleOnPath, 
				ld.obsFront, ld.obsSide, ld.obsRear ,ld.exitDistance, ld.params->seg_goal.distance,
				ld.currentLane.lane, ld.currentLane.segment 
				);
	}
}



// EOF 'ModLogPlanner.cc'
