/*!
 * \file QHSM1.cc
 * \brief Quantum Hierarchical State Machine Level 1
 *
 * \author Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */




/*********************************
 * Include header 
 *********************************/
#include "QHSM1.hh"
DEFINE_THIS_FILE; // Assertion debug helping trick (see Qassert)


/*********************************
 * Basic class functions 
 *********************************/

// Initial state (top state)
void QHSM2_road::initial(QEvent const *) 
{
	PRINT_INFO;
	qprint("TOP-%s;\n",__FUNCTION__); 
	// Init any internal variables here
	

	Q_INIT(&QHSM2_road::S2_DR_NP);  		// Initial transition, make transition to road
}

void QHSM2_road::destroy()
{
	// Clear up internal variables here
}

/******************************************************************
 * 						State methods 
 ******************************************************************/
QSTATE QHSM2_road::S21_DR_NP(QEvent const *e);
{
	PRINT_INFO; qprint("Signal: %u    ",e->sig);
	switch (e->sig) {
	 case Q_ENTRY_SIG: qprint("%s-ENTRY;\n",__FUNCTION__); return 0;
     case Q_EXIT_SIG:  qprint("%s-EXIT;\n", __FUNCTION__); return 0;
     //case Q_INIT_SIG:  qprint("%s-INIT;\n", __FUNCTION__); return 0;
	}
	qprint("Unrecognized signal catched: %u",e->sig);
    return (QSTATE)&QHSM2_road::top;
}

QSTATE QHSM2_road::S22_STO_NP(QEvent const *e)
{
	
}


QSTATE QHSM2_road::S23_DR_P(QEvent const *e)
{
	
}


QSTATE QHSM2_road::S24_STO_P(QEvent const *e)
{
	
}


QSTATE QHSM2_road::S25_DR_PR(QEvent const *e)
{
	
}


QSTATE QHSM2_road::S26_STO_PR(QEvent const *e)
{
	
}


QSTATE QHSM2_road::S27_ASTERN(QEvent const *e)
{
	
}



// EOF 'QHSM1.cc'
