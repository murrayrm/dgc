/*!
 * \file QHSM1.hh
 * \brief Quantum Hierarchical State Machine Level 1
 *
 * \author Miro Samek, modified by Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */


#ifndef QHSM2_road_hh
#define QHSM2_road_hh


/*********************************
 * Includes
 *********************************/
#include "QHSM1.hh"


/*********************************
* Initiate class variables
**********************************/


/*********************************
 * Class define
 *********************************/
class QHSM2_road : public QHSM1 {
public:
	/* 	Constructor, takes no arguments, but calls base class 
	 	with member function pointer to initial state */ 
	QHSM2_road(): QHSM1( (QPseudoState)&QHSM2_road::initial ) {}	
	virtual ~QHSM2_road() {QHSM2_road::destroy(); }
	
protected:
	void destroy();
	
	void initial(QEvent const *e); 	// Initial (startup) state
		// The list of different available level 2x-states
		QSTATE S21_DR_NP(QEvent const *e);
		QSTATE S22_STO_NP(QEvent const *e);
		QSTATE S23_DR_P(QEvent const *e);
		QSTATE S24_STO_P(QEvent const *e);
		QSTATE S25_DR_PR(QEvent const *e);
		QSTATE S26_STO_PR(QEvent const *e);
		QSTATE S27_ASTERN(QEvent const *e);

		
		
private:                               
	// Extended state variables goes here
};





#endif
// EOF 'QHSM2_road.hh'
