/**********************************
 * Header for Qassert.cc
 * Author: Miro Samek, modified by Stefan Skoog
 * Date:   July 2008
 * 
 **********************************/

#ifndef Qassert_hh
#define Qassert_hh


/** NASSERT macro disables all contract validations
 * (assertions, preconditions, postconditions, and invariants).
 */  

// Preprocess switch to disable ASSERTions
#ifndef NASSERT 
                  /* callback invoked in case of assertion failure */
extern void onAssert__(char const *file, unsigned line);

#define DEFINE_THIS_FILE \
   static const char THIS_FILE__[] = __FILE__

#define ASSERT(test_)\
   if (test_) {      \
   }                 \
   else onAssert__(THIS_FILE__, __LINE__)

#define REQUIRE(test_)   ASSERT(test_)
#define ENSURE(test_)    ASSERT(test_)
#define INVARIANT(test_) ASSERT(test_)
#define ALLEGE(test_)    ASSERT(test_)


#else  // #ifndef NASSERT - disable all links to assert()  

#define DEFINE_THIS_FILE extern const char THIS_FILE__[]
#define ASSERT(test_)    
#define REQUIRE(test_)   
#define ENSURE(test_)    
#define INVARIANT(test_) 
#define ALLEGE(test_)\
   if (test_) {      \
   }                 \
   else
#endif


#endif

// EOF 'Qassert.hh'
