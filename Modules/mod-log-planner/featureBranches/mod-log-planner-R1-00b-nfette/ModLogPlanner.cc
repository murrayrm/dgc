/*!
 * \file ModLogPlanner.cc
 * \brief Source code for Modular Logic Planner
 *
 * \author Stefan Skoog
 * \date Created June 2008
 *
 * \ingroup mod-log-planner
 *
 */



/*********************************
 * Include header
 *********************************/
#include "ModLogPlanner.hh"
DEFINE_THIS_FILE; // Assertion debug helping trick (see Qassert)


/*********************************
* Initiate (static) class variables
**********************************/




/*********************************
 * Methods
 *********************************/

// Constructor
ModLogPlanner::ModLogPlanner() 
{
        // Call alternative init function
        init();
        return;
}

// Alternative constructor stuff
int ModLogPlanner::init() 
{
        qprint("\nInitializing 'ModLogPlanner'...\n");
        
        // Read config file and save into class variables
        readConfig();
        
        // Cycle counter, begin at zero
        cycle_counter = 0;
        
        // Init current status, complete state representation
        this->current_status.state       = DRIVE;
        this->current_status.probability = 0.99;
        this->current_status.flag        = PASS;
        this->current_status.region      = ROAD_REGION;
        this->current_status.planner     = RAIL_PLANNER;
        this->current_status.obstacle    = OBSTACLE_SAFETY;
        
  switch (SM_MODE) {
    case 1:
      // use parsed state machine
      parsha = new ParsedSM("dummy01.psm");
      parsha->init();
      break;
  
    case 0:
      defualt:
      // Create the quantum hierarchical state machine
      qhsm = new QHSM1();
      // Init state machine; VERY IMPORTANT!
      qhsm->init();
      
      // Create an QEvent to feed to qhsm
      qe = new QEvent();
      qe->sig = Q_USER_SIG;   // Load signal with first user definable signal 
  }
        
  // Create a new IntersectionHandler
  //IntersectionHandling = new CIntersectionHandling();

  qprint("\nModule 'ModLogPlanner' initiated successfully.\n\n");
  return 0;
}

// Destructor
ModLogPlanner::~ModLogPlanner()
{
        // Get rid of all created objects and class references
        destroy();  // Call alternate destructor
        return;
}

// Alternative destructor
void ModLogPlanner::destroy() 
{
        // Deallocate all claimed objects...
        return;
}

// Read config file
void ModLogPlanner::readConfig() 
{
        // Read from file into workspace
        //... insert code here...
        //return;

  char *path;
  // include this function from dgcutils/cfgfile.h
  path=dgcFindConfigFile("ModLogPlanner.conf","mod-log-planner");
  FILE* configFile = fopen(path,"r");
  if (configFile == NULL) {
    Console::addMessage("Warning: Couldn't read ModLogPlanner.conf.");
    // FIXME: choose & use appropriate logging function.
    Log::getStream(1)<<"Warning: Couldn't read ModLogPlanner.conf."<<endl;
    //qprint("Warning: Couldn't read ModLogPlanner.conf.\n");
  }
  else {
    // include this class from temp-planner-interfaces/ConfigFile.hh
    ConfigFile config(path);
    // Example:
    config.readInto(LIFE, "LIFE");
    config.readInto(SM_MODE, "SM_MODE");
    
    Log::getStream(1)<<"Info: Read Logic Planner configuration successfully."<<endl;
    fclose(configFile);

    // Log all the parameters
    Log::getStream(5) << "LIFE: " << LIFE << endl;
    Log::getStream(5) << "SM_MODE: " << SM_MODE;
    switch (SM_MODE) {
      case 1: Log::getStream(5) << " (Parsed SM)" << endl; break;
      case 0:
      default: Log::getStream(5) << " (Quantum SM)" << endl;
    }
  }
}



// * * * * * * * Plan logic * * * * * * * //
Err_t ModLogPlanner::planLogic(vector<StateProblem_t> &problems, PlanGraph *graph, Err_t prevErr, 
                         VehicleState &vehState, Map *map, 
                         Logic_params_t &params, int& currentSegmentId, int& currentLaneId,
                         bool& replanInZone) 
{
  // DEBUGGING: Print message every 10th time call here
  if ( !(++cycle_counter % 10) ) {
    qprint("-->\tModLogPlanner call # %0ld\n", cycle_counter);
  }

  switch (SM_MODE) {
    // Use the parsed state machine.
    case 1:
      // Forward the arguments.
      return parsha->planLogic(problems, graph, prevErr, 
                         vehState, map, 
                         params, currentSegmentId, currentLaneId,
                         replanInZone); 

    break;
    
    // Use the quantum state machine.
    case 0:
    default:
/*
      // Load QEvent with exactly the same state problems as current problems
      qe->problem->state =            this->current_status.state;
      qe->problem->probability =  this->current_status.probability;
      qe->problem->flag =                 this->current_status.flag;
      qe->problem->region =               this->current_status.region;
      qe->problem->planner =              this->current_status.planner;
      qe->problem->obstacle =     this->current_status.obstacle;

      // Fabricate a signal to the state machine
      qe->sig = Q_USER_SIG;  // User sig is first free signal to use
      //qe->params = &params; 

      // Segment type
      //qprint("Segment type: %s",qe->params->segment_type)
        
      // Run state machine dispatcher
      qhsm->dispatch(qe);

      // Add new problem to problem queue
      problems.push_back( *(qe->problem) );
*/
      problems.push_back( current_status );

      // Change return data when code is starting to work
      return LP_OK;
  }
}








// EOF 'ModLogPlanner.cc'
