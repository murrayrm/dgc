// ParsedMachines.hh
// Define a few environments and systems for ParsedSMs.
// By Nicholas Fette.
// see DGC07:User:Nfette/Summer 2008

#ifndef _PARSED_MACHINES_HH_
#define _PARSED_MACHINES_HH_
#include "ParsedSM.hh" // Defines ParsedSM, SMState, Environment, and System.
#include "ParseUtils.hh" // For pretty printing.
#include <cmath> // For pow(base, exp) in hash.

// Unfortunately, I can't compile standalone any longer.
#include <temp-planner-interfaces/PlannerInterfaces.h>

using namespace std;

class TrafficSM {
  public:
    virtual ~TrafficSM() = 0;
    virtual Err_t planLogic(vector<StateProblem_t> &problems,
      PlanGraph *graph, Err_t prevErr, VehicleState &vehState, Map *map, 
      Logic_params_t &params, int& currentSegmentId, int& currentLaneId,
      bool& replanInZone) = 0;
};

// A basic implementation of a TrafficSM setup with a ParsedSM.
class TrafficStop : public TrafficSM, public Environment, public System {
  private:
    ParsedSM *sam;
  public:
    TrafficStop(ParsedSM *psm) : Environment(), System(), sam(psm) {
      sam->setEnvironment(this);
      sam->setSystem(this);
    }
    ~TrafficStop() { };
};

// Example of an Environment and System (need not be in the same class)
// for running a counter or timer and displaying the result.
class timerGame : public Environment, public System {
  protected:
    int time;
    vector<bool> lastVals;
    vector<string> lastNames;
  public:
    timerGame() : Environment(), System() {
      time = 0;
    }
    ~timerGame() {}
    // for Environment
    vector<bool> getInputs(vector<string> varnames) {
      // cout << "Tiger: giving away inputs at time " << time << endl;
      vector<bool> result;
      for (vector<string>::iterator iter = varnames.begin();
        iter != varnames.end(); iter++) {
        if (*iter == "a") {
          // high bit
          result.push_back((time / 2) % 2);
        } else if (*iter == "b") {
          // low bit
          result.push_back(time % 2);
        } else {
          // fake bit (not supposed to happen)
          result.push_back(false);
        }
      }
      time ++;
      return result;
    }
    // for System
    void execute(vector<bool> outputVals, vector<string> outputNames) {
      lastVals = outputVals;
      lastNames = outputNames;
    }
    ostream & displayLastCall(ostream &os) const {
      ParseUtils::setVectorDelims("<",">","");
      os << lastNames << " = " << lastVals << endl;
      return os;
    }
};

// Allow a user to play the environment, for testing purposes.
class manualStepper: public Environment {
  private:
    istream *is;
  public:
    manualStepper(istream *input) : Environment(), is(input) { }
    ~manualStepper() { }

    // Wait for a user to enter inputs through the input stream.
    // Each character is treated as a bit.
    // '0' and ' ' are treated as zero.
    // Anything else is treated as one.
    vector<bool> getInputs() {
      vector<bool> result;
      string s;
      bool bit;
      if (!getline (*is, s)) {
        throw SMException("File is over");
      }
      for (int i = 0; i < (int)s.length(); i++) {
        bit = !(s[i] == '0' || s[i] == ' ');
        result.push_back(bit);
      }
      // User might want to know that these are correct. Or not, hey.
      //cout << "Your input was " << result << endl;
      return result;
    }
    vector<bool> getInputs(vector<string> varnames) {
      // Maybe I should prompt first? Not now, anyway.
      return getInputs();
    }
};

ostream & operator<<(ostream &os, const timerGame &tiger) {
  return tiger.displayLastCall(os);
}

#endif // _PARSED_MACHINES_HH_
// EOF ParsedMachines.hh
