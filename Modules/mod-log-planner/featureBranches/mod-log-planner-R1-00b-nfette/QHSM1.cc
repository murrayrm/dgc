/*!
 * \file QHSM1.cc
 * \brief Quantum Hierarchical State Machine Level 1
 *
 * \author Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */




/*********************************
 * Include header 
 *********************************/
#include "QHSM1.hh"
DEFINE_THIS_FILE; // Assertion debug helping trick (see Qassert)


/*********************************
 * Basic class functions 
 *********************************/

// Initial state (top state)
void QHSM1::initial(QEvent const *) 
{
	PRINT_INFO;
	qprint("TOP-%s;\n",__FUNCTION__); 
	// Init any internal variables here
	

	Q_INIT(&QHSM1::S0);  		// Initial transition, make transition to road
}

void QHSM1::destroy()
{
	qprint("QHSM1 GOT A TERMINATE SIGNAL!\nNow transistioning to S0 and exit");
	// Transition to S0
	Q_TRAN(&QHSM1::S0);
	// Now it's safe to exit
	return;
}

/******************************************************************
 * 						State methods 
 ******************************************************************/
QSTATE QHSM1::S0(QEvent const *e)
{
	PRINT_INFO; qprint("Signal: %u    ",e->sig);
	switch (e->sig) {
	 case Q_ENTRY_SIG: qprint("%s-ENTRY;\n",__FUNCTION__); return 0;
     case Q_EXIT_SIG:  qprint("%s-EXIT;\n", __FUNCTION__); return 0;
     case Q_INIT_SIG:  qprint("%s-INIT;\n", __FUNCTION__); Q_INIT(&QHSM1::S2_road); return 0;
	}
	qprint("Unrecognized signal catched: %u\n",e->sig);
    return (QSTATE)&QHSM1::top;
}

QSTATE QHSM1::S1_pause(QEvent const *e)
{
	PRINT_INFO; qprint("Signal: %u    ",e->sig);
	switch (e->sig) {
     case Q_ENTRY_SIG: qprint("%s-ENTRY;\n",__FUNCTION__); return 0;
     case Q_EXIT_SIG:  qprint("%s-EXIT;\n", __FUNCTION__); return 0;
     //case Q_INIT_SIG:  qprint("%s-INIT;\n", __FUNCTION__); return 0;
	}
	qprint("Unrecognized signal '%u' sent to top state",e->sig);
    return (QSTATE)&QHSM1::S0;
}

QSTATE QHSM1::S2_road(QEvent const *e)
{
	PRINT_INFO; qprint("Signal: %u    ",e->sig);
	switch (e->sig) {
     case Q_ENTRY_SIG: qprint("%s-ENTRY;\n",__FUNCTION__); return 0;
     case Q_EXIT_SIG:  qprint("%s-EXIT;\n", __FUNCTION__); return 0;
     //case Q_INIT_SIG:  qprint("%s-INIT;\n", __FUNCTION__); return 0;
	}
	qprint("Unrecognized signal '%u' sent to top state",e->sig);
    return (QSTATE)&QHSM1::S0;
}

QSTATE QHSM1::S3_zone(QEvent const *e) 
{
	PRINT_INFO;
	switch (e->sig) {
     case Q_ENTRY_SIG: qprint("%s-ENTRY;\n",__FUNCTION__); return 0;
     case Q_EXIT_SIG:  qprint("%s-EXIT;\n", __FUNCTION__); return 0;
     //case Q_INIT_SIG:  qprint("%s-INIT;\n", __FUNCTION__); return 0;
	}
    return (QSTATE)&QHSM1::S0;
}

QSTATE QHSM1::S4_offroad(QEvent const *e)
{
	PRINT_INFO;
	switch (e->sig) {
     case Q_ENTRY_SIG: qprint("%s-ENTRY;\n",__FUNCTION__); return 0;
     case Q_EXIT_SIG:  qprint("%s-EXIT;\n", __FUNCTION__); return 0;
     //case Q_INIT_SIG:  qprint("%s-INIT;\n", __FUNCTION__); return 0;
	}
    return (QSTATE)&QHSM1::S0;
}

QSTATE QHSM1::S5_intersect(QEvent const *e)
{
	PRINT_INFO;
	switch (e->sig) {
     case Q_ENTRY_SIG: qprint("%s-ENTRY;\n",__FUNCTION__); return 0;
     case Q_EXIT_SIG:  qprint("%s-EXIT;\n", __FUNCTION__); return 0;
     //case Q_INIT_SIG:  qprint("%s-INIT;\n", __FUNCTION__); return 0;
	}
    return (QSTATE)&QHSM1::S0;
}

QSTATE QHSM1::S6_uturn(QEvent const *e)
{
	PRINT_INFO;
	switch (e->sig) {
     case Q_ENTRY_SIG: qprint("%s-ENTRY;\n",__FUNCTION__); return 0;
     case Q_EXIT_SIG:  qprint("%s-EXIT;\n", __FUNCTION__); return 0;
     //case Q_INIT_SIG:  qprint("%s-INIT;\n", __FUNCTION__); return 0;
	}
    return (QSTATE)&QHSM1::S0;
}

QSTATE QHSM1::S7_fail(QEvent const *e)
{
	PRINT_INFO;
	switch (e->sig) {
     case Q_ENTRY_SIG: qprint("%s-ENTRY;\n",__FUNCTION__); return 0;
     case Q_EXIT_SIG:  qprint("%s-EXIT;\n", __FUNCTION__); return 0;
     //case Q_INIT_SIG:  qprint("%s-INIT;\n", __FUNCTION__); return 0;
	}
    return (QSTATE)&QHSM1::S0;
}
	










// EOF 'QHSM1.cc'
