/*!
 * \file ModLogPlanner.cc
 * \brief Source code for Modular Logic Planner
 *
 * \author Stefan Skoog
 * \date Created June 2008
 *
 * \ingroup mod-log-planner
 *
 */



/*********************************
 * Include header
 *********************************/
#include "ModLogPlanner.hh"
DEFINE_THIS_FILE; // Assertion debug helping trick (see Qassert)


/*********************************
* Initiate (static) class variables
**********************************/




/*********************************
 * Methods
 *********************************/

/**
 * @brief 	Modular Logic PLanner Constructor
 *			Creates and initiates this logical planner.
 * @return  None
 */
ModLogPlanner::ModLogPlanner() 
{
	// Call alternative init function
	init();
	return;
}

/**
 * @brief 	Modular Logic PLanner initializer
 *			Reads config file, resets internal variables, creates Quantum Hierarchical State Machine.
 * @return  (int)0 if good
 */
int ModLogPlanner::init() 
{
	qprint("\nInitializing 'ModLogPlanner'...");
	
	// Read config file and save into class variables
	readConfig();
	
	// Cycle counter, begin at zero
	cycle_counter = 0;
	
	// Init current status, complete state representation
	current_status.state       = DRIVE;
	current_status.probability = 0.99;
	current_status.flag        = PASS;
	current_status.region      = ROAD_REGION;
	current_status.planner     = RAIL_PLANNER;
	current_status.obstacle    = OBSTACLE_SAFETY;

	// Init LogicData parameters
	logicData.filteredVelocity = 0;		// Good to initialize!
	
	// Create a new StateProblem_t with a copy of current_status
	logicData.problem = new StateProblem_t;
	memcpy( logicData.problem, &current_status, sizeof(StateProblem_t) );
	
	// Create the quantum hierarchical state machine
	// Creating with 'new' avoids putting the state machine on the stack...
	qprint("\n[%s]: Passing logicData pointer %p to QHSM at creation...",__FUNCTION__,&logicData);
	qhsm = new QHSM1(logicData);
	// Init state machine; VERY IMPORTANT!
	qhsm->init();

	// Create a new IntersectionHandler
	logicData.intHandler = new CIntersectionHandling();
	
	qprint("Module 'ModLogPlanner' initiated successfully.\n\n");
	
	return 0;
}

// Destructor
ModLogPlanner::~ModLogPlanner()
{
	// Get rid of all created objects and class references
	destroy();  // Call alternate destructor
	return;
}

// Alternative destructor
void ModLogPlanner::destroy() 
{
	// Deallocate all claimed objects...
	delete qhsm;
	delete logicData.problem;
	
	return;
}

/**
 * @brief 	Modular Logic PLanner config file reader
 *			Reads config file and store data in internal variables. Prints to log.
 * @return  (int)0 if good
 */
int ModLogPlanner::readConfig() 
{
	QLOG(2,"\nReading configuration file... ");
	// Read from file into workspace
	char *path;

	path=dgcFindConfigFile("ModLogPlanner.conf","mod-log-planner");
	FILE* configFile = fopen(path,"r");
	if (configFile == NULL) {
		qprint("Warning: Couldn't read ModLogPlanner configuration file!\n");
    	QLOG(1,"\nWarning: Couldn't read ModLogPlanner configuration file!\n");
    	cerr << "ModLogPlanner couldn't read config file" << endl;
	} else {
		ConfigFile config(path);
	    config.readInto( logicData.NOMINAL_SAFETYBUFFER, 	"NOMINAL_SAFETYBUFFER");
	    config.readInto( logicData.MAX_YIELD_TIME, 			"MAX_YIELD_TIME");
	    config.readInto( logicData.DESIRED_DECELERATION, 	"DESIRED_DECELERATION");
	    config.readInto( logicData.ASTERN_MAX_TIME, 		"ASTERN_MAX_TIME");
	    config.readInto( logicData.UTURN_MAX_TIME, 			"UTURN_MAX_TIME");
	    config.readInto( logicData.ROAD_TO_OFFROAD_TIME,	"ROAD_TO_OFFROAD_TIME");
	    config.readInto( logicData.DRIVE_VEL_REQUIREMENT, 	"DRIVE_VEL_REQUIREMENT");
	    config.readInto( logicData.INTERSECT_VEL_REQUIREMENT,"INTERSECT_VEL_REQUIREMENT");
	    config.readInto( logicData.INTERSECT_STOPLINE_DIST, "INTERSECT_STOPLINE_DIST");
	    config.readInto( logicData.ZONE_VEL_REQUIREMENT,	"ZONE_VEL_REQUIREMENT");
	    
	    config.readInto( logicData.VEL_FILTER_FS_HIGH, 		"VEL_FILTER_FS_HIGH");
	    config.readInto( logicData.VEL_FILTER_FS_LOW, 		"VEL_FILTER_FS_LOW");
	    config.readInto( logicData.VEL_FILTER_RESET, 		"VEL_FILTER_RESET");
	    
	    
	    fclose(configFile);
	    QLOG(2,"done!\n")
	    
	    // Log all the parameters
	    QLOG(4,"ModLogPlanner config parameters:\n");
	    QLOG(4,"NOMINAL_SAFETYBUFFER: %f meters\n",			logicData.NOMINAL_SAFETYBUFFER);
	    QLOG(4,"MAX_YIELD_TIME: %u seconds\n",				logicData.MAX_YIELD_TIME);
	    QLOG(4,"DESIRED_DECELERATION: %f m/s^2\n",			logicData.DESIRED_DECELERATION);
	    QLOG(4,"ASTERN_MAX_TIME: %u s\n",					logicData.ASTERN_MAX_TIME);
	    QLOG(4,"UTURN_MAX_TIME: %u s\n",					logicData.UTURN_MAX_TIME);
	    QLOG(4,"ROAD_TO_OFFROAD_TIME: %u s\n",				logicData.ROAD_TO_OFFROAD_TIME);
	    
	    QLOG(4,"DRIVE_VEL_REQUIREMENT %f m/s\n",			logicData.DRIVE_VEL_REQUIREMENT);
		QLOG(4,"INTERSECT_VEL_REQUIREMENT %f m/s\n",		logicData.INTERSECT_VEL_REQUIREMENT);
		QLOG(4,"INTERSECT_STOPLINE_DIST %f m\n",			logicData.INTERSECT_STOPLINE_DIST);
	    QLOG(4,"ZONE_VEL_REQUIREMENT %f m/s\n",	 			logicData.ZONE_VEL_REQUIREMENT);
	    
	    QLOG(4,"VEL_FILTER_FS_HIGH %f units/execution\n",	logicData.VEL_FILTER_FS_HIGH);
	    QLOG(4,"VEL_FILTER_FS_LOW %f units/execution\n", 	logicData.VEL_FILTER_FS_LOW);
	    QLOG(4,"VEL_FILTER_RESET %f m/s\n", 				logicData.VEL_FILTER_RESET);
	    
	    QLOG(4,"\nConfig file read successfully.\n");
	    return 0;
	}
	return -1;
}



/**
 * @brief 	Modular Logic PLanner main method - plan logic
 *			Requires a lot of data from caller. 		
 * 
 * @return  An Err_t object (temp-planner-interfaces/PlannerInterfaces.h)
 * 			Also, the main result is returned as a StateProblem_t pushed into the vector (first argument)
 */
Err_t ModLogPlanner::planLogic(vector<StateProblem_t*> &problems, PlanGraph *graph, Err_t prevErr, 
			 VehicleState &vehState, Map *map, Logic_params_t &params, 
			 int& currentSegmentId, int& currentLaneId, bool& replanInZone ) 
{
	
	qprint("\n-->\tModLogPlanner call # %0ld", ++cycle_counter);
	
	// ** ** Now here's some few 'MAGIC' things to do before actual planning starts ** ** //
	
	// Update MapElements. If a car has stopped for long enough, it's considered as a static obstacle
	Utils::updateStoppedMapElements(map);
	
	
	// ** ** End of magic ** ** //
	
	
	// Save all incoming data into public struct
	logicData.problem = new StateProblem_t;	// Create new (empty) stateProblem to work with this cycle
	logicData.graph = graph;
	logicData.prevErr = prevErr;
	logicData.newErr  = LP_OK;
	logicData.vehState = &vehState;
	logicData.map = map;
	logicData.params = &params;
	logicData.currentSegmentId = currentSegmentId;
	logicData.currentLaneId = currentLaneId;

	// Update other important parameters
	updateLogicData(logicData);
	
	// Load new state problem with current data
	qhsm->copyStateProblem( logicData.problem, &current_status);
	//memcpy(logicData.problem, &current_status, sizeof(StateProblem_t) );
	
	// Get and print segment type received from planner and/or mission planner
	qprint("\n[%s]: segGoals: %s", __FUNCTION__, params.seg_goal.toString().c_str() );
	qprint("\n[%s]: EstopStatus: %s", __FUNCTION__, estopString[params.m_estop] );
	
    // - - - - Fabricate a signal to the state machine - - - - - //
    if (params.m_estop != EstopRun ) { 
    	qprint("\n[%s]: Got Estop, sending Q_ESTOP event",__FUNCTION__);
    	// Paused, disabled or E-stopped
		qe.sig = Q_ESTOP;
		// Set last seg type to pause so that we can resume from disable signal (work?)
		last_seg_type = SegGoals::EMERGENCY_STOP;     
    } else if ( (int)last_seg_type != (int)params.segment_type) {
    	qprint("\n[%s]: New segment, sending Q_NEW_SEGMENT event",__FUNCTION__);
    	qe.sig = Q_NEW_SEGMENT_TYPE;
    	last_seg_type = params.segment_type;
    } else if (params.seg_goal.goalID != last_goalID) {
    	// We have a new goal from missionPLanner!
    	qprint("\n[%s]: New goal from mPlanner!",__FUNCTION__);
    	qe.sig = Q_NEW_GOAL;
    	last_goalID = params.seg_goal.goalID;  // Save as old goalID
    } else if ( logicData.stoppingDistance > logicData.stoplineDistance ) {
		qprint("\n[%s]: Intersection is close enough, sending Q_STOP_INTERSECTION event",__FUNCTION__);
		// Time to let velocity planner search for the stop line!
		qe.sig = Q_STOP_INTERSECTION;
	} else if (prevErr) {
    	// We have got a problem! Figure out which one and what to do!
    	if (prevErr != last_err) {	// Print only if it's new
			qprint("\n[%s]: Planner error: %u = %s", __FUNCTION__, prevErr, plannerErrorsToString(prevErr).c_str() );
			QCONSOLE("Err: %s", plannerErrorsToString(prevErr).c_str() );
    	}
    	qe.sig = Q_USER_SIG;  	// Send ordinary signal for now
    	last_err = prevErr;		// Update as old error
    	
	} else {
		qprint("\n[%s]: No special signals, sending Q_USER_SIG event",__FUNCTION__);
		qe.sig = Q_USER_SIG;	//  Nothing happend since last execution, send standard signal to state machine
	}

	

	// Print logic data, tabbed and nice
	printLogicData(logicData);
	
	
	// Run state machine dispatcher with new signal in QEvent
	qhsm->dispatch( &qe );	
	
	// Update intersection handler consol
	logicData.intHandler->updateConsole();
	
	// Save result in internal StateProblem_t
	memcpy( &current_status, logicData.problem, sizeof(StateProblem_t) );
	
	// Copy derived stateProblems to planner's vector
	problems.push_back( logicData.problem );
		
    // Change return data when code is starting to work
	return logicData.newErr;
}


/**
 * @brief 	Modular Logic PLanner reset
 *			Re-initiates the state machine 		
 * 
 * @return  void
 */
void ModLogPlanner::resetState()
{
	qprint("\nRESET 'ModLogPlanner'...");
	
	// Re-read config file
	readConfig();
	
	// Reset cycle counter
	cycle_counter = 0;
	
	// Re-init state problem
	current_status.state       = DRIVE;
	current_status.probability = 0.99;
	current_status.flag        = PASS;
	current_status.region      = ROAD_REGION;
	current_status.planner     = RAIL_PLANNER;
	current_status.obstacle    = OBSTACLE_SAFETY;

	// Init LogicData parameters
	logicData.filteredVelocity = 0;		// Good to initialize!
	
	// Re-init state machine. Hopefully, it will not crash
	qhsm->init();
	
}

// Blaha
void ModLogPlanner::updateIntersectionHandlingConsole()
{
	return;
}


/**
 * @brief 	Modular Logic PLanner update logic data
 *			Get info from peripheral classes, modify, and save in Logic_data structure 
 * 
 * @return  non-0 for success
 */
int ModLogPlanner::updateLogicData(Logic_data_t& ld)
{
	ld.currentVelocity   = 	AliceStateHelper::getVelocityMag( *(ld.vehState) );
	ld.stoplineDistance = 	Utils::getDistToStopline( ld.params->m_path );
	if ( abs( (long long)ld.stoplineDistance ) > 1000) {	// Stoplines further than 1 km away is considered non-existent
		ld.stoplineDistance = INFINITY;
	}
	ld.laneLabel = 			Utils::getCurrentLane( *(ld.vehState), ld.graph );
	ld.obstacleDistance = 	Utils::getNearestObsDist( *(ld.vehState), ld.map, ld.laneLabel);
	ld.obstacleOnPath = 	Utils::getNearestObsOnPath( ld.params->m_path, ld.graph, *(ld.vehState) );
	ld.exitDistance = 		Utils::getDistToExit( *(ld.vehState), ld.map, ld.params->seg_goal );
	ld.stoppingDistance = 	(ld.currentVelocity * ld.currentVelocity)/(2.0*ld.DESIRED_DECELERATION);
	
	// Prevent bad pointers from messing up this data collection
	if (ld.params->m_path==NULL || ld.params->m_path==(void*)0xFFFFFFFF ) {
		ld.obsSide =	 		ld.params->m_path->nodes[0]->status.obsSideDist;
		ld.obsRear =	 		ld.params->m_path->nodes[0]->status.obsRearDist;
		ld.obsFront =	 		ld.params->m_path->nodes[0]->status.obsFrontDist;	
	} else {
		qprint("\n[%s]: PlanGraphPath not yet established",__FUNCTION__);
		ld.obsSide  = INFINITY;
		ld.obsRear  = INFINITY;
		ld.obsFront = INFINITY;
	}
	
	// Low pass filter Alice's speed and store in LogicData
	ld.velLowPass(ld.currentVelocity);

	return 1;	// Succeeded!
}

void ModLogPlanner::printLogicData(Logic_data_t& ld)
{
	qprint(	"\n[%s]: "
			"\n\tPrev Error:   %u"
			"\n\tVelocity:     %f m/s"
			"\n\tFilteredVel:  %f m/s"
			"\n\tStopline:     %f meters"
			"\n\tStopDistance: %f meters"
			"\n\tObstacle:     %f meters"
			"\n\tObstOnPath:   %f meters"
			"\n\tObsFront:     %f meters"
			"\n\tObsSide:      %f meters"
			"\n\tObsRear:      %f meters"
			"\n\tExit:         %f meters"
			"\n\tSegExit:      %f meters"
			"\n\tLane/Segment: %d/%d",
			__FUNCTION__,ld.prevErr, ld.currentVelocity, ld.filteredVelocity, 
			ld.stoplineDistance, ld.stoppingDistance, ld.obstacleDistance, ld.obstacleOnPath, 
			ld.obsFront, ld.obsSide, ld.obsRear ,ld.exitDistance, ld.params->seg_goal.distance,
			ld.laneLabel.lane, ld.laneLabel.segment 
			);
}



// EOF 'ModLogPlanner.cc'
