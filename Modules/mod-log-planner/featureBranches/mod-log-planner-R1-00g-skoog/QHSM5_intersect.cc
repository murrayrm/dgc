/*!
 * \file QHSM5_intersect.cc
 * \brief Quantum Hierarchical State Machine Level 2 - Intersection
 *
 * \author Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */





/******************************************************************
 * 						State helping methods 
 ******************************************************************/
bool QHSM1::intersectionJustVisited(Logic_data_t* ld)
{
	if (ld->last_intersection_exit < ld->INTERSECT_DEADTIME) {
		QLOG(6,"\nIntersection signal IGNORED due to deadtime");
		return true;
	}
	return false;
}




/******************************************************************
 * 						State methods 
 ******************************************************************/

/**
 * @brief 	Logic Level 2 State 
 *			Alice drives throug an intersection
 * @return  QSTATE
 */
QSTATE QHSM1::S51_drive(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY;
		 LVL2_ENTRY;
		 
		 // Change state problem rules
		 ld->problem->state = DRIVE;
		 ld->problem->planner = RAIL_PLANNER;
		 // 'Reset' velocity filter
		 ld->resetFilter(1.1);	// Reset filter to have some headroom in time before a new strategy is selected

		 return 0;
	 }
	 //case Q_STOP_INTERSECTION: // Allow super super state to change state due to new segments now!
	 case Q_STOP_INTERSECTION:	// Treat 'new' intersection signal as user signal
     case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 QLOG(7,"\tAlice is driving THROUGH an intersection\t");
    	 
    	 // If low passed velocity is under ~0.2 m/s, Alice is considered stopped, stalled, jammed.
     	 if ( ld->filteredVelocity < ld->INTERSECT_VEL_REQUIREMENT ) {
     		 QLOG(7,"Progress is too low, switching to astern");
     		 Q_TRAN(&QHSM1::S23_astern); return 0;
     	 } else if (ld->filteredVelocity > 1.8) {
     		 // We are driving pretty fast, meaning we can NOT still be in an intersection with this speed!?
     		 QLOG(7,"Progress is too HIGH to be in an intersection, switching to default segment handler.  ");
     		 return segment_type(ld->params);	// Switch to missionplanner specified road segment
     	 } else if ( since(ld->last_intersection_enter) > ld->MAX_YIELD_TIME*3) {
     		 // Very long time ago we stopped for this intersection. Change method to get out.
     		 QLOG(7,"Have been in this intersection way too long, switching to default segment handler.  ");
     		 return segment_type(ld->params);	// Switch to missionplanner specified road segment
     	 }

    	 return 0;
     }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S5_intersect;
}



/**
 * @brief 	Logic Level 2 State 
 *			Alice is stopped and is waiting in/outside an intersection
 * @return  QSTATE
 */
QSTATE QHSM1::S52_wait(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY; 
		 LVL2_ENTRY;
		 ld->problem->state = STOP_INT;		// Just tell velocity planner to stop by the stopline...
		 return 0;
	 }
	 case Q_NEW_SEGMENT_TYPE:	// Treat new segments as user signal, we are probably standing right at the change between two segments
	 case Q_STOP_INTERSECTION:	// Treat 'new' intersection signal as user signal
	 case Q_USER_SIG: { 
		 // Do 'ordinary' stuff
		 double stop_duration = since(ld->last_lvl2_tran);
		 QLOG(7,"\tAlice has stopped in intersection for %f seconds\n", stop_duration);
		 
		 /*  BROKEN 2008-07-31   =(   seg-fault in intHandler->populateWaypoints->MapPrior:operator==
		 // Run fancy intersection handler
		 CIntersectionHandling::IntersectionReturn inter_ret;  // Return value
		 inter_ret = ld->intHandler->checkIntersection( *(ld->vehState), 
				 ld->params->seg_goal, ld->params->mergingWaypoints, ld->params->readConfig);
		 
		 // Interpret the answear from the fancy IntersectionHandler
		 switch(inter_ret){
		   case CIntersectionHandling::RESET: {
			   QLOG(6,"Intersection handler reset requested");
			   break;
		   }
		   case CIntersectionHandling::WAIT_PRECEDENCE: {
			   QLOG(6,"Intersection handler is waiting for precedence");
			   break;
		   }
		   case CIntersectionHandling::WAIT_MERGING: {
			   QLOG(6,"Intersection handler is waiting for merging");
			   break;
		   }
		   case CIntersectionHandling::WAIT_CLEAR: {
			   QLOG(6,"Intersection handler is waiting for clear intersection");
			   break;
		   }
		   case CIntersectionHandling::GO: {
			   // Finally, go!
			   QLOG(6,"Intersection handler says: GO!");
			   Q_TRAN(&QHSM1::S51_drive); 
			   return 0;
			   break;
		   }
		   case CIntersectionHandling::JAMMED: {
			   QLOG(6,"Intersection handler consider us jammed");
			   break;
		   }
		 }
		 */
		 
		 // Timeout
		 if ( stop_duration > 2 ) {
			 // Boring, just drive straight out in the intersection using default planner
			 QLOG(7,"Intersection state time out! Switching to drive");
			 Q_TRAN(&QHSM1::S51_drive); 
			 return 0;
		 }
	    	 
		 return 0;	
	 }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S5_intersect;
}



/**
 * @brief 	Logic Level 2 State 
 *			Alice is stopping at the stopline. Not authorized to go on until correctly stopped at the line!
 * @return  QSTATE
 */
QSTATE QHSM1::S53_stop_at_line(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY; 
		 LVL2_ENTRY;
		 // Just tell velocity planner to stop by the stopline...
		 ld->problem->state = STOP_INT;		
		 return 0;
	 }
	 case Q_NEW_SEGMENT_TYPE:	// Treat new segments as user signal, we are probably standing right at the change between two segments
	 case Q_STOP_INTERSECTION:	// Treat 'new' intersection signal as user signal
	 case Q_USER_SIG: { 
		 // Do 'ordinary' stuff
		 double duration = since( (double)ld->last_intersection_enter);
		 QLOG(7,"\tAlice has been stopping for %f seconds", duration);
		 
		 // Actually stopped? Near the stopline? For three seconds?
		 // Use real velocity, not filtered...
		 if (  (ld->currentVelocity < ld->INTERSECT_VEL_REQUIREMENT) &&
			   (fabs(ld->stoplineDistance) < ld->INTERSECT_STOPLINE_DIST) &&
			   (duration > 3) ) {
			 QLOG(6,"\nAlice successfully stopped at the stopline...");
			 QCONSOLE("\nAlice successfully stopped at the stopline.");
			 Q_TRAN(&QHSM1::S52_wait); 
			 return 0;
		 } else if ((ld->currentVelocity < ld->INTERSECT_VEL_REQUIREMENT) &&
		   			(ld->INTERSECT_STOPLINE_DIST == INFINITY) &&
		   			(duration > 3) ) {
			 // Stopline disappeared, we are standing pretty still for at least three seconds
		   	 QLOG(6,"\nStopline disappeared, switching to merge!");
		   	 Q_TRAN(&QHSM1::S54_merge);
		   	 return 0;
		 }
		 
		 
		 // Timeout in this mode
		 if ( duration > ld->MAX_YIELD_TIME*2 ) {
			 // Time is up! Go through intersection.
			 QLOG(7,"\tWaited in intersection long enough (%f s), switching to drive",duration);
			 QCONSOLE("\nStop at line timeout!");
			 Q_TRAN(&QHSM1::S51_drive); return 0;
		 }
	    	 
		 return 0;	
	 }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S5_intersect;
}


/**
 * @brief 	Logic Level 2 State 
 *			Alice is running but needs to merge with traffic from other lanes
 * @return  QSTATE
 */
QSTATE QHSM1::S54_merge(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY; 
		 LVL2_ENTRY;

		 // Change state problem rules
		 ld->problem->state = DRIVE;
		 ld->problem->planner = RAIL_PLANNER;
		 // 'Reset' velocity filter
		 ld->resetFilter(2.0);	// Reset filter to have some headroom for stop-and-waits.
		 
		 return 0;
	 }
	 //case Q_NEW_SEGMENT_TYPE:	// Treat new segments as user signal, we are probably standing right at the change between two segments
	 case Q_STOP_INTERSECTION:	// Treat 'new' intersection signal as user signal
	 case Q_USER_SIG: { 
		 // Do 'ordinary' stuff
		 double duration = since( (double)ld->last_lvl2_tran );
		 QLOG(7,"\tAlice has been trying to merge for %f seconds\n", duration);
		 
		 /*  INT-HANDLER BROKEN 2008-07-31   =(   seg-fault in intHandler->populateWaypoints->MapPrior:operator==
		 // Run fancy intersection handler
		 CIntersectionHandling::IntersectionReturn inter_ret;  // Return value
		 inter_ret = ld->intHandler->checkIntersection( *(ld->vehState), 
				 ld->params->seg_goal, ld->params->mergingWaypoints, ld->params->readConfig);
		 
		 // Interpret the answear from the fancy IntersectionHandler
		 switch(inter_ret){
		   case CIntersectionHandling::RESET: {
			   QLOG(6,"Intersection handler reset requested");
			   break;
		   }
		   case CIntersectionHandling::WAIT_PRECEDENCE: {
			   QLOG(6,"Intersection handler is waiting for precedence");
			   break;
		   }
		   case CIntersectionHandling::WAIT_MERGING: {
			   QLOG(6,"Intersection handler is waiting for merging");
			   break;
		   }
		   case CIntersectionHandling::WAIT_CLEAR: {
			   QLOG(6,"Intersection handler is waiting for clear intersection");
			   break;
		   }
		   case CIntersectionHandling::GO: {
			   // Finally, go!
			   QLOG(6,"Intersection handler says: GO!");
			   Q_TRAN(&QHSM1::S51_drive); 
			   return 0;
			   break;
		   }
		   case CIntersectionHandling::JAMMED: {
			   QLOG(6,"Intersection handler consider us jammed");
			   break;
		   }
		 }
		 */
		 
		 // Timeout
		 if ( duration > ld->MAX_YIELD_TIME ) {
			 // Boring, just drive straight out in the intersection using default planner
			 QCONSOLE("Intersection state time out! Switching to drive");
			 QLOG(6,"Intersection state time out! Switching to drive");
			 Q_TRAN(&QHSM1::S51_drive); 
			 return 0;
		 }
	    	 
		 return 0;	
	 }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S5_intersect;
}


// EOF 'QHSM5_intersect.cc'
