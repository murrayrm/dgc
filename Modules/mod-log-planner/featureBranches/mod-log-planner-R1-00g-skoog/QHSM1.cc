/*!
 * \file QHSM1.cc
 * \brief Quantum Hierarchical State Machine Level 1
 *
 * \author Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */



/*********************************
 * Include header 
 *********************************/
#include "QHSM1.hh"
DEFINE_THIS_FILE; // Assertion debug helping trick (see Qassert)


/*********************************
 * Basic class functions 
 *********************************/

// 1 st constructor
QHSM1::QHSM1(Logic_data_t& logicData): QHSM( (QPseudoState)&QHSM1::initial ), ld(&logicData) 
{ 
	QLOG(9,"\n[%s]:    Got logicData pointer %p upon creation.",__FUNCTION__,ld);
	QLOG(9,"\n[%s]:    first logicData problem pointer: %p.",__FUNCTION__,ld->problem);
}

// 2nd constructor
QHSM1::QHSM1(QPseudoState initial): QHSM( (QPseudoState)&QHSM1::initial )
{
	// Nothing
}

/**
 * @brief 	Logic Level 1 initializer
 *			Defines and executes initial state transition.
 * 			Initializes internal state variables.
 * @return  None
 */
void QHSM1::initial(QEvent const *e) // <--- DO NOT TRY TO DEREFERENCE THIS QEvent! 
{
	QLOG(2,"\nInitializing Quantum Hierarcical State Machine");
	QLOG(9,"\nDrilling my way down to the initial state...");
	PRINT_INFO; qprint(" \tTOP-%s;",__FUNCTION__); 
	
	/***********  Init any internal variables here  ***********/
		
	// Create default rules for all level 1 states
	static StateProblem_t S1 = 	{PAUSE, 0.99, NO_PASS, ROAD_REGION,  RAIL_PLANNER, OBSTACLE_SAFETY };
	S1_pause_problem = 		&S1;
	static StateProblem_t S2 = 	{DRIVE, 0.99, NO_PASS, ROAD_REGION,  RAIL_PLANNER, OBSTACLE_SAFETY };
	S2_road_problem = 		&S2;
	static StateProblem_t S3 = 	{DRIVE, 0.99, PASS,	   ZONE_REGION,  S1PLANNER,    OBSTACLE_SAFETY };
	S3_zone_problem = 		&S3;     	
	static StateProblem_t S4 = 	{DRIVE, 0.99, OFFROAD, ROAD_REGION,  S1PLANNER,    OBSTACLE_SAFETY };
	S4_offroad_problem = 	&S4; 
	//static StateProblem_t S5 = 	{DRIVE, 0.99, NO_PASS, INTERSECTION, RAIL_PLANNER, OBSTACLE_SAFETY }; // Alternative problem
	static StateProblem_t S5 = 	{DRIVE, 0.99, NO_PASS, ROAD_REGION, RAIL_PLANNER,  OBSTACLE_SAFETY };
	S5_intersect_problem = 	&S5;
	static StateProblem_t S6 = 	{UTURN, 0.99, NO_PASS, ROAD_REGION,  S1PLANNER,    OBSTACLE_SAFETY };
	S6_uturn_problem = 		&S6;
	static StateProblem_t S7 =		{PAUSE, 0.99, NO_PASS, ROAD_REGION,  RAIL_PLANNER, OBSTACLE_SAFETY };
	S7_fail_problem = 		&S7;
	
	
	Q_INIT(&QHSM1::S0_top);  		// Initial transition, make transition to road
}

/**
 * @brief 	Quantum Hierarchical State Machine, Logic Level 1 destructor
 *			Performs a transition to the highest known state and exits.
 * @return  None
 */
void QHSM1::destroy()
{
	QLOG(8,"QHSM1 GOT A TERMINATE SIGNAL!\nNow transistioning to S0_top and exit");
	// Transition to S0_top
	Q_TRAN(&QHSM1::S0_top);
	// Now it's safe to exit
	return;
}


/******************************************************************
 * 						State helping methods 
 ******************************************************************/

// Default way to handle new segment types sent from from mission planner
QSTATE QHSM1::segment_type(Logic_params_t const *p)
{
 	switch (p->segment_type) {
 	  case SegGoals::ROAD_SEGMENT:	QLOG(6,"On Road, switch to road state.");	
 	  									Q_TRAN(&QHSM1::S2_road); return 0; 
 	  case SegGoals::PARKING_ZONE:	QLOG(6,"In parking zone, switch to zone state.");	
 	  									Q_TRAN(&QHSM1::S3_zone); return 0;
 	  case SegGoals::PREZONE:		QLOG(6,"Prezone segment type, swith to zone state"); 	
 	  									Q_TRAN(&QHSM1::S3_zone); return 0;
 	  case SegGoals::INTERSECTION:	QLOG(6,"In intersection, switch to intersection state.");	
 	  									Q_TRAN(&QHSM1::S5_intersect); return 0;
 	  case SegGoals::UTURN:			QLOG(6,"U-turn requested, switch to U-turn state.");	
 	  									Q_TRAN(&QHSM1::S6_uturn); return 0;
 	  case SegGoals::DRIVE_AROUND:	QLOG(6,"Just drive around, use offroad planning"); 	
 	  									Q_TRAN(&QHSM1::S4_offroad); return 0;
 	  case SegGoals::RESET:			QLOG(6,"RESET SIGNAL FROM mPLANNER, pause state"); 	
 	  									Q_TRAN(&QHSM1::S1_pause); return 0;
 	  case SegGoals::END_OF_MISSION: QLOG(6,"End of mission, pause state");
 	  								    QCONSOLE("End of mission, WOHOO! (pause state)");
 	  									Q_TRAN(&QHSM1::S1_pause); return 0;
 	  case SegGoals::EMERGENCY_STOP: QLOG(6,"EM-STOP SEGMENT! Switch to pause state"); 	
 	  									Q_TRAN(&QHSM1::S1_pause); return 0;
 	  case SegGoals::START_CHUTE: 	QLOG(6,"Start Chute!?, switch to pause state instead"); 	
 	  									Q_TRAN(&QHSM1::S1_pause); return 0;
 	  case SegGoals::BACK_UP:		QLOG(6,"Reversing requested by MP, swith to ???"); 	return 0; 	  										 	  
 	  case SegGoals::UNKNOWN:		QLOG(6,"Unknown segment type, switch to pause state"); 	
 	  									Q_TRAN(&QHSM1::S1_pause); return 0;
 	  default: QLOG(6,"Mission planner supplied a strange road segment type!\n"); 	 		  

 	}  
  	return (QSTATE)&QHSM1::top;
}

// Default way to handle internal Planner errors
/*
QSTATE QHSM1::segment_type(Err_t const error)
{
	
	switch (error) {
	  case :
	  case :
	  default: QLOG(5,"[%s]: Odd or undefined planner error!",__FUNCTION__);
	}
}
*/

// Default way to handle unrecognized signals
void QHSM1::defaultStateError()
{
	qprint(" \tUnrecognized signal, returning super state.")	
}


// Copy state problems from  source to target
void QHSM1::copyStateProblem(StateProblem_t* target, const StateProblem_t* source)
{
	//qprint("\n[%s]: Got StateProblem_t target pointer: %p",__FUNCTION__,target);

	// Cath evil null pointers
	if (target==NULL || target == (void*)0xFFFFFFFF ) {
		qprint("\n%s: Nughty target! NULL pointer, aborting copy...",__FUNCTION__);
		return;
	}
	if ( source==NULL ) {
		qprint("\n%s: Source was null, using default road problem. \n",__FUNCTION__);
		// Let source be an preset "Road" StateEvent and recursively return
		copyStateProblem( target, S1_pause_problem );
		return;
	}

	memcpy( target, source, sizeof(StateProblem_t) );

	// Backup solution when/if memcpy fails
	//target->state = source->state;
	//target->probability = source->probability;
	//target->flag = source->flag;
	//target->region = source->region;
	//target->planner = source->planner;
	//target->obstacle = source->obstacle;

	return;
}


// Quick complete print of a StateProblem_t, mostly used for debugging
void QHSM1::printStateProblem(const StateProblem_t* s)
{
	if (s==NULL) {
		QLOG(7,"STATE PROBLEM WAS NULL POINTER!\n")
	} else {
		QLOG(7,"state problem %p: { %s, %s, %s, %s, %s }\n", s,
				stateString[s->state], flagString[s->flag],
				regionString[s->region], plannerString[s->planner],
				obstacleString[s->obstacle] );
	}
}


// Derive time difference in seconds between input time stamp and now
int QHSM1::since(uint64_t timestamp)
{
	if ( timestamp == 0 ) return 0;
	return ( (Utils::getTime() - timestamp) / ((int)(1E6)) );
}
double QHSM1::since(double timestamp)
{
	if ( timestamp == 0 ) return 0;
	return ( (Utils::getTime() - timestamp) / 1.0E6 );
}




/******************************************************************
 * 						State methods 
 ******************************************************************/

/**
 * @brief 	Logic Level 0 State: S0_top 
 *			Absolute superstate
 * @return  QSTATE
 */
QSTATE QHSM1::S0_top(QEvent* e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_EMPTY_SIG: break;
	 case Q_ENTRY_SIG: PRINT_ENTRY; return 0;
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  PRINT_INIT; Q_INIT(&QHSM1::S1_pause); return 0;
     case Q_USER_SIG: 			return segment_type(ld->params);
     case Q_NEW_SEGMENT_TYPE:  	return segment_type(ld->params);
     case Q_ESTOP: {
    	 qprint("eSTOP TRIGGED, pausing..."); 
    	 Q_TRAN(&QHSM1::S1_pause); 
    	 return 0;
     }
     case Q_NEW_GOAL:  {
    	 QLOG(6,"New goal, reset progress filter");
    	 ld->resetFilter(ld->VEL_FILTER_RESET);
    	 return 0;
     }
     case Q_STOP_INTERSECTION: {
    	 // Intersection is very close, tell vel-planner to stop close to it!
    	 qprint("Intersection ahead!");
    	 Q_TRAN(&QHSM1::S5_intersect); return 0;
     }
     default: qprint("Dunno what to do with this signal...");  // NO RETURN VALUE ALLOWED on default!
	}
	// This is the superstate: This (only this) state should return the top state (inherited from QHSM core)
    return (QSTATE)&QHSM1::top; 
}


/**
 * @brief 	Logic Level 1 State: S1_pause 
 *			Alice is paused
 * @return  QSTATE
 */
QSTATE QHSM1::S1_pause(QEvent* e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {	
		 PRINT_ENTRY; 
	 	 LVL1_ENTRY;
		 copyStateProblem( ld->problem, S1_pause_problem ); 
		 ld->last_paused = Utils::getTime();
    	 return 0;
     }
     case Q_USER_SIG: {
    	 qprint("\t Alice has been paused for %f seconds", since( (double)(ld->last_paused) ) );
    	 return 0;
     }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
     case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
	
    return (QSTATE)&QHSM1::S0_top;
}


/**
 * @brief 	Logic Level 1 State: S2_road 
 *			Alice is driving on the road with a set of rules defined by level >=2 substates
 * @return  QSTATE
 */
QSTATE QHSM1::S2_road(QEvent* e)
{
	PRINT_STATE(e); 
	switch (e->sig) {
	 case Q_ENTRY_SIG: { 
		PRINT_ENTRY; 
	 	LVL1_ENTRY;
	 	copyStateProblem( ld->problem, S2_road_problem ); 
    	return 0;
     } 
     /*case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 qprint("\tAlice is driving...");
    	 return 0;
     }*/
     case Q_INIT_SIG: PRINT_INIT; Q_INIT(&QHSM1::S21_drive); return 0;
     case Q_EXIT_SIG: PRINT_EXIT; return 0;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S0_top;
}


/**
 * @brief 	Logic Level 1 State: S3_zone 
 *			Alice is driving on an open-zone area with a set of rules specified by level >=2 substates
 * @return  QSTATE
 */
QSTATE QHSM1::S3_zone(QEvent* e) 
{
	PRINT_STATE(e); 
	switch (e->sig) {
	 case Q_ENTRY_SIG: { 
		PRINT_ENTRY; 
	 	LVL1_ENTRY;	
	 	copyStateProblem( ld->problem, S3_zone_problem); 
    	ld->last_zone_enter = Utils::getTime();
	 	return 0;
     }
     case Q_EXIT_SIG: {
    	 PRINT_EXIT; 
    	 ld->last_zone_exit = Utils::getTime();
    	 return 0;
     }
     case Q_INIT_SIG:  PRINT_INIT; Q_INIT(&QHSM1::S31_safe); return 0;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S0_top;
}


/**
 * @brief 	Logic Level 1 State: S4_road 
 *			Alice is driving off-road with a set of rules defined by level >=2 substates
 * @return  QSTATE
 */
QSTATE QHSM1::S4_offroad(QEvent* e)
{
	PRINT_STATE(e); 
	switch (e->sig) {
	 case Q_ENTRY_SIG: {  
		PRINT_ENTRY; 
	 	LVL1_ENTRY;
	 	copyStateProblem( ld->problem, S4_offroad_problem); 
    	return 0;
     }
     case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 double duration = since( (double)ld->last_lvl1_tran );
    	 
    	 if (duration > ld->OFFROAD_MAX_TIME) {
    		 // Been offroad too long now. Switch to default segment handler!
    		 QLOG(7,"OFFROAD time limit (%d). Switching to default segment handler",ld->OFFROAD_MAX_TIME);
    		 QCONSOLE("OFFROAD time limit. Switching to default segment handler");
    		 return segment_type(ld->params);	// Switch to missionplanner specified road segment
    	 }
    	 
    	 
    	 qprint("\tAlice is OFFROAD, since %f seconds", duration);
    	 return 0;
     }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S0_top;
}


/**
 * @brief 	Logic Level 1 State: S5_intersect 
 *			Alice is in an intersection with a set of rules defined by level >=2 substates
 * @return  QSTATE
 */
QSTATE QHSM1::S5_intersect(QEvent* e)
{
	PRINT_STATE(e); 
	switch (e->sig) {
	 case Q_ENTRY_SIG: { 
		PRINT_ENTRY; 
	 	LVL1_ENTRY;
	 	copyStateProblem( ld->problem, S5_intersect_problem); 
		ld->last_intersection_enter = Utils::getTime(); // Save this timestamp
    	return 0;
     }
     /*case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 qprint("\tAlice is in an intersection...");

	// TODO
	// Make sure there is some hysterisis on the region between intersection and road region


    	 return 0;
     }*/
     case Q_EXIT_SIG: {
    	 PRINT_EXIT; 
    	 ld->last_intersection_exit = Utils::getTime();
    	 return 0;
     }
     case Q_INIT_SIG:  {
    	 PRINT_INIT;
    	 // Intersection with stopline?
    	 if (ld->stoplineDistance == INFINITY ) {
    		 // No defined/found stopline
    		 QLOG(6,"Intersection without stopline, don't stop: merge!");
    		 Q_INIT(&QHSM1::S54_merge); 
    		 return 0;
    		 
    	 } else {
    		 // There is an stopline, make follower stop right at it
    		 QLOG(6,"Intersection with stopline, stop!");
    		 Q_INIT(&QHSM1::S53_stop_at_line); 
    		 return 0;
    	 }
     }
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S0_top;
}


/**
 * @brief 	Logic Level 1 State: S6_uturn 
 *			Alice is performing an U-turn with a set of rules defined by level >=2 substates
 * @return  QSTATE
 */
QSTATE QHSM1::S6_uturn(QEvent* e)
{
	PRINT_STATE(e); 
	switch (e->sig) {
	 case Q_ENTRY_SIG: { 
		PRINT_ENTRY; 
	 	LVL1_ENTRY;
	 	copyStateProblem( ld->problem, S6_uturn_problem); 
    	return 0;
     }
     case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 
    	 // Velocity planner is supposed to signal when U-turn is done
    	 if (ld->prevErr & VP_UTURN_FINISHED) {
    		 // Switch state to... uhm... whatever mission planer recommends.
    		 qprint("vPlanner thinks the U-turn is done, swithing to default segment handler");
    		 return segment_type(ld->params);	// Switch to missionplanner specified road segment
    	 }
    	 
    	 // For some reason, if U-turn takes toooo long...
    	 int duration = since(ld->last_lvl1_tran);	// Time since entered S2 (seconds)
		 if (duration > ld->UTURN_MAX_TIME) {
    		 // Switch state to... uhm... whatever mission planer recommends.
    		 qprint("Time limit for U-turn passed! Switching to default segment handler");
    		 return segment_type(ld->params);	// Switch to missionplanner specified road segment
		 }
    	 
    	 qprint("\tAlice is doing a U-turn...");
    	 return 0;
     }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S0_top;
}


/**
 * @brief 	Logic Level 1 State: S7_fail 
 *			Alice has failed with every attempt to make progress
 * @return  QSTATE
 */
QSTATE QHSM1::S7_fail(QEvent* e)
{
	PRINT_STATE(e); 
	switch (e->sig) {
	 case Q_ENTRY_SIG: { 
		PRINT_ENTRY; 
	 	LVL1_ENTRY;
	 	copyStateProblem( ld->problem, S7_fail_problem); 
    	return 0;
     }
     case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 qprint("\tAlice has failed =(");
    	 return 0;
     }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S0_top;
}
	

// This is just a temporary fix to include other states!
// It will not work like this when the design pattern is all worked out.
#include "QHSM2_road.cc"
#include "QHSM3_zone.cc"
#include "QHSM5_intersect.cc"


// EOF 'QHSM1.cc'
