#ifndef INTERSECTIONHANDLING_HH
#define INTERSECTIONHANDLING_HH

// stf package includes
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>
#include <time.h>

// skynet, talker, and interfaces
#include "skynet/sn_msg.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/SkynetTalker.hh"
#include "map/MapElementTalker.hh"
#include "dgcutils/DGCutils.hh"
#include "sys/time.h"
#include "map/Map.hh"
#include "gcinterfaces/SegGoals.hh"
#include <trajutils/maneuver.h>
#include "temp-planner-interfaces/PlannerInterfaces.h"
#include "temp-planner-interfaces/LogClass.hh"

#define MAPSTART 6000
#define SENDCHANNEL -2
#define DEBUGCHANNEL -10

class CIntersectionHandling
{ 
public:
  enum IntersectionReturn
    { 
      RESET,
      WAIT_PRECEDENCE,
      WAIT_MERGING,
      WAIT_CLEAR,
      GO,
      JAMMED
    };

  // Functions for dyn. obstacles at intersections
  CIntersectionHandling();
  virtual ~CIntersectionHandling();

  void reset(PlanGraph* g, Map* m);
  void updateConsole();
  IntersectionReturn checkIntersection(VehicleState vehState, SegGoals currSegment, vector<PointLabel> &mergingWaypoints, bool readConfig);

private:

  struct waypoints_t {
    PointLabel waypoint;
    bool monitorWhileMerging;
    point2 crosspoint;
  };
  
  // function calls
  void populateWaypoints(PointLabel InitialWayPointEntry);
  void detectIntersectingTrafficWhileMerging(SegGoals currSegment);
  void findStoplines(PointLabel InitialWayPointEntry);
  void maintainPrecedenceList(VehicleState vehState);
  int checkFeasibility(bool &result, PointLabel entryPointLabel, PointLabel exitPointLabel);
  bool checkVisibility(VehicleState Alice,MapElement obstacle);
  int getTransitionAlongGraph(point2arr &bound, PointLabel WaypointEntry, PointLabel WaypointExit, SegGoals currSegment, bool grow, int leftright);
  int getTransition(point2arr& transition, PointLabel entryPointLabel, PointLabel exitPointLabel);
  bool crossPoint(point2arr a, point2arr b, point2 &crosspoint);
  bool inSection(point2 a, point2 b, point2 x);
  void outputLog(IntersectionReturn previousReturnValue, IntersectionReturn ReturnValue);
  void getConfigFromFile();
  double getOrientation(double velX, double velY);
  IntersectionReturn checkPrecedence();
  IntersectionReturn checkMerging(SegGoals currSegment);
  IntersectionReturn checkClearance(SegGoals currSegment);

  // Graph, map and logging
  Map* localMap;
  PlanGraph* graph;
  CLog* logIntersection;

  // Lists for checking the intersection
  PointLabel entryWaypoint;
  IntersectionReturn ReturnValue;
  vector<precedenceList_t> precedenceList;
  vector<MapElement> clearanceList, mergingList;
  vector<point2arr> clearanceLeftBound;
  vector<point2arr> clearanceRightBound;
  point2arr corridorAlongGraphRight, corridorAlongGraphLeft;
  vector<PointLabel> waypointsWithStop;
  vector<waypoints_t> waypointsNoStop;
  vector<PointLabel> waypointsEntries;
  bool jammedTimerStarted;

  // merging
  double extendMergingGap;
  unsigned long long mergingExtendWaitingTimer;

  //flags
  bool firstCheckPrecedence;
  bool firstCheckClearance;
  bool firstCheckIntersection;
  time_t timestamp;

  // logging and timing
  bool timerPrecedenceStarted;
  bool logPrecedenceCounter;
  int logLostVisibilityCounter;
  int logLostIdCounter;
  int logNearbyObstacleCounter;
  int logUnfeasibleTurnCounter;
  unsigned long long intersectionStartTime;
  double intersectionTimer;
  bool clearset;
  unsigned long long cleartime;
  int switchMergingClearCounter;

  // debug information sent to MapViewer
  int mapcounter;

  // CONSTANTS
  double DISTANCE_STOPLINE_OBSTACLE;
  double DISTANCE_STOPLINE_ALICE;
  double VELOCITY_OBSTACLE_THRESHOLD;
  double DISTANCE_OBSTACLE_THRESHOLD;
  double DISTANCE_OBSTACLE_THRESHOLD_SAVE;
  double ETA_EPS;
  bool INTERSECTION_SAFETY_FLAG;
  bool INTERSECTION_CABMODE_FLAG;
  double TRACKING_VEHICLE_SEPERATION;
  double FLICKER_FILTER_THRESHOLD;
  double INTERSECTION_TIMEOUT;
  double INTERSECTION_TIME_CLEARANCE;
  double INTERSECTION_TIME_CLEARANCE_EXTENDED;
  double INTERSECTION_TIME_MERGING;
  double INTERSECTION_GROW_CORRIDOR;
  double MAXIMUM_VELOCITY;
  double INTERSECTION_MERGING_LANE_OFFSET;
  double INTERSECTION_CABMODE_TIMEOUT;
  double INTERSECTION_MERGING_TIMEGAP;
};

#endif  // INTERSECTIONHANDLING_HH
