/*!
 * \file ModLogPlanner.cc
 * \brief Source code for Modular Logic Planner
 *
 * \author Stefan Skoog
 * \date June 2008
 *
 * \ingroup mod-log-planner
 *
 */



/*********************************
 * Include header
 *********************************/
#include "ModLogPlanner.hh"
DEFINE_THIS_FILE; // Assertion debug helping trick (see Qassert)


/*********************************
* Initiate (static) class variables
**********************************/




/*********************************
 * Methods
 *********************************/

// Constructor
ModLogPlanner::ModLogPlanner() 
{
	// Call alternative init function
	init();
	return;
}

// Alternative constructor stuff
int ModLogPlanner::init() 
{
	qprint("\nInitializing 'ModLogPlanner'...\n");
	
	// Read config file and save into class variables
	//readConfig();
	
	// Cycle counter, begin at zero
	cycle_counter = 0;
	
	// Init current status, complete state representation
	this->current_status.state       = DRIVE;
	this->current_status.probability = 0.99;
	this->current_status.flag        = PASS;
	this->current_status.region      = ROAD_REGION;
	this->current_status.planner     = RAIL_PLANNER;
	this->current_status.obstacle    = OBSTACLE_SAFETY;
	
	
	// Create the quantum hierarchical state machine
	qhsm = new QHSM1();
	// Init state machine
	
	qhsm->init();
	
	// Create an event to feed to qhsm
	qe = new QEvent();
	qe->sig = Q_USER_SIG;	// Load signal with first user definable signal 
	
	
	// Create a new IntersectionHandler
	//IntersectionHandling = new CIntersectionHandling();
	
	
	qprint("\nModule 'ModLogPlanner' initiated successfully.\n\n");
	return 0;
}

// Destructor
ModLogPlanner::~ModLogPlanner()
{
	// Get rid of all created objects and class references
	destroy();  // Call alternate destructor
	return;
}

// Alternative destructor
void ModLogPlanner::destroy() 
{
	// Deallocate all claimed objects...
	
	return;
}

// Read config file
void ModLogPlanner::readConfig() 
{
	// Read from file into workspace
	//... insert code here...
	return;
}



// * * * * * * * Plan logic * * * * * * * //
Err_t ModLogPlanner::planLogic(vector<StateProblem_t> &problems, PlanGraph *graph, Err_t prevErr, 
			 VehicleState &vehState, Map *map, 
			 Logic_params_t &params, int& currentSegmentId, int& currentLaneId,
			 bool& replanInZone) 
{
	
	
	// Print message every 10th time call here
	if ( !(++cycle_counter % 10) ) {
		qprint("ModLogPlanner call # %0ld", cycle_counter);
	}
	
	
	/*
    // Add dummy return data as state problem
    StateProblem_t problem;
    problem.state = this->current_status.state;
    problem.probability = this->current_status.probability;
    problem.flag = this->current_status.flag;
    problem.region = this->current_status.region;
    problem.planner = this->current_status.planner;
    problem.obstacle = this->current_status.obstacle;
	*/	
	
	qe->problem->state = this->current_status.state;
    qe->problem->probability = this->current_status.probability;
    qe->problem->flag = this->current_status.flag;
    qe->problem->region = this->current_status.region;
    qe->problem->planner = this->current_status.planner;
    qe->problem->obstacle = this->current_status.obstacle;

    // Fabricate a signal to the state machine
	qe->sig = Q_USER_SIG;	// User sig is first free signal to use
	
	// Run state machine dispatcher
	qhsm->dispatch(qe);
    
    // Add problem to problem queue
    problems.push_back( *(qe->problem) );
	
	//problems.push_back( current_status );
    
    
    // Change return data when code is starting to show up
	return LP_OK;
}








// EOF 'ModLogPlanner.cc'
