/*!
 * \file Qevent.cc
 * \brief Quantum Hierarchical State Machine Event Definitions
 *
 * \author AMiro Samek, modified by Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */



/*********************************
 * Includes
 *********************************/
//#include <temp-planner-interfaces/ConfigFile.hh>
//#include <temp-planner-interfaces/Console.hh>
//#include <temp-planner-interfaces/Log.hh>
#include "QEvent.hh"
#include "Qassert.hh"
DEFINE_THIS_FILE; // Assertion debug helping trick (see Qassert)

/*********************************
* Initiate (static) class variables
**********************************/
//QEvent const pkgStdEvt[] = {Q_EMPTY_SIG, Q_INIT_SIG, Q_ENTRY_SIG, Q_EXIT_SIG, USER_SIG};



/*********************************
 * Methods
 *********************************/



// EOF 'Qevent.cc'
