/**********************************
 * Header for QEvent.cc
 * Author: Miro Samek, modified by Stefan Skoog
 * Date:   July 2008
 * 
 **********************************/

#ifndef QEvent_hh
#define QEvent_hh

// Includes
#include <temp-planner-interfaces/PlannerInterfaces.h>	// State problem definition from here
#include <gcinterfaces/SegGoals.hh>
#include <map/MapPrior.hh>


/******************************************************************
 * Macros for QEvent enum->string conversion
 ******************************************************************/
#define FOR_ALL_QSIGNALS(QS)\
		QS(Q_EMPTY_SIG) QS(Q_INIT_SIG) QS(Q_ENTRY_SIG) QS(Q_EXIT_SIG)\
		QS(Q_USER_SIG) QS(Q_NEW_SEGMENT_TYPE) QS(Q_STOP_INTERSECTION)

#define DECLARE_QSIGNAL(A) A,
enum QSignal {
	FOR_ALL_QSIGNALS(DECLARE_QSIGNAL) QSIGNAL_COUNT
};

#define MAP_QSIGNAL_TO_STRING(A) #A,
const char *const QSignalStr[QSIGNAL_COUNT] = {
	FOR_ALL_QSIGNALS(MAP_QSIGNAL_TO_STRING)
};


/******************************************************************
 * Definition of QEvent
 ******************************************************************/
struct QEvent {
	QSignal sig;		// Signal of the event instance
	//StateProblem_t *problem;	// Definition of state problems 
	//Logic_params_t params;		// A lot of data from planner and missionPlanner
};

// This pre-defined array is needed AND reserved for state transitions only! 
extern QEvent pkgStdQEvent[]; 


/******************************************************************
 * Definition of 
 ******************************************************************/
typedef struct {
	//vector<StateProblem_t*> problems;	// Vector of pointers to StateProblems_t. Since I didn't get this to work due to a lot of segmentation faults, I'm going with a single problem instead,
	StateProblem_t* problem;	// Pointer to StateProblems_t
	PlanGraph* graph;
	Err_t prevErr;
	VehicleState* vehState; 
	Map* map; 
	Logic_params_t* params;
	int currentSegmentId; 
	int currentLaneId;
	bool replanInZone;

	
	
	// More important data about current vehicle state in environment 
	double currentVelocity;
	double filteredVelocity;
	double stoplineDistance;
	double obstacleDistance;
	double exitDistance;
	double obstacleOnPath;
	double stoppingDistance;
	
	double obsSide;
	double obsRear;
	double obsFront;
	
	LaneLabel laneLabel;
	
	// Some functions
	
	// Do funky funky filtering with velocity
	void velLowPass(double newVel)
	{
		// Do funky, magic, non-linear filtering
		if (newVel > filteredVelocity) {
			// New speed is higher than filter speed, update with rather short time constant
			filteredVelocity = filteredVelocity*(0.5) + newVel*(0.5);
		} else {
			// New speed is lower than filter speed, update with slooow time constant
			filteredVelocity = filteredVelocity*(0.9) + newVel*(0.1);	
		}
	}
	// Do funky funky filtering with velocity
	void resetFilter(void)
	{
		filteredVelocity = 2.0;
	}
	// Do funky funky filtering with velocity
	void resetFilter(double value)
	{
		filteredVelocity = value;
	}
	
} Logic_data_t;



#endif

// EOF 'QEvent.hh'
