/**********************************
 * Qmacros.hh
 * Macros for making debugging and such easier
 * Author: Stefan Skoog
 * Date:   July 2008
 * 
 **********************************/

#ifndef Qmacros_hh
#define Qmacros_hh


/*********************************
* MACROS
**********************************/

// Choose from {DEBUG_PRINTF, DEBUG_CONSOLE, DEBUG_LOG}
#define DEBUG_LOG

#ifdef DEBUG_PRINTF
  #define qprint(...) printf(__VA_ARGS__);
#endif
#ifdef DEBUG_CONSOLE
  //#define qprint(...) Console::addMessage(__VA_ARGS__);
  static char qprintbuff[128];	// Define char buffer to use with sprintf
  #define qprint(...) sprintf(qprintbuff,__VA_ARGS__);Console::addMessage(qprintbuff);
#endif
#ifdef DEBUG_LOG
  static char qprintbuff[128];	// Define char buffer to use with sprintf
  #define qprint(...) sprintf(qprintbuff,__VA_ARGS__);Log::getStream(1) << qprintbuff;
#endif
  
#ifndef qprint
  #define qprint(...)  	// Empty function
#endif

  
  
#ifndef QLOG
  static char qlogbuff[128];	// Define char buffer to use with sprintf
  #define QLOG(level, ... ) sprintf(qprintbuff,__VA_ARGS__);Log::getStream(level) << qlogbuff;
#endif  
  
  
// Print info in functions at breakpoints
#ifndef PRINT_INFO
  #define PRINT_INFO	 	qprint("\n\t[%s] ",__FUNCTION__);
#endif

// This is used in almost all states to print/log end of substate chain  
#ifndef NO_SUBSTATES
  #define NO_SUBSTATES		qprint("%s-INIT;\t No more substates to enter.\n", __FUNCTION__); break;
#endif
  
  
// Print info in functions at breakpoints
#ifndef PRINT_SIGNAL
  // e must be of type 'QEvent const *e'
  #define PRINT_SIGNAL(e)	qprint(" \tSignal: %s \t", QSignalStr[e->sig] );
#endif

  
// Print basic state information, unless state is trigged by null or EMPTY_SIG
#ifndef PRINT_STATE
  // e must be of type 'QEvent (const) *e'
  #define PRINT_STATE(e)	if(e && (e->sig)!=Q_EMPTY_SIG ) { qprint("\n   State: [%s] \tSignal: %s \t",__FUNCTION__,QSignalStr[e->sig] );}
#endif

  
// Print problem pointer info from QEvent and it's problem
#ifndef PRINT_POINTERS
  // e must be of type 'QEvent (const) *e'
  #define PRINT_POINTERS(e)	qprint("\n [%s]: Pointer to:\n\t QEvent: %p\n\tProblem: %p\n",__FUNCTION__,(e),(e)->problem );
#endif

  
// Print extensive info in functions at breakpoints
#ifndef PRINT_INFO_EXT
  #define PRINT_INFO_EXT 	cout <<"Time: "<<__TIME__<<" "<<__DATE__<<" in file "<<__FILE__<<" in line "<<__LINE__<<" in function \""<< __PRETTY_FUNCTION__ <<"\""<<endl;
#endif

  
  
  
#endif
// EOF 'Qmacros.hh'
