/*!
 * \file QHSM2_road.cc
 * \brief Quantum Hierarchical State Machine Level 2 - Road
 *
 * \author Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */



/**
 * @brief 	Logic Level 2 State 
 *			Alice drives
 * @return  QSTATE
 */
QSTATE QHSM1::S21_drive(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 qprint("%s-ENTRY;",__FUNCTION__);
		 
		 // Change state problem rules
		 ld->problem->state = DRIVE;
		 ld->problem->planner = RAIL_PLANNER;

		 return 0;
	 }
     case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 qprint("\tAlice is driving...");
   	 
    	 return 0;
     }
     case Q_EXIT_SIG:  qprint("%s-EXIT;\n", __FUNCTION__); return 0;
     case Q_INIT_SIG:  qprint("%s-INIT;", __FUNCTION__); Q_INIT(&QHSM1::S212_nopassing); return 0;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S2_road;
}


/**
 * @brief 	Logic Level 2 State 
 *			Alice is stopped because of some obstacle in the way
 * @return  QSTATE
 */
QSTATE QHSM1::S22_stop(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 qprint("%s-ENTRY;",__FUNCTION__); 
		 ld->problem->state = STOP_OBS;
		 last_stopobs = Utils::getTime();
		 return 0;
	 }
	 case Q_USER_SIG: { 
		 // Do 'ordinary' stuff
		 int stop_duration = (Utils::getTime()-last_stopobs)/((int)1E6);
		 qprint("\tAlice has stopped for obstacle, since %d seconds", stop_duration );
		 
		 // Stopped for obstacle too long?
		 if ( stop_duration > 8 ) {
			 if ( stop_duration > 200 ) {
				 // Silly big, some time measure errors. Reset it!
				 last_stopobs = Utils::getTime();
			 } else {
				 // Back up!
				 Q_TRAN(&QHSM1::S23_astern); return 0;
			 }
		 }
	    	 
		 return 0;	
	 }
     case Q_EXIT_SIG:  qprint("%s-EXIT;\n", __FUNCTION__); return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S2_road;
}


/**
 * @brief 	Logic Level 2 State 
 *			Alice reverses (to get away from obstacle)
 * @return  QSTATE
 */
QSTATE QHSM1::S23_astern(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 qprint("%s-ENTRY;",__FUNCTION__);  
		 ld->problem->state = BACKUP;
		 ld->problem->planner = S1PLANNER;
		 // Save time to start backing up
		 last_astern = Utils::getTime(); 
		 
		 return 0;
	 }
	 case Q_USER_SIG: { 
	    	 // Do 'ordinary' stuff
	    	 
	    	 // Measure for how long we have been backing up. IN SECONDS (Don't change this to milliseconds!)
			 int astern_duration = (Utils::getTime()-last_astern)/((int)1E6);
	    	 qprint("\tAlice is backing up, since %d seconds", astern_duration);
	    	 
			 if (astern_duration > 10) {
				 // Switch back to drive if we're backed up for too long
				 Q_TRAN(&QHSM1::S2_road); return 0;
			 }
	    	 
	    	 return 0;
	 }
     case Q_EXIT_SIG:  qprint("%s-EXIT;\n", __FUNCTION__); return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S2_road;
}


// This is just a temporary fix to include other states!
// It will not work like this when the design pattern is all worked out.
#include "QHSM21_drive.cc"


// EOF 'QHSM2_road.cc'
