/**********************************
 * QMacros.hh
 * Macros for making debugging and such easier
 * Author: Stefan Skoog
 * Date:   July 2008
 * 
 **********************************/

#ifndef QMacros_hh
#define QMacros_hh


/*********************************
* MACROS
**********************************/

// Define print buffert for string conversion
static char qprintbuff[128];	// Define char buffer to use with sprintf

// Choose from {DEBUG_PRINTF, DEBUG_CONSOLE, DEBUG_LOG}
#define DEBUG_LOG

#ifdef DEBUG_PRINTF
  #define qprint(...) printf(__VA_ARGS__);
#endif
#ifdef DEBUG_CONSOLE
  #define qprint(...) sprintf(qprintbuff,__VA_ARGS__);Console::addMessage(qprintbuff);
#endif
#ifdef DEBUG_LOG
  #define qprint(...) sprintf(qprintbuff,__VA_ARGS__);Log::getStream(1) << qprintbuff;
#endif
#ifndef qprint
  #define qprint(...)  	// Empty function, does not compile any qprints at all!
#endif

// Print stuff in the logs with level 'level'
#ifndef QLOG
  #define QLOG(level, ... ) sprintf(qprintbuff,__VA_ARGS__);Log::getStream(level) << qprintbuff;
  //#define QLOG(level, ... ) Log::getStream(1) << "\nTest logging macro, level "<< level << "says: " << __VA_ARGS__;
#endif  
  
// Prnt stuff in the traffic mini-console
#ifndef QCONSOLE
  #define QCONSOLE(...) sprintf(qprintbuff,__VA_ARGS__);Console::addMessage(qprintbuff);
#endif
  
// Print stuff to the error log in the traffic console
#ifndef QERR
  #define QERR(...) sprintf(qprintbuff,__VA_ARGS__); cerr << (qprintbuff);
#endif
  

// Print info in functions at breakpoints
#define PRINT_INFO	 		qprint("\n\t[%s] ",__FUNCTION__);


// This is used in almost all states to print/log end of substate chain  
#define NO_SUBSTATES		qprint("%s-INIT;\t No more substates to enter.\n", __FUNCTION__); \
							QCONSOLE("New state: [%s]\n",__FUNCTION__); \
							Utils::displayPlannerState(__FUNCTION__); \
							break;
  
#define PRINT_ENTRY			qprint("%s-ENTRY;",__FUNCTION__); 
#define PRINT_EXIT			qprint("%s-EXIT; ",__FUNCTION__);
#define PRINT_INIT			qprint("%s-INIT; ",__FUNCTION__);
  
// Print info in functions at breakpoints
// e must be of type 'QEvent const *e'
#define PRINT_SIGNAL(e)	qprint(" \tSignal: %s \t", QSignalStr[e->sig] );

  
// Print basic state information, unless state is trigged by null or EMPTY_SIG
// e must be of type 'QEvent (const) *e'
#define PRINT_STATE(e)	if(e && (e->sig)!=Q_EMPTY_SIG ) { qprint("\n  State: [%s] \tSignal: %s \t",__FUNCTION__,QSignalStr[e->sig] );}


  
// Print problem pointer info from QEvent and it's problem
// e must be of type 'QEvent (const) *e'
#define PRINT_POINTERS(e)	qprint("\n [%s]: Pointer to:\n\t QEvent: %p\n\tProblem: %p\n",__FUNCTION__,(e),(e)->problem );
  
// Print extensive info in functions at breakpoints
#define PRINT_INFO_EXT 		cout <<"Time: "<<__TIME__<<" "<<__DATE__<<" in file "<<__FILE__<<" in line "<<__LINE__<<" in function \""<< __PRETTY_FUNCTION__ <<"\""<<endl;

// Common entry tasks for all states in same logic level
// Use inside QHSM to be able to access ld.
#define LVL1_ENTRY			ld->last_lvl1_tran = Utils::getTime();
#define LVL2_ENTRY			ld->last_lvl2_tran = Utils::getTime();
#define LVL3_ENTRY			ld->last_lvl3_tran = Utils::getTime(); 		 		
  
  
#endif
// EOF 'QMacros.hh'
