/*!
 * \file UT_ModLogPlanner.cc
 * \brief Unit Test Source Code for Modular Logic Planner
 *
 * \author Stefan Skoog
 * \date June 2008
 *
 * \ingroup mod-log-planner
 *
 */



/*********************************
 * Includes
 *********************************/
//#include <temp-planner-interfaces/PlannerInterfaces.h>
//#include <temp-planner-interfaces/CmdArgs.hh>
#include <stdio.h>
#include <dgcutils/DGCutils.hh>	// Uses pause/sleep from here
#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <temp-planner-interfaces/ConfigFile.hh>
//#include <temp-planner-interfaces/Console.hh>
#include <temp-planner-interfaces/Log.hh>
#include "ModLogPlanner.hh"
#include "Qassert.hh"
#include "QHSM1.hh"
DEFINE_THIS_FILE; // Assertion debug helping trick (see Qassert)


/*********************************
* Initiate (static) class variables
**********************************/
using namespace std;




/*********************************
 * Main Function
 *********************************/

int main(int argc, char **args)
{
	cout << "This is just testing." << endl;

	/* Initializes Planner and start the gcmodule */
	cout << endl << "Initialize QHSM...";
	QHSM1 *qhsm = new QHSM1();
	qhsm->init();	// Initialize state
	cout << endl<<"...done initializing." << endl;
	
	// Create QEvent to feed QHSM
	QEvent* qe;
	// Fill it up with resonable data
	qe->problem->state = DRIVE;
    qe->problem->probability = 0.99;
    qe->problem->flag = PASS;
    qe->problem->region = ROAD_REGION;
    qe->problem->planner = RAIL_PLANNER;
    qe->problem->obstacle = OBSTACLE_SAFETY;
	
    
	//  Eternal main loop
	// Generate periodic events and call QHSM to evalueate them
	int i=0;	// Periodic loop counter
	while (i++<10) {

		qe->sig = i+Q_USER_SIG;
		cout <<endl<<endl<< "New event: "<< qe->sig;
		
		// Dispatch QHSM!
		qhsm->dispatch(qe);
		
		// Pause execution
		DGCusleep( (int)(2E6) );	// mikroseconds to milliseconds
			
	}

	cout 	<< endl<<endl<<endl<<endl<<"Test program done."
			<< endl<<"Cleaning up this mess..."<<endl;

	delete qhsm;

	cout << endl << "Main: Bye bye!" << endl;
    return 0;
}


// EOF 'UT_ModLogPlanner.cc'
