/*!
 * \file QHSM3_zone.cc
 * \brief Quantum Hierarchical State Machine Level 2 - Zone
 *
 * \author Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */



/**
 * @brief 	Logic Level 2 State 
 *			Alice is safe with high buffer
 * @return  QSTATE
 */
QSTATE QHSM1::S31_safe(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY;
		 LVL2_ENTRY;
		 
		 ld->problem->state = DRIVE;
		 ld->problem->obstacle = OBSTACLE_SAFETY;

		 return 0;
	 }
     case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 double duration = since( (double)ld->last_lvl2_tran );

    	 
    	 if ( ld->filteredVelocity < ld->ZONE_VEL_REQUIREMENT ) {
    		 
	    	 
	    	 // Complete planner failure?
	    	 if ( ld->prevErr & S1PLANNER_FAILED ) {
	    		 QLOG(7,"\tPlanning error, backing up");
	    		 QCONSOLE("\tPlanning error, backing up");
	    		 Q_TRAN(&QHSM1::S34_astern);   // Backup
	    		 return 0;
	    	 }
	    	 
	    	 // If planner can't reach the checkpoint end
	    	 if ( ld->prevErr & S1PLANNER_PATH_INCOMPLETE ) {
	     		 QLOG(7,"\tPlanning error, backing up");
	     		 QCONSOLE("\tPlanning error, backing up");
	     		 Q_TRAN(&QHSM1::S34_astern);   // Backup
	     		 return 0;
	     	 }
	    	 
	    	 // Planner found a path, but it is not suitable
	    	 if ( ld->prevErr & S1PLANNER_COST_TOO_HIGH ) {
	     		 QLOG(7,"\tPlanning error, backing up");
	     		 QCONSOLE("\tPlanning error, backing up");
	     		 Q_TRAN(&QHSM1::S34_astern);   // Backup
	     		 return 0;
	     	 }
	    	 
	    	 // Path is too short or simply not valid
	    	 if ( ld->prevErr & PP_NOPATH_LEN ) {
	     		 QLOG(7,"\tPlanning error, backing up");
	     		 QCONSOLE("\tPlanning error, backing up");
	     		 Q_TRAN(&QHSM1::S34_astern);   // Backup
	     		 return 0;
	     	 }
	    	 
	    	 
	    	 // Low progress and timeout?
	     	 if ( duration > ld->ZONE_SAFE_TIMEOUT ) {
	     		 QLOG(7,"\tProgress is too low, switch to agressive");
	     		 QCONSOLE("\nProgress is too low, use agressive mode");
	     		 Q_TRAN(&QHSM1::S32_agressive); 
	     		 return 0;
	      	 }	 
    	 } 
    	  

    	 qprint("\tAlice is driving in zone, safe, since %f seconds ago",duration);
    	 return 0;
     }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S3_zone;
}


/**
 * @brief 	Logic Level 2 State 
 *			Alice is agressive with small safety buffer
 * @return  QSTATE
 */
QSTATE QHSM1::S32_agressive(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY; 
		 LVL2_ENTRY;
		 
		 ld->problem->state = DRIVE;
		 ld->problem->obstacle = OBSTACLE_AGGRESSIVE;
		 
		 ld->last_agressive = GET_TIME;
		 ld->resetFilter();	
		 return 0;
	 }
	 case Q_USER_SIG: { 
		 // Do 'ordinary' stuff
		 double duration = since( (double)ld->last_lvl2_tran);
    	 
		 
		 // Did we just back up (astern)?
		 int diff = (ld->last_zone_enter - ld->last_lvl2_tran)/(int)(1E6);
		 QLOG(1,"\nZoneDiff: %d",diff);
		 if (diff > 1.5*ld->ZONE_SAFE_TIMEOUT) {
			 /* We did just backup, 'cause we didn't come directly from safe mode.
			  * Behave differently
			  * If path is still blocked, we have to come up with something else!
			  * 
			  * 
			  * 
			  * 
			  * 
			  */
			 QLOG(6,"\nZone planning failure!?");
			 // ???
			 return 0;
		 }
		 
		 
		 
		 // No progress either?
    	 // If tried for one minute AND no (filtered) velocity, switch to bare
		 if ( duration > ld->ZONE_AGRESSIVE_TIMEOUT &&
			  ld->filteredVelocity < ld->ZONE_VEL_REQUIREMENT ) {
			 QLOG(7,"\tProgress is too low, switch to bare");
     		 QCONSOLE("\nProgress is too low, use bare mode");
			 Q_TRAN(&QHSM1::S33_bare); 
			 return 0;
		 }
	
		 qprint("\tAlice is driving AGRESSIVLY in zone since %f seconds ago",duration);	 
		 return 0;	
	 }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S3_zone;
}


/**
 * @brief 	Logic Level 2 State 
 *			Alice is crazy and has nearly NO SAFETYBUFFER
 * @return  QSTATE
 */
QSTATE QHSM1::S33_bare(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY; 
		 LVL2_ENTRY;

		 ld->problem->state = DRIVE;
		 ld->problem->obstacle = OBSTACLE_BARE;
		 
		 ld->last_bare = GET_TIME;
		 ld->resetFilter();	
		 return 0;
	 }
	 case Q_USER_SIG: { 	    	 
		 // Measure for how long we have been in this state
	 	 double duration = since( (double)ld->last_lvl2_tran);
		 
	 	 
	 	 
		 // No progress either?
		 // If tried for two minutes AND no (filtered) velocity, switch to offroad!
		 if ( duration > ld->ZONE_BARE_TIMEOUT &&
			  ld->filteredVelocity < ld->ZONE_VEL_REQUIREMENT) {
			 QLOG(6,"\nZone BARE timeout (%f s), swithing to OFFROAD!",duration);
			 QCONSOLE("\nZone BARE timeout, swithing to OFFROAD!");
			 // Switch to OFFROAD mode!
			 Q_TRAN(&QHSM1::S4_offroad); 
			 return 0;
		 }
		 
		 // Default print
		 QLOG(7,"\tAlice is driving BARE in zone since %f seconds ago",duration);
		 return 0;
	 }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S3_zone;
}



/**
 * @brief 	Logic Level 2 State 
 *			Alice is backing up in zone
 * @return  QSTATE
 */
QSTATE QHSM1::S34_astern(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY;
		 LVL2_ENTRY;
		 
		 ld->problem->state = BACKUP;
		 ld->problem->planner = S1PLANNER;
		 ld->problem->region = ROAD_REGION;	// Have to trick zone planner to force backup in zone
		 // Issue a replan
		 ld->params->planFromCurrPos = true;
		 ld->replanInZone = true;
		 
		 ld->resetFilter();	
		 return 0;
	 }
     case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 double duration = since( (double)ld->last_lvl2_tran );

    	/*
    	 // Failure?
    	 if ( ld->prevErr & S1PLANNER_FAILED ) {
    		 // Do something about it
    		 return 0;
    	 }
    	*/
    	 
    	 // Timeout or done
    	 if ( (ld->prevErr & VP_BACKUP_FINISHED) ||
    		  (ld->prevErr & VP_UTURN_FINISHED)  ||
    	      (duration > ld->ASTERN_MAX_TIME)
    	      ) {
    		 QLOG(6,"\tBackup ");
    		 if (ld->prevErr == VP_BACKUP_FINISHED) {
    			 QLOG(6,"finished");
    		 }
    		 if (duration > ld->ASTERN_MAX_TIME) {
    			 QLOG(6,"timeout");
    		 }
    		 // Tell pPlanner to replan
    		 ld->params->planFromCurrPos = true;
    		 
    		 QLOG(6,", swithing to zone agressive"); 
    		 	 
    		 ld->problem->region = ZONE_REGION; // Restore correct region flag
    		 Q_TRAN(&QHSM1::S32_agressive); 
    		 return 0;
     	 } 

    	 qprint("\tAlice is backing up in zone, since %f seconds ago", duration);
    	 return 0;
     }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S3_zone;
}

// EOF 'QHSM3_zone.cc'
