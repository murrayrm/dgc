/**********************************
 * Header for QEvent.cc
 * Author: Miro Samek, modified by Stefan Skoog
 * Date:   July 2008
 * 
 **********************************/

#ifndef QEvent_hh
#define QEvent_hh

// Includes
#include <temp-planner-interfaces/PlannerInterfaces.h>	// State problem definition from here


typedef unsigned int QSignal;  // Define QSignal

enum {	// Standard QSignals  
	Q_EMPTY_SIG,
	Q_INIT_SIG,
	Q_ENTRY_SIG,
	Q_EXIT_SIG,
	Q_USER_SIG
};

struct QEvent {
	QSignal sig;		// Signal of the event instance
	StateProblem_t *problem;
};

const QEvent pkgStdEvt[] = {
	{Q_EMPTY_SIG, NULL}, 
	{Q_INIT_SIG, NULL}, 
	{Q_ENTRY_SIG, NULL}, 
	{Q_EXIT_SIG, NULL}, 
	{Q_USER_SIG, NULL} 
};





#endif

// EOF 'QEvent.hh'
