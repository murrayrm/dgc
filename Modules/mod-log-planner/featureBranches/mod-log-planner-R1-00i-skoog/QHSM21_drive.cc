/*!
 * \file QHSM21_drive.cc
 * \brief Quantum Hierarchical State Machine, Logic Level 3 - (Road)Drive
 *
 * \author Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */



/**
 * @brief 	Logic Level 3 State 
 *			Alice drives with passing allowed
 * @return  QSTATE
 */
QSTATE QHSM1::S211_passing(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY;
		 LVL3_ENTRY;
		 // Change state problem rules
		 ld->problem->flag = PASS;
		 ld->resetFilter();	// Reset filter to have some headroom in time before a new strategy is selected
		 return 0;
	 }
     case Q_USER_SIG: { 
    	 // Do 'ordinary' stuff
    	 
    	 if (ld->currentLane.lane != ld->desiredLane.lane) {
    		 // We're merging to the "other" lane
    		 QCONSOLE("\nMerging to the other lane (%d -> %d",ld->desiredLane.lane, ld->currentLane.lane);
    	 }    	 
    	 
    	 if (ld->currentLane.lane > 1) {
    		 // In the 'other' lane!
    		 // Make sure we leave passign mode when the obstacle disappears!
    		 if (ld->obstacleInLane == INFINITY) {
    			 // Obstacle gone! Switch bak to driving in the right/correct lane
    			 QLOG(7,"Passing an obstacle, switch back to right lane");
    			 Q_TRAN(&QHSM1::S212_nopassing); 
    			 return 0;
    		 }
    	 }
    	 
    	 
	   	 // Passing not successful?
       	 // If filtered velocity is under ~0.2 m/s, Alice is considered stopped.
    	 if ( ld->filteredVelocity < ld->DRIVE_VEL_REQUIREMENT ) {
    		 qprint("Progress is too low, trying to back up...");
    		 Q_TRAN(&QHSM1::S23_astern); 
    		 return 0;
    	 }

    	 // Default printout
    	 QLOG(7,"\tAlice is driving with passing allowed...");
    	 return 0;
     }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
	 case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S21_drive;
}



/**
 * @brief 	Logic Level 3 State 
 *			Alice is driving, passing is NOT allowed
 * @return  QSTATE
 */
QSTATE QHSM1::S212_nopassing(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY; 
		 LVL3_ENTRY;
		 
		 ld->problem->flag = NO_PASS;
		 ld->resetFilter();	// Reset filter to have some headroom in time before a new strategy is selected
		 return 0;
	 }
	 case Q_USER_SIG: { 
		 // Do 'ordinary' stuff

		 // Try this, cool?
		 // If level1 duration is big, and level2 duration small, we must have stopped and/or backed up already
		 // Alt. take: If difference between these times are substantial, we have a potential cycle
		 int diff = (ld->last_lvl2_tran - ld->last_lvl1_tran)/(int)(1E6); 
		 if ( diff > 2 ) {
			 QLOG(7,"\nlvl1-lvl2 difference is %d seconds", diff);

			 /* We entered this state from another level 2 state... possible cycle!
			  * Behave different!
			  * 
			  * We are most likely in this scenario:
			  * Alice stopped for obstacle, waited, backed up, and switched into drive again (here).
			  * If sensors have not chenged their mind about the obstacle (it's for real),
			  * it's probably better to drive around it in some other way, 
			  * which implies a replan request through mission planner. 
			  */

			 /* First, check if path planner still think it's a bad idea to drive forward.
			  * If no errors show up, the obstacle is probably gone.
			  * Empirical studies show that these three error must be combined to be sure
			  * the pat is blocked  
			  */ 
			 if ( ld->prevErr & PP_COLLISION_SAFETY ) {
				 // Obvious blocked path!
				 if (ld->params->seg_goal.illegalPassingAllowed) {
					 // Pasing is allowed according to mPlanner, switch to passing mode
					 QLOG(7,"\nLane is blocked, passing allowed, switching to passing!");	 
					 Q_TRAN(&QHSM1::S211_passing);
					 return 0;
					 
				 } else {
					 // Ask mission planner for a replan (failure error)
					 QLOG(6,"\nLane blocked, not allowed to pass; mission failure!");
					 ld->newErr |= LP_FAIL_MPLANNER_LANE_BLOCKED;
					 return 0;
				 }
			 }
			 
			 
		 } else if (ld->obstacleOnPath < 1.2*ld->ROAD_OBS_DISTANCE) {
			 // Almost obstacle stopping distance?
			 // Tell missionplanner that we won't make it in this lane
			 QCONSOLE("\nThis path is not applicable, mission failure: 'lane blocked'");
			 QLOG(6,"\nThis path is not applicable, mission failure: 'lane blocked'");
			 ld->newErr |= LP_FAIL_MPLANNER_LANE_BLOCKED;
			 return 0;
			 
		 } else if (ld->obstacleOnPath < ld->ROAD_OBS_DISTANCE) {
			 // Is there a new obstacle on our path, too close?
			 QCONSOLE("\nObstacle in our path, stopping!");
			 QLOG(6,"\tObstacle in our path, stopping!");
			 // We're not allowed to pass. 
			 // Pause and see if obstacle disappear
			 Q_TRAN(&QHSM1::S22_stop); 
			 return 0;
		 }
		 
		 
	   	 // No passing not successful to make progress? Try passing if it's allowed
       	 // If low passed velocity is under ~0.2 m/s, Alice is considered stopped.
    	 if ( ld->filteredVelocity < ld->DRIVE_VEL_REQUIREMENT ) {
    		 QLOG(6,"\nProgress is to low");
    		 // Are we allowed to pass according to mPlanner?
    		 if (ld->params->seg_goal.illegalPassingAllowed) {
	    		 QLOG(6,", switching to pass mode");
	    		 Q_TRAN(&QHSM1::S211_passing); return 0;	    		 
	    	 } else {
	    		 QLOG(6,", not allowed to pass, sending failure: 'road blocked' to mPlanner.");
	      		 // Set error message to mPlanner
	       		 ld->newErr |= LP_FAIL_MPLANNER_ROAD_BLOCKED;	
	    	 }
    	 }
		 
    	 // Default printout
		 QLOG(7,"\tAlice is driving, but is not allowed to pass");
		 return 0;	
	 }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S21_drive;
}



/**
 * @brief 	Logic Level 3 State 
 *			Alice is driving and is allowed to both pass and reverse
 * @return  QSTATE
 */
QSTATE QHSM1::S213_passingreverse(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY;  
		 LVL3_ENTRY;
		 
		 ld->problem->flag = PASS_REV;
		 ld->resetFilter(3.0);	// Reset filter to have some headroom in time before a new strategy is selected
		 return 0;
	 }
	 case Q_USER_SIG: { 
	     // Do 'ordinary' stuff
		 
		 
		 // Did we arrive here by a transition from astern? 
		 int diff = (ld->last_astern - ld->last_lvl1_tran)/(int)(1E6); 
		 if ( abs(diff) < 2 ) {
			 QLOG(7,"\nlvl1-lvl2 difference is %d seconds", diff);
			 QLOG(7,"\nEnded up here from astern!?");

			 // Path planner can still not find a legal path
			 if ( ld->prevErr & PP_NOPATH_LEN ) {
				 // Ask mission planner for a replan (failure error)
				 QLOG(6,"\nThis path is not applicable, mission failure: road blocked!");
				 QCONSOLE("\nMission failure: Road blocked!");
				 ld->newErr |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
				 return 0;
			 }
		 }
	 
    	 if (ld->currentLane.lane != ld->desiredLane.lane) {
    		 // We're merging to the "other" lane
    		 QCONSOLE("\nMerging to the other lane (%d -> %d",ld->desiredLane.lane, ld->currentLane.lane);
    	 } 
    	 
    	 if (ld->currentLane.lane > 1) {
    		 // In the 'other' lane!
    		 // Make sure we leave passign mode when the obstacle disappears!
    		 if (ld->obstacleInLane == INFINITY) {
    			 // Obstacle gone! Switch bak to driving in the right/correct lane
    			 QLOG(7,"Passing an obstacle, switch back to right lane");
    			 Q_TRAN(&QHSM1::S212_nopassing); 
    			 return 0;
    		 }
    	 }
    	 
    	 
	   	 // Passingreverse not successful?
       	 // If low passed velocity is under ~0.2 m/s, Alice is considered stopped.
    	 if ( ld->filteredVelocity < ld->DRIVE_VEL_REQUIREMENT ) {
    		 QLOG(7,"\nProgress is too low");

    		 // We just visited astern mode, so tell mPlaner we've failed to get around this obstacle
	    	 if ( since(ld->last_astern) > (2*ld->ASTERN_MAX_TIME) ) {
	    		 qprint(", sending failed to mPlanner.");
	    		 QLOG(6,"\nThis path is not applicable, mission failure: 'road blocked'");
	    		 // Set error message (to mPLanner?)
	    		 ld->newErr |= LP_FAIL_MPLANNER_ROAD_BLOCKED;	
	    	 } else {
	    		 qprint(", trying to back up...");
	    		 Q_TRAN(&QHSM1::S23_astern); return 0;
	    	 }
    	 }
    	 
    	 
    	 // Tried this too many times? 
    	 // If we have been stuck in 2_road states too long, switch to offroad!
    	 if ( since(ld->last_lvl1_tran) > ld->ROAD_TO_OFFROAD_TIME ) {  	// Compare time since entered S2 (seconds)
    		 qprint("State 'road' has not made enough progress for %d seconds, use OFFROAD!",ld->ROAD_TO_OFFROAD_TIME);
    		 Q_TRAN(&QHSM1::S4_offroad); return 0;
    	 }
    	 
    	 // Print what's happening if no special events occored
    	 QLOG(7,"\tAlice is driving with passing reverse allowed");
    	 return 0;
	 }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S21_drive;
}




// EOF 'QHSM2_road.cc'
