/*!
 * \file QHSM1.hh
 * \brief Quantum Hierarchical State Machine Level 1
 *
 * \author Miro Samek, modified by Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */


#ifndef QHSM1_hh
#define QHSM1_hh


/*********************************
 * Includes
 *********************************/
//#include <temp-planner-interfaces/ConfigFile.hh>
//#include <temp-planner-interfaces/Console.hh>
#include <temp-planner-interfaces/Log.hh>
#include <temp-planner-interfaces/Utils.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <temp-planner-interfaces/AliceStateHelper.hh>
//#include <dgcutils/DGCutils.hh>	// Uses pause/sleep from here
#include "IntersectionHandling.hh"
#include "Qassert.hh"
#include "QMacros.hh"
#include "QEvent.hh"
#include "QHSM.hh"
#include <stdlib.h>
#include <stdio.h>


/*********************************
* MACROS
**********************************/
// Have a look in QMacros.hh




/*********************************
 * Class define
 *********************************/
class QHSM1 : public QHSM {
public:
	/* 	Constructor, takes no arguments, but calls base class 
	 	with member function pointer to initial state.
	 	Two different constructors are more convenient, one for creating class and one for inheriting */ 
	QHSM1(Logic_data_t& logicData); 
	QHSM1(QPseudoState initial);
	virtual ~QHSM1() { QHSM1::destroy(); }
	
	// Send initializer to QHSM core
	void record(QPseudoState initial) { QHSM::record(initial);}
	
	void printStateProblem(const StateProblem_t* s);
	void copyStateProblem(StateProblem_t* target, const StateProblem_t* source);
	float since(uint64_t timestamp);
	//float since(float    timestamp);
	float distance(Logic_data_t* ld, point2 from);

	
protected:
	void destroy(); // Alternative destructor 
		
	void initial(QEvent const *e); 	// Initial (startup) state
		// The list of different available level 0/1-states
		QSTATE S0_top(QEvent *e);		// Superstate
		  QSTATE S1_pause(QEvent *e);	
		  QSTATE S2_road(QEvent *e);	
			 // This is just a temporary fix to include other states!
			 // It will not work like this when the design pattern is all worked out.
		     #include "QHSM2_road.hh"
			   #include "QHSM21_drive.hh"
		  QSTATE S3_zone(QEvent *e);	
			 #include "QHSM3_zone.hh" 
		  QSTATE S4_offroad(QEvent *e);	
		  QSTATE S5_intersect(QEvent *e);
             #include "QHSM5_intersect.hh"
		  QSTATE S6_uturn(QEvent *e);				
		  QSTATE S7_fail(QEvent *e);		


	// Helping methods
	void defaultStateError(void);
	point2 getPos(Logic_data_t* ld);
	QSTATE segment_type(Logic_params_t const *p);

public:
	// Pointer to data structure that contains all we need
	Logic_data_t* ld;
	bool intersectionJustVisited(Logic_data_t* ld);
	
protected:	
	// Define provisional state problems for each level 1 state
	StateProblem_t* S1_pause_problem;
	StateProblem_t* S2_road_problem;
    StateProblem_t* S3_zone_problem;
    StateProblem_t* S4_offroad_problem;
    StateProblem_t* S5_intersect_problem;
    StateProblem_t* S6_uturn_problem;
    StateProblem_t* S7_fail_problem;

		  
private:           
	

};





#endif
// EOF 'QHSM1.hh'
