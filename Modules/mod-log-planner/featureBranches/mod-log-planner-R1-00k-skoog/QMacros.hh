/**********************************
 * QMacros.hh
 * Macros for making debugging and programming easier and more proper looking
 * Author: Stefan Skoog
 * Date:   July 2008
 * 
 **********************************/

#ifndef QMacros_hh
#define QMacros_hh

#include <string>
#include <cstring>

/*********************************
* State Machine helping MACROS
**********************************/

// Define print buffert for string conversion
static char qprintbuff[128];	// Define char buffer to use with sprintf

// Choose from {DEBUG_PRINTF, DEBUG_CONSOLE, DEBUG_LOG}
#define DEBUG_LOG

#ifdef DEBUG_PRINTF
  #define qprint(...) printf(__VA_ARGS__);
#endif
#ifdef DEBUG_CONSOLE
  #define qprint(...) sprintf(qprintbuff,__VA_ARGS__);Console::addMessage(qprintbuff);
#endif
#ifdef DEBUG_LOG
  #define qprint(...) sprintf(qprintbuff,__VA_ARGS__);Log::getStream(1) << qprintbuff;
#endif
#ifndef qprint
  #define qprint(...)  	// Empty function, does not compile any qprints at all!
#endif

// Print stuff (with printf-formatting) in the logs with level 'level'
#ifndef QLOG
  #define QLOG(level, ... ) sprintf(qprintbuff,__VA_ARGS__);Log::getStream(level) << qprintbuff;
#endif  
  
// Prnt stuff (with printf-formatting) in the traffic mini-console
#ifndef QCONSOLE
  #define QCONSOLE(...) sprintf(qprintbuff,__VA_ARGS__);Console::addMessage(qprintbuff);
#endif
  
// Print stuff (with printf-formatting) to the error log in the traffic console
#ifndef QERR
  #define QERR(...) sprintf(qprintbuff,__VA_ARGS__); cerr << (qprintbuff);
#endif
  
// Use this function to get current time
#define GET_TIME			Utils::getTime()

// Print function name
#define PRINT_INFO	 		qprint("\n\t[%s] ",__FUNCTION__);
// Print extensive info in functions at breakpoints
#define PRINT_INFO_EXT 		cout <<"Time: "<<__TIME__<<" "<<__DATE__<<" in file "<<__FILE__<<" in line "<<__LINE__<<" in function \""<< __PRETTY_FUNCTION__ <<"\""<<endl;

// Get state name, which is the function name, but for convenience make it fixed-width!
#define STATE_NAME_LENGTH 20		// 'S213_passingreverse' = 19 chars
#define SIGNAL_NAME_LENGTH 20		// 'Q_STOP_INTERSECTION' = 19 chars
static char namebuff[MAX(STATE_NAME_LENGTH,SIGNAL_NAME_LENGTH)+1];		// One more is for string termination
static char statenamebuff[STATE_NAME_LENGTH+1];		// One more is for string termination
#define FIX_NAME_WIDTH(string,maxlen)  strncpy( (char*)memset(namebuff,' ',maxlen), string, MIN(strlen(string),maxlen) )
//#define STATE_NAME  strncpy( (char*)memset(statenamebuff,' ',STATE_NAME_LENGTH), __FUNCTION__, MIN(strlen(__FUNCTION__),STATE_NAME_LENGTH) )
#define STATE_NAME  strncpy( (char*)memset(statenamebuff,' ',STATE_NAME_LENGTH),__FUNCTION__, MIN(strlen(__FUNCTION__),STATE_NAME_LENGTH) )

/*
char* makeConstLen(const char* input, int len) {
	int done=0;
	for(int i=0; i<len; i++) {			// Run through ALL buffer
		if (input[i]=='\0') done=1;		// Stop copying input char if string terminator is found
		if (done) {						
			statenamebuff[i] = ' ';		// Fill up with white spaceds if done
		} else {
			statenamebuff[i] = input[i]; // Copy input string
		}
	}
	statenamebuff[len]='\0';	// Terminate outpu string
	return statenamebuff;
}
#define STATE_NAME  makeConstLen(__FUNCTION__,STATE_NAME_LENGTH)
*/

    
// This is used in almost all states to print/log end of substate chain, as well as display state info in mapViewer 
#define NO_SUBSTATES		PRINT_INIT; QLOG(6,"No more sub-states to enter.\n"); \
							QCONSOLE("New state: [%s]\n",__FUNCTION__); \
							Utils::displayPlannerState(__FUNCTION__); \
							break;
  
#define PRINT_ENTRY			QLOG(2,"State ENTRY;\t"); 
#define PRINT_EXIT			QLOG(2,"State EXIT; \t");
#define PRINT_INIT			QLOG(2,"State INIT; \t");
  
// Print info in functions at breakpoints
// e must be of type 'QEvent *e'
#define PRINT_SIGNAL(e)	qprint(" \tSignal: %s \t", QSignalStr[e->sig] );

  
// Print basic state information, unless state is trigged by null or EMPTY_SIG
// e must be of type 'QEvent (const) *e'
#define PRINT_STATE(e)	if(e && (e->sig)!=Q_EMPTY_SIG ) { qprint("\n  @ %s %s\t",STATE_NAME, FIX_NAME_WIDTH(QSignalStr[e->sig],SIGNAL_NAME_LENGTH) );}


// Print problem pointer info from QEvent and it's problem
// e must be of type 'QEvent *e'
#define PRINT_POINTERS(e)	qprint("\n [%s]: Pointer to:\n\t QEvent: %p\n\tProblem: %p\n",__FUNCTION__,(e),(e)->problem );

// Common entry tasks for all states in same logic level
// Use only inside QHSM to be able to access ld.
#define LVL1_ENTRY			ld->last_lvl1_tran = GET_TIME; ld->last_lvl1_pos = getPos(ld);
#define LVL2_ENTRY			ld->last_lvl2_tran = GET_TIME; ld->last_lvl2_pos = getPos(ld);
#define LVL3_ENTRY			ld->last_lvl3_tran = GET_TIME; ld->last_lvl3_pos = getPos(ld);
  
  
#endif
// EOF 'QMacros.hh'
