/*!
 * \file QHSM2_road.cc
 * \brief Quantum Hierarchical State Machine Level 2 - Road
 *
 * \author Stefan Skoog
 * \date July 2008
 *
 * \ingroup mod-log-planner
 *
 */



/**
 * @brief 	Logic Level 2 State 
 *			Alice drives
 * @return  QSTATE
 */
QSTATE QHSM1::S21_drive(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		PRINT_ENTRY;
	 	LVL2_ENTRY;
		// Change state problem rules
		ld->problem->state = DRIVE;
		ld->problem->planner = RAIL_PLANNER;
		ld->problem->obstacle = OBSTACLE_SAFETY;
		return 0;
	 }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG: {
    	 PRINT_INIT;
    	 if (ld->params->seg_goal.illegalPassingAllowed) {
    		 qprint(" Passing allowed ");
    		 Q_INIT(&QHSM1::S211_passing); return 0;
    		 return 0;
    	 } else {    	 
    		 qprint(" Passing not allowed ");
    		 Q_INIT(&QHSM1::S212_nopassing);
    		 return 0;
    	 }
     }
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S2_road;
}


/**
 * @brief 	Logic Level 2 State 
 *			Alice is stopped because of some obstacle in the way
 * @return  QSTATE
 */
QSTATE QHSM1::S22_stop(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		PRINT_ENTRY; 
	 	LVL2_ENTRY;
		ld->problem->state = STOP_OBS;
		ld->last_stopobs = GET_TIME;
		return 0;
	 }
	 case Q_USER_SIG: { 
		 // Do 'ordinary' stuff
		 float stop_duration = since( ld->last_stopobs );
		 
		 /* ToDo:
		  * Why did we stop? (read error signals)
		  * What can we do about it? (replan path, switch path strategy/planner)
		  * Watch out for limit cycles
		  */
		 
		 if ( ld->currentVelocity < ld->DRIVE_VEL_REQUIREMENT ) {
			 // Alice has stopped
			 QLOG(7," Alice has stopped.");
			 
			 // Path still blocked?
			 if ( (ld->prevErr & PP_COLLISION_BARE ) || 
				  (ld->prevErr & PP_NOPATH_LEN)
			 	 ) {
				 QLOG(7,"Still stuff in the way to drive, switch to examine mode\t");
				 Q_TRAN(&QHSM1::S24_examine); 
				 return 0;
			 }
			 
		 } else {
			 QLOG(7," Alice is stopping...");
		 }
		 
		 // Stopped for obstacle too long?
		 if ( stop_duration > ld->MAX_YIELD_TIME ) {
			 // Back up!
			 QLOG(7,"StopObs timeout (%d seconds), backing up.", ld->MAX_YIELD_TIME );
			 Q_TRAN(&QHSM1::S23_astern); 
			 return 0;
		 }
		 
		 // Default printout
		 QLOG(7,"Alice has stopped for obstacle, since %f seconds", stop_duration );
		 return 0;	
	 }
	 
		 
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S2_road;
}


/**
 * @brief 	Logic Level 2 State 
 *			Alice reverses (to get away from obstacle)
 * @return  QSTATE
 */
QSTATE QHSM1::S23_astern(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		 PRINT_ENTRY;  
		 LVL2_ENTRY;
		 ld->problem->state = BACKUP;
		 ld->problem->planner = S1PLANNER;
		 // Save time to start backing up
		 ld->last_astern = GET_TIME; 
		 // Save the position we startet to back up from
		 QLOG(7,"This is %f meters from the last position we backed up from",distance(ld, ld->last_astern_pos) );
		 // ToDo: If we end up trying to back up from same place as last time, do something else!
		 ld->last_astern_pos = getPos(ld);
		 
		 return 0;
	 }
	 case Q_USER_SIG: { 

    	 // Measure for how long we have been backing up. IN SECONDS (Don't change this to milliseconds!)
		 float astern_duration = since( ld->last_astern);
    	 
    	 // We are either done with backup (signal from pPlanner), or timed out. Switch back to drive.
    	 if ( (ld->prevErr & VP_BACKUP_FINISHED) ||
    		  (ld->prevErr & VP_UTURN_FINISHED)  ||
    	      (astern_duration > ld->ASTERN_MAX_TIME)
    	      ) {
    		 QLOG(6,"Backup ");
    		 if ( (ld->prevErr == VP_BACKUP_FINISHED) ||
    		      (ld->prevErr & VP_UTURN_FINISHED)
    			 ) {
    			 QLOG(6,"finished");
    		 } 
    		 if (astern_duration > ld->ASTERN_MAX_TIME) {
    			 QLOG(6,"timeout");
    		 }
    		 // Tell pPlanner to replan
    		 ld->params->planFromCurrPos = true;
    		 
    		 QLOG(6,", swithing to "); 
    		 
    		 // Switch back to drive if we're backed up for too long
	    	 // ...depending what we are allowed to do...
			 if (ld->params->seg_goal.illegalPassingAllowed) {
				 QLOG(6,"passingreverse");
				 Q_TRAN(&QHSM1::S213_passingreverse);
	    		 return 0;
	    	 } else {    	 
	    		 QLOG(6,"nopassing");
	    		 Q_TRAN(&QHSM1::S212_nopassing);
	    		 return 0;
	    	 }
    	 } else if ( ld->prevErr & PP_NOPATH_LEN ) {
    		 // We have a problem with the path planner!
    		 QLOG(6,"S1planner is stupid, it can't find a way out of here. Switch to offroad agressive mode! ");
    		 // May need this to get out of here
    		 ld->problem->obstacle = OBSTACLE_AGGRESSIVE;
    		 ld->params->planFromCurrPos = true;
    		 ld->replanInZone = true;
    		 Q_TRAN(&QHSM1::S4_offroad);
    		 return 0;
    	 }
	   	 
    	 QLOG(6,"Alice is backing up, since %f seconds", astern_duration); 
    	 return 0;
	 }
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S2_road;
}




/**
 * @brief 	Logic Level 2 State 
 *			Alice is stopped because of some obstacle in the way
 * @return  QSTATE
 */
QSTATE QHSM1::S24_examine(QEvent *e)
{
	PRINT_STATE(e);
	switch (e->sig) {
	 case Q_ENTRY_SIG: {
		PRINT_ENTRY; 
	 	LVL2_ENTRY;
		
	 	// Which planner and strategy is best to use here?
	 	ld->problem->planner = RAIL_PLANNER;	
	 	ld->problem->flag = PASS_REV;	// Well, reversing may not be allowed from mission planner, but it's the best way to ger rail planner to really find new paths around obstacles!
	 	ld->problem->obstacle = OBSTACLE_AGGRESSIVE;  // Get intimate with obstacles!
		
		return 0;
	 }
	 case Q_USER_SIG: { 
		 // Do 'ordinary' stuff
		 float duration = since( ld->last_lvl2_tran );
		 
		 /* ToDo:
		  * Make Alice look around to eliminate groundstrikes, spurious obstacles and that kind of stuff
		  */
		 
		 if ( (ld->prevErr & PP_COLLISION_BARE) ||
			  (ld->prevErr & PP_NOPATH_LEN)
			 ) {
			 ld->params->planFromCurrPos = true;
			 ld->replanInZone = true;
		 }
		 
		 
		 // Actually making progress in this state? Switch back to default segment strategy
		 if ( distance(ld, ld->last_lvl2_pos) > ld->EXAMINE_MAX_DISTANCE ) {
			 QLOG(9,"\nMoved %f meters. ",distance(ld, ld->last_lvl2_pos));
			 QLOG(7,"\nMaking too much progress (%d m), switching to default segment",ld->EXAMINE_MAX_DISTANCE);
		 }
		 
		 // State timeout
		 if ( duration > ld->MAX_YIELD_TIME ) {
			 QLOG(7,"Examine (StopObs) timeout (%d seconds).", ld->MAX_YIELD_TIME );
			 if ( since(ld->last_goal) > ld->GOAL_MIN_TIME ) {
	    		 QLOG(6," Sending 'road blocked' failure to mPlanner.");
				 ld->newErr |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
				 return 0;
			 }
			 
			 // Back up!
			 Q_TRAN(&QHSM1::S21_drive); 
			 return 0;
		 }
		 
		 // Default printout
		 QLOG(7,"Alice has been examining obstacles for %f seconds", duration );
		 return 0;	
	 }
	 
		 
     case Q_EXIT_SIG:  PRINT_EXIT; return 0;
     case Q_INIT_SIG:  NO_SUBSTATES;
	 case Q_EMPTY_SIG: break;
     default: defaultStateError();
	}
    return (QSTATE)&QHSM1::S2_road;
}


// This is just a temporary fix to include other states!
// It will not work like this when the design pattern is all worked out.
#include "QHSM21_drive.cc"


// EOF 'QHSM2_road.cc'
