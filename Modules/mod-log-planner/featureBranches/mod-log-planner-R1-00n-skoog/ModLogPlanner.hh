/**********************************
 * Header for ModLogPlanner.cc
 * Author: Stefan Skoog
 * Date: Created June 2008
 * 
 **********************************/

#ifndef MOD_LOG_PLANNER_HH_
#define MOD_LOG_PLANNER_HH_

/** @file

@brief Perform modular logic planning and generate state problem for path- & velocity-planners to solve. 

*/


 /********************************
 * Includes
 *********************************/
#include <temp-planner-interfaces/AliceStateHelper.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <temp-planner-interfaces/ConfigFile.hh>
#include <temp-planner-interfaces/Console.hh>
#include <temp-planner-interfaces/Utils.hh>
#include <temp-planner-interfaces/Log.hh>
#include <interfaces/ActuatorState.h>	// Em-stop signal
#include <dgcutils/cfgfile.h>
#include "IntersectionHandling.hh"
#include "QMacros.hh"
#include "QHSM.hh"		// THE state machine


 /********************************
 * Class definitions
 *********************************/

/// @brief Main class handling the logics
class ModLogPlanner 
{
	
  public:

	/*! Constructor */
	ModLogPlanner();

	/*! Destructor */
	~ModLogPlanner();
	
	/*! Inittialization, just for backward compatibleness */	
	int init();

	/*! Alternative destructor, just for backward compatibleness */
	void destroy();
	
	/*! Read input config file and store in class variables  */
	int readConfig();
	
	/*! Do the actual logic planning!  */
	Err_t planLogic(vector<StateProblem_t*> &problems, PlanGraph *graph, Err_t prevErr, 
			 VehicleState &vehState, Map *map, 
			 Logic_params_t &params, int& currentSegmentId, int& currentLaneId,
			 bool& replanInZone);	
	
	void resetState();
	void updateIntersectionHandlingConsole(void);
	void updateDisplayInfo(void);
	void obstacleTimeStamps(Logic_data_t& logicData, StateProblem_t old);
	void printLogicData(Logic_data_t& ld, bool extended);
	
  protected:
	  // Just call som eternal functions to update important information in LogicData structure
	int updateLogicData(Logic_data_t& ld);
    void velLowPass(float& filterVel, float newVel);
	  

    // - - - - - - - - VARIABLES - - - - - - - - - //
  public:
	// An event to feed to state machine
	QEvent qe;
	// All data passed to ModLogPlanner
	Logic_data_t logicData;
	
  protected:
	  
	  
  private:
	// Finite State Machine current status, specified in temp-planner-interfaces/PlannerInterfaces.h
	// The struct name contains 'problem', but this time it's not used for a problem representation
	StateProblem_t current_status;
	SegGoals::SegmentType last_seg_type;
	Err_t last_err;
	int last_goalID;
	
	// Reference to the state machine framework
	QHSM *qhsm;

	
	// Intersection Handler
	//CIntersectionHandling* IntersectionHandling;
	
	// This one is only needed for development, remove it when things start to work
	long cycle_counter;

};


#endif /*MOD_LOG_PLANNER_HH_*/
// EOF 'ModLogPlanner.hh'
