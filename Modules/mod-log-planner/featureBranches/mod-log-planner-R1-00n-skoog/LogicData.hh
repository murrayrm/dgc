/**********************************
 * Header for LogicData.cc
 * Author: Miro Samek, modified by Stefan Skoog
 * Date:   Created July 2008
 * 
 **********************************/

#ifndef LogicData_hh
#define LogicData_hh

// Includes
#include <temp-planner-interfaces/PlannerInterfaces.h>	// State problem definition from here
#include <gcinterfaces/SegGoals.hh>
#include <map/MapPrior.hh>
#include "IntersectionHandling.hh"


/******************************************************************
 * Macros for QEvent enum->string conversion
 ******************************************************************/
#define FOR_ALL_QSIGNALS(QS)\
		QS(Q_EMPTY_SIG) QS(Q_INIT_SIG) QS(Q_ENTRY_SIG) QS(Q_EXIT_SIG)\
		QS(Q_USER_SIG) QS(Q_NEW_SEGMENT_TYPE) QS(Q_STOP_INTERSECTION) \
		QS(Q_ESTOP) QS(Q_NEW_GOAL)

#define DECLARE_QSIGNAL(A) A,
enum QSignal {
	FOR_ALL_QSIGNALS(DECLARE_QSIGNAL) QSIGNAL_COUNT
};

#define MAP_QSIGNAL_TO_STRING(A) #A,
const char *const QSignalStr[QSIGNAL_COUNT] = {
	FOR_ALL_QSIGNALS(MAP_QSIGNAL_TO_STRING)
};


/******************************************************************
 * Definition of QEvent
 ******************************************************************/
struct QEvent {
	QSignal sig;		// Signal of the event instance
	// Noting else here since all useful information is in the shared Logic_data_t stucture (here below).
};

// This pre-defined array is needed AND reserved for state transitions only! 
extern QEvent pkgStdQEvent[]; 


/******************************************************************
 * Definition of LogicData structure
 ******************************************************************/
typedef struct {
	
	// - - - - - - - - Some basic logic data passed from planner every execution cycle - - - - - - - - //
	//vector<StateProblem_t*> problems;	// Vector of pointers to StateProblems_t. Since I didn't get this to work due to a lot of segmentation faults, I'm going with a single problem instead,
	StateProblem_t* problem;	// Pointer to StateProblems_t
	PlanGraph* graph;
	Err_t newErr; 
	Err_t prevErr;
	VehicleState* vehState; 
	Map* map; 
	Logic_params_t* params;
	int currentSegmentId; 
	int currentLaneId;
	bool replanInZone;

	// Helping objects
	CIntersectionHandling* intHandler;
	
	// - - - - - - - - Time stamps - - - - - - - - //
	// Logic level 1
	uint64_t last_lvl1_tran;			// The last transition between lvl 1 states
	uint64_t last_intersection_enter;	// Last time any intersection state was started (entered)
	uint64_t last_intersection_exit;	// Last time any intersection state was started (entered)
	uint64_t last_zone_enter;			// Last time zone state was entered
	uint64_t last_zone_exit;			// Last time zone state was exited
	uint64_t last_paused;				// Last time state paused was used
	
	// Logic level 2
	uint64_t last_lvl2_tran;		// The last transition between lvl 2 states
	uint64_t last_stopobs;			// Last time Alice was stopped due to obstacle
	uint64_t last_astern;			// Last time Alice tried to back up
	uint64_t last_aggressive;		// Last time Alice tried to run in agressive mode
	uint64_t last_bare;				// Last time Alice tried to run in bare mode
	
	// Higher logic levels
	uint64_t last_lvl3_tran;		// The last transition between lvl 3 states

	// Other
	uint64_t last_goal;			// Timestamp of last mission. To keep track of how long we have been working with the current mission
	
	// - - - - - - - - Position stamps - - - - - - - - //
	point2 last_lvl1_pos;
	point2 last_lvl2_pos;
	point2 last_lvl3_pos;
	point2 last_intersection_pos;	// This means last stopline stopped at (not the position where we switched to actual instersection mode))
	point2 last_stopobs_pos;
	point2 last_astern_pos;
	point2 last_examine_pos;
	
	// - - - - - - - - Derived spatial data - - - - - - - - // 
	float currentVelocity;
	float filteredVelocity;
	float stoplineDistance;
	float intersectDistance;
	float obstacleDistance;
	float exitDistance;
	float obstacleOnPath;
	float obstacleInLane;
	float stoppingDistance;
	point2 exitPoint;				// Coordinates to closest mission sub-goal. Good to measure and compare if we are making progress in the right direction. 
	
	float obsSide;
	float obsRear;
	float obsFront;
	
	LaneLabel currentLane;
	LaneLabel desiredLane;
	
	// - - - - - - - - mod-log-planner config data - - - - - - - - //
	// Please see config file for documentation for these system parameters
	float NOMINAL_SAFETYBUFFER;
	int PRINT_LOGIC_DATA;
	float ROAD_OBS_DISTANCE;
	float DESIRED_DECELERATION;
	int GOAL_MIN_TIME;
	int ASTERN_MAX_TIME;
	int UTURN_MAX_TIME;
	int MAX_YIELD_TIME;
	int ROAD_TO_OFFROAD_TIME;
	int EXAMINE_MAX_DISTANCE;
	int EXAMINE_MIN_DISTANCE;
	float DRIVE_VEL_REQUIREMENT;
	float INTERSECT_VEL_REQUIREMENT;
	float INTERSECT_STOPLINE_DIST;
	int INTERSECT_MIN_STOP_TIME;
	int INTERSECT_MAX_STOP_TIME;
	int INTERSECT_START_HANDLER;
	float INTERSECT_DEADTIME;
	float INTERSECT_MIN_DISTANCE;
	float INTERSECT_MAX_DISTANCE;
	float ZONE_VEL_REQUIREMENT;
	int ZONE_SAFE_TIMEOUT;
	int ZONE_AGRESSIVE_TIMEOUT;
	int ZONE_BARE_TIMEOUT;
	int OFFROAD_MAX_TIME;
	int OFFROAD_MAX_DISTANCE;
	float VEL_FILTER_FS_HIGH;
	float VEL_FILTER_FS_LOW;
	float VEL_FILTER_RESET;
	
	
	// - - - - - - - - Some helping functions - - - - - - - - //
	
	void resetTimeStamps(uint64_t now) 
	{		
		// Reset all time stamps
		last_intersection_enter = now;	
		last_intersection_exit = now;	
		last_zone_enter = now;			
		last_zone_exit = now;			
		last_paused = now;				
		last_stopobs = now-(long)60E6;;		// A minute ago
		last_astern = now-(long)60E6;;		// A minute ago				
		last_aggressive = now-(long)60E6;	// A minute ago				
		last_bare = now-(long)60E6;;		// A minute ago		
		last_goal = now;
	}
	
	void resetPositionStamps(point2 here) 
	{
		// Set all position stamps to 'here'
		last_lvl1_pos = here;
		last_lvl2_pos = here;
		last_lvl3_pos = here;
		last_intersection_pos = here;
		last_stopobs_pos = here;
		last_astern_pos = here;
		exitPoint = here;
	}
	
	void resetMisc()
	{
		//graph = NULL;
		newErr = 0; 
		prevErr = 0;
		//vehState = NULL; 
		//map = NULL; 
		//params = NULL;
		currentSegmentId = 0; 
		currentLaneId = 0;
		replanInZone = false;
	}
	
	
	// Do funky funky filtering with velocity
	void velLowPass(float newVel)
	{
		// Do funky, magic, non-symmetric low pass IIR filtering
		if (newVel > filteredVelocity) {
			// New speed is higher than filter speed, update with rather short time constant
			filteredVelocity = filteredVelocity*(1.0-VEL_FILTER_FS_HIGH) + newVel*(VEL_FILTER_FS_HIGH);
		} else {
			// New speed is lower than filter speed, update with slooow time constant
			filteredVelocity = filteredVelocity*(1.0-VEL_FILTER_FS_LOW) + newVel*(VEL_FILTER_FS_LOW);	
		}
	}
	// Do reset of funky funky velocity filter
	void resetFilter(void)
	{
		filteredVelocity = VEL_FILTER_RESET;
	}
	// Another way to reset, with free reset value
	void resetFilter(float value)
	{
		filteredVelocity = value;
	}
	
} Logic_data_t;



#endif

// EOF 'LogicData.hh'
