% UT_fwdrev.cmd - command sequence to test forward and reverse driving
% RMM, 23 Sep 07
%
%
% This command script is used with the UT_drive program to send
% commands to asim that cause it to drive forward and backward along a
% set of paths.  The commands involve going along the same paths in
% both directions, so we should end up back at the origin after each
% sequence.
%
% Usage:
%   * Run 'asim --nopause -no-noise'
%   * Run 'UT_drive < UT_fwdrev.cmd'
%   * View on mapviewer (or planviewer)

%
% Drive forward for 5 seconds and then backwards
%
p Commanding forward motion for 5 seconds, then stop and reverse
s 0		% steering at zero
t 1		% transmission in drive

p   accelerating
a 0.2		% accelarate
w 20000000	% sleep for 20 seconds
p   slowing
a 0		% come to a stop
w 2000000	% sleep for 2 seconds
p   stopping
a -0.2		% come to a stop
w 1000000	% sleep for 1 seconds
a -0.3		% come to a stop
w 1000000	% sleep for 1 seconds
a -0.4		% come to a stop
w 2000000	% sleep for 2 seconds
a -0.5		% come to a stop
w 2000000	% sleep for 2 seconds (enough to stop)

% Now shift into reverse
t -1		% shift into reverse
w 500000	% sleep for 0.5 second (allow trans to settle)

p   accelerating (backward)
a 0.2		% accelarate
w 20000000	% sleep for 5 seconds
p   slowing
a 0		% come to a stop
w 2000000	% sleep for 5 seconds
p   stopping
a -0.2		% come to a stop
w 1000000	% sleep for 1 seconds
a -0.3		% come to a stop
w 1000000	% sleep for 1 seconds
a -0.4		% come to a stop
w 2000000	% sleep for 2 seconds
a -0.5		% come to a stop
w 2000000	% sleep for 2 seconds (enough to stop)

p At this point we should be back at the origin (roughly)
w 200000	% sleep for 2 seconds

%
% Now try the same manuever, but with the steering wheel turned
%

p Turning steering
s 0.5		% steering at zero
w 500000	% sleep for 0.5 second (allow steering to settle)
t 1		% transmission in drive
w 500000	% sleep for 0.5 second (allow trans to settle)

p Commanding forward motion for 5 seconds, then stop and reverse
p   accelerating
a 0.2		% accelarate
w 20000000	% sleep for 20 seconds
p   slowing
a 0		% come to a stop
w 2000000	% sleep for 2 seconds
p   stopping
a -0.2		% come to a stop
w 1000000	% sleep for 1 seconds
a -0.3		% come to a stop
w 1000000	% sleep for 1 seconds
a -0.4		% come to a stop
w 2000000	% sleep for 2 seconds
a -0.5		% come to a stop
w 2000000	% sleep for 2 seconds (enough to stop)

% Now shift into reverse
t -1		% shift into reverse
w 500000	% sleep for 5 seconds (enough to stop)

p   accelerating (backward)
a 0.2		% accelarate
w 20000000	% sleep for 5 seconds
p   slowing
a 0		% come to a stop
w 2000000	% sleep for 5 seconds
p   stopping
a -0.2		% come to a stop
w 1000000	% sleep for 1 seconds
a -0.3		% come to a stop
w 1000000	% sleep for 1 seconds
a -0.4		% come to a stop
w 2000000	% sleep for 2 seconds
a -0.5		% come to a stop
w 2000000	% sleep for 2 seconds (enough to stop)

p At this point we should be back at the origin (roughly)
w 200000	% sleep for 2 seconds

s 0		% reset the steering to zero
t 1		% shift back into forward gear

% End of script
p All done
q
