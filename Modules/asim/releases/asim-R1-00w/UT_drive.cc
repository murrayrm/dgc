/*!
 * \file UT_drive.cc
 * \brief Unit test to send individual driving commands
 *
 * \author Richard M. Murray
 * \date 23 September 2007
 *
 * The 'UT_drive' program allows actuator commands to be sent to Alice
 * so that the simulation can be checked for correctness.  The program
 * implements a very simple parse that reads commands from a file (or
 * the command line) and sends skynet messages to asim.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <readline/readline.h>
#include "skynet/skynet.hh"
#include "interfaces/sn_types.h"
#include "interfaces/ActuatorCommand.h"

int main(int argc, char **argv)
{
  char *cmd;

  /* Create a skynet connection that we can use to send messages */
  int snkey = skynet_findkey(argc, argv);
  skynet_server_t *server = skynet_connect(snkey, SNadrive);

  while ((cmd = readline(NULL)) != NULL) {
    drivecmd_t drivecmd;

    switch (*cmd) {
    case '%':			// comment lines
    case '#':
    case '\0':			// blank line
      break;
      
    case 'h':
    case '?':
      fprintf(stderr, "Command summary:\n");
      fprintf(stderr, "  h, ?\t\tprint this summary\n");
      break;

    case 'p':			// print a string
      fprintf(stdout, "%s\n", cmd[1] == ' ' ? cmd+2 : cmd+1);
      break;

    case 's':			// send steering command
      drivecmd.my_actuator = steer;
      drivecmd.my_command_type = set_position;
      drivecmd.number_arg = atof(cmd+1);
      skynet_send_message(server, SNdrivecmd, (char *) &drivecmd, 
			  sizeof(drivecmd), 0);
      break;

    case 'a':			// send accel command
      drivecmd.my_actuator = accel;
      drivecmd.my_command_type = set_position;
      drivecmd.number_arg = atof(cmd+1);
      skynet_send_message(server, SNdrivecmd, (char *) &drivecmd, 
			  sizeof(drivecmd), 0);
      break;

    case 't':			// send trans command
      drivecmd.my_actuator = trans;
      drivecmd.my_command_type = set_position;
      drivecmd.number_arg = atof(cmd+1);
      skynet_send_message(server, SNdrivecmd, (char *) &drivecmd, 
			  sizeof(drivecmd), 0);
      break;

    case 'q':			// quit program
      exit(0);

    case 'w':			// wait (sleep) for some number of microseconds
      usleep(atoi(cmd+1));
      break;

    default:
      fprintf(stderr, "Unknown command %c; type h for help\n", *cmd);
      break;
    }

    free(cmd);
  }
  return 0;
}
