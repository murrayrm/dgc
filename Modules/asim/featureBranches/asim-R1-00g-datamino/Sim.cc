/*!
 * \file Sim.cc
 * \brief Simulation object for Alice
 *
 * \author Dima Kogan
 * \date 2004?
 *
 * This file defines the Sim class, which implements a simulation of Alice.
 *
 */

#include <math.h>
#include "Sim.hh"
#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"

/* Simulation data */
SIM_DATUM d;

/* Simulation status */
int QUIT_PRESSED = 0;
int PAUSED       = 1;
int RESET_PRESSED        = 0;

//vehicle_t* g_pVehicle;
//int user_quit();
int pause_simulation();
int unpause_simulation();
int reset_simulation();

/*!
 * \fn asim::asim
 * \brief Sim class constructor
 *
 * The Sim constructor initializes the simulator datum (internal
 * state), initiates the skynet conection, and sets various internal
 * simulation parameters.
 */

asim::asim(int sn_key, double steerLag, 
	   double steerRateLimit, int noise, bool estop, bool trans)
  : CSkynetContainer(SNasim, sn_key),
    m_steerLag(steerLag),
    m_steerRateLimit(steerRateLimit)
{
  // Set the initial commands to zero
  d.steercmd = 0;
  d.accelcmd = 0;
  d.transcmd = GEAR_DRIVE;

  // Set flags that control our operation
  simnoise = noise;
  use_estop = estop;
  use_trans = trans;

  // Initial position not specified unless otherwise indicated
  haveInitPos = false;

  // Put the simulator in pause initially
  pause_simulation();

  // Initialize the vehicle state
  d.homeNorthing = 0;		// set home values to zero by defaul
  d.homeEasting = 0;
  d.homeYaw = 0;
  SimInit();			// initailize simulation state variables

  // Initialize the actuator status
  d.actuatorState.m_steerstatus = 1;		// steering always used
  d.actuatorState.m_gasstatus = 1;		// gas always used
  d.actuatorState.m_brakestatus = 1;		// brake always used
  d.actuatorState.m_estopstatus = estop;		// from command line
  d.actuatorState.m_transstatus = trans;		// from command line
  d.actuatorState.m_obdiistatus = 0;		// no OBD II emulation
  
  // Create a mutex to protect access to the system state
  DGCcreateMutex(&m_stateMutex);

  broadcast_statesock = m_skynet.get_send_sock(SNstate);
  actuatorStateSocket = m_skynet.get_send_sock(SNactuatorstate);
}

/*!
 * \fn asim::asim
 * \brief Sim class deconstructor
 *
 * Standard deconstructor; free up resources we have been using.
 */

asim::~asim()
{
  DGCdeleteMutex(&m_stateMutex);
}

/*!
 * \fn asim::SimInit
 * \brief Initialize the vehicle state
 *
 * The SimInit member function intilizes the state of the vehicle to
 * its default state (specified from config files)
 *
 */

void asim::SimInit(void)
{
  DGCgettime(d.lastUpdate);
  d.startTime = d.lastUpdate;		// save start time

  /* 
   * Default state
   *
   * The default state of the vehicle is sitting at rest at the
   * origin.  The position and orientation of the vehicle can be
   * overwritten via command line flags before the active loop is
   * called.
   *
   */
  simState.n   = d.homeNorthing;	// northing (of reard wheel)
  simState.e   = d.homeEasting;		// easting (of rear wheels)
  simState.nd  = 0;			// northing velocity
  simState.ed  = 0;			// easting velocity
  simState.ndd = 0;			// northing acceleration
  simState.edd = 0;			// easting acceleration
  simState.yaw = d.homeYaw;		// yaw angle
  simState.yawd = 0;			// yaw velocity
  simState.phi = 0;			// steering wheel pointed straight
#ifdef DEBUG
  cerr << "asim::SimInit initialized to " <<
       << simState.n << ", " << simState.e << ", " << simState.yaw << endl;
#endif

  /* 
   * Initialize the internal vehicle state
   *
   * The simulation uses a bicycle model (see SimModel.cc) that has
   * some internal states that need to be initialized.  This call
   * initializes the center of mass of the vehicle and also sets some
   * parameters used by the simulation.  It also sets the simulation
   * clock to zero.
   */
  simEngine.Init(simState, 0.0, m_steerLag, m_steerRateLimit);
}

/*!
 * \fn asim::broadcast
 * \brief Broadcast the vehicle state data
 * 
 * The broadcast member function sends the internal state of the
 * vehicle out across skynet, emulating this same functionality within
 * adrive.
 */

void asim::broadcast()
{
  pthread_mutex_t* pMutex = &m_stateMutex;

  /* Send out the contents of the simulation state from the datum */
  if (m_skynet.send_msg(broadcast_statesock, &d.SS, sizeof(d.SS),
			0, &pMutex) != sizeof(d.SS)) {
      cerr << "asim::broadcast(): error sending vehicle state" << endl;
  }

  /* Send out the current actuator state from the dataum */
  if (m_skynet.send_msg(actuatorStateSocket, &d.actuatorState,
			sizeof(d.actuatorState), 0, &pMutex) !=
      sizeof(d.actuatorState)) {
      cerr << "asim::broadcast(): error sending actuator state" << endl;
  }
}

/*!
 * \fn asim:Active
 * \brief Main execution loop for simulation
 *
 * The Active loop runs the simulation.
 */

void asim::Active() 
{
  unsigned long long dt;

  // Let the outside world know we are running
  cerr << "entering asim::Active loop" << endl;

  // Set the initial vehicle state to pause
  d.actuatorState.m_estoppos = ESTP_PAUSE;

  while (!QUIT_PRESSED) {
    DGCusleep(25000);		// Run at 40 Hz
    
    // handle time update
    DGCgettime(dt);    
    dt -= d.lastUpdate;
    DGCgettime(d.lastUpdate);

    // Update the time we use for the sparrow display (seconds).
    // This should be the same as the time returned by DGCgettime so
    // we can check the value in other modules.
    d.sparrowTime = (double) d.lastUpdate * 1e-6;

    /*
     * Process actuator commands
     *
     * The actuator commands are read by a separate thread and made
     * available to us in the daatum.  We process these commands here
     * and also fill the actuatorState structure with the data, so that
     * the actuator state is properly broadcast by the broadcast
     * thread.
     */

    // Set the commanded values of the actuators based on skynet
    d.actuatorState.m_steercmd = d.steercmd;
    d.actuatorState.m_gascmd = d.accelcmd > 0 ? d.accelcmd : 0;
    d.actuatorState.m_brakecmd = d.accelcmd > 0 ? 0 : -d.accelcmd;
    if (use_trans) d.actuatorState.m_transcmd = d.transcmd;

    // Set the comands that the simulator will use
    simEngine.SetCommands(d.steercmd, d.accelcmd, d.transcmd);

    // Execute the simulation
    if(!PAUSED) {
      simEngine.RunSim(DGCtimetosec(dt));
    } 

    // Reset the simulation here if we were asked to do so
    if (RESET_PRESSED) {
      SimInit();
      RESET_PRESSED = 0;
    }

    // Update the vehicle state information to match simulation state
    // This copies the data from the vehicle state to the d.SS struct
    updateState();

    /*
     * Update actuator state based on current vehicle motion
     */

    // Steering: determine based on current steering angle 
    // now set the actuator outputs (simulate them!)
    d.actuatorState.m_steerpos = simState.phi / VEHICLE_MAX_AVG_STEER;
    d.actuatorState.m_gaspos = d.accelcmd > 0 ? d.accelcmd : 0;
    d.actuatorState.m_brakepos = d.accelcmd > 0 ?0 : -d.accelcmd;
    d.actuatorState.m_transpos = d.transcmd;
    if (use_estop) {
      // Simulate the DARPA estop command
      d.actuatorState.m_estoppos = d.estopcmd;
      d.actuatorState.m_dstoppos = d.estopcmd;
      d.actuatorState.m_astoppos = ESTP_RUN;
      d.actuatorState.m_cstoppos = ESTP_RUN;
      d.actuatorState.m_estop_update_time = d.lastUpdate;
    }
      
    // Simulate engine RPM based on wheel speed for now
    d.actuatorState.m_engineRPM = sqrt( pow(simState.nd, 2) + 
				   pow(simState.ed, 2) );

    // Broadcast the vehicle and actuator state
    broadcast();
  }
  // Let the outside world know we are stopped
  cerr << "leaving asim::Active loop" << endl;
}

/*!
 * \fn asim::setHomePosition
 * \brief set the vehicle initial position to a given location
 *
 * This member function allows you to reset the initial position of
 * the vehicle.  It should be called before the Active() function is
 * called.
 */

void asim::setHomePosition(double N, double E, double Y)
{
  d.homeNorthing = N;		// northing (of reard wheel)
  d.homeEasting = E;		// easting (of rear wheels)
  d.homeYaw = Y;		// yaw angle (in radians)

  // Now reinitialize the entire state
  SimInit();
}

void asim::setNorthing(double N) { d.homeNorthing = N; SimInit(); }
void asim::setEasting(double E) { d.homeEasting = E; SimInit(); }
void asim::setYaw(double Y) { d.homeYaw = Y; SimInit(); }
 

/*!
 * \fn asim::updateState
 * \brief update state interface data to match simuation
 *
 * This member function fills the state struct based on the current
 * vehicle state from the simulator.
 */

void asim::updateState(void)
{
  unsigned long long time;

  // Compute simulation time 
  DGCgettime(time);

  // Reset noise time every second
  if(time - noisetime > 1000000) {
    noisetime = time;
  }

  // Get the state of the vehicle from the simulator
  simState = simEngine.GetRearState();

  // Updating the state struct
  DGClockMutex(&m_stateMutex);

  // Not simulating the UTM zone
  //d.SS.utmZone = 0;
  //d.SS.utmLetter = ' ';

  // Set the utm pose
  d.SS.utmNorthing = simState.n + simnoise*.0000001*(time - noisetime);
  d.SS.utmEasting  = simState.e + simnoise*.0000001*(time - noisetime);
  d.SS.utmNorthVel = simState.nd;
  d.SS.utmEastVel  = simState.ed;
  d.SS.utmYawRate  = simState.yawd;	
  d.SS.utmYaw      = simState.yaw;

  // Set portions of the date that we don't simulate to zero
  d.SS.utmAltitude = 0.0;
  d.SS.utmAltitudeVel = 0.0;
  d.SS.utmPitchRate= 0.0;
  d.SS.utmRollRate = 0.0;
  d.SS.utmPitch    = 0.0;
  d.SS.utmRoll     = 0.0;

  // Rough logic for local pose estimates; we record and subtract the
  // initial robot offset.
  if (!this->haveInitPos) {
    this->initPosN = d.SS.utmNorthing;
    this->initPosE = d.SS.utmEasting;
    this->haveInitPos = true;    
  }

  // Compute the local pose
  d.SS.localX = d.SS.utmNorthing - this->initPosN;
  d.SS.localY = d.SS.utmEasting - this->initPosE;
  d.SS.localZ = -d.SS.utmAltitude;
  d.SS.localRoll = d.SS.utmRoll;
  d.SS.localPitch = d.SS.utmPitch;
  d.SS.localYaw = d.SS.utmYaw;

  // Finally, compute velocities in the vehicle frame
# warning vehicle frame data not currently computed
  d.SS.vehXVel = 0;
  d.SS.vehYVel = 0;
  d.SS.vehZVel = 0;
  d.SS.vehRollRate = 0;
  d.SS.vehPitchRate = 0;
  d.SS.vehYawRate = 0;

  // Save the timestamp on the data
  // done indirectly in this way to bypass reference access to a packed member
  unsigned long long timestamp;
  DGCgettime(timestamp);
  d.SS.timestamp = timestamp;

  DGCunlockMutex(&m_stateMutex);
}

// Pause the simulation
int pause_simulation()
{
  d.estopcmd = ESTP_PAUSE;
  PAUSED = 1;
  return PAUSED;
}

// Unpause the simulation
int unpause_simulation()
{
  d.estopcmd = ESTP_RUN;
  PAUSED = 0;
  return PAUSED;
}

// Reset the simulation
int reset_simulation()
{
  RESET_PRESSED = 1;
  return RESET_PRESSED;
}
