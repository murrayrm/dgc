#
# This is the source file for managing asimcommand-line options.  
# It uses gengetopt (http://www.gnu.org/software/gengetopt/gengetopt.html)
#
# Richard Murray, 2007-01-13
#

package "asim"
purpose "Asim is a kinematic simulator for Alice"
version "1.0"

# Options for specifying the initial conditions
option "rddf" - "set initial condition from RDDF file" string 
       default="rddf.dat" no
option "rddf-start" - "set the start point for the RDDF file" int
       default="0" no
option "rndf" - "set initial condition from an RNDF file" string no
option "rndf-start" - "set the start point as a waypoint from the RNDF"
	string default="1.1.1" no
option "initial-northing" N "set the initial Northing" double default="0" no
option "initial-easting" E "set the initial Easting" double default="0" no
option "initial-yaw" Y "set the initial Yaw" double default="0" no
option "initial-accel" - "set the initial acceleration command" double default="0" no
option "initial-steer" - "set the initial steering command" double default="0" no
option "northing-offset" - "set the offset for the initial Northing"
	double default="0" no
option "easting-offset" - "set the offset for the initial Easting"
	double default="0" no
option "yaw-offset" - "set the offset for the initial Yaw (in degree)"
	double default="0" no

# Options for turning off actuator threads
option "disable-brake" b "disable brake interface (not implemented)" flag off
option "disable-sparrow" d "turn off sparrow display (not implemented)" 
	flag  off
option "disable-estop" e "disable estop interface" flag off
option "disable-gas" g "disable throttle interface (not implemented)" flag off
option "disable-obd" o "disable OBD II interface (not implemented)" flag off
option "disable-steer" s "disable steering interface (not implemented)"
	flag off
option "disable-trans" t "disable transmission (and ignition) interface"
  flag off 

# Options to set internal parameter values
option "steer-lag" - "set steering lag" double default="0" no
option "accel-lag" - "set accel lag - in seconds" double default="-1.0" no
option "steer-rate" - "set steering rate limit" double default="0.7" no
option "nonoise" - "turn off noise in reported position" flag on

# Configuration files
# text ""
option "config" - "configuration file"
  string default="asim.config" no
option "noconfig" - "don't load configuration file at startup" flag off

# Standard options
# text ""
option "skynet-key" S "skynet key" int default="0" no
