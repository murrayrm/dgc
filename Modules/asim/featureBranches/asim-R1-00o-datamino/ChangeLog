Thu May 17  8:57:30 2007	murray (murray)

	* version R1-00o
	BUGS:  
	FILES: Sim.cc(23452)
	added OBD-II computations

Tue May 15 11:28:16 2007	murray (murray)

	* version R1-00n
	BUGS:  
	FILES: Sim.cc(22950), Sim.hh(22950), SimMain.cc(22950),
		asim.ggo(22950)
	added --gcdrive flag that sends actuator state on separate port

	FILES: Sim.cc(23162), simdisplay.cc(23162), simdisplay.dd(23162)
	added manual control of DARPA estop

	FILES: Sim.cc(23165)
	updated documentatino for setHomePosition

2007-05-15  Richard Murray  <murray@kona.local>

	* simdisplay.cc: added estop controls

Sun May  6 13:51:26 2007	 (ahoward)

	* version R1-00m
	BUGS:  
	FILES: Makefile.yam(22086)
	Tweaks for build portability

	FILES: Sim.cc(22230), Sim.hh(22230), SimMain.cc(22230)
	Added support for utm zones

	FILES: Sim.cc(22302), Sim.hh(22302), SimMain.cc(22302),
		asim.ggo(22302)
	Added command-line option for setting the altitude

Mon Apr 30  8:10:07 2007	murray (murray)

	* version R1-00l
	BUGS:  
	FILES: Sim.cc(21673), SimMain.cc(21673), SimModel.cc(21673),
		actuator.cc(21673)
	updated documentation

	FILES: SimModel.cc(21672)
	added missing return value in GetFrontState

Thu Apr  5 23:25:22 2007	Nok Wongpiromsarn (nok)

	* version R1-00k
	BUGS: 
	FILES: SimMain.cc(19155), asim.ggo(19155)
	Added commandline options for setting offset in initial position

Wed Apr  4 11:08:22 2007	murray (murray)

	* version R1-00j
	BUGS: 3199
	FILES: SimMain.cc(19118), SimModel.cc(19118)
	updated angle to be in the range -pi to pi

2007-04-04  murray  <murray@gclab.dgc.caltech.edu>

	* SimModel.cc (RunSim), SimMain.cc (initializeFromRDDF,
	initializeFromRNDF): got rid of shift of angle to be in 0 to PI.
	Now in the range -PI to PI.

Sat Mar 17 20:00:59 2007	Laura Lindzey (lindzey)

	* version R1-00i
	BUGS: 
	FILES: Sim.cc(18376), Sim.hh(18376), SimMain.cc(18376),
		actuator.cc(18376), asim.ggo(18376)
	adding sluggish mode to asim (delays actuator commands) for
	tplanner testing

Thu Mar 15 22:52:02 2007	murray (murray)

	* version R1-00h
	BUGS: 3199
	FILES: SimMain.cc(17898), SimModel.cc(17898)
	shifted yaw to be in range of 0 to 2 pi, to match astate

	FILES: Makefile.yam(17899)
	commented out doxygen flag


Sat Mar 10 11:49:28 2007	murray (murray)

	* version R1-00g
	BUGS: 
	FILES: Makefile.yam(16992), SimMain.cc(16992), asim.ggo(16992)
	added initialization from RNDF

2007-03-10  Richard Murray  <murray@kona.local>

	* asim.ggo: added options for setting start point from RNDF
	* SimMain.cc (initializeFromRNDF): new function to set initial state
	of the simulator from the RNDF

Mon Mar  5 12:47:53 2007	murray (murray)

	* version R1-00f
	BUGS: 
	FILES: Sim.cc(16600), Sim.hh(16600), SimMain.cc(16600),
		asim.ggo(16600)
	added initial-northing, initial-easting, initial-yaw processing

2007-03-05  Richard Murray  <murray@kona.local>

	* SimMain.cc (main): added initial state processing
	* asim.ggo: added initial-easting, initial-northing, initial-yaw
	* Sim.cc, Sim.hh: added setNorthing, setEasting, setYaw functions

Sat Mar  3 11:04:01 2007	murray (murray)

	* version R1-00e
	BUGS: 3151
	FILES: Sim.hh(16295), SimMain.cc(16295), actuator.cc(16295),
		asim.ggo(16295)
	updated --snkey to --sknet-key and added --initial-steer and
	--initial-accel commands

2007-03-03  murray  <murray@gclab.dgc.caltech.edu>

	* actuator.cc (set_accel): added new function to set acceleration
	(used for command line initial-accel) and steering (...)
	* SimMain.cc (main): added processing of accel and steer
	initialization 

	* asim.ggo: Changed snkey to skynet-key (3151) and added
	"initial-accel" option.

Tue Feb 20 15:48:44 2007	Nok Wongpiromsarn (nok)

	* version R1-00d
	BUGS: 
	FILES: Sim.cc(15172), Sim.hh(15172), actuator.cc(15172)
	Fixed asim so it compiles against revision R2-00a of the interfaces
	and the skynet modules

Wed Jan 31 13:09:42 2007	Andrew Howard (ahoward)

	* version R1-00c
	BUGS: 
	FILES: Sim.cc(13676), Sim.hh(13676), simdisplay.dd(13676)
	Fixed sparrow time display

Sat Jan 27 17:58:46 2007	murray (murray)

	* version R1-00b
	BUGS: 3080
	FILES: Makefile.yam(13210)
	removed bad makefile.yam target; was creating link problems

Sat Jan 13 22:02:19 2007	murray (murray)

	* version R1-00a
	BUGS: 
	New files: Makefile Pending Sim.cc Sim.hh SimMain.cc SimModel.cc
		SimModel.hh actuator.cc asim.ggo planar_definitions.hh
		simdisplay.cc simdisplay.dd simtest.cc

	FILES: Makefile.yam(12756), README(12756)
	Fairly extensive update of asim (formerly simulator)  * Got rid of
	functionality that was never used (eg, simpleModel) * Got rid of
	dependence on adrive (only requires interfaces) * Added ability to
	receive actuation messages (replaced adrive code) * Made first pass
	at implementing coding standards * Added lots of comments (doxygen)

	FILES: Makefile.yam(12772)
	modifications to get asim running with other modules; see ChangeLog
	13 Jan 07 entry for detailed list of changes

2007-01-13  Richard Murray  <murray@kona.local>

	* Sim.cc (asim::updateState): removed use of deprecated VehicleState
	members (accelerations of various sorts, mainly)

	* actuator.cc (asim::actuatorCommandThread): reset SN listen to
	listen from SNadrive_commander instead of ALLMODULES

	* simdisplay.cc (asim::SparrowDisplayLoop): added errlog
	initialization to allow for easier debuggin

	* Sim.cc (asim::broadcast): broadcast current actuator state
	* Sim.hh, Sim.cc (asim::asim): added actuatorStateSocket

	* asim.ggo: added --rddf and --rddf-start option
	* SimMain.cc (initializeFromRDDF): new function to parse initial
	condition from RDDF
	* Sim.hh: added homeNorthing, homeEasting, homeYaw to keep track of
	initial location of vehicle
	* Sim.cc (asim::asim, asim::SimInit): initialized home variables
	(asim::setHomePosition): new function to set home variables

2007-01-13  murray  <murray@gclab.dgc.caltech.edu>

	* Sim.cc, Sim.hh: reformated code and added comments
	* SimModel.cc, SimModel.hh: reformated code and added comments

	* Sim.hh, Sim.cc, actuator.cc: added actuation interface

	* SimMain.cc, asim.ggo: added gengetopt interface, similar to adrive

2007-01-11  murray  <murray@gclab.dgc.caltech.edu>

	* Sim.cc (SimInit): removed simple engine model code (was off by
	default) 

Thu Jan 11  7:55:04 2007	murray (murray)

	* version R1-00
	Created asim module.















