/*!
 * \file UT_model.cc
 * \brief Probe the vehicle model
 *
 * \author Richard M. Murray
 * \date 24 September 2007
 *
 * This program allows you to query the value of the right hand side
 * of the vehicle kinematics by entering a state and getting back the
 * derivative.
 * 
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "SimModel.hh"

int main(int argc, char ** argv)
{
  double state[NUM_STATES], deriv[NUM_STATES];
  double Flon[2], Flat[2], alpha[2];
  double phi;
  SimModel model;

  /* Get the actuator commands */
  printf("Actuation (steer, accel, trans): "); 
  scanf("%lg %lg %lg", &model.steer, &model.pedal, &model.trans);
  phi = model.steer;

  while (1) {
    printf("State (x, y, th, thd, vlat, vlon): ");
    scanf("%lg %lg %lg %lg %lg %lg", state, state+1, state+2,
	  state+3, state+4, state+5);

    /* Call the model */
    model.sideslip(state, phi, alpha);
    model.dTire_Long(Flon, state);
    model.FTire_Lat(state, phi, Flat);
    model.dPlanar_State(state, Flon, phi, deriv);

    /* Print out the results */
    printf("  alpha: %g %g\n", alpha[0], alpha[1]);
    printf("  Flon: %g %g\n", Flon[0], Flon[1]);
    printf("  Flat: %g %g\n", Flat[0], Flat[1]);
    printf("  Deriv: %g %g %g %g %g %g\n", deriv[0], deriv[1], deriv[2],
	   deriv[3], deriv[4], deriv[5]);
  }
  return 0;
}
