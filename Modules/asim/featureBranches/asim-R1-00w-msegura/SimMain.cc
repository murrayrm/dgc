/*!
 * \file SimMain.cc
 * \brief Main program for simulator
 *
 * \author Dima Kogan, Lars Cremean, Richard Murray
 * \date 2004?
 *
 * This is the main program for asim.  It parses command line
 * arguments, starts up display and actuatorCommand threads and then
 * calls the simulator.
 *
 * \page Asim
 * \section asim_usage Usage
 *
 * Asim is a simple simulator for Alice.  It uses a dynamic 'bicycle'
 * model that keeps track of the forces acting on the tires and
 * generates the state of the vehicle.
 *
 * Usage: asim [options]
 *
 * Use the --help option to get a full list of options.
 *
 * \section asim_concept Concept of operation
 *
 * The main function of the simulator is to accept AdriveCmd messages,
 * integrate the equation of motion for the vehicle and broadcast the
 * VehicleState and ActuatorState.  In this mode, it emulates the
 * combination of adrive, astate and Alice and should be useful for
 * simulations that require this level of kinematic information.
 *
 * There are a couple of important differences between Alice and asim:
 *   - Limited actuator interlocks
 *   - No actuator failure emulation (commands always read, etc)
 *
 */

#include <math.h>
using namespace std;

#include "Sim.hh"
#include "dgcutils/DGCutils.hh"
#include "dgcutils/RDDF.hh"
#include "skynet/skynet.hh"
#include "cmdline.h"

/* Functions defined later in this file */
int initializeFromRDDF(asim &, char *, int, double, double, double);
int initializeFromRNDF(asim &, char *, char *, double, double, double);

int main(int argc, char **argv)
{
  /* Process command line options */
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);
  if (cmdline.config_given &&
      cmdline_parser_configfile(cmdline.config_arg, &cmdline, 0, 0, 1) != 0) {
    exit(1);
  }

  /* Retrieve the skynet key */
  int optSNKey = skynet_findkey(argc, argv);
  cerr << "Constructing skynet with KEY = " << optSNKey << endl;

  /* Create the simulator object */
  asim sim(optSNKey, cmdline.accel_lag_arg, cmdline.steer_lag_arg, cmdline.steer_rate_arg,
	   !cmdline.nonoise_given, (bool) !cmdline.disable_estop_given,
	   (bool) !cmdline.disable_estop_given);
  if (cmdline.gcdrive_flag) sim.gcdriveInit();

  /* Reset initial condition based on command line flags */
  /*!
   * By default the initial condition is set to the origin.  If the
   * --rddf or --rndf flags are given, the initial condition is set
   * based on the location of the RDDF or RNDF waypoint.  The
   * --rddf-start or --rndf-start arguments are used to specify the
   * waypoint (as an integer for RDDF and as a waypoint specification
   * (i.j.k) for RNDF).  If both arguments are present, the RNDF
   * argument takes precedence.  Finally, the initial northing,
   * easting and yaw can be set individually using the
   * --initial-{northing,easting,yaw} arguments.  These override any
   * previous initial conditions set with the RDDF or RNDF.
   */

  /* Start with RDDF file, if specified */
  if (cmdline.rddf_given) {
    // Use an RDDF file to set the initial condition
    if (initializeFromRDDF(sim, cmdline.rddf_arg, cmdline.rddf_start_arg, 
			   cmdline.northing_offset_arg, cmdline.easting_offset_arg, 
			   cmdline.yaw_offset_arg) < 0)
      exit(1);
  }

  /* Check for RNDF argument */
  if (cmdline.rndf_given) {
    // Use an RNDF file to set the initial condition
    if (cmdline.rndf_arg == NULL) {
      cerr << argv[0] << ": no RNDF file given" << endl;
      exit(1);
    } else if (initializeFromRNDF(sim, cmdline.rndf_arg, 
				  cmdline.rndf_start_arg, 
				  cmdline.northing_offset_arg, 
				  cmdline.easting_offset_arg, 
				  cmdline.yaw_offset_arg) < 0) {
      exit(1);
    }
  }

  /* Reset individual initial conditions if specified */
  if (cmdline.initial_northing_given || (!cmdline.rddf_given && !cmdline.rndf_given)) { 
    sim.setNorthing(cmdline.initial_northing_arg + cmdline.northing_offset_arg); 
  }
  if (cmdline.initial_easting_given || (!cmdline.rddf_given && !cmdline.rndf_given)) { 
    sim.setEasting(cmdline.initial_easting_arg + cmdline.easting_offset_arg); 
  }
  if (cmdline.initial_yaw_given || (!cmdline.rddf_given && !cmdline.rndf_given)) { 
    double yaw = cmdline.initial_yaw_arg + cmdline.yaw_offset_arg*M_PI/180;
    while (yaw > M_PI)
    { 
      yaw -= 2 * M_PI;
    }
 
    while (yaw <= -M_PI)
    {
      yaw += 2 * M_PI;
    }
    sim.setYaw(yaw); 
  }

  /* Initialize the altitude */
  /*!
   * This can be useful for testing modules that expect 3D data (so we dont
   * get nasty suprises when using astate, which reports a non-zero altitude).
   */
  sim.utmAltitude = cmdline.utm_altitude_arg;
  
  /*!
   * The --initial-accel and --initial-steer commands can be used to
   * set the initial acceleration and steering commands used the by
   * simulator.  This is mainly useful for applying the brake so that
   * the simulator does not "drift" when it is not being sent any
   * commands.  Both flags take arguments between -1 and 1, with -1
   * corresponding to full brake or full left and +1 corresponding to
   * full throttle and full right.
   */
  if (cmdline.initial_accel_given) set_accel(cmdline.initial_accel_arg);
  if (cmdline.initial_steer_given) set_steer(cmdline.initial_steer_arg);

  /*!
   * The --stepsize option sets the step size of the simulator, in microseconds
   */
  sim.stepusec = cmdline.stepsize_arg;

  /*!
   * The --no-pause command can be used to start the simulator in run
   * mode (useful for automated testing.
   */
  if (cmdline.no_pause_flag) unpause_simulation();

  /*!
   * The --log=file option can be used to open up a log file
   */
  if (cmdline.log_given) sim.logInit(cmdline.log_arg);

  /* Start up the actuator command thread */
  DGCstartMemberFunctionThread(&sim, &asim::actuatorCommandThread);

  /* Start up the display loop thread */
  if (!cmdline.disable_console_flag) 
    DGCstartMemberFunctionThread(&sim, &asim::SparrowDisplayLoop);

  /* Call the simulator */
  sim.Active();

  return 0;
}

/*!
 * \fn int initializeFromRDDF(asim &sim, char *RDDFfile, int startPoint, 
 *            double northingOffset, double eastingOffset, double yawOffset)
 * \brief Initialize the state of the simulator by reading an RDDF file
 *
 * This function reads an RDDF file and then sets the initial
 * condition of the vehicle to be aligned with a specified segment.
 *
 */

#include "dgcutils/findroute.h"

int initializeFromRDDF(asim &sim, char *RDDFfile, int startPoint, double northingOffset,
		       double eastingOffset, double yawOffset)
{
  RDDF rddf(dgcFindRouteFile(RDDFfile));

  // Let the user know that we hear him or her
  cerr << "asim: initializing state from RDDF file: " << RDDFfile 
       << "( " << rddf.getNumTargetPoints() << " points)" << endl;

  // check the startPoint to make sure it's not too big or small
  int num_points = rddf.getNumTargetPoints();
  if (startPoint < 0 || startPoint > num_points-2) {
    cerr << "asim: start point " << startPoint << " is not in RDDF" << endl;
    return -1;
  }

  // move the startPoint to index the waypoint array properly. This makes no
  // assumptions about the number of the first waypoint, but does assume that
  // they are numbered consequently
  startPoint -= rddf.getTargetPoints()[0].number;
  startPoint = (startPoint > 0 ? startPoint : 0);

  // Figure out the coordinates of the start point
  double northing = rddf.getWaypointNorthing(startPoint) + northingOffset;
  double easting = rddf.getWaypointEasting(startPoint) + eastingOffset;

  // Now figure out the angle that we should use
  double yaw = atan2(rddf.getWaypointEasting(startPoint+1)  - easting,
		      rddf.getWaypointNorthing(startPoint+1) - northing)
               + yawOffset*M_PI/180;

  while (yaw > M_PI)
  { 
    yaw -= 2 * M_PI;
  }
 
  while (yaw <= -M_PI)
  {
    yaw += 2 * M_PI;
  }


  // Now place the vehicle at the start of the RDDF, in proper orientation 
  sim.setHomePosition(northing, easting, yaw, 0, 0);

  return 0;
}

/*!
 * \fn int initializeFromRNDF(asim &sim, char *RNDFfile, char *RNDFwaypoint,
 *           double northingOffset, double eastingOffset, double yawOffset)
 * \brief Initialize the state of the simulator by reading an RNDF file
 *
 * This function reads an RNDF file and then sets the initial
 * condition of the vehicle to be aligned with the first segment.
 *
 */

#include "rndf/RNDF.hh"
#include "dgcutils/cfgfile.h"
using namespace std;

int initializeFromRNDF(asim &sim, char *RNDFfile, char *RNDFwaypoint, double northingOffset,
                       double eastingOffset, double yawOffset)
{
  RNDF rndf;
  Waypoint *waypoint;
  
  /* Parse the waypoint and make sure it makes sense */
  int segment, lane, id;
  if (sscanf(RNDFwaypoint, "%d.%d.%d", &segment, &lane, &id) < 3) {
    cerr << "asim: invalid waypoint specification '" << RNDFwaypoint << "'" 
	 << endl;
    return -1;
  }
 
  /* Open up and parse the RNDF file */
  char *file = dgcFindRouteFile(RNDFfile);
  if (!rndf.loadFile(file)) {
    cerr << "asim: couldn't load '" << RNDFfile << "'" << endl;
    return -1;
  }

  /* Get the first waypoint; this will be used for the site offset */
  waypoint = rndf.getWaypoint(1, 1, 1);
  if (waypoint == NULL) {
    cerr << "asim: couldn't find waypoint '" << RNDFwaypoint << "'" << endl;
    return -1;
  }

  /* Set site offset */
  sim.sitePosN = waypoint->getNorthing();
  sim.sitePosE = waypoint->getEasting();
  sim.sitePosA = 0;
  sim.haveSiteOffset = true;
 
  fprintf(stderr, "using site frame offset %.0f %.0f\n", sim.sitePosN, sim.sitePosE);
  
  /* Find the RNDF waypoint */
  waypoint = rndf.getWaypoint(segment, lane, id);
  if (waypoint == NULL) {
    cerr << "asim: couldn't find waypoint '" << RNDFwaypoint << "'" << endl;
    return -1;
  }

  // Figure out the coordinates of the start point
  double northing = waypoint->getNorthing() + northingOffset;;
  double easting = waypoint->getEasting() + eastingOffset;;

  int zone = waypoint->getUtmZone();
  int letter = waypoint->getUtmLetter();

  // Figure out the heading from either the next or previous waypoint
  double yaw = 0;
  Waypoint *nextwaypoint, *prevwaypoint;
  if ((nextwaypoint = rndf.getWaypoint(segment, lane, id+1)) != NULL) {
    // Use the next waypoint to figure out heading
    yaw = atan2(nextwaypoint->getEasting()  - easting,
		nextwaypoint->getNorthing() - northing);
  } else if (id > 1 && 
	     (prevwaypoint = rndf.getWaypoint(segment, lane, id-1)) != NULL) {
    // Use the previous waypoint to figure out the heading
    yaw = atan2(easting - prevwaypoint->getEasting(),
		northing - prevwaypoint->getNorthing());
  } else {
    cerr << "asim: can't find next or previous waypoint to determine heading" 
	 << endl;
    return -1;
  }

  yaw = yaw + yawOffset*M_PI/180;
  while (yaw > M_PI)
  { 
    yaw -= 2 * M_PI;
  }
 
  while (yaw <= -M_PI)
  {
    yaw += 2 * M_PI;
  }


  // Set the initial condition for the simulation
  sim.setHomePosition(northing, easting, yaw, zone, letter);
  return 0;
}


