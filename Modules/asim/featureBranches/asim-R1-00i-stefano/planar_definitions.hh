// Constant definitions for the planar dynamic bicycle model
// Haomiao Huang
// 1/21/2005


// Planar Bicycle Model Definitions

// Indices for referring to which tire
enum TIRE_INDEX{
  FRONT,
  REAR,
  NUM_TIRES,
};

// indices for referring to the state variables 
enum STATE_INDEX{
  X_I,
  Y_I,
  TH_I,
  THD_I,
  VLAT_I,
  VLON_I,
  NUM_STATES,
};

#define V_EPS .05
