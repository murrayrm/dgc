/*!
 * \file UT_pacejka.cc
 * \brief Print out laternal force curves
 *
 * \author Richard M. Murray
 * \date 24 September 2007
 *
 * This file contains a simple function to output the Pacejka Magic
 * Formula tire model.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "SimModel.hh"

int main(int argc, char ** argv)
{
  double alpha;
  SimModel model;

  for (alpha = -M_PI; alpha <= M_PI; alpha += 0.01)
    printf("%g %g\n", alpha, model.pacejka(alpha));

  return 0;
}
