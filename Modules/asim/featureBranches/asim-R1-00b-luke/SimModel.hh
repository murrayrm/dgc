/*!
 * \file SimModel.hh
 * \brief Header file for SimModel class
 *
 * \author Haomiao Huang
 * \date 1/15/2005
 * 
 * This is the model that the simulator updates/runs to get vehicle
 * state.  The model is a dynamic bicycle model that models the forces
 * acting upon the tires (lateral and longitudinal) to generate the
 * state of the vehicle.  Sideslip is included.  For more information
 * on the model itself see Wiki documentation.
 */

#ifndef SIMMODEL_HH
#define SIMMODEL_HH

using namespace std;
#include "alice/AliceConstants.h"
#include "planar_definitions.hh"

/*!
 * \struct EngineStates
 * \brief States for the engine dynamics
 *
 * Currently there are only two, for the forces from the engine at the
 * wheels.  As the longitudinal dynamics model
 */
typedef struct EngineStates
{
  double simFf;     ///< forward wheels long. force (N)
  double simFr;     ///< rear wheels long. force (N)
};

/*! 
 * \struct simState
 * \brief Data structure for all of the states used by the model.  
 */
struct simState
{
  // dynamic bicycle model states
  double simX;			///< northing (m)
  double simY;			///< easting (m)
  double simTheta;		///< yaw (rad cw from N)
  double simDTheta;		///< delta yaw (rad/s)
  double simVLon;		///< longitudinal velocity (m/s)
  double simVLat;		///< lateral velocity (m/s)

  // steering states
  double simPhi;		///< actual steering angle (rad)  
  
  EngineStates simEngine;	///< states of the engine/lateral dynamics  
};

/*!
 *
 * \struct StateReport
 * \brief This is the struct used to report state back up to higher levels
 */
struct StateReport
{
  double n;			///< northing
  double nd;			///< northing velocity
  double ndd;			///< northing accel
  double e;			///< easting
  double ed;			///< easting velocity
  double edd;			///< easting accel
  double yaw;			///< heading angle
  double yawd;			///< change in heading angle
  double phi;			///< steering angle

  // Constructor - initialize everything to zero
  StateReport() {
    memset(this, 0, sizeof(*this));
  }	
};

/*!
 * \class SimModel
 * \brief Vehicle model class
 */
class SimModel 
{
private:
  // steering parameters
  double m_steerLag;
  double m_steerRateLimit;

  // private variables
  double steer;			///< current steering command
  double pedal;			///< current pedal command
  double trans;			///< current transmission command
  double t_last;		///< time of last simulation update
  double step; ///< time step from last time of simulation to new time
  simState state;		///< planar state
  double steer_phi;		///< de-normalized steering in rad

  // debug
  double max_step ;

  // private functions
  /// calculate the derivatives of the longitudinal tire forces
  void dTire_Long(double* forces, double* planar);

  /// calculate the lateral tire forces
  void FTire_Lat(double* planar, double steering, double* forces);

  /// calculate the derivative of the steering angle
  void dPhi(double phi, double& dphi);

  /// calculate the derivatives of the vehicle positional state
  void dPlanar_State(double* planar, double* ln_forces,
		     double steering, double* dplanar);

  /// calculate the sideslip angles
  void sideslip(double* planar, double steer, double* alpha);

  /// calculate the lateral tire forces based on slip angle and normal load
  double pacejka(double alpha);

public:
  // Constructors/Destructor
  SimModel();
  ~SimModel();
  
  /// Copy Constructor 
  SimModel(simState initial, double initTime);  
  
  /// Initialization
  void Init(StateReport& initial, double iTime,
	    double steerLag, double steerRateLimit); 

  /// Gives the simulator the time delta  since last update and then
  /// solves the diff eqs forward to arrive at the new state.  
  void RunSim(double step);
  
  /// Send commands to the simulated vehicle
  void SetCommands(double SteerCmd, double PedalCmd, double TransCmd);

  /// Sets the current state to NewState
  void SetState(simState NewState);

  /// Returns the current state of the CG
  simState GetState(); 

  /// sets the front axle state
  void set_front_state(double dt);

  /// Returns the state of the center front axle
  StateReport GetFrontState();

  /// Returns the state of the center rear axle
  StateReport GetRearState();
};

#endif
