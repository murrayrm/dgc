/*!
 * \file SimMain.cc
 * \brief Main program for simulator
 *
 * \author Dima Kogan, Lars Cremean, Richard Murray
 * \date 2004?
 *
 * This is the main program for asim.  It parses command line
 * arguments, starts up display and actuatorCommand threads and then
 * calls the simulator.
 *
 * \page Asim
 * \section asim_usage Usage
 *
 * Asim is a simple simulator for Alice.  It uses a dynamic 'bicycle'
 * model that keeps track of the forces acting on the tires and
 * generates the state of the vehicle.
 *
 * Usage: asim [options]
 *
 * Use the --help option to get a full list of options.
 *
 * \section asim_concept Concept of operation
 *
 * The main function of the simulator is to accept AdriveCmd messages,
 * integrate the equation of motion for the vehicle and broadcast the
 * VehicleState and ActuatorState.  In this mode, it emulates the
 * combination of adrive, astate and Alice and should be useful for
 * simulations that require this level of kinematic information.
 *
 * There are a couple of important differences between Alice and asim:
 *   - Limited actuator interlocks
 *   - No actuator failure emulation (commands always read, etc)
 *
 */

#include <math.h>
using namespace std;

#include "Sim.hh"
#include "dgcutils/DGCutils.hh"
#include "dgcutils/RDDF.hh"
#include "skynet/skynet.hh"
#include "cmdline.h"

/* Functions defined later in this file */
void initializeFromRDDF(asim &, char *, int);

int main(int argc, char **argv)
{
  /* Process command line options */
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);
  if (cmdline.config_given &&
      cmdline_parser_configfile(cmdline.config_arg, &cmdline, 0, 0, 1) != 0) {
    exit(1);
  }

  /* Retrieve the skynet key */
  int optSNKey = skynet_findkey(argc, argv);
  cerr << "Constructing skynet with KEY = " << optSNKey << endl;

  /* Create the simulator object */
  asim sim(optSNKey, cmdline.steer_lag_arg, cmdline.steer_rate_arg,
	   !cmdline.nonoise_given, (bool) !cmdline.disable_estop_given,
	   (bool) !cmdline.disable_estop_given);

  /* Reset initial condition based on command line flags */
  if (cmdline.rddf_given) {
    // Use an RDDF file to set the initial condition
    initializeFromRDDF(sim, cmdline.rddf_arg, cmdline.rddf_start_arg);
  }

  /* Start up the actuator command thread */
  DGCstartMemberFunctionThread(&sim, &asim::actuatorCommandThread);

  /* Start up the display loop thread */
  DGCstartMemberFunctionThread(&sim, &asim::SparrowDisplayLoop);

  /* Call the simulator */
  sim.Active();

  return 0;
}

/*!
 * \fn initializeFromRDDF(asim sim, char *RDDFfile)
 * \brief Initialize the state of the simulator by reading an RDDF file
 *
 * This function reads an RDDF file and then sets the initial
 * condition of the vehicle to be aligned with the first segment.
 *
 */

void initializeFromRDDF(asim &sim, char *RDDFfile, int startPoint)
{
  RDDF rddf(RDDFfile);

  // Let the user know that we hear him or her
  cerr << "asim: initializing state from RDDF file: " << RDDFfile 
       << "( " << rddf.getNumTargetPoints() << " points)" << endl;

  // check the startPoint to make sure it's not too big or small
  int num_points = rddf.getNumTargetPoints();
  if (startPoint < 0 || startPoint > num_points-2) {
    cerr << "asim: start point " << startPoint << " is not in RDDF" << endl;
  }

  // move the startPoint to index the waypoint array properly. This makes no
  // assumptions about the number of the first waypoint, but does assume that
  // they are numbered consequently
  startPoint -= rddf.getTargetPoints()[0].number;
  startPoint = (startPoint > 0 ? startPoint : 0);

  // Figure out the coordinates of the start point
  double northing = rddf.getWaypointNorthing(startPoint);
  double easting = rddf.getWaypointEasting(startPoint);

  // Now figure out the angle that we should use
  double yaw = atan2(rddf.getWaypointEasting(startPoint+1)  - easting,
		      rddf.getWaypointNorthing(startPoint+1) - northing);

  // Now place the vehicle at the start of the RDDF, in proper orientation 
  sim.setHomePosition(northing, easting, yaw);
}
