/*!
 * \file UT_model.cc
 * \brief Probe the vehicle model
 *
 * \author Richard M. Murray
 * \date 24 September 2007
 *
 * This program allows you to query the value of the right hand side
 * of the vehicle kinematics by entering a state and getting back the
 * derivative.
 * 
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "SimModel.hh"

int main(int argc, char ** argv)
{
  double vstate[NUM_STATES], deriv[NUM_STATES];
  double Flon[2], Flat[2], alpha[2];
  double phi;
  SimModel model;

  /* Get the actuator commands */
  printf("Actuation (steer, accel, trans): "); 
  scanf("%lg %lg %lg", &model.steer, &model.pedal, &model.trans);
  phi = model.steer;

  while (1) {
    printf("State (x, y, th, thd, vlat, vlon): ");
    scanf("%lg %lg %lg %lg %lg %lg", vstate, vstate+1, vstate+2,
	  vstate+3, vstate+4, vstate+5);

    /* Call the model */
    model.sideslip(vstate, phi, alpha);
    model.dTire_Long(Flon, vstate);
    model.FTire_Lat(vstate, phi, Flat);
    model.dPlanar_State(vstate, Flon, phi, deriv);
    
    /* Print out the results */
    printf("  alpha: %g %g\n", alpha[0], alpha[1]);
    printf("  Flon: %g %g\n", Flon[0], Flon[1]);
    printf("  Flat: %g %g\n", Flat[0], Flat[1]);
    printf("  Deriv: %g %g %g %g %g %g\n", deriv[0], deriv[1], deriv[2],
	   deriv[3], deriv[4], deriv[5]);

    /* Print the state information at the rear */
    model.state.simX = vstate[X_I];
    model.state.simY = vstate[Y_I];
    model.state.simTheta = vstate[TH_I];
    model.state.simDTheta = vstate[THD_I];
    model.state.simVLat = vstate[VLAT_I];
    model.state.simVLon = vstate[VLON_I];
    StateReport rear = model.GetRearState();
    printf("  Rear: %g %g %g %g %g %g\n", rear.n, rear.e, rear.nd, rear.ed, 
	   rear.yaw, rear.yawd);
  }
  return 0;
}
