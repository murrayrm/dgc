              Release Notes for "asim" module

Release R1-00l-ahoward (Sun May  6 13:52:17 2007):
  Added --utm-altitude to set a constant altitude offset.

Release R1-00l (Mon Apr 30  8:10:16 2007):
	Added missing return value in GetFrontState that was generating
	warnings.  Also fixed up some documentation errors.   No change in
	function.

Release R1-00k (Thu Apr  5 23:25:25 2007):
	Added options --northing-offset, --easting-offset and 
	--yaw-offset for adding offset in initial position.

Release R1-00j (Wed Apr  4 11:08:27 2007):
	This release of asim reverts to having the yaw angle between -PI and
	PI, consistent with astate R1-00g.

Release R1-00i (Sat Mar 17 20:01:07 2007):
	adding "sluggish" mode to asim (lag in accel command)

Release R1-00h (Thu Mar 15 22:52:10 2007):
	Updated the yaw angle to be between 0 and 2 PI, which matches
	astate.  Note that the standard in the math library (eg, what atan2
	returns) is -PI to PI, so people will need to be careful when they
	compute error in theta.

Release R1-00g (Sat Mar 10 11:49:34 2007):
	This release of 'asim' supports the --rndf option and --rndf-start
	option to allow you to specify the initial position and heading from
	and RNDF file.  The --rndf-start flags should specify the waypoint
	in segment.lane.id format (defaults to 1.1.1).  The heading is
	determined by looking at the next waypoint (or previous, if you are
	at the end of the segment) and set to point along the segment.  You
	can override this with the --initial-yaw argument, if you like.

Release R1-00f (Mon Mar  5 12:48:01 2007):
	Added new flags for setting the initial condition of the simulation.
	Use --initial-northing, --initial-easting and --initial-yaw to set
	the corresponding state variables.  These are processed after the
	--rddf argument, so you can read and RDDF and then update the
	heading if you want.  No other changes in functinality.

Release R1-00e (Sat Mar  3 11:04:09 2007):
	Added two new flags: --initial-accel and --initial-steer to set
	initial acceleration and steering commands.  The accel command can
	be used to keep Alice in place (use asim --initial-accel=-0.5, for
	example).  Also updated the sknyet key processing to use -S or
	--skynet-key (bug 3151)

Release R1-00d (Tue Feb 20 15:48:49 2007):
	Fixed include statements so this revision of asim compiles against revision R2-00a of the interfaces and the skynet modules

Release R1-00c (Wed Jan 31 13:09:47 2007):
	<* Pleaase insert a release notes entry here. If none, then please delete these lines *>

Release R1-00b (Sat Jan 27 17:58:49 2007):
	This is a minor release that fixes a problem with the YaM makefile
	that was causing bad symbolic links to get created.  No change
	in functionality.

	Also testing out the ability to generate automatic bug entries.
	The ChangeLog should generate an entry in bug 3080, if it is working.

Release R1-00a (Sat Jan 13 22:02:24 2007):
	This is the initial release of asim.  It is a cleaned up version 
	of the simulator package under dgc/trunk.  It accepts actuator
	commands, simulates the dynamics of the vehicle, and repots
	the actuator and vehicle state via the appropriate interfaces.  It 
	does not require adrive or the serial library.

	The vehicle dynamics in the simulation include tire forces,
	steering dynamics and relatively accurate kinematics.  The 
	interface has been updated to report the position of the rear
	wheel of the vehicle, consistent with astate.

	This version of the simulator uses gengetopt options, has doxygen
	comments for all files and functions, and roughly conforms to
	the draft coding standards.  

Release R1-00 (Thu Jan 11  7:55:04 2007):
	Created.












