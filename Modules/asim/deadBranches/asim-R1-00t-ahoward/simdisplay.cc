/*!
 * \file simdisplay.cc
 * \brief Simulator display support
 *
 * \author Lars Cremean
 * \date 2004
 *
 * This file contains the support functions needed for the sparrow display.
 */

#include <unistd.h>
#include <iostream>
using namespace std;

#include "Sim.hh"
#include "sparrow/display.h"
#include "sparrow/dbglib.h"
#include "sparrow/errlog.h"

extern SIM_DATUM d;		// simulator internal data (display vars)
extern int QUIT_PRESSED;	// keys that we need to know about
extern int PAUSED;
extern int RESET_PRESSED;

#include "simdisplay.h"		// display table
int estop_pause(long), estop_disable(long), estop_run(long);

/*!
 * \fn asim::SparrowDisplayLoop
 * \brief Display loop (using sparrow)
 *
 * This is the main function that runs the sparrow display loop.
 * Standard sparrow code.
 */

void asim::SparrowDisplayLoop() 
{
  dbg_all = 0;			// turn off sparrow debugging messages

  /* Initialize sparrow */
  if (dd_open() < 0) exit(1);

  /* Set up some handy key bindings */
  dd_bindkey('Q', user_quit);
  dd_bindkey('p', pause_simulation);
  dd_bindkey('u', unpause_simulation);
  dd_bindkey('r', reset_simulation);

  dd_bindkey('P', estop_pause);
  dd_bindkey('R', estop_run);
  dd_bindkey('D', estop_disable);

  /* Select the table to display */
  dd_usetbl(vddtable);

  /* Turn on an error buffer for easier debugging */
# ifdef DEBUG
  dd_errlog_init(50);
  dd_errlog_bindkey();
# endif

  // Wait a bit, because other threads will print some stuff out
  usleep(500000);

  /* Enter the loop until the user quits */
  dd_loop();	

  /* Clsoe the display and notify everyone we are done */
  dd_close();
  QUIT_PRESSED = 1; 		// signal everyone that we are done
}

/*
 * Callback functions
 *
 * The functions below are used to implement callbacks in sparrow.
 *
 */

// TODO: Set the shutdown flag in the quit function
int user_quit(long arg)
{
  return DD_EXIT_LOOP;
}

// Pause the simulation
// Wrapper method to allow binding to sparrow keys
int pause_simulation(long arg)
{
  return pause_simulation();
}

// Unpause the simulation
// Wrapper method to allow binding to sparrow keys
int unpause_simulation(long arg)
{
  return unpause_simulation();
}

// Reset the simulation
// Wrapper method to allow binding to sparrow keys
int reset_simulation(long arg)
{
  return reset_simulation();
}

/* Estop callbacks */
int estop_pause(long arg) { d.actuatorState.m_dstoppos = EstopPause; return 0; }
int estop_run(long arg) { d.actuatorState.m_dstoppos = EstopRun; return 0; }
int estop_disable(long arg) { d.actuatorState.m_dstoppos = EstopDisable; return 0;}
