/*!
 * \fn actuator.cc
 * \brief Actuation interface for asim
 *
 * \author Richard M. Murray
 * \date 13 January 2007
 *
 * This file contains the code for implementing the actuator interface
 * for asim.  At this point, it just reads in the commanded actuator
 * state and stores it in the datum.
 *
 */

#include "Sim.hh"
extern SIM_DATUM d;
extern int QUIT_PRESSED;

/*!
 * \fn sim::actuatorCommandThread
 * \brief Thread for reading actuation commands
 *
 * This function is called as a thread that reads actuation commands
 * and puts them into the simulation command members.
 */
void asim::actuatorCommandThread()
{
  /* Listen for commands from any sender */
  int command_socket = m_skynet.listen(SNdrivecmd, SNadrive_commander);
  drivecmd_t adrive_command;

  while (!QUIT_PRESSED) {
    // Read command from skynet (blocks until read)
    m_skynet.get_msg(command_socket, &adrive_command, 
		     sizeof(adrive_command), 0);

    // Decide what to do based on the actuator that is commanded
    switch (adrive_command.my_actuator) {
    case steer:
      switch (adrive_command.my_command_type) {
      case set_position:
	d.steercmd = adrive_command.number_arg;
	break;
	  
      default:
      cerr << "sim::actuatorCommandThread (steer): " 
	   << "received unknown command " << adrive_command.my_command_type
	   << endl;
      }
      d.steer_cnt++;
      break;

    case accel:
      switch (adrive_command.my_command_type) {
      case set_position:
	d.accelcmd = adrive_command.number_arg;
	break;
	  
      default:
      cerr << "sim::actuatorCommandThread (accel): " 
	   << "received unknown command " << adrive_command.my_command_type
	   << endl;
      }
      d.accel_cnt++;
      break;

    case estop:
      switch (adrive_command.my_command_type) {
      case set_position:
	d.estopcmd = (int) adrive_command.number_arg;
	break;
	  
      default:
      cerr << "sim::actuatorCommandThread (estop): " 
	   << "received unknown command " << adrive_command.my_command_type
	   << endl;
      }
      d.estop_cnt++;
      break;

    case trans:
      switch (adrive_command.my_command_type) {
      case set_position:
	d.transcmd = (int) adrive_command.number_arg;
	break;
	  
      default:
      cerr << "sim::actuatorCommandThread (trans): " 
	   << "received unknown command " << adrive_command.my_command_type
	   << endl;
      }
      d.trans_cnt++;
      break;

    default:
      cerr << "sim::actuatorCommandThread: " 
	   << "received unknown actuator " << adrive_command.my_actuator
	   << endl;
      break;
    }
  }
}

