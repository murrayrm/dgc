/*!
 * \file Sim.hh
 * \brief Class definition for simulator
 *
 * \author Dima Kogan
 * \date 2004?
 *
 * This header file contains the Sim class.
 *
 */

#ifndef SIM_HH
#define SIM_HH

#include "skynet/SkynetContainer.hh"
#include "interfaces/VehicleState.h"
#include "interfaces/ActuatorState.h"
#include "interfaces/ActuatorCommand.h"
#include "SimModel.hh"

#define ESTP_DISABLE 0
#define ESTP_PAUSE 1
#define ESTP_RUN 2

#define GEAR_DRIVE 1
#define GEAR_PARK 0
#define GEAR_REVERSE -1

#define MAXDELAY 100 //max delay for accelcmd

using namespace std;

/*!
 * \struct SIM_DATUM
 * \brief Simulator state
 *
 * The SIM_DATUM structure contains all of the information that
 * defines the current state of the vehicle.
 *
 */
 
struct SIM_DATUM {
  /** Vehicle state struct, from VehicleState.hh. */
  VehicleState SS;

  /** Actuator state structure; use for sending vehicle state */
  ActuatorState actuatorState;

  /* Time stamp (UNIX time in us). */
  unsigned long long lastUpdate;
  unsigned long long startTime;	//!< starting time for the simulation
  double sparrowTime;		//!< time used for sparrow display 

  /* Home position */
  double homeNorthing;		//!< Northing home
  double homeEasting;		//!< Easting home
  double homeYaw;		//!< Yaw home
  int homeZone;      //!< UTM zone number
  char homeLetter;   //!< UTM zone letter

  // Actuator commands - these are filled by actuator receive thread
  double accelcmd;		//!< commanded acceleration
  double steercmd;		//!< commanded steering
  int transcmd;			//!< commanded transmission (gear)
  int estopcmd;			//!< commanded estop 

  /// Command counters (for display purposes)
  int steer_cnt, trans_cnt, accel_cnt, estop_cnt;
};

/*!
 *
 * \class asim
 * \brief Simulation object
 *
 * The asim class is used to represent a complete vehicle simulation.
 * It is derived from CSkynetContainer so that it can receive commands
 * and send vehicle and state data.
 */

class asim : public CSkynetContainer
{
  double m_steerLag, m_steerRateLimit;
  unsigned long long  noisetime;
  int simnoise;
  bool use_estop, use_trans;

  // Hack for local pose
  bool haveInitPos;
  double initPosN, initPosE;

  StateReport	simState;
  SimModel	simEngine;

  double accelDelay[MAXDELAY];
  int inputAccelDelay;
  int accelPtr;

  pthread_mutex_t m_stateMutex;

  void SimInit(void);
  void updateState(void);

  int broadcast_statesock;
  int actuatorStateSocket;

  bool logflag;			//!< Logging enabled
  FILE *logfp;			//!< Log file pointer 

  public:
  int stepusec;			//!< Simulation step size, usec 
  
  // UTM altitude; this allows us to send a fixed non-zero altitude to
  // test correct handling of 2D/3D conversions.
  double utmAltitude;

  public:
  
  asim(int sn_key, double accelLag, double steerLag, double steerRateLimit,
       int noise, bool estop, bool trans);
  ~asim();

  void StateComm();
  void DriveComm();

  void Active();

  void broadcast();

  void SimUpdate(float deltaT);

  void SparrowDisplayLoop();
  void actuatorCommandThread();

  void setHomePosition(double, double, double, int zone, char letter);
  void setNorthing (double);
  void setEasting (double);
  void setYaw (double);

  void gcdriveInit();
  void logInit(char *);
};

/* Global functions for changing the state of the simulator */
int pause_simulation();
int unpause_simulation();
int reset_simulation();
void set_accel(double);
void set_steer(double);

#endif
