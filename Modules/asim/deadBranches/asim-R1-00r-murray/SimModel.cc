/*!
 * \file SimModel.cc
 * \brief Alice simulation model
 *
 * \author Haomiao Huang
 * \date 1/15/2005
 *
 * This is the model that the simulator updates/runs to get vehicle state.  
 * The model is a dynamic bicycle model that models the forces acting upon
 * the tires (lateral and longitudinal) to generate the state of the vehicle.
 * Sideslip is included.  For more information on the model itself see Wiki
 * documentation. 
 *
 * Note: this simulator uses the position of the center of gravity as
 * the reference internally.  You should access the position through
 * the appropriate accessor functions, GetFrontState() and
 * GetRearState().
 *
 * The detailed parameters that define the vehicle (L_f, L_r, etc) are
 * defined in 'AliceConstants.h'.
 */

// TODO: Turn into command-line parameter
int FORCE_NO_SIDESLIP = 0;

#include <math.h>
#include <iostream>
#include <string>
#include <unistd.h>
#include <fstream>
using namespace std;

#include "SimModel.hh"

// Public functions
SimModel::SimModel() {}		///< Constructor (noop) 
SimModel::~SimModel() {}	///< Destructor (noop) 

/*!
 * \fn void SimModel::Init(StateReport& initial, double iTime, double, double);
 * \brief initialize simulator state
 *
 * Initialize the simulator to the conditions as set in the struct and
 * also sets the initial time, and the various steering modeling
 * parameters.  Assumes the location of the rear wheels is given as
 * the initial conditon.
 *
 */

void SimModel::Init(StateReport& initial, double iTime, 
		    double steerLag, double steerRateLimit) 
{ 
  // Steering parameters 
  m_steerLag       = steerLag;
  m_steerRateLimit = steerRateLimit;

  // Initial position of the front wheels
  state.simX = initial.n + L_r * cos(initial.yaw);
  state.simY = initial.e +  L_r * sin(initial.yaw);
  state.simTheta = initial.yaw;
  state.simDTheta = initial.yawd;
  state.simVLon = hypot(initial.nd, initial.ed);
  state.simVLat = 0.0;
  state.simPhi = initial.phi;
  
  // initializes to no brakes/throttle and no steering
  steer =  0.0;
  pedal = -1.0;

  /* Keep track of the offset for the time we will be sent */
  t_last = iTime;
}

/*!
 * \fn void SimModel::RunSim(double step)
 * \brief run the simulation foward in time
 *
 * Sets the actuator commands and then generates the derivatives using
 * private functions that define the dynamics.  Integrates the
 * derivatives forward using a 4th-order Runge Kutta solver.
 *
 * Quick overview of Runge-Kutta Integration: Method of integrating by
 * averaging derivatives across a time step
 *
 * ydot = f(t_n, y_n)
 * k1 = f(t_n, y_n)
 * k2 = f(t_n + dt/2, y_n + k1 * dt/2)
 * k3 = f(t_n + dt/2, y_n + k2 * dt/2)
 * k4 = f(t_n + dt, y_n + k3 * dt)
 * y_n+1 = y_n + dt  * (k1 + 2*k2 + 2 * k3 + k4) / 6
 *
 */
void SimModel::RunSim(double step)
{
  int i; // index for counting

  double F_tlon[NUM_TIRES];  // array containing tire long. forces 
  double PState[NUM_STATES];    // array of planar state
  double phi;                   // steering angle 

  // auxiliary copies of the state arrays for manipulation later 
  double P_aux[NUM_STATES];
  double phi_aux;

  // arrays for storing calculated derivatives
  double k1[NUM_STATES], k2[NUM_STATES], k3[NUM_STATES], k4[NUM_STATES];
  double kp1, kp2, kp3, kp4;

  // now set the variables in the array
  // All computational operations will be on the array rather than on the 
  // struct for simplicity
  PState[X_I] = state.simX;
  PState[Y_I] = state.simY;
  PState[TH_I] = state.simTheta;
  PState[THD_I] = state.simDTheta;
  PState[VLON_I] = state.simVLon;
  PState[VLAT_I] = state.simVLat;

  // steering 
  phi = state.simPhi;
 
  // copy over to the auxilaray array 
  memcpy(P_aux, PState, NUM_STATES * sizeof(double));
  phi_aux = phi;

  // Calculate the change in time from the last update
  //step = currTime - t_last;
  //t_last = currTime;

  // ****
  // Calculate k1
  // k1 = f(y_n)
  // get the tire derivatives 
  dTire_Long(F_tlon, P_aux);

  // get the steering derivatives
  dPhi(phi, kp1);

  // get the planar derivatives
  dPlanar_State(PState, F_tlon, phi, k1);
  // ****
  
  // ****
  // Calculate k2
  // k2 = f(y_n  + dt/2)

  // steering
  phi_aux += kp1 * step / 2.0;
  dPhi(phi_aux, kp2);

  // planar
  for (i = 0; i<NUM_STATES; i++) {
    P_aux[i] += k1[i] * step / 2.0;
  }
  // tires 
  dTire_Long(F_tlon, P_aux);
  dPlanar_State(P_aux, F_tlon, phi_aux, k2);

  // ****
  // restore aux arrays 
  memcpy(P_aux, PState, NUM_STATES * sizeof(double));
  phi_aux = phi;


  // ****
  // Calculate k3
  // k3 = f(y_n  + k2 * dt /2)

  // steering
  phi_aux += kp2 * step / 2.0;
  dPhi(phi_aux, kp3);

  // planar
  for (i = 0; i<NUM_STATES; i++) {
    P_aux[i] += k2[i] * step / 2.0;
  }

  // tires  
  dTire_Long(F_tlon, P_aux);
  dPlanar_State(P_aux, F_tlon, phi_aux, k3);

  // ****
  // restore aux arrays 
  memcpy(P_aux, PState, NUM_STATES * sizeof(double));
  phi_aux = phi;

  // ****
  // Calculate k4
  // k4 = f(y_n  + k3 * dt)

  // steering
  phi_aux += kp3 * step;
  dPhi(phi_aux, kp4);

  // planar
  for (i = 0; i < NUM_STATES; i++) {
    P_aux[i] += k3[i] * step;
  }
  // tires
  dTire_Long(F_tlon, P_aux);
  dPlanar_State(P_aux, F_tlon, phi_aux, k4);
  // ****

  // Add to get new state
  // y_n+1 = y_n + dt * (k1 + 2*k2 + 2*k3 + k4) / 6

  // steering
  phi += step * (kp1 + 2.0 * kp2 + 2.0 * kp3 + kp4) / 6.0;

  if(m_steerLag == 0.0) {

    // Override to have steering rate-limited instead of first order
    // lag "step" is the time since the last update. max steering
    // change in step time is step * m_steerRateLimit

    if((steer_phi - state.simPhi) > step*m_steerRateLimit) {
      // steering rate is saturated to the right
      phi = state.simPhi + step*m_steerRateLimit;
    } else if((steer_phi - state.simPhi) < -step*m_steerRateLimit) {
      // steering rate is saturated to the left
      phi = state.simPhi - step*m_steerRateLimit;
    } else {
      // steering rate is not saturated
      phi = steer_phi;
    }
  }  

  // planar
  for (i = 0; i < NUM_STATES; i++) {
    PState[i] += step * (k1[i] + 2.0 * k2[i] + 2.0 * k3[i] + k4[i]) / 6.0;
  } 

  // **** Copy New States back into Old State ****
  if(sqrt(pow(PState[VLAT_I],2)+ pow(PState[VLON_I],2))>.2) {
      state.simX = PState[X_I]; 
      state.simY = PState[Y_I]; 
      state.simTheta = PState[TH_I]; 
  }
  state.simDTheta = PState[THD_I]; 
  state.simVLon = PState[VLON_I]; 
  state.simVLat = PState[VLAT_I]; 
  state.simPhi = phi;

  //if (state.simVLon < V_EPS)
  //{
  //  state.simVLon = 0;
  //}

  // normalize theta
  state.simTheta = atan2(sin(state.simTheta), cos(state.simTheta));
  
  state.simEngine.simFf = F_tlon[FRONT];
  state.simEngine.simFr = F_tlon[REAR];  

  return;
}

/*!
 * \fn SimModel::SetCommands(double, double, double)
 *
 * This function sets the global steering and pedal commands for
 * future runs of the simulator.  Also converts normalized steering
 * command into a commanded steering angle in radians.  Assumes that
 * normalized steering is from -1 to 1, and that steering commanad is
 * piecewise linear from phi_min to 0, then from 0 to phi_max.
 */
void SimModel::SetCommands(double SteerCmd, double PedalCmd, double TransCmd)
{
  steer = TransCmd*SteerCmd;

  // conversion to radians.  Assumes that normalized steering is commanded
  // from -1 to 1.  Treats the 0 to -VEHICLE_MAX_LEFT_STEER as linear, then 0 
  // to VEHICLE_MAX_RIGHT_STEER as another linear section
  if (steer < 0.0) {
    steer_phi =  VEHICLE_MAX_LEFT_STEER * steer; 

  } else {
    steer_phi = VEHICLE_MAX_RIGHT_STEER * steer;
  }

  pedal = PedalCmd;
  trans = TransCmd;
}

/*!
 * \fn SimModel::GetState()
 * \brief return location of center of mass
 *
 * Used to access the state struct with outside functions
 */
simState SimModel::GetState()
{
  return(state);
}

/*!
 * \fn SimModel::GetRearState()
 * \brief Gives the location of the center of the rear axle 
 *
 * This function computes the location of the rear wheels of the
 * vehicle.  This is computed based on the current state of the center
 * of gravity.
 */
StateReport SimModel::GetRearState()
{
  StateReport rear;

  double thd, vlat, vlon, x, y, theta, phi, xd, yd;

  /* Get the state of the center of mass */
  x = state.simX;
  y = state.simY;
  vlon = state.simVLon;
  vlat = state.simVLat;
  theta = state.simTheta;
  thd = state.simDTheta;
  phi = state.simPhi;

  // Compute the velocity of the rear wheels 
  xd = vlon * cos(theta) - (vlat - L_r * thd) * sin(theta);
  yd = vlon * sin(theta) + (vlat - L_r * thd) * cos(theta);

  // Compute northing, easting and velocities
  rear.n = x - L_r * cos(theta);
  rear.e = y - L_r * sin(theta);
  rear.nd = xd;
  rear.ed = yd;

  // Compute the yaw angle for the vehicle, and derivatives
  rear.yaw = FORCE_NO_SIDESLIP ? atan2(yd, xd) : theta;
  rear.yawd = thd;

  // Steering wheel angle
  rear.phi = phi; 

  return rear;
}

/*!
 * \fn SimModel::GetFrontState()
 * \brief Return location of front wheels
 *
 * Gives the state of the center of the front axle by doing the appropriate
 * conversions from the CG to the front axle
 */
StateReport SimModel::GetFrontState()
{
  StateReport front;
  double thd, vlat, vlon, x, y, theta, phi, xd, yd;
  
  x = state.simX;
  y = state.simY;
  vlon = state.simVLon;
  vlat = state.simVLat;
  theta = state.simTheta;
  thd = state.simDTheta;
  phi = state.simPhi;

  xd = vlon * cos(theta) - (vlat + L_f * thd) * sin(theta);
  yd = vlon * sin(theta) + (vlat + L_f * thd) * cos(theta);

  front.n = x + L_f * cos(theta);
  front.e = y + L_f * sin(theta);
  front.nd = xd;
  front.ed = yd;

  if( FORCE_NO_SIDESLIP )  {
      front.yaw = atan2(yd, xd);

  } else {
    front.yaw = theta;
  }
  front.yawd = thd;
  front.phi = phi; 

  return front;
}

/*
 * Private functions
 *
 * The functions below are private functions of the class.
 *
 */

/*!
 * \fn void SimModel::dTire_Long(double* forces, double* dforces)
 * \brief Tire model
 * 
 * This private function simulates the engine dynamics to generate the
 * derivatives of longitudinal force on the front and rear tires.
 * Current implementation is nothing.  Will put in actual dynamics
 * when we get info from STI.
 */
void SimModel::dTire_Long(double* forces, double* planar)
{
  double vlon = planar[VLON_I];
  double w_f, w_engine, w_wheel, throt_act, throttle, brake;
  double Td, Tf, Te, T_wheel, F_eng, R_we;
  double F_brake, b_act;
  
  if (pedal  >= 0.0 ) {
    throttle = fmin(pedal, 1.0);
    brake = 0;
  } else {
    throttle = 0.0;
    brake = fmax(pedal, -1.0);
  }

  // get the speeds of everything
  R_we =  R_TC * R_TRANS1 * R_DIFF * R_TRANSFER;
  w_wheel = vlon / R_WHEEL;
  w_engine = w_wheel * R_we;
  w_f = fmax(w_engine, W_IDLE);

  // calculate engine forces now
  // get the nonlinear throttle mapping
  throt_act = 1 - exp( -K_T1 * pow(throttle,K_T2));

  // Get the torques and convert to newton-m from lb-ft
  Tf = FUDGE * (K_F1 + K_F2  * w_f + K_F3 * pow(w_f, 2.0)) * LBFT2NM;
  Td = FUDGE * (K_F4 + K_F5  * w_f + K_F6 * pow(w_f, 2.0)) * LBFT2NM;

  // get the resultant torque from throttle setting
  Te = Tf * throt_act + Td * (1.0 - throt_act); 

  // get the torque at the wheel
  T_wheel = Te * R_we; 
  
  // force is torque * wheel radius
  F_eng = T_wheel * R_WHEEL; 

  // calculate brake forces
  // invert brake to go from 0 to 1 in stead of -1
  brake = -brake;
  b_act = 1.0 - exp( -K_B1 * pow(brake,K_B2));
  if (vlon > V_EPS) {
    F_brake = b_act * BF_MAX;

  } else if (vlon < - V_EPS) {
    F_brake = -b_act * BF_MAX;

  } else {
    F_brake = 0.0;
  }

  // resultant force of wheels, two-wheel drive, so just use rear for now
  forces[FRONT] = 0.0;
  forces[REAR] = F_eng + F_brake;
}

/*!
 * \fn void SimModel::FTire_Lat(double* forces, double, double *)
 * \brief Lateral tire forces
 *
 * This model calculates the lateral tire forces on the tires based on
 * the current state of the vehicle and the current steering angle.  
 * The model used is Pacejka's Magic Formula tire model (yes, that's it's
 * real name).  Details on the model implementation can be found in Wiki 
 * models documentation. 
 */
void SimModel::FTire_Lat(double* planar, double phi, double* forces)
{
  double alpha[NUM_TIRES];

  // first calculate sideslip angles
  sideslip(planar, phi, alpha);

  // now use pajecka to calculate the tire forces
  forces[FRONT] = pacejka(alpha[FRONT]);
  forces[REAR] = pacejka(alpha[REAR]);
}

/*!
 * \fn void SimModel::dPhi(double phi, double &dphi)
 * \brief Steering dynamics
 *
 * Calculates the derivative of the steering angle based on the steering
 * dynamics.  Currently model as first-order lag, but changing the model
 * can be easily done by changing the equations here:
 *
 *   dphi = -tau * phi + tau  * u
 *
 * phi: steering angle
 * tau: lag time constant
 * u: command steering angle
 *
 * Note: commanded steering angle is in radians, not normalized
 */
void SimModel::dPhi(double phi, double& dphi)
{
  dphi = m_steerLag * (steer_phi - phi);
}

/*!
 * \fn void SimModel::dPlanar_State(double *, double *, double, double *)
 * \brief Compute derivatives of the planar state
 * 
 * Takes the current planar state of the vehicle, the engine tire forces,
 * and the current steering angle and calculates the derivatives of the planar
 * state (x, y, theta, thetadot, lateral velocity, long. velocity). The model
 * used is a dynamic planar bicycle that uses nonlinear tire forces.  The
 * model is described in greater detail on Wiki documentation. 
 */
void SimModel::dPlanar_State(double* planar, double* ln_forces, 
			     double  phi, double* dplanar)
{

  double F_lat[NUM_TIRES];
  double x, y, th, thd, vlat, vlon;

  // Extract out the variables from the state to make things cleaner
  x = planar[X_I]; 
  y = planar[Y_I]; 
  th = planar[TH_I]; 
  thd = planar[THD_I]; 
  vlat = trans*planar[VLAT_I]; 
  vlon = trans*planar[VLON_I]; 

  // get the lateral tire forces
  FTire_Lat(planar, phi, F_lat);

  // calculate the derivatives
  // See the documentation in the Wiki for details on equations
  dplanar[TH_I] = thd;
  dplanar[X_I] = vlon * cos(th) - vlat * sin(th);
  dplanar[Y_I] = vlon * sin(th) + vlat * cos(th);

  // Accelerations
  dplanar[VLAT_I] = (ln_forces[FRONT] * phi + F_lat[FRONT] + F_lat[REAR]) / 
    VEHICLE_MASS - thd * vlon;
  dplanar[VLON_I] = (ln_forces[FRONT] + ln_forces[REAR] - F_lat[FRONT] * 
		     phi) / VEHICLE_MASS + vlat * thd;
  dplanar[THD_I] = (L_f  * ln_forces[FRONT] * phi  + L_f * F_lat[FRONT] - 
		    L_r * F_lat[REAR]) / I_VEH;
}

/*!
 * \fn void SimModel::sideslip(double* planar, double steer, double* alpha)
 * \brief Compute sideslip angles
 * 
 * Calculates the front and rear sideslip angles based on the current
 * planar state of the vehicle.  See Wiki for detailed documentation.
 *
 */
void SimModel::sideslip(double* planar, double phi, double* alpha)
{
  double vlat, vlon, thd;

  vlat = planar[VLAT_I];
  vlon = planar[VLON_I];
  thd = planar[THD_I];

  alpha[FRONT] = phi - atan2(L_f * thd + vlat, vlon);
  alpha[REAR] = atan2(L_r * thd - vlat, vlon);  
}

/*!
 * \fn void SimModel::pacejka(double alpha)
 * \brief Compute laterial tire forces using Pacejka formula
 *
 * Calculate the lateral tire forces based on slip angle and normal load
 * Uses the Pacejka Magic Formula (empirical formula with data from STI).
 */
double SimModel::pacejka(double alpha)
{
  double Fy;
  Fy = TIRE_Dy * sin(TIRE_Cy * 
                     atan(TIRE_By * alpha - TIRE_Ey *(TIRE_By * alpha - 
                          atan(TIRE_By * alpha))));

  return Fy;
}

