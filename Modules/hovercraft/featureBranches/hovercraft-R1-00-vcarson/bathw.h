#ifndef __BATHW_H__
#define __BATHW_H__
/* File:	bathw.h
 * Description:	The Bat hardware library provides a low-level interface to the
 *		MVWT II Hovercraft (refered hereto as the Bat). Specifically,
 *		this file should provide interfaces to both the fan outputs and
 *		the sensor inputs. Communication is performed, via serial
 *		connection, with an ATMEL microcontroller programmed by Hans
 *		Scholze which generates PWM signals for the fans and reads
 *		sensor data from digital and analog I/O ports. This library is
 *		a wrapper for that serial communcation.
 *
 *		Documentation for each function call can be found in front of
 *		that function's prototype below.
 * OS:		Embedix Linux (Zaurus PDA SA-1110 Architecture)
 * Author:	Rob Christy
 * CVS:		$Id: bathw.h,v 1.5 2003/08/30 03:31:48 waydo Exp $
 *		vim:ts=8:sts=4:sw=4
 */

/* System includes */
#include <inttypes.h>
#include <termios.h>
#include <math.h>

/* Definitions and constants */

    // Default function arguments
#define	DEFAULT_COMM_DEVICE		    "/dev/ttyS0"
#define DEFAULT_COMM_SPEED		    B115200

    // Error codes
enum
{
    BATENONE = 0,		// No error
    BATEREINIT,			// Multiple initializations attempted without cleanup
    BATEOPEN,			// Could not open serial port
    BATEBAUD,			// Could not set serial baud rate
    BATESTTY,			// Could not activate terminal settings
    BATEWRERR,			// A serial write opeation failed
    BATEINVAL,			// An invalid argument was passed to a function
    BATERDERR,			// A serial read operation failed
    BATEUNINIT,			// The bat hardware is not initialized
    BATECOUNT			// Not a real error: the number of error types we have
};

    // Fan constants
#define FAN_MAX				    255
#define FAN_NO_CHANGE			    (-1)
#define FAN_UNDEF			    (-2)

    // Sensor constants
enum {
    SENSOR_GYRO = 0,
    SENSOR_ACCEL,
    SENSOR_HEADING,
    SENSOR_COUNT
};


/* Types */
struct sensor_info
{
    char mnemonic;
    char enabled;
    int (*handler)(uint8_t *);
    uint8_t datasize;
};


/* Functions */

    /* Init/Deinit Functions */
/* Function:	bathw_init
 *
 * Description:	This function initializes the communication (serial) channel to
 *		the Bat hardware. It should be called before any other Bat I/O
 *		functions are called. bathw_init may be called multiple times
 *		only if interspersed with calls to bathw_cleanup. Multiple
 *		calls to bathw_init without bathw_cleanup calls will result in
 *		BATEREINIT errors (see above). 
 *
 *		Note that this function does NOT reset any of the input or
 *		output settings. That is, the programmer cannot make any
 *		assumptions about the output level of the fans or the selected
 *		set of sensors after this call.  Thus, if the programmer wants
 *		to ensure that the fans are turned off, for example, he or she
 *		must call bathw_setfanoutputs(0, 0, 0) immediately after
 *		calling bathw_init.
 *
 * Arguments:	const char *comm_device - the device through which Bat hardware
 *		    communication will be performed. Typically, the constant
 *		    DEFAULT_COMM_DEVICE should be used here.
 *		speed_t speed - the baud rate at which communication should
 *		    occur. The standard baud rate constant (B50-B230400)
 *		    appropriate for Atmel communication must fill this
 *		    argument. Typically, the constant DEFAULT_COMM_SPEED should
 *		    be used here.
 *
 * Returns:	0 if the initialization is successful, -1 on an error. Check
 *		the status of the bathw_errno or bathw_strerror to determine
 *		which error.
 *
 * Errors:	BATERINIT - an attempt to reinitialize the bat hardware was
 *		    made without properly cleaning up the last initialization
 *		BATEOPEN - the serial device could not be opened
 *		BATEBAUD - the baud rate of the serial device could not be set
 *		BATESTTY - the serial port settings could not be activated
 */
int bathw_init(const char *comm_device, speed_t speed);


/* Function:	bathw_cleanup
 *
 * Description:	This function closes the communication channel used to
 *		communicate to the Bat hardware. This function should be called
 *		after all Bat I/O functions have finished.
 */
void bathw_cleanup();


    /* Error handling */
/* Function:	bathw_errno
 *
 * Description:	Returns the error code corresponding to the most recent bathw
 *		library error. The codes and descriptions can be found at the
 *		top of this file.
 *
 * Returns:	The error code
 */
#ifdef GCC
int bathw_errno() __attribute__ ((always_inline));
#else
int bathw_errno();
#endif	// GCC

/* Function:	bathw_strerror
 *
 * Description:	Returns a string describing the most recent bathw library
 *		error.
 *
 * Returns:	A pointer to the string constant or NULL if the error code has
 *		been corrupted.
 */
#ifdef GCC
const char *bathw_strerror() __attribute__ ((always_inline));
#else
const char *bathw_strerror();
#endif	// GCC


    /* I/O Functions */
/* Function:	bathw_get_serial_fd
 *
 * Description:	Returns the file descriptor associated with the serial port.
 *		This piece of data is necessary to watch for read events on the
 *		serial port. For example, the Linux libc functions select and
 *		poll can block on the return value of this function to indicate
 *		when there is sensor data available.
 *
 * Returns:	The file descriptor or -1 if the file descriptor has not yet
 *		been initialized.
 */
#ifdef GCC
int bathw_get_serial_fd() __attribute__ ((always_inline));
#else
int bathw_get_serial_fd();
#endif // GCC


    /* Fan functions */
/* Function:	bathw_setfanoutputs
 *
 * Description: Set the output levels for the three fans on the Bat. Each
 *		argument may take on a value between 0 and FAN_MAX (currently
 *		255) or FAN_NO_CHANGE to indicate that that fan level should
 *		not be altered.
 *
 * Arguments:	int left - the new output level for the left fan 
 *		    (or FAN_NO_CHANGE)
 *		int right - the new output level for the right fan 
 *		    (or FAN_NO_CHANGE)
 *		int lift - the new output level for the central lift fan 
 *		    (or FAN_NO_CHANGE)
 *
 * Returns:	0 if the new output levels were sent to the hardware, -1 on
 *		error (check bathw_errno())
 *
 * Errors:	BATEWRERR - the fan write operation failed
 *		BATEINVAL - one of the values passed to one of the fans was
 *		    invalid
 */
int bathw_setfanoutputs(int left, int right, int lift);

/* Function:	bathw_getfanoutputs
 *
 * Description: Returns the current output setting for each fan. If a NULL
 *		pointer is passed as one of the arguments, then the output
 *		value for that fan is not returned. Conversely, if a non-NULL
 *		pointer is passed but the fan level has not yet been set (ie.
 *		it is unknown), then FAN_UNDEF is returned in that location.
 *
 * Arguments:	int *left - the location where the output level for the left
 *		    fan should be stored
 *		int *right - the location where the output level for the right
 *		    fan should be stored
 *		int *lift - the location where the output level for the lift
 *		    fan should be stored
 */
#ifdef GCC
void bathw_getfanoutputs(int *left, int *right, int *lift) __attribute__ ((always_inline));
#else
void bathw_getfanoutputs(int *left, int *right, int *lift);
#endif	// GCC

    /* Sensor functions */

    /* Common functions */
/* Function:	bathw_update_sensors
 * Description: Reads data from the Atmel and updates any enabled sensors whose
 *		data has arrived. This function should be called whenever a
 *		read event occurs on the serial file descriptor (see
 *		bathw_get_comm_fd()).
 *
 *		Note that the serial read that is performed is blocking. This
 *		means that if no read event occurs on the serial file
 *		descriptor then bathw_update_sensors may not return
 *		immediately.
 *
 * Returns:	0 on success, -1 on error
 *
 * Errors:	BATERDERR - the serial read operation failed
 */
int bathw_update_sensors();

    /* Gyro */
/* Function:	bathw_enable_gyro
 *
 * Description:	Enables the reading of gyro data from the Atmel. The caller
 *		must provide a buffer in which to store the read gyro data.
 *
 * Arguments:	const char *calibfile - This argument is the filename of the
 *		    file containing the gyro calibration data. This data is
 *		    necessary to convert the raw data obtained from the Bat
 *		    hardware into meaningful angular rotation values.
 *		float *gyro_buff - The location where gyro data should be
 *		    stored after a bathw_update_sensors yields new gyro data.
 *		    The size of this buffer should be sizeof(float). That is, 
 *			float my_buff;
 *			bathw_enable_gyro("example_calib.data", &my_buff);
 *		    Should work just fine. On update, a completely calibrated
 *		    value for the angular rate of rotation in radians per
 *		    second will be stored in this location.
 *
 * Returns:	0 on success, -1 on error
 *
 * Errors:	EINVAL - gryo_buff isn't a valid, non-NULL memory address
 */
int bathw_enable_gyro(const char *calibfile, float *gyro_buff);

/* Function:	bathw_disable_gyro
 *
 * Description:	Disables the Atmel's reading of gyro data.
 *
 * Returns:	0 on success, -1 on error (currently never happens)
 */
int bathw_disable_gyro();

    /* Accelerometers */
/* Function:	bathw_enable_accel
 *
 * Description:	Enables the reading of accelerometer data from the Atmel. The
 *		caller must provide buffers in which to store the read
 *		accelerometer data.
 *
 * Arguments:	const char *calibfile - This argument is the filename of the
 *		    file containing the accelerometer calibration data. This
 *		    data is necessary to convert the raw data obtained from the
 *		    Bat hardware into meaningful linear acceleration values.
 *		float *axbuff 
 *		float *aybuff - The locations where accelerometer data should be stored
 *		    after a bathw_update_sensors yields new accelerometer data. The size
 *		    of this buffer should be sizeof(float). That is, 
 *			float my_xbuff, my_ybuff;
 *			bathw_enable_gyro("example_calib.data", &my_xbuff, &,my_ybuff);
 *		    Should work just fine. On update, a completely calibrated
 *		    value for the linear acceleration in both the X and Y
 *		    directions (in meters per second) should be stored in the
 *		    buffer locations.
 *
 * Returns:	0 on success, -1 on error
 *
 * Errors:	EINVAL - one of the arguments isn't a valid, non-NULL memory address
 */
int bathw_enable_accel(const char *calibfile, float *axbuff, float *aybuff);

/* Function:	bathw_disable_accel
 *
 * Description:	Disables the Atmel's reading of accelerometer data
 *
 * Returns:	0 on success, -1 on error (currently never happens)
 */
int bathw_disable_accel();

    /* Heading sensors */
/* Function:	bathw_enable_heading
 *
 * Description:	Enables the reading of heading sensor data from the Atmel. The
 *		caller must provide buffers in which to store the read
 *		heading data.
 *
 * Arguments:	const char *calibfile - The filename of the file containing
 *		    calibration data for the heading sensors. The data in this
 *		    file is necessary to convert the raw data received from the
 *		    Bat hardware into meaningful heading data.
 *		float *hbuff - The location where heading data should be stored
 *		    after a bathw_update_sensors yields new heading data. The size
 *		    of this buffer should be sizeof(float). That is, 
 *			float my_buff;
 *			bathw_enable_gyro("example_calib.data", &my_buff, ...);
 *		    Should work just fine. On update, a completely calibrated
 *		    value for the angular heading (in CCW radians departure
 *		    from magnetic due north) should be stored in the buffer
 *		    location.
 *
 * Returns:	0 on success, -1 on error
 *
 * Errors:	EINVAL - one of the arguments isn't a valid, non-NULL memory address
 */
//int bathw_enable_heading(const char *calibdata, float *hbuff, int *X_min, int *X_max, int *Y_min, int *Y_max);
int bathw_enable_heading(const char *calibdata, float *hbuff);

/* Function:	bathw_disable_heading
 *
 * Description: Disables the Atmel's reading of heading sensor data.
 *
 * Returns:	0 on success, -1 on error (currently never happens)
 */
int bathw_disable_heading();

/* Function:    bathw_heading_reset
 *
 * Description: Reset the heading sensor by creating a short-time 
 *              big magnetic field. This will draw a big current from
 *              the power supply and PLEASE NEVER USED IT REPEATEDLY!
 *
 * Returns:     0 on success, -1 on error (currently never happens)
 */
int bathw_heading_reset();
                                                                                                                             
#endif	// __BATHW_H__
