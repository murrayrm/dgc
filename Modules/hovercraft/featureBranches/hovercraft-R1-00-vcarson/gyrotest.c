//This is a small program used to test gyro sersor
//Zhipu Jin, Feb. 2004

#include <stdio.h>
#include "bathw.h"

int main()
{
	float gyro_value;
	long int counter1;	
	
	if(bathw_init(DEFAULT_COMM_DEVICE, DEFAULT_COMM_SPEED) == -1)
	{
		fprintf(stderr, "Could not initialize Bat hardware: %s\n", bathw_strerror());
		return 1;
	}

	printf("Enabling gyro, please wait....\n");
	if(bathw_enable_gyro("gyro_calib.data", &gyro_value) == -1)
	{
		fprintf(stderr, "Could not enable gyro sensor: %s\n", bathw_strerror());
		return 1;
	}
	
	printf("Done!\n");

	counter1=0;

	while(counter1 <= 2000)
	{
		//printf("Start updating...\n");
		bathw_update_sensors();
		printf("Gyro values = %f \n", gyro_value);
		counter1++;
	}

	bathw_disable_gyro();
	bathw_cleanup();

	return 0;
}

