//This is a small program used to test gyro sersor
//Zhipu Jin, Feb. 2004

#include <stdio.h>
#include <signal.h>
#include "bathw.h"

int stopflag;

int main()
{
	float gyro_value;
	long int counter1;	
	int fleft, fright, fanoffset;
	int F, T;
	float K;
	float aspeed;
	void TimeOut(int);

	if(bathw_init(DEFAULT_COMM_DEVICE, DEFAULT_COMM_SPEED) == -1)
	{
		fprintf(stderr, "Could not initialize Bat hardware: %s\n", bathw_strerror());
		return 1;
	}

	printf("Enabling gyro, please wait....\n");
	if(bathw_enable_gyro("gyro_calib.data", &gyro_value) == -1)
	{
		fprintf(stderr, "Could not enable gyro sensor: %s\n", bathw_strerror());
		return 1;
	}
	
	printf("Done!\n");
	
	//counter1=0;
	fanoffset=55;
	F=1*210;
	T=0;
	K=5;
	aspeed=0.00;
	
	stopflag=0;
	//signal(SIGALRM, TimeOut);
	alarm(4);

	while(stopflag==0)
	{
		printf("Start updating...\n");
		bathw_update_sensors();
		printf("Gyro values = %f \n", gyro_value);

		T=(int)K*(gyro_value-aspeed);
		fleft=(F+T)/2;
		fright=(F-T)/2;

		//set the fan speeds
		bathw_setfanoutputs(fleft+fanoffset, fright+fanoffset, 180+fanoffset);
	}

	bathw_setfanoutputs(0,0,0);
	bathw_disable_gyro();
	bathw_cleanup();

	return 0;
}

int TimeOut(int signo)
{
	char ch;
	
	printf("time out!\n");
	stopflag=1;

}

