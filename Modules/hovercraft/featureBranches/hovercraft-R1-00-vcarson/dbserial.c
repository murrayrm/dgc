#include <fcntl.h>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/types.h>

#define DEFAULT_BAUD	    B115200
int serial_fd = -1;
struct termios *old_ttyconsole = NULL;
struct termios *old_ttyserial = NULL;

int init_ttys(speed_t baud)
{
    static struct termios curr_ttyconsole;
    static struct termios curr_ttyserial;
    struct termios new_ttyconsole;
    struct termios new_ttyserial;

    // Fetch current settings
    tcgetattr(STDIN_FILENO, &curr_ttyconsole);
    tcgetattr(STDIN_FILENO, &new_ttyconsole);
    tcgetattr(serial_fd, &curr_ttyserial);
    tcgetattr(serial_fd, &new_ttyserial);

    // Save old settings
    old_ttyconsole = &curr_ttyconsole;
    old_ttyserial = &curr_ttyserial;

    // Make new settings
    new_ttyconsole.c_lflag &= ~ICANON;
    new_ttyconsole.c_lflag &= ~ECHO;
    if (    cfsetispeed(&new_ttyconsole, baud) == -1 || 
	    cfsetospeed(&new_ttyconsole, baud) == -1)
    {
	perror("Could not set console baud");
	return -1;
    }

    new_ttyserial.c_lflag &= ~ICANON;
    new_ttyserial.c_lflag &= ~ECHO;
    if (    cfsetispeed(&new_ttyserial, baud) == -1 || 
	    cfsetospeed(&new_ttyserial, baud) == -1)
    {
	perror("Could not set serial baud");
	return -1;
    }

    // Try to envoke the new settings
    if (tcsetattr(STDIN_FILENO, TCSANOW, &new_ttyconsole) == -1)
    {
	perror("Could not change console terminal settings");
	return -1;
    }
    if (tcsetattr(serial_fd, TCSANOW, &new_ttyserial) == -1)
    {
	perror("Could not change serial terminal settings");
	return -1;
    }

    return 0;
}

void cleanup()
{
    if (old_ttyconsole != NULL)
	if (tcsetattr(STDIN_FILENO, TCSANOW, old_ttyconsole) == -1)
	    perror("Could not restore console terminal settings");
    if (old_ttyserial != NULL)
	if (tcsetattr(serial_fd, TCSANOW, old_ttyserial) == -1)
	    perror("Could not restore serial terminal settings");

    if (serial_fd != -1)
	close(serial_fd);
}

int main()
{
    int ret;
    speed_t baud = DEFAULT_BAUD;
    unsigned char c;
    fd_set readfds;
    
    if ((serial_fd = open("/dev/ttyS0", O_RDWR)) == -1)
    {
	perror("Could not open the serial port for reading or writing");
	return 1;
    }

    if (init_ttys(baud) == -1)
    {
	cleanup();
	return 1;
    }

    while (1)
    { 
	FD_ZERO(&readfds);

	FD_SET(STDIN_FILENO, &readfds);
	FD_SET(serial_fd, &readfds);

	// assume STDIN_FILENO == 0
	if (select(serial_fd + 1, &readfds, NULL, NULL, NULL) == -1)
	{
	    perror("Select failed");
	    break;
	}

	if (FD_ISSET(STDIN_FILENO, &readfds))
	{
	    if ((ret = getc(stdin)) == -1)
	    {
		perror("Couldn't read from stdin");
		break;
	    }

	    c = (unsigned char)ret;
	    printf("Writing  '%c' == %2x (%3d) to serial\n", c, c);

	    if (write(serial_fd, &c, sizeof(c)) == -1)
	    {
		perror("Couldn't write to serial port");
		break;
	    }
	}

	if (FD_ISSET(serial_fd, &readfds))
	{
	    if ((ret = read(serial_fd, &c, sizeof(c))) == -1)
	    {
		perror("Couldn't read from serial port");
		break;
	    }
	    
	    if (ret)
		printf("Received '%c' == %2x (%3d) from serial\n", c, c);
	}
    }

    cleanup();
    return 0;
}
