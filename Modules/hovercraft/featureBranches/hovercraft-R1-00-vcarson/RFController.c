/* File:	RFController.c
 * Description:	This is the RoboFlag controller for the Bat. That is,
 *		this program runs on the Bat Zaurus during a RoboFlag game.
 *		Essentially, this program sits, waiting for command data from
 *		the RoboFlag arbiter (forwarded by MVServer) and then attempts
 *		to execute those commands to the best of its abilities.
 * OS:		Embedix Linux (Zaurus PDA SA-1110 Architecture)
 * Author:	Rob Christy
 * CVS:		$Id: RFController.c,v 1.5 2003/08/25 22:28:01 waydo Exp $
 */

/* System Includes */
#include <errno.h>
#include <ieee754.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/times.h>
#include <sys/types.h>

/* Local Includes */

#include "../../include/mvwt/MVWT1VisionPacket.h" 
#include "../../include/mvwt/MVCommandPacket.h" 

#include "controller.h"
#include "bathw.h"

/* Definitions */
/* These definitions approximate the quality of data we expect to receive from
 * the vision system */
#define THETA_NOISE			0.087266463	// radians = 5 degrees
#define VELOCITY_NOISE			0.05		// m/s
#define VELOCITY_ZERO			0.0001		// m/s
#define LIFT_ON				190

#define STATE_SIZE			6
#define STATE_X				0
#define STATE_Y				1
#define STATE_THETA			2
#define STATE_X_DOT			3
#define STATE_Y_DOT			4
#define STATE_THETA_DOT			5

#define REF_SIZE			2
#define REF_X_VEL			0
#define REF_Y_VEL			1

#define DEFAULT_GAINS_FILE		"gains.txt"
#define DEFAULT_PARAMS_FILE		"parameters.txt"

#define USAGE "\
usage:\n\
 RFController [-h] [-c PORT] [-v PORT] VEHICLE_NUM\n\
This is the RoboFlag controller for Bats. It will listen for and execute\n\
commands from RoboflagArbiter.exe forwarded by MVServer. VEHICLE_NUM should\n\
be the identifying number of the vehicle.\n\n\
Option:\n\
    -c	Select the port on which commands are to be received (default %d)\n\
    -v	Select the port on which vision data is to be received (default %d)\n\
    -h	Display this usage message\n"


/* File-wide variables */

int comm_sock = -1;
int vision_sock = -1;
int vehicle_id;
float ref[REF_SIZE] = { 0, 0 };

/* Functions */

/* Function:    word_swap
 * Description: Swaps the words of the double word argument. Note that here the
 *		term "word" refers to a word on a 32-bit architecture. To make
 *		this function truly a word swap, int's would be used here
 *		rather than uint32_t's. They are not, however, because the
 *		primary use of this function is for swapping IEEE 754 "float
 *		words" which are 32 bits.
 * Argument:	void *dword - a pointer to a 64-bit value whose 32-bit halves
 *		    are to be swapped.
 */
void word_swap(void *dword)
{
    uint32_t tmp;
    
    tmp = ((uint32_t *)dword)[0];
    ((uint32_t *)dword)[0] = ((uint32_t *)dword)[1];
    ((uint32_t *)dword)[1] = tmp;
}

/* Function:	init_command_socket
 * Description:	Initializes the socket on which Roboflag commands are to be
 *		received. This means creating the socket and binding it to a
 *		local address (port).
 * Argument:	short command_port - the port to which this socket is bound in
 *		    host byte order
 * Returns:	0 on success, -1 on error
 */
int init_command_socket(short command_port)
{
    struct sockaddr_in sa;

    // Create command socket
    if ((comm_sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
	fprintf(stderr, "FATAL: socket: %s\nCould not create command socket.\n",
		strerror(errno));
	return -1;
    }

    // We will receive commands on the local port command_port
    sa.sin_family = AF_INET;
    sa.sin_port = htons(command_port);
    sa.sin_addr.s_addr = htonl(INADDR_ANY);

    // Bind the socket so we can receive commands
    if (bind(comm_sock, (struct sockaddr *)&sa, sizeof(sa)) == -1)
    {
	fprintf(stderr, "FATAL: bind: %s\nCould not bind command socket to local port %d.\n",
		strerror(errno), command_port);
	return -1;
    }
    
    return 0;
}

/* Function:	init_vision_socket
 * Description:	Initializes the socket on which MVWT vision data will be
 *		received. This means both creating the socket and binding it to
 *		a local port.
 * Argument:	short vision_port - the port, in host byte order, to which the
 *		    socket should be bound
 * Returns:	0 on success, -1 on error
 */
int init_vision_socket(short vision_port)
{
    struct sockaddr_in sa;

    // Create the vision socket
    if ((vision_sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
	fprintf(stderr, "FATAL: socket: %s\nCould not create vision socket.\n",
		strerror(errno));
	return -1;
    }

    // We will receive commands on the local port vision_port
    sa.sin_family = AF_INET;
    sa.sin_port = htons(vision_port);
    sa.sin_addr.s_addr = htonl(INADDR_ANY);

    // Bind the socket so we can receive commands
    if (bind(vision_sock, (struct sockaddr *)&sa, sizeof(sa)) == -1)
    {
	fprintf(stderr, "FATAL: bind: %s\nCould not bind vision socket to local port %d.\n",
		strerror(errno), vision_port);
	return -1;
    }

    return 0;
}

/* Function:	handle_command
 * Description: Once data is received on the command socket, this function
 *		should be called to read the data and take the appropriate
 *		action. Actually, the controller update occurs in
 *		handle_vision.  Since our controller is static and vision data
 *		should update at a faster rate than reference data, updates are
 *		synchonized with new state data. This function, in actuality
 *		only sets the reference value to be used at the controller
 *		update step. 
 * Returns:	0 on success, -1 on error
 */
int handle_command()
{
    int num_bytes;
    int speed;
    int sret;
    CommandPacket cp;
    fd_set readfds;
    struct timeval tv;

    // This is the fetch loop. We fetch commands (ignoring them, mostly
    // commands) until there are no more in the queue.
    do
    {
	// Read the next packets-worth of data off the network interface
	if ((num_bytes = recv(comm_sock, &cp, sizeof(cp), MSG_WAITALL)) == -1)
	{
	    fprintf(stderr, "recv: %s\nCould not receive command packet\n",
		    strerror(errno));
	    return 0;
	}

	// Make sure that we got a whole packet
	if (num_bytes < sizeof(cp))
	{
	    fprintf(stderr, "Incomplete command packet dropped. Size = %d\n", num_bytes);
	    return 0;
	}

	// Since both CMD_SHUTDOWN and CMD_IDLE are imperative, we don't want
	// to skip them
	switch (cp.type)
	{
	    case CMD_SHUTDOWN:
		// We're supposed to shutdown this program
		return -1;
		break;

	    case CMD_IDLE:
		// Stop the robot

		break;

	    case CMD_VELOCITY:
		// Velocity commands are handled outside of the fetch loop
		break;

	    default:
		fprintf(stderr, "FATAL: Invalid command received.\n");
		return -1;
		break;
	}

	// Now do a select with a zero-time timeout. This will effectively test
	// to see if there any data to be read on the command socket.
	FD_ZERO(&readfds);
	FD_SET(comm_sock, &readfds);
	tv.tv_sec = 0;
	tv.tv_usec = 0;

	if ((sret = select(comm_sock + 1, &readfds, NULL, NULL, &tv)) == -1)
	{
	    fprintf(stderr, "FATAL: select %s\nSelect failed.\n", strerror(errno));
	    return -1;
	}

	// Loop and get the next packet if it has already arrived
    } while (sret);

    // Finally, execute the last velocity command we encountered
    if (cp.type == CMD_VELOCITY)
    {
	printf("Got sent a command, v = %f, %f\n", cp.xvel.f, cp.yvel.f);
	// make command available
	ref[REF_X_VEL] = cp.xvel.f;
	ref[REF_Y_VEL] = cp.yvel.f;
    }

    return 0;
}

/* Function:	handle_vision
 * Description:	Once vision data is received this function should be called to
 *		read and handle it. Specifically, once new state data is
 *		received, this function updates the controller and sends the
 *		new controller outputs to the fans.
 * Returns:	0 on success, -1 on error
 */
int handle_vision()
{
    int num_bytes;
    int sret;
    int fn_r, fn_l;
    float fanforces[2];
    VisionPacket vp;	
    fd_set readfds;
    struct timeval tv;
    static char visible = 0;
    static uint8_t leftlevel = 0; 
    static uint8_t rightlevel = 0; 
    static uint8_t liftlevel = 0;
    static int dropped = 0;
    static float state[STATE_SIZE];

    printf("Got vision.\n");

    do
    {
	// Read the next packets-worth of data off the network interface
	if ((num_bytes = recv(vision_sock, &vp, sizeof(vp), MSG_WAITALL)) == -1)
	{
	    fprintf(stderr, "recv: %s\nCould not receive vision packet\n");
	    return 0;
	}

	// Make sure that we got a whole packet
	if (num_bytes < sizeof(vp))
	{
	    fprintf(stderr, "Incomplete vision packet dropped. Size = %d\n", num_bytes);
	    return 0;
	}

	// If we've lost a packet,
	if (((union ieee754_float *)(&vp.vehicle[vehicle_id].x))->f == 0)
	{
	    // Use the last theta as long as we haven't lost too many
	    if (dropped < MAX_FRAME_SKIP)
		dropped++;
	    // If we have lost too many, we've become invisible
	    else
		visible = 0;
	}
	// Otherwise, extract this robot's current angle from the vision packet
	else
	{
	    // Correct the float word order of the incoming data
	    word_swap(&vp.vehicle[vehicle_id].x);
	    word_swap(&vp.vehicle[vehicle_id].y);
	    word_swap(&vp.vehicle[vehicle_id].theta);
	    word_swap(&vp.vehicle[vehicle_id].xdot);
	    word_swap(&vp.vehicle[vehicle_id].ydot);
	    word_swap(&vp.vehicle[vehicle_id].thetadot);

	    // Copy the new vision data to where the controller can use it
	    state[STATE_X]		= vp.vehicle[vehicle_id].x;
	    state[STATE_Y]		= vp.vehicle[vehicle_id].y;
	    state[STATE_THETA]		= vp.vehicle[vehicle_id].theta;
	    state[STATE_X_DOT]		= vp.vehicle[vehicle_id].xdot;
	    state[STATE_Y_DOT]		= vp.vehicle[vehicle_id].ydot;
	    state[STATE_THETA_DOT]	= vp.vehicle[vehicle_id].ydot;

	    printf("got vision: state = (%f, %f, %f, %f, %f, %f)\n",
		state[0], state[1], state[2], state[3], state[4], state[5]);
	    // So we've dropped zero consecutive packets and the vehicle is visible
	    dropped = 0;
	    visible = 1;
	}

	// Now do a select with a zero-time timeout. This will effectively test
	// to see if there any data to be read on the vision socket.
	FD_ZERO(&readfds);
	FD_SET(vision_sock, &readfds);
	tv.tv_sec = 0;
	tv.tv_usec = 0;

	if ((sret = select(vision_sock + 1, &readfds, NULL, NULL, &tv)) == -1)
	{
	    fprintf(stderr, "FATAL: select %s\nSelect failed.\n", strerror(errno));
	    return -1;
	}

	// Loop and get the next packet if it has already arrived
    } while (sret);

    // If the vehicle is visible, envoke the controller
    if (visible)
    {

      
      ref[0] = 0.2;
      ref[1] = 0.2;

	printf("Is visible.\n");
	// Special case: if the commanded reference is zero, just shut everything off
	if (fabsf(ref[0]) < VELOCITY_ZERO && fabsf(ref[1]) < VELOCITY_ZERO)
	{
	    printf("Turnning off everything\n");
	    bathw_setfanoutputs(0, 0, 0);
	    leftlevel = rightlevel = liftlevel = 0;
	}
	// Otherwise, fly as usual
	else
	{
	    /*
	    printf("Updating controller; state = (%f, %f, %f, %f, %f, %f); ref = (%f, %f)\n",
		state[0], state[1], state[2], state[3], state[4], state[5],
		ref[0], ref[1]);
	    */
	    update_controller(state, ref, fanforces);

	    // Do force mapping
	    // This is hardcoded at the moment and a more elegant, flexible
	    // solution should be implemented as soon as we get REAL force map data
	    fn_r = (fanforces[0] + 0.03985) / 0.003745;
	    fn_l = (fanforces[1] + 0.03985) / 0.003745;

	    printf("fan forces: right=%d left= %d \n", fn_r, fn_l);
	    
	    // Send forces to hardware
	    bathw_setfanoutputs(
		    fn_l == leftlevel    ? FAN_NO_CHANGE : fn_l,
		    fn_r == rightlevel   ? FAN_NO_CHANGE : fn_r, 
		    LIFT_ON == liftlevel ? FAN_NO_CHANGE : LIFT_ON);
	    leftlevel = fn_l;
	    rightlevel = fn_r;
	    liftlevel = LIFT_ON;

	    printf("fan output: %d %d %d\n", leftlevel, rightlevel, liftlevel);
	}
    }
    // If the vehicle is not visible, shut it off
    else
    {
	// Shut off the vehicle
	bathw_setfanoutputs(0, 0, 0);
	leftlevel = rightlevel = liftlevel = 0;
    }
}

/* Function:	cleanup
 * Description:	Close file handles and perform Bat hardware-related cleanup
 */
void cleanup()
{
    // Do the Bat-related cleanup
    bathw_setfanoutputs(0, 0, 0);
    bathw_cleanup();

    // Close any sockets that are open
    if (comm_sock != -1)
	close(comm_sock);

    if (vision_sock != -1)
	close(vision_sock);
}

/* Function:	main
 * Description: C language-defined code entry point
 * Arguments:	Standard command-line arguments
 * Returns:	Program error code
 */
int main(int argc, char **argv)
{
    int c, targ, swidth, sret;
    char *endptr;

    // Set the default ports to be overwritten if command-line options are used
    short comm_port = DEFAULT_COMMAND_PORT;
    short vision_port = DEFAULT_VISION_PORT;

    fd_set readfds;
    struct timeval tv;

    // Get the command-line arguments
    while ((c = getopt(argc, argv, "c:v:h")) != -1)
    {
	switch (c)
	{
	    case 'c':
		targ = strtol(optarg, &endptr, 0);

		if (*endptr || targ < 0 || targ > USHRT_MAX)
		    fprintf(stderr, "Invalid port %s ignored\n", optarg);
		else
		    comm_port = targ;
		break;

	    case 'v':
		targ = strtol(optarg, &endptr, 0);

		if (*endptr || targ < 0 || targ > USHRT_MAX)
		    fprintf(stderr, "Invalid port %s ignored\n", optarg);
		else
		    vision_port = targ;
		break;

	    case 'h':
		printf(USAGE, DEFAULT_COMMAND_PORT, DEFAULT_VISION_PORT);
		break;

	    case '?':
		fprintf(stderr, "Unrecognized option '%c' ignored.\n", c);
		break;

	    default:
		break;
	}
    }

    // Check to make sure that there is exactly one argument left
    if (optind != argc - 1)
    {
	fprintf(stderr, "Argument missing. Which vehicle is this?\n" USAGE,
		DEFAULT_COMMAND_PORT,
		DEFAULT_VISION_PORT);
	return 1;
    }

    // Get the vehicle id off of the command line
    vehicle_id = strtol(argv[optind], &endptr, 0);
    if (*endptr || vehicle_id <= 0 || vehicle_id > NUM_VEHICLES)
    {
	fprintf(stderr, "Argument is invalid vehicle identifier. It must be an integer between 0 and %d.\n" USAGE,
		NUM_VEHICLES,
		DEFAULT_COMMAND_PORT,
		DEFAULT_VISION_PORT);
	return 1;
    }
    // We used the zero-based vehicle id's here
    vehicle_id--;

    // Load the controller
    if (load_controller(DEFAULT_GAINS_FILE, DEFAULT_PARAMS_FILE) == -1)
    {
	cleanup();
	return 1;
    }

    // Set up the networking
    if (init_command_socket(comm_port) == -1)
    {
	cleanup();
	return 1;
    }

    if (init_vision_socket(vision_port) == -1)
    {
	cleanup();
	return 1;
    }

    // Initialize Bat hardware
    if (bathw_init(DEFAULT_COMM_DEVICE, DEFAULT_COMM_SPEED) == -1)
    {
	fprintf(stderr, "bathw_init: %s\nCould not initialize Bat hardware\n",
		bathw_strerror());
	cleanup();
	return 1;
    }

    // Initialize the fans to zero output
    bathw_setfanoutputs(0, 0, 0);

    // Main loop
    swidth = (vision_sock > comm_sock ? vision_sock : comm_sock) + 1;

    while (1)
    {
	// This is a dummy loop at the moment. At some point in the near future
	// it will serve to periodically check the battery
	do
	{
	    FD_ZERO(&readfds);
	    FD_SET(comm_sock, &readfds);
	    FD_SET(vision_sock, &readfds);

	    tv.tv_sec = 1;
	    tv.tv_usec = 0;

	    if ((sret = select(swidth, &readfds, NULL, NULL, NULL)) == -1)
	    {
		fprintf(stderr, "FATAL: select %s\nSelect failed.\n", strerror(errno));
		cleanup();
		return 1;
	    }

	} while (!sret);

	// The vision update must come before the command update because having
	// stale vision data can cause loops
	if (FD_ISSET(vision_sock, &readfds) &&
		handle_vision() == -1)
	    break;

	if (FD_ISSET(comm_sock, &readfds) && 
		handle_command() == -1)
	    break;
    }

    // Cleanup and exit
    cleanup();
    return 0;
}
