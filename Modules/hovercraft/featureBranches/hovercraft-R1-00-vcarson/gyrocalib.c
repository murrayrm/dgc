#include <errno.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>
#include "bathw.h"

// We're stealing the bathw libraries internal table so we can read unadjusted
// gyro calibration data.
extern struct sensor_info sensors[SENSOR_COUNT];

int raw_gyro_handler(uint8_t *buffer);

int bat_fd = -1;
int samples = 0;
FILE *logfile = NULL;

int main(int argc, char **argv)
{
    int c;
    char *filename = "gyrodata.log";
    float unused;
    float time;
    fd_set readfds;
    struct timeval starttime;
    struct timeval endtime;

    while ((c = getopt(argc, argv, "f:")) != -1)
    {
	switch (c)
	{
	    case 'f':
		filename = optarg;
		break;

	    default:
		fprintf(stderr, "Unrecognized option ignored.\n");
		break;
	}
    }

    // Initialize the serial communication to the hovercraft 
    if (bathw_init(DEFAULT_COMM_DEVICE, DEFAULT_COMM_SPEED) == -1)
    {
	fprintf(stderr, "bathw_init: %s\nCould not initialize hardware.\n",
		bathw_strerror());
	bathw_cleanup();
	return 1;
    }

    // Open the log file
    if ((logfile = fopen(filename, "w")) == NULL)
    {
	fprintf(stderr, "fopen: %s\nCould not open log file.\n",
		strerror(errno));
	bathw_cleanup();
	return 1;
    }

    // Get the serial file descriptor
    bat_fd = bathw_get_serial_fd();

    // Hijack the gyro update function
    sensors[SENSOR_GYRO].handler = &raw_gyro_handler;

    // Start receiving gyro data
    if (bathw_enable_gyro("gyro_calib.data", &unused) == -1)
    {
	fprintf(stderr, "bathw_enable_gyro: %s\nCould not enable the gyro.\n",
		bathw_strerror());
	bathw_cleanup();
	return 1;
    }

    gettimeofday(&starttime, NULL);

    while (1)
    {
	FD_ZERO(&readfds);
	FD_SET(bat_fd, &readfds);
	FD_SET(STDIN_FILENO, &readfds);
    
	// Assumes that STDIN_FILENO == 0
	if (select(bat_fd + 1, &readfds, NULL, NULL, NULL) == -1)
	{
	    fprintf(stderr, "select failed. bad.\n"); 
	    break;
	}

	if (FD_ISSET(STDIN_FILENO, &readfds))
	    break;

	if (FD_ISSET(bat_fd, &readfds))
	{
	    if (bathw_update_sensors() == -1)
		fprintf(stderr, "bathw_update_sensors: %s\nSensor update failed\nread: %s",
			bathw_strerror(), strerror(errno));
	}
    }

    gettimeofday(&endtime, NULL);

    time = (float)(endtime.tv_sec - starttime.tv_sec) + 1e-6*(float)(endtime.tv_usec - starttime.tv_usec);
    printf("Took %d samples in %f seconds (%f samples/sec)\n",
	    samples, time, (float)samples/time);

    // Shutdown
    fclose(logfile);
    bathw_cleanup();
}

int raw_gyro_handler(uint8_t *gd)
{
    fprintf(logfile, "%d\n", (gd[1] & 3) * 256 + gd[0]);
    samples++;

    return 0;
}
