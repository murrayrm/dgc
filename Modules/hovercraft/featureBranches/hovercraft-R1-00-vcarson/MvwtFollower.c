#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <ieee754.h>
#include <sys/times.h>
#include <sys/types.h>
#include "bathw.h"

#include "MvwtFollower.h"
#include "../../include/mvwt/MVParameters.h"
#include "../../include/falcon/traj.h"



/// Global Variables


/// Output file names
FILE* m_sfp;
FILE* m_efp;
FILE* m_ffp;


/// Output file names
char* m_stateFile;
char* m_errFile; 
char* m_forceFile ;

/// The current trajectory
TRAJ_DATA* m_traj;

/// Vehicle ID number
int m_vehicleId;

/// Vision socket number
int m_visionSocket;

/// Vision port
int m_visionPort;

/// Command socket number
int m_commandSocket;

/// The current vision packet
float m_state[11];

/// The errors in state with
/// with respect to projection
/// onto current trajectory
float m_trajError[11];

/// The current point on the trajectory that
/// nearest to
float m_trajPoint[11];

/// Gains data
GainsData m_gains;

/// Controller type
int m_ctrlType;

/// File descriptor
fd_set m_readfds;

int initialize() {

    if (initVisionSocket(m_visionPort) == -1) {
	return 1;
    }

    // Initialize Bat hardware
    if (bathw_init(DEFAULT_COMM_DEVICE, DEFAULT_COMM_SPEED) == -1)
    {
	fprintf(stderr, "bathw_init: %s\nCould not initialize Bat hardware\n",
		bathw_strerror());
	return 1;
    }

    // Initialize the fans to zero output
    bathw_setfanoutputs(0, 0, 0);

    FD_ZERO(&m_readfds);
    FD_SET(m_visionSocket, &m_readfds);

    printf("Initialized the vision_socket.\n");       
}
int deinitialize() {
  
  // Do the Bat-related cleanup
   bathw_setfanoutputs(0, 0, 0);
   bathw_cleanup();

     // Close any sockets that are open
     if (m_commandSocket != -1)
	 close(m_commandSocket);

     if (m_visionSocket != -1)
	 close(m_visionSocket);
 }

void control() {

  while(!isTrajComplete()) {
    
    updateState();
    
    if (LAT == m_ctrlType) {
      calculateStateError();
      lateralControl();
    }
  }
}

void lateralControl() {

  static uint8_t leftlevel = 0; 
  static uint8_t rightlevel = 0; 
  static uint8_t liftlevel = 0;

  int fn_r, fn_l;
  float u_1, u_2;
  float forces[2], fanForces[3];

  int offset = 175;

  u_1 = -m_gains.y1*m_trajError[Y] - m_gains.t1*m_trajError[THETA] - m_gains.ydot1*m_trajError[YDOT]- 
    m_gains.tdot1*m_trajError[THETADOT] + m_trajError[U1FF];

  u_2 = -m_gains.y2*m_trajError[Y] - m_gains.t2*m_trajError[THETA] - m_gains.ydot2*m_trajError[YDOT]- 
    m_gains.tdot2*m_trajError[THETADOT] + m_trajError[U2FF];

  // calculate fan forces
  //forces[0] = max(0, min(0.5*(u_1 + u_2),f_max));
  //forces[1] = max(0, min(0.5*(u_1 - u_2),f_max));

  forces[0] = u_1;
  forces[1] = u_2;

  printf("forces : right=%d left= %d \n", u_1, u_2);
  // fn_r = maximum(minimum(forces[0], FMAX), 0);
  //fn_l = maximum(minimum(forces[1], FMAX), 0);

  fn_r = forces[0] - offset;
  fn_l = forces[1] + offset;


  fanForces[0] = fn_l;
  fanForces[1] = -fn_r;
  fanForces[2] = LIFT_ON+offset;


  writeOutputData(m_ffp, fanForces, m_forceFile);

  printf("fan forces: right=%d left= %d \n", fn_r, fn_l);

  //printf("fan forces: right=%d left= %d \n", fn_r, fn_l);


  
  // Send forces to hardware
  //  bathw_setfanoutputs(
  //	      fn_l == leftlevel    ? FAN_NO_CHANGE : fn_l,
  //	      fn_r == rightlevel   ? FAN_NO_CHANGE : fn_r, 
  //	      LIFT_ON == liftlevel ? FAN_NO_CHANGE : LIFT_ON);


  bathw_setfanoutputs( fn_l, -fn_r, LIFT_ON+offset);
  

  printf("fan output: %d %d %d\n", fn_l, fn_r, LIFT_ON);
  printf("fan output: %d %d %d\n", fn_l, fn_r, LIFT_ON);
  printf("fan output: %d %d %d\n", fn_l, fn_r, LIFT_ON);
  printf("fan output: %d %d %d\n", fn_l, fn_r, LIFT_ON);

  sleep(0.2);
}


int closestTrajPoint() {

  int nearestIndex = 0; // save value to avoid going backwards
  double  nearestDistSq;
  double  currentDistSq;
  double rowVal[11];
  double time;
  int i = 0; 
  
  // Compute the nearest point on the trajectory to where we currently are
  nearestDistSq = 1.0e20; //FIX what is this? 
  for(i=nearestIndex; i<(*traj_rows)(m_traj); i++) {
    (*traj_row)(m_traj, i, &time, rowVal);
    currentDistSq =
      pow(m_state[X] - rowVal[2+X],2.0) +
      pow(m_state[Y] - rowVal[2+Y] ,2.0);
    
    if(currentDistSq < nearestDistSq) {
      nearestDistSq = currentDistSq;
      nearestIndex = i;
    }
  }
    return nearestIndex; 
}

void calculateStateError() {

  int i, status, trajpoint, startPoint; 
  double trajTime;
  double trajStartVector[11], trajEndVector[11], trajVector[11];
  float refVel; 

  /// Find the nearest point on the desired trajectory 
  /// normal  to the current vehicle orientation.

  // Get the trajectory entry for the segment we are on
  startPoint = closestTrajPoint();
  trajpoint = startPoint;

  // Get the start point and end point
  (*traj_row)(m_traj, startPoint, &trajTime, trajStartVector);
  (*traj_row)(m_traj, startPoint+1, &trajTime, trajEndVector);


  // Get the trajectory for the closest point 
  traj_read(m_traj, trajVector, trajTime);


  printf(" trajVector = (%8f, %8f, %8f, %8f, %8f, %8f,%8f, %8f,%8f, %8f)\n",
	 trajVector[T], trajVector[X], trajVector[Y], trajVector[THETA], trajVector[XDOT], trajVector[YDOT], 
	 trajVector[THETADOT],trajVector[U1FF], trajVector[U2FF]);
  
  // Compute lateral distance in body coordinates
  m_trajError[T] = 0;
  m_trajError[X] = 0;


  //Compute y error 
  m_trajError[Y] =
   - (m_state[X] - trajVector[2+X])*sin(trajVector[2+THETA]) + 
   (m_state[Y] - trajVector[2+Y])*cos(trajVector[2+THETA]);

  // Compute error in heading versus angle along the path
  m_trajError[THETA] = m_state[THETA] - trajVector[2+THETA];

  // Figure out the velocity along the path

  refVel = sqrt(pow(trajVector[2+XDOT], 2.0) +
  		     pow(trajVector[2+YDOT], 2.0));

  // Compute the lateral velocity error in body coordinates
  m_trajError[XDOT] = 0;
  m_trajError[YDOT] = -refVel * sin(m_trajError[THETA]);

  // Compute heading rate error along the path
  m_trajError[THETADOT] = m_state[THETADOT] - trajVector[2+THETADOT];

  writeStateData(m_efp, m_trajError, m_errFile);
  printf(" trajError = (%8f, %8f, %8f, %8f, %8f, %8f)\n",
	 m_trajError[X], m_trajError[Y], m_trajError[THETA], m_trajError[XDOT], m_trajError[YDOT], 
	 m_trajError[THETADOT]);  

}


void updateState() {
  estimateState();

}

void estimateState() {
  if (FD_ISSET(m_visionSocket, &m_readfds)) {
    printf("Vision socket is set.\n");       
    receiveVisionData();
  }
  
  getNewGyroData();
  
  /** Do some sensor fusion to estimate the state here */
  
}

 void getNewVisionData() {

 }

 void getNewGyroData() {
 }


 int initVisionSocket()
 {
     struct sockaddr_in sa;

     // Create the vision socket
     if ((m_visionSocket = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
     {
	 fprintf(stderr, "FATAL: socket: %s\nCould not create vision socket.\n",
		 strerror(errno));
	 return -1;
     }

     // We will receive commands on the local port m_visionPort
     sa.sin_family = AF_INET;
     sa.sin_port = htons(m_visionPort);
     sa.sin_addr.s_addr = htonl(INADDR_ANY);

     // Bind the socket so we can receive commands
     if (bind(m_visionSocket, (struct sockaddr *)&sa, sizeof(sa)) == -1)
     {
	 fprintf(stderr, "FATAL: bind: %s\nCould not bind vision socket to local port %d.\n",
		 strerror(errno), m_visionPort);
	 return -1;
     }

     return 0;
 }

 int receiveVisionData() {

     int num_bytes;
     int sret;
     float fanforces[2];
     VisionPacket vp;	
     struct timeval tv;

     static char visible = 0;
     static uint8_t leftlevel = 0; 
     static uint8_t rightlevel = 0; 
     static uint8_t liftlevel = 0;
     static int dropped = 0;

     do
     {
	 // Read the next packets-worth of data off the network interface
	 if ((num_bytes = recv(m_visionSocket, &vp, sizeof(vp), MSG_WAITALL)) == -1)
	 {
	     fprintf(stderr, "recv: %s\nCould not receive vision packet\n");
	     return 0;
	 }

	 // Make sure that we got a whole packet
	 if (num_bytes < sizeof(vp))
	 {
	     fprintf(stderr, "Incomplete vision packet dropped. Size = %d\n", num_bytes);
	     return 0;
	 }


	 // If we've lost a packet,
	 if (((union ieee754_float *)(&vp.vehicle[m_vehicleId].x))->f == 0)
	 {
	     // Use the last theta as long as we haven't lost too many
	     if (dropped < MAX_FRAME_SKIP)
		 dropped++;
	     // If we have lost too many, we've become invisible
	     else
		 visible = 0;
	 }
	 // Otherwise, extract this robot's current angle from the vision packet
	 //else
	 //{


	 // Correct the float word order of the incoming data
	 wordSwap(&vp.vehicle[m_vehicleId].x);
	 wordSwap(&vp.vehicle[m_vehicleId].y);
	 wordSwap(&vp.vehicle[m_vehicleId].theta);
	 wordSwap(&vp.vehicle[m_vehicleId].xdot);
	 wordSwap(&vp.vehicle[m_vehicleId].ydot);
	 wordSwap(&vp.vehicle[m_vehicleId].thetadot);
	 
	 // Copy the new vision data to where the controller can use it
	 // If it is zero, then use the last state
	 if ((0 != vp.vehicle[m_vehicleId].x) && (0 != vp.vehicle[m_vehicleId].y) 
	     && (0 != vp.vehicle[m_vehicleId].theta) && (0 != vp.vehicle[m_vehicleId].xdot) 
	     && (0 != vp.vehicle[m_vehicleId].ydot) && (0 != vp.vehicle[m_vehicleId].thetadot)) {

	   m_state[X]		= vp.vehicle[m_vehicleId].x;
	   m_state[Y]		= vp.vehicle[m_vehicleId].y;
	   m_state[THETA]		= vp.vehicle[m_vehicleId].theta;
	   m_state[XDOT]		= vp.vehicle[m_vehicleId].xdot;
	   m_state[YDOT]		= vp.vehicle[m_vehicleId].ydot;
	   m_state[THETADOT]	= vp.vehicle[m_vehicleId].thetadot;

	 }

	 
	 writeStateData(m_sfp, m_state, m_stateFile);
	 printf("m_state (%8f, %8f, %8f, %8f, %8f, %8f)\n",
		m_state[X], m_state[Y], m_state[THETA], m_state[XDOT], m_state[YDOT], m_state[THETADOT]);
	 
	 //printf("Press return to continue\n");
	 //getchar();
	 
	 // So we've dropped zero consecutive packets and the vehicle is visible
	 dropped = 0;
	 visible = 1;

	 if (0 != m_state[X] || 0 != m_state[Y] ||
	     0 != m_state[THETA] || 0 != m_state[XDOT] || 0 != m_state[YDOT]  || 0 != m_state[THETADOT]) 
	   sret = 0;
	 else 
	   sret = 1;
	 
	 //}

	 // Now do a select with a zero-time timeout. This will effectively test
	 // to see if there any data to be read on the vision socket.

	 
/* 	 FD_ZERO(&m_readfds); */
/* 	 FD_SET(m_visionSocket, &m_readfds); */
/* 	 tv.tv_sec = 0; */
/* 	 tv.tv_usec = 0; */

/* 	 if ((sret = select(m_visionSocket + 1, &m_readfds, NULL, NULL, &tv)) == -1) */
/* 	 { */
/* 	     fprintf(stderr, "FATAL: select %s\nSelect failed.\n", strerror(errno)); */
/* 	     return -1; */
/* 	 } */

	 // Loop and get the next packet if it has already arrived
     } while (sret);
 }


 void wordSwap(void *dword)
 {
     uint32_t tmp;

     tmp = ((uint32_t *)dword)[0];
     ((uint32_t *)dword)[0] = ((uint32_t *)dword)[1];
     ((uint32_t *)dword)[1] = tmp;
 }


void getRecedingHorizonTraj() {

}

int isTrajComplete() {
  /* Calculate the current state and compare to 
     the end point of the receding horizon trajectory */
  //printf("isTrajComplete() FALSE");
  return 0; //false
}


float minimum( float a, float b ) {
  
  if (a < b) return a;
  else return b;

}

float maximum( float a, float b ) {

  if (a > b) return a;
  else return b;

}


int writeStateData(FILE *fp,const float data[11], const char* filename){

  int i,j;

  for (i=0; i<11; i++){
    fprintf(fp, "%f  ",(data[i]));
  }
  fprintf(fp,"\n");
  return 0;
}


int writeOutputData(FILE *fp,const float data[3], const char* filename) {
  
  int i,j;
  
  for (i=0; i<3; i++){
    fprintf(fp, "%f  ",(data[i]));
  }
  fprintf(fp, "\n");
  return 0;

}
