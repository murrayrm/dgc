// controller.c
// Author: Steve Waydo <waydo@cds.caltech.edu>
// Code snippets for MVWT II "Bat" controller
// vim:ts=8:sts=4:sw=2

#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

#define PI 3.14159

// static variables
float K[8];
float F_0;
float T_0;
float f_max;

// helpers
float max( float a, float b );
float min( float a, float b );

// need to load in controller gains, feedforward thrust, and max thrust
// K is a 2x4 matrix (will be a size 8 array of floats)
// and is stored in the file gains.txt
// this file must have 8 floats in it
// feedforward thrust (F_0) and max thrust (f_max) are stored in parameters.txt
// which must have 2 floats in it (feedforward thrust first)
// returns 1 on success, -1 on failure
int load_controller(const char *gainfile, const char *paramfile) {

  int i, err;

  FILE * Kfile;
  FILE * Pfile;

  // load controller gains from gains.txt
  if ((Kfile = fopen(gainfile, "r"))  == NULL)
  {
    fprintf(stderr, "fopen: %s\nCould not open controller gains definition file\n",
	strerror(errno));
    return -1;
  }

  for ( i = 0; i<8; i++ ) {
    err = fscanf( Kfile, "%f", &K[i] );
    if ( err < 0 ) {
      fprintf(stderr, "fscanf: %s\nCould not read gains from file\n",
	  strerror(errno));
      return -1;
    }
  }

  fclose(Kfile);
  
  // load feedforward thrust and max thrust from parameters.txt
  if ((Pfile = fopen(paramfile, "r")) == NULL)
  {
    fprintf(stderr, "fopen: %s\nCould not open controller parameters file\n",
	strerror(errno));
    return -1;
  }


  F_0 = 1;
  T_0 = 2;
  f_max = 0.7;

  //Fake it for now 
  /**
  err = fscanf( Pfile, "%f", &F_0 );

 
  if ( err < 0 ) {
    fprintf(stderr, "fscanf: %s\nCould not read parameters from file\n",
	strerror(errno));
    return -1;
  }  


  err = fscanf( Pfile, "%f", &T_0 );
  if ( err < 0 ) {
    fprintf(stderr, "fscanf: %s\nCould not read parameters from file\n",
	strerror(errno));
    return -1;
  }
  err = fscanf( Pfile, "%f", &f_max );

  if ( err < 0 ) {
    fprintf(stderr, "fscanf: %s\nCould not read parameters from file\n",
	strerror(errno));
    return -1;
  }

  fclose(Pfile);
  */
  return 1;

}

// takes the state and reference, performs a coordinate transformation,
// and generates control forces
// state[6] = { x, y, theta, xdot, ydot, thetadot }
// ref[2] = { xdot_ref, ydot_ref }
// forces[2] = { f_r, f_l }
// error vector is of form e = { theta, thetadot, xi_1, xi_2 }
// we're usng floats everywhere to save on computation time
void update_controller( float const state[6], const float ref[2], float forces[2] ) {

  float theta_ref, s_ref;
  float xi_1, xi_2, theta_err;
  float u_1, u_2;

  theta_ref = atan2(ref[1], ref[0]);

  theta_err = state[2] - theta_ref;
  // make sure to unwrap theta
  if (theta_err > PI) theta_err = theta_err - 2*PI;
  if (theta_err < -PI) theta_err = theta_err + 2*PI;

  // reference speed
  s_ref = sqrt( pow(ref[0],2) + pow(ref[1],2) );

  // generate error states
  xi_1 = (state[3]*ref[0] + state[4]*ref[1])/s_ref - s_ref;
  xi_2 = (-state[3]*ref[1] + state[4]*ref[0])/s_ref;

  // calculate controls
  // [u_1 u_2]' = -K*[theta thetadot xi_1 xi_2]'
  u_1 = -K[0]*theta_err - K[1]*state[5] - K[2]*xi_1 - K[3]*xi_2 + F_0;
  u_2 = -K[4]*theta_err - K[5]*state[5] - K[6]*xi_1 - K[7]*xi_2 + T_0;

  // calculate fan forces
  forces[0] = max(0, min(0.5*(u_1 + u_2),f_max));
  forces[1] = max(0, min(0.5*(u_1 - u_2),f_max));

}

// helper functions
float min( float a, float b ) {
  
  if (a < b) return a;
  else return b;

}

float max( float a, float b ) {

  if (a > b) return a;
  else return b;

}
