#include <errno.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "democmd.h"

#define ESCAPE			    27
#define DEFAULT_VEHICLE_PORT	    2020

int comm_sock = -1;
struct termios *old_term;

int init_console()
{
    // The first of these is static so that it can be accessed (via the
    // old_term pointer) by the cleanup function
    static struct termios curr_term;
    struct termios new_term;

    // Get two copies of the terminal settings (I don't know what's in the
    // termios structure, so I don't make the second copy myself)
    tcgetattr(STDIN_FILENO, &curr_term);
    tcgetattr(STDIN_FILENO, &new_term);

    // Save the old terminal settings
    old_term = &curr_term;

    // Disable buffered input and keyboard echo for the new settings
    new_term.c_lflag &= ~ICANON;
    new_term.c_lflag &= ~ECHO;

    // Try to envoke the new settings
    if (tcsetattr(STDIN_FILENO, TCSANOW, &new_term) == -1)
    {
	fprintf(stderr, "WARNING: tcsetattr: %s\nUnable to change terminal settings.\n", strerror(errno));
	return -1;
    }

    return 0;
}

int init_comm_sock(const char *host)
{
    struct sockaddr_in sa;
    struct hostent *he;

    if ((comm_sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
	fprintf(stderr, "FATAL: socket %s\nCould not create socket for host %s\n",
		strerror(errno), host);
	return -1;
    }

    if ((he = gethostbyname(host)) == NULL)
    {
	fprintf(stderr, "FATAL: gethostbyname %s\nCould not lookup host %s\n",
		strerror(errno), host);
	return -1;
    }

    sa.sin_family = AF_INET;
    sa.sin_port = htons(DEFAULT_VEHICLE_PORT);
    sa.sin_addr = *(struct in_addr *)he->h_addr_list[0];

    if (connect(comm_sock, (struct sockaddr *)&sa, sizeof(sa)) == -1)
    {
	fprintf(stderr, "FATAL: connect: %s\nCould not set connect to address for commands.\n",
		strerror(errno));
	return -1;
    }
}

void cleanup()
{
    if (old_term != NULL)
	if (tcsetattr(STDIN_FILENO, TCSANOW, old_term) == -1)
	    fprintf(stderr, "tcsetattr: %s\nCould not restore terminal settings.\n",
		    strerror(errno));

    if (comm_sock != -1)
	close(comm_sock);
}

void send_command(DemoCommand dc)
{
    int num_bytes;
    if ((num_bytes = send(comm_sock, &dc, sizeof(dc), 0)) == -1)
    {
	fprintf(stderr, "send: %s\nCould not send command to robot.\n",
		strerror(errno));
	return;
    }

    if (num_bytes < sizeof(dc))
    {
	fprintf(stderr, "Could not send whole command.\n");
	return;
    }
}

int main(int argc, char **argv)
{
    int c;
    char lift = 0;
    
    if (argc != 2)
    {
	printf("What's the vehicle IP?\n");
	return 1;
    }

    if (init_console())
	return 1;
    
    printf("Init console OK\n");

    if (init_comm_sock(argv[1]) == -1)
	return 1;

    printf("Init comm sock OK\n");

    do
    {
	c = fgetc(stdin);

	
	printf("Got command %c\n", c);

	switch (c)
	{
	    case 'f':
		send_command(DCMD_LEFTDOWN);
		break;
	    case 'r':
		send_command(DCMD_LEFTUP);
		break;
	    case 'j':
		send_command(DCMD_RIGHTDOWN);
		break;
	    case 'u':
		send_command(DCMD_RIGHTUP);
		break;
	    case '0':
		send_command(DCMD_IDLE);
		break;
	    case ' ':
		send_command(DCMD_LIFTTOGGLE);
		break;
	    default:
		break;
	}

    } while (c != ESCAPE);

    send_command(DCMD_SHUTDOWN);

    cleanup();
}
