#include <stdio.h>
#include <string.h>

#define LINEBUFFSIZE	    256

char linebuff[LINEBUFFSIZE];

int main(int argc, char **argv)
{
 
 
  FILE *serial; 
  printf("About to open serial port \n");  

    if ((serial = fopen("/dev/ttyS0", "r+")) == NULL)
    {
	printf("Could not open the serial port for reading and writing\n");
    }

    while (!feof(serial))
    {
	if (fread(linebuff, 3, 1, serial) < 1)
	    perror("fread: ");
	else
	{
	    linebuff[3] = '\0';
	    fprintf(serial, "serial echo: %s\n", linebuff);

	    printf("local echo: %s\n", linebuff);
	}
	fflush(serial);
    }

    printf("Closed");
    return 0;
}
