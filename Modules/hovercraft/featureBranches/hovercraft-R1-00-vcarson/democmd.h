#ifndef __DEMOCMD_H__
#define __DEMOCMD_H__

#include <inttypes.h>

typedef uint8_t DemoCommand;

#define DCMD_LEFTUP		0
#define DCMD_LEFTDOWN		1
#define DCMD_RIGHTUP		2
#define DCMD_RIGHTDOWN		3
#define DCMD_IDLE		4
#define DCMD_SHUTDOWN		5
#define DCMD_LIFTTOGGLE		6

#endif
