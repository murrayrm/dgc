/// \file namsmotortest.cc
/// \author Nam Nguyen
/// \date 17 Feb 2008
/// \brief left and right fan motor test program

///controls (type in and press enter):
///"a": left fan only full speed
///"d": right fan only full speed
///"w": both fans full speed
///"s": both fans stopped
///input nothing to exit


#include <stdio.h>
#include <string.h>
#include "bathw.h"

using namespace std;

int main() {
  //initialize
  if(bathw_init(DEFAULT_COMM_DEVICE, DEFAULT_COMM_SPEED) == -1) {
    printf(stderr, "Could not initialize Bat hardware: %s\n", bathw_strerror());
    return 1;
  }
  bathw_setfanoutputs(0, 0, 0)
    
    
    bathw_setfanoutputs(0, FAN_MAX, 0);
  bathw_setfanoutputs(FAN_MAX, FAN_MAX, 0);
  bathw_setfanoutputs(0, 0, 0);
  
}
//cleanup
bathw_cleanup();
printf( "byebye!");
return 1;
}
