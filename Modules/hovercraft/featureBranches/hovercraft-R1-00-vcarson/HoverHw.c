#include "MvwtFollower.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "bathw.h"
#include "traj.h"

#define USHRT_MAX  100




static void printUsage() {

  printf("HoverHw [-h] [-c COMMAND PORT] [-v VISION PORT ] [-n VEHICLE NUMBER] [-g GAINS FILE] [-t TRAJ FILE]"); 
  printf("-c Select the port on which commands are to be received"); 
  printf("-v Select the port on which vision data is to be received"); 
  printf("-n Select the vehicle number, see the vision computer"); 
  printf("-g Select the gains file for the controller "); 
  printf("-t Select the trajectory file to follow"); 
  printf("-s Select the file to print state data to"); 
  printf("-e Select the file to print traj error data to"); 
  printf("-f Select the file to print forces to"); 
  printf("-h Display usage message ");
  exit(0);
}


static void printOptions(){

  printf("HoverHw ");
  // printf(sim?"SIMULATION":"HARDWARE \n");
  printf("COMMAND PORT: %d \n",m_commandSocket);  
  printf("VISION PORT: %d \n",m_visionPort); 
  printf("VEHICLE NUMBER: %d \n",m_vehicleId ); 
  printf("end of print options \n" ); 
  //FIX how to print char*
  //printf("GAINS FILE: %c \n",*gainsFile ); 
  //printf("TRAJECTORY  FILE : %c \n",*trajFile); 
}


int main(int argc, char **argv) {

  // Set the default ports to be overwritten if command-line options are used
  int c, numRows;
  char* sarg;
  int iarg, targ;
  char *endptr, *trajFile, *gainsFile, *stateFile, *errFile, *forceFile ;
  int sim = 0; //false

  m_visionPort = DEFAULT_VISION_PORT;
  m_commandSocket = DEFAULT_COMMAND_PORT;
  m_vehicleId = 13; //Based on current vision hat in operation


  // Get the command-line arguments
  while ((c = getopt(argc, argv, "s:e:f:c:v:n:g:t:h")) != -1) {

    switch (c) {
     case 's':
       m_stateFile = optarg;
       if ((m_sfp = fopen(m_stateFile, "w")) == NULL)
	 return -1;
       break;
     case 'e':
       m_errFile = optarg;
       if ((m_efp = fopen(m_errFile, "w")) == NULL)
	 return -1;
       break;
     case 'f':
       m_forceFile = optarg;
       if ((m_ffp = fopen(m_forceFile, "w")) == NULL)
	 return -1;
       break;
     case 'c':
       iarg = strtol(optarg, &endptr, 0);
      
       if (*endptr || targ < 0 || targ > USHRT_MAX)
 	printf("Invalid port %s ignored\n", c);
       else
	 m_commandSocket = targ;
       break;
      
     case 'v':
       iarg = strtol(optarg, &endptr, 0);
      
       if (*endptr || targ < 0 || targ > USHRT_MAX)
 	printf("Invalid port %s ignored\n", c);
       else
 	m_visionPort = targ;
       break;

     case 'n':
       targ = strtol(optarg, &endptr, 0);
      
       if (*endptr || iarg < 0 || targ > USHRT_MAX)
 	printf("Invalid vehicle number %s ignored\n", c);
       else
 	m_vehicleId = targ;
       break;

     case 'g':
       gainsFile = optarg;
       break;

     case 't':
       trajFile = optarg;
       break;
      
     case 'h':
       printUsage();
       return;
       break;
      
     case '?':
       printf("Unrecognized option '%c' ignored.\n", c);
       break;
      
     default:
       break;
     }
   }

  //printf("Printing options\n");
  printOptions();

  printf("Hello Hovercraft!");
    
  //FIX: For now include the gains here

  //Decrement vehicle id for vision system 
  m_vehicleId --;

  printf("Traj file %s, \n", trajFile);
  m_gains.y1 = 22;
  m_gains.t1 = 5;
  m_gains.ydot1 = 16 ;
  m_gains.tdot1 = 0.8;
  
  m_gains.y2 = -22;
  m_gains.t2 = -5;
  m_gains.ydot2 = -16 ;
  m_gains.tdot2 = -0.8;
  
  if (NULL == trajFile)
    return 1; 

  m_traj = traj_load(trajFile);

  m_ctrlType = LAT;

  printf("TRAJ numRows= %d \n",numRows);

  initialize();
  
  control(); 

  printf("Goodbye Hovercraft!");
    
  return 0; 
}
