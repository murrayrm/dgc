#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <linux/serial.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "democmd.h"

#define DEFAULT_BAUD			B115200
#define DEFAULT_COMMAND_PORT		2020
#define LIFT_VALUE			180

// We'll use this just for now. Maybe we'll come up with a more robust command type later
typedef struct 
{
    uint8_t cmd;
    uint8_t n_args __attribute__ ((packed));
    uint8_t arg __attribute__ ((packed));
} FanCommand;

int serial_fd = -1;
int net_fd = -1;
struct termios *old_ttyserial = NULL;
uint8_t lift_val;

int init_serial(speed_t baud)
{
    static struct termios curr_ttyserial;
    struct termios new_ttyserial;

    // Open the serial port
    if ((serial_fd = open("/dev/ttyS0", O_RDWR)) == -1)
    {
	fprintf(stderr, "FATAL: open: %s\nCould not open serial port.\n",
		strerror(errno));
	return -1;
    }

    // Fetch current settings
    tcgetattr(serial_fd, &curr_ttyserial);
    tcgetattr(serial_fd, &new_ttyserial);

    // Save old settings
    old_ttyserial = &curr_ttyserial;

    printf("serial settings: IXON %d\nIXOFF %d\nHUPCL %d\nCLOCAL %d\nCRTSCTS %d\n",
	    old_ttyserial->c_iflag & IXON,
	    old_ttyserial->c_iflag & IXOFF,
	    old_ttyserial->c_cflag & HUPCL,
	    old_ttyserial->c_cflag & CLOCAL,
	    old_ttyserial->c_cflag & CRTSCTS );

    // Make new settings
    new_ttyserial.c_iflag &= ~IXON;	    // disable XON/XOFF flow control
    new_ttyserial.c_iflag &= ~IXOFF;
    new_ttyserial.c_cflag &= ~PARENB;	    // disable parity bit
    new_ttyserial.c_cflag &= ~CSTOPB;	    // use only one stop bit
    new_ttyserial.c_cflag &= ~HUPCL;	    // don't hang up the modem lines on close
    new_ttyserial.c_cflag |=  CLOCAL;	    // ignore all modem control lines
    new_ttyserial.c_cflag &= ~CRTSCTS;	    // disable modem flow control
    new_ttyserial.c_lflag &= ~ICANON;	    // disable line buffering
    new_ttyserial.c_lflag &= ~ECHO;	    // disable local echo

    new_ttyserial.c_cflag  = (new_ttyserial.c_cflag & ~CSIZE) | CS8;	// set data size to 8 bits
    if (    cfsetispeed(&new_ttyserial, baud) == -1 || 
	    cfsetospeed(&new_ttyserial, baud) == -1)
    {
	fprintf(stderr, "FATAL: cfsetspeed: %s\nCould not set serial baud.\n",
		strerror(errno));
	return -1;
    }

    // Try to envoke the new settings
    if (tcsetattr(serial_fd, TCSANOW, &new_ttyserial) == -1)
    {
	fprintf(stderr, "FATAL: tcsetattr: %s\nCould not change serial terminal settings.\n",
		strerror(errno));
	return -1;
    }

    return 0;
}

int init_network(short port)
{
    struct sockaddr_in sa;

    // Create command socket
    if ((net_fd = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
	fprintf(stderr, "FATAL: socket: %s\nCould not create command socket.\n",
		strerror(errno));
	return -1;
    }

    // We will receive commands on the local port "port"
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    sa.sin_addr.s_addr = htonl(INADDR_ANY);

    // Bind the socket so we can receive commands
    if (bind(net_fd, (struct sockaddr *)&sa, sizeof(sa)) == -1)
    {
	fprintf(stderr, "FATAL: bind: %s\nCould not bind command socket to local port %d.\n",
		strerror(errno), port);
	return -1;
    }
    
    return 0;
}

void cleanup()
{
    if (old_ttyserial != NULL)
	if (tcsetattr(serial_fd, TCSANOW, old_ttyserial) == -1)
	    fprintf(stderr, "tcsetattr: %s\nCould not restore serial port settings.\n", 
		    strerror(errno));

    if (net_fd != -1)
	close(net_fd);

    if (serial_fd != -1)
	close(serial_fd);
}

void send_command(uint8_t cmd, uint8_t arg)
{

  static FanCommand fc = { 0, 1, 0 };	    // read as: uninit, 1, uninit
  
  fc.cmd = cmd;
  fc.arg = arg;

  printf("About to send the fan command\n");  
  if (write(serial_fd, &fc, sizeof(fc)) < sizeof(fc)) {
    fprintf(stderr, "write: %s\nCould not send command to motors.\n",
	    strerror(errno));
  }
  printf("I sent the bloody command!\n", cmd, arg);
}


int handle_network()
{
    static uint8_t left = 0;
    static uint8_t right = 0;
    static uint8_t lift = 0;
    int num_bytes;
    DemoCommand dc;

    printf("Reading network stuff\n");
    // Read a single byte command over the network
    if ((num_bytes = recv(net_fd, &dc, sizeof(dc), MSG_WAITALL)) == -1)
    {
	fprintf(stderr, "recv: %s\nCould not receive command packet.\n",
		strerror(errno));
	return 0;
    }

    // Make sure we got that byte
    if (num_bytes < sizeof(dc))
    {
	fprintf(stderr, "Incomplete command packet dropped. Size = %d\n", num_bytes);
	return 0;
    }

    switch (dc)
    {
	case DCMD_LEFTUP:
	    // crank up the left
	    if (left < 255)
	    {
		left++;
		send_command('l', left);
	    }
	    break;

	case DCMD_LEFTDOWN:
	    // pull down the left
	    if (left > 0)
	    {
		left--;
		send_command('l', left);
	    }
	    break;

	case DCMD_RIGHTUP:
	    // crank up the right
	    if (right < 255)
	    {
		right++;
		send_command('r', right);
	    }
	    break;

	case DCMD_RIGHTDOWN:
	    // pull down the right
	    if (right > 0)
	    {
		right--;
		send_command('r', right);
	    }
	    break;
	
	case DCMD_IDLE:
	    // kill both motors
	    right = left = 0;
	    send_command('r', right);
	    send_command('l', left);
	    break;

	case DCMD_LIFTTOGGLE:
	    if (lift < lift_val)
		lift = lift_val;
	    else
		lift = 0;
	    send_command('u', lift);
	    break;

	case DCMD_SHUTDOWN:
	    return -1;

	default:
	    fprintf(stderr, "Unrecognized command %d\n", dc);
	    break;
    }

    printf("left: % 3d right: % 3d lift: % 3d\n", left, right, lift);
    fflush(stdout);
    return 0;
}

// This is code to throw away the sensor data that we're getting on the serial
// port. I shouldn't really need to do this, though.
int discard_sensors()
{
    char stat;
    int num_bytes;
    uint8_t header;
    uint8_t size = 0;
    uint8_t trash[256];
    struct serial_struct ss;
    
    printf("Discarding sensor data\n");
    if ((num_bytes = read(serial_fd, &header, sizeof(header))) == -1)
    {
	fprintf(stderr, "FATAL: read: %s\nCould not read sensor data.\n",
		strerror(errno));

	// Fetch tty info
	ioctl(serial_fd, TIOCGSERIAL, &ss);
	printf("Serial info: type %x  - line %x - flags %x\n",
		ss.type, ss.line, ss.flags);

	return 0;
    }
    else if (num_bytes < sizeof(header))
    {
	fprintf(stderr, "Only %d bytes read for header\n", num_bytes);
	return 0;
    }
    printf("Read sensor data\n");
    
    switch (header)
    {
	case 'G':
	    size = 2;
	    break;

	case 'H':
	    size = 2;
	    break;

	case 'A':
	    size = 3;
	    break;

	default:
	    printf("got header: '%c' (%d)\n", header, header);
    }

    if (size)
    {
	if ((num_bytes = read(serial_fd, trash, (size_t)size)) == -1)
	{
	    fprintf(stderr, "FATAL: read: %s\nCould not read sensor data.\n",
		    strerror(errno));
	    return 0;
	}
	else if (num_bytes < size)
	{
	    fprintf(stderr, "Only %d bytes read for body (should be %d)\n", num_bytes, size);
	    return 0;
	}
    }
    
    switch (header)
    {
	case 'G':
	    printf("Got gyro data: %d\n", trash[0] + 256 * trash[1]);
	    break;

	case 'H':
	    printf("Got heading data: %d %d\n", trash[0], trash[1]);
	    break;

	case 'A':
	    printf("Got accel data: %d/%d %d/%d\n", trash[0], trash[2], trash[1], trash[2]);
	    break;

	default:
	    break;
    }

    printf("Done discarding sensor data\n");
    return 0;
}

int main(int argc, char **argv)
{
    int n;
    int width;
    int fdflags;
    fd_set readfds;
    fd_set exfds;

    if (argc > 1)
    {
	n = atoi(argv[1]);
	if (n < 0 || n > 255)
	    lift_val = 255;
	else
	    lift_val = (uint8_t)n;
    }
    else
	lift_val = 255;

    if (init_serial(DEFAULT_BAUD) == -1)
    {
	cleanup();
	return 1;
    }

    if ((fdflags = fcntl(serial_fd, F_GETFL)) == -1)
    {
	fprintf(stderr, "Could not get file flags\n");
	return 1;
    }

    if (init_network(DEFAULT_COMMAND_PORT) == -1)
    {
	cleanup();
	return 1;
    }

    send_command('l', 0);
    send_command('r', 0);
    send_command('u', 0);

    width = (net_fd > serial_fd ? net_fd : serial_fd) + 1;
    while (1)
    {
	FD_ZERO(&readfds);
	FD_SET(net_fd, &readfds);
	FD_SET(serial_fd, &readfds);

	FD_ZERO(&exfds);
	FD_SET(net_fd, &exfds);
	FD_SET(serial_fd, &exfds);

	printf("Selecting\n");
	if (select(width, &readfds, NULL, &exfds, NULL) == -1)
	{
	    fprintf(stderr, "FATAL: select: %s\nSelect failed.\n",
		    strerror(errno));
	    return 1;
	}
	printf("Done selecting\n");

	if (FD_ISSET(net_fd, &exfds))
	    fprintf(stderr, "Exception on net_fd\n");

	if (FD_ISSET(serial_fd, &exfds))
	    fprintf(stderr, "Exception on serial_fd\n");

	if (FD_ISSET(net_fd, &readfds) &&
		handle_network() == -1)
	    break;

	if (FD_ISSET(serial_fd, &readfds) &&
		discard_sensors() == -1)
	    break;
    }
    printf("\n");

    send_command('l', 0);
    send_command('r', 0);
    send_command('u', 0);

    cleanup();
    return 0;
}
