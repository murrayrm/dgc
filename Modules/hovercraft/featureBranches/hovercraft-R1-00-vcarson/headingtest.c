//This is a small program used to test heading sensor
//Zhipu Jin, Feb. 2004

#include <stdio.h>
//int Xmin, Xmax, Ymin, Ymax;
#include "bathw.h"

int main()
{
	float heading_value;
	long int counter1;	
	//FILE *hid;	
	//Xmin=1000; Ymin=1000; Xmax=0; Ymax=0;
	
	if(bathw_init(DEFAULT_COMM_DEVICE, DEFAULT_COMM_SPEED) == -1)
	{
		fprintf(stderr, "Could not initialize Bat hardware: %s\n", bathw_strerror());
		return 1;
	}

	printf("Enabling heading, please wait....\n");
	//if(bathw_enable_heading("heading_calib.dat", &heading_value, &Xmin, &Xmax, &Ymin, &Ymax) == -1)
	if(bathw_enable_heading("heading_calib.dat", &heading_value) == -1)
	{
		fprintf(stderr, "Could not enable heading sensor: %s\n", bathw_strerror());
		return 1;
	}	
	printf("Done!\n");

	bathw_heading_reset();
	printf("Reset the heading sensor! \n");

	counter1=0;

	//while(counter1 < 4000)
	while(1)
	{
		bathw_update_sensors();
		printf("heading = %f \n", heading_value*180/3.14159);
		counter1++;
	}

	bathw_disable_heading();
	bathw_cleanup();

	//hid = fopen("heading_calib.dat", "w");
	//printf("Xmin=%d, Xmax=%d, Ymin=%d, Ymax=%d\n", Xmin, Xmax, Ymin, Ymax);
	//fprintf(hid, "%f, %f\n", (float)(Xmin+Xmax)/2.0, (float)(Ymin+Ymax)/2.0);
	//fclose(hid);

	return 0;
}

