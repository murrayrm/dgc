#include <pthread.h>
#include <stdio.h>

void *phello(void *id)
{
	printf("Hello, this is thread %d\n", id);
	pthread_exit(NULL);
}

int main()
{
	pthread_t mythread;
	pthread_attr_t attr;
	int rc, t;
	
	t=1;
	pthread_attr_init(&attr);
	pthread_create(&mythread, NULL, phello, (void *)t);

	for(rc=0;rc<10000;rc++)
		{t=1;}
		
	pthread_exit(NULL);
}
