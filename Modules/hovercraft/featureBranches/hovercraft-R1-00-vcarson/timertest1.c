#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <pthread.h>

#include "bathw.h"

int sig_count = 0;
int break_count = 0;
int activeflag;

//sensor data
float gyro_value;
float accel_x, accel_y;
float heading_value;

//fan force
float fl, fr, F, T;
int fan_left, fan_right, fan_lift;
int fan_offset;

char string[20];
int length;

#define FANOFFSET	55
#define Foffset		210

float heading, control_var;
FILE * logfile;

void *Getcommand(void *threadid)
{
	//printf("Thread %d begins!\n", threadid);

	while(1){	
		length=fscanf(stdin, "%s", &string);
	        if (length > 0){
                        if (string[0] == 'b'){
                                printf("Begin to run! sig_count = %d\n", sig_count);
                                activeflag=1;
                        }
                        if (string[0] == 'l'){
                                printf("Make a left turn (-90 degree)! sig_count = %d\n", sig_count);
				heading=heading+3.1415926/2.0;
				if(heading > 3.1415926) heading=heading - 2* 3.1415926;
				break_count=25;
				}
                        if (string[0] == 'r'){
                                printf("Make a right turn (+90 degree)! sig_count = %d\n", sig_count);
				heading=heading-3.1415926/2.0;
				if(heading < -3.1415926) heading=heading +2*3.1415926;
				break_count=25;
				}
			if (string[0] == 't'){
                                printf("Make a back turn (+180 degree)! sig_count = %d\n", sig_count);
                                heading=heading-3.1415926;
                                if(heading < -3.1415926) heading=heading +2*3.1415926;
                                break_count=25;
                                }

                        if (string[0] == 's'){
                                printf("Stop the vehicle! sig_count = %d\n", sig_count);
                                activeflag=0;
                        }
                        if (string[0] == 'q'){
                                printf("Quit!\n");
                                activeflag=-1;
				break;
                        }                                                                                                     
                printf("Input a command (b, r, l, t, s, q):");
                }
	}
	pthread_exit(NULL);
}	

void timer_handler(int sig)
{
        sig_count++;
	
	//add the fun force sending
/*	//This is used to test the command;
	if (string[0] == 'b')
        	bathw_setfanoutputs(FANOFFSET, FANOFFSET, FANOFFSET+100);
        
	if (string[0] == 'l')
                bathw_setfanoutputs(FANOFFSET+100, FANOFFSET, FANOFFSET+100);        
	
	if (string[0] == 'r')
                bathw_setfanoutputs(FANOFFSET, FANOFFSET+100, FANOFFSET+100);
        
	if (string[0] == 's')
                bathw_setfanoutputs(FANOFFSET, FANOFFSET, FANOFFSET);                                                                                                                         
	if (string[0] == 'q')
                bathw_setfanoutputs(0,0,0);
*/
	if(break_count>0){
		bathw_setfanoutputs(0,0,0);
		break_count--;
		return;
	}

        //This is a simple heading based feedback controller
	if(activeflag == 1){
		
		//control_var=control_var*0.1111+0.5556*(heading_value - heading);
		//T=17.4444*control_var-11.2778*(heading_value - heading);
		
		T=-0.3605*(heading_value - heading)-0.1151*(gyro_value*3.1415926/180.0);
		//forcemapping
		//T=forcemap(T);
		T=T/0.004;
		F=Foffset;

		fan_left=(int)((F-T)/2) + FANOFFSET;
		fan_right=(int)((F+T)/2) + FANOFFSET;
		fan_lift = 180 + FANOFFSET;
		bathw_setfanoutputs(fan_left, fan_right, fan_lift);

		fprintf(logfile, "%d, %f, %f, %f, %f\n", sig_count, heading_value, gyro_value, accel_x, accel_y);

	}
	else if(activeflag == -1){
		bathw_setfanoutputs(0,0,0);
	}
	else if(activeflag == 0)
		bathw_setfanoutputs(0,0,0);
                                                                                                              
}

int main(int argc, char **argv)
{
    int c, i, rc, id;
    int priority;
    long delay = 20000; //0.02 second
    char *endptr;
    struct sigaction sa;
    struct itimerval itv;
    struct timeval starttime;
    struct timeval endtime;
    double dtime;
    pthread_t threads;
    FILE * traj;

	//initial the serial port
	if(bathw_init(DEFAULT_COMM_DEVICE, DEFAULT_COMM_SPEED) == -1)
        {
                fprintf(stderr, "Could not initialize Bat hardware: %s\n", bathw_strerror());
                return 1;
        }
	
	//initial gyro
	printf("Enabling gyro, please wait... ");
        if(bathw_enable_gyro("gyro_calib.data", &gyro_value) == -1)
        {
                fprintf(stderr, "Could not enable gyro sensor: %s\n", bathw_strerror());
                return 1;
        }                                                                         
        printf("Done!\n");

	printf("Enabling accelerometer, please wait... ");
	if(bathw_enable_accel("accel_calib.data", &accel_x, &accel_y) == -1)
        {
                fprintf(stderr, "Could not enable accelerometer: %s\n", bathw_strerror());
                return 1;
        }
	printf("Done!\n");

	printf("Enabling heading, please wait... ");
        if(bathw_enable_heading("heading_calib.dat", &heading_value) == -1)
        {
                fprintf(stderr, "Could not enable heading sensor: %s\n", bathw_strerror());
                return 1;
        }
        printf("Done!\n");

        bathw_heading_reset();
        printf("Reset the heading sensor! \n");
                                                                                                    
	//first, stop the fans
	activeflag=0;
	bathw_setfanoutputs(0,0,0);

	id=1;
	rc = pthread_create(&threads, NULL, Getcommand, (void*)id);
	if(rc){
		fprintf(stderr, "ERROR: return code from pthread_creat() is %d.\n", rc);
		exit(-1);
	}
	printf("Input a command (b, r, l, t, s, q):");

	//setup the timer
	priority = -10; //between -20 to 20, lower priorities cause more favorable scheduling.

	if (setpriority(PRIO_PROCESS, 0, priority) == -1)
		fprintf(stderr, "Could not set process priority. You're probably not root.\n");

	sa.sa_handler = &timer_handler;
    	sa.sa_flags = 0;

	if (sigaction(SIGALRM, &sa, NULL) == -1)
    	{
		fprintf(stderr, "sigaction: %s\nCould not setup timer handler.\n",
			strerror(errno));
		return 1;
    	}

	itv.it_interval.tv_sec = 0;
    	itv.it_interval.tv_usec = delay;
    	itv.it_value.tv_sec = 0;
    	itv.it_value.tv_usec = delay;

    	if (setitimer(ITIMER_REAL, &itv, NULL) == -1)
    	{
		fprintf(stderr, "setitimer: %s\nCould not setup timer.\n",
			strerror(errno));
		return 1;
    	}

    	sa.sa_handler = SIG_IGN;
    	itv.it_interval.tv_sec =	0;
    	itv.it_interval.tv_usec =	0;
    	itv.it_value.tv_sec =	0;
    	itv.it_value.tv_usec =	0;

	gettimeofday(&starttime, NULL);

	//get initial heading;
	traj=fopen("inital.dat", "r");
	fscanf(traj, "%f", &heading);
	//printf("heading = %f\n", heading);
	fclose(traj);

	logfile=fopen("logfile.dat", "w");

	control_var=0.0;

	// Wait for keypress
    	while(activeflag != -1)
    	{
		bathw_update_sensors();
                //printf("G=%f, H=%f, AX=%f, AY=%f \n", gyro_value, heading_value*180/3.1416, accel_x, accel_y);
		
	}
	bathw_setfanoutputs(0,0,0);

	gettimeofday(&endtime, NULL);

    	if (sigaction(SIGALRM, &sa, NULL) == -1)
    	{
		fprintf(stderr, "sigaction: %s\nCould not restore timer handler.\n",
			strerror(errno));
		return 1;
    	}

    	if (setitimer(ITIMER_REAL, &itv, NULL) == -1)
    	{
		fprintf(stderr, "setitimer: %s\nCould not stop timer.\n",
			strerror(errno));
		return 1;
    	}    

	bathw_disable_gyro();
	bathw_disable_accel();
	bathw_disable_heading();
        bathw_cleanup();

	fclose(logfile);

	dtime = ((double)(endtime.tv_sec - starttime.tv_sec) +
             1e-6*(double)(endtime.tv_usec - starttime.tv_usec));
    	printf("Signals: %d Time: %f Rate: %f\n", sig_count, dtime, (double)sig_count / dtime);
	pthread_exit(NULL);

    	return 0;
}
