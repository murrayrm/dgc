#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/resource.h>
#include <sys/time.h>

int sig_count = 0;

void timer_handler(int sig)
{
    sig_count++;
}

int main(int argc, char **argv)
{
    int c;
    int sret;
    int priority;
    int delay = 20000; //0.02 second
    char *endptr;
    struct sigaction sa;
    struct itimerval itv;
    struct timeval starttime;
    struct timeval endtime;
    fd_set readfds;
    double dtime;

    while ((c = getopt(argc, argv, "p:u:")) != -1)
    {
	switch (c)
	{
	    case 'p':
		priority = strtol(optarg, &endptr, 0);
		if (*endptr || priority < -20 || priority > 20)
		    fprintf(stderr, "Invalid priority specified.\n");
		else if (setpriority(PRIO_PROCESS, 0, priority) == -1)
		    fprintf(stderr, "Could not set process priority. You're probably not root.\n");
		break;

	    case 'u':
		delay = strtol(optarg, &endptr, 0);
		if (*endptr || delay <= 0 || delay >= 1000000)
		{
		    fprintf(stderr, "Invalid delay specified. Using 16 usecs.\n");
		    delay = 16;
		}
		break;

	    default:
		fprintf(stderr, "Ignoring unrecogized argument '%c'\n", optopt);
		break;
	}
    }

    sa.sa_handler = &timer_handler;
    sa.sa_flags = 0;

    if (sigaction(SIGALRM, &sa, NULL) == -1)
    {
	fprintf(stderr, "sigaction: %s\nCould not setup timer handler.\n",
		strerror(errno));
	return 1;
    }

    itv.it_interval.tv_sec =	0;
    itv.it_interval.tv_usec =	delay;
    itv.it_value.tv_sec =	0;
    itv.it_value.tv_usec =	delay;

    gettimeofday(&starttime, NULL);

    if (setitimer(ITIMER_REAL, &itv, NULL) == -1)
    {
	fprintf(stderr, "setitimer: %s\nCould not setup timer.\n",
		strerror(errno));
	return 1;
    }

    sa.sa_handler = SIG_IGN;
    itv.it_interval.tv_sec =	0;
    itv.it_interval.tv_usec =	0;
    itv.it_value.tv_sec =	0;
    itv.it_value.tv_usec =	0;

    // Wait for keypress
    FD_ZERO(&readfds);
    FD_SET(STDIN_FILENO, &readfds);

    do
    {
	if ((sret = select(STDIN_FILENO + 1, &readfds, NULL, NULL, NULL)) == -1 && errno != EINTR)
	    fprintf(stderr, "select: %s\nSelect failed.\n", strerror(errno));
    } while (sret == -1 && errno == EINTR);
     
    if (sigaction(SIGALRM, &sa, NULL) == -1)
    {
	fprintf(stderr, "sigaction: %s\nCould not restore timer handler.\n",
		strerror(errno));
	return 1;
    }

    gettimeofday(&endtime, NULL);

    if (setitimer(ITIMER_REAL, &itv, NULL) == -1)
    {
	fprintf(stderr, "setitimer: %s\nCould not stop timer.\n",
		strerror(errno));
	return 1;
    }
    
    dtime = ((double)(endtime.tv_sec - starttime.tv_sec) +
	     1e-6*(double)(endtime.tv_usec - starttime.tv_usec));
    printf("Signals: %d Time: %f Rate: %f\n", sig_count, dtime, (double)sig_count / dtime);
    
    return 0;
}
