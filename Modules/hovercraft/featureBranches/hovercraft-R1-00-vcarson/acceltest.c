//This is a small program used to test accelerometer
//Zhipu Jin, Feb. 2004

#include <stdio.h>
#include "bathw.h"

int main()
{
	float accel_x, accel_y, period;
	long int counter1;	
	
	if(bathw_init(DEFAULT_COMM_DEVICE, DEFAULT_COMM_SPEED) == -1)
	{
		fprintf(stderr, "Could not initialize Bat hardware: %s\n", bathw_strerror());
		return 1;
	}

	if(bathw_enable_accel("accel_calib.data", &accel_x, &accel_y) == -1)
	{
		fprintf(stderr, "Could not enable accelerometer: %s\n", bathw_strerror());
		return 1;
	}
	
	printf("Complete initialization.....\n");

	counter1=0;

	while(1)
	{
		//printf("Start updating...\n");
		bathw_update_sensors();
		printf("accel_x= %f , accel_y= %f \n", accel_x, accel_y);
		counter1++;
	}

	bathw_disable_accel();
	bathw_cleanup();

	return 0;
}

