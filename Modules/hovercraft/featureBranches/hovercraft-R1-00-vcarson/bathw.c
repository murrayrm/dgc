/* File:	bathw.c
 * Description:	The Bat hardware library provides a low-level interface to the
 *		MVWT II Hovercraft (refered hereto as the Bat). Specifically,
 *		this file should provide interfaces to both the fan outputs and
 *		the sensor inputs. Communication is performed, via serial
 *		connection, with an Atmel microcontroller programmed by Hans
 *		Scholze which generates PWM signals for the fans and reads
 *		sensor data from digital and analog I/O ports. This library is
 *		a wrapper for that serial communcation.
 *
 *		For thorough documentation of the functions included in this
 *		library and their usage, see the accompanying header file,
 *		bathw.h.
 * OS:		Embedix Linux (Zaurus PDA SA-1110 Architecture)
 * Author:	Rob Christy
 * CVS:		$Id: bathw.c,v 1.5 2003/08/30 03:31:48 waydo Exp $
 *		vim:ts=8:sts=4:sw=4
 */

/* System Includes */
#include <stdio.h>
#include <fcntl.h>
#include <inttypes.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

/* Local Includes */
#include "bathw.h"

/* Prototypes of local functions */
int bathw_update_gyro(uint8_t *buffer);
int bathw_update_accel(uint8_t *buffer);
int bathw_update_heading(uint8_t *buffer);

/* Constants */

const char *baterrstrs[BATECOUNT] = {
    "No error",							    // BATENONE
    "Multiple initializations attempted without cleanup",	    // BATEREINIT
    "Could not open serial port",				    // BATEOPEN
    "Could not set serial baud rate",				    // BATEBAUD
    "Could not activate terminal settings",			    // BATESTTY
    "A serial write operation failed",				    // BATEWRERR
    "An invalid argument was passed to a function",		    // BATEINVAL
    "A serial read operation failed",				    // BATERDERR
    "The bat hardware is not initialized",			    // BATEUNINIT
								    // BATECOUNT
};


/* Variables */
    /* Initialization and error handling */
int serial_fd = -1;
unsigned int curr_error = BATENONE;
struct termios *old_ttyserial = NULL;

    /* Cached fan output levels */
int left_val = FAN_UNDEF;
int right_val = FAN_UNDEF;
int lift_val = FAN_UNDEF;

    /* Common sensor info */
struct sensor_info sensors[SENSOR_COUNT] = 
{
    {	'G',    0,  &bathw_update_gyro,	    2	    },
    {	'A',	0,  &bathw_update_accel,    6	    },
    {	'H',	0,  &bathw_update_heading,  4	    },
};

    /* Specific sensor data */
    /* Gyro data */
float *gyro_buffer;
float gyro_offset;
float gyro_scale;
//used for set the initial offset
float temp1, temp2, temp3;
int init_counter_g, init_counter_a;
#define INI_TIMES	400

    /* Accelerometer data */
float *accel_xbuffer;
float *accel_ybuffer;
float accel_xoffset;
float accel_yoffset;
// FIXME : Accelerometer calibration variables go here

    /* Heading data */
float *heading_buffer;
//int *X_min, *X_max, *Y_min, *Y_max;
FILE *heading_fid;
float  hx_offset, hy_offset;
// FIXME : Heading calibration variables go here

/* Function definitions */

int bathw_init(const char *comm_device, speed_t speed)
{
    struct termios new_ttyserial;
    static struct termios curr_ttyserial;

    if (serial_fd != -1 || old_ttyserial != NULL)
    {
	curr_error = BATEREINIT;
	return -1;
    }

    // Attempt to open the serial device
    if ((serial_fd = open(comm_device, O_RDWR)) == -1)
    {
	curr_error = BATEOPEN;
	return -1;
    }

    // Get current serial settings
    tcgetattr(serial_fd, &curr_ttyserial);
    memcpy(&new_ttyserial, &curr_ttyserial, sizeof(curr_ttyserial));

    // Save old settings
    old_ttyserial = &curr_ttyserial;

    // Make new settings
    new_ttyserial.c_iflag &= ~IXON;	    // turn off XON/XOFF flow control
    new_ttyserial.c_iflag &= ~IXOFF;
    new_ttyserial.c_lflag &= ~ICANON;	    // turn off line buffering
    new_ttyserial.c_lflag &= ~ECHO;	    // turn off local echo
    new_ttyserial.c_cflag &= ~PARENB;	    // turn off parity bit
    new_ttyserial.c_cflag &= ~CSTOPB;	    // use only one stop bit
    new_ttyserial.c_cflag &= ~HUPCL;	    // don't hang up the modem lines on close
    new_ttyserial.c_cflag |=  CLOCAL;	    // ignore all modem control lines
    new_ttyserial.c_cflag &= ~CRTSCTS;	    // turn off modem flow control
    new_ttyserial.c_cflag  = (new_ttyserial.c_cflag & ~CSIZE) | CS8;	// set data size to 8 bits

    // Set both the input and output speeds
    if (    cfsetispeed(&new_ttyserial, speed) == -1 || 
	    cfsetospeed(&new_ttyserial, speed) == -1)
    {
	curr_error = BATEBAUD;
	return -1;
    }

    // Try to envoke the new settings
    if (tcsetattr(serial_fd, TCSANOW, &new_ttyserial) == -1)
    {
	curr_error = BATESTTY;
	return -1;
    }
    
    curr_error = BATENONE;
    return 0;
}

void bathw_cleanup()
{
    // Reset terminal settings
    if (old_ttyserial != NULL && serial_fd != -1)
    {
	tcsetattr(serial_fd, TCSANOW, old_ttyserial);
	old_ttyserial = NULL;
    }

    // Close serial port
    if (serial_fd != -1)
    {
	close(serial_fd);
	serial_fd = -1;
    }
}

int bathw_errno()
{
    // While this seems needlessly inefficient, hopefully it should be inlined
    // and it's good for abstraction layers
    return curr_error;
}

const char *bathw_strerror()
{
    // The same deal about inlining applies here
    if (curr_error < BATECOUNT)
	return baterrstrs[curr_error];

    // This means something's really messed up...
    return NULL;
}

int bathw_get_serial_fd()
{
    return serial_fd;
}

// A nifty helper macro for the function below
#define	WRITE_TO_FAN(f, n)  \
    if ((n) >= 0 && (n) <= FAN_MAX)\
    {\
	fancmd[0] = (f);\
	fancmd[2] = (uint8_t)(n);\
	if (write(serial_fd, fancmd, 3) < 3)\
	{\
	    curr_error = BATEWRERR;\
	    return -1;\
	}\
	n##_val = (n);\
    }\
    else if ((n) != FAN_NO_CHANGE)\
    {\
	curr_error = BATEINVAL;\
	return -1;\
    }

int bathw_setfanoutputs(int left, int right, int lift)
{
    uint8_t fancmd[3];   

    if (serial_fd == -1)
    {
	curr_error = BATEUNINIT;
	return -1;
    }

    // This component of the command is common to each fan, ie. each fan only
    // takes 1 data argument.
    fancmd[1] = 1;

    if(left < 0) left=0;
    if(left > FAN_MAX) left=FAN_MAX;
    if(right < 0) right=0;
    if(right > FAN_MAX) right=FAN_MAX;
    if(lift < 0) lift=0;
    if(lift > FAN_MAX) lift=FAN_MAX;
                                                                                                              
    // Envoke our nifty helper macro to write all of the fan output	
    WRITE_TO_FAN('l', left);
    WRITE_TO_FAN('r', right);
    WRITE_TO_FAN('u', lift);

    curr_error = BATENONE;
    return 0;
}

#undef WRITE_TO_FAN

//WARNNING: This function is used to reset the heading sensor and 
//will draw a BIG current for a short of time. NEVER reuse this function
//repeatedly!!!

int bathw_heading_reset()
{
	uint8_t fancmd[3];
	if (serial_fd == -1)
	{
		curr_error = BATEUNINIT;
		return -1;
	}

	fancmd[0]='h';
	fancmd[1]=1;
        fancmd[2] = (uint8_t)(1);
        if (write(serial_fd, fancmd, 3) < 3)
        {
            curr_error = BATEWRERR;
            return -1;
        }
 
	curr_error = BATENONE;
	return 0;
}

void bathw_getfanoutputs(int *left, int *right, int *lift)
{
    // Return a value for each non-NULL pointer
    if (left != NULL)
	*left = left_val;

    if (right != NULL)
	*right = right_val;

    if (lift != NULL)
	*lift = lift_val;
}

int bathw_update_sensors()
{
    int i;
    uint8_t header;
    uint8_t buffer[1 << sizeof(uint8_t)];

    // Read the first two bytes from the serial port they will tell us what
    // type of data we're dealing with
    if (read(serial_fd, &header, sizeof(header)) < sizeof(header))
    {
	curr_error = BATERDERR;
	return -1;
    }
       
    // Loop through the set of known sensors
    for (i = 0; i < SENSOR_COUNT; i++)
	// If we find one that matches the incoming data
	if ((char)header == sensors[i].mnemonic)
	{
	    // Read the data
	    if (read(serial_fd, &buffer, (size_t)sensors[i].datasize) < sensors[i].datasize)
	    {
		curr_error = BATERDERR;
		return -1;
	    }
	    
	  	// And if the sensor is enabled
	    if (sensors[i].enabled)
		// Then call the special handler to deal with it
		if ((*sensors[i].handler)(buffer) == -1)
		    return -1;
	    break;
	}

    curr_error = BATENONE;
    return 0;
}

    /* Sensor specific functions */
    /* Gyro */
int bathw_enable_gyro(const char *calibfile, float *gbuff)
{
    // gbuff must be non-NULL
    if (gbuff == NULL)
	return -1;

    // FIXME : Here is where we load the gyro calibration data from calibfile
    gyro_offset = 0;
    gyro_scale = (5.0/1024)*909.1/6.5; //change the output to be deg/sec.
    
    // Here is where we send the gyro enable command to the Atmel
	// no such command exists yet, but hopefully will at some point

    // Tag the sensor as being enabled
    sensors[SENSOR_GYRO].enabled = 1;

    // Save the buffer pointer
    gyro_buffer = gbuff;
    
    //Add the initial process here!
    init_counter_g=0;
    temp1=0;

    while(init_counter_g < INI_TIMES)
    {
	bathw_update_sensors();
        temp1 = temp1 + (*gbuff);
	init_counter_g++;
    }

	gyro_offset = temp1 / INI_TIMES;
	
    return 0;
}

int bathw_update_gyro(uint8_t *buffer)
{
	// Correct the data using the calibration data
   	// printf("buffer[0]= %d, buffer[1]= %d \n", buffer[0], buffer[1]);
    *gyro_buffer = gyro_scale * (128.0 * (float)buffer[1] + (float)buffer[0]) - gyro_offset;

    return 0;
}

int bathw_disable_gyro()
{
    // Tag the sensor as being disabled
    sensors[SENSOR_GYRO].enabled = 0;

    // Here is where we send the gyro disable command to the Atmel
	// no such command exists yet, but hopefully will at some point

    return 0;
}

    /* Accelerometers */
int bathw_enable_accel(const char *calibfile, float *axbuff, float *aybuff)
{
    // FIXME : Here is were we load the calibration data for the accelerometers
    // from calibfile

    // Here is where we send the accelerometer enable command to the Atmel
	// no such command exists yet, but hopefully will at some point

    // Tag the sensor as being enabled
    sensors[SENSOR_ACCEL].enabled = 1;

    // Save the buffer pointers
    accel_xbuffer = axbuff;
    accel_ybuffer = aybuff;
    
    //Add the initial process here!
    init_counter_a=0;
    temp2=0; temp3=0;
    accel_xoffset=0;
    accel_yoffset=0;                                                                                                                         
    while(init_counter_a < INI_TIMES)
    {
        bathw_update_sensors();
        temp2 = temp2 + (*axbuff);
	temp3 = temp3 + (*aybuff);
        init_counter_a++;
    }
                                                                                                                             
        accel_xoffset = temp2 / INI_TIMES;
	accel_yoffset = temp3 / INI_TIMES;

    return 0;
}

int bathw_update_accel(uint8_t *buffer)
{
    // FIXME : Use the calibration data to normalize the results and store them
    // in the buffers here

	*accel_xbuffer = (float)(buffer[0]+128.0*buffer[1]);
	*accel_xbuffer = *accel_xbuffer / (float)(buffer[4]+128.0*buffer[5]);
	*accel_xbuffer = (*accel_xbuffer - 0.5)/0.125 - accel_xoffset;
	
	*accel_ybuffer = (float)(buffer[2]+128.0*buffer[3]);
	*accel_ybuffer = *accel_ybuffer / (float)(buffer[4]+128.0*buffer[5]);
	*accel_ybuffer = (*accel_ybuffer - 0.5)/0.125 -accel_yoffset;

    return 0;
}

int bathw_disable_accel()
{
    // Tag the sensor as being disabled
    sensors[SENSOR_ACCEL].enabled = 0;

    // Here is where we send the accelerometer disable command to the Atmel
	// no such command exists yet, but hopefully will at some point

    return 0;
}

    /* Heading sensors (magnetometers) */
//int bathw_enable_heading(const char *calibfile, float *hbuff, int *hxmin, int *hxmax, int *hymin, int *hymax)
int bathw_enable_heading(const char *calibfile, float *hbuff)
{
    // FIXME : Here is were we load the calibration data for the heading
    // sensors from calibfile

    // Here is where we send the heading sensor enable command to the Atmel
	// no such command exists yet, but hopefully will at some point

    // Tag the sensor as being enabled
    sensors[SENSOR_HEADING].enabled = 1;

    // Save the buffer pointer
    heading_buffer = hbuff;
    //X_min=hxmin; X_max=hxmax, Y_min=hymin, Y_max=hymax;
	
	//read offsets from calibfile file
	heading_fid = fopen(calibfile, "r");
	fscanf(heading_fid, "%f, %f\n", &hx_offset, &hy_offset);
	printf("xoffset=%f, yoffset=%f\n", hx_offset, hy_offset);
	fclose(heading_fid);

    return 0;
}

int bathw_update_heading(uint8_t *buffer)
{
    // FIXME : Use the calibration data to normalize the results and store them
    // in the buffers here
    
	//printf("headingx=%d, headingy=%d \n", buffer[0]+128*buffer[1], buffer[2]+128*buffer[3]);
	//if ((buffer[0]+128*buffer[1]) < *X_min) *X_min= buffer[0]+128*buffer[1];
        //if ((buffer[0]+128*buffer[1]) > *X_max) *X_max= buffer[0]+128*buffer[1];
        //if ((buffer[2]+128*buffer[3]) < *Y_min) *Y_min= buffer[2]+128*buffer[3];
        //if ((buffer[2]+128*buffer[3]) > *Y_max) *Y_max= buffer[2]+128*buffer[3];        

        *heading_buffer = (float)(buffer[0]+128*buffer[1])-hx_offset;
	*heading_buffer =(float) atan2((double)(buffer[2]+128*buffer[3])-hy_offset, (double)(*heading_buffer));

    return 0;
}

int bathw_disable_heading()
{
    // Tag the sensor as being disabled
    sensors[SENSOR_HEADING].enabled = 0;

    // Here is where we send the heading sensor disable command to the Atmel
	// no such command exists yet, but hopefully will at some point

    return 0;
}
