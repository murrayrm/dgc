#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <ieee754.h>
#include <sys/socket.h>
#include <sys/times.h>
#include <sys/types.h>
#include "bathw.h"

#include "Follower.hh"
#include "../../include/mvwt/MVParameters.h"
#include <netinet/in.h>

Follower::Follower(int vehicleId)
  : m_vehicleId(vehicleId)
  , m_visionSocket(0) 
  , m_commandSocket(0) {
} 

Follower::~Follower(){

  delete m_gains; 
  delete m_traj;

}

int Follower::initialize(short visionPort, short commandPort, GainsData* gains, TRAJ_DATA* traj) {
   int err = 0 ; 

   if (-1 == initVisionSocket(visionPort)) {
     err++;
   }
   
   m_gains = gains;  
   setNewTrajectory(traj);
   std::cout<<"Follower::initialize(), errors "<<err<<std::endl;

   return err;
 }

int Follower::deinitialize() {
  
  // Do the Bat-related cleanup
   bathw_setfanoutputs(0, 0, 0);
   bathw_cleanup();

     // Close any sockets that are open
     if (m_commandSocket != -1)
	 close(m_commandSocket);

     if (m_visionSocket != -1)
	 close(m_visionSocket);
 }

 void Follower::control() {

   if (isTrajComplete()) {
   }

   updateState();

   std::cout<<"Follower::control()"<<std::endl;
   if (LAT == m_ctrlType) {
     lateralControl();
   }
 }

void Follower::lateralControl() {

  static uint8_t leftlevel = 0; 
  static uint8_t rightlevel = 0; 
  static uint8_t liftlevel = 0;

  int fn_r, fn_l;
  float u_1, u_2;
  float forces[2];

  u_1 = -m_gains->y1*m_trajError[Y] - m_gains->t1*m_trajError[THETA] - m_gains->ydot1*m_trajError[YDOT]- 
    m_gains->tdot1*m_trajError[THETADOT] + m_trajError[U1FF];

  u_2 = -m_gains->y2*m_trajError[Y] - m_gains->t2*m_trajError[THETA] - m_gains->ydot2*m_trajError[YDOT]- 
    m_gains->tdot2*m_trajError[THETADOT] + m_trajError[U2FF];

  // calculate fan forces
  //forces[0] = max(0, min(0.5*(u_1 + u_2),f_max));
  //forces[1] = max(0, min(0.5*(u_1 - u_2),f_max));

  forces[0] = 0.5*(u_1 + u_2);
  forces[1] = 0.5*(u_1 - u_2);

  fn_r = (forces[0] + 0.03985) / 0.003745;
  fn_l = (forces[1] + 0.03985) / 0.003745;

  printf("fan forces: right=%d left= %d \n", fn_r, fn_l);
  
  // Send forces to hardware
  bathw_setfanoutputs(
		      fn_l == leftlevel    ? FAN_NO_CHANGE : fn_l,
		      fn_r == rightlevel   ? FAN_NO_CHANGE : fn_r, 
		      LIFT_ON == liftlevel ? FAN_NO_CHANGE : LIFT_ON);
  leftlevel = fn_l;
  rightlevel = fn_r;
  liftlevel = LIFT_ON;
  
  printf("fan output: %d %d %d\n", leftlevel, rightlevel, liftlevel);

}


int Follower::closestTrajPoint() {

  int nearestIndex = 0; // save value to avoid going backwards
  double  nearestDistSq;
  double  currentDistSq;
  double rowVal[11];
  double time;
  
  // Compute the nearest point on the trajectory to where we currently are
  nearestDistSq = 1.0e20;
  for(int i=nearestIndex; i<traj_rows(m_traj); i++) {
    traj_row(m_traj, i, &time, rowVal);
    currentDistSq =
      pow(m_state[X] - rowVal[2+X],2.0) +
      pow(m_state[Y] - rowVal[2+Y] ,2.0);
    
    if(currentDistSq < nearestDistSq) {
      nearestDistSq = currentDistSq;
      nearestIndex = i;
    }
  }
    return nearestIndex; 
}

void Follower::calculateStateError() {

  int i, status, trajpoint, startPoint; 
  double trajTime;
  double trajStartVector[11], trajEndVector[11], trajVector[11];

  /// Find the nearest point on the desired trajectory 
  /// normal  to the current vehicle orientation.

  // Get the trajectory entry for the segment we are on
  startPoint = closestTrajPoint();
  trajpoint = startPoint;

  // Get the start point and end point
  traj_row(m_traj, startPoint, &trajTime, trajStartVector);
  traj_row(m_traj, startPoint+1, &trajTime, trajEndVector);


  // Get the trajectory for the closest point 
  traj_read(m_traj, trajVector, trajTime);

  // Compute lateral distance in body coordinates

  m_trajError[T] = 0;
  m_trajError[X] = 0;

  //Compute y error 
  m_trajError[Y] =
   - (m_state[X] - trajVector[2+X])*sin(trajVector[2+THETA]) + 
   (m_state[Y] - trajVector[2+Y])*cos(trajVector[2+THETA]);

  // Compute error in heading versus angle along the path
  m_trajError[THETA] = m_state[THETA] - trajVector[2+THETA];

  // Figure out the velocity along the path
  double refVel = sqrt(pow(trajVector[2+XDOT], 2.0) +
		     pow(trajVector[2+YDOT], 2.0));

  // Compute the lateral velocity error in body coordinates
  m_trajError[XDOT] = 0;
  m_trajError[YDOT] = -refVel * sin(m_trajError[THETA]);

  // Compute heading rate error along the path
  m_trajError[THETADOT] = m_state[THETADOT] - trajVector[2+THETADOT];

}


void Follower::updateState() {
   /* copy what is returned by estimateState into 
      m_state member */
   estimateState();

 }

 void Follower::estimateState() {
   getNewVisionData();
   getNewGyroData();

   /** Do some sensor fusion to estimate the state here */

 }

 void Follower::getNewVisionData() {

 }

 void Follower::getNewGyroData() {
 }


 int Follower::initVisionSocket(short vision_port)
 {
     struct sockaddr_in sa;

     // Create the vision socket
     if ((m_visionSocket = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
     {
	 fprintf(stderr, "FATAL: socket: %s\nCould not create vision socket.\n",
		 strerror(errno));
	 return -1;
     }

     // We will receive commands on the local port vision_port
     sa.sin_family = AF_INET;
     sa.sin_port = htons(vision_port);
     sa.sin_addr.s_addr = htonl(INADDR_ANY);

     // Bind the socket so we can receive commands
     if (bind(m_visionSocket, (struct sockaddr *)&sa, sizeof(sa)) == -1)
     {
	 fprintf(stderr, "FATAL: bind: %s\nCould not bind vision socket to local port %d.\n",
		 strerror(errno), vision_port);
	 return -1;
     }

     return 0;
 }

 int Follower::receiveVisionData() {

     int num_bytes;
     int sret;
     float fanforces[2];
     VisionPacket vp;	
     fd_set readfds;
     struct timeval tv;

     static char visible = 0;
     static uint8_t leftlevel = 0; 
     static uint8_t rightlevel = 0; 
     static uint8_t liftlevel = 0;
     static int dropped = 0;

     do
     {
	 // Read the next packets-worth of data off the network interface
	 if ((num_bytes = recv(m_visionSocket, &vp, sizeof(vp), MSG_WAITALL)) == -1)
	 {
	     fprintf(stderr, "recv: %s\nCould not receive vision packet\n");
	     return 0;
	 }

	 // Make sure that we got a whole packet
	 if (num_bytes < sizeof(vp))
	 {
	     fprintf(stderr, "Incomplete vision packet dropped. Size = %d\n", num_bytes);
	     return 0;
	 }

	 // If we've lost a packet,
	 if (((union ieee754_float *)(&vp.vehicle[m_vehicleId].x))->f == 0)
	 {
	     // Use the last theta as long as we haven't lost too many
	     if (dropped < MAX_FRAME_SKIP)
		 dropped++;
	     // If we have lost too many, we've become invisible
	     else
		 visible = 0;
	 }
	 // Otherwise, extract this robot's current angle from the vision packet
	 else
	 {
	     // Correct the float word order of the incoming data
	     wordSwap(&vp.vehicle[m_vehicleId].x);
	     wordSwap(&vp.vehicle[m_vehicleId].y);
	     wordSwap(&vp.vehicle[m_vehicleId].theta);
	     wordSwap(&vp.vehicle[m_vehicleId].xdot);
	     wordSwap(&vp.vehicle[m_vehicleId].ydot);
	     wordSwap(&vp.vehicle[m_vehicleId].thetadot);

	     // Copy the new vision data to where the controller can use it
	     m_state[X]		= vp.vehicle[m_vehicleId].x;
	     m_state[Y]		= vp.vehicle[m_vehicleId].y;
	     m_state[THETA]		= vp.vehicle[m_vehicleId].theta;
	     m_state[XDOT]		= vp.vehicle[m_vehicleId].xdot;
	     m_state[YDOT]		= vp.vehicle[m_vehicleId].ydot;
	     m_state[THETADOT]	= vp.vehicle[m_vehicleId].ydot;

	     printf("got vision: state = (%8f, %8f, %8f, %8f, %8f, %8f)\r",
		 m_state[0], m_state[1], m_state[2], m_state[3], m_state[4], m_state[5]);
	     // So we've dropped zero consecutive packets and the vehicle is visible
	     dropped = 0;
	     visible = 1;
	 }

	 // Now do a select with a zero-time timeout. This will effectively test
	 // to see if there any data to be read on the vision socket.
	 FD_ZERO(&readfds);
	 FD_SET(m_visionSocket, &readfds);


	 if ((sret = select(m_visionSocket + 1, &readfds, NULL, NULL, &tv)) == -1)
	 {
	     fprintf(stderr, "FATAL: select %s\nSelect failed.\n", strerror(errno));
	     return -1;
	 }

	 // Loop and get the next packet if it has already arrived
     } while (sret);

 }


 void Follower::wordSwap(void *dword)
 {
     uint32_t tmp;

     tmp = ((uint32_t *)dword)[0];
     ((uint32_t *)dword)[0] = ((uint32_t *)dword)[1];
     ((uint32_t *)dword)[1] = tmp;
 }

 void Follower::receiveNewTrajectory() {
 }

 void Follower::setNewTrajectory(TRAJ_DATA* trajData) {

   m_traj = trajData;
   std::cout<<"setNewTrajectory(), numRows="<<(*traj_rows)(m_traj)<<std::endl;
   (*traj_disp)(m_traj);
}

void Follower::getRecedingHorizonTraj() {

}

bool Follower::isTrajComplete() {
  /* Calculate the current state and compare to 
     the end point of the receding horizon trajectory */
  std::cout<<"isTrajComplete() FALSE"<<std::endl;
  return false;
}


