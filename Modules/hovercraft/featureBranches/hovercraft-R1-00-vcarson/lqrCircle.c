/* File:        lqrCircle.c
 * Description: This program is an example to show how to use multi-threads
 *              to realize feedback control loop on Bats. The feedbacks 
 *              include all the onboard sensors and vision data. The 
 *              controller is a lqr controller. All the code should be rewritten
 *              by C++.
 * OS:          Embedix Linux (Zaurus PDA SA-1110 Architecture)
 * Author:      Zhipu Jin
 */

/* System Includes */
#include <errno.h>
#include <ieee754.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/times.h>
#include <sys/time.h>
#include <sys/types.h>
#include <signal.h>      
#include <sys/select.h>
#include <sys/resource.h>
#include <pthread.h>
                                                               
/* Local Includes */
#include "MVCommandPacket.h"
#include "MVWT1VisionPacket.h"
#include "bathw.h"

#define STATE_SIZE                      6
#define STATE_X                         0
#define STATE_Y                         1
#define STATE_THETA                     2
#define STATE_X_DOT                     3
#define STATE_Y_DOT                     4
#define STATE_THETA_DOT                 5

#define FANOFFSET			55
#define PI				3.14159265                          
#define LIFT				180

int controller_freq = 50; // the controller will update at 50 Hz
int activeflag;

//sensor data
float gyro_value;
float accel_x, accel_y;
float heading_value;

//vision data
float vision_state[STATE_SIZE];
//int comm_sock = -1;
int vision_sock = -1;
short vision_port;
int vehicle_id;

//controller states
float controller_state[6];

//fan force
float fl, fr; //real fan force value
int fan_left, fan_right, fan_lift; //fan force signals
int fan_offset;

//for the user inputs
char string[20];
int length;

//LQR controller matrix
float Dc[2][6];

//trajectory parameters
float R, speed_r, xc, yc;

//parameters of hovercraft
float m0=0.749;
float mu=0.015; //should be smaller?
float J=0.0031;
float psi=0.005; //should be smaller?
float r_f=0.089;
float force_array[41];

/* Function:    word_swap
 * Description: Swaps the words of the double word argument. Note that here the
 *              term "word" refers to a word on a 32-bit architecture. To make
 *              this function truly a word swap, int's would be used here
 *              rather than uint32_t's. They are not, however, because the
 *              primary use of this function is for swapping IEEE 754 "float
 *              words" which are 32 bits.
 * Argument:    void *dword - a pointer to a 64-bit value whose 32-bit halves
 *                  are to be swapped.
 */
void word_swap(void *dword)
{
    uint32_t tmp;
                                                                                      
    tmp = ((uint32_t *)dword)[0];
    ((uint32_t *)dword)[0] = ((uint32_t *)dword)[1];
    ((uint32_t *)dword)[1] = tmp;
}

/* Function:    init_vision_socket
 * Description: Initializes the socket on which MVWT vision data will be
 *              received. This means both creating the socket and binding it to
 *              a local port.
 * Argument:    short vision_port - the port, in host byte order, to which the
 *                  socket should be bound
 * Returns:     0 on success, -1 on error
 */
int init_vision_socket(short vision_port)
{
    struct sockaddr_in sa;
                                                                                                                             
    // Create the vision socket
    if ((vision_sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        fprintf(stderr, "FATAL: socket: %s\nCould not create vision socket.\n",
                strerror(errno));
        return -1;
    }
                                                                                                                             
    // We will receive commands on the local port vision_port
    sa.sin_family = AF_INET;
    sa.sin_port = htons(vision_port);
    sa.sin_addr.s_addr = htonl(INADDR_ANY);
                                                                                                                             
    // Bind the socket so we can receive commands
    if (bind(vision_sock, (struct sockaddr *)&sa, sizeof(sa)) == -1)
    {
        fprintf(stderr, "FATAL: bind: %s\nCould not bind vision socket to local port %d.\n",
                strerror(errno), vision_port);
        return -1;
    }

    return 0;
}

/* Function:    handle_vision
 * Description: Once vision data is received this function should be called to
 *              read and handle it. Specifically, once new state data is
 *              received, this function updates the controller and sends the
 *              new controller outputs to the fans.
 * Returns:     0 on success, -1 on error
 */
int handle_vision()
{
    int num_bytes;
    int sret;
    int fn_r, fn_l;
    float fanforces[2];
    VisionPacket vp;
    fd_set readfds;
    struct timeval tv;
    static char visible = 0;
    static uint8_t leftlevel = 0;
    static uint8_t rightlevel = 0;
    static uint8_t liftlevel = 0;
    static int dropped = 0;
    
    //printf("Got vision.\n");
   
    do
    {
        // Read the next packets-worth of data off the network interface
        if ((num_bytes = recv(vision_sock, &vp, sizeof(vp), MSG_WAITALL)) == -1)
        {
            fprintf(stderr, "recv: %s\nCould not receive vision packet\n");
            return 0;
        }
                                                                                                                             
        // Make sure that we got a whole packet
        if (num_bytes < sizeof(vp))
        {
            fprintf(stderr, "Incomplete vision packet dropped. Size = %d\n", num_bytes);
            return 0;
        }
                                                                                                                             
        // If we've lost a packet,
        if (((union ieee754_float *)(&vp.vehicle[vehicle_id].x))->f == 0)
        {
            // Use the last theta as long as we haven't lost too many
            if (dropped < MAX_FRAME_SKIP)
                dropped++;
            // If we have lost too many, we've become invisible
            else
                visible = 0;
        }
        // Otherwise, extract this robot's current angle from the vision packet
        else
        {
            // Correct the float word order of the incoming data
            word_swap(&vp.vehicle[vehicle_id].x);
            word_swap(&vp.vehicle[vehicle_id].y);
            word_swap(&vp.vehicle[vehicle_id].theta);
            word_swap(&vp.vehicle[vehicle_id].xdot);
            word_swap(&vp.vehicle[vehicle_id].ydot);
            word_swap(&vp.vehicle[vehicle_id].thetadot);
                                                                                                                             
            // Copy the new vision data to where the controller can use it
            vision_state[STATE_X]              = vp.vehicle[vehicle_id].x;
            vision_state[STATE_Y]              = vp.vehicle[vehicle_id].y;
            vision_state[STATE_THETA]          = vp.vehicle[vehicle_id].theta;
            vision_state[STATE_X_DOT]          = vp.vehicle[vehicle_id].xdot;
            vision_state[STATE_Y_DOT]          = vp.vehicle[vehicle_id].ydot;
            vision_state[STATE_THETA_DOT]      = vp.vehicle[vehicle_id].ydot;
                                                                                                                             
            //printf("got vision: state = (%8f, %8f, %8f, %8f, %8f, %8f)\r",
            //    state[0], state[1], state[2], state[3], state[4], state[5]);
            // So we've dropped zero consecutive packets and the vehicle is visible
            dropped = 0;
            visible = 1;
        }
                                                                                                                             
        // Now do a select with a zero-time timeout. This will effectively test
        // to see if there any data to be read on the vision socket.
        FD_ZERO(&readfds);
        FD_SET(vision_sock, &readfds);
        tv.tv_sec = 0;
        tv.tv_usec = 0;
                                                                                                                             
        if ((sret = select(vision_sock + 1, &readfds, NULL, NULL, &tv)) == -1)
        {
            fprintf(stderr, "FATAL: select %s\nSelect failed.\n", strerror(errno));
            return -1;
        }
                                                                                                                             
        // Loop and get the next packet if it has already arrived
    } while (sret);
}

/* Function:    cleanup
 * Description: Close file handles and perform Bat hardware-related cleanup
 */
void cleanup()
{
    // Do the Bat-related cleanup
    bathw_setfanoutputs(0, 0, 0);
    bathw_cleanup();
    
    // Close any sockets that are open
    if (vision_sock != -1)
        close(vision_sock);
}

float radnorm(float angle)
{
        float u;
                                                                                                           
        u = fmod( angle, 2*PI );
        if( u > PI  ) u = u - 2*PI;
        if( u < -PI ) u = u + 2*PI;
                                                                                                           
        return u;                                                                                                           
}  

void force_map(float f, int * signal)
{
	//force_map here
	int i;

	if(f <= 0){
		 *signal=0;
		 return;
	}

	if(f >= 0.706){
		 *signal=185;
		 return;
	}

	for(i=0; i<41; i++){
		if(force_array[i] < f)
			continue;
		else{
			*signal=5*i+(int)(5*(f-force_array[i-1])/(force_array[i]-force_array[i-1]));
			break;
		} 
	}

	return;
}                             
                                                                                              
//the user input thread
void *Getcommand(void *threadid)
{
	//printf("Thread %d begins!\n", threadid);

	while(1){	
		length=fscanf(stdin, "%s", &string);
	        if (length > 0){
                        if (string[0] == 'b'){
                                printf("Begin to run!\n");
                                activeflag=1;
                        }
                        if (string[0] == 's'){
                                printf("Stop the vehicle!\n");
                                activeflag=0;
                        }
                        if (string[0] == 'q'){
                                printf("Quit!\n");
                                activeflag=-1;
				break;
                        }                                                                                                     
                printf("Input a command (b for begin,s for stop, q for quit):");
                }
	}
	pthread_exit(NULL);
}	

//the vision update thread
void *VisionUpdate(void *threadid)
{
	//struct timeval tv;
	fd_set readfds;
	int sret;

	if (init_vision_socket(vision_port) == -1)
    	{
        	if(vision_sock != -1)
			close(vision_sock);
        	pthread_exit(NULL);
    	}
        
	//printf("Thread %d begins!\n", threadid);
	while(activeflag != -1){
        // This is a dummy loop at the moment. At some point in the near future
        // it will serve to periodically check the battery
        	do
        	{
            	FD_ZERO(&readfds);
            	FD_SET(vision_sock, &readfds);
                                                                                
            	//tv.tv_sec = 1;
            	//tv.tv_usec = 0;
                                                                                
            	if ((sret = select(vision_sock+1, &readfds, NULL, NULL, NULL)) == -1)
            	{
                	fprintf(stderr, "FATAL: select %s\nSelect failed.\n", strerror(errno));
                	cleanup();
                	pthread_exit(NULL);
            	}
                                                                                
        	} while (!sret);
                                                                                
        // The vision update must come before the command update because having
        // stale vision data can cause loops
        	if (FD_ISSET(vision_sock, &readfds) &&
                	handle_vision() == -1)
            	break;
	}

	if(vision_sock != -1)
		close(vision_sock);

	pthread_exit(NULL);
}

//the sensor update thread
void *SensorUpdate(void *threadid)
{
        //printf("Thread %d begins!\n", threadid);
        while(activeflag != -1){
        //update sensors data here.
	bathw_update_sensors();                      
        }

        pthread_exit(NULL);
}

//controller update code
void timer_handler(int sig)
{
	int i,k;
       	float termD[2];
       	float output[2];
       	float error[5];
       	float r, gamma, alpha;
	float Fp_e, Fs_e, Fp_nom, Fs_nom;

        //update controller's states here and calculate the control laws
	if(activeflag == 1){
	//update controller states
		//step 0: Decide direction and start point
        	r=sqrt((vision_state[0]-xc)*(vision_state[0]-xc)+(vision_state[1]-yc)*(vision_state[1]-yc));
        	//alpha=atan(m0*speed_r/mu/R);
		alpha=PI/8;
        	gamma=atan2((vision_state[1]-yc),(vision_state[0]-xc))+PI/2;
		//gamma=radnorm(gamma);
		
		//printf("alpha = %f, gamma =%f\n", alpha, gamma);  
        	vision_state[2]=radnorm(vision_state[2]);
	    	
		// Make sure the vehicle has a gyro.
          	vision_state[5]=gyro_value*PI/180.0;	//change back to rad/sec

		//step 1: get the error vector.
                                                                                          
       		//get the errors
       		error[0]=vision_state[3]*cos(gamma) + vision_state[4]*sin(gamma) - speed_r; //speed error       
		error[1]=(r-R)/speed_r; //the controller will independ on the speed.
       		error[2]=((vision_state[0]-xc)*vision_state[3]+(vision_state[1]-yc)*vision_state[4])/r;
       		error[3]=gamma+alpha-vision_state[2];
       		error[4]=-vision_state[5]+speed_r/R;
                                                                                          
        	//upwrap the angle
        	error[3]=radnorm(error[3]);
                                                                                          
		//step 2: calculate controller's states.
                                                                                                           
		for ( i=0; i < 2; i++ ) {
      			termD[i] = 0;
      			for ( k=0; k < 5; k++) {
         			termD[i] += Dc[i][k]*error[k];
      			}
      			output[i] = termD[i];
    		}
                                                                                                           
	    	//step 3: generate the fan_error force.
                                                                                                           
        	Fp_e = 0.5*(output[0]+output[1]);
        	Fs_e = 0.5*(output[0]-output[1]);
                
		//step 4: generate the fan_normal force.
        	Fp_nom=0.5*(sqrt((m0*speed_r*speed_r/R)*(m0*speed_r*speed_r/R)+(mu*speed_r)*(mu*speed_r))-psi/r_f*speed_r/R);
        	Fs_nom=Fp_nom+(psi/r_f)*speed_r/R;
                                                                                 
	    	//step 5: get the fan force.                                                                                           
	        fl = Fp_e + Fp_nom;
        	fr = Fs_e + Fs_nom;
	
	//force mapping

		force_map(fl, &fan_left);
		force_map(fr, &fan_right);

		fan_lift=LIFT;
	
	//send the signal to fans. left fan is a little weak
		bathw_setfanoutputs(fan_left+fan_offset+1, fan_right+fan_offset, fan_lift+fan_offset);	
	
	}
	else if(activeflag == -1){
		bathw_setfanoutputs(0,0,0);
	}
	else if(activeflag == 0)
		bathw_setfanoutputs(0,0,0);                                                                                                             
}

int main(int argc, char **argv)
{
    int c, i, j;
    int priority;

    //for the timer
    long delay;
    char *endptr;
    struct sigaction sa;
    struct itimerval itv;
    struct timeval starttime;
    struct timeval endtime;
    double dtime;

    //for multi-threads
    pthread_t threads[3];
    int rc, id;
    FILE * traj, * Ctrl_file, * forcemap_file;

    // Set the default ports to be overwritten if command-line options are used
    vision_port = DEFAULT_VISION_PORT;

    // Get the vehicle id off of the command line
    if(argc <= 1){
	printf("%s usage: %s <the ID of the hat>\n", argv[0], argv[0]);
	exit(1);
    }

    vehicle_id = atoi(argv[1]);
    //printf("%s id=%d \n", argv[1], vehicle_id);
    if (vehicle_id <= 0 || vehicle_id > NUM_VEHICLES)
    {
        printf("Argument is invalid vehicle identifier. It must be an integer between 0 and %d.\n",
                NUM_VEHICLES);
        return 1;
    }
    // We used the zero-based vehicle id's here
    vehicle_id--;
    
    delay = 1000000/controller_freq; //for the timer
    //printf("Delay = %d \n", delay);

        //initial the serial port
	if(bathw_init(DEFAULT_COMM_DEVICE, DEFAULT_COMM_SPEED) == -1)
        {
                fprintf(stderr, "Could not initialize Bat hardware: %s\n", bathw_strerror());
                return 1;
        }
	
	//initial sensors
	printf("Enabling gyro, please wait... ");
        if(bathw_enable_gyro("gyro_calib.data", &gyro_value) == -1)
        {
                fprintf(stderr, "Could not enable gyro sensor: %s\n", bathw_strerror());
                return 1;
        }                                                                         
        printf("Done!\n");

	printf("Enabling accelerometer, please wait... ");
	if(bathw_enable_accel("accel_calib.data", &accel_x, &accel_y) == -1)
        {
                fprintf(stderr, "Could not enable accelerometer: %s\n", bathw_strerror());
                return 1;
        }
	printf("Done!\n");

	printf("Enabling heading, please wait... ");
        if(bathw_enable_heading("heading_calib.dat", &heading_value) == -1)
        {
                fprintf(stderr, "Could not enable heading sensor: %s\n", bathw_strerror());
                return 1;
        }
        printf("Done!\n");

        bathw_heading_reset();
        printf("Reset the heading sensor! \n");
                                                                                                    
	//first, stop the fans
	activeflag=0;
	bathw_setfanoutputs(0,0,0);

        printf("Input a command (b for begin, s for stop, q for quit):");

	//change the priority of the porcess
	priority = -10; //between -20 to 20, lower priorities cause more favorable scheduling.

	if (setpriority(PRIO_PROCESS, 0, priority) == -1)
		fprintf(stderr, "Could not set process priority. You're probably not root.\n");

	//set timer
	sa.sa_handler = &timer_handler;
    	sa.sa_flags = 0;

	if (sigaction(SIGALRM, &sa, NULL) == -1)
    	{
		fprintf(stderr, "sigaction: %s\nCould not setup timer handler.\n",
			strerror(errno));
		return 1;
    	}

	itv.it_interval.tv_sec = 0;
    	itv.it_interval.tv_usec = delay;
    	itv.it_value.tv_sec = 0;
    	itv.it_value.tv_usec = delay;

    	if (setitimer(ITIMER_REAL, &itv, NULL) == -1)
    	{
		fprintf(stderr, "setitimer: %s\nCould not setup timer.\n",
			strerror(errno));
		return 1;
    	}

    	sa.sa_handler = SIG_IGN;
    	itv.it_interval.tv_sec =	0;
    	itv.it_interval.tv_usec =	0;
    	itv.it_value.tv_sec =	0;
    	itv.it_value.tv_usec =	0;

	gettimeofday(&starttime, NULL);

	//get trajectory parameters;
	//printf("Read circel.rc file\n");
	//getchar();
	traj=fopen("circle.rc", "r");
	if(traj == NULL){
		printf("Cannot open circle.rc file!\n");
		exit(1);
	}
	fscanf(traj, "%f, %f, %f, %f, %f, %f", &R, &speed_r, &xc, &yc, &mu, &psi);
	//printf("R=%f, speed_r=%f, xc=%f, yc=%f\n", R, speed_r, xc, yc);
	fclose(traj);

        //get lqr controller parameters;
        //printf("Read lqr_ctrl.rc file\n");
        //getchar();
        Ctrl_file=fopen("lqr_ctrl.rc", "r");
        if(Ctrl_file == NULL){
                printf("Cannot open lqr_ctrl.rc file!\n");
                exit(1);
        }
        
	for (i=0; i<2; i++)
        	for (j=0; j<5; j++)
			fscanf(Ctrl_file, "%f, ", &Dc[i][j]);

	//printf("Dc[0][0]=%f, Dc[1][0]=%f, Dc[1][4]=%f\n", Dc[0][0], Dc[1][0], Dc[1][4]);
        fclose(Ctrl_file);

        //get force map parameters;
        //printf("forcemap.rc file\n");
        //getchar();
        forcemap_file=fopen("forcemap.rc", "r");
        if(forcemap_file == NULL){
                printf("Cannot open lqr_ctrl.rc file!\n");
                exit(1);
        }
                                                                                      
        for (i=0; i<41; i++)
        	fscanf(forcemap_file, "%f  ", &force_array[i]);

	//printf("array[0]=%f, array[40]=%f\n", force_array[0], force_array[40]);                 
        fclose(forcemap_file);                                                                                      
        //generate the threads
        //printf("Start generating child threads! \n");
                                                                                                                   
        id = 1;
        rc = pthread_create(&threads[0], NULL, Getcommand, (void*)id);
        if(rc){
                fprintf(stderr, "ERROR: return code from pthread_creat() is %d.\n", rc);
                exit(-1);
        }
        //printf("Thread 1 is created!\n");
                                                                                                                 
        id = 2;
        rc = pthread_create(&threads[1], NULL, VisionUpdate, (void*)id);
        if(rc){
                fprintf(stderr, "ERROR: return code from pthread_creat() is %d.\n", rc);
                exit(-1);
        }
        //printf("Thread 2 is created!\n");
                                                                                                                   
        id = 3;
        rc = pthread_create(&threads[2], NULL, SensorUpdate, (void*)id);
        if(rc){
                fprintf(stderr, "ERROR: return code from pthread_creat() is %d.\n", rc);
                exit(-1);
        }
        //printf("Thread 3 is created!\n");
                                                                                                                   

	// loop in the main thread
    	while(activeflag != -1)
    	{
		//bathw_update_sensors();
		//printf("gyro= %8f, vision_x= %8f \r", gyro_value, vision_state[0]);
                //printf("G=%f, H=%f, AX=%f, AY=%f \r", gyro_value, heading_value*180/PI, accel_x, accel_y);
		//printf("got vision: state = (%8f, %8f, %8f, %8f, %8f, %8f)\r", vision_state[0], vision_state[1], vision_state[2], vision_state[3], vision_state[4], vision_state[5]);
	}

	
	bathw_setfanoutputs(0,0,0);

	gettimeofday(&endtime, NULL);
    	if (sigaction(SIGALRM, &sa, NULL) == -1)
    	{
		fprintf(stderr, "sigaction: %s\nCould not restore timer handler.\n",
			strerror(errno));
		return 1;
    	}

    	if (setitimer(ITIMER_REAL, &itv, NULL) == -1)
    	{
		fprintf(stderr, "setitimer: %s\nCould not stop timer.\n",
			strerror(errno));
		return 1;
    	}    

	bathw_disable_gyro();
	bathw_disable_accel();
	bathw_disable_heading();
        bathw_cleanup();

	dtime = ((double)(endtime.tv_sec - starttime.tv_sec) +
             1e-6*(double)(endtime.tv_usec - starttime.tv_usec));
    	printf("Time: %f\n", dtime);

	//pthread_exit(NULL);
    	return 0;
}
