#ifndef __CONTROLLER_H__
#define __CONTROLLER_H__
// controller.h
// Author: Steve Waydo <waydo@cds.caltech.edu>
// Code snippets for MVWT II "Bat" controller
// vim:ts=8:sts=4:sw=2

int load_controller(const char *gainfile, const char *paramfile);
void update_controller( float const state[6], const float ref[2], float forces[2] );

#endif // __CONTROLLER_H__
