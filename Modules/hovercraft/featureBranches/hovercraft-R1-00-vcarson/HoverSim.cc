#include <iostream>
#include "Follower.hh"


static void printUsage() {

  std::cout <<"hoversim [-h] [-c COMMAND PORT] [-v VISION PORT ] [-n VEHICLE NUMBER] [-g GAINS FILE] [-t TRAJ FILE]"<<std::endl; 
  std::cout <<"-c Select the port on which commands are to be received"<<std::endl; 
  std::cout <<"-v Select the port on which vision data is to be received"<<std::endl; 
  std::cout <<"-n Select the vehicle number"<<std::endl; 
  std::cout <<"-g Select the gains file for the controller "<<std::endl; 
  std::cout <<"-t Select the trajectory file to follow"<<std::endl; 
  std::cout <<"-h Display usage message "<<std::endl;
  exit(0);
}


static void printOptions(bool sim, short commPort, short visionPort, int vehicleId, char* gainsFile, char* trajFile) {

  std::cout <<"hoversim "<<std::endl;
  std::cout <<(sim?"SIMULATION":"HARDWARE")<<std::endl;
  std::cout <<"COMMAND PORT: "<<commPort<<std::endl;  
  std::cout <<"VISION PORT: "<<visionPort<<std::endl; 
  std::cout <<"VEHICLE NUMBER: "<<vehicleId <<std::endl; 
  std::cout <<"GAINS FILE: "<<gainsFile <<std::endl; 
  std::cout <<"TRAJECTORY  FILE : "<<trajFile <<std::endl; 
}


int main(int argc, char **argv) {


  // Set the default ports to be overwritten if command-line options are used
  short commPort = DEFAULT_COMMAND_PORT;
  short visionPort = DEFAULT_VISION_PORT;
  int vehicleId = 1;


  int arg,targ;

  char *endptr, *trajFile, *gainsFile; 

  bool sim = false;

  // Get the command-line arguments
  while ((arg = getopt(argc, argv, "s:c:v:n:g:t:h")) != -1) {
    switch (arg) {
    case 's':
      sim = true; 
    case 'c':
      targ = strtol(optarg, &endptr, 0);
      
      if (*endptr || targ < 0 || targ > USHRT_MAX)
	fprintf(stderr, "Invalid port %s ignored\n", optarg);
      else
	commPort = targ;
      break;
      
    case 'v':
      targ = strtol(optarg, &endptr, 0);
      
      if (*endptr || targ < 0 || targ > USHRT_MAX)
	fprintf(stderr, "Invalid port %s ignored\n", optarg);
      else
	visionPort = targ;
      break;

    case 'n':
      targ = strtol(optarg, &endptr, 0);
      
      if (*endptr || targ < 0 || targ > USHRT_MAX)
	fprintf(stderr, "Invalid vehicle number %s ignored\n", optarg);
      else
	vehicleId = targ;
      break;

    case 'g':
      gainsFile = optarg;
      break;

    case 't':
      trajFile = optarg;
      break;
      
    case 'h':
      printUsage();
      break;
      
    case '?':
      fprintf(stderr, "Unrecognized option '%c' ignored.\n", arg);
      break;
      
    default:
      break;
    }
  }

  printOptions(sim, commPort, visionPort, vehicleId, gainsFile, trajFile);

  std::cout<<"Hello Hovercraft!"<<std::endl;
    
  Follower::Follower* follower = new Follower(vehicleId);
    
  GainsData* gains = new GainsData();    

  ///For now just include the gains here 
  gains->xGain = 1; 
  gains->yGain = 2; 
  gains->thetaGain = 3; 

  gains->xdotGain = 4;
  gains->ydotGain = 5;
  gains->thetadotGain = 6;
    
  gains->xddotGain = 7;
  gains->yddotGain = 8;
  gains->thetaddotGain = 9;
    
  TRAJ_DATA* traj;    
  traj = (*traj_load)(trajFile);

  follower->initialize(visionPort, commPort, gains, traj);

  follower->control(); 
  delete follower; 
    
  std::cout<<"Goodbye Hovercraft!"<<std::endl;
    
  return 0; 
}
