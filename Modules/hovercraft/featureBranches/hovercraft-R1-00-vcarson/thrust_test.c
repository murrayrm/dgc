//This is a small program used to test thrust
//Zhipu Jin, Feb. 2004

#include <stdio.h>
#include "bathw.h"

int main()
{
	int leftfan, rightfan, liftfan;
	int offset;
	char ch;
	
	offset = 55; //this is the value where the fan begins to run

	if(bathw_init(DEFAULT_COMM_DEVICE, DEFAULT_COMM_SPEED) == -1)
	{
		fprintf(stderr, "Could not initialize Bat hardware: %s\n", bathw_strerror());
		return 1;
	}

	printf("Complete initialization.....\n");

	leftfan=offset; rightfan=offset; liftfan=offset;

	while(fscanf(stdin, "%c", &ch))
	{
		printf("i: right++, k: right--, e: left++, d: left--, s: stop\n");
		if((ch == 'i'))
		{
			//leftfan++;
			rightfan++;
			liftfan=180+offset;		
			printf("Left=%d, right=%d, lift=%d \n", leftfan-offset, rightfan-offset, liftfan-offset);
	                bathw_setfanoutputs(leftfan, rightfan, liftfan);
		}                                                                                                               
                else if ((ch == 'k'))
                {
                        rightfan--;
                        liftfan=180+offset;
                        printf("Left=%d, right=%d, lift=%d \n", leftfan-offset, rightfan-offset, liftfan-offset);
                        bathw_setfanoutputs(leftfan, rightfan, liftfan);
                }
                else if ((ch == 'e'))
                {
                        leftfan++;
                        liftfan=180+offset;
                        printf("Left=%d, right=%d, lift=%d \n", leftfan-offset, rightfan-offset, liftfan-offset);
                        bathw_setfanoutputs(leftfan, rightfan, liftfan);
                }
		else if ((ch == 'd'))
		{
			leftfan--;
			liftfan=180+offset;
			printf("Left=%d, right=%d, lift=%d \n", leftfan-offset, rightfan-offset, liftfan-offset);
	                bathw_setfanoutputs(leftfan, rightfan, liftfan);
		}
		else if (ch == 's')
		{
			leftfan = offset;
			rightfan= offset;
			liftfan = offset;			
			printf("Left=%d, right=%d, lift=%d \n", leftfan-offset, rightfan-offset, liftfan-offset);
			bathw_setfanoutputs(leftfan, rightfan, liftfan);
		}
		else if (ch == 'q')
			break;
	}

	bathw_setfanoutputs(0,0,0);
	bathw_cleanup();

	return 0;
}
