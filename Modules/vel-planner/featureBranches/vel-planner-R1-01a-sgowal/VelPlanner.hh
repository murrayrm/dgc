#ifndef VELPLANNER_HH_
#define VELPLANNER_HH_

#include <assert.h>
#include <iostream>
#include <sstream>
#include <fstream>

#include <frames/pose3.h>
#include <frames/point2.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <alice/AliceConstants.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ActuatorState.h>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>
#include <trajutils/maneuver.h>
#include <trajutils/man2traj.hh>
#include <trajutils/traj.hh>
#include <temp-planner-interfaces/CmdArgs.hh>

/// @brief Vehicle kinematic properties.
struct VelPlannerKinematics
{
  /// Distance between front and rear wheels (m).
  double wheelBase;

  /// Maximum steering angle (degrees).  This is the largest angle
  /// that the lowel-level will command.  This value is also used to
  /// scale the measured steering position in ActuatorState (which is
  /// in the domain [-1, +1]) to a steering angle.
  double maxSteer;
};


/// @brief General speed limits.
struct VelPlannerStaticParams
{  
  /// Max speed in intersections (m/s).
  double maxIntersectionSpeed;

  /// Max speed in zones (m/s).
  double maxZoneSpeed;

  /// Max speed when reversing (m/s).
  double maxReverseSpeed;

  /// Speed error, at which speed is planned
  /// from current velocity instead of planned. (m/s)
  double replanSpeedErr;

  /// Speed output to traj when the desired speed is 0 m/s. (m/s)
  double almostZeroSpeed;

  /// Speed under which the vehicle is considered to be stopped (m/s).
  double stopSpeed;

  /// Minimum reference speed when accelerating (m/s).
  double minStartSpeed;

  /// Maximum allowed acceleration (m/s/s).
  double maxAccel;

  /// Maximum allowed deceleration (m/s/s).
  double maxDecel;

  /// Deceleration to use when planner sends a pause (m/s/s).
  double pauseDecel;

  /// Maximum possible deceleration (m/s/s).
  double maxEmergencyDecel;

  /// Maximum lateral acceleration (m/s/s).
  double maxLatAccel;

  /// Maximum rate of steering angle (rad/s)
  double maxSteerRate;

  /// Stop line distance (m).  This is the desired distance between the stop
  /// line and Alice's front.
  double stopDist;

  /// Maximum distance between the stop line and Alice's front,
  /// for vel-planner to consider Alice to be close enough to stop. (m)
  double stopDistTol;

  /// Limit speed based on lateral error.
  /// Ramp down maximum allowed speed linearly
  /// from m_params.maxCorrSpeed when the error is <= m_params.minCorrDist
  /// to m_params.minCorrSpeed when the error is >= m_params.maxCorrDist.
  double minCorrDist;  // (m)
  double maxCorrDist;  // (m)
  double minCorrSpeed;  // (m/s)
  double maxCorrSpeed;  // (m/s)

  /// Never set speed lower than this to give the steering wheels time to turn.
  double minSteerRateSpeedLimit;

  /// Distance to look ahead when smothing accelerations (m)
  double smoothAccelDist;

  /// Check if path is feasible and cut corners if necessary. 1=enabled, 0=disabled
  int feasibilize;

  /// Output to mapwierer. 1=enabled, 0=disabled
  int outputToMapViewer;
};


class VelPlanner {

public: 

  static int init();
  static void destroy();
  static Err_t planVel(CTraj* traj, Path_t* path, StateProblem_t problem, VehicleState vehState, Vel_params_t params);
  static void plotPath(Path_t *path, int startNode, int sendSubGroup);
  static void plotSpeed(double speed, int sendSubGroup);
  static void plotPaths(Path_t *path_1, Path_t *path_2, int sendSubGroup);
  static void plotTraj(CTraj *traj, int sendSubGroup);
  static void plotMap(int *map, int length, int sendSubGroup);
  static void plotPathDists(Path_t *sparsePath, Path_t *densePath, int *sparseToDenseMap, int sendSubGroup);
  static void printMatlab(CTraj* traj);
private :
  static CMapElementTalker m_meTalker;

  // Vehicle kinematic properties
  static VelPlannerKinematics m_kin;

  // Speed limits
  static VelPlannerStaticParams m_params;
  
  //public:  // FIXME: Make private?

  static Err_t calcDists(Path_t *path);
  static Err_t smoothPath(Path_t *path);
  static Err_t densifyPath(const Path_t *sparsePath, Path_t *densePath,
                           int *sparseToDenseMap, int *mapLength,
                           const VehicleState *vehState, CTraj *traj);
  static Err_t calculateCurvatures(Path_t *densePath);
  static Err_t genSpeedProfile(const Path_t *sparsePath, Path_t *densePath,
                               const VehicleState *vehState, const int *sparseToDenseMap, int mapLength,
                               Vel_params_t velParams, CTraj *traj, StateProblem_t *stateProb, int *startNode);
  static Err_t genTraj(const Path_t *densePath, CTraj *traj);

  /*/// @brief Generate the speed profile.
  /// @param[in,out] path Planned path; this function fills out the speed array.
  /// @param[in] vehState Current vehicle state data.
  /// @param[in] actState Current actuator state data.
  /// @return Returns zero on success and non-zero on error.
  static Err_t oldGenSpeedProfile(Path_t *path, const VehicleState *vehState,
                                  const ActuatorState *actState, StateProblem_t stateProb);

  /// @brief Generate a vehicle trajectory.
  static Err_t oldGenTraj(const Path_t *path,
              const VehicleState *vehState, const ActuatorState *actState,
              CTraj *traj);
  */

private:

  //static StateProblem_t stateProb;
  //static Vel_params_t velParams;

  static GraphNode m_graphNodes[1000];
  
};

#endif /*VELPLANNER_HH_*/

