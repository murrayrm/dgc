#ifndef VELPLANNER_HH_
#define VELPLANNER_HH_

#include <assert.h>
#include <iostream>
#include <sstream>
#include <fstream>

#include <frames/pose3.h>
#include <frames/point2.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <alice/AliceConstants.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ActuatorState.h>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>
#include <trajutils/maneuver.h>
#include <trajutils/man2traj.hh>
#include <trajutils/traj.hh>
#include <temp-planner-interfaces/CmdArgs.hh>

/// @brief Vehicle kinematic properties.
struct VelPlannerKinematics
{
  /// Distance between front and rear wheels (m).
  double wheelBase;

  /// Maximum steering angle (degrees).  This is the largest angle
  /// that the lowel-level will command.  This value is also used to
  /// scale the measured steering position in ActuatorState (which is
  /// in the domain [-1, +1]) to a steering angle.
  double maxSteer;
};


/// @brief General speed limits.
struct VelPlannerSpeeds
{  
  /// Max speed for driving in lanes (m/s).
  double maxLaneSpeed;

  /// Max speed for taking turns (m/s).
  double maxTurnSpeed;

  /// Max speed for driving in close proximity to obstacles (m/s).
  double maxNearSpeed;

  /// Max speed for driving off-road (m/s).
  double maxOffRoadSpeed;

  /// Speed for crossing stop lines (m/s).
  double maxStopSpeed;

  /// Maximum acceleration (m/s/s).
  double maxAccel;

  /// Maximum deceleration (m/s/s).
  double maxDecel;

  /// Speed increment over the current speed (m/s).  This is a hacky way of generating
  /// a ramp-up from the current speed.
  double deltaSpeed;

  /// Stop line distance (m).  This is the desired distance between the stop
  /// lane and Alice's rear axel.
  double stopDist;

  /// Stop line tolerance (m).  This is the size of the stopping region in which
  /// Alice will plan to stop.
  double stopTol;

  /// Stop line stopping time (sec).
  double stopTime;

  /// Distance at which to start signalling turns.
  double signalDist;
};


class VelPlanner {

public: 

  static int init();
  static void destroy();
  static Err_t planVel(CTraj* traj, Path_t* path, StateProblem_t problem, VehicleState vehState, Vel_params_t params);
  static void display(int sendSubgroup, CTraj* traj);
  static void printMatlab(CTraj* traj);
private :
  static CMapElementTalker meTalker;

  // Vehicle kinematic properties
  static VelPlannerKinematics kin;

  // Speed limits
  static VelPlannerSpeeds speeds;
  
public:

  /// @brief Generate the speed profile.
  /// @param[in,out] path Planned path; this function fills out the speed array.
  /// @param[in] vehState Current vehicle state data.
  /// @param[in] actState Current actuator state data.
  /// @return Returns zero on success and non-zero on error.
  static Err_t genSpeedProfile(Path_t *path,
                      const VehicleState *vehState, const ActuatorState *actState);

  /// @brief Generate a vehicle trajectory.
  static Err_t genTraj(const Path_t *path,
              const VehicleState *vehState, const ActuatorState *actState,
              CTraj *traj);

private:

  static StateProblem_t stateProb;
  static Vel_params_t velParams;
  
};

#endif /*VELPLANNER_HH_*/

