#ifndef VELPLANNER_HH_
#define VELPLANNER_HH_

#include <frames/pose3.h>
#include <frames/point2.hh>

#include "temp-planner-interfaces/TrajPlanner.hh"
#include "temp-planner-interfaces/PlannerInterfaces.h"

#include <alice/AliceConstants.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ActuatorState.h>

#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>
#include <trajutils/traj.hh>

//#include "planner/CmdArgs.hh"

class VelPlanner {

public: 

  static int init();
  static void destroy();
  static Err_t planVel(CTraj* traj, Path_t* path, StateProblem_t problem, VehicleState vehState, Vel_params_t params);
  static void display(int sendSubgroup, CTraj* traj);
private :
  static TrajPlanner* trajPlanner;
  static CMapElementTalker meTalker;

};

#endif /*VELPLANNER_HH_*/




