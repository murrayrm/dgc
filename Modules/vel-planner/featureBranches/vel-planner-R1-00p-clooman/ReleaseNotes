              Release Notes for "vel-planner" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "vel-planner" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "vel-planner" module can be found in
the ChangeLog file.

Release R1-00p (Fri Aug 24 17:58:32 2007):
	Now populates dists array in sparse path.

Release R1-00o (Thu Aug 23 23:15:46 2007):
	Fixed bug that caused VelPlanner to segfault sometimes.

	Now makes sure to keep Alice stopped if she has stopped close 
	enough to stopline. Should prevent bunnyhopping.

	Improved handling of end of trajectory.

Release R1-00n (Thu Aug 23  4:13:47 2007):
	Fixed bug that often caused vel-planner to command big 
	accelerations after stoplines.

	Added speed limit in intersections. Currently set to 2 m/s.

Release R1-00m (Mon Aug 20 19:29:24 2007):
	Accounting for the distToStop field in node

Release R1-00l (Tue Aug 14 12:10:23 2007):
	Updated makefile to reflect changes in temp-planner-interfaces

Release R1-00k (Tue Aug 14  9:43:49 2007):
	Adjusted Makefile to work with the new console

Release R1-00j (Mon Aug 13  6:24:02 2007):
	Improved speed limitation based on distance to path.

	Now never gives completely unfeasible decelerations.

	Doesn't decrease speed based on unfeasible curvature in input path.

	Shows current speed in path graph.

	Sets reference speed below maximum allowed and replans with 
	deceleration from current speed if it comes too close to the maximum.

Release R1-00i (Mon Aug  6 23:46:09 2007):
	Limits speed based on distance to path.

	Handles getting input paths whrere the first node is a 
	null-pointer.

Release R1-00h (Mon Aug  6  3:57:44 2007):
	Now doesn't core dump when receiving paths of length 1.

	Looks for obstacles further ahead even though the speed is low. 
	This resolves a bug causing alice to come too close to obstacles.

	Moved a paranthes that caused the deceleration command to be 
	incorrect.

	Respects the speed limit in the mdf.

	Replans from current speed instead of old reference speed if the 
	speed error is too big. This decision will be made in follower 
	when the interface is set up.

Release R1-00g (Sat Aug  4  3:36:16 2007):
	Did a big restructuring. VelPlanner now densifies the beginning 
of its input path before calculating the speed profile.
	Added features such as speed limitation based on curvature and 
time needed to turn the wheels at changes of curvature.
	Currently does not use the speed limitation from the mdf due to 
problems reading it. VelPlanner limits the speed as it sees fit and has 
an upper limit of 15 m/s.

Release R1-00f (Mon Jul 30  8:02:09 2007):
	Using local coordinates now

Release R1-00e (Fri Jul 27 16:35:51 2007):
	Updated planner to account for changes in file locations 
(moved some files from planner to temp-planner-interfaces and 
trickling through the changes).
	Updated the stopping/separation distances.

Release R1-00d (Thu Jul 19 18:52:36 2007):
	Lowered maximum speed
	Slowing down at the end of a trajectory
	Using the PAUSE and STOP_OBS states
	Added separation distance when stopping in front of obstacles

Release R1-00c (Tue Jul 17 22:53:51 2007):
	Ported relevant graph-planner and trajplanner functions to the 
	vel planner.
	Added unit test.
	Added printMatlab function to print the traj in matlab (for now).
	Removed the state machine.
	Started interfacing with the logic-planner state problem.

Release R1-00b (Mon Jul 16 14:09:44 2007):
	Now using Graph as Graph_t

Release R1-00a (Fri Jul 13 16:35:10 2007):
	Initial implementation of the vel planner. Currently, this is 
only wrapper functions around selected graph-planner functions (which 
currently live in temp-planner-interfaces). This in indended to form a 
basis for the vel planner only.

Release R1-00 (Tue Jul 10 17:04:43 2007):
	Created.

















