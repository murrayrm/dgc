
/* 
 * Author: Who can tell?
*/

#include <math.h>
#include <alice/AliceConstants.h>
#include <trajutils/maneuver.h>
#include <trajutils/man2traj.hh>
#include <temp-planner-interfaces/Log.hh>
#include <temp-planner-interfaces/Console.hh>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <temp-planner-interfaces/AliceStateHelper.hh>
#include <temp-planner-interfaces/Utils.hh>
#include "dgcutils/cfgfile.h"
#include "dgcutils/DGCutils.hh"
#include "ConfigFile.hh"
#include "VelPlanner.hh"


// Maximum number of nodes in any path.
// This is a HACK; the code should also be checking for array overruns.
#define MAX_NODES 10000

CMapElementTalker VelPlanner::m_meTalker;
VelPlannerKinematics VelPlanner::m_kin;
VelPlannerStaticParams VelPlanner::m_params;
// StateProblem_t VelPlanner::stateProb;
// Vel_params_t VelPlanner::velParams;

static int newNodeCount = 0;  // Number of used nodes in m_graphNodes
static PlanGraphNode m_graphNodes[MAX_NODES];
static int m_sparseToDenseMap[MAX_NODES];

// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)

// Additional limits
#define UINT64_MAX	18446744073709551615ULL
#define LARGE_DBL         100000000

#define MAX_ZONE_INDEX (0)

// Useful macros
#ifndef MIN
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#endif
#ifndef MAX
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#endif
#define square(x) ((x)*(x))

 
int VelPlanner::init()
{
  // Set vehicle properties
  m_kin.wheelBase = VEHICLE_WHEELBASE;
  m_kin.maxSteer = VEHICLE_MAX_AVG_STEER; //30.0 * M_PI/180;

  // set parameters
  char* configFilePath = dgcFindConfigFile("vel-planner.config", "vel-planner");
  ConfigFile config(configFilePath);
  free(configFilePath);
  m_params.maxIntersectionSpeed = config.read<double>("maxIntersectionSpeed", 2.0);
  m_params.maxZoneSpeed = config.read<double>("maxZoneSpeed", 2.0);
  m_params.maxReverseSpeed = config.read<double>("maxReverseSpeed", 2.0);
  m_params.replanSpeedErr = config.read<double>("replanSpeedErr", 0.49);
  m_params.replanSpeedErrRail = config.read<double>("replanSpeedErrRail", 0.95);
  m_params.almostZeroSpeed = config.read<double>("almostZeroSpeed", 0.1);
  m_params.stopSpeed = config.read<double>("stopSpeed", 0.2);
  m_params.minStartSpeed = config.read<double>("minStartSpeed", 0.25);
  m_params.maxAccel = config.read<double>("maxAccel", 1.0);
  m_params.maxDecel = config.read<double>("maxDecel", -0.75);
  m_params.pauseDecel = config.read<double>("pauseDecel", -1.25);
  m_params.maxEmergencyDecel = config.read<double>("maxEmergencyDecel", -3.0);
  m_params.maxLatAccel = config.read<double>("maxLatAccel", 0.75);
  m_params.maxSteerRate = config.read<double>("maxSteerRate", 0.2);
  m_params.maxSteerRateRail = config.read<double>("maxSteerRateRail", 0.2);
  m_params.minSteerRateSpeedLimit = config.read<double>("minSteerRateSpeedLimit", 0.5);
  m_params.minSteerRateSpeedLimitRail = config.read<double>("minSteerRateSpeedLimitRail", 2.0);
  m_params.stopDist = config.read<double>("stopDist", 0.2);
  m_params.stopDistTol = config.read<double>("stopDistTol", 0.8);
  m_params.nearStopSpeed = config.read<double>("nearStopSpeed", 1.0);
  m_params.nearStopDist = config.read<double>("nearStopDist", 5.0);
  m_params.obsDist = config.read<double>("obsDist", 2.5);
  m_params.followDist = config.read<double>("followDist", 2.0);
  m_params.carDist = config.read<double>("carDist", 1.5);
  m_params.minCorrDist = config.read<double>("minCorrDist", 0.1);
  m_params.maxCorrDist = config.read<double>("maxCorrDist", 0.8);
  m_params.minCorrSpeed = config.read<double>("minCorrSpeed", 2.0);
  m_params.maxCorrSpeed = config.read<double>("maxCorrSpeed", 15.0);
  m_params.minCloseDist = config.read<double>("minCloseDist", 1.0);
  m_params.maxCloseDist = config.read<double>("maxCloseDist", 5.0);
  m_params.minCloseSpeed = config.read<double>("minCloseSpeed", 1.0);
  m_params.maxCloseSpeed = config.read<double>("maxCloseSpeed", 15.0);
  m_params.smoothAccelDist = config.read<double>("smoothAccelDist", 5.0);
  m_params.smoothMinSpeed = config.read<double>("smoothMinSpeed", 1.0);
  m_params.railCurvFiltLen = config.read<double>("railCurvFiltLen", 2.0);
  m_params.curvFiltLen = config.read<double>("curvFiltLen", 0.5);
  m_params.feasibilize = config.read<int>("feasibilize", 1);
  m_params.outputToMapViewer = config.read<int>("outputToMapViewer", 1);
  m_params.useBadReverse = config.read<int>("useBadReverse", 0);
  
  // Initialize the map element talker
  m_meTalker.initSendMapElement(CmdArgs::sn_key);

  return 0;
}

void VelPlanner::destroy()
{
  return;
}


Err_t VelPlanner::planVel(CTraj* traj, PlanGraph* graph, PlanGraphPath* sparsePathOrig, StateProblem_t stateProb,
                          VehicleState vehState, Vel_params_t velParams)
{
  PlanGraphPath sparsePathCopy;
  PlanGraphPath *sparsePath = &sparsePathCopy;
  static PlanGraphPath densePath;
  int mapLength;
  int startNode = 0;
  newNodeCount = 0;

  bool stopVehicle = !(sparsePathOrig != NULL && sparsePathOrig->pathLen > 1 && sparsePathOrig->nodes[0] != NULL);

  // Check if any of the node pointers is a NULL-pointer
  if (!stopVehicle) {
    for (int i = 0; i < sparsePathOrig->pathLen; i++) {
      if (sparsePathOrig->nodes[i] == NULL) {
        stopVehicle = true;
        Log::getStream(1) << "VelPlanner received corrupt path:" << endl
                          << "\tActual size: " << i << endl
                          << "\tpathLen: " << sparsePathOrig->pathLen << endl;
        break;
      }
    }
  }

  if (!stopVehicle) {
    if (((m_params.feasibilize) && (stateProb.planner == RAIL_PLANNER))
        || (sparsePathOrig->directions[0] == PLAN_GRAPH_PATH_REV
            && (stateProb.planner == RAIL_PLANNER || stateProb.planner == S1PLANNER || !m_params.useBadReverse))) {
      // Copy sparse path if doing feasibilization or reversing
      // NOTE: only copies what is actually used in vel-planner
      // NOTE: This is not beautiful, since it may have to be changed it if PlanGraphPath changes
      //sparsePathCopy.valid = sparsePathOrig->valid;
      sparsePathCopy.collideObs = sparsePathOrig->collideObs;
      sparsePathCopy.collideCar = sparsePathOrig->collideCar;
      sparsePathCopy.hasStop = sparsePathOrig->hasStop;
      sparsePathCopy.stopDistAlongPath = sparsePathOrig->stopDistAlongPath;
      sparsePathCopy.stopDistNode = sparsePathOrig->stopDistNode;
      sparsePathCopy.stopIndex = sparsePathOrig->stopIndex;
      //sparsePathCopy.hasRow = sparsePathOrig->hasRow;
      //sparsePathCopy.rowWaypoint = sparsePathOrig->rowWaypoint;
      //sparsePathCopy.stopWaypoint = sparsePathOrig->stopWaypoint;
      sparsePathCopy.dist = sparsePathOrig->dist;
      sparsePathCopy.pathLen = sparsePathOrig->pathLen;
      memcpy(sparsePathCopy.nodes, sparsePathOrig->nodes, sparsePathOrig->pathLen * sizeof(PlanGraphNode*));
      //memcpy(sparsePathCopy.directions, sparsePathOrig->directions,
      //       sparsePathOrig->pathLen * sizeof(PlanGraphPathDirection));
      sparsePathCopy.directions[0] = sparsePathOrig->directions[0];
    } else {
      sparsePath = sparsePathOrig;
    }

    calcDists(sparsePath);

    // Check that no two adjacent nodes are on top of eachother
    for (int i = 1; i < sparsePath->pathLen; i++) {
      if (sparsePath->dists[i] == sparsePath->dists[i-1]) {
        stopVehicle = true;
        Log::getStream(1) << "VelPlanner received corrupt path:" << endl
                          << "\tTwo nodes on top of eachother" << endl
                          << "\tPath indeces: " << i-1 << ", " << i << endl
                          << "\tGraph indeces: " << sparsePath->nodes[i-1]->nodeId << ", "
                          << sparsePath->nodes[i-1]->nodeId << endl
                          << "\tnorth: " << sparsePath->nodes[i]->pose.pos.x
                          << " east: " << sparsePath->nodes[i]->pose.pos.y << endl;
        break;
      }
    }

  }    

  if (!stopVehicle){
    if ((m_params.feasibilize) && (stateProb.planner == RAIL_PLANNER)) {
      smoothPath(sparsePath);
    }

    if (sparsePathOrig->directions[0] == PLAN_GRAPH_PATH_REV
        && (stateProb.planner == RAIL_PLANNER || stateProb.planner == S1PLANNER || !m_params.useBadReverse)) {
      // Going in reverse
      // Replace all the nodes in the path with copies and add M_PI to the headings
      for (int i = 0; i < sparsePath->pathLen; i++) {
        m_graphNodes[newNodeCount] = *sparsePath->nodes[i];
        sparsePath->nodes[i] = &m_graphNodes[newNodeCount];
        sparsePath->nodes[i]->pose.rot += M_PI;
        newNodeCount++;
      }
    }

    fprintf(stderr, "densify\n");
    densifyPath(sparsePath, &densePath, m_sparseToDenseMap, &mapLength, vehState, traj);
    fprintf(stderr, "curves\n");
    calculateCurvatures(sparsePath, &densePath, m_sparseToDenseMap, mapLength, &stateProb);
    fprintf(stderr, "speeds\n");
    genSpeedProfile(graph, sparsePath, &densePath, vehState, m_sparseToDenseMap, mapLength,
                    velParams, traj, &stateProb, &startNode);
    fprintf(stderr, "traj\n");
    genTraj(&densePath, sparsePath->directions[0], traj);
    // plotPathDists(sparsePath, &densePath, sparseToDenseMap, -14);
  }
  if (stopVehicle) {
    densePath.pathLen = 0;  // FIXME: Check that old path isn't reused
    /// This will cause follower to stop the car
    point2 currPos;
    currPos.set(AliceStateHelper::getPositionRearAxle(vehState));
    traj->startDataInput();
    traj->addPoint(currPos.x, 0, 0, currPos.y, 0, 0);
    traj->setDirection(1);
  }

  if (m_params.outputToMapViewer || CmdArgs::visualization_level >= 2) {
    //oldGenSpeedProfile(sparsePath, &vehState, &actState, stateProb);
    //oldGenTraj(sparsePath, &vehState, &actState, traj);
    
    //plotPath(sparsePath, -15);
    //plotTraj(traj, -15);
    plotPath(&densePath, startNode, -15);
    plotSpeed(AliceStateHelper::getVelocityMag(vehState), -15);
    //plotPaths(&densePath, sparsePath, -14);
    //plotMap(sparseToDenseMap, mapLength, -14);
  }

  return VP_OK;
}


void VelPlanner::plotPath(PlanGraphPath *path, int startNode, int sendSubGroup)
{
  MapElement me;
  point2 point;
  vector<point2> points;

  // Clear plot if there is no path
  if (path->pathLen <= 1 || path->nodes[0] == NULL) {
    for (int i = 0; i < 8; i++) {
      me.setId(i);
      me.setTypeClear();
      m_meTalker.sendMapElement(&me, sendSubGroup);
    }
    return;
  }

  /*// Steering angle commands
  for (int i = 0; i < path->pathLen; i++) {
    point.set((double)i, 10 * path->path[i]->steerAngle);
    points.push_back(point);
    }*/
  // Dists
  /*for (int i = 0; i < path->pathLen; i++) {
    point.set((double)i, path->dists[i]);
    points.push_back(point);
  }
  me.setColor(MAP_COLOR_ORANGE);
  me.setGeometry(points);
  me.setId(17);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup+1);
  me.setId(18);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup+1);*/

  // IDs
  /*for (int i = 0; i < path->pathLen; i++) {
    point.set((double)i, path->path[i]->index);
    points.push_back(point);
  }
  me.setColor(MAP_COLOR_MAGENTA);
  me.setGeometry(points);
  me.setId(31);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup+1);
  me.setId(32);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup+1);*/

  // Vehicle headings
  points.clear();
  for (int i = 0; i < path->pathLen; i++) {
    point.set(path->dists[i]-path->dists[startNode], 10 * (fmod(path->nodes[i]->pose.rot/M_PI + 1, 2) - 1));
    points.push_back(point);
  }
  me.setColor(MAP_COLOR_GREEN);
  me.setGeometry(points);
  me.setId(0);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  me.setId(1);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);

  // Curvatures
  points.clear();
  for (int i = 0; i < path->pathLen; i++) {
    point.set(path->dists[i]-path->dists[startNode], 10 / VEHICLE_MAX_AVG_STEER * atan(VEHICLE_WHEELBASE * path->curvatures[i]));
    points.push_back(point);
  }
  me.setColor(MAP_COLOR_RED);
  me.setGeometry(points);
  me.setId(2);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  me.setId(3);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);

  // Speeds
  points.clear();
  for (int i = 0; i < path->pathLen; i++) {
    point.set(path->dists[i]-path->dists[startNode], path->speeds[i]);
    points.push_back(point);
  }
  me.setColor(MAP_COLOR_CYAN);
  me.setGeometry(points);
  me.setId(4);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  me.setId(5);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);

  // Accelerations
  points.clear();
  for (int i = 0; i < path->pathLen; i++) {
    point.set(path->dists[i]-path->dists[startNode], 10 * path->accelerations[i]);
    points.push_back(point);
  }
  me.setColor(MAP_COLOR_BLUE);
  me.setGeometry(points);
  me.setId(6);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  me.setId(7);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);

}

void VelPlanner::plotSpeed(double speed, int sendSubGroup)
{
  MapElement me;
  point2 point;

  point.set(0.0, speed);
  me.setColor(MAP_COLOR_ORANGE);
  me.setGeometry(point);
  me.setId(72);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);
}

void VelPlanner::plotPaths(PlanGraphPath *path_1, PlanGraphPath *path_2, int sendSubGroup)
{
  MapElement me;
  point2 point;
  vector<point2> points;

  // Path 1
  for (int i = 0; i < path_1->pathLen; i++) {
    point.set(path_1->nodes[i]->pose.pos.x, path_1->nodes[i]->pose.pos.y);
    points.push_back(point);
  }
  me.setColor(MAP_COLOR_BLUE);
  me.setGeometry(points);
  me.setId(0);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  me.setId(1);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);

  // Path 2
  points.clear();
  for (int i = 0; i < path_2->pathLen; i++) {
    point.set(path_2->nodes[i]->pose.pos.x, path_2->nodes[i]->pose.pos.y);
    points.push_back(point);
  }
  me.setColor(MAP_COLOR_ORANGE);
  me.setGeometry(points);
  me.setId(2);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  /*me.setId(3);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);*/
}


void VelPlanner::plotTraj(CTraj *traj, int sendSubGroup)
{
  MapElement me;
  point2 point;
  vector<point2> points;

  // Steering commands
  for (int i = 0; i < traj->getNumPoints(); i++) {
    TrajPoint trajPoint = traj->interpolGetPoint(i, 0.0);
    double steerCommand = atan(VEHICLE_WHEELBASE * (trajPoint.nd*trajPoint.edd - trajPoint.ed*trajPoint.ndd) /
                               pow(trajPoint.nd*trajPoint.nd + trajPoint.ed*trajPoint.ed, 1.5)) / VEHICLE_MAX_AVG_STEER;
    point.set((double)i*0.05, 10 * steerCommand);
    points.push_back(point);
  }
  me.setColor(MAP_COLOR_MAGENTA);
  me.setGeometry(points);
  me.setId(100);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  me.setId(101);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);
}

void VelPlanner::plotMap(int *map, int length, int sendSubGroup)
{
  MapElement me;
  point2 point;
  vector<point2> points;

  for (int i = 0; i < length; i++) {
    point.set(i, map[i]);
    points.push_back(point);
  }
  me.setColor(MAP_COLOR_ORANGE);
  me.setGeometry(points);
  me.setId(21);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  me.setId(22);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);

}

void VelPlanner::plotPathDists(PlanGraphPath *sparsePath, PlanGraphPath *densePath, int *sparseToDenseMap, int sendSubGroup)
{
  MapElement me;
  point2 point;
  vector<point2> sparseVec;
  vector<point2> denseVec;

  // Dists
  for (int i = 0; i < sparsePath->pathLen; i++) {
    point.set((double)i, sparsePath->dists[i]);
    sparseVec.push_back(point);
    point.set((double)i, densePath->dists[sparseToDenseMap[i]]);
    denseVec.push_back(point);
  }

  me.setColor(MAP_COLOR_ORANGE);
  me.setGeometry(sparseVec);
  me.setId(17);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  me.setId(18);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);

  me.setColor(MAP_COLOR_GREEN);
  me.setGeometry(denseVec);
  me.setId(19);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  me.setId(20);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);
}



Err_t VelPlanner::calcDists(PlanGraphPath *path)
{
  path->dists[0] = 0.0;

  for (int i = 1; i < path->pathLen; i++) {
    double dist = sqrt(square(path->nodes[i]->pose.pos.x - path->nodes[i - 1]->pose.pos.x)
                       + square(path->nodes[i]->pose.pos.y - path->nodes[i - 1]->pose.pos.y));
    path->dists[i] = path->dists[i - 1] + dist;
  }
  return VP_OK;
}


Err_t VelPlanner::smoothPath(PlanGraphPath *path)
{
  // Keep these values, they are used to make computations efficient.
  const int RIGHT = 1;
  const int LEFT = -1;

  const double TURNING_RADIUS = VEHICLE_MIN_TURNING_RADIUS;
  const double OVERSHOOT_TOL = 0.1;
  const double FIRST_NODE_DIST = 30.0;
  const double FILTER_LENGTH = 2 * TURNING_RADIUS;
  //const double FIX_DIST = TURNING_RADIUS * M_PI / 2;
  const int MAX_MOVED_NODES = 200;

  static PlanGraphNode movedNodes[MAX_MOVED_NODES];
  static int nMovedNodes = 0;
  static int filterEvents[MAX_MOVED_NODES];
  static int nFilterEvents = 0;

  int debug[20][20];
  memset(debug, 0, sizeof(debug));


  ////////////////////////////////////////////////////////////
  // Check whether to transfer nodes from previous filtering /
  ////////////////////////////////////////////////////////////

  {
    int firstNodeToTransfer = -1;
    int lastNodeToTransfer = -1;
    bool transfer = false;
    
    // Check if first node in current path was moved in previuos cycle.
    // Transfer moved nodes only in that case.
    for (int i = 0; i < nMovedNodes; i++) {
      if (movedNodes[i].nodeId == path->nodes[0]->nodeId) {
        firstNodeToTransfer = i;
        transfer = true;
        break;
      }
    }

    if (transfer) {
      // Find last node to transfer
      transfer = false;
      for (int i = 0; i < nFilterEvents; i++) {
        if (filterEvents[i] <= firstNodeToTransfer
            && filterEvents[i + 1] > firstNodeToTransfer) {
          lastNodeToTransfer = filterEvents[i + 1] - 1;
          transfer = true;
          break;
        }
      }
    }

    if (transfer) {
      // Only transfer if all nodes in the filter event are still in the path
      // Don't transfer zone nodes
      for (int i = firstNodeToTransfer; i <= lastNodeToTransfer; i++) {
        if (movedNodes[i].nodeId != path->nodes[i - firstNodeToTransfer]->nodeId
            /*|| movedNodes[i].nodeId <= MAX_ZONE_INDEX*/) {
          transfer = false;
          break;
        }
      }
    }
    
    // Now, set up moved nodes arrays for the rest of the filtering
    if (transfer) {
      for (int i = firstNodeToTransfer; i <= lastNodeToTransfer; i++) {
        movedNodes[i - firstNodeToTransfer] = movedNodes[i];
        path->nodes[i] = &movedNodes[i];
      }
      nMovedNodes = lastNodeToTransfer - firstNodeToTransfer + 1;
      nFilterEvents = 1;
      filterEvents[0] = 0;
      filterEvents[1] = nMovedNodes;
    } else {
      nMovedNodes = 0;
      nFilterEvents = 0;
    }
  }
  

  ////////////////////////////////////////////////////////////
  // Filter the path                                         /
  ////////////////////////////////////////////////////////////
  for (int i = 0; path->dists[i] <= FIRST_NODE_DIST && i < path->pathLen; i++) {
    double x = path->nodes[i]->pose.pos.x;
    double y = path->nodes[i]->pose.pos.y;
    double yawOfFirstNode = path->nodes[i]->pose.rot;

    double rightCenterX = x - sin(yawOfFirstNode)*TURNING_RADIUS;
    double rightCenterY = y + cos(yawOfFirstNode)*TURNING_RADIUS;
    double leftCenterX = x + sin(yawOfFirstNode)*TURNING_RADIUS;
    double leftCenterY = y - cos(yawOfFirstNode)*TURNING_RADIUS;

    int worstNode = -1;
    double worstDistance = TURNING_RADIUS - OVERSHOOT_TOL;
    int worstNodeSide = 0;
    double yawOfWorstNode = 0;

    for (int j = i + 1; path->dists[j] - path->dists[i] < FILTER_LENGTH && j < path->pathLen; j++) {
      // Compute distances of node to centers of followable circles
      double rightDist = sqrt(square(path->nodes[j]->pose.pos.x - rightCenterX) + square(path->nodes[j]->pose.pos.y - rightCenterY));
      double leftDist = sqrt(square(path->nodes[j]->pose.pos.x - leftCenterX) + square(path->nodes[j]->pose.pos.y - leftCenterY));

      debug[i][j] = 1;

      // Check if node is "more unreachable" than any other examined node.
      if (rightDist < worstDistance || leftDist < worstDistance) {
        debug[i][j] = -1;
        
        // Make sure the two nodes don't create an S-shape.
        double dirToOtherNode = atan2(path->nodes[j]->pose.pos.y - y, path->nodes[j]->pose.pos.x - x);
        double yawOfOtherNode = path->nodes[j]->pose.rot;

        double modDirToOther = fmod(dirToOtherNode - yawOfFirstNode + 31*M_PI, 2*M_PI) - M_PI;
        double modYawDiff = fmod(yawOfOtherNode - yawOfFirstNode + 31*M_PI, 2*M_PI) - M_PI;

        if (modYawDiff > modDirToOther && modDirToOther > 0 || modYawDiff < modDirToOther && modDirToOther < 0) {
          debug[i][j] = -2;

          // The nodes don't create an S-shape, use it.
          worstNode = j;
          yawOfWorstNode = yawOfOtherNode;
          if (modDirToOther > 0) {
            worstDistance = rightDist;
            worstNodeSide = RIGHT;
          } else {
            worstDistance = leftDist;
            worstNodeSide = LEFT;
          }
        }
      }
    }

    /*// Don't move any nodes if one of the candidates is a zone-node
    for (int j = i; j <= worstNode; j++) {
      if (path->nodes[j]->index <= MAX_ZONE_INDEX) {
        worstNode = -1;
      }
      }*/
    
    if (worstNode >= 0) {
      // Nodes to be moved were found
      filterEvents[nFilterEvents++] = nMovedNodes;
      
      // Determine the center of the circle to put the filtered nodes on.
      // It should be on the intersection of lines parallel with firstNode and worstNode shifted TURNING_RADIUS to the right or left.
      // First node line: a*x + b*y = e
      // First node line: c*x + d*y = f
      //
      // |x|       1     | d  -b| |e|
      // | | = --------- |      |*| |
      // |y|   a*d - b*c |-c   a| |f|

      double a = -sin(yawOfFirstNode);
      double b =  cos(yawOfFirstNode);
      double c = -sin(yawOfWorstNode);
      double d =  cos(yawOfWorstNode);
      double e = -sin(yawOfFirstNode)*(x - sin(yawOfFirstNode)*TURNING_RADIUS*worstNodeSide)
        + cos(yawOfFirstNode)*(y + cos(yawOfFirstNode)*TURNING_RADIUS*worstNodeSide);
      double f = -sin(yawOfWorstNode)*(path->nodes[worstNode]->pose.pos.x
                                       - sin(yawOfWorstNode)*TURNING_RADIUS*worstNodeSide)
        + cos(yawOfWorstNode)*(path->nodes[worstNode]->pose.pos.y
                               + cos(yawOfWorstNode)*TURNING_RADIUS*worstNodeSide);
      
      double centerX = (d*e - b*f)/(a*d - b*c);
      double centerY = (-c*e + a*f)/(a*d - b*c);

      if (m_params.outputToMapViewer || CmdArgs::visualization_level >= 3) {
        MapElement me;
        point2 point;
        //vector<point2> points;
        
        // Center point
        point.set(centerX, centerY);
        //points.push_back(point);
        me.setColor(MAP_COLOR_MAGENTA);
        me.setGeometry(point);
        me.setId(74390);
        me.setTypePoints();
        m_meTalker.sendMapElement(&me, -2);
      }

      double angleSpan = fmod(yawOfWorstNode - yawOfFirstNode + 31*M_PI, 2*M_PI) - M_PI;

      // Move nodes
      for (int j = i; j <= worstNode; j++) {
        double nodeDir = atan2(path->nodes[j]->pose.pos.y - centerY, path->nodes[j]->pose.pos.x - centerX);
        double dirRelToStart = fmod(nodeDir + worstNodeSide*M_PI/2.0 - yawOfFirstNode + 31*M_PI, 2*M_PI) - M_PI;
        if ((worstNodeSide > 0 && dirRelToStart > 0 && dirRelToStart < angleSpan
             || worstNodeSide < 0 && dirRelToStart < 0 && dirRelToStart > angleSpan)
            && nMovedNodes < MAX_MOVED_NODES) {
          // Move node
          movedNodes[nMovedNodes] = *path->nodes[j];
          path->nodes[j] = &movedNodes[nMovedNodes];
          movedNodes[nMovedNodes].pose.pos.x = centerX + cos(nodeDir)*TURNING_RADIUS;
          movedNodes[nMovedNodes].pose.pos.y = centerY + sin(nodeDir)*TURNING_RADIUS;
          movedNodes[nMovedNodes].pose.rot = nodeDir + worstNodeSide*M_PI/2.0;
          nMovedNodes++;
        }
      }
      filterEvents[nFilterEvents] = nMovedNodes;
    }
  }
  

  // Show debug information in mapviewer
  if (m_params.outputToMapViewer || CmdArgs::visualization_level >= 3) {
    MapElement me;
    point2 point;
    vector<point2> points;
    
    // Feasible
    for (int i = 0; i < 20; i++) {
      for (int j = 0; j < 20; j++) {
        if (debug[i][j] == 1) {
          point.set(i, j);
          points.push_back(point);
        }
      }
    }
    me.setColor(MAP_COLOR_GREEN);
    me.setGeometry(points);
    me.setId(0);
    me.setTypeLine();
    m_meTalker.sendMapElement(&me, -17);
    me.setId(1);
    me.setTypePoints();
    m_meTalker.sendMapElement(&me, -17);
    
    // S-points
    points.clear();
    for (int i = 0; i < 20; i++) {
      for (int j = 0; j < 20; j++) {
        if (debug[i][j] == -1) {
          point.set(i, j);
          points.push_back(point);
        }
      }
    }
    me.setColor(MAP_COLOR_PINK);
    me.setGeometry(points);
    me.setId(2);
    me.setTypeLine();
    m_meTalker.sendMapElement(&me, -17);
    me.setId(3);
    me.setTypePoints();
    m_meTalker.sendMapElement(&me, -17);
    
    // Moved points
    points.clear();
    for (int i = 0; i < 20; i++) {
      for (int j = 0; j < 20; j++) {
        if (debug[i][j] == -2) {
          point.set(i, j);
          points.push_back(point);
        }
      }
    }
    me.setColor(MAP_COLOR_RED);
    me.setGeometry(points);
    me.setId(4);
    me.setTypeLine();
    m_meTalker.sendMapElement(&me, -17);
    me.setId(5);
    me.setTypePoints();
    m_meTalker.sendMapElement(&me, -17);
    
  }



  return VP_OK;
}



Err_t VelPlanner::densifyPath(const PlanGraphPath *sparsePath, PlanGraphPath *densePath,
                              int *sparseToDenseMap, int *mapLength,
                              VehicleState vehState, CTraj *traj)
{
  Pose2D poseA, poseB;
  PlanGraphNode *nodeA, *nodeB;
  Maneuver *maneuver;
  double startSpeed;

  // Create vehicle model
  Vehicle vehicle;
  vehicle.max_curvature = tan(m_kin.maxSteer)/m_kin.wheelBase;
  vehicle.wheelbase = m_kin.wheelBase;
  vehicle.steerlimit = m_kin.maxSteer;

  // Get reference speed of beginning of this
  // path projected onto old traj.
  {
    TrajPoint trajPoint = traj->interpolGetClosest(sparsePath->nodes[0]->pose.pos.x,
                                                   sparsePath->nodes[0]->pose.pos.y,
                                                   NULL, NULL);
    startSpeed = sqrt(trajPoint.nd * trajPoint.nd + trajPoint.ed * trajPoint.ed);
  }
  
  // Current speed of Alice
  double curSpeed = AliceStateHelper::getVelocityMag(vehState);
  
  // Speed to usen when deciding how far to plan
  double planSpeed = MAX(curSpeed, startSpeed);

  // Don't densify the path longer than the feed forward will look ahead
  double denseDist = planSpeed*1.0 + 3.0;

  // Don't plan furhter than the smallest distance in which Alice can stop
  /*double planDist = planSpeed*planSpeed / (-m_params.maxDecel) / 2.0 + 3.0;
  if (planDist < denseDist) {
    planDist = denseDist;
    }*/
  // For now, make planDist really big so planning uses the entire sparsePath
  double planDist = 1000;

  // Spacing between two points in densified path [m]
  double stepSize = 0.2;

  densePath->dists[0] = 0;
  int sparseIndex = 0;
  int denseIndex = 0;
  sparseToDenseMap[sparseIndex] = denseIndex;
  densePath->nodes[denseIndex] = sparsePath->nodes[sparseIndex];
  sparseIndex++;
  denseIndex++;
  while ((sparseIndex <= 1 || densePath->dists[denseIndex - 1] < denseDist + densePath->dists[sparseToDenseMap[1]])
         && sparseIndex < sparsePath->pathLen) {
    // Nodes to generate maneuver between
    nodeA = sparsePath->nodes[sparseIndex - 1];
    nodeB = sparsePath->nodes[sparseIndex];

    //// Start and end poses of maneuver
    // For non-vehicle nodes, use the node pose and assume zero-steering angle
    poseA.x = nodeA->pose.pos.x;
    poseA.y = nodeA->pose.pos.y;
    poseA.theta = nodeA->pose.rot;
    // Final pose on node B
    poseB.x = nodeB->pose.pos.x;
    poseB.y = nodeB->pose.pos.y;
    poseB.theta = nodeB->pose.rot;
    
    // Approximated length of curren maneuver
    double maneuverLength = sqrt((poseB.x-poseA.x)*(poseB.x-poseA.x) + (poseB.y-poseA.y)*(poseB.y-poseA.y));

    // Number of sampling points on this maneuver
    int numSteps = (int) ceil(maneuverLength / stepSize);

    // Generate maneuver
    maneuver = maneuver_pose2pose(&vehicle, &poseA, &poseB);
    for (int i = 1; i < numSteps; i++) {
      PlanGraphNode *newNode = &m_graphNodes[newNodeCount++];
      Pose2D pose = maneuver_evaluate_pose(&vehicle, maneuver, ((double)i)/numSteps);
      newNode->pose.pos.x = pose.x;
      newNode->pose.pos.y = pose.y;
      newNode->pose.rot = pose.theta;
      densePath->nodes[denseIndex] = newNode;
      densePath->dists[denseIndex] = densePath->dists[denseIndex - 1]
        + sqrt((densePath->nodes[denseIndex]->pose.pos.x
                - densePath->nodes[denseIndex - 1]->pose.pos.x)
               * (densePath->nodes[denseIndex]->pose.pos.x
                  - densePath->nodes[denseIndex - 1]->pose.pos.x)
               + (densePath->nodes[denseIndex]->pose.pos.y
                  - densePath->nodes[denseIndex - 1]->pose.pos.y)
               * (densePath->nodes[denseIndex]->pose.pos.y
                  - densePath->nodes[denseIndex - 1]->pose.pos.y));
      denseIndex++;
    }
    maneuver_free(maneuver);
    sparseToDenseMap[sparseIndex] = denseIndex;
    densePath->nodes[denseIndex] = sparsePath->nodes[sparseIndex];
    densePath->dists[denseIndex] = densePath->dists[denseIndex - 1]
      + sqrt((densePath->nodes[denseIndex]->pose.pos.x
              - densePath->nodes[denseIndex - 1]->pose.pos.x)
             * (densePath->nodes[denseIndex]->pose.pos.x
                - densePath->nodes[denseIndex - 1]->pose.pos.x)
             + (densePath->nodes[denseIndex]->pose.pos.y
                - densePath->nodes[denseIndex - 1]->pose.pos.y)
             * (densePath->nodes[denseIndex]->pose.pos.y
                - densePath->nodes[denseIndex - 1]->pose.pos.y));
    denseIndex++;

    sparseIndex++;
  }
  
  // Copy the non-densified part of the path
  while (densePath->dists[denseIndex - 1] < planDist && sparseIndex < sparsePath->pathLen) {
    sparseToDenseMap[sparseIndex] = denseIndex;
    densePath->nodes[denseIndex] = sparsePath->nodes[sparseIndex];
    densePath->dists[denseIndex] = densePath->dists[denseIndex - 1]
      + sqrt((densePath->nodes[denseIndex]->pose.pos.x
              - densePath->nodes[denseIndex - 1]->pose.pos.x)
             * (densePath->nodes[denseIndex]->pose.pos.x
                - densePath->nodes[denseIndex - 1]->pose.pos.x)
             + (densePath->nodes[denseIndex]->pose.pos.y
                - densePath->nodes[denseIndex - 1]->pose.pos.y)
             * (densePath->nodes[denseIndex]->pose.pos.y
                - densePath->nodes[denseIndex - 1]->pose.pos.y));
    denseIndex++;
    sparseIndex++;
  }

  if (sparseIndex < sparsePath->pathLen) {
    sparseToDenseMap[sparseIndex] = denseIndex;
    densePath->nodes[denseIndex] = sparsePath->nodes[sparseIndex];
    densePath->dists[denseIndex] = densePath->dists[denseIndex - 1]
      + sqrt((densePath->nodes[denseIndex]->pose.pos.x
              - densePath->nodes[denseIndex - 1]->pose.pos.x)
             * (densePath->nodes[denseIndex]->pose.pos.x
                - densePath->nodes[denseIndex - 1]->pose.pos.x)
             + (densePath->nodes[denseIndex]->pose.pos.y
                - densePath->nodes[denseIndex - 1]->pose.pos.y)
             * (densePath->nodes[denseIndex]->pose.pos.y
                - densePath->nodes[denseIndex - 1]->pose.pos.y));
    denseIndex++;
    sparseIndex++;
  }
  
  *mapLength = sparseIndex;
  densePath->pathLen = denseIndex;
  
  return VP_OK;
}

Err_t VelPlanner::calculateCurvatures(PlanGraphPath* sparsePath, PlanGraphPath* densePath,
                                      int* sparseToDenseMap, int mapLength, StateProblem_t* stateProb)
{
  // Anlges of lines between nodes
  float prevEdgeAng, nextEdgeAng;
  float curX, curY, nextX, nextY;
  float distToPrev, distToNext;
  float curYaw;
  float prevCurv, nextCurv;
  int i;
  PlanGraphPath* path;
  
  if (stateProb->planner == RAIL_PLANNER) {
    // If using rail planner, calculate curvatures in
    // sparsePath and make linear interpolation,
    // since the paths it creates have so bad headings.
    path = sparsePath;
  } else {
    path = densePath;
  }

  // Allocate sufficient space for curvatures
  float *curvatures = new float[path->pathLen];
      
  curX = path->nodes[0]->pose.pos.x;
  curY = path->nodes[0]->pose.pos.y;
  nextX = path->nodes[1]->pose.pos.x;
  nextY = path->nodes[1]->pose.pos.y;

  distToNext = path->dists[1] - path->dists[0];

  curYaw = path->nodes[0]->pose.rot;
    
  nextEdgeAng = atan2(nextY - curY, nextX - curX);

  nextCurv = 2.0 * sin(nextEdgeAng - curYaw) / distToNext;

  curvatures[0] = nextCurv;

  for (i = 1; i < path->pathLen - 1; i++) {
    curX = nextX;
    curY = nextY;
    prevEdgeAng = nextEdgeAng;
    distToPrev = distToNext;

    nextX = path->nodes[i + 1]->pose.pos.x;
    nextY = path->nodes[i + 1]->pose.pos.y;

    distToNext = path->dists[i + 1] - path->dists[i];
    
    curYaw = path->nodes[i]->pose.rot;
    
    nextEdgeAng = atan2(nextY - curY, nextX - curX);
    
    prevCurv = 2.0 * sin(curYaw - prevEdgeAng) / distToPrev;
    nextCurv = 2.0 * sin(nextEdgeAng - curYaw) / distToNext;

    //curvatures[i] = fabs(prevCurv) > fabs(nextCurv) ? prevCurv : nextCurv;
    curvatures[i] = (prevCurv + nextCurv) / 2.0;
  }
  prevEdgeAng = nextEdgeAng;
  distToPrev = distToNext;
  curYaw = path->nodes[i]->pose.rot;
  prevCurv = 2.0 * sin(curYaw - prevEdgeAng) / distToPrev;
  curvatures[i] = prevCurv;

  /*Log::getStream(0) << "Curvatures: ";
  for (int j = 0; j < path->pathLen; j++) {
    Log::getStream(0) << curvatures[j] << ", ";
    }*/

  // If using rail planner, simply make linerar interpolation
  // between the sparse path points, since the input is so crappy.
  if (stateProb->planner == RAIL_PLANNER) {
    static int firstNodeIndex = -1;
    static float firstNodeCurv;
    static int secondNodeIndex = -1;
    static float secondNodeCurv;

    // TODO fix index versus nodeId
    // Remeber previously calculated curvatures so the
    // position of the previous node (that we no longer get) can be used.
    if ((int)(sparsePath->nodes[0]->nodeId) == firstNodeIndex) {
      curvatures[0] = firstNodeCurv;
    } else if ((int)(sparsePath->nodes[0]->nodeId) == secondNodeIndex) {
      firstNodeIndex = secondNodeIndex;
      firstNodeCurv = secondNodeCurv;
      curvatures[0] = firstNodeCurv;
    }
    secondNodeIndex = sparsePath->nodes[1]->nodeId;
    secondNodeCurv = curvatures[1];

    for (int j = 0; j < mapLength - 1; j++) {
      float dist = densePath->dists[sparseToDenseMap[j+1]] - densePath->dists[sparseToDenseMap[j]];
      float startCurv = curvatures[j];
      float curvDiff = curvatures[j+1] - startCurv;
      for (int k = sparseToDenseMap[j]; k < sparseToDenseMap[j+1]; k++) {
        densePath->curvatures[k] = startCurv
          + curvDiff*(densePath->dists[k] - densePath->dists[sparseToDenseMap[j]])/dist;
      }
    }
    densePath->curvatures[sparseToDenseMap[mapLength-1]] = curvatures[mapLength-1];

    // Low-pass filter curvatures. Do it causally to make it 
    // continuous even when the path is shortened in steps behind Alice.
    for (int j = 0; densePath->dists[j] < 30.0 && j < densePath->pathLen; j++) {
      double sum = 0.0;
      int n = 0;
      for (int k = j;
           densePath->dists[k] - densePath->dists[j] < m_params.railCurvFiltLen
             && k < densePath->pathLen;
           k++) {
        sum += densePath->curvatures[k];
        n++;
      }
      densePath->curvatures[j] = sum / n;
    }
  } else {
    
    // Smooth curvatures and transfer values to densePath
    for (i = 0; i < densePath->pathLen; i++) {
      const float avgDist = m_params.curvFiltLen;
      int iMin, iMax;
      float sum = 0;
      for (iMax = i;
           iMax < densePath->pathLen && (densePath->dists[iMax] - densePath->dists[i]) <= avgDist;
           iMax++) {
        sum += curvatures[iMax];
      }
      for (iMin = i - 1;
           iMin >= 0 && (densePath->dists[i] - densePath->dists[iMin]) < avgDist;
           iMin--) {
        sum += curvatures[iMin];
      }
      densePath->curvatures[i] = sum / (iMax - iMin - 1);
      
    }
  }

  // Clean up
  delete [] curvatures;

  return VP_OK;
}


Err_t VelPlanner::genSpeedProfile(PlanGraph* graph, const PlanGraphPath *sparsePath, PlanGraphPath *densePath,
                                  VehicleState vehState, const int *sparseToDenseMap, int mapLength,
                                  Vel_params_t velParams, CTraj *traj, StateProblem_t *stateProb, int *returnStartNode)
{
  // Index of node on densePath closest to Alice's rear axle
  int closestNode;
  // Index of closest node on densePath behind Alice
  int startNode;
  // Reference speed at startNode
  double startSpeed;

  // Calculate current speed
  //  double curSpeed = sqrt(vehState->utmNorthVel*vehState->utmNorthVel + vehState->utmEastVel*vehState->utmEastVel);
  double curSpeed = AliceStateHelper::getVelocityMag(vehState);
  // get the current pos from vehstate
  point2 currPos;
  currPos.set(AliceStateHelper::getPositionRearAxle(vehState));
  // get current heading
  double currYaw = AliceStateHelper::getHeading(vehState);

  // Set maxSpeed of the path
  double maxSpeed = 0.9 * velParams.maxSpeed;
  // Reverse speed limit
  if (sparsePath->directions[0] != PLAN_GRAPH_PATH_FWD) {
    maxSpeed = MIN(maxSpeed, m_params.maxReverseSpeed);
  }
  // Zone speed limit
  if (sparsePath->nodes[0]->flags.isZone) {
    maxSpeed = MIN(maxSpeed, m_params.maxZoneSpeed);
  }


  // Limit speed based on lateral error.
  // Ramp down maximum allowed speed linearly
  // from m_params.maxCorrSpeed when the error is <= m_params.minCorrDist
  // to m_params.minCorrSpeed when the error is >= m_params.maxCorrDist.
  double distToPathSquared = 1000000000; // 25 trips around the world
  closestNode = densePath->pathLen - 1;
  for (int i = 0; i < densePath->pathLen; i++) {
    double nextDistToPathSquared = (pow(currPos.x - densePath->nodes[i]->pose.pos.x, 2)
                                    + pow(currPos.y - densePath->nodes[i]->pose.pos.y, 2));
    if (nextDistToPathSquared < distToPathSquared) {
      distToPathSquared = nextDistToPathSquared;
    } else {
      closestNode = MAX(0, i - 1);
      break;
    }
  }
  double closestYaw = densePath->nodes[closestNode]->pose.rot;
  {
    double rearLatErr = -(currPos.x - densePath->nodes[closestNode]->pose.pos.x)*sin(closestYaw)
      + (currPos.y - densePath->nodes[closestNode]->pose.pos.y)*cos(closestYaw);
    double yawErr = currYaw - closestYaw;
    yawErr = Utils::getAngleInRange(yawErr);
    //    yawErr = fmod(yawErr + 31*M_PI, 2*M_PI) - M_PI;
    double frontLatErr = rearLatErr + sin(yawErr)*VEHICLE_WHEELBASE;
    double maxErr = MAX(fabs(yawErr*VEHICLE_WHEELBASE), MAX(fabs(rearLatErr), fabs(frontLatErr)));
    if (maxErr > m_params.minCorrDist) {
      double newMaxSpeed = m_params.maxCorrSpeed 
        - (maxErr - m_params.minCorrDist)*(m_params.maxCorrSpeed - m_params.minCorrSpeed)/(m_params.maxCorrDist - m_params.minCorrDist);
      if (newMaxSpeed < m_params.minCorrSpeed) {
        newMaxSpeed = m_params.minCorrSpeed;
      }
      if (newMaxSpeed < maxSpeed) {
        maxSpeed = newMaxSpeed;
      }
    }
  }

  // Determine which is the closest node behind Alice
  {
    double x = densePath->nodes[closestNode]->pose.pos.x;
    double y = densePath->nodes[closestNode]->pose.pos.y;
    double scalProd = (currPos.x - x)*cos(closestYaw) + (currPos.y - y)*sin(closestYaw);
    if (scalProd >= 0) {
      startNode = closestNode;
    } else {
      startNode = MAX(0, closestNode - 1);
    }
    *returnStartNode = startNode;
  }

  // Get reference speed of startNode projected onto old traj.
  {
    TrajPoint trajPoint = traj->interpolGetClosest(densePath->nodes[startNode]->pose.pos.x,
                                                   densePath->nodes[startNode]->pose.pos.y,
                                                   NULL, NULL);
    startSpeed = sqrt(trajPoint.nd * trajPoint.nd + trajPoint.ed * trajPoint.ed);
  }

  // Decide if the velocity planning should start from the current
  // speed or the reference speed from the previous control cycle.
  // FIXME: this should be decided in follower
  {
    double replanSpeedErr;
    if (stateProb->planner == RAIL_PLANNER) {
      replanSpeedErr = m_params.replanSpeedErrRail;
    } else {
      replanSpeedErr = m_params.replanSpeedErr;
    }
    if (startSpeed - curSpeed > replanSpeedErr) {
      startSpeed = curSpeed;
    }
  }


  // Set speed of all nodes but the first to maximum allowed in this segment
  // Also allow car to decelerate if needed
  {
    double decelDist = (maxSpeed*maxSpeed - curSpeed*curSpeed) / 2.0 / m_params.maxDecel;
    for (int i = densePath->pathLen - 1; densePath->dists[i] >= decelDist && i > 0; i--) {
      densePath->speeds[i] = maxSpeed;
    }
  }
  
  // Set speed of startNode and all nodes before it
  for (int i = 0; i <= startNode; i++) {
    densePath->speeds[i] = startSpeed;
  }

  // Make sure to always be able to stop at the end of the path
  if (true /*startSpeed*startSpeed / 2.0
      / (densePath->dists[densePath->pathLen - 1] - densePath->dists[startNode])
      > -m_params.maxDecel*/) {
    densePath->speeds[densePath->pathLen - 1] = m_params.almostZeroSpeed;
  }

  // Limit speed in intersections
  {
    int curr = startNode + 1;
    for (int i = 0; i < mapLength; i++) {
      if (sparsePath->nodes[i]->flags.isTurn) {
        int dest = sparseToDenseMap[MIN(mapLength - 1, i + 1)] - 1;
        for ( ; curr <=dest; curr++) {
          if (densePath->speeds[curr] > m_params.maxIntersectionSpeed) {
            densePath->speeds[curr] = m_params.maxIntersectionSpeed;
          }
        }
      } else {
        curr = MAX(startNode + 1, sparseToDenseMap[MAX(0, i - 1)] + 1);
      }
    }
  }

  // Limit speed based on curvature
  for (int i = startNode + 1; i < densePath->pathLen; i++) {
    double curv = fabs(densePath->curvatures[i]);
    if (curv != 0) {
      curv = MIN(curv, 1/VEHICLE_MIN_TURNING_RADIUS);
      double maxSpeed = sqrt(m_params.maxLatAccel / curv);
      if (maxSpeed < densePath->speeds[i]) {
        densePath->speeds[i] = maxSpeed;
      }
    }
  }

  // Limit speed to give the wheels time to turn
  {
    double maxSteerRate, minSteerRateSpeedLimit;
    if (stateProb->planner == RAIL_PLANNER) {
      maxSteerRate = m_params.maxSteerRateRail;
      minSteerRateSpeedLimit = m_params.minSteerRateSpeedLimitRail;
    } else {
      maxSteerRate = m_params.maxSteerRate;
      minSteerRateSpeedLimit = m_params.minSteerRateSpeedLimit;
    }
    for (int i = startNode + 1; i < densePath->pathLen - 1; i++) {
      double dist = densePath->dists[i + 1] - densePath->dists[i];
      double deltaPhi = fabs(atan(VEHICLE_WHEELBASE * densePath->curvatures[i + 1])
                             - atan(VEHICLE_WHEELBASE * densePath->curvatures[i]));
      double maxSpeed = dist / (deltaPhi / maxSteerRate);
      if (maxSpeed < minSteerRateSpeedLimit) {
        maxSpeed = minSteerRateSpeedLimit;
      }
      if (maxSpeed < densePath->speeds[i]) {
        densePath->speeds[i] = maxSpeed;
      }
    }
  }

  // Pause
  {
    // Used to detect the transition into pause state
    static bool isPaused = false;

    // ID of node to stop before
    static int pauseNodeID;
    // Distance from rear axle to pauseNode when stopped
    static double pauseDist;
    
    if (stateProb->state == PAUSE) {

      int denseIndex, sparseIndex;
      
      if (!isPaused) {
        // Determine where to stop
        double stoppingDist = startSpeed*startSpeed/(-2.0*m_params.pauseDecel);
        // Find first dense node past stopping dist (or last node in path)
        for (denseIndex = 0;
             densePath->dists[denseIndex] - densePath->dists[startNode] < stoppingDist
               && denseIndex < densePath->pathLen - 1;
             denseIndex++);
        // Find corresponding sparse node or closest beyond
        for (sparseIndex = 0;
             sparseToDenseMap[sparseIndex] < denseIndex
               && sparseIndex < mapLength - 1;
             sparseIndex++);

        // Save ID of node to stop before
        pauseNodeID = sparsePath->nodes[sparseIndex]->nodeId;
        pauseDist = densePath->dists[sparseToDenseMap[sparseIndex]] - densePath->dists[startNode] - stoppingDist;
        
        isPaused = true;
      }

      // Find node to stop before
      for (sparseIndex = mapLength - 1;
           ((int)(sparsePath->nodes[sparseIndex]->nodeId) != pauseNodeID
            || stateProb->planner != RAIL_PLANNER)
             && sparseIndex > 0;
           sparseIndex--);

      // Set speed of nodes after that to almost zero
      {
        double stopDist = densePath->dists[sparseToDenseMap[sparseIndex]] - pauseDist;
        for (denseIndex = densePath->pathLen - 2;
             densePath->dists[denseIndex + 1] > stopDist
               && denseIndex >= startNode;
             denseIndex--) {
          densePath->speeds[denseIndex] = m_params.almostZeroSpeed;
        }
      }  

    } else {
      // Reset pause condition
      isPaused = false;
    }
  }

  // find the closest obstacle in our path and plan to stop before it
  /*if (stateProb->state == STOP_OBS)*/ { // Always stop for obstacles for now
    double obsPathDist = LARGE_DBL;
    for (int i = 0; i < mapLength; i++) {
      PlanGraphNode *node;
      node = sparsePath->nodes[i];
      if (node->status.obsCollision && graph->isStatusFresh(node->status.obsTime)) {
        obsPathDist = densePath->dists[sparseToDenseMap[MAX(0, i - 1)]] - m_params.obsDist*VEHICLE_LENGTH + 1.0;
        break;
      }
    }
    if (obsPathDist < LARGE_DBL){
      for (int i = densePath->pathLen - 1; densePath->dists[i] >= obsPathDist && i > startNode;i--){
        densePath->speeds[i] = m_params.almostZeroSpeed;
      }
    }
  }

  // find the closest car in our path and plan to match the vel
  if (sparsePath->collideCar) {
    double carPathDist = LARGE_DBL;
    for (int i = 0; i < mapLength; i++) {
      PlanGraphNode *node;
      node = sparsePath->nodes[i];
      if (node->status.carCollision && graph->isStatusFresh(node->status.carTime)) {
        // Keep distance to car
        double holdOffDist;
        if (sparsePath->hasStop) {
          // Check if the car is on the other side of a stop line
          double distToStopLine = densePath->dists[sparseToDenseMap[sparsePath->stopIndex]] + sparsePath->stopDistNode;
          double distToCar = densePath->dists[sparseToDenseMap[MAX(0, i - 1)]] + DIST_REAR_AXLE_TO_FRONT + 1.0;
          if (distToStopLine < distToCar) {
            // Other car on other side of stop line => go closer
            holdOffDist = m_params.carDist;
          } else {
            // Other car on this side of stop line => following
            holdOffDist = m_params.followDist*VEHICLE_LENGTH;
          }
        } else {
          // No stop line => following
          holdOffDist = m_params.followDist*VEHICLE_LENGTH;
        }
        carPathDist = densePath->dists[sparseToDenseMap[MAX(0, i - 1)]] - holdOffDist + 1.0;
        break;
      }
    }
    if (carPathDist < LARGE_DBL){
      for (int i = densePath->pathLen - 1; densePath->dists[i] >= carPathDist && i > startNode;i--){
        densePath->speeds[i] = m_params.almostZeroSpeed;
      }
    }
  }

  // Find the first stop sign.  
  {
    // Remembers if speed has been below m_params.stopSpeed
    // within m_params.stopDistTol while state == STOP_INT
    static bool isStopped = false;
    
    if (stateProb->state == STOP_INT && sparsePath->hasStop) {
      if (isStopped) {
        // If Alice has stopped close enough to stop line,
        // keep her stopped to prevent bunny hopping
        for (int i = startNode; i < densePath->pathLen; i++){
          densePath->speeds[i] = m_params.almostZeroSpeed;
        }
      } else {
        /// Compute distances
        /*// Distance from the beginning of the path to where the rear
        // axle center is if the front of the vehicle is at the stop line.
        double pathStopLineDist = densePath->dists[sparseToDenseMap[sparsePath->stopIndex]]
          + sparsePath->stopDistNode - DIST_REAR_AXLE_TO_FRONT;
        // Distance from the beginning of the path to where we want the rear axle to stop.
        double pathStopDist = pathStopLineDist - m_params.stopDist;
        // Distance from the vehicle's front to the stop line.
        double aliceStopLineDist = pathStopLineDist - densePath->dists[startNode];*/

        double aliceStopLineDist = sparsePath->stopDistAlongPath;
        double pathStopDist = aliceStopLineDist + densePath->dists[startNode] - m_params.stopDist;
	

        if (m_params.outputToMapViewer || CmdArgs::visualization_level >= 2) {
          MapElement me;
          point2 point;
    
          me.setTypePoints();

          point.set(sparsePath->stopDistAlongPath, -1.0);
          me.setGeometry(point);
          me.setColor(MAP_COLOR_RED);
          me.setId(100);
          m_meTalker.sendMapElement(&me, -15);

          point.set(pathStopDist, -1.5);
          me.setGeometry(point);
          me.setColor(MAP_COLOR_BLUE);
          me.setId(101);
          m_meTalker.sendMapElement(&me, -15);

          point.set(densePath->dists[startNode], -2.0);
          me.setGeometry(point);
          me.setColor(MAP_COLOR_GREEN);
          me.setId(102);
          m_meTalker.sendMapElement(&me, -15);
        }

        // Set speed of nodes after stop point to (almost) zero
        int j;
        for (j = densePath->pathLen - 1;
             densePath->dists[j] >= pathStopDist && j > startNode;
             j--){
          densePath->speeds[j] = m_params.almostZeroSpeed;
        }
        // Limit speed of nodes just before the stop line
        for (;
             densePath->dists[j] >= pathStopDist - m_params.nearStopDist && j > startNode;
             j--) {
          if (densePath->speeds[j] > m_params.nearStopSpeed) {
            densePath->speeds[j] = m_params.nearStopSpeed;
          }
        }
            
        // Check if Alice has stopped
        if (curSpeed < m_params.stopSpeed && aliceStopLineDist < m_params.stopDistTol) {
          isStopped = true;
        }
      }
    } else {
      // Reset isStopped
      isStopped = false;
    }
  }

  // To debug stop lines with logic planner
  if (m_params.outputToMapViewer || CmdArgs::visualization_level >= 3) {
    MapElement me;
    point2 point;
    
    me.setTypePoints();

    if (sparsePath->hasStop) {
      point.set(0, sparsePath->stopDistAlongPath);
      me.setGeometry(point,1.0);
      me.setColor(MAP_COLOR_RED);
      me.setId(100);
      m_meTalker.sendMapElement(&me, -30);
      
      point.set(5.0, startSpeed);
      me.setGeometry(point,1.0);
      me.setColor(MAP_COLOR_BLUE);
      me.setId(101);
      m_meTalker.sendMapElement(&me, -30);
      
      point.set(6.0, curSpeed);
      me.setGeometry(point,1.0);
      me.setColor(MAP_COLOR_GREEN);
      me.setId(102);
      m_meTalker.sendMapElement(&me, -30);
    }
    else {
      me.setTypeClear();
      me.setId(100);
      m_meTalker.sendMapElement(&me, -30);
      me.setId(101);
      m_meTalker.sendMapElement(&me, -30);
      me.setId(102);
      m_meTalker.sendMapElement(&me, -30);
    }
  }

  
  // Slow down near obstacles
  for (int i = 0; i < mapLength; i++) {
    double dist = m_params.maxCloseDist;
    // Check for obstacles
    if (graph->isStatusFresh(sparsePath->nodes[i]->status.obsDistTime)
        && sparsePath->nodes[i]->status.obsSideDist < dist) {
      dist = sparsePath->nodes[i]->status.obsSideDist;
    }
    // If an obstacle is close enough, slow down
    if (dist < m_params.maxCloseDist) {
      double newMaxSpeed = m_params.minCloseSpeed 
        + (dist - m_params.minCloseDist)*(m_params.maxCloseSpeed - m_params.minCloseSpeed)
        /(m_params.maxCloseDist - m_params.minCloseDist);
      if (newMaxSpeed < m_params.minCloseSpeed) {
        newMaxSpeed = m_params.minCloseSpeed;
      }
      if (newMaxSpeed < densePath->speeds[sparseToDenseMap[i]]) {
        densePath->speeds[sparseToDenseMap[i]] = newMaxSpeed;
      }
    }
  }

  // If accelerating, make sure the reference speed is at least m_params.minStartSpeed
  if (densePath->speeds[startNode + 1] > densePath->speeds[startNode]
      && densePath->speeds[startNode] < m_params.minStartSpeed) {
    for (int i = 0; i <= startNode; i++) {
      densePath->speeds[i] = m_params.minStartSpeed;
    }
  }

  // Limit the accelerations
  for (int i = 0; i < densePath->pathLen - 1; i++)
  {
    double d0, d1, s0, s1, acc, speed;
    
    d0 = densePath->dists[i + 0];
    d1 = densePath->dists[i + 1];    
    s0 = densePath->speeds[i + 0];    
    s1 = densePath->speeds[i + 1];

    //assert(d1 >= d0);

    // Give smaller accelerations at low speeds to get comfy start
    double locMaxAccel;
    if (s0 < 1.0) {
      locMaxAccel = m_params.maxAccel / 2;
    } else {
      locMaxAccel = m_params.maxAccel;
    }
    
    // Compute acceleration; if it is high, trim the speed
    acc = (s1 * s1 - s0 * s0) / (d1 - d0) / 2;
    if (acc > locMaxAccel)
    {
      acc = locMaxAccel;
      speed = sqrt(2 * acc * (d1 - d0) + s0 * s0);
      // MSG("%f %f %f   %f %f", d1 - d0, s1 - s0, s1, acc, speed);
      //assert(speed <= s1);
      densePath->speeds[i + 1] = speed;
    }
    densePath->accelerations[i] = acc;
  }
  densePath->accelerations[densePath->pathLen - 1] = 0;

  // Limit the decelerations
  {
    double maxDecel = m_params.maxDecel;
    for (int i = densePath->pathLen - 2; i > startNode; i--) {
      // Check if deceleration is too big
      // Remark: maxDecel < 0
      if (densePath->accelerations[i] < maxDecel) {
        // Change deceleration for next edge
        densePath->accelerations[i] = maxDecel;
        // Calculate new speed
        double distToNext  = densePath->dists[i + 1] - densePath->dists[i];
        double nextSpeed = densePath->speeds[i + 1];
        double newSpeed = sqrt(nextSpeed*nextSpeed - 2.0 * maxDecel * distToNext);
        densePath->speeds[i] = newSpeed;
        // Calculate new deceleration for previous edge
        double distToPrev = densePath->dists[i] - densePath->dists[i - 1];
        double prevSpeed = densePath->speeds[i - 1];
        double newPrevDecel = (newSpeed*newSpeed - prevSpeed*prevSpeed) / 2.0 / distToPrev;
        densePath->accelerations[i - 1] = newPrevDecel;
      }

      // Check if maximum deceleration has to be increased
      // Calculate deceleration required from first node to here
      double firstSpeed = densePath->speeds[0];
      double thisSpeed = densePath->speeds[i];
      double reqDecel = (thisSpeed*thisSpeed - firstSpeed*firstSpeed)
        / 2.0 / (densePath->dists[i] - densePath->dists[startNode]);
      if (reqDecel < maxDecel) {
        maxDecel = reqDecel;
      }

      // If the required deceleration is bigger than can be actuated,
      // increase velocities to give a nearly feasible trajectory.
      if (reqDecel < m_params.maxEmergencyDecel) {
        i--;
        for (; i > startNode; i--) {
          densePath->speeds[i] = sqrt(firstSpeed*firstSpeed 
                                      + 2.0*reqDecel*(densePath->dists[i] - densePath->dists[startNode]));
          densePath->accelerations[i] = reqDecel;
        }
        densePath->accelerations[0] = reqDecel;
        break;
      }
    }
  }

  // Smoth out the accelerations
  if (densePath->pathLen > startNode + 1){
    int minima[1000];
    int nMinima = 0;
    float* speeds = densePath->speeds;
    float* dists = densePath->dists;
    
    // Find minima
    if (speeds[startNode] < speeds[startNode + 1]) {
      minima[0] = startNode;
      nMinima = 1;
    }
    for (int i = startNode + 1; i < densePath->pathLen - 1; i++) {
      if ((speeds[i] < speeds[i+1] && speeds[i] <= speeds[i-1])
          || (speeds[i] <= speeds[i+1] && speeds[i] < speeds[i-1])) {
        minima[nMinima] = i;
        nMinima++;
      }
    }
    if (speeds[densePath->pathLen - 1] < speeds[densePath->pathLen-2]) {
      minima[nMinima] = densePath->pathLen - 1;
      nMinima++;
    }

    // Link close minima
    for (int i = 0; i < nMinima - 1; i++) {
      // First node in the segment to smooth
      int firstNode = minima[i];
      int lastNode;

      // Make sure this smoothification doesn't reduce speed below m_params.smoothMinSpeed
      for ( ; speeds[firstNode] < m_params.smoothMinSpeed && firstNode < minima[i + 1]; firstNode++);
      if (firstNode == minima[i + 1]) {
        // The speed never got above m_params.smoothMinSpeed
        // Use the next minimum as the start of an evened segment
        continue;
      }

      // Minimum accleration to a local velocity minimum point. (m/s^2)
      double minAcc = 100.0;
      int minAccNode = -1;
      // Find node within m_params.smoothAccelDist that requires the lowest
      // constant acceleration to connect the velocity profile.
      for (int j = i + 1;
           j < nMinima
             && dists[minima[j]] - dists[minima[i]] <= m_params.smoothAccelDist;
           j++) {
        double acc = (square(speeds[minima[j]]) - square(speeds[minima[i]]))
          / (dists[minima[j]] - dists[minima[i]]) / 2.0;
        if (acc < minAcc) {
          minAcc = acc;
          minAccNode = j;
        }
        if (speeds[minima[j]] < m_params.smoothMinSpeed) {
          // Don't look further if the speed of this node is below m_params.smoothMinSpeed
          minAcc = acc;
          minAccNode = j;
          break;
        }
      }
      // Change velocity profile if needed
      if (minAccNode != -1) {
        lastNode = minima[minAccNode];
        if (speeds[lastNode] < m_params.smoothMinSpeed) {
          for (; speeds[lastNode] < m_params.smoothMinSpeed && lastNode > firstNode + 1; lastNode--);
          minAcc = (square(speeds[lastNode]) - square(speeds[firstNode])) / (dists[lastNode] - dists[firstNode]) / 2.0;
        }

        // Only do something if more than two nodes have speed higher than m_params.smoothMinSpeed
        if (lastNode >= firstNode + 2) {
          densePath->accelerations[firstNode] = minAcc;
          for (int k = firstNode + 1; k < lastNode; k++) {
            densePath->speeds[k] = sqrt(square(densePath->speeds[firstNode]) 
                                        + 2 * minAcc * (dists[k] - dists[firstNode]));
            densePath->accelerations[k] = minAcc;
          }
          i = minAccNode - 1;
        }
      }
    }

  }

  // HACK Output deceleration feed forward when reference
  //  is (almost) zero, to make sure we stop.
  for (int i = 0; densePath->dists[i] < 20.0 && i < densePath->pathLen; i++) {
    if (densePath->speeds[i] <= m_params.almostZeroSpeed) {
      densePath->accelerations[i] = -1.0;
    }
  }

  return VP_OK;
}


Err_t VelPlanner::genTraj(const PlanGraphPath *path, PlanGraphPathDirection direction, CTraj *traj)
{
  traj->startDataInput();
  for (int i = 0; i < path->pathLen; i++) {    
    double speed = path->speeds[i];
    double yaw = path->nodes[i]->pose.rot;
    traj->addPoint(path->nodes[i]->pose.pos.x,
                   speed * cos(yaw),
                   path->accelerations[i] * cos(yaw)
                     - speed * speed * path->curvatures[i] * sin(yaw),
                   path->nodes[i]->pose.pos.y,
                   speed * sin(yaw),
                   path->accelerations[i] * sin(yaw)
                     + speed * speed * path->curvatures[i] * cos(yaw));
  }

  if ((path != NULL) && (path->nodes[0] != NULL)) {
    if (direction == PLAN_GRAPH_PATH_FWD)
      traj->setDirection(1);
    else 
      traj->setDirection(-1);
  }

  return VP_OK;
}


void VelPlanner::printMatlab(CTraj* traj)
{
  ofstream file_out;
  file_out.open("plot_graph.m");
  file_out<< "clear all; close all;" << endl;
  
  if (1) {
    ostringstream x, y, dx, dy, ddx, ddy;
    x << "traj_x = [ ";
    y << "traj_y = [ ";
    dx << "traj_dx = [ ";
    dy << "traj_dy = [ ";
    ddx << "traj_ddx = [ ";
    ddy << "traj_ddy = [ ";
    for (int i=0; i<traj->getNumPoints(); i++) {
      x << traj->getNorthing(i) << " ";
      y << traj->getEasting(i) << " ";
      dx << traj->getNorthingDiff(i, 1) << " ";
      dy << traj->getEastingDiff(i, 1) << " ";
      ddx << traj->getNorthingDiff(i, 2) << " ";
      ddy << traj->getEastingDiff(i, 2) << " ";
    }
    x << "];" << endl;
    y << "];" << endl;
    dx << "];" << endl;
    dy << "];" << endl;
    ddx << "];" << endl;
    ddy << "];" << endl;
    file_out << "% traj output" << endl << x.str() << y.str() << dx.str() << dy.str() << ddx.str() << ddy.str();
    file_out << "speed = sqrt(traj_dx.^2 + traj_dy.^2);" << endl;
    file_out << "acc = sqrt(traj_ddx.^2 + traj_ddy.^2);" << endl;
    file_out << "arclen(1) = 0;" << endl;
    file_out << "for ii=2:length(traj_x) arclen(ii)=arclen(ii-1)+sqrt((traj_x(ii)-traj_x(ii-1))^2 + (traj_y(ii)-traj_y(ii-1))^2);  end" << endl;
    file_out << "plot(traj_x, traj_y, 'g-'); hold off;" << endl;
    file_out << "figure;" << endl;
    file_out << "plot(arclen, speed, 'g'); hold on;" << endl << "plot(arclen, acc, 'r');" << endl;
  }
  file_out.close();

  return;
}

