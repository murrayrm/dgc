/**********************************************************
 **
 **  UT_VELPLANNER.CC
 **
 **    Author: Noel du Toit
 **    Created: Tue Jul 17
 **
 **
 **********************************************************
 **
 **  Unit test for the velocity planner
 **
 **********************************************************/

#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <graph-updater/GraphUpdater.hh>
#include <path-planner/PathPlanner.hh>
#include "VelPlanner.hh"
#include <interfaces/VehicleState.h>
#include <interfaces/ActuatorState.h>
#include <frames/pose3.h>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <iostream>
#include <sstream>
#include <fstream>
#include <trajutils/traj.hh>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>
#include <skynet/skynet.hh>

using namespace std;

int main(int argc, char **args)
{
  // INITIALIZE THE MAP
  Map* map = new Map();
  map->loadRNDF(args[1]);
  map->prior.delta = point2(3778410.5, 403942.3);
  vec3_t offset = { 3778410.5, 403942.3, 0 };

  //insert an obstacle in the map
  MapElement el;
  el.setId(-1);
  el.setTypeObstacle();
  point2 tmppt;
  PointLabel ptlabel(1,1,3);
  map->getWaypoint(tmppt, ptlabel);
  el.setGeometry(tmppt,3.0);
  map->data.push_back(el);

  // INITIALIZE THE GRAPH
  Graph_t* graph;
  GraphNode* node;
    
  CmdArgs::RNDF_file = args[1];
  CmdArgs::sn_key = skynet_findkey(argc, args);
  GraphUpdater::init(&graph, offset, map);
  offset = graph->getNode(0)->pose.pos;

 
  // PLAN PATH
  Path_t path;
  memset(&path, 0 ,sizeof(path));
  VehicleState vehState;
  memset(&vehState, 0, sizeof(vehState));
  ActuatorState actState;
  memset(&actState, 0, sizeof(actState));
  Cost_t cost;
  pose3_t finPose;
  double roll, pitch, yaw;

  // set the vehicle state to the first waypoint
  GraphNode* initNode = graph->getNodeFromRndfId(1,1,2);
  vehState.localX = initNode->pose.pos.x;
  vehState.localY = initNode->pose.pos.y;
  vehState.localZ = initNode->pose.pos.z;
  quat_to_rpy(initNode->pose.rot, &roll, &pitch, &yaw);
  vehState.localYaw = yaw;

  //  vehState.localX = 0;
  //  vehState.localY = .05;
  //  vehState.localZ = 0;
  //  quat_to_rpy(initNode->pose.rot, &roll, &pitch, &yaw);
  //  vehState.localYaw = -0.79;

  // set the actuator state
  //actState.m_steerPos
  
  // set final pose
  GraphNode* finalNode = graph->getNodeFromRndfId(1,1,4);
  finPose = finalNode->pose;
  //  finPose.pos.x = 107.2;
  // finPose.pos.y = -32.4;
  // finPose.pos.z = 0;
  // finPose.rot = quat_from_rpy(0, 0, -0.79);

  // plan the path
  Path_params_t pathParams;
  GraphUpdater::genVehicleSubGraph(graph, vehState, actState);
  PathPlanner::init();
  int err = PathPlanner::planPath(&path, cost, graph, vehState, finPose, pathParams);
  
  // VELOCITY PLANNER
  err = VelPlanner::init();
  // initialize trajectory
  CTraj traj(3);
  // initialize state problem
  StateProblem_t stateProblem;
  stateProblem.state = DRIVE;
  stateProblem.probability = 1.0;
  stateProblem.flag = NO_PASS;

  // plan the velocity
  Vel_params_t velParams;
  err = VelPlanner::planVel(&traj, &path, stateProblem, vehState, velParams);


  // DISPLAY INFORMATION IN MAPVIEWER
  if (1) {
    // DISPLAY GRAPH
    GraphUpdater::display(10, graph, vehState);

    // DISPLAY PATH
    PathPlanner::display(10, &path);

    // DISPLAY TRAJ
    VelPlanner::display(10, &traj);
    VelPlanner::printMatlab(&traj);
  }
}
