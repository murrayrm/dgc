Wed Sep 12  0:50:50 2007	Magnus Linderoth (mlinderoth)

	* version R1-00t-mlinderoth
	BUGS:  
	FILES: VelPlanner.cc(38473)
	Changed parameters for how to limit speed due to lateral error.

Thu Sep  6 22:51:37 2007	Magnus Linderoth (mlinderoth)

	* version R1-00t
	BUGS:  
	FILES: VelPlanner.cc(37721)
	Restored replanning from current velocity at big errors, which
	broke at some point.

	FILES: VelPlanner.cc(37747)
	Improved speed limitation due to lateral error. In addidtion to
	lateral error of rear wheels, now also looks at lateral error of
	front wheels and angular error.

Mon Sep  3 13:17:49 2007	Sven Gowal (sgowal)

	* version R1-00s
	BUGS:  
	FILES: VelPlanner.cc(37254)
	Limited max speed in reverse to be 2 m/s

Sun Sep  2 17:40:08 2007	Sven Gowal (sgowal)

	* version R1-00r
	BUGS:  
	FILES: VelPlanner.cc(37106)
	Hacked pause to actually stop (until Magnus can properly fix it)

Wed Aug 29 20:34:47 2007	Noel duToit (ndutoit)

	* version R1-00q
	BUGS:  
	FILES: VelPlanner.cc(36420)
	Now sets the direction of the traj depending on the direction of
	the nodes in the path.

Fri Aug 24 17:58:27 2007	Magnus Linderoth (mlinderoth)

	* version R1-00p
	BUGS:  
	FILES: VelPlanner.cc(35472), VelPlanner.hh(35472)
	Now populates dists array in sparsePath

Thu Aug 23 23:15:43 2007	Magnus Linderoth (mlinderoth)

	* version R1-00o
	BUGS:  
	FILES: VelPlanner.cc(35344)
	Fixed segfault (hopefully) caused by uninitialized startNode in two
	places. Clears debug plot if receiving empty path

	FILES: VelPlanner.cc(35366)
	Now Alice never starts moving again if she has come to a stop close
	enough to a stopline. Always sets speed of last node in path to
	zero even if it is far away.

Thu Aug 23  4:13:42 2007	Magnus Linderoth (mlinderoth)

	* version R1-00n
	BUGS:  
	FILES: VelPlanner.cc(35058, 35068), VelPlanner.hh(35058)
	Now bases velocity planning on the closest node in densified path
	(not first node) projected onto old traj.

	FILES: VelPlanner.cc(35070)
	Added speed limit in intersections. NOTE: previous commit was
	accidental and doesn't compile.

Mon Aug 20 19:29:21 2007	Sven Gowal (sgowal)

	* version R1-00m
	BUGS:  
	FILES: VelPlanner.cc(34385)
	Accounts for the distToStop field in node

Tue Aug 14 12:10:18 2007	Noel duToit (ndutoit)

	* version R1-00l
	BUGS:  
	FILES: Makefile.yam(33360)
	Changed the makefile for the new cSpecs interface.

Tue Aug 14  9:46:40 2007	Christian Looman (clooman)

	* version R1-00k
	BUGS:  
	FILES: Makefile.yam(33296)
	commit after merge

	FILES: Makefile.yam(33258)
	Adjusted Makefile to work with the new console

Mon Aug 13  6:23:57 2007	Magnus Linderoth (mlinderoth)

	* version R1-00j
	BUGS:  
	FILES: VelPlanner.cc(32980)
	No longer decreases speed based on unfeasible curvatures. Removed
	unusead variables in VelPlannerSpeeds. Corrected expression for
	stop distance when pausing. Removed decrease of speed due to big
	lateral error, will make a better version soon.

	FILES: VelPlanner.cc(32995)
	Decreased stopping distances. maxSteer in VelPlannerKinematics is
	decreased to the actual value.

	FILES: VelPlanner.cc(33071), VelPlanner.hh(33071)
	Removed unused parameters and renamed members. Reintroduced speed
	limiting based on lateral error, now better than before. Replan
	velocity profile if breaking speed limit. Nolonger modifies speed
	of first node.

	FILES: VelPlanner.cc(33072), VelPlanner.hh(33072)
	Added current velocity in graph

	FILES: VelPlanner.cc(33073)
	Lower acceleration at low speeds to get a smoth start

	FILES: VelPlanner.cc(33075)
	Now limits deceleration in its velocity profile.

	FILES: VelPlanner.hh(32977)
	Removed unused variables in VelPlannerSpeeds

Mon Aug  6 23:46:06 2007	Magnus Linderoth (mlinderoth)

	* version R1-00i
	BUGS:  
	FILES: VelPlanner.cc(32448, 32456)
	Now handles the case where the first node in the path is a
	null-pointer

	FILES: VelPlanner.cc(32457)
	Now limits speed based on lateral error. NOTE: Previous commit was
	accidental and does not compile.


Mon Aug  6  3:57:41 2007	Magnus Linderoth (mlinderoth)

	* version R1-00h
	BUGS:  
	FILES: VelPlanner.cc(32193)
	Now handles paths with length 1. Looks far ahead to stop for
	obstacles even if speed is low. Fixed bug in calculation of
	decelerations.

	FILES: VelPlanner.cc(32194)
	Now respects speed limits in mdf.

	FILES: VelPlanner.cc(32195)
	Now replans from current speed instead of reference speed if error
	is too big.

Sat Aug  4  3:36:10 2007	Magnus Linderoth (mlinderoth)

	* version R1-00g
	BUGS:  
	FILES: VelPlanner.cc(31353), VelPlanner.hh(31353)
	Initial modification to do densifying of path before velocity
	planning

	FILES: VelPlanner.cc(31559), VelPlanner.hh(31559)
	Restructuring velocity planning. Now creates densified paths.
	Acceleration and velocity fields not set yet.

	FILES: VelPlanner.cc(31900), VelPlanner.hh(31900)
	Restructured. Now densifies path before velocity planning. Limits
	speed based on curvature. Limits speed to give wheels time to turn.

	FILES: VelPlanner.cc(31909)
	Commented out plotting of path variables in a MapViewer

	FILES: VelPlanner.cc(31939)
	Modified maximum acceleration and deceleration. Reenabled plotting
	of path data.

Mon Jul 30  8:02:06 2007	Sven Gowal (sgowal)

	* version R1-00f
	BUGS:  
	FILES: VelPlanner.cc(30856)
	Now using local coords

Fri Jul 27 16:35:44 2007	Noel duToit (ndutoit)

	* version R1-00e
	BUGS:  
	FILES: UT_velPlanner.cc(30386), VelPlanner.hh(30386)
	made changes for the move of some files in planner to the planner
	interfaces library.

	FILES: VelPlanner.cc(30566, 30574)
	Updated separation distances

	FILES: VelPlanner.cc(30619)
	Reset separation distance

Thu Jul 19 18:52:30 2007	Sven Gowal (sgowal)

	* version R1-00d
	BUGS:  
	FILES: UT_velPlanner.cc(29569), VelPlanner.cc(29569)
	initial implementation of vel-planning for obstacles. Not working
	yet, so needs more debugging.

	FILES: VelPlanner.cc(29406)
	Changed max speed to 5.0m/s

	FILES: VelPlanner.cc(29602)
	Added separation distance when stopping in front of obstacles

	FILES: VelPlanner.cc(29604)
	Using the STOP_OBS state now

	FILES: VelPlanner.cc(29613)
	Added Pause

	FILES: VelPlanner.cc(29624), VelPlanner.hh(29624)
	slows us down near the end of the traj.

	FILES: VelPlanner.cc(29685, 29713)
	Lowered max speed

Tue Jul 17 22:53:46 2007	Noel duToit (ndutoit)

	* version R1-00c
	BUGS:  
	New files: UT_velPlanner.cc
	FILES: Makefile.yam(29343), VelPlanner.cc(29343),
		VelPlanner.hh(29343)
	Finished porting code over from graph-planner to vel-planner. Need
	to still remove state machine and update the planner so that we
	come to a stop at a stopline/intersection, and also regard the
	states passed from logic-planner.

	FILES: VelPlanner.cc(29253), VelPlanner.hh(29253)
	Initial move of graph-planner (TrajPlanner.*) code to vel-planner.
	In the process of adding/modifying the functions. Also in the
	process of removing the FSM in the graph-planner velocity planner
	to inface with the logic-planner.

	FILES: VelPlanner.cc(29307)
	Continued debugging

	FILES: VelPlanner.cc(29373), VelPlanner.hh(29373)
	Removed state machine and implemented initial usage of
	logic-planner state for intersection handling.

Mon Jul 16 14:09:39 2007	Francisco zabala (zabala)

	* version R1-00b
	BUGS:  
	FILES: VelPlanner.cc(29162), VelPlanner.hh(29162)
	Changed Graph_t to be Graph (instead of GraphPlanner)

Fri Jul 13 16:35:08 2007	Noel duToit (ndutoit)

	* version R1-00a
	BUGS:  
	New files: VelPlanner.cc VelPlanner.hh
	FILES: Makefile.yam(28740)
	initial implementation of the vel planner library.

	FILES: Makefile.yam(28762)
	Moved old graph-planner files to the temp-planner-interfaces
	library and correctly link to that now. Removed the redundant
	files.

	FILES: Makefile.yam(28907)
	Added display function to now display the path on MapViewer.

Tue Jul 10 17:04:43 2007	Noel duToit (ndutoit)

	* version R1-00
	Created vel-planner module.






















