Wed Oct 24 23:46:27 2007	Noel duToit (ndutoit)

	* version R1-01j-nav
	BUGS:  
	FILES: VelPlanner.cc(46129)
	Added stop line debug info on channel -30 in mapviewer.

	FILES: VelPlanner.cc(46139)
	more output on -30

	FILES: VelPlanner.cc(46145)
	Now only replans from current speed if the actual speed is much
	below the reference speed, never if the actual speed is above the
	reference speed. This should help preventing overshooting at stop
	lines, particularly in simulation.

	FILES: VelPlanner.cc(46147), VelPlanner.hh(46147),
		vel-planner.config(46147)
	Tuned curvature filter to handle noisy paths better.

	FILES: VelPlanner.cc(46218), VelPlanner.hh(46218),
		vel-planner.config(46218)
	Now limits speed near obstacles. Is controlled by minCloseSpeed,
	maxCloseSpeed, minCloseDist and maxCloseDist in vel-planner.config.
	The parameters are described in VelPlanner.hh

	FILES: VelPlanner.cc(46234)
	Added handling of corrupt paths. Checks if any of the path->pathLen
	first pointers in path->path is a NULL-pointer. Checks if two
	consecutive nodes in the path have the same coordinates. In case it
	finds a corrupt path it stops the vehicle. Should prevent some
	seg-faults.

	FILES: VelPlanner.cc(46311)
	Not using carDist fields anymore

	FILES: VelPlanner.cc(46346)
	Stop two vehicle lengths behind cars.

	FILES: vel-planner.config(46340)
	Changed parameters for slowing down near obstacles. Now the
	distances are from the side of Alice, not from Alice's center line.

	FILES: vel-planner.config(46451)
	Filtering curvatures harder to be less aggressive on steering.

Mon Oct 22  8:26:23 2007	Noel duToit (ndutoit)

	* version R1-01j
	BUGS:  
	FILES: VelPlanner.cc(45832), VelPlanner.hh(45832),
		vel-planner.config(45832)
	Restored code that got lost in latest release. Added code for
	slowing down near obstacles, disabled right now, since it doesn't
	work yet.

	FILES: vel-planner.config(46040)
	Aim to stop 0.7m before stop line

Sat Oct 20 12:55:17 2007	Noel duToit (ndutoit)

	* version R1-01i
	BUGS:  
	FILES: VelPlanner.cc(45709), VelPlanner.hh(45709),
		vel-planner.config(45709)
	moving dead branch back after botched yam operation

	FILES: VelPlanner.cc(45186), VelPlanner.hh(45186),
		vel-planner.config(45186)
	Changed vel-planner so it wants the heading of the nodes to be the
	yaw of the vehicle, not the direction of movement (Different when
	reversing.) Right now it uses the new stuff only when the paths
	come from rail planner. This can be changed by setting
	useBadReverse=0 in velplanner.config. This option should be removed
	when all planners have changed.

	FILES: VelPlanner.cc(45199), vel-planner.config(45199)
	Removed for-loop in stop line handling that was unneccesary with
	the new structure of PlanGraphPath.

	FILES: VelPlanner.cc(45457), vel-planner.config(45457)
	Fixed bug that caused Alice never to stop for obstacles. Now always
	uses the correct way of handling reverse paths for s1planner

	FILES: VelPlanner.cc(45599)
	Changed method to calculate distance to stopline to be consistent
	with logic-planner. Added some stop line debugging to mapviewer
	sendsubgroup -15.

Tue Oct 16 21:31:20 2007	Noel duToit (ndutoit)

	* version R1-01h
	BUGS:  
	FILES: VelPlanner.cc(45009)
	* adapted to new stop line structure

	FILES: VelPlanner.cc(45017)
	Modified to adapt to final stop line structure

Mon Oct 15 23:39:36 2007	Noel duToit (ndutoit)

	* version R1-01g
	BUGS:  
	FILES: VelPlanner.cc(44766), VelPlanner.hh(44766),
		vel-planner.config(44766)
	Added lowpass filter on curvatures when using rail planner. Now
	makes sure the smoothification of the accelerations doesn't
	decrease the speed below smoothMinSpeed. This previously caused the
	car not to move it was stopped and wanted to stop less than the
	distance smoothAccelDist away.

	FILES: VelPlanner.cc(44791), vel-planner.config(44791)
	Now looks at CmdArgs::visualization_level

	FILES: VelPlanner.cc(44884)
	increased the stopping distance before obstacles so that alce do
	not have to back up as often.

Mon Oct 15  8:00:30 2007	Noel duToit (ndutoit)

	* version R1-01f
	BUGS:  
	FILES: VelPlanner.cc(44108), VelPlanner.hh(44108)
	switched the state accessors to use the wrapper functions defined
	in AliceStateHelper. Fixed the warnings.

	FILES: VelPlanner.cc(44170), VelPlanner.hh(44170)
	Directions are now properly populated and used to set the direction
	of the path.

	FILES: VelPlanner.cc(44172)
	Fixed a bug that made pause not work properly in zones.

	FILES: VelPlanner.cc(44238)
	removed some more debug info.

	FILES: VelPlanner.cc(44401)
	Now stops for obstacles both in STOP_OBS and STOP_INT

	FILES: VelPlanner.cc(44600), VelPlanner.hh(44600)
	Updated freshness handling

	FILES: vel-planner.config(44505)
	Reenabling ouput to mapviewer

Thu Oct 11  0:37:05 2007	Andrew Howard (ahoward)

	* version R1-01e
	BUGS:  
	FILES: VelPlanner.cc(43861), VelPlanner.hh(43861)
	Switched to plan graph

	FILES: VelPlanner.cc(43880), VelPlanner.hh(43880),
		vel-planner.config(43880)
	Port to new graph structure

	FILES: VelPlanner.cc(43927)
	Removed refs to local frame

Mon Oct  8 18:08:06 2007	Christian Looman (clooman)

	* version R1-01d
	BUGS:  
	FILES: vel-planner.config(43599)
	changed max. velocity within in intersections to 15 m/s

Mon Oct  8 13:38:54 2007	Magnus Linderoth (mlinderoth)

	* version R1-01c
	BUGS:  
	FILES: VelPlanner.cc(43538)
	Switched from using 3D-poses in GraphNode to using 2D-poses

Fri Oct  5  5:16:45 2007	Magnus Linderoth (mlinderoth)

	* version R1-01b
	BUGS:  
	FILES: VelPlanner.cc(42938), VelPlanner.hh(42938),
		vel-planner.config(42938)
	Modified how curvatures are calculated when using rail planner.

Tue Oct  2 11:57:59 2007	Noel duToit (ndutoit)

	* version R1-01a
	BUGS:  
	FILES: Makefile.yam(42221), VelPlanner.cc(42221),
		VelPlanner.hh(42221)
	Moved the display and print functions to the tpi-Utils object.
	Commented out deprecated unit test in makefile to speed up
	compilation. To use, uncomment the UT in the PROJ_BINS line.

Tue Oct  2 16:10:39 2007	Sven Gowal (sgowal)

	* version R1-01a
	BUGS:  
	FILES: UT_velPlanner.cc(42328)
	Unit test now compiles

Sun Sep 30 10:25:39 2007	Noel duToit (ndutoit)

	* version R1-01
	BUGS:  
	FILES: VelPlanner.cc(41830)
	Fixed bug in feasibilize fcn when dealing with s1planner. This
	requires more investigation though.

Sun Sep 30  2:28:08 2007	Magnus Linderoth (mlinderoth)

	* version R1-00z
	BUGS:  
	FILES: VelPlanner.cc(41584), VelPlanner.hh(41584)
	Feasibilizes paths by cutting corners. Still doesn't remember
	between cycles. Need to optimize some slow implementations.

	FILES: VelPlanner.cc(41630)
	Now remembers filtering from previous cycle and copies it if
	appropriate.

	FILES: VelPlanner.cc(41636, 41637)
	Optimized copying of sparsePath.

	FILES: VelPlanner.cc(41639), VelPlanner.hh(41639),
		vel-planner.config(41639)
	Added option to disable feasibilization and output to mapviewer.
	Note: previous commit was accidental and doesn't compile.

	FILES: VelPlanner.cc(41664), VelPlanner.hh(41664),
		vel-planner.config(41664)
	Make sure paths aren't feasibilized in zones, since I'm not
	guaranteed unique node indeces. Fixed bug that caused Alice not to
	stop propperly when getting a pause from planner.

Fri Sep 21 16:37:50 2007	Noel duToit (ndutoit)

	* version R1-00y
	BUGS:  
	FILES: VelPlanner.cc(39857), VelPlanner.hh(39857)
	fixed a bug for planning the vel in zones. Added a print function
	to print the traj to log.

	FILES: VelPlanner.cc(39883)
	fixed the pause handling in zones

Wed Sep 19 19:56:48 2007	Magnus Linderoth (mlinderoth)

	* version R1-00x
	BUGS:  
	FILES: VelPlanner.cc(39467), VelPlanner.hh(39467),
		vel-planner.config(39467)
	Added functionality to smooth accelerations.

	FILES: vel-planner.config(39468)
	Increased stopSpeed.

	FILES: vel-planner.config(39518)
	Adjusted maxZoneSpeed and smoothDist.

	FILES: VelPlanner.cc(39467), VelPlanner.hh(39467),
		vel-planner.config(39467)
	Added functionality to smooth accelerations.

	FILES: vel-planner.config(39468)
	Increased stopSpeed.

Tue Sep 18  5:24:14 2007	Magnus Linderoth (mlinderoth)

	* version R1-00w
	BUGS:  
	New files: ConfigFile.cc ConfigFile.hh vel-planner.config
	FILES: Makefile.yam(39084), VelPlanner.cc(39084),
		VelPlanner.hh(39084)
	Moved parameters to config file, so you don't have to recompile
	when changing a paramter.

	FILES: Makefile.yam(39092)
	Fixed typo in Makefile.yam

	FILES: VelPlanner.cc(39093), VelPlanner.hh(39093)
	Moved calculation of curvatures out of densifyPath to new function
	calculateCurvatures.

	FILES: VelPlanner.cc(39094)
	Densifies path longer to account for that Alice might not be at the
	beginning of the first edge.

	FILES: VelPlanner.cc(39095), VelPlanner.hh(39095)
	Fixed bug in speed limiting to give the wheels time to turn.
	Parameterized reverse speed.

	FILES: VelPlanner.cc(39259)
	Refined pausing functionality.

	FILES: VelPlanner.cc(39261), VelPlanner.hh(39261)
	Parameterized minimum reference speed for acceleration.

Fri Sep 14  3:25:52 2007	Magnus Linderoth (mlinderoth)

	* version R1-00v
	BUGS:  
	FILES: VelPlanner.cc(38747)
	Use mapLength instead of spersePath->pathLen as length of
	sperseToDenseMap when setting intersection speed.

Wed Sep 12  0:50:50 2007	Magnus Linderoth (mlinderoth)

	* version R1-00u
	BUGS:  
	FILES: VelPlanner.cc(38473)
	Changed parameters for how to limit speed due to lateral error.

Thu Sep  6 22:51:37 2007	Magnus Linderoth (mlinderoth)

	* version R1-00t
	BUGS:  
	FILES: VelPlanner.cc(37721)
	Restored replanning from current velocity at big errors, which
	broke at some point.

	FILES: VelPlanner.cc(37747)
	Improved speed limitation due to lateral error. In addidtion to
	lateral error of rear wheels, now also looks at lateral error of
	front wheels and angular error.

Mon Sep  3 13:17:49 2007	Sven Gowal (sgowal)

	* version R1-00s
	BUGS:  
	FILES: VelPlanner.cc(37254)
	Limited max speed in reverse to be 2 m/s

Sun Sep  2 17:40:08 2007	Sven Gowal (sgowal)

	* version R1-00r
	BUGS:  
	FILES: VelPlanner.cc(37106)
	Hacked pause to actually stop (until Magnus can properly fix it)

Wed Aug 29 20:34:47 2007	Noel duToit (ndutoit)

	* version R1-00q
	BUGS:  
	FILES: VelPlanner.cc(36420)
	Now sets the direction of the traj depending on the direction of
	the nodes in the path.

Fri Aug 24 17:58:27 2007	Magnus Linderoth (mlinderoth)

	* version R1-00p
	BUGS:  
	FILES: VelPlanner.cc(35472), VelPlanner.hh(35472)
	Now populates dists array in sparsePath

Thu Aug 23 23:15:43 2007	Magnus Linderoth (mlinderoth)

	* version R1-00o
	BUGS:  
	FILES: VelPlanner.cc(35344)
	Fixed segfault (hopefully) caused by uninitialized startNode in two
	places. Clears debug plot if receiving empty path

	FILES: VelPlanner.cc(35366)
	Now Alice never starts moving again if she has come to a stop close
	enough to a stopline. Always sets speed of last node in path to
	zero even if it is far away.

Thu Aug 23  4:13:42 2007	Magnus Linderoth (mlinderoth)

	* version R1-00n
	BUGS:  
	FILES: VelPlanner.cc(35058, 35068), VelPlanner.hh(35058)
	Now bases velocity planning on the closest node in densified path
	(not first node) projected onto old traj.

	FILES: VelPlanner.cc(35070)
	Added speed limit in intersections. NOTE: previous commit was
	accidental and doesn't compile.

Mon Aug 20 19:29:21 2007	Sven Gowal (sgowal)

	* version R1-00m
	BUGS:  
	FILES: VelPlanner.cc(34385)
	Accounts for the distToStop field in node

Tue Aug 14 12:10:18 2007	Noel duToit (ndutoit)

	* version R1-00l
	BUGS:  
	FILES: Makefile.yam(33360)
	Changed the makefile for the new cSpecs interface.

Tue Aug 14  9:46:40 2007	Christian Looman (clooman)

	* version R1-00k
	BUGS:  
	FILES: Makefile.yam(33296)
	commit after merge

	FILES: Makefile.yam(33258)
	Adjusted Makefile to work with the new console

Mon Aug 13  6:23:57 2007	Magnus Linderoth (mlinderoth)

	* version R1-00j
	BUGS:  
	FILES: VelPlanner.cc(32980)
	No longer decreases speed based on unfeasible curvatures. Removed
	unusead variables in VelPlannerSpeeds. Corrected expression for
	stop distance when pausing. Removed decrease of speed due to big
	lateral error, will make a better version soon.

	FILES: VelPlanner.cc(32995)
	Decreased stopping distances. maxSteer in VelPlannerKinematics is
	decreased to the actual value.

	FILES: VelPlanner.cc(33071), VelPlanner.hh(33071)
	Removed unused parameters and renamed members. Reintroduced speed
	limiting based on lateral error, now better than before. Replan
	velocity profile if breaking speed limit. Nolonger modifies speed
	of first node.

	FILES: VelPlanner.cc(33072), VelPlanner.hh(33072)
	Added current velocity in graph

	FILES: VelPlanner.cc(33073)
	Lower acceleration at low speeds to get a smoth start

	FILES: VelPlanner.cc(33075)
	Now limits deceleration in its velocity profile.

	FILES: VelPlanner.hh(32977)
	Removed unused variables in VelPlannerSpeeds

Mon Aug  6 23:46:06 2007	Magnus Linderoth (mlinderoth)

	* version R1-00i
	BUGS:  
	FILES: VelPlanner.cc(32448, 32456)
	Now handles the case where the first node in the path is a
	null-pointer

	FILES: VelPlanner.cc(32457)
	Now limits speed based on lateral error. NOTE: Previous commit was
	accidental and does not compile.


Mon Aug  6  3:57:41 2007	Magnus Linderoth (mlinderoth)

	* version R1-00h
	BUGS:  
	FILES: VelPlanner.cc(32193)
	Now handles paths with length 1. Looks far ahead to stop for
	obstacles even if speed is low. Fixed bug in calculation of
	decelerations.

	FILES: VelPlanner.cc(32194)
	Now respects speed limits in mdf.

	FILES: VelPlanner.cc(32195)
	Now replans from current speed instead of reference speed if error
	is too big.

Sat Aug  4  3:36:10 2007	Magnus Linderoth (mlinderoth)

	* version R1-00g
	BUGS:  
	FILES: VelPlanner.cc(31353), VelPlanner.hh(31353)
	Initial modification to do densifying of path before velocity
	planning

	FILES: VelPlanner.cc(31559), VelPlanner.hh(31559)
	Restructuring velocity planning. Now creates densified paths.
	Acceleration and velocity fields not set yet.

	FILES: VelPlanner.cc(31900), VelPlanner.hh(31900)
	Restructured. Now densifies path before velocity planning. Limits
	speed based on curvature. Limits speed to give wheels time to turn.

	FILES: VelPlanner.cc(31909)
	Commented out plotting of path variables in a MapViewer

	FILES: VelPlanner.cc(31939)
	Modified maximum acceleration and deceleration. Reenabled plotting
	of path data.

Mon Jul 30  8:02:06 2007	Sven Gowal (sgowal)

	* version R1-00f
	BUGS:  
	FILES: VelPlanner.cc(30856)
	Now using local coords

Fri Jul 27 16:35:44 2007	Noel duToit (ndutoit)

	* version R1-00e
	BUGS:  
	FILES: UT_velPlanner.cc(30386), VelPlanner.hh(30386)
	made changes for the move of some files in planner to the planner
	interfaces library.

	FILES: VelPlanner.cc(30566, 30574)
	Updated separation distances

	FILES: VelPlanner.cc(30619)
	Reset separation distance

Thu Jul 19 18:52:30 2007	Sven Gowal (sgowal)

	* version R1-00d
	BUGS:  
	FILES: UT_velPlanner.cc(29569), VelPlanner.cc(29569)
	initial implementation of vel-planning for obstacles. Not working
	yet, so needs more debugging.

	FILES: VelPlanner.cc(29406)
	Changed max speed to 5.0m/s

	FILES: VelPlanner.cc(29602)
	Added separation distance when stopping in front of obstacles

	FILES: VelPlanner.cc(29604)
	Using the STOP_OBS state now

	FILES: VelPlanner.cc(29613)
	Added Pause

	FILES: VelPlanner.cc(29624), VelPlanner.hh(29624)
	slows us down near the end of the traj.

	FILES: VelPlanner.cc(29685, 29713)
	Lowered max speed

Tue Jul 17 22:53:46 2007	Noel duToit (ndutoit)

	* version R1-00c
	BUGS:  
	New files: UT_velPlanner.cc
	FILES: Makefile.yam(29343), VelPlanner.cc(29343),
		VelPlanner.hh(29343)
	Finished porting code over from graph-planner to vel-planner. Need
	to still remove state machine and update the planner so that we
	come to a stop at a stopline/intersection, and also regard the
	states passed from logic-planner.

	FILES: VelPlanner.cc(29253), VelPlanner.hh(29253)
	Initial move of graph-planner (TrajPlanner.*) code to vel-planner.
	In the process of adding/modifying the functions. Also in the
	process of removing the FSM in the graph-planner velocity planner
	to inface with the logic-planner.

	FILES: VelPlanner.cc(29307)
	Continued debugging

	FILES: VelPlanner.cc(29373), VelPlanner.hh(29373)
	Removed state machine and implemented initial usage of
	logic-planner state for intersection handling.

Mon Jul 16 14:09:39 2007	Francisco zabala (zabala)

	* version R1-00b
	BUGS:  
	FILES: VelPlanner.cc(29162), VelPlanner.hh(29162)
	Changed Graph_t to be Graph (instead of GraphPlanner)

Fri Jul 13 16:35:08 2007	Noel duToit (ndutoit)

	* version R1-00a
	BUGS:  
	New files: VelPlanner.cc VelPlanner.hh
	FILES: Makefile.yam(28740)
	initial implementation of the vel planner library.

	FILES: Makefile.yam(28762)
	Moved old graph-planner files to the temp-planner-interfaces
	library and correctly link to that now. Removed the redundant
	files.

	FILES: Makefile.yam(28907)
	Added display function to now display the path on MapViewer.

Tue Jul 10 17:04:43 2007	Noel duToit (ndutoit)

	* version R1-00
	Created vel-planner module.











































