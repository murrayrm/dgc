#include "VelPlanner.hh"
#include <math.h>

// TODO: this is temporary and should be removed in the future
#include "graph-updater/GraphUpdater.hh"

CMapElementTalker VelPlanner::meTalker;

#define MAXSTEER 30
#define MAXSPEED 10

TrajPlanner* VelPlanner::trajPlanner;

int VelPlanner::init()
{
  
  trajPlanner = new TrajPlanner(GraphUpdater::getUnderlyingGraph());
  
  // Set vehicle properties
  if (true)
  {
    TrajPlannerKinematics kin;
    trajPlanner->getKinematics(&kin);
    kin.wheelBase = VEHICLE_WHEELBASE;
    kin.maxSteer = MAXSTEER * M_PI/180;
    trajPlanner->setKinematics(&kin);

    TrajPlannerSpeeds speeds;
    trajPlanner->getSpeeds(&speeds);
    speeds.maxLaneSpeed = MAXSPEED;
    trajPlanner->setSpeeds(&speeds);
  }

  // Initialize the map element talker
  meTalker.initSendMapElement(CmdArgs::sn_key);

  return 0;
}

void VelPlanner::destroy()
{
  delete trajPlanner;
  return;
}


Err_t VelPlanner::planVel(CTraj* traj, Path_t* path, VehicleState vehState, Vel_params_t params)
{
  ActuatorState actState;
  trajPlanner->genSpeedProfile(path, &vehState, &actState);
  trajPlanner->genTraj(path, &vehState, &actState, traj);

  return VP_OK;
}


void VelPlanner::display(int sendSubgroup, CTraj* traj)
{
    MapElement me;
    int counter=14000;
    point2 point;
    vector<point2> points;
    MapId mapId;
    
    mapId = counter;
    for (int i=0; i<traj->getNumPoints(); i++) {
      point.set(traj->getNorthing(i),traj->getEasting(i));
      points.push_back(point);
    }
    me.setId(mapId);
    me.setTypePlanningTraj();
    me.setGeometry(points);
    meTalker.sendMapElement(&me,sendSubgroup);  


}
