#include "VelPlanner.hh"
#include <math.h>

CMapElementTalker VelPlanner::meTalker;
VelPlannerKinematics VelPlanner::kin;
VelPlannerSpeeds VelPlanner::speeds;
StateProblem_t VelPlanner::stateProb;
Vel_params_t VelPlanner::velParams;

// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)

// Additional limits
#define UINT64_MAX	18446744073709551615ULL
#define DBL_MAX         100000000
// Useful macros
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

#define MAXSTEER 30
#define MAXSPEED 10

int VelPlanner::init()
{
  // Set vehicle properties
  kin.wheelBase = VEHICLE_WHEELBASE;
  kin.maxSteer = MAXSTEER * M_PI/180;
  // set speeds
  speeds.maxLaneSpeed = MAXSPEED;
  speeds.maxTurnSpeed = 2.0;
  speeds.maxNearSpeed = 1.0;
  speeds.maxOffRoadSpeed = 1.0;
  speeds.maxStopSpeed = 0.5;
  speeds.maxAccel = +0.50;
  speeds.maxDecel = -0.50;
  speeds.deltaSpeed = 0.25;
  speeds.stopDist = 5;
  speeds.stopTol = 2.2;
  speeds.stopTime = 5.0;
  speeds.signalDist = 30.0;
  
  // Initialize the map element talker
  meTalker.initSendMapElement(CmdArgs::sn_key);

  return 0;
}

void VelPlanner::destroy()
{
  return;
}


Err_t VelPlanner::planVel(CTraj* traj, Path_t* path, StateProblem_t problem, VehicleState vehState, Vel_params_t params)
{
  // TODO: either implement usage of actState in the velocity planner, or remove completely
  ActuatorState actState;
  stateProb = problem;
  velParams = params;

  genSpeedProfile(path, &vehState, &actState);

  genTraj(path, &vehState, &actState, traj);

  return VP_OK;
}


void VelPlanner::display(int sendSubgroup, CTraj* traj)
{
    MapElement me;
    int counter=140000;
    point2 point;
    vector<point2> points;
    MapId mapId;
    
    mapId = counter;
    for (int i=0; i<traj->getNumPoints(); i++) {
      point.set(traj->getNorthing(i),traj->getEasting(i));
      points.push_back(point);
    }
    me.setId(mapId);
    me.setTypePlanningTraj();
    me.setGeometry(points);
    meTalker.sendMapElement(&me,sendSubgroup);
}


int VelPlanner::genSpeedProfile(Path_t *path,
                                 const VehicleState *vehState,
                                 const ActuatorState *actState)
{
  int i;
  double currentSpeed;
  double stopDist, dist;
  
  // Get the vehicle speed.
  currentSpeed = sqrt(pow(vehState->utmNorthVel,2) + pow(vehState->utmEastVel, 2));
  
  // If there is no path, there is nothing to do
  if (path->pathLen <= 0)
    return 0;

  // Zeroth pass: walk along the path and construct the distance
  // profile.
  path->dists[0] = 0;
  for (i = 1; i < path->pathLen; i++) 
  {
    GraphNode *nodeA, *nodeB;
    double dx, dy, dd;
    nodeA = path->path[i - 1];
    nodeB = path->path[i + 0];
    dx = nodeB->pose.pos.x - nodeA->pose.pos.x;
    dy = nodeB->pose.pos.y - nodeA->pose.pos.y;
    dd = sqrt(dx * dx + dy * dy);
    assert(dd > 0);
    path->dists[i] = path->dists[i - 1] + dd;
  }

  // First pass: walk along the path and construct the nominal speed profile.
  // We give a small initial acceleration to get Alice moving.
  for (i = 0; i < path->pathLen; i++) 
  {
    GraphNode *node;
    node = path->path[i];
    if (i == 0)
      path->speeds[i] = MIN(currentSpeed + speeds.deltaSpeed, speeds.maxLaneSpeed);
    else
      path->speeds[i] = speeds.maxLaneSpeed;
  }
  
  // Second pass: stop if there is a collision on this path.  TODO: this should
  // produce a soft stop.
  if (path->collideObs >= GRAPH_COLLIDE_MAP_INNER && path->goalDist < 100) // MAGIC
  {
    MSG("path has collision; hard stop");
    for (i = 0; i < path->pathLen; i++)
      path->speeds[i] = 0;
  }

  // Third pass: walk along the path and construct the max speed
  // profile.
  for (i = 0; i < path->pathLen; i++) 
  {
    GraphNode *node;
    double speed;
      
    node = path->path[i];
    speed = path->speeds[i];

    // Check for corridor/lane speeds
    if (node->type != GRAPH_NODE_TURN && node->segmentId > 0)
    {
      // If driving in a segment, consider the maximum speed for this
      // segment.  The miminum speed is currently ignored.
      //double minSpeed = 0;
      double maxSpeed = MAXSPEED;
      // mdf.getSpeedLimits(node->segmentId, &minSpeed, &maxSpeed);
      if (maxSpeed > 0)
        speed = MIN(speed, maxSpeed);
    }
    else
    {
      // Are we driving through an intersection?  If so, use the
      // intersection speed.
      speed = MIN(speed, speeds.maxTurnSpeed);
    }

    // Check for collisions with obstacles.  
    if (node->collideObs == GRAPH_COLLIDE_MAP_INNER)
      speed = 0;
    else if (node->collideObs == GRAPH_COLLIDE_MAP_OUTER)
      speed = MIN(speed, speeds.maxNearSpeed);

    // If the path is collides with obstacles (i.e., we can't reach the goal),
    // we might as well stop early.  This causes a soft stop.
    if (path->collideObs >= GRAPH_COLLIDE_MAP_OUTER && node->collideObs > 0)
      speed = 0;

    // Check for collisions with cars.
    if (node->collideCar == GRAPH_COLLIDE_MAP_INNER ||
        node->collideCar == GRAPH_COLLIDE_LANE_INNER)
      speed = 0;
    else if (node->collideCar > 0)
      speed = MIN(speed, speeds.maxNearSpeed);

    path->speeds[i] = speed;
  }
  
  // Find the first stop sign.  
  if (STOP_INT==stateProb.state) {
    stopDist = DBL_MAX;
    for (i = 0; i < path->pathLen; i++) {
      GraphNode *node;
      node = path->path[i];
      if (node->isExit && node->isStop) {
	stopDist = path->dists[i] - speeds.stopDist;
	break;
      }
    }
    
    if (stopDist < DBL_MAX)
    {
      for (i = 0; i < path->pathLen; i++)
      {
	dist = stopDist-path->dists[i];
	if (dist < speeds.stopTol/2)
	    path->speeds[i] = 0;
      }
    }
  }

  // Smooth out the accelerations
  for (i = 0; i < path->pathLen - 1; i++)
  {
    double d0, d1, s0, s1, acc, speed;
    
    d0 = path->dists[i + 0];
    d1 = path->dists[i + 1];    
    s0 = path->speeds[i + 0];    
    s1 = path->speeds[i + 1];

    assert(d1 > d0);
    
    // Compute acceleration; if it is high, trim the speed
    acc = (s1 * s1 - s0 * s0) / (d1 - d0) / 2;
    if (acc > speeds.maxAccel)
    {
      acc = speeds.maxAccel;
      speed = sqrt(2 * acc * (d1 - d0) + s0 * s0);
      //MSG("%f %f %f   %f %f", d1 - d0, s1 - s0, s1, acc, speed);
      assert(speed <= s1);
      path->speeds[i + 1] = MIN(path->speeds[i + 1], speed);
    }
  }

  // Smooth out the deccelerations
  for (i = path->pathLen - 1; i > 0; i--)
  {
    double d0, d1, s0, s1, dec, speed;
    
    d0 = path->dists[i + 0];
    d1 = path->dists[i - 1];    
    s0 = path->speeds[i + 0];    
    s1 = path->speeds[i - 1];

    assert(d1 < d0);
    
    // Compute acceleration; if it is high, trim the speed
    dec = (s1 * s1 - s0 * s0) / (d1 - d0) / 2;
    if (dec < speeds.maxDecel)
    {
      dec = speeds.maxDecel;
      speed = sqrt(2 * dec * (d1 - d0) + s0 * s0);
      //MSG("%f %f %f   %f %f", d1 - d0, s1 - s0, s1, dec, speed);
      assert(speed <= s1);
      path->speeds[i - 1] = MIN(path->speeds[i - 1], speed);
    }
  }

  return 0;
}

// Generate a vehicle trajectory.
int VelPlanner::genTraj(const Path_t *path,
                         const VehicleState *vehState,
                         const ActuatorState *actState,                           
                         CTraj *traj)
{
  int i, numSteps, numSpeeds;
  double roll, pitch, yaw;
  Vehicle *vp;
  Maneuver *maneuver;
  GraphNode *nodeA, *nodeB;
  double speeds[2], dists[2];
  VehicleConfiguration configA;  
  Pose2D poseA, poseB;
  double stepSize;

  // Spacing between trajectory points
  stepSize = 0.05; // MAGIC

  // Make sure we have a path
  if (path->pathLen < 2)
  {
    MSG("no valid destination, stopping vehicle");
    traj->startDataInput();
    traj->addPoint(vehState->utmNorthing, 0, 0,
                   vehState->utmEasting, 0, 0);
    return 0;
  }

  assert(path->pathLen >= 1);
  
  // Vehicle properties
  vp = maneuver_create_vehicle(kin.wheelBase, kin.maxSteer);
      
  traj->startDataInput();

  // Start with the vehicle node
  nodeA = path->path[0];
  speeds[0] = path->speeds[0];
  dists[0] = path->dists[0];

  // Look for the first non-volatile node; this is the destination for the
  // first maneuver
  nodeB = NULL;
  for (i = 1; i < path->pathLen; i++)
  {
    nodeB = path->path[i];
    speeds[1] = path->speeds[i];
    dists[1] = path->dists[i];
    if (nodeB->type != GRAPH_NODE_VOLATILE)
      break;
  }
  assert(nodeB != NULL);

  // Compute the number of speeds to consider along this maneuver.
  numSpeeds = i + 1;
    
  // Compute the number of interpolation steps, based on the path
  // distance between the two nodes.
  numSteps = (int) ((dists[1] - dists[0]) / stepSize) + 1;

  // Use the initial configuration given by the state.
  // This causes oscillations.
  //configA.x = vehState->utmNorthing;
  //configA.y = vehState->utmEasting;
  //configA.theta = vehState->utmYaw;
  //configA.phi = actState->m_steerpos * kin.maxSteer;
  //speedA = vehState->vehSpeed;

  // Use the initial configuration given by the node.
  // This produces stable behavior at low speeds
  quat_to_rpy(nodeA->pose.rot, &roll, &pitch, &yaw);
  configA.x = nodeA->pose.pos.x;
  configA.y = nodeA->pose.pos.y;
  configA.theta = yaw;
  configA.phi = nodeA->steerAngle;

  quat_to_rpy(nodeB->pose.rot, &roll, &pitch, &yaw);
  poseB.x = nodeB->pose.pos.x;
  poseB.y = nodeB->pose.pos.y;
  poseB.theta = yaw;
    
  // Generate a trajectory from the maneuver.
  maneuver = maneuver_config2pose(vp, &configA, &poseB);
  maneuver_profile_single(vp, maneuver, numSpeeds, path->speeds, numSteps, traj);
  maneuver_free(maneuver);
  
  // Create the rest of the maneuvers
  for (; i < path->pathLen - 1; i++) 
  {
    nodeA = path->path[i + 0];
    nodeB = path->path[i + 1];

    speeds[0] = path->speeds[i + 0];
    speeds[1] = path->speeds[i + 1];

    dists[0] = path->dists[i + 0];
    dists[1] = path->dists[i + 1];
          
    // Compute the number of interpolation steps, based on the path
    // distance between the two nodes.
    numSteps = (int) ((dists[1] - dists[0]) / stepSize) + 1;

    // For non-vehicle nodes, use the node pose and assume zero-steering angle
    quat_to_rpy(nodeA->pose.rot, &roll, &pitch, &yaw);
    poseA.x = nodeA->pose.pos.x;
    poseA.y = nodeA->pose.pos.y;
    poseA.theta = yaw;

    // Final pose on node B
    quat_to_rpy(nodeB->pose.rot, &roll, &pitch, &yaw);
    poseB.x = nodeB->pose.pos.x;
    poseB.y = nodeB->pose.pos.y;
    poseB.theta = yaw;

    // Generate a trajectory from the maneuver list.
    maneuver = maneuver_pose2pose(vp, &poseA, &poseB);
    maneuver_profile_generate(vp, 1, &maneuver, speeds, numSteps, traj);
    maneuver_free(maneuver);
 
    // Dont plan too far into the future
    if (dists[1] > 40.0) // MAGIC
      break;
  }

  return 0;
}


void VelPlanner::printMatlab(CTraj* traj)
{
  ofstream file_out;
  file_out.open("plot_graph.m");
  file_out<< "clear all; close all;" << endl;
  
  if (1) {
    ostringstream x, y, dx, dy, ddx, ddy;
    x << "traj_x = [ ";
    y << "traj_y = [ ";
    dx << "traj_dx = [ ";
    dy << "traj_dy = [ ";
    ddx << "traj_ddx = [ ";
    ddy << "traj_ddy = [ ";
    for (int i=0; i<traj->getNumPoints(); i++) {
      x << traj->getNorthing(i) << " ";
      y << traj->getEasting(i) << " ";
      dx << traj->getNorthingDiff(i, 1) << " ";
      dy << traj->getEastingDiff(i, 1) << " ";
      ddx << traj->getNorthingDiff(i, 2) << " ";
      ddy << traj->getEastingDiff(i, 2) << " ";
    }
    x << "];" << endl;
    y << "];" << endl;
    dx << "];" << endl;
    dy << "];" << endl;
    ddx << "];" << endl;
    ddy << "];" << endl;
    file_out << "% traj output" << endl << x.str() << y.str() << dx.str() << dy.str() << ddx.str() << ddy.str();
    file_out << "speed = sqrt(traj_dx.^2 + traj_dy.^2);" << endl;
    file_out << "acc = sqrt(traj_ddx.^2 + traj_ddy.^2);" << endl;
    file_out << "arclen(1) = 0;" << endl;
    file_out << "for ii=2:length(traj_x) arclen(ii)=arclen(ii-1)+sqrt((traj_x(ii)-traj_x(ii-1))^2 + (traj_y(ii)-traj_y(ii-1))^2);  end" << endl;
    file_out << "plot(traj_x, traj_y, 'g-'); hold off;" << endl;
    file_out << "figure;" << endl;
    file_out << "plot(arclen, speed, 'g'); hold on;" << endl << "plot(arclen, acc, 'r');" << endl;
  }
  file_out.close();

  return;
}

