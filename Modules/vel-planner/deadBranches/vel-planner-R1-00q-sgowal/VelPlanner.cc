#include "VelPlanner.hh"
#include <alice/AliceConstants.h>
#include <math.h>
#include <temp-planner-interfaces/Log.hh>
#include <temp-planner-interfaces/Console.hh>

CMapElementTalker VelPlanner::m_meTalker;
VelPlannerKinematics VelPlanner::m_kin;
VelPlannerStaticParams VelPlanner::m_params;
// StateProblem_t VelPlanner::stateProb;
// Vel_params_t VelPlanner::velParams;
GraphNode VelPlanner::m_graphNodes[1000];

// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)

// Additional limits
#define UINT64_MAX	18446744073709551615ULL
#define DBL_MAX         100000000
// Useful macros
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define square(x) ((x)*(x))

#define REPLAN_SPEED_ERR (3.0)
#define ALMOST_ZERO_SPEED 0.1
#define STOP_SPEED 0.2
#define STOP_DIST_TOL 0.8

int VelPlanner::init()
{
  // Set vehicle properties
  m_kin.wheelBase = VEHICLE_WHEELBASE;
  m_kin.maxSteer = VEHICLE_MAX_AVG_STEER; //30.0 * M_PI/180;
  // set speeds
  //m_params.maxLaneSpeed = 6.0;
  //m_params.maxTurnSpeed = 2.0;
  //m_params.maxNearSpeed = 1.0;
  //m_params.maxOffRoadSpeed = 1.0;
  //m_params.maxStopSpeed = 0.5;
  m_params.maxIntersectionSpeed = 2.0;
  m_params.maxAccel = 1.0;
  m_params.maxDecel = -0.75;
  m_params.maxEmergencyDecel = -3.0;
  m_params.maxLatAccel = 0.75;
  m_params.maxSteerRate = 0.1;
  //m_params.deltaSpeed = 0.25;
  m_params.stopDist = DIST_REAR_AXLE_TO_FRONT + 0.2;
  //m_params.stopTol = 1.1;
  //m_params.stopTime = 5.0;
  //m_params.signalDist = 30.0;
  
  // Initialize the map element talker
  m_meTalker.initSendMapElement(CmdArgs::sn_key);

  return 0;
}

void VelPlanner::destroy()
{
  return;
}


Err_t VelPlanner::planVel(CTraj* traj, Path_t* sparsePath, StateProblem_t stateProb,
                          VehicleState vehState, Vel_params_t velParams)
{
  // TODO: either implement usage of actState in the velocity planner, or remove completely
  ActuatorState actState;
  /*stateProb = problem;
  velParams = params;*/
  static Path_t densePath;
  int sparseToDenseMap[1000];
  int mapLength;
  int startNode = 0;


  if (sparsePath->pathLen > 1 && sparsePath->path[0] != NULL) {


    densifyPath(sparsePath, &densePath, sparseToDenseMap, &mapLength, &vehState, traj);
    genSpeedProfile(sparsePath, &densePath, &vehState, sparseToDenseMap, mapLength,
                    velParams, traj, &stateProb, &startNode);
    genTraj(&densePath, traj);
    calcDists(sparsePath);
    // plotPathDists(sparsePath, &densePath, sparseToDenseMap, -14);
  } else {
    densePath.pathLen = 0;  // FIXME: Check that old path isn't reused
    /// This will cause follower to stop the car
    traj->startDataInput();
    traj->addPoint(vehState.localX, 0, 0, vehState.localY, 0, 0);
    traj->setDirection(1);
  }


  //oldGenSpeedProfile(sparsePath, &vehState, &actState, stateProb);
  //oldGenTraj(sparsePath, &vehState, &actState, traj);
  
  //plotPath(sparsePath, -15);
  //plotTraj(traj, -15);
  plotPath(&densePath, startNode, -15);
  plotSpeed(sqrt(vehState.utmNorthVel*vehState.utmNorthVel + vehState.utmEastVel*vehState.utmEastVel), -15);
  //plotPaths(&densePath, sparsePath, -14);
  //plotMap(sparseToDenseMap, mapLength, -14);

  return VP_OK;
}


void VelPlanner::display(int sendSubgroup, CTraj* traj)
{
    MapElement me;
    int counter=140000;
    point2 point;
    vector<point2> points;
    MapId mapId;
    
    mapId = counter;
    for (int i=0; i<traj->getNumPoints(); i++) {
      point.set(traj->getNorthing(i),traj->getEasting(i));
      points.push_back(point);
    }
    me.setId(mapId);
    me.setTypePlanningTraj();
    me.setGeometry(points);
    m_meTalker.sendMapElement(&me,sendSubgroup);
}


void VelPlanner::plotPath(Path_t *path, int startNode, int sendSubGroup)
{
  MapElement me;
  point2 point;
  vector<point2> points;

  // Clear plot if there is no path
  if (path->pathLen <= 1 || path->path[0] == NULL) {
    for (int i = 0; i < 8; i++) {
      me.setId(i);
      me.setTypeClear();
      m_meTalker.sendMapElement(&me, sendSubGroup);
    }
    return;
  }

  /*// Steering angle commands
  for (int i = 0; i < path->pathLen; i++) {
    point.set((double)i, 10 * path->path[i]->steerAngle);
    points.push_back(point);
    }*/
  // Dists
  /*for (int i = 0; i < path->pathLen; i++) {
    point.set((double)i, path->dists[i]);
    points.push_back(point);
  }
  me.setColor(MAP_COLOR_ORANGE);
  me.setGeometry(points);
  me.setId(17);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup+1);
  me.setId(18);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup+1);*/

  // Vehicle headings
  points.clear();
  for (int i = 0; i < path->pathLen; i++) {
    double roll, pitch, yaw;
    quat_to_rpy(path->path[i]->pose.rot, &roll, &pitch, &yaw);
    point.set(path->dists[i]-path->dists[startNode], 10 * yaw);
    points.push_back(point);
  }
  me.setColor(MAP_COLOR_GREEN);
  me.setGeometry(points);
  me.setId(0);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  me.setId(1);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);

  // Curvatures
  points.clear();
  for (int i = 0; i < path->pathLen; i++) {
    point.set(path->dists[i]-path->dists[startNode], 10 / VEHICLE_MAX_AVG_STEER * atan(VEHICLE_WHEELBASE * path->curvatures[i]));
    points.push_back(point);
  }
  me.setColor(MAP_COLOR_RED);
  me.setGeometry(points);
  me.setId(2);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  me.setId(3);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);

  // Speeds
  points.clear();
  for (int i = 0; i < path->pathLen; i++) {
    point.set(path->dists[i]-path->dists[startNode], path->speeds[i]);
    points.push_back(point);
  }
  me.setColor(MAP_COLOR_CYAN);
  me.setGeometry(points);
  me.setId(4);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  me.setId(5);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);

  // Accelerations
  points.clear();
  for (int i = 0; i < path->pathLen; i++) {
    point.set(path->dists[i]-path->dists[startNode], 10 * path->accelerations[i]);
    points.push_back(point);
  }
  me.setColor(MAP_COLOR_BLUE);
  me.setGeometry(points);
  me.setId(6);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  me.setId(7);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);

}

void VelPlanner::plotSpeed(double speed, int sendSubGroup)
{
  MapElement me;
  point2 point;

  point.set(0.0, speed);
  me.setColor(MAP_COLOR_ORANGE);
  me.setGeometry(point);
  me.setId(72);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);
}

void VelPlanner::plotPaths(Path_t *path_1, Path_t *path_2, int sendSubGroup)
{
  MapElement me;
  point2 point;
  vector<point2> points;

  // Path 1
  for (int i = 0; i < path_1->pathLen; i++) {
    point.set(path_1->path[i]->pose.pos.x, path_1->path[i]->pose.pos.y);
    points.push_back(point);
  }
  me.setColor(MAP_COLOR_BLUE);
  me.setGeometry(points);
  me.setId(0);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  me.setId(1);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);

  // Path 2
  points.clear();
  for (int i = 0; i < path_2->pathLen; i++) {
    point.set(path_2->path[i]->pose.pos.x, path_2->path[i]->pose.pos.y);
    points.push_back(point);
  }
  me.setColor(MAP_COLOR_ORANGE);
  me.setGeometry(points);
  me.setId(2);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  /*me.setId(3);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);*/
}


void VelPlanner::plotTraj(CTraj *traj, int sendSubGroup)
{
  MapElement me;
  point2 point;
  vector<point2> points;

  // Steering commands
  for (int i = 0; i < traj->getNumPoints(); i++) {
    TrajPoint trajPoint = traj->interpolGetPoint(i, 0.0);
    double steerCommand = atan(VEHICLE_WHEELBASE * (trajPoint.nd*trajPoint.edd - trajPoint.ed*trajPoint.ndd) /
                               pow(trajPoint.nd*trajPoint.nd + trajPoint.ed*trajPoint.ed, 1.5)) / VEHICLE_MAX_AVG_STEER;
    point.set((double)i*0.05, 10 * steerCommand);
    points.push_back(point);
  }
  me.setColor(MAP_COLOR_MAGENTA);
  me.setGeometry(points);
  me.setId(100);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  me.setId(101);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);
}

void VelPlanner::plotMap(int *map, int length, int sendSubGroup)
{
  MapElement me;
  point2 point;
  vector<point2> points;

  for (int i = 0; i < length; i++) {
    point.set(i, map[i]);
    points.push_back(point);
  }
  me.setColor(MAP_COLOR_ORANGE);
  me.setGeometry(points);
  me.setId(21);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  me.setId(22);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);

}

void VelPlanner::plotPathDists(Path_t *sparsePath, Path_t *densePath, int *sparseToDenseMap, int sendSubGroup)
{
  MapElement me;
  point2 point;
  vector<point2> sparseVec;
  vector<point2> denseVec;

  // Dists
  for (int i = 0; i < sparsePath->pathLen; i++) {
    point.set((double)i, sparsePath->dists[i]);
    sparseVec.push_back(point);
    point.set((double)i, densePath->dists[sparseToDenseMap[i]]);
    denseVec.push_back(point);
  }

  me.setColor(MAP_COLOR_ORANGE);
  me.setGeometry(sparseVec);
  me.setId(17);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  me.setId(18);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);

  me.setColor(MAP_COLOR_GREEN);
  me.setGeometry(denseVec);
  me.setId(19);
  me.setTypeLine();
  m_meTalker.sendMapElement(&me, sendSubGroup);
  me.setId(20);
  me.setTypePoints();
  m_meTalker.sendMapElement(&me, sendSubGroup);
}



Err_t VelPlanner::calcDists(Path_t *path)
{
  path->dists[0] = 0.0;

  for (int i = 1; i < path->pathLen; i++) {
    double dist = sqrt(square(path->path[i]->pose.pos.x - path->path[i - 1]->pose.pos.x)
                       + square(path->path[i]->pose.pos.y - path->path[i - 1]->pose.pos.y));
    path->dists[i] = path->dists[i - 1] + dist;
  }
  return VP_OK;
}



Err_t VelPlanner::densifyPath(const Path_t *sparsePath, Path_t *densePath,
                              int *sparseToDenseMap, int *mapLength,
                              const VehicleState *vehState, CTraj *traj)
{
  Pose2D poseA, poseB;
  GraphNode *nodeA, *nodeB;
  double roll, pitch, yaw;
  Maneuver *maneuver;
  double startSpeed;

  // Create vehicle model
  Vehicle vehicle;
  vehicle.max_curvature = tan(m_kin.maxSteer)/m_kin.wheelBase;
  vehicle.wheelbase = m_kin.wheelBase;
  vehicle.steerlimit = m_kin.maxSteer;

  // Get reference speed of beginning of this
  // path projected onto old traj.
  {
    TrajPoint trajPoint = traj->interpolGetClosest(sparsePath->path[0]->pose.pos.x,
                                                   sparsePath->path[0]->pose.pos.y,
                                                   NULL, NULL);
    startSpeed = sqrt(trajPoint.nd * trajPoint.nd + trajPoint.ed * trajPoint.ed);
  }
  
  // Current speed of Alice
  double curSpeed = sqrt(vehState->utmNorthVel*vehState->utmNorthVel + vehState->utmEastVel*vehState->utmEastVel);
  
  // Speed to usen when deciding how far to plan
  double planSpeed = MAX(curSpeed, startSpeed);

  // Don't densify the path longer than the feed forward will look ahead
  double denseDist = planSpeed*1.0 + 3.0;

  // Don't plan furhter than the smallest distance in which Alice can stop
  /*double planDist = planSpeed*planSpeed / (-m_params.maxDecel) / 2.0 + 3.0;
  if (planDist < denseDist) {
    planDist = denseDist;
    }*/
  // For now, make planDist really big so planning uses the entire sparsePath
  double planDist = 1000;

  // Spacing between two points in densified path [m]
  double stepSize = 0.1;

  int newNodeCount = 0;  // Number of used nodes in m_graphNodes
  densePath->dists[0] = 0;
  int sparseIndex = 0;
  int denseIndex = 0;
  sparseToDenseMap[sparseIndex] = denseIndex;
  densePath->path[denseIndex] = sparsePath->path[sparseIndex];
  sparseIndex++;
  denseIndex++;
  while (densePath->dists[denseIndex - 1] < denseDist && sparseIndex < sparsePath->pathLen) {
    // Nodes to generate maneuver between
    nodeA = sparsePath->path[sparseIndex - 1];
    nodeB = sparsePath->path[sparseIndex];

    //// Start and end poses of maneuver
    // For non-vehicle nodes, use the node pose and assume zero-steering angle
    quat_to_rpy(nodeA->pose.rot, &roll, &pitch, &yaw);
    poseA.x = nodeA->pose.pos.x;
    poseA.y = nodeA->pose.pos.y;
    poseA.theta = yaw;
    // Final pose on node B
    quat_to_rpy(nodeB->pose.rot, &roll, &pitch, &yaw);
    poseB.x = nodeB->pose.pos.x;
    poseB.y = nodeB->pose.pos.y;
    poseB.theta = yaw;
    
    // Approximated length of curren maneuver
    double maneuverLength = sqrt((poseB.x-poseA.x)*(poseB.x-poseA.x) + (poseB.y-poseA.y)*(poseB.y-poseA.y));

    // Number of sampling points on this maneuver
    int numSteps = (int) ceil(maneuverLength / stepSize);

    // Generate maneuver
    maneuver = maneuver_pose2pose(&vehicle, &poseA, &poseB);
    for (int i = 1; i < numSteps; i++) {
      GraphNode *newNode = &m_graphNodes[newNodeCount++];
      Pose2D pose = maneuver_evaluate_pose(&vehicle, maneuver, ((double)i)/numSteps);
      newNode->pose = pose3_set(vec3_set(pose.x, pose.y, 0),
                                quat_from_ypr(pose.theta, 0, 0));
      densePath->path[denseIndex] = newNode;
      densePath->dists[denseIndex] = densePath->dists[denseIndex - 1]
        + sqrt((densePath->path[denseIndex]->pose.pos.x
                - densePath->path[denseIndex - 1]->pose.pos.x)
               * (densePath->path[denseIndex]->pose.pos.x
                  - densePath->path[denseIndex - 1]->pose.pos.x)
               + (densePath->path[denseIndex]->pose.pos.y
                  - densePath->path[denseIndex - 1]->pose.pos.y)
               * (densePath->path[denseIndex]->pose.pos.y
                  - densePath->path[denseIndex - 1]->pose.pos.y));
      denseIndex++;
    }
    maneuver_free(maneuver);
    sparseToDenseMap[sparseIndex] = denseIndex;
    densePath->path[denseIndex] = sparsePath->path[sparseIndex];
    densePath->dists[denseIndex] = densePath->dists[denseIndex - 1]
      + sqrt((densePath->path[denseIndex]->pose.pos.x
              - densePath->path[denseIndex - 1]->pose.pos.x)
             * (densePath->path[denseIndex]->pose.pos.x
                - densePath->path[denseIndex - 1]->pose.pos.x)
             + (densePath->path[denseIndex]->pose.pos.y
                - densePath->path[denseIndex - 1]->pose.pos.y)
             * (densePath->path[denseIndex]->pose.pos.y
                - densePath->path[denseIndex - 1]->pose.pos.y));
    denseIndex++;

    sparseIndex++;
  }
  
  // Copy the non-densified part of the path
  while (densePath->dists[denseIndex - 1] < planDist && sparseIndex < sparsePath->pathLen) {
    sparseToDenseMap[sparseIndex] = denseIndex;
    densePath->path[denseIndex] = sparsePath->path[sparseIndex];
    densePath->dists[denseIndex] = densePath->dists[denseIndex - 1]
      + sqrt((densePath->path[denseIndex]->pose.pos.x
              - densePath->path[denseIndex - 1]->pose.pos.x)
             * (densePath->path[denseIndex]->pose.pos.x
                - densePath->path[denseIndex - 1]->pose.pos.x)
             + (densePath->path[denseIndex]->pose.pos.y
                - densePath->path[denseIndex - 1]->pose.pos.y)
             * (densePath->path[denseIndex]->pose.pos.y
                - densePath->path[denseIndex - 1]->pose.pos.y));
    denseIndex++;
    sparseIndex++;
  }

  if (sparseIndex < sparsePath->pathLen) {
    sparseToDenseMap[sparseIndex] = denseIndex;
    densePath->path[denseIndex] = sparsePath->path[sparseIndex];
    densePath->dists[denseIndex] = densePath->dists[denseIndex - 1]
      + sqrt((densePath->path[denseIndex]->pose.pos.x
              - densePath->path[denseIndex - 1]->pose.pos.x)
             * (densePath->path[denseIndex]->pose.pos.x
                - densePath->path[denseIndex - 1]->pose.pos.x)
             + (densePath->path[denseIndex]->pose.pos.y
                - densePath->path[denseIndex - 1]->pose.pos.y)
             * (densePath->path[denseIndex]->pose.pos.y
                - densePath->path[denseIndex - 1]->pose.pos.y));
    denseIndex++;
    sparseIndex++;
  }
  
  *mapLength = sparseIndex;
  densePath->pathLen = denseIndex;

  //// Calculate curvatures
  double curvatures[1000]; //FIXME: remove? change size?
  // Anlges of lines between nodes
  double prevEdgeAng, nextEdgeAng;
  double curX, curY, nextX, nextY;
  double distToPrev, distToNext;
  double curYaw;
  double prevCurv, nextCurv;
  int i;

  curX = densePath->path[0]->pose.pos.x;
  curY = densePath->path[0]->pose.pos.y;
  nextX = densePath->path[1]->pose.pos.x;
  nextY = densePath->path[1]->pose.pos.y;

  distToNext = densePath->dists[1] - densePath->dists[0];

  quat_to_rpy(densePath->path[0]->pose.rot, &roll, &pitch, &yaw);
  curYaw = yaw;
    
  nextEdgeAng = atan2(nextY - curY, nextX - curX);

  nextCurv = 2.0 * sin(nextEdgeAng - curYaw) / distToNext;

  curvatures[0] = nextCurv;

  for (i = 1; i < densePath->pathLen - 1; i++) {
    curX = nextX;
    curY = nextY;
    prevEdgeAng = nextEdgeAng;
    distToPrev = distToNext;

    nextX = densePath->path[i + 1]->pose.pos.x;
    nextY = densePath->path[i + 1]->pose.pos.y;

    distToNext = densePath->dists[i + 1] - densePath->dists[i];
    
    quat_to_rpy(densePath->path[i]->pose.rot, &roll, &pitch, &yaw);
    curYaw = yaw;
    
    nextEdgeAng = atan2(nextY - curY, nextX - curX);
    
    prevCurv = 2.0 * sin(curYaw - prevEdgeAng) / distToPrev;
    nextCurv = 2.0 * sin(nextEdgeAng - curYaw) / distToNext;

    //curvatures[i] = fabs(prevCurv) > fabs(nextCurv) ? prevCurv : nextCurv;
    curvatures[i] = (prevCurv + nextCurv) / 2.0;
  }
  prevEdgeAng = nextEdgeAng;
  distToPrev = distToNext;
  quat_to_rpy(densePath->path[i]->pose.rot, &roll, &pitch, &yaw);
  curYaw = yaw;
  prevCurv = 2.0 * sin(curYaw - prevEdgeAng) / distToPrev;
  curvatures[i] = prevCurv;


  // Smooth curvatures
  for (i = 0; i < densePath->pathLen; i++) {
    const double avgDist = 1.0; //FIXME magic
    int iMin, iMax;
    double sum = 0;
    for (iMax = i;
         iMax < densePath->pathLen && (densePath->dists[iMax] - densePath->dists[i]) <= avgDist;
         iMax++) {
      sum += curvatures[iMax];
    }
    for (iMin = i - 1;
         iMin >= 0 && (densePath->dists[i] - densePath->dists[iMin]) < avgDist;
         iMin--) {
      sum += curvatures[iMin];
    }
    densePath->curvatures[i] = sum / (iMax - iMin - 1);
    
  }


  return VP_OK;
}


Err_t VelPlanner::genSpeedProfile(const Path_t *sparsePath, Path_t *densePath,
                                  const VehicleState *vehState, const int *sparseToDenseMap, int mapLength,
                                  Vel_params_t velParams, CTraj *traj, StateProblem_t *stateProb, int *returnStartNode)
{
  // Remembers if speed has been below STOP_SPEED
  // within STOP_DIST_TOL while state == STOP_INT
  static bool isStopped = false;

  // Index of node on densePath closest to Alice's rear axle
  int closestNode;
  // Index of closest node on densePath behind Alice
  int startNode;
  // Reference speed at startNode
  double startSpeed;

  // Calculate current speed
  double curSpeed = sqrt(vehState->utmNorthVel*vehState->utmNorthVel + vehState->utmEastVel*vehState->utmEastVel);

  // Decide if the velocity planning should start from the current
  // speed or the reference speed from the previous control cycle.
  // FIXME: this should be decided in follower
  if (fabs(curSpeed - startSpeed) > REPLAN_SPEED_ERR
      || curSpeed >= velParams.maxSpeed) {
    startSpeed = curSpeed;
  }

  double maxSpeed = 0.9 * velParams.maxSpeed;

  // Limit speed based on lateral error
  const double MIN_CORR_DIST = 0.3;
  const double MAX_CORR_DIST = 1.5;
  const double MIN_CORR_SPEED = 2.0;
  const double MAX_CORR_SPEED = 15.0;
  double distToPath;
  double distToPathSquared = 1000000000; // 25 trips around the world
  closestNode = densePath->pathLen - 1;
  for (int i = 0; i < densePath->pathLen; i++) {
    double nextDistToPathSquared = (pow(vehState->localX - densePath->path[i]->pose.pos.x, 2)
                                    + pow(vehState->localY - densePath->path[i]->pose.pos.y, 2));
    if (nextDistToPathSquared < distToPathSquared) {
      distToPathSquared = nextDistToPathSquared;
    } else {
      distToPath = sqrt(distToPathSquared);
      closestNode = MAX(0, i - 1);
      break;
    }
  }
  if (distToPath > MIN_CORR_DIST) {
    double newMax = MAX_CORR_SPEED 
      - (distToPath - MIN_CORR_DIST)*(MAX_CORR_SPEED - MIN_CORR_SPEED)/(MAX_CORR_DIST - MIN_CORR_DIST);
    if (newMax < MIN_CORR_SPEED) {
      newMax = MIN_CORR_SPEED;
    }
    if (newMax < maxSpeed) {
      maxSpeed = newMax;
    }
  }

  // Determine which is the closest node behind Alice
  {
    double roll, pitch, yaw;
    quat_to_rpy(densePath->path[closestNode]->pose.rot, &roll, &pitch, &yaw);
    double x = densePath->path[closestNode]->pose.pos.x;
    double y = densePath->path[closestNode]->pose.pos.y;
    double scalProd = (vehState->localX - x)*cos(yaw) + (vehState->localY - y)*sin(yaw);
    if (scalProd >= 0) {
      startNode = closestNode;
    } else {
      startNode = MAX(0, closestNode - 1);
    }
    *returnStartNode = startNode;
  }

  // Get reference speed of startNode projected onto old traj.
  {
    TrajPoint trajPoint = traj->interpolGetClosest(densePath->path[startNode]->pose.pos.x,
                                                   densePath->path[startNode]->pose.pos.y,
                                                   NULL, NULL);
    startSpeed = sqrt(trajPoint.nd * trajPoint.nd + trajPoint.ed * trajPoint.ed);
  }

  
  // Set speed of all nodes but the first to maximum allowed in this segment
  // Also allow car to decelerate if needed
  {
    double decelDist = (maxSpeed*maxSpeed - curSpeed*curSpeed) / 2.0 / m_params.maxDecel;
    for (int i = densePath->pathLen - 1; densePath->dists[i] >= decelDist && i > 0; i--) {
      densePath->speeds[i] = maxSpeed;
    }
  }
  
  // Set speed of startNode and all nodes before it
  for (int i = 0; i <= startNode; i++) {
    densePath->speeds[i] = startSpeed;
  }

  // Make sure to always be able to stop at the end of the path
  if (true /*startSpeed*startSpeed / 2.0
      / (densePath->dists[densePath->pathLen - 1] - densePath->dists[startNode])
      > -m_params.maxDecel*/) {
    densePath->speeds[densePath->pathLen - 1] = ALMOST_ZERO_SPEED;
  }

  // Limit speed in intersections
  {
    int curr = startNode + 1;
    for (int i = 0; i < sparsePath->pathLen; i++) {
      if (sparsePath->path[i]->type & GRAPH_NODE_TURN) {
        int dest = sparseToDenseMap[MIN(sparsePath->pathLen - 1, i + 1)] - 1;
        for ( ; curr <=dest; curr++) {
          if (densePath->speeds[curr] > m_params.maxIntersectionSpeed) {
            densePath->speeds[curr] = m_params.maxIntersectionSpeed;
          }
        }
      } else {
        curr = MAX(startNode + 1, sparseToDenseMap[MAX(0, i - 1)] + 1);
      }
    }
  }

  // Limit speed based on curvature
  for (int i = startNode + 1; i < densePath->pathLen; i++) {
    double curv = fabs(densePath->curvatures[i]);
    if (curv != 0) {
      curv = MIN(curv, 1/VEHICLE_MIN_TURNING_RADIUS);
      double maxSpeed = sqrt(m_params.maxLatAccel / curv);
      if (maxSpeed < densePath->speeds[i]) {
        densePath->speeds[i] = maxSpeed;
      }
    }
  }

  // Limit speed to give the wheels time to turn
  for (int i = startNode + 1; i < densePath->pathLen - 1; i++) {
    double dist = densePath->dists[i + 1] - densePath->dists[i];
    double deltaPhi = fabs(atan(VEHICLE_WHEELBASE * densePath->curvatures[i + 1])
                           - atan(VEHICLE_WHEELBASE * densePath->curvatures[i + 1]));
    double maxSpeed = dist / (deltaPhi / m_params.maxSteerRate);
    if (maxSpeed < densePath->speeds[i]) {
      densePath->speeds[i] = maxSpeed;
    }
  }

  // Pause
  if (stateProb->state == PAUSE) {

    double stopping_dist = startSpeed*startSpeed/(-2.0*m_params.maxDecel);
    for (int i = 0; i<densePath->pathLen; i++) {
      densePath->speeds[i] = ALMOST_ZERO_SPEED;
    }

    /*
    for (int i = densePath->pathLen - 1;
         (densePath->dists[i] - densePath->dists[startNode]) >= stopping_dist && i >= 0;
         i--) {
      densePath->speeds[i] = ALMOST_ZERO_SPEED;
    }
    */
  }

  // find the closest obstacle in our path and plan to stop before it
  if (stateProb->state == STOP_OBS) {
    double obsDist = DBL_MAX;
    for (int i = 0; i < mapLength; i++) {
      GraphNode *node;
      node = sparsePath->path[i];
      if (node->collideObs) {
        obsDist = densePath->dists[sparseToDenseMap[MAX(0, i - 1)]] - VEHICLE_LENGTH;
        break;
      }
    }
    if (obsDist < DBL_MAX){
      for (int i = densePath->pathLen - 1; densePath->dists[i] >= obsDist && i > startNode;i--){
        densePath->speeds[i] = ALMOST_ZERO_SPEED;
      }
    }
  }

  // find the closest car in our path and plan to match the vel
  if (sparsePath->collideCar) {
    double obsDist = DBL_MAX;
    for (int i = 0; i < mapLength; i++) {
      GraphNode *node;
      node = sparsePath->path[i];
      if (node->collideCar) {
        obsDist = densePath->dists[sparseToDenseMap[MAX(0, i - 1)]] - VEHICLE_LENGTH;
        break;
      }
    }
    if (obsDist < DBL_MAX){
      for (int i = densePath->pathLen - 1; densePath->dists[i] >= obsDist && i > startNode;i--){
        densePath->speeds[i] = ALMOST_ZERO_SPEED;
      }
    }
  }

  // Find the first stop sign.  
  if (stateProb->state == STOP_INT) {
    if (isStopped) {
      // If Alice has stopped clos enough to stop line,
      // keep her stopped to prevent bunnuy hopping
      for (int i = startNode; i < densePath->pathLen; i++){
        densePath->speeds[i] = ALMOST_ZERO_SPEED;
      }
    } else {
      double pathStopDist;
      double pathStopLineDist;
      double aliceStopLineDist;
      for (int i = 0; i < mapLength; i++) {
        GraphNode *node;
        node = sparsePath->path[i];
        if (node->isStop) {
          // Compute distances
          pathStopLineDist = densePath->dists[sparseToDenseMap[i]] + node->distToStop - DIST_REAR_AXLE_TO_FRONT;
          pathStopDist = pathStopLineDist - 0.2;
          aliceStopLineDist = pathStopLineDist - densePath->dists[startNode];
          
          // Set speed of nodes after stop point to (almost) zero
          for (int j = densePath->pathLen - 1; densePath->dists[j] >= pathStopDist && j > startNode; j--){
            densePath->speeds[j] = ALMOST_ZERO_SPEED;
          }
          
          // Check if Alice has stopped
          if (curSpeed < STOP_SPEED && aliceStopLineDist < STOP_DIST_TOL) {
            isStopped = true;
          }

          break;
        }
      }
    }
  } else {
    // Reset isStopped
    isStopped = false;
  }

  // If accelerating, make sure the reference speed is at least 0.25
  if (densePath->speeds[startNode + 1] > densePath->speeds[startNode] && densePath->speeds[startNode] < 0.25) {
    for (int i = 0; i <= startNode; i++) {
      densePath->speeds[i] = 0.25;
    }
  }

  // Smooth out the accelerations
  for (int i = 0; i < densePath->pathLen - 1; i++)
  {
    double d0, d1, s0, s1, acc, speed;
    
    d0 = densePath->dists[i + 0];
    d1 = densePath->dists[i + 1];    
    s0 = densePath->speeds[i + 0];    
    s1 = densePath->speeds[i + 1];

    assert(d1 >= d0);

    // Give smaller accelerations at low speeds to get comfy start
    double locMaxAccel;
    if (s0 < 1.0) {
      locMaxAccel = m_params.maxAccel / 2;
    } else {
      locMaxAccel = m_params.maxAccel;
    }
    
    // Compute acceleration; if it is high, trim the speed
    acc = (s1 * s1 - s0 * s0) / (d1 - d0) / 2;
    if (acc > locMaxAccel)
    {
      acc = locMaxAccel;
      speed = sqrt(2 * acc * (d1 - d0) + s0 * s0);
      // MSG("%f %f %f   %f %f", d1 - d0, s1 - s0, s1, acc, speed);
      assert(speed <= s1);
      densePath->speeds[i + 1] = speed;
    }
    densePath->accelerations[i] = acc;
  }
  densePath->accelerations[densePath->pathLen - 1] = 0;

  // Smooth out the decelerations
  {
    double maxDecel = m_params.maxDecel;
    for (int i = densePath->pathLen - 2; i > startNode; i--) {
      // Check if deceleration is too big
      // Remark: maxDecel < 0
      if (densePath->accelerations[i] < maxDecel) {
        // Change deceleration for next edge
        densePath->accelerations[i] = maxDecel;
        // Calculate new speed
        double distToNext  = densePath->dists[i + 1] - densePath->dists[i];
        double nextSpeed = densePath->speeds[i + 1];
        double newSpeed = sqrt(nextSpeed*nextSpeed - 2.0 * maxDecel * distToNext);
        densePath->speeds[i] = newSpeed;
        // Calculate new deceleration for previous edge
        double distToPrev = densePath->dists[i] - densePath->dists[i - 1];
        double prevSpeed = densePath->speeds[i - 1];
        double newPrevDecel = (newSpeed*newSpeed - prevSpeed*prevSpeed) / 2.0 / distToPrev;
        densePath->accelerations[i - 1] = newPrevDecel;
      }

      // Check if maximum deceleration has to be increased
      // Calculate deceleration required from first node to here
      double firstSpeed = densePath->speeds[0];
      double thisSpeed = densePath->speeds[i];
      double reqDecel = (thisSpeed*thisSpeed - firstSpeed*firstSpeed)
        / 2.0 / (densePath->dists[i] - densePath->dists[startNode]);
      if (reqDecel < maxDecel) {
        maxDecel = reqDecel;
      }

      // If the required deceleration is bigger than can be actuated,
      // increase velocities to give a nearly feasible trajectory.
      if (reqDecel < m_params.maxEmergencyDecel) {
        i--;
        for (; i > startNode; i--) {
          densePath->speeds[i] = sqrt(firstSpeed*firstSpeed 
                                      + 2.0*reqDecel*(densePath->dists[i] - densePath->dists[startNode]));
          densePath->accelerations[i] = reqDecel;
        }
        densePath->accelerations[0] = reqDecel;
        break;
      }
    }
  }

  return VP_OK;
}


Err_t VelPlanner::genTraj(const Path_t *path, CTraj *traj)
{
  traj->startDataInput();
  for (int i = 0; i < path->pathLen; i++) {
    double speed = path->speeds[i];
    double roll, pitch, yaw;
    quat_to_rpy(path->path[i]->pose.rot, &roll, &pitch, &yaw);    
    traj->addPoint(path->path[i]->pose.pos.x,
                   speed * cos(yaw),
                   path->accelerations[i] * cos(yaw)
                     - speed * speed * path->curvatures[i] * sin(yaw),
                   path->path[i]->pose.pos.y,
                   speed * sin(yaw),
                   path->accelerations[i] * sin(yaw)
                     + speed * speed * path->curvatures[i] * cos(yaw));
  }

  if ((path != NULL) && (path->path[0] != NULL)) {
    if (path->path[0]->pathDir == GRAPH_PATH_FWD)
      traj->setDirection(1);
    else 
      traj->setDirection(-1);
  }

  return VP_OK;
}


/*Err_t VelPlanner::oldGenSpeedProfile(Path_t *path,
                                     const VehicleState *vehState,
                                     const ActuatorState *actState,
                                     StateProblem_t stateProb)
{
  int i;
  double currentSpeed;
  double stopDist, dist, obsDist;
  
  // Get the vehicle speed.
  currentSpeed = sqrt(pow(vehState->utmNorthVel,2) + pow(vehState->utmEastVel, 2));
  
  // If there is no path, there is nothing to do
  if (path->pathLen <= 0)
    return VP_OK;

  // Zeroth pass: walk along the path and construct the distance
  // profile.
  path->dists[0] = 0;
  for (i = 1; i < path->pathLen; i++) 
  {
    GraphNode *nodeA, *nodeB;
    double dx, dy, dd;
    nodeA = path->path[i - 1];
    nodeB = path->path[i + 0];
    dx = nodeB->pose.pos.x - nodeA->pose.pos.x;
    dy = nodeB->pose.pos.y - nodeA->pose.pos.y;
    dd = sqrt(dx * dx + dy * dy);
    assert(dd > 0);
    path->dists[i] = path->dists[i - 1] + dd;
  }

  // First pass: walk along the path and construct the nominal speed profile.
  // We give a small initial acceleration to get Alice moving.
  for (i = 0; i < path->pathLen; i++) 
  {
    GraphNode *node;
    node = path->path[i];
    if (i == 0)
      path->speeds[i] = MIN(currentSpeed + m_params.deltaSpeed, m_params.maxLaneSpeed);
    else
      path->speeds[i] = m_params.maxLaneSpeed;
  }
  
  // Second pass: stop if there is a collision on this path.  TODO: this should
  // produce a soft stop.
  //  if (path->collideObs >= GRAPH_COLLIDE_MAP_INNER && path->goalDist < 100) // MAGIC
  // {
  //  MSG("path has collision; hard stop");
  //  for (i = 0; i < path->pathLen; i++)
  //    path->speeds[i] = 0;
  // }

  // Third pass: walk along the path and construct the max speed
  // profile.
  for (i = 0; i < path->pathLen; i++) 
  {
    GraphNode *node;
    double speed;
      
    node = path->path[i];
    speed = path->speeds[i];

    // Check for corridor/lane speeds
    if (node->type != GRAPH_NODE_TURN && node->segmentId > 0)
    {
      // If driving in a segment, consider the maximum speed for this
      // segment.  The miminum speed is currently ignored.
      //double minSpeed = 0;
      double maxSpeed = m_params.maxLaneSpeed;
      // mdf.getSpeedLimits(node->segmentId, &minSpeed, &maxSpeed);
      if (maxSpeed > 0)
        speed = MIN(speed, maxSpeed);
    }
    else
    {
      // Are we driving through an intersection?  If so, use the
      // intersection speed.
      speed = MIN(speed, m_params.maxTurnSpeed);
    }
    path->speeds[i] = speed;
  }

  // Pause
  #define PAUSE_DECELERATION 0.5
  if (PAUSE == stateProb.state) {
    for (i = 0; i < path->pathLen; i++) {
      double stopping_dist = sqrt(currentSpeed*currentSpeed/(2*PAUSE_DECELERATION));
      if (path->dists[i] >= stopping_dist) {
        path->speeds[i] = 0;
      }
    }
  }
  
  // find the closest obstacle in our path and plan to stop before it
  if (STOP_OBS == stateProb.state || UTURN == stateProb.state) {
    obsDist = DBL_MAX;
    for (i = 0; i < path->pathLen; i++) {
      GraphNode *node;
      node = path->path[i];
      if (node->collideObs) {
        obsDist = path->dists[i] - 2*VEHICLE_LENGTH - DIST_REAR_AXLE_TO_FRONT;
        break;
      }
    }
    if (obsDist < DBL_MAX){
      for (i = 0; i < path->pathLen; i++){
        dist = obsDist-path->dists[i];
        if (dist < 0)
          path->speeds[i] = 0;
      }
    }
  }

  // find the closest car in our path and plan to match the vel
  if (path->collideCar) {
    obsDist = DBL_MAX;
    for (i = 0; i < path->pathLen; i++) {
      GraphNode *node;
      node = path->path[i];
      if (node->collideCar) {
        obsDist = path->dists[i] - VEHICLE_LENGTH - DIST_REAR_AXLE_TO_FRONT;
        break;
      }
    }
    if (obsDist < DBL_MAX){
      for (i = 0; i < path->pathLen; i++){
        dist = obsDist-path->dists[i];
        if (dist < 0)
          path->speeds[i] = path->path[i]->carVel;
      }
    }
  }

  // Find the first stop sign.  
  if (STOP_INT == stateProb.state) {
    stopDist = DBL_MAX;
    for (i = 0; i < path->pathLen; i++) {
      GraphNode *node;
      node = path->path[i];
      if (node->isStop) {
        stopDist = path->dists[i] - DIST_REAR_AXLE_TO_FRONT - 0.2;
        break;
      }
    }
    
    if (stopDist < DBL_MAX) {
      for (i = 0; i < path->pathLen; i++){
        dist = stopDist-path->dists[i];
        if (dist < m_params.stopTol)
          path->speeds[i] = 0;
      }
    }
  }

  // when we get close to the end of traj, slow us down
  if (path->dists[path->pathLen-1] < 10)
    path->speeds[path->pathLen-1] = 0;


  // Smooth out the accelerations
  for (i = 0; i < path->pathLen - 1; i++)
  {
    double d0, d1, s0, s1, acc, speed;
    
    d0 = path->dists[i + 0];
    d1 = path->dists[i + 1];    
    s0 = path->speeds[i + 0];    
    s1 = path->speeds[i + 1];

    assert(d1 > d0);
    
    // Compute acceleration; if it is high, trim the speed
    acc = (s1 * s1 - s0 * s0) / (d1 - d0) / 2;
    if (acc > m_params.maxAccel)
    {
      acc = m_params.maxAccel;
      speed = sqrt(2 * acc * (d1 - d0) + s0 * s0);
      // MSG("%f %f %f   %f %f", d1 - d0, s1 - s0, s1, acc, speed);
      assert(speed <= s1);
      path->speeds[i + 1] = MIN(path->speeds[i + 1], speed);
    }
  }

  // Smooth out the deccelerations
  for (i = path->pathLen - 1; i > 0; i--)
  {
    double d0, d1, s0, s1, dec, speed;
    
    d0 = path->dists[i + 0];
    d1 = path->dists[i - 1];    
    s0 = path->speeds[i + 0];    
    s1 = path->speeds[i - 1];

    assert(d1 < d0);
    
    // Compute acceleration; if it is high, trim the speed
    dec = (s1 * s1 - s0 * s0) / (d1 - d0) / 2;
    if (dec < m_params.maxDecel)
    {
      dec = m_params.maxDecel;
      speed = sqrt(2 * dec * (d1 - d0) + s0 * s0);
      //MSG("%f %f %f   %f %f", d1 - d0, s1 - s0, s1, dec, speed);
      assert(speed <= s1);
      path->speeds[i - 1] = MIN(path->speeds[i - 1], speed);
    }
  }

  return VP_OK;
}

// Generate a vehicle trajectory.
Err_t VelPlanner::oldGenTraj(const Path_t *path,
                         const VehicleState *vehState,
                         const ActuatorState *actState,                           
                         CTraj *traj)
{
  int i, numSteps, numSpeeds;
  double roll, pitch, yaw;
  Vehicle *vp;
  Maneuver *maneuver;
  GraphNode *nodeA, *nodeB;
  double speeds[2], dists[2];
  VehicleConfiguration configA;  
  Pose2D poseA, poseB;
  double stepSize;

  // Spacing between trajectory points
  stepSize = 0.05; // MAGIC

  // Make sure we have a path
  if (path->pathLen < 2)
  {
    MSG("no valid destination, stopping vehicle");
    traj->startDataInput();
    traj->addPoint(vehState->localX, 0, 0,
                   vehState->localY, 0, 0);
    return VP_OK;
  }

  assert(path->pathLen >= 1);
  
  // Vehicle properties
  vp = maneuver_create_vehicle(kin.wheelBase, kin.maxSteer);
      
  traj->startDataInput();

  // Start with the vehicle node
  nodeA = path->path[0];
  speeds[0] = path->speeds[0];
  dists[0] = path->dists[0];

  // Look for the first non-volatile node; this is the destination for the
  // first maneuver
  nodeB = NULL;
  for (i = 1; i < path->pathLen; i++)
  {
    nodeB = path->path[i];
    speeds[1] = path->speeds[i];
    dists[1] = path->dists[i];
    if (nodeB->type != GRAPH_NODE_VOLATILE)
      break;
  }
  assert(nodeB != NULL);

  // Compute the number of speeds to consider along this maneuver.
  numSpeeds = i + 1;
    
  // Compute the number of interpolation steps, based on the path
  // distance between the two nodes.
  numSteps = (int) ((dists[1] - dists[0]) / stepSize) + 1;

  // Use the initial configuration given by the state.
  // This causes oscillations.
  //configA.x = vehState->utmNorthing;
  //configA.y = vehState->utmEasting;
  //configA.theta = vehState->utmYaw;
  //configA.phi = actState->m_steerpos * kin.maxSteer;
  //speedA = vehState->vehSpeed;

  // Use the initial configuration given by the node.
  // This produces stable behavior at low speeds
  quat_to_rpy(nodeA->pose.rot, &roll, &pitch, &yaw);
  configA.x = nodeA->pose.pos.x;
  configA.y = nodeA->pose.pos.y;
  configA.theta = yaw;
  configA.phi = nodeA->steerAngle;

  quat_to_rpy(nodeB->pose.rot, &roll, &pitch, &yaw);
  poseB.x = nodeB->pose.pos.x;
  poseB.y = nodeB->pose.pos.y;
  poseB.theta = yaw;
    
  // Generate a trajectory from the maneuver.
  maneuver = maneuver_config2pose(vp, &configA, &poseB);
  maneuver_profile_single(vp, maneuver, numSpeeds, path->speeds, numSteps, traj);
  maneuver_free(maneuver);
  
  // Create the rest of the maneuvers
  for (; i < path->pathLen - 1; i++) 
  {
    nodeA = path->path[i + 0];
    nodeB = path->path[i + 1];

    speeds[0] = path->speeds[i + 0];
    speeds[1] = path->speeds[i + 1];

    dists[0] = path->dists[i + 0];
    dists[1] = path->dists[i + 1];
          
    // Compute the number of interpolation steps, based on the path
    // distance between the two nodes.
    numSteps = (int) ((dists[1] - dists[0]) / stepSize) + 1;

    // For non-vehicle nodes, use the node pose and assume zero-steering angle
    quat_to_rpy(nodeA->pose.rot, &roll, &pitch, &yaw);
    poseA.x = nodeA->pose.pos.x;
    poseA.y = nodeA->pose.pos.y;
    poseA.theta = yaw;

    // Final pose on node B
    quat_to_rpy(nodeB->pose.rot, &roll, &pitch, &yaw);
    poseB.x = nodeB->pose.pos.x;
    poseB.y = nodeB->pose.pos.y;
    poseB.theta = yaw;

    // Generate a trajectory from the maneuver list.
    maneuver = maneuver_pose2pose(vp, &poseA, &poseB);
    maneuver_profile_generate(vp, 1, &maneuver, speeds, numSteps, traj);
    maneuver_free(maneuver);
 
    // Dont plan too far into the future
    if (dists[1] > 40.0) // MAGIC
      break;
  }

  traj->setDirection(1);
  return VP_OK;
}*/


void VelPlanner::printMatlab(CTraj* traj)
{
  ofstream file_out;
  file_out.open("plot_graph.m");
  file_out<< "clear all; close all;" << endl;
  
  if (1) {
    ostringstream x, y, dx, dy, ddx, ddy;
    x << "traj_x = [ ";
    y << "traj_y = [ ";
    dx << "traj_dx = [ ";
    dy << "traj_dy = [ ";
    ddx << "traj_ddx = [ ";
    ddy << "traj_ddy = [ ";
    for (int i=0; i<traj->getNumPoints(); i++) {
      x << traj->getNorthing(i) << " ";
      y << traj->getEasting(i) << " ";
      dx << traj->getNorthingDiff(i, 1) << " ";
      dy << traj->getEastingDiff(i, 1) << " ";
      ddx << traj->getNorthingDiff(i, 2) << " ";
      ddy << traj->getEastingDiff(i, 2) << " ";
    }
    x << "];" << endl;
    y << "];" << endl;
    dx << "];" << endl;
    dy << "];" << endl;
    ddx << "];" << endl;
    ddy << "];" << endl;
    file_out << "% traj output" << endl << x.str() << y.str() << dx.str() << dy.str() << ddx.str() << ddy.str();
    file_out << "speed = sqrt(traj_dx.^2 + traj_dy.^2);" << endl;
    file_out << "acc = sqrt(traj_ddx.^2 + traj_ddy.^2);" << endl;
    file_out << "arclen(1) = 0;" << endl;
    file_out << "for ii=2:length(traj_x) arclen(ii)=arclen(ii-1)+sqrt((traj_x(ii)-traj_x(ii-1))^2 + (traj_y(ii)-traj_y(ii-1))^2);  end" << endl;
    file_out << "plot(traj_x, traj_y, 'g-'); hold off;" << endl;
    file_out << "figure;" << endl;
    file_out << "plot(arclen, speed, 'g'); hold on;" << endl << "plot(arclen, acc, 'r');" << endl;
  }
  file_out.close();

  return;
}

