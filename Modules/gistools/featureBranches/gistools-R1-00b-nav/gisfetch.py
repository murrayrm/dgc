#!/usr/bin/python

# Desc: Download map tiles from TerraServer
# Author: Andrew Howard
# Date: 14 Feb 2007

import os
import math
import urllib
import sys
from gisutm import LLtoUTM, UTMtoLL



def fetch_terra_image(lat, lon, scale, type, cols, rows):
    """Fetch an aerial image from TerraServer"""

    # Convert from lat/lon to UTM
    (zone, easting, northing) = LLtoUTM(23, lat, lon)

    # Select scale factor from pixel scal
    if scale == 0.5:
        snumber = 9
        sdiv = 100
    elif scale == 1.0:
        snumber = 10
        sdiv = 200
    else:
        assert(False)

    # Shift to hundred-meter tile boundary
    easting -= easting % sdiv
    northing -= northing % sdiv

    # Compute corresponding lat/lon
    (lat, lon) = UTMtoLL(23, northing, easting, zone)

    #print "get %s %d %d %.6f %.6f" % (zone, easting, northing, lat, lon)
    
    # Map corners
    ax = (easting - cols * scale / 2) / sdiv
    bx = (easting + cols * scale / 2) / sdiv - 1
    ay = (northing - rows * scale / 2) / sdiv
    by = (northing + rows * scale / 2) / sdiv - 1

    url = "http://terraserver-usa.com/download.ashx?t=%d&s=%d&x=%d:%d&y=%d:%d&z=%d" \
          % (type, snumber, ax, bx, ay, by, int(zone[:-1]))

    print "getting: %s" % url
    
    ifile = urllib.urlopen(url)
    img = ifile.read()
    ifile.close()

    ofile = open('tile.jpg', 'w')
    ofile.write(img)
    ofile.close()
    
    # Convert to uncompressed PPM
    name = 'terraimage_%+010d_%+010d_%06d.ppm' % (lat * 1e6, lon * 1e6, scale * 1000)

    print 'saving %s' % name    
    os.system('convert tile.jpg %s' % name)
    os.system('rm tile.jpg')

    return

    

if __name__ == '__main__':

    # Default to CalTech area
    #lat = 34.139059
    #lon = -118.123620
    #size = 100

    # TODO: decent command line handling
    
    # Get command line options
    if len(sys.argv) == 5 and sys.argv[1] == 'geo':
        lat = float(sys.argv[2])
        lon = float(sys.argv[3])
        size = float(sys.argv[4])
        (zone, east, north) = LLtoUTM(23, lat, lon)
    elif len(sys.argv) == 6 and sys.argv[1] == 'utm':
        zone = sys.argv[2]
        east = float(sys.argv[3])
        north = float(sys.argv[4])
        size = float(sys.argv[5])
        (lat, lon) = UTMtoLL(23, north, east, zone)
        (zone, east, north) = LLtoUTM(23, lat, lon)
    else:
        sys.exit(-1)

    print 'GEO %f %f' % (lat, lon)
    print 'UTM %s %d %d' % (zone, east, north)

    # Most areas have type 1 (mono) at 1m resolution.
    # Some areas have type 4 (color) at 0.5m resolution.
    type = 1
    scale = 1.0

    # Compute bounding box in UTM coordinates
    a_east = east - size/2
    b_east = east + size/2
    a_north = north - size/2
    b_north = north + size/2

    # Compute bounding box in lat/lon    
    (a_lat, a_lon) = UTMtoLL(23, a_north, a_east, zone)
    (b_lat, b_lon) = UTMtoLL(23, b_north, b_east, zone)

    # Default overlap between adjacent maps
    overlap = 0.5

    # Default image size
    image_cols = image_rows = 800

    # Compute number of tiles to exactly fill the request area
    count_east = (b_east - a_east) / (image_cols * scale)
    count_north = (b_north - a_north) / (image_rows * scale)

    # Compute number of tiles to have the requested overlap
    count_east /= (1 - overlap)
    count_north /= (1 - overlap)
    if count_east < 1:
        count_east = 1
    if count_north < 1:
        count_north = 1

    # Compute step size in lat/lon
    step_lat = (b_lat - a_lat) / count_north
    step_lon = (b_lon - a_lon) / count_east

    # Round up to ensure we get at least the area requested
    count_east = int(count_east + 0.5)
    count_north = int(count_north + 0.5)

    print "Fetching %dx%d tiles, each %dx%dm with %d%% overlap" \
          % (count_east, count_north, image_cols * scale, image_rows * scale, overlap * 100)

    # Get the tiles
    count = 0
    for j in range(-count_north/2, +count_north/2):
        for i in range(-count_east/2, +count_east/2):
            tile_lat = lat - j * step_lat
            tile_lon = lon + i * step_lon
            count += 1
            print "fetching %+03d,%+03d (%d of %d) at %.6f,%.6f" % \
                  (i, j, count, count_north * count_east, tile_lat, tile_lon)
            fetch_terra_image(tile_lat, tile_lon, scale, type, image_cols, image_rows)


    

    
