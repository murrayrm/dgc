
# Desc: Breakup a large geo-rectified image into tiles
# Author: Andrew H
# Date: 22 Oct 2007

import os
import sys
from gisutm import LLtoUTM, UTMtoLL
from math import *


def make_tiles(filename, size, scale, corners, output, zoom):
    """Create tiles from a big image file."""

    # Output tile size (pixels)
    sx = 800 * zoom
    sy = 800 * zoom

    # Step size (less than the tile size to allow overlap)
    dx = sx/2
    dy = sy/2
    nx = size[0]/dx
    ny = size[1]/dy

    # Upper left corner
    lon = corners[0][2][0]
    lat = corners[0][2][1]
    (zone, ULeast, ULnorth) = LLtoUTM(23, lat, lon)

    # Lower left corner
    lon = corners[1][2][0]
    lat = corners[1][2][1]
    (zone, LLeast, LLnorth) = LLtoUTM(23, lat, lon)

    # Upper right corner
    lon = corners[2][2][0]
    lat = corners[2][2][1]
    (zone, UReast, URnorth) = LLtoUTM(23, lat, lon)

    # Lower right corner
    lon = corners[3][2][0]
    lat = corners[3][2][1]
    (zone, LReast, LRnorth) = LLtoUTM(23, lat, lon)

    # Compute the rotation angle
    rotA = atan2(URnorth - ULnorth, UReast - ULeast)
    rotB = atan2(LRnorth - LLnorth, LReast - LLeast)
    print 'rotation %f %f (degrees)' % (rotA * 180/pi, rotB * 180/pi)

    # Compute diagonal skews
    diagA = sqrt((ULeast - LReast)**2 + (ULnorth - LRnorth)**2)
    diagB = sqrt((LLeast - UReast)**2 + (LLnorth - URnorth)**2)
    print 'diagonals %f %f' % (diagA, diagB)

    # Construct the transform from image coordinates to UTM coordinates
    rot = (rotA + rotB)/2
    M = [[0, 0, 0], [0, 0, 0]]
    M[0][0] = +cos(rot); M[0][1] = -sin(rot); M[0][2] = ULeast
    M[1][0] = +sin(rot); M[1][1] = +cos(rot); M[1][2] = ULnorth
       
    for j in range(0, ny):
        
        for i in range(0, nx):

            # Compute the top-left image coordinate
            col = i * dx
            row = j * dy

            # Compute the UTM value for the center of the tile
            ox = +(col + sx/2) * scale
            oy = -(row + sy/2) * scale
            east  = M[0][0]*ox + M[0][1]*oy + M[0][2]
            north = M[1][0]*ox + M[1][1]*oy + M[1][2]

            # Compute the lat/lon at the center of the tile
            (lat, lon) = UTMtoLL(23, north, east, zone)

            print 'extracting %d %d %.6f %.6f' % (i, j, lat, lon)

            # Construct coded filename.  Lat/lon are in micro-degrees;
            # scale is in mm.
            outfile = '%s_%+d_%+d_%05d' % \
                      (output, int(lat * 1e6), int(lon * 1e6), int(scale * 1e3 / zoom))

            # Grab the raw tile
            cmd = 'stream -map rgb -extract %dx%d%+d%+d %s %s.rgb' % \
                  (sx, sy, col, row, filename, 'temp')
            os.system(cmd)

            # Convert to usable format
            cmd = 'convert -size %dx%d -depth 8 -rotate %.3f -resize %f%% %s.rgb %s.ppm' % \
                  (sx, sy, -rot*180/pi, 100/zoom, 'temp', outfile)
            os.system(cmd)
    
    return


if len(sys.argv) < 2:
  print 'usage: %s <eltoro|victorville>'
  sys.exit(1)

dataset = sys.argv[1]

if dataset == 'eltoro':
    
    # El Toro data
    filename = '/dgc/aerial-images/Orange/Orange-CA-1F-2007.tif'
    output = '/dgc/aerial-images/tile'
    size = [15386, 15431] # cols, rows
    scale = 12 * 0.0254
    zoom = 2
    corners = (
        ('Upper Left',  ( 6632960.100, 1709061.625),  (-117.7656588,33.6883882)),
        ('Lower Left',  ( 6632960.100, 1693675.656),  (-117.7657767,33.6461115)),
        ('Upper Right', ( 6648391.070, 1709061.625),  (-117.7149376,33.6882787)),
        ('Lower Right', ( 6648391.070, 1693675.656),  (-117.7150809,33.6460021)))
    make_tiles(filename, size, scale, corners, output, zoom)


if dataset == 'victorville':
    
    # Victorville data
    filename = '/dgc/aerial-images/Victorville/Victorville-CA-6in-2005.tif'
    output = '/dgc/aerial-images/tile'
    size = [17131, 24637] # cols, rows
    scale = 6 * 0.0254
    zoom = 4
    corners = (
        ('Upper Left',    ( 2056448.489,  621426.645),  (-117.3846260,34.5931197)),
        ('Lower Left',    ( 2056448.489,  618815.875),  (-117.3848002,34.5695838)),
        ('Upper Right',   ( 2060203.175,  621426.645),  (-117.3436953,34.5929056)),
        ('Lower Right',   ( 2060203.175,  618815.875),  (-117.3438811,34.5693697)))
    make_tiles(filename, size, scale, corners, output, zoom)
