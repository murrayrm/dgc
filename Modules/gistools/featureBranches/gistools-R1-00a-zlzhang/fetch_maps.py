#!/usr/bin/python

# Desc: Download map tiles from Google Maps
# Author: Andrew Howard
# Date: 14 Feb 2007

import os
import math
import urllib
import sys
from LatLongUTMconversion import *



def fetch_google_map(lat, lon, zoom, cols, rows):
    """Fetch a tile using the old google maps interface."""

    req = 'latitude_e6=%d&longitude_e6=%d&zm=%d&w=%d&h=%d&cc=us&min_priority=2' % \
          (lat * 1e6, lon * 1e6, zoom, cols, rows)

    print 'loading %s' % req
    ifile = urllib.urlopen("http://maps.google.com/mapdata?%s" % req)
    img = ifile.read()
    ifile.close()

    ofile = open('tile.gif', 'w')
    ofile.write(img)
    ofile.close()

    # Convert to compressed PPM
    name = 'googlemap_%+010d_%+010d_%06d.ppm.gz' % (lat * 1e6, lon * 1e6, zoom)
    print 'saving %s' % name    
    os.system('convert tile.gif %s' % name)
    os.system('rm tile.gif')
    
    return



def fetch_terra_image(lat, lon, cols, rows):
    """Fetch an aerial image from TerraServer"""

    # TODO: works for 800x800 images, but may not produce correct alignment
    # for other sizes
    assert(cols == 800 and rows == 800)

    # Convert from lat/lon to UTM
    (zone, easting, northing) = LLtoUTM(23, lat, lon)

    # Hardwired for TS images at 0.5m/pixel
    scale = 0.5

    # Shift to hundred-meter tile boundary
    easting -= easting % 100
    northing -= northing % 100

    # Compute corresponding lat/lon
    (lat, lon) = UTMtoLL(23, northing, easting, zone)

    #print "get %s %d %d %.6f %.6f" % (zone, easting, northing, lat, lon)
    
    # Map corners
    ax = (easting - cols * scale / 2) / 100
    bx = (easting + cols * scale / 2) / 100 - 1
    ay = (northing - rows * scale / 2) / 100
    by = (northing + rows * scale / 2) / 100 - 1

    url = "http://terraserver-usa.com/download.ashx?t=4&s=9&x=%d:%d&y=%d:%d&z=%d" \
          % (ax, bx, ay, by, int(zone[:-1]))

    #print "getting: %s" % url
    
    ifile = urllib.urlopen(url)
    img = ifile.read()
    ifile.close()

    ofile = open('tile.jpg', 'w')
    ofile.write(img)
    ofile.close()
    
    # Convert to compressed PPM
    name = 'terraimage_%+010d_%+010d_%06d.ppm.gz' % (lat * 1e6, lon * 1e6, scale * 1000)

    print 'saving %s' % name    
    os.system('convert tile.jpg %s' % name)
    os.system('rm tile.jpg')

    return



    

if __name__ == '__main__':

    # Default to CalTech area
    lat = 34.139059
    lon = -118.123620
    size = 1000
        
    if len(sys.argv) < 4:
        print 'usage: %s <LAT> <LON> <SIZE>' % sys.argv[0]
        print '  LAT is the latitude in decimals degrees'
        print '  LON is the longitude in decimal degrees'
        print '  SIZE is the map size in meters'
        print 'For example:'
        print '  %s 34.139059 -118.123620 1000' % sys.argv[0]
        print "will get the one-square kilometer centered on Alice's garage."
        print
        sys.exit(-1)

    # Get command line options
    lat = float(sys.argv[1])
    lon = float(sys.argv[2])
    size = float(sys.argv[3])
        
    # Compute UTM coordiantes
    (zone, east, north) = LLtoUTM(23, lat, lon)

    # Compute bounding box in UTM coordinates
    a_east = east - size/2
    b_east = east + size/2
    a_north = north - size/2
    b_north = north + size/2

    # Compute bounding box in lat/lon    
    (a_lat, a_lon) = UTMtoLL(23, a_north, a_east, zone)
    (b_lat, b_lon) = UTMtoLL(23, b_north, b_east, zone)

    # Default pixel size (m)
    scale = 0.50

    # Default overlap between adjacent maps
    overlap = 0.5

    # Default image size
    image_cols = image_rows = 800

    # Compute number of tiles to exactly fill the request area
    count_east = (b_east - a_east) / (image_cols * scale)
    count_north = (b_north - a_north) / (image_rows * scale)

    # Compute number of tiles to have the requested overlap
    count_east /= (1 - overlap)
    count_north /= (1 - overlap)

    # Compute step size in lat/lon
    step_lat = (b_lat - a_lat) / count_north
    step_lon = (b_lon - a_lon) / count_east

    # Round up to ensure we get at least the area requested
    count_east = int(count_east + 0.5)
    count_north = int(count_north + 0.5)

    print "Fetching %dx%d tiles, each %dx%dm with %d%% overlap" \
          % (count_east, count_north, image_cols * scale, image_rows * scale, overlap * 100)

    # Get the tiles
    count = 0
    for j in range(-count_north/2, +count_north/2):
        for i in range(-count_east/2, +count_east/2):
            tile_lat = lat - j * step_lat
            tile_lon = lon + i * step_lon
            count += 1
            print "fetching %+03d,%+03d (%d of %d) at %.6f,%.6f" % \
                  (i, j, count, count_north * count_east, tile_lat, tile_lon)
            fetch_terra_image(tile_lat, tile_lon, image_cols, image_rows)


    

    
