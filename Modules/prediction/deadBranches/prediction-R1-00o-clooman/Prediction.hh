#ifndef PREDICTION_HH_
#define PREDICTION_HH_
#include "map/MapElementTalker.hh"
#include "map/Map.hh"
// stf package includes
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_statistics.h>
#include <time.h>

// skynet, talker, and interfaces
#include "skynet/sn_msg.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/SkynetTalker.hh"
#include "map/MapElementTalker.hh"
#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"
#include "temp-planner-interfaces/PlannerInterfaces.h"

// Other modules/def's
#include "map/Map.hh"
#include <cotk/cotk.h>
#include <ncurses.h>
#define SPOOF_MAP TRUE
#define NO_SPOOF_MAP FALSE

struct predTraj
{
  // timestamp
  uint64_t traj_time;

  // possible traj at a certain timestamp
  point2arr traj_space;

  // id for mapping
  int traj_id;
};


class PredictionObstacle
{
public:
  MapElement me;

  vector<predTraj> traj;
  bool updated;

  int collisionCounter;
  point2 collisionPoint;
  uint64_t collisionTime;
  int mapid_traj;
};

class Prediction
{

public: 
  
  enum predictionReturn
    { 
      CLEAR,
      COLLISION
    };

  static int init(Graph_t* graph, Map* localMap);
  static void destroy();

  /// @brief updates the Vehicle List with all found obstacles and their predicted trajectories
  static void updateList(VehicleState vehState);

  // @brief Returns predicted collisions
  static predictionReturn predictCollision(VehicleState vehState, PredictionObstacle &pObstacle, int &numberObstacles);

  // @brief computes the Particles of found obstacles
  static void computeParticles(MapElement me, LaneLabel lane, vector<predTraj> &traj);
  // @brief Gets time
  static uint64_t getTime();
  static bool USE_PARTICLES;
  static bool PRINT_TRAJ;
private :

  // @brief Updates Alice and its trajectory
  static void updateAlice(VehicleState vehState);

  // @brief Computes trajectories for Alice or any other obstacle
  static void createTrajectory(MapElement &me, vector<predTraj> *traj, bool mapflag, bool useParticleFilter);

  // @brief Utility to add two angles
  static int addAngles(double &angle_out, double angle_in1, double angle_in2);


  // @brief Checks whether obstacle is within intersections
  static bool withinIntersection(MapElement me);

  static int mapcounter,pmapcounter;
  static vector<PredictionObstacle> PredictionList;
  static PredictionObstacle alicePrediction;
  static CMapElementTalker debugMap;

  // time in seconds that algorithm predicts
  static const double PREDICTION_HORIZON;

  // frequency of prediction in [Hz]
  static const double PREDICTION_RESOLUTION;

  // seperation distance threshold between Alice and any obstacle that indicates a serious collision
  // it should be dependant on the maxium velocity and the prediction resolution
  static const double SEPERATION_THRESHOLD;
  // if distance is greater than the CANCEL_THRESHOLD, the algorithm cancel looking for other collisions at the same timestep
  // as even the Particles aren't scattered that far
  static const double CANCEL_THRESHOLD;

  // how often does a collision need to occur that it will be handled as a serious collision threat
  static const double COLLISION_THRESHOLD;

  // map debug channel
  static const int sendChannel;

  // Graph
  static Graph_t* p_graph;
  static Map* p_map;
};

#endif /*PREDICTION_HH_*/
