#ifndef PREDICTION_HH_
#define PREDICTION_HH_
#include "map/MapElementTalker.hh"
#include "map/Map.hh"
// stf package includes
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>

// skynet, talker, and interfaces
#include "skynet/sn_msg.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/SkynetTalker.hh"
#include "map/MapElementTalker.hh"
#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"

// Other modules/def's
#include "map/Map.hh"
#include <cotk/cotk.h>
#include <ncurses.h>

class VehicleList
{
public:
  MapElement me;
  point2arr traj;
  vector<uint64_t> traj_time;
  int traj_id;
  bool updated;
  int collisionCounter;
};

class classPrediction : public CStateClient, public CMapElementTalker
{

public: 
  
  /*! Constructor */
  classPrediction(int skynetKey, bool bWaitForStateFill,
                 int debugLevel, int verboseLevel, 
                 bool log);
  /*! Destructor */
  virtual ~classPrediction();

  void PredictionLoop(void);
  void updateObstacles();
  void updateAlice();
  void updateMapThread();
  void createTrajectory(MapElement &me, point2arr &traj, vector<uint64_t> &traj_time,bool mapflag);
  int addAngles(double &angle_out, double angle_in1, double angle_in2);
  bool predictCollision(point2 &collisionPoint);
  uint64_t getTime();
  bool withinIntersection(MapElement me);
  void populateWayPoints(Map* localMap, PointLabel InitialWayPointEntry, vector<PointLabel> &WP_vector);

private :



  /*! The updated vehicle state  */
  VehicleState vehState;
  int mapcounter;
  vector<VehicleList> vehList;
  point2arr alice_traj;
  vector<uint64_t> alice_traj_time;
  Map* localMap;

  /*! The current map */
  pthread_mutex_t mapMutex;

  // time in seconds that algorithm predicts
  static const double PREDICTION_HORIZON = 10.0;

  // frequency of prediction in [Hz]
  static const double PREDICTION_RESOLUTION=0.1;

  // seperation distance threshold between Alice and any obstacle that indicates a serious collision
  // it should be dependant on the maxium velocity and the prediction resolution
  static const double SEPERATION_THRESHOLD=15 * 0.1;

  // how often does a collision need to occur that it will be handled as a serious collision threat
  static const double COLLISION_THRESHOLD=10;

  // map debug channel
  static const int sendChannel=-10;
};

#endif /*PREDICTION_HH_*/
