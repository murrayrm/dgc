              Release Notes for "prediction" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "prediction" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "prediction" module can be found in
the ChangeLog file.

Release R1-01-nav (Mon Oct 22  8:14:44 2007):
        * Deleted velocity hack. Should be fixed in simulation now.
        * Fixed Darpa Ball

Release R1-01 (Sat Oct 20 12:26:43 2007):
	* removed reversing the heading when we go in reverse- it is now always yaw rather than the direction we're going  some logging	added
	* changed set of clothoids used in road region

Release R1-00z (Mon Oct 15 23:39:16 2007):
	* Adapted to the new Utils display functions with the 
visualization levels.

Release R1-00y (Mon Oct 15  7:59:26 2007):
	Moved thread handling into prediction to allow clean exit.
	Added debug info and updated prediction box for lane change.

Release R1-00x (Fri Oct  5  8:33:02 2007):
	deleted the log-outputs that caused core dumps once in a while

Release R1-00w (Tue Oct  2  9:58:03 2007):
	Uses AliceStateHelper functions now

Release R1-00v (Mon Oct  1 17:03:49 2007):
	The prediction thread does not send any update to the Console anymore. This is the first stop to reduce
	the likeliness for segfaulting

Release R1-00u (Thu Sep 27 21:17:06 2007):
	Increased verbose level for logging.

Release R1-00t (Tue Sep 25 11:37:30 2007):
	Other vehicle's velocity is limited to 15m/s. While merging, the safety box is based on the relative velocity.

Release R1-00s (Mon Sep 24 18:33:29 2007):
	Prediction does not send collision if obstacle is behind Alice and in the same lane.
	If Alice wants to change lanes on a multi-lane road, prediction creates safety merging corridor of 10s for each obstacle based on its velocity.

Release R1-00r (Fri Sep 21 18:13:51 2007):
	Runs in own thread now. Also, uses for Alice, it uses trajectory created by path-planner and vel-planner

Release R1-00q (Thu Sep 13 17:51:58 2007):
	removed libraries in Makefile that are used by ParticleFilter 
and deactivated the ParticleFilter part of prediction. This should speed 
up compiling and linking in a short-term

Release R1-00p (Mon Sep 10 10:27:06 2007):
	Solved the problems with the 10s - loops. Prediction might be still too slow to run within the planner loop depending on the number
	of obstacles around Alice.

Release R1-00o (Mon Sep  3  7:33:39 2007):
	Tweaked code.

Release R1-00n (Thu Aug 30 22:18:51 2007):
	Fixed bug. Vehicles were not deleted properly out of its internal list when they
	disappeared from the map.

Release R1-00m (Thu Aug 30 17:17:43 2007):
	Deleted some couts and improved mapviewer cleanup in case a 
	vehicle disappears out of the map

Release R1-00l (Thu Aug 30 15:24:27 2007):
	Fixed bug in detectCollision.

Release R1-00k (Tue Aug 21 19:09:22 2007):
	Integrated new algorithm from mapprediction. Now particles 
	follow turns and intersections. The module is still under 
construction and won't be used in real testing yet.

Release R1-00j (Tue Aug 21 12:40:25 2007):
	Removed CmdArgs.cc and uses temp-planner-interfaces/CmdArgs.cc 
now

Release R1-00i (Mon Aug 20 21:35:17 2007):
	Removed map functions getWaypoint and getHeading and replaces them with the graph functions. Therefor the Unittest does not work yet as it does not load the graph yet.

Release R1-00h (Tue Aug 14 19:31:28 2007):
	Adjusted makefile for CSpecs

Release R1-00g (Tue Aug 14 10:11:40 2007):
	Adjusted Makefile for the new consoel

Release R1-00f (Thu Aug  9 11:26:21 2007):
	Cleaned up code so it compiles on a Mac

Release R1-00e (Wed Aug  8 17:21:52 2007):
	Now planner decides whether prediction uses particle filters or not. Otherwise changes in prediction could potentially slow down the planning loop

Release R1-00d (Thu Aug  2 21:51:52 2007):
	Added ParticleFilters. There is a hardwired constant to switch back and forth between ParticleFilters and the simple predicted 
	trajectory. Using the simple prediction for the upcoming test days.

Release R1-00c (Tue Jul 31 15:47:27 2007):
	Added UnitTest for Prediction Module

Release R1-00b (Tue Jul 31 13:41:05 2007):
The prediction framework is now part of the planning stack and is called from within the planner loop. Prediction can be turned 
off by using the command line argument --noprediction.

So far prediction does not affect the planning behaviour yet. It just outputs the amount of monitored obstacles and 
predicts a possible collision to the planner console. The trajectories can be seen with mapviewer on the recvsubgroup -10.

Release R1-00a (Fri Jul 27 18:08:57 2007):
This is the first release. Prediction looks at dyn. obstacles and predicts their trajectory. It compares those trajectories to Alice and will predict 
possible collisions. So far, the prediction is very simple and this release will be the framework for future implementation of particle filters, etc

Release R1-00 (Thu Jul 19 16:43:52 2007):
	Created.



























