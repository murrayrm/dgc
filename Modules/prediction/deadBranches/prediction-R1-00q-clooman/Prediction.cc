//TODO
// - connect to Alice's seggoals
// - predict obstaclces out of lane
// - check collision


#include "Prediction.hh"
#include "temp-planner-interfaces/CmdArgs.hh"
#include <gcinterfaces/SegGoals.hh>
#include "logic-planner/IntersectionHandling.hh"
#include "temp-planner-interfaces/AliceStateHelper.hh"
#include <limits.h>
#include <temp-planner-interfaces/Log.hh>
#include <temp-planner-interfaces/Console.hh>
// #include "ParticleFilter.hh"
#include "dgcutils/DGCutils.hh"

using namespace std;

CPrediction::CPrediction(int key)
{
  sn_key = key;
  DGCcreateMutex(&updateListMutex);

  mapcounter = 5000;
  pmapcounter = 7000;
  clearCounter = 0;
  sentToMapFlag = false;
  debugMap.initSendMapElement(sn_key);
  loopActive = false;
  temp_map = new Map();
  p_map = new Map();
  print_traj = true;
}

void CPrediction::updateVariables(CTraj* traj, Map* localMap)
{
  if (localMap == NULL)
    return;

  DGClockMutex(&updateListMutex);

  temp_path_north.clear();
  temp_path_east.clear();
  temp_vel_north.clear();
  temp_vel_east.clear();

  // copy path
  for (int i = 0; i<traj->getNumPoints(); i++) {
    // copy path
    temp_path_north.push_back(traj->getNdiffarray(0)[i]);
    temp_path_east.push_back(traj->getEdiffarray(0)[i]);
    // copy velocity profile
    temp_vel_north.push_back(traj->getNdiffarray(1)[i]);
    temp_vel_east.push_back(traj->getEdiffarray(1)[i]);
  }

  // copy map
  *temp_map = *localMap;
  
  DGCunlockMutex(&updateListMutex);
}


CPrediction::~CPrediction()
{
  delete temp_map;
  delete p_map;
  DGCdeleteMutex(&updateListMutex);
}

void CPrediction::updateAlice()
{
  predTraj tempTraj;
  alicePrediction.traj.clear();

  // convert space trajectory into more useful structure
  vector<point2> space;
  vector<double> distInSpace;
  vector<double> velProfil;
  double dist = 0.0;
  space.clear();
  distInSpace.clear();
  velProfil.clear();
 
  for (unsigned int i = 0; i<path_north.size(); i++) {
    if (i!=0)
      dist += sqrt(pow(path_north[i] - path_north[i-1], 2) + pow(path_east[i] - path_east[i-1], 2));
    space.push_back(point2(path_north[i], path_east[i]));
    distInSpace.push_back(dist);
    velProfil.push_back(sqrt(pow(vel_north[i],2) + pow(vel_east[i],2)));
  }

  // store current location
  unsigned long long starttime;
  DGCgettime(starttime);
  tempTraj.traj_time = starttime;
  tempTraj.traj_space = point2(path_north[0], path_east[0]);
  alicePrediction.traj.push_back(tempTraj);

  // compute trajectory in time and space
  double t, s, v;
  t = 0;
  v = velProfil[0];
  int index=0;
  while (t<PREDICTION_HORIZON) {
    t+=PREDICTION_RESOLUTION; // time step in [s]
  
    s = v * t;

    // the following search algorithm is not very exact and won't return the absolut correct velocity
    // each time. But as the time steps in prediction are very small, this should be sufficient
    for (unsigned l=index; l<distInSpace.size(); l++) {
      if (distInSpace[l]>s) {
        tempTraj.traj_space = space[l];
        v = velProfil[l];
        // save index and decrease it; this should speed up the search in the next loop
        index = l-1;
        break;
      }
    }
      
    tempTraj.traj_time = starttime + t*1000000;
    alicePrediction.traj.push_back(tempTraj);
  }

  // output trajectory to map
  if (print_traj) {
    MapElement me;
    point2arr points;
    // send traj to map
    me.setTypePoints();
    me.setColor(MAP_COLOR_GREEN,100);

    for (unsigned l=0; l< alicePrediction.traj.size(); l++)
      points.push_back(alicePrediction.traj[l].traj_space);

    me.id=1302;
    me.setGeometry(points);
    debugMap.sendMapElement(&me,sendChannel);
  }  
}

bool CPrediction::withinIntersection(MapElement me)
{
  vector<PointLabel> StopLines;
  point2 p_stopline, p_temp, p;
  LaneLabel lane;
  p.set(me.center);
  
  bool inLane=false;

  if (p_map->getLane(lane,p) != -1)
    if (p_map->isPointInLane(p,lane))
        inLane=true;

  if (inLane) {
    point2arr centerline;
    point2 startpoint;
    
    // get centerline from lane where obstacle is
    p_map->getLaneCenterLine(centerline,lane);
    
    // the predicted trajectory should stop at stoplines
    double distance_temp;
    if (p_map->getLaneStopLines(StopLines,lane)!=0) {
      double distance_stopline=INFINITY;
      
      // Find closest stopline in obstacle's direction of travel and save PointLabel and distance
      int index=-1;
      for (unsigned int i=0; i<StopLines.size(); i++) {
        p_map->getWaypoint(p_temp,StopLines[i]);
        p_map->getDistAlongLine(distance_temp,centerline,p_stopline,p);
        
        if (distance_temp>-10.0 && distance_temp<distance_stopline) {
          index=i;
          distance_stopline=distance_temp;
          p_stopline = p_temp;
        }
      }
          
      // if stopline was found and obstacle close to stopline, return true
      if (index != -1 && sqrt(pow(me.position.x - p_stopline.x, 2) + pow(me.position.y - p_stopline.y, 2)) < 10.0)
        return true;
    }
  }

  return false;
}

void CPrediction::updateList()
{

  // Create copies
  DGClockMutex(&updateListMutex);
  if (temp_map == NULL || temp_path_north.size() == 0) {
    DGCunlockMutex(&updateListMutex);
    return;
  }
  *p_map = *temp_map;
  path_north.clear();
  path_east.clear();
  vel_north.clear();
  vel_east.clear();
  path_north = temp_path_north;
  path_east = temp_path_east;
  vel_north = temp_vel_north;
  vel_east = temp_vel_east;
  DGCunlockMutex(&updateListMutex);


  // If there is no valid path yet, return out of the function
  if (path_north.size() == 0)
    return;

  // update Alice
  updateAlice();

  // receive all obstacles within 500m which should get practically all obstacles as ladars are limited
  vector<MapElement> obstacles;
  point2 position_alice = point2(path_north[0], path_east[0]);
  p_map->getObsNearby(obstacles,position_alice,500);

  for (unsigned i =0; i< obstacles.size(); i++) {
    // only allow VEHICLES
    if (obstacles[i].isVehicle() && !obstacles[i].isPredicted()) {
      bool found=false;

      // search for this obstacle in the list
      for (unsigned int j=0; j<PredictionList.size(); j++)
        if (PredictionList[j].me.id==obstacles[i].id) {
          // Create trajectory
          if (!withinIntersection(obstacles[i]))
            createTrajectory(obstacles[i], &(PredictionList[j].traj), SPOOF_MAP);
          // do not create any trajectory when obstacle is within an intersection
          else
            PredictionList[j].traj.clear();
          
                  
          PredictionList[j].me=obstacles[i];
          
          // set flag so that list will be updated later
          PredictionList[j].updated=true;
          found=true;
          
          Log::getStream(1)<<"Prediction: Update obstacle "<<PredictionList[j].me.id<<" Pos "<<PredictionList[j].me.center<<endl;
          break;
        }
  
      // if me was not found in vehicle list, add it
      if (!found) {
        PredictionObstacle predListTemp;
        
        // Create trajectory
        if (!withinIntersection(obstacles[i]))
          createTrajectory(obstacles[i], &(predListTemp.traj), SPOOF_MAP);
        // do not create any trajectory when obstacle is within an intersection
        else
          predListTemp.traj.clear();
        
        
        predListTemp.me=obstacles[i];
        predListTemp.updated=true;
        predListTemp.collisionCounter=0;
        // save new element to list
              PredictionList.push_back(predListTemp);
              Log::getStream(1)<<"Prediction: Add obstacle "<<obstacles[i].id<<" Pos "<<obstacles[i].center<<endl;
      }
    }
  }

  MapElement me;
  // clean up vehicle list & output to map
  vector<PredictionObstacle>::iterator v_it;
  vector<PredictionObstacle> tempList;
  for (v_it=PredictionList.begin(); v_it!=PredictionList.end(); v_it++) {
    PredictionObstacle vl=*v_it;
    
    if (vl.updated) {
      // update flag and save it
      vl.updated=false;
      tempList.push_back(vl);
      *v_it=vl;
      
      if (print_traj) {
        // send obstacle to map
        me=vl.me;
        me.setColor(MAP_COLOR_YELLOW,100);
        debugMap.sendMapElement(&me,sendChannel);
        
        // send traj to map
        me.setTypePoints();
        me.setColor(MAP_COLOR_RED,100);
        
        // lets try something
        point2arr points;
        
        for (unsigned l=0; l<vl.traj.size(); l++)
          points.push_back(vl.traj[l].traj_space);
        
        me.id=pmapcounter++;
        vl.mapid_traj = pmapcounter;
        me.setGeometry(points);
        debugMap.sendMapElement(&me,sendChannel);
      }
    }
    else {
      Log::getStream(1)<<"Prediction: Remove obstacle "<<vl.me.id<<" Pos "<<vl.me.center<<endl;
      // clear obstacle from map
      me=vl.me;
      me.setTypeClear();
      debugMap.sendMapElement(&me,sendChannel);
      
      me.id = vl.mapid_traj;
      debugMap.sendMapElement(&me,sendChannel);
    }
  }

  PredictionList.clear();
  PredictionList = tempList;
}

void CPrediction::createTrajectory(MapElement &me, vector<predTraj> *traj, bool mapflag)
{
  // make sure that traj-vector is empty
  traj->clear();

  point2_uncertain pos=me.center;
  point2_uncertain vel=me.velocity;
  point2 p,point;
  p.set(pos);
  double angle,dist,d,orientation,t;
  unsigned long long starttime;

  LaneLabel lane;

  // return the distance that is predicted given the current speed
  double velAbs=sqrt(pow(vel.x,2) + pow(vel.y,2));
  double dist_prediction=velAbs * PREDICTION_HORIZON;
  double time_prediction=PREDICTION_HORIZON;

  // avoid bug in mapper
  if (mapflag)
    vel.y=-vel.y;

  // hack for obstacle's orientation
  orientation=acos(fabs(vel.x)/velAbs);

  if (vel.x>=0 && vel.y<0)
    orientation=-orientation;
  else if (vel.x<0 && vel.y<0)
    orientation=-M_PI + orientation;
  else if (vel.x<0 && vel.y>=0)
    orientation=M_PI - orientation;

  stringstream s;

  bool inLane=false;

  if (p_map->getLane(lane,p) != -1)
    if (p_map->isPointInLane(p,lane))
      inLane=true;

  // if obstacle is in lane
  if (inLane) {
    point2arr centerline;
    point2 startpoint;
    
    // get centerline from lane where obstalce is
    p_map->getLaneCenterLine(centerline,lane);
    
    // this function also returns in the second argument the projection of p into the lane
    p_map->getHeading(angle,startpoint,lane,p);
    
    // the predicted trajectory should stop at stoplines
    vector<PointLabel> StopLines;
    
    point2 p_stopline;
    double distance_temp;
    
    if (p_map->getLaneStopLines(StopLines,lane)!=0) {
      double distance_stopline=INFINITY;
      
      // Find closest stopline in Alice's direction of travel and save PointLabel and distance
      for (unsigned int i=0; i<StopLines.size(); i++) {
        p_map->getWaypoint(p_stopline,StopLines[i]);
        p_map->getDistAlongLine(distance_temp,centerline,p_stopline,p);
        
        if (distance_temp>0 && distance_temp<distance_stopline)
          distance_stopline=distance_temp;
      }
       
      // if stopline is closer than the predicted distance, then shorten the predicted distance and predicated time
      if (distance_stopline<INFINITY && distance_stopline<dist_prediction) {
        dist_prediction=distance_stopline;
        time_prediction=distance_stopline/velAbs;
      }
    }
    
    // some variables that we need later
    predTraj tempTraj;
    DGCgettime(starttime);
    
    // store startpoint
    tempTraj.traj_space = startpoint;
    tempTraj.traj_time = starttime; 
    traj->push_back(tempTraj);
    
    // this function call is needed for later. it computes the distance from the beginning of the centerline to the startpoint
    p_map->getDistAlongLine(dist,centerline,startpoint);
    
    // estimate the direction of travel within the lane. It might be possible that vehicle is travelling in wrong lane
    // calculate the difference of the angle lane and the heading of obstacle
    // if the absolute difference is larger than pi, both headings are opposite to each other
    double heading_delta;
    
    addAngles(heading_delta,-angle,orientation);
    
    t=0;
    // loop along the centerline and create trajectory
    while (t<time_prediction) {
      t+=PREDICTION_RESOLUTION; // time step in [s]
      d=velAbs*t;
      
      // depending on obstacle's heading regarding the lane's heading, calculate the distance
      // if correct direction of travel ...
      if ((heading_delta <= M_PI/4) && (heading_delta >= -M_PI/40))
        p_map->getPointAlongLine(point,centerline,dist+d);
      // if in opposite direction of travel...
      else if ((heading_delta >= 3*M_PI/4) || (heading_delta <= -3*M_PI/4))
        p_map->getPointAlongLine(point,centerline,dist-d);
      // else...diagonal to lane ...
      else {
        point.x = p.x + cos(orientation) * d;
        point.y = p.y + sin(orientation) * d;
      }
      
      tempTraj.traj_time = starttime+ t*1000000;
      tempTraj.traj_space = point;
      
      traj->push_back(tempTraj);
    }
  }
  // if obstacle is out of lane, compute predicated trajectory along the obstacle's orientation
  else {
    predTraj tempTraj;
    
    // store startpoint
    tempTraj.traj_time = 0;
    tempTraj.traj_space = p;
    traj->push_back(tempTraj);
    
    t=0;
    d=0;
    while (t<time_prediction) {
      t+=PREDICTION_RESOLUTION; // time step in [s]
      d=velAbs*t;
      
      point.x = p.x + cos(orientation) * d;
      point.y = p.y + sin(orientation) * d;
      
      tempTraj.traj_time =  starttime+ uint64_t(t*1000000);
      tempTraj.traj_space = point;
      
      traj->push_back(tempTraj);
    }
  }
}

int CPrediction::addAngles(double &angle_out, double angle_in1, double angle_in2) {
  if ((angle_in1+angle_in2)>M_PI) 
    angle_out=angle_in1+angle_in2-2*M_PI;
  else if ((angle_in1+angle_in2)<=-M_PI)
    angle_out=angle_in1+angle_in2+2*M_PI;
  else
    angle_out=angle_in1+angle_in2;
  
  return 0;
}

void CPrediction::predictCollision()
{
  point2 p_alice,p_obstacle;
  double dist;
  unsigned int upperBound;
  bool possibleCollision=false;
  bool cancelLoop=false;
  LaneLabel lane;

  if (path_north.size() == 0)
    return;

   // loop through all found obstacles and compare trajectories
  for (unsigned int i=0; i<PredictionList.size(); i++) {
    // as Alice and the obstacle might have different prediction horizons, adjust upperBound
    if (alicePrediction.traj.size()<=PredictionList[i].traj.size())
      upperBound=alicePrediction.traj.size();
    else
      upperBound=PredictionList[i].traj.size();

    possibleCollision=false;
    for (unsigned int j=0; j<upperBound; j++) {
      cancelLoop=false;
      p_obstacle = PredictionList[i].traj[j].traj_space;
      p_alice = alicePrediction.traj[j].traj_space;
      
      // compute distance between both points of trajectories
      dist = sqrt( pow(p_alice.x-p_obstacle.x,2) + pow(p_alice.y-p_obstacle.y,2) );
      
      double dist2currentAlice = sqrt( pow(path_north[0]-p_obstacle.x,2) + pow(path_east[0]-p_obstacle.y,2) );

      // if dist is smaller than threshold, assume collision and increse collision counter
      // at the same time, don't send any collisions that are closer than 1.5m This might distract Alice (wheet, etc.)
      if (dist<SEPERATION_THRESHOLD && dist2currentAlice>1.5) {
        PredictionList[i].collisionCounter++;
        PredictionList[i].collisionPoint = p_obstacle;
        PredictionList[i].collisionTime = PredictionList[i].traj[j].traj_time;
        possibleCollision=true;
        
        // only count collision once in case the trajectories are so close
        // this will also automatically return the closest collision as the vector is time discretisated
        break;
      }
    }

    // if no collision was detected, reset counter
    if (!possibleCollision)
      PredictionList[i].collisionCounter=0;
  }


   // return the number of obstacles that are stored in the vehicle list. This is the number of obstacles that are under prediction
   int index=-1;

   unsigned long long t = -1;
   // If there might be several collisions, find and return the closest one
   for (unsigned int i=0; i<PredictionList.size(); i++)
     if ((PredictionList[i].collisionCounter>=COLLISION_THRESHOLD) && (PredictionList[i].collisionTime <= t)) {
       index=i;
       t = PredictionList[i].collisionTime;
     }
   
   // if at least one very possible collision was found, return its coordinates and time
   MapElement me;
   string predStatus;
   if (index != -1) {
     me.setId(MODtrafficplanner,400);
     me.setTypePredictedVehicle();
     me.setColor(MAP_COLOR_RED);
     me.setGeometry(PredictionList[index].collisionPoint, 1.5);
     me.setPosition(PredictionList[index].collisionPoint);
     debugMap.sendMapElement(&me,1);
     debugMap.sendMapElement(&me,-2);
     sentToMapFlag = true;

     Log::getStream(1)<<"Collision: send collisionPoint "<<PredictionList[index].collisionPoint<<endl;
     predStatus="COLLISION";
   }
   else if (index == -1 && sentToMapFlag && clearCounter<COLLISION_THRESHOLD)
     clearCounter++;
   else if (index == -1 && sentToMapFlag && clearCounter>=COLLISION_THRESHOLD) {
     // clear predicted collision point out of map
     me.setId(MODtrafficplanner,400);
     me.setTypeClear();
     debugMap.sendMapElement(&me,1);
     debugMap.sendMapElement(&me,-2);
     sentToMapFlag = false;

     predStatus="CLEAR    ";
     clearCounter = 0;
   }
   else
     predStatus="CLEAR    ";

   Console::updatePred(predStatus,PredictionList.size());
}

void CPrediction::clearMap()
{
  if (sentToMapFlag) {
    MapElement me;
    me.setId(MODtrafficplanner,400);
    me.setTypeClear();
    debugMap.sendMapElement(&me,1);
    debugMap.sendMapElement(&me,-2);
    sentToMapFlag = false;
  }
}

void CPrediction::loop()
{
  while(true) {
    if (loopActive) {
      updateList();
      predictCollision();
      usleep(100000);
    }
    else {
      Console::updatePred("OFF      ",0);
      sleep(1);
    }
  }
}

void CPrediction::startLoop()
{
  if (!loopActive) {
    loopActive = true;

    Console::addMessage("Prediction started");
    Log::getStream(1)<<"Prediction started"<<endl;
  }
}

void CPrediction::stopLoop()
{
  if (loopActive) {
    clearMap();
    loopActive = false;

    Console::addMessage("Prediction closed");
    Log::getStream(1)<<"Prediction closed"<<endl;
  }
}

bool CPrediction::isRunning()
{
  return loopActive;
}
