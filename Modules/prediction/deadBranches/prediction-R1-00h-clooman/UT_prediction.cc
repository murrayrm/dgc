/**********************************************************
 **
 **  UT_PLANNER.CC
 **
 **    Author: Noel du Toit
 **    Created: Mon Jul 16 16:20
 **
 **
 **********************************************************
 **
 **  Unit test for the planner stack
 **
 **********************************************************/

#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ActuatorState.h>
#include <frames/pose3.h>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <iostream>
#include <sstream>
#include <fstream>
#include <trajutils/traj.hh>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>
#include <skynet/skynet.hh>

#include "Prediction.hh"


using namespace std;

int main(int argc, char **args)
{
  // INITIALIZE THE MAP
  Map* map = new Map();
  map->loadRNDF(args[1]);

  CmdArgs::RNDF_file = args[1];
  CmdArgs::sn_key = skynet_findkey(argc, args);

  // create sample obstacle
  MapElement me;
  me.id=1111;
  me.setTypeVehicle();
  point2 p;
  p.x=0.0;
  p.y=-130.0;
  me.setGeometry(p, 2.0, 5.0);
  me.velocity=point2(0.0, 5.00);
  map->addEl(me);

  // create sample Alice
  VehicleState vehState;
  vehState.localX=-4;
  vehState.localY=-140;
  vehState.localYaw=M_PI /2;
  vehState.utmNorthVel=0;
  vehState.utmEastVel=5;

  PredictionObstacle pObstacle;
  int numberObstacles=0;
  Prediction::init(map);

  Prediction::USE_PARTICLES = true;
  Prediction::PRINT_TRAJ = false;
  uint64_t start = Prediction::getTime();
  Graph_t* graph;
  Prediction::updateList(graph, map,vehState);
  uint64_t ende = Prediction::getTime();

  cout<<"run time "<<(ende-start)/1000000.0<<endl;

  Prediction::predictionReturn pReturn;

 
  for (unsigned int i=1; i<=5; i++)
    {
      pReturn = Prediction::predictCollision(pObstacle,numberObstacles);
  
      if (pReturn == Prediction::CLEAR)
        cout<<"Loop "<< i << " Status: CLEAR"<<endl;
      else if (pReturn == Prediction::COLLISION)
        cout<<"Loop "<< i << " Status: COLLISION"<<endl;
      else
        cout<<"Loop "<< i << " Status: UNKOWN"<<endl;
    }

  cout<<"Number of obstacles found in Prediction: "<<numberObstacles<<endl;

}

