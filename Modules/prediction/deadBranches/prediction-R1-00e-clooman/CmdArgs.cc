#include "CmdArgs.hh"

int CmdArgs::sn_key = 0;
bool CmdArgs::debug = false;
bool CmdArgs::use_RNDF = false;
string CmdArgs::RNDF_file = "";
int CmdArgs::verbose_level = 0;
bool CmdArgs::logging = false;
bool CmdArgs::console = false;
bool CmdArgs::lane_cost = false;
string CmdArgs::log_path = "";
