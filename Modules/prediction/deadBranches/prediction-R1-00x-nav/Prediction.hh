#ifndef PREDICTION_HH_
#define PREDICTION_HH_
#include "map/MapElementTalker.hh"
#include "map/Map.hh"
// stf package includes
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>
#include <time.h>

// skynet, talker, and interfaces
#include "skynet/sn_msg.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/SkynetTalker.hh"
#include "map/MapElementTalker.hh"
#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"
#include "temp-planner-interfaces/PlannerInterfaces.h"
#include "trajutils/traj.hh"
#include "temp-planner-interfaces/Console.hh"

// Other modules/def's
#include "map/Map.hh"
#include <cotk/cotk.h>
#include <ncurses.h>
#define SPOOF_MAP TRUE
#define NO_SPOOF_MAP FALSE

struct predTraj
{
  // timestamp
  unsigned long long traj_time;

  // possible traj at a certain timestamp
  point2 traj_space;

  // id for mapping
  int traj_id;
};


class PredictionObstacle
{
public:
  MapElement me;

  vector<predTraj> traj;
  point2arr mergingSpace;
  bool updated;

  int collisionCounter;
  point2 collisionPoint;
  unsigned long long collisionTime;
  int mapid_traj;

  // variables for laneChanges
  bool laneChangeSentToMap;
  int mapid_laneChange;
};

class CPrediction
{

public: 
  CPrediction(int sn_key);
  virtual ~CPrediction();

  void updateVariables(CTraj* traj, Map* localMap, bool PredictionChangeLane, VehicleState vehS);
  void startLoop();
  void stopLoop();
  bool print_traj;
  bool isRunning();

private:
  static void loop(CPrediction *self);

  // Prediction thread
  pthread_t thread;
  
private :
  // @brief Updates Alice and its trajectory
  void updateList();
  void updateAlice();
  void predictCollision();
  // @brief Computes trajectories for Alice or any other obstacle
  void createTrajectory(MapElement &me, vector<predTraj> *traj, bool mapflag, point2arr *spaceMerging);
  // @brief Utility to add two angles
  int addAngles(double &angle_out, double angle_in1, double angle_in2);
  // @brief Checks whether obstacle is within intersections
  bool withinIntersection(MapElement me);
  void clearMap();
  void checkLaneChange();
  int getFreeMapId();

  int mapcounter;
  int pmapcounter;
  vector<PredictionObstacle> PredictionList;
  PredictionObstacle alicePrediction;
  CMapElementTalker debugMap;
  Map* p_map;
  Map* temp_map;
  vector<double> temp_path_north, temp_path_east, temp_vel_north, temp_vel_east;
  vector<double> path_north, path_east, vel_north, vel_east;
  VehicleState vehState, vehStateTemp;
  pthread_mutex_t updateListMutex;
  int sn_key;
  bool sentToMapFlag, loopActive;
  bool laneChangeTemp, laneChange;
  int clearCounter;
};

// time in seconds that algorithm predicts
const double PREDICTION_HORIZON = 30.0;

// frequency of prediction in [Hz]
const double PREDICTION_RESOLUTION = 0.1;

// seperation distance threshold between Alice and any obstacle that indicates a serious collision
// it should be dependant on the maxium velocity and the prediction resolution
const double SEPERATION_THRESHOLD = 15 * 0.1;

// if distance is greater than the CANCEL_THRESHOLD, the algorithm cancel looking for other collisions at the same timestep
// as even the Particles aren't scattered that far
const double CANCEL_THRESHOLD = 15 * 0.1 +2;

// how often does a collision need to occur that it will be handled as a serious collision threat
const double COLLISION_THRESHOLD = 5;

// map debug channel
const int sendChannel = -10;


#endif /*PREDICTION_HH_*/
