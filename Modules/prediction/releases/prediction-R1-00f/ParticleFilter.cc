/*!ParticleFilter.cc
 * Author: Pete Trautman
 * Last revision: April 18 2007
 * */

#include "ParticleFilter.hh"

using namespace std;

ParticleFilter::ParticleFilter(double resampleFactor,int N,                                  point2& cpt,point2& vel,int modelType, double sigma)
{
   //initialization of randomization variables for gsl.
   gsl_rng_env_setup();
   m_T = gsl_rng_default;
   m_r = gsl_rng_alloc (m_T);
   m_sigma=sigma;
 
   //initialization of scalars
   m_resampleFactor = resampleFactor;
   m_N = N;
   m_Cpt = cpt;
   m_modelType=modelType;
   
   //initialization of vectors
   for (int i=0; i<m_N;i++)
   {
    m_Noise.x = gsl_ran_gaussian(m_r,m_sigma);
    m_Noise.y = gsl_ran_gaussian(m_r,m_sigma);
    
    m_particlesNow.push_back(m_Cpt+m_Noise);
    m_particlesPlus.push_back(m_Cpt+m_Noise);
    
    m_postWeights.push_back(1.0/m_N);
    m_preWeights.push_back(1.0/m_N);

   }
   
   double u;
   point2 tmpvel;
   for(int i=0;i<m_N;i++)
   {
     u = gsl_rng_uniform(m_r);
     tmpvel.x = vel.x*(u-0.5);
     tmpvel.y = vel.y*u;
     m_velPlus.push_back(tmpvel);
     m_velNow.push_back(tmpvel);
     
     m_omegaNow.push_back(0.1*(u-0.5));
     m_omegaPlus.push_back(0.1*(u-0.5));

     m_ang.push_back(atan(m_velNow[i].x/m_velNow[i].y));
   }
}

ParticleFilter::~ParticleFilter() 
{
}

point2arr ParticleFilter::getParticlesNow()
{
  return m_particlesNow;
}

vector<double> ParticleFilter::getang()
{
  return m_ang;
}

point2arr ParticleFilter::getVelNow()
{
  return m_velNow;
}

void ParticleFilter::kalmanFilter()
{
  CvKalman* kalman;
  int dynam_params=1;
  int measure_params=1;
  int control_params=0;
  //CvMat* state = cvCreateMat( 2, 1, CV_32FC1 );
  //CvMat* process_noise = cvCreateMat( 2, 1, CV_32FC1 );
  kalman=cvCreateKalman( dynam_params, measure_params, control_params);
}

point2arr ParticleFilter::sampleConstVel(point2 sampleFactor,double deltaT)
{  
   //constant velocity model with noise
   point2 m_sampleFactor(sampleFactor.x,sampleFactor.y);
   for(int i=0;i<m_N;i++)
    { 
    m_Noise.x = gsl_ran_gaussian(m_r,m_sampleFactor.x*m_sigma);
    m_Noise.y = gsl_ran_gaussian(m_r,m_sampleFactor.y*m_sigma);    
    
    m_particlesPlus[i]=m_particlesNow[i]+m_velNow[i]*deltaT+m_Noise;
    
    m_ang[i] = atan(m_velNow[i].x/m_velNow[i].y);
    
    //u = gsl_rng_uniform(m_r);
    //m_Noise.x = m_Noise.x/10;
    //u = gsl_rng_uniform(m_r);
    //m_Noise.y = u/10;
    m_velPlus[i]=m_velNow[i];//+m_Noise;
    }
  return m_particlesPlus;
}

point2arr ParticleFilter::sampleCoordTurn(point2 sampleFactor,double deltaT)
{   //coordinated turn model
    point2 m_sampleFactor(sampleFactor.x,sampleFactor.y);
    for(int i=0;i<m_N;i++)
    {
     m_Noise.x = gsl_ran_gaussian(m_r,m_sampleFactor.x*m_sigma);
     m_Noise.y = gsl_ran_gaussian(m_r,m_sampleFactor.y*m_sigma); 

     m_particlesPlus[i].x=m_particlesNow[i].x+
		(m_velNow[i].x*sin(deltaT*m_omegaNow[i]))/m_omegaNow[i]+
		(m_velNow[i].y*(1-cos(deltaT*m_omegaNow[i])))/m_omegaNow[i]+
		 m_Noise.x;
     m_velPlus[i].x=m_velNow[i].x*cos(deltaT*m_omegaNow[i])-
                    m_velNow[i].y*sin(deltaT*m_omegaNow[i]);
     
     m_particlesPlus[i].y=m_particlesNow[i].y+
		(m_velNow[i].y*sin(deltaT*m_omegaNow[i]))/m_omegaNow[i]+
		(m_velNow[i].x*(1-cos(deltaT*m_omegaNow[i])))/m_omegaNow[i]+
		 m_Noise.y;
     m_velPlus[i].y=m_velNow[i].y*cos(deltaT*m_omegaNow[i])+
                    m_velNow[i].x*sin(deltaT*m_omegaNow[i]);   
     
     m_omegaPlus[i] = m_omegaNow[i];

     m_ang[i] = atan(m_velNow[i].x/m_velNow[i].y);
     //m_ang[i]= m_omegaPlus[i]*deltaT;
    }
  return m_particlesPlus;
}

void ParticleFilter::weight(vector<point2arr>& lbound,vector<point2arr>& rbound)
                            
{  //weighting Plus against the measurement; returns postWeights, takes preWeights, 

   //for(vector<point2arr>::iterator it=lbound.begin();it!=lbound.end(); it++)
   for(int i=0;i<m_N;i++)//calculating weights
     { 
       point2 mbound = (lbound[i][0]+rbound[i][0])/2.0;
       //point2 mbound = (*it[0]+*it[0])/2.0
       double x=mbound.x-m_particlesPlus[i].x;
       double logWeight=pow(x,2);
       double wSigma=1;	
       m_postWeights[i]=m_preWeights[i]*exp(-logWeight/wSigma);
     }
     
      double sum = 0;
   for(int i=0;i<m_N;i++)//calculating normalizer
     {
       sum=sum+m_postWeights[i];
     }
   
   for(int i=0;i<m_N;i++)//normalization
     {
       m_postWeights[i]=m_postWeights[i]/sum;
     }
}

void ParticleFilter::getPostWeights(vector<double>& postWeights)
{
  postWeights = m_postWeights;
}

void ParticleFilter::resample(int partitions)
{   //resampling using Neff.  returns preWeights either =post or =1/N
    //returns Now either = resampledPlus or just Plus
  
  double particleVar=0;
   for(int k=0; k<m_N;k++)
    {
     particleVar=particleVar+pow(m_postWeights[k],2);
    }
   double Neff=1/particleVar;
   //cout<<"Neff"<<Neff<<endl;
 if(Neff<=m_resampleFactor*m_N)
  { //&&&&&&&&&&if
	//cout<<"resample"<<endl;
     double P[m_N];
   for(int i=0;i<m_N;i++)
    {
     P[i]=m_postWeights[i];
    } 
     
     //gsl initialization
     gsl_ran_discrete_t* g;
     size_t sampleIndex;
     g = gsl_ran_discrete_preproc(sizeof(m_N), P);
     
     //variance in gaussian sampling
     double sigmax = 0.2*m_sigma;
     double sigmay = 0.2*m_sigma;
   for(int i=0;i<m_N;i++)  //resampling loop
     {
      m_Noise.x = gsl_ran_gaussian(m_r,sigmax);
      m_Noise.y = gsl_ran_gaussian(m_r,sigmay); 
      
      sampleIndex = gsl_ran_discrete (m_r,g); //returns sampleIndex i with Prob=weight(i)
      m_particlesNow[i] = m_particlesPlus[(int)sampleIndex]+m_Noise; //new particles are born from old
      m_velNow[i]=m_velPlus[i];
      m_omegaNow[i]=m_omegaPlus[i];
     }
    
   for(int i=0;i<m_N;i++)//resetting the weights
     {
      m_preWeights[i]=1.0/m_N;
     }
  }//&&&&&&&&&if

 else //if resampling unneccesary, then everything is just as .predict said
  {
   //cout<<"don't resample"<<endl;
   m_preWeights=m_postWeights;
   m_particlesNow=m_particlesPlus;
   m_velNow=m_velPlus;
  }
}




















