#ifndef CMDARGS_HH_
#define CMDARGS_HH_

#include <string>

using namespace std;

class CmdArgs {
  public:
    static int sn_key;
    static bool debug;
    static bool use_RNDF;
    static string RNDF_file;
    static int verbose_level;
    static bool logging;
    static bool console;
    static bool lane_cost;
    static string log_path;
};

#endif
