//TODO
// - connect to Alice's seggoals
// - predict obstaclces out of lane
// - check collision


#include "Prediction.hh"
#include "temp-planner-interfaces/CmdArgs.hh"
#include <gcinterfaces/SegGoals.hh>
#include "logic-planner/IntersectionHandling.hh"
#include "temp-planner-interfaces/AliceStateHelper.hh"
#include <limits.h>
#include <temp-planner-interfaces/Log.hh>
#include <temp-planner-interfaces/Console.hh>
#include "ParticleFilter.hh"
#include "dgcutils/DGCutils.hh"

using namespace std;

int Prediction::mapcounter;
int Prediction::pmapcounter;
vector<PredictionObstacle> Prediction::PredictionList;
PredictionObstacle Prediction::alicePrediction;
CMapElementTalker Prediction::debugMap;
bool Prediction::USE_PARTICLES;
bool Prediction::PRINT_TRAJ = false;
Graph_t* Prediction::p_graph = NULL;
Map* Prediction::p_map = NULL;

const double Prediction::PREDICTION_HORIZON = 50.0;

  // frequency of prediction in [Hz]
const double Prediction::PREDICTION_RESOLUTION=0.05;

  // seperation distance threshold between Alice and any obstacle that indicates a serious collision
  // it should be dependant on the maxium velocity and the prediction resolution
const double Prediction::SEPERATION_THRESHOLD=15 * 0.1;
  // if distance is greater than the CANCEL_THRESHOLD, the algorithm cancel looking for other collisions at the same timestep
  // as even the Particles aren't scattered that far
const double Prediction::CANCEL_THRESHOLD=15 * 0.1 +2;

  // how often does a collision need to occur that it will be handled as a serious collision threat
const double Prediction::COLLISION_THRESHOLD=5;

  // map debug channel
const int Prediction::sendChannel=-10;


int Prediction::init(Graph_t* graph, Map* localMap)
{
  // Initialize the debug map
   debugMap.initSendMapElement(CmdArgs::sn_key);

   MapElement el;
   for (unsigned int k=0; k<localMap->prior.data.size(); k++)
     {
       localMap->prior.getEl(el,k);
       debugMap.sendMapElement(&el,sendChannel);
     }

   p_map = localMap;
   p_graph = graph;

   mapcounter=5000;
   pmapcounter=7000;
   return 0;
}


void Prediction::destroy()
{
  return;
}

void Prediction::updateAlice(VehicleState vehState)
{
  MapElement alice;
  alice.set_alice(vehState);
  debugMap.sendMapElement(&alice,sendChannel);

  // spoof information as MapElement doesn't contain correct information
  alice.velocity.x=vehState.utmNorthVel;
  alice.velocity.y=vehState.utmEastVel;
  alice.orientation=vehState.localYaw;
  //  alice.center=point2(vehState.utmNorthing - vehState.localX,vehState.utmEasting - vehState.localY);

  alice.center=point2(vehState.localX,vehState.localY);

  // compute traj. This will be replaced later by the graph
  createTrajectory(alice, &alicePrediction.traj, NO_SPOOF_MAP, USE_PARTICLES);

  if (PRINT_TRAJ) {
    MapElement me;
    point2arr points;
    // send traj to map
    me.setTypePoints();
    me.setColor(MAP_COLOR_GREEN,100);

    for (unsigned l=0; l< alicePrediction.traj.size(); l++)
        for (unsigned n=0; n<alicePrediction.traj[l].traj_space.size(); n++)
          points.push_back(alicePrediction.traj[l].traj_space[n]);

    me.id=pmapcounter++;
    me.setGeometry(points);
    debugMap.sendMapElement(&me,sendChannel);
  }  
}

bool Prediction::withinIntersection(MapElement me)
{
  bool returnvalue=false;
  point2 p;
  LaneLabel lane;
  p.set(me.center);
  
  bool inLane=false;

  if (p_map->getLane(lane,p) != -1)
    if (p_map->isPointInLane(p,lane))
        inLane=true;

  if (inLane) {
    point2arr centerline;
    point2 startpoint;
    
    // get centerline from lane where obstacle is
    p_map->getLaneCenterLine(centerline,lane);
    
    // the predicted trajectory should stop at stoplines
    vector<PointLabel> StopLines;
    
    point2 p_stopline;
    double distance_temp;
    if (p_map->getLaneStopLines(StopLines,lane)!=0) {
      double distance_stopline=INFINITY;
      
      // Find closest stopline in Alice's direction of travel and save PointLabel and distance
      int index=-1;
      for (unsigned int i=0; i<StopLines.size(); i++) {
        p_map->getWaypoint(p_stopline,StopLines[i]);
        p_map->getDistAlongLine(distance_temp,centerline,p_stopline,p);
        
        if (distance_temp>-20 && distance_temp<distance_stopline) {
          index=i;
          distance_stopline=distance_temp;
        }
      }
          
      // if stopline was found
      if (index != -1) {
        vector<PointLabel> WayPointsEntries,WayPointsWithStop,WayPointsNoStop,WayPointExits,WayPoint;
        point2arr leftBound,rightBound;
        vector<MapElement> obstacles;
        
        // populate Waypoints
        WayPointsEntries.clear();
        IntersectionHandling::populateWayPoints(p_graph, p_map, StopLines[index], WayPointsEntries, FALSE);
        
        // Check all WayPoint, get their Exit and check whether Entry and Exit are in the same lane
        for (unsigned i=0; i<WayPointsEntries.size(); i++) {
          // get all exits for this entry
          p_map->getWayPointExits(WayPointExits,WayPointsEntries[i]);
          for (unsigned j=0;j<WayPointExits.size(); j++) {
            // Create TransitionBound between this WayPointEntry and WayPointExit
            IntersectionHandling::getTransition(p_graph, p_map, leftBound, WayPointsEntries[i], WayPointExits[j], -1);
            IntersectionHandling::getTransition(p_graph, p_map, rightBound, WayPointsEntries[i], WayPointExits[j], 1);

            // Get all obstacles within these bounds
            p_map->getObsInBounds(obstacles, leftBound, rightBound);
            
            // look for obstacle in the list
            for (unsigned k=0; k<obstacles.size(); k++)
              if (obstacles[k].id==me.id)
                returnvalue=true;
          }
        }
      }
    }
  }

  return returnvalue;
}

void Prediction::updateList(VehicleState vehState)
{
  pmapcounter = 7000;
  // update Alice
  updateAlice(vehState);

  // receive all obstacles within 500m which should get practically all obstacles as ladars are limited
  vector<MapElement> obstacles;
  point2 position_alice = AliceStateHelper::getPositionFrontBumper(vehState);
  p_map->getObsNearby(obstacles,position_alice,500);

  for (unsigned i =0; i< obstacles.size(); i++)
    {
      // only allow VEHICLES
      if (obstacles[i].type==ELEMENT_VEHICLE)
        {
          bool found=false;

          // search for this obstacle in the list
          for (unsigned int j=0; j<PredictionList.size(); j++)
              if (PredictionList[j].me.id==obstacles[i].id)
                {
                  // Create trajectory
                  if (!withinIntersection(obstacles[i]))
                      createTrajectory(obstacles[i], &(PredictionList[j].traj), SPOOF_MAP, USE_PARTICLES);
                  // do not create any trajectory when obstacle is within an intersection
                  else
                      PredictionList[j].traj.clear();

                  
                  PredictionList[j].me=obstacles[i];
                  
                  // set flag so that list will be updated later
                  PredictionList[j].updated=true;
                  found=true;

                  Log::getStream(1)<<"Prediction: Update obstacle "<<PredictionList[j].me.id<<" Pos "<<PredictionList[j].me.center<<endl;
                  break;
                }
  
          // if me was not found in vehicle list, add it
          if (!found)
            {
              PredictionObstacle predListTemp;

              // Create trajectory
              if (!withinIntersection(obstacles[i]))
                createTrajectory(obstacles[i], &(predListTemp.traj), SPOOF_MAP, USE_PARTICLES);
              // do not create any trajectory when obstacle is within an intersection
              else
                  predListTemp.traj.clear();


              predListTemp.me=obstacles[i];
              predListTemp.updated=true;
              predListTemp.collisionCounter=0;
              // save new element to list
              PredictionList.push_back(predListTemp);
              Log::getStream(1)<<"Prediction: Add obstacle "<<obstacles[i].id<<" Pos "<<obstacles[i].center<<endl;
            }
        }

    }

  MapElement me;
  // clean up vehicle list & output to map
  vector<PredictionObstacle>::iterator v_it;
  vector<PredictionObstacle> tempList;
  for (v_it=PredictionList.begin(); v_it!=PredictionList.end(); v_it++)
    {
      PredictionObstacle vl=*v_it;
      
      if (vl.updated)
        {
          // update flag and save it
          vl.updated=false;
          tempList.push_back(vl);
          *v_it=vl;
          
          if (PRINT_TRAJ) {
            // send obstacle to map
            me=vl.me;
            me.setColor(MAP_COLOR_YELLOW,100);
            debugMap.sendMapElement(&me,sendChannel);
            
            // send traj to map
            me.setTypePoints();
            me.setColor(MAP_COLOR_RED,100);

            // lets try something
            point2arr points;
            
            for (unsigned l=0; l<vl.traj.size(); l++)
                for (unsigned n=0; n<vl.traj[l].traj_space.size(); n++)
                    points.push_back(vl.traj[l].traj_space[n]);

            me.id=pmapcounter++;
            vl.mapid_traj = pmapcounter;
            me.setGeometry(points);
            debugMap.sendMapElement(&me,sendChannel);
          }
        }
      else
        {
          Log::getStream(1)<<"Prediction: Remove obstacle "<<vl.me.id<<" Pos "<<vl.me.center<<endl;
          // clear obstacle from map
          me=vl.me;
          me.setTypeClear();
          debugMap.sendMapElement(&me,sendChannel);

          me.id = vl.mapid_traj;
          debugMap.sendMapElement(&me,sendChannel);
        }
    }

  PredictionList.clear();
  PredictionList = tempList;
}

void Prediction::createTrajectory(MapElement &me, vector<predTraj> *traj, bool mapflag, bool useParticleFilter)
{
  // make sure that traj-vector is empty
  traj->clear();

  //  double orientation=me.orientation;
  //  double orientationVar=me.orientationVar;
  point2_uncertain pos=me.center;
  point2_uncertain vel=me.velocity;
  point2 p,point;
  p.set(pos);
  double angle,dist,d,orientation,t;
  uint64_t starttime;

  LaneLabel lane;

  // return the distance that is predicted given the current speed
  double velAbs=sqrt(pow(vel.x,2) + pow(vel.y,2));
  double dist_prediction=velAbs * PREDICTION_HORIZON;
  double time_prediction=PREDICTION_HORIZON;

  // avoid bug in mapper
  if (mapflag)
    vel.y=-vel.y;

  // hack for obstacle's orientation
  orientation=acos(fabs(vel.x)/velAbs);

  if (vel.x>=0 && vel.y<0)
    orientation=-orientation;
  else if (vel.x<0 && vel.y<0)
    orientation=-M_PI + orientation;
  else if (vel.x<0 && vel.y>=0)
    orientation=M_PI - orientation;

  stringstream s;

  bool inLane=false;

  if (p_map->getLane(lane,p) != -1)
    if (p_map->isPointInLane(p,lane))
      inLane=true;

  // if obstacle is in lane
  if (inLane)
    {
       if (useParticleFilter)
         {
           Console::addMessage("Using particles");
           // in addition call ParticleFilter here
           MapElement me2;
           me2=me;
           if (me2.type==ELEMENT_VEHICLE)
             me2.velocity.y=-me2.velocity.y;
           computeParticles(me2, lane, *traj);
           // end ParticleFilter
         }
       else
        {
          point2arr centerline;
          point2 startpoint;
          
          // get centerline from lane where obstalce is
          p_map->getLaneCenterLine(centerline,lane);
          
          // this function also returns in the second argument the projection of p into the lane
          p_map->getHeading(angle,startpoint,lane,p);
          
          // the predicted trajectory should stop at stoplines
          vector<PointLabel> StopLines;
          
          point2 p_stopline;
          double distance_temp;
          
          if (p_map->getLaneStopLines(StopLines,lane)!=0)
            {
              double distance_stopline=INFINITY;
              
              // Find closest stopline in Alice's direction of travel and save PointLabel and distance
              for (unsigned int i=0; i<StopLines.size(); i++)
                {
                  p_map->getWaypoint(p_stopline,StopLines[i]);
                  p_map->getDistAlongLine(distance_temp,centerline,p_stopline,p);
                  
                  if (distance_temp>0 && distance_temp<distance_stopline)
                    distance_stopline=distance_temp;
                }
              
              // if stopline is closer than the predicted distance, then shorten the predicted distance and predicated time
              if (distance_stopline<INFINITY && distance_stopline<dist_prediction)
                {
                  dist_prediction=distance_stopline;
                  time_prediction=distance_stopline/velAbs;
                }
            }
          
          // some variables that we need later
          predTraj tempTraj;
          starttime=getTime();

          // store startpoint
          tempTraj.traj_space.push_back(startpoint);
          tempTraj.traj_time = starttime; 
          traj->push_back(tempTraj);
          
          // this function call is needed for later. it computes the distance from the beginning of the centerline to the startpoint
          p_map->getDistAlongLine(dist,centerline,startpoint);
          
          // estimate the direction of travel within the lane. It might be possible that vehicle is travelling in wrong lane
          // calculate the difference of the angle lane and the heading of obstacle
          // if the absolute difference is larger than pi, both headings are opposite to each other
          double heading_delta;
          
          addAngles(heading_delta,-angle,orientation);
          
          t=0;
          d=0;

          // loop along the centerline and create trajectory
      
          while (t<time_prediction)
            {
              t+=PREDICTION_RESOLUTION; // time step in [s]
              d=velAbs*t;
              tempTraj.traj_time = starttime+ uint64_t(t*1000000);
              
              // depending on obstacle's heading regarding the lane's heading, calculate the distance
              // if correct direction of travel ...
              if ((heading_delta <= M_PI/4) && (heading_delta >= -M_PI/40))
                p_map->getPointAlongLine(point,centerline,dist+d);
              // if in opposite direction of travel...
              else if ((heading_delta >= 3*M_PI/4) || (heading_delta <= -3*M_PI/4))
                p_map->getPointAlongLine(point,centerline,dist-d);
              // else...diagonal to lane ...
              else
                {
                  point.x = p.x + cos(orientation) * d;
                  point.y = p.y + sin(orientation) * d;
                }
              
              tempTraj.traj_space.clear();
              tempTraj.traj_space.push_back(point);
              
              traj->push_back(tempTraj);
            }
        }
    }
  // if obstacle is out of lane, compute predicated trajectory along the obstacle's orientation
  else
    {
      predTraj tempTraj;

      // store startpoint
      tempTraj.traj_time=0;
      tempTraj.traj_space.push_back(p);
      traj->push_back(tempTraj);

      t=0;
      d=0;
      while (t<time_prediction)
        {
          t+=PREDICTION_RESOLUTION; // time step in [s]
          d=velAbs*t;
          tempTraj.traj_time =  starttime+ uint64_t(t*1000000);

          point.x = p.x + cos(orientation) * d;
          point.y = p.y + sin(orientation) * d;

          tempTraj.traj_space.clear();
          tempTraj.traj_space.push_back(point);

          traj->push_back(tempTraj);
        }
    }
}

int Prediction::addAngles(double &angle_out, double angle_in1, double angle_in2) {
  if ((angle_in1+angle_in2)>M_PI) 
    angle_out=angle_in1+angle_in2-2*M_PI;
  else if ((angle_in1+angle_in2)<=-M_PI)
    angle_out=angle_in1+angle_in2+2*M_PI;
  else
    angle_out=angle_in1+angle_in2;
  
  return 0;
}

uint64_t Prediction::getTime()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
}

Prediction::predictionReturn Prediction::predictCollision(VehicleState vehState, PredictionObstacle &pObstacle, int &numberObstacles)
{
  point2 p_alice,p_obstacle;
  double dist;
  unsigned int upperBound;
  bool possibleCollision=false;
  bool cancelLoop=false;
  LaneLabel lane;


   // loop through all found obstacles and compare trajectories
   for (unsigned int i=0; i<PredictionList.size(); i++)
     {
       // as Alice and the obstacle might have different prediction horizons, adjust upperBound
       if (alicePrediction.traj.size()<=PredictionList[i].traj.size())
         upperBound=alicePrediction.traj.size();
       else
         upperBound=PredictionList[i].traj.size();

       possibleCollision=false;
       for (unsigned int j=0; j<upperBound; j++)
         {
           cancelLoop=false;
           for (unsigned int k=0; k<PredictionList[i].traj[j].traj_space.size(); k++)
             {
               for (unsigned int l=0; l<alicePrediction.traj[j].traj_space.size(); l++)
                 {
                   p_obstacle = PredictionList[i].traj[j].traj_space[k];
                   p_alice = alicePrediction.traj[j].traj_space[l];
          
                   // compute distance between both points of trajectories
                   dist = sqrt( pow(p_alice.x-p_obstacle.x,2) + pow(p_alice.y-p_obstacle.y,2) );

                   double dist2currentAlice = sqrt( pow(vehState.localX-p_obstacle.x,2) + pow(vehState.localY-p_obstacle.y,2) );
          
                   // if dist is smaller than threshold, assume collision and increse collision counter
                   // at the same time, don't send any collisions that are closer than 1.5m This might distract Alice (wheet, etc.)
                   if (dist<SEPERATION_THRESHOLD && dist2currentAlice>1.5)
                     {
                       PredictionList[i].collisionCounter++;
                       PredictionList[i].collisionPoint = p_obstacle;
                       PredictionList[i].collisionTime = PredictionList[i].traj[j].traj_time;
                       possibleCollision=true;
                       
                       // only count collision once in case the trajectories are so close
                       // this will also automatically return the closest collision as the vector is time discretisated
                       break;
                     }
                   else if (dist>=CANCEL_THRESHOLD)
                     {
                       cancelLoop = true;
                       break;
                     }
                 }
               if (possibleCollision || cancelLoop )
                 break;
             }
           if (possibleCollision)
             break;
         }

       // if no collision was detected, reset counter
       if (!possibleCollision)
         PredictionList[i].collisionCounter=0;
     }

   // initialize the return values
   pObstacle.collisionTime = 0;
   pObstacle.collisionPoint = point2(0,0);

   // set collisionTime to maximum
   pObstacle.collisionTime=-1;

   // return the number of obstacles that are stored in the vehicle list. This is the number of obstacles that are under prediction
   numberObstacles = PredictionList.size();
   int index=-1;

   // If there might be several collisions, find and return the closest one
   for (unsigned int i=0; i<PredictionList.size(); i++)
     {
       if ((PredictionList[i].collisionCounter>=COLLISION_THRESHOLD) && (PredictionList[i].collisionTime <= pObstacle.collisionTime)) {
         pObstacle.collisionTime = PredictionList[i].collisionTime;
         pObstacle.collisionPoint = PredictionList[i].collisionPoint;
         pObstacle.me = PredictionList[i].me;
         index=i;
       }
     }

   // if at least one very possible collision was found, return its coordinates and time
   MapElement me;
   if (index != -1)
     {
       // send predicted collision to debugMap
       if (PRINT_TRAJ) {
         me.id=9999;
         me.setTypePoints();

         me.setColor(MAP_COLOR_RED,100);
         me.setGeometry(pObstacle.collisionPoint, 2.0);
         debugMap.sendMapElement(&me,sendChannel);
       }
       return COLLISION;
     }
   else
     {
       // clear predicted collision point out of map
       if (PRINT_TRAJ) {
        me.id=9999;
        me.setTypeClear();
        debugMap.sendMapElement(&me,sendChannel);
       }
     }


   return CLEAR;
}



void Prediction::computeParticles(MapElement me, LaneLabel lane, vector<predTraj> &traj)
{
  // initialize
  double resampleFactor = 0.9; //Neff <= resampleFactor*N
  int N = 100; //number of particles
  int modelType=1;
  double sigma=0.5; //initial covariance in the samples
  vector<double> postWeights;

  // create ParticleFilter
  point2 position, velocity;
  position.set(me.center);
  velocity.set(me.velocity);
  double meansAbsVel = sqrt(pow(me.velocity.x, 2) + pow(me.velocity.y, 2));
  ParticleFilter dob1PF(resampleFactor, N, position, velocity, modelType, sigma, meansAbsVel);   
  point2arr particlesNow=dob1PF.getParticlesNow();

  // Variables for prediction loop
  vector<double> ang(N);
  point2 dob1size(2,5);
  predTraj tempTraj;
   
  MapElement covEll;
  double datax[N],datay[N],covAng[N];
  double varX,varY,angCovEll;
  point2 cptCovEll;
  int sizeEll=100;
  point2arr ell(sizeEll);
  point2 ellPt;
  vector<point2> centerPoint(N);

  for (int i =0; i< PREDICTION_HORIZON/PREDICTION_RESOLUTION; ++i)
    { 

      int mapc=888;
      MapElement dob1;
      ang = dob1PF.getang();
     for(int k=0; k<N;k++)
         {   
          dob1.id=mapc++;
          dob1.setTypeVehicle();

          dob1.setColor(MAP_COLOR_RED,100);
          dob1.setGeometry(particlesNow[k], 5, 2,ang[k]);
          debugMap.sendMapElement(&dob1,sendChannel);
         }

      point2 sampleFactor(0.4,0.4); //noise in sampling distribution
      point2arr particlesPlus = dob1PF.sampleConstVel(sampleFactor,PREDICTION_RESOLUTION*2); 

      vector<double> centerHeading(N); //map sampling
      for(int k=0; k<N;k++) 
       {
        p_map->getHeading(centerHeading[k],centerPoint[k],lane,particlesNow[k]);
       }

      particlesPlus=dob1PF.sampleMap(centerHeading,sampleFactor,PREDICTION_RESOLUTION);
      particlesNow=dob1PF.getParticlesNow();

      
       //covariance ellipse calculations
      for(int k=0; k<N;k++)
       {
        datax[k] = particlesNow[k].x;
	datay[k] = particlesNow[k].y;
        covAng[k] = centerHeading[k];
	//covAng[k] = ang[k];
       }
      
      cptCovEll.x = gsl_stats_mean(datax,1,N);
      cptCovEll.y = gsl_stats_mean(datay,1,N);
      angCovEll = gsl_stats_mean(covAng,1,N);
      
      varX = gsl_stats_variance_m(datax,1,N,cptCovEll.x);
      varY = gsl_stats_variance_m(datay,1,N,cptCovEll.y);
      
      
      for(i=0;i<sizeEll;i++)
       {  
         ell[i].x = sqrt(varX)*cos(i);
         ell[i].y = sqrt(varY)*sin(i);
	 ell[i].x = ell[i].x*cos(angCovEll)+ell[i].y*sin(angCovEll);
	 ell[i].y = -ell[i].y*cos(angCovEll)+ell[i].x*sin(angCovEll);
	 ell[i].x = ell[i].x +cptCovEll.x;
	 ell[i].y = ell[i].y +cptCovEll.y;
        }
      
      covEll.id=-122;
      covEll.setColor(MAP_COLOR_YELLOW,100);
      covEll.setGeometry(ell);
      debugMap.sendMapElement(&covEll,sendChannel);

      usleep(100000);
    }
}

          
          
