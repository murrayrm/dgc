#include <getopt.h>
#include "cmdline.h"
#include <iostream>
#include "Prediction.hh"
#include "dgcutils/DGCutils.hh"
#include "dgcutils/cfgfile.h"
#include "skynet/skynet.hh"
#include <sys/time.h>
#include <sys/stat.h>
#include "CmdArgs.hh"

using namespace std;             
     
int main(int argc, char **argv)              
{
  //  gengetopt_args_info cmdline;    
 
  //  CmdArgs::sn_key = skynet_findkey(argc, argv);
 
  //  if (cmdline_parser(argc, argv, &cmdline) != 0)
  //    exit (1);

  // Initialize the map with rndf if given
  //  if (cmdline.rndf_given){
  //    CmdArgs::use_RNDF = true;
  //    CmdArgs::RNDF_file = cmdline.rndf_arg;
  //    if (!CmdArgs::console)
  //      cout << "RNDF Filename in = "  << CmdArgs::RNDF_file << endl;
  //  }     

  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0)
  {
    exit (1);
  }
  // Figure out what skynet key to use
  int sn_key = skynet_findkey(argc, argv);
  
  int debugLevel, verboseLevel;
  debugLevel = cmdline.debug_arg;
  verboseLevel = cmdline.verbose_arg; 
  CmdArgs::RNDF_file = cmdline.rndf_arg;
  CmdArgs::sn_key = skynet_findkey(argc, argv);
  classPrediction* aPrediction = new classPrediction(sn_key, !cmdline.nowait_given, debugLevel, verboseLevel, cmdline.log_flag);
  DGCstartMemberFunctionThread(aPrediction, &classPrediction::updateMapThread);
  aPrediction->PredictionLoop();
  return 0;
}

