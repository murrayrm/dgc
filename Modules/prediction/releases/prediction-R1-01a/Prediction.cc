//TODO
// - connect to Alice's seggoals
// - predict obstaclces out of lane
// - check collision


#include "Prediction.hh"
#include "temp-planner-interfaces/CmdArgs.hh"
#include <gcinterfaces/SegGoals.hh>
#include "logic-planner/IntersectionHandling.hh"
#include "temp-planner-interfaces/AliceStateHelper.hh"
#include <limits.h>
#include <temp-planner-interfaces/Log.hh>
#include <temp-planner-interfaces/Console.hh>
#include <temp-planner-interfaces/Utils.hh>
#include "dgcutils/cfgfile.h"
#include "dgcutils/DGCutils.hh"
#include "temp-planner-interfaces/ConfigFile.hh"

using namespace std;

CPrediction::CPrediction(int key, int visualization_level)
{
  // Output statistic information
  time_t currentTime;
  time(&currentTime);
  logPrediction = new CLog(1);
  string filename = CmdArgs::log_filename;

  filename.replace(filename.find("planner"),7,"prediction");
  logPrediction->setGenericLogFile(filename.c_str());
  logPrediction->getStream(1)<<"% Starting up Prediction at "<<ctime(&currentTime)<<endl;

  sn_key = key;
  DGCcreateMutex(&updateListMutex);

  mapcounter = 5000;
  pmapcounter = 1;
  clearCounter = 0;
  darpaBallCounter = 0;
  sentCollisionToMap = false;
  loopActive = false;
  temp_map = new Map();
  p_map = new Map();
  laneChangeTemp=false;
  laneChange = false;
  predictionMap.initSendMapElement(sn_key);
  vlevel = visualization_level;
  darpaBall = false;

  // read configuration file
  char *path;
  path=dgcFindConfigFile("prediction.conf","prediction");
  if (fopen(path,"r") == NULL) {
    logPrediction->getStream(1)<<"Warning: No configuration file"<<endl;
    PREDICTION_HORIZON = 30.0;
    PREDICTION_RESOLUTION = 0.1;
    SEPERATION_THRESHOLD = 15*0.1;
    CANCEL_THRESHOLD = 15 * 0.1 +2;
    COLLISION_THRESHOLD = 5;
    sendChannel = -10;
    DARPABALL_COLLISION_THRESHOLD = 1.5;
    DARPABALL_COUNTER_THRESHOLD = 5;
    DARPABALL_ALICE_VELOCITY = 10.0;
    PREDICTION_SIZE = 2.0;
  }
  else {
    logPrediction->getStream(1)<<"Read configuration file"<<endl;
    ConfigFile config(path);
    
    config.readInto(PREDICTION_HORIZON,"PREDICTION_HORIZON");
    config.readInto(PREDICTION_RESOLUTION,"PREDICTION_RESOLUTION");
    config.readInto(SEPERATION_THRESHOLD,"SEPERATION_THRESHOLD");
    config.readInto(CANCEL_THRESHOLD,"CANCEL_THRESHOLD");
    config.readInto(COLLISION_THRESHOLD,"COLLISION_THRESHOLD");
    config.readInto(sendChannel,"sendChannel");
    config.readInto(DARPABALL_COLLISION_THRESHOLD,"DARPABALL_COLLISION_THRESHOLD");
    config.readInto(DARPABALL_COUNTER_THRESHOLD,"DARPABALL_COUNTER_THRESHOLD");
    config.readInto(DARPABALL_ALICE_VELOCITY,"DARPABALL_ALICE_VELOCITY");
    config.readInto(PREDICTION_SIZE,"PREDICTION_SIZE");
  }

}

void CPrediction::updateVariables(CTraj* traj, Map* localMap, bool PredictionChangeLane, VehicleState vehS, bool &returnDarpaBall)
{
  if (localMap == NULL)
    return;

  DGClockMutex(&updateListMutex);

  temp_path_north.clear();
  temp_path_east.clear();
  temp_vel_north.clear();
  temp_vel_east.clear();

  // copy path
  for (int i = 0; i<traj->getNumPoints(); i++) {
    // copy path
    temp_path_north.push_back(traj->getNdiffarray(0)[i]);
    temp_path_east.push_back(traj->getEdiffarray(0)[i]);
    // copy velocity profile
    temp_vel_north.push_back(traj->getNdiffarray(1)[i]);
    temp_vel_east.push_back(traj->getEdiffarray(1)[i]);
  }

  // copy map
  *temp_map = *localMap;

  laneChangeTemp = PredictionChangeLane;

  vehStateTemp = vehS;
  
  DGCunlockMutex(&updateListMutex);

  // return whether the DarpaBall scenario occurs
  if (darpaBallCounter>DARPABALL_COUNTER_THRESHOLD) {
    returnDarpaBall = true;
    logPrediction->getStream(1)<<"DARPA BALL DARPA BALL"<<endl;
  }
  else
    returnDarpaBall = false;
}

int CPrediction::getFreeMapId()
{
  int i=pmapcounter;
  while (true) {
    bool found = false;
    for (unsigned int k=0; k<PredictionList.size(); k++)
      if (PredictionList[k].mapid_traj == i || PredictionList[k].mapid_laneChange == i)
        found = true;
    if (!found)
      return i;
    i++;
  }
}

CPrediction::~CPrediction()
{
  delete temp_map;
  delete p_map;
  DGCdeleteMutex(&updateListMutex);
}

void CPrediction::updateAlice()
{
  predTraj tempTraj;
  alicePrediction.traj.clear();

  // convert space trajectory into more useful structure
  vector<point2> space;
  vector<double> distInSpace;
  vector<double> velProfil;
  double dist = 0.0;
  space.clear();
  distInSpace.clear();
  velProfil.clear();
 
  for (unsigned int i = 0; i<path_north.size(); i++) {
    if (i!=0)
      dist += sqrt(pow(path_north[i] - path_north[i-1], 2) + pow(path_east[i] - path_east[i-1], 2));
    space.push_back(point2(path_north[i], path_east[i]));
    distInSpace.push_back(dist);
    velProfil.push_back(sqrt(pow(vel_north[i],2) + pow(vel_east[i],2)));
  }

  // store current location
  unsigned long long starttime;
  DGCgettime(starttime);
  tempTraj.traj_time = starttime;
  point2 currentPosition = AliceStateHelper::getPositionFrontBumper(vehState);
  tempTraj.traj_space = currentPosition;
  alicePrediction.traj.push_back(tempTraj);

  // compute trajectory in time and space
  double t, s, v;
  t = 0;
  v =  AliceStateHelper::getVelocityMag(vehState);
  int index=0;
  while (t<PREDICTION_HORIZON) {
    t+=PREDICTION_RESOLUTION; // time step in [s]
  
    s = v * t;

    // the following search algorithm is not very exact and won't return the absolut correct velocity
    // each time. But as the time steps in prediction are very small, this should be sufficient
    for (unsigned l=index; l<distInSpace.size(); l++) {
      if (distInSpace[l]>s) {
        tempTraj.traj_space = space[l];
        v = velProfil[l];
        // save index and decrease it; this should speed up the search in the next loop
        index = l-1;
        break;
      }
    }
      
    tempTraj.traj_time = starttime + (uint64_t) t*1000000;
    alicePrediction.traj.push_back(tempTraj);
  }

  // send traj to map
  point2arr points;
  for (unsigned l=0; l< alicePrediction.traj.size(); l++)
    points.push_back(alicePrediction.traj[l].traj_space);
  
  if (vlevel>2) {
    MapElement me;
    me.id = 1302;
    me.setTypePoints();
    me.setGeometry(points);
    me.setColor(MAP_COLOR_GREEN,100);
    predictionMap.sendMapElement(&me, sendChannel);

    MapElement alice;
    alice.set_alice(vehState);
    predictionMap.sendMapElement(&alice,sendChannel);
  }
}

bool CPrediction::withinIntersection(MapElement me)
{
  vector<PointLabel> StopLines;
  point2 p_stopline, p_temp, p;
  LaneLabel lane;
  p.set(me.center);
  
  bool inLane=false;

  if (p_map->getLane(lane,p) != -1)
    if (p_map->isPointInLane(p,lane))
      inLane=true;

  if (inLane) {
    point2arr centerline;
    point2 startpoint;
    
    // get centerline from lane where obstacle is
    p_map->getLaneCenterLine(centerline,lane);
    
    // the predicted trajectory should stop at stoplines
    double distance_temp;
    if (p_map->getLaneStopLines(StopLines,lane)!=0) {
      double distance_stopline=INFINITY;
      
      // Find closest stopline in obstacle's direction of travel and save PointLabel and distance
      int index=-1;
      for (unsigned int i=0; i<StopLines.size(); i++) {
        p_map->getWaypoint(p_temp,StopLines[i]);
        p_map->getDistAlongLine(distance_temp,centerline,p_stopline,p);
        
        if (distance_temp>-10.0 && distance_temp<distance_stopline) {
          index=i;
          distance_stopline=distance_temp;
          p_stopline = p_temp;
        }
      }
          
      // if stopline was found and obstacle close to stopline, return true
      if (index != -1 && sqrt(pow(me.position.x - p_stopline.x, 2) + pow(me.position.y - p_stopline.y, 2)) < 10.0)
        return true;
    }
  }

  return false;
}

void CPrediction::updateList()
{

  // Create copies
  DGClockMutex(&updateListMutex);
  if (temp_map == NULL || temp_path_north.size() == 0) {
    DGCunlockMutex(&updateListMutex);
    return;
  }
  *p_map = *temp_map;
  path_north.clear();
  path_east.clear();
  vel_north.clear();
  vel_east.clear();
  path_north = temp_path_north;
  path_east = temp_path_east;
  vel_north = temp_vel_north;
  vel_east = temp_vel_east;
  laneChange = laneChangeTemp;
  vehState = vehStateTemp;
  DGCunlockMutex(&updateListMutex);

  // If there is no valid path yet, return out of the function
  if (path_north.size() == 0)
    return;

  // update Alice
  updateAlice();

  // receive all obstacles within 500m which should get practically all obstacles as ladars are limited
  vector<MapElement> obstacles;
  point2 position_alice = point2(path_north[0], path_east[0]);
  p_map->getObsNearby(obstacles,position_alice,500);

  for (unsigned i =0; i< obstacles.size(); i++) {
    // only allow VEHICLES
    if (obstacles[i].isVehicle() && !obstacles[i].isPredicted()) {
      bool found=false;

      // search for this obstacle in the list
      for (unsigned int j=0; j<PredictionList.size(); j++)
        if (PredictionList[j].me.id==obstacles[i].id) {
          // Create trajectory
          if (!withinIntersection(obstacles[i]))
            createTrajectory(obstacles[i], &(PredictionList[j].traj), &(PredictionList[j].mergingSpace));
          // do not create any trajectory when obstacle is within an intersection
          else
            PredictionList[j].traj.clear();
          
                  
          PredictionList[j].me=obstacles[i];
          
          // set flag so that list will be updated later
          PredictionList[j].updated=true;
          found=true;
          
          //          logPrediction->getStream(1)<<"Prediction: Update obstacle "<<PredictionList[j].me.id<<" Pos "<<PredictionList[j].me.center<<endl;
          break;
        }
  
      // if me was not found in vehicle list, add it
      if (!found) {
        PredictionObstacle predListTemp;
        
        // Create trajectory
        if (!withinIntersection(obstacles[i]))
          createTrajectory(obstacles[i], &(predListTemp.traj), &(predListTemp.mergingSpace));
        // do not create any trajectory when obstacle is within an intersection
        else
          predListTemp.traj.clear();
        
        
        predListTemp.me=obstacles[i];
        predListTemp.mapid_traj = getFreeMapId();
        predListTemp.mapid_laneChange = getFreeMapId();
        predListTemp.updated=true;
        predListTemp.collisionCounter=0;
        predListTemp.laneChangeSentToMap = false;
        // save new element to list
        PredictionList.push_back(predListTemp);
        //        logPrediction->getStream(1)<<"Prediction: Add obstacle "<<obstacles[i].id<<" Pos "<<obstacles[i].center<<endl;
      }
    }
  }

  // clean up vehicle list & output to map
  vector<PredictionObstacle>::iterator v_it;
  vector<PredictionObstacle> tempList;
  for (v_it=PredictionList.begin(); v_it!=PredictionList.end(); v_it++) {
    PredictionObstacle vl=*v_it;
    
    if (vl.updated) {
      // update flag and save it
      vl.updated=false;
      tempList.push_back(vl);
      *v_it=vl;
      
      // send obstacle to map
      if (vlevel>2) {
        MapElement me;

        // send obstacle itself
        predictionMap.sendMapElement(&(vl.me), sendChannel);
      
        // send traj to map
        point2arr points;
        for (unsigned l=0; l<vl.traj.size(); l++)
          points.push_back(vl.traj[l].traj_space);
        me.id = vl.mapid_traj;
        me.setTypePoints();
        me.setGeometry(points);
        me.setColor(MAP_COLOR_RED,100);
        predictionMap.sendMapElement(&me, sendChannel);
      }
    }
    else {
      MapElement me;
      if (vlevel>2) {
        // clear obstacle from map
        me.id = vl.me.id;
        me.setTypeClear();
        predictionMap.sendMapElement(&me, sendChannel);
        
        // clear trajectory
        me.id = vl.mapid_traj;
        predictionMap.sendMapElement(&me, sendChannel);
      }        

      // clear boxes created while lane changing
      if (vl.laneChangeSentToMap) {
        me.setId(MODtrafficplanner,vl.mapid_laneChange);
        me.setTypeClear();
        predictionMap.sendMapElement(&me, 0);
        predictionMap.sendMapElement(&me, -2);
        if (vlevel>2)
          predictionMap.sendMapElement(&me, sendChannel);
      }
    }
  }

  PredictionList.clear();
  PredictionList = tempList;
}

double CPrediction::getOrientation(double velX, double velY)
{
  double velAbs = min(sqrt(pow(velX,2) + pow(velY,2)), 15.0);
  double orientation=acos(fabs(velX)/velAbs);

  if (velX>=0 && velY<0)
    orientation=-orientation;
  else if (velX<0 && velY<0)
    orientation=-M_PI + orientation;
  else if (velX<0 && velY>=0)
    orientation=M_PI - orientation;

  return orientation;
}

void CPrediction::createTrajectory(MapElement &me, vector<predTraj> *traj, point2arr *spaceMerging)
{
  // make sure that traj-vector is empty
  traj->clear();
  spaceMerging->clear();

  point2_uncertain pos=me.center;
  point2_uncertain vel=me.velocity;
  point2 p,point;
  p.set(pos);
  double angle,dist,d,orientation,t;
  unsigned long long starttime;

  LaneLabel lane;
  // return the distance that is predicted given the current speed
  double velAbs=min(sqrt(pow(vel.x,2) + pow(vel.y,2)), 15.0);
  double dist_prediction=velAbs * PREDICTION_HORIZON;
  double time_prediction=PREDICTION_HORIZON;

  // hack for obstacle's orientation
  orientation = getOrientation(vel.x, vel.y);
  //  logPrediction->getStream(1)<<"createTrajectory velocity. vel.x="<<vel.x<<" vel.y="<<vel.y<<endl;
  //  logPrediction->getStream(1)<<"createTrajectory orientation problem. Estimated orientation = "<<orientation<<" MapElement.orientation="<<me.orientation<<endl;

  stringstream s;

  bool inLane=false;

  if (p_map->getLane(lane,p) != -1)
    if (p_map->isPointInLane(p,lane))
      inLane=true;

  // if obstacle is in lane
  if (inLane) {
    point2arr centerline;
    point2 startpoint;
    
    // get centerline from lane where obstalce is
    p_map->getLaneCenterLine(centerline,lane);
    
    // this function also returns in the second argument the projection of p into the lane
    p_map->getHeading(angle,startpoint,lane,p);
    
    // the predicted trajectory should stop at stoplines
    vector<PointLabel> StopLines;
    
    point2 p_stopline;
    double distance_temp;
    
    if (p_map->getLaneStopLines(StopLines,lane)!=0) {
      double distance_stopline=INFINITY;
      
      // Find closest stopline in Alice's direction of travel and save PointLabel and distance
      for (unsigned int i=0; i<StopLines.size(); i++) {
        p_map->getWaypoint(p_stopline,StopLines[i]);
        p_map->getDistAlongLine(distance_temp,centerline,p_stopline,p);
        
        if (distance_temp>0 && distance_temp<distance_stopline)
          distance_stopline=distance_temp;
      }
       
      // if stopline is closer than the predicted distance, then shorten the predicted distance and predicated time
      if (distance_stopline<INFINITY && distance_stopline<dist_prediction) {
        dist_prediction=distance_stopline;
        time_prediction=distance_stopline/velAbs;
      }
    }
    
    // some variables that we need later
    predTraj tempTraj;
    DGCgettime(starttime);
    
    // store startpoint
    tempTraj.traj_space = startpoint;
    tempTraj.traj_time = starttime; 
    traj->push_back(tempTraj);
    
    // this function call is needed for later. it computes the distance from the beginning of the centerline to the startpoint
    p_map->getDistAlongLine(dist,centerline,startpoint);
    
    // estimate the direction of travel within the lane. It might be possible that vehicle is travelling in wrong lane
    // calculate the difference of the angle lane and the heading of obstacle
    // if the absolute difference is larger than pi, both headings are opposite to each other
    double heading_delta;
    
    addAngles(heading_delta,-angle,orientation);
    
    t=0;
    // loop along the centerline and create trajectory
    while (t<time_prediction) {
      t+=PREDICTION_RESOLUTION; // time step in [s]
      d=velAbs*t;
      
      // depending on obstacle's heading regarding the lane's heading, calculate the distance
      // if correct direction of travel ...
      if ((heading_delta <= M_PI/4) && (heading_delta >= -M_PI/4))
        p_map->getPointAlongLine(point,centerline,dist+d);
      // if in opposite direction of travel...
      else if ((heading_delta >= 3*M_PI/4) || (heading_delta <= -3*M_PI/4))
        p_map->getPointAlongLine(point,centerline,dist-d);
      // else...diagonal to lane ...
      else {
        point.x = p.x + cos(orientation) * d;
        point.y = p.y + sin(orientation) * d;
      }
      
      tempTraj.traj_time = starttime+ (uint64_t)t*1000000;
      tempTraj.traj_space = point;
      
      traj->push_back(tempTraj);
    }

    if (laneChange) {
      //      logPrediction->getStream(1)<<"createTrajectory; laneChange true"<<endl;
      // create safety corridor in case that Alice wants to perform a lange change
      double v_alice =  AliceStateHelper::getVelocityMag(vehState);
      double heading_alice = AliceStateHelper::getHeading(vehState);
      addAngles(heading_delta, -heading_alice, orientation);

      double relVel = 0.0;

      // if going the same direction, use velocity difference
      if ((heading_delta <= M_PI/4) && (heading_delta >= -M_PI/4)) {
        if (v_alice<velAbs)
          relVel = velAbs-v_alice + velAbs*2;
        else
          relVel = velAbs*2;
      }      
      // if going the other direction, add velocity
      else if ((heading_delta >= 3*M_PI/4) || (heading_delta <= -3*M_PI/4))
        relVel = velAbs + v_alice;

      logPrediction->getStream(1)<<"createTrajectory: laneChange v_alice="<<v_alice<<" ObsVelAbs="<<velAbs<<" headingDelta="<<heading_delta<<" relVel= "<<relVel<<endl;

      if (relVel > 0.0) {
        t=0;
        d=0;
        while (t<10.0) {
          t+=PREDICTION_RESOLUTION*4; // time step in [s]
          d=relVel*t;
	  
          point.x = p.x + cos(orientation) * d;
          point.y = p.y + sin(orientation) * d;
          spaceMerging->push_back(point);
        }
      }
    }
  }
  // if obstacle is out of lane, compute predicated trajectory along the obstacle's orientation
  else {
    predTraj tempTraj;
    
    // store startpoint
    tempTraj.traj_time = 0;
    tempTraj.traj_space = p;
    traj->push_back(tempTraj);
    
    t=0;
    d=0;
    while (t<time_prediction) {
      t+=PREDICTION_RESOLUTION; // time step in [s]
      d=velAbs*t;
      
      point.x = p.x + cos(orientation) * d;
      point.y = p.y + sin(orientation) * d;
      
      tempTraj.traj_time =  starttime+ uint64_t(t*1000000);
      tempTraj.traj_space = point;
      
      traj->push_back(tempTraj);
    }
  }
}

int CPrediction::addAngles(double &angle_out, double angle_in1, double angle_in2) {
  if ((angle_in1+angle_in2)>M_PI) 
    angle_out=angle_in1+angle_in2-2*M_PI;
  else if ((angle_in1+angle_in2)<=-M_PI)
    angle_out=angle_in1+angle_in2+2*M_PI;
  else
    angle_out=angle_in1+angle_in2;
  
  return 0;
}

void CPrediction::predictCollision()
{
  point2 p_aliceTraj, p_obstacleTraj, p_alice;
  double distTraj;
  bool possibleCollision=false;
  bool cancelLoop=false;
  

  bool darpaBallOccured = false;
  
  LaneLabel lane;

  if (path_north.size() == 0)
    return;

  // loop through all found obstacles and compare trajectories
  for (unsigned int i=0; i<PredictionList.size(); i++) {
    // as Alice and the obstacle might have different prediction horizons, adjust upperBound
    possibleCollision=false;
    double minDarpaBall = INFINITY;
    bool gotCollision = false;
    for (unsigned int j=0; j<PredictionList[i].traj.size(); j++) {
      cancelLoop=false;

      p_obstacleTraj = PredictionList[i].traj[j].traj_space;
      p_alice = AliceStateHelper::getPositionFrontBumper(vehState);
      
      // compute distance between both points of trajectories
      double dist2currentAlice = sqrt( pow(p_alice.x - p_obstacleTraj.x,2) + pow(p_alice.y - p_obstacleTraj.y,2) );

      if (dist2currentAlice<minDarpaBall)
        minDarpaBall = dist2currentAlice;

      // DARPA Ball
      double aliceVelocity = AliceStateHelper::getVelocityMag(vehState);
      double aliceHeading = AliceStateHelper::getHeading(vehState);

      double obstacleHeading = getOrientation(PredictionList[i].me.velocity.x, PredictionList[i].me.velocity.y);

      double heading_delta;
      addAngles(heading_delta, -aliceHeading, obstacleHeading);
    
      bool oppositeDirection = false;
      if ((heading_delta >= 3*M_PI/4) || (heading_delta <= -3*M_PI/4))
        oppositeDirection = true;
      
      if (dist2currentAlice < DARPABALL_COLLISION_THRESHOLD && aliceVelocity<DARPABALL_ALICE_VELOCITY && oppositeDirection) {
        darpaBallOccured = true;

        if (vlevel>2) {
          MapElement me;
          me.id=9000;
          me.setTypePoints();
          me.setGeometry(p_obstacleTraj, PREDICTION_SIZE);
          me.setPosition(p_obstacleTraj);
          me.setColor(MAP_COLOR_MAGENTA);
          predictionMap.sendMapElement(&me, sendChannel);
          predictionMap.sendMapElement(&me, -2);
        }
      }
      else if (dist2currentAlice < DARPABALL_COLLISION_THRESHOLD)
        logPrediction->getStream(1)<<"I would like to play DARPA Ball. But... AliceVel ="<<aliceVelocity<<" or oppositeDirection = "<<oppositeDirection<<endl;


      // only if Alice's trajectory is valid at this index
      if (j<alicePrediction.traj.size() && !gotCollision) {
        p_aliceTraj = alicePrediction.traj[j].traj_space;
        // compute distance between both points of trajectories
        distTraj = sqrt( pow(p_aliceTraj.x-p_obstacleTraj.x,2) + pow(p_aliceTraj.y-p_obstacleTraj.y,2) );

        
        // if dist is smaller than threshold, assume collision and increse collision counter
        // at the same time, don't send any collisions that are closer than 1.5m This might distract Alice (wheet, etc.)
        if (distTraj<SEPERATION_THRESHOLD && dist2currentAlice>1.5) {
          bool reject = false;
          // first check, whether obstacle is approaching from behind; this should not result in a predicted collision
          LaneLabel currentLaneAlice, currentLaneObstacle;
          p_map->getLane(currentLaneAlice, p_aliceTraj);
          p_map->getLane(currentLaneObstacle, p_obstacleTraj);
          
          // if obstacle is in the same lane as Alice, but behind Alice, reject this collision
          if (p_map->isPointInLane(p_obstacleTraj, currentLaneObstacle) && p_aliceTraj == p_obstacleTraj) {
            point2arr centerline;
            p_map->getLaneCenterLine(centerline, currentLaneAlice);
            double dist;
            p_map->getDistAlongLine(dist, centerline, p_aliceTraj, p_obstacleTraj);
            if (dist<0.0)
              reject = true;
          }

          if (!reject) {
            PredictionList[i].collisionCounter++;
            PredictionList[i].collisionPoint = p_obstacleTraj;
            PredictionList[i].collisionTime = PredictionList[i].traj[j].traj_time;
            possibleCollision=true;
          }

          // only count collision once in case the trajectories are so close
          // this will also automatically return the closest collision as the vector is time discretisated
          gotCollision = true;
        }
      }

      logPrediction->getStream(1)<<"minDarpaball = "<<minDarpaBall<<endl;
    }

    // if no collision was detected, reset counter
    if (!possibleCollision)
      PredictionList[i].collisionCounter=0;
  }


  // DARPA Ball
  if (darpaBallOccured)
    darpaBallCounter++;
  else
    darpaBallCounter=0;
  
  // return the number of obstacles that are stored in the vehicle list. This is the number of obstacles that are under prediction
  int index=-1;
  
  unsigned long long t = -1;
  // If there might be several collisions, find and return the closest one
  for (unsigned int i=0; i<PredictionList.size(); i++)
    if ((PredictionList[i].collisionCounter>=COLLISION_THRESHOLD) && (PredictionList[i].collisionTime <= t)) {
      index=i;
      t = PredictionList[i].collisionTime;
    }
  
  // if at least one very possible collision was found, return its coordinates and time
  MapElement me;
  string predStatus;
  if (index != -1) {
    me.setId(MODtrafficplanner,400);
    me.setTypePredictedVehicle();
    me.setColor(MAP_COLOR_RED);
    me.setFrameTypeSite();
    me.setGeometry(PredictionList[index].collisionPoint, PREDICTION_SIZE);
    me.setPosition(PredictionList[index].collisionPoint);
    
    predictionMap.sendMapElement(&me, 0);
    predictionMap.sendMapElement(&me, -2);
    
    sentCollisionToMap = true;
    
    logPrediction->getStream(1)<<"Collision: send collisionPoint "<<PredictionList[index].collisionPoint<<endl;
    clearCounter = 0;
  }
  else if (index == -1 && sentCollisionToMap && clearCounter<COLLISION_THRESHOLD)
    clearCounter++;
  else if (index == -1 && sentCollisionToMap && clearCounter>=COLLISION_THRESHOLD) {
    // clear predicted collision point out of map
    me.setId(MODtrafficplanner,400);
    me.setFrameTypeSite();
    me.setTypeClear();
    predictionMap.sendMapElement(&me, 0);
    predictionMap.sendMapElement(&me, -2);
    
    sentCollisionToMap = false;
    
    clearCounter = 0;
  }
  else
    predStatus="CLEAR    ";
}


void CPrediction::checkLaneChange()
{
  MapElement me;

  for (unsigned int i=0; i<PredictionList.size(); i++) {
    // check for size in mergineSpace structure. It might be empty if other vehicle is slower than Alice
    if (laneChange && PredictionList[i].mergingSpace.size()>0) {
      PredictionList[i].laneChangeSentToMap = true;
        
      // create polygon
      point2arr poly, temp;
      poly = PredictionList[i].mergingSpace.get_offset(1);
      temp = PredictionList[i].mergingSpace.get_offset(-1);
      temp.reverse();
      poly.connect(temp);
      
      // send polygon to map
      me.setId(MODtrafficplanner, PredictionList[i].mapid_laneChange);
      me.setTypePoly();
      me.setFrameTypeSite();
      me.setGeometry(poly);
      me.setColor(MAP_COLOR_RED);
      me.setTypePredictedVehicle();
      predictionMap.sendMapElement(&me, 0);
      predictionMap.sendMapElement(&me, -2);
      if (vlevel>2)
        predictionMap.sendMapElement(&me, sendChannel);
    }
    // clean up map
    else {
      // only clear it when it was sent before
      if (PredictionList[i].laneChangeSentToMap) {
        PredictionList[i].laneChangeSentToMap = false;
        
        // send clear to map
        me.setId(MODtrafficplanner, PredictionList[i].mapid_laneChange);
        me.setTypeClear();
        predictionMap.sendMapElement(&me, 0);
        predictionMap.sendMapElement(&me, -2);
        if (vlevel>2)
          predictionMap.sendMapElement(&me, sendChannel);
      }
    }
  }
}


void CPrediction::clearMap()
{
  MapElement me;

  if (sentCollisionToMap) {
    me.setId(MODtrafficplanner, 400);
    me.setTypeClear();
    predictionMap.sendMapElement(&me, 0);
    predictionMap.sendMapElement(&me, -2);

    sentCollisionToMap = false;
  }

  for (unsigned int i=0; i<PredictionList.size(); i++) {
    if (PredictionList[i].laneChangeSentToMap) {
      PredictionList[i].laneChangeSentToMap = false;
      
      // send clear to map
      me.setId(MODtrafficplanner, PredictionList[i].mapid_laneChange);
      me.setTypeClear();
      predictionMap.sendMapElement(&me, 0);
      predictionMap.sendMapElement(&me, -2);
      if (vlevel>2)
        predictionMap.sendMapElement(&me, sendChannel);
    }
  }
}

void CPrediction::loop(CPrediction *self)
{
  while(true) {
    pthread_testcancel();
    if (self->loopActive) {
      self->updateList();
      self->predictCollision();
      self->checkLaneChange();
      usleep(100000);
    }
  }
}

void CPrediction::startLoop()
{
  if (!loopActive) {

    // Start the thread
    if (pthread_create(&this->thread, NULL, (void*(*)(void*)) loop, this) == 0)
      loopActive = true;
    Log::getStream(1)<<"Prediction started"<<endl;
    logPrediction->getStream(1)<<"Prediction started"<<endl;
  }
}

void CPrediction::stopLoop()
{
  if (loopActive) {

    // Stop the thread
    pthread_cancel(this->thread);
    pthread_join(this->thread, NULL);

    // Clean up
    clearMap();
    loopActive = false;
    Log::getStream(1)<<"Prediction stopped"<<endl;
    logPrediction->getStream(1)<<"Prediction stopped"<<endl;
  }
}

bool CPrediction::isRunning()
{
  return loopActive;
}
