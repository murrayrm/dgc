// TODO
// - connect to Alice's seggoals
// - predict obstaclces out of lane
// - check collision


#include "Prediction.hh"
#include "CmdArgs.hh"
#include <gcinterfaces/SegGoals.hh>

using namespace std;

classPrediction::classPrediction(int skynetKey, bool bWaitForStateFill,
                                 int debugLevel, int verboseLevel, 
                                 bool log)
  : CSkynetContainer(MODtrafficplanner, skynetKey)
  , CStateClient(bWaitForStateFill)
{
   localMap = new Map();
   localMap->loadRNDF(CmdArgs::RNDF_file);

   DGCcreateMutex(&mapMutex);

   UpdateState();
   point2 statedelta(m_state.utmNorthing-m_state.localX, m_state.utmEasting-m_state.localY);
   localMap->prior.delta = statedelta;
   DGClockMutex(&mapMutex);
   localMap->prior.delta = statedelta;
   DGCunlockMutex(&mapMutex);

 
   initRecvMapElement(CmdArgs::sn_key,-2);
   initSendMapElement(CmdArgs::sn_key);

   cout<<"Skynet key "<<CmdArgs::sn_key<<endl;

   MapElement el;
   for (unsigned int k=0; k<localMap->prior.data.size(); k++)
     {
       localMap->prior.getEl(el,k);
       sendMapElement(&el,sendChannel);
     }

   mapcounter=5000;
}


classPrediction::~classPrediction()
{
  delete localMap;
  DGCdeleteMutex(&mapMutex);
}

void classPrediction::populateWayPoints(Map* localMap, PointLabel InitialWayPointEntry, vector<PointLabel> &WP_vector)
{
  vector<PointLabel> WayPointExits, WayPoint;

  // Obtain all WayPointExits for the initial WayPointEntry
  localMap->getWayPointExits(WayPointExits,InitialWayPointEntry);

  for (unsigned int i=0; i<WayPointExits.size(); i++)
    {
      localMap->getWayPointEntries(WayPoint,WayPointExits[i]);

      // Look whether WayPointEntry already exists in list
      for (unsigned int j=0; j<WayPoint.size(); j++)
        {
          bool found=false;
          for (unsigned int k=0; k<WP_vector.size(); k++)
            {
              if (WayPoint[j]==WP_vector[k])
                {
                  found=true;
                  break;
                }
            }

          // If not, add it to list and call function recursivly
          if (!found)
            {
              WP_vector.push_back(WayPoint[j]);
              populateWayPoints(localMap,WayPoint[j],WP_vector);
            }
        }
    }
}

bool classPrediction::withinIntersection(MapElement me)
{
  point2 p;
  LaneLabel lane;
  p.set(me.center);
  

  bool inLane=false;

  if (localMap->getLane(lane,p) != -1)
    if (localMap->isPointInLane(p,lane))
        inLane=true;

  if (inLane)
    {
      point2arr centerline;
      point2 startpoint;

      // get centerline from lane where obstalce is
      localMap->getLaneCenterLine(centerline,lane);

      // the predicted trajectory should stop at stoplines
      vector<PointLabel> StopLines;

      point2 p_stopline;
      double distance_temp;
      if (localMap->getLaneStopLines(StopLines,lane)!=0)
        {
          double distance_stopline=INFINITY;
          
          // Find closest stopline in Alice's direction of travel and save PointLabel and distance
          int index=-1;
          for (unsigned int i=0; i<StopLines.size(); i++)
            {
              localMap->getWaypoint(p_stopline,StopLines[i]);
              localMap->getDistAlongLine(distance_temp,centerline,p_stopline,p);
              
              if (distance_temp>-20 && distance_temp<distance_stopline)
                {
                  index=i;
                  distance_stopline=distance_temp;
                }
            }
          

          // if stopline was found
          if (index != -1)
            {
              vector<PointLabel> WayPointsEntries,WayPointsWithStop,WayPointsNoStop,WayPointExits,WayPoint;
              point2arr leftBound,rightBound;
              vector<MapElement> obstacles;

              // populate Waypoints
              WayPointsEntries.clear();
              populateWayPoints(localMap, StopLines[index], WayPointsEntries);
              
              // Check all WayPoint, get their Exit and check whether Entry and Exit are in the same lane
               for (unsigned i=0; i<WayPointsEntries.size(); i++)
                 {
                   // get all exits for this entry
                   localMap->getWayPointExits(WayPointExits,WayPointsEntries[i]);
                   for (unsigned j=0;j<WayPointExits.size(); j++)
                     {
                       // Create TransitionBound between this WayPointEntry and WayPointExit
                       localMap->getShortTransitionBounds(leftBound, rightBound, WayPointsEntries[i], WayPointExits[j]);
          
                       // Get all obstacles within these bounds
                       DGClockMutex(&mapMutex);
                       localMap->getObsInBounds(obstacles, leftBound, rightBound);
                       DGCunlockMutex(&mapMutex);
                      
                       // look for obstacle in the list
                       for (unsigned k=0; k<obstacles.size(); k++)
                           if (obstacles[k].id==me.id)
                             return true;
                     }
                 }
            }
        }
    }
  else
    return false;

  return false;
}

void classPrediction::updateObstacles()
{
  MapElement me;

  // reset updated flag
  for (unsigned i =0; i< localMap->data.size(); ++i)
    {
      DGClockMutex(&mapMutex);
      me=localMap->data[i];
      DGCunlockMutex(&mapMutex);

      // only allow VEHICLES
      if (me.type==ELEMENT_VEHICLE)
        {
          bool found=false;

          // search for this obstacle in the list
          for (unsigned int j=0; j<vehList.size(); j++)
              if (vehList[j].me.id==me.id)
                {
                  // Create trajectory

                   if (!withinIntersection(me))
                     createTrajectory(me,vehList[j].traj,vehList[j].traj_time,TRUE);
                   else
                     {
                       vehList[j].traj.clear();
                       vehList[j].traj_time.clear();
                     }

                  vehList[j].me=me;
                  
                  // set flag so that list will be updated later
                  vehList[j].updated=true;
                  found=true;
                  break;
                }
  
          // if me was not found in vehicle list, add it
          if (!found)
            {
              VehicleList vehListTemp;
                createTrajectory(me,vehListTemp.traj,vehListTemp.traj_time,TRUE);
               if (!withinIntersection(me))
                 createTrajectory(me,vehListTemp.traj,vehListTemp.traj_time,TRUE);
               else
                 {
                   vehListTemp.traj.clear();
                   vehListTemp.traj_time.clear();
                 }

              vehListTemp.me=me;
              vehListTemp.traj_id=++mapcounter;
              vehListTemp.updated=true;
              vehListTemp.collisionCounter=0;
              // save new element to list
              vehList.push_back(vehListTemp);
            }
        }
    }

   // clean up vehicle list & output to map
   vector<VehicleList>::iterator v_it;
   for (v_it=vehList.begin(); v_it!=vehList.end(); v_it++)
     {
       VehicleList vl=*v_it;
       if (vl.updated)
         {
           // update flag and save it
           vl.updated=false;
           *v_it=vl;

           // send obstacle to map
           me=vl.me;
           me.setColor(MAP_COLOR_YELLOW,100);
           sendMapElement(&me,sendChannel);
          
           // send traj to map
           me.id=vl.traj_id;
           me.setTypeTravPath();
           me.setColor(MAP_COLOR_RED,100);
           me.setGeometry(vl.traj);
           sendMapElement(&me,sendChannel);
         }
        else
          {
            // clear obstacle from map
            me=vl.me;
            me.setTypeClear();
            sendMapElement(&me,sendChannel);

            // clear traj from map
            me.id=vl.traj_id;
            me.setTypeClear();
            sendMapElement(&me,sendChannel);
            vehList.erase(v_it);
            v_it--;
          }
     }
}

void classPrediction::createTrajectory(MapElement &me, point2arr &traj, vector<uint64_t> &traj_time,bool mapflag)
{
  //  double orientation=me.orientation;
  //  double orientationVar=me.orientationVar;
  point2_uncertain pos=me.center;
  point2_uncertain vel=me.velocity;
  point2 p,point;
  p.set(pos);
  double angle,dist,d,orientation,t;
  uint64_t starttime;

  LaneLabel lane;

  // return the distance that is predicted given the current speed
  double velAbs=sqrt(pow(vel.x,2) + pow(vel.y,2));
  double dist_prediction=velAbs * PREDICTION_HORIZON;
  double time_prediction=PREDICTION_HORIZON;

  // avoid bug in mapper
  if (mapflag)
    vel.y=-vel.y;


  // hack for obstacle's orientation
  orientation=acos(fabs(vel.x)/velAbs);

  if (vel.x>=0 && vel.y<0)
    orientation=-orientation;
  else if (vel.x<0 && vel.y<0)
    orientation=-M_PI + orientation;
  else if (vel.x<0 && vel.y>=0)
    orientation=M_PI - orientation;

  // clear trajectory
  traj.clear();
  traj_time.clear();
  starttime=getTime();

  stringstream s;

  bool inLane=false;

  if (localMap->getLane(lane,p) != -1)
      if (localMap->isPointInLane(p,lane))
        inLane=true;

  // if obstacle is in lane
  if (inLane)
    {
      point2arr centerline;
      point2 startpoint;
      
      // get centerline from lane where obstalce is
      localMap->getLaneCenterLine(centerline,lane);
      
      // this function also returns in the second argument the projection of p into the lane
      localMap->getHeading(angle,startpoint,lane,p);
      
      // the predicted trajectory should stop at stoplines
      vector<PointLabel> StopLines;
      
      point2 p_stopline;
      double distance_temp;
      
      if (localMap->getLaneStopLines(StopLines,lane)!=0)
        {
          double distance_stopline=INFINITY;
              
          // Find closest stopline in Alice's direction of travel and save PointLabel and distance
          for (unsigned int i=0; i<StopLines.size(); i++)
            {
              localMap->getWaypoint(p_stopline,StopLines[i]);
              localMap->getDistAlongLine(distance_temp,centerline,p_stopline,p);
              
              if (distance_temp>0 && distance_temp<distance_stopline)
                distance_stopline=distance_temp;
            }
          
          // if stopline is closer than the predicted distance, then shorten the predicted distance and predicated time
          if (distance_stopline<INFINITY && distance_stopline<dist_prediction)
            {
              dist_prediction=distance_stopline;
              time_prediction=distance_stopline/velAbs;
            }
        }
      
      // store startpoint
      traj.push_back(startpoint);
      traj_time.push_back(starttime); 
      
      // this function call is needed for later. it computes the distance from the beginning of the centerline to the startpoint
      localMap->getDistAlongLine(dist,centerline,startpoint);
      
      // estimate the direction of travel within the lane. It might be possible that vehicle is travelling in wrong lane
      // calculate the difference of the angle lane and the heading of obstacle
      // if the absolute difference is larger than pi, both headings are opposite to each other
      double heading_delta;
      
      addAngles(heading_delta,-angle,orientation);
      
      // send debug information as MapElement.Label
      s<<"Lane "<<angle<<", ";
      s<<"Head "<<orientation<<", ";
      s<<"Delta "<<heading_delta<<", ";
      me.label.push_back(s.str());
      
      t=0;
      d=0;
      // loop along the centerline and create trajectory
      while (t<time_prediction)
        {
          t+=PREDICTION_RESOLUTION; // time step in [s]
          d=velAbs*t;
          traj_time.push_back(starttime+ uint64_t(t*1000000) );
          
          // depending on obstacle's heading regarding the lane's heading, calculate the distance
          // if correct direction of travel ...
          if ((heading_delta <= M_PI/4) && (heading_delta >= -M_PI/40))
            localMap->getPointAlongLine(point,centerline,dist+d);
          // if in opposite direction of travel...
          else if ((heading_delta >= 3*M_PI/4) || (heading_delta <= -3*M_PI/4))
            localMap->getPointAlongLine(point,centerline,dist-d);
          // else...diagonal to lane ...
          else
            {
              point.x = p.x + cos(orientation) * d;
              point.y = p.y + sin(orientation) * d;
            }
          
          traj.push_back(point);
        }
    }
  // if obstacle is out of lane, compute predicated trajectory along the obstacle's orientation
  else
    {
      // store startpoint
      traj.push_back(p);

      t=0;
      d=0;
      while (t<time_prediction)
        {
          t+=PREDICTION_RESOLUTION; // time step in [s]
          d=velAbs*t;
          traj_time.push_back(starttime+ uint64_t(t*1000000) );

          point.x = p.x + cos(orientation) * d;
          point.y = p.y + sin(orientation) * d;
          traj.push_back(point);
        }
    }


}

void classPrediction::updateAlice()
{
  MapElement alice;
  alice.set_alice(m_state);
  sendMapElement(&alice,sendChannel);

  // spoof information as MapElement doesn't contain correct information
  alice.velocity.x=vehState.utmNorthVel;
  alice.velocity.y=vehState.utmEastVel;
  alice.orientation=vehState.localYaw;
  //  alice.center=point2(vehState.utmNorthing - vehState.localX,vehState.utmEasting - vehState.localY);

  alice.center=point2(vehState.localX,vehState.localY);

  // compute traj. This will be replaced later by the graph
  createTrajectory(alice,alice_traj,alice_traj_time,FALSE);

  MapElement me;
  me.id=999;
  me.setTypeTravPath();
  me.setColor(MAP_COLOR_GREEN,100);
  me.setGeometry(alice_traj);

  sendMapElement(&me,sendChannel);
}


void classPrediction::updateMapThread()
{
   MapElement recvEl;
   int bytesRecv=0;

   while(true)
     {
        bytesRecv = recvMapElementBlock(&recvEl,-2);
   
         if (bytesRecv>0)
           {
             DGClockMutex(&mapMutex);
             localMap->addEl(recvEl);
             DGCunlockMutex(&mapMutex);
           }
         else
             usleep(100);
     }

}

int classPrediction::addAngles(double &angle_out, double angle_in1, double angle_in2) {
  if ((angle_in1+angle_in2)>M_PI) 
    angle_out=angle_in1+angle_in2-2*M_PI;
  else if ((angle_in1+angle_in2)<=-M_PI)
    angle_out=angle_in1+angle_in2+2*M_PI;
  else
    angle_out=angle_in1+angle_in2;
  
  return 0;
}

uint64_t classPrediction::getTime()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
}

bool classPrediction::predictCollision(point2 &collisionPoint)
{
  point2 p_alice,p_obstacle;
  double dist,angle;
  unsigned int upperBound;
  bool possibleCollision=false;
  bool collisionThreshold=false;

  LaneLabel lane;

  //  bool collisionflag=false;
  for (unsigned int i=0; i<vehList.size(); i++)
    {
      // as Alice and the obstacle might have different prediction horizone, adjust upperBound
      if (alice_traj.size()<=vehList[i].traj.size())
        upperBound=alice_traj.size();
      else
        upperBound=vehList[i].traj.size();

      possibleCollision=false;
      for (unsigned int j=0; j<upperBound; j++)
        {
          p_alice = alice_traj[j];
          p_obstacle = vehList[i].traj[j];
          
          // compute distance between both points of trajectories
          dist = sqrt( pow(p_alice.x-p_obstacle.x,2) + pow(p_alice.y-p_obstacle.y,2) );
          
          // if dist is smaller than threshold, assume collision and increse collision counter
          if (dist<SEPERATION_THRESHOLD)
            {
              vehList[i].collisionCounter++;
              possibleCollision=true;

              if (vehList[i].collisionCounter>=COLLISION_THRESHOLD)
                {
                  collisionThreshold=true;

                  // compute collision point; not very exact, but should satisfy our needs
                  localMap->getLane(lane,p_alice);
                  localMap->getHeading(angle,collisionPoint,lane,p_obstacle);
                }
              
              // only count collision once in case the trajectories are so close
              // this will also automatically return the closest collision as the vector is time discretisated
              break;
            }
        }
      if (!possibleCollision)
        vehList[i].collisionCounter=0;
      
      if (vehList[i].collisionCounter>0)
        cout<<"Collision counter... "<<vehList[i].collisionCounter<<endl;
    }

  if (collisionThreshold)
    {
      MapElement me;
      me.id=9999;
      me.setTypePoly();
      me.setColor(MAP_COLOR_RED,100);
      me.setGeometry(collisionPoint,3);
      sendMapElement(&me,sendChannel);
    }

  return collisionThreshold;
}


void classPrediction::PredictionLoop(void)
{
  int counter=0;
  point2 collisionPoint;
  while (true)
    {
       UpdateState();
       vehState=m_state;
       updateAlice();
       updateObstacles();
       counter+=predictCollision(collisionPoint);
    }
}
