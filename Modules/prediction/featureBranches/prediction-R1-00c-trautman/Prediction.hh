#ifndef PREDICTION_HH_
#define PREDICTION_HH_
#include "map/MapElementTalker.hh"
#include "map/Map.hh"
// stf package includes
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>

// skynet, talker, and interfaces
#include "skynet/sn_msg.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/SkynetTalker.hh"
#include "map/MapElementTalker.hh"
#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"

// Other modules/def's
#include "map/Map.hh"
#include <cotk/cotk.h>
#include <ncurses.h>

class PredictionObstacle
{
public:
  MapElement me;
  point2arr traj;
  vector<uint64_t> traj_time;
  int traj_id;
  bool updated;

  int collisionCounter;
  point2 collisionPoint;
  uint64_t collisionTime;
};

class Prediction
{

public: 
  
  enum predictionReturn
    { 
      CLEAR,
      COLLISION
    };

  static int init(Map* localMap);
  static void destroy();

  /// @brief updates the Vehicle List with all found obstacles and their predicted trajectories
  static void updateList(Map* localMap,  VehicleState vehState);

  // @brief Returns predicted collisions
  static predictionReturn predictCollision(PredictionObstacle &pObstacle, int &numberObstacles);

private :

  // @brief Updates Alice and its trajectory
  static void updateAlice(Map* localMap, VehicleState vehState);

  // @brief Computes trajectories for Alice or any other obstacle
  static void createTrajectory(Map* localMap, MapElement &me, point2arr &traj, vector<uint64_t> &traj_time,bool mapflag);

  // @brief Utility to add two angles
  static int addAngles(double &angle_out, double angle_in1, double angle_in2);

  // @brief Gets time
  static uint64_t getTime();

  // @brief Checks whether obstacle is within intersections
  static bool withinIntersection(Map* localMap, MapElement me);

  static int mapcounter;
  static vector<PredictionObstacle> PredictionList;
  static point2arr alice_traj;
  static vector<uint64_t> alice_traj_time;
  static CMapElementTalker debugMap;

  // time in seconds that algorithm predicts
  static const double PREDICTION_HORIZON = 10.0;

  // frequency of prediction in [Hz]
  static const double PREDICTION_RESOLUTION=0.1;

  // seperation distance threshold between Alice and any obstacle that indicates a serious collision
  // it should be dependant on the maxium velocity and the prediction resolution
  static const double SEPERATION_THRESHOLD=15 * 0.1;

  // how often does a collision need to occur that it will be handled as a serious collision threat
  static const double COLLISION_THRESHOLD=10;

  // map debug channel
  static const int sendChannel=-10;
};

#endif /*PREDICTION_HH_*/
