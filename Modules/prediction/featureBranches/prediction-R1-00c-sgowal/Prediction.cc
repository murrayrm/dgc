// TODO
// - connect to Alice's seggoals
// - predict obstaclces out of lane
// - check collision


#include "Prediction.hh"
#include "CmdArgs.hh"
#include <gcinterfaces/SegGoals.hh>
#include "logic-planner/IntersectionHandling.hh"
#include "temp-planner-interfaces/AliceStateHelper.hh"
#include <limits.h>
#include <temp-planner-interfaces/Log.hh>
#include <temp-planner-interfaces/Console.hh>

using namespace std;

int Prediction::mapcounter;
vector<PredictionObstacle> Prediction::PredictionList;
point2arr Prediction::alice_traj;
vector<uint64_t> Prediction::alice_traj_time;
CMapElementTalker Prediction::debugMap;


int Prediction::init(Map* localMap)
{
  // Initialize the debug map
   debugMap.initSendMapElement(CmdArgs::sn_key);

   MapElement el;
   for (unsigned int k=0; k<localMap->prior.data.size(); k++)
     {
       localMap->prior.getEl(el,k);
       debugMap.sendMapElement(&el,sendChannel);
     }

   mapcounter=5000;
   return 0;
}


void Prediction::destroy()
{
  return;
}

void Prediction::updateAlice(Map* localMap, VehicleState vehState)
{
  MapElement alice;
  alice.set_alice(vehState);
  debugMap.sendMapElement(&alice,sendChannel);

  // spoof information as MapElement doesn't contain correct information
  alice.velocity.x=vehState.utmNorthVel;
  alice.velocity.y=vehState.utmEastVel;
  alice.orientation=vehState.localYaw;
  //  alice.center=point2(vehState.utmNorthing - vehState.localX,vehState.utmEasting - vehState.localY);

  alice.center=point2(vehState.localX,vehState.localY);

  // compute traj. This will be replaced later by the graph
  createTrajectory(localMap,alice,alice_traj,alice_traj_time,FALSE);

  MapElement me;
  me.id=999;
  me.setTypeTravPath();
  me.setColor(MAP_COLOR_GREEN,100);
  me.setGeometry(alice_traj);

  debugMap.sendMapElement(&me,sendChannel);
}


bool Prediction::withinIntersection(Map* localMap, MapElement me)
{
  point2 p;
  LaneLabel lane;
  p.set(me.center);
  

  bool inLane=false;

  if (localMap->getLane(lane,p) != -1)
    if (localMap->isPointInLane(p,lane))
        inLane=true;

  if (inLane)
    {
      point2arr centerline;
      point2 startpoint;

      // get centerline from lane where obstalce is
      localMap->getLaneCenterLine(centerline,lane);

      // the predicted trajectory should stop at stoplines
      vector<PointLabel> StopLines;

      point2 p_stopline;
      double distance_temp;
      if (localMap->getLaneStopLines(StopLines,lane)!=0)
        {
          double distance_stopline=INFINITY;
          
          // Find closest stopline in Alice's direction of travel and save PointLabel and distance
          int index=-1;
          for (unsigned int i=0; i<StopLines.size(); i++)
            {
              localMap->getWaypoint(p_stopline,StopLines[i]);
              localMap->getDistAlongLine(distance_temp,centerline,p_stopline,p);
              
              if (distance_temp>-20 && distance_temp<distance_stopline)
                {
                  index=i;
                  distance_stopline=distance_temp;
                }
            }
          
          // if stopline was found
          if (index != -1)
            {
              vector<PointLabel> WayPointsEntries,WayPointsWithStop,WayPointsNoStop,WayPointExits,WayPoint;
              point2arr leftBound,rightBound;
              vector<MapElement> obstacles;

              // populate Waypoints
              WayPointsEntries.clear();
              IntersectionHandling::populateWayPoints(localMap, StopLines[index], WayPointsEntries, FALSE);
              
              // Check all WayPoint, get their Exit and check whether Entry and Exit are in the same lane
               for (unsigned i=0; i<WayPointsEntries.size(); i++)
                 {
                   // get all exits for this entry
                   localMap->getWayPointExits(WayPointExits,WayPointsEntries[i]);
                   for (unsigned j=0;j<WayPointExits.size(); j++)
                     {
                       // Create TransitionBound between this WayPointEntry and WayPointExit
                       localMap->getShortTransitionBounds(leftBound, rightBound, WayPointsEntries[i], WayPointExits[j]);
          
                       // Get all obstacles within these bounds
                       localMap->getObsInBounds(obstacles, leftBound, rightBound);
                      
                       // look for obstacle in the list
                       for (unsigned k=0; k<obstacles.size(); k++)
                           if (obstacles[k].id==me.id)
                             return true;
                     }
                 }
            }
        }
    }
  else
    return false;

  return false;
}

void Prediction::updateList(Map* localMap, VehicleState vehState)
{
  // update Alice
  updateAlice(localMap, vehState);

  // receive all obstacles within 500m which should get practically all obstacles as ladars are limited
  vector<MapElement> obstacles;
  point2 position_alice = AliceStateHelper::getPositionFrontBumper(vehState);
  localMap->getObsNearby(obstacles,position_alice,500);

  for (unsigned i =0; i< obstacles.size(); i++)
    {
      // only allow VEHICLES
      if (obstacles[i].type==ELEMENT_VEHICLE)
        {
          bool found=false;

          // search for this obstacle in the list
          for (unsigned int j=0; j<PredictionList.size(); j++)
              if (PredictionList[j].me.id==obstacles[i].id)
                {
                  // Create trajectory
                  if (!withinIntersection(localMap,obstacles[i]))
                     createTrajectory(localMap, obstacles[i],PredictionList[j].traj,PredictionList[j].traj_time,TRUE);
                  else
                    {
                      PredictionList[j].traj.clear();
                      PredictionList[j].traj_time.clear();
                     }
                  
                  PredictionList[j].me=obstacles[i];
                  
                  // set flag so that list will be updated later
                  PredictionList[j].updated=true;
                  found=true;
                  break;
                }
  
          // if me was not found in vehicle list, add it
          if (!found)
            {
              PredictionObstacle predListTemp;

              if (!withinIntersection(localMap,obstacles[i]))
                createTrajectory(localMap, obstacles[i], predListTemp.traj, predListTemp.traj_time, TRUE);
              else
                {
                  predListTemp.traj.clear();
                  predListTemp.traj_time.clear();
                }

              predListTemp.me=obstacles[i];
              predListTemp.traj_id=++mapcounter;
              predListTemp.updated=true;
              predListTemp.collisionCounter=0;
              // save new element to list
              PredictionList.push_back(predListTemp);
            }
        }
    }

  MapElement me;
   // clean up vehicle list & output to map
   vector<PredictionObstacle>::iterator v_it;
   for (v_it=PredictionList.begin(); v_it!=PredictionList.end(); v_it++)
     {
       PredictionObstacle vl=*v_it;

       if (vl.updated)
         {
           // update flag and save it
           vl.updated=false;
           *v_it=vl;

           // send obstacle to map
           me=vl.me;
           me.setColor(MAP_COLOR_YELLOW,100);
           debugMap.sendMapElement(&me,sendChannel);
          
           // send traj to map
           me.id=vl.traj_id;
           me.setTypeTravPath();
           me.setColor(MAP_COLOR_RED,100);
           me.setGeometry(vl.traj);
           debugMap.sendMapElement(&me,sendChannel);
         }
        else
          {
            // clear obstacle from map
            me=vl.me;
            me.setTypeClear();
            debugMap.sendMapElement(&me,sendChannel);

            // clear traj from map
            me.id=vl.traj_id;
            me.setTypeClear();
            debugMap.sendMapElement(&me,sendChannel);
            PredictionList.erase(v_it);
            v_it--;
          }
     }
}

void Prediction::createTrajectory(Map* localMap, MapElement &me, point2arr &traj, vector<uint64_t> &traj_time,bool mapflag)
{
  //  double orientation=me.orientation;
  //  double orientationVar=me.orientationVar;
  point2_uncertain pos=me.center;
  point2_uncertain vel=me.velocity;
  point2 p,point;
  p.set(pos);
  double angle,dist,d,orientation,t;
  uint64_t starttime;

  LaneLabel lane;

  // return the distance that is predicted given the current speed
  double velAbs=sqrt(pow(vel.x,2) + pow(vel.y,2));
  double dist_prediction=velAbs * PREDICTION_HORIZON;
  double time_prediction=PREDICTION_HORIZON;

  // avoid bug in mapper
  if (mapflag)
    vel.y=-vel.y;


  // hack for obstacle's orientation
  orientation=acos(fabs(vel.x)/velAbs);

  if (vel.x>=0 && vel.y<0)
    orientation=-orientation;
  else if (vel.x<0 && vel.y<0)
    orientation=-M_PI + orientation;
  else if (vel.x<0 && vel.y>=0)
    orientation=M_PI - orientation;

  // clear trajectory
  traj.clear();
  traj_time.clear();
  starttime=getTime();

  stringstream s;

  bool inLane=false;

  if (localMap->getLane(lane,p) != -1)
      if (localMap->isPointInLane(p,lane))
        inLane=true;

  // if obstacle is in lane
  if (inLane)
    {
      point2arr centerline;
      point2 startpoint;
      
      // get centerline from lane where obstalce is
      localMap->getLaneCenterLine(centerline,lane);
      
      // this function also returns in the second argument the projection of p into the lane
      localMap->getHeading(angle,startpoint,lane,p);
      
      // the predicted trajectory should stop at stoplines
      vector<PointLabel> StopLines;
      
      point2 p_stopline;
      double distance_temp;
      
      if (localMap->getLaneStopLines(StopLines,lane)!=0)
        {
          double distance_stopline=INFINITY;
              
          // Find closest stopline in Alice's direction of travel and save PointLabel and distance
          for (unsigned int i=0; i<StopLines.size(); i++)
            {
              localMap->getWaypoint(p_stopline,StopLines[i]);
              localMap->getDistAlongLine(distance_temp,centerline,p_stopline,p);
              
              if (distance_temp>0 && distance_temp<distance_stopline)
                distance_stopline=distance_temp;
            }
          
          // if stopline is closer than the predicted distance, then shorten the predicted distance and predicated time
          if (distance_stopline<INFINITY && distance_stopline<dist_prediction)
            {
              dist_prediction=distance_stopline;
              time_prediction=distance_stopline/velAbs;
            }
        }
      
      // store startpoint
      traj.push_back(startpoint);
      traj_time.push_back(starttime); 
      
      // this function call is needed for later. it computes the distance from the beginning of the centerline to the startpoint
      localMap->getDistAlongLine(dist,centerline,startpoint);
      
      // estimate the direction of travel within the lane. It might be possible that vehicle is travelling in wrong lane
      // calculate the difference of the angle lane and the heading of obstacle
      // if the absolute difference is larger than pi, both headings are opposite to each other
      double heading_delta;
      
      addAngles(heading_delta,-angle,orientation);
      
      // send debug information as MapElement.Label
      s<<"Lane "<<angle<<", ";
      s<<"Head "<<orientation<<", ";
      s<<"Delta "<<heading_delta<<", ";
      me.label.push_back(s.str());
      
      t=0;
      d=0;
      // loop along the centerline and create trajectory

      while (t<time_prediction)
        {
          t+=PREDICTION_RESOLUTION; // time step in [s]
          d=velAbs*t;
          traj_time.push_back(starttime+ uint64_t(t*1000000) );
          
          // depending on obstacle's heading regarding the lane's heading, calculate the distance
          // if correct direction of travel ...
          if ((heading_delta <= M_PI/4) && (heading_delta >= -M_PI/40))
            localMap->getPointAlongLine(point,centerline,dist+d);
          // if in opposite direction of travel...
          else if ((heading_delta >= 3*M_PI/4) || (heading_delta <= -3*M_PI/4))
            localMap->getPointAlongLine(point,centerline,dist-d);
          // else...diagonal to lane ...
          else
            {
              point.x = p.x + cos(orientation) * d;
              point.y = p.y + sin(orientation) * d;
            }
          
          traj.push_back(point);
        }
    }
  // if obstacle is out of lane, compute predicated trajectory along the obstacle's orientation
  else
    {
      // store startpoint
      traj.push_back(p);

      t=0;
      d=0;
      while (t<time_prediction)
        {
          t+=PREDICTION_RESOLUTION; // time step in [s]
          d=velAbs*t;
          traj_time.push_back(starttime+ uint64_t(t*1000000) );

          point.x = p.x + cos(orientation) * d;
          point.y = p.y + sin(orientation) * d;
          traj.push_back(point);
        }
    }
}

int Prediction::addAngles(double &angle_out, double angle_in1, double angle_in2) {
  if ((angle_in1+angle_in2)>M_PI) 
    angle_out=angle_in1+angle_in2-2*M_PI;
  else if ((angle_in1+angle_in2)<=-M_PI)
    angle_out=angle_in1+angle_in2+2*M_PI;
  else
    angle_out=angle_in1+angle_in2;
  
  return 0;
}

uint64_t Prediction::getTime()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
}

Prediction::predictionReturn Prediction::predictCollision(PredictionObstacle &pObstacle, int &numberObstacles)
{
  point2 p_alice,p_obstacle;
  double dist;
  unsigned int upperBound;
  bool possibleCollision=false;
  LaneLabel lane;


  // loop through all found obstacles and compare trajectories
  for (unsigned int i=0; i<PredictionList.size(); i++)
    {
      // as Alice and the obstacle might have different prediction horizons, adjust upperBound
      if (alice_traj.size()<=PredictionList[i].traj.size())
        upperBound=alice_traj.size();
      else
        upperBound=PredictionList[i].traj.size();

      possibleCollision=false;
      for (unsigned int j=0; j<upperBound; j++)
        {
          p_alice = alice_traj[j];
          p_obstacle = PredictionList[i].traj[j];
          
          // compute distance between both points of trajectories
          dist = sqrt( pow(p_alice.x-p_obstacle.x,2) + pow(p_alice.y-p_obstacle.y,2) );
          
          // if dist is smaller than threshold, assume collision and increse collision counter
          if (dist<SEPERATION_THRESHOLD)
            {
              PredictionList[i].collisionCounter++;
              PredictionList[i].collisionPoint = PredictionList[i].traj[j];
              PredictionList[i].collisionTime = PredictionList[i].traj_time[j];
              possibleCollision=true;
              
              // only count collision once in case the trajectories are so close
              // this will also automatically return the closest collision as the vector is time discretisated
              break;
            }
        }

      // if no collision was detected, reset counter
      if (!possibleCollision)
        PredictionList[i].collisionCounter=0;
    }


  // initialize the return values
  pObstacle.collisionTime = 0;
  pObstacle.collisionPoint = point2(0,0);

  // this is a hack to get the maximum integer as something like INT_MAX does not work with uint64. Did not find a function yet to get the maximum for uint64
  for (unsigned int i=0; i<PredictionList.size(); i++)
    if (PredictionList[i].collisionTime > pObstacle.collisionTime)
      pObstacle.collisionTime = PredictionList[i].collisionTime;

  // return the number of obstacles that are stored in the vehicle list. This is the number of obstacles that are under prediction
  numberObstacles = PredictionList.size();
  int index=-1;

  // If there might be several collisions, find and return the closest one
  for (unsigned int i=0; i<PredictionList.size(); i++)
    {
      if ((PredictionList[i].collisionCounter>=COLLISION_THRESHOLD) && (PredictionList[i].collisionTime <= pObstacle.collisionTime))
        {
          pObstacle.collisionTime = PredictionList[i].collisionTime;
          pObstacle.collisionPoint = PredictionList[i].collisionPoint;
          pObstacle.me = PredictionList[i].me;
          index=i;
        }
    }

  // if at least one very possible collision was found, return its coordinates and time
  MapElement me;
  if (index != -1)
    {
      // send predicted collision to debugMap
      me.id=9999;
      me.setTypePoints();

      me.setColor(MAP_COLOR_RED,100);
      me.setGeometry(pObstacle.collisionPoint, 5.0);
      debugMap.sendMapElement(&me,sendChannel);

      return COLLISION;
    }
  else
    {
      // clear predicted collision point out of map
      me.id=9999;
      me.setTypeClear();
      debugMap.sendMapElement(&me,sendChannel);
    }


  return CLEAR;
}
