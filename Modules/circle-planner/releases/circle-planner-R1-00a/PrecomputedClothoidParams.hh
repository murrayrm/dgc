/* 
 * PrecomputedClothoidParams.hh: list of parameters used to define the properties of the precomputed 
 * clothoid data base, and to control the expansion of the Clothoid Node Tree
 *
 * Based on Code written by Kenny Osland, modified by Joel Burdick
 *
 */


/* ---------------------------------------------------------------------------------------- */
/*                   Constants and Definitions related to Clothoids                         */
/* ---------------------------------------------------------------------------------------- */

// distance between points in a clothoid trajectory (measured in meters)
#define PATH_SECTION_LENGTH			0.25

// maximum curvature for a path that alice can physically drive
#define PATH_MAX_DYN_FEASIBLE_CURVATURE		(1/VEHICLE_MIN_TURNING_RADIUS)


// how many different discrete lengths to compute

#define CLOTHOID_DB_NUM_LENGTHS			6
#define CLOTHOID_DB_MAX_LENGTH			9.0

/* the number of discrete increments in curvature between 0 and the max curvature.  Note that
   the total number of curvatures increments will be 2 * CLOTHOID_DB_NUM_CURVATURES + 1, as 
   the zero curvature path and positive/negative curvature paths will be computed */

#define CLOTHOID_DB_NUM_CURVATURES		3

// there will be initial curvature = 0 (the +1 term) and a number of paths = 
// DB_NUM_CURVATURES between 0 and the max curvature on either side (the 2* term)

#define CLOTHOID_DB_INIT_CURV_IDX_BOUND		(2 * CLOTHOID_DB_NUM_CURVATURES + 1)

/*  from each initial curvature, the set of possible final curvatures will 
    DB_NUM_FINAL_CURVATURES increments on eitherside of the initial curvature. 
    this must be <= DB_NUM_CURVATURES
*/

#define CLOTHOID_DB_NUM_FINAL_CURVATURES	2

// number of different final curvatures:  works the same way as DB_INIT_CURV_IDX_BOUND

#define CLOTHOID_DB_FINAL_CURV_IDX_BOUND	(2 * CLOTHOID_DB_NUM_FINAL_CURVATURES + 1)

