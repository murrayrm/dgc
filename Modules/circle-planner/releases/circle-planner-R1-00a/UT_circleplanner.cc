/*  unit test of Path Tree Planner components.  This is just a way to test components
    without having to interface to the rest of the system */

#include <stream.h>
#include "frames/pose2.hh"
#include "CirclePlanner.hh"

using namespace std; 

int main()
{
  /* dummy start and goal for testing */
  pose2 startPose = pose2(5.0,5.0,0.0);
  pose2 goalPose   = pose2(50.0, -15.0, -0.7 * M_PI);

  // create an instance of the planner for testing.  This sets up a start node, and sets the start node ptr
  // to the starting pose node.  It also also sets the goal node (which is needed for the A* class).

  cerr << "starting Circle Planner... " << endl;

  //instantiate a new planner
  CirclePlanner * curPlanner = new CirclePlanner(startPose,goalPose);   

  // create paths and A* search them.
  bool planresult = curPlanner->CirclePlannerMain();

  if(planresult)
    {
      cerr << "Circle Planner found a solution " << endl;
      cerr << "  Saving all potential solution paths in clothoidTree.dat.. " << endl;
      ofstream tree_file("clothoidTree.dat");
      curPlanner->m_startNodePtr->printAllPaths(&tree_file);

      cerr << "  Saving Solution Path in clothoidPath.dat... " << endl;
      ofstream path_file("clothoidPath.dat");
      for(vector<Clothoid *>::iterator j = curPlanner->m_forwardPath.begin(); j!=curPlanner->m_forwardPath.end(); j++)
	{
	  (*j)->printClothoid(&path_file);
	};
    }
  else
    {
      cerr << "  Circle Planner couldn't find a solution " << endl;
    };

  cout << "    Good bye " << endl;
  return 1;
};


