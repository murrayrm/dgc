/*
 * tempCircleInterface.cc
 * 
 * functions to interface circleplanner with the planning stack
 *
 * Based on code originally written by Kenny Oslund, May 07.
 * Updated and maintained by Joel BUrdick
 * 18 Sept. 07
 */

#include "tempCircleInterface.hh"

// set up storage for Graph Path nodes needed by planner
GraphNode CirclePlannerInterface::nodeArray[GRAPH_PATH_MAX_NODES];

/* run through a new planning cycle and return the results.  cSpecs is a container with current 
   information on the state of Alice and the environment.  path is the path that we want to return. */

Err_t CirclePlannerInterface::GenerateTraj(CSpecs_t *cSpecs, Path_t *path)
{
  cerr << " got into CirclePlannerInterface " << endl;
  Err_t returnStatus;

  if (cSpecs ==  NULL)
    {
      cerr << "   CirclePlannerInterface::GenerateTraj -- CSpecs is NULL " << endl;
      return 0;
    }
  else   // if CSpecs is valid, then update the problem parameters and generate a new trajectory
    {
      // get Alice's starting and goal poses via CSpecs
      vector<double> conds = cSpecs->getStartingState();   
      pose2 startPose(conds.at(STATE_IDX_X),conds.at(STATE_IDX_Y),conds.at(STATE_IDX_THETA));
      // get the desired goal pose
      vector<double> condf=cSpecs->getFinalState();
      pose2 goalPose(condf.at(STATE_IDX_X),condf.at(STATE_IDX_Y),condf.at(STATE_IDX_THETA));
      
      // print message to console for debugging
      /*
      Console::addMessage("CIRCLEPLANNER:: ******** new planning cycle ******** ");
      Console::addMessage("CIRCLEPLANNER:: planning from: x: %lf y: %lf ang: %lf", startPose.x, 
                                           startPose.y, startPose.ang);
      Console::addMessage("CIRCLEPLANNER:: to: x: %lf y: %lf ang: %lf", goalPose.x, goalPose.y, goalPose.ang);
      */

      //make an instance of  CirclePlanner with the given start and goal states and the create the plan.
      // if the plan is successful, then convert CirclePlanners's path representation to a GraphPath. 

      CirclePlanner * curPlanner = new CirclePlanner(startPose,goalPose);
      bool planStatus = curPlanner->CirclePlannerMain();
      if (planStatus == true)
	{
	  populatePath(curPlanner,path);
	  returnStatus = LP_OK;
	}
      else
	{
	  returnStatus = false;
	};
      //      curPlanner->~CirclePlanner();  // Now that we've made a plan, or failed at it, clean up the memory usage
    };
  return returnStatus;
}

// a function to transform the solution path representation made in Circle Planner to the Graph/GraphNode 
// representation used by planner

void CirclePlannerInterface::populatePath(CirclePlanner * planPtr, Path_t *path)
{
  if (!planPtr->m_forwardPath.empty())         // if we found a path
    {
      // set variables in the GraphPath structure
      path->valid = true;                     // path is valid if we found it
      path->collideObs = 0;                   // we don't think there are any obstacle or car collisions
      path->collideCar = 0;

      double pathLength = 0.0;                // variable that will accumulate total path length
      if (planPtr->m_forwardPath.size() > 0) 
	{
	  int i = 0;                          // The number of points in the path

	  // iterator over the clothoids in m_forwardPath
	  for (vector<Clothoid *>::iterator j = planPtr->m_forwardPath.begin(); j != planPtr->m_forwardPath.end(); j++)
	    {
	      vector<pose2> clothoid = (*j)->m_clothoidPoints;
	      pathLength += (*j)->m_length;
	      // iterate over the points in the current clothoid
	      for (vector<pose2>::iterator k = clothoid.begin(); k != clothoid.end(); k++, i++) 
		{
		  nodeArray[i].pose.pos.x = (*k).x;
		  nodeArray[i].pose.pos.y = (*k).y;
		  nodeArray[i].pose.pos.z = 0;

		  if ( (*j)->m_reverse )
		    { nodeArray[i].pathDir = GRAPH_PATH_REV; }
		  else
		    { nodeArray[i].pathDir = GRAPH_PATH_FWD;  };

		  path->curvatures[i] = (*j)->m_initCurv;    // is this really necessary?
		  nodeArray[i].pose.rot = quat_from_rpy(0, 0, (*k).ang);
		  path->path[i] = &(nodeArray[i]);
		  /*		  setPoint(i, *k, (*j)->m_reverse, path); */
		};
	    };
	  path->goalDist = pathLength;       // distance to goal is sum of clothoid lengths
	  path->pathLen = i;                 // total number of nodes in this path
	};
    };
  return;
};

// currently, this is not used.  Just kept here for documentation purposes
void CirclePlannerInterface::setPoint(int index, pose2 newPt, bool reverse, Path_t *path)
{
  nodeArray[index].pose.pos.x = newPt.x;
  nodeArray[index].pose.pos.y = newPt.y;
  nodeArray[index].pose.pos.z = 0;
  if ( reverse )
    {
      nodeArray[index].pathDir = GRAPH_PATH_REV;
    }
  else
    {
      nodeArray[index].pathDir = GRAPH_PATH_FWD;
      nodeArray[index].pose.rot = quat_from_rpy(0, 0, newPt.ang);
      path->path[index] = &(nodeArray[index]);
    };
};





