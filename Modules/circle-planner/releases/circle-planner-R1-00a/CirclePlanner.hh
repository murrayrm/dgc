/*
 * CirclePlanner.hh: Define the Circle Planner Class, which is a zone planner.
 * 
 * Written by Joel Burdick.starting Sept 10, 2007
 */

/* ---------------------------------------------------------------------------------------- */
/*                                Includes and Defines                                      */
/* ---------------------------------------------------------------------------------------- */

#include <iostream>
#include <stdio.h>

// stl includes
#include <algorithm>
#include <set>
#include <vector>
#include "frames/point2.hh"
#include "frames/pose2.hh"

#include "temp-planner-interfaces/GraphPath.hh"
#include "temp-planner-interfaces/CmdArgs.hh"
#include "temp-planner-interfaces/PlannerInterfaces.h"   // needed for Err_t definition

#include "PathTree.hh"

#ifndef CIRCLEPLANNER_HH
#define CIRCLEPLANNER_HH

using namespace std;


/* ---------------------------------------------------------------------------------------- */
/*                                   The Circle Planner class                               */
/* ---------------------------------------------------------------------------------------- */

class CirclePlanner
{
public:                                    
  enum                            // the different kinds of circle "handedness and handedness combinations
    {
      LEFT_HAND_CIRCLE,           // two basic types of handedness we must deal with         
      RIGHT_HAND_CIRCLE,   
      RIGHT_HAND_START_CIRCLE,    // four types of start and goal circles
      LEFT_HAND_START_CIRCLE,
      RIGHT_HAND_GOAL_CIRCLE,
      LEFT_HAND_GOAL_CIRCLE,    
      RIGHT_RIGHT_COMBO,          // different types of start and goal handedness combinations
      RIGHT_LEFT_COMBO,
      LEFT_RIGHT_COMBO,
      LEFT_LEFT_COMBO
    };

  pose2 m_startPose;
  PTNode * m_startNodePtr;             // pointer to the start, or base, node of the graph of path nodes and edges   

  pose2 m_goalPose;
  PTNode * m_goalNodePtr;              // pointer to goal node

  vector<Clothoid *> m_solutionPath;   // sequence of clothoids (in reverse order) that will define the solution.
  vector<Clothoid *> m_forwardPath;    // the reverse (properly ordered) solution path

  unsigned int m_nClothoids;           // keep track of the number of Clothoids produced.(for debugging)

private:

  float Cross2D(point2 V1,point2 V2);      // simple 2-dimensional vector operations
  float Dot2D(point2 V1, point2 V2);
  float FindTheta(point2 V1, point2 V2);   // Find the angle between two unit vectors on a 

public:

  CirclePlanner(pose2 startPose, pose2 goalPose);              // constructor
  ~CirclePlanner();                                            // destructor
  // makes straight and circular path segments needed to construct plans
  PTEdge * makeStraightLine(point2 startPoint, point2 endPoint, bool reverse);
  PTEdge * makeForwardCircularArc(point2 startCenter, float startAngle, float stopAngle, unsigned int handedness);
  PTEdge * makeReverseCircularArc(point2 startCenter, float startAngle, float stopAngle, unsigned int handedness);
  // construct bitangents between a pair of circles.
  void CirclePair(point2 startCenter, point2 goalCenter, unsigned int handedness);
  // make all of the bitangents between a pair of start circles and a pair goal circles.
  void makeQuadCirclePlan();
  // main function to construct a plan once a planner is instantiated
  bool CirclePlannerMain();

  // functions and data that interface CirclePlanner with Planner

  static GraphNode nodeArray[GRAPH_PATH_MAX_NODES];        // list of the Graph Nodes that make up the solution  
  static Err_t GenerateTraj(CSpecs_t *cSpecs, Path_t *path);   /* update the Path tree and return a path */

  /* convert the path generated by CirclePlanner to a form that can be used by the planner */
  void populateGraphPath(Path_t *path);


};

#endif            // CIRCLEPLANNER_HH
