/* 
 * CirclePlannerParams.hh: list of parameters used by Circle Planner
 *
 * Based on Code written by Kenny Osland, modified by Joel Burdick
 */

/* ---------------------------------------------------------------------------------------- */
/*                   Constants and Definitions related to Clothoids                         */
/* ---------------------------------------------------------------------------------------- */

// distance between points in a trajectory (measured in meters) for Circle Planner
#define CP_PATH_SECTION_LENGTH			0.5

// the turning radius used for planning, which should be slightly over Alice's minimum turning radius
#define TURNING_RADIUS (VEHICLE_MIN_TURNING_RADIUS + 0.3)

// the multiplying factor used in setting length-based cost of a path when using reverse
#define REVERSE_PATH_PENALTY                    3.0


/* ---------------------------------------------------------------------------------------- */
/*                   Constants and Definitions related to Path Tree                         */
/* ---------------------------------------------------------------------------------------- */

// the "stand off" distances
#define START_STAND_OFF_DISTANCE                10.0        // in meters
#define GOAL_STAND_OFF_DISTANCE                10.0        // in meters
