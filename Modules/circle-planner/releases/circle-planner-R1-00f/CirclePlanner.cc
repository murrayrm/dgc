/* CirclePlanner.cc: functions that implement a nonholonomic motion planner for a car.
 * based on the concept of "circles" at the goal and start--hence the name circle-planner.
 *
 * Written by Joel Burdick: Sept. 08, 2007
 *
 */

/* ------------------------------------------------------------------------------------- */
/*                                 Defines and Includes                                  */
/* ------------------------------------------------------------------------------------- */
#include "frames/quat.h"
#include "temp-planner-interfaces/PlannerInterfaces.h"
#include "cspecs/CSpecs.hh"

#include "CirclePlanner.hh"
#include "AStar.hh"

/* ------------------------------------------------------------------------------------- */
/*                                Constuctors/Destructors                                */
/* ------------------------------------------------------------------------------------- */

// simple constructor that sets the start and goal states for the planner
CirclePlanner::CirclePlanner(pose2 startPose, pose2 goalPose)
{
  m_startPose = startPose;
  m_startNodePtr = new PTNode(startPose);
  m_goalPose = goalPose;
  m_goalNodePtr  = new PTNode(goalPose);
  m_nClothoids = 0;
};


// simple destructor
CirclePlanner::~CirclePlanner()
{
  delete m_startNodePtr;
  delete &m_startPose;
  delete & m_goalPose;
  delete m_goalNodePtr;
  delete &m_solutionPath;
  delete &m_forwardPath;
};

/* ------------------------------------------------------------------------------------- */
/*                                Simple helper Functions                                */
/* ------------------------------------------------------------------------------------- */

// 2-dimensional cross-product
float CirclePlanner::Cross2D(point2 V1,point2 V2)
{
  return (V1.x * V2.y - V1.y * V2.x);
};

// 2-dimensional dot-product
float CirclePlanner::Dot2D(point2 V1, point2 V2)
{
  return (V1.x * V2.x + V1.y * V2.y);
};

double CirclePlanner::unwrap_angle(double inAngle)
{
  double outAngle=inAngle;
  while (outAngle<-M_PI)
    {outAngle += 2.0 * M_PI;};
  while (outAngle > M_PI)
    {outAngle -= 2.0 * M_PI;};
  return outAngle;
};


// find the angle, of vector V2 relative to vector  V1 (need not assum that V1 and V2 are unit vectors)
float CirclePlanner::FindTheta(point2 V1, point2 V2)
{
  float normV1 = sqrt(V1.x * V1.x + V1.y * V1.y);
  float normV2 = sqrt(V2.x * V2.x + V2.y * V2.y);
  float CAngle = Dot2D(V1,V2)/(normV1 * normV2);
  float SAngle = Cross2D(V1,V2)/(normV1 * normV2);
  double returnAngle = atan2(SAngle,CAngle);
  return (unwrap_angle(returnAngle));
};

  
/* ------------------------------------------------------------------------------------- */
/*                           Basic Path Construction Functions                           */
/* ------------------------------------------------------------------------------------- */

// make a straight line path corresponding to a bitangent between two circles

PTEdge * CirclePlanner::makeStraightLine(point2 startPoint, point2 endPoint, bool reverse)
{
  unsigned int i;
  float dx = endPoint.x - startPoint.x;
  float dy = endPoint.y - startPoint.y;
  float length = sqrt(dx * dx + dy * dy);
  float angle = atan2(dy,dx);
  

  float fraction;
  // determine the number of points that will be used in this segment
  unsigned int npoints = (unsigned int) (1+ floor(length/CP_PATH_SECTION_LENGTH));

  // allocate a new clothoid segment at the starting point of the straight line, and then link in the 
  // subsequent points.  Note that straight lines are Clothoids with zero initial and final curvature
  m_nClothoids++;
  Clothoid * newClothoidPtr = new Clothoid(0.0,0.0,length,reverse, m_nClothoids);  

  for(i=1; i<npoints; i++)
    {
      fraction = (float) i/npoints;
      pose2 * cpt= new pose2((startPoint.x + fraction * dx),(startPoint.y + fraction * dy),angle);
      if (reverse)
	{
	  cpt->ang += M_PI;
	  cpt->unwrap_angle();
	}
      newClothoidPtr->m_clothoidPoints.push_back((*cpt));
    };
  PTEdge * newPTEdgePtr = new PTEdge(newClothoidPtr);

  return newPTEdgePtr;
};

//------------------------------------------------------------------------------------------
// make a circular arc path in which the vehicle goes backward
PTEdge * CirclePlanner::makeForwardCircularArc(point2 startCenter, float startAngle, float stopAngle, 
						 unsigned int handedness, double RADIUS)
{
  float deltaAngle, arclength, stepAngle;
  unsigned int i, npoints;
  Clothoid * newClothoidPtr;

  if (handedness == RIGHT_HAND_CIRCLE)             // clockwise rotation (decreasing angle) is forward motion
    {
      if (startAngle>0)
	{
	  if (stopAngle > 0)
	    {
	      if (stopAngle > startAngle)    
		{
		  deltaAngle = stopAngle - startAngle - 2.0 * M_PI;
		}
	      else   
		{
		  deltaAngle = (stopAngle - startAngle);
		};
	    }
	  else       // (startAngle > 0) && (stopAngle < 0)
	    {
	      deltaAngle = stopAngle-startAngle;
	    };
	}         
      else           // if we reach here, (startAngle <= 0)
	{ 
	  if (stopAngle>0)        // (Pi > stopAngle > 0) && (-Pi < startAngle < 0) 
	    {
	      deltaAngle = stopAngle - startAngle - 2.0 * M_PI;
	    }
	  else       // (-PI < stopAngle <= 0) && (-Pi < startAngle <= 0)
	    {
	      if (stopAngle > startAngle) 
		{
		  deltaAngle = stopAngle - startAngle - 2.0 * M_PI;
		}
	      else
		{ 
		  deltaAngle = (stopAngle - startAngle);
		};
	    };     
	};
    }
  else    //handedness == LEFT_HAND, so that counterclockwise rotation is forward motion.(with increasing angle)
    {
      if (startAngle > 0)
	{
	  if (stopAngle > 0)
	    {
	      if (stopAngle > startAngle)
		{
		  deltaAngle = stopAngle-startAngle;
		}
	      else
		{	      
		  deltaAngle = stopAngle-startAngle + 2.0 * M_PI;
		};
	    }
	  else  // (startAngle > 0) && (stopAngle < 0)
	    {
	      deltaAngle = stopAngle-startAngle + 2.0 * M_PI;
	    };
	}
      else                       // (startAngle < 0)
	{
	  if (stopAngle > 0)     // (startAngle < 0) && (stopAngle > 0)
	    {
	      deltaAngle = stopAngle-startAngle;
	    }
	  else                   // (startAngle < 0) && (stopAngle < 0)
	    {
	      if (stopAngle >  startAngle)
		{
		  deltaAngle = stopAngle-startAngle;
		}
	      else
		{
		  deltaAngle = stopAngle-startAngle + 2.0 * M_PI;
		};
	    };
	};
    };

  // compute the distance the vehicle will go around this portion of the circle.
  arclength = fabs( RADIUS * deltaAngle);  

  // Allocate a Clothoid.  NOTE, in the following call, the turning radius should be given the appropriate sign.
  // Right now this information is not used, but it might be in the future.
  m_nClothoids++;
  newClothoidPtr = new Clothoid(RADIUS, RADIUS, arclength, false, m_nClothoids);

  npoints = 1 + (unsigned int) floor(arclength/CP_PATH_SECTION_LENGTH);
  if (npoints == 1) {npoints = 2;}                                       // check for this exceptional condition that 
                                                                         //    arclength < PATH_SECTION_LENGTH
  stepAngle = deltaAngle/(npoints -1);

  for(i=0; i<npoints; i++)
    {
      float curAngle = startAngle + (float) i * stepAngle;
      pose2 newPose = pose2( (startCenter.x + cos(curAngle) * RADIUS), 
			     (startCenter.y + sin(curAngle) * RADIUS), curAngle);

      if (handedness == RIGHT_HAND_CIRCLE)
	{newPose.ang -= M_PI/2.0; }
      else
	{newPose.ang += M_PI/2.0; }
      newPose.unwrap_angle();

      newClothoidPtr->m_clothoidPoints.push_back(newPose);
    };
 
  PTEdge * newPTEdgePtr = new PTEdge(newClothoidPtr);

  return newPTEdgePtr;
};


//------------------------------------------------------------------------------------------
// make a circular arc path in which the vehicle goes backward
PTEdge * CirclePlanner::makeReverseCircularArc(point2 startCenter, float startAngle, float stopAngle, 
						 unsigned int handedness, double RADIUS)
{
  float deltaAngle, arclength, stepAngle;
  unsigned int i, npoints;
  Clothoid * newClothoidPtr;

  if (handedness == RIGHT_HAND_CIRCLE)             // counterclockwise rotation (increasing angle) is reverse motion
    {
      if (startAngle>0)
	{
	  if (stopAngle > 0)
	    {
	      if (stopAngle > startAngle)    
		{
		  deltaAngle = stopAngle - startAngle;
		}
	      else   
		{
		  deltaAngle = stopAngle - startAngle + 2.0 * M_PI;
		};
	    }
	  else       // (startAngle > 0) && (stopAngle < 0)
	    {
	      deltaAngle = stopAngle-startAngle + 2.0 * M_PI;
	    };
	}         
      else           // if we reach here, (startAngle <= 0)
	{ 
	  if (stopAngle>0)        // (Pi > stopAngle > 0) && (-Pi < startAngle < 0) 
	    {
	      deltaAngle = stopAngle - startAngle;
	    }
	  else       // (-PI < stopAngle <= 0) && (-Pi < startAngle <= 0)
	    {
	      if (stopAngle > startAngle) 
		{
		  deltaAngle = stopAngle - startAngle;
		}
	      else
		{ 
		  deltaAngle = stopAngle - startAngle + 2.0 * M_PI;
;
		};
	    };     
	};
    }
  else    //handedness == LEFT_HAND, so that clockwise rotation is reverse motion.(with decreasing angle)
    {
      if (startAngle > 0)
	{
	  if (stopAngle > 0)
	    {
	      if (stopAngle > startAngle)
		{
		  deltaAngle = stopAngle-startAngle - 2.0 * M_PI;
		}
	      else
		{	      
		  deltaAngle = stopAngle-startAngle;
		};
	    }
	  else  // (startAngle > 0) && (stopAngle < 0)
	    {
	      deltaAngle = stopAngle-startAngle;
	    };
	}
      else                       // (startAngle < 0)
	{
	  if (stopAngle > 0)     // (startAngle < 0) && (stopAngle > 0)
	    {
	      deltaAngle = stopAngle-startAngle - 2.0 * M_PI;
	    }
	  else                   // (startAngle < 0) && (stopAngle < 0)
	    {
	      if (stopAngle >  startAngle)
		{
		  deltaAngle = stopAngle-startAngle - 2.0 * M_PI;
		}
	      else
		{
		  deltaAngle = stopAngle-startAngle;
		};
	    };
	};
    };
  // compute the distance the vehicle will go around start circle.
  arclength = fabs(RADIUS * deltaAngle);

  // Allocate a Clothoid.  NOTE, in the following call, the turning radius should be given the appropriate sign.
  // Right now this information is not practically used, but it might be in the future.
  m_nClothoids++;
  newClothoidPtr = new Clothoid(RADIUS,RADIUS, arclength, true,m_nClothoids);

  npoints = 1+ (unsigned int) floor(arclength/CP_PATH_SECTION_LENGTH);
  if (npoints == 1) {npoints = 2;}                                       // check for this exceptional condition that 
                                                                         //    arclength < PATH_SECTION_LENGTH
  stepAngle = deltaAngle/(npoints -1);

  for(i=0; i<npoints; i++)
    {
      float curAngle = startAngle + (float) i * stepAngle;
      pose2 newPose = pose2( (startCenter.x + cos(curAngle) * RADIUS), 
			     (startCenter.y + sin(curAngle) * RADIUS), curAngle);

      if (handedness == RIGHT_HAND_CIRCLE)
	{newPose.ang -= M_PI/2.0; }
      else
	{newPose.ang += M_PI/2.0; }
      newPose.unwrap_angle();

      newClothoidPtr->m_clothoidPoints.push_back(newPose);
    };

  PTEdge * newPTEdgePtr = new PTEdge(newClothoidPtr);

  return newPTEdgePtr;
};


/* ------------------------------------------------------------------------------------- */
/*                                   Main Work Functions                                 */
/* ------------------------------------------------------------------------------------- */

// Compute the relevant bitangents for a pair of start and goal circles, then links the paths 
// to the Path Tree Start Node to form a graph, which is then A* searched

void CirclePlanner::CirclePair(point2 startCenter, point2 goalCenter, unsigned int handedness, double RADIUS)
{
  float x12 = goalCenter.x - startCenter.x;          // distances between centers of start and goal circle
  float y12 = goalCenter.y - startCenter.y;
  float L12 = sqrt(x12 * x12 + y12 * y12);
  float phi12 = atan2(y12,x12);                      // angle made by vector from startCenter to GoalCenter

  const point2 xaxis = point2(1.0,0.0);              // the unit vector colinear with the x-axis
  point2 v1(- RADIUS * sin(phi12), RADIUS * cos(phi12));             

  //  find the angles corresponding to the  robot's start and goal poses,
  point2 startLine = point2(m_startPose.x,m_startPose.y) - startCenter;
  point2 goalLine = point2(m_goalPose.x,m_goalPose.y) - goalCenter;
  float startAngle = FindTheta(xaxis,startLine);     // angle on start cicle made by Alice's start pose
  float goalAngle = FindTheta(xaxis,goalLine);       // angle made on goal circle by Alice's goal pose

  // First check to see if the circles are overlapping.  If so, there make the bitangents that are 
  // appropriate for this case

  // variables needed to construct and store paths 
  PTEdge * startForwardCircle1Ptr;      PTEdge * startReverseCircle1Ptr;
  PTEdge * goalForwardCircle1Ptr;       PTEdge * goalReverseCircle1Ptr;

  PTEdge * startForwardCircle2Ptr;      PTEdge * startReverseCircle2Ptr;
  PTEdge * goalForwardCircle2Ptr;       PTEdge * goalReverseCircle2Ptr;

  // if both the start and goal circles are both right or left handed, only outer bitangents are relevant
  if ( (handedness == RIGHT_RIGHT_COMBO) || (handedness == LEFT_LEFT_COMBO) )
    {
      // first compute the points of contact of the two outer bitangents
      point2 z1a = startCenter + v1;         // location of circle 1's first outer bitangent contact point
      point2 z1b = startCenter - v1;         // location of circle 1's second outer bitangent contact point
      point2 z2a = goalCenter  + v1;         // location of circle 2's first outer bitangent contact point
      point2 z2b = goalCenter  - v1;         // location of circle 2's second outer bitangent contact point

      //-------------------------- paths and nodes related to the outer bitangents ------------------------

      // set up the paths on the starting and goal circles.  as well as the angles corresponding to the 
      // bitangent's contact points
 
      float thetaz1a = FindTheta(xaxis,v1);              // angle that bitangent at z1a makes
      float thetaz2a = thetaz1a;                         // angle that bitangent at z2a makes
      float thetaz1b = -thetaz1a;  float thetaz2b = thetaz1b;

      // make the simple straight line segment associated with the first outer bitangent.  We'll link to
      // it in a moment. Also construct the two circular arcs that connect the staring pose to the 
      // bitangent point, and the distal bitangent to the goal pose.

      PTNode * biTangent1StartNode;      PTNode * biTangent1EndNode;      PTEdge * biTangent1Ptr;          
      PTNode * biTangent2StartNode;      PTNode * biTangent2EndNode;      PTEdge * biTangent2Ptr;

      if (handedness == RIGHT_RIGHT_COMBO)
	{
	  // make segments and nodes associated with first bitangent
	  startForwardCircle1Ptr = makeForwardCircularArc(startCenter, startAngle, thetaz1a, RIGHT_HAND_CIRCLE, RADIUS);
	  startReverseCircle1Ptr = makeReverseCircularArc(startCenter, startAngle, thetaz1a, RIGHT_HAND_CIRCLE, RADIUS); 
	  biTangent1StartNode = new PTNode(pose2(z1a.x,z1a.y,thetaz1a - 0.5 * M_PI));
	  biTangent1Ptr = makeStraightLine(z1a,z2a,false);
	  biTangent1EndNode = new PTNode(pose2(z2a.x,z2a.y,thetaz2a - 0.5 * M_PI));
	  goalForwardCircle1Ptr = makeForwardCircularArc(goalCenter, thetaz2a, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle1Ptr = makeReverseCircularArc(goalCenter, thetaz2a, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);

	  // make segments and nodes associated with second bitangent
	  startForwardCircle2Ptr = makeForwardCircularArc(startCenter, startAngle, thetaz1b, RIGHT_HAND_CIRCLE, RADIUS);
	  startReverseCircle2Ptr = makeReverseCircularArc(startCenter, startAngle, thetaz1b, RIGHT_HAND_CIRCLE, RADIUS); 
	  biTangent2StartNode = new PTNode(pose2(z1b.x,z1b.y,thetaz1b - 0.5 * M_PI));
	  biTangent2Ptr = makeStraightLine(z1b,z2b,true);
	  biTangent2EndNode = new PTNode(pose2(z2b.x,z2b.y,thetaz2b - 0.5 * M_PI));
	  goalForwardCircle2Ptr = makeForwardCircularArc(goalCenter, thetaz2b, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle2Ptr = makeReverseCircularArc(goalCenter, thetaz2b, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);

	}
      else   // else we have a LEFT_LEFT_COMBO
	{
	  // make segments and nodes associated with first bitangent
	  startForwardCircle1Ptr = makeForwardCircularArc(startCenter, startAngle, thetaz1a, LEFT_HAND_CIRCLE, RADIUS);
	  startReverseCircle1Ptr = makeReverseCircularArc(startCenter, startAngle, thetaz1a, LEFT_HAND_CIRCLE, RADIUS);
	  biTangent1StartNode = new PTNode(pose2(z1a.x,z1a.y,thetaz1a + 0.5 * M_PI));
	  biTangent1Ptr = makeStraightLine(z1a,z2a,false); 
	  biTangent1EndNode = new PTNode(pose2(z2a.x,z2a.y,thetaz2a + 0.5 * M_PI));
	  goalForwardCircle1Ptr = makeForwardCircularArc(goalCenter, thetaz2a, goalAngle, LEFT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle1Ptr = makeReverseCircularArc(goalCenter, thetaz2a, goalAngle, LEFT_HAND_CIRCLE, RADIUS);

	  // make segments and nodes associated with second bitangent
	  startForwardCircle2Ptr = makeForwardCircularArc(startCenter, startAngle, thetaz1b, LEFT_HAND_CIRCLE, RADIUS);
	  startReverseCircle2Ptr = makeReverseCircularArc(startCenter, startAngle, thetaz1b, LEFT_HAND_CIRCLE, RADIUS);
	  biTangent2StartNode = new PTNode(pose2(z1b.x,z1b.y,thetaz1b + 0.5 * M_PI));
	  biTangent2Ptr = makeStraightLine(z1b,z2b,true); 
	  biTangent2EndNode = new PTNode(pose2(z2b.x,z2b.y,thetaz2b + 0.5 * M_PI));
	  goalForwardCircle2Ptr = makeForwardCircularArc(goalCenter, thetaz2b, goalAngle, LEFT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle2Ptr = makeReverseCircularArc(goalCenter, thetaz2b, goalAngle, LEFT_HAND_CIRCLE, RADIUS);

	};

      // now splice all of the pieces together, starting from the goal and working backwards to the start.  

      // components associated with 1st bitangent
      goalForwardCircle1Ptr->m_childPTNodePtr = this->m_goalNodePtr;      // link goal to this edge
      biTangent1EndNode->m_outEdges.push_back(goalForwardCircle1Ptr);     // link one of the goal circular edges

      goalReverseCircle1Ptr->m_childPTNodePtr = this->m_goalNodePtr;      // link goal to this edge
      biTangent1EndNode->m_outEdges.push_back(goalReverseCircle1Ptr);     // link the other goal circular edge

      biTangent1Ptr->m_childPTNodePtr = biTangent1EndNode;                // link bitangent to distal node 
      biTangent1StartNode->m_outEdges.push_back(biTangent1Ptr);           // link the start of the bitangent

      startForwardCircle1Ptr->m_childPTNodePtr = biTangent1StartNode;      // link a start circular arc to bitangent
      this->m_startNodePtr->m_outEdges.push_back(startForwardCircle1Ptr);  // link the start of the bi-tangent to 
      startReverseCircle1Ptr->m_childPTNodePtr = biTangent1StartNode;      // link a start circular arc to bitangent
      this->m_startNodePtr->m_outEdges.push_back(startReverseCircle1Ptr);

      // components associated with 2nd bitangent
      goalForwardCircle2Ptr->m_childPTNodePtr = this->m_goalNodePtr;        // link goal to this edge
      biTangent2EndNode->m_outEdges.push_back(goalForwardCircle2Ptr);      // link one of the goal circular edges

      goalReverseCircle2Ptr->m_childPTNodePtr = this->m_goalNodePtr;       // link goal to this edge
      biTangent2EndNode->m_outEdges.push_back(goalReverseCircle2Ptr);      // link the other goal circular edge

      biTangent2Ptr->m_childPTNodePtr = biTangent2EndNode;                 // link bitangent to its distal node
      biTangent2StartNode->m_outEdges.push_back(biTangent2Ptr);            // link the start of the bitangent

      startForwardCircle2Ptr->m_childPTNodePtr = biTangent2StartNode;      // link a start circular arc to bitangent
      this->m_startNodePtr->m_outEdges.push_back(startForwardCircle2Ptr);  // link the start of the bi-tangent to 
      startReverseCircle2Ptr->m_childPTNodePtr = biTangent2StartNode;      // link a start circular arc to bitangent
      this->m_startNodePtr->m_outEdges.push_back(startReverseCircle2Ptr);
    }
  else  // must be RIGHT_LEFT_COMBO or LEFT_RIGHT_COMBO, which implies that only the inner bitangents are active
    {   // first we check if the circles are overlapping. If so, then the inner bitangents do not exist, and we
        // must return before constructing this bitangents.
      if (L12 < (2.0 * RADIUS))
	{
	  return; 
	};
      
      float coseta = 2 * RADIUS / L12;  
      float eta = acos(coseta);
      float psiPlus = phi12 + eta;  float psiMinus = phi12 - eta;
      float cPsiMinus = cos(psiMinus), sPsiMinus = sin(psiMinus);
      float cPsiPlus = cos(psiPlus), sPsiPlus = sin(psiPlus);

      // vectors n1 and n2 define the locations of the bi-tangent contact points
      point2 n1 = RADIUS * point2(cPsiPlus, sPsiPlus);       // related to first inner bitangent
      point2 n2 = RADIUS * point2(cPsiMinus, sPsiMinus);     // related to second inner bitangent

      // now create the points where the inner bi-tangents contact the first circle
      point2 y1a = startCenter + n1;  point2 y2a = goalCenter - n2;           // end points of 1st inner bitangent
      point2 y1b = startCenter + n2;  point2 y2b = goalCenter - n1;           // end points of 2nd inner bitangent
      float thetay1a = FindTheta(xaxis,n1);  
      float thetay1b = FindTheta(xaxis,n2);
      float thetay2a = FindTheta(xaxis,(-1.0 * n2));
      float thetay2b = FindTheta(xaxis,(-1.0 * n1));

      PTEdge * crossTangent1Ptr;            PTEdge * crossTangent2Ptr;
      PTNode * crossTangent1StartNode;      PTNode * crossTangent1EndNode;      
      PTNode * crossTangent2StartNode;      PTNode * crossTangent2EndNode;      

      if (handedness == RIGHT_LEFT_COMBO)
	{
	  // make segments and nodes associated with first inner bitangent
	  startForwardCircle1Ptr = makeForwardCircularArc(startCenter, startAngle, thetay1a, RIGHT_HAND_CIRCLE, RADIUS);
	  startReverseCircle1Ptr = makeReverseCircularArc(startCenter, startAngle, thetay1a, RIGHT_HAND_CIRCLE, RADIUS); 
	  crossTangent1StartNode = new PTNode(pose2(y1a.x,y1a.y,thetay1a - 0.5 * M_PI));
	  crossTangent1EndNode = new PTNode(pose2(y2b.x,y2b.y,thetay2b + 0.5 * M_PI));
	  crossTangent1Ptr = makeStraightLine(y1a,y2b,false);
	  goalForwardCircle1Ptr = makeForwardCircularArc(goalCenter, thetay2b, goalAngle, LEFT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle1Ptr = makeReverseCircularArc(goalCenter, thetay2b, goalAngle, LEFT_HAND_CIRCLE, RADIUS);

	  // make segments and nodes associated with second inner bitangent
	  startForwardCircle2Ptr = makeForwardCircularArc(startCenter, startAngle, thetay1b, RIGHT_HAND_CIRCLE, RADIUS);
	  startReverseCircle2Ptr = makeReverseCircularArc(startCenter, startAngle, thetay1b, RIGHT_HAND_CIRCLE, RADIUS); 
	  crossTangent2StartNode = new PTNode(pose2(y1b.x,y1b.y,thetay1b - 0.5 * M_PI));
	  crossTangent2Ptr = makeStraightLine(y1b,y2a,true);
	  crossTangent2EndNode = new PTNode(pose2(y2a.x,y2a.y,thetay2a +0.5 * M_PI));
	  goalForwardCircle2Ptr = makeForwardCircularArc(goalCenter, thetay2a, goalAngle, LEFT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle2Ptr = makeReverseCircularArc(goalCenter, thetay2a, goalAngle, LEFT_HAND_CIRCLE, RADIUS);
	}
      else     // else must be LEFT_RIGHT_COMBO 
	{
	  // make segments and nodes associated with first bitangent
	  startForwardCircle1Ptr = makeForwardCircularArc(startCenter, startAngle, thetay1a, LEFT_HAND_CIRCLE, RADIUS);
	  startReverseCircle1Ptr = makeReverseCircularArc(startCenter, startAngle, thetay1a, LEFT_HAND_CIRCLE, RADIUS);
	  crossTangent1StartNode = new PTNode(pose2(y1a.x,y1a.y,thetay1a + 0.5 * M_PI));
	  crossTangent1Ptr = makeStraightLine(y1a,y2b,true); 
	  crossTangent1EndNode = new PTNode(pose2(y2b.x,y2b.y,thetay2b - 0.5 * M_PI));
	  goalForwardCircle1Ptr = makeForwardCircularArc(goalCenter, thetay2b, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle1Ptr = makeReverseCircularArc(goalCenter, thetay2b, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);

	  // make segments and nodes associated with second bitangent
	  startForwardCircle2Ptr = makeForwardCircularArc(startCenter, startAngle, thetay1b, LEFT_HAND_CIRCLE, RADIUS);
	  startReverseCircle2Ptr = makeReverseCircularArc(startCenter, startAngle, thetay1b, LEFT_HAND_CIRCLE, RADIUS);
	  crossTangent2StartNode = new PTNode(pose2(y1b.x,y1b.y,thetay1b + 0.5 * M_PI));
	  crossTangent2Ptr = makeStraightLine(y1b,y2a,false); 
	  crossTangent2EndNode = new PTNode(pose2(y2a.x,y2a.y,thetay2a - 0.5 * M_PI));
	  goalForwardCircle2Ptr = makeForwardCircularArc(goalCenter, thetay2a, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle2Ptr = makeReverseCircularArc(goalCenter, thetay2a, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);
	};

      // splice all of the pieces together, starting from the goal and working backwards to the start.  

      // components associated with 1st bitangent
      goalForwardCircle1Ptr->m_childPTNodePtr = this->m_goalNodePtr;        // link goal to this edge
      crossTangent1EndNode->m_outEdges.push_back(goalForwardCircle1Ptr);    // link one of the goal circular edges

      goalReverseCircle1Ptr->m_childPTNodePtr = this->m_goalNodePtr;        // link goal to this edge
      crossTangent1EndNode->m_outEdges.push_back(goalReverseCircle1Ptr);    // link the other goal circular edge

      crossTangent1Ptr->m_childPTNodePtr = crossTangent1EndNode;            // link node at end of bitangent to the bitangent
      crossTangent1StartNode->m_outEdges.push_back(crossTangent1Ptr);       // link the start of the bitangent

      startForwardCircle1Ptr->m_childPTNodePtr = crossTangent1StartNode;    // link one start circular arc to start of bitangent
      this->m_startNodePtr->m_outEdges.push_back(startForwardCircle1Ptr);   // link the start of the bi-tangent to 
      startReverseCircle1Ptr->m_childPTNodePtr = crossTangent1StartNode;    // link one start circular arc to start of bitangent
      this->m_startNodePtr->m_outEdges.push_back(startReverseCircle1Ptr);

      // components associated with 2nd bitangent
      goalForwardCircle2Ptr->m_childPTNodePtr = this->m_goalNodePtr;        // link goal to this edge
      crossTangent2EndNode->m_outEdges.push_back(goalForwardCircle2Ptr);    // link one of the goal circular edges

      goalReverseCircle2Ptr->m_childPTNodePtr = this->m_goalNodePtr;        // link goal to this edge
      crossTangent2EndNode->m_outEdges.push_back(goalReverseCircle2Ptr);    // link the other goal circular edge

      crossTangent2Ptr->m_childPTNodePtr = crossTangent2EndNode;            // link node at end of bitangent to the bitangent
      crossTangent2StartNode->m_outEdges.push_back(crossTangent2Ptr);       // link the start of the bitangent

      startForwardCircle2Ptr->m_childPTNodePtr = crossTangent2StartNode;    // link one start circular arc to start of bitangent
      this->m_startNodePtr->m_outEdges.push_back(startForwardCircle2Ptr);   // link the start of the bi-tangent to 
      startReverseCircle2Ptr->m_childPTNodePtr = crossTangent2StartNode;    // link one start circular arc to start of bitangent
      this->m_startNodePtr->m_outEdges.push_back(startReverseCircle2Ptr);
    };					      
  return;
};

// makes the basic set of paths that involve the pair of starting and goal circles.
void CirclePlanner::makeQuadCirclePlan(double RADIUS)
{
  // calculate the centers of the starting circles
  point2 startOffset = point2( -(RADIUS * sin(this->m_startPose.ang)),(RADIUS * cos(this->m_startPose.ang)) );
  point2 leftStartCircleCenter = point2((this->m_startPose.x + startOffset.x),
					(this->m_startPose.y + startOffset.y));
  point2 rightStartCircleCenter = point2((this->m_startPose.x - startOffset.x),
					 (this->m_startPose.y - startOffset.y));

  // calculate the centers of the goal circles
  point2 goalOffset = point2( -(RADIUS * sin(this->m_goalPose.ang)), (RADIUS * cos(this->m_goalPose.ang)) );
  point2 leftGoalCircleCenter = point2((this->m_goalPose.x + goalOffset.x),(this->m_goalPose.y + goalOffset.y));
  point2 rightGoalCircleCenter = point2((this->m_goalPose.x - goalOffset.x),(this->m_goalPose.y - goalOffset.y));

  //make up the paths for the combinations of START and GOAL circles


  CirclePair(rightStartCircleCenter, rightGoalCircleCenter, RIGHT_RIGHT_COMBO, RADIUS);
  CirclePair(leftStartCircleCenter, leftGoalCircleCenter, LEFT_LEFT_COMBO, RADIUS);
  CirclePair(rightStartCircleCenter, leftGoalCircleCenter, RIGHT_LEFT_COMBO, RADIUS);
  CirclePair(leftStartCircleCenter, rightGoalCircleCenter, LEFT_RIGHT_COMBO,RADIUS);

};


// remove all infeasible paths (those that intersect the zone boundary).  Right now this is a simple wrapper.
// but it might get expanded in the future.  See CirclePlannerUtils.cc for CullNodeOutEdges
void CirclePlanner::CullInfeasiblePaths()
{
  CullNodeOutEdges(this->m_startNodePtr);
};

#if CIRCLE_PLANNER_DEBUG                                  
void CirclePlanner::CullInfeasiblePaths(ofstream *f)    // version for debuggin
{
  CullNodeOutEdges(this->m_startNodePtr, f);
};
#endif

/* --------------------------------------------------------------------------------------------- */
// the main function for Circle Planner.  This assumes that an instance of Circle Planner has
// already been created (given information about starting and goal states, etc).  This function 
// has the following structure:  1) create the possible paths to solve the problem; 2) remove the
// paths that are physically infeasible; 3) create an instance of the A* search class, and
// then search all of the feasible paths for the best path.
/* --------------------------------------------------------------------------------------------- */

bool CirclePlanner::CirclePlannerMain(CSpecs_t *cSpecs)
{
  bool returnStatus;
  unsigned int SearchState;                            // status of Search
  unsigned int SearchSteps=1;                          // how many "Search Steps" have we taken

  Log::getStream(1) << "CIRCLEPLANNER:: entered CirclePlanner Main" << endl;

  this->makeQuadCirclePlan(TURNING_RADIUS);            // set up all of the possible start/goal paths.

  // now remove illegal paths (ones where Alices intersects the zone boundary)
  // we do this by first getting the zone boundary information, then converting it to a right handed
  // coordinate system.  Then we cull the paths based on this boundary information.

  point2arr leftHandedZoneBoundary = cSpecs->getBoundingPolygon();
  this->m_rightHandedZoneBoundary = LeftToRightPointArray(leftHandedZoneBoundary);

#if CIRCLE_PLANNER_DEBUG
  cerr << "  Saving all potential solution paths in clothoidTree.dat.. " << endl;
  ofstream tree_file("clothoidTree.dat");
  this->m_startNodePtr->printAllPaths(&tree_file);
  cerr << "  putting zone boundary in tree file " << endl;
  this->printZoneBoundary(&tree_file); 
#endif

#if CIRCLE_PLANNER_DEBUG
  this->CullInfeasiblePaths(&tree_file);
#else
  this->CullInfeasiblePaths();
#endif

#if CIRCLE_PLANNER_DEBUG
  cerr << "  Saving  culled paths in culledTree.dat.. " << endl;
  ofstream ctree_file("culledTree.dat");
  this->m_startNodePtr->printAllPaths(&ctree_file);
#endif

  // now that feasible paths have been generated, make an AStarSearch instance, then set start and Goal states.
  AStarSearch astarsearch;                          
  astarsearch.SetStartAndGoalStates(this->m_startNodePtr, this->m_goalNodePtr); 

  // main A* search loop that increments A* search via one pass through the Open List at each step.
  do
    {
      SearchState = astarsearch.SearchStep(this);       // take one search step
      SearchSteps++;
    }
  while (SearchState == AStarSearch::SEARCH_STATE_SEARCHING);

  // now that the search is completed, check for success.  If successful, put together the path and return.
  if(SearchState==AStarSearch::SEARCH_STATE_SUCCEEDED)
    {
      Console::addMessage("CIRCLEPLANNER:: A* search successful");
      Log::getStream(5) << "CIRCLEPLANNER:: A* search successful" << endl;
      // due to nature of the A* search, the solution path gets saved in reverse order.  So, let's 
      // reverse it in preparation for the planner interface to create a graph path
      for(vector<Clothoid *>::reverse_iterator i=this->m_solutionPath.rbegin(); i != this->m_solutionPath.rend(); ++i)
	{
	  this->m_forwardPath.push_back( (*i) );
	  returnStatus = true;
	};
    }
  else
    { 
      Log::getStream(5) << "CIRCLEPLANNER:: A* search failed" << endl;
      Console::addMessage("CIRCLEPLANNER:: A* FAILED ");
      returnStatus = false; 
    };

  Log::getStream(1) << "CIRCLEPLANNER:: exited CirclePlanner Main" << endl;
  return returnStatus;
};

/* ------------------------------------------------------------------------------------- */
/*                           Planner Interface Functions                                 */
/* ------------------------------------------------------------------------------------- */

// set up storage for Graph Path nodes needed by planner
GraphNode CirclePlanner::nodeArray[GRAPH_PATH_MAX_NODES];


/* ------------------------------------------------------------------------------------------------- */
// run through a new planning cycle and return the results.  cSpecs is a container with current 
// information on the state of Alice and the environment.  path is the path that we want to return.
// Note that because of the weird choice of northing and easting for Alice, we need to transform all of
// the resulting calculations in CirclePlanner (which are done in a right=handed reference frame) to 
// Alice's left-handed reference frame
/* ------------------------------------------------------------------------------------------------- */

Err_t CirclePlanner::GenerateTraj(CSpecs_t *cSpecs, Path_t *path)
{
  Err_t returnStatus;

  if (cSpecs ==  NULL)               // first check that CSpecs is providing data
    {
      Console::addMessage("CIRCLEPLANNER:: -- CSpecs is NULL, Exiting ");
      return CIRCLE_PLANNER_FAILED;
    };

  // get Alice's starting (conds) and goal (condg) poses via CSpecs.  Remember to switch from Alice's
  // left-handed coordinate system to a right-handed coordinate system.

  vector<double> conds = cSpecs->getStartingState();   
  pose2 startPose(conds.at(STATE_IDX_Y),conds.at(STATE_IDX_X),((M_PI/2.0) - conds.at(STATE_IDX_THETA)) );
  startPose.unwrap_angle();

  vector<double> condg=cSpecs->getFinalState();
  pose2 goalPose(condg.at(STATE_IDX_Y),condg.at(STATE_IDX_X),((M_PI/2.0) - condg.at(STATE_IDX_THETA)) );
  goalPose.unwrap_angle();
  
  Console::addMessage("CIRCLEPLANNER:: ******** new planning cycle ******** ");
  Console::addMessage("CIRCLEPLANNER:: start pose: x: %lf y: %lf ang: %lf", startPose.x, startPose.y, startPose.ang);
  Console::addMessage("CIRCLEPLANNER::  goal pose: x: %lf y: %lf ang: %lf", goalPose.x, goalPose.y, goalPose.ang);
  
  //make an instance of  CirclePlanner with the given start and goal states and then create the plan with
  // CirclePlannerMain. if the plan is successful, convert CirclePlanners's path representation to a GraphPath,
  // also taking into account the right hand to left hand coordinate frame transformation

  CirclePlanner * curPlanner = new CirclePlanner(startPose,goalPose);
  bool planStatus = curPlanner->CirclePlannerMain(cSpecs);

  if (planStatus == true)
    {
#if CIRCLE_PLANNER_DEBUG
      cerr << "  printing path to clothoidPath.m " << endl;
      ofstream path_file("clothoidPath.dat");
      for(vector<Clothoid *>::iterator j = curPlanner->m_forwardPath.begin(); j!=curPlanner->m_forwardPath.end(); j++)
	{ 
	  (*j)->printClothoid(&path_file); 
	  curPlanner->printAliceBoxPath((*j),&path_file);
	};
#endif
      curPlanner->populateGraphPath(path);
      returnStatus = CIRCLE_PLANNER_OK;
    }
  else
    {
      returnStatus = CIRCLE_PLANNER_FAILED;
    };
  return returnStatus;
};

// ------------------------------------------------------------------------------------------------------
// a function to transform the solution path representation made in Circle Planner to the Graph/GraphNode 
// representation used by planner.  This function also takes care of the transformation from a right-handed
// coordinate (which is the type of coordinate system used by Circle Planner for all of its internal calculations
// to the left-handed reference used by Alice

void CirclePlanner::populateGraphPath(Path_t *path)
{
  Log::getStream(6) << "   CIRCLEPLANNER Log: starting populateGraph " << endl;
  if (!this->m_forwardPath.empty())           // if we found a feasible path, then ....
    {
      // set variables in the GraphPath structure
      path->valid = true;                     // path is valid if we found it
      path->collideObs = 0;                   // we don't think there are any obstacle or car collisions
      path->collideCar = 0;

      double pathLength = 0.0;                // variable that will accumulate total path length

      if (this->m_forwardPath.size() > 0) 
	{
	  int i = 0;                          // The number of points in the path
	  // iterator over the clothoids path segments in m_forwardPath
	  for (vector<Clothoid *>::iterator j = this->m_forwardPath.begin(); j != this->m_forwardPath.end(); j++)
	    {
	      vector<pose2> clothoid = (*j)->m_clothoidPoints;
	      pathLength += (*j)->m_length;

	      // iterate over the points in the current clothoid.  Node, counter "i" gets updated here
	      Log::getStream(9) << "   CIRCLEPLANNER Log: populateGraph " << endl;
	      for (vector<pose2>::iterator k = clothoid.begin(); k != clothoid.end(); k++, i++) 
		{

		  Log::getStream(9) << "       x = " << (*k).x << ", y= " << (*k).y << ", ang= " << (*k).ang ;

		  // note, DON'T FORGET THAT WE MUST CONVERT BACK TO ALICE'S LEFT HAND COORDINATE SYSTEM!
		  nodeArray[i].pose.pos.x = (*k).y;
		  nodeArray[i].pose.pos.y = (*k).x;
		  nodeArray[i].pose.pos.z = 0;
		  double tempAngle = 0.5 * M_PI - (*k).ang;
		  nodeArray[i].pose.rot = quat_from_rpy(0, 0, unwrap_angle(tempAngle));
		  
		  // HACK:  Magnus asked me to give the nodes distinct negative numbers so as not to interfere
		  // with some other part of the system. I don't understand, but I'll follow his request ....
		  nodeArray[i].index = -(i+1);
		  if ( (*j)->m_reverse )                                   // add forward/reverse info
		    { 
		      Log::getStream(9) << ", path is backward " << endl;
		      nodeArray[i].pathDir = GRAPH_PATH_REV; 
		    }
		  else
		    {
		      Log::getStream(9) << ", path is forward " << endl;
		      nodeArray[i].pathDir = GRAPH_PATH_FWD;  
		    };
		  path->curvatures[i] = (*j)->m_initCurv;                  // this is used by velocity planner
		  path->path[i] = &(nodeArray[i]);
		};
	    };
	  path->goalDist = pathLength;       // distance to goal is sum of clothoid lengths
	  path->pathLen = i;                 // total number of nodes in this path
	};
      Log::getStream(6) << "   CIRCLEPLANNER: finishing populateGraph " << endl;
    };
  return;
};


