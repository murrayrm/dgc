/*
 * CirclePlanner.hh: Define the Circle Planner Class, which is a zone planner.
 * 
 * Written by Joel Burdick.starting Sept 10, 2007
 */

/* ---------------------------------------------------------------------------------------- */
/*                                         Includes                                         */
/* ---------------------------------------------------------------------------------------- */

#include <iostream>
#include <stdio.h>

// stl includes
#include <algorithm>
#include <set>
#include <vector>
#include "frames/point2.hh"
#include "frames/pose2.hh"

#include "temp-planner-interfaces/GraphPath.hh"
#include "temp-planner-interfaces/CmdArgs.hh"
#include "temp-planner-interfaces/PlannerInterfaces.h"   // needed for Err_t definition

#include <temp-planner-interfaces/Console.hh>
#include <temp-planner-interfaces/Log.hh>

#include "PathTree.hh"

/* ---------------------------------------------------------------------------------------- */
/*                                        Defines                                           */
/* ---------------------------------------------------------------------------------------- */

#define CIRCLE_PLANNER_DEBUG 0                           // set to zero for no debug info, or 1 for debug info

#ifndef CIRCLEPLANNER_HH
#define CIRCLEPLANNER_HH

using namespace std;

/* ---------------------------------------------------------------------------------------- */
/*                                 The Circle Planner class                                 */
/* ---------------------------------------------------------------------------------------- */

class CirclePlanner
{
public:                                    
  enum                            // the different kinds of circle "handedness and handedness combinations
    {
      LEFT_HAND_CIRCLE,           // two basic types of handedness we must deal with         
      RIGHT_HAND_CIRCLE,   
      RIGHT_HAND_START_CIRCLE,    // four types of start and goal circles
      LEFT_HAND_START_CIRCLE,
      RIGHT_HAND_GOAL_CIRCLE,
      LEFT_HAND_GOAL_CIRCLE,    
      RIGHT_RIGHT_COMBO,          // different types of start and goal handedness combinations
      RIGHT_LEFT_COMBO,
      LEFT_RIGHT_COMBO,
      LEFT_LEFT_COMBO
    };

  pose2 m_startPose;
  PTNode * m_startNodePtr;             // pointer to the start, or base, node of the graph of path nodes and edges   

  pose2 m_goalPose;
  PTNode * m_goalNodePtr;              // pointer to goal node

  point2arr m_rightHandedZoneBoundary; // a copy of the CSpecs boundary information, in a right handed coordinate frame

  vector<Clothoid *> m_solutionPath;   // sequence of clothoids (in reverse order) that will define the solution.
  vector<Clothoid *> m_forwardPath;    // the reverse (properly ordered) solution path

  unsigned int m_nClothoids;           // keep track of the number of Clothoids produced.(for debugging)

private:

  float Cross2D(point2 V1,point2 V2);      // simple 2-dimensional vector operations
  float Dot2D(point2 V1, point2 V2);
  float FindTheta(point2 V1, point2 V2);   // Find the angle between two unit vectors on a 
  double unwrap_angle(double inAngle);     // simple function to put angles in (-PI, PI)

  // private utility functions from CirclePlannerUtils.hh
  bool PointInCircle(point2 p, point2 circleCenter, double R);   // is point p in circle?
  bool PointInSegment(point2 x1, point2 x2, point2 p);           // is p in the segment[x1,x2]?
  bool ConvexVertex(point2 prev, point2 current, point2 next);   // is current vertex convex?
  bool SegmentCircleIntersection(point2 p1, point2 p2, point2 circleCenter, double R);
  void printPoint2arr(point2arr parray, ofstream *f);    // print a point2arr
  void printZoneBoundary(ofstream *f);
  void printAliceBoxPath(Clothoid * cPtr, ofstream *f);
public:

  CirclePlanner(pose2 startPose, pose2 goalPose);              // constructor
  ~CirclePlanner();                                            // destructor

  // makes straight and circular path segments needed to construct plans
  PTEdge * makeStraightLine(point2 startPoint, point2 endPoint, bool reverse);
  PTEdge * makeForwardCircularArc(point2 startCenter, float startAngle, float stopAngle, unsigned int handedness, double RADIUS);
  PTEdge * makeReverseCircularArc(point2 startCenter, float startAngle, float stopAngle, unsigned int handedness, double RADIUS);

  // construct bitangents between a pair of circles.
  void CirclePair(point2 startCenter, point2 goalCenter, unsigned int handedness, double RADIUS);

  // make all of the bitangents between a pair of start circles and a pair goal circles.
  void makeQuadCirclePlan(double RADIUS);

  // weed out the paths where Alice intersects the zone boundary
  void CullInfeasiblePaths();
  void CullInfeasiblePaths(ofstream *f);                       // the alternative version for debugging

  // main circle planner function
  bool CirclePlannerMain(CSpecs_t *cSpecs);

  // utility functions from CirclePlannerUtils.cc
  point2arr LeftToRightPointArray(point2arr leftArray);
  point2arr AliceBoundingBox(pose2 alicePose);
  bool IsPathLegal(Clothoid * cPtr);
  bool IsPathLegal(Clothoid * cPtr, ofstream *f);              // the alternative version for debugging
  void CullNodeOutEdges(PTNode * nodePtr);
  void CullNodeOutEdges(PTNode * nodePtr, ofstream *f);        // the alternative version for debugging

  // ---------------------------------------------------------------------------------------------------------
  // functions and data that interface CirclePlanner with Planner

  static GraphNode nodeArray[GRAPH_PATH_MAX_NODES];        // list of the Graph Nodes that make up the solution  
  static Err_t GenerateTraj(CSpecs_t *cSpecs, Path_t *path);   /* update the Path tree and return a path */

  /* convert the path generated by CirclePlanner to a form that can be used by the planner */
  void populateGraphPath(Path_t *path);
};

#endif            // CIRCLEPLANNER_HH
