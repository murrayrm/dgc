/*
 * AStar.cc: helper and work functions for the A* algorithm that searches the PathTree.  This 
 * code is partially based on the A* algorithm implementation by Justin Heyes-Joens
 *
 */

#include "CirclePlanner.hh"
#include "AStar.hh"

// Initialize the Start and Goal states, as well as initialize the A* Search data structures
// and variables
void AStarSearch::SetStartAndGoalStates(PTNode * startNodePtr, PTNode * goalNodePtr)
{
  m_Start = new Node();                                 // allocate search node for the start node
  m_AllocateNodeCount ++;
  m_Goal =  new Node();                                 // allocate search node for the goal node
  assert((m_Start != NULL && m_Goal != NULL));          // check that space is allocated
  m_AllocateNodeCount ++;

  m_Start->m_PTNodePtr = startNodePtr;
  m_Goal->m_PTNodePtr = goalNodePtr;
  m_State = SEARCH_STATE_SEARCHING;                     // set status to "searching" mode

  // Initialise the AStar specific cost-function parts of the Start Node
  m_Start->g = 0;                                       // cost to reach start goal is zero!
  m_Start->h = m_Start->m_PTNodePtr->GoalDistanceEstimate( m_Goal->m_PTNodePtr );
  m_Start->f = m_Start->g + m_Start->h;
  m_Start->parent = NULL;

  m_OpenList.push_back( m_Start );        // Push the start node on the Open list.  The heap is now unsorted
  
  // Sort back element into heap using the STL heap functions.  
  push_heap( m_OpenList.begin(), m_OpenList.end(), HeapCompare_f() );

  m_Steps = 0;                                          // Initialise counter for search steps
};


// This function returns the successors (or children) to a Clothoid Tree Node.  This
//  is needed by the A* algorithm.  It works by first checking to see if the node has 
//  children.  If so, then it loops through all links to children of the current node,
//  and then calls the A* function "AddSuccessor" for each child in order to get a new 
//  search tree node and link associated with the Successor.  Node, the for loop
//  should use an iterator, but I couldn't get the iterator approach to compile.
//  this should be checked to see if it really works---JWB
 
bool AStarSearch::GetSuccessors(PTNode * PTNodePtr)
{
  bool ret;

  if (!PTNodePtr)                                          // if NULL pointer, complain and return false
    {
      cerr << "     GetSuccessors: Empty PTNode Pointer " << endl;
      return false;
    }
  else                                                     // else, check to see if the node has children.
    {
      if (!PTNodePtr->m_outEdges.empty())                  // if there are children, add successor nodes for each child
	{                                
	  // iterate through all of the edges pointing away from this node.
	  for(vector <PTEdge *>::iterator j = PTNodePtr->m_outEdges.begin(); j != PTNodePtr->m_outEdges.end(); j++)
	    {
	      ret = this->AddSuccessor((*j));

	      if (!ret)                                    // if false, there's a memory problem
		{ 
		  cerr << "    A* GetSuccessors: A* Search couldn't allocate memory" << endl;
		  return false;
		}
	    };
	  return true;
	};   
      return true;                                        // if you reach here, the node has no children
    };                                                    // there are no children, but that's okay--we'll 
};                                                        // expand later if needed 

//----------------------------------------------------------------------------------------------
// This creates a node in the search tree for a successor.  Since the list of successors is created by
// searching through outgoing edges to a parent PTNode, the distal end of the edge is the successor that we seek.
bool AStarSearch::AddSuccessor(PTEdge * PTEdgePtr)
{
  Node *node = new Node();
  m_AllocateNodeCount ++;
  if( node )
    {
      node->m_PTNodePtr = PTEdgePtr->m_childPTNodePtr;
      node->m_incomingCost = PTEdgePtr->m_edgeCost;
      node->m_incomingEdgePtr = PTEdgePtr;
      m_Successors.push_back( node );
      return true;
    };
  return false;
};

// function to return the cost of moving from the current Clothoid Node to the 
//  successor Clothoid Node.  Alternatively, you can think of this as the cost of
//  getting from the parent node to the node pointed to by NodePtr
float AStarSearch::GetCost(Node * NodePtr)
{
  return NodePtr->m_incomingCost + NodePtr->m_PTNodePtr->m_nodeCost;
};


/* ------------------------------------------------------------------------------------- */
// Advances the A* search by one step.  This is the main work function of the A* search
// algorithm.  First we check for a few failure conditions.  If we haven't failed, then 
// we check to see if we're at the goal.  If so, then backstep from the goal to the start,
// and then return success.  If not, then process all of the successors to the current 
// node selected from the open list.  
//
// This version of SearchStep implements a version of the implicit A* search.  If a node 
// does not have children, then iterate through the precomputed clothoid data base to see
// if any of the paths are useful. If so, then link them in and process them.

unsigned int AStarSearch::SearchStep(CirclePlanner * curPlanner)
{
  // First, break if the user has not initialised the search.  (this step can probably be eliminated)
  assert( (m_State > SEARCH_STATE_NOT_INITIALISED) && (m_State < SEARCH_STATE_INVALID) );
  
  // In case a searchstep is called after the search has succeeded, this will prevent failure
  // (I'm not sure it's really necessary---JWB.  Remove if possible)
  if( (m_State == SEARCH_STATE_SUCCEEDED) || (m_State == SEARCH_STATE_FAILED)  )
    { return m_State; };
      
  // Failure occurs when the open list is empty (nothing left to search).
  // So, if the open list is empty, declare failure and return to the main control loop.
  if( m_OpenList.empty() )
    {
      cerr << "In A* Search: the open list is empty, and thus searching has failed" << endl;
      FreeAllNodes();                             // if failed, clean up all the memory used in search
      m_State = SEARCH_STATE_FAILED;
      return m_State;
    }
		
  // Incremment step count if we haven't failed,
  m_Steps ++;

  // Pop the best node off the open list--the one with the lowest total cost f.
  // (you need to understand the STL heap functions to understand this step.  See
  //  http://www.cplusplus.com/reference/algorithm/pop_heap.html for details.  The C++ STL 
  // function pop_heap puts the best node at the back of the  heap.  It then needs to be 
  // stripped off using the pop_back function. ---JWB.)
      
  Node *n = m_OpenList.front();                  // get pointer to the best node in the list
  pop_heap( m_OpenList.begin(), m_OpenList.end(), HeapCompare_f() );
  m_OpenList.pop_back();

  // Check for the goal. Once we pop that, we're done.  Also handle special cases first 
  if( n->m_PTNodePtr->IsGoal( m_Goal->m_PTNodePtr ) )
    {
      // We'll use the passed in Goal Node as the final end point, so copy the parent pointer of n.
      m_Goal->parent = n->parent;
      m_Goal->m_incomingEdgePtr = n->m_incomingEdgePtr;

      // Now back trace from the goal to the start so that we can strip off the solution path.  The back trace 
      // starts with an "if" in order to handle the case that the goal state equals the start state.
      if( false == n->m_PTNodePtr->IsSameState( m_Start->m_PTNodePtr ) )
	{
          // push the last clothoid onto the solutionPath
          curPlanner->m_solutionPath.push_back(n->m_incomingEdgePtr->m_clothoid);
	  FreeNode( n );                       // we don't need n any more, as it points to goal, which we'll just use
	  Node *nodeChild = m_Goal;            // start the back step sequence with the goal state.  First set the child to goal
	  Node *nodeParent = m_Goal->parent;   // Then set the parent to the goal's parent
	  
	  // now step back through the sequence of nodes from the goal to the start
	  do
	    {
	      nodeParent->child = nodeChild;  
	      nodeChild = nodeParent;
	      nodeParent = nodeParent->parent;
	      if (nodeChild != m_Start)
		{
		  curPlanner->m_solutionPath.push_back(nodeChild->m_incomingEdgePtr->m_clothoid);
		};
	    } 
	  while( nodeChild != m_Start ); // Start is always the first node by definition
	};

      FreeUnusedNodes();                       // now that we've found the goal, delete nodes that aren't needed for the solution
      m_State = SEARCH_STATE_SUCCEEDED;        //search succeeded if at goal
      return m_State;
    }
  else              // we're not at goal node, so now process the successors of this node
    {
      // We now generate the successors of this node, which are contained in m_Successors.
      m_Successors.clear();                    // clear the vector of successor nodes to prepare for new successors list
		  
      // GetSuccessors looks for all of the Clothoid Tree Node's children, and then calls AddSuccessor for each one 
      // to generate a new node in the search spanning tree.First check for memory allocation problems.  If not, proceed.

      bool ret = GetSuccessors(n->m_PTNodePtr); 

      if( !ret )                                        // check to see if memory allocated,  If not, 
	{                                               // clean up the mess and report memory failure
	  cerr <<"   A* Search: there's problem in GetSuccessors" << endl;
	  // free the search tree nodes that may previously have been added if we're out of memory
	  for(vector< Node * >::iterator successor = m_Successors.begin(); successor != m_Successors.end(); successor ++ )
	    { FreeNode( (*successor) ); }
	  m_Successors.clear();           // clear the vector of successor nodes to n
	  FreeAllNodes();       	  // free up everything else we allocated
	  m_State = SEARCH_STATE_OUT_OF_MEMORY;
	  return m_State;
	};
	  
      // Now handle each successor to the current node ...
      for(vector< Node * >::iterator successor = m_Successors.begin(); successor != m_Successors.end(); successor ++ )
	{                                                         // iterate over successors
	  // 	The g value for this successor ...
	  float newg = n->g + GetCost( (*successor) );
	  
	  // Next we check if the successor node is on the open or closed lists.  If the node is on one of these lists,
          // then we must check if the existing or the successor node does the best job.

	  vector< Node * >::iterator openlist_result;
	  for(openlist_result = m_OpenList.begin(); openlist_result != m_OpenList.end(); openlist_result ++ )
	    {
	      if( (*openlist_result)->m_PTNodePtr->IsSameState( (*successor)->m_PTNodePtr ) )
		{ break; }                                // if successor node is equivalent to an open list node,
	    }                                             // then stop iterating and check if the successor is better
	  if( openlist_result != m_OpenList.end() )
	    {
	      if( (*openlist_result)->g <= newg )         // if the equivalent state on theh open list has lower cost,g, then throw 
		{                                         // away the successor, since the one on Open is cheaper
		  FreeNode( (*successor) );
		  continue;                    		
		}
	    }
	  // Now we do the same search on the closed list (i.e., see if there is a cheaper equivalent node on the closed list.
	  vector< Node * >::iterator closedlist_result;
	  for( closedlist_result = m_ClosedList.begin(); closedlist_result != m_ClosedList.end(); closedlist_result ++ )
	    {
	      if( (*closedlist_result)->m_PTNodePtr->IsSameState( (*successor)->m_PTNodePtr ) )
		{ break; };                                   // if successor node is equivalent to an open list node,
	    };
	      
	  if( closedlist_result != m_ClosedList.end() )             // if we found an equivalent node on the closed list, ....
	    {
	      if( (*closedlist_result)->g <= newg ) 	            // if the pre-existing node is cheaper ,then
		{                                                   // get rid of the successor, as we now know that the
		  FreeNode( (*successor) );                         // successor pose isn't fruitful (since a cheaper node 
		  continue;                                         // is already on the closed list)
		}
	    }
	  // If we didn't find an equivalent and cheaper node on the open or closed lists, then this node is the 
          // best node so far for that particular state. so lets keep it and set up its AStar specific data ...
	  (*successor)->parent = n;
	  (*successor)->g = newg;
	  (*successor)->h = (*successor)->m_PTNodePtr->GoalDistanceEstimate( m_Goal->m_PTNodePtr );
	  (*successor)->f = (*successor)->g + (*successor)->h;
	      
	  // since the successor node is cheaper than the equivalent ones on open and closed, remove the ones on open and closed
	  if( closedlist_result != m_ClosedList.end() )
	    {
	      FreeNode(  (*closedlist_result) ); 	      // free up the memory of the one on the closed list
	      m_ClosedList.erase( closedlist_result );        // now pull out this more expensive node from the closed list
	    };
	  if( openlist_result != m_OpenList.end() )
	    {	   
	      FreeNode( (*openlist_result) ); 
	      m_OpenList.erase( openlist_result );
		  
	      // re-make the open list heap now that we've removed a node from it. Note that make_heap is used 
              // instead of sort_heap because sort_heap called on an invalid heap does not work
	      make_heap( m_OpenList.begin(), m_OpenList.end(), HeapCompare_f() );
	    };

	  // now put the successor node on the Open list, and then reorder the list 
          // NOTE: I think we can combine the the make_heap above and this make heap into one step?
	  m_OpenList.push_back( (*successor) );                                   // heap now unsorted
	  push_heap( m_OpenList.begin(), m_OpenList.end(), HeapCompare_f() ); 	  // sort back element into heap
	};
      m_ClosedList.push_back( n );                            // push n onto Closed, as we have expanded it now
    };                                                        // end of else (the node is not the goal, so expand it)

  return m_State;                                             // Succeeded bool is false at this point. 
}                                                             // end of SearchStep()


/* --------------------------------------------------------------------------------- */
/*                             Memory Management Functions                           */
/* --------------------------------------------------------------------------------- */

// This is called when a search fails or is cancelled to free all used memory 
void  AStarSearch::FreeAllNodes()
{
  // iterate open list and delete all nodes
  vector< Node * >::iterator iterOpen = m_OpenList.begin();
  
  while( iterOpen != m_OpenList.end() )
    {
      Node *n = (*iterOpen);
      FreeNode( n );
      iterOpen ++;
    }
  m_OpenList.clear();

  // iterate closed list and delete unused nodes
  vector< Node * >::iterator iterClosed;
  for( iterClosed = m_ClosedList.begin(); iterClosed != m_ClosedList.end(); iterClosed ++ )
    {
      Node *n = (*iterClosed);
      FreeNode( n );
    }
  m_ClosedList.clear();
  FreeNode(m_Goal);      // delete the goal
};


// This call is made when the search ends, since a lot of nodes may be created that are still present 
// when the search ends. FreeUnusedNodes deletes them once the search ends
void AStarSearch::FreeUnusedNodes()
{
  // iterate open list and delete unused nodes
  vector< Node * >::iterator iterOpen = m_OpenList.begin();
  while( iterOpen != m_OpenList.end() )
    {
      Node *n = (*iterOpen);
      if( !n->child )
	{
	  FreeNode( n );
	  n = NULL;
	}
      iterOpen ++;
    }
  m_OpenList.clear();

  // iterate closed list and delete unused nodes
  vector< Node * >::iterator iterClosed;
  for( iterClosed = m_ClosedList.begin(); iterClosed != m_ClosedList.end(); iterClosed ++ )
    {
      Node *n = (*iterClosed);
      if( !n->child )
	{
	  FreeNode( n );
	  n = NULL;
	}
    }
  m_ClosedList.clear();
};


// free up a node and adjust node counters
void AStarSearch::FreeNode( Node *node )
{
  m_AllocateNodeCount--;
  delete node;
};

/* ---------------------------------------------------------------------------------------- */
/*                          Functions for Traversing the Solution                           */
/* ---------------------------------------------------------------------------------------- */

// Get start node
PTNode * AStarSearch::GetSolutionStart()
{
  m_CurrentSolutionNode = m_Start;
  if( m_Start )
    { return m_Start->m_PTNodePtr;  }
  else
    {  
      cerr << " GetSolutionStart: m_Start is empty" << endl;
      return NULL;  
    };
};
	
// Get next node ---------------------------------------------------------------------------
PTNode * AStarSearch::GetSolutionNext()
{
  if( m_CurrentSolutionNode )
    {
      if( m_CurrentSolutionNode->child )
	{
	  Node * child = m_CurrentSolutionNode->child;
	  m_CurrentSolutionNode = m_CurrentSolutionNode->child;
	  return child->m_PTNodePtr;
	};
    };
  return NULL;
};
	
// Get end node ----------------------------------------------------------------------------
PTNode * AStarSearch::GetSolutionEnd()
{
  m_CurrentSolutionNode = m_Goal;
  if( m_Goal )
    { return m_Goal->m_PTNodePtr;  }
  else
    { 
      return NULL;
      cerr << "      GetSolutionEnd: m_Goal is NULL! " << endl;
    };
}
	
// Step solution iterator backwards -------------------------------------------------------
PTNode * AStarSearch::GetSolutionPrev()
{
  if( m_CurrentSolutionNode )
    {
      if( m_CurrentSolutionNode->parent )
	{
	  Node *parent = m_CurrentSolutionNode->parent;
	  m_CurrentSolutionNode = m_CurrentSolutionNode->parent;
	  return parent->m_PTNodePtr;
	};
    };
  return NULL;
};
