/* CirclePlannerUtils.cc: basic utility functions that get used by CirclePlanner
 *
 * Written by Joel Burdick (Sept. 26, 2007)
 */

/* ----------------------------------------------------------------------------------- */
/*                                     includes                                        */
/* ----------------------------------------------------------------------------------- */

#include <alice/AliceConstants.h>
#include "CirclePlanner.hh"

/* ----------------------------------------------------------------------------------- */
/*                                     defines                                         */
/* ----------------------------------------------------------------------------------- */

// HACK: these values are defined in LogicPlanner.cc, but are copied here so that we don't
// have to include that file
#define ALICE_SAFETY_FRONT   (1.0)
#define ALICE_SAFETY_REAR    (0.5)
#define ALICE_SAFETY_SIDE    (0.5)

/* ----------------------------------------------------------------------------------- */
/*                                 transformations                                     */
/* ----------------------------------------------------------------------------------- */

// transforms a point2arr in left handed Northing-Easting coordinates to right handed representation
point2arr CirclePlanner::LeftToRightPointArray(point2arr leftArray)
{
  point2arr rightArray; //allocate storage for new right handed array

  // iterate through all of the point2 elements of leftArray
  for(vector<point2>::iterator j=leftArray.arr.begin(); j!=leftArray.arr.end(); j++)
    {
      // transform from left handed representation to right handed representation
      point2 rightPoint = point2((*j).y, (*j).x);
      rightArray.arr.push_back(rightPoint);
    };
  return rightArray;
};


/* ----------------------------------------------------------------------------------- */
/*                                geometric functions                                  */
/* ----------------------------------------------------------------------------------- */


// check if the point p lies in the circle with center circleCenter and radius R.  Returns a boolean 
bool CirclePlanner::PointInCircle(point2 p, point2 circleCenter, double R)
{
  double dx = p.x - circleCenter.x;  double dy = p.y - circleCenter.y;
  if ((dx * dx + dy * dy) <= (R * R))
    {return true; }
  else
    {return false;};
};

// ------------------------------------------------------------------------------------
// check if the point p lies in the line segment bounded by x1 and x2.  Returns a boolean
bool CirclePlanner::PointInSegment(point2 x1, point2 x2, point2 p)
{
  bool point_lies_in_segment;

  point2 term1 = p - x1;   point2 term2 = x2 - x1;   point2 term3 = p - x2;

  if ((Dot2D(term1,term2)>0.0) && (Dot2D(term3,term2) < 0.0))
    {
      point_lies_in_segment = true;
    }
  else
    {
      point_lies_in_segment = false;
    };
  return point_lies_in_segment;
};


// ------------------------------------------------------------------------------------
// check if the vertex "current" is convex relative to its neighbors "previous" and "next".  This
// calculation assumes that the vertex indices are increasing in a clockwise fashion.

bool CirclePlanner::ConvexVertex(point2 prev, point2 current, point2 next)
{
  point2 v1 = next - current;
  point2 v2 = current - prev;

  float vertexAngle = FindTheta(v1,v2);
  if (vertexAngle >= 0.0)
    {return true;}
  else
    {return false;};
};


// ------------------------------------------------------------------------------------
// check if the line segment with endpoints p1 and p, intersects a circle whose center is at circleCenter 
// and whose radius is Radius.  FIrst we check if the underlying line has an intersection with the circle, and 
// if so, then check if the line segment intersects the circle
// HACK: not sure if this function really works.  It hasn't been tested yet.
bool CirclePlanner::SegmentCircleIntersection(point2 p1, point2 p2, point2 circleCenter, double R)
{
  bool intersect;
  point2 x1 = p1 - circleCenter;
  point2 x2 = p2 - circleCenter;

  point2 dx = x2 - x1;
  double dr_square = dx.x * dx.x + dx.y * dx.y;
  double D = x1.x * x2.y - x1.y * x2.x;
  double Delta = dr_square * R * R - D * D ;

  if (Delta < 0.0)                          // no intersection possible with the line
    {
      intersect = false;
    }
  else
    {
      if (Delta == 0.0)                     // rare case that line might intersect in a point
	{
	  // check if the one possible intersection point is in the segment
	  point2 p_int = point2((D * dx.y/ dr_square),( - D * dx.x/dr_square));
	  intersect = PointInSegment(x1,x2,p_int);
	}
      else                                  // two possible intersections.  Check if they're in segment
	{
	  point2 p_mean = point2((D * dx.y/ dr_square),( - D * dx.x/dr_square));
	  double signum;
	  if (dx.y<0.0)
	    {signum = -1.0;}
	  else
	    {signum = 1.0;};
	  double var1 = sqrt(Delta/dr_square);
	  point2 p_delta= point2((signum * dx.x * var1), (fabs(dx.y) * var1));
	  point2 p_int1 = p_mean + p_delta; point2 p_int2 = p_mean - p_delta;
	  if ( (PointInSegment(x1,x2,p_int1)) || (PointInSegment(x1,x2,p_int2)) )
	    {intersect = true; }
	  else
	    {intersect = false;};
	};
    };
  return intersect;
};

/* --------------------------------------------------------------------------------------------------- */
/*                      functions that aid in checking feasibility of paths                            */
/* --------------------------------------------------------------------------------------------------- */

// function that takes a hypothetical pose for Alice and returns a pointer to the (right handed)
// polygon that safely bounds Alice at that pose.

point2arr CirclePlanner::AliceBoundingBox(pose2 alicePose)
{
  point2 point;
  point2arr aliceBox=point2arr();

  double offsetFront = DIST_REAR_AXLE_TO_FRONT + ALICE_SAFETY_FRONT;
  double offsetRear = -(DIST_REAR_TO_REAR_AXLE + ALICE_SAFETY_REAR);
  double offsetSide = VEHICLE_WIDTH + ALICE_SAFETY_SIDE;
  double c_heading = cos(alicePose.ang);   double s_heading = sin(alicePose.ang);

  // Front left
  point.set(alicePose.x + (offsetFront)*c_heading-(offsetSide)*s_heading,
	    alicePose.y + (offsetFront)*s_heading+(offsetSide)*c_heading);
  aliceBox.push_back(point);

  // Front right
  point.set(alicePose.x + (offsetFront)*c_heading-(-offsetSide)*s_heading, 
	    alicePose.y + (offsetFront)*s_heading+(-offsetSide)*c_heading);
  aliceBox.push_back(point);

  // Rear right
  point.set(alicePose.x + (offsetRear)*c_heading-(-offsetSide)*s_heading, 
	    alicePose.y + (offsetRear)*s_heading+(-offsetSide)*c_heading);
  aliceBox.push_back(point);

  // Rear left
  point.set(alicePose.x + (offsetRear)*c_heading-(offsetSide)*s_heading, 
	    alicePose.y + (offsetRear)*s_heading+(offsetSide)*c_heading);
  aliceBox.push_back(point);

  // Front left again to close the polygon - not sure why this is necessary
  point.set(alicePose.x + (offsetFront)*c_heading-(offsetSide)*s_heading, 
	    alicePose.y + (offsetFront)*s_heading+(offsetSide)*c_heading);
  aliceBox.push_back(point);
  
  return aliceBox;
};

//----------------------------------------------------------------------------------------------------------
// check if all points in a clothoid (referred by cPtr) are legal.  This checks if Alice's potential pose at
// each clothoid Pose is legal with respect to the bounding zone polygon.  If one pose intersects the zone boundary,
// then the entire clothoid is considered illegal

bool CirclePlanner::IsClothoidLegal(Clothoid * cPtr)
{
  bool testIntersection;
  //iterate though all points of the Clothoid
  for (vector<pose2>::iterator i=cPtr->m_clothoidPoints.begin(); i!=cPtr->m_clothoidPoints.end(); i++)
    {
      point2arr curAliceBoundingBox = AliceBoundingBox((*i));
      testIntersection = this->m_rightHandedZoneBoundary.is_intersect(curAliceBoundingBox);
      if (testIntersection==true)
	{ return false;	};
    };
  return true;
};


//----------------------------------------------------------------------------------------------------------
// eliminate (from the set of paths which eminate from this given Path Node) any edges that are illegal.  Then with the
// remaining edges, recursively call this function on the edges' distal nodes.
void CirclePlanner::CullNodeOutEdges(PTNode * nodePtr)
{
  bool clothoidTest;
  // first check if node has any out edges.  If not, there's nothing to do
  if (!nodePtr->m_outEdges.empty())
    {
      // eliminate any outEdges that are illegal (alice intersects the boundary)
      for (vector<PTEdge *>::iterator i=nodePtr->m_outEdges.begin(); i!=nodePtr->m_outEdges.end(); i++)
	{
	  clothoidTest = IsClothoidLegal((*i)->m_clothoid);
	  if( clothoidTest == false)
	    {
	      nodePtr->m_outEdges.erase(i);
	    };
	};
      // now, cull the edges going out of the remaining child nodes.
      for (vector<PTEdge *>::iterator i=nodePtr->m_outEdges.begin(); i!=nodePtr->m_outEdges.end(); i++)
	{
	  CullNodeOutEdges((*i)->m_childPTNodePtr);
	};
    };
  return;
};


