/* 
 * ClothoidParams.hh: list of parameters used to define the properties of the 
 * trajectories used by Circle Planner
 *
 * Based on Code written by Kenny Osland, modified by Joel Burdick
 *
 */

/* ---------------------------------------------------------------------------------------- */
/*                   Constants and Definitions related to Clothoids                         */
/* ---------------------------------------------------------------------------------------- */

// distance between points in a trajectory (measured in meters) for Circle Planner
#define CP_PATH_SECTION_LENGTH			0.5

// maximum curvature for a path that alice can physically drive
// #define PATH_MAX_DYN_FEASIBLE_CURVATURE		(1/VEHICLE_MIN_TURNING_RADIUS)

// the turning radius used for planning, which should be slightly over Alice's minimum turning radius
#define TURNING_RADIUS (VEHICLE_MIN_TURNING_RADIUS + 0.3)
