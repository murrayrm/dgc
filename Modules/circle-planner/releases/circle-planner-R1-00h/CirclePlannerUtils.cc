/* CirclePlannerUtils.cc: basic utility functions that get used by CirclePlanner
 *
 * Written by Joel Burdick (Sept. 26, 2007)
 */

/* ----------------------------------------------------------------------------------- */
/*                                     includes                                        */
/* ----------------------------------------------------------------------------------- */

#include <alice/AliceConstants.h>
#include "CirclePlanner.hh"

/* ----------------------------------------------------------------------------------- */
/*                                 transformations                                     */
/* ----------------------------------------------------------------------------------- */

// transforms a point2arr in left handed Northing-Easting coordinates to right handed representation
point2arr CirclePlanner::LeftToRightPointArray(point2arr leftArray)
{
  point2arr rightArray; //allocate storage for new right handed array

  // iterate through all of the point2 elements of leftArray
  for(vector<point2>::iterator j=leftArray.arr.begin(); j!=leftArray.arr.end(); j++)
    {
      // transform from left handed representation to right handed representation
      point2 rightPoint = point2((*j).y, (*j).x);
      rightArray.arr.push_back(rightPoint);
    };
  return rightArray;
};


/* ----------------------------------------------------------------------------------- */
/*                                geometric functions                                  */
/* ----------------------------------------------------------------------------------- */


// check if the point p lies in the circle with center circleCenter and radius R.  Returns a boolean 
bool CirclePlanner::PointInCircle(point2 p, point2 circleCenter, double R)
{
  double dx = p.x - circleCenter.x;  double dy = p.y - circleCenter.y;
  if ((dx * dx + dy * dy) <= (R * R))
    {return true; }
  else
    {return false;};
};

// ------------------------------------------------------------------------------------
// check if the point p lies in the line segment bounded by x1 and x2.  Returns a boolean
bool CirclePlanner::PointInSegment(point2 x1, point2 x2, point2 p)
{
  bool point_lies_in_segment;

  point2 term1 = p - x1;   point2 term2 = x2 - x1;   point2 term3 = p - x2;

  if ((Dot2D(term1,term2)>0.0) && (Dot2D(term3,term2) < 0.0))
    {
      point_lies_in_segment = true;
    }
  else
    {
      point_lies_in_segment = false;
    };
  return point_lies_in_segment;
};


// ------------------------------------------------------------------------------------
// check if the vertex "current" is convex relative to its neighbors "previous" and "next".  This
// calculation assumes that the vertex indices are increasing in a clockwise fashion.

bool CirclePlanner::ConvexVertex(point2 prev, point2 current, point2 next)
{
  point2 v1 = next - current;
  point2 v2 = current - prev;

  float vertexAngle = FindTheta(v1,v2);
  if (vertexAngle >= 0.0)
    {return true;}
  else
    {return false;};
};


// ------------------------------------------------------------------------------------
// check if the line segment with endpoints p1 and p, intersects a circle whose center is at circleCenter 
// and whose radius is Radius.  FIrst we check if the underlying line has an intersection with the circle, and 
// if so, then check if the line segment intersects the circle
// HACK: not sure if this function really works.  It hasn't been tested yet.
bool CirclePlanner::SegmentCircleIntersection(point2 p1, point2 p2, point2 circleCenter, double R)
{
  bool intersect;
  point2 x1 = p1 - circleCenter;
  point2 x2 = p2 - circleCenter;

  point2 dx = x2 - x1;
  double dr_square = dx.x * dx.x + dx.y * dx.y;
  double D = x1.x * x2.y - x1.y * x2.x;
  double Delta = dr_square * R * R - D * D ;

  if (Delta < 0.0)                          // no intersection possible with the line
    {
      intersect = false;
    }
  else
    {
      if (Delta == 0.0)                     // rare case that line might intersect in a point
	{
	  // check if the one possible intersection point is in the segment
	  point2 p_int = point2((D * dx.y/ dr_square),( - D * dx.x/dr_square));
	  intersect = PointInSegment(x1,x2,p_int);
	}
      else                                  // two possible intersections.  Check if they're in segment
	{
	  point2 p_mean = point2((D * dx.y/ dr_square),( - D * dx.x/dr_square));
	  double signum;
	  if (dx.y<0.0)
	    {signum = -1.0;}
	  else
	    {signum = 1.0;};
	  double var1 = sqrt(Delta/dr_square);
	  point2 p_delta= point2((signum * dx.x * var1), (fabs(dx.y) * var1));
	  point2 p_int1 = p_mean + p_delta; point2 p_int2 = p_mean - p_delta;
	  if ( (PointInSegment(x1,x2,p_int1)) || (PointInSegment(x1,x2,p_int2)) )
	    {intersect = true; }
	  else
	    {intersect = false;};
	};
    };
  return intersect;
};

/* --------------------------------------------------------------------------------------------------- */
/*                                printing and visualization functions                                 */
/* --------------------------------------------------------------------------------------------------- */

// print the contents of a point2arr.  node, an extra zero (corresponding to the position of an 
// orientation is printed in order to be consistent with other debugging strategies)
void CirclePlanner::printPoint2arr(point2arr parray, ofstream *f)
{
  for (vector<point2>::iterator i=parray.arr.begin(); i!=parray.arr.end(); i++)
    { *f << (*i).x << " " << (*i).y << " 0.0 " <<  endl;  };
};

// print the contents of a point2arr.  node, an extra zero (corresponding to the position of an 
// orientation is printed in order to be consistent with other debugging strategies)
void CirclePlanner::printPoint2arr(point2arr parray, ostream *f)
{
  for (vector<point2>::iterator i=parray.arr.begin(); i!=parray.arr.end(); i++)
    { *f << (*i).x << " " << (*i).y << " 0.0 " <<  endl;  };
};

// print Alice Box with many points for clear visibility
void CirclePlanner::printDenseAliceBox(pose2 alicePose, ofstream *f)
{
  point2arr aliceBox = AliceBoundingBox(alicePose);

  int npoints=15;

  vector<point2>::iterator j;
  point2 dx;
  for(vector<point2>::iterator i= aliceBox.arr.begin(); i!= aliceBox.arr.end(); i++)
    {
      j=i; j++;
      if(j< aliceBox.arr.end())
	{ dx = (*j)-(*i);}
      else
	{  dx = (* aliceBox.arr.begin()) - (*i); };
      int iter;
      for(iter=0;iter<npoints;iter++)
	{
	  point2 printPoint = (*i) + (((double) iter) /( (double) npoints)) * dx;
	  *f << printPoint.x << " " << printPoint.y << " 0.0 " << endl;
	};
    };
};


// print the corners of Alice's bounding box for each pose along the clothoid path referenced by cPtr
void CirclePlanner::printAliceBoxPath(Clothoid * cPtr, ofstream *f)
{
  for (vector<pose2>::iterator i=cPtr->m_clothoidPoints.begin(); i!=cPtr->m_clothoidPoints.end(); i++)
    {
      point2arr curAliceBoundingBox = AliceBoundingBox((*i));
      printPoint2arr(curAliceBoundingBox,f); 
    };
};

// plot the boundaries of a zone for debugging purposes
void CirclePlanner::printZoneBoundary(ofstream *f)
{
  int npoints=20;

  vector<point2>::iterator j;
  point2 dx;
  for(vector<point2>::iterator i=this->m_rightHandedZoneBoundary.arr.begin();
      i!=this->m_rightHandedZoneBoundary.arr.end(); i++)
    {
      j=i; j++;
      if(j<this->m_rightHandedZoneBoundary.arr.end())
	{ dx = (*j)-(*i);}
      else
	{  dx = (* this->m_rightHandedZoneBoundary.arr.begin()) - (*i);	};
      int iter;
      for(iter=0;iter<npoints;iter++)
	{
	  point2 printPoint = (*i) + (((double) iter) /( (double) npoints)) * dx;
	  *f << printPoint.x << " " << printPoint.y << " 0.0 " << endl;
	};
    };
};

void CirclePlanner::LogZoneBoundary()
{
  Log::getStream(5) << "CIRCLEPLANNER:: RH Zone Boundary Points are:" << endl;

  vector<point2>::iterator j;
  for(vector<point2>::iterator i=this->m_rightHandedZoneBoundary.arr.begin();
      i!=this->m_rightHandedZoneBoundary.arr.end(); i++)
    {
      point2 printPoint = (*i);
      Log::getStream(5) << "x= " << printPoint.x << ", y= " << printPoint.y << endl;
    };
};

/* --------------------------------------------------------------------------------------------------- */
/*                      functions that aid in checking feasibility of paths                            */
/* --------------------------------------------------------------------------------------------------- */

// function that takes a hypothetical pose for Alice and returns a pointer to the (right handed)
// polygon that safely bounds Alice at that pose.  This function uses fixed parameters to define
// the safety box.  Functions below use CSpecs to get relevant safety perimeter information
point2arr CirclePlanner::AliceBoundingBox(pose2 alicePose)
{
  point2 point1, point2, point3, point4;
  point2arr aliceBox = point2arr();

  double offsetFront = DIST_REAR_AXLE_TO_FRONT + ALICE_SAFETY_FRONT;
  double offsetRear = -(DIST_REAR_TO_REAR_AXLE + ALICE_SAFETY_REAR);
  double offsetSide = VEHICLE_WIDTH + ALICE_SAFETY_SIDE;
  double c_heading = cos(alicePose.ang);   double s_heading = sin(alicePose.ang);

  // intermediate calculations to save on the operation count
  double term1 = offsetFront * c_heading; double term2 = offsetSide * s_heading;
  double term3 = offsetFront * s_heading; double term4 = offsetSide * c_heading;

  double term5 = offsetRear * c_heading;  double term6 = offsetRear * s_heading; 
  // Front left
  point1.set(alicePose.x + term1 - term2, alicePose.y + term3 + term4);
  aliceBox.arr.push_back(point1);

  // Front right
  point2.set(alicePose.x + term1 + term2, alicePose.y + term3 - term4);
  aliceBox.arr.push_back(point2);

  // Rear right
  point3.set(alicePose.x + term5 + term2, alicePose.y + term6 - term4);
  aliceBox.arr.push_back(point3);

  // Rear left
  point4.set(alicePose.x + term5 - term2, alicePose.y + term6 + term4);
  aliceBox.arr.push_back(point4);

  // Front left again to close the polygon - not sure why this is necessary
  /*  point1.set(alicePose.x + term1 -term2, alicePose.y + term3 + term4); */
  aliceBox.arr.push_back(point1);
  
  return aliceBox;
};

// another version of the AliceBoundingBox function that uses the CSpecs perimeter safety margins
// information to compute the size of the bounding box.  Note that this calculation is done
// in a right handed coordinate system.
// HACK: note that the right and left side safety buffers are assumed to the be the same (and
// governed by the rigth side buffer size from CSpecs.  This is done to slightly speed the
// calculations.

point2arr CirclePlanner::AliceBoundingBox(pose2 alicePose, vector<double> safetyMargins)
{
  point2 point1, point2, point3, point4;
  point2arr aliceBox = point2arr();

  double offsetFront = DIST_REAR_AXLE_TO_FRONT + safetyMargins.at(SAFETY_FRONT);
  double offsetRear = -(DIST_REAR_TO_REAR_AXLE + safetyMargins.at(SAFETY_BACK) );
  double offsetSide =  0.5 * (VEHICLE_WIDTH + safetyMargins.at(SAFETY_LEFT) + safetyMargins.at(SAFETY_RIGHT) );
  double c_heading = cos(alicePose.ang);   double s_heading = sin(alicePose.ang);

  // intermediate calculations to save on the operation count
  double term1 = offsetFront * c_heading; double term2 = offsetSide * s_heading;
  double term3 = offsetFront * s_heading; double term4 = offsetSide * c_heading;

  double term5 = offsetRear * c_heading;  double term6 = offsetRear * s_heading; 
  // Front left
  point1.set(alicePose.x + term1 - term2, alicePose.y + term3 + term4);
  aliceBox.arr.push_back(point1);

  // Front right
  point2.set(alicePose.x + term1 + term2, alicePose.y + term3 - term4);
  aliceBox.arr.push_back(point2);

  // Rear right
  point3.set(alicePose.x + term5 + term2, alicePose.y + term6 - term4);
  aliceBox.arr.push_back(point3);

  // Rear left
  point4.set(alicePose.x + term5 - term2, alicePose.y + term6 + term4);
  aliceBox.arr.push_back(point4);

  // Front left again to close the polygon - not sure why this is necessary
  /*  point1.set(alicePose.x + term1 -term2, alicePose.y + term3 + term4); */
  aliceBox.arr.push_back(point1);
  
  return aliceBox;
};

// this is yet another version of a function to create a polygon that is equal to Alice's bounding box
// plus safety perimeters.  This one grows Alice by an additional factor (specified by the double 
// "buffer").  This function is used by AdjustZone
point2arr CirclePlanner::AliceZoneGrowBox(pose2 alicePose, vector<double> safetyMargins, double buffer)
{
  point2 point1, point2, point3, point4;
  point2arr aliceBox = point2arr();

  double offsetFront = DIST_REAR_AXLE_TO_FRONT + safetyMargins.at(SAFETY_FRONT) + buffer;
  double offsetRear = -(DIST_REAR_TO_REAR_AXLE + safetyMargins.at(SAFETY_BACK)+ buffer);
  double offsetSide =  0.5 * (VEHICLE_WIDTH + safetyMargins.at(SAFETY_LEFT) + safetyMargins.at(SAFETY_RIGHT) ) + buffer;
  double c_heading = cos(alicePose.ang);   double s_heading = sin(alicePose.ang);

  // intermediate calculations to save on the operation count
  double term1 = offsetFront * c_heading; double term2 = offsetSide * s_heading;
  double term3 = offsetFront * s_heading; double term4 = offsetSide * c_heading;

  double term5 = offsetRear * c_heading;  double term6 = offsetRear * s_heading; 
  // Front left
  point1.set(alicePose.x + term1 - term2, alicePose.y + term3 + term4);
  aliceBox.arr.push_back(point1);

  // Front right
  point2.set(alicePose.x + term1 + term2, alicePose.y + term3 - term4);
  aliceBox.arr.push_back(point2);

  // Rear right
  point3.set(alicePose.x + term5 + term2, alicePose.y + term6 - term4);
  aliceBox.arr.push_back(point3);

  // Rear left
  point4.set(alicePose.x + term5 - term2, alicePose.y + term6 + term4);
  aliceBox.arr.push_back(point4);

  // Front left again to close the polygon - not sure why this is necessary
  /*  point1.set(alicePose.x + term1 -term2, alicePose.y + term3 + term4); */
  aliceBox.arr.push_back(point1);
  
  return aliceBox;
};

// check to see if a given pose of Alice (defined by posePoint) is legal with respect to the zone boundary.
// this function is mainly used by the AdjustZone function.  Right now this only checks for the zone boundary.
// HACK: needed to update this to include obstacles.

bool CirclePlanner::IsPoseLegal(pose2 curPose, vector<double> safetyMargins, double buffer)
{
  bool testIntersection;

  point2arr curAliceBoundingBox = AliceZoneGrowBox(curPose, safetyMargins, buffer);
  testIntersection = this->m_rightHandedZoneBoundary.is_intersect(curAliceBoundingBox);
  if (testIntersection==true)
    { return false; }
  else
    { return true; };
};


//----------------------------------------------------------------------------------------------------------
// check if all points in a clothoid (referred by cPtr) are legal.  This checks if Alice's potential pose at
// each clothoid Pose is legal with respect to the bounding zone polygon.  If one pose intersects the zone boundary,
// then the entire clothoid is considered illegal.
// HACK: right now we check every point in the clothoid.  Perhaps we can subsample poses to get a more efficient algorithm?
// HACK: right now this check uses the simplest version of the Alice Bounding Box

bool CirclePlanner::IsPathLegal(Clothoid * cPtr)
{
  bool testIntersection;

  //iterate though all points of the Clothoid to check for overlap of Alice and zone boundary
  for (vector<pose2>::iterator i=cPtr->m_clothoidPoints.begin(); i!=cPtr->m_clothoidPoints.end(); i++)
    {
      point2arr curAliceBoundingBox = AliceBoundingBox((*i));
      testIntersection = this->m_rightHandedZoneBoundary.is_intersect(curAliceBoundingBox);
      if (testIntersection==true)
	{ 
	  return false;	
	};
    };
  return true;
};


#if CIRCLE_PLANNER_DEBUG
// the alternate version of above for debugging purposes
bool CirclePlanner::IsPathLegal(Clothoid * cPtr, ofstream *f)
{
  bool testIntersection;

  //iterate though all points of the Clothoid to check for overlap of Alice and zone boundary
  for (vector<pose2>::iterator i=cPtr->m_clothoidPoints.begin(); i!=cPtr->m_clothoidPoints.end(); i++)
    {
      point2arr curAliceBoundingBox = AliceBoundingBox((*i));
      /*      printPoint2arr(curAliceBoundingBox,f);  */
      testIntersection = this->m_rightHandedZoneBoundary.is_intersect(curAliceBoundingBox);
      if (testIntersection==true)
	{ 
#if CIRCLE_PLANNER_DEBUG
	  cerr << "     IsPathLegal: Clothoid " << cPtr->m_clothoidIndex << " is not feasible " << endl;
	  this->printDenseAliceBox((*i), f); 
#endif
	  return false;	
	};
    };
#if CIRCLE_PLANNER_DEBUG
  cerr << "     IsPathLegal: Clothoid " << cPtr->m_clothoidIndex << " is feasible " << endl;
#endif
  return true;
};
#endif

// assuming that alice is at aliceLocation and that at least some of her is sticking into the
// corridor, grow the corridor by unioning an area around Alice (with some extra buffer as 
// well) with the existing corridor. This is to ensure that alice's location is legal. note,
// this calcuation is done in Right Handed coordinate system, and will replace CirclePlanner's
// copy of zone boundary with the new adjusted one.
void CirclePlanner::GrowZoneBoundary(pose2 aliceLocation, CSpecs_t *cSpecs, double buffer)
{
  // grow alice to make sure there is a buffer around her
  vector<double> safetyMargins = cSpecs->getPerimeterSafetyMargins();
  point2arr grownAlice = AliceZoneGrowBox(aliceLocation, safetyMargins, buffer);

  vector<point2arr> grownRegion;                               // empty variable which gets set by get_poly_union below
  int numPolygons = this->m_rightHandedZoneBoundary.get_poly_union(grownAlice, grownRegion);
  if (numPolygons == 1)
    { this->m_rightHandedZoneBoundary = grownRegion.front();}
  else
    {
      cout << " GROWZONEBOUNDARY ERROR " << endl;
      Log::getStream(1) << "CIRCLEPLANNER: ERROR:  Alice out of corridor,growZoneBoundary cannot fix this " << endl;
      return;
    };
};

// This function is required because Planner only ensures that Alice's rear axle is inside
// a zone boundary when it passes control over to Circle-Planner.  However, Alice's rear
// bumper may be sticking out of the zone corridor, which would formally cause all of 
// CirclePlanner's paths to be illegal, since Alice's starting pose is illegal in this case.  
// This HACK function adjusts the zone boundary around Alice's initial and final poses to 
// guarantee that they are legally inside the new adjusted zone boundary

void CirclePlanner::AdjustZone(CSpecs_t * cSpecs)
{
  vector<double> safetyMargins = cSpecs->getPerimeterSafetyMargins();

  // if starting pose isn't legal, expand the zone near start pose
  if ( !IsPoseLegal(this->m_startPose,safetyMargins, START_GROW_ALICE) )  
    {
      Log::getStream(9) << "CLOTHOIDPLANNER: growing corridor for entry pose" << endl;
      this->GrowZoneBoundary(this->m_startPose,cSpecs,START_GROW_ALICE);
    };
  if ( !IsPoseLegal(this->m_goalPose, safetyMargins, GOAL_GROW_ALICE) )
    { 
      Log::getStream(9) << "CLOTHOIDPLANNER: growing corridor for goal pose" << endl;
      this->GrowZoneBoundary(this->m_goalPose,cSpecs,GOAL_GROW_ALICE);
    };
};


/* ------------------------------------------------------------------------------------------------- */
// this function "culls" infeasible path segments from the path tree
/* ------------------------------------------------------------------------------------------------- */

void CirclePlanner::CullNodeOutEdges(PTNode * nodePtr)
{
  if(nodePtr->m_outEdges.empty())              // if node has no outEdges, there's nothing to do
    {
      nodePtr->m_hasNodeBeenCulled = true;
      return;
    }
  else
    { // first check to see if this node has already been visited and culled.  If not, proceed.  If so, return
      if (nodePtr->m_hasNodeBeenCulled == false)
	{ 	 
	  // Now eliminate infeasible edges.  We can't immediately erase them from the outEdges list because 
          // we're iterating on that list.  So, we'll save the feasible edges to reconstruct outEdges at the end
	  vector<PTEdge *> feasibleEdgeList;                    
	  feasibleEdgeList.clear();

	  for (vector<PTEdge *>::iterator i=nodePtr->m_outEdges.begin(); i!=nodePtr->m_outEdges.end(); i++)
	    {
	      PTNode * childPtr = (*i)->m_childPTNodePtr;       // distal node of the present edge i

	      // before trying to cull an edge, first cull the outEdges of this edge's distal node
	      if (!childPtr->m_outEdges.empty())
		{   CullNodeOutEdges(childPtr);  };

	      if( IsPathLegal((*i)->m_clothoid) == true)      // if edge is feasible, push it on the feasible list.
		{  feasibleEdgeList.push_back((*i)); };
	    };
	  nodePtr->m_hasNodeBeenCulled = true;

	  nodePtr->m_outEdges.clear();                         // now reattach all of the feasible outEdges to the node
	  for (vector<PTEdge *>::iterator j=feasibleEdgeList.begin(); j!=feasibleEdgeList.end(); j++)
	    { nodePtr->m_outEdges.push_back((*j)); };
	}
      else
	{return; };
    };
};


#if CIRCLE_PLANNER_DEBUG
// the alternate version of CullNodeOutEdges for debugging
void CirclePlanner::CullNodeOutEdges(PTNode * nodePtr, ofstream *f)
{
  if(nodePtr->m_outEdges.empty())              // if node has no outEdges, there's nothing to do
    {
      nodePtr->m_hasNodeBeenCulled = true;
      return;
    }
  else
    { // first check to see if this node has already been visited and culled.  If not, proceed.  If so, return
      if (nodePtr->m_hasNodeBeenCulled == false)
	{ 	 
	  // Now we'll eliminate the infeasible edges.  We can't erase them from the list of outEdges immediately,
	  // because we're iterating on that list.  So, we'll save up the list of feasible edges, and reconstruct
	  // them at the end.
	  vector<PTEdge *> feasibleEdgeList;                    
	  feasibleEdgeList.clear();

	  for (vector<PTEdge *>::iterator i=nodePtr->m_outEdges.begin(); i!=nodePtr->m_outEdges.end(); i++)
	    {
	      PTNode * childPtr = (*i)->m_childPTNodePtr;       // distal node of the present edge i

	      // before trying to cull an edge, first cull the outEdges of this edge's distal node
	      if (!childPtr->m_outEdges.empty())
		{   CullNodeOutEdges(childPtr,f);  };

	      if( IsPathLegal((*i)->m_clothoid,f) == true)      // if edge is feasible, push it on the feasible list.
		{
		  feasibleEdgeList.push_back((*i)); 
		  cerr << "      CullEdges:  keeping Edge " << (*i)->m_clothoid->m_clothoidIndex << endl; 
		}
	      else
		{  cerr << "      CullEdges:  will erase Edge " << (*i)->m_clothoid->m_clothoidIndex << endl;  };
	    };
	  nodePtr->m_hasNodeBeenCulled = true;

	  nodePtr->m_outEdges.clear();	          // now reattach all of the feasible outEdges to the node
	  for (vector<PTEdge *>::iterator j=feasibleEdgeList.begin(); j!=feasibleEdgeList.end(); j++)
	    { nodePtr->m_outEdges.push_back((*j)); };
	}
      else
	{return; };
    };
};
#endif
