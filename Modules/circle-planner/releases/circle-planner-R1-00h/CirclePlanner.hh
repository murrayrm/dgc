/*
 * CirclePlanner.hh: Define the Circle Planner Class, which is a zone planner.
 * 
 * Written by Joel Burdick.starting Sept 10, 2007
 */

/* ---------------------------------------------------------------------------------------- */
/*                                         Includes                                         */
/* ---------------------------------------------------------------------------------------- */

#include <iostream>
#include <stdio.h>

#include <algorithm>     // need this to sue stl heap functions
#include <set>
#include <vector>

#include <frames/point2.hh>
#include <frames/pose2.hh>

#include <temp-planner-interfaces/GraphPath.hh>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <cspecs/CSpecs.hh>
#include <frames/quat.h>

#include <temp-planner-interfaces/Console.hh>
#include <temp-planner-interfaces/Log.hh>

#include "PathTree.hh"

/* ---------------------------------------------------------------------------------------- */
/*                                        Defines                                           */
/* ---------------------------------------------------------------------------------------- */
#ifndef CIRCLEPLANNER_HH
#define CIRCLEPLANNER_HH

using namespace std;

/* ---------------------------------------------------------------------------------------- */
/*                                 The Circle Planner class                                 */
/* ---------------------------------------------------------------------------------------- */

class CirclePlanner
{
private:                                           // data structures needed to "densify" circle plan graph

  class IncomingBiTangent   // class that contains information about all of a cirlce's incoming bitangents
  {
  public:
    bool m_reverse;
    PTEdge * m_incomingEdgePtr;
    PTNode * m_incomingNodePtr;

    //simple constructor for the Incoming BiTangent data structure
    IncomingBiTangent(PTNode * nodePtr, PTEdge * edgePtr, bool reverse)
    {
      m_incomingNodePtr= nodePtr;
      m_incomingEdgePtr = edgePtr;
      m_reverse = reverse;
    };
  };

  class OutgoingBiTangent    // class that contains information about all of a circle's  outoing bitangents
  {
  public:
    bool m_reverse;
    PTNode * m_outgoingNodePtr;
    
    //simple constructor
    OutgoingBiTangent(PTNode * nodePtr, bool reverse)
    {
      m_outgoingNodePtr = nodePtr;
      m_reverse = reverse;
    };
  };

  // class to hold data that is used for splicing together various circles in Circle-Planner
  class CircleData
  {
  public:
    double m_Radius;                                       // the radius of this cicle
    point2 m_center;                                       // the circle center
    PTNode * m_circleNodePtr;
    unsigned int m_handedness;                             // right, left, or ambidextrous
    
    vector<IncomingBiTangent *> In;                        // a list of the oncoming BiTangents
    vector<OutgoingBiTangent *> Out;                       // a list of the oncoming BiTangents
    
    // minimal constructor that sets up data in the CircleData container
    CircleData(PTNode * circleNodePtr, point2 circleCenter, double circleRadius, unsigned int handedness)
    {
      m_Radius = circleRadius; 
      m_center = circleCenter; 
      m_circleNodePtr = circleNodePtr;
      m_handedness = handedness; 
      In.clear();  Out.clear();
    };

    // alternative constructor that sets up data in the CircleData container
    /*
    CircleData(pose2 circlePose, point2 circleCenter, double circleRadius, unsigned int handedness)
    {
      m_Radius = circleRadius; 
      m_center = circleCenter; 
      m_circleNodePtr = new PTNode(circlePose);
      m_handedness = handedness; 
      In.clear();  Out.clear();
    };
    */
  };

public:                                    
  enum                            // the different kinds of circle directions, handedness, and handedness combinations
    {
      FORWARD,                    // Alice's direction along a path segment
      REVERSE,
      LEFT_HAND_CIRCLE,           // two basic types of handedness we must deal with         
      RIGHT_HAND_CIRCLE,   
      OMNI_HAND_CIIRCLE,
      RIGHT_HAND_START_CIRCLE,    // four types of start and goal circles
      LEFT_HAND_START_CIRCLE,
      RIGHT_HAND_GOAL_CIRCLE,
      LEFT_HAND_GOAL_CIRCLE,    
      RIGHT_RIGHT_COMBO,          // different types of start and goal handedness combinations
      RIGHT_LEFT_COMBO,
      RIGHT_OMNI_COMBO,
      LEFT_RIGHT_COMBO,
      LEFT_LEFT_COMBO,
      LEFT_OMNI_COMBO,
      OMNI_RIGHT_COMBO,
      OMNI_LEFT_COMBO,
      OMNI_OMNI_COMBO
    };

  // containers for basic data needed by Circle Planner to do its job
  pose2 m_startPose;
  PTNode * m_startNodePtr;             // pointer to the start, or base, node of the graph of path nodes and edges   
  CircleData * m_rightStartCircle;     // data related to the right starting circle
  CircleData * m_leftStartCircle;      // data related to the left starting circle

  pose2 m_goalPose;
  PTNode * m_goalNodePtr;              // pointer to goal node
  CircleData * m_rightGoalCircle;      // data related to the right starting circle
  CircleData * m_leftGoalCircle;       // data related to the left starting circle

  point2arr m_rightHandedZoneBoundary; // a copy of the CSpecs boundary information, in a right handed coordinate frame
  vector<CSpecsObstacle> m_rightHandedObstacles;    //

  vector<Clothoid *> m_solutionPath;   // sequence of clothoids (in reverse order) that will define the solution.
  vector<Clothoid *> m_forwardPath;    // the reverse (properly ordered) solution path

  unsigned int m_nClothoids;           // keep track of the number of Clothoids produced.(for debugging and indexing)

private:                               // internal functions and structures needed by CirclePlanner to compute its plans

  float Cross2D(point2 V1,point2 V2);      // simple 2-dimensional vector operations
  float Dot2D(point2 V1, point2 V2);
  float FindTheta(point2 V1, point2 V2);   // Find the angle between two unit vectors on a 
  double unwrap_angle(double inAngle);     // simple function to put angles in (-PI, PI)

  // private utility functions from CirclePlannerUtils.hh
  bool PointInCircle(point2 p, point2 circleCenter, double R);   // is point p in circle?
  bool PointInSegment(point2 x1, point2 x2, point2 p);           // is p in the segment[x1,x2]?
  bool ConvexVertex(point2 prev, point2 current, point2 next);   // is current vertex convex?
  bool SegmentCircleIntersection(point2 p1, point2 p2, point2 circleCenter, double R);
  void printPoint2arr(point2arr parray, ofstream *f);    // print a point2arr
  void printPoint2arr(point2arr parray, ostream *f);     // print a point2arr, different output file type
  void printZoneBoundary(ofstream *f);
  void printDenseAliceBox(pose2 alicePose, ofstream *f);
  void printAliceBoxPath(Clothoid * cPtr, ofstream *f);
  void LogZoneBoundary();

public:                                                        // main planning functions

  /*  CirclePlanner(pose2 startPose, pose2 goalPose);       */       // constructor
  CirclePlanner(pose2 startPose, pose2 goalPose, double RADIUS);              // alternate constructor
  ~CirclePlanner();                                            // destructor

  // makes straight and circular path segments needed to construct plans
  PTEdge * makeStraightLine(point2 startPoint, point2 endPoint, bool reverse);
  PTEdge * makeForwardCircularArc(point2 startCenter, float startAngle, float stopAngle, unsigned int handedness, double RADIUS);
  PTEdge * makeReverseCircularArc(point2 startCenter, float startAngle, float stopAngle, unsigned int handedness, double RADIUS);

  // public utility functions from CirclePlannerUtils.cc
  point2arr LeftToRightPointArray(point2arr leftArray);
  point2arr AliceBoundingBox(pose2 alicePose);
  point2arr AliceBoundingBox(pose2 alicePose, vector<double> safetyMargins);   // another version of bounding box
  point2arr AliceZoneGrowBox(pose2 alicePose, vector<double> safetyMargins, double buffer);

  bool IsPoseLegal(pose2 curPose, vector<double> safetyMargins, double buffer);
  bool IsPathLegal(Clothoid * cPtr);
  bool IsPathLegal(Clothoid * cPtr, ofstream *f);              // the alternative version for debugging
  bool ExpansiveIsPathLegal(PTEdge *edgePtr, int depth);
  bool ExpansiveIsPathLegal(PTEdge *edgePtr, ofstream *f, int depth);  // the alternative version for debugging
  void GrowZoneBoundary(pose2 aliceLocation, CSpecs_t * cSpecs, double buffer);
  void AdjustZone(CSpecs_t * cSpecs);
  void CullNodeOutEdges(PTNode * nodePtr);
  void CullNodeOutEdges(PTNode * nodePtr, ofstream *f);        // the alternative version for debugging
  void CullAndExpandNodeOutEdges(PTNode * nodePtr, int depth);
  void CullAndExpandNodeOutEdges(PTNode * nodePtr, ofstream *f, int depth);  // the alternative version for debugging
  // construct bitangents between the starting and goal circles
  void StartGoalCirclePair(CircleData * startCircle, CircleData * goalCircle, unsigned int handedness, double RADIUS);

  // construct bitangents between the start circle and a non-goal circle, storing loose ends in a circleData container
  void StartCirclePair(CircleData * startCircle, CircleData * circle2, unsigned int handedness);

  // construct bitangents between non-goal circle and the goal circle, storing loose ends in a circleData container
  void  CircleGoalPair(CircleData * circle1, CircleData * goalCircle, unsigned int handedness, double RADIUS);

  // function to splice together incoming and outgoing bitangents on a circle
  void CircleSplice(CircleData * circle);

  // make all of the bitangents between a pair of start circles and a pair goal circles.
  void makeQuadCirclePlan(double RADIUS);

  // insert additional circles that "stand off" from the start and goal positions.
  void OldMakeStandOffPaths(double RADIUS);
  void MakeStandOffPaths(double RADIUS);

  // weed out the paths where Alice intersects the zone boundary
  void CullInfeasiblePaths();
  void CullInfeasiblePaths(ofstream *f);                       // the alternative version for debugging

  // main circle planner function
  bool CirclePlannerMain(CSpecs_t *cSpecs);

  // ---------------------------------------------------------------------------------------------------------
  // functions and data that interface CirclePlanner with Planner

  static GraphNode nodeArray[GRAPH_PATH_MAX_NODES];        // list of the Graph Nodes that make up the solution  
  static Err_t GenerateTraj(CSpecs_t *cSpecs, Path_t *path);   /* update the Path tree and return a path */

  /* convert the path generated by CirclePlanner to a form that can be used by the planner */
  void populateGraphPath(Path_t *path);
  void populate2DGraphPath(Path_t *path);
};

#endif            // CIRCLEPLANNER_HH
