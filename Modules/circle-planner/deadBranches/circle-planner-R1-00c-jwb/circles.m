<< Graphics`Arrow`

R=1.0;

Cross2D[V1_,V2_]:=
  Block[{},
	Return[V1[[1]] V2[[2]] - V1[[2]] V2[[1]]];
	];
Dot2D[V1_,V2_]:=
  Block[{},
	Return[V1[[1]] V2[[1]] + V1[[2]] V2[[2]]];
	];

FindTheta[V1_,V2_]:=
  Block[{CosAngle,SinAngle},
        CosAngle = Dot2D[V1,V2];          (* find cos of angle between two unit vectors *)
	SinAngle = Cross2D[V1,V2];        (* find sin of angle between two unit vectors *)
	Return[ArcTan[CosAngle,SinAngle]];
	];

CirclePair[PS_,ThetaS_,PF_,ThetaF_,Center1_,Center2_]:=
  Block[{circle1,circle2,x12,y12,L12,phi12,PsiPlus,PsiMinus,cPsiPlus,sPsiPlus,cPsiMinus,sPsiMinus,v1,n1,n2},
	circle1=Graphics[Circle[Center1,R]];
	circle2=Graphics[Circle[Center2,R]];
	x12 = Center2[[1]]-Center1[[1]];
	y12 = Center2[[2]]-Center1[[2]];
	L12=Sqrt[x12 x12 + y12 y12];
	phi12 = ArcTan[x12,y12];
	eta = ArcCos[(2 R)/L12];
	(* compute basic angles *)
	PsiPlus = phi12+eta;
	PsiMinus = phi12-eta;
	cPsiPlus = Cos[PsiPlus];
	sPsiPlus = Sin[PsiPlus];
	cPsiMinus = Cos[PsiMinus];
	sPsiMinus = Sin[PsiMinus];
	(* computer basic vectors *)
	v1 = {(-Sin[phi12]),(Cos[phi12])};
	n1 = {cPsiPlus, sPsiPlus};
	n2 = {cPsiMinus,sPsiMinus};
	(* set up contact points, and the their tangents for first circle*)
	z2a = Center1 + R v1;                 (* first contact point *)
        ZeroOrient2 = PS - Center1;           (* vector which defines zero angle on the circle *)
        ZeroOrient2Theta = FindTheta[{1.0,0.0},ZeroOrient2];  (* determine how much angle to x-axis *)
        thetaz2a = FindTheta[ZeroOrient2,v1]; (* angle that contact point makes with zero orientation line *)
        tempv = {(R Cos[ZeroOrient2Theta+thetaz2a]),(R Sin[ZeroOrient2Theta+thetaz2a])};
	CLine1 = Graphics[{Thickness[0.01],RGBColor[1,0,1],Line[{Center1,(Center1+tempv)}]}];
	z2b = Center1 - R v1;
        thetaz2b = FindTheta[ZeroOrient2,-v1];
        tempv = {(R Cos[ZeroOrient2Theta+thetaz2b]),(R Sin[ZeroOrient2Theta+thetaz2b])};
	CLine2 = Graphics[{Thickness[0.01],RGBColor[1,0,1],Line[{Center1,(Center1+tempv)}]}];
	(*                                       *)
	(* now do the same for the second circle *)
	(*                                       *)
        ZeroOrient4 = PF-Center2;
        ZeroOrient4Theta = FindTheta[{1.0,0.0},ZeroOrient4];
	z4a = Center2 + R v1;
        thetaz4a=FindTheta[ZeroOrient4,v1];
        tempv = {(R Cos[ZeroOrient4Theta+thetaz4a]),(R Sin[ZeroOrient4Theta+thetaz4a])};
	CLine3 = Graphics[{Thickness[0.01],RGBColor[1,0,1],Line[{Center2,(Center2+tempv)}]}];
	z4b = Center2 - R v1;
        thetaz4b=FindTheta[ZeroOrient4,-v1];
        tempv = {(R Cos[ZeroOrient4Theta+thetaz4b]),(R Sin[ZeroOrient4Theta+thetaz4b])};
	CLine4 = Graphics[{Thickness[0.01],RGBColor[1,0,1],Line[{Center2,(Center2+tempv)}]}];
	(* *)
	y2a = Center1 + R n1;
        thetay2a=FindTheta[ZeroOrient2,n1];
        tempv = {(R Cos[ZeroOrient2Theta+thetay2a]),(R Sin[ZeroOrient2Theta+thetay2a])};
	YLine1 = Graphics[{Thickness[0.01],RGBColor[0,1,1],Line[{Center1,(Center1+tempv)}]}];
	y2b = Center1 + R n2;
        thetay2b=FindTheta[ZeroOrient2,n2];
        tempv = {(R Cos[ZeroOrient2Theta+thetay2b]),(R Sin[ZeroOrient2Theta+thetay2b])};
	YLine2 = Graphics[{Thickness[0.01],RGBColor[0,1,1],Line[{Center1,(Center1+tempv)}]}];
	y4a = Center2 - R n2;
        thetay4a=FindTheta[ZeroOrient4,-n2];
        tempv = {(R Cos[ZeroOrient4Theta+thetay4a]),(R Sin[ZeroOrient4Theta+thetay4a])};
	YLine3 = Graphics[{Thickness[0.01],RGBColor[0,1,1],Line[{Center2,(Center2+tempv)}]}];
	y4b = Center2 - R n1;
        thetay4b=FindTheta[ZeroOrient4,-n1];
        tempv = {(R Cos[ZeroOrient4Theta+thetay4b]),(R Sin[ZeroOrient4Theta+thetay4b])};
	YLine4 = Graphics[{Thickness[0.01],RGBColor[0,1,1],Line[{Center2,(Center2+tempv)}]}];
	(* make graphics objects to view *)
	z2ap = Graphics[{PointSize[0.03],RGBColor[1,0,0],Point[z2a]}];
	z2bp = Graphics[{PointSize[0.03],RGBColor[1,0,0],Point[z2b]}];
	z4ap = Graphics[{PointSize[0.03],RGBColor[1,0,0],Point[z4a]}];
	z4bp = Graphics[{PointSize[0.03],RGBColor[1,0,0],Point[z4b]}];
	y2ap = Graphics[{PointSize[0.03],RGBColor[0,1,0],Point[y2a]}];
	y2bp = Graphics[{PointSize[0.03],RGBColor[0,1,0],Point[y2b]}];
	y4ap = Graphics[{PointSize[0.03],RGBColor[0,1,0],Point[y4a]}];
	y4bp = Graphics[{PointSize[0.03],RGBColor[0,1,0],Point[y4b]}];
	(* contact lines *)
	TangentLine1 = Graphics[{Thickness[0.01],RGBColor[1,0,0],Line[{z2a,z4a}]}];
	TangentLine2 = Graphics[{Thickness[0.01],RGBColor[1,0,0],Line[{z2b,z4b}]}];
	Cross1 = Graphics[{Thickness[0.01],RGBColor[0,1,0],Line[{y2a,y4b}]}];
	Cross2 = Graphics[{Thickness[0.01],RGBColor[0,1,0],Line[{y2b,y4a}]}];
	(* return the graphics function *)
	result = Show[{circle1,circle2,z2ap,z2bp,z4ap,z4bp,y2ap,y2bp,y4ap,y4bp,TangentLine1,TangentLine2,Cross1,Cross2,CLine1,CLine2,CLine3,CLine4,YLine1,YLine2,YLine3,YLine4}, Axes->True, AspectRatio->1];
	Return[result];
	];

(* Compute all relevant bitangents to staring/ending circles *)

  CirclePlan[PS_,ThetaS_,PF_,ThetaF_]:=
  Block[{C1,C2,C3,C4,offset1,offset2,C24,C13,C23,C14,StartArrow,GoalArrow},
	(* compute basic circle parameters *)
        offset1 = {-Sin[ThetaS],Cos[ThetaS]};
	C1 = PS + offset1;
	C2 = PS - offset1;
	offset2 = {(-R Sin[ThetaF]),(R Cos[ThetaF])};
	C3 = PF + offset2;
	C4 = PF - offset2;
	(* compute paths associated with pairs of circles *)
	C24 = CirclePair[PS,ThetaS,PF,ThetaF,C2,C4];
	C13 = CirclePair[PS,ThetaS,PF,ThetaF,C1,C3];
	C23 = CirclePair[PS,ThetaS,PF,ThetaF,C2,C3];
	C14 = CirclePair[PS,ThetaS,PF,ThetaF,C1,C4];
        arrowp1 = PS + 2.0{Cos[ThetaS],Sin[ThetaS]};
	StartArrow = Graphics[Arrow[PS,arrowp1]];
        arrowp2 = PF + 2.0{Cos[ThetaF],Sin[ThetaF]};
        GoalArrow  = Graphics[Arrow[PF,arrowp2]];
	Return[Show[{C24,C13,C23,C14,StartArrow,GoalArrow},Axes->True,AspectRatio->1]];
	];

RightPlan[PS_,ThetaS_,PF_,ThetaF_]:=
  Block[{C1,C2,C3,C4,offset,C24,arrowp1,arrowp2,StartArrow,GoalArrow},
	(* compute basic circle parameters *)
	C1 = {-R,0.0};
	C2 = {R,0.0};
	offset = {(-R Sin[ThetaF]),(R Cos[ThetaF])};
	C3 = PF + offset;
	C4 = PF - offset;
	(* compute paths associated with pairs of circles *)
	C24 = CirclePair[PS,ThetaS,PF,ThetaF,C2,C4];
        arrowp1 = PS + 2.0{Cos[ThetaS],Sin[ThetaS]};
	StartArrow = Graphics[Arrow[PS,arrowp1]];
        arrowp2 = PF + 2.0{Cos[ThetaF],Sin[ThetaF]};
        GoalArrow  = Graphics[Arrow[PF,arrowp2]];
	Return[Show[{C24,StartArrow,GoalArrow},Axes->True,AspectRatio->1]];
	];

(* RightPlan[{0.0,0.0},(Pi/2),{4.0,4.0},0.5];  *)

CirclePlan[{0.0,0.0},(Pi/2),{4.0,4.0},0.5];
