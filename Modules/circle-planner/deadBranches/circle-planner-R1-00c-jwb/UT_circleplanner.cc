/*  unit test of Path Tree Planner components.  This is just a way to test components
    without having to interface to the rest of the system */

#include <stream.h>
#include <stdio.h>
#include "frames/pose2.hh"
#include "CirclePlanner.hh"
#include "cspecs/CSpecs.hh"

using namespace std; 

int DummyCSpecs(FILE * ut_spec, CSpecs_t *cSpecs)
{
  double start[STATE_NUM_ELEMENTS], end[STATE_NUM_ELEMENTS];
  int numParkingSpaces;
  vector<point2arr> parkingSpaces;
  int trafficState;
  point2arr boundary;

  cerr << endl;   cerr << endl;   cerr << endl;
  cerr << "-------------------- Data from Simulation File ------------------ " << endl << endl;

  fscanf(ut_spec, "%lf,%lf,%lf\n", start + STATE_IDX_X, start + STATE_IDX_Y, start + STATE_IDX_THETA);
  cout << "read initial state:" << start[STATE_IDX_X] << " " << start[STATE_IDX_Y] << " " << start[STATE_IDX_THETA] << endl;
  fscanf(ut_spec, "%lf,%lf,%lf\n", end + STATE_IDX_X, end + STATE_IDX_Y, end + STATE_IDX_THETA);
  cout << "read final state: " << end[STATE_IDX_X] << " " << end[STATE_IDX_Y] << " " << end[STATE_IDX_THETA] << endl;
  fscanf(ut_spec, "%d\n", &numParkingSpaces);
  cout << numParkingSpaces << " parking spaces" << endl;
  for ( int i = 0; i < numParkingSpaces; i++)
    {
      point2 Pt1, Pt2;
      fscanf(ut_spec, "%lf,%lf,%lf,%lf\n", &(Pt1.x), &(Pt1.y), &(Pt2.x), &(Pt2.y) );
      point2arr space;
      space.push_back(Pt1); space.push_back(Pt2);
      parkingSpaces.push_back(space);
      cout << "parking space num " << i << " is at " << Pt1 << " to " << Pt2 << endl;
    }

  fscanf(ut_spec, "%d", &trafficState);
  cout << "traffic state: " << trafficState << endl;
  
  point2 Pt;
  while ( fscanf(ut_spec, "%lf, %lf\n", &(Pt.x), &(Pt.y) ) != EOF )
    {
      cout << "reading in boundary point " << Pt << endl;
      boundary.push_back(Pt);
    }

  cerr << "------------------------------------------------------------------- " << endl;
  cerr << endl;  cerr << endl; 
	
  // generate the bitmap params
  BitmapParams bmParams;
  bmParams.resX = 0.1;
  bmParams.resY = 0.1;
  bmParams.width = 800;
  bmParams.height = 800;
  bmParams.baseVal = 1 ;
  bmParams.outOfBounds = 100;
  vector<MapElement> obstacles;
  
  point2 initPos(start[STATE_IDX_X], start[STATE_IDX_Y]);
  // 	ZoneCorridor::getBitmapParams(bmParams, polygonParams, initPos, boundary, parkingSpaces, obstacles);
  
  // populate the cspecs
  cSpecs->setStartingState(start);
  cSpecs->setFinalState(end);
  cSpecs->setBoundingPolygon(boundary);
  cSpecs->setCostMap(bmParams);
  cSpecs->setTrafficState(trafficState);

  return 0;
}


int main(int argc, char **argv)
{
  /* dummy start and goal for testing */
  /*
  pose2 startPose = pose2(5.0,5.0,0.0);
  pose2 goalPose   = pose2(-25.0, -10.0, -0.5 * M_PI);
  */

  if (argc < 2)
    {
      cout	<< "Usage: UT_clothoidplanner UT_spec.dat" << endl
		<< "where UT_spec.dat is a file in the following format:" << endl
		<< "initCond.x, initCond.y, initCond.yaw" << endl
		<< "finalCond.x, finalCond.y, finalCond.yaw" << endl
		<< "number of parking spaces" << endl
		<< "space1.x1, space1.y1, space1.x2, space1.y2" << endl
		<< "space2.x1, space2.y1, space2.x2, space2.y2" << endl
		<< "..." << endl
		<< "spaceN.x1, spaceN.y1, spaceN.x2, spaceN.y2" << endl
		<< "TrafficState" << endl
		<< "perimiterPt1.x, perimiterPt1.y" << endl
		<< "perimiterPt2.x, perimiterPt2.y" << endl
		<< "..." << endl
		<< "perimiterPt1N.x, perimiterPtN.y" << endl;
      return 1;
    }
  
  FILE *ut_spec = fopen(argv[1], "r");

  CSpecs_t *cSpecs = new CSpecs();
  Path_t path;
  memset(&path,0,sizeof(path));

  DummyCSpecs(ut_spec, cSpecs);        // load in dummy CSpecs data

  // create an instance of the planner for testing.  This sets up a start node, and sets the start node ptr
  // to the starting pose node.  It also also sets the goal node (which is needed for the A* class).

  cerr << "UT_circleplanner:  Calling Circle Planner GenerateTraj... " << endl;

  Err_t error = CirclePlanner::GenerateTraj(cSpecs,&path);

  if(error == CIRCLE_PLANNER_OK)
    {
      cerr << "  UT_circleplanner:: Circle Planner succeeded " << endl;

      /*
      cerr << "  UT_circleplanner::  printing path to clothoidPath.m " << endl;
      ofstream path_file("clothoidPath.dat");
      for(vector<Clothoid *>::iterator j = curPlanner->m_forwardPath.begin(); j!=curPlanner->m_forwardPath.end(); j++)
	{  (*j)->printClothoid(&path_file); };
      */
    }
  else
    {
      cerr << "  Circle Planner Failed! " << endl;
    };
};


