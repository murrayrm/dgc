/*
 * CirclePlanner.hh: Define the Circle Planner Class, which is a zone planner.
 * 
 * Written by Joel Burdick.starting Sept 10, 2007
 */

/* ---------------------------------------------------------------------------------------- */
/*                                         Includes                                         */
/* ---------------------------------------------------------------------------------------- */

#include <iostream>
#include <stdio.h>

#include <algorithm>     // need this to sue stl heap functions
#include <set>
#include <vector>
#include <math.h>

#include <frames/point2.hh>
#include <frames/pose2.hh>

#include <temp-planner-interfaces/PlanGraphPath.hh>
#include <temp-planner-interfaces/PlanGraph.hh>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <cspecs/CSpecs.hh>
#include <frames/quat.h>

#include <temp-planner-interfaces/Console.hh>
#include <temp-planner-interfaces/Log.hh>

#include "PathTree.hh"

/* ---------------------------------------------------------------------------------------- */
/*                                        Defines                                           */
/* ---------------------------------------------------------------------------------------- */
#ifndef CIRCLEPLANNER_HH
#define CIRCLEPLANNER_HH

using namespace std;

/* ---------------------------------------------------------------------------------------- */
/*                                 The Circle Planner class                                 */
/* ---------------------------------------------------------------------------------------- */

class CirclePlanner
{
private:                                           

  /* ----------------------------------------------------------------- */
  /*      data structures needed to "densify" circle plan graph        */
  /* ----------------------------------------------------------------- */

  // class that contains information about all of a circle's incoming bitangents
  class IncomingBiTangent
  {
  public:
    bool m_reverse;
    PTEdge * m_incomingEdgePtr;
    PTNode * m_incomingNodePtr;

    //simple constructor for the Incoming BiTangent data structure
    IncomingBiTangent(PTNode * nodePtr, PTEdge * edgePtr, bool reverse)
    {
      m_incomingNodePtr= nodePtr;
      m_incomingEdgePtr = edgePtr;
      m_reverse = reverse;
    };
  };

  // class that contains information about all of a circle's  outoing bitangents
  class OutgoingBiTangent
  {
  public:
    bool m_reverse;
    PTNode * m_outgoingNodePtr;
    
    //simple constructor
    OutgoingBiTangent(PTNode * nodePtr, bool reverse)
    {
      m_outgoingNodePtr = nodePtr;
      m_reverse = reverse;
    };
  };

  // class to hold data that is used for splicing together various circles in Circle-Planner
  class CircleData
  {
  public:
    double m_Radius;                                       // the radius of this cicle
    point2 m_center;                                       // the circle center
    PTNode * m_circleNodePtr;
    unsigned int m_handedness;                             // right, left, or ambidextrous handedness
    
    vector<IncomingBiTangent *> In;                        // a list of the incoming BiTangents
    vector<OutgoingBiTangent *> Out;                       // a list of the outgoing BiTangents
    
    // minimal constructor that sets up data in the CircleData container
    CircleData(PTNode * circleNodePtr, point2 circleCenter, double circleRadius, unsigned int handedness)
    {
      m_Radius = circleRadius; 
      m_center = circleCenter; 
      m_circleNodePtr = circleNodePtr;
      m_handedness = handedness; 
      In.clear();  Out.clear();
    };

    // another constructor that's useful for reflexVertices
    CircleData(point2 circleCenter, double circleRadius, unsigned int handedness)
    {
      m_Radius = circleRadius; 
      m_center = circleCenter; 
      m_circleNodePtr = NULL;
      m_handedness = handedness; 
      In.clear();  Out.clear();
    };
  };                              // end of class CircleData
  
  // container to keep useful data about reflex vertices.  This data helps construct the circle graph
  class ReflexVertexData
  {
  private:
    point2 m_vertex;                  // coordinates of reflex vertex
    point2 m_vertexNormal;            // unit vector pointing out of reflex vertex
    unsigned int m_orientation;       // orientation of the polygon to which this reflex vertex belongs
    double m_prevTheta;               // signed angle of polygon edge vector (reflexVertex - previousVertex)
    double m_nextTheta;               // signed angle of polygon edge vector (nextVertex - reflexVertex)        
    double m_connectRadius;             // radius of vertex's connection circle
    vector <CircleData *> incomingCircles;  // list of circles related to this vertex with incoming edges
    vector <CircleData *> outgoingCircles;  // list of circles related to this vertex with outgoing edges

  public:
    // constructor for reflex Vertices
    ReflexVertexData(point2 previous, point2 reflexVertex, point2 next, unsigned int orientation, double connectRadius)
    {
      m_vertex = reflexVertex;
      m_orientation = orientation;
      const point2 xaxis = point2(1.0,0.0);              // the unit vector colinear with the x-axis
      point2 v1 = reflexVertex - previous;               // compute angles of adjacent edges
      point2 v2 = next - reflexVertex;
      m_prevTheta = CirclePlanner::FindTheta(xaxis,v1);
      m_nextTheta = FindTheta(xaxis,v2);
      double normalAngle = 0.5 * (unwrap_angle(m_nextTheta + M_PI) + m_prevTheta);
      m_vertexNormal = point2( cos(normalAngle), sin(normalAngle));
    };
  };

public:                                    
  enum                            // the different kinds of circle directions, handedness, and handedness combinations
    {
      /*
      FORWARD,                    // Alice's direction along a path segment
      REVERSE,
      */
      CLOCKWISE,                  // orientation of polygons, etc.
      COUNTER_CLOCKWISE,
      ORIENTATION_ERROR,
      LEFT_HAND_CIRCLE,           // two basic types of circle handedness we must deal with         
      RIGHT_HAND_CIRCLE,   
      OMNI_HAND_CIRCLE,
      RIGHT_HAND_START_CIRCLE,    // four types of start and goal circles
      LEFT_HAND_START_CIRCLE,
      RIGHT_HAND_GOAL_CIRCLE,
      LEFT_HAND_GOAL_CIRCLE,    
      RIGHT_RIGHT_COMBO,          // different types of start and goal handedness combinations
      RIGHT_LEFT_COMBO,
      RIGHT_OMNI_COMBO,
      LEFT_RIGHT_COMBO,
      LEFT_LEFT_COMBO,
      LEFT_OMNI_COMBO,
      OMNI_RIGHT_COMBO,
      OMNI_LEFT_COMBO,
      OMNI_OMNI_COMBO
    };

  // containers for basic data needed by Circle Planner to do its job
  pose2 m_startPose;
  PTNode * m_startNodePtr;             // pointer to the start, or base, node of the graph of path nodes and edges   
  CircleData * m_rightStartCircle;     // data related to the right starting circle
  CircleData * m_leftStartCircle;      // data related to the left starting circle

  pose2 m_goalPose;
  PTNode * m_goalNodePtr;              // pointer to goal node
  CircleData * m_rightGoalCircle;      // data related to the right starting circle
  CircleData * m_leftGoalCircle;       // data related to the left starting circle

  point2arr m_rightHandedZoneBoundary; // a copy of the CSpecs boundary information, in a right handed coordinate frame
  vector<point2arr> m_obstacles;       // vector of obstacles, where each obstacle is a polygon represented as a point2arr
  vector<double> m_safetyMargins;      // Alice safety Margins
  double m_aliceHalfWidth;             // half width of Alice (plus current safety margins), needed for planning

  // information on reflex vertices on zone boundaries and obstacles
  vector<ReflexVertexData *> m_zoneReflexVertices;
  vector< vector<ReflexVertexData *> > m_obstacleReflexVertices;  // reflex vertices on obstacles. For each obstacle, 
                                                               // pointers to the reflex vertices are stored in a vector.
                                                               // the vector of all these vectors stores all of the data
  unsigned int m_nClothoids;           // keep track of the number of Clothoids produced.(for debugging and indexing)
  vector<Clothoid *> m_solutionPath;   // sequence of clothoids (in reverse order) that will define the solution.
  vector<Clothoid *> m_forwardPath;    // the reverse (properly ordered) solution path

private:                               // internal functions and structures needed by CirclePlanner to compute its plans

  // private utility, debuggin, and reporting functions
  static float Cross2D(point2 V1,point2 V2);      // simple 2-dimensional vector operations
  static float Dot2D(point2 V1, point2 V2);
  static float FindTheta(point2 V1, point2 V2);   // Find the angle between two unit vectors on a 
  static double unwrap_angle(double inAngle);     // simple function to put angles in (-PI, PI)
  bool PointInCircle(point2 p, point2 circleCenter, double R);   // is point p in circle?
  bool PointInSegment(point2 x1, point2 x2, point2 p);           // is p in the segment[x1,x2]?
  bool SegmentCircleIntersection(point2 p1, point2 p2, point2 circleCenter, double R);

  bool ConvexVertex(point2 prev, point2 current, point2 next);   // is current vertex convex?
  unsigned int TriangleOrientation(point2arr * triangle);        // determine orientation of a triangle
  unsigned int PolyOrientation(point2arr * polygon);             // determine orientation of a polygon

  void printPoint2arr(point2arr parray, ofstream *f);    // print a point2arr
  void printPoint2arr(point2arr parray, ostream *f);     // print a point2arr, different output file type
  void printZoneBoundary(ofstream *f);
  void printDenseAliceBox(pose2 alicePose, vector<double> safetyMargins, ofstream *f);
  void printAliceBoxPath(Clothoid * cPtr, ofstream *f);
  void LogZoneBoundary();

public:                                                        // main planning functions

  /*  CirclePlanner(pose2 startPose, pose2 goalPose);       */       // constructor
  CirclePlanner(pose2 startPose, pose2 goalPose, double RADIUS);              // alternate constructor
  ~CirclePlanner();                                            // destructor

  // makes straight and circular path segments needed to construct plans
  PTEdge * makeStraightLine(point2 startPoint, point2 endPoint, bool reverse);
  PTEdge * makeForwardCircularArc(point2 startCenter, float startAngle, float stopAngle, unsigned int handedness, double RADIUS);
  PTEdge * makeReverseCircularArc(point2 startCenter, float startAngle, float stopAngle, unsigned int handedness, double RADIUS);

  // public utility functions from CirclePlannerUtils.cc
  point2arr LeftToRightPointArray(point2arr leftArray);
  point2arr AliceBoundingBox(pose2 alicePose, vector<double> safetyMargins);   // basic version of bounding box
  point2arr AliceBufferedBoundingBox(pose2 alicePose, vector<double> safetyMargins, double buffer);   // basic version of bounding box
  point2arr AliceZoneGrowBox(pose2 alicePose, vector<double> safetyMargins, double buffer);  // another version of bounding box

  bool IsZonePoseLegal(pose2 curPose, vector<double> safetyMargins, double buffer);
  bool IsPoseLegal(pose2 curPose, vector<double> safetyMargins);
  bool IsPathLegal(Clothoid * cPtr);
  bool IsPathLegal(Clothoid * cPtr, ofstream *f);                      // alternate version for debuggin
  bool ExpansiveIsPathLegal(PTEdge *edgePtr, int depth);
  bool ExpansiveIsPathLegal(PTEdge *edgePtr, ofstream *f, int depth);  // the alternative version for debugging
  void GrowZoneBoundary(pose2 aliceLocation, CSpecs_t * cSpecs, double buffer);
  void AdjustZoneBoundary(CSpecs_t * cSpecs);

  bool IsReflexVertex(point2 prev, point2 current, point2 next, unsigned int orientation);
  void findReflexVertices();
  void CircleVertex(CircleData * circle, ReflexVertexData * vertex);
  void makeZoneReflexPaths();

  void CullNodeOutEdges(PTNode * nodePtr);
  void CullNodeOutEdges(PTNode * nodePtr, ofstream *f);        // the alternative version for debugging
  void CullAndExpandNodeOutEdges(PTNode * nodePtr, int depth);
  void CullAndExpandNodeOutEdges(PTNode * nodePtr, ofstream *f, int depth);  // the alternative version for debugging

  // construct bitangents between the starting and goal circles
  void StartGoalCirclePair(CircleData * startCircle, CircleData * goalCircle, unsigned int handedness, double RADIUS);

  // construct bitangents between the start circle and a non-goal circle, storing loose ends in a circleData container
  void StartCirclePair(CircleData * startCircle, CircleData * circle2, unsigned int handedness);

  // construct bitangents between non-goal circle and the goal circle, storing loose ends in a circleData container
  void  CircleGoalPair(CircleData * circle1, CircleData * goalCircle, unsigned int handedness, double RADIUS);

  // function to splice together incoming and outgoing bitangents on a circle
  void CircleSplice(CircleData * circle);

  // make all of the bitangents between a pair of start circles and a pair goal circles.
  void MakeQuadCirclePlan(double RADIUS);

  // insert additional circles that "stand off" from the goal positions.
  void MakeGoalStandOffPaths(double RADIUS);

  // weed out the paths where Alice intersects the zone boundary
  void CullInfeasiblePaths();
  void CullInfeasiblePaths(ofstream *f);                       // the alternative version for debugging

  // main circle planner function
  Err_t CirclePlannerMain(CSpecs_t *cSpecs, bool useStandOff, bool findClosestPath);

  // ---------------------------------------------------------------------------------------------------------
  // functions and data that interface CirclePlanner with Planner

  static PlanGraphNode nodeArray[PLAN_GRAPH_PATH_MAX_NODES];        // list of the Graph Nodes that make up the solution  
  static Err_t GenerateTraj(CSpecs_t *cSpecs, PlanGraphPath *path);   /* update the Path tree and return a path */

  /* convert the path generated by CirclePlanner to a form that can be used by the planner */
  void populate2DGraphPath(PlanGraphPath *path);
};                                                        //   needed during code transition

#endif            // CIRCLEPLANNER_HH
