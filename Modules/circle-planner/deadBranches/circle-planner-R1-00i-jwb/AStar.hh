/*
 * AStar.hh: Define the A-Star Class, which is used to search the Path Tree
 *
 * This defines an A* Algorithm Implementation using STL (for building and maintaining heaps).  
 * 
 * This code is adapted from the algorithm written by Justin Heyes-Jones.  However, I had
 * problems with that implementations's memory management system, and so specialized and 
 * simplified the code.
 *
 * Joel Burdick.
*/

/* ---------------------------------------------------------------------------------------- */
/*                                Includes and Defines                                      */
/* ---------------------------------------------------------------------------------------- */


#include <iostream>
#include <stdio.h>
#include <assert.h>

// stl includes
#include <algorithm>
#include <set>
#include <vector>

#ifndef ASTAR_HH
#define ASTAR_HH

using namespace std;

/* ---------------------------------------------------------------------------------------- */
/*  The A-Star search class defines a "Node" class, which are the nodes of the search       */
/*  spanning tree, then the classical data structures needed for the A-Star search, then    */
/*  the main worker functions, and then some debugging functions.                           */
/* ---------------------------------------------------------------------------------------- */

class AStarSearch
{

public:                                     // Public DATA associated with A-Star search class 
  enum                                      // enumerate search status possibilities
    {
      SEARCH_STATE_NOT_INITIALISED,
      SEARCH_STATE_SEARCHING,
      SEARCH_STATE_SUCCEEDED,
      SEARCH_STATE_FAILED,
      SEARCH_STATE_OUT_OF_MEMORY,
      SEARCH_STATE_INVALID
    };

  /* -------------------------------------------------------------------------------------- */   
  // A node represents a possible state in the spanning tree that is used to search the
  // graph/state space. The node is a container for linking information, cost information
  // and a copy of a ClothoidTreeNode
  /* -------------------------------------------------------------------------------------- */   

public:
  
  class Node               // Node Class.  These are the nodes of the spanning tree
    {
    public:
      
      Node *parent;                               // used during the search to record the parent of successor nodes
      Node *child;                                // used after the search to view the search in reverse
      float g;                                    // cost of this node + it's predecessors
      float h;                                    // heuristic estimate of cost to goal
      float f;                                    // sum of cumulative cost of predecessors and self and heuristic
      float m_incomingCost;                       // cost of edge coming in to this node of search tree
      PTEdge * m_incomingEdgePtr;                 // initial Edge coming in to the PTNode associated with this search Node
      PTNode * m_PTNodePtr;                       // ptr to an existing node of the Path Tree
 
      void printSearchNode()
      {
	cout << " g= " << g << "h= " << h << " f= " << endl;
	m_PTNodePtr->printPTNodePose(&cout);
      };
      
      Node()                                      // basic constructor, which initializes node to zero
	{
	  parent = NULL;
	  child = NULL;
	  g = 0.0;
	  h = 0.0;
	  f = 0.0;
	  m_incomingCost = 0.0;
	  m_incomingEdgePtr = NULL;
	};

      Node(PTNode * NodePtr)            // constructor that initializes a pointer to a Path Tree Node 
	{
	  parent = NULL;
	  child = NULL;
	  g = 0.0;
	  h = 0.0;
	  f = 0.0;
	  m_incomingCost = 0.0;
	  m_incomingEdgePtr = NULL;
	  m_PTNodePtr = NodePtr;
	};

    };

  /* -------------------------------------------------------------------------------------- */   
  // For sorting the heap, the STL needs a "compare function" that lets us compare the f value of 
  // two nodes.  Note, for this to work, the total cost estimate ("f") must have been set on each node.

  class HeapCompare_f 
    {
    public:
      bool operator() ( const Node *x, const Node *y ) const
	{ return x->f > y->f;}
    };

private:                           // main data variables needed to run the A-star searching algorithm

  // The "Open" list.  While it is a simple vector in this definition, it will 
  // be maintained as a Heap by using the STL heap functions. 
  vector< Node *> m_OpenList;

  // The "Closed" list of the A-star search algorithm.  Again, it is implemented as a vector.
  vector< Node * > m_ClosedList; 

  // Successors.  container for list of successors being explored
  vector< Node * > m_Successors;

  unsigned int m_State;	                // State of the search process
  int m_Steps;                   	// Counts steps

  Node *m_Start;                 	// Start and goal state pointers
  Node *m_Goal;

  Node *m_CurrentSolutionNode;          // Pointer to current node being considered during search

public: // methods
  int m_AllocateNodeCount;              // how many search tree nodes have been allocated
	// constructor for the AStarSearch class. The constructor just initialises private data
  AStarSearch()
  {
    m_AllocateNodeCount = 0;
    m_State = SEARCH_STATE_NOT_INITIALISED;
    m_CurrentSolutionNode = NULL;
    m_Steps = 0;
  };

  // initialize start and goal states, as well as A* search variables/structure
  void SetStartAndGoalStates(PTNode * startNodePtr, PTNode * goalNodePtr);

  // get successor nodes of a Clothoid Tree Node
  bool GetSuccessors(PTNode * PTNodePtr);

  bool AddSuccessor(PTEdge * PTEdgePtr);

  float GetCost(Node * NodePtr);

  // main work function.  Search Step advances the search by one "step"
  unsigned int SearchStep(CirclePlanner * curPlanner);

  /* --------------------------------------------------------------------------------- */
  // Memory Management Functions
  //---------------------------------------------------------------------------------- */

  void FreeAllNodes();        // This is called when a search fails or is cancelled to free all used memory 
  void FreeUnusedNodes();                     // free all unused nodes at the end of the search
  void FreeNode( Node *node );                // free up a node and adjust node counters

};      // end of class defn.

#endif            // ASTAR_HH
