/* 
 * CirclePlannerInterface.cc: functions that implement an interface between Circle Planner
 * the planning system.
 *
 * Written by Joel Burdick: Sept. 08, 2007
 *
 */

/* ------------------------------------------------------------------------------------- */
/*                                 Defines and Includes                                  */
/* ------------------------------------------------------------------------------------- */

#include "CirclePlanner.hh"

/* ------------------------------------------------------------------------------------- */
/*                           Planner Interface Functions                                 */
/* ------------------------------------------------------------------------------------- */

// set up storage for Graph Path nodes needed by planner
PlanGraphNode CirclePlanner::nodeArray[PLAN_GRAPH_PATH_MAX_NODES];
vector<Clothoid *> CirclePlanner::m_lastSolution;

/* ------------------------------------------------------------------------------------------------- */
// run through a new planning cycle and return the results.  cSpecs is a container with current 
// information on the state of Alice and the environment.  path is the path that we want to return.
// Note that because of the weird choice of northing and easting for Alice, we need to transform all of
// the resulting calculations in CirclePlanner (which are done in a right=handed reference frame) to 
// Alice's left-handed reference frame
/* ------------------------------------------------------------------------------------------------- */

Err_t CirclePlanner::GenerateTraj(CSpecs_t *cSpecs, PlanGraphPath *path, bool firstTime)
{
  Err_t returnStatus;

  if (cSpecs ==  NULL)               // first check that CSpecs is providing data
    {
      Console::addMessage("CIRCLEPLANNER:: -- CSpecs is NULL, Exiting ");
      return CIRCLE_PLANNER_FAILED;
    };

  // get Alice's starting (conds) and goal (condg) poses via CSpecs.  Remember to switch from Alice's
  // left-handed coordinate system to a right-handed coordinate system.

  vector<double> conds = cSpecs->getStartingState();   
  pose2 startPose(conds.at(STATE_IDX_Y),conds.at(STATE_IDX_X),((M_PI/2.0) - conds.at(STATE_IDX_THETA)) );
  startPose.unwrap_angle();

  vector<double> condg=cSpecs->getFinalState();
  pose2 goalPose(condg.at(STATE_IDX_Y),condg.at(STATE_IDX_X),((M_PI/2.0) - condg.at(STATE_IDX_THETA)) );
  goalPose.unwrap_angle();

  pose2 LHstartPose(conds.at(STATE_IDX_X),conds.at(STATE_IDX_Y),conds.at(STATE_IDX_THETA) );  
  pose2 LHgoalPose(condg.at(STATE_IDX_X),condg.at(STATE_IDX_Y),condg.at(STATE_IDX_THETA) );
  
  Console::addMessage("CIRCLEPLANNER:: ******** new planning cycle ******** ");
  Console::addMessage("CIRCLEPLANNER:: RH start: x: %lf y: %lf ang: %lf LHang: %lf", startPose.x, startPose.y, startPose.ang, LHstartPose.ang);
  Console::addMessage("CIRCLEPLANNER:: RH goal : x: %lf y: %lf ang: %lf LHang: %lf", goalPose.x, goalPose.y, goalPose.ang, LHgoalPose.ang);

  Log::getStream(6) << "CIRCLEPLANNER: Start: x =" << startPose.x << ", y=" << startPose.y << ", ang=" << startPose.ang << ", L ang=" << LHstartPose.ang << endl;
  Log::getStream(6) << "CIRCLEPLANNER: Goal: x =" << goalPose.x << ", y=" << goalPose.y << ", ang=" << goalPose.ang  << " L, ang=" << LHgoalPose.ang << endl;

  //make an instance of  CirclePlanner with the given start and goal states and then create the plan with
  // CirclePlannerMain. if the plan is successful, then process the plan according to the replan flag

  CirclePlanner * curPlanner = new CirclePlanner(startPose, goalPose, TURNING_RADIUS);

  if(firstTime)    // if this is the first time being called, pass back the newly generated trajectory, and store 
    {              //    it for potential later reuse.
      Err_t planStatus = curPlanner->CirclePlannerMain(cSpecs, false);

      m_lastSolution.clear();                          // clear old solution, since this is the first time in this zone
      if (planStatus == CIRCLE_PLANNER_OK)
	{
	  Console::addMessage("CIRCLEPLANNER:: using fresh plan");
	  Log::getStream(6) << "  CIRCLEPLANNER:  using fresh plan in interface" <<endl; 
      	  curPlanner->populate2DGraphPath(path);
	  m_lastSolution.clear();
	  m_lastSolution = curPlanner->CopySolutionPath(); // store a copy of the new solution as the reference solution
	  returnStatus = CIRCLE_PLANNER_OK;  
	}
      else  
	{
	  m_lastSolution.clear();         // solution no good, so leave indication
	  returnStatus = planStatus;
	};
    }
  else     // not the first time in this zone, so first check if last solution s still valid.
    {
      if(m_lastSolution.empty())                   // if there is no last solution, then we must replan
	{
	  Log::getStream(6) << "CIRCLEPLANNERINTERFACE: last Solution empty: replan" << endl;
	  Console::addMessage("CIRCLEPLANNER:: last solution empty: replan");
	  Err_t planStatus = curPlanner->CirclePlannerMain(cSpecs, false);
	  if (planStatus==CIRCLE_PLANNER_OK)
	    {
	      Console::addMessage("CIRCLEPLANNER:: new plan is feasible");
	      Log::getStream(6) << "CIRCLEPLANNERINTERFACE: new plan feasible" << endl;
	      returnStatus=CIRCLE_PLANNER_OK;
	      curPlanner->populate2DGraphPath(path);
	      m_lastSolution.clear();
	      m_lastSolution = curPlanner->m_forwardPath;
	    }
	  else
	    { 
	      Console::addMessage("CIRCLEPLANNER:: new plan is NOT feasible in new Goal clause");
	      Log::getStream(6) << "CIRCLEPLANNERINTERFACE: new plan is NOT feasible" << endl;
	      m_lastSolution.clear();
	      returnStatus=planStatus;
	    };
	}
      else         // the last solution is non trivial, so now check if it's useful
	{
	  // clip off the path points that we've already passed
	  vector<Clothoid *> tempPath = curPlanner->RestructureLastSolution(startPose);  
	  Log::getStream(6) << "      CIRCLEPLANNERINTERFACE: tempPath of size " << tempPath.size() << endl;

	  if (tempPath.empty())          // if this is true, Alice was too far from the old path, so return 
	    {                            // new path, and store new one as lastSolution
	      Log::getStream(6) << "CIRCLEPLANNERINTERFACE: Could NOT use old plan, replanning" << endl;
	      Console::addMessage("CIRCLEPLANNER:: could not use old plan, replaning");
	      
	      Err_t planStatus = curPlanner->CirclePlannerMain(cSpecs, false);
	      
	      if (planStatus==CIRCLE_PLANNER_OK)
		{
		  Console::addMessage("CIRCLEPLANNER:: new plan is feasible");
		  Log::getStream(6) << "CIRCLEPLANNERINTERFACE: new plan is feasible" << endl;
		  returnStatus=CIRCLE_PLANNER_OK;
		  m_lastSolution.clear();
		  m_lastSolution = curPlanner->m_forwardPath;
		}
	      else
		{ 
		  Console::addMessage("CIRCLEPLANNER:: new plan is NOT feasible");
		  Log::getStream(6) << "CIRCLEPLANNERINTERFACE: new plan is NOT feasible" << endl;
		  returnStatus=planStatus;
		};
	    }
	  else     // restructured path is not empty, and therefore potentially valid
	    {
	      Log::getStream(6) << "     CIRCLEPLANNERINTERFACE: tempPath is not empty " << endl;

	      if (curPlanner->HasGoalChanged(tempPath))    // check if goal has changed, since Planner doesn't do that for us
		{
		  Log::getStream(6) << "CIRCLEPLANNERINTERFACE: Have a new goal" << endl;
		  Console::addMessage("CIRCLEPLANNER:: Have a new goal!");
	  
		  Err_t planStatus = curPlanner->CirclePlannerMain(cSpecs, false);
		  if (planStatus==CIRCLE_PLANNER_OK)
		    {
		      Console::addMessage("CIRCLEPLANNER:: new plan for new goal is feasible");
		      Log::getStream(6) << "CIRCLEPLANNERINTERFACE:  new plan for new goal is feasible" << endl;
		      returnStatus=CIRCLE_PLANNER_OK;
		      curPlanner->populate2DGraphPath(path);
		      m_lastSolution.clear();
		      m_lastSolution = curPlanner->m_forwardPath;
		    }
		  else
		    { 
		      Console::addMessage("CIRCLEPLANNER:: new plan is for new goal is NOT feasible");
		      Log::getStream(6) << "CIRCLEPLANNERINTERFACE:  new plan for new goal is NOT feasible" << endl;
		      m_lastSolution.clear();
		      returnStatus=planStatus;
		    };
		}  // end of if goal has changed
	      else                           // else, goal hasn't changed, so let's proceed
		{
		  if(curPlanner->ArePathSegmentsLegal(tempPath))  // Is old solution still valid?
		    {
		      Log::getStream(6) << "CIRCLEPLANNERINTERFACE: old path is still legal" << endl;
		      Console::addMessage("CIRCLEPLANNER:: old path is still feasible");
		      curPlanner->populate2DGraphPath(path,tempPath);
		      m_lastSolution.clear();
		      m_lastSolution = tempPath;             // store the restructured path as the last solution
		      returnStatus = CIRCLE_PLANNER_OK;
		    }
		  else                   // else the old path is not feasible, so construct a new one
		    {
		      Log::getStream(6) << "CIRCLEPLANNERINTERFACE: old plan is not good, constructing new one" << endl;
		      Console::addMessage("CIRCLEPLANNER:: old path is NOT feasible, generating new plan");
		      Err_t planStatus = curPlanner->CirclePlannerMain(cSpecs, false);
		      if (planStatus==CIRCLE_PLANNER_OK)
			{
			  curPlanner->populate2DGraphPath(path);
			  m_lastSolution.clear();
			  m_lastSolution = curPlanner->m_forwardPath;
			  returnStatus=CIRCLE_PLANNER_OK;
			}
		      else
			{ 
			  m_lastSolution.clear();
			  returnStatus= planStatus; 
			};
		    }; // end of if path segments are legal
		};     // end of if/else has goal changed
	    };         // end of if/else tempPath empty
	};                 // end of if/else last solution trivial
    };                 // end of if/else first time in zone
  return returnStatus;
};

// ------------------------------------------------------------------------------------------------------
// a function to transform the solution path representation made in Circle Planner to the Graph/GraphNode 
// representation used by planner.  This function also takes care of the transformation from a right-handed
// coordinate (which is the type of coordinate system used by Circle Planner for all of its internal calculations
// to the left-handed reference used by Alice

// ------------------------------------------------------------------------------------------------------
// the 2D version of populateGraphPath, as vel-planner is now going toa 2D representation

void CirclePlanner::populate2DGraphPath(PlanGraphPath *path)
{
  Log::getStream(6) << "   CIRCLEPLANNER Log: starting populateGraph " << endl;
  if (!this->m_forwardPath.empty())           // if we found a feasible path, then ....
    {
      // set variables in the GraphPath structure
      path->valid = true;                     // path is valid if we found it
      path->collideObs = 0;                   // we don't think there are any obstacle or car collisions
      path->collideCar = 0;

      double pathLength = 0.0;                // variable that will accumulate total path length
      
      if (this->m_forwardPath.size() > 0) 
        {
          int i = 0;                          // The number of points in the path
          // iterator over the clothoids path segments in m_forwardPath
          for (vector<Clothoid *>::iterator j = this->m_forwardPath.begin(); j != this->m_forwardPath.end(); j++)
            {
              vector<pose2> clothoid = (*j)->m_clothoidPoints;
              pathLength += (*j)->m_length;
              
              // iterate over the points in the current clothoid.  Node, counter "i" gets updated here
              for (vector<pose2>::iterator k = clothoid.begin(); k != clothoid.end(); k++, i++) 
                {
                  Log::getStream(6) << "       x = " << (*k).x << ", y= " << (*k).y << ", ang= " << (*k).ang ; 
                  
                  // note, DON'T FORGET THAT WE MUST CONVERT BACK TO ALICE'S LEFT HAND COORDINATE SYSTEM!
                  nodeArray[i].pose.pos.x = (*k).y;
                  nodeArray[i].pose.pos.y = (*k).x;

		  // temporary hack to be compatible with velocity planner's way of handling reverse paths
		  double tempAngle;
		  if( (*j)->m_reverse )
		    {tempAngle =  0.5 * M_PI - (*k).ang;}
		  else
		    {tempAngle =  0.5 * M_PI - (*k).ang;};
                  nodeArray[i].pose.rot = unwrap_angle(tempAngle);

                  // store other info about path direction, curvature, index, etc.
                  nodeArray[i].nodeId = (i+1);
                  path->nodes[i] = &(nodeArray[i]);
                  if ( (*j)->m_reverse )                                   // add forward/reverse info
                    { 
                      Log::getStream(6) << ", backward " << endl;
                      path->directions[i] = PLAN_GRAPH_PATH_REV; 
                    }
                  else
                    {
                      Log::getStream(6) << ", forward " << endl;
                      path->directions[i] = PLAN_GRAPH_PATH_FWD;  
                    };
                  path->curvatures[i] = (*j)->m_initCurv;                  // this is used by velocity planner
                };
            };
          path->dist = pathLength;           // distance to goal is sum of clothoid lengths
          path->pathLen = i;                 // total number of nodes in this path
        };
      Log::getStream(6) << "   CIRCLEPLANNER: pathLen in 1st Populate2DGraph = " << path->pathLen << endl;
      Log::getStream(6) << "   CIRCLEPLANNER: finished 1st populate2DGraph " << endl;
    }
  else
    {
      Log::getStream(6) << "   CIRCLEPLANNER: empty path populate2DGraph " << endl;
      Console::addMessage("CIRCLEPLANNER::empty path in populate2DGraph!");
    };
  return;
};

/* --------------------------------------------------------------------------------------------------------------------------------  */
// another version of the function where the segmented clothoid path is passed in as a parameter
void CirclePlanner::populate2DGraphPath(PlanGraphPath *path, vector<Clothoid *> cPath)
{
  Log::getStream(6) << "   CIRCLEPLANNER Log: starting populateGraph " << endl;
  if (!cPath.empty())           // if we found a feasible path, then ....
    {
      // set variables in the GraphPath structure
      path->valid = true;                     // path is valid if we found it
      path->collideObs = 0;                   // we don't think there are any obstacle or car collisions
      path->collideCar = 0;

      double pathLength = 0.0;                // variable that will accumulate total path length
      
      if (cPath.size() > 0) 
        {
          int i = 0;                          // The number of points in the path
          // iterator over the clothoids path segments in m_forwardPath
          for (vector<Clothoid *>::iterator j = cPath.begin(); j != cPath.end(); j++)
            {
              vector<pose2> clothoid = (*j)->m_clothoidPoints;
              pathLength += (*j)->m_length;
              
              // iterate over the points in the current clothoid.  Node, counter "i" gets updated here
              for (vector<pose2>::iterator k = clothoid.begin(); k != clothoid.end(); k++, i++) 
                {
                  Log::getStream(6) << "       x = " << (*k).x << ", y= " << (*k).y << ", ang= " << (*k).ang ; 
                  
                  // note, DON'T FORGET THAT WE MUST CONVERT BACK TO ALICE'S LEFT HAND COORDINATE SYSTEM!
                  nodeArray[i].pose.pos.x = (*k).y;
                  nodeArray[i].pose.pos.y = (*k).x;

		  // temporary hack to be compatible with velocity planner's way of handling reverse paths
		  double tempAngle;
		  if( (*j)->m_reverse )
		    {tempAngle = 0.5 * M_PI - (*k).ang;}
		  else
		    {tempAngle =  0.5 * M_PI - (*k).ang;};
                  nodeArray[i].pose.rot = unwrap_angle(tempAngle);

                  // store other info about path direction, curvature, index, etc.
                  nodeArray[i].nodeId = (i+1);
                  path->nodes[i] = &(nodeArray[i]);

                  if ( (*j)->m_reverse )                                   // add forward/reverse info
                    { 
                      Log::getStream(6) << ", backward " << endl;
                      path->directions[i] = PLAN_GRAPH_PATH_REV; 
                    }
                  else
                    {
                      Log::getStream(6) << ", forward " << endl;
                      path->directions[i] = PLAN_GRAPH_PATH_FWD;  
                    };
                  path->curvatures[i] = (*j)->m_initCurv;                  // this is used by velocity planner
                };
            };
          path->dist = pathLength;           // distance to goal is sum of clothoid lengths
          path->pathLen = i;                 // total number of nodes in this path
        };
      Log::getStream(6) << "   CIRCLEPLANNER: pathLen in 2nd Populate2DGraph = " << path->pathLen << endl;
      Log::getStream(6) << "   CIRCLEPLANNER: finishing 2nd populate2DGraph " << endl;
    }
  else
    {
      Console::addMessage("CIRCLEPLANNER: empty path in 2nd populate2DGraph!");
      Log::getStream(6) << "   CIRCLEPLANNER: empty path 2nd populate2DGraph " << endl;
      Log::getStream(6) << "   CIRCLEPLANNER: empty path in populate2DGraph! " << endl;
    };
  return;
};

/* --------------------------------------------------------------------------------------------------------------------------------  */
// this function takes Alice's current position and then restructures the last solution so that it starts approximately from Alice's
// current position.

vector<Clothoid *> CirclePlanner::RestructureLastSolution(pose2 alicePose)
{
  point2 aliceLeftHandPos = point2(alicePose.y,alicePose.x);
  point2 projectionPt;               // the point on the interpolation of the path that is closest to Alice's current pose
  double distToAlice;                 // distance to point on path that is closest to Alice's current position
  point2arr pointsPath;               // just the position part of the solution path

  vector<Clothoid *> returnPath;
  returnPath.clear();

  // now construct a point2arr consisting of path positions.  We need this for the projection operation
  for(vector<Clothoid *>::iterator cPtr = m_lastSolution.begin(); cPtr != m_lastSolution.end(); cPtr++)
    {
      vector<pose2> curClothoid = (*cPtr)->m_clothoidPoints;
      for(vector<pose2>::iterator pPtr = curClothoid.begin(); pPtr != curClothoid.end(); pPtr++)
	{ 
	  point2 pathPoint = point2((*pPtr).y,(*pPtr).x);
	  pointsPath.arr.push_back(pathPoint); 
	};
    };
    
  double aliceLeftHandYaw =  0.5 * M_PI - alicePose.ang;
  // find the point along the path that is closest to Alice's current position.
  int test = Utils::distToProjectedPoint(projectionPt,distToAlice,aliceLeftHandPos,aliceLeftHandYaw,pointsPath,ACCEPTABLE_ALICE_ERROR,5);

  if (test != 0)        // if this is true, then we had difficulty finding a good projection of Alice's pose onto the path
    {
      Log::getStream(6) << "CIRCLEPLANNER: difficulty in finding projection of Alice pose onto path" << endl; 
      return returnPath;                   // return an empty path, since Alice was too far away.  This will force the calling
    }                                      // function to choose a newly found path instead.
  else                 // okay, we can get a good projection onto the path
    {
      Log::getStream(6) << "CIRCLEPLANNER: found a projection of Alice pose onto path" << endl; 
      // determine where to restart the previous solution
      int totalPreviousPoints = 0;
      int aliceIndex = floor( (distToAlice/CP_PATH_SECTION_LENGTH) );          // get index of Alice's closest pose

      // loop through all of the Path segments to find the one containing Alice's projected pose.
      
      if(distToAlice <= 0.0)
	{
	  returnPath = m_lastSolution;
	}
      else
	{
      	  vector<Clothoid *>::iterator segIndex = m_lastSolution.begin();          // index of a path segment
	  int count = 1;
	  bool intersectionFound = false;
	  do
	    {
	      int curClothoidSize = (*segIndex)->m_clothoidPoints.size();          // how many points in the current clothoids?
	      int curAliceIndex = aliceIndex - totalPreviousPoints;                // adjust Alice's index to align with numbering in current clothoid segment

	      if (curAliceIndex < curClothoidSize)                                 // Alice projection must be in this path segment
		{
		  // first make a new shortened path segment that leads from Alice's current position to end of this Path Segment
		  double newClothoidLength = CP_PATH_SECTION_LENGTH * (curClothoidSize - curAliceIndex -1);
		  Clothoid * newSegmentPtr = new Clothoid((*segIndex)->m_initCurv, (*segIndex)->m_finalCurv, newClothoidLength, (*segIndex)->m_reverse);
		  
		  // copy the distal points of this path to the newly Shortened Clothoid
		  newSegmentPtr->m_clothoidPoints.clear();            
		  for(vector<pose2>::iterator pPtr = ((*segIndex)->m_clothoidPoints.begin() + curAliceIndex +1); 
		      pPtr != (*segIndex)->m_clothoidPoints.end(); pPtr++)
		    {newSegmentPtr->m_clothoidPoints.push_back( (* pPtr) ); };

		  // now reassemble the path by pushing back newly shortened segment, followed by a copy of the pointer to the rest of the
		  // path segments into the new vector<Clothoid *> returnPath
		  returnPath.push_back(newSegmentPtr);
		  for(vector<Clothoid *>::iterator remainder = (segIndex+1); remainder != m_lastSolution.end(); remainder++)
		    {  returnPath.push_back( (* remainder) ); };
		  intersectionFound = true;
		}
	      else  // Alice's pose is not in this clothoid, so let's go to the next one.
		{
		  totalPreviousPoints += curClothoidSize;                          // update the total number of path points that have been considered
		  segIndex++;
		};
	      count ++;
	    }
	  while ( (segIndex != m_lastSolution.end()) && (!intersectionFound) );
	};
    };
  return returnPath;
};

/* --------------------------------------------------------------------------------------------------------------------------------  */
double CirclePlanner::PathCost(vector<Clothoid *> cPath)
{
  double cost=0.0;
  for(vector<Clothoid *>::iterator cPtr = cPath.begin(); cPtr != cPath.end(); cPtr++)
    { cost+= (* cPtr)->m_localEdgeCost;}
  return cost;
};

void CirclePlanner::PrintClothoidSegments(vector<Clothoid *> cPath)
{
  for(vector<Clothoid *>::iterator j=cPath.begin(); j!=cPath.end(); j++)
    {
      (*j)->printClothoid(&cout);
    }
};

/* --------------------------------------------------------------------------------------------------------------------------------  */
bool CirclePlanner::HasGoalChanged(vector<Clothoid *> cPath)
{
  Clothoid * lastClothoidPtr = cPath.back();                     // get last path segment
  pose2 lastPose = lastClothoidPtr->m_clothoidPoints.back(); // get last pose2 of this last segment

  pose2 goalPose = this->m_goalPose;

  Log::getStream(6) << "HasGoalChanged: last x =" << lastPose.x << ", y=" <<lastPose.y << ", ang=" <<lastPose.ang << endl;
  Log::getStream(6) << "HasGoalChanged: goal x =" << goalPose.x << ", y=" <<goalPose.y << ", ang=" <<goalPose.ang << endl;
  double xdiff = goalPose.x - lastPose.x;
  double ydiff = goalPose.y - lastPose.y;
  double angdiff = goalPose.ang - lastPose.ang;
  double pdiff2 = xdiff * xdiff + ydiff * ydiff;

  if ((pdiff2 < GOAL_XY2) && ((angdiff * angdiff) < GOAL_ANG2))
    { return false; }
  else
    { return true; };
};

