/*
 * Orientation.cc: code to determine the orientation of a polygon 
 * (such as a Zone boundary or an obstacle)
 * 
 * based on code by Carlos Moreno, adapted to the DGC data formats
 * by Joel Burdick, Oct. 2007
 */


#include "CirclePlanner.hh"

// determine the orientation of a Triangle.  This is needed for the algorithm to find the
// orientation a polygon
unsigned int CirclePlanner::TriangleOrientation (point2arr * triangle) 
{
  vector<point2>:: iterator index = triangle->arr.begin();
  point2 v1 = (* index);
  point2 v2 = *(index +1);
  point2 v3 = *(index +2);

  double signed_area = (v1.x * (v2.y - v3.y) + v2.x * (v3.y - v1.y) + v3.x * (v1.y - v2.y));
  return signed_area > 0 ? COUNTER_CLOCKWISE : (signed_area < 0 ? CLOCKWISE : 0);
};

/* ---------------------------------------------------------------------------------------- */
// function that will return the orientation (clockwise or counterclockwise) of a polygon (which is 
// represented via a point2arr).  This function works as follows.  First we find a subset of three 
// successive vertices that do not contain any other polygon vertex inside the triangle formed by
// the three successive vertices.  Then we select a point (the geometric mean of the three vertices)
// inside the triangle.  We can then determine if this point is inside or outside of the polygon.  
// Combining that information with the orientation of the triangle, we can determine the orientation 
// of the polygon.  Note, we use a pointer for input to save time.

unsigned int CirclePlanner::PolyOrientation(point2arr * polygon)
{
  bool empty_triangle_found = false;
  point2arr Triangle;                            

  // First loop through all of the polygon vertices.  Consider each successive triplet of vertices 
  // as a triangle.  For each triangle, see if it contains other  vertices of the polygon).  When we
  // find a triangle with no internal vertices, then we're ready for the next step.

  int polysize = polygon->arr.size();                    // check for special cases. 
  if (polysize <= 2)
    {
      cerr << "    PolyOrientation:  received incomplete polygon " << endl;
      return ORIENTATION_ERROR;
    };
  if (polysize == 3)                                    // check if input is a triangle.
    { 
      return TriangleOrientation(polygon);   
    }
  else                                                 // if not a triangle, then carry out calculation
    { 
      // iterators for polygon vertices

      vector<point2>::iterator previous = (polygon->arr.end() -1);
      vector<point2>::iterator current = polygon->arr.begin();
      vector<point2>::iterator next;

      do 
	{ 
	  Triangle.arr.clear();

	  if ( current < (polygon->arr.end() -1) )   // make a triangle from the three successive vertices
	    { next = (current +1); }
	  else
	    { next = polygon->arr.begin();};

	  Triangle.arr.push_back( (*previous) );   
	  Triangle.arr.push_back( (*current) );
	  Triangle.arr.push_back( (*next) );

	  bool triangle_is_empty = true;	  

	  // see if all of the other poly vertices are not in the interior of the Triangel T.  
          // If so, then we have a candidate for checking the polygon orientation
	  for(vector<point2>::iterator vertex = polygon->arr.begin(); vertex != polygon->arr.end(); 
	      vertex++)
	    {   
	      // focus on vertices that aren't in the triangle
	      if ( (vertex!= previous) && (vertex!= current) && (vertex!= next) )
		{
		  if (Triangle.is_inside_poly( (*vertex) ) )  // check if vertex is in the triangle
		    { triangle_is_empty = false;  };
		};
	    };
	  if (triangle_is_empty)                              // if no vertices in triangle, exit loop
	    { empty_triangle_found = true;}
	  else                                                // else, iterate to next step
	    {
	      previous = current;
	      current++;
	    };
	}
      while ( (current != polygon->arr.end() ) && (!empty_triangle_found) );

      // We now have two cases: (1) all the points inside the triangle are inside the polygon; 
      // or (2) all the points inside the triangle are outside the polygon. Given the orientation
      // of Triangle, and checking  if a point inside Triangle is also inside the polygon, we can 
      // determine the orientation of the polygon.

      // generate a test point inside the triangle
      point2 ptest = (1/3.0) * ( Triangle.arr[0] + Triangle.arr[1] + Triangle.arr[2] ); 

      if ( Triangle.is_inside_poly(ptest) )    // if the test point is inside the polygon
	{
	  if ( TriangleOrientation(&Triangle) == COUNTER_CLOCKWISE )
	    { return COUNTER_CLOCKWISE;  }
	  else
	    { return CLOCKWISE;  };
	}
      else
	{
	  if (TriangleOrientation(&Triangle) == COUNTER_CLOCKWISE)
	    { return CLOCKWISE;  }
	  else
	    { return COUNTER_CLOCKWISE;  };
	};
    };
};
