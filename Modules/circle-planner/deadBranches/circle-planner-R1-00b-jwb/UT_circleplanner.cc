/*  unit test of Path Tree Planner components.  This is just a way to test components
    without having to interface to the rest of the system */

#include <stream.h>
#include <stdio.h>
#include "frames/pose2.hh"
#include "CirclePlanner.hh"
#include "cspecs/CSpecs.hh"

using namespace std; 

int DummyCSpecs(CSpecs_t *cSpecs)
{

  //	FILE *ut_spec = fopen("UT_park"eltoro_exit1.dat", "r");
  //	FILE *ut_spec = fopen("UT_park"stluke_exit1.dat", "r");
  //	FILE *ut_spec = fopen("UT_park"stluke_enter1.dat", "r");
  //	FILE *ut_spec = fopen("UT_uturn.dat", "r");
  FILE *ut_spec = fopen("UT_park_eltoro_enter1.dat","r");

  double start[STATE_NUM_ELEMENTS], end[STATE_NUM_ELEMENTS];
  int numParkingSpaces;
  vector<point2arr> parkingSpaces;
  int trafficState;
  point2arr boundary;

  fscanf(ut_spec, "%lf,%lf,%lf\n", start + STATE_IDX_X, start + STATE_IDX_Y, start + STATE_IDX_THETA);
  cout << "read initial state:" << start[STATE_IDX_X] << " " << start[STATE_IDX_Y] << " " << start[STATE_IDX_THETA] << endl;
  fscanf(ut_spec, "%lf,%lf,%lf\n", end + STATE_IDX_X, end + STATE_IDX_Y, end + STATE_IDX_THETA);
  cout << "read final state: " << end[STATE_IDX_X] << " " << end[STATE_IDX_Y] << " " << end[STATE_IDX_THETA] << endl;
  fscanf(ut_spec, "%d\n", &numParkingSpaces);
  cout << numParkingSpaces << " parking spaces" << endl;
  for ( int i = 0; i < numParkingSpaces; i++)
    {
      point2 Pt1, Pt2;
      fscanf(ut_spec, "%lf,%lf,%lf,%lf\n", &(Pt1.x), &(Pt1.y), &(Pt2.x), &(Pt2.y) );
      point2arr space;
      space.push_back(Pt1); space.push_back(Pt2);
      parkingSpaces.push_back(space);
      cout << "parking space num " << i << " is at " << Pt1 << " to " << Pt2 << endl;
    }

  fscanf(ut_spec, "%d", &trafficState);
  cout << "traffic state: " << trafficState << endl;
  
  point2 Pt;
  while ( fscanf(ut_spec, "%lf, %lf\n", &(Pt.x), &(Pt.y) ) != EOF )
    {
      cout << "reading in boundary point " << Pt << endl;
      boundary.push_back(Pt);
    }
	
  // generate the bitmap params
  BitmapParams bmParams;
  bmParams.resX = 0.1;
  bmParams.resY = 0.1;
  bmParams.width = 800;
  bmParams.height = 800;
  bmParams.baseVal = 1 ;
  bmParams.outOfBounds = 100;
  vector<MapElement> obstacles;
  
  point2 initPos(start[STATE_IDX_X], start[STATE_IDX_Y]);
  // 	ZoneCorridor::getBitmapParams(bmParams, polygonParams, initPos, boundary, parkingSpaces, obstacles);
  
  // populate the cspecs
  cSpecs->setStartingState(start);
  cSpecs->setFinalState(end);
  cSpecs->setBoundingPolygon(boundary);
  cSpecs->setCostMap(bmParams);
  cSpecs->setTrafficState(trafficState);

  return 0;
}


int main()
{
  /* dummy start and goal for testing */
  /*
  pose2 startPose = pose2(5.0,5.0,0.0);
  pose2 goalPose   = pose2(-25.0, -10.0, -0.5 * M_PI);
  */

  CSpecs_t *cSpecs = new CSpecs();
  Path_t * path;

  // create an instance of the planner for testing.  This sets up a start node, and sets the start node ptr
  // to the starting pose node.  It also also sets the goal node (which is needed for the A* class).

  cerr << "starting Circle Planner... " << endl;

  DummyCSpecs(cSpecs);        // load in dummy CSpecs data

  CirclePlanner::GenerateTraj(cSpecs,path);

  /*
  //instantiate a new planner
  CirclePlanner * curPlanner = new CirclePlanner(startPose,goalPose);   

  // create paths and A* search them.
  bool planresult = curPlanner->CirclePlannerMain(cSpecs);

  if(planresult)
    {
      cerr << "Circle Planner found a solution " << endl;
      cerr << "  Saving all potential solution paths in clothoidTree.dat.. " << endl;
      ofstream tree_file("clothoidTree.dat");
      curPlanner->m_startNodePtr->printAllPaths(&tree_file);

      cerr << "  Saving Solution Path in clothoidPath.dat... " << endl;
      ofstream path_file("clothoidPath.dat");
      for(vector<Clothoid *>::iterator j = curPlanner->m_forwardPath.begin(); j!=curPlanner->m_forwardPath.end(); j++)
	{
	  (*j)->printClothoid(&path_file);
	};
      cout << "    Good bye " << endl;
      return true;
    }
  else
    {
      cerr << "  Circle Planner couldn't find a solution " << endl;
      cout << "    Good bye " << endl;
      return false;
    };
  */
};


