/* CirclePlannerInterface.cc: functions that implement an interface between Circle Planner
 * the planning system.
 *
 * Written by Joel Burdick: Sept. 08, 2007
 *
 */

/* ------------------------------------------------------------------------------------- */
/*                                 Defines and Includes                                  */
/* ------------------------------------------------------------------------------------- */

#include "CirclePlanner.hh"

/* ------------------------------------------------------------------------------------- */
/*                           Planner Interface Functions                                 */
/* ------------------------------------------------------------------------------------- */

// set up storage for Graph Path nodes needed by planner
GraphNode CirclePlanner::nodeArray[GRAPH_PATH_MAX_NODES];


/* ------------------------------------------------------------------------------------------------- */
// run through a new planning cycle and return the results.  cSpecs is a container with current 
// information on the state of Alice and the environment.  path is the path that we want to return.
// Note that because of the weird choice of northing and easting for Alice, we need to transform all of
// the resulting calculations in CirclePlanner (which are done in a right=handed reference frame) to 
// Alice's left-handed reference frame
/* ------------------------------------------------------------------------------------------------- */

Err_t CirclePlanner::GenerateTraj(CSpecs_t *cSpecs, Path_t *path)
{
  Err_t returnStatus;

  if (cSpecs ==  NULL)               // first check that CSpecs is providing data
    {
      Console::addMessage("CIRCLEPLANNER:: -- CSpecs is NULL, Exiting ");
      return CIRCLE_PLANNER_FAILED;
    };

  // get Alice's starting (conds) and goal (condg) poses via CSpecs.  Remember to switch from Alice's
  // left-handed coordinate system to a right-handed coordinate system.

  vector<double> conds = cSpecs->getStartingState();   
  pose2 startPose(conds.at(STATE_IDX_Y),conds.at(STATE_IDX_X),((M_PI/2.0) - conds.at(STATE_IDX_THETA)) );
  startPose.unwrap_angle();

  vector<double> condg=cSpecs->getFinalState();
  pose2 goalPose(condg.at(STATE_IDX_Y),condg.at(STATE_IDX_X),((M_PI/2.0) - condg.at(STATE_IDX_THETA)) );
  goalPose.unwrap_angle();
  
  Console::addMessage("CIRCLEPLANNER:: ******** new planning cycle ******** ");
  Console::addMessage("CIRCLEPLANNER:: start pose: x: %lf y: %lf ang: %lf", startPose.x, startPose.y, startPose.ang);
  Console::addMessage("CIRCLEPLANNER::  goal pose: x: %lf y: %lf ang: %lf", goalPose.x, goalPose.y, goalPose.ang);
  
  //make an instance of  CirclePlanner with the given start and goal states and then create the plan with
  // CirclePlannerMain. if the plan is successful, convert CirclePlanners's path representation to a GraphPath,
  // also taking into account the right hand to left hand coordinate frame transformation

  CirclePlanner * curPlanner = new CirclePlanner(startPose,goalPose, TURNING_RADIUS);
  bool planStatus = curPlanner->CirclePlannerMain(cSpecs);

  if (planStatus == true)
    {
#if CIRCLE_PLANNER_DEBUG
      cerr << "  printing path to clothoidPath.m " << endl;
      ofstream path_file("clothoidPath.dat");
      for(vector<Clothoid *>::iterator j = curPlanner->m_forwardPath.begin(); j!=curPlanner->m_forwardPath.end(); j++)
	{ 
	  (*j)->printClothoid(&path_file); 
	  curPlanner->printAliceBoxPath((*j),&path_file);
	};
#endif
      /*      curPlanner->populate2DGraphPath(path); */
      curPlanner->populateGraphPath(path);
      returnStatus = CIRCLE_PLANNER_OK;
    }
  else
    {
      returnStatus = CIRCLE_PLANNER_FAILED;
    };
  return returnStatus;
};

// ------------------------------------------------------------------------------------------------------
// a function to transform the solution path representation made in Circle Planner to the Graph/GraphNode 
// representation used by planner.  This function also takes care of the transformation from a right-handed
// coordinate (which is the type of coordinate system used by Circle Planner for all of its internal calculations
// to the left-handed reference used by Alice

void CirclePlanner::populateGraphPath(Path_t *path)
{
  Log::getStream(6) << "   CIRCLEPLANNER Log: starting populateGraph " << endl;
  if (!this->m_forwardPath.empty())           // if we found a feasible path, then ....
    {
      // set variables in the GraphPath structure
      path->valid = true;                     // path is valid if we found it
      path->collideObs = 0;                   // we don't think there are any obstacle or car collisions
      path->collideCar = 0;

      double pathLength = 0.0;                // variable that will accumulate total path length

      if (this->m_forwardPath.size() > 0) 
	{
	  int i = 0;                          // The number of points in the path
	  // iterator over the clothoids path segments in m_forwardPath
	  for (vector<Clothoid *>::iterator j = this->m_forwardPath.begin(); j != this->m_forwardPath.end(); j++)
	    {
	      vector<pose2> clothoid = (*j)->m_clothoidPoints;
	      pathLength += (*j)->m_length;

	      // iterate over the points in the current clothoid.  Node, counter "i" gets updated here
	      Log::getStream(9) << "   CIRCLEPLANNER Log: populateGraph " << endl;
	      for (vector<pose2>::iterator k = clothoid.begin(); k != clothoid.end(); k++, i++) 
		{
		  Log::getStream(9) << "       x = " << (*k).x << ", y= " << (*k).y << ", ang= " << (*k).ang ;

		  // note, DON'T FORGET THAT WE MUST CONVERT BACK TO ALICE'S LEFT HAND COORDINATE SYSTEM!
		  nodeArray[i].pose.pos.x = (*k).y;
		  nodeArray[i].pose.pos.y = (*k).x;
		  nodeArray[i].pose.pos.z = 0;
		  double tempAngle = 0.5 * M_PI - (*k).ang;
		  nodeArray[i].pose.rot = quat_from_rpy(0, 0, unwrap_angle(tempAngle));
		  
		  // HACK:  Magnus asked me to give the nodes distinct negative numbers so as not to interfere
		  // with some other part of the system. I don't understand, but I'll follow his request ....
		  nodeArray[i].index = -(i+1);
		  if ( (*j)->m_reverse )                                   // add forward/reverse info
		    { 
		      Log::getStream(9) << ", path is backward " << endl;
		      nodeArray[i].pathDir = GRAPH_PATH_REV; 
		    }
		  else
		    {
		      Log::getStream(9) << ", path is forward " << endl;
		      nodeArray[i].pathDir = GRAPH_PATH_FWD;  
		    };
		  path->curvatures[i] = (*j)->m_initCurv;                  // this is used by velocity planner
		  path->path[i] = &(nodeArray[i]);
		};
	    };
	  path->goalDist = pathLength;       // distance to goal is sum of clothoid lengths
	  path->pathLen = i;                 // total number of nodes in this path
	};
      Log::getStream(6) << "   CIRCLEPLANNER: finishing populateGraph " << endl;
    };
  return;
};


// ------------------------------------------------------------------------------------------------------
// the 2D version of populateGraphPath, as vel-planner is now going toa 2D representation

void CirclePlanner::populate2DGraphPath(Path_t *path)
{
  Log::getStream(6) << "   CIRCLEPLANNER Log: starting populateGraph " << endl;
  if (!this->m_forwardPath.empty())           // if we found a feasible path, then ....
    {
      // set variables in the GraphPath structure
      path->valid = true;                     // path is valid if we found it
      path->collideObs = 0;                   // we don't think there are any obstacle or car collisions
      path->collideCar = 0;

      double pathLength = 0.0;                // variable that will accumulate total path length

      if (this->m_forwardPath.size() > 0) 
	{
	  int i = 0;                          // The number of points in the path
	  // iterator over the clothoids path segments in m_forwardPath
	  for (vector<Clothoid *>::iterator j = this->m_forwardPath.begin(); j != this->m_forwardPath.end(); j++)
	    {
	      vector<pose2> clothoid = (*j)->m_clothoidPoints;
	      pathLength += (*j)->m_length;

	      // iterate over the points in the current clothoid.  Node, counter "i" gets updated here
	      Log::getStream(9) << "   CIRCLEPLANNER Log: populateGraph " << endl;
	      for (vector<pose2>::iterator k = clothoid.begin(); k != clothoid.end(); k++, i++) 
		{
		  Log::getStream(9) << "       x = " << (*k).x << ", y= " << (*k).y << ", ang= " << (*k).ang ;

		  // note, DON'T FORGET THAT WE MUST CONVERT BACK TO ALICE'S LEFT HAND COORDINATE SYSTEM!
		  nodeArray[i].pose_x = (*k).y;
		  nodeArray[i].pose_y = (*k).x;
		  double tempAngle = 0.5 * M_PI - (*k).ang;
		  nodeArray[i].pose_h = unwrap_angle(tempAngle);
		  
		  // HACK:  Magnus asked me to give the nodes distinct negative numbers so as not to interfere
		  // with some other part of the system. I don't understand, but I'll follow his request ....
		  nodeArray[i].index = -(i+1);
		  if ( (*j)->m_reverse )                                   // add forward/reverse info
		    { 
		      Log::getStream(9) << ", path is backward " << endl;
		      nodeArray[i].pathDir = GRAPH_PATH_REV; 
		    }
		  else
		    {
		      Log::getStream(9) << ", path is forward " << endl;
		      nodeArray[i].pathDir = GRAPH_PATH_FWD;  
		    };
		  path->curvatures[i] = (*j)->m_initCurv;                  // this is used by velocity planner
		  path->path[i] = &(nodeArray[i]);
		};
	    };
	  path->goalDist = pathLength;       // distance to goal is sum of clothoid lengths
	  path->pathLen = i;                 // total number of nodes in this path
	};
      Log::getStream(6) << "   CIRCLEPLANNER: finishing populateGraph " << endl;
    };
  return;
};



