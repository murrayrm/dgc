/*  unit test of Path Tree Planner components.  This is just a way to test components
    without having to interface to the rest of the system */

#include <stream.h>
#include "frames/pose2.hh"
#include "CirclePlanner.hh"
#include "AStar.hh"

using namespace std; 

int main()
{
  /* dummy start and goal for testing */
  pose2 startPose = pose2(5.0,5.0,0.0);
  pose2 goalPose   = pose2(50.0, 5.0, 0.7 * M_PI);

  // create an instance of the planner for testing.  This sets up a start node, and sets the start node ptr
  // to the starting pose node.  It also also sets the goal node (which is needed for the A* class).

  CirclePlanner * curPlanner = new CirclePlanner(startPose,goalPose);   
  curPlanner->makeCirclePlan();
  cerr << "CirclePlanner: " << curPlanner->m_nClothoids  << " clothoids were created " << endl;

  // dump out all of the paths to see how the planner does
  ofstream tree_file("clothoidTree.dat");
  curPlanner->m_startNodePtr->printAllPaths(&tree_file);

  // make an instance of AStarSearch and then set start and Goal states
  AStarSearch astarsearch;                              // Create an instance of the search class...
  astarsearch.SetStartAndGoalStates(curPlanner->m_startNodePtr, curPlanner->m_goalNodePtr); 
  unsigned int SearchState;                            // status of Search
  unsigned int SearchSteps=1;                          // how many "Search Steps" have we taken

  // main A* search loop that increments A* search via one pass through the Open List at each step.
  do
    {
      cerr << " A* Search Step: " << SearchSteps << endl;
      SearchState = astarsearch.SearchStep(curPlanner);                      // take one search step
      SearchSteps++;
    }
  while ((SearchState == AStarSearch::SEARCH_STATE_SEARCHING) && (SearchSteps < 10000));

  if(SearchState==AStarSearch::SEARCH_STATE_SUCCEEDED)
    {
      cerr << endl << " Search Found the Goal!" << endl;

      // print out solution path which is a vector of successive clothoids
      ofstream path_file("clothoidPath.dat");
      for(vector<Clothoid *>::reverse_iterator i=curPlanner->m_solutionPath.rbegin(); 
	  i != curPlanner->m_solutionPath.rend(); ++i)
	{  (*i)->printClothoid(&path_file);  };
    };

  cerr << " Total Search Nodes Allocated = " << astarsearch.m_AllocateNodeCount << endl;

  return 1;
}

