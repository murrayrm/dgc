/* CircleSplicer.cc: Methods to create and splice together circles as part of the graph construction 
 * needed by circleplanner.
 *
 * Written by Joel Burdick
 * October 1, 2007
 */

/* ------------------------------------------------------------------------------------- */
/*                                 Defines and Includes                                  */
/* ------------------------------------------------------------------------------------- */
#include "CirclePlanner.hh"

/* ------------------------------------------------------------------------------------- */
/*                                  Main Work Functions                                  */
/* ------------------------------------------------------------------------------------- */


// Compute the relevant bitangents for a pair of circles, where the first circle is one of the start circles,
// and the second circle IS NOT a goal circle.  Link the starting circle portion of the paths into the path 
// graph structure,and set up the second circle's incoming tangent data set.  Note, this function assumes 
// that both the start circle and the second circle have the same radius, RADIUS.

void CirclePlanner::StartCirclePair(point2 startCenter, CircleData * circle2, unsigned int handedness, double RADIUS)
{

  point2 center2 = circle2->m_center;
  float x12 = center2.x - startCenter.x;             // distances between centers of start and goal circle
  float y12 = center2.y - startCenter.y;
  float L12 = sqrt(x12 * x12 + y12 * y12);
  float phi12 = atan2(y12,x12);                      // angle made by vector from startCenter to GoalCenter

  const point2 xaxis = point2(1.0,0.0);              // the unit vector colinear with the coordinate system x-axis
  point2 v1(- RADIUS * sin(phi12), RADIUS * cos(phi12));             

  //  find the angle corresponding to the  robot's starting pose on the start circle
  point2 startLine = point2(m_startPose.x,m_startPose.y) - startCenter;
  float startAngle = FindTheta(xaxis,startLine);               // angle on start circle made by Alice's start pose

  // variables needed to construct and store paths 
  PTEdge * startForwardCircle1Ptr;      PTEdge * startReverseCircle1Ptr;
  PTEdge * startForwardCircle2Ptr;      PTEdge * startReverseCircle2Ptr;
  PTEdge * startForwardCircle3Ptr;      PTEdge * startReverseCircle3Ptr;
  PTEdge * startForwardCircle4Ptr;      PTEdge * startReverseCircle4Ptr;

  // if both the start and second circles are both right or left handed, only outer bitangents are relevant
  if ( (handedness == RIGHT_RIGHT_COMBO) || (handedness == LEFT_LEFT_COMBO) )
    {
      // first compute the points of contact of the two outer bitangents
      point2 z1a = startCenter + v1;         // location of start circle's first outer bitangent contact point
      point2 z1b = startCenter - v1;         // location of start circle's second outer bitangent contact point
      point2 z2a = center2  + v1;            // location of circle 2's first outer bitangent contact point
      point2 z2b = center2  - v1;            // location of circle 2's second outer bitangent contact point

      //-------------------------- paths and nodes related to the outer bitangents ------------------------

      // set up the paths on the starting and goal circles.  as well as the angles corresponding to the 
      // bitangent's contact points
 
      float thetaz1a = FindTheta(xaxis,v1);              // angle that bitangent at z1a makes
      float thetaz2a = thetaz1a;                         // angle that bitangent at z2a makes
      float thetaz1b = -thetaz1a;  float thetaz2b = thetaz1b;

      // make the simple straight line segment associated with the first outer bitangent. Also construct the two 
      // circular arcs that connect the staring pose to the bitangent point.

      PTNode * biTangent1StartNode;      PTNode * biTangent1EndNode;      PTEdge * biTangent1Ptr;          
      PTNode * biTangent2StartNode;      PTNode * biTangent2EndNode;      PTEdge * biTangent2Ptr;

      if (handedness == RIGHT_RIGHT_COMBO)
	{
	  // make segments and nodes associated with first bitangent
	  startForwardCircle1Ptr = makeForwardCircularArc(startCenter, startAngle, thetaz1a, RIGHT_HAND_CIRCLE, RADIUS);
	  startReverseCircle1Ptr = makeReverseCircularArc(startCenter, startAngle, thetaz1a, RIGHT_HAND_CIRCLE, RADIUS); 
	  biTangent1StartNode = new PTNode(pose2(z1a.x,z1a.y,thetaz1a - 0.5 * M_PI));
	  biTangent1Ptr = makeStraightLine(z1a,z2a,false);
	  biTangent1EndNode = new PTNode(pose2(z2a.x,z2a.y,thetaz2a - 0.5 * M_PI));

	  // save node at end of bitangent for later use
	  IncomingBiTangent * BiTangent1End = new IncomingBiTangent(biTangent1EndNode,biTangent1Ptr, false);
	  circle2->In.push_back(BiTangent1End);
	 
	  // make segments and nodes associated with second bitangent
	  startForwardCircle2Ptr = makeForwardCircularArc(startCenter, startAngle, thetaz1b, RIGHT_HAND_CIRCLE, RADIUS);
	  startReverseCircle2Ptr = makeReverseCircularArc(startCenter, startAngle, thetaz1b, RIGHT_HAND_CIRCLE, RADIUS); 
	  biTangent2StartNode = new PTNode(pose2(z1b.x,z1b.y,thetaz1b - 0.5 * M_PI));
	  biTangent2Ptr = makeStraightLine(z1b,z2b,true);
	  biTangent2EndNode = new PTNode(pose2(z2b.x,z2b.y,thetaz2b - 0.5 * M_PI));

	  IncomingBiTangent * BiTangent2End = new IncomingBiTangent(biTangent2EndNode,biTangent2Ptr, true);
	  circle2->In.push_back(BiTangent2End);
	 
	};
      if (handedness == LEFT_LEFT_COMBO)
	{
	  // make segments and nodes associated with first bitangent
	  startForwardCircle1Ptr = makeForwardCircularArc(startCenter, startAngle, thetaz1a, LEFT_HAND_CIRCLE, RADIUS);
	  startReverseCircle1Ptr = makeReverseCircularArc(startCenter, startAngle, thetaz1a, LEFT_HAND_CIRCLE, RADIUS);
	  biTangent1StartNode = new PTNode(pose2(z1a.x,z1a.y,thetaz1a + 0.5 * M_PI));
	  biTangent1Ptr = makeStraightLine(z1a,z2a,false); 
	  biTangent1EndNode = new PTNode(pose2(z2a.x,z2a.y,thetaz2a + 0.5 * M_PI));

	  // save node at end of bitangent for later use
	  IncomingBiTangent * BiTangent1End = new IncomingBiTangent(biTangent1EndNode,biTangent1Ptr, false);
	  circle2->In.push_back(BiTangent1End);

	  // make segments and nodes associated with second bitangent
	  startForwardCircle2Ptr = makeForwardCircularArc(startCenter, startAngle, thetaz1b, LEFT_HAND_CIRCLE, RADIUS);
	  startReverseCircle2Ptr = makeReverseCircularArc(startCenter, startAngle, thetaz1b, LEFT_HAND_CIRCLE, RADIUS);
	  biTangent2StartNode = new PTNode(pose2(z1b.x,z1b.y,thetaz1b + 0.5 * M_PI));
	  biTangent2Ptr = makeStraightLine(z1b,z2b,true); 
	  biTangent2EndNode = new PTNode(pose2(z2b.x,z2b.y,thetaz2b + 0.5 * M_PI));

	  IncomingBiTangent * BiTangent2End = new IncomingBiTangent(biTangent2EndNode,biTangent2Ptr, true);
	  circle2->In.push_back(BiTangent2End);
	};
      // now splice the pieces together, starting from the distal bitangent nodes

      // components associated with 1st bitangent
      biTangent1Ptr->m_childPTNodePtr = biTangent1EndNode;                // link bitangent to distal node 
      biTangent1StartNode->m_outEdges.push_back(biTangent1Ptr);           // link the start of the bitangent
      startForwardCircle1Ptr->m_childPTNodePtr = biTangent1StartNode;      // link a start circular arc to bitangent
      this->m_startNodePtr->m_outEdges.push_back(startForwardCircle1Ptr);  // link the start of the bi-tangent to 
      startReverseCircle1Ptr->m_childPTNodePtr = biTangent1StartNode;      // link a start circular arc to bitangent
      this->m_startNodePtr->m_outEdges.push_back(startReverseCircle1Ptr);

      // components associated with 2nd bitangent
      biTangent2Ptr->m_childPTNodePtr = biTangent2EndNode;                 // link bitangent to its distal node
      biTangent2StartNode->m_outEdges.push_back(biTangent2Ptr);            // link the start of the bitangent
      startForwardCircle2Ptr->m_childPTNodePtr = biTangent2StartNode;      // link a start circular arc to bitangent
      this->m_startNodePtr->m_outEdges.push_back(startForwardCircle2Ptr);  // link the start of the bi-tangent to 
      startReverseCircle2Ptr->m_childPTNodePtr = biTangent2StartNode;      // link a start circular arc to bitangent
      this->m_startNodePtr->m_outEdges.push_back(startReverseCircle2Ptr);
    };
  // for RIGHT_LEFT_COMBO or LEFT_RIGHT_COMBO, only the inner bitangents are active
  if ((handedness == RIGHT_LEFT_COMBO) || (handedness == LEFT_RIGHT_COMBO) )
    {   
      // first check if the circles overlap. If so, the inner bitangents do not exist, 
      if (L12 < (2.0 * RADIUS))
	{  return; };
      
      float coseta = 2 * RADIUS / L12;  
      float eta = acos(coseta);
      float psiPlus = phi12 + eta;  float psiMinus = phi12 - eta;
      float cPsiMinus = cos(psiMinus), sPsiMinus = sin(psiMinus);
      float cPsiPlus = cos(psiPlus), sPsiPlus = sin(psiPlus);

      // vectors n1 and n2 define the locations of the bi-tangent contact points on the 2nd circle's boundary
      point2 n1 = RADIUS * point2(cPsiPlus, sPsiPlus);       // related to first inner bitangent
      point2 n2 = RADIUS * point2(cPsiMinus, sPsiMinus);     // related to second inner bitangent

      // now create the points where the inner bi-tangents contact the first circle
      point2 y1a = startCenter + n1;  point2 y2a = center2 - n2;           // end points of 1st inner bitangent
      point2 y1b = startCenter + n2;  point2 y2b = center2 - n1;           // end points of 2nd inner bitangent
      float thetay1a = FindTheta(xaxis,n1);  
      float thetay1b = FindTheta(xaxis,n2);
      float thetay2a = FindTheta(xaxis,(-1.0 * n2));
      float thetay2b = FindTheta(xaxis,(-1.0 * n1));

      PTEdge * crossTangent1Ptr;            PTEdge * crossTangent2Ptr;
      PTNode * crossTangent1StartNode;      PTNode * crossTangent1EndNode;      
      PTNode * crossTangent2StartNode;      PTNode * crossTangent2EndNode;      

      if (handedness == RIGHT_LEFT_COMBO)
	{
	  // make segments and nodes associated with first inner bitangent
	  startForwardCircle1Ptr = makeForwardCircularArc(startCenter, startAngle, thetay1a, RIGHT_HAND_CIRCLE, RADIUS);
	  startReverseCircle1Ptr = makeReverseCircularArc(startCenter, startAngle, thetay1a, RIGHT_HAND_CIRCLE, RADIUS); 
	  crossTangent1StartNode = new PTNode(pose2(y1a.x,y1a.y,thetay1a - 0.5 * M_PI));
	  crossTangent1EndNode = new PTNode(pose2(y2b.x,y2b.y,thetay2b + 0.5 * M_PI));
	  crossTangent1Ptr = makeStraightLine(y1a,y2b,false);

	  // save node at end of bitangent for later use
	  IncomingBiTangent * crossTangent1End = new IncomingBiTangent(crossTangent1EndNode,crossTangent1Ptr, false);
	  circle2->In.push_back(crossTangent1End);

	  // make segments and nodes associated with second inner bitangent
	  startForwardCircle2Ptr = makeForwardCircularArc(startCenter, startAngle, thetay1b, RIGHT_HAND_CIRCLE, RADIUS);
	  startReverseCircle2Ptr = makeReverseCircularArc(startCenter, startAngle, thetay1b, RIGHT_HAND_CIRCLE, RADIUS); 
	  crossTangent2StartNode = new PTNode(pose2(y1b.x,y1b.y,thetay1b - 0.5 * M_PI));
	  crossTangent2Ptr = makeStraightLine(y1b,y2a,true);
	  crossTangent2EndNode = new PTNode(pose2(y2a.x,y2a.y,thetay2a +0.5 * M_PI));

	  // save node at end of bitangent for later use
	  IncomingBiTangent * crossTangent2End = new IncomingBiTangent(crossTangent2EndNode,crossTangent2Ptr, true);
	  circle2->In.push_back(crossTangent2End);
	}
      else     // else must be LEFT_RIGHT_COMBO 
	{
	  // make segments and nodes associated with first bitangent
	  startForwardCircle1Ptr = makeForwardCircularArc(startCenter, startAngle, thetay1a, LEFT_HAND_CIRCLE, RADIUS);
	  startReverseCircle1Ptr = makeReverseCircularArc(startCenter, startAngle, thetay1a, LEFT_HAND_CIRCLE, RADIUS);
	  crossTangent1StartNode = new PTNode(pose2(y1a.x,y1a.y,thetay1a + 0.5 * M_PI));
	  crossTangent1Ptr = makeStraightLine(y1a,y2b,true); 
	  crossTangent1EndNode = new PTNode(pose2(y2b.x,y2b.y,thetay2b - 0.5 * M_PI));

	  // save node at end of bitangent for later use
	  IncomingBiTangent * crossTangent1End = new IncomingBiTangent(crossTangent1EndNode,crossTangent1Ptr, false);
	  circle2->In.push_back(crossTangent1End);

	  // make segments and nodes associated with second bitangent
	  startForwardCircle2Ptr = makeForwardCircularArc(startCenter, startAngle, thetay1b, LEFT_HAND_CIRCLE, RADIUS);
	  startReverseCircle2Ptr = makeReverseCircularArc(startCenter, startAngle, thetay1b, LEFT_HAND_CIRCLE, RADIUS);
	  crossTangent2StartNode = new PTNode(pose2(y1b.x,y1b.y,thetay1b + 0.5 * M_PI));
	  crossTangent2Ptr = makeStraightLine(y1b,y2a,false); 
	  crossTangent2EndNode = new PTNode(pose2(y2a.x,y2a.y,thetay2a - 0.5 * M_PI));

	  // save node at end of bitangent for later use
	  IncomingBiTangent * crossTangent2End = new IncomingBiTangent(crossTangent2EndNode,crossTangent2Ptr, true);
	  circle2->In.push_back(crossTangent2End);
	};

      // splice all of the pieces together, starting from the tangent nodes on circle 2 and working backwards to the start.  

      // components associated with 1st bitangent
      crossTangent1Ptr->m_childPTNodePtr = crossTangent1EndNode;            // link node at end of bitangent to the bitangent
      crossTangent1StartNode->m_outEdges.push_back(crossTangent1Ptr);       // link the start of the bitangent

      startForwardCircle1Ptr->m_childPTNodePtr = crossTangent1StartNode;    // link one start circular arc to start of bitangent
      this->m_startNodePtr->m_outEdges.push_back(startForwardCircle1Ptr);   // link the start of the bi-tangent to 
      startReverseCircle1Ptr->m_childPTNodePtr = crossTangent1StartNode;    // link one start circular arc to start of bitangent
      this->m_startNodePtr->m_outEdges.push_back(startReverseCircle1Ptr);

      // components associated with 2nd bitangent
      crossTangent2Ptr->m_childPTNodePtr = crossTangent2EndNode;            // link node at end of bitangent to the bitangent
      crossTangent2StartNode->m_outEdges.push_back(crossTangent2Ptr);       // link the start of the bitangent

      startForwardCircle2Ptr->m_childPTNodePtr = crossTangent2StartNode;    // link one start circular arc to start of bitangent
      this->m_startNodePtr->m_outEdges.push_back(startForwardCircle2Ptr);   // link the start of the bi-tangent to 
      startReverseCircle2Ptr->m_childPTNodePtr = crossTangent2StartNode;    // link one start circular arc to start of bitangent
      this->m_startNodePtr->m_outEdges.push_back(startReverseCircle2Ptr);
    };					      

  // if the second circle is omnidirectional, then all 4 bitangents are potentially active 
  if ( (handedness == RIGHT_OMNI_COMBO) || (handedness == LEFT_OMNI_COMBO) )
    {
      //-------------------------- paths and nodes related to the outer bitangents ------------------------
 
      // set up the starting circle paths, as well as the angles corresponding to the bitangent's contact points
      float thetaz1a = FindTheta(xaxis,v1);              // angle that outer bitangent at z1a makes
      float thetaz2a = thetaz1a;                         // angle that outer bitangent at z2a makes
      float thetaz1b = -thetaz1a;  float thetaz2b = thetaz1b;

      // first compute the points of contact of the two outer bitangents
      point2 z1a = startCenter + v1;         // location of start circle's first outer bitangent contact point
      point2 z1b = startCenter - v1;         // location of start circle's second outer bitangent contact point
      point2 z2a = center2  + v1;            // location of circle 2's first outer bitangent contact point
      point2 z2b = center2  - v1;            // location of circle 2's second outer bitangent contact point

      //-------------------------- paths and nodes related to the inner bitangents ------------------------
      float coseta = 2 * RADIUS / L12;  
      float eta = acos(coseta);
      float psiPlus = phi12 + eta;  float psiMinus = phi12 - eta;
      float cPsiMinus = cos(psiMinus), sPsiMinus = sin(psiMinus);
      float cPsiPlus = cos(psiPlus), sPsiPlus = sin(psiPlus);

      // vectors n1 and n2 define the locations of the bi-tangent contact points on the 2nd circle's boundary
      point2 n1 = RADIUS * point2(cPsiPlus, sPsiPlus);       // related to first inner bitangent
      point2 n2 = RADIUS * point2(cPsiMinus, sPsiMinus);     // related to second inner bitangent

      // now create the points where the inner bi-tangents contact the first circle
      point2 y1a = startCenter + n1;  point2 y2a = center2 - n2;           // end points of 1st inner bitangent
      point2 y1b = startCenter + n2;  point2 y2b = center2 - n1;           // end points of 2nd inner bitangent
      float thetay1a = FindTheta(xaxis,n1);  
      float thetay1b = FindTheta(xaxis,n2);
      float thetay2a = FindTheta(xaxis,(-1.0 * n2));
      float thetay2b = FindTheta(xaxis,(-1.0 * n1));

      // make the simple straight line segment associated with the first outer bitangent. Also construct the two 
      // circular arcs that connect the staring pose to the bitangent point.

      PTNode * biTangent1StartNode;      PTNode * biTangent1EndNode;      PTEdge * biTangent1Ptr;          
      PTNode * biTangent2StartNode;      PTNode * biTangent2EndNode;      PTEdge * biTangent2Ptr;
      PTNode * crossTangent3StartNode;   PTNode * crossTangent3EndNode;   PTEdge * crossTangent3Ptr;
      PTNode * crossTangent4StartNode;      PTNode * crossTangent4EndNode;      PTEdge * crossTangent4Ptr;

      if (handedness == RIGHT_OMNI_COMBO)
	{
	  // make segments and nodes associated with first bitangent
	  startForwardCircle1Ptr = makeForwardCircularArc(startCenter, startAngle, thetaz1a, RIGHT_HAND_CIRCLE, RADIUS);
	  startReverseCircle1Ptr = makeReverseCircularArc(startCenter, startAngle, thetaz1a, RIGHT_HAND_CIRCLE, RADIUS); 
	  biTangent1StartNode = new PTNode(pose2(z1a.x,z1a.y,thetaz1a - 0.5 * M_PI));
	  biTangent1Ptr = makeStraightLine(z1a,z2a,false);
	  biTangent1EndNode = new PTNode(pose2(z2a.x,z2a.y,thetaz2a - 0.5 * M_PI));

	  // save node at end of bitangent1 for later use
	  IncomingBiTangent * BiTangent1End = new IncomingBiTangent(biTangent1EndNode,biTangent1Ptr, false);
	  circle2->In.push_back(BiTangent1End);
	 
	  // make segments and nodes associated with second bitangent
	  startForwardCircle2Ptr = makeForwardCircularArc(startCenter, startAngle, thetaz1b, RIGHT_HAND_CIRCLE, RADIUS);
	  startReverseCircle2Ptr = makeReverseCircularArc(startCenter, startAngle, thetaz1b, RIGHT_HAND_CIRCLE, RADIUS); 
	  biTangent2StartNode = new PTNode(pose2(z1b.x,z1b.y,thetaz1b - 0.5 * M_PI));
	  biTangent2Ptr = makeStraightLine(z1b,z2b,true);
	  biTangent2EndNode = new PTNode(pose2(z2b.x,z2b.y,thetaz2b - 0.5 * M_PI));

	  // save node at end of bitangent2 for later use
	  IncomingBiTangent * BiTangent2End = new IncomingBiTangent(biTangent2EndNode,biTangent2Ptr, true);
	  circle2->In.push_back(BiTangent2End);

	  // make segments and nodes associated with third (1st inner) bitangent
	  startForwardCircle3Ptr = makeForwardCircularArc(startCenter, startAngle, thetay1a, RIGHT_HAND_CIRCLE, RADIUS);
	  startReverseCircle3Ptr = makeReverseCircularArc(startCenter, startAngle, thetay1a, RIGHT_HAND_CIRCLE, RADIUS); 
	  crossTangent3StartNode = new PTNode(pose2(y1a.x,y1a.y,thetay1a - 0.5 * M_PI));
	  crossTangent3EndNode = new PTNode(pose2(y2b.x,y2b.y,thetay2b + 0.5 * M_PI));
	  crossTangent3Ptr = makeStraightLine(y1a,y2b,false);

	  // save node at end of bitangent3 for later use
	  IncomingBiTangent * crossTangent3End = new IncomingBiTangent(crossTangent3EndNode,crossTangent3Ptr, false);
	  circle2->In.push_back(crossTangent3End);

	  // make segments and nodes associated with fourth (2nd inner inner) bitangent
	  startForwardCircle4Ptr = makeForwardCircularArc(startCenter, startAngle, thetay1b, RIGHT_HAND_CIRCLE, RADIUS);
	  startReverseCircle4Ptr = makeReverseCircularArc(startCenter, startAngle, thetay1b, RIGHT_HAND_CIRCLE, RADIUS); 
	  crossTangent4StartNode = new PTNode(pose2(y1b.x,y1b.y,thetay1b - 0.5 * M_PI));
	  crossTangent4Ptr = makeStraightLine(y1b,y2a,true);
	  crossTangent4EndNode = new PTNode(pose2(y2a.x,y2a.y,thetay2a +0.5 * M_PI));

	  // save node at end of bitangent 4 for later use
	  IncomingBiTangent * crossTangent4End = new IncomingBiTangent(crossTangent4EndNode,crossTangent4Ptr, true);
	  circle2->In.push_back(crossTangent4End);
	};
      if (handedness == LEFT_OMNI_COMBO)
	{
	  // make segments and nodes associated with first bitangent
	  startForwardCircle1Ptr = makeForwardCircularArc(startCenter, startAngle, thetaz1a, LEFT_HAND_CIRCLE, RADIUS);
	  startReverseCircle1Ptr = makeReverseCircularArc(startCenter, startAngle, thetaz1a, LEFT_HAND_CIRCLE, RADIUS);
	  biTangent1StartNode = new PTNode(pose2(z1a.x,z1a.y,thetaz1a + 0.5 * M_PI));
	  biTangent1Ptr = makeStraightLine(z1a,z2a,false); 
	  biTangent1EndNode = new PTNode(pose2(z2a.x,z2a.y,thetaz2a + 0.5 * M_PI));

	  // save node at end of bitangent for later use
	  IncomingBiTangent * BiTangent1End = new IncomingBiTangent(biTangent1EndNode,biTangent1Ptr, false);
	  circle2->In.push_back(BiTangent1End);

	  // make segments and nodes associated with second bitangent
	  startForwardCircle2Ptr = makeForwardCircularArc(startCenter, startAngle, thetaz1b, LEFT_HAND_CIRCLE, RADIUS);
	  startReverseCircle2Ptr = makeReverseCircularArc(startCenter, startAngle, thetaz1b, LEFT_HAND_CIRCLE, RADIUS);
	  biTangent2StartNode = new PTNode(pose2(z1b.x,z1b.y,thetaz1b + 0.5 * M_PI));
	  biTangent2Ptr = makeStraightLine(z1b,z2b,true); 
	  biTangent2EndNode = new PTNode(pose2(z2b.x,z2b.y,thetaz2b + 0.5 * M_PI));

	  IncomingBiTangent * BiTangent2End = new IncomingBiTangent(biTangent2EndNode,biTangent2Ptr, true);
	  circle2->In.push_back(BiTangent2End);

	  // make segments and nodes associated with first bitangent
	  startForwardCircle3Ptr = makeForwardCircularArc(startCenter, startAngle, thetay1a, LEFT_HAND_CIRCLE, RADIUS);
	  startReverseCircle3Ptr = makeReverseCircularArc(startCenter, startAngle, thetay1a, LEFT_HAND_CIRCLE, RADIUS);
	  crossTangent3StartNode = new PTNode(pose2(y1a.x,y1a.y,thetay1a + 0.5 * M_PI));
	  crossTangent3Ptr = makeStraightLine(y1a,y2b,true); 
	  crossTangent3EndNode = new PTNode(pose2(y2b.x,y2b.y,thetay2b - 0.5 * M_PI));

	  // save node at end of bitangent for later use
	  IncomingBiTangent * crossTangent3End = new IncomingBiTangent(crossTangent3EndNode,crossTangent3Ptr, false);
	  circle2->In.push_back(crossTangent3End);

	  // make segments and nodes associated with second bitangent
	  startForwardCircle4Ptr = makeForwardCircularArc(startCenter, startAngle, thetay1b, LEFT_HAND_CIRCLE, RADIUS);
	  startReverseCircle4Ptr = makeReverseCircularArc(startCenter, startAngle, thetay1b, LEFT_HAND_CIRCLE, RADIUS);
	  crossTangent4StartNode = new PTNode(pose2(y1b.x,y1b.y,thetay1b + 0.5 * M_PI));
	  crossTangent4Ptr = makeStraightLine(y1b,y2a,false); 
	  crossTangent4EndNode = new PTNode(pose2(y2a.x,y2a.y,thetay2a - 0.5 * M_PI));

	  // save node at end of bitangent for later use
	  IncomingBiTangent * crossTangent4End = new IncomingBiTangent(crossTangent4EndNode,crossTangent4Ptr, true);
	  circle2->In.push_back(crossTangent4End);

	};
      // now splice the pieces together, starting from the distal bitangent nodes

      // components associated with 1st bitangent
      biTangent1Ptr->m_childPTNodePtr = biTangent1EndNode;                // link bitangent to distal node 
      biTangent1StartNode->m_outEdges.push_back(biTangent1Ptr);           // link the start of the bitangent
      startForwardCircle1Ptr->m_childPTNodePtr = biTangent1StartNode;      // link a start circular arc to bitangent
      this->m_startNodePtr->m_outEdges.push_back(startForwardCircle1Ptr);  // link the start of the bi-tangent to 
      startReverseCircle1Ptr->m_childPTNodePtr = biTangent1StartNode;      // link a start circular arc to bitangent
      this->m_startNodePtr->m_outEdges.push_back(startReverseCircle1Ptr);

      // components associated with 2nd bitangent
      biTangent2Ptr->m_childPTNodePtr = biTangent2EndNode;                 // link bitangent to its distal node
      biTangent2StartNode->m_outEdges.push_back(biTangent2Ptr);            // link the start of the bitangent
      startForwardCircle2Ptr->m_childPTNodePtr = biTangent2StartNode;      // link a start circular arc to bitangent
      this->m_startNodePtr->m_outEdges.push_back(startForwardCircle2Ptr);  // link the start of the bi-tangent to 
      startReverseCircle2Ptr->m_childPTNodePtr = biTangent2StartNode;      // link a start circular arc to bitangent
      this->m_startNodePtr->m_outEdges.push_back(startReverseCircle2Ptr);

      // components associated with 3rd bitangent
      crossTangent3Ptr->m_childPTNodePtr = crossTangent3EndNode;            // link node at end of bitangent to the bitangent
      crossTangent3StartNode->m_outEdges.push_back(crossTangent3Ptr);       // link the start of the bitangent

      startForwardCircle3Ptr->m_childPTNodePtr = crossTangent3StartNode;    // link one start circular arc to start of bitangent
      this->m_startNodePtr->m_outEdges.push_back(startForwardCircle3Ptr);   // link the start of the bi-tangent to 
      startReverseCircle1Ptr->m_childPTNodePtr = crossTangent3StartNode;    // link one start circular arc to start of bitangent
      this->m_startNodePtr->m_outEdges.push_back(startReverseCircle3Ptr);

      // components associated with 4th bitangent
      crossTangent4Ptr->m_childPTNodePtr = crossTangent4EndNode;            // link node at end of bitangent to the bitangent
      crossTangent4StartNode->m_outEdges.push_back(crossTangent4Ptr);       // link the start of the bitangent

      startForwardCircle4Ptr->m_childPTNodePtr = crossTangent4StartNode;    // link one start circular arc to start of bitangent
      this->m_startNodePtr->m_outEdges.push_back(startForwardCircle4Ptr);   // link the start of the bi-tangent to 
      startReverseCircle4Ptr->m_childPTNodePtr = crossTangent4StartNode;    // link one start circular arc to start of bitangent
      this->m_startNodePtr->m_outEdges.push_back(startReverseCircle4Ptr);

    };
  return;
};

//---------------------------------------------------------------------------------------------------------------
// Compute the relevant bitangents for a pair of circles, where the first circle is neither a start or goal 
// circle, and the second circle IS a goal circle.  Note, this function assumes  that both circles have the 
// same radius, RADIUS.
//---------------------------------------------------------------------------------------------------------------

void CirclePlanner::CircleGoalPair(CircleData * circle1, point2 goalCenter, unsigned int handedness, double RADIUS)
{
  point2 center1 = circle1->m_center;
  float x12 = goalCenter.x - center1.x;          // distances between centers of start and goal circle
  float y12 = goalCenter.y - center1.y;
  float L12 = sqrt(x12 * x12 + y12 * y12);
  float phi12 = atan2(y12,x12);                      // angle made by vector from startCenter to GoalCenter

  const point2 xaxis = point2(1.0,0.0);              // the unit vector colinear with the x-axis
  point2 v1(- RADIUS * sin(phi12), RADIUS * cos(phi12));             

  //  find the angles corresponding to the  robot's goal pose
  point2 goalLine = point2(m_goalPose.x,m_goalPose.y) - goalCenter;
  float goalAngle = FindTheta(xaxis,goalLine);       // angle made on goal circle by Alice's goal pose

  // variables needed to construct and store paths 
  PTEdge * goalForwardCircle1Ptr;       PTEdge * goalReverseCircle1Ptr;
  PTEdge * goalForwardCircle2Ptr;       PTEdge * goalReverseCircle2Ptr;
  PTEdge * goalForwardCircle3Ptr;       PTEdge * goalReverseCircle3Ptr;
  PTEdge * goalForwardCircle4Ptr;       PTEdge * goalReverseCircle4Ptr;

  //--------------------------------------------------------------------------------------------------------
  // if both the first circle  and goal circle have the same handedness, only the outer bitangents are relevant
  if ( (handedness == RIGHT_RIGHT_COMBO) || (handedness == LEFT_LEFT_COMBO) )
    {
      // first compute the points of contact of the two outer bitangents
      point2 z1a = center1 + v1;             // location of circle 1's first outer bitangent contact point
      point2 z1b = center1 - v1;             // location of circle 1's second outer bitangent contact point
      point2 z2a = goalCenter  + v1;         // location of first outer bitangent contact point on goal circle
      point2 z2b = goalCenter  - v1;         // location of second outer bitangent contact point on goal circle

      //-------------------------- paths and nodes related to the outer bitangents ------------------------

      // set up the paths on the starting and goal circles.  as well as the angles corresponding to the 
      // bitangent's contact points
 
      float thetaz1a = FindTheta(xaxis,v1);              // angle that bitangent at z1a makes
      float thetaz2a = thetaz1a;                         // angle that bitangent at z2a makes
      float thetaz1b = -thetaz1a;  float thetaz2b = thetaz1b;

      // make the simple straight line segment associated with the first outer bitangent.  We'll link to
      // it in a moment. Also construct the two circular arcs that connect the staring pose to the 
      // bitangent point, and the distal bitangent to the goal pose.

      PTNode * biTangent1StartNode;      PTNode * biTangent1EndNode;      PTEdge * biTangent1Ptr;          
      PTNode * biTangent2StartNode;      PTNode * biTangent2EndNode;      PTEdge * biTangent2Ptr;

      if (handedness == RIGHT_RIGHT_COMBO)
	{
	  // make segments and nodes associated with first bitangent
	  biTangent1StartNode = new PTNode(pose2(z1a.x,z1a.y,thetaz1a - 0.5 * M_PI));
	  biTangent1Ptr = makeStraightLine(z1a,z2a,false);
	  biTangent1EndNode = new PTNode(pose2(z2a.x,z2a.y,thetaz2a - 0.5 * M_PI));
	  goalForwardCircle1Ptr = makeForwardCircularArc(goalCenter, thetaz2a, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle1Ptr = makeReverseCircularArc(goalCenter, thetaz2a, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);

	  // save this tangent point on the first circle for later splicing
	  OutgoingBiTangent * biTangent1 = new OutgoingBiTangent(biTangent1StartNode, false);
	  circle1->Out.push_back(biTangent1);

	  // make segments and nodes associated with second bitangent
	  biTangent2StartNode = new PTNode(pose2(z1b.x,z1b.y,thetaz1b - 0.5 * M_PI));
	  biTangent2Ptr = makeStraightLine(z1b,z2b,true);
	  biTangent2EndNode = new PTNode(pose2(z2b.x,z2b.y,thetaz2b - 0.5 * M_PI));
	  goalForwardCircle2Ptr = makeForwardCircularArc(goalCenter, thetaz2b, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle2Ptr = makeReverseCircularArc(goalCenter, thetaz2b, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);

	  // save this second tangent point on first circle for later splicing
	  OutgoingBiTangent * biTangent2 = new OutgoingBiTangent(biTangent2StartNode, true);
	  circle1->Out.push_back(biTangent2);
	}
      else   // else we have a LEFT_LEFT_COMBO
	{
	  // make segments and nodes associated with first bitangent
	  biTangent1StartNode = new PTNode(pose2(z1a.x,z1a.y,thetaz1a + 0.5 * M_PI));
	  biTangent1Ptr = makeStraightLine(z1a,z2a,false); 
	  biTangent1EndNode = new PTNode(pose2(z2a.x,z2a.y,thetaz2a + 0.5 * M_PI));
	  goalForwardCircle1Ptr = makeForwardCircularArc(goalCenter, thetaz2a, goalAngle, LEFT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle1Ptr = makeReverseCircularArc(goalCenter, thetaz2a, goalAngle, LEFT_HAND_CIRCLE, RADIUS);

	  // save this first tangent point on first circle for later splicing
	  OutgoingBiTangent * biTangent1 = new OutgoingBiTangent(biTangent1StartNode, false);
	  circle1->Out.push_back(biTangent1);

	  // make segments and nodes associated with second bitangent
	  biTangent2StartNode = new PTNode(pose2(z1b.x,z1b.y,thetaz1b + 0.5 * M_PI));
	  biTangent2Ptr = makeStraightLine(z1b,z2b,true); 
	  biTangent2EndNode = new PTNode(pose2(z2b.x,z2b.y,thetaz2b + 0.5 * M_PI));
	  goalForwardCircle2Ptr = makeForwardCircularArc(goalCenter, thetaz2b, goalAngle, LEFT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle2Ptr = makeReverseCircularArc(goalCenter, thetaz2b, goalAngle, LEFT_HAND_CIRCLE, RADIUS);

	  // save this second tangent point on the first circle for later splicing
	  OutgoingBiTangent * biTangent2 = new OutgoingBiTangent(biTangent2StartNode, true);
	  circle1->Out.push_back(biTangent2);
	};

      // now splice all of the pieces together, starting from the goal and working backwards

      // components associated with 1st bitangent
      goalForwardCircle1Ptr->m_childPTNodePtr = this->m_goalNodePtr;      // link goal to this edge
      biTangent1EndNode->m_outEdges.push_back(goalForwardCircle1Ptr);     // link one of the goal circular edges

      goalReverseCircle1Ptr->m_childPTNodePtr = this->m_goalNodePtr;      // link goal to this edge
      biTangent1EndNode->m_outEdges.push_back(goalReverseCircle1Ptr);     // link the other goal circular edge

      biTangent1Ptr->m_childPTNodePtr = biTangent1EndNode;                // link bitangent to distal node 
      biTangent1StartNode->m_outEdges.push_back(biTangent1Ptr);           // link the start of the bitangent

      // components associated with 2nd bitangent
      goalForwardCircle2Ptr->m_childPTNodePtr = this->m_goalNodePtr;      // link goal to this edge
      biTangent2EndNode->m_outEdges.push_back(goalForwardCircle2Ptr);     // link one of the goal circular edges

      goalReverseCircle2Ptr->m_childPTNodePtr = this->m_goalNodePtr;      // link goal to this edge
      biTangent2EndNode->m_outEdges.push_back(goalReverseCircle2Ptr);     // link the other goal circular edge

      biTangent2Ptr->m_childPTNodePtr = biTangent2EndNode;                // link bitangent to its distal node
      biTangent2StartNode->m_outEdges.push_back(biTangent2Ptr);           // link the start of the bitangent
    };
  //--------------------------------------------------------------------------------------------------------
  // if the first circle and the goal circle comprise RIGHT_LEFT_COMBO or LEFT_RIGHT_COMBO, then
  // only the inner bitangents are active.  But, these will only be active if the two circles do not overlap
  if ((handedness == RIGHT_LEFT_COMBO) || (handedness == LEFT_RIGHT_COMBO) )
    {   
      // first check if the circles overlap. If so, the inner bitangents do not exist, 
      if (L12 > (2.0 * RADIUS))
	{  return; };
      
      float coseta = 2 * RADIUS / L12;  
      float eta = acos(coseta);
      float psiPlus = phi12 + eta;  float psiMinus = phi12 - eta;
      float cPsiMinus = cos(psiMinus), sPsiMinus = sin(psiMinus);
      float cPsiPlus = cos(psiPlus), sPsiPlus = sin(psiPlus);

      // vectors n1 and n2 define the locations of the bi-tangent contact points
      point2 n1 = RADIUS * point2(cPsiPlus, sPsiPlus);       // related to first inner bitangent
      point2 n2 = RADIUS * point2(cPsiMinus, sPsiMinus);     // related to second inner bitangent

      // now create the points where the inner bi-tangents contact the first circle
      point2 y1a = center1 + n1;  point2 y2a = goalCenter - n2;           // end points of 1st inner bitangent
      point2 y1b = center1 + n2;  point2 y2b = goalCenter - n1;           // end points of 2nd inner bitangent
      float thetay1a = FindTheta(xaxis,n1);  
      float thetay1b = FindTheta(xaxis,n2);
      float thetay2a = FindTheta(xaxis,(-1.0 * n2));
      float thetay2b = FindTheta(xaxis,(-1.0 * n1));

      PTEdge * crossTangent1Ptr;            PTEdge * crossTangent2Ptr;
      PTNode * crossTangent1StartNode;      PTNode * crossTangent1EndNode;      
      PTNode * crossTangent2StartNode;      PTNode * crossTangent2EndNode;      

      if (handedness == RIGHT_LEFT_COMBO)
	{
	  // make segments and nodes associated with first inner bitangent
	  crossTangent1StartNode = new PTNode(pose2(y1a.x,y1a.y,thetay1a - 0.5 * M_PI));
	  crossTangent1EndNode = new PTNode(pose2(y2b.x,y2b.y,thetay2b + 0.5 * M_PI));
	  crossTangent1Ptr = makeStraightLine(y1a,y2b,false);
	  goalForwardCircle1Ptr = makeForwardCircularArc(goalCenter, thetay2b, goalAngle, LEFT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle1Ptr = makeReverseCircularArc(goalCenter, thetay2b, goalAngle, LEFT_HAND_CIRCLE, RADIUS);

	  // save this tangent point on the first circle for later splicing
	  OutgoingBiTangent * biTangent1 = new OutgoingBiTangent(crossTangent1StartNode, false);
	  circle1->Out.push_back(biTangent1);

	  // make segments and nodes associated with second inner bitangent
	  crossTangent2StartNode = new PTNode(pose2(y1b.x,y1b.y,thetay1b - 0.5 * M_PI));
	  crossTangent2Ptr = makeStraightLine(y1b,y2a,true);
	  crossTangent2EndNode = new PTNode(pose2(y2a.x,y2a.y,thetay2a +0.5 * M_PI));
	  goalForwardCircle2Ptr = makeForwardCircularArc(goalCenter, thetay2a, goalAngle, LEFT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle2Ptr = makeReverseCircularArc(goalCenter, thetay2a, goalAngle, LEFT_HAND_CIRCLE, RADIUS);

	  // save the second tangent point on the first circle for later splicing
	  OutgoingBiTangent * biTangent2 = new OutgoingBiTangent(crossTangent2StartNode, false);
	  circle1->Out.push_back(biTangent2);

	}
      else     // else must be LEFT_RIGHT_COMBO 
	{
	  // make segments and nodes associated with first bitangent
	  crossTangent1StartNode = new PTNode(pose2(y1a.x,y1a.y,thetay1a + 0.5 * M_PI));
	  crossTangent1Ptr = makeStraightLine(y1a,y2b,true); 
	  crossTangent1EndNode = new PTNode(pose2(y2b.x,y2b.y,thetay2b - 0.5 * M_PI));
	  goalForwardCircle1Ptr = makeForwardCircularArc(goalCenter, thetay2b, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle1Ptr = makeReverseCircularArc(goalCenter, thetay2b, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);

	  // save this first tangent point on first circle for later splicing
	  OutgoingBiTangent * biTangent1 = new OutgoingBiTangent(crossTangent1StartNode, false);
	  circle1->Out.push_back(biTangent1);

	  // make segments and nodes associated with second bitangent
	  crossTangent2StartNode = new PTNode(pose2(y1b.x,y1b.y,thetay1b + 0.5 * M_PI));
	  crossTangent2Ptr = makeStraightLine(y1b,y2a,false); 
	  crossTangent2EndNode = new PTNode(pose2(y2a.x,y2a.y,thetay2a - 0.5 * M_PI));
	  goalForwardCircle2Ptr = makeForwardCircularArc(goalCenter, thetay2a, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle2Ptr = makeReverseCircularArc(goalCenter, thetay2a, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);

	  // save the second tangent point on first circle for later splicing
	  OutgoingBiTangent * biTangent2 = new OutgoingBiTangent(crossTangent2StartNode, false);
	  circle1->Out.push_back(biTangent2);
	};
      // splice all of the pieces together, starting from the goal and working backwards to the start.  

      // components associated with 1st bitangent
      goalForwardCircle1Ptr->m_childPTNodePtr = this->m_goalNodePtr;        // link goal to this edge
      crossTangent1EndNode->m_outEdges.push_back(goalForwardCircle1Ptr);    // link one of the goal circular edges

      goalReverseCircle1Ptr->m_childPTNodePtr = this->m_goalNodePtr;        // link goal to this edge
      crossTangent1EndNode->m_outEdges.push_back(goalReverseCircle1Ptr);    // link the other goal circular edge

      crossTangent1Ptr->m_childPTNodePtr = crossTangent1EndNode;            // link node at end of bitangent to the bitangent
      crossTangent1StartNode->m_outEdges.push_back(crossTangent1Ptr);       // link the start of the bitangent

      // components associated with 2nd bitangent
      goalForwardCircle2Ptr->m_childPTNodePtr = this->m_goalNodePtr;        // link goal to this edge
      crossTangent2EndNode->m_outEdges.push_back(goalForwardCircle2Ptr);    // link one of the goal circular edges

      goalReverseCircle2Ptr->m_childPTNodePtr = this->m_goalNodePtr;        // link goal to this edge
      crossTangent2EndNode->m_outEdges.push_back(goalReverseCircle2Ptr);    // link the other goal circular edge

      crossTangent2Ptr->m_childPTNodePtr = crossTangent2EndNode;            // link node at end of bitangent to the bitangent
      crossTangent2StartNode->m_outEdges.push_back(crossTangent2Ptr);       // link the start of the bitangent
    };					      

  //--------------------------------------------------------------------------------------------------------
  // if the first circle is Omnidirectional, then all four bitangents are potentially active for a
  // OMNI_RIGHT_COMBO or OMNI_LEFT_COMBO.  Note, a goal circle can never be omnidirectional
  if ((handedness == OMNI_RIGHT_COMBO) || (handedness == OMNI_LEFT_COMBO) )
    {   
      // first compute the points of contact of the two outer bitangents
      point2 z1a = center1 + v1;         // location of start circle's first outer bitangent contact point
      point2 z1b = center1 - v1;         // location of start circle's second outer bitangent contact point
      point2 z2a = goalCenter  + v1;            // location of circle 2's first outer bitangent contact point
      point2 z2b = goalCenter  - v1;            // location of circle 2's second outer bitangent contact point

      //-------------------------- paths and nodes related to the outer bitangents ------------------------

      // set up the paths on the starting and goal circles.  as well as the angles corresponding to the 
      // bitangent's contact points
 
      float thetaz1a = FindTheta(xaxis,v1);              // angle that bitangent at z1a makes
      float thetaz2a = thetaz1a;                         // angle that bitangent at z2a makes
      float thetaz1b = -thetaz1a;  float thetaz2b = thetaz1b;

      //-------------------------- paths and nodes related to the inner bitangents ------------------------
      float coseta = 2 * RADIUS / L12;  
      float eta = acos(coseta);
      float psiPlus = phi12 + eta;  float psiMinus = phi12 - eta;
      float cPsiMinus = cos(psiMinus), sPsiMinus = sin(psiMinus);
      float cPsiPlus = cos(psiPlus), sPsiPlus = sin(psiPlus);

      // vectors n1 and n2 define the locations of the bi-tangent contact points on the goal circle's boundary
      point2 n1 = RADIUS * point2(cPsiPlus, sPsiPlus);       // related to first inner bitangent
      point2 n2 = RADIUS * point2(cPsiMinus, sPsiMinus);     // related to second inner bitangent

      // now create the points where the inner bi-tangents contact the first circle
      point2 y1a = center1 + n1;  point2 y2a = goalCenter - n2;           // end points of 1st inner bitangent
      point2 y1b = center1 + n2;  point2 y2b = goalCenter - n1;           // end points of 2nd inner bitangent
      float thetay1a = FindTheta(xaxis,n1);  
      float thetay1b = FindTheta(xaxis,n2);
      float thetay2a = FindTheta(xaxis,(-1.0 * n2));
      float thetay2b = FindTheta(xaxis,(-1.0 * n1));

      PTNode * biTangent1StartNode;      PTNode * biTangent1EndNode;      PTEdge * biTangent1Ptr;          
      PTNode * biTangent2StartNode;      PTNode * biTangent2EndNode;      PTEdge * biTangent2Ptr;
      PTNode * crossTangent3StartNode;      PTNode * crossTangent3EndNode;      PTEdge * crossTangent3Ptr;
      PTNode * crossTangent4StartNode;      PTNode * crossTangent4EndNode;      PTEdge * crossTangent4Ptr;

      // this case in effect has the two out bitangents from a RIGHT_RIGHT_COMBO and the two inner bitangents
      // from a LEFT_RIGHT_COMBO.
      if (handedness == OMNI_RIGHT_COMBO)
	{
	  // make segments and nodes associated with first (outer) bitangent
	  biTangent1StartNode = new PTNode(pose2(z1a.x,z1a.y,thetaz1a - 0.5 * M_PI));
	  biTangent1Ptr = makeStraightLine(z1a,z2a,false);
	  biTangent1EndNode = new PTNode(pose2(z2a.x,z2a.y,thetaz2a - 0.5 * M_PI));
	  goalForwardCircle1Ptr = makeForwardCircularArc(goalCenter, thetaz2a, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle1Ptr = makeReverseCircularArc(goalCenter, thetaz2a, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);

	  // save the first outer bitangent point on the first circle for later splicing
	  OutgoingBiTangent * biTangent1 = new OutgoingBiTangent(biTangent1StartNode, false);
	  circle1->Out.push_back(biTangent1);

	  // make segments and nodes associated with the second (outer) bitangent
	  biTangent2StartNode = new PTNode(pose2(z1b.x,z1b.y,thetaz1b - 0.5 * M_PI));
	  biTangent2Ptr = makeStraightLine(z1b,z2b,true);
	  biTangent2EndNode = new PTNode(pose2(z2b.x,z2b.y,thetaz2b - 0.5 * M_PI));
	  goalForwardCircle2Ptr = makeForwardCircularArc(goalCenter, thetaz2b, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle2Ptr = makeReverseCircularArc(goalCenter, thetaz2b, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);

	  // save the second outer bitangent point on the first circle for later splicing
	  OutgoingBiTangent * biTangent2 = new OutgoingBiTangent(biTangent2StartNode, false);
	  circle1->Out.push_back(biTangent2);

	  //  before building the inner bitangents, first check if the circles overlap. If so, the inner 
	  // bitangents do not exist, 
	  if (L12 > (2.0 * RADIUS))                                // if true, then circles do not overlap.
	    {
	      // make segments and nodes associated with third bitangent (first inner bitangent)
	      crossTangent3StartNode = new PTNode(pose2(y1a.x,y1a.y,thetay1a + 0.5 * M_PI));
	      crossTangent3Ptr = makeStraightLine(y1a,y2b,true); 
	      crossTangent3EndNode = new PTNode(pose2(y2b.x,y2b.y,thetay2b - 0.5 * M_PI));
	      goalForwardCircle3Ptr = makeForwardCircularArc(goalCenter, thetay2b, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);
	      goalReverseCircle3Ptr = makeReverseCircularArc(goalCenter, thetay2b, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);

	      // save the third bitangent point on the first circle for later splicing
	      OutgoingBiTangent * biTangent3 = new OutgoingBiTangent(crossTangent3StartNode, false);
	      circle1->Out.push_back(biTangent3);

	      // make segments and nodes associated with fourth bitangent (second inner bitangent)
	      crossTangent4StartNode = new PTNode(pose2(y1b.x,y1b.y,thetay1b + 0.5 * M_PI));
	      crossTangent4Ptr = makeStraightLine(y1b,y2a,false); 
	      crossTangent4EndNode = new PTNode(pose2(y2a.x,y2a.y,thetay2a - 0.5 * M_PI));
	      goalForwardCircle4Ptr = makeForwardCircularArc(goalCenter, thetay2a, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);
	      goalReverseCircle4Ptr = makeReverseCircularArc(goalCenter, thetay2a, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);

	      // save the fourth bitangent point on the first circle for later splicing
	      OutgoingBiTangent * biTangent4 = new OutgoingBiTangent(crossTangent4StartNode, false);
	      circle1->Out.push_back(biTangent4);
	    };
	}
      else   // else must be the case of a OMNI_LEFT_COMBO, which has two outer bitangents from a LEFT_LEFT_COMBO, 
	{    // and possibly the inner bitangents from a RIGHT_LEFT_COMBO

	  // make segments and nodes associated with first (outer) bitangent
	  biTangent1StartNode = new PTNode(pose2(z1a.x,z1a.y,thetaz1a + 0.5 * M_PI));
	  biTangent1Ptr = makeStraightLine(z1a,z2a,false); 
	  biTangent1EndNode = new PTNode(pose2(z2a.x,z2a.y,thetaz2a + 0.5 * M_PI));
	  goalForwardCircle1Ptr = makeForwardCircularArc(goalCenter, thetaz2a, goalAngle, LEFT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle1Ptr = makeReverseCircularArc(goalCenter, thetaz2a, goalAngle, LEFT_HAND_CIRCLE, RADIUS);

	  // save the first outer bitangent point on the first circle for later splicing
	  OutgoingBiTangent * biTangent1 = new OutgoingBiTangent(biTangent1StartNode, false);
	  circle1->Out.push_back(biTangent1);

	  // make segments and nodes associated with second bitangent
	  biTangent2StartNode = new PTNode(pose2(z1b.x,z1b.y,thetaz1b + 0.5 * M_PI));
	  biTangent2Ptr = makeStraightLine(z1b,z2b,true); 
	  biTangent2EndNode = new PTNode(pose2(z2b.x,z2b.y,thetaz2b + 0.5 * M_PI));
	  goalForwardCircle2Ptr = makeForwardCircularArc(goalCenter, thetaz2b, goalAngle, LEFT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle2Ptr = makeReverseCircularArc(goalCenter, thetaz2b, goalAngle, LEFT_HAND_CIRCLE, RADIUS);
	  
	  // save the second outer bitangent point on the first circle for later splicing
	  OutgoingBiTangent * biTangent2 = new OutgoingBiTangent(biTangent2StartNode, false);
	  circle1->Out.push_back(biTangent2);

	  //  before building the inner bitangents, first check if the circles overlap. If so, the inner 
	  // bitangents do not exist, 
	  if (L12 > (2.0 * RADIUS))                                // if true, then circles do not overlap.
	    {
	      // make segments and nodes associated with third (first inner) bitangent
	      crossTangent3StartNode = new PTNode(pose2(y1a.x,y1a.y,thetay1a - 0.5 * M_PI));
	      crossTangent3EndNode = new PTNode(pose2(y2b.x,y2b.y,thetay2b + 0.5 * M_PI));
	      crossTangent3Ptr = makeStraightLine(y1a,y2b,false);
	      goalForwardCircle3Ptr = makeForwardCircularArc(goalCenter, thetay2b, goalAngle, LEFT_HAND_CIRCLE, RADIUS);
	      goalReverseCircle3Ptr = makeReverseCircularArc(goalCenter, thetay2b, goalAngle, LEFT_HAND_CIRCLE, RADIUS);

	      // save the third bitangent point on the first circle for later splicing
	      OutgoingBiTangent * biTangent3 = new OutgoingBiTangent(crossTangent3StartNode, false);
	      circle1->Out.push_back(biTangent3);

	      // make segments and nodes associated with fourth (second inner) bitangent
	      crossTangent4StartNode = new PTNode(pose2(y1b.x,y1b.y,thetay1b - 0.5 * M_PI));
	      crossTangent4Ptr = makeStraightLine(y1b,y2a,true);
	      crossTangent4EndNode = new PTNode(pose2(y2a.x,y2a.y,thetay2a +0.5 * M_PI));
	      goalForwardCircle4Ptr = makeForwardCircularArc(goalCenter, thetay2a, goalAngle, LEFT_HAND_CIRCLE, RADIUS);
	      goalReverseCircle4Ptr = makeReverseCircularArc(goalCenter, thetay2a, goalAngle, LEFT_HAND_CIRCLE, RADIUS);

	      // save the fourth bitangent point on the first circle for later splicing
	      OutgoingBiTangent * biTangent4 = new OutgoingBiTangent(crossTangent4StartNode, false);
	      circle1->Out.push_back(biTangent4);
	    };
	};

      // now splice all of the pieces together, starting from the goal and working backwards to the start.  

      // components associated with 1st outer bitangent
      goalForwardCircle1Ptr->m_childPTNodePtr = this->m_goalNodePtr;      // link goal to this edge
      biTangent1EndNode->m_outEdges.push_back(goalForwardCircle1Ptr);     // link one of the goal circular edges

      goalReverseCircle1Ptr->m_childPTNodePtr = this->m_goalNodePtr;      // link goal to this edge
      biTangent1EndNode->m_outEdges.push_back(goalReverseCircle1Ptr);     // link the other goal circular edge

      biTangent1Ptr->m_childPTNodePtr = biTangent1EndNode;                // link bitangent to distal node 
      biTangent1StartNode->m_outEdges.push_back(biTangent1Ptr);           // link the start of the bitangent

      // components associated with 2nd outer bitangent
      goalForwardCircle2Ptr->m_childPTNodePtr = this->m_goalNodePtr;        // link goal to this edge
      biTangent2EndNode->m_outEdges.push_back(goalForwardCircle2Ptr);      // link one of the goal circular edges

      goalReverseCircle2Ptr->m_childPTNodePtr = this->m_goalNodePtr;       // link goal to this edge
      biTangent2EndNode->m_outEdges.push_back(goalReverseCircle2Ptr);      // link the other goal circular edge

      biTangent2Ptr->m_childPTNodePtr = biTangent2EndNode;                 // link bitangent to its distal node
      biTangent2StartNode->m_outEdges.push_back(biTangent2Ptr);            // link the start of the bitangent

      // components associated with third (1st inner) bitangent
      goalForwardCircle3Ptr->m_childPTNodePtr = this->m_goalNodePtr;        // link goal to this edge
      crossTangent3EndNode->m_outEdges.push_back(goalForwardCircle3Ptr);    // link one of the goal circular edges

      goalReverseCircle3Ptr->m_childPTNodePtr = this->m_goalNodePtr;        // link goal to this edge
      crossTangent3EndNode->m_outEdges.push_back(goalReverseCircle3Ptr);    // link the other goal circular edge

      crossTangent3Ptr->m_childPTNodePtr = crossTangent3EndNode;            // link node at end of bitangent to the bitangent
      crossTangent3StartNode->m_outEdges.push_back(crossTangent3Ptr);       // link the start of the bitangent

      // components associated with 4th (2nd inner) bitangent
      goalForwardCircle4Ptr->m_childPTNodePtr = this->m_goalNodePtr;        // link goal to this edge
      crossTangent4EndNode->m_outEdges.push_back(goalForwardCircle4Ptr);    // link one of the goal circular edges

      goalReverseCircle4Ptr->m_childPTNodePtr = this->m_goalNodePtr;        // link goal to this edge
      crossTangent4EndNode->m_outEdges.push_back(goalReverseCircle4Ptr);    // link the other goal circular edge

      crossTangent4Ptr->m_childPTNodePtr = crossTangent4EndNode;            // link node at end of bitangent to the bitangent
      crossTangent4StartNode->m_outEdges.push_back(crossTangent4Ptr);       // link the start of the bitangent
    };
  return;
};

// splice the ingoing and outgoing paths of a circle which is described by a CircleData container.
// HACK:  note, this current algorithm ASSUMES that the outgoing biTangent Node has only one emanating edges.  Should this
// situation change (so that a bitangent node has two or more outgoing edges), then this function must be appropriately 
// modified

void CirclePlanner::CircleSplice(CircleData * circle)
{
  const point2 xaxis = point2(1.0,0.0);              // the unit vector colinear with the x-axis

  cerr << endl;  cerr << "  ENTERING CIRCLESPLICE " << endl;  cerr << endl;

  //iterate over all incoming and outgoing loose ends, and try to tie them together
  for(vector<IncomingBiTangent *>::iterator j = circle->In.begin(); j != circle->In.end(); j++)
    {
      pose2 inNodePose = (*j)->m_incomingNodePtr->m_nodePose;
      point2 inVector =  point2((inNodePose.x - circle->m_center.x),(inNodePose.y - circle->m_center.y));
      float inAngle = FindTheta(xaxis,inVector);

      for(vector<OutgoingBiTangent *>::iterator k = circle->Out.begin(); k != circle->Out.end(); k++)
	{
	  if( (circle->m_handedness == RIGHT_HAND_CIRCLE) || (circle->m_handedness == LEFT_HAND_CIRCLE) )
	    {
	      pose2 outNodePose = (*k)->m_outgoingNodePtr->m_nodePose;
	      point2 outVector =  point2((outNodePose.x - circle->m_center.x),(outNodePose.y - circle->m_center.y));
	      float outAngle = FindTheta(xaxis,outVector);

	      // make forward and reverse circular arcs that connects incoming to outgoing tangents
	      PTEdge * forwardArcPtr = makeForwardCircularArc(circle->m_center, inAngle, outAngle, circle->m_handedness, 
							      circle->m_Radius);
	      PTEdge * reverseArcPtr = makeReverseCircularArc(circle->m_center, inAngle, outAngle,
							      circle->m_handedness, circle->m_Radius);
	      // now link these edges first to the outgoing node, then the incoming node
	      forwardArcPtr->m_childPTNodePtr = (*k)->m_outgoingNodePtr;
	      reverseArcPtr->m_childPTNodePtr = (*k)->m_outgoingNodePtr;
	      (*j)->m_incomingNodePtr->m_outEdges.push_back(forwardArcPtr);
	      (*j)->m_incomingNodePtr->m_outEdges.push_back(reverseArcPtr);
	    }
	  else
	    {
	      cerr << "   CIRCLESPLICE: omnidirectional circles are not implemented yet! " << endl;
	    };
	};
    };
};
  

// made some additional circles that "stand off" the start and goal positions. This function assumes that an 
// instance of circle planner has been instantiated, so that the start and goal poses are known.
void CirclePlanner::MakeStandOffPaths(double RADIUS)
{
  // stand off relative to start
  double c_startAngle = cos(m_startPose.ang);    double s_startAngle = sin(m_startPose.ang);
  pose2 startStandOffPose;
  startStandOffPose.x = this->m_startPose.x + START_STAND_OFF_DISTANCE * c_startAngle;
  startStandOffPose.y = this->m_startPose.y + START_STAND_OFF_DISTANCE * s_startAngle;
  startStandOffPose.ang = m_startPose.ang;

  // compute the centers of the two circles at the starting standoff distance
  point2 startStandOffCenterR = point2(startStandOffPose.x + RADIUS * s_startAngle, startStandOffPose.y - RADIUS * c_startAngle);
  point2 startStandOffCenterL  = point2(startStandOffPose.x - RADIUS * s_startAngle, startStandOffPose.y + RADIUS * c_startAngle);

  // put info on standoff circles in containers for later use
  CircleData * startStandoffCircleR = new CircleData(startStandOffCenterR, RADIUS, RIGHT_HAND_CIRCLE,5);
  CircleData * startStandoffCircleL = new CircleData(startStandOffCenterL, RADIUS, LEFT_HAND_CIRCLE,6);

  // make paths between both start circles and both of the start-standoff circles.  This get automatically linked to the Path Tree
  StartCirclePair(this->m_rightStartCircle->m_center, startStandoffCircleR, RIGHT_RIGHT_COMBO, RADIUS);
  StartCirclePair(this->m_rightStartCircle->m_center, startStandoffCircleL, RIGHT_LEFT_COMBO, RADIUS);
  StartCirclePair(this->m_leftStartCircle->m_center, startStandoffCircleR, LEFT_RIGHT_COMBO, RADIUS);
  StartCirclePair(this->m_leftStartCircle->m_center, startStandoffCircleL, LEFT_LEFT_COMBO, RADIUS);

  // make paths between start-standoff circles and the goal circles.  These must be linked in to the graph subsequently
  CircleGoalPair(startStandoffCircleR, this->m_rightGoalCircle->m_center, RIGHT_RIGHT_COMBO, RADIUS);
  CircleGoalPair(startStandoffCircleR, this->m_leftGoalCircle->m_center, RIGHT_LEFT_COMBO, RADIUS);
  CircleGoalPair(startStandoffCircleL, this->m_rightGoalCircle->m_center, LEFT_RIGHT_COMBO, RADIUS);
  CircleGoalPair(startStandoffCircleL, this->m_leftGoalCircle->m_center, LEFT_LEFT_COMBO, RADIUS);

  // now link the paths entering and leaving the starting standoff circles
  CircleSplice(startStandoffCircleR);
  CircleSplice(startStandoffCircleL);

  //---------------------------------------------------------------------------------------------------------------------
  // compute stand off circles relative to the goal
  double c_goalAngle = cos(m_goalPose.ang);    double s_goalAngle = sin(m_goalPose.ang);
  pose2 goalStandOffPose;
  goalStandOffPose.x = this->m_goalPose.x - GOAL_STAND_OFF_DISTANCE * c_goalAngle;
  goalStandOffPose.y = this->m_goalPose.y - GOAL_STAND_OFF_DISTANCE * s_goalAngle;
  goalStandOffPose.ang = m_goalPose.ang;

  // find the centers of the two circles at the goal standoff distance
  point2 goalStandOffCenterR = point2(goalStandOffPose.x + RADIUS * s_goalAngle, goalStandOffPose.y - RADIUS * c_goalAngle);
  point2 goalStandOffCenterL = point2(goalStandOffPose.x - RADIUS * s_goalAngle, goalStandOffPose.y + RADIUS * c_goalAngle);

  // put info on standoff circles in containers for later use
  CircleData * goalStandoffCircleR = new CircleData(goalStandOffCenterR, RADIUS, RIGHT_HAND_CIRCLE, 7);
  CircleData * goalStandoffCircleL = new CircleData(goalStandOffCenterL, RADIUS, RIGHT_HAND_CIRCLE, 8);

  // make paths between both start circles and both of the goal-standoff circles.  These are automatically linked to the Path Tree
  StartCirclePair(this->m_rightStartCircle->m_center, goalStandoffCircleR, RIGHT_RIGHT_COMBO, RADIUS);
  StartCirclePair(this->m_rightStartCircle->m_center, goalStandoffCircleL, RIGHT_LEFT_COMBO, RADIUS);
  StartCirclePair(this->m_leftStartCircle->m_center, goalStandoffCircleR, LEFT_RIGHT_COMBO, RADIUS);
  StartCirclePair(this->m_leftStartCircle->m_center, goalStandoffCircleL, LEFT_LEFT_COMBO, RADIUS);

  //make paths between both of the goal-standoff circles and the two goal circles
  CircleGoalPair(goalStandoffCircleR, this->m_rightGoalCircle->m_center, RIGHT_RIGHT_COMBO, this->m_rightGoalCircle->m_Radius);
  CircleGoalPair(goalStandoffCircleR, this->m_leftGoalCircle->m_center, RIGHT_LEFT_COMBO, this->m_leftGoalCircle->m_Radius);
  CircleGoalPair(goalStandoffCircleL, this->m_rightGoalCircle->m_center, LEFT_RIGHT_COMBO, this->m_rightGoalCircle->m_Radius);
  CircleGoalPair(goalStandoffCircleL, this->m_leftGoalCircle->m_center, LEFT_LEFT_COMBO, this->m_leftGoalCircle->m_Radius);

  // now link the paths entering and leaving the goal standoff circles
  CircleSplice(goalStandoffCircleR);
  CircleSplice(goalStandoffCircleL);

};

  
  
