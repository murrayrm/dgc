/*
 * StandOff.cc:  Functions that implement this simple idea of a "standoff" at the goal configuration.  This
 *               could be potentially useful for parking.
 *
 * Written By Joel Burdick, Oct. 16, 2007
 */


#include "CirclePlanner.hh"


/* --------------------------------------------------------------------------------------------------------------- */

// make some additional circles that "stand off" the goal positions. This function assumes that an 
// instance of circle planner has been instantiated, so that the start and goal poses are known, and reside in
// the CirclePlanner container slots.  This function will create one pair of circles "before" the
// goal, and another pair of circles "after" the goal.  This is potentially useful when dealing with parking slots.

void CirclePlanner::MakeGoalStandOffPaths(double RADIUS)
{
  // compute stand off circles relative to the goal: one "behind" the goal (pose1) and "in front of" the goal (pose2)
  double c_goalAngle = cos(m_goalPose.ang);    double s_goalAngle = sin(m_goalPose.ang);
  pose2 StandOffPose1, StandOffPose2;
  StandOffPose1.x = this->m_goalPose.x - GOAL_STAND_OFF_DISTANCE * c_goalAngle;
  StandOffPose1.y = this->m_goalPose.y - GOAL_STAND_OFF_DISTANCE * s_goalAngle;
  StandOffPose1.ang = m_goalPose.ang;
  StandOffPose2.x = this->m_goalPose.x + GOAL_STAND_OFF_DISTANCE * c_goalAngle;
  StandOffPose2.y = this->m_goalPose.y + GOAL_STAND_OFF_DISTANCE * s_goalAngle;
  StandOffPose2.ang = m_goalPose.ang;

  PTNode * StandOff1NodePtr = new PTNode(StandOffPose1);  PTNode * StandOff2NodePtr = new PTNode(StandOffPose2);

  // find the centers of the standoff circles
  point2 StandOff1CenterR = point2(StandOffPose1.x + RADIUS * s_goalAngle, StandOffPose1.y - RADIUS * c_goalAngle);
  point2 StandOff1CenterL = point2(StandOffPose1.x - RADIUS * s_goalAngle, StandOffPose1.y + RADIUS * c_goalAngle);
  point2 StandOff2CenterR = point2(StandOffPose2.x + RADIUS * s_goalAngle, StandOffPose2.y - RADIUS * c_goalAngle);
  point2 StandOff2CenterL = point2(StandOffPose2.x - RADIUS * s_goalAngle, StandOffPose2.y + RADIUS * c_goalAngle);

  // put info on standoff circles in containers for later use
  CircleData * Standoff1CircleR = new CircleData(StandOff1NodePtr, StandOff1CenterR, RADIUS, RIGHT_HAND_CIRCLE);
  CircleData * Standoff1CircleL = new CircleData(StandOff1NodePtr, StandOff1CenterL, RADIUS, LEFT_HAND_CIRCLE);
  CircleData * Standoff2CircleR = new CircleData(StandOff2NodePtr, StandOff2CenterR, RADIUS, RIGHT_HAND_CIRCLE);
  CircleData * Standoff2CircleL = new CircleData(StandOff2NodePtr, StandOff2CenterL, RADIUS, LEFT_HAND_CIRCLE);

  // make paths between start circles and the goal-standoff circles.  These are automatically linked to the Path Tree
  StartCirclePair(this->m_rightStartCircle, Standoff1CircleR, RIGHT_RIGHT_COMBO);
  StartCirclePair(this->m_rightStartCircle, Standoff1CircleL, RIGHT_LEFT_COMBO);
  StartCirclePair(this->m_leftStartCircle, Standoff1CircleR, LEFT_RIGHT_COMBO);
  StartCirclePair(this->m_leftStartCircle, Standoff1CircleL, LEFT_LEFT_COMBO);

  StartCirclePair(this->m_rightStartCircle, Standoff2CircleR, RIGHT_RIGHT_COMBO);
  StartCirclePair(this->m_rightStartCircle, Standoff2CircleL, RIGHT_LEFT_COMBO);
  StartCirclePair(this->m_leftStartCircle, Standoff2CircleR, LEFT_RIGHT_COMBO);
  StartCirclePair(this->m_leftStartCircle, Standoff2CircleL, LEFT_LEFT_COMBO);

  //make paths between the standoff circles and the goal circles
  CircleGoalPair(Standoff1CircleR, this->m_rightGoalCircle, RIGHT_RIGHT_COMBO, RADIUS);
  CircleGoalPair(Standoff1CircleR, this->m_leftGoalCircle, RIGHT_LEFT_COMBO, RADIUS);
  CircleGoalPair(Standoff1CircleL, this->m_rightGoalCircle, LEFT_RIGHT_COMBO, RADIUS);
  CircleGoalPair(Standoff1CircleL, this->m_leftGoalCircle, LEFT_LEFT_COMBO, RADIUS);

  CircleGoalPair(Standoff2CircleR, this->m_rightGoalCircle, RIGHT_RIGHT_COMBO, RADIUS);
  CircleGoalPair(Standoff2CircleR, this->m_leftGoalCircle, RIGHT_LEFT_COMBO, RADIUS);
  CircleGoalPair(Standoff2CircleL, this->m_rightGoalCircle, LEFT_RIGHT_COMBO, RADIUS);
  CircleGoalPair(Standoff2CircleL, this->m_leftGoalCircle, LEFT_LEFT_COMBO, RADIUS);

  // now link the paths entering and leaving the goal standoff circles
  CircleSplice(Standoff1CircleR);
  CircleSplice(Standoff1CircleL);
  CircleSplice(Standoff2CircleR);
  CircleSplice(Standoff2CircleL);
};


