/*
 * Clothoid.hh
 *
 * Definition of Clothoid Class, which is a container for a discretized clothoid curve
 * Written by Joel Burdick
 * August 23, 2007
 */

#ifndef CLOTHOID_HH
#define CLOTHOID_HH

/* ---------------------------------------------------------------------------------------- */
/*                                         Includes                                         */
/* ---------------------------------------------------------------------------------------- */
#include <vector.h>
#include <math.h>
#include "frames/pose2.hh"
#include "trajutils/CPath.hh"
#include "ClothoidParams.hh"

/* ----------------------------------------------------------------------------------------- */
/*  define a simple Cloithoid Class.  This probably could be done as a simple structure, but */
/*  the use of a class gives us some flexibility.  See Clothoid.cc for definitions of helper */
/*  functions                                                                                */
/* ----------------------------------------------------------------------------------------- */

class Clothoid {

public:                               
  /* ------------------------------- Basic Clothoid Data  ---------------------------------- */
  double m_initCurv, m_finalCurv;      // initial and final curvature of clothoid
  double m_sharpness;                  // "sharpness" ratio 
  double m_length;                     // length of clothoid path segment
  bool m_reverse;                      // true if Alice should drive this path in reverse 
  vector<pose2> m_clothoidPoints;      // points that make up discretized representation of the clothoid
                                       // make this public so that the data is easy to retrieve
  double m_localEdgeCost;              // cost of this clothoid edge, only due to its length
                                       //   The PTEdge cost may include other factors
  int m_clothoidIndex;                 // identifier for this clothoid

  /* ---------------------------- Constructors/destructors --------------------------------- */

  // This constructor generates the Clothoid points from scratch, knowing the parameters 
  Clothoid(pose2 startPose, double initCurv, double finalCurv, double length, bool reverse); 

  // This constructor is needed by the circle planner to allocate memory for a clothoid
  Clothoid(double initCurv, double finalCurv, double length, bool reverse); 

  // This constructor, which uses an index, is used for debuggin
  Clothoid(double initCurv, double finalCurv, double length, bool reverse, int index); 

  // This simpler constructor just allocates memory for the clothoid, allowing some other functions to 
  //     fill in the detailed clothoid information
  Clothoid();

  // destructor
  ~Clothoid();

  /* --------------------------------- Accessors/Reporters --------------------------------- */
  void setInitCurv(double initial_Curvature);                  // set the initial clothoid curvature
  void setFinalCurv(double final_Curvature);                   // set the final clothoid curvature
  void setCurvLength(double Curve_Length);                     // set length of Clothoid
  void setSharpness(double initCurv, double finalCurv, double length);   // set sharpness ratio
  double getCurvature(double position);                        // function to get curavature at the
                                                               // give arc length "position"
  void printClothoid (ostream *f);                             // print Clothoid data
  void printClothoidEndPoint (ostream *f);                             // print Clothoid data
  int getNumPoints();
};                                                         


#endif		// CLOTHOID_HH

