close all;
clear all;

% plot a path (set of points)
figure(2);
dlmread clothoidPath.dat;
clothoids = ans;
plot(clothoids(:,1), clothoids(:,2), '.','MarkerSize',10);
hold on;
xlabel('X (meters)');
ylabel('Y (meters)');
title('Clothoid Tree');
axis equal;

return;
