/*
 * PathTree.hh
 *
 * Definitions of Path Tree Node and Edge Classes
 * Written by Joel Burdick
 * Sept. 8, 2007
 */

/* ----------------------------- Includes and Definitions -------------------------------- */
#include <vector>
#include <math.h>

#include "Clothoid.hh" 

#ifndef PATHTREE_HH
#define PATHTREE_HH


/* ---------------------------------------------------------------------------------------- */
/*                             PathTreeNode Class Definition                                */
/*                                                                                          */
/*  This class implements a node of a path tree, and appropriate functions to modify,       */
/*  access, etc. the associated path segments.  A path tree consists of a point (the        */
/*  starting point of the emanating path segments), and a list of pointers, which represent */
/*  edges to Children nodes. The  edges are physically associated with the path segements   */
/*  connecting the node to the Children.  These physical edges are represented by a vector  */
/*  of discretizated  points of the path segment.                                           */
/* ---------------------------------------------------------------------------------------- */

/* ---------------------------------------------------------------------------------------- */
/* Define Nodes of the Path Tree via a class so we can have constructors, destructors, as   */
/*  well as accessor/reporter functions                                                     */
/* ---------------------------------------------------------------------------------------- */
class PTEdge;

class PTNode                               // PtNode <-> PathTreeNode
{
public:
  /* ------------- Basic Data in this Class ------------------- */
  pose2 m_nodePose;                        // Pose of this clothoid node
  vector<PTEdge *>  m_outEdges;            // Pointers to outgoing edges that connect with other nodes
  PTEdge * m_incomingEdge;                 // pointer to the edge whose child is this node
                                           //    this is needed for the backtrace of the solution
  double m_nodeCost;                       // allow for a cost which is specific to this node.
  bool m_hasNodeBeenCulled;                  // flag that enables efficiency and lack of errors for 
                                           // edge culling process (for obstacle and perimeters)

  /* --------- Constructors/Destructors/Initialization --------- */
  PTNode(pose2 nodePose);                 // nominal constructor
  PTNode();                                // basic constructor needed by A* algorithm
  //  ~PTNode();                                // basic constructor needed by A* algorithm
  void clearPTNode();                      // zero the data in a path tree node 

  /* ---------------- Accessors/Reporters ---------------------- */
  double getTotalCost();                   // estmated total path cost (cost to node + heuristic)
  /* return vector of pointers to the children of this node*/
  vector<PTNode *> getNodeChildren(PTNode * NodePtr);  
  void printPTNodePose(ostream *f);                          // print out node contents
  void printPTNode(ostream *f);                              // print out node contents
  void printAllPaths(ostream *f);                                  // recursively print out contents of 
  void printAllForwardPaths(ostream *f);                                  // recursively print out contents of 
  void printAllBackwardPaths(ostream *f);                                  // recursively print out contents of 
  void printAllPathEndPoints(ostream *f);                           // print endpoints of all paths descending from current
                                                                       //     Tree Node and its children
  /* ----------------------------------------------------------- */
  /*                  A-Star search functions                    */
  /* ----------------------------------------------------------- */

  // function to estimate distance from current node base to goal configuration.
  float GoalDistanceEstimate(PTNode * goalPosePtr);

  // determine if solution is at the goal
  bool IsGoal(PTNode * goalNodePtr);

  bool IsSameState(PTNode * comparisonNodePtr);
  bool IsSameState(pose2 comparisonPose);
};

/* ---------------------------------------------------------------------------------------- */
/*                             PathTreeEdge Class Definition                                */
/*                                                                                          */
/*  This class implements an edge of a path tree, and appropriate functions to modify,      */
/*  access, etc. the associated path edges.  A path tree edge consists of a link to a       */
/*  successor Path Node, along with the associated list of poses (i.e., clothoid path)      */
/*  which connect the parent node pose to the child node pose                               */
/* ---------------------------------------------------------------------------------------- */

class PTEdge                               // PtEdge <-> PathTreeEdge
{
public:
  /* ------------- Basic Data in this Class ------------------- */
  PTNode * m_childPTNodePtr;               // Pointer to the child Path Tree Node, I.e. the distal node at the end of this edge
  PTNode * m_parentPTNodePtr;              // Pointer to the parent Path Tree Node
  Clothoid * m_clothoid;                   // the physical clothoid associated with this edge
  double m_edgeCost;                       // cost of traversing this edge from the parent node of this edge to its child node.
  unsigned int m_ID;                       // I.D. number of the Edge (for counting purposes)
  
  /* --------- Constructors/Destructors/Initialization --------- */
  PTEdge();                                // basic constructor to allocate space
  PTEdge(Clothoid * edgeClothoid);
  PTEdge(PTNode * childNodePtr, Clothoid * edgeClothoid);                 // nominal constructor
  //  ~PTEdge();
  void clearPTEdge();                      // zero the data in a path tree edge

  /* ---------------- Accessors/Reporters ---------------------- */
  void printPTEdge(ostream *f,bool printParent);            // print out data of the edge;
  void printPTEdgeClothoid(ostream *f);
};


#endif            // PATHTREE_HH
