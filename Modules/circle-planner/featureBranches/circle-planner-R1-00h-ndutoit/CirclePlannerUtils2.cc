/* CirclePlannerUtils.cc: basic utility functions that get used by CirclePlanner
 *
 * Written by Joel Burdick (Sept. 26, 2007)
 */

/* ----------------------------------------------------------------------------------- */
/*                                     includes                                        */
/* ----------------------------------------------------------------------------------- */

#include <alice/AliceConstants.h>
#include "CirclePlanner.hh"

/* ------------------------------------------------------------------------------------------------- */
// this function, like IsPathLegal, test for the legality of a path.  However, when a path is illegal,
// because it collides with a boundary, it will recursive expand the path from a point a little
// before the the collision point.  The amount of recursion is controlled by "depth"
/* ------------------------------------------------------------------------------------------------- */

#if CIRCLE_PLANNER_DEBUG
bool CirclePlanner::ExpansiveIsPathLegal(PTEdge * edgePtr, ofstream *f, int depth)
{
  bool testIntersection;
  int j=1;
  Clothoid * cPtr = edgePtr->m_clothoid;

  //iterate though all points of the Clothoid to check for overlap of Alice and zone boundary
  for (vector<pose2>::iterator i=cPtr->m_clothoidPoints.begin(); i!=cPtr->m_clothoidPoints.end(); i++, j++)
    {
      point2arr curAliceBoundingBox = AliceBoundingBox((*i));
      /*      printPoint2arr(curAliceBoundingBox,f); */

      testIntersection = this->m_rightHandedZoneBoundary.is_intersect(curAliceBoundingBox);

      // If there is an intersection, then try to first  expand out new paths from a pose just before 
      // intersection.  We won't expand out paths that are too short, as they are not worth pursuing.
      if (testIntersection==true)                        // if alice intersects boundary
	{ 
	  cerr << "   Expansive: found collision on clothoid " << cPtr->m_clothoidIndex << endl;
	  if ( (depth > 0) && (j>=5) )                   // if the current clothoid has at least 5 poses, and we haven't 
	    {                                            //    gone beyond the specified depth level, then expand the paths
	      cerr << " Path is not legal, and but expanding " << endl;
	      j -=2;                                     // go back two path points from the end
	      cPtr->m_clothoidPoints.resize(j);          // clip off all of the poses after the jth pose

	      // make a new Path Node at the clipped end of the clothoid, and link it to the associated edge
	      pose2 endPose = cPtr->m_clothoidPoints.back();
	      edgePtr->m_childPTNodePtr = new PTNode(endPose);  
	      this->printDenseAliceBox(endPose, f); 

	      // now expand from this new node.  First, create circles passing through this node
	      double cosAngle = cos(endPose.ang);    double sinAngle = sin(endPose.ang);
	      point2 rightCenter = point2(endPose.x + TURNING_RADIUS * sinAngle, endPose.y - TURNING_RADIUS * cosAngle);
	      point2 leftCenter  = point2(endPose.x - TURNING_RADIUS * sinAngle, endPose.y + TURNING_RADIUS * cosAngle);
	      double radius = this->m_rightGoalCircle->m_Radius;
	      CircleData * RCirclePtr = new CircleData(edgePtr->m_childPTNodePtr,rightCenter, radius, RIGHT_HAND_CIRCLE);
	      CircleData * LCirclePtr = new CircleData(edgePtr->m_childPTNodePtr,leftCenter, radius, LEFT_HAND_CIRCLE);

	      // Next, create the paths from these circles to the goal circles
	      StartGoalCirclePair(RCirclePtr, this->m_rightGoalCircle, RIGHT_RIGHT_COMBO, radius);
	      StartGoalCirclePair(RCirclePtr, this->m_leftGoalCircle, RIGHT_LEFT_COMBO, radius);
	      StartGoalCirclePair(LCirclePtr, this->m_rightGoalCircle, LEFT_RIGHT_COMBO, radius);
	      StartGoalCirclePair(LCirclePtr, this->m_leftGoalCircle, LEFT_LEFT_COMBO, radius);

	      // now process these newly developed paths, because the main Cull routine will otherwise not process them.
	      CullAndExpandNodeOutEdges(edgePtr->m_childPTNodePtr, f, (depth -1));
	      return true;
	    }
	  else                   // we can't expand this path due to depth or size restrictions, so it is a dead path
	    { 
	      cerr << " Path is not legal, and not expanding " << endl;
	      return  false;
	    };  
	};
    };
  cerr << " found no intersection in this path " << endl;
  return true;
};
#endif

bool CirclePlanner::ExpansiveIsPathLegal(PTEdge * edgePtr, int depth)
{
  bool testIntersection;
  int j=1;
  Clothoid * cPtr = edgePtr->m_clothoid;

  //iterate though all points of the Clothoid to check for overlap of Alice and zone boundary
  for (vector<pose2>::iterator i=cPtr->m_clothoidPoints.begin(); i!=cPtr->m_clothoidPoints.end(); i++, j++)
    {
      point2arr curAliceBoundingBox = AliceBoundingBox((*i));

      testIntersection = this->m_rightHandedZoneBoundary.is_intersect(curAliceBoundingBox);

      // If there is an intersection, then try to first  expand out new paths from a pose just before 
      // intersection.  We won't expand out paths that are too short, as they are not worth pursuing.
      if (testIntersection==true)                        // if alice intersects boundary
	{ 
	  if ( (depth > 0) && (j>=5) )                   // if the current clothoid has at least 5 poses, and we haven't 
	    {                                            //    gone beyond the specified depth level, then expand the paths
	      j -=2;                                     // go back two path points from the end
	      cPtr->m_clothoidPoints.resize(j);          // clip off all of the poses after the jth pose

	      // make a new Path Node at the clipped end of the clothoid, and link it to the associated edge
	      pose2 endPose = cPtr->m_clothoidPoints.back();
	      edgePtr->m_childPTNodePtr = new PTNode(endPose);  

	      // now expand from this new node.  First, create circles passing through this node
	      double cosAngle = cos(endPose.ang);    double sinAngle = sin(endPose.ang);
	      point2 rightCenter = point2(endPose.x + TURNING_RADIUS * sinAngle, endPose.y - TURNING_RADIUS * cosAngle);
	      point2 leftCenter  = point2(endPose.x - TURNING_RADIUS * sinAngle, endPose.y + TURNING_RADIUS * cosAngle);
	      double radius = this->m_rightGoalCircle->m_Radius;
	      CircleData * RCirclePtr = new CircleData(edgePtr->m_childPTNodePtr,rightCenter, radius, RIGHT_HAND_CIRCLE);
	      CircleData * LCirclePtr = new CircleData(edgePtr->m_childPTNodePtr,leftCenter, radius, LEFT_HAND_CIRCLE);

	      // Next, create the paths from these circles to the goal circles
	      StartGoalCirclePair(RCirclePtr, this->m_rightGoalCircle, RIGHT_RIGHT_COMBO, radius);
	      StartGoalCirclePair(RCirclePtr, this->m_leftGoalCircle, RIGHT_LEFT_COMBO, radius);
	      StartGoalCirclePair(LCirclePtr, this->m_rightGoalCircle, LEFT_RIGHT_COMBO, radius);
	      StartGoalCirclePair(LCirclePtr, this->m_leftGoalCircle, LEFT_LEFT_COMBO, radius);

	      // now process these newly developed paths, because the main Cull routine will otherwise not process them.
	      CullAndExpandNodeOutEdges(edgePtr->m_childPTNodePtr, (depth -1));
	      return true;
	    }
	  else                   // we can't expand this path due to depth or size restrictions, so it is a dead path
	    { 
	      return  false;
	    };  
	};
    };
  return true;
};

/* ------------------------------------------------------------------------------------------------------------ */

#if CIRCLE_PLANNER_DEBUG
// the alternate version of CullNodeOutEdges for debugging
void CirclePlanner::CullAndExpandNodeOutEdges(PTNode * nodePtr, ofstream *f, int depth)
{
  if(nodePtr->m_outEdges.empty())              // if node has no outEdges, there's nothing to do
    {
      nodePtr->m_hasNodeBeenCulled = true;
      return;
    }
  else
    { // first check to see if this node has already been visited and culled.  If it has not been visited, then proceed 
      // with the culling operation.  If it has been visited, then just return
      if ( !nodePtr->m_hasNodeBeenCulled )
	{ 	 
	  // Now eliminate the infeasible edges.  We can't erase them from the outEdges list immediately because we're 
	  // iterating on that list.  So, save up the feasible edges, and reconstruct the list after the iteration
	  vector<PTEdge *> feasibleEdgeList;                    
	  feasibleEdgeList.clear();

	  for (vector<PTEdge *>::iterator i=nodePtr->m_outEdges.begin(); i!=nodePtr->m_outEdges.end(); i++)
	    {
	      PTNode * childPtr = (*i)->m_childPTNodePtr;       // distal node of the present edge i

	      // before trying to cull an edge, first cull the outEdges of this edge's distal node
	      if ( !childPtr->m_outEdges.empty() )
		{   CullAndExpandNodeOutEdges(childPtr, f, depth);  };

	      if( ExpansiveIsPathLegal( (*i), f, depth ) == true)      
		{
		  feasibleEdgeList.push_back((*i)); 
		  /*  cerr << "      CullEdges:  keeping Edge " << (*i)->m_clothoid->m_clothoidIndex << endl;  */
		}
	      else
		{  
		  /* cerr << "      CullEdges:  will erase Edge " << (*i)->m_clothoid->m_clothoidIndex << endl;  */
		};
	    };
	  nodePtr->m_hasNodeBeenCulled = true;

	  nodePtr->m_outEdges.clear();	          // now reattach all of the feasible outEdges to the node
	  for (vector<PTEdge *>::iterator j=feasibleEdgeList.begin(); j!=feasibleEdgeList.end(); j++)
	    { nodePtr->m_outEdges.push_back((*j)); };
	}
      else
	{return; };
    };
};
#endif

// the alternate version of CullNodeOutEdges for debugging
void CirclePlanner::CullAndExpandNodeOutEdges(PTNode * nodePtr, int depth)
{
  if(nodePtr->m_outEdges.empty())              // if node has no outEdges, there's nothing to do
    {
      nodePtr->m_hasNodeBeenCulled = true;
      return;
    }
  else
    { // first check to see if this node has already been visited and culled.  If it has not been visited, then proceed 
      // with the culling operation.  If it has been visited, then just return
      if ( !nodePtr->m_hasNodeBeenCulled )
	{ 	 
	  // Now eliminate the infeasible edges.  We can't erase them from the outEdges list immediately because we're 
	  // iterating on that list.  So, save up the feasible edges, and reconstruct the list after the iteration
	  vector<PTEdge *> feasibleEdgeList;                    
	  feasibleEdgeList.clear();

	  for (vector<PTEdge *>::iterator i=nodePtr->m_outEdges.begin(); i!=nodePtr->m_outEdges.end(); i++)
	    {
	      PTNode * childPtr = (*i)->m_childPTNodePtr;       // distal node of the present edge i

	      // before trying to cull an edge, first cull the outEdges of this edge's distal node
	      if ( !childPtr->m_outEdges.empty() )
		{   CullAndExpandNodeOutEdges(childPtr, (depth - 1) );  };

	      if( ExpansiveIsPathLegal( (*i), depth ) == true)      
		{
		  feasibleEdgeList.push_back((*i)); 
		  /*  cerr << "      CullEdges:  keeping Edge " << (*i)->m_clothoid->m_clothoidIndex << endl;  */
		}
	      else
		{  
		  /* cerr << "      CullEdges:  will erase Edge " << (*i)->m_clothoid->m_clothoidIndex << endl;  */
		};
	    };
	  nodePtr->m_hasNodeBeenCulled = true;

	  nodePtr->m_outEdges.clear();	          // now reattach all of the feasible outEdges to the node
	  for (vector<PTEdge *>::iterator j=feasibleEdgeList.begin(); j!=feasibleEdgeList.end(); j++)
	    { nodePtr->m_outEdges.push_back((*j)); };
	}
      else
	{return; };
    };
};

