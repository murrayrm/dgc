/*  unit test of Path Tree Planner components.  This is just a way to test components
    without having to interface to the rest of the system */

#include "CirclePlanner.hh"

using namespace std; 

int DummyCSpecs(FILE * ut_spec, CSpecs_t *cSpecs)
{
  double start[STATE_NUM_ELEMENTS], end[STATE_NUM_ELEMENTS];
  int numParkingSpaces;
  vector<point2arr> parkingSpaces;
  int trafficState;
  point2arr boundary;

  cerr << endl;   cerr << endl;   cerr << endl;
  cerr << "-------------------- Data from Simulation File ------------------ " << endl << endl;

  fscanf(ut_spec, "%lf,%lf,%lf\n", start + STATE_IDX_X, start + STATE_IDX_Y, start + STATE_IDX_THETA);
  cerr << "read initial state:" << start[STATE_IDX_X] << " " << start[STATE_IDX_Y] << " " << start[STATE_IDX_THETA] << endl;
  fscanf(ut_spec, "%lf,%lf,%lf\n", end + STATE_IDX_X, end + STATE_IDX_Y, end + STATE_IDX_THETA);
  cerr << "read final state: " << end[STATE_IDX_X] << " " << end[STATE_IDX_Y] << " " << end[STATE_IDX_THETA] << endl;
  fscanf(ut_spec, "%d\n", &numParkingSpaces);
  cerr << numParkingSpaces << " parking spaces" << endl;
  for ( int i = 0; i < numParkingSpaces; i++)
    {
      point2 Pt1, Pt2;
      fscanf(ut_spec, "%lf,%lf,%lf,%lf\n", &(Pt1.x), &(Pt1.y), &(Pt2.x), &(Pt2.y) );
      point2arr space;
      space.push_back(Pt1); space.push_back(Pt2);
      parkingSpaces.push_back(space);
      cerr << "parking space num " << i << " is at " << Pt1 << " to " << Pt2 << endl;
    }

  fscanf(ut_spec, "%d", &trafficState);
  cerr << "traffic state: " << trafficState << endl;
  
  point2 Pt;
  while ( fscanf(ut_spec, "%lf, %lf\n", &(Pt.x), &(Pt.y) ) != EOF )
    {
      cout << "reading in boundary point " << Pt << endl;
      boundary.push_back(Pt);
    }

  cerr << "------------------------------------------------------------------- " << endl;
  cerr << endl;  cerr << endl; 
	
  // generate the bitmap params
  BitmapParams bmParams;
  bmParams.resX = 0.1;
  bmParams.resY = 0.1;
  bmParams.width = 800;
  bmParams.height = 800;
  bmParams.baseVal = 1 ;
  bmParams.outOfBounds = 100;
  vector<MapElement> obstacles;
  
  point2 initPos(start[STATE_IDX_X], start[STATE_IDX_Y]);
  // 	ZoneCorridor::getBitmapParams(bmParams, polygonParams, initPos, boundary, parkingSpaces, obstacles);
  
  // populate the cspecs
  cSpecs->setStartingState(start);
  cSpecs->setFinalState(end);
  cSpecs->setBoundingPolygon(boundary);
  cSpecs->setCostMap(bmParams);
  cSpecs->setTrafficState(trafficState);

  return 0;
}


int main(int argc, char **argv)
{

  if (argc < 2)
    {
      cout	<< "Usage: UT_clothoidplanner UT_spec.dat" << endl
		<< "where UT_spec.dat is a file in the following format:" << endl
		<< "initCond.x, initCond.y, initCond.yaw" << endl
		<< "finalCond.x, finalCond.y, finalCond.yaw" << endl
		<< "number of parking spaces" << endl
		<< "space1.x1, space1.y1, space1.x2, space1.y2" << endl
		<< "space2.x1, space2.y1, space2.x2, space2.y2" << endl
		<< "..." << endl
		<< "spaceN.x1, spaceN.y1, spaceN.x2, spaceN.y2" << endl
		<< "TrafficState" << endl
		<< "perimiterPt1.x, perimiterPt1.y" << endl
		<< "perimiterPt2.x, perimiterPt2.y" << endl
		<< "..." << endl
		<< "perimiterPt1N.x, perimiterPtN.y" << endl;
      return 1;
    }
  
  FILE *ut_spec = fopen(argv[1], "r");                      // open the file with test data

  CSpecs_t *cSpecs = new CSpecs();                          // make a dummy CSpecs container
  PlanGraphPath path;                                       // set up a dummy path variable to spoof planner interface
  memset(&path,0,sizeof(path));                             // make sure that sufficient space is saved 
                                                            //   for the path data (or we get crashes!)
  DummyCSpecs(ut_spec, cSpecs);                             // load in dummy CSpecs data

  // Generate a trajectory, as would be requested by planner.
  Err_t error = CirclePlanner::GenerateTraj(cSpecs,&path);


#if CIRCLE_PLANNER_DEBUG
  if(error == CIRCLE_PLANNER_OK)
    { cerr << "  UT_circleplanner:: Circle Planner succeeded " << endl; }
  else
    { cerr << "  Circle Planner Failed! " << endl;  };
#endif
};


