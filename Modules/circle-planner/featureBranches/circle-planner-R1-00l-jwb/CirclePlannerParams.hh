/* 
 * CirclePlannerParams.hh: list of parameters used by Circle Planner. So, if you want to 
 * modify CirclePlanner's behavior, adjust the parameters here.
 *
 * Based on Code written by Kenny Osland, modified by Joel Burdick
 */

/* ---------------------------------------------------------------------------------------- */
/*                              Definitions related to Debugging                            */
/* ---------------------------------------------------------------------------------------- */

#define CIRCLE_PLANNER_DEBUG 0                           // set to zero for no debug info, or 1 for debug info
                                                         // debugging prints lots of internal state info.
/* ---------------------------------------------------------------------------------------- */
/*                           Definitions related to Path Expansion                          */
/* ---------------------------------------------------------------------------------------- */

// EXPAND_DEPTH determines how many recursive levels CullAndExpandNodeOutEdges will use when
// expanding out potential paths.  The greater this number, the more likely that CirclePlanner will
// find a solution in more complex situations like u-turns.  But, also the greater computational complexity

#define EXPAND_DEPTH                      2

/* ---------------------------------------------------------------------------------------- */
/*                   Constants and Definitions related to Clothoids                         */
/* ---------------------------------------------------------------------------------------- */

// distance between points in a trajectory (measured in meters) for Circle Planner
#define CP_PATH_SECTION_LENGTH			0.3

// the turning radius used for planning, which should be slightly over Alice's minimum turning radius
#define TURNING_RADIUS (VEHICLE_MIN_TURNING_RADIUS + 0.3)

#define TURNING_LENGTH             10         // distance in meters over which we want to transition Alice's steering

// maximum clothoid sharpness such that clothoid goes from -curvature_max to +curvature_max in TURNING_LENGTH
#define MAX_SHARPNESS              (2.0/TURNING_RADIUS * TURNING_LENGTH)

#define DELTA_MIN                  1.0/(TURNING_RADIUS * TURNINGRADIUS * MAX_SHARPNESS)

// the multiplying factor used in setting length-based cost of a path when using reverse
#define REVERSE_PATH_PENALTY                    3.0


/* ---------------------------------------------------------------------------------------- */
/*                   Constants and Definitions related to Path Tree                         */
/* ---------------------------------------------------------------------------------------- */

#define GOAL_XY_EPSILON 0.4             // threshold for determining if path segment end point is "close 
                                        // enough" (in terms of translation to the goal to declare victory
#define GOAL_ANG_EPSILON 0.15            // threshold for determining if path segment end point is "close 
                                        // enough in terms of angular difference
#define GOAL_XY2  (GOAL_XY_EPSILON * GOAL_XY_EPSILON)
#define GOAL_ANG2 (GOAL_ANG_EPSILON * GOAL_ANG_EPSILON)

//--------------------------------------
#define SAME_XY_EPSILON 0.2             // threshold for determining if a path segment end point is "close 
                                        //    enough" (in translation) to be the same point
#define SAME_ANG_EPSILON 0.2            // threshold for determining if a path segment end point is "close 
                                        //    enough" in terms of angular difference
#define SAME_XY2  (GOAL_XY_EPSILON * GOAL_XY_EPSILON)
#define SAME_ANG2 (GOAL_ANG_EPSILON * GOAL_ANG_EPSILON)

/* ---------------------------------------------------------------------------------------- */
/*                Constants and Definitions related to stand off circles                   */
/* ---------------------------------------------------------------------------------------- */

// the "stand off" distances
#define START_STAND_OFF_DISTANCE               4.0        // in meters
#define GOAL_STAND_OFF_DISTANCE                3.0        // in meters

/* ---------------------------------------------------------------------------------------- */
/*                Constants and Definitions related to zones and obstacles                  */
/* ---------------------------------------------------------------------------------------- */

// HACK: these values are defined in LogicPlanner.cc, but are copied here so that we don't
// have to include that file
#define ALICE_SAFETY_FRONT   (1.0)
#define ALICE_SAFETY_REAR    (0.5)
#define ALICE_SAFETY_SIDE    (0.5)


// ------------ parameters related 
#define START_GROW_ALICE 2.0                   // amount, in meters, to grow zone boundary
                                               // around Alice at starting pose
#define GOAL_GROW_ALICE 2.0                    // amount, in meters, to grow zone boundary
                                               // around Alice at goal pose

/* ---------------------------------------------------------------------------------------- */
/*                       Constants and Definitions related to planning                      */
/* ---------------------------------------------------------------------------------------- */

#define NEW_PATH_IMPROVEMENT   1.2             // ratio that new path cost must be to old path cost to be considered 
                                               // a significant improvement
#define ACCEPTABLE_ALICE_ERROR 1.0             // the distance, in meters, which is acceptable for alice's pose to be displaced relative to solution path
#define EXTENSION_LENGTH       2.0             // length of the "extension" along a path segment which is used for point projection
