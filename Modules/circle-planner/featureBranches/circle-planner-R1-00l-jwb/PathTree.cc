/*
 * PathTree.cc
 * Helper functions for the PathTree Node and Edge Classes (PathTree.hh). The Path Tree Node class 
 * is a container for the via points and path segments. The PathTree Node and Edge classes build upon 
 * the Clothoid class to make trees of Clothoid Trajectories.
 *
 * Based on code originally written by Kenny Oslund.  
 * Modified by Joel Burdick starting Aug. 22, 2007
 */

#include "PathTree.hh"

/* ----------------------------------------------------------------------------------------- */
/*                  Constructors, Destructors, for Nodes and Edges                           */
/* ----------------------------------------------------------------------------------------- */

/* nominal constructor */
PTNode::PTNode(pose2 nodePose)
{
  m_nodePose = nodePose;                        // set the node's pose
  m_outEdges.clear();                           // We don't have any children yet, so clear the outgoing edges. 
  m_nodeCost = 0.0;
  m_hasNodeBeenCulled = false;
};

// minimal constructor for Edge Class.  Node, we initially set the edgeCost to be equal to the cost of the 
// physical clothoid that is associated with that edge. Later, additional costs due to the environment can
// be added to the edgeCost
PTEdge::PTEdge(Clothoid * edgeClothoid)
{
  m_childPTNodePtr = NULL;
  m_parentPTNodePtr = NULL;
  m_clothoid = edgeClothoid;
  m_edgeCost = edgeClothoid->m_localEdgeCost;
};

// nominal constructor for Edge Class.  Node, we initially set the edgeCost to be equal to the cost of the 
// physical clothoid that is associated with that edge. Later, additional costs due to the environment can
// be added to the edgeCost
PTEdge::PTEdge(PTNode * childNodePtr, Clothoid * edgeClothoid)
{
  m_childPTNodePtr = childNodePtr;
  m_parentPTNodePtr = NULL;
  m_clothoid = edgeClothoid;
  m_edgeCost = edgeClothoid->m_localEdgeCost;
};


/* ----------------------------------------------------------------------------------------- */
/*                          Initialization/ low level calculation                            */
/* ----------------------------------------------------------------------------------------- */

/* clear all the information in a Node */
void PTNode::clearPTNode()
{
  m_nodePose = pose2(0.0,0.0,0.0);              // set node pose to the origin
  m_outEdges.clear();                           // unlink to any children nodes
  m_incomingEdge = NULL;
  m_nodeCost = 0.0;                             // zeros the node cost
};

/* clear all the information in an Edge */
void PTEdge::clearPTEdge()
{
  m_childPTNodePtr = NULL;
  m_parentPTNodePtr = NULL;
  m_clothoid = NULL;
  m_edgeCost = 0.0;

};

/* ----------------------------------------------------------------------------------------- */
/*                               Accessor and Reporter Functions                             */
/* ----------------------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------------------- */
/*               Functions to print out Clothoid Tree Node and Edge Data                     */
/* ----------------------------------------------------------------------------------------- */

// print just the pose associated with a Path Tree Node
void PTNode::printPTNodePose(ostream *f)
{
  cout << "   nodePose: x= " << m_nodePose.x << "y= " << m_nodePose.y << " ang= " << m_nodePose.ang << endl;
  return;
};

// print contents of a node
void PTNode::printPTNode(ostream *f)
{
  this->printPTNodePose(f);
  if (!m_outEdges.empty())                                // check to see if the node has outgoing .  If so, print them
   {                                                     
      for (vector<PTEdge *>::iterator i = m_outEdges.begin(); i != m_outEdges.end(); i++)
	{ (*i)->printPTEdge(f,false); };
    }
  else
    { cout << "   Clothoid Tree Node has NO outgoing edges " << endl; };
  
  return;
};

/* print the contents of an Edge */
void PTEdge::printPTEdge(ostream *f, bool printParent)
{
  if (m_childPTNodePtr != NULL)
    {
      cout << " Printing Data of Child Node " << endl;
      m_childPTNodePtr->printPTNode(f);
    }
  else
    {cout << "   this edge has no child pointer" << endl;};

  if ( m_parentPTNodePtr != NULL)
    {
      if (printParent)
	{
	  cout << " Printing Data of Parent Node " << endl;
	  m_parentPTNodePtr->printPTNode(f);
	};
    }
  else
    {cerr << "   this edge has no parent pointer" << endl;};

  if (m_clothoid != NULL)
    {
      cout << " printing this edge's clothoid " << endl;
      m_clothoid->printClothoid(f);
    }
  else
    { cout << "    this edge has no associated clothoid " << endl;};

  cout << "     this edge has cost " << m_edgeCost << endl;
};


void PTEdge::printPTEdgeClothoid(ostream *f)
{
  m_clothoid->printClothoid(f);
};


/* print the contents of a clothoid Node, and all of its children.  First we print out the
   clothoids associated with each edge to a child.  Then we recursively print out nodes associated with each
   child node */
void PTNode::printAllPaths(ostream *f)
{
  if (!m_outEdges.empty())
    {
    for (vector<PTEdge *>::iterator i = m_outEdges.begin(); i != m_outEdges.end(); i++)  
      {
	(*i)->printPTEdgeClothoid(f);
      };
    if (!m_outEdges.empty())       
      {
	for (vector<PTEdge *>::iterator i = m_outEdges.begin(); i != m_outEdges.end(); i++)
	  {
	    (*i)->m_childPTNodePtr->printAllPaths(f);
	  };
      };
    };
  return;
}

/* print the contents of a clothoid Node, and all of its children.  First we print out the
   clothoids associated with each edge to a child.  Then we recursively print out nodes associated with each
   child node */
void PTNode::printAllForwardPaths(ostream *f)
{
  if (!m_outEdges.empty())
    {
    for (vector<PTEdge *>::iterator i = m_outEdges.begin(); i != m_outEdges.end(); i++)  
      {
	if ( !(*i)->m_clothoid->m_reverse )
	  {
	    (*i)->printPTEdgeClothoid(f);
	  };
      };
    if (!m_outEdges.empty())       
      {
	for (vector<PTEdge *>::iterator i = m_outEdges.begin(); i != m_outEdges.end(); i++)
	  {
	    (*i)->m_childPTNodePtr->printAllForwardPaths(f);
	  };
      };
    };
  return;
}

void PTNode::printAllBackwardPaths(ostream *f)
{
  if (!m_outEdges.empty())
    {
    for (vector<PTEdge *>::iterator i = m_outEdges.begin(); i != m_outEdges.end(); i++)  
      {
	if ( (*i)->m_clothoid->m_reverse )
	  {
	    cerr << "     PrintBackwards: clothoid " << (*i)->m_clothoid->m_clothoidIndex << endl;
	    (*i)->printPTEdgeClothoid(f);
	  };
      };
    if (!m_outEdges.empty())       
      {
	for (vector<PTEdge *>::iterator i = m_outEdges.begin(); i != m_outEdges.end(); i++)
	  {
	    (*i)->m_childPTNodePtr->printAllBackwardPaths(f);
	  };
      };
    };
  return;
}

/* print just the endpoints of all clothoids descending from the given node. */
void PTNode::printAllPathEndPoints(ostream *f)
{
  if (!m_outEdges.empty())
    {
    for (vector<PTEdge *>::iterator i = m_outEdges.begin(); i != m_outEdges.end(); i++)  
      {
	(*i)->m_clothoid->printClothoidEndPoint(f);
      };
    if (!m_outEdges.empty())       
      {
	for (vector<PTEdge *>::iterator i = m_outEdges.begin(); i != m_outEdges.end(); i++)
	  {
	    (*i)->m_childPTNodePtr->printAllPathEndPoints(f);
	  };
      };
    };
  return;
}


/* ----------------------------------------------------------------------------------------- */
/*                       Functions related to A-Star Searching                               */ 
/* ----------------------------------------------------------------------------------------- */

// estimate the distance from the base point of the current Clothoid Tree Node to the Goal pose.
// Right now we use a simple Euclidean distance. This should use a more sophisticated Reeds-Shep
// distance calculation to take Alice's wheels' non-holonomic constraints into account.  Even
// better, this should check for the NEAREST point on a clothoid to the goal, and then return
// both the distance, as well as the point on the clothoid that is closest to the goal.

float PTNode::GoalDistanceEstimate(PTNode * goalNodePtr)
{
    float dist;

  float xdiff = (float) (goalNodePtr->m_nodePose.x - m_nodePose.x);
  float ydiff = (float) (goalNodePtr->m_nodePose.y - m_nodePose.y);
  float angdiff = (float) (goalNodePtr->m_nodePose.ang - m_nodePose.ang);

  dist = sqrt(xdiff * xdiff + ydiff * ydiff + angdiff * angdiff);
  return dist;
};


// This function should return "true" if  "close enough" to the goal.  Currently we use 
// only a simple threshold comparison.  The translational distance between the goal
// and the current node must be less than GOAL_XY_EPSILON,  AND the angular difference
// between the current node and the goal node must be less than GOAL_XY_EPSISON.
// Here's a better idea which will take a lot of time to implment: This function should 
// find the closest point on the path to the goal (not just checking
// against the end point).  If it's close enough to meet the threshold, then return
// true, and also adjust the path so that it ends at the closest point.

bool PTNode::IsGoal(PTNode * goalNodePtr)               
{
  float xdiff = (float) (goalNodePtr->m_nodePose.x - m_nodePose.x);
  float ydiff = (float) (goalNodePtr->m_nodePose.y - m_nodePose.y);
  float angdiff = (float) (goalNodePtr->m_nodePose.ang - m_nodePose.ang);
  float pdiff2 = xdiff * xdiff + ydiff * ydiff;

  if ((pdiff2 < GOAL_XY2) && ((angdiff * angdiff) < GOAL_ANG2))
    {
      return 1; }
  else
    { return 0;  };
};

// This function should return "true" if  the comparison Node postion is "close enough" to base
// node postion.  Currently we use only a simple threshold comparison.  The translational 
// distance between the current and comparison node must be less than SAME_XY_EPSILON,  AND the 
// angular difference between the current and comparison nodes must be less than SAME_XY_EPSISON.
// Here's a better idea which will take a lot of time to implment: This function should 
// find the closest point on the clothoid to the the comparison point.
//
// For convenience, there are two version of this function with slightly different inputs

bool PTNode::IsSameState(pose2 comparisonPose)               
{
  pose2 curPose = m_nodePose;
  float xdiff = comparisonPose.x - curPose.x;
  float ydiff = comparisonPose.y - curPose.y;
  float angdiff = comparisonPose.ang - m_nodePose.ang;
  float pdiff2 = xdiff * xdiff + ydiff * ydiff;
 
  if ((pdiff2 < SAME_XY2) && ((angdiff * angdiff) < SAME_ANG2))
    { return 1; }
  else
    { return 0; };
};


bool PTNode::IsSameState(PTNode * comparisonNodePtr)               
{

  pose2 curPose = m_nodePose;
  pose2 comparisonPose = comparisonNodePtr->m_nodePose;
  float xdiff = comparisonPose.x - curPose.x;
  float ydiff = comparisonPose.y - curPose.y;
  float angdiff = comparisonPose.ang - m_nodePose.ang;
  float pdiff2 = xdiff * xdiff + ydiff * ydiff;

  if ((pdiff2 < SAME_XY2) && ((angdiff * angdiff) < SAME_ANG2))
    { return 1; }
  else
    { return 0; };
};


