/*
 * Clothoid.cc
 * Helper functions for the Clothoid Class. The Clothoid class is a container for representations 
 * of clothoid curves (the basic curves that we're using to build our trajectories), while the 
 * ClothoidTreeNode class builds upon the Clothoid class to make trees of Clothoid Trajectories
 *
 * Based on code originally written by Kenny Oslund.  
 * Modified by Joel Burdick starting Aug. 22, 2007
 */

#include "Clothoid.hh"

/* ------------------------------------------------------------------------------------ */
/* ---------------------------------- constructors  ----------------------------------- */
/* ------------------------------------------------------------------------------------ */

/* Old Historical constructor for the Clothoid Class. This is used by circle-planner to make 
 * "blending" clothiods between circular arcs and straight lines, or between two circular arcs. 
 * This constructor fills in the clothoid  points based on the curvature and length parameters, 
 * as well as the starting position. The points are generated by a simple trapezoidal integration.  
 * While one could improve upon this integration scheme, it seems to work reasonably well.
 */

Clothoid::Clothoid(pose2 startPose, double initCurv, double finalCurv, double length, bool reverse)
{
  pose2 point(startPose);

  m_clothoidPoints.clear();                     // clear clothoid poses just to be sure
  m_initCurv = initCurv;
  m_finalCurv = finalCurv;
  m_length = length;
  setSharpness(initCurv, finalCurv, length);    // calculate and set clothoid sharpness factor
  m_reverse = reverse;                          // driving in reverse?

  // the "local cost" is initiallyl set to path length for forward paths, and a multipel of the path length for reverse paths.
  if (m_reverse)                               
    { 
      point.rotateInPlace(M_PI);
      m_localEdgeCost = REVERSE_PATH_PENALTY * length;
    }
  else
    { m_localEdgeCost = length;};

  m_clothoidPoints.push_back(point);            // save the starting pose as first point in the list

  double s, cosTheta, sinTheta;
  double prevCosTheta, prevSinTheta;

  prevCosTheta = cos(point.ang);
  prevSinTheta = sin(point.ang);

  double prevCurv = m_initCurv, curCurv;

  // number of segments to use in discretization of Clothoid Path
  int segmentPoints = (int)round(m_length/CP_PATH_SECTION_LENGTH) + 1; 

  // iterate along trajectory calculating curvature, ang, x and y.  This is Osland's original
  // trapezoidal integration rule approach.  This could be improved to minimize round-off error,
  // but it seems to work reasonably well effects

  for (int j=1; j<segmentPoints; j++)
    {
      s = j * CP_PATH_SECTION_LENGTH;                          // calculate current arc length
      curCurv = getCurvature(s);                            // get path curvature at this arc length
      
      // Use trapezoidal numerical integration to get angle of tangent to curve: 
      //      theta(s) = /int^s_0 k(t) dt
      point.rotateInPlace( (prevCurv + curCurv) * CP_PATH_SECTION_LENGTH * 0.5 );
 
      cosTheta = cos(point.ang);
      sinTheta = sin(point.ang);
      // integrate angle to get x-coordinate: x(s) = /int^s_0 cos /theta (t) dt
      point.x +=  (cosTheta + prevCosTheta) * CP_PATH_SECTION_LENGTH * 0.5;
      // integrate angle to get y-coordinate: y(s) = /int^s_0 sin /theta (t) dt
      point.y +=  (sinTheta + prevSinTheta) * CP_PATH_SECTION_LENGTH * 0.5;
      
      prevCurv = curCurv;                                  // iterate on curvature and angles
      prevCosTheta = cosTheta;
      prevSinTheta = sinTheta;

      /* flip the point back around to point in the same direction as alice
	 before saving if we're going in reverse (points for the running 
	 calculation are still pointing backwards) */

      if (m_reverse){point.rotateInPlace(-M_PI);};      
      m_clothoidPoints.push_back(point);               // add this point to end of list
    };
  return;
};

// constructor used by circle-planner: just sets up the clothoid memory and a few parameters
Clothoid::Clothoid(double initCurv, double finalCurv, double length, bool reverse)
{
  m_clothoidPoints.clear();                     // clear clothoid poses just to be sure

  setInitCurv(initCurv);                        // set initial and final curvatures
  setFinalCurv(finalCurv);   
  setCurvLength(length);                        // set clothoid length 
  setSharpness(initCurv, finalCurv, length);    // calculate and set clothoid sharpness factor
  m_reverse = reverse;                          // driving in reverse?
  m_length = length;

  if (m_reverse)                                // set the initial cost of this edge
    { m_localEdgeCost = REVERSE_PATH_PENALTY * length;}          // if a reverse path, make its cost higher
  else
    { m_localEdgeCost = length;};
};

// constructor used by circle-planner: just sets up the clothoid memory and a few parameters
Clothoid::Clothoid(double initCurv, double finalCurv, double length, bool reverse, int index)
{
  m_clothoidPoints.clear();                     // clear clothoid poses just to be sure

  setInitCurv(initCurv);                        // set initial and final curvatures
  setFinalCurv(finalCurv);   
  setCurvLength(length);                        // set clothoid length 
  setSharpness(initCurv, finalCurv, length);    // calculate and set clothoid sharpness factor
  m_reverse = reverse;                          // driving in reverse?
  m_length = length;
  m_clothoidIndex = index;

  if (m_reverse)                                // set the initial cost of this edge
    { m_localEdgeCost = REVERSE_PATH_PENALTY * length;}          // if a reverse path, make its cost higher
  else
    { m_localEdgeCost = length;};
};

// Alternate Constructor which primarily just allocated memory for an instance of the clothoid class 
Clothoid::Clothoid()
{
  m_clothoidPoints.clear(); 
  m_initCurv =0.0; m_finalCurv=0.0;
  m_sharpness = 0.0; m_length=0.0;
  m_localEdgeCost = 0.0;
};

/* ------------------------------------------------------------------------------------ */
/* ----------------------------------  destructor ------------------------------------- */
/* ------------------------------------------------------------------------------------ */

/*
Clothoid::~Clothoid()
{
  // delete all the pose2's in the clothoid list
  m_clothoidPoints.~vector();
};
*/

/* ------------------------------------------------------------------------------------ */
/*                                accessor/storage functions                            */
/* ------------------------------------------------------------------------------------ */


void Clothoid::setInitCurv(double initial_Curvature)         // set the initial clothoid curvature
{
  m_initCurv = initial_Curvature;
};

void Clothoid::setFinalCurv(double final_Curvature)         // set the final clothoid curvature
{
  m_finalCurv = final_Curvature;
};

void Clothoid::setCurvLength(double Curve_Length)           // set length of Clothoid
{
  m_length = Curve_Length;
};

// calculate and set the clothoid's sharpness factor
void Clothoid::setSharpness(double initCurv, double finalCurv, double length)                  
{
  m_sharpness = (finalCurv - initCurv)/length; 
};


/* accessor function that estimates the curvature at a point along the clothoid given by parameter
   "position" (where position is measured in meters from the beginning of the clothoid) */
double Clothoid::getCurvature(double position)
{
  return (m_initCurv + position * m_sharpness);
}

int Clothoid::getNumPoints()
{
  int npoints = m_clothoidPoints.size();
  return npoints;
};

/* ------------------------------------------------------------------------------------ */
/*                           printing/visualization functions                           */
/* ------------------------------------------------------------------------------------ */

/* printing functions to dump internal data for purposes of debuggin */
void Clothoid::printClothoid(ostream *f)
{
  if (!(this == NULL))                                                  // check to see if clothoid is NULL
    {
      if (m_clothoidPoints.size() > 0)                                  // now check for nonzero # of points
	{for (vector<pose2>::iterator i = m_clothoidPoints.begin(); i != m_clothoidPoints.end(); i++)
	  {   i->print(f);   };
	};
    };
  return;
}


void Clothoid::printClothoidEndPoint(ostream *f)
{
  if (!(this == NULL))                                                  // check to see if clothoid is NULL
    {
      pose2 endPose = m_clothoidPoints.back();
      endPose.print(f);
    };
  return;
}

/* ------------------------------------------------------------------------------------ */
/*                                  copying functions                                   */
/* ------------------------------------------------------------------------------------ */


Clothoid * Clothoid::CopyClothoid()
{
  Clothoid * returnPtr;
  returnPtr = new Clothoid(this->m_initCurv,this->m_finalCurv,this->m_length, this->m_reverse);
  for(vector<pose2>::iterator j = this->m_clothoidPoints.begin(); j!=this->m_clothoidPoints.end(); j++)
    {
      pose2 copyPoint((*j).x,(*j).y,(*j).ang);
      returnPtr->m_clothoidPoints.push_back(copyPoint);
    };
  return returnPtr;
};


/* ------------------------------------------------------------------------------------ */
/*                               Other Work Functions                                   */
/* ------------------------------------------------------------------------------------ */

// displace a clothoid by dPose
Clothoid *  Clothoid::DisplaceClothoid(pose2 dPose)
{
  Clothoid *  returnClothoid = new Clothoid(this->m_initCurv, this->m_finalCurv, this->m_length, this->m_reverse, this->m_clothoidIndex);
  
  for(vector<pose2>::iterator i=this->m_clothoidPoints.begin(); i!=this->m_clothoidPoints.end(); i++)
    {
      double cangle = cos(dPose.ang); double sangle=sin(dPose.ang);
      pose2 curPose = (*i);
      pose2 displacedPoint = pose2((curPose.x * cangle - curPose.y * sangle), 
				   (curPose.x * sangle + curPose.y * cangle), 
				   (curPose.ang + dPose.ang));
      returnClothoid->m_clothoidPoints.push_back(displacedPoint);
    };
  return returnClothoid;
};

