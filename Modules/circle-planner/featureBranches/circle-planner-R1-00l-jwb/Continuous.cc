/* CirclePlanner.cc: functions that implement a nonholonomic motion planner for a car.
 * based on the concept of "circles" at the goal and start--hence the name circle-planner.
 * This file contains the main start-goal parts of circle planner, while CircleSplicer.cc
 * contains related code to "splice together" other circles.
 *
 * Written by Joel Burdick: Oct. 24, 2007
 *
 */

/* ------------------------------------------------------------------------------------- */
/*                                 Defines and Includes                                  */
/* ------------------------------------------------------------------------------------- */

#include "CirclePlanner.hh"

/* ------------------------------------------------------------------------------------- */
/*                           Basic Path Construction Functions                           */
/* ------------------------------------------------------------------------------------- */


// construct fixed clothoids that are helpful for blending Alice's steering. This function
// also sets up some fixed parameters that are useful elsewhere.  Because this is strictly
// a computational approach based on some approximations, we take the average of a couple of
//  results to get some key quantities.

void CirclePlanner::MakeConnectionClothoids()
{
  pose2 startPose = pose2(0.0,0.0,0.0);

  // make connecting clothoids for 4 key different types of CC circles
  m_fwdLeftClothoid = new Clothoid(startPose, 0.0, (1.0/TURNING_RADIUS), (TURNING_LENGTH/2.0), false);
  m_fwdLeftInvClothoid = new Clothoid(startPose, (1.0/TURNING_RADIUS), 0.0, (TURNING_LENGTH/2.0), false);

  m_fwdRightClothoid = new Clothoid(startPose, 0.0, (-1.0/TURNING_RADIUS), (TURNING_LENGTH/2.0), false);
  m_fwdRightInvClothoid = new Clothoid(startPose, (-1.0/TURNING_RADIUS), 0.0, (TURNING_LENGTH/2.0), false);

  m_revLeftClothoid = new Clothoid(startPose, 0.0, (-1.0/TURNING_RADIUS), (TURNING_LENGTH/2.0), true);
  m_revLeftInvClothoid = new Clothoid(startPose, (-1.0/TURNING_RADIUS), 0.0, (TURNING_LENGTH/2.0), true);

  m_revRightClothoid = new Clothoid(startPose, 0.0, (1.0/TURNING_RADIUS), (TURNING_LENGTH/2.0), true);
  m_revRightInvClothoid = new Clothoid(startPose, (1.0/TURNING_RADIUS), 0.0, (TURNING_LENGTH/2.0), true);

  // save the last pose of each canonical connecting clothoid for convenience
  m_fwdLeftEndPose     =  m_fwdLeftClothoid->m_clothoidPoints.back();
  m_fwdLeftInvEndPose  =  m_fwdLeftInvClothoid->m_clothoidPoints.back();

  m_fwdRightEndPose    =  m_fwdRightClothoid->m_clothoidPoints.back();
  m_fwdRightInvEndPose =  m_fwdRightInvClothoid->m_clothoidPoints.back();

  m_revLeftEndPose     =  m_revLeftClothoid->m_clothoidPoints.back();
  m_revLeftInvEndPose  =  m_revLeftInvClothoid->m_clothoidPoints.back();

  m_revRightEndPose    =  m_revRightClothoid->m_clothoidPoints.back();
  m_revRightInvEndPose =  m_revRightInvClothoid->m_clothoidPoints.back();

  point2 fwdLeftOffset = TURNING_RADIUS * point2(- sin(m_fwdLeftEndPose.ang), cos(m_fwdLeftEndPose.ang));
  m_fwdLeftCenter = point2(m_fwdLeftEndPose.x, m_fwdLeftEndPose.y) + fwdLeftOffset;

  point2 fwdRightOffset = TURNING_RADIUS * point2( sin(m_fwdRightEndPose.ang),-cos(m_fwdRightEndPose.ang));
  m_fwdRightCenter = point2(m_fwdRightEndPose.x, m_fwdRightEndPose.y) + fwdRightOffset;

  point2 revLeftOffset = TURNING_RADIUS * point2( - sin(m_revLeftEndPose.ang), cos(m_revLeftEndPose.ang));
  m_revLeftCenter  = point2(m_revLeftEndPose.x, m_revLeftEndPose.y) + revLeftOffset;
  
  point2 revRightOffset = TURNING_RADIUS * point2( sin(m_revRightEndPose.ang), -cos(m_revRightEndPose.ang) );
  m_fwdRightCenter = point2(m_revRightEndPose.x, m_revRightEndPose.y) + revRightOffset;

  // compute the radius of the CC circle.  We'll do this by taking the average of four values
  m_CCRadius = 0.5 * (sqrt(m_fwdLeftCenter.x * m_fwdLeftCenter.x + m_fwdLeftCenter.y * m_fwdLeftCenter.y) +
				     sqrt(m_fwdRightCenter.x * m_fwdRightCenter.x + m_fwdRightCenter.y * m_fwdRightCenter.y) + 
				     sqrt(m_revLeftCenter.x * m_revLeftCenter.x + m_revLeftCenter.y * m_revLeftCenter.y) +
				     sqrt(m_revRightCenter.x * m_revRightCenter.x + m_revRightCenter.y * m_revRightCenter.y));

  // compute the "beta" angle, which determines how we connect start or goal to mu-bitangents
  double betaFwdLeft = FindTheta(m_fwdLeftCenter,fwdLeftOffset);
  double betaFwdRight = -1.0 * FindTheta(m_fwdRightCenter,fwdRightOffset);
  double betaRevLeft = -1.0 * FindTheta(m_revLeftCenter,revLeftOffset);
  double betaRevRight = FindTheta(m_revRightCenter,revRightOffset);

  m_betaAngle = 0.25 * (betaFwdLeft + betaFwdRight + betaRevLeft + betaRevRight);

  // compute and store the key angle "mu", which is needed for determing the mu-bitangents. Again, average over results
  point2 x_axis = point2(1.0,0.0);
  double muAngleFwdLeft = 0.5 * M_PI - FindTheta(x_axis,m_fwdLeftCenter);
  double muAngleFwdRight = 0.5 * M_PI + FindTheta(x_axis,m_fwdRightCenter);
  point2 neg_x_axis = point2(-1.0,0.0);
  double muAngleRevLeft = 0.5 * M_PI + FindTheta(neg_x_axis,m_revLeftCenter);;
  double muAngleRevRight = 0.5 * M_PI - FindTheta(neg_x_axis,m_revRightCenter);; ;

  m_muAngle = 0.25 * (muAngleFwdLeft + muAngleFwdRight + muAngleRevLeft + muAngleRevRight);
};

PTEdge * CirclePlanner::makeFwdCircArc(point2 startCenter, double startAngle, double arc_angle, unsigned int handedness, double radius)
{
  int i, npoints;
  double arclength = fabs(radius * arc_angle);
  
  // Allocate a forward Clothoid for this circular arc. 
  Clothoid * newClothoidPtr = new Clothoid(radius, radius, arclength, false, m_nClothoids);
  m_nClothoids++; 

  npoints = 1 + (unsigned int) floor(arclength/CP_PATH_SECTION_LENGTH);
  if (npoints == 1) {npoints = 2;}                   // check for exceptional condition: arclength < PATH_SECTION_LENGTH
  double stepAngle = arc_angle/(npoints -1);         // the amount of arc angle to increment between path points

  for(i=1; i<npoints; i++)
    {
      double curAngle = startAngle + (double) i * stepAngle;
      pose2 newPose = pose2( (startCenter.x + cos(curAngle) * radius), (startCenter.y + sin(curAngle) * radius), curAngle);
      if (handedness == RIGHT_HAND_CIRCLE)
	{newPose.ang -= M_PI/2.0; }
      else
	{newPose.ang += M_PI/2.0; }
      newPose.unwrap_angle();
      newClothoidPtr->m_clothoidPoints.push_back(newPose);
    };
  PTEdge * newPTEdgePtr = new PTEdge(newClothoidPtr);
  return newPTEdgePtr;
};

PTEdge * MakeSymClothoid(pose2 startPose, pose2 endPose)
{
  
};

// make paths that connect a start node (or start-like node) to a bitangent through nominally "forward" paths.  Some
// paths are actually use reverse, but only as a shorter alternative to long forward paths.  This function returns 
// a pointer to the first PTEdge in the chain of paths.  Multiple paths are linked inside this function.

PTEdge * CirclePlanner::startFwdConnect(pose2 startPose, pose2 endPose, point2 startCenter, float startAngle, float stopAngle, 
					unsigned int handedness, double RADIUS)
{		  		  
  double deltaAngle;
  Clothoid * newClothoidPtr;

  // first find the angle (using a right handed system) between the "start" angle and the "stop" angle.  The
  // complex if/then/else structure below accounts for all of the possible sign and magnitude combinations of 
  // the start/stop angles

  if (handedness == RIGHT_HAND_CIRCLE)             // clockwise rotation (decreasing angle) is forward motion
    {
      if (startAngle>0)
	{
	  if (stopAngle > 0)
	    {
	      if (stopAngle > startAngle)    
		{  deltaAngle = stopAngle - startAngle - 2.0 * M_PI; }
	      else   
		{ deltaAngle = (stopAngle - startAngle); };
	    }
	  else       // (startAngle > 0) && (stopAngle < 0)
	    { deltaAngle = stopAngle-startAngle;  };
	}         
      else           // if we reach here, (startAngle <= 0)
	{ 
	  if (stopAngle>0)        // (Pi > stopAngle > 0) && (-Pi < startAngle < 0) 
	    { deltaAngle = stopAngle - startAngle - 2.0 * M_PI;  }
	  else       // (-PI < stopAngle <= 0) && (-Pi < startAngle <= 0)
	    {
	      if (stopAngle > startAngle) 
		{ deltaAngle = stopAngle - startAngle - 2.0 * M_PI; }
	      else
		{  deltaAngle = (stopAngle - startAngle); };
	    };     
	};
    }
  else    //handedness == LEFT_HAND, so that counterclockwise rotation is forward motion.(with increasing angle)
    {
      if (startAngle > 0)
	{
	  if (stopAngle > 0)
	    {
	      if (stopAngle > startAngle)
		{ deltaAngle = stopAngle-startAngle; }
	      else
		{ deltaAngle = stopAngle-startAngle + 2.0 * M_PI; };
	    }
	  else  // (startAngle > 0) && (stopAngle < 0)
	    { deltaAngle = stopAngle-startAngle + 2.0 * M_PI; };
	}
      else                       // (startAngle < 0)
	{
	  if (stopAngle > 0)     // (startAngle < 0) && (stopAngle > 0)
	    { deltaAngle = stopAngle-startAngle;  }
	  else                   // (startAngle < 0) && (stopAngle < 0)
	    {
	      if (stopAngle >  startAngle)
		{ deltaAngle = stopAngle-startAngle; }
	      else
		{ deltaAngle = stopAngle-startAngle + 2.0 * M_PI; };
	    };
	};
    };
  // now that deltaAngle has been computed, determine what types of connection paths we need.
  // first consider counter-clockwise rotation (where Alice goes in CCW direction from start to stop)
  PTEdge * returnEdge;            // the pointer that will get returned

  if (handedness == LEFT_HAND_CIRCLE)  
    {
      if(deltaAngle >= 0.0)  	  // if true, then connecting path is nominally forward 
	{
	  if ( deltaAngle > (2.0 * CirclePlanner::m_betaAngle) )
	    {  
	      // connecting path is forward clothoid, forward circular arc, forward clothoid, 
	      // Fist, displace the cannonical clothoid to the starting Pose, and make an associated PTEdge
	      Clothoid * firstClothoid = m_fwdLeftClothoid->DisplaceClothoid(startPose);
	      returnEdge = new PTEdge(firstClothoid);     // the edge pointer that will be returned and linked elsewhere
	      
	      // next make a PTNode at the end of this first Clothoid
	      PTNode * nodePtr1 = new PTNode(firstClothoid->m_clothoidPoints.back());
	      
	      // next make the circular arc component and its associated PTEdge
	      double circular_arc_angle = deltaAngle - (2.0 * CirclePlanner::m_betaAngle);
	      PTEdge * fwdCircArcEdge = 
		makeForwardCircularArc(startCenter, (startAngle + CirclePlanner::m_betaAngle), 
				       (startAngle + CirclePlanner::m_betaAngle + circular_arc_angle),
				       LEFT_HAND_CIRCLE, TURNING_RADIUS);
	      // next make a PTnode at the end of the circular arc
	      pose2 endArcPose = fwdCircArcEdge->m_clothoid->m_clothoidPoints.back();
	      PTNode * nodePtr2 = new PTNode(endArcPose);

	      Clothoid * secondClothoid = CirclePlanner::m_fwdLeftInvClothoid->DisplaceClothoid(endArcPose);
	      PTEdge * thirdEdge = new PTEdge(secondClothoid);

	      // now link up the result starting from the end, and prepare it to pass back. 
	      nodePtr2->m_outEdges.push_back(thirdEdge);
	      fwdCircArcEdge->m_childPTNodePtr = nodePtr2;
	      nodePtr1->m_outEdges.push_back(fwdCircArcEdge);
	      returnEdge->m_childPTNodePtr = nodePtr1;
	    }
	  else 
	    {
	      if( deltaAngle > (2.0 * m_muAngle))  // the connect path consists of two symmetric clothoid segments.
		{
		  returnEdge =  MakeSymClothoid(startPose, endPose);
		}
	      else 
		{ 
		  if( deltaAngle == (2.0 * m_muAngle) )       // the connecting path is a straight line
		    { 
		      point2 startPoint = point2(startPose.x,startPose.y);
		      point2 endPoint = point2(endPose.x,endPose.y);
		      returnEdge = makeStraightLine(startPoint, endPoint, false);  
		    }
		  else                    // the connecting path is fwd clothoid, reverse circular arc, and fwd clothoid, as well as
		    {                     // an alternative path that is forward clothoid, long forward arc, and forwad clothoid.
		      // this clothoid is the same for both choices.
		      Clothoid * firstClothoid = CirclePlanner::m_fwdLeftClothoid->DisplaceClothoid(startPose);
		      returnEdge = new PTEdge(firstClothoid);

		      // next make a PTNode at the end of this first Clothoid
		      PTNode * nodePtr1 = new PTNode(firstClothoid->m_clothoidPoints.back());

		      double angle1 = startAngle + CirclePlanner::m_betaAngle;
		      double angle2 = stopAngle  - 2.0 * CirclePlanner::m_betaAngle;
		      PTEdge * revCircArcEdge = makeReverseCircularArc(startCenter,angle1,angle2, LEFT_HAND_CIRCLE, TURNING_RADIUS);
		      
		      // next make a PTnode at the end of the circular arc
		      pose2 endArcPose = revCircArcEdge->m_clothoid->m_clothoidPoints.back();
		      PTNode * nodePtr2 = new PTNode(endArcPose);

		      Clothoid * secondClothoid = CirclePlanner::m_fwdLeftInvClothoid->DisplaceClothoid(endArcPose);
		      PTEdge * thirdEdge = new PTEdge(secondClothoid);

		      // now link up the result starting from the end, and prepare it to pass back. 
		      nodePtr2->m_outEdges.push_back(thirdEdge);
		      revCircArcEdge->m_childPTNodePtr = nodePtr2;
		      nodePtr1->m_outEdges.push_back(revCircArcEdge);
		      returnEdge->m_childPTNodePtr = nodePtr1;
		    };
		};
	    };
	};
    };
  return returnEdge;
};

/* ------------------------------------------------------------------------------------- */
/*                                   Main Work Functions                                 */
/* ------------------------------------------------------------------------------------- */


// Compute the relevant mu-bitangents for a pair of circles that either are the Start/Goal circles, or will behave
// like start and goal circles.  The the bitangent paths are linked to the Path Tree Start Node to form part 
// of the Path Tree graph that will get searched for solutions

void CirclePlanner::CCStartGoalPair(pose2 startPose, pose2 goalPose, unsigned int handedness)
{
  // first let's find the "outer" or "external" mu-bitangents

  point2 startPt = point2(startPose.x, startPose.y);  double startAngle = startPose.ang;
  point2 goalPt = point2(goalPose.x, goalPose.y);  double goalAngle = startPose.ang;

  // transform the canonical bsic four starting circle centers to the current coordinate system
  point2 fwdLeftStartLine = RotVect(m_fwdLeftCenter,startAngle);
  point2 startFwdLeftCenter = startPt + fwdLeftStartLine;

  point2 revLeftStartLine = RotVect(m_revLeftCenter,startAngle);
  point2 startRevLeftCenter = startPt +   revLeftStartLine;

  point2 fwdRightStartLine = RotVect(m_fwdRightCenter,startAngle);
  point2 startFwdRightCenter = startPt + fwdRightStartLine;

  point2 revRightStartLine = RotVect(m_revRightCenter,startAngle);
  point2 startRevRightCenter = startPt + revRightStartLine;

  // transform the canonical bsic four goal circle centers to the current coordinate system
  point2 fwdLeftGoalLine = RotVect(m_fwdLeftCenter,goalAngle);
  point2 goalFwdLeftCenter = goalPt + fwdLeftGoalLine;

  point2 revLeftGoalLine =  RotVect(m_revLeftCenter,goalAngle);
  point2 goalRevLeftCenter = goalPt +revLeftGoalLine;

  point2 fwdRightGoalLine = RotVect(m_fwdRightCenter,goalAngle);
  point2 goalFwdRightCenter = goalPt + fwdRightGoalLine;

  point2 revRightGoalLine = RotVect(m_revRightCenter,goalAngle);
  point2 goalRevRightCenter = goalPt + revRightGoalLine;

  // now compute key mu-bitangent contact points for a forward left start circle and a forward left goal circle;

  double x12 = goalFwdLeftCenter.x - startFwdLeftCenter.x;          // distances between centers of left start and left goal circles
  double y12 = goalFwdLeftCenter.y - startFwdLeftCenter.y;
  double L12 = sqrt(x12 * x12 + y12 * y12);  
  double phi12 = atan2(y12,x12);                                    // angle made by vector from startCenter to GoalCenter
  const point2 x_axis = point2(1.0,0.0);                            // the unit vector colinear with the x-axis
  const point2 neg_x_axis = point2(1.0,0.0);                        // the unit vector colinear with the negative x-axis

  // angles on the CC circle which define location of the mu-bitangent contact points
  double theta1a = phi12 + 0.5 * M_PI - m_muAngle;
  double theta1b = phi12 - 0.5 * M_PI + m_muAngle;
  double theta2a = phi12 + 0.5 * M_PI + m_muAngle;
  double theta2b = phi12 - 0.5 * M_PI - m_muAngle;

  // the points where the mu-bitangents intersect a pair of circles (start fwd left  and goal fwd left)
  point2 z1a = startFwdLeftCenter + m_CCRadius * point2(cos(theta1a),sin(theta1a));
  point2 z1b = startFwdLeftCenter + m_CCRadius * point2(cos(theta1b),sin(theta1b));
  point2 z2a = goalFwdLeftCenter + m_CCRadius * point2(cos(theta2a),sin(theta2a));
  point2 z2b = goalFwdLeftCenter + m_CCRadius * point2(cos(theta2b),sin(theta2b));

  //  find the angles corresponding to the robot's start and goal poses,
  float fwdLeftStartAngle = FindTheta(x_axis,(-1.0 * fwdLeftStartLine));     // angle made on fwd left start circle by Alice's start pose
  float fwdLeftGoalAngle = FindTheta(x_axis,(-1.0 * fwdLeftGoalLine));       // angle made on fwd left goal circle by Alice's goal pose

  // First check to see if the circles are overlapping too much (ADD THIS)

  // now make the clothoids that connect the start to the first outer bitangent
  
  // now make the clothoid bi-tangent
  PTEdge * biTangent1Ptr = makeStraightLine(z1a,z2a,true);             // this bitangent between LeftFwd and LeftFwd circles is in reverse

  // now make the clothoids that connect the distal end of the bitangent to the goal circle
  PTEdge * startPath = startFwdConnect(startPose, goalPose, startFwdLeftCenter, fwdLeftStartAngle, fwdLeftGoalAngle, LEFT_HAND_CIRCLE, TURNING_RADIUS);

  //now connect all of the pieces 

  return;
};
