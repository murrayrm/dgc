close all;
clear all;

% plot a single clothoid or clothoid tree
figure(3);
dlmread culledTree.dat;
clothoids = ans;
plot(clothoids(:,1), clothoids(:,2), '.','MarkerSize',10);
hold on;
xlabel('X (meters)');
ylabel('Y (meters)');
title('Clothoid Tree');
axis equal;

return;

