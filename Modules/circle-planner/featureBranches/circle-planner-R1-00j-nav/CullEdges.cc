/*
 * CullEdges.cc: Various related functions that use collision checking to "cull" edges from the Path Tree
 *
 * Written by Joel Burdick, Oct. 2007
 */

/* ----------------------------------------------------------------------------------- */
/*                                     includes                                        */
/* ----------------------------------------------------------------------------------- */

#include <alice/AliceConstants.h>
#include "CirclePlanner.hh"

/* ------------------------------------------------------------------------------------------------- */
// this function "culls" infeasible path segments from the path tree based on collision checking
/* ------------------------------------------------------------------------------------------------- */

void CirclePlanner::CullNodeOutEdges(PTNode * nodePtr)
{
  if(nodePtr->m_outEdges.empty())              // if node has no outEdges, there's nothing to do
    {
      nodePtr->m_hasNodeBeenCulled = true;
      return;
    }
  else
    { // first check to see if this node has already been visited and culled.  If not, proceed.  If so, return
      if (nodePtr->m_hasNodeBeenCulled == false)
	{ 	 
	  // Now eliminate infeasible edges.  We can't immediately erase them from the outEdges list because 
          // we're iterating on that list.  So, we'll save the feasible edges to reconstruct outEdges at the end
	  vector<PTEdge *> feasibleEdgeList;                    
	  feasibleEdgeList.clear();

	  for (vector<PTEdge *>::iterator i=nodePtr->m_outEdges.begin(); i!=nodePtr->m_outEdges.end(); i++)
	    {
	      PTNode * childPtr = (*i)->m_childPTNodePtr;       // distal node of the present edge i

	      // before trying to cull an edge, first cull the outEdges of this edge's distal node
	      if (!childPtr->m_outEdges.empty())
		{   CullNodeOutEdges(childPtr);  };

	      if( IsPathLegal((*i)->m_clothoid) == true)      // if edge is feasible, push it on the feasible list.
		{  feasibleEdgeList.push_back((*i)); };
	    };
	  nodePtr->m_hasNodeBeenCulled = true;

	  nodePtr->m_outEdges.clear();                         // now reattach all of the feasible outEdges to the node
	  for (vector<PTEdge *>::iterator j=feasibleEdgeList.begin(); j!=feasibleEdgeList.end(); j++)
	    { nodePtr->m_outEdges.push_back((*j)); };
	}
      else
	{return; };
    };
};


#if CIRCLE_PLANNER_DEBUG
// the alternate version of CullNodeOutEdges that spits out internal state for debugging purposes
void CirclePlanner::CullNodeOutEdges(PTNode * nodePtr, ofstream *f)
{
  if(nodePtr->m_outEdges.empty())              // if node has no outEdges, there's nothing to do
    {
      nodePtr->m_hasNodeBeenCulled = true;
      return;
    }
  else
    { // first check to see if this node has already been visited and culled.  If not, proceed.  If so, return
      if (nodePtr->m_hasNodeBeenCulled == false)
	{ 	 
	  // Now we'll eliminate the infeasible edges.  We can't erase them from the list of outEdges immediately,
	  // because we're iterating on that list.  So, we'll save up the list of feasible edges, and reconstruct
	  // them at the end.
	  vector<PTEdge *> feasibleEdgeList;                    
	  feasibleEdgeList.clear();

	  for (vector<PTEdge *>::iterator i=nodePtr->m_outEdges.begin(); i!=nodePtr->m_outEdges.end(); i++)
	    {
	      PTNode * childPtr = (*i)->m_childPTNodePtr;       // distal node of the present edge i

	      // before trying to cull an edge, first cull the outEdges of this edge's distal node
	      if (!childPtr->m_outEdges.empty())
		{   CullNodeOutEdges(childPtr,f);  };

	      if( IsPathLegal((*i)->m_clothoid,f) == true)      // if edge is feasible, push it on the feasible list.
		{
		  feasibleEdgeList.push_back((*i)); 
		};
	    };
	  nodePtr->m_hasNodeBeenCulled = true;

	  nodePtr->m_outEdges.clear();	          // now reattach all of the feasible outEdges to the node
	  for (vector<PTEdge *>::iterator j=feasibleEdgeList.begin(); j!=feasibleEdgeList.end(); j++)
	    { nodePtr->m_outEdges.push_back((*j)); };
	}
      else
	{return; };
    };
};
#endif


//-------------------------------------------------------------------------------------------------------------
// remove all infeasible paths (those that intersect the zone boundary).  Right now this is a simple wrapper.
// but it might get expanded in the future.  See CirclePlannerUtils.cc for CullNodeOutEdges
void CirclePlanner::CullInfeasiblePaths()
{
  CullNodeOutEdges(this->m_startNodePtr);
};

#if CIRCLE_PLANNER_DEBUG                                  
void CirclePlanner::CullInfeasiblePaths(ofstream *f)    // version for debugging
{
  CullNodeOutEdges(this->m_startNodePtr, f);
};
#endif


/* ------------------------------------------------------------------------------------------------- */
// this function, like IsPathLegal, test for the legality of a path.  However, when a path is illegal,
// because it collides with a boundary, it will recursive expand the path from a point a little
// before the the collision point.  The amount of recursion is controlled by the "depth" variable
/* ------------------------------------------------------------------------------------------------- */


bool CirclePlanner::ExpansiveIsPathLegal(PTEdge * edgePtr, int depth)
{
  bool testIntersection;
  int j=1;
  Clothoid * cPtr = edgePtr->m_clothoid;

  //iterate though all points of the Clothoid to check for overlap of Alice and zone boundary
  for (vector<pose2>::iterator i=cPtr->m_clothoidPoints.begin(); i!=cPtr->m_clothoidPoints.end(); i++, j++)
    {
      point2arr curAliceBoundingBox = AliceBoundingBox( (*i), m_safetyMargins);

      testIntersection = this->m_rightHandedZoneBoundary.is_intersect(curAliceBoundingBox);

      // If there is an intersection, then try to first  expand out new paths from a pose just before 
      // intersection.  We won't expand out paths that are too short, as they are not worth pursuing.
      if (testIntersection==true)             // if alice intersects boundary
	{ 
	  if ( (depth > 0) && (j>=5) )        // if the current clothoid has at least 5 poses, and we haven't 
	    {                                 //    gone beyond the specified depth level, then expand the paths
	      j -=2;                                     // go back two path points from the end
	      cPtr->m_clothoidPoints.resize(j);          // clip off all of the poses after the jth pose

	      // make a new Path Node at the clipped end of the clothoid, and link it to the associated edge
	      pose2 endPose = cPtr->m_clothoidPoints.back();
	      edgePtr->m_childPTNodePtr = new PTNode(endPose);  

	      // now expand from this new node.  First, create circles passing through this node
	      double cosAngle = cos(endPose.ang);    double sinAngle = sin(endPose.ang);
	      point2 rightCenter = point2(endPose.x + TURNING_RADIUS * sinAngle, endPose.y - TURNING_RADIUS * cosAngle);
	      point2 leftCenter  = point2(endPose.x - TURNING_RADIUS * sinAngle, endPose.y + TURNING_RADIUS * cosAngle);
	      double radius = this->m_rightGoalCircle->m_Radius;
	      CircleData * RCirclePtr = new CircleData(edgePtr->m_childPTNodePtr,rightCenter, radius, RIGHT_HAND_CIRCLE);
	      CircleData * LCirclePtr = new CircleData(edgePtr->m_childPTNodePtr,leftCenter, radius, LEFT_HAND_CIRCLE);

	      // Next, create the paths from these circles to the goal circles
	      StartGoalCirclePair(RCirclePtr, this->m_rightGoalCircle, RIGHT_RIGHT_COMBO, radius);
	      StartGoalCirclePair(RCirclePtr, this->m_leftGoalCircle, RIGHT_LEFT_COMBO, radius);
	      StartGoalCirclePair(LCirclePtr, this->m_rightGoalCircle, LEFT_RIGHT_COMBO, radius);
	      StartGoalCirclePair(LCirclePtr, this->m_leftGoalCircle, LEFT_LEFT_COMBO, radius);

	      // now process these newly developed paths, because the main Cull routine will otherwise not process them.
	      CullAndExpandNodeOutEdges(edgePtr->m_childPTNodePtr, (depth -1));
	      return true;
	    }
	  else            // we can't expand this path due to depth or size restrictions, so it is a dead path
	    {  return false;  };  
	};
    };
  return true;
};

#if CIRCLE_PLANNER_DEBUG
// another version that spits out internal state for debugging purposes.
bool CirclePlanner::ExpansiveIsPathLegal(PTEdge * edgePtr, ofstream *f, int depth)
{
  bool testIntersection;
  int j=1;
  Clothoid * cPtr = edgePtr->m_clothoid;

  //iterate though all points of the Clothoid to check for overlap of Alice and zone boundary
  for (vector<pose2>::iterator i=cPtr->m_clothoidPoints.begin(); i!=cPtr->m_clothoidPoints.end(); i++, j++)
    {
      point2arr curAliceBoundingBox = AliceBoundingBox((*i), m_safetyMargins);
      /*      printPoint2arr(curAliceBoundingBox,f); */

      testIntersection = this->m_rightHandedZoneBoundary.is_intersect(curAliceBoundingBox);

      // If there is an intersection, then try to first  expand out new paths from a pose just before 
      // intersection.  We won't expand out paths that are too short, as they are not worth pursuing.
      if (testIntersection==true)                        // if alice intersects boundary
	{ 
	  if ( (depth > 0) && (j>=5) )           // if current clothoid has at least 5 poses, and we haven't 
	    {                                    //    gone beyond the specified depth, then expand the paths
	      j -=2;                                     // go back two path points from the end
	      cPtr->m_clothoidPoints.resize(j);          // clip off all of the poses after the jth pose

	      // make a new Path Node at the clipped end of the clothoid, and link it to the associated edge
	      pose2 endPose = cPtr->m_clothoidPoints.back();
	      edgePtr->m_childPTNodePtr = new PTNode(endPose);  
	      this->printDenseAliceBox(endPose, m_safetyMargins, f); 

	      // now expand from this new node.  First, create circles passing through this node
	      double cosAngle = cos(endPose.ang);    double sinAngle = sin(endPose.ang);
	      point2 rightCenter= point2(endPose.x+TURNING_RADIUS * sinAngle,endPose.y-TURNING_RADIUS * cosAngle);
	      point2 leftCenter = point2(endPose.x-TURNING_RADIUS * sinAngle,endPose.y+TURNING_RADIUS * cosAngle);
	      double radius = this->m_rightGoalCircle->m_Radius;
	      CircleData * RCirclePtr = new CircleData(edgePtr->m_childPTNodePtr,rightCenter, radius, RIGHT_HAND_CIRCLE);
	      CircleData * LCirclePtr = new CircleData(edgePtr->m_childPTNodePtr,leftCenter, radius, LEFT_HAND_CIRCLE);

	      // Next, create the paths from these circles to the goal circles
	      StartGoalCirclePair(RCirclePtr, this->m_rightGoalCircle, RIGHT_RIGHT_COMBO, radius);
	      StartGoalCirclePair(RCirclePtr, this->m_leftGoalCircle, RIGHT_LEFT_COMBO, radius);
	      StartGoalCirclePair(LCirclePtr, this->m_rightGoalCircle, LEFT_RIGHT_COMBO, radius);
	      StartGoalCirclePair(LCirclePtr, this->m_leftGoalCircle, LEFT_LEFT_COMBO, radius);

	      // now process these newly developed paths, because the main Cull routine will otherwise not process them.
	      CullAndExpandNodeOutEdges(edgePtr->m_childPTNodePtr, f, (depth -1));
	      return true;
	    }
	  else            // we can't expand this path due to depth or size restrictions, so it is a dead path
	    { 
	      return  false;
	    };  
	};
    };
  return true;
};
#endif

/* ------------------------------------------------------------------------------------------------------------ */

void CirclePlanner::CullAndExpandNodeOutEdges(PTNode * nodePtr, int depth)
{
  if(nodePtr->m_outEdges.empty())              // if node has no outEdges, there's nothing to do
    {
      nodePtr->m_hasNodeBeenCulled = true;
      return;
    }
  else
    { // first check if this node has already been visited and culled.  If it has not been visited, 
      // then proceed  with the culling operation.  If it has been visited, then just return
      if ( !nodePtr->m_hasNodeBeenCulled )
	{ 	 
	  // Now eliminate infeasible edges. We can't immediately erase them from the outEdges list because 
	  // we're iterating on that list.  So, save up the feasible edges, and reconstruct the list after
	  vector<PTEdge *> feasibleEdgeList;                    
	  feasibleEdgeList.clear();

	  for (vector<PTEdge *>::iterator i=nodePtr->m_outEdges.begin(); i!=nodePtr->m_outEdges.end(); i++)
	    {
	      PTNode * childPtr = (*i)->m_childPTNodePtr;       // distal node of the present edge i

	      // before trying to cull an edge, first cull the outEdges of this edge's distal node
	      if ( !childPtr->m_outEdges.empty() )
		{   CullAndExpandNodeOutEdges(childPtr, (depth - 1) );  };

	      if( ExpansiveIsPathLegal( (*i), depth ) == true)      
		{ feasibleEdgeList.push_back((*i)); };
	    };
	  nodePtr->m_hasNodeBeenCulled = true;

	  nodePtr->m_outEdges.clear();	          // now reattach all of the feasible outEdges to the node
	  for (vector<PTEdge *>::iterator j=feasibleEdgeList.begin(); j!=feasibleEdgeList.end(); j++)
	    { nodePtr->m_outEdges.push_back((*j)); };
	}
      else             // we reach here if the node's edges have already been culled.  Then just return
	{return; };
    };
};


#if CIRCLE_PLANNER_DEBUG
// the alternate version of CullNodeOutEdges for debugging
void CirclePlanner::CullAndExpandNodeOutEdges(PTNode * nodePtr, ofstream *f, int depth)
{
  if(nodePtr->m_outEdges.empty())              // if node has no outEdges, there's nothing to do
    {
      nodePtr->m_hasNodeBeenCulled = true;
      return;
    }
  else
    { // first check if this node has already been visited and culled.  If it has not been visited,
      // then proceed  with the culling operation.  If it has been visited, then just return
      if ( !nodePtr->m_hasNodeBeenCulled )
	{ 	 
	  // Now eliminate infeasible edges.  We can't immediately erase them from the outEdges list because 
	  // we're iterating on that list.  So, save up feasible edges, and reconstruct the list after 
	  vector<PTEdge *> feasibleEdgeList;                    
	  feasibleEdgeList.clear();

	  for (vector<PTEdge *>::iterator i=nodePtr->m_outEdges.begin(); i!=nodePtr->m_outEdges.end(); i++)
	    {
	      PTNode * childPtr = (*i)->m_childPTNodePtr;       // distal node of the present edge i

	      // before trying to cull an edge, first cull the outEdges of this edge's distal node
	      if ( !childPtr->m_outEdges.empty() )
		{   CullAndExpandNodeOutEdges(childPtr, f, depth);  };

	      if( ExpansiveIsPathLegal( (*i), f, depth ) == true)      
		{
		  feasibleEdgeList.push_back((*i)); 
		};
	    };
	  nodePtr->m_hasNodeBeenCulled = true;

	  nodePtr->m_outEdges.clear();	          // now reattach all of the feasible outEdges to the node
	  for (vector<PTEdge *>::iterator j=feasibleEdgeList.begin(); j!=feasibleEdgeList.end(); j++)
	    { nodePtr->m_outEdges.push_back((*j)); };
	}
      else
	{return; };
    };
};
#endif

