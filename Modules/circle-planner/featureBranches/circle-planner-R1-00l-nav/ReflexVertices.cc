/* ReflexVertices.cc: functions that get used by CirclePlanner to deal with reflex vertices
 * on zones and obstacles.  They may have an impact on the Path Tree graph.
 *
 * Written by Joel Burdick (starting Oct. 12, 2007)
 */

/* ----------------------------------------------------------------------------------- */
/*                                     includes                                        */
/* ----------------------------------------------------------------------------------- */

#include "CirclePlanner.hh"

/* --------------------------------------------------------------------------------------------------- */
/*               functions that aid in finding reflex vertices on boundaries and obstacles             */
/* --------------------------------------------------------------------------------------------------- */


// ------------------------------------------------------------------------------------
// check if a vertex is a "reflex" vertex ( the polygon is locally convex). This
// calculation assumes that the vertex indices are increasing in a clockwise fashion.

bool CirclePlanner::IsReflexVertex(point2 prev, point2 current, point2 next, unsigned int orientation)
{
  point2 v1 = next - current;
  point2 v2 = current - prev;

  float vertexAngle = FindTheta(v1,v2);
  if (vertexAngle >= 0.0)
    {
      if (orientation == CLOCKWISE)
	{ return true;}
      else
	{ return false;};
    }
  else  // angle is negative
    {
      if (orientation == COUNTER_CLOCKWISE)
	{ return true;}
      else
	{return false;};
    };
};


// find the reflex vertices on the zone boundary, if any.  We just loop through all of the zone Boundary vertices,
// and then loop through all of the obstacle polygon vertices looking for reflex vertices.  If any are found, the 
// are pushed onto the relevant container vectors.
void CirclePlanner::findReflexVertices()
{
  // first find the reflex vertices on the zone boundary.
  m_zoneReflexVertices.clear();
  unsigned int zoneOrientation = PolyOrientation(&(this->m_rightHandedZoneBoundary));

  vector<point2>::iterator previous = (m_rightHandedZoneBoundary.arr.end() -1);
  vector<point2>::iterator current = m_rightHandedZoneBoundary.arr.begin();
  vector<point2>::iterator next;
  
  do 
    {
      if ( current < (this->m_rightHandedZoneBoundary.arr.end() -1) )
	{ next = (current +1); }
      else
	{ next = this->m_rightHandedZoneBoundary.arr.begin();};
      
      // if the vertex is a reflex vertex, then store it and its relevant information in a reflexVertexData container
      if ( IsReflexVertex(*previous,*current,*next,zoneOrientation) )
	{ 
	  ReflexVertexData * rvertex = new ReflexVertexData(*previous, *current, *next, zoneOrientation, m_aliceHalfWidth);
	  m_zoneReflexVertices.push_back(rvertex);
	};
      previous=current;
      current++;

    }
  while (current != m_rightHandedZoneBoundary.arr.end() );

  // ------------------------------------------------------------------------
  // now process each of the obstacles to find reflex vertices

  if( !m_obstacles.empty() )            // first check if there any obstalces to worry about
    {
      m_obstacleReflexVertices.clear();

      for(vector<point2arr>::iterator obst = m_obstacles.begin(); obst!= m_obstacles.end(); obst++)
	{
	  unsigned int obstacleOrientation = PolyOrientation(&(*obst) );    //find orientation of the obstacle
	  previous = (obst->arr.end() - 1);  
	  current =  obst->arr.begin();
	  
	  vector<ReflexVertexData *> tempContainer;
	  tempContainer.clear();

	  do 
	    {
	      if ( current < (obst->arr.end() -1) )
		{ next = (current +1); }
	      else
		{ next = obst->arr.begin();};
	      
	      if ( IsReflexVertex(*previous,*current,*next,obstacleOrientation) )
		{ 
		  ReflexVertexData * rvertex = new ReflexVertexData(*previous, *current, *next, 
								    obstacleOrientation,m_aliceHalfWidth);
		  tempContainer.push_back( rvertex );
		};
	      previous=current;
	      current++;
	    }
	  while (current != obst->arr.end() );
	  m_obstacleReflexVertices.push_back(tempContainer);
	};
    };
};      // end of findReflexVertices

/* --------------------------------------------------------------------------------------------------- */
/*          functions that using reflex vertices on boundaries and obstacles to generate paths         */
/* --------------------------------------------------------------------------------------------------- */

