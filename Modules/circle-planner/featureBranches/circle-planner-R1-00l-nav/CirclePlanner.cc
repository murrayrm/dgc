/* CirclePlanner.cc: functions that implement a nonholonomic motion planner for a car.
 * based on the concept of "circles" at the goal and start--hence the name circle-planner.
 * This file contains the main start-goal parts of circle planner, while CircleSplicer.cc
 * contains related code to "splice together" other circles.
 *
 * Written by Joel Burdick: Sept. 08, 2007
 *
 */

/* ------------------------------------------------------------------------------------- */
/*                                 Defines and Includes                                  */
/* ------------------------------------------------------------------------------------- */

#include "CirclePlanner.hh"
#include "AStar.hh"

/* ------------------------------------------------------------------------------------- */
/*                         Constuctors/Destructors/Initializers                          */
/* ------------------------------------------------------------------------------------- */

// constructor that sets the start and goal states for the planner and also stores 
// info on start/goal circles in CircleData containers
CirclePlanner::CirclePlanner(pose2 startPose, pose2 goalPose, double RADIUS)
{
  m_startPose = startPose;
  m_startNodePtr = new PTNode(startPose);
  m_goalPose = goalPose;
  m_goalNodePtr  = new PTNode(goalPose);
  m_nClothoids = 0;

  // calculate and store the centers of the starting circles
  point2 startOffset = point2( -(RADIUS * sin(m_startPose.ang)),(RADIUS * cos(m_startPose.ang)) );
  point2 leftStartCircleCenter = point2((m_startPose.x + startOffset.x),(m_startPose.y + startOffset.y));
  point2 rightStartCircleCenter = point2((m_startPose.x - startOffset.x),(m_startPose.y - startOffset.y));

  m_rightStartCircle = new CircleData(m_startNodePtr, rightStartCircleCenter, RADIUS, RIGHT_HAND_CIRCLE);
  m_leftStartCircle = new CircleData(m_startNodePtr, leftStartCircleCenter, RADIUS, LEFT_HAND_CIRCLE);

  // calculate the centers of the goal circles
  point2 goalOffset = point2( -(RADIUS * sin(m_goalPose.ang)), (RADIUS * cos(m_goalPose.ang)) );
  point2 leftGoalCircleCenter = point2((m_goalPose.x + goalOffset.x),(m_goalPose.y + goalOffset.y));
  point2 rightGoalCircleCenter = point2((m_goalPose.x - goalOffset.x),(m_goalPose.y - goalOffset.y));

  // store info about goal circles
  m_rightGoalCircle = new CircleData(m_goalNodePtr, rightGoalCircleCenter, RADIUS, RIGHT_HAND_CIRCLE);
  m_leftGoalCircle = new CircleData(m_goalNodePtr, leftGoalCircleCenter, RADIUS, LEFT_HAND_CIRCLE);
};


// simple destructor.  I'm sure we should do better than this.
CirclePlanner::~CirclePlanner()
{
  delete m_startNodePtr;
  delete &m_startPose;
  delete & m_goalPose;
  delete m_goalNodePtr;
  m_solutionPath.~vector();
  m_forwardPath.~vector();
};

void CirclePlanner::Init()
{
  m_lastSolution.clear();
}

/* ------------------------------------------------------------------------------------- */
/*                                Simple helper Functions                                */
/* ------------------------------------------------------------------------------------- */

// 2-dimensional cross-product
float CirclePlanner::Cross2D(point2 V1,point2 V2)
{
  return (V1.x * V2.y - V1.y * V2.x);
};

// 2-dimensional dot-product
float CirclePlanner::Dot2D(point2 V1, point2 V2)
{
  return (V1.x * V2.x + V1.y * V2.y);
};

double CirclePlanner::unwrap_angle(double inAngle)
{
  double outAngle=inAngle;
  while (outAngle<-M_PI)
    {outAngle += 2.0 * M_PI;};
  while (outAngle > M_PI)
    {outAngle -= 2.0 * M_PI;};
  return outAngle;
};


// find the angle, of vector V2 relative to vector  V1 (need not assum that V1 and V2 are unit vectors)
float CirclePlanner::FindTheta(point2 V1, point2 V2)
{
  float normV1 = sqrt(V1.x * V1.x + V1.y * V1.y);
  float normV2 = sqrt(V2.x * V2.x + V2.y * V2.y);
  float CAngle = Dot2D(V1,V2)/(normV1 * normV2);
  float SAngle = Cross2D(V1,V2)/(normV1 * normV2);
  double returnAngle = atan2(SAngle,CAngle);
  return (unwrap_angle(returnAngle));
};

  
/* ------------------------------------------------------------------------------------- */
/*                           Basic Path Construction Functions                           */
/* ------------------------------------------------------------------------------------- */

// make a straight line path corresponding to a bitangent between two circles

PTEdge * CirclePlanner::makeStraightLine(point2 startPoint, point2 endPoint, bool reverse)
{
  unsigned int i;
  float dx = endPoint.x - startPoint.x;
  float dy = endPoint.y - startPoint.y;
  float length = sqrt(dx * dx + dy * dy);
  float angle = atan2(dy,dx);
  

  float fraction;
  // determine the number of points that will be used in this segment
  unsigned int npoints = (unsigned int) (1+ floor(length/CP_PATH_SECTION_LENGTH));

  // allocate a new clothoid segment at the starting point of the straight line, and then link in the 
  // subsequent points.  Note that straight lines are Clothoids with zero initial and final curvature
  m_nClothoids++;
  Clothoid * newClothoidPtr = new Clothoid(0.0,0.0,length, reverse, m_nClothoids);  

  for(i=1; i<npoints; i++)
    {
      fraction = (float) i/npoints;
      pose2 * cpt= new pose2((startPoint.x + fraction * dx),(startPoint.y + fraction * dy),angle);
      if (reverse)
	{
	  cpt->ang += M_PI;
	  cpt->unwrap_angle();
	}
      newClothoidPtr->m_clothoidPoints.push_back((*cpt));
    };
  PTEdge * newPTEdgePtr = new PTEdge(newClothoidPtr);

  return newPTEdgePtr;
};

//------------------------------------------------------------------------------------------
// make a circular arc path in which the vehicle goes backward
PTEdge * CirclePlanner::makeForwardCircularArc(point2 startCenter, float startAngle, float stopAngle, 
						 unsigned int handedness, double RADIUS)
{
  float deltaAngle, arclength, stepAngle;
  unsigned int i, npoints;
  Clothoid * newClothoidPtr;

  if (handedness == RIGHT_HAND_CIRCLE)             // clockwise rotation (decreasing angle) is forward motion
    {
      if (startAngle>0)
	{
	  if (stopAngle > 0)
	    {
	      if (stopAngle > startAngle)    
		{
		  deltaAngle = stopAngle - startAngle - 2.0 * M_PI;
		}
	      else   
		{
		  deltaAngle = (stopAngle - startAngle);
		};
	    }
	  else       // (startAngle > 0) && (stopAngle < 0)
	    {
	      deltaAngle = stopAngle-startAngle;
	    };
	}         
      else           // if we reach here, (startAngle <= 0)
	{ 
	  if (stopAngle>0)        // (Pi > stopAngle > 0) && (-Pi < startAngle < 0) 
	    {
	      deltaAngle = stopAngle - startAngle - 2.0 * M_PI;
	    }
	  else       // (-PI < stopAngle <= 0) && (-Pi < startAngle <= 0)
	    {
	      if (stopAngle > startAngle) 
		{
		  deltaAngle = stopAngle - startAngle - 2.0 * M_PI;
		}
	      else
		{ 
		  deltaAngle = (stopAngle - startAngle);
		};
	    };     
	};
    }
  else    //handedness == LEFT_HAND, so that counterclockwise rotation is forward motion.(with increasing angle)
    {
      if (startAngle > 0)
	{
	  if (stopAngle > 0)
	    {
	      if (stopAngle > startAngle)
		{
		  deltaAngle = stopAngle-startAngle;
		}
	      else
		{	      
		  deltaAngle = stopAngle-startAngle + 2.0 * M_PI;
		};
	    }
	  else  // (startAngle > 0) && (stopAngle < 0)
	    {
	      deltaAngle = stopAngle-startAngle + 2.0 * M_PI;
	    };
	}
      else                       // (startAngle < 0)
	{
	  if (stopAngle > 0)     // (startAngle < 0) && (stopAngle > 0)
	    {
	      deltaAngle = stopAngle-startAngle;
	    }
	  else                   // (startAngle < 0) && (stopAngle < 0)
	    {
	      if (stopAngle >  startAngle)
		{
		  deltaAngle = stopAngle-startAngle;
		}
	      else
		{
		  deltaAngle = stopAngle-startAngle + 2.0 * M_PI;
		};
	    };
	};
    };

  // compute the distance the vehicle will go around this portion of the circle.
  arclength = fabs( RADIUS * deltaAngle);  

  // Allocate a Clothoid.  NOTE, in the following call, the turning radius should be given the appropriate sign.
  // Right now this information is not used, but it might be in the future.
  m_nClothoids++;
  newClothoidPtr = new Clothoid(RADIUS, RADIUS, arclength, false, m_nClothoids);

  npoints = 1 + (unsigned int) floor(arclength/CP_PATH_SECTION_LENGTH);
  if (npoints == 1) {npoints = 2;}                                       // check for this exceptional condition that 
                                                                         //    arclength < PATH_SECTION_LENGTH
  stepAngle = deltaAngle/(npoints -1);

  for(i=0; i<npoints; i++)
    {
      float curAngle = startAngle + (float) i * stepAngle;
      pose2 newPose = pose2( (startCenter.x + cos(curAngle) * RADIUS), 
			     (startCenter.y + sin(curAngle) * RADIUS), curAngle);

      if (handedness == RIGHT_HAND_CIRCLE)
	{newPose.ang -= M_PI/2.0; }
      else
	{newPose.ang += M_PI/2.0; }
      newPose.unwrap_angle();

      newClothoidPtr->m_clothoidPoints.push_back(newPose);
    };
 
  PTEdge * newPTEdgePtr = new PTEdge(newClothoidPtr);

  return newPTEdgePtr;
};


//------------------------------------------------------------------------------------------
// make a circular arc path in which the vehicle goes backward
PTEdge * CirclePlanner::makeReverseCircularArc(point2 startCenter, float startAngle, float stopAngle, 
						 unsigned int handedness, double RADIUS)
{
  float deltaAngle, arclength, stepAngle;
  unsigned int i, npoints;
  Clothoid * newClothoidPtr;

  if (handedness == RIGHT_HAND_CIRCLE)             // counterclockwise rotation (increasing angle) is reverse motion
    {
      if (startAngle>0)
	{
	  if (stopAngle > 0)
	    {
	      if (stopAngle > startAngle)    
		{
		  deltaAngle = stopAngle - startAngle;
		}
	      else   
		{
		  deltaAngle = stopAngle - startAngle + 2.0 * M_PI;
		};
	    }
	  else       // (startAngle > 0) && (stopAngle < 0)
	    {
	      deltaAngle = stopAngle-startAngle + 2.0 * M_PI;
	    };
	}         
      else           // if we reach here, (startAngle <= 0)
	{ 
	  if (stopAngle>0)        // (Pi > stopAngle > 0) && (-Pi < startAngle < 0) 
	    {
	      deltaAngle = stopAngle - startAngle;
	    }
	  else       // (-PI < stopAngle <= 0) && (-Pi < startAngle <= 0)
	    {
	      if (stopAngle > startAngle) 
		{
		  deltaAngle = stopAngle - startAngle;
		}
	      else
		{ 
		  deltaAngle = stopAngle - startAngle + 2.0 * M_PI;
;
		};
	    };     
	};
    }
  else    //handedness == LEFT_HAND, so that clockwise rotation is reverse motion.(with decreasing angle)
    {
      if (startAngle > 0)
	{
	  if (stopAngle > 0)
	    {
	      if (stopAngle > startAngle)
		{
		  deltaAngle = stopAngle-startAngle - 2.0 * M_PI;
		}
	      else
		{	      
		  deltaAngle = stopAngle-startAngle;
		};
	    }
	  else  // (startAngle > 0) && (stopAngle < 0)
	    {
	      deltaAngle = stopAngle-startAngle;
	    };
	}
      else                       // (startAngle < 0)
	{
	  if (stopAngle > 0)     // (startAngle < 0) && (stopAngle > 0)
	    {
	      deltaAngle = stopAngle-startAngle - 2.0 * M_PI;
	    }
	  else                   // (startAngle < 0) && (stopAngle < 0)
	    {
	      if (stopAngle >  startAngle)
		{
		  deltaAngle = stopAngle-startAngle - 2.0 * M_PI;
		}
	      else
		{
		  deltaAngle = stopAngle-startAngle;
		};
	    };
	};
    };
  // compute the distance the vehicle will go around start circle.
  arclength = fabs(RADIUS * deltaAngle);

  // Allocate a Clothoid.  NOTE, in the following call, the turning radius should be given the appropriate sign.
  // Right now this information is not practically used, but it might be in the future.
  m_nClothoids++;
  newClothoidPtr = new Clothoid(RADIUS,RADIUS, arclength, true, m_nClothoids);

  npoints = 1+ (unsigned int) floor(arclength/CP_PATH_SECTION_LENGTH);
  if (npoints == 1) {npoints = 2;}                                       // check for this exceptional condition that 
                                                                         //    arclength < PATH_SECTION_LENGTH
  stepAngle = deltaAngle/(npoints -1);

  for(i=0; i<npoints; i++)
    {
      float curAngle = startAngle + (float) i * stepAngle;
      pose2 newPose = pose2( (startCenter.x + cos(curAngle) * RADIUS), 
			     (startCenter.y + sin(curAngle) * RADIUS), curAngle);

      if (handedness == RIGHT_HAND_CIRCLE)
	{newPose.ang -= M_PI/2.0; }
      else
	{newPose.ang += M_PI/2.0; }
      newPose.unwrap_angle();

      newClothoidPtr->m_clothoidPoints.push_back(newPose);
    };

  PTEdge * newPTEdgePtr = new PTEdge(newClothoidPtr);

  return newPTEdgePtr;
};

/* ------------------------------------------------------------------------------------- */
/*                                   Main Work Functions                                 */
/* ------------------------------------------------------------------------------------- */

// Compute the relevant bitangents for a pair of circles that either are the Start/Goal circles, or will behave
// like start and goal circles.  The the bitangent paths are linked to the Path Tree Start Node to form part 
// of the Path Tree graph that will get searched for solutions

void CirclePlanner::StartGoalCirclePair(CircleData * startCircle, CircleData * goalCircle, unsigned int handedness, double RADIUS)
{
  point2 startCenter = startCircle->m_center;
  point2 goalCenter = goalCircle->m_center;
  PTNode * startNodePtr = startCircle->m_circleNodePtr;
  PTNode * goalNodePtr = goalCircle->m_circleNodePtr;
  pose2 startPose = startNodePtr->m_nodePose;
  pose2 goalPose = goalNodePtr->m_nodePose;

  float x12 = goalCenter.x - startCenter.x;          // distances between centers of start and goal circle
  float y12 = goalCenter.y - startCenter.y;
  float L12 = sqrt(x12 * x12 + y12 * y12);
  float phi12 = atan2(y12,x12);                      // angle made by vector from startCenter to GoalCenter

  const point2 xaxis = point2(1.0,0.0);              // the unit vector colinear with the x-axis
  point2 v1(- RADIUS * sin(phi12), RADIUS * cos(phi12));             

  //  find the angles corresponding to the  robot's start and goal poses,
  point2 startLine = point2(startPose.x, startPose.y) - startCenter;
  point2 goalLine = point2(goalPose.x,goalPose.y) - goalCenter;
  float startAngle = FindTheta(xaxis,startLine);     // angle on start cicle made by Alice's start pose
  float goalAngle = FindTheta(xaxis,goalLine);       // angle made on goal circle by Alice's goal pose

  // First check to see if the circles are overlapping.  If so, there make the bitangents that are 
  // appropriate for this case

  // variables needed to construct and store paths 
  PTEdge * startForwardCircle1Ptr;      PTEdge * startReverseCircle1Ptr;
  PTEdge * goalForwardCircle1Ptr;       PTEdge * goalReverseCircle1Ptr;

  PTEdge * startForwardCircle2Ptr;      PTEdge * startReverseCircle2Ptr;
  PTEdge * goalForwardCircle2Ptr;       PTEdge * goalReverseCircle2Ptr;

  // if both the start and goal circles are both right or left handed, only outer bitangents are relevant
  if ( (handedness == RIGHT_RIGHT_COMBO) || (handedness == LEFT_LEFT_COMBO) )
    {
      // first compute the points of contact of the two outer bitangents
      point2 z1a = startCenter + v1;         // location of circle 1's first outer bitangent contact point
      point2 z1b = startCenter - v1;         // location of circle 1's second outer bitangent contact point
      point2 z2a = goalCenter  + v1;         // location of circle 2's first outer bitangent contact point
      point2 z2b = goalCenter  - v1;         // location of circle 2's second outer bitangent contact point

      //-------------------------- paths and nodes related to the outer bitangents ------------------------

      // set up the paths on the starting and goal circles.  as well as the angles corresponding to the 
      // bitangent's contact points
 
      float thetaz1a = FindTheta(xaxis,v1);              // angle that bitangent at z1a makes
      float thetaz2a = thetaz1a;                         // angle that bitangent at z2a makes
      float thetaz1b = FindTheta(xaxis,(-1.0 * v1));
      float thetaz2b = thetaz1b;

      // make the simple straight line segment associated with the first outer bitangent.  We'll link to
      // it in a moment. Also construct the two circular arcs that connect the staring pose to the 
      // bitangent point, and the distal bitangent to the goal pose.

      PTNode * biTangent1StartNode;      PTNode * biTangent1EndNode;      PTEdge * biTangent1Ptr;          
      PTNode * biTangent2StartNode;      PTNode * biTangent2EndNode;      PTEdge * biTangent2Ptr;

      if (handedness == RIGHT_RIGHT_COMBO)
	{
	  // make segments and nodes associated with first bitangent
	  startForwardCircle1Ptr = makeForwardCircularArc(startCenter, startAngle, thetaz1a,  RIGHT_HAND_CIRCLE, RADIUS);
	  startReverseCircle1Ptr = makeReverseCircularArc(startCenter, startAngle, thetaz1a,  RIGHT_HAND_CIRCLE, RADIUS); 
	  biTangent1StartNode = new PTNode(pose2(z1a.x,z1a.y,thetaz1a - 0.5 * M_PI));
	  biTangent1Ptr = makeStraightLine(z1a,z2a,false);
	  biTangent1EndNode = new PTNode(pose2(z2a.x,z2a.y,thetaz2a - 0.5 * M_PI));
	  goalForwardCircle1Ptr = makeForwardCircularArc(goalCenter, thetaz2a, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle1Ptr = makeReverseCircularArc(goalCenter, thetaz2a, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);

	  // make segments and nodes associated with second bitangent
	  startForwardCircle2Ptr = makeForwardCircularArc(startCenter, startAngle, thetaz1b, RIGHT_HAND_CIRCLE, RADIUS);
	  startReverseCircle2Ptr = makeReverseCircularArc(startCenter, startAngle, thetaz1b, RIGHT_HAND_CIRCLE, RADIUS); 
	  biTangent2StartNode = new PTNode(pose2(z1b.x,z1b.y,thetaz1b - 0.5 * M_PI));
	  biTangent2Ptr = makeStraightLine(z1b,z2b,true);
	  biTangent2EndNode = new PTNode(pose2(z2b.x,z2b.y,thetaz2b - 0.5 * M_PI));
	  goalForwardCircle2Ptr = makeForwardCircularArc(goalCenter, thetaz2b, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle2Ptr = makeReverseCircularArc(goalCenter, thetaz2b, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);
	}
      else   // else we have a LEFT_LEFT_COMBO
	{
	  // make segments and nodes associated with first bitangent
	  startForwardCircle1Ptr = makeForwardCircularArc(startCenter, startAngle, thetaz1a, LEFT_HAND_CIRCLE, RADIUS);
	  startReverseCircle1Ptr = makeReverseCircularArc(startCenter, startAngle, thetaz1a,  LEFT_HAND_CIRCLE, RADIUS);
	  biTangent1StartNode = new PTNode(pose2(z1a.x,z1a.y,thetaz1a + 0.5 * M_PI));
	  biTangent1Ptr = makeStraightLine(z1a,z2a,true); 
	  biTangent1EndNode = new PTNode(pose2(z2a.x,z2a.y,thetaz2a + 0.5 * M_PI));
	  goalForwardCircle1Ptr = makeForwardCircularArc(goalCenter, thetaz2a, goalAngle, LEFT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle1Ptr = makeReverseCircularArc(goalCenter, thetaz2a, goalAngle, LEFT_HAND_CIRCLE, RADIUS);

	  // make segments and nodes associated with second bitangent
	  startForwardCircle2Ptr = makeForwardCircularArc(startCenter, startAngle, thetaz1b, LEFT_HAND_CIRCLE, RADIUS);
	  startReverseCircle2Ptr = makeReverseCircularArc(startCenter, startAngle, thetaz1b, LEFT_HAND_CIRCLE, RADIUS);
	  biTangent2StartNode = new PTNode(pose2(z1b.x,z1b.y,thetaz1b + 0.5 * M_PI));
	  biTangent2Ptr = makeStraightLine(z1b,z2b,false); 
	  biTangent2EndNode = new PTNode(pose2(z2b.x,z2b.y,thetaz2b + 0.5 * M_PI));
	  goalForwardCircle2Ptr = makeForwardCircularArc(goalCenter, thetaz2b, goalAngle, LEFT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle2Ptr = makeReverseCircularArc(goalCenter, thetaz2b, goalAngle, LEFT_HAND_CIRCLE, RADIUS);
	};

      // now splice all of the pieces together, starting from the goal and working backwards to the start.

      // components associated with 1st bitangent
      goalForwardCircle1Ptr->m_childPTNodePtr = goalNodePtr;           // link "goal" to this edge
      biTangent1EndNode->m_outEdges.push_back(goalForwardCircle1Ptr);  // link one of goal circular edges

      goalReverseCircle1Ptr->m_childPTNodePtr = goalNodePtr;           // link goal to this edge
      biTangent1EndNode->m_outEdges.push_back(goalReverseCircle1Ptr);  // link the other goal circular edge

      biTangent1Ptr->m_childPTNodePtr = biTangent1EndNode;             // link bitangent to distal node 
      biTangent1StartNode->m_outEdges.push_back(biTangent1Ptr);        // link the start of the bitangent

      startForwardCircle1Ptr->m_childPTNodePtr = biTangent1StartNode;  // link start circ. arc to bitangent
      startNodePtr->m_outEdges.push_back(startForwardCircle1Ptr);      // link the start Node to circle
      startReverseCircle1Ptr->m_childPTNodePtr = biTangent1StartNode;  // link start circ. arc to bitangent
      startNodePtr->m_outEdges.push_back(startReverseCircle1Ptr);

      // components associated with 2nd bitangent
      goalForwardCircle2Ptr->m_childPTNodePtr = goalNodePtr;          // link goal to this edge
      biTangent2EndNode->m_outEdges.push_back(goalForwardCircle2Ptr); // link one of goal circular edges

      goalReverseCircle2Ptr->m_childPTNodePtr = goalNodePtr;          // link goal to this edge
      biTangent2EndNode->m_outEdges.push_back(goalReverseCircle2Ptr); // link the other goal circular edge

      biTangent2Ptr->m_childPTNodePtr = biTangent2EndNode;            // link bitangent to its distal node
      biTangent2StartNode->m_outEdges.push_back(biTangent2Ptr);       // link the start of the bitangent

      startForwardCircle2Ptr->m_childPTNodePtr = biTangent2StartNode; // link start circ. arc to bitangent
      startNodePtr->m_outEdges.push_back(startForwardCircle2Ptr);     // link startNode to circ. arc
      startReverseCircle2Ptr->m_childPTNodePtr = biTangent2StartNode; // link start circ. arc to bitangent
      startNodePtr->m_outEdges.push_back(startReverseCircle2Ptr);
    }
  else  // must be RIGHT_LEFT_COMBO or LEFT_RIGHT_COMBO, in which case, only inner bitangents are active
    {   // first we check if the circles are overlapping. If so, then the inner bitangents do not exist, 
        // and we  must return before constructing this bitangents.
      if (L12 < (2.0 * RADIUS))
	{ return; };
      
      float coseta = 2 * RADIUS / L12;  
      float eta = acos(coseta);
      float psiPlus = phi12 + eta;  float psiMinus = phi12 - eta;
      float cPsiMinus = cos(psiMinus), sPsiMinus = sin(psiMinus);
      float cPsiPlus = cos(psiPlus), sPsiPlus = sin(psiPlus);

      // vectors n1 and n2 define the locations of the bi-tangent contact points
      point2 n1 = RADIUS * point2(cPsiPlus, sPsiPlus);       // related to first inner bitangent
      point2 n2 = RADIUS * point2(cPsiMinus, sPsiMinus);     // related to second inner bitangent

      // now create the points where the inner bi-tangents contact the first circle
      point2 y1a = startCenter + n1;  point2 y2a = goalCenter - n2;    // end points of 1st inner bitangent
      point2 y1b = startCenter + n2;  point2 y2b = goalCenter - n1;    // end points of 2nd inner bitangent
      float thetay1a = FindTheta(xaxis,n1);  
      float thetay1b = FindTheta(xaxis,n2);
      float thetay2a = FindTheta(xaxis,(-1.0 * n2));
      float thetay2b = FindTheta(xaxis,(-1.0 * n1));

      PTEdge * crossTangent1Ptr;            PTEdge * crossTangent2Ptr;
      PTNode * crossTangent1StartNode;      PTNode * crossTangent1EndNode;      
      PTNode * crossTangent2StartNode;      PTNode * crossTangent2EndNode;      

      if (handedness == RIGHT_LEFT_COMBO)
	{
	  // make segments and nodes associated with first inner bitangent
	  startForwardCircle1Ptr = makeForwardCircularArc(startCenter, startAngle, thetay1a, RIGHT_HAND_CIRCLE, RADIUS);
	  startReverseCircle1Ptr = makeReverseCircularArc(startCenter, startAngle, thetay1a, RIGHT_HAND_CIRCLE, RADIUS); 
	  crossTangent1StartNode = new PTNode(pose2(y1a.x,y1a.y,thetay1a - 0.5 * M_PI));
	  crossTangent1EndNode = new PTNode(pose2(y2b.x,y2b.y,thetay2b + 0.5 * M_PI));
	  crossTangent1Ptr = makeStraightLine(y1a,y2b,false);
	  goalForwardCircle1Ptr = makeForwardCircularArc(goalCenter, thetay2b, goalAngle, LEFT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle1Ptr = makeReverseCircularArc(goalCenter, thetay2b, goalAngle, LEFT_HAND_CIRCLE, RADIUS);

	  // make segments and nodes associated with second inner bitangent
	  startForwardCircle2Ptr = makeForwardCircularArc(startCenter, startAngle, thetay1b, RIGHT_HAND_CIRCLE, RADIUS);
	  startReverseCircle2Ptr = makeReverseCircularArc(startCenter, startAngle, thetay1b, RIGHT_HAND_CIRCLE, RADIUS); 
	  crossTangent2StartNode = new PTNode(pose2(y1b.x,y1b.y,thetay1b - 0.5 * M_PI));
	  crossTangent2Ptr = makeStraightLine(y1b,y2a,true);
	  crossTangent2EndNode = new PTNode(pose2(y2a.x,y2a.y,thetay2a +0.5 * M_PI));
	  goalForwardCircle2Ptr = makeForwardCircularArc(goalCenter, thetay2a, goalAngle, LEFT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle2Ptr = makeReverseCircularArc(goalCenter, thetay2a, goalAngle, LEFT_HAND_CIRCLE, RADIUS);
	}
      else     // else must be LEFT_RIGHT_COMBO 
	{
	  // make segments and nodes associated with first bitangent
	  startForwardCircle1Ptr = makeForwardCircularArc(startCenter, startAngle, thetay1a, LEFT_HAND_CIRCLE, RADIUS);
	  startReverseCircle1Ptr = makeReverseCircularArc(startCenter, startAngle, thetay1a, LEFT_HAND_CIRCLE, RADIUS);
	  crossTangent1StartNode = new PTNode(pose2(y1a.x,y1a.y,thetay1a + 0.5 * M_PI));
	  crossTangent1Ptr = makeStraightLine(y1a,y2b,true); 
	  crossTangent1EndNode = new PTNode(pose2(y2b.x,y2b.y,thetay2b - 0.5 * M_PI));
	  goalForwardCircle1Ptr = makeForwardCircularArc(goalCenter, thetay2b, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle1Ptr = makeReverseCircularArc(goalCenter, thetay2b, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);

	  // make segments and nodes associated with second bitangent
	  startForwardCircle2Ptr = makeForwardCircularArc(startCenter, startAngle, thetay1b, LEFT_HAND_CIRCLE, RADIUS);
	  startReverseCircle2Ptr = makeReverseCircularArc(startCenter, startAngle, thetay1b, LEFT_HAND_CIRCLE, RADIUS);
	  crossTangent2StartNode = new PTNode(pose2(y1b.x,y1b.y,thetay1b + 0.5 * M_PI));
	  crossTangent2Ptr = makeStraightLine(y1b,y2a,false); 
	  crossTangent2EndNode = new PTNode(pose2(y2a.x,y2a.y,thetay2a - 0.5 * M_PI));
	  goalForwardCircle2Ptr = makeForwardCircularArc(goalCenter, thetay2a, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);
	  goalReverseCircle2Ptr = makeReverseCircularArc(goalCenter, thetay2a, goalAngle, RIGHT_HAND_CIRCLE, RADIUS);
	};

      // splice all of the pieces together, starting from the goal and working backwards to the start.  

      // components associated with 1st bitangent
      goalForwardCircle1Ptr->m_childPTNodePtr = goalNodePtr;        // link goal to this edge
      crossTangent1EndNode->m_outEdges.push_back(goalForwardCircle1Ptr); // link goal circ. edge to Node

      goalReverseCircle1Ptr->m_childPTNodePtr = goalNodePtr;        // link goal to this edge
      crossTangent1EndNode->m_outEdges.push_back(goalReverseCircle1Ptr); // link goal circ. edge to Node

      crossTangent1Ptr->m_childPTNodePtr = crossTangent1EndNode;    // link bitangent endNode to startNode
      crossTangent1StartNode->m_outEdges.push_back(crossTangent1Ptr);   // link the start of the bitangent

      startForwardCircle1Ptr->m_childPTNodePtr = crossTangent1StartNode; // link start circ. arc to bitan. start
      startNodePtr->m_outEdges.push_back(startForwardCircle1Ptr);        // link startNode to circ. arc.
      startReverseCircle1Ptr->m_childPTNodePtr = crossTangent1StartNode; // link start circ. arc to bitang. start
      startNodePtr->m_outEdges.push_back(startReverseCircle1Ptr);

      // components associated with 2nd bitangent
      goalForwardCircle2Ptr->m_childPTNodePtr = goalNodePtr;        // link goal to this edge
      crossTangent2EndNode->m_outEdges.push_back(goalForwardCircle2Ptr); // link bitang to goal circ. arc

      goalReverseCircle2Ptr->m_childPTNodePtr = goalNodePtr;        // link goal to this edge
      crossTangent2EndNode->m_outEdges.push_back(goalReverseCircle2Ptr); // link bitang to other goal circ. arc

      crossTangent2Ptr->m_childPTNodePtr = crossTangent2EndNode;    // link node at bitangent end to bitangent
      crossTangent2StartNode->m_outEdges.push_back(crossTangent2Ptr);      // link the start of the bitangent

      startForwardCircle2Ptr->m_childPTNodePtr = crossTangent2StartNode;   // link start circ. arc to bitan start
      startNodePtr->m_outEdges.push_back(startForwardCircle2Ptr);          // link startNode to circ. arc.
      startReverseCircle2Ptr->m_childPTNodePtr = crossTangent2StartNode;   // link start circ. arc to bitan start
      startNodePtr->m_outEdges.push_back(startReverseCircle2Ptr);          // link startNode to circ. arc.
    };					      
  return;
};

// ---------------------------------------------------------------------------------------------------------
// makes the basic set of paths that involve the pair of starting and goal circles.
// this function should be the first path generation function called, as it also fills out 
// key data structures (concerning the start and goal circle geometries) that are needed by subsequent 
// parts of the algorithm.  Bool "useStandoff" determines if we use standoffs (e.g., for parking slots).
// If bool returnClosestPath is true, then CirclePlanner will return a path which takes Alice closest
// to the goal when no path goes exactly to the goal.

void CirclePlanner::MakeQuadCirclePlan(double RADIUS)
{
  //make up the paths for the combinations of START and GOAL circles

  StartGoalCirclePair(this->m_rightStartCircle, this->m_rightGoalCircle, RIGHT_RIGHT_COMBO, RADIUS);
  StartGoalCirclePair(this->m_leftStartCircle, this->m_leftGoalCircle, LEFT_LEFT_COMBO, RADIUS);
  StartGoalCirclePair(this->m_rightStartCircle, this->m_leftGoalCircle, RIGHT_LEFT_COMBO, RADIUS);
  StartGoalCirclePair(this->m_leftStartCircle, this->m_rightGoalCircle, LEFT_RIGHT_COMBO,RADIUS);

};

/* --------------------------------------------------------------------------------------------- */
// the main function for Circle Planner.  This assumes that an instance of Circle Planner has
// already been created (given information about starting and goal states, etc).  This function 
// has the following structure:  1) create the possible paths to solve the problem; 2) remove the
// paths that are physically infeasible; 3) create an instance of the A* search class, and
// then search all of the feasible paths for the best path.
/* --------------------------------------------------------------------------------------------- */

Err_t CirclePlanner::CirclePlannerMain(CSpecs_t *cSpecs, bool useStandOff)
{
  Err_t returnStatus;
  unsigned int SearchState;                            // status of A* Search on Path Graph
  unsigned int SearchSteps=1;                          // how many "Search Steps" have we taken

  Log::getStream(6) << "CIRCLEPLANNER:: in Main" << endl;

  // -------------------- Initialize CSpecs and Configuration Data -------------------------------
  // get cSpecs data on Zone Boundary, convert to right hand coordinates, and adjust the boundary if necessary
  point2arr leftHandedZoneBoundary = cSpecs->getBoundingPolygon();
  this->m_rightHandedZoneBoundary = LeftToRightPointArray(leftHandedZoneBoundary);
  this->AdjustZoneBoundary(cSpecs);

  /*
  this->LogZoneBoundary();                             // log the zone boundary info for debugging
  */

  // get cSpecs on Obstacles, convert them to right hand coordinates, and store containers for later use
  this->m_obstacles.clear();
  vector<point2arr> leftHandedObstacles = cSpecs->getPolyObstacles();
  Console::addMessage("CIRCLEPLANNER:: Number of Obstacles = %i",leftHandedObstacles.size());
  Log::getStream(3) << "CIRCLEPLANNER:: number of osbtacles  " << leftHandedObstacles.size() << endl;

  // get safety Margin info and put in CirclePlanner containers for later use
  this->m_safetyMargins = cSpecs->getPerimeterSafetyMargins();
  m_aliceHalfWidth = 0.5 * (VEHICLE_WIDTH + m_safetyMargins.at(SAFETY_LEFT) + m_safetyMargins.at(SAFETY_RIGHT) );

  // ------------------------- Initialize basic paths used by CirclePlanner -------------------------------

  this->MakeQuadCirclePlan(TURNING_RADIUS);            // set up all of the possible start/goal paths.

  /*
  if(useStandOff)                                      // if standoffs are requested, make additional stand-off circles
    { this->MakeGoalStandOffPaths(TURNING_RADIUS);};
  */

#if CIRCLE_PLANNER_DEBUG
  cerr << "  Saving all basec solution paths in clothoidTree.dat.. " << endl;
  this->m_startNodePtr->printAllPaths(&CirclePlanner::m_tree_file);
  cerr << "  putting zone boundary in tree file " << endl;
  this->printZoneBoundary(&m_tree_file); 
  cerr << "  printing dense Alice Box " << endl;
  this->printDenseAliceBox(this->m_startPose, cSpecs->getPerimeterSafetyMargins(), &m_tree_file);
  this->printDenseAliceBox(this->m_goalPose, cSpecs->getPerimeterSafetyMargins(), &m_tree_file);
#endif

  // now remove illegal paths (ones where Alices intersects the zone boundary or obstacles), and expand out
  // paths from zone/obstacle boundaries where possible

#if CIRCLE_PLANNER_DEBUG
  this->CullAndExpandNodeOutEdges(this->m_startNodePtr, &m_ctree_file, EXPAND_DEPTH);
  cerr << "  Saving  culled paths in culledTree.dat.. " << endl;
  this->m_startNodePtr->printAllPaths(&m_ctree_file);
  this->printZoneBoundary(&m_ctree_file);
  this->printDenseAliceBox(m_startPose, cSpecs->getPerimeterSafetyMargins(), &m_ctree_file);
  this->printDenseAliceBox(m_goalPose, cSpecs->getPerimeterSafetyMargins(), &m_ctree_file);
#else
  this->CullAndExpandNodeOutEdges(this->m_startNodePtr, EXPAND_DEPTH);
#endif

  // now that feasible paths have been generated, make an AStarSearch instance, then set start and Goal states.
  AStarSearch astarsearch;                          
  astarsearch.SetStartAndGoalStates(this->m_startNodePtr, this->m_goalNodePtr); 

  // main A* search loop that increments A* search via one pass through the Open List at each step.
  do
    {
      SearchState = astarsearch.SearchStep(this);       // take one search step
      SearchSteps++;
    }
  while (SearchState == AStarSearch::SEARCH_STATE_SEARCHING);

  // now that the search is completed, check for success.  If successful, put together the path and return.
  if(SearchState==AStarSearch::SEARCH_STATE_SUCCEEDED)
    {
      Console::addMessage("CIRCLEPLANNER:: A* search successful");
      Log::getStream(5) << "CIRCLEPLANNER:: A* search successful" << endl;

      // due to nature of the A* search, the solution path gets saved in reverse order.  So,
      // reverse the path in preparation for the planner interface to create a graph path
      for(vector<Clothoid *>::reverse_iterator i=this->m_solutionPath.rbegin(); i != this->m_solutionPath.rend(); ++i)
	{ 
	  this->m_forwardPath.push_back( (*i) ); 
	};
#if CIRCLE_PLANNER_DEBUG
      cerr << "  printing path to clothoidPath.m " << endl;
      for(vector<Clothoid *>::iterator j = this->m_forwardPath.begin(); j!= this->m_forwardPath.end(); j++)
	{ 
	  (*j)->printClothoid(&m_path_file); 
	  this->printDenseAliceBox(this->m_startPose, cSpecs->getPerimeterSafetyMargins(), &m_path_file);
	  this->printDenseAliceBox(this->m_goalPose, cSpecs->getPerimeterSafetyMargins(), &m_path_file);
	  this->printZoneBoundary(&m_path_file);
	};
#endif
      returnStatus = CIRCLE_PLANNER_OK;
    }
  else
    { 
      if(m_startNodePtr->m_outEdges.empty())
	{
	  Log::getStream(4) << "CIRCLEPLANNER A* failed: Start is probably blocked " << endl;
	  Console::addMessage("CIRCLEPLANNER:: A* FAILED, start is blocked ");
	  returnStatus = S1PLANNER_START_BLOCKED;
	}
      else
	{
	  Log::getStream(4) << "CIRCLEPLANNER A* failed: Goal is probably blocked " << endl;
	  Console::addMessage("CIRCLEPLANNER:: A* FAILED, Goal is blocked ");
	  returnStatus = S1PLANNER_END_BLOCKED;
	};
    };

  return returnStatus;
};

