/*
 * display file for test program
 *
 * RMM, 22 Jul 92
 *
 */

/* Declare callbacks */
extern int toggle_onoff(long), user_quit(long), graph_data(long);
extern int capture_data(long), dump_data(long);

extern int ival_callback(long), dval_callback(long);

/* User specified update routines */
extern int user_clock(DD_ACTION, int);

/* Object ID's (offset into table) */
#define ONOFF	38
#define QUITB	39


#ifdef __cplusplus
extern "C" {
#endif

/* Allocate space for buffer storage */
static char buffer[56];

static DD_IDENT display[] = {
{1, 1, (void *)("01"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{1, 29, (void *)("Dynamic Display Module"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{2, 1, (void *)("02"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{2, 32, (void *)("RMM, 21 Jul 92"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{3, 1, (void *)("03"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{4, 1, (void *)("04 Data items:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{5, 1, (void *)("05   Use '=' to change entries in value column."), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{6, 1, (void *)("06   Mirrored values should update automatically (not selectable)"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{7, 1, (void *)("07  Callback values should update via function call (not selectable)"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 1, (void *)("08"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{9, 1, (void *)("09    type"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{9, 17, (void *)("value"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{9, 25, (void *)("mirror"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{9, 33, (void *)("callback"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{10, 1, (void *)("10    -----"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{10, 17, (void *)("-----"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{10, 25, (void *)("------"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{10, 33, (void *)("--------"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{11, 1, (void *)("11    Integer"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{12, 1, (void *)("12    Double"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 1, (void *)("13"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{14, 1, (void *)("14"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{15, 1, (void *)("15 Extern values"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{16, 1, (void *)("16  This value is updated automatically (not selectable)."), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{17, 1, (void *)("17"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{18, 1, (void *)("18  Time spent:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{19, 1, (void *)("19"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{20, 1, (void *)("20 Option buttons:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{21, 1, (void *)("21  The option buttons flash and execute a function when pressed."), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{22, 1, (void *)("22"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{23, 1, (void *)("23"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{11, 17, (void *)(&(ival)), dd_short, "%5u", buffer+0, 1, ival_callback, (long)(long) 0, 0, 0, Data, "ival", -1},
{11, 25, (void *)(&(ival)), dd_short, "%5u", buffer+8, 0, dd_nilcbk, (long)(long) 0, GREEN, 0, Data, "ival", -1},
{11, 33, (void *)(&(dd_dummyvar)), dd_short, "%5u", buffer+16, 0, dd_nilcbk, (long)(long) 0, YELLOW, 0, Data, "ival_c", -1},
{12, 17, (void *)(&(dval)), dd_double, "%5g", buffer+24, 1, dval_callback, (long)(long) 0, 0, 0, Data, "dval", -1},
{12, 25, (void *)(&(dd_dummyvar)), dd_double, "%5g", buffer+32, 0, dd_nilcbk, (long)(long) 0, GREEN, 0, Data, "dval-2", -1},
{12, 33, (void *)(&(dval_c)), dd_double, "%5g", buffer+40, 0, dd_nilcbk, (long)(long) 0, YELLOW, 0, Data, "dval_c", -1},
{18, 17, (void *)(&(time_spent)), user_clock, "%4u", buffer+48, 0, dd_nilcbk, (long)(long) 0, 0, 0, Data, "time_spent", -1},
{23, 4, (void *)("ON/OFF"), dd_label, "NULL", (char *) NULL, 1, toggle_onoff, (long)(long) 0, 0, 0, Button, "", -1},
{23, 12, (void *)("QUIT"), dd_label, "NULL", (char *) NULL, 1, user_quit, (long)(long) 0, 0, 0, Button, "", -1},
{23, 18, (void *)("GRAF"), dd_label, "NULL", (char *) NULL, 1, graph_data, (long)(long) 0, 0, 0, Button, "", -1},
{23, 25, (void *)("CAPTURE"), dd_label, "NULL", (char *) NULL, 1, capture_data, (long)(long) 0, 0, 0, Button, "", -1},
{23, 34, (void *)("DUMP"), dd_label, "NULL", (char *) NULL, 1, dump_data, (long)(long) 0, 0, 0, Button, "", -1},
DD_End};

#ifdef __cplusplus
}
#endif

