/*
 * virtual.h - definitions for virtual drivers
 *
 * \author Richard M. Murray
 * \date 21 Jul 93
 *
 */
 
/* Function declarations */
int virtual_driver(DEV_ACTION, ...);

