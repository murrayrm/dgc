/*!MapPrediction.cc
 * Author: Pete Trautman
 * Last revision: April 18 2007
 * */

#include "MapPrediction.hh"

using namespace std;

CMapPrediction::CMapPrediction(int skynetKey, bool bWaitForStateFill,
                                 int debugLevel, int verboseLevel, 
                                 bool log)
  : CSkynetContainer(MODtrafficplanner, skynetKey)
  , CStateClient(bWaitForStateFill)
{
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Assign member variables based on cmdline input
  m_snKey = skynetKey;
  if (debugLevel>0){
    m_debug = true;
    cout << "debug is on" << endl;
  }
  else m_debug = false;
  if (verboseLevel>0) {
    m_verbose = true;
    cout << "verbose is on" << endl;
  }
  else m_verbose = false;
  
  m_log = log;
  if (m_log) 
    cout << "logging is on" << endl;


  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Mutexes
  // Local Map
  DGCcreateMutex(&m_LocalMapMutex);

  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Conditions
 
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Initialization
  // Local Map
  initRecvMapElement(skynetKey);
  initSendMapElement(skynetKey);
  if(!m_recvLocalMap){
    //bool loadMapFromRNDF =  loadRNDF(string filename) {return localmap.loadRNDF(filename);
  }
  m_localMap = new Map();
}

CMapPrediction::~CMapPrediction() 
{
  // delete pointers
  delete m_localMap;
  // delete mutexes
  DGCdeleteMutex(&m_LocalMapMutex);
}


void CMapPrediction::getLocalMapThread()
{

  MapElement recvEl;
  int bytesRecv;
  while (true){
  bytesRecv = recvMapElementBlock(&recvEl,1);
 
  if (bytesRecv>0){
      DGClockMutex(&m_LocalMapMutex);
    m_localMap->addEl(recvEl);
    DGCunlockMutex(&m_LocalMapMutex);
  }else {
    cout << "Error in CMapPrediction::getLocalMapThread, received value from recvMapElementBlock = " << bytesRecv << endl;
    usleep(100);
  }
}
 
}

void CMapPrediction::MapPredictionLoop(void)
{
  //=====================================================
  UpdateState(); // this gives m_state
  point2 statedelta(m_state.utmNorthing-m_state.localX, m_state.utmEasting-m_state.localY);
  m_localMap->prior.delta = statedelta;         
  //======================================================
   
   MapElement recvEl;
   MapElement tempEl;
   int sendChannel=0;

  while(1){
  DGClockMutex(&m_LocalMapMutex);

  for (unsigned i =0; i< m_localMap->data.size(); ++i)
    {    
     sendMapElement(&m_localMap->data[i], sendChannel);
    }

  for (unsigned i =0; i< m_localMap->prior.data.size(); ++i)
    {
      m_localMap->prior.getEl(tempEl,i);
      sendMapElement(&tempEl, sendChannel);
    }

  DGCunlockMutex(&m_LocalMapMutex);

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
 //here's where the consequential stuff begins
   
   MapElement dob1; //dynamic obstacle 1
   MapElement alice; //alice
   MapElement lineBoundary; //line boundaries functioning as measurements
   int horizon=130; //horizon of prediction
   double range=10;

   LaneLabel dob1label(1,2); 
   point2 dob1size(2,5);//good size for block cars

   //initialization of the obstacles; this should be read in from mapper in the future
   point2 dob1cpt(-5,-72);//initial position straight down
   //point2 dob1cpt(30,-72);//initial position on turn
   //point2 dob1cpt(0,8);//initial position bottom
   point2 vel(0.5,5);//initial velocity straight down
   //point2 vel(5,0);//initial velocity turn
      
   int N = 50; //number of particles
   double sigma=0.5; //initial covariance in the samples
   double resampleFactor = 0.9; //Neff <= resampleFactor*N
   point2 sampleFactor(0.1,0.4); //noise in sampling distribution
   int modelType=1;
   int partitions = 2;
   point2 noise(0,0);
   vector<double> ang(N);
   //double anginit = 0.05;//-2*acos(-1)/4; ang 0 = straight down; ccw from that is neg 2pi
   point2arr particlesPlus;
   point2arr particlesNow;
   point2arr velNow;
   ParticleFilter dob1PF(resampleFactor,N,dob1cpt,vel,modelType,sigma);   
   
   velNow = dob1PF.getVelNow();
   particlesNow=dob1PF.getParticlesNow();
     
   double deltaT=0.05;	
   long long int start,end;
  //MAIN PREDICTION LOOP           
  for (int i =0; i< horizon; ++i)
   { 
     ang = dob1PF.getang();
     //cout<<"ang"<<ang<<endl;
     for(int k=0; k<N;k++)
         {   
          dob1.id=k;
          dob1.setTypeVehicle();

          dob1.setColor(MAP_COLOR_RED,100);
          dob1.setGeometry(particlesNow[k], dob1size.x, dob1size.y, ang[k]);
          sendMapElement(&dob1,sendChannel);
         }
       //begin particle filter
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
   
   start=DGCgettime();
   
   particlesPlus=dob1PF.sampleConstVel(sampleFactor,deltaT);//predicts Now to Plus
   //particlesPlus=dob1PF.sampleCoordTurn(sampleFactor,deltaT);
   
   //collecting the measurements for each particle; in general, one measurement
   //will exist for particle set at time t 
     vector<point2arr> lbound(N),rbound(N); //measurement vector
   for(int k=0; k<N;k++)
    {
     m_localMap->getBounds(lbound[k],rbound[k],dob1label,particlesPlus[k],range);
    }
    //end calculating "Measurement"
 
    dob1PF.weight(lbound,rbound);
    
    dob1PF.resample(partitions);
    
    particlesNow=dob1PF.getParticlesNow();
    velNow=dob1PF.getVelNow();
	//cout<<"velNow"<<velNow<<endl;
    
    //end PF
    
    end = DGCgettime();
   deltaT = (end-start)/100000.0;
    
 
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

    //alice's state
    UpdateState(); //updates m_state
    alice.set_alice(m_state);
    sendMapElement(&alice,sendChannel);
    //end alice's state    

    //here we retrieve the lineboundary at time t = i
    //m_localMap->getBounds(lbound, rbound, dob1label,dob1cpt, range);

    usleep(100000);
    }
  }
}

