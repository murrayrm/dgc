/*!ParticleFilter.cc
 * Author: Pete Trautman
 * Last revision: April 18 2007
 * */

#include "ParticleFilter.hh"

using namespace std;

ParticleFilter::ParticleFilter()

//int skynetKey, int debugLevel, 
				  //int verboseLevel, bool log)
{
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Assign member variables based on cmdline input
  /*m_snKey = skynetKey;
  if (debugLevel>0){
    m_debug = true;
    cout << "debug is on" << endl;
  }
  else m_debug = false;
  if (verboseLevel>0) {
    m_verbose = true;
    cout << "verbose is on" << endl;
  }
  else m_verbose = false;
  
  m_log = log;
  if (m_log) 
    cout << "logging is on" << endl; */
}

ParticleFilter::~ParticleFilter() 
{
}

//real functions

/*void ParticleFilter::kalmanFilter()
{
  CvKalman* kalman;
  int dynam_params=1;
  int measure_params=1;
  int control_params=0;
  CvMat* state = cvCreateMat( 2, 1, CV_32FC1 );
  CvMat* process_noise = cvCreateMat( 2, 1, CV_32FC1 );
  kalman=cvCreateKalman( dynam_params, measure_params, control_params);
}*/


void ParticleFilter::setN(int N)
{
  m_N = N;
}

void ParticleFilter::initialize(point2arr& particlesPlus,point2arr& particlesNow,
				vector<double>& postWeights,vector<double>& preWeights,
				point2& dob1cpt, int modelType)
{
   m_modelType=modelType;
   //initializing the random number seeds
   gsl_rng_env_setup();
     
   m_T = gsl_rng_default;
   m_r = gsl_rng_alloc (m_T);
   m_sigma=1;
   
   point2 noise;
   
   for (int i =0; i< m_N; ++i)
   {
    noise.x = gsl_ran_gaussian(m_r,m_sigma);
    noise.y = gsl_ran_gaussian(m_r,m_sigma);
    particlesNow[i]=dob1cpt+noise;
    particlesPlus[i]=dob1cpt+noise;
    postWeights[i]=1.0/m_N;
    preWeights[i]=1.0/m_N;
   }
}

void ParticleFilter::predict(point2arr& particlesPlus, point2arr& particlesNow)
{
   //double u = gsl_rng_uniform(r);
   point2 noise;
   point2 v;
  
   v.x = 0;
   v.y = 0.5;
   double sigmax = 0.5*m_sigma;
   double sigmay = 0.5*m_sigma;
   for(int i=0;i<m_N;i++)
   { 
    //constant velocity model with noise
    noise.x = gsl_ran_gaussian(m_r,sigmax);
    noise.y = gsl_ran_gaussian(m_r,sigmay);    
    particlesPlus[i]=particlesNow[i]+v+noise;
   }
}

void ParticleFilter::weight(vector<double>& postWeights, vector<double>& preWeights,
                            point2arr& particlesPlus, vector<point2arr>& lbound,
                            vector<point2arr>& rbound)
{
   //for(vector<point2arr>::iterator it=lbound.begin();it!=lbound.end(); it++)
   for(int i=0;i<m_N;i++)//calculating weights
     { 
       point2 mbound = (lbound[i][0]+rbound[i][0])/2.0;
       //point2 mbound = (*it[0]+*it[0])/2.0
       double x=mbound.x-particlesPlus[i].x;
       double logWeight=pow(x,2);
       double wSigma=10;	
       postWeights[i]=preWeights[i]*exp(-logWeight/wSigma);
     }
     
      double sum = 0;
   for(int i=0;i<m_N;i++)//calculating normalizer
     {
       sum=sum+postWeights[i];
     }
   
   for(int i=0;i<m_N;i++)//normalization
     {
       postWeights[i]=postWeights[i]/sum;
     }
}

void ParticleFilter::resample(vector<double>& preWeights, vector<double>& postWeights,
                point2arr& particlesNow,point2arr& particlesPlus, double resampleFactor)
{
     
  double particleVar=0;
   for(int k=0; k<m_N;k++)
    {
     particleVar=particleVar+pow(postWeights[k],2);
    }
   double Neff=1/particleVar;
     //cout<<"Neff"<<Neff<<endl;
 
 if(Neff<=resampleFactor*m_N)
  { //&&&&&&&&&&if
     cout<<"resample"<<endl;    
     double P[m_N];
     for(int i=0;i<m_N;i++)
      {
	P[i]=postWeights[i];
      } 
     
     gsl_ran_discrete_t* g;//lookup table 
     size_t sampleIndex;
     g = gsl_ran_discrete_preproc(sizeof(m_N), P);
     
   for(int i=0;i<m_N;i++)  //resampling loop
     {
      sampleIndex = gsl_ran_discrete (m_r,g); //returns sampleIndex i with Prob=weight(i)
      particlesNow[i] = particlesPlus[sampleIndex]; //new particles are born from old ones
     }
     
   for(int i=0;i<m_N;i++)//resetting the weights
     {
      preWeights[i]=1.0/m_N;
     }
  }//&&&&&&&&&if

 else //if resampling unneccesary, then everything is just as .predict said
  {
   cout<<"don't resample"<<endl;
   preWeights=postWeights;
   particlesNow=particlesPlus;
  }
}




















