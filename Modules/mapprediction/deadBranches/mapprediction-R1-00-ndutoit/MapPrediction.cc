/*!MapPrediction.cc
 * Author: Noel duToit
 * Last revision: Feb 24 2007
 * */

#include "MapPrediction.hh"

using namespace std;

CMapPrediction::CMapPrediction(int skynetKey, bool bWaitForStateFill,
                                 int debugLevel, int verboseLevel, 
                                 bool log)
  : CSkynetContainer(MODtrafficplanner, skynetKey)
  , CStateClient(bWaitForStateFill)
{
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Assign member variables based on cmdline input
  m_snKey = skynetKey;
  if (debugLevel>0){
    m_debug = true;
    cout << "debug is on" << endl;
  }
  else m_debug = false;
  if (verboseLevel>0) {
    m_verbose = true;
    cout << "verbose is on" << endl;
  }
  else m_verbose = false;
  
  m_log = log;
  if (m_log) 
    cout << "logging is on" << endl;


  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Mutexes
  // Local Map
  DGCcreateMutex(&m_LocalMapMutex);

  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Conditions
 
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Initialization
  // Local Map
  initRecvMapElement(skynetKey);
  initSendMapElement(skynetKey);
  if(!m_recvLocalMap){
    //bool loadMapFromRNDF =  loadRNDF(string filename) {return localmap.loadRNDF(filename);
  }
  m_localMap = new Map();
}

CMapPrediction::~CMapPrediction() 
{
  // delete pointers
  delete m_localMap;
  // delete mutexes
  DGCdeleteMutex(&m_LocalMapMutex);
}


void CMapPrediction::getLocalMapThread()
{

  MapElement recvEl;
  int bytesRecv;
  while (true){
  bytesRecv = recvMapElementBlock(&recvEl,1);
 
  if (bytesRecv>0){
      DGClockMutex(&m_LocalMapMutex);
    m_localMap->addEl(recvEl);
    DGCunlockMutex(&m_LocalMapMutex);
  }else {
    cout << "Error in CMapPrediction::getLocalMapThread, received value from recvMapElementBlock = " << bytesRecv << endl;
    usleep(100);
  }
}
  //if (recvLocalMap)
  //{
  //TODO: Put Sam's code here
  //}
  //else
  //{
  //TODO: initialize map object from the rndf and use that to plan
  //}
}

void CMapPrediction::MapPredictionLoop(void)
{

}
