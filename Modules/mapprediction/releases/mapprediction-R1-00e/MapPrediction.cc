/*!MapPrediction.cc
 * Author: Pete Trautman
 * Last revision: April 18 2007
 * */

#include "MapPrediction.hh"

using namespace std;

CMapPrediction::CMapPrediction(int skynetKey, bool bWaitForStateFill,
                                 int debugLevel, int verboseLevel, 
                                 bool log)
  : CSkynetContainer(MODtrafficplanner, skynetKey)
  , CStateClient(bWaitForStateFill)
{
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Assign member variables based on cmdline input
  m_snKey = skynetKey;
  if (debugLevel>0){
    m_debug = true;
    cout << "debug is on" << endl;
  }
  else m_debug = false;
  if (verboseLevel>0) {
    m_verbose = true;
    cout << "verbose is on" << endl;
  }
  else m_verbose = false;
  
  m_log = log;
  if (m_log) 
    cout << "logging is on" << endl;


  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Mutexes
  // Local Map
  DGCcreateMutex(&m_LocalMapMutex);

  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Conditions
 
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Initialization
  // Local Map
  initRecvMapElement(skynetKey);
  initSendMapElement(skynetKey);
  if(!m_recvLocalMap){
    //bool loadMapFromRNDF =  loadRNDF(string filename) {return localmap.loadRNDF(filename);
  }
  m_localMap = new Map();
}

CMapPrediction::~CMapPrediction() 
{
  // delete pointers
  delete m_localMap;
  // delete mutexes
  DGCdeleteMutex(&m_LocalMapMutex);
}


void CMapPrediction::getLocalMapThread()
{

  MapElement recvEl;
  int bytesRecv;
  while (true){
  bytesRecv = recvMapElementBlock(&recvEl,1);
 
  if (bytesRecv>0){
      DGClockMutex(&m_LocalMapMutex);
    m_localMap->addEl(recvEl);
    DGCunlockMutex(&m_LocalMapMutex);
  }else {
    cout << "Error in CMapPrediction::getLocalMapThread, received value from recvMapElementBlock = " << bytesRecv << endl;
    usleep(100);
  }
}
 
}

void CMapPrediction::MapPredictionLoop(void)
{
  //=====================================================
  UpdateState(); // this gives m_state
  point2 statedelta(m_state.utmNorthing-m_state.localX, m_state.utmEasting-m_state.localY);
  m_localMap->prior.delta = statedelta;         
  //======================================================
   
   MapElement recvEl;
   MapElement tempEl;
   int sendChannel=0;

  while(1){
  DGClockMutex(&m_LocalMapMutex);

  for (unsigned i =0; i< m_localMap->data.size(); ++i)
    {    
     sendMapElement(&m_localMap->data[i], sendChannel);
    }

  for (unsigned i =0; i< m_localMap->prior.data.size(); ++i)
    {
      m_localMap->prior.getEl(tempEl,i);
      sendMapElement(&tempEl, sendChannel);
    }

  DGCunlockMutex(&m_LocalMapMutex);

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
 //here's where the consequential stuff begins
   
   MapElement dob1; //dynamic obstacle 1
   MapElement alice; //alice
   MapElement lineBoundary; //line boundaries functioning as measurements
   int horizon=430; //horizon of prediction
   
   //LaneLabel dob1label(1,2); //straight down
   //LaneLabel dob1label(4,2); //turn
   LaneLabel dob1label(2,1); //for stlukesmall
   
   point2 dob1size(2,5);//good size for block cars

   //initialization of the obstacles; this should be read in from mapper in the future
   //point2 dob1cpt(-5,-10);//initial position straight down
   //point2 dob1cpt(30,-72);//initial position on turn
   point2 dob1cpt(135,50);//initial position on turn
   //point2 dob1cpt(0,8);//initial position bottom
   point2 vel(4,-8);//initial velocity straight down
   //point2 vel(1,1);//initial velocity turn
      
   int N = 100; //number of particles
   double sigma=0.5; //initial covariance in the samples
   double resampleFactor = 0.0; //Neff <= resampleFactor*N
   
   //point2 sampleFactor(5,0.4); //noise for downward velocity
   point2 sampleFactor(0.1,0.1); //noise for turn velocity
   int modelType=1;
   int partitions = 2;
   point2 noise(0,0);
   vector<double> ang(N);
   
   point2arr particlesPlus;
   point2arr particlesNow;
   point2arr projpts(N);
   
   double deltaT=0.05;
   double meanAbsVel = 8.0;	
   vector<double> centerHeading(N);
   vector<point2arr> lbound(N),rbound(N); //measurement vector
   vector<point2> centerPoint(N);
   vector<point2arr> centerLine(N);
   
   ParticleFilter dob1PF(resampleFactor,N,dob1cpt,vel,modelType,sigma,meanAbsVel);   
    
   particlesNow=dob1PF.getParticlesNow();
    
  //MAIN PREDICTION LOOP           
  for (int i =0; i< horizon; ++i)
   { 
     ang = dob1PF.getang();
     //cout<<"ang"<<ang<<endl;
     for(int k=0; k<N;k++)
         {   
          dob1.id=k;
          dob1.setTypeVehicle();

          dob1.setColor(MAP_COLOR_RED,100);
          dob1.setGeometry(particlesNow[k], dob1size.y, dob1size.x, ang[k]);
          sendMapElement(&dob1,sendChannel);
         }
       //begin particle filter
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
         
   for(int k=0; k<N;k++)
    {
     //m_localMap->getBounds(lbound[k],rbound[k],dob1label,particlesPlus[k],range);
     m_localMap->getLaneCenterLine(centerLine[k],dob1label);
     m_localMap->getHeading(centerHeading[k],centerPoint[k],dob1label,particlesNow[k]);
     centerHeading[k] = centerHeading[k];
    }
     
/*1*/    particlesPlus=dob1PF.sampleMap(centerHeading,sampleFactor,deltaT);
    /*
    for(int k=0; k<N;k++)
     {
      m_localMap->getProjectToLine(projpts[k],centerLine[k],particlesPlus[k]); 
     }
    */
/*2*/     //dob1PF.weight(projpts);
    
/*3*/     //dob1PF.resample(partitions);
    
    particlesNow=dob1PF.getParticlesNow();
    
    //end PF
    
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

    
    UpdateState(); //updates m_state
    alice.set_alice(m_state);
    sendMapElement(&alice,sendChannel);
/*
    lineBoundary.clear();
    lineBoundary.id=-1;
    lineBoundary.setTypePoints();
    lineBoundary.setGeometry(centerPoint);
    lineBoundary.setColor(MAP_COLOR_GREEN,100);
    sendMapElement(&lineBoundary,sendChannel);
    
    lineBoundary.clear();
    lineBoundary.id=-1;
    lineBoundary.setTypeLaneLine();
    lineBoundary.setGeometry(centerLine[0]);
    lineBoundary.setColor(MAP_COLOR_ORANGE,100);
    sendMapElement(&lineBoundary,sendChannel);
*/
    lineBoundary.clear();  
    lineBoundary.id=-2;
    lineBoundary.setTypeLaneLine();
    lineBoundary.setGeometry(lbound[0]);
    lineBoundary.setColor(MAP_COLOR_GREEN,100);
    sendMapElement(&lineBoundary,sendChannel);

    lineBoundary.clear();
    lineBoundary.id=-3;
    lineBoundary.setTypeLaneLine();
    lineBoundary.setGeometry(rbound[0]);
    lineBoundary.setColor(MAP_COLOR_MAGENTA,100);
    sendMapElement(&lineBoundary,sendChannel);
    
    usleep(100000);
    }
  }
}

