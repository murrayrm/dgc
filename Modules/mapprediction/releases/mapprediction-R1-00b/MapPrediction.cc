/*!MapPrediction.cc
 * Author: Pete Trautman
 * Last revision: April 18 2007
 * */

#include "MapPrediction.hh"

using namespace std;

CMapPrediction::CMapPrediction(int skynetKey, bool bWaitForStateFill,
                                 int debugLevel, int verboseLevel, 
                                 bool log)
  : CSkynetContainer(MODtrafficplanner, skynetKey)
  , CStateClient(bWaitForStateFill)
{
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Assign member variables based on cmdline input
  m_snKey = skynetKey;
  if (debugLevel>0){
    m_debug = true;
    cout << "debug is on" << endl;
  }
  else m_debug = false;
  if (verboseLevel>0) {
    m_verbose = true;
    cout << "verbose is on" << endl;
  }
  else m_verbose = false;
  
  m_log = log;
  if (m_log) 
    cout << "logging is on" << endl;


  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Mutexes
  // Local Map
  DGCcreateMutex(&m_LocalMapMutex);

  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Conditions
 
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Initialization
  // Local Map
  initRecvMapElement(skynetKey);
  initSendMapElement(skynetKey);
  if(!m_recvLocalMap){
    //bool loadMapFromRNDF =  loadRNDF(string filename) {return localmap.loadRNDF(filename);
  }
  m_localMap = new Map();
}

CMapPrediction::~CMapPrediction() 
{
  // delete pointers
  delete m_localMap;
  // delete mutexes
  DGCdeleteMutex(&m_LocalMapMutex);
}


void CMapPrediction::getLocalMapThread()
{

  MapElement recvEl;
  int bytesRecv;
  while (true){
  bytesRecv = recvMapElementBlock(&recvEl,1);
 
  if (bytesRecv>0){
      DGClockMutex(&m_LocalMapMutex);
    m_localMap->addEl(recvEl);
    DGCunlockMutex(&m_LocalMapMutex);
  }else {
    cout << "Error in CMapPrediction::getLocalMapThread, received value from recvMapElementBlock = " << bytesRecv << endl;
    usleep(100);
  }
}
 
}

void CMapPrediction::MapPredictionLoop(void)
{
  //=====================================================
  UpdateState(); // this gives m_state
  point2 statedelta(m_state.utmNorthing-m_state.localX, m_state.utmEasting-m_state.localY);
  m_localMap->prior.delta = statedelta;         
  //======================================================
   
   MapElement recvEl;
   MapElement tempEl;
   //int bytesRecv;
   int sendChannel=0;

  while(1){
  DGClockMutex(&m_LocalMapMutex);

  for (unsigned i =0; i< m_localMap->data.size(); ++i)
    {    
     sendMapElement(&m_localMap->data[i], sendChannel);
    }

  for (unsigned i =0; i< m_localMap->prior.data.size(); ++i)
    {
      m_localMap->prior.getEl(i,tempEl);
      sendMapElement(&tempEl, sendChannel);
    }

  DGCunlockMutex(&m_LocalMapMutex);

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
 //here's where the consequential stuff begins
   
   MapElement dob1; //dynamic obstacle 1
   MapElement alice; //alice
   MapElement lineBoundary; //line boundaries functioning as measurements
   int N = 50; //number of particles
   int horizon=100; //horizon of prediction
   double resampleFactor = 0.5;

   LaneLabel dob1label(1,2); 
   double range=10;

   //id identifiers for my obstacles
   vector<int> iddob1; 
   vector<int> idalice;
   iddob1.push_back(1); 
   idalice.push_back(0);
 
   //initialization of the obstacles; this should be read in from mapper in the future
   point2 dob1cpt(-4,-60);
   point2 dob1size(2,5);
   point2 noise(0,0);
   double dob1ang = -0.05;

   //the particle filter for dob1
   ParticleFilter dob1PF;   
   point2arr particlesPlus(N);//only centerpoints for right now
   point2arr particlesNow(N);
   
   int modelType=1;
   vector<double> postWeights(N);
   vector<double> preWeights(N);
   
   //ParticleFilter pdob1PF;
   dob1PF.setN(N);
   dob1PF.initialize(particlesPlus,particlesNow,postWeights,
		     preWeights,dob1cpt,modelType);
	
  //MAIN PREDICTION LOOP           
  for (int i =0; i< horizon; ++i)
   { 
         //the output loop 
     for(int k=0; k<N;k++)
         {   
          dob1.set_block_obs(iddob1,particlesNow[k],dob1ang,dob1size.x,dob1size.y);
          dob1.plot_color=MAP_COLOR_RED;
          sendMapElement(&dob1,sendChannel); //here's how we send the obstacle to skynet
          iddob1.clear();
	  iddob1.push_back(k);
         }
    
    //begin particle filter
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
   
 
   dob1PF.predict(particlesPlus,particlesNow); //predicts Now to Plus
   
   //collecting the measurements for each particle; in general, one measurement
   //will exist for particle set at time t 
   vector<point2arr> lbound(N),rbound(N); //measurement vector
  
   for(int k=0; k<N;k++)
    {
     m_localMap->getBounds(lbound[k],rbound[k],dob1label,particlesPlus[k],range);
    }
    //end calculating "Measurement"
 
    //weighting Plus against the measurement; returns postWeights, takes preWeights, 
    dob1PF.weight(postWeights,preWeights,particlesPlus,lbound,rbound);
    //end weighting

    //resampling using Neff.  returns preWeights either =post or =1/N
    //returns Now either = resampledPlus or just Plus
    dob1PF.resample(preWeights,postWeights,particlesNow,particlesPlus,resampleFactor);
    //end resampling

    //end PF
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

    //alice's state
    UpdateState(); //updates m_state
    alice.set_alice(m_state);
    sendMapElement(&alice,sendChannel);
    //end alice's state    

    //here we retrieve the lineboundary at time t = i
    //m_localMap->getBounds(lbound, rbound, dob1label,dob1cpt, range);
		
    lineBoundary.clear();
    lineBoundary.set_id(1,2,-4);
    lineBoundary.set_laneline();
    lineBoundary.set_geometry(lbound[0]);
    lineBoundary.plot_color=MAP_COLOR_GREEN;
    sendMapElement(&lineBoundary,sendChannel);

    lineBoundary.clear();
    lineBoundary.set_id(1,2,-5);
    lineBoundary.set_laneline();
    lineBoundary.set_geometry(rbound[0]);
    lineBoundary.plot_color=MAP_COLOR_MAGENTA;
    sendMapElement(&lineBoundary,sendChannel);
    ///finish retrieving lineboundary
    usleep(100000);
    }
  }
}








//fun stuff

//calculating the "measurement"
    //point2 cptave(0,0);
    //for(int k=0; k<N;k++)
    //  {
    //   cptave.x = cptave.x + particlesPlus[k].x;
    //   cptave.y = cptave.y + particlesPlus[k].y;
    //  }
    //    cptave.x=cptave.x/N;
    //	cptave.y=cptave.y/N;
   
//point2 centerpt;
    //centerpt = dob1.center;

//here's a block obstacle; first need 
//1. a MapElement dob1
//2. dob1.set_block_obs(id, centerpoint, yaw, w,h)
//alice.center.x = m_state.localX;
    //alice.center.y = m_state.localY;	

//here is an experiment section which inserts some vehicles; use this part 
//to set obstacles in rndf (and which particles run over;
//1.  need to find a way to label/weight rndf portions
//2.  
 //point2 center;
 //center =tempEl.center;
 //point2arr geom; 
 //geom =tempEl.geometry;

 //id.clear();  //clears the id
 //id.push_back(24); //redefines the id

 //--------------------------------------------------
 // sending state to viewer
 //--------------------------------------------------
 
 //how do we get alice to move through the scene?
 //use planner stack; ./mapviewer listens to everything.
 //UpdateState();
 //alice.set_alice(m_state);
 //sendMapElement(&alice,sendChannel);
 //usleep(100000);
	//MapElement pointout
        //pointout.clear();
	//pointout.set_id(100);
	//pointout.set_points();
	//rbound = rbound+point2(10,0);
	
	//point2 tmppt;
	//for (int k =0; k <10; ++k)
	//  { 
 	//   tmppt = rbound[0];
	//   tmppt = tmppt + point2(k,k);
	//   rbound.push_back(tmppt);//.push_back makes a new entry in the vect of value tmppt.
        //  }

	//pointout.set_geometry(rbound);
        //pointout.plot_color=MAP_COLOR_YELLOW;
	//sendMapElement(&pointout,sendChannel);


