/*!ParticleFilter.cc
 * Author: Pete Trautman
 * Last revision: April 18 2007
 * */

#include "ParticleFilter.hh"

using namespace std;

ParticleFilter::ParticleFilter(double resampleFactor,int N,point2& cpt,point2& vel,int modelType, double sigma,double meanAbsVel)
{
   //initialization of randomization variables for gsl.
   gsl_rng_env_setup();
   m_T = gsl_rng_default;
   m_r = gsl_rng_alloc (m_T);
   m_sigma=sigma;
 
   //initialization of scalars
   m_resampleFactor = resampleFactor;
   m_N = N;
   m_Cpt = cpt;
   m_modelType=modelType;
   m_meanAbsVel = meanAbsVel;
   
   //initialization of vectors
   for (int i=0; i<m_N;i++)
   {
    m_Noise.x = gsl_ran_gaussian(m_r,m_sigma);
    m_Noise.y = gsl_ran_gaussian(m_r,m_sigma);
    
    m_particlesNow.push_back(m_Cpt+m_Noise);
    m_particlesPlus.push_back(m_Cpt+m_Noise);
    
    //m_Weights.push_back(1.0/m_N);
   }
   
   double u;
   point2 tmpvel;
   for(int i=0;i<m_N;i++)
   {
     u = gsl_rng_uniform(m_r);
     m_Noise.x = gsl_ran_gaussian(m_r,m_sigma);
     m_Noise.y = gsl_ran_gaussian(m_r,m_sigma);
     tmpvel.x = vel.x+m_Noise.x;//vel.x*(u-0.5);
     tmpvel.y = vel.y+m_Noise.y;//vel.y*(u+0.5);
     m_velPlus.push_back(tmpvel);
     m_velNow.push_back(tmpvel);
     
     m_omegaNow.push_back(0.000001*(u-0.5));
     m_omegaPlus.push_back(0.000001*(u-0.5));

     m_ang.push_back(-atan(m_velNow[i].x/m_velNow[i].y));
   }
}

ParticleFilter::~ParticleFilter() 
{
}

point2arr ParticleFilter::getParticlesNow()
{
  return m_particlesNow;
}

vector<double> ParticleFilter::getang()
{
  return m_ang;
}

point2arr ParticleFilter::getVelNow()
{
  return m_velNow;
}

void ParticleFilter::kalmanFilter()
{
  CvKalman* kalman;
  int dynam_params=1;
  int measure_params=1;
  int control_params=0;
  //CvMat* state = cvCreateMat( 2, 1, CV_32FC1 );
  //CvMat* process_noise = cvCreateMat( 2, 1, CV_32FC1 );
  kalman=cvCreateKalman(dynam_params,measure_params,control_params);
}

point2arr ParticleFilter::sampleConstVel(point2 sampleFactor,double deltaT)
{  
   //constant velocity model with noise
   point2 m_sampleFactor(sampleFactor.x,sampleFactor.y);
   double u;
   for(int i=0;i<m_N;i++)
    { 
    m_Noise.x = gsl_ran_gaussian(m_r,m_sampleFactor.x*m_sigma);
    m_Noise.y = gsl_ran_gaussian(m_r,m_sampleFactor.y*m_sigma);    
    
    m_particlesPlus[i]=m_particlesNow[i]+m_velNow[i]*deltaT;//+m_Noise;
    
    m_ang[i] = atan(m_velNow[i].y/m_velNow[i].x);
    
    u = gsl_rng_uniform(m_r);
    m_Noise.x = (u-0.5)/10;
    u = gsl_rng_uniform(m_r);
    m_Noise.y = (u-0.5)/10;
    m_velPlus[i]=m_velNow[i];//+m_Noise;

    }
  return m_particlesPlus;
}

point2arr ParticleFilter::sampleCoordTurn(point2 sampleFactor,double deltaT)
{   //coordinated turn model
    point2 m_sampleFactor(sampleFactor.x,sampleFactor.y);
    for(int i=0;i<m_N;i++)
    {
     m_Noise.x = gsl_ran_gaussian(m_r,m_sampleFactor.x*m_sigma);
     m_Noise.y = gsl_ran_gaussian(m_r,m_sampleFactor.y*m_sigma); 

     m_particlesPlus[i].x=m_particlesNow[i].x+
		(m_velNow[i].x*sin(deltaT*m_omegaNow[i]))/m_omegaNow[i]+
		(m_velNow[i].y*(1-cos(deltaT*m_omegaNow[i])))/m_omegaNow[i]+
		 m_Noise.x;
     m_velPlus[i].x=m_velNow[i].x*cos(deltaT*m_omegaNow[i])-
                    m_velNow[i].y*sin(deltaT*m_omegaNow[i]);
     
     m_particlesPlus[i].y=m_particlesNow[i].y+
		(m_velNow[i].y*sin(deltaT*m_omegaNow[i]))/m_omegaNow[i]+
		(m_velNow[i].x*(1-cos(deltaT*m_omegaNow[i])))/m_omegaNow[i]+
		 m_Noise.y;
     m_velPlus[i].y=m_velNow[i].y*cos(deltaT*m_omegaNow[i])+
                    m_velNow[i].x*sin(deltaT*m_omegaNow[i]);   
     
     m_omegaPlus[i] = m_omegaNow[i];

     m_ang[i] = atan(m_velNow[i].y/m_velNow[i].x);
     //m_ang[i]= m_omegaPlus[i]*deltaT;
    }
  return m_particlesPlus;
}

point2arr ParticleFilter::sampleMap(vector<double>& centerHeading, point2 sampleFactor,double deltaT)
{
 //constant velocity model with noise
   point2 m_sampleFactor(sampleFactor.x,sampleFactor.y);
   double absVel, accelNoise, accelVar = 10;//accel var gives spread along dir of travel
      
   for(int i=0;i<m_N;i++)
    { 
    m_Noise.x = gsl_ran_gaussian(m_r,m_sampleFactor.x*m_sigma);
    m_Noise.y = gsl_ran_gaussian(m_r,m_sampleFactor.y*m_sigma);    
    accelNoise = gsl_ran_gaussian(m_r,accelVar);
        
    absVel = m_meanAbsVel + accelNoise;
    
    m_velNow[i].x =  absVel*cos(centerHeading[i]);
    m_velNow[i].y =  absVel*sin(centerHeading[i]);

    m_particlesPlus[i]=m_particlesNow[i]+m_velNow[i]*deltaT;//+m_Noise;
    
    m_ang[i] = atan(m_velNow[i].y/m_velNow[i].x);
    
    }

  m_particlesNow=m_particlesPlus;
  m_velNow=m_velPlus;
  return m_particlesPlus;
}

void ParticleFilter::weight(point2arr& projpts)
                            
{  //weighting Plus against the measurement; returns postWeights, takes preWeights, 
     double dist,distx,disty, logDist;
     double wSigma = 10,wSigmay=10,wSigmax=1;
          
   for(int i=0;i<m_N;i++)//calculating weights
     {   
       distx = projpts[i].x - m_particlesPlus[i].x;
       disty = projpts[i].y - m_particlesPlus[i].y;
       dist = pow(distx,2) + pow(disty,2);
       logDist = sqrt(dist);
       m_postWeights[i]=m_preWeights[i]*exp(-logDist/wSigma);
       m_postWeights[i]=m_preWeights[i]
                        *exp(-pow(distx,2)/wSigmax - pow(disty,2)/wSigmay);
     }
     
      double sum = 0;
   for(int i=0;i<m_N;i++)//calculating normalizer
     {
       sum=sum+m_postWeights[i];
     }
   
   for(int i=0;i<m_N;i++)//normalization
     {
       m_postWeights[i]=m_postWeights[i]/sum;
     }
}

void ParticleFilter::resample(int partitions)
{   //resampling using Neff.  returns preWeights either =post or =1/N
    //returns Now either = resampledPlus or just Plus
  
  double particleVar=0;
   for(int k=0; k<m_N;k++)
    {
     particleVar=particleVar+pow(m_postWeights[k],2);
    }
   double Neff=1/particleVar;
   cout<<"Neff"<<Neff<<endl;
 if(Neff<=m_resampleFactor*m_N)
  { //&&&&&&&&&&if
	//cout<<"resample"<<endl;
     double P[m_N];
   for(int i=0;i<m_N;i++)
    {
     P[i]=m_postWeights[i];
    } 
     
     //gsl initialization
     gsl_ran_discrete_t* g;
     size_t sampleIndex;
     g = gsl_ran_discrete_preproc(sizeof(m_N), P);
     
     //variance in gaussian sampling
     double sigmax = 0.2*m_sigma;
     double sigmay = 0.2*m_sigma;
   for(int i=0;i<m_N;i++)  //resampling loop
     {
      m_Noise.x = gsl_ran_gaussian(m_r,sigmax);
      m_Noise.y = gsl_ran_gaussian(m_r,sigmay); 
      
      sampleIndex = gsl_ran_discrete (m_r,g); //returns sampleIndex i with Prob=weight(i)
      m_particlesNow[i] = m_particlesPlus[sampleIndex];//+m_Noise; //new particles are born from old
      m_velNow[i]=m_velPlus[i];
      m_omegaNow[i]=m_omegaPlus[i];
     }
    
   for(int i=0;i<m_N;i++)//resetting the weights
     {
      m_preWeights[i]=1.0/m_N;
     }
  }//&&&&&&&&&if

 else //if resampling unneccesary, then everything is just as .predict said
  {
   //cout<<"don't resample"<<endl;
   m_preWeights=m_postWeights;
   m_particlesNow=m_particlesPlus;
   m_velNow=m_velPlus;
  }
}




















