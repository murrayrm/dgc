/*! ParticleFilter.hh
 * Pete Trautman
 * April 18 2007
 */

#ifndef PARTICLEFILTER_HH
#define PARTICLEFILTER_HH

// stf package includes
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
//run gsl-config --libs,--cflags tomorrow to see if its installed; then put 
//these variables in your makefile so that it will run
//#include <cv.h>
//#include <highgui.h>
//open cv stuff

// skynet, talker, and interfaces
#include "skynet/sn_msg.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/SkynetTalker.hh"
#include "map/MapElementTalker.hh"
#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"

// Alice std includes
#include "alice/AliceConstants.h"

// Other modules/def's
#include "map/Map.hh"
#include <cotk/cotk.h>
#include <ncurses.h>

/*! ParticleFilter class
 * This is the main class for map prediction where the information from 
 * mapping is predicted forward in time so that tplanner can make a decisions. This function inherits from StateClient and LocalMapTalker
 * \brief Main class for map prediction function  
 */ 
class ParticleFilter
{ 
public:

  /*! Contstructor */
  ParticleFilter();//int skynetKey, int debugLevel,int verboseLevel, bool log);
  
   /*! Standard destructor */
  ~ParticleFilter();

  //void kalmanFilter();

  void setN(int N);

  void initialize(point2arr& particlesPlus,point2arr& particlesNow,
       vector<double>& postWeights,vector<double>& preWeights,
       point2& dob1cpt,int modelType);

  void predict(point2arr& particlesPlus, point2arr& particlesNow);
 	//particleplus are the particles RETURNED after sampling;
	// particlesnow are the particles PASSED to the function
	
  void weight(vector<double>& postWeights, vector<double>& preWeights,
              point2arr& particlesPlus,
              vector<point2arr>& lbound,vector<point2arr>& rbound);
	//postweights are the weights RETURNED after being changed by the measurements;
	//preweights are the weights PASSED

  void resample(vector<double>& preWeights, vector<double>& postWeights,
                point2arr& particlesNow,point2arr& particlesPlus,double resampleFactor);
	//equalweights are (RETURNED) all the resampled weights so that they're all = 1/N;
	//postweights (PASSED) are the weights after measurement incorporation.
	//particlefinal is (RETURNED) is the particle set after resampling
	//particleplus is the (PASSED) particle set predicted forward.

private:
  
  const gsl_rng_type* m_T; //rng seed stuff
   
  gsl_rng* m_r; //rnd seed stuff

  double m_sigma; //random gaussian std

  int m_N; //the number of particles
  
  int m_modelType; //this is the sampling distribution being used.  Look in predict for what the types are
  
  /*!\param m_snKey is a skynet key that is set in MissionPlannerMain and
   * never changed. */
  int m_snKey;

  /*!\param m_debugLevel specifies level of debugging
   */
  bool m_debug;

  /*!\param m_verbose specifies level of debugging
   */
  bool m_verbose;

  /*!\param m_log indicates whether logging was enabled
   */
  bool m_log;
};

#endif  // PARTICLEFILTER_HH

