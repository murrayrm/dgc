/*! MapPrediction.hh
 * Pete Trautman
 * April 18 2007
 */

#ifndef MAPPREDICTION_HH
#define MAPPREDICTION_HH

// stf package includes
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>

// skynet, talker, and interfaces
#include "skynet/sn_msg.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/SkynetTalker.hh"
#include "map/MapElementTalker.hh"
#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"
#include "ParticleFilter.hh"

// Alice std includes
#include "alice/AliceConstants.h"

// Other modules/def's
#include "map/Map.hh"
#include <cotk/cotk.h>
#include <ncurses.h>

/*! MapPrediction class
 * This is the main class for map prediction where the information from 
 * mapping is predicted forward in time so that tplanner can make a decisions. This function inherits from StateClient and LocalMapTalker
 * \brief Main class for map prediction function  
 */ 
class CMapPrediction : public CStateClient, public CMapElementTalker
{ 
public:

  /*! Contstructor */
  CMapPrediction(int skynetKey, bool bWaitForStateFill,
                 int debugLevel, int verboseLevel, 
                 bool log);
  /*! Standard destructor */
  ~CMapPrediction();


  /////////////////////////////////////////////////////////////////////
  // Threads
  /////////////////////////////////////////////////////////////////////
  /*! this is the function that continually reads the latest object map*/
  void getLocalMapThread();

  /*! this is the function that continually reads dplanner status*/

  /////////////////////////////////////////////////////////////////////
  // Main planning loop
  /////////////////////////////////////////////////////////////////////
  /*! the main mapprediction loop*/
  void MapPredictionLoop(void);

  /////////////////////////////////////////////////////////////////////
  // initialize map from rndf file
  /////////////////////////////////////////////////////////////////////
  bool loadRNDF(string filename) 
  {
    bool loadedMap = m_localMap->loadRNDF(filename);
    //    localMap->loadRNDF(filename);
    return loadedMap;
  }


private:

  /*!\param m_snKey is a skynet key that is set in MissionPlannerMain and
   * never changed. */
  int m_snKey;
 
  /*!\param m_debugLevel specifies level of debugging
   */
  bool m_debug;

  /*!\param m_verbose specifies level of debugging
   */
  bool m_verbose;

  /*!\param m_log indicates whether logging was enabled
   */
  bool m_log;

  // Local Map
  Map * m_localMap;
  bool m_recvLocalMap;

  pthread_mutex_t m_LocalMapMutex;
};

#endif  // MAPPREDICTION_HH

