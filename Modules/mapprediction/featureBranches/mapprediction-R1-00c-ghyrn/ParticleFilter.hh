/*! ParticleFilter.hh
 * Pete Trautman
 * April 18 2007
 */

#ifndef PARTICLEFILTER_HH
#define PARTICLEFILTER_HH

// stf package includes
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
//run gsl-config --libs,--cflags tomorrow to see if its installed; then put 
//these variables in your makefile so that it will run
#include <cv.h>
#include <highgui.h>
#include <time.h>
//open cv stuff

// skynet, talker, and interfaces
#include "skynet/sn_msg.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/SkynetTalker.hh"
#include "map/MapElementTalker.hh"
#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"

// Alice std includes
#include "alice/AliceConstants.h"

// Other modules/def's
#include "map/Map.hh"
#include <cotk/cotk.h>
#include <ncurses.h>

/*! ParticleFilter class
 * This is the main class for map prediction where the information from 
 * mapping is predicted forward in time so that tplanner can make a decisions. This function inherits from StateClient and LocalMapTalker
 * \brief Main class for map prediction function  
 */ 
class ParticleFilter
{ 
public:

  /*! Contstructor */
   ParticleFilter(double resampleFactor,int N,
   point2& m_Cpt,point2& vel,int modelType,double sigma);
  
   /*! Standard destructor */
  ~ParticleFilter();

  void kalmanFilter();

  point2arr getParticlesNow();

  vector<double> getang();

  point2arr getVelNow();

  point2arr sampleConstVel(point2 sampleFactor,double deltaT);
 	//particleplus are the particles RETURNED after sampling;
	// particlesnow are the particles PASSED to the function
	
  point2arr sampleCoordTurn(point2 sampleFactor,double deltaT);

  void weight(vector<point2arr>& lbound,vector<point2arr>& rbound);     
         //postweights are the weights RETURNED after being changed by the measurements;
	//preweights are the weights PASSED

  void resample(int partitions);
	//equalweights are (RETURNED) all the resampled weights so that they're all = 1/N;
	//postweights (PASSED) are the weights after measurement incorporation.
	//particlefinal is (RETURNED) is the particle set after resampling
	//particleplus is the (PASSED) particle set predicted forward.

private:

  vector<double> m_ang;

  vector<double> m_omegaPlus;

  vector<double> m_omegaNow;  
	
  point2 m_sampleFactor;

  point2arr m_velPlus;

  point2arr m_velNow;

  point2arr m_particlesPlus;
  
  point2arr m_particlesNow;
  
  vector<double> m_postWeights;
  
  vector<double> m_preWeights;
  
  point2 m_Cpt;

  point2 m_Noise;
  
  double m_resampleFactor;
 
  int m_N; //the number of particles
  
  const gsl_rng_type* m_T; //rng seed stuff
   
  gsl_rng* m_r; //rnd seed stuff

  double m_sigma; //random gaussian std

  int m_modelType; //this is the sampling distribution being used.  Look in sample for what the types are
  
};

#endif  // PARTICLEFILTER_HH

