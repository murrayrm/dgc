              Release Notes for "radarfeeder" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "radarfeeder" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "radarfeeder" module can be found in
the ChangeLog file.

Release R1-00l-tamas (Sun Sep  2 14:09:36 2007):
	Now ignores radar data when yaw/pan rates are too large as well. Also, fixed PTU radar config file, because it hadn't been changed
to have the correct radar position, and was instead off by a good 4 meters or so (oops). This probably caused the bad radar returns yesterday.

Release R1-00l (Thu Aug 30 15:02:02 2007):
	Added code to ignore PTU data if we're moving too fast.

Release R1-00k (Thu Aug 30 14:47:11 2007):
	Added SENSNET_PTU_RADAR.CFG to the Makefile ETC links.

Release R1-00j (Tue Aug 28 14:23:01 2007):
	Fixed PTU radar transform code.

Release R1-00i (Tue Aug 21 21:22:43 2007):
	Health status messages now get sent more often so process 
control doesn't try to restart the feeder.

Release R1-00h (Mon Aug  6 22:30:44 2007):
	one line change, to make radarfeeder adhere to general convention
	of using moduleId rather than sensorId when connecting to spread

	This was tested on Alice this evening


Release R1-00g (Wed Jul 25 13:04:26 2007):
	Changed the way object status is reported so that stationary 
objects (as flagged by the radar) can be properly filtered out by the 
perceptor; added sleeps to main loop if driver can't connect.

Release R1-00f (Thu Jul 19 18:43:47 2007):
	Updated indexing code again, made sure old tracks are deleted. 
Added basic transform code for future PTU radar. Added health monitoring 
status messages.

Release R1-00e (Tue Jul 17 16:34:02 2007):
	Fixed the handling of status messages and of track IDs, and (in 
theory) added code to support a PTU radar (SENSNET_PTU_RADAR)

Release R1-00d (Thu Jul 12 23:28:52 2007):
  Fixed a small bug in sending of negative yaw rate and velocity values, where the radar expects an absolute value integer with a single bit flipped 
for sign (unlike actual ints). Also created diagnostic utility to read and write radar parameter values, now I just need the parameters.

Release R1-00c (Mon May  7 18:32:01 2007):
  Fixed the yaw-rate data byte that's being sent such that an appropriate bit is set to "valid". Minor fix. 

Release R1-00b (Fri Apr 27 22:37:25 2007):
  Tested, more-or-less working version of radarfeeder.

Release R1-00a (Wed Apr 25 10:49:00 2007):
	This is the first cut at the radarfeeder. It's essentially a trimmed down version of ladarfeeder with some slight 
modifications here and there. It compiles and works fine in the lab and is ready to use on Alice. The command line options 
are very similar to ladarfeeder with the added option of specifying the number of objects you desire to track. The radar 
is capable of detecting up to 20 tracks (the reason I made this optional is because if you request more tracks to 
detect than there are actual number of objects, the results can be noisy). There is also a config file that can be used to 
specify the location of the radar relative to the vehicle and also the number of tracks. 

Release R1-00 (Tue Apr 24 10:40:24 2007):
	Created.













