#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <GL/glut.h>
#include <sys/types.h>
#include <string.h>
#include <pthread.h>
#include <vector>
#include <string>
#include <stdio.h>
#include <ntcan/ntcan.h>
#include "radar_driver.hh"

using namespace std;

//OpenGL variables
float lastx, lasty;
bool lbuttondown = false;
bool rbuttondown = false;
int32_t txhandle;
int32_t rxhandle;


#define XMIN  		-25
#define XMAX   		 50
#define YMIN  		-20
#define YMAX   		 20

const int WINDOW_WIDTH=(XMAX-XMIN)*20;
const int WINDOW_HEIGHT=(YMAX-YMIN)*20;

#define MAX_MESSLEN     102400

struct point2D
{
  double x;
  double y;
};

//==================
// GLOBAL VARIABLES
// =================
RadarObject mov_objects[ALLOWABLE_NUM_TARGETS];
Radar *driver = new Radar(); 
point2D state;

void drawString (char *s)
{
  unsigned int i;
  for (i = 0; i < strlen (s); i++)
    glutBitmapCharacter (GLUT_BITMAP_HELVETICA_12, s[i]);
}

void drawCircle(double x_c, double y_c, double R)
{
  uint NUM_PTS;

  if(R>0.25)
    NUM_PTS = 100;
  else
    NUM_PTS = 50;

  double theta_tmp, x_tmp, y_tmp;
  glLineWidth(1.5);
  glColor3f(1.0,0.0,0.0);
  glBegin(GL_LINE_LOOP);
  for(uint i=0; i<NUM_PTS; i++)
    {
      theta_tmp =(double)i/NUM_PTS*2*M_PI;
      x_tmp = R*cos(theta_tmp)+x_c;
      y_tmp = R*sin(theta_tmp)+y_c;
      glVertex2f(x_tmp,y_tmp);
    }
  glEnd();

}

void display(void)
{
  static char label[100];

  glClearColor (0.0,0.0,0.0,1.0); //clear the screen to black
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //clear the color buffer and the depth buffer


  //====================
  /* draw the floor */
  //====================
  glBegin(GL_QUADS);
  glColor3f(0.0f, 0.0f, 0.0f);
  glVertex2f(XMIN, YMIN);
  glVertex2f(XMIN, YMAX);
  glVertex2f(XMAX, YMAX);
  glVertex2f(XMAX, YMIN);		
  glEnd();
  
  
  //====================
  //draw coordinate axes
  //====================
  glLineWidth (4);
  glColor3f (0.0F, 0.0F, 1.0F);
  glBegin(GL_LINES);
  glVertex2f(0,0);
  glVertex2f(XMAX,0);
  glVertex2f(0,YMAX);
  glVertex2f(0,YMIN);
  glEnd();


  //====================
  //draw fine grid
  //====================
  //horizontal lines
  glLineWidth (0.5);
  for (int i= YMIN; i < YMAX+1; i++)
    {
      glBegin (GL_LINES);
      glColor3f (0.25F, 0.25F, 0.25F);
      glVertex2f ( 0, i);
      glVertex2f ( XMAX, i);
      glEnd ();

      sprintf(label, "%d", i);
      glColor3f (0.0F, 1.0F, 0.0F);
      glRasterPos2f(-0.20,i+0.05);
      drawString(label);  

    };

  //vertical lines
  glLineWidth (0.5);
  for (int i= 0; i < XMAX+1; i++)
    {
      glBegin (GL_LINES);
      glColor3f (0.25F, 0.25F, 0.25F);
      glVertex2f (i, YMIN);
      glVertex2f (i, YMAX);
      glEnd ();

      sprintf(label, "%d", i);
      glColor3f (0.0F, 1.0F, 0.0F);
      glRasterPos2f(i+0.05,-0.20);
      drawString(label);  
    };


  //====================
  //draw field of view
  //====================
  glLineWidth (0.5);
  glBegin(GL_LINES);
  glColor3f(1.0f,1.0f,0.0f);
  glVertex2f(0.0f,0.0f);
  glVertex2f(XMAX, (XMAX/cos(-6.35*M_PI/180))*sin(-6.35*M_PI/180));
  glEnd();

  glBegin(GL_LINES);
  glColor3f(1.0f,1.0f,0.0f);
  glVertex2f(0.0f,0.0f);
  glVertex2f(XMAX, (XMAX/cos(6.4*M_PI/180))*sin(6.4*M_PI/180));
  glEnd();

  //====================
  //draw robot pose
  //====================
  // FILL STATE VALUES HERE
  state.x = 0; 
  state.y = 0;

  glColor3f(1.0f, 1.0f, 1.0f);
  glPointSize(10);
  glBegin(GL_POINTS);
  glVertex2f(state.x, state.y);
  glEnd();


  //====================
  //draw chart box
  //====================
  //vertical lines
  glColor3f (0.0F, 1.0F, 0.0F);
  glLineWidth (0.5);
  glBegin(GL_LINES);
  glVertex2f (XMIN+0.5, YMIN+6);
  glVertex2f (XMIN+0.5, YMAX-3);
  glEnd();

  glColor3f (0.0F, 1.0F, 0.0F);
  glLineWidth (0.5);
  glBegin(GL_LINES);
  glVertex2f (XMIN+3.5, YMIN+6);
  glVertex2f (XMIN+3.5, YMAX-3);
  glEnd();
  
  glLineWidth (0.5);
  glBegin(GL_LINES);
  glVertex2f (XMIN+6.75, YMIN+6);
  glVertex2f (XMIN+6.75, YMAX-3);
  glEnd();

  glLineWidth (0.5);
  glBegin(GL_LINES);
  glVertex2f (XMIN+12, YMIN+6);
  glVertex2f (XMIN+12, YMAX-3);
  glEnd();

  glLineWidth (0.5);
  glBegin(GL_LINES);
  glVertex2f (XMIN+17, YMIN+6);
  glVertex2f (XMIN+17, YMAX-3);
  glEnd();

  glLineWidth (0.5);
  glBegin(GL_LINES);
  glVertex2f (XMIN+21, YMIN+6);
  glVertex2f (XMIN+21, YMAX-3);
  glEnd();

  //horizontal lines
  glLineWidth (0.5);
  double x_level;
  double step = 1.5;
  for (int i= 0; i < 21; i++)
    {
      x_level = YMIN+6+i*step;
      glBegin (GL_LINES);
      glVertex2f (XMIN+0.5, x_level);
      glVertex2f (XMIN+21, x_level);
      glEnd ();
    };

  //====================
  //print radar data
  //====================
  for (int i= 0; i < ALLOWABLE_NUM_TARGETS; i++)
    {     
      sprintf(label, "%s %d","trk:",i); 
      glColor3f (0.0F, 1.0F, 0.0F);
      glRasterPos2f(XMIN+0.75,(YMAX-5)-i*1.5);
      drawString(label);  
    };

  sprintf(label, "%s","track No." );
  glColor3f (0.0F, 1.0F, 0.0F);
  glRasterPos2f(XMIN+3.75,(YMAX-3.75));
  drawString(label);  

  sprintf(label, "%s","velocity" );
  glColor3f (0.0F, 1.0F, 0.0F);
  glRasterPos2f(XMIN+7.75,(YMAX-3.75));
  drawString(label);  

  sprintf(label, "%s","range" );
  glColor3f (0.0F, 1.0F, 0.0F);
  glRasterPos2f(XMIN+13.5,(YMAX-3.75));
  drawString(label);  

  sprintf(label, "%s","yaw" );
  glColor3f (0.0F, 1.0F, 0.0F);
  glRasterPos2f(XMIN+18.5,(YMAX-3.75));
  drawString(label);  

  double value;
  int value_int;
  
  point2D pose;

  for (int i= 0; i < ALLOWABLE_NUM_TARGETS; i++)
    {     
      
      if(mov_objects[i].status==1)
	{
	  mov_objects[i].status=0;

	  pose.x = mov_objects[i].range*cos(mov_objects[i].yaw);
	  pose.y = mov_objects[i].range*sin(mov_objects[i].yaw);

	  //draw radar data
	  drawCircle(pose.x,pose.y,0.25);
	  drawCircle(pose.x,pose.y,0.05);	 

	  //fill track ID data
	  value_int = i;
	  sprintf(label, "%d",value_int); 
	  glColor3f (0.0F, 1.0F, 0.0F);
	  glRasterPos2f(XMIN+3.75,(YMAX-5)-i*1.5);
	  drawString(label);  

	  //fill velocity data
	  value = mov_objects[i].velocity;
	  sprintf(label, "%3.3f",value); 
	  glColor3f (0.0F, 1.0F, 0.0F);
	  glRasterPos2f(XMIN+7.50,(YMAX-5)-i*1.5);
	  drawString(label); 

	  //fill range data
	  value = mov_objects[i].range;
	  sprintf(label, "%3.3f",value); 
	  glColor3f (0.0F, 1.0F, 0.0F);
	  glRasterPos2f(XMIN+13.25,(YMAX-5)-i*1.5);
	  drawString(label);  

	  //fill yaw data
	  value = mov_objects[i].yaw*180/M_PI;
	  sprintf(label, "%3.3f",value); 
	  glColor3f (0.0F, 1.0F, 0.0F);
	  glRasterPos2f(XMIN+18.00,(YMAX-5)-i*1.5);
	  drawString(label);  

	}      
      else
	{
	  //fill track ID data
	  sprintf(label, "%s","NULL"); 
	  glColor3f (0.0F, 1.0F, 0.0F);
	  glRasterPos2f(XMIN+3.75,(YMAX-5)-i*1.5);
	  drawString(label);  
	  
	  //fill velocity data
	  sprintf(label, "%s","NULL"); 
	  glColor3f (0.0F, 1.0F, 0.0F);
	  glRasterPos2f(XMIN+7.50,(YMAX-5)-i*1.5);
	  drawString(label); 
	  
	  //fill range data
	  sprintf(label, "%s","NULL"); 
	  glColor3f (0.0F, 1.0F, 0.0F);
	  glRasterPos2f(XMIN+13.25,(YMAX-5)-i*1.5);
	  drawString(label);  
	  
	  //fill yaw data
	  sprintf(label, "%s","NULL"); 
	  glColor3f (0.0F, 1.0F, 0.0F);
	  glRasterPos2f(XMIN+18.00,(YMAX-5)-i*1.5);
	  drawString(label); 
	}

    };


  glutSwapBuffers();
  
}

void resize(int w, int h)
{
  glLoadIdentity();
  glViewport (0, 0, w, h); //set the viewport to the current window specifications
  glMatrixMode(GL_PROJECTION);
  gluOrtho2D(XMIN,XMAX,YMIN,YMAX);
}

void mouse(int button, int state, int x, int y)
{

  if(button == GLUT_LEFT_BUTTON)
    {
      if(state == GLUT_DOWN)
	{
	  lbuttondown = true;
	}
      else
	{
	  lbuttondown = false;
	}
    }

  if(button == GLUT_RIGHT_BUTTON)
    {
      if(state == GLUT_DOWN)
	{
	  rbuttondown = true;
	}
      else
	{
	  rbuttondown = false;
	}
    }
  

}

void motion(int x, int y)
{
  if (lbuttondown)
    {
      float diffx=(x-lastx)/70; //check the difference between the current x and the last x position  
      float diffy=(y-lasty)/70;

      lastx = x;
      lasty = y;

      glTranslatef(diffx, -diffy, 0);

      glutPostRedisplay();     
    }

  if (rbuttondown)
    {     

      GLint _viewport[4];
      GLdouble _modelMatrix[16];
      GLdouble _projMatrix[16];
      GLdouble objx, objy, objz;

      glGetIntegerv(GL_VIEWPORT,_viewport);
      glGetDoublev(GL_PROJECTION_MATRIX, _projMatrix);
      glGetDoublev(GL_MODELVIEW_MATRIX, _modelMatrix);

      gluUnProject(WINDOW_WIDTH/2,WINDOW_HEIGHT/2,0,_modelMatrix,_projMatrix,_viewport,&objx, &objy, &objz);

      float dy=(y-lasty); //check the difference between the current y and the last y position  

      double s = exp((double)dy*0.01);

      glTranslatef(objx,objy,0);
      glScalef(s,s,s);
      glTranslatef(-objx,-objy,0);

      lasty = y;
      lastx = x;

      glutPostRedisplay();  



    }
  
}


void motionPassive(int x, int y)
{
  lastx = x;
  lasty = y;
}

void idle (void)
{
  int retvalue;

  // grab tracks
  retvalue = driver->captureTracks();
  if(retvalue==-1)
    {
      printf("getTracks failed. Abort.\n");      
    }           
  
  // extract data from captured tracks
  for(int i=0; i<ALLOWABLE_NUM_TARGETS;i++)
    {
      mov_objects[i].velocity = driver->getTrackVelocity(i);
      mov_objects[i].range = driver->getTrackRange(i);
      mov_objects[i].yaw = driver->getTrackYaw(i);
      mov_objects[i].credibility = driver->getTrackCredibility(i);
      mov_objects[i].status = driver->getTrackStatus(i);
    }  

  // send state
  retvalue = driver->sendState(0,0);
  if(retvalue==-1)
    {
      printf("sendState failed. Abort.\n");      
    }            
  
  glutPostRedisplay();
  
}

int main(int argc, char **argv)
{  
  int retvalue;

  memset(mov_objects,0,sizeof(mov_objects));

  // Initialize radar 
  retvalue = driver->initRadar();  
  if(retvalue==-1) 
    { 
      printf("could not initialize radar. Abort.\n"); 
      return(-1);
    }

  // Set number of tracks to be 4
  driver->setNumTracks(1);
  printf("set the number of tracks OK\n");
  
  // Start send track request
  retvalue = driver->sendTrackRequest();
  if(retvalue==-1)
    {
      printf("could not send track request. Abort.\n");
      return(-1);
    }
  printf("sent track request OK\n");
  
  // Send initial state
  retvalue = driver->sendState(0,0);
  if(retvalue==-1)
    {
      printf("sendState failed. Abort.\n");
      return(-1);
    }
  printf("sent state information OK\n");

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
  glutInitWindowPosition(40, 40);
  glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
  glutCreateWindow("radar Viewer");
  
  glClearColor(0.0, 0.0, 0.0, 0.0); //black bkgrnd
  glColor3f(1.0, 1.0, 1.0);
  
  glutDisplayFunc(display); 

  glutMouseFunc(mouse);
  glutMotionFunc(motion);
  glutPassiveMotionFunc(motionPassive);
  
  glutReshapeFunc(resize);
  glutIdleFunc(idle);
  
  glutMainLoop();
  
  return 0;
  
}
