#include "radar_driver.hh"

using namespace std;

// Default constructor
Radar::Radar()
{

  memset(this, 0, sizeof(this));

  // ----------------------------------------------
  // Initialize private variables to default values
  // ----------------------------------------------
  numTracks = 5;
  numTargets = 5;
  
  baud = 2;

  net=0;                  /* logical netnumber */
  mode=0;                 /* mode for canOpen */
  txqueuesize=40;         /* maximum number of messages to transmit = 40 since 20 moving objects and 2 frames/object */
  rxqueuesize=40;         /* maximum number of messages to receive  = 40 since 20 moving objects and 2 frames/object */
  txtimeout=100;          /* timeout for transmit */
  rxtimeout=10000;        /* timeout for receiving data */  

  return;
}

// Destructor
Radar::~Radar()
{
  canClose(txhandle);
  canClose(rxhandle_targets);
  canClose(rxhandle_tracks);
  return;
}

// Initialize the radar
int Radar::initRadar()
{
  int retvalue;
  
  // ------------------------------------
  // Open CAN handle for sending messages
  // ------------------------------------
  retvalue = canOpen(net,
		     mode,
		     txqueuesize,
		     rxqueuesize,
		     txtimeout,
		     rxtimeout,
		     &txhandle);
  
  if(retvalue!=NTCAN_SUCCESS)
    {      
      printf("canOpen() transmit handle failed with error %d!\n", retvalue);
      return(-1);
    }
  else    
    printf("opened transmit handle OK !\n");
  
  // ------------------------
  // Set Baudrate for sending
  // ------------------------
  retvalue = canSetBaudrate(txhandle,baud);
  if(retvalue!=0)
    {
      printf("canSetBaudrate() failed with error %d!\n", retvalue);
      canClose(txhandle);
      return(-1);
    }
  else    
    printf("function canSetBaudrate() returned OK ! \n");
    


  /// ----------------------------------
  /// Open CAN handle for reading tracks
  /// ----------------------------------
  retvalue = canOpen(net,
		     mode,
		     txqueuesize,
		     rxqueuesize,
		     txtimeout,
		     rxtimeout,
		     &rxhandle_tracks);

  if(retvalue!=NTCAN_SUCCESS)
    {      
      printf("canOpen() track handle failed with error %d!\n", retvalue);
      return(-1);
    }
  else
    printf("opened track handle OK !\n");

  // -------------------------------
  // Add handle ID for track message
  // -------------------------------
  retvalue = canIdAdd(rxhandle_tracks,0x663);
  if(retvalue!=NTCAN_SUCCESS)
    {
      printf("canIdAdd() failed with error %d!\n", retvalue);
      printf(" could not add handle ID for general status message.\n");
      canClose(rxhandle_tracks);
      return(-1);
    }
 
  /// ------------------------------------------
  /// Open CAN handle for reading target message
  /// ------------------------------------------
  retvalue = canOpen(net,
		     mode,
		     txqueuesize,
		     rxqueuesize,
		     txtimeout,
		     rxtimeout,
		     &rxhandle_targets);

  if(retvalue!=NTCAN_SUCCESS)
    {      
      printf("canOpen() target handle failed with error %d!\n", retvalue);
      return(-1);
    }
  else
    printf("opened target handle OK !\n");

  // --------------------------------
  // Add handle ID for target message
  // --------------------------------
  retvalue = canIdAdd(rxhandle_targets,0x662);
  if(retvalue!=NTCAN_SUCCESS)
    {
      printf("canIdAdd() failed with error %d!\n", retvalue);
      printf(" could not add handle ID for general status message.\n");
      canClose(rxhandle_targets);
      return(-1);
    }

  return(0);

}

/// ---------------------------------
/// Set the number of tracks to track
/// ---------------------------------
void Radar::setNumTracks(uint8_t trackTotal)
{
  numTracks = trackTotal;
}


/// ---------------------------------
/// Set the number of targets to track
/// ---------------------------------
void Radar::setNumTargets(uint8_t targetTotal)
{
  numTargets = targetTotal;
}

/// --------------------------
/// Send State to radar
/// --------------------------
int Radar::sendState(double speed, double yawrate)
{
  //wheelspeed given in m/s but must be coded as km/hr
  //yawrate given in rad/s but must be coded as degrees/sec

  double newspeed = speed*3.6; //km/hr
  double newyawrate = yawrate*180/M_PI; //deg/s

  /* NOTE: if the radar return seems rather strange, it may be 
   * because of the ordering of the bits; the documentation 
   * seems rather ambiguous about which bit numbering scheme
   * is used. Right now, the 0th bit is assumed as the LSb and
   * as such, then the 16th bit which holds the sign of the 
   * number must be moved to the 0th bit position to be in 
   * accordance with the documentation
   */
  int16_t sendspeed = (int16_t)(newspeed/0.01); //conversion factor
  int16_t tmpspeed = 0;
  int16_t lastbit = 0;

  // do a cyclic shift for the speed
  tmpspeed = tmpspeed | sendspeed;
  tmpspeed = tmpspeed << 1;

  lastbit = tmpspeed | sendspeed;
  lastbit = lastbit >> 15;

  sendspeed = 0;
  sendspeed = tmpspeed + lastbit;

  uint8_t MSB_speed = (uint8_t)( (sendspeed >> 8) & 0xFF);
  uint8_t LSB_speed = (uint8_t)( (sendspeed & 0xFF) );

  int len, retvalue;                 /* Bufsize in number of messages for canRead() */
  CMSG wheelSpeed, yawSpeed;              /* can message buffer */

  //-----------------------
  //send wheel speed
  //-----------------------
  wheelSpeed.id=0x4A0;
  wheelSpeed.len=0x08;

  // next two bytes deal with FL wheel speed
  wheelSpeed.data[0] = 0x00; //LSB_speed;
  wheelSpeed.data[1] = 0x00; //MSB_speed; 

  // next two bytes deal with FR wheel speed
  wheelSpeed.data[2] = 0x00; //LSB_speed;
  wheelSpeed.data[3] = 0x00; //MSB_speed;

  // next two bytes deal with RL wheel speed
  wheelSpeed.data[4] = 0x00; //LSB_speed;
  wheelSpeed.data[5] = 0x00; //MSB_speed;

  // next two bytes deal with RR wheel speed
  wheelSpeed.data[6] = 0x00; //LSB_speed;
  wheelSpeed.data[7] = 0x00; //MSB_speed;
  
  len = 1;
  
  retvalue = canSend(txhandle,&wheelSpeed,&len);
  if(retvalue != NTCAN_SUCCESS)
    {
      printf("ERROR: unable to send wheel speed!!\n");  
      return(-1);   
    }

  // now we store the yawrate
  int16_t sendyaw = (int16_t)(newyawrate/0.01); // conversion factor
  uint8_t MSB_yaw = (uint8_t)( (sendyaw >> 8) & 0xFF);
  uint8_t LSB_yaw = (uint8_t)( (sendyaw & 0xFF) );

  //-----------------------
  //send yaw rate
  //-----------------------
  yawSpeed.id=0x4A8;
  yawSpeed.len=0x08;

  // next two bytes deal with the yawrate
  yawSpeed.data[0] = 0x00; //LSB_yaw; 
  yawSpeed.data[1] = 0x00; //MSB_yaw;
  
  // next 4 bytes are not used
  yawSpeed.data[2] = 0x00;
  yawSpeed.data[3] = 0x00;
  yawSpeed.data[4] = 0x00;
  yawSpeed.data[5] = 0x00;

  // next byte deals with counter for overflowing message counter
  // as sign of life
  yawSpeed.data[6] = 0x00;

  // last byte is checksum
  yawSpeed.data[7] = 0x00;

  len = 1;
  
  retvalue = canSend(txhandle,&yawSpeed,&len);
  if(retvalue != NTCAN_SUCCESS)
    {
      printf("ERROR: unable to send yaw rate!!\n");
      return(-1);
    }

  return(0);

}

//-----------------------------
// Send request for tracks
//-----------------------------
int Radar::sendTrackRequest()
{
  CMSG command;
  int len; 
  int retvalue;

  // --------------------------
  // Send command for continuous broadcast
  // --------------------------
  command.id=0x660; //ID for tracks command
  command.len=0x08; //length of command is 8 bytes
  command.data[0] = 0x07; // command to request tracks (0x01 is targets; 0x07 is tracks)
  command.data[1] = numTracks; // number of targets to detect; 0x14 is max
  command.data[2] = 0x00; // normally set to 0
  command.data[3] = 0x00; // normally set to 0; 1 if requesting extended status message
  command.data[4] = 0x00; // normally set to 0; 1 if requestion extended target message
  command.data[5] = 0x00; // not used
  command.data[6] = 0xFF; // continuous sending of this message 0xFF
  command.data[7] = 0x00; // not used
  
  len = 1;
  
  retvalue = canSend( txhandle,
		      &command,
		      &len);
  
  if (retvalue != NTCAN_SUCCESS)
    {
      printf("ERROR: canSend failed() with error %d!\n", retvalue);
      return(-1);
    }
  

  return(0);
}


//-----------------------------
// Send request for targets
//-----------------------------
int Radar::sendTargetRequest()
{
  CMSG command;
  int len; 
  int retvalue;

  // --------------------------
  // Send command for continuous broadcast
  // --------------------------
  command.id=0x660; //ID for tracks command
  command.len=0x08; //length of command is 8 bytes
  command.data[0] = 0x01; // command to request tracks (0x01 is targets; 0x07 is tracks)
  command.data[1] = numTargets; // number of targets to detect; 0x14 is max
  command.data[2] = 0x00; // normally set to 0
  command.data[3] = 0x00; // normally set to 0; 1 if requesting extended status message
  command.data[4] = 0x00; // normally set to 0; 1 if requestion extended target message
  command.data[5] = 0x00; // not used
  command.data[6] = 0xFF; // continuous sending of this message 0xFF
  command.data[7] = 0x00; // not used
  
  len = 1;
  
  retvalue = canSend( txhandle,
		      &command,
		      &len);
  
  if (retvalue != NTCAN_SUCCESS)
    {
      printf("ERROR: canSend failed() with error %d!\n", retvalue);
      return(-1);
    }
  

  return(0);
}


//-----------------------------
// Capture radar track information
//-----------------------------
int Radar::captureTracks()
{
  CMSG cmsg[40];

  int numFramesExpected = (int)( 2*numTracks );
  int numFramesGiven = numFramesExpected; //for upper limit
  int frameCounter=0;
  int retvalue;

  bool foundStart = false;
  
  char tmpByte;
  int tmpFrameNumber;
  uint tmpTrackNumber;

  uint8_t j;
  uint16_t rawVel = 0;
  int16_t rawVel_int = 0;
  double realVel = 0;
  uint temp_dist1, temp_dist2;
  uint rawDist;
  double realDist;

  double realYaw;

  int credibility;

  int trackId;

  //this loop is so that we continually read off the CAN until our buffer
  //is filled with the right number of frames corresponding to the number 
  //of tracks we're expecting
  while(true)
    {  
      retvalue = canRead(rxhandle_tracks, &cmsg[0],&numFramesGiven,NULL);
      if(retvalue==NTCAN_RX_TIMEOUT)
	{
	  printf("canRead() returned timeout\n");
	  printf("is the radar on?\n");
	  return(-1);
	}
      else if(retvalue!=NTCAN_SUCCESS)
	{
	  printf("canRead() failed with error %d!\n", retvalue);
	  return(-1);
	}
     
      tmpByte = cmsg[0].data[0];
      tmpFrameNumber = (int)( (tmpByte)&0x07 );
      tmpTrackNumber = (uint)( (tmpByte)>> 3 );
      
      //the start of the message will have frameNumber=4 & trackNumber=0
      if(tmpFrameNumber==4 && tmpTrackNumber==0)
	foundStart=true;	

      if(foundStart==true)
	{	  
	  for(int k=0; k<numFramesGiven; k++) // len is length of frames received
	    {
	      for(int i=0; i<(int)(cmsg[k].len & 0x0f); i++) // each frame has 8 bytes
		{
		  frame_buffer[frameCounter][i] = cmsg[k].data[i];		  
		}	      

	      frameCounter++;	      

	      if(frameCounter==numFramesExpected)				 
		  break;		

	    }
	}
	 
      if(frameCounter==numFramesExpected)       
	break;
     
    }

  //now that frame buffer is full and ordered, let's extract the data    
  for(uint8_t m=0; m<numTracks; m++)
    {
      j = m*2;      
      
      // trackId can be either the assigned order number or a unique identifier
      trackId = (uint)((uint8_t) ((frame_buffer[j+1][5])&0x1F)); //unique identifier

      // extract velocity
      rawVel = rawVel | frame_buffer[j][2];	  
      rawVel = rawVel << 8; 
      rawVel = rawVel | frame_buffer[j][3];
      rawVel_int = (int16_t)rawVel;
      realVel = rawVel_int*0.01f;
         
      // extract range
      temp_dist1 =  (uint)((uint8_t)frame_buffer[j][4]);
      temp_dist2 =  (uint)((uint8_t)frame_buffer[j][5]);      
      rawDist = temp_dist1*256+temp_dist2;      
      realDist = rawDist*0.01; 
      
      // extract yaw
      realYaw = (signed char)frame_buffer[j+1][4]*0.05f;

      
      credibility = (int)( ((frame_buffer[j][1])&0xFC)>>2 );

      tracks[trackId].velocity =  realVel;
      tracks[trackId].range = realDist;
      tracks[trackId].yaw = realYaw*M_PI/180;	           
      tracks[trackId].credibility = credibility;
      tracks[trackId].status = 1;
	
    }


  return(0);
}

//-----------------------------
// Capture radar target information
//-----------------------------
int Radar::captureTargets()
{

  CMSG cmsg[40];

  int numFramesExpected = (int)( 2*numTargets );
  int numFramesGiven = numFramesExpected; //for upper limit
  int frameCounter=0;
  int retvalue;

  bool foundStart = false;
  
  char tmpByte;
  int tmpFrameNumber;
  uint tmpTargetNumber;

  uint8_t j;
  uint16_t rawVel = 0;
  int16_t rawVel_int = 0;
  double realVel = 0;
  uint temp_dist1, temp_dist2;
  uint rawDist;
  double realDist;

  double realYaw;

  //this loop is so that we continually read off the CAN until our buffer
  //is filled with the right number of frames corresponding to the number 
  //of targets we're expecting
  while(true)
    {  
      retvalue = canRead(rxhandle_targets, &cmsg[0],&numFramesGiven,NULL);
      if(retvalue==NTCAN_RX_TIMEOUT)
	{
	  printf("canRead() returned timeout\n");
	  printf("is the radar on?\n");
	  return(-1);
	}
      else if(retvalue!=NTCAN_SUCCESS)
	{
	  printf("canRead() failed with error %d!\n", retvalue);
	  return(-1);
	}
     
      tmpByte = cmsg[0].data[0];
      tmpFrameNumber = (int)( (tmpByte)&0x07 );
      tmpTargetNumber = (uint)( (tmpByte)>> 3 );
      
      //the start of the message will have frameNumber=4 & targetNumber=0
      if(tmpFrameNumber==4 && tmpTargetNumber==0)
	foundStart=true;	

      if(foundStart==true)
	{	  
	  for(int k=0; k<numFramesGiven; k++) // len is length of frames received
	    {
	      for(int i=0; i<(int)(cmsg[k].len & 0x0f); i++) // each frame has 8 bytes
		{
		  frame_buffer[frameCounter][i] = cmsg[k].data[i];		  
		}	      

	      frameCounter++;	      

	      if(frameCounter==numFramesExpected)
		{
		  printf("filled buffer!!\n");
		  break;
		}

	    }
	}
	 
      if(frameCounter==numFramesExpected)
	{
	  printf("filled buffer!!\n");
	  break;
	}	
    }

  //now that frame buffer is full and ordered, let's extract the data    
  for(uint8_t m=0; m<numTargets; m++)
    {
      j = m*2;      

      // there is a second frame in j+1 but there is no important info there

      // extract velocity
      rawVel = rawVel | frame_buffer[j][2];	  
      rawVel = rawVel << 8; 
      rawVel = rawVel | frame_buffer[j][3];
      rawVel_int = (int16_t)rawVel;
      realVel = rawVel_int*0.01f;
         
      // extract range
      temp_dist1 =  (uint)((uint8_t)frame_buffer[j][4]);
      temp_dist2 =  (uint)((uint8_t)frame_buffer[j][5]);      
      rawDist = temp_dist1*256+temp_dist2;      
      realDist = rawDist*0.01;       
      
      // extract yaw
      realYaw = (signed char)frame_buffer[j][1]*0.05f;
      
      targets[m].velocity =  realVel;
      targets[m].range = realDist;
      targets[m].yaw = realYaw*M_PI/180;	           
      targets[m].status = 1;
	
    }

  return(0);
}

// -----------------------
// Get i-th track velocity
// -----------------------
double Radar::getTrackVelocity(int id)
{
  return(tracks[id].velocity);
}

// -----------------------
// Get i-th track range
// -----------------------
double Radar::getTrackRange(int id)
{
  return(tracks[id].range);
}

// -----------------------
// Get i-th track yaw
// -----------------------
double Radar::getTrackYaw(int id)
{
  return(tracks[id].yaw);
}

// --------------------------
// Get i-th track credibility
// --------------------------
int Radar::getTrackCredibility(int id)
{
  return(tracks[id].credibility);
}

// -----------------------
// Get i-th track status
// -----------------------
int Radar::getTrackStatus(int id)
{
  return(tracks[id].status);
}

// -----------------------
// Get i-th target velocity
// -----------------------
double Radar::getTargetVelocity(int i)
{
  return(targets[i].velocity);
}

// -----------------------
// Get i-th target range
// -----------------------
double Radar::getTargetRange(int i)
{
  return(targets[i].velocity);
}

// -----------------------
// Get i-th target yaw
// -----------------------
double Radar::getTargetYaw(int i)
{
  return(targets[i].yaw);
}

// -----------------------
// Get i-th target status
// -----------------------
int Radar::getTargetStatus(int i)
{
  return(targets[i].status);
}
// -----------------------
// Close all handles 
// -----------------------
int Radar::disconnect()
{
  canClose(txhandle);
  canClose(rxhandle_targets);
  canClose(rxhandle_tracks);
  return(0);
}



#if RADAR_DRIVER_TEST

int main(int argc, char *argv[])
{

  int retvalue;

  Radar *driver;  
  driver = new Radar();

  memset(tracks,0,sizeof(tracks));

  double velocity[ALLOWABLE_NUM_TARGETS];
  double range[ALLOWABLE_NUM_TARGETS];
  double yaw[ALLOWABLE_NUM_TARGETS];
  int credibility[ALLOWABLE_NUM_TARGETS];
  int status[ALLOWABLE_NUM_TARGETS];
  

  // Initialize radar 
  retvalue = driver->initRadar();  
  if(retvalue==-1) 
    { 
      printf("could not initialize radar. Abort.\n"); 
      return(-1);
    }

  // Set number of tracks to be 4
  driver->setNumTracks(4);
  printf("set the number of tracks OK\n");

  // Start send track request
  retvalue = driver->sendTrackRequest();
  if(retvalue==-1)
    {
      printf("could not send track request. Abort.\n");
      return(-1);
    }
  printf("sent track request OK\n");
  
  // Send initial state
  retvalue = driver->sendState(0,0);
  if(retvalue==-1)
    {
      printf("sendState failed. Abort.\n");
      return(-1);
    }
  printf("sent state information OK\n");


  // Start infinite loop
  for(;;)
    {      
      // grab tracks
      retvalue = driver->captureTracks();
      if(retvalue==-1)
	{
	  printf("getTracks failed. Abort.\n");
	  return(-1);
	}           

      // extract data from captured tracks
      for(int i=0; i<ALLOWABLE_NUM_TARGETS;i++)
	{
	  velocity[i] = getTrackVelocity(i);
	  range[i] = getTrackRange(i);
	  yaw[i] = getTrackYaw(i);
	  credibility[i] = getTrackCredibility(i);
	  status[i] = getTrackStatus(i);
	}      

      // send state
      retvalue = driver->sendState(0,0);
      if(retvalue==-1)
	{
	  printf("sendState failed. Abort.\n");
	  return(-1);
	}          

      // print tracks
      for(int i=0; i<ALLOWABLE_NUM_TARGETS; i++)
	{
	  printf("track[%d].velocity: %f\n", i, velocity[i]);
	  printf("track[%d].range: %f\n", i, range[i]);
	  printf("track[%d].yaw: %f\n", i, yaw[i]);
	  printf("track[%d].credibility: %f\n", i, credibility[i]);
	  printf("track[%d].status: %d\n", i, status[i]);
	}
      
    }

  return 0;
}

#endif
