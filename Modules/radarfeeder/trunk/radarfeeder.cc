
/*!
 * \file radarfeeder.cc
 * \brief Radar feeder module
 * \date 24 Apr 2007
 * \author Jeremy Ma
 * 
 * Reads radar data from driver and publishes it to sensnet.
 * This includes both tracks and targets read from the radar,
 * see wiki for more information. Modified by Tamas Szalay.
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include <math.h>

#include <alice/AliceConstants.h>
#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <frames/pose3.h>
#include <frames/mat44.h>
#include <skynet/sn_msg.hh>
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_log.h>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ProcessState.h>
#include <interfaces/RadarRangeBlob.h>
#include <interfaces/PTUStateBlob.h>
#include <cotk/cotk.h>

#include "radar_driver.hh"
#include "cmdline.h"

//alice's max vel to use PTU, m/s
#define MAX_STOP_VEL 2.00f
//alice's max pan to use PTU, deg/sec
#define MAX_PAN_VEL 0.05f
#define MAX_TILT_VEL 0.05f
//alice's max yawrate to use either radar, rad/sec
#define MAX_YAW_RATE 1.00f
//alice's min dist to consider obstacles for front radar
// -- let it be the range of the ladars
#define MIN_OBS_DIST 20
#define MIN_OBS_VEL 3

/// @brief Radar feeder class
class RadarFeeder
{
  public:   

  /// Default constructor
  RadarFeeder();

  /// Default destructor
  ~RadarFeeder();

  /// Parse the command line
  int parseCmdLine(int argc, char **argv);
  
  /// Parse the config file
  int parseConfigFile(const char *configPath);

  /// Initialize feeder for live capture
  int initLive(const char *configPath);

  /// Finalize the feeder for live capture
  int finiLive();

  /// Capture a scan 
  int captureLive();

  /// Initialize sensnet
  int initSensnet(const char *configPath);

  /// Finalize sensnet
  int finiSensnet();
  
  /// Publish data over sensnet
  int writeSensnet();

  /// Update the process state
  int updateProcessState();

  /// Get the predicted vehicle state
  int getState(uint64_t timestamp);

  public:

  // Initialize console display
  int initConsole();

  // Finalize console display
  int finiConsole();

  /// Console button callback
  static int onUserQuit(cotk_t *console, RadarFeeder *self, const char *token);

  /// Console button callback
  static int onUserPause(cotk_t *console, RadarFeeder *self, const char *token);
  
  /// Console button callback
  static int onUserLog(cotk_t *console, RadarFeeder *self, const char *token);

  public:

  // Program options
  gengetopt_args_info options;

  // Default configuration path
  char *defaultConfigPath;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;

  // Our module id (SkyNet)
  modulename moduleId;

  // Our sensor id (SensNet)
  sensnet_id_t sensorId;

  // Should we quit?
  bool quit;

  // Should we pause?
  bool pause;

  // Id of the radar
  int radarId;

  // Radar driver
  Radar* radar;
  
  // Sensor-to-vehicle transform
  float sens2veh[4][4];

  // Sensor-to-tool transform for PTU radar
  float sens2tool[4][4];

  // Calibration stats (display only).
  // Sensor position (x,y,z) and rotation (roll,pitch,yaw).
  vec3_t sensPos, sensRot;
  
  // Log file name
  char logName[1024];
  
  // Is logging enabled?
  bool enableLog;

  // SensNet handle
  sensnet_t *sensnet;

  // SensNet log handle
  sensnet_log_t *sensnet_log;
  
  // Blob buffer
  RadarRangeBlob *blob;

  // PTU blob
  PTUStateBlob ptublob;

  // is PTU stopped?
  bool ptustopped;

  // time when PTU stopped
  uint64_t ptustoptime;

  // tme to wait for PTU to have been stopped
  int ptuwait;

  // Console text display
  cotk_t *console;

  // Current scan id
  int scanId;

  // Current scan time (microseconds)
  uint64_t scanTime;

  // Current vehicle state data
  VehicleState state;
  
  // Start time for computing stats
  uint64_t startTime;
  
  // Capture stats
  int capCount;
  int capCountThisSec;
  uint64_t capTime;
  uint64_t lastSec; //last second measured
  double capRate, capPeriod;

  // Logging stats
  int logCount, logSize;

  // Number of tracks
  uint8_t numberOfTracks;

  // Number of targets
  uint8_t numberOfTargets;

  // Health status
  int healthStatus;
};


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
RadarFeeder::RadarFeeder()
{
  memset(this, 0, sizeof(*this));

  this->scanId = -1;
  this->ptustopped = false;

  return;
}


// Default destructor
RadarFeeder::~RadarFeeder()
{
  return;
}


// Parse the command line
int RadarFeeder::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;
    
  // Fill out the default config path
  this->defaultConfigPath = dgcFindConfigDir("radarfeeder");

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out sensor id
  this->sensorId = sensnet_id_from_name(this->options.sensor_id_arg);
  if (this->sensorId <= SENSNET_NULL_SENSOR)
    return ERROR("invalid sensor id: %s", this->options.sensor_id_arg);

  // Fill out the number of tracks
  this->numberOfTracks = (uint8_t)(this->options.tracks_arg);

  // Fill out the number of targets
  this->numberOfTargets = (uint8_t)(this->options.targets_arg);

  return 0;
}


// Parse the config file
int RadarFeeder::parseConfigFile(const char *configPath)
{  
  // Load options from the configuration file
  char filename[256];
  snprintf(filename, sizeof(filename), "%s/%s.CFG",
           configPath, sensnet_id_to_name(this->sensorId));
  if (cmdline_parser_configfile(filename, &this->options, false, false, false) != 0)
    MSG("unable to process configuration file %s", filename);

  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);

  // Parse transform
  float px, py, pz;
  float rx, ry, rz;

  if (sscanf(this->options.sens_pos_arg, "%f, %f, %f", &px, &py, &pz) < 3)
    return ERROR("syntax error in sensor pos argument");
  if (sscanf(this->options.sens_rot_arg, "%f, %f, %f", &rx, &ry, &rz) < 3)
    return ERROR("syntax error in sensor rot argument");
    
  pose3_t pose;
  pose.pos = vec3_set(px, py, pz);
  pose.rot = quat_from_rpy(rx, ry, rz);
  
  // now that both radars are on PTUs, both conig files represent sens2tool
  pose3_to_mat44f(pose, this->sens2tool);

  this->ptuwait = this->options.ptu_wait_arg;
  
  // Record euler angles for display only
  this->sensPos = vec3_set(px, py, pz);
  this->sensRot.x = rx * 180/M_PI;
  this->sensRot.y = ry * 180/M_PI;
  this->sensRot.z = rz * 180/M_PI;

  return 0;
}

// Initialize feeder for live capture
int RadarFeeder::initLive(const char *configPath)
{
  int retvalue;

  // Load configuration file
  if (this->parseConfigFile(configPath) != 0)
    return -1;

  // Initialize radar object
  this->radar = new Radar();
  
  // Initialize radar 
  retvalue = this->radar->initRadar(this->options.serial_number_arg);
  if(retvalue==-1) 
    { 
      printf("could not initialize radar. Abort.\n"); 
      return(-1);
    }
  
  // Set number of tracks to be numberOfTracks
  this->radar->setNumTracks(this->numberOfTracks);
  printf("set the number of tracks to %d\n", (int)(this->numberOfTracks));

  // Set number of targets to be numberOfTargets
  this->radar->setNumTracks(this->numberOfTracks);
  printf("set the number of targets to %d\n", (int)(this->numberOfTracks));
 
  if(this->numberOfTracks!=0)
    {
      // Start send track request
      retvalue = this->radar->sendTrackRequest();
      if(retvalue==-1)
	{
	  printf("could not send track request. Abort.\n");
	  return(-1);
	}
      printf("sent initial track request OK\n");
    }
   
  if(this->numberOfTargets!=0)
    {
      // Start send track request
      retvalue = this->radar->sendTargetRequest();
      if(retvalue==-1)
	{
	  printf("could not send track request. Abort.\n");
	  return(-1);
	}
      printf("sent initial track request OK\n");
    }
  
  // Send initial state of 0,0
  retvalue = this->radar->sendState(0,0);
  if(retvalue==-1)
    {
      printf("sendState failed. Abort.\n");
      return(-1);
    }
  printf("sent state information OK\n");
  
  return 0;
}

int RadarFeeder::finiLive()
{
  this->radar->disconnect();
  return 0;
}

// Capture a scan 
int RadarFeeder::captureLive()
{
  this->scanId += 1;
  this->scanTime = DGCgettime();

  // if we hit an error, it stays this
  this->healthStatus = 1;

  // Get the matching state data
  if (this->getState(this->scanTime) != 0)
    return ERROR("unable to get state; ignoring scan");

  // Send state

  double speed = this->state.vehSpeed;
  double yawrate = this->state.vehYawRate;

  // Need to transform speed and yawrate for ptu
  speed *= cos(this->ptublob.currpan);
  yawrate += this->ptublob.currpanspeed;

  // If command-line param given for sending fake state data, use it
  if (this->options.yawrate_given)
    yawrate = this->options.yawrate_arg;
  if (this->options.velocity_given)
    speed = this->options.velocity_arg;

  if(this->radar->sendState( speed, yawrate ) !=0 )
    return -1;

  if(this->numberOfTracks!=0)
  {
    // Read data from sensor.
    if(this->radar->captureTracks() != 0)
      return -1;
      
    // extract data from captured tracks
    memset(this->blob->tracks, 0, sizeof(this->blob->tracks));
    for(int i=0; i<RADAR_BLOB_MAX_TRACKS;i++)
    {
      this->blob->tracks[i].velocity = this->radar->getTrackVelocity(i);
      this->blob->tracks[i].range = this->radar->getTrackRange(i);
      this->blob->tracks[i].yaw = this->radar->getTrackYaw(i);
      this->blob->tracks[i].credibility = this->radar->getTrackCredibility(i);
      this->blob->tracks[i].status = this->radar->getTrackStatus(i);
    }    
  }
  
  if(this->numberOfTargets!=0)
  {
    // Read data from sensor.
    if(this->radar->captureTargets() != 0)
      return -1;
      
    // extract data from captured targets
    memset(this->blob->targets, 0, sizeof(this->blob->targets));
    for(int i=0; i<RADAR_BLOB_MAX_TARGETS;i++)
    {
      this->blob->targets[i].velocity = this->radar->getTargetVelocity(i);
      this->blob->targets[i].range = this->radar->getTargetRange(i);
      this->blob->targets[i].yaw = this->radar->getTargetYaw(i);
      this->blob->targets[i].credibility = 0; // target returns give no credibility
      this->blob->targets[i].status = this->radar->getTargetStatus(i);
    }
  }

  if (this->console)
  {
    cotk_printf(this->console, "%capid%", A_NORMAL, "%5d %9.3f",
                this->scanId, fmod((double) this->scanTime * 1e-6, 10000));
    if (this->state.timestamp > 0)
      cotk_printf(this->console, "%slat%", A_NORMAL,
                  "%+6dms", (int) (this->state.timestamp - this->scanTime) / 1000);
  }

  this->healthStatus = 2;

  return 0;
}


// Initialize sensnet
int RadarFeeder::initSensnet(const char *configPath)
{    
  // Check that blob size is a multiple of [something].  This allows
  // for DMA transfers.
  if (sizeof(RadarRangeBlob) % 512 != 0)
    return ERROR("invalid blob size %d; needs padding of %d",
                 sizeof(RadarRangeBlob),
                 512 - sizeof(RadarRangeBlob) % 512);

  // Create page-aligned blob to enable DMA logging
  this->blob = (RadarRangeBlob*) valloc(sizeof(RadarRangeBlob));

  // Initialize SensNet
  this->sensnet = sensnet_alloc();
  if (sensnet_connect(this->sensnet,
                      this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return ERROR("unable to connect to sensnet");    
  
  // Subscribe to vehicle state messages
  if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, sizeof(VehicleState)) != 0)
    return ERROR("unable to join state group");

  // Subscribe to process state messages
  if (sensnet_join(this->sensnet, this->moduleId, SNprocessRequest, sizeof(ProcessRequest)) != 0)
    return ERROR("unable to join process group");

  if (this->sensorId==SENSNET_PTU_RADAR) 
    {
      // Subscribe to AMTEC PTU state messages
      if (sensnet_join(this->sensnet, SENSNET_MF_PTU, SENSNET_PTU_STATE_BLOB, sizeof(this->ptublob)) != 0)
	return ERROR("unable to join PTU state group");
    }
  else
    {
      // Subscribe to Directed Perception PTU state messages
      if (sensnet_join(this->sensnet, SENSNET_LF_PTU, SENSNET_PTU_STATE_BLOB, sizeof(this->ptublob)) != 0)
	return ERROR("unable to join PTU state group");
    }


  // Initialize logging
  if (this->options.enable_log_flag)
  {
    time_t t;
    char timestamp[64];
    char cmd[256];
    sensnet_log_header_t header;

    // Construct log name
    t = time(NULL);
    strftime(timestamp, sizeof(timestamp), "%F-%a-%H-%M", localtime(&t));
    snprintf(this->logName, sizeof(this->logName), "%s/%s-%s",
             this->options.log_path_arg, timestamp, sensnet_id_to_name(this->sensorId));

    MSG("opening log %s", this->logName);

    // Initialize sensnet logging
    this->sensnet_log = sensnet_log_alloc();
    assert(this->sensnet_log);
    memset(&header, 0, sizeof(header));
    if (sensnet_log_open_write(this->sensnet_log, this->logName, &header, true) != 0)
      return ERROR("unable to open log: %s", this->logName);

    // Copy configuration files
    snprintf(cmd, sizeof(cmd), "cp %s/%s.CFG %s",
             configPath, sensnet_id_to_name(this->sensorId), this->logName);
    system(cmd);
        
  }

  return 0;
}


// Finalize sensnet
int RadarFeeder::finiSensnet()
{  

  if (this->sensnet_log)
  {
    sensnet_log_close(this->sensnet_log);
    sensnet_log_free(this->sensnet_log);
    this->sensnet_log = NULL;
  }

  sensnet_leave(this->sensnet, this->moduleId, SNprocessRequest);
  sensnet_leave(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate);
  if (this->sensorId==SENSNET_PTU_RADAR)
    sensnet_leave(this->sensnet, SENSNET_MF_PTU, SENSNET_PTU_STATE_BLOB);
  else
    sensnet_leave(this->sensnet, SENSNET_LF_PTU, SENSNET_PTU_STATE_BLOB);

  sensnet_disconnect(this->sensnet);
  sensnet_free(this->sensnet);
  this->sensnet = NULL;

  free(this->blob);
  this->blob = NULL;
  
  return 0;
}


// Publish data
int RadarFeeder::writeSensnet()
{
  pose3_t pose;
  RadarRangeBlob *blob;

  blob = this->blob;

  // Construct the blob header
  blob->blobType = SENSNET_RADAR_BLOB;
  blob->sensorId = this->sensorId;
  blob->scanId = this->scanId;
  blob->timestamp = this->scanTime;
  blob->state = this->state;

  bool ignoredata = false;

  // Transform for PTU
  float sens2ptu[4][4];
  
  //Sensor to veh transform, w/PTU
  mat44f_mul(sens2ptu, this->ptublob.tool2ptu, this->sens2tool);
  mat44f_mul(this->sens2veh, this->ptublob.ptu2veh, sens2ptu);
  
  if (fabs(this->ptublob.currpanspeed) < MAX_PAN_VEL && 
      (fabs(this->ptublob.currtiltspeed) < MAX_TILT_VEL))
    {
      if (!ptustopped) //we just stopped
	ptustoptime = DGCgettime();
      ptustopped = true;
    }
  else
    {
      ptustopped = false;
    }
  bool timewait = ((int)((DGCgettime()-ptustoptime)/1000) > ptuwait);
  
  // only use data if we are stopped and have been for longer than ptuwait
  if(this->sensorId==SENSNET_PTU_RADAR)
    {
      if(ptustopped && timewait)
	ignoredata = false;
      else
	ignoredata = true;
    }
  
  if (fabs(this->state.localYawRate) > MAX_YAW_RATE)
    ignoredata = true;    
  
  if(this->sensorId == SENSNET_PTU_RADAR)
    {    
      if (fabs(this->state.vehSpeed) > MAX_STOP_VEL)
	ignoredata = true;
    }

  if (ignoredata)
    for (int i=0;i<RADAR_BLOB_MAX_TRACKS;i++)
      this->blob->tracks[i].status |= 0x08;

  for(int i=0; i<RADAR_BLOB_MAX_TRACKS; i++)
    {
      if(this->blob->tracks[i].range < MIN_OBS_DIST)
	this->blob->tracks[i].status |= 0x08;
    }

  // Sensor to vehicle transform
  memcpy(blob->sens2veh, this->sens2veh, sizeof(this->sens2veh));
  mat44f_inv(blob->veh2sens, blob->sens2veh);
  
  // Vehicle to local transform
  pose.pos = vec3_set(blob->state.localX,
		      blob->state.localY,
		      blob->state.localZ);
  pose.rot = quat_from_rpy(blob->state.localRoll,
			   blob->state.localPitch,
			   blob->state.localYaw);  
  pose3_to_mat44f(pose, blob->veh2loc);  
  mat44f_inv(blob->loc2veh, blob->veh2loc);
  
  // Reset reseved values
  memset(blob->reserved, 0, sizeof(blob->reserved));

  // Copy the scan data
  memcpy(blob->tracks, this->blob->tracks, sizeof(this->blob->tracks));

  // Write blob
  if (sensnet_write(this->sensnet, SENSNET_METHOD_CHUNK, this->sensorId, SENSNET_RADAR_BLOB,
                    this->scanId, sizeof(*blob), blob) != 0)
    return ERROR("unable to write blob");
  
  // Write to log
  if (this->sensnet_log && (this->enableLog || this->options.always_log_flag))
  {
    if (sensnet_log_write(this->sensnet_log, DGCgettime(),
                          this->sensorId, SENSNET_RADAR_BLOB,
                          this->scanId, sizeof(*blob), blob) != 0)
      return ERROR("unable to write blob");
  }

  if (this->sensnet_log && (this->enableLog || this->options.always_log_flag) && this->console)
  {    
    // Keep some stats on logging
    this->logCount += 1;
    this->logSize += sizeof(*blob);
    cotk_printf(this->console, "%log%", A_NORMAL, "%df %dMb",
                this->logCount, this->logSize / 1024 / 1024);
  }
  return 0;
}


// Get the predicted vehicle state
int RadarFeeder::getState(uint64_t timestamp)
{
  int blobId;
  int ptublobId;

  // Default to all zeros in state
  memset(&this->state, 0, sizeof(this->state));
  
  // Get the current state value
  if (sensnet_read(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate,
		   &blobId, sizeof(this->state), &this->state) != 0)
    return ERROR("unable to read state data");
  if(blobId < 0)
    return ERROR("state is invalid");

  if (this->sensorId == SENSNET_PTU_RADAR) 
    {      
      // Default to all 0s
      memset(&this->ptublob, 0, sizeof(this->ptublob));
      
      if (sensnet_read(this->sensnet, SENSNET_MF_PTU, SENSNET_PTU_STATE_BLOB,
		       &ptublobId, sizeof(this->ptublob), &this->ptublob) != 0)
	return ERROR("unable to read PTU state data");
      if (ptublobId < 0)
	return ERROR("PTU state invalid");
    }
  else
    {
      // Default to all 0s
      memset(&this->ptublob, 0, sizeof(this->ptublob));
      
      if (sensnet_read(this->sensnet, SENSNET_LF_PTU, SENSNET_PTU_STATE_BLOB,
		       &ptublobId, sizeof(this->ptublob), &this->ptublob) != 0)
	return ERROR("unable to read PTU state data");
      if (ptublobId < 0)
	return ERROR("PTU state invalid");
    }
  
  // Update the display
  if (this->console)
    {
    cotk_printf(this->console, "%stime%", A_NORMAL, "%9.3f",
                fmod((double) state.timestamp * 1e-6, 10000));
    cotk_printf(this->console, "%spos%", A_NORMAL, "%+09.3f %+09.3f %+09.3f",
                state.localX, state.localY, state.localZ);
    cotk_printf(this->console, "%srot%", A_NORMAL, "%+06.1f %+06.1f %+06.1f",
                this->state.localRoll*180/M_PI, 
                this->state.localPitch*180/M_PI, 
                this->state.localYaw*180/M_PI);
    cotk_printf(this->console, "%sspeed%", A_NORMAL, "%+06.3f %+06.1f",
                state.vehSpeed, state.vehYawRate*180/M_PI);
  }
  
  return 0;
}

// Update the process state
int RadarFeeder::updateProcessState()
{
  int blobId;
  ProcessRequest request;
  ProcessResponse response;

  // Send heart-beat message
  memset(&response, 0, sizeof(response));  
  response.moduleId = this->moduleId;
  response.timestamp = DGCgettime();
  response.logSize = this->logSize / 1024;
  response.healthStatus = this->healthStatus;
  sensnet_write(sensnet, SENSNET_METHOD_CHUNK,
                this->moduleId, SNprocessResponse, 0, sizeof(response), &response);
  
  // Read process request
  if (sensnet_read(this->sensnet, this->moduleId, SNprocessRequest,
                   &blobId, sizeof(request), &request) != 0)
    return 0;
  if (blobId < 0)
    return 0;

  // If we have request data, override the console values
  this->quit = request.quit;
  this->enableLog = request.enableLog;

  if (request.quit)
    MSG("remote quit request");
  
  return 0;
}

// Template for console
//234567890123456789012345678901234567890123456789012345678901234567890123456789
static char *consoleTemplate =
"RadarFeeder $Revision$                                                     \n"
"                                                                           \n"
"Skynet: %spread%                                                           \n"
"Sensor: %sensor%                                                           \n"
"No. of Tracks : %trackNum%                                                 \n"
"No. of Targets: %targetNum%                                                \n"
"                                                                           \n"
"Capture                           State                                    \n"
"Scan     : %capid%                     Time   : %stime% %slat%             \n"
"Log      : %log%                       Pos    : %spos%                     \n"
"         : %logname%                   Rot    : %srot%                     \n"
"Cap Rate : %caprate%                   Speed  : %sspeed%                   \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%|%LOG%]                                                     \n";


// Initialize console display
int RadarFeeder::initConsole()
{
  char filename[1024];
    
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
  cotk_bind_toggle(this->console, "%LOG%", " LOG ", "Ll",
                   (cotk_callback_t) onUserLog, this);

  // Initialize the display
  snprintf(filename, sizeof(filename), "%s/%s.msg",
           this->options.log_path_arg, sensnet_id_to_name(this->sensorId));
  if (cotk_open(this->console, filename) != 0)
    return -1;


  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));
  cotk_printf(this->console, "%sensor%", A_NORMAL, sensnet_id_to_name(this->sensorId));    
  cotk_printf(this->console, "%logname%", A_NORMAL, this->logName);
  cotk_printf(this->console, "%trackNum%", A_NORMAL, "%d",(int)(this->numberOfTracks));
  cotk_printf(this->console, "%targetNum%", A_NORMAL, "%d",(int)(this->numberOfTargets));

  return 0;
}


// Finalize console display
int RadarFeeder::finiConsole()
{
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Handle button callbacks
int RadarFeeder::onUserQuit(cotk_t *console, RadarFeeder *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int RadarFeeder::onUserPause(cotk_t *console, RadarFeeder *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}


// Handle user events; occurs in sparrow thread
int RadarFeeder::onUserLog(cotk_t *console, RadarFeeder *self, const char *token)
{
  assert(self);
  self->enableLog = !self->enableLog;
  MSG("log %s", (self->enableLog ? "on" : "off"));
  return 0;
}


// Main program thread
int main(int argc, char **argv)
{
  RadarFeeder *feeder;

  // Create feeder
  feeder = new RadarFeeder();
  assert(feeder);
 
  // Parse command line options
  if (feeder->parseCmdLine(argc, argv) != 0)
    return -1;

  // Initialize for live capture
  if (feeder->initLive(feeder->defaultConfigPath) != 0)
    return -1;

  // Initialize sensnet
  if (feeder->initSensnet(feeder->defaultConfigPath) != 0)
    return -1;


  // Initialize console
  if (!feeder->options.disable_console_flag)
  {
    if (feeder->initConsole() != 0)
      return -1;
  }

  feeder->startTime = DGCgettime();
  feeder->lastSec = feeder->startTime;

  int errMsg;

  // Start processing
  while (!feeder->quit)
  {
    errMsg = 0;
    // Do heartbeat occasionally
    if (feeder->capCount % 5 == 0)
      feeder->updateProcessState();

    // Capture incoming scan directly into the radar buffer
    if (feeder->captureLive() != 0) 
      {
	errMsg = 1;
	ERROR("Live capture failed!\n");
	usleep(50000);
      }
    else
      {
	MSG("status: good");
      }

    // Compute some diagnostics
    feeder->capCount += 1;
    feeder->capCountThisSec += 1;
    feeder->capTime = DGCgettime() - feeder->startTime;
    if ((DGCgettime() - feeder->lastSec) > 1000000) {
      feeder->lastSec = DGCgettime();
      feeder->capRate = feeder->capCountThisSec;
      feeder->capPeriod = 1000.0 / feeder->capRate;
      // Output capture rate
      cotk_printf(feeder->console, "%caprate%", A_NORMAL, "%05.1f Hz    ",
		  feeder->capRate);
      feeder->capCountThisSec=0;
    }
    //feeder->capRate = (float) 1000 * feeder->capCount / feeder->capTime;

    // Update the console
    if (feeder->console)
      cotk_update(feeder->console);
    
    // If paused, give up our time slice.
    if (feeder->pause)
    {
      usleep(50000);
      continue;
    }
    
    // Publish data
    if (feeder->writeSensnet() != 0) {
      errMsg = 2;
      ERROR("Sensnet write failed!\n");
    }
  }
  
  // Clean up
  feeder->finiConsole();
  feeder->finiSensnet();
  feeder->finiLive();
  cmdline_parser_free(&feeder->options);  
  delete feeder;

  if (errMsg == 1)
    MSG("error during live capture");
  if (errMsg == 2)
    MSG("error writing sensnet");

  MSG("feeder->quit=%s", (feeder->quit?"true":"false"));
  MSG("program exited cleanly");
  
  return 0;
}
