Wed Sep 26 17:56:16 2007	Jeremy Ma (jerma)

	* version R1-00r
	BUGS:  
	FILES: SENSNET_PTU_RADAR.CFG(40587), SENSNET_RADAR.CFG(40587),
		cmdline.c(40587), cmdline.ggo(40587), cmdline.h(40587),
		radar_driver.cc(40587), radar_driver.hh(40587),
		radar_viewer.cc(40587), radarfeeder.cc(40587)
	adding compatibility to use multiple CAN devices on the same
	machine; the CFG files now specify a serial number specific to the
	CANbus and looks to connect on that device only.

Sun Sep 16 12:08:57 2007	Tamas Szalay (tamas)

	* version R1-00q
	BUGS:  
	FILES: radarfeeder.cc(39007)
	Fixed another bug. With a negative sign. No more random obstacles
	from PTU radar, really really, I mean it.

Fri Sep 14  1:45:48 2007	Tamas Szalay (tamas)

	* version R1-00p
	BUGS:  
	FILES: cmdline.c(38722), cmdline.ggo(38722), cmdline.h(38722),
		radarfeeder.cc(38722)
	Added a pause after PTU stops before PTU data is considered, to
	give the radar time to remove tracks picked up while panning. Use
	--ptu-wait to set time, default is 500 ms.

Wed Sep 12  1:48:22 2007	Tamas Szalay (tamas)

	* version R1-00o
	BUGS:  
	FILES: SENSNET_RADAR.CFG(38494)
	Tweaked config file for position and orientation.

Mon Sep 10 17:53:12 2007	Tamas Szalay (tamas)

	* version R1-00n
	BUGS:  
	FILES: SENSNET_RADAR.CFG(38181), radarfeeder.cc(38181)
	Disregards PTU data based on tilt rate as well; tweaked config file
	to reflect actual orientation.

Sun Sep  2 14:09:31 2007	Tamas Szalay (tamas)

	* version R1-00m
	BUGS:  
	FILES: SENSNET_PTU_RADAR.CFG(37129), radarfeeder.cc(37129)
	Radar returns are now ignored when PTU or alice are turning too
	fast, and PTU returns still ignored when alice moving > 0.75 m/s.
	Also fixed PTU config file - was screwed up.

Thu Aug 30 15:05:36 2007	Tamas Szalay (tamas)

	* version R1-00l
	BUGS:  
	FILES: Makefile.yam(36567), radarfeeder.cc(36567)
	Set doxygen documentation to true, and accumulated changes from
	sync merge

	FILES: radarfeeder.cc(36519)
	Disregard PTU tracks if we're moving too fast

	FILES: radarfeeder.cc(36532)
	Changed stop threshold.

	FILES: radarfeeder.cc(36537)
	oops bugsies

	FILES: radarfeeder.cc(36544)
	changed stop thresh

Thu Aug 30 14:47:07 2007	Jeremy Ma (jerma)

	* version R1-00k
	BUGS:  
	FILES: Makefile.yam(36518)
	changed makefile to include link to SENSNET_PTU_RADAR.CFG in the
	etc folder

Tue Aug 28 14:22:58 2007	Tamas Szalay (tamas)

	* version R1-00j
	BUGS:  
	FILES: radarfeeder.cc(35896)
	Fixed PTU transform.

Tue Aug 21 21:22:40 2007	Tamas Szalay (tamas)

	* version R1-00i
	BUGS:  
	FILES: radarfeeder.cc(34812)
	Increased rate at which health status messages are being sent.

Mon Aug  6 22:30:39 2007	Laura Lindzey (lindzey)

	* version R1-00h
	BUGS:  
	FILES: radarfeeder.cc(32424)
	one line change, to make radarfeeder adhere to general convention
	of using moduleId rather than sensorId when connecting to spread

Wed Jul 25 13:04:19 2007	Tamas Szalay (tamas)

	* version R1-00g
	BUGS:  
	FILES: radar_driver.cc(30022), radar_driver.hh(30022),
		radarfeeder.cc(30022)
	Updated status to keep track of whether obstacle is stationary or
	not as reported by radar.

	FILES: radar_driver.cc(30039), radar_viewer.cc(30039)
	Added extended status information to viewer

Thu Jul 19 18:43:42 2007	Tamas Szalay (tamas)

	* version R1-00f
	BUGS:  
	FILES: radar_driver.cc(29413)
	Trying something else with ID, removed warning

	FILES: radarfeeder.cc(29688)
	Added health status monitoring

Tue Jul 17 16:33:51 2007	Tamas Szalay (tamas)

	* version R1-00e
	BUGS:  
	New files: SENSNET_PTU_RADAR.CFG VARIABLES
	FILES: radar_diag.cc(29082), radar_diag.hh(29082),
		radarfeeder.cc(29082)
	Added logging to diagnostic utility

	FILES: radar_driver.cc(29026), radar_driver.hh(29026),
		radarfeeder.cc(29026)
	Added doxygen comments

	FILES: radar_driver.cc(29257), radar_viewer.cc(29257)
	Added backup of radar internal variables, fixed reporting/use of
	status information

	FILES: radar_driver.cc(29316)
	Fixed handling of track IDs

	FILES: radarfeeder.cc(29318)
	Minor fixes

Thu Jul 12 23:27:56 2007	Tamas Szalay (tamas)

	* version R1-00d
	BUGS:  
	New files: radar_diag.cc radar_diag.hh
	FILES: Makefile.yam(28849), cmdline.c(28849), cmdline.ggo(28849),
		cmdline.h(28849), radar_driver.cc(28849),
		radarfeeder.cc(28849)
	Fixed two bugs that erroneously reported negative yaw rates and
	velocities, added diagnostic tool to read/write internal radar
	parameters

	FILES: radar_driver.cc(28966)
	Added radar diagnostic utility to read and write radar internal
	parameters

Mon May  7 18:31:58 2007	Jeremy Ma (jerma)

	* version R1-00c
	BUGS:  
	FILES: radar_driver.cc(22447)
	slight modification to the yaw_speed to set the 6th bit to "valid"

Fri Apr 27 22:37:05 2007	Andrew Howard (ahoward)

	* version R1-00b
	BUGS:  
	New files: radar_viewer_test.cc
	FILES: Makefile.yam(21074), SENSNET_RADAR.CFG(21074),
		cmdline.c(21074), cmdline.ggo(21074), cmdline.h(21074),
		radar_driver.cc(21074), radar_driver.hh(21074),
		radar_viewer.cc(21074), radarfeeder.cc(21074)
	minor modifications to allow user-specification of number of tracks
	and number of targets; also changes to visualization for
	credibility.

	FILES: SENSNET_RADAR.CFG(21143), radarfeeder.cc(21143)
	Tweaks for data collection

	FILES: radar_driver.cc(21142), radar_viewer.cc(21142)
	minor changes to driver and viewer.

	FILES: radar_driver.cc(21178)
	Fast

Wed Apr 25 11:03:04 2007	Jeremy Ma (jerma)

	* version R1-00a
	BUGS:  
	New files: SENSNET_RADAR.CFG cmdline.c cmdline.ggo cmdline.h
		radar_driver.cc radar_driver.hh radar_viewer.cc
		radarfeeder.cc
	FILES: Makefile.yam(20461)
	Committing the driver files and a simple viewer to test the AC20
	radar.

	FILES: Makefile.yam(20547)
	making some quick commits before doing a major change to the driver
	code.

Tue Apr 24 10:40:24 2007	Jeremy Ma (jerma)

	* version R1-00
	Created radarfeeder module.




















