/*
 * \file radar_driver.hh
 * \brief Driver for the TRW radar
 * \author Jeremy Ma, adapted from the sick_driver.c
 * \date 05 April 2007
*/

#ifndef RADAR_DRIVER_H
#define RADAR_DRIVER_H

#include <stdio.h>
#include <ntcan/ntcan.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <iostream>

#define ALLOWABLE_NUM_TARGETS 20

 //all relative to radar
struct RadarObject{
  double range;
  double yaw;
  double velocity;
  int credibility;
  int status; //0 is dead; 1 is alive
};

class Radar
{
  
public:

  // Default constructor
  Radar();

  // Default destructor
  ~Radar();

  // Initialize the radar
  int initRadar();

  // Set the number of tracks
  void setNumTracks(uint8_t trackTotal);
 
  // Set number of targets
  void setNumTargets(uint8_t targetTotal);

  // Send state information to radar (km/hr and deg/sec)
  int sendState(double speed, double yawrate);

  // Send request for tracks
  int sendTrackRequest();

  // Send request for targets
  int sendTargetRequest();

  // Capture radar track information
  int captureTracks();

  // Capture radar target information
  int captureTargets();

  // Get i-th track velocity
  double getTrackVelocity(int id);

  // Get i-th track range
  double getTrackRange(int id);

  // Get i-th track yaw
  double getTrackYaw(int id);

  // Get i-th track credibility
  int getTrackCredibility(int id);

  // Get i-th track status
  int getTrackStatus(int id);

  // Get i-th target velocity
  double getTargetVelocity(int i);

  // Get i-th target range
  double getTargetRange(int i);

  // Get i-th target yaw
  double getTargetYaw(int i);

  // Get i-th target status
  int getTargetStatus(int i);

  // Close all handles 
  int disconnect();

private:
  
  // CAN Handle for transmission of commands
  int32_t txhandle;

  // CAN Handle for receiving target messages
  int32_t rxhandle_targets;

  // CAN Handle for receiving track messages
  int32_t rxhandle_tracks;

  // frame buffer for storing data frames from the radar
  char frame_buffer[2*ALLOWABLE_NUM_TARGETS][8]; //2 frames per object and 8 bytes in each frame

  // number of tracks for radar to maintain
  uint8_t numTracks;
  
  // number of targets for radar to maintain
  uint8_t numTargets;

  // baud rate (2 is actually 500kbps)
  unsigned long baud;
  
  // logical netnumber
  int net;

  // mode for canOpen
  unsigned long mode; 

  // maximum number of messages to transmit = 40 since 20 moving objects and 2 frames/object
  long txqueuesize;  

  // maximum number of messages to receive = 40 since 20 moving objects and 2 frames/object
  long rxqueuesize;  

  // timeout for transmit
  long txtimeout;   

  // timeout for receiving data
  long rxtimeout;          

  // array of structs for track storage
  RadarObject tracks[ALLOWABLE_NUM_TARGETS];

  // array of structs for target storage
  RadarObject targets[ALLOWABLE_NUM_TARGETS];

  // Counter for yaw rate state message
  uint8_t yawcounter;

};

#endif
