/*!
 * \file radar_driver.cc
 * \brief Low-level driver for radar
 * \author Jeremy Ma
 * 
 * The driver communicates with the radar via an interface called CAN,
 * which is enabled via USB->CAN hardware we have. Messages have
 * different CAN IDs, and each receiving CAN handle can have multiple
 * IDs it is listening for at a time.
 */

#include "radar_driver.hh"
#include <assert.h>
#include <errno.h>

using namespace std;

// Default constructor
Radar::Radar()
{

  memset(this, 0, sizeof(this));

  // ----------------------------------------------
  // Initialize private variables to default values
  // ----------------------------------------------
  numTracks = 10;
  numTargets = 10;

  yawcounter = 0;
  
  baud = NTCAN_BAUD_500;

  net=0;                  /* logical netnumber */
  mode=0;                 /* mode for canOpen */
  txqueuesize=40;         /* maximum number of messages to transmit = 40 since 20 moving objects and 2 frames/object */
  rxqueuesize=40;         /* maximum number of messages to receive  = 40 since 20 moving objects and 2 frames/object */
  txtimeout=100;          /* timeout for transmit */
  rxtimeout=500;          /* timeout for receiving data */  

  return;
}

// Destructor
Radar::~Radar()
{
  canClose(txhandle);
  canClose(rxhandle_targets);
  canClose(rxhandle_tracks);
  return;
}

// Initialize the radar
int Radar::initRadar()
{
  int retvalue;
  
  // ------------------------------------
  // Open CAN handle for sending messages
  // ------------------------------------
  retvalue = canOpen(net,
		     mode,
		     txqueuesize,
		     rxqueuesize,
		     txtimeout,
		     rxtimeout,
		     &txhandle);
  
  if(retvalue!=NTCAN_SUCCESS)
    {      
      printf("canOpen() transmit handle failed with error %d!\n", retvalue);
      if (retvalue == NTCAN_NET_NOT_FOUND)
	printf("(Net not found, make sure USB-CAN initialized properly"
	       " and feeder not already running)\n");
      return(-1);
    }
  else    
    printf("opened transmit handle OK !\n");
  
  // ------------------------
  // Set Baudrate for sending
  // ------------------------
  retvalue = canSetBaudrate(txhandle,baud);
  if(retvalue!=0)
    {
      printf("canSetBaudrate() failed with error %d!\n", retvalue);
      canClose(txhandle);
      return(-1);
    }
  else    
    printf("function canSetBaudrate() returned OK ! \n");
    


  /// ----------------------------------
  /// Open CAN handle for reading tracks
  /// ----------------------------------
  retvalue = canOpen(net,
		     mode,
		     txqueuesize,
		     rxqueuesize,
		     txtimeout,
		     rxtimeout,
		     &rxhandle_tracks);

  if(retvalue!=NTCAN_SUCCESS)
    {      
      printf("canOpen() track handle failed with error %d!\n", retvalue);
      return(-1);
    }
  else
    printf("opened track handle OK !\n");

  // -------------------------------
  // Add handle ID for track message
  // -------------------------------
  retvalue = canIdAdd(rxhandle_tracks,0x663);
  if(retvalue!=NTCAN_SUCCESS)
    {
      printf("canIdAdd() failed with error %d!\n", retvalue);
      printf(" could not add handle ID for general status message.\n");
      canClose(rxhandle_tracks);
      return(-1);
    }
 
  /// ------------------------------------------
  /// Open CAN handle for reading target message
  /// ------------------------------------------
  retvalue = canOpen(net,
		     mode,
		     txqueuesize,
		     rxqueuesize,
		     txtimeout,
		     rxtimeout,
		     &rxhandle_targets);

  if(retvalue!=NTCAN_SUCCESS)
    {      
      printf("canOpen() target handle failed with error %d!\n", retvalue);
      return(-1);
    }
  else
    printf("opened target handle OK !\n");

  // --------------------------------
  // Add handle ID for target message
  // --------------------------------
  retvalue = canIdAdd(rxhandle_targets,0x662);
  if(retvalue!=NTCAN_SUCCESS)
    {
      printf("canIdAdd() failed with error %d!\n", retvalue);
      printf(" could not add handle ID for general status message.\n");
      canClose(rxhandle_targets);
      return(-1);
    }

  return(0);

}

/// ---------------------------------
/// Set the number of tracks to track
/// ---------------------------------
void Radar::setNumTracks(uint8_t trackTotal)
{
  numTracks = trackTotal;
}


/// ---------------------------------
/// Set the number of targets to track
/// ---------------------------------
void Radar::setNumTargets(uint8_t targetTotal)
{
  numTargets = targetTotal;
}

/// --------------------------
/// Send State to radar
/// --------------------------
int Radar::sendState(double speed, double yawrate)
{
  //wheelspeed given in m/s but must be coded as km/hr
  //yawrate given in rad/s but must be coded as degrees/sec
  //both speeds are in local reference frame

  double newspeed = speed*3.6; //km/hr
  double newyawrate = yawrate*180/M_PI; //deg/s

  /* NOTE: if the radar return seems rather strange, it may be 
   * because of the ordering of the bits; the documentation 
   * seems rather ambiguous about which bit numbering scheme
   * is used. Right now, the 0th bit is assumed as the LSb and
   * as such, then the 16th bit which holds the sign of the 
   * number must be moved to the 0th bit position to be in 
   * accordance with the documentation
   */
  int16_t sendspeed = (int16_t)(newspeed/0.01); //conversion factor
  sendspeed = abs(sendspeed); // radar expects magnitude
  int16_t tmpspeed = 0;
  int16_t lastbit = (newspeed>0)?0:1; // forward/reverse

  // shift speed over by one, and replace last bit with the direction bit
  tmpspeed = (sendspeed << 1);
  sendspeed = (tmpspeed | lastbit);

  uint8_t MSB_speed = (uint8_t)( (sendspeed >> 8) & 0xFF);
  uint8_t LSB_speed = (uint8_t)( (sendspeed & 0xFF) );

  // Display speed in binary
  /*  printf("\nSpeed: %d\n",sendspeed);
  for (uint8_t i=128;i>0;i>>=1)
    printf((MSB_speed&i)?"1":"0");
  for (uint8_t i=128;i>0;i>>=1)
  printf((LSB_speed&i)?"1":"0");*/

  int len, retvalue;                 /* Bufsize in number of messages for canRead() */
  CMSG wheelSpeed, yawSpeed;              /* can message buffer */

  //-----------------------
  //send wheel speed
  //-----------------------
  wheelSpeed.id=0x4A0;
  wheelSpeed.len=0x08;

  // next two bytes deal with FL wheel speed
  wheelSpeed.data[0] = LSB_speed;
  wheelSpeed.data[1] = MSB_speed; 

  // next two bytes deal with FR wheel speed
  wheelSpeed.data[2] = LSB_speed;
  wheelSpeed.data[3] = MSB_speed;

  // next two bytes deal with RL wheel speed
  wheelSpeed.data[4] = LSB_speed;
  wheelSpeed.data[5] = MSB_speed;

  // next two bytes deal with RR wheel speed
  wheelSpeed.data[6] = LSB_speed;
  wheelSpeed.data[7] = MSB_speed;
  
  len = 1;
  
  retvalue = canSend(txhandle,&wheelSpeed,&len);
  if(retvalue != NTCAN_SUCCESS)
    {
      printf("ERROR: unable to send wheel speed!!\n");  
      return(-1);   
    }

  // now we store the yawrate
  // NOTE: The radar expects the numerical value as magnitude,
  // with a sign bit that sets +/-; this is not the way negative
  // numbers are stored by default

  int16_t sendyaw = (int16_t)(newyawrate/0.01); // conversion factor
  sendyaw = abs(sendyaw);
  uint8_t MSB_yaw = (uint8_t)( (sendyaw >> 8) & 0xFF);
  uint8_t LSB_yaw = (uint8_t)( (sendyaw & 0xFF) );
  uint8_t send_yawcounter = 0;
  uint8_t checksum = 0;

  //-----------------------
  //need to set the valid sign 
  //-----------------------
  // in MSB_yaw bit 6 (for bits 0-7); 0 is valid, 1 is invalid
  // need to mask the MSB_yaw with 00111111(i.e. 0x3F)
  // we are making the sign bit (bit 7) 0 because we'll set it next
  MSB_yaw &= 0x3F;

  // Set sign bit. Note that 1 is positive yaw rate for radar, 
  // which corresponds to counter-clockwise rotation of 
  // Alice (which is negative in Alice)
  if (newyawrate > 0) MSB_yaw |= 128;

  // Display yaw rate in binary
  /*  printf("\nYaw: %d\n",sendyaw);
  for (uint8_t i=128;i>0;i>>=1)
    printf((MSB_yaw&i)?"1":"0");
  for (uint8_t i=128;i>0;i>>=1)
  printf((LSB_yaw&i)?"1":"0");*/

  //-----------------------
  //send yaw rate
  //-----------------------
  yawSpeed.id=0x4A8;
  yawSpeed.len=0x08;

  // next two bytes deal with the yawrate
  yawSpeed.data[0] = LSB_yaw; 
  yawSpeed.data[1] = MSB_yaw;
  
  // next 4 bytes are not used
  yawSpeed.data[2] = 0x00;
  yawSpeed.data[3] = 0x00;
  yawSpeed.data[4] = 0x00;
  yawSpeed.data[5] = 0x00;

  // next byte deals with counter for overflowing message counter
  // as sign of life
  send_yawcounter = yawcounter & 0xFF;
  send_yawcounter = send_yawcounter << 4;
  yawSpeed.data[6] = yawcounter;

  // last byte is checksum
  for(int i=0; i<8; i++)
    {
      checksum = yawSpeed.data[i] + checksum;
    }
  yawSpeed.data[7] = checksum;

  len = 1;
  
  retvalue = canSend(txhandle,&yawSpeed,&len);
  if(retvalue != NTCAN_SUCCESS)
    {
      printf("ERROR: unable to send yaw rate!!\n");
      return(-1);
    }

  yawcounter++;

  if(yawcounter>15)
    yawcounter=0;

  return(0);

}

//-----------------------------
// Send request for tracks
//-----------------------------
int Radar::sendTrackRequest()
{
  CMSG command;
  int len; 
  int retvalue;

  // --------------------------
  // Send command for continuous broadcast
  // --------------------------
  command.id=0x660; //ID for tracks command
  command.len=0x08; //length of command is 8 bytes
  command.data[0] = 0x07; // command to request tracks (0x01 is targets; 0x07 is tracks)
  command.data[1] = numTracks; // number of targets to detect; 0x14 is max
  command.data[2] = 0x00; // normally set to 0
  command.data[3] = 0x00; // normally set to 0; 1 if requesting extended status message
  command.data[4] = 0x00; // normally set to 0; 1 if requestion extended target message
  command.data[5] = 0x00; // not used
  command.data[6] = 0xFF; // continuous sending of this message 0xFF
  command.data[7] = 0x00; // not used
  
  len = 1;
  
  retvalue = canSend( txhandle,
		      &command,
		      &len);
  
  if (retvalue != NTCAN_SUCCESS)
    {
      printf("ERROR: canSend failed() with error %d!\n", retvalue);
      return(-1);
    }
  

  return(0);
}


//-----------------------------
// Send request for targets
//-----------------------------
int Radar::sendTargetRequest()
{
  CMSG command;
  int len; 
  int retvalue;

  // --------------------------
  // Send command for continuous broadcast
  // --------------------------
  command.id=0x660; //ID for tracks command
  command.len=0x08; //length of command is 8 bytes
  command.data[0] = 0x01; // command to request tracks (0x01 is targets; 0x07 is tracks)
  command.data[1] = numTargets; // number of targets to detect; 0x14 is max
  command.data[2] = 0x00; // normally set to 0
  command.data[3] = 0x00; // normally set to 0; 1 if requesting extended status message
  command.data[4] = 0x00; // normally set to 0; 1 if requestion extended target message
  command.data[5] = 0x00; // not used
  command.data[6] = 0xFF; // continuous sending of this message 0xFF
  command.data[7] = 0x00; // not used
  
  len = 1;
  
  retvalue = canSend( txhandle,
		      &command,
		      &len);
  
  if (retvalue != NTCAN_SUCCESS)
    {
      printf("ERROR: canSend failed() with error %d!\n", retvalue);
      return(-1);
    }
  

  return(0);
}


//-----------------------------
// Capture radar track information
//-----------------------------
int Radar::captureTracks()
{
  CMSG cmsg[40];

  int numFramesExpected = (int)( 2*numTracks );
  int numFramesGiven = numFramesExpected; //for upper limit
  int frameCounter=0;
  int retvalue;

  bool foundStart = false;
  
  char tmpByte;
  int tmpFrameNumber;
  uint tmpTrackNumber;

  uint8_t j;
  uint16_t rawVel = 0;
  int16_t rawVel_int = 0;
  double realVel = 0;
  uint temp_dist1, temp_dist2;
  uint rawDist;
  double realDist;
  double realYaw;
  int credibility;
  int status;

  int trackId;

  // clear tracks, the radar will give us what we need
  memset(tracks,0,sizeof(tracks));

  //this loop is so that we continually read off the CAN until our buffer
  //is filled with the right number of frames corresponding to the number 
  //of tracks we're expecting
  while(true)
  {  
    retvalue = canRead(rxhandle_tracks, &cmsg[0],&numFramesGiven,NULL);
    if(retvalue==NTCAN_RX_TIMEOUT)
    {
      printf("canRead() returned timeout\n");
      printf("is the radar on?\n");
      return(-1);
    }
    else if(retvalue!=NTCAN_SUCCESS)
    {
      printf("canRead() failed with error %d!\n", retvalue);
      return(-1);
    }
     
    tmpByte = cmsg[0].data[0];
    tmpFrameNumber = (int)( (tmpByte)&0x07 );
    tmpTrackNumber = (uint)( (tmpByte)>> 3 );
      
    //the start of the message will have frameNumber=4 & trackNumber=0
    if(tmpFrameNumber==4 && tmpTrackNumber==0)
      foundStart=true;	

    if(foundStart==true)
    {	  
      for(int k=0; k<numFramesGiven; k++) // len is length of frames received
	    {
	      for(int i=0; i<(int)(cmsg[k].len & 0x0f); i++) // each frame has 8 bytes
        {
          frame_buffer[frameCounter][i] = cmsg[k].data[i];		  
        }	      

	      frameCounter++;	      

	      if(frameCounter==numFramesExpected)				 
          break;		

	    }
    }
	 
    if(frameCounter==numFramesExpected)       
      break;
     
  }

  //int numDead = 0;

  //now that frame buffer is full and ordered, let's extract the data    
  for(uint8_t m=0; m<numTracks; m++)
  {
    j = m*2;      
      
    // trackId can be either the assigned order number or a unique identifier
    trackId = (uint)((uint8_t) ((frame_buffer[j+1][5])&0x1F)); //unique identifier
    //cout << "Track index: " << (int)m << " and ID: " << trackId;
    //cout << " and that other thing: " << (int)(frame_buffer[j][0] >> 3) << endl;
    //trackId = m;

    // is track alive?
    status = ((frame_buffer[j][6] & 0xE0)>>5);
    if (status) {
      tracks[trackId].status = status;
      if (frame_buffer[j][7] & 0x10) //stationary?
	tracks[trackId].status |= 8;
    }
    else {
      tracks[trackId].status = 0;
      continue;
    }

    // extract velocity
    rawVel = frame_buffer[j][2];	  
    rawVel = rawVel << 8; 
    rawVel = rawVel | frame_buffer[j][3];
    rawVel_int = (int16_t)rawVel;
    realVel = rawVel_int*0.01f;
         
    // extract range
    temp_dist1 =  (uint)((uint8_t)frame_buffer[j][4]);
    temp_dist2 =  (uint)((uint8_t)frame_buffer[j][5]);      
    rawDist = temp_dist1*256+temp_dist2;      
    realDist = rawDist*0.01; 
      
    // extract yaw
    realYaw = (signed char)frame_buffer[j+1][4]*0.05f;

      
    credibility = (int)( ((frame_buffer[j][1])&0xFC)>>2 );
    //credibility = (int)( (frame_buffer[j+1][1]&0x1F));

    tracks[trackId].velocity =  realVel;
    tracks[trackId].range = realDist;
    tracks[trackId].yaw = realYaw*M_PI/180;	           
    tracks[trackId].credibility = credibility;
  }

  return(0);
}

//-----------------------------
// Capture radar target information
//-----------------------------
int Radar::captureTargets()
{

  CMSG cmsg[40];

  int numFramesExpected = (int)( 2*numTargets );
  int numFramesGiven = numFramesExpected; //for upper limit
  int frameCounter=0;
  int retvalue;

  bool foundStart = false;
  
  char tmpByte;
  int tmpFrameNumber;
  uint tmpTargetNumber;

  uint8_t j;
  uint16_t rawVel = 0;
  int16_t rawVel_int = 0;
  double realVel = 0;
  uint temp_dist1, temp_dist2;
  uint rawDist;
  double realDist;

  double realYaw;

  //this loop is so that we continually read off the CAN until our buffer
  //is filled with the right number of frames corresponding to the number 
  //of targets we're expecting
  while(true)
  {  
    retvalue = canRead(rxhandle_targets, &cmsg[0],&numFramesGiven,NULL);
    if(retvalue==NTCAN_RX_TIMEOUT)
    {
      printf("canRead() returned timeout\n");
      printf("is the radar on?\n");
      return(-1);
    }
    else if(retvalue!=NTCAN_SUCCESS)
    {
      printf("canRead() failed with error %d!\n", retvalue);
      return(-1);
    }
     
    tmpByte = cmsg[0].data[0];
    tmpFrameNumber = (int)( (tmpByte)&0x07 );
    tmpTargetNumber = (uint)( (tmpByte)>> 3 );
      
    //the start of the message will have frameNumber=4 & targetNumber=0
    if(tmpFrameNumber==4 && tmpTargetNumber==0)
      foundStart=true;	

    if(foundStart==true)
    {	  
      for(int k=0; k<numFramesGiven; k++) // len is length of frames received
	    {
	      for(int i=0; i<(int)(cmsg[k].len & 0x0f); i++) // each frame has 8 bytes
        {
          frame_buffer[frameCounter][i] = cmsg[k].data[i];		  
        }	      

	      frameCounter++;	      

	      if(frameCounter==numFramesExpected)
          break;	      
	      
	    }
    }
	 
    if(frameCounter==numFramesExpected)       
      break;
  }

  //now that frame buffer is full and ordered, let's extract the data    
  for(uint8_t m=0; m<numTargets; m++)
  {
    j = m*2;      

    // is target alive?
    if (!(frame_buffer[j][7] & 0x40)) {
      targets[m].status = 1;
    }
    else {
      targets[m].status = 0;
      continue;
    }

    // there is a second frame in j+1 but there is no important info there

    // extract velocity
    rawVel = frame_buffer[j][2];	  
    rawVel = rawVel << 8; 
    rawVel = rawVel | frame_buffer[j][3];
    rawVel_int = (int16_t)rawVel;
    realVel = rawVel_int*0.01f;
         
    // extract range
    temp_dist1 =  (uint)((uint8_t)frame_buffer[j][4]);
    temp_dist2 =  (uint)((uint8_t)frame_buffer[j][5]);      
    //rawDist = temp_dist1*256+temp_dist2;      
    rawDist = (temp_dist1 << 8) | temp_dist2;
    assert(rawDist>=0);
    realDist = (int)(rawDist)*0.01;
    assert(realDist>=0);

      
    // extract yaw
    realYaw = (signed char)frame_buffer[j][1]*0.05f;
      
    targets[m].velocity =  realVel;
    targets[m].range = realDist;
    targets[m].yaw = realYaw*M_PI/180;	           
  }

  return(0);
}

// -----------------------
// Get i-th track velocity
// -----------------------
double Radar::getTrackVelocity(int id)
{
  return(tracks[id].velocity);
}

// -----------------------
// Get i-th track range
// -----------------------
double Radar::getTrackRange(int id)
{
  return(tracks[id].range);
}

// -----------------------
// Get i-th track yaw
// -----------------------
double Radar::getTrackYaw(int id)
{
  return(tracks[id].yaw);
}

// --------------------------
// Get i-th track credibility
// --------------------------
int Radar::getTrackCredibility(int id)
{
  return(tracks[id].credibility);
}

// -----------------------
// Get i-th track status
// -----------------------
int Radar::getTrackStatus(int id)
{
  return(tracks[id].status);
}

// -----------------------
// Get i-th target velocity
// -----------------------
double Radar::getTargetVelocity(int i)
{
  return(targets[i].velocity);
}

// -----------------------
// Get i-th target range
// -----------------------
double Radar::getTargetRange(int i)
{
  return(targets[i].range);
}

// -----------------------
// Get i-th target yaw
// -----------------------
double Radar::getTargetYaw(int i)
{
  return(targets[i].yaw);
}

// -----------------------
// Get i-th target status
// -----------------------
int Radar::getTargetStatus(int i)
{
  return(targets[i].status);
}
// -----------------------
// Close all handles 
// -----------------------
int Radar::disconnect()
{
  canClose(txhandle);
  canClose(rxhandle_targets);
  canClose(rxhandle_tracks);
  return(0);
}


#ifdef RADAR_DRIVER_TEST

int main(int argc, char *argv[])
{

  int retvalue;

  Radar *driver;  
  driver = new Radar();

  //memset(tracks,0,sizeof(tracks));

  double velocity[ALLOWABLE_NUM_TARGETS];
  double range[ALLOWABLE_NUM_TARGETS];
  double yaw[ALLOWABLE_NUM_TARGETS];
  int credibility[ALLOWABLE_NUM_TARGETS];
  int status[ALLOWABLE_NUM_TARGETS];
  

  // Initialize radar 
  retvalue = driver->initRadar();  
  if(retvalue==-1) 
    { 
      printf("could not initialize radar. Abort.\n"); 
      return(-1);
    }

  // Set number of tracks to be 4
  driver->setNumTracks(5);
  printf("set the number of tracks OK\n");

  // Start send track request
  retvalue = driver->sendTrackRequest();
  if(retvalue==-1)
    {
      printf("could not send track request. Abort.\n");
      return(-1);
    }
  printf("sent track request OK\n");
  
  // Send initial state
  retvalue = driver->sendState(0,0);
  if(retvalue==-1)
    {
      printf("sendState failed. Abort.\n");
      return(-1);
    }
  printf("sent state information OK\n");


  // Start infinite loop
  for(;;)
    {      
      // grab tracks
      retvalue = driver->captureTracks();
      if(retvalue==-1)
	{
	  printf("getTracks failed. Abort.\n");
	  return(-1);
	}           

      // extract data from captured tracks
      for(int i=0; i<ALLOWABLE_NUM_TARGETS;i++)
	{
	  velocity[i] = driver->getTrackVelocity(i);
	  range[i] = driver->getTrackRange(i);
	  yaw[i] = driver->getTrackYaw(i);
	  credibility[i] = driver->getTrackCredibility(i);
	  status[i] = driver->getTrackStatus(i);
	}      

      // send state
      retvalue = driver->sendState(0,0);
      if(retvalue==-1)
	{
	  printf("sendState failed. Abort.\n");
	  return(-1);
	}          

      // print tracks
      for(int i=0; i<ALLOWABLE_NUM_TARGETS; i++)
	{
	  printf("track[%d].velocity: %f\n", i, velocity[i]);
	  printf("track[%d].range: %f\n", i, range[i]);
	  printf("track[%d].yaw: %f\n", i, yaw[i]);
	  printf("track[%d].credibility: %d\n", i, credibility[i]);
	  printf("track[%d].status: %d\n", i, status[i]);
	}
      
    }

  return 0;
}

#endif
