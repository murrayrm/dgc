#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <GL/glut.h>
#include <sys/types.h>
#include <string.h>
#include <interfaces/sn_types.h>
#include <dgcutils/DGCutils.hh>
#include <assert.h>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/RadarRangeBlob.h>
#include <alice/AliceConstants.h>

using namespace std;

#define XMIN  		-25
#define XMAX   		 50
#define YMIN  		-20
#define YMAX   		 20

const int WINDOW_WIDTH=(XMAX-XMIN)*20;
const int WINDOW_HEIGHT=(YMAX-YMIN)*20;

struct point2D
{
  double x;
  double y;
};

//==================
// SENSNET VARIABLES
//==================
sensnet_t *sensnet;
char* spreadDaemon;
int skynetKey;

//==================
// BLOB VARIABLES
//==================
RadarRangeBlob* blob = new RadarRangeBlob();
bool TRACKS = false;
bool TARGETS = false;

//==================
// OPENGL VARIABLES
//==================
float lastx, lasty;
bool lbuttondown = false;
bool rbuttondown = false;


void drawString (char *s)
{
  unsigned int i;
  for (i = 0; i < strlen (s); i++)
    glutBitmapCharacter (GLUT_BITMAP_HELVETICA_12, s[i]);
}

void drawCircle(double x_c, double y_c, double R)
{
  uint NUM_PTS;

  if(R>0.25)
    NUM_PTS = 100;
  else
    NUM_PTS = 50;

  double theta_tmp, x_tmp, y_tmp;
  glLineWidth(1.5);
  glColor3f(1.0,0.0,0.0);
  glBegin(GL_LINE_LOOP);
  for(uint i=0; i<NUM_PTS; i++)
    {
      theta_tmp =(double)i/NUM_PTS*2*M_PI;
      x_tmp = R*cos(theta_tmp)+x_c;
      y_tmp = R*sin(theta_tmp)+y_c;
      glVertex2f(x_tmp,y_tmp);
    }
  glEnd();

}

void display(void)
{
  static char label[100];

  glClearColor (0.0,0.0,0.0,1.0); //clear the screen to black
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //clear the color buffer and the depth buffer


  //====================
  /* draw the floor */
  //====================
  glBegin(GL_QUADS);
  glColor3f(0.0f, 0.0f, 0.0f);
  glVertex2f(XMIN, YMIN);
  glVertex2f(XMIN, YMAX);
  glVertex2f(XMAX, YMAX);
  glVertex2f(XMAX, YMIN);		
  glEnd();
  
  
  //====================
  //draw coordinate axes
  //====================
  glLineWidth (4);
  glColor3f (0.0F, 0.0F, 1.0F);
  glBegin(GL_LINES);
  glVertex2f(0,0);
  glVertex2f(XMAX,0);
  glVertex2f(0,YMAX);
  glVertex2f(0,YMIN);
  glEnd();


  //====================
  //draw fine grid
  //====================
  //horizontal lines
  glLineWidth (0.5);
  for (int i= YMIN; i < YMAX+1; i++)
    {
      glBegin (GL_LINES);
      glColor3f (0.25F, 0.25F, 0.25F);
      glVertex2f ( 0, i);
      glVertex2f ( XMAX, i);
      glEnd ();

      sprintf(label, "%d", i);
      glColor3f (0.0F, 1.0F, 0.0F);
      glRasterPos2f(-0.20,i+0.05);
      drawString(label);  

    };

  //vertical lines
  glLineWidth (0.5);
  for (int i= 0; i < XMAX+1; i++)
    {
      glBegin (GL_LINES);
      glColor3f (0.25F, 0.25F, 0.25F);
      glVertex2f (i, YMIN);
      glVertex2f (i, YMAX);
      glEnd ();

      sprintf(label, "%d", i);
      glColor3f (0.0F, 1.0F, 0.0F);
      glRasterPos2f(i+0.05,-0.20);
      drawString(label);  
    };


  //====================
  //draw field of view
  //====================
  glLineWidth (0.5);
  glBegin(GL_LINES);
  glColor3f(1.0f,1.0f,0.0f);
  glVertex2f(0.0f,0.0f);
  glVertex2f(XMAX, (XMAX/cos(-6.35*M_PI/180))*sin(-6.35*M_PI/180));
  glEnd();

  glBegin(GL_LINES);
  glColor3f(1.0f,1.0f,0.0f);
  glVertex2f(0.0f,0.0f);
  glVertex2f(XMAX, (XMAX/cos(6.4*M_PI/180))*sin(6.4*M_PI/180));
  glEnd();

  //====================
  //draw chart box
  //====================
  //vertical lines
  glColor3f (0.0F, 1.0F, 0.0F);
  glLineWidth (0.5);
  glBegin(GL_LINES);
  glVertex2f (XMIN+0.5, YMIN+6);
  glVertex2f (XMIN+0.5, YMAX-3);
  glEnd();

  glColor3f (0.0F, 1.0F, 0.0F);
  glLineWidth (0.5);
  glBegin(GL_LINES);
  glVertex2f (XMIN+3.5, YMIN+6);
  glVertex2f (XMIN+3.5, YMAX-3);
  glEnd();
  
  glLineWidth (0.5);
  glBegin(GL_LINES);
  glVertex2f (XMIN+6.75, YMIN+6);
  glVertex2f (XMIN+6.75, YMAX-3);
  glEnd();

  glLineWidth (0.5);
  glBegin(GL_LINES);
  glVertex2f (XMIN+12, YMIN+6);
  glVertex2f (XMIN+12, YMAX-3);
  glEnd();

  glLineWidth (0.5);
  glBegin(GL_LINES);
  glVertex2f (XMIN+17, YMIN+6);
  glVertex2f (XMIN+17, YMAX-3);
  glEnd();

  glLineWidth (0.5);
  glBegin(GL_LINES);
  glVertex2f (XMIN+21, YMIN+6);
  glVertex2f (XMIN+21, YMAX-3);
  glEnd();

  //horizontal lines
  glLineWidth (0.5);
  double x_level;
  double step = 1.5;
  for (int i= 0; i < 21; i++)
    {
      x_level = YMIN+6+i*step;
      glBegin (GL_LINES);
      glVertex2f (XMIN+0.5, x_level);
      glVertex2f (XMIN+21, x_level);
      glEnd ();
    };

  //==================================
  //print radar data & plot radar data
  //==================================
  if(TRACKS)
    {
      for (int i= 0; i < RADAR_BLOB_MAX_TRACKS; i++)
	{     
	  sprintf(label, "%s %d","trk:",i); 
	  glColor3f (0.0F, 1.0F, 0.0F);
	  glRasterPos2f(XMIN+0.75,(YMAX-5)-i*1.5);
	  drawString(label);  

	  sprintf(label, "%s","track No." );
	  glColor3f (0.0F, 1.0F, 0.0F);
	  glRasterPos2f(XMIN+3.75,(YMAX-3.75));
	  drawString(label);  

	}
    }

  if(TARGETS)
    {
      for (int i= 0; i < RADAR_BLOB_MAX_TARGETS; i++)
	{     
	  sprintf(label, "%s %d","tgt:",i); 
	  glColor3f (0.0F, 1.0F, 0.0F);
	  glRasterPos2f(XMIN+0.75,(YMAX-5)-i*1.5);
	  drawString(label);  

	  sprintf(label, "%s","target No." );
	  glColor3f (0.0F, 1.0F, 0.0F);
	  glRasterPos2f(XMIN+3.75,(YMAX-3.75));
	  drawString(label);  

	}
    }

  sprintf(label, "%s","velocity" );
  glColor3f (0.0F, 1.0F, 0.0F);
  glRasterPos2f(XMIN+7.75,(YMAX-3.75));
  drawString(label);  

  sprintf(label, "%s","range" );
  glColor3f (0.0F, 1.0F, 0.0F);
  glRasterPos2f(XMIN+13.5,(YMAX-3.75));
  drawString(label);  

  sprintf(label, "%s","yaw" );
  glColor3f (0.0F, 1.0F, 0.0F);
  glRasterPos2f(XMIN+18.5,(YMAX-3.75));
  drawString(label);  

  double value;
  int value_int;
  
  point2D pose;

  if(TRACKS)
    {
      for (int i= 0; i < RADAR_BLOB_MAX_TRACKS; i++)
	{     
	  
	  if(blob->tracks[i].status==1)
	    {
	      blob->tracks[i].status=0;
	      
	      pose.x = blob->tracks[i].range*cos(blob->tracks[i].yaw);
	      pose.y = blob->tracks[i].range*sin(blob->tracks[i].yaw);
	      
	      //draw radar data
	      drawCircle(pose.x,pose.y,0.25);
	      drawCircle(pose.x,pose.y,0.05);	 
	      
	      //fill track ID data
	      value_int = i;
	      sprintf(label, "%d",value_int); 
	      glColor3f (0.0F, 1.0F, 0.0F);
	      glRasterPos2f(XMIN+3.75,(YMAX-5)-i*1.5);
	      drawString(label);  
	      
	      //fill velocity data
	      value = blob->tracks[i].velocity;
	      sprintf(label, "%3.3f",value); 
	      glColor3f (0.0F, 1.0F, 0.0F);
	      glRasterPos2f(XMIN+7.50,(YMAX-5)-i*1.5);
	      drawString(label); 
	      
	      //fill range data
	      value = blob->tracks[i].range;
	      sprintf(label, "%3.3f",value); 
	      glColor3f (0.0F, 1.0F, 0.0F);
	      glRasterPos2f(XMIN+13.25,(YMAX-5)-i*1.5);
	      drawString(label);  

	      //fill yaw data
	      value = blob->tracks[i].yaw*180/M_PI;
	      sprintf(label, "%3.3f",value); 
	      glColor3f (0.0F, 1.0F, 0.0F);
	      glRasterPos2f(XMIN+18.00,(YMAX-5)-i*1.5);
	      drawString(label);  
	      
	    }      
	  else
	    {
	      //fill track ID data
	      sprintf(label, "%s","NULL"); 
	      glColor3f (0.0F, 1.0F, 0.0F);
	      glRasterPos2f(XMIN+3.75,(YMAX-5)-i*1.5);
	      drawString(label);  
	      
	      //fill velocity data
	      sprintf(label, "%s","NULL"); 
	      glColor3f (0.0F, 1.0F, 0.0F);
	      glRasterPos2f(XMIN+7.50,(YMAX-5)-i*1.5);
	      drawString(label); 
	      
	      //fill range data
	      sprintf(label, "%s","NULL"); 
	      glColor3f (0.0F, 1.0F, 0.0F);
	      glRasterPos2f(XMIN+13.25,(YMAX-5)-i*1.5);
	      drawString(label);  
	      
	      //fill yaw data
	      sprintf(label, "%s","NULL"); 
	      glColor3f (0.0F, 1.0F, 0.0F);
	      glRasterPos2f(XMIN+18.00,(YMAX-5)-i*1.5);
	      drawString(label); 
	    }
	}

    }
  
  if(TARGETS)
    {
      
      for (int i= 0; i < RADAR_BLOB_MAX_TARGETS; i++)
	{     	  
	  if(blob->targets[i].status==1)
	    {
	      blob->targets[i].status=0;
	      
	      pose.x = blob->targets[i].range*cos(blob->targets[i].yaw);
	      pose.y = blob->targets[i].range*sin(blob->targets[i].yaw);
	      
	      //draw radar data
	      drawCircle(pose.x,pose.y,0.25);
	      drawCircle(pose.x,pose.y,0.05);	 
	      
	      //fill track ID data
	      value_int = i;
	      sprintf(label, "%d",value_int); 
	      glColor3f (0.0F, 1.0F, 0.0F);
	      glRasterPos2f(XMIN+3.75,(YMAX-5)-i*1.5);
	      drawString(label);  
	      
	      //fill velocity data
	      value = blob->targets[i].velocity;
	      sprintf(label, "%3.3f",value); 
	      glColor3f (0.0F, 1.0F, 0.0F);
	      glRasterPos2f(XMIN+7.50,(YMAX-5)-i*1.5);
	      drawString(label); 
	      
	      //fill range data
	      value = blob->targets[i].range;
	      sprintf(label, "%3.3f",value); 
	      glColor3f (0.0F, 1.0F, 0.0F);
	      glRasterPos2f(XMIN+13.25,(YMAX-5)-i*1.5);
	      drawString(label);  

	      //fill yaw data
	      value = blob->targets[i].yaw*180/M_PI;
	      sprintf(label, "%3.3f",value); 
	      glColor3f (0.0F, 1.0F, 0.0F);
	      glRasterPos2f(XMIN+18.00,(YMAX-5)-i*1.5);
	      drawString(label);  
	      
	    }      
	  else
	    {
	      //fill track ID data
	      sprintf(label, "%s","NULL"); 
	      glColor3f (0.0F, 1.0F, 0.0F);
	      glRasterPos2f(XMIN+3.75,(YMAX-5)-i*1.5);
	      drawString(label);  
	      
	      //fill velocity data
	      sprintf(label, "%s","NULL"); 
	      glColor3f (0.0F, 1.0F, 0.0F);
	      glRasterPos2f(XMIN+7.50,(YMAX-5)-i*1.5);
	      drawString(label); 
	      
	      //fill range data
	      sprintf(label, "%s","NULL"); 
	      glColor3f (0.0F, 1.0F, 0.0F);
	      glRasterPos2f(XMIN+13.25,(YMAX-5)-i*1.5);
	      drawString(label);  
	      
	      //fill yaw data
	      sprintf(label, "%s","NULL"); 
	      glColor3f (0.0F, 1.0F, 0.0F);
	      glRasterPos2f(XMIN+18.00,(YMAX-5)-i*1.5);
	      drawString(label); 
	    }
	}

    }

  glutSwapBuffers();
  
}

void resize(int w, int h)
{
  glLoadIdentity();
  glViewport (0, 0, w, h); //set the viewport to the current window specifications
  glMatrixMode(GL_PROJECTION);
  gluOrtho2D(XMIN,XMAX,YMIN,YMAX);
}

void mouse(int button, int state, int x, int y)
{

  if(button == GLUT_LEFT_BUTTON)
    {
      if(state == GLUT_DOWN)
	{
	  lbuttondown = true;
	}
      else
	{
	  lbuttondown = false;
	}
    }

  if(button == GLUT_RIGHT_BUTTON)
    {
      if(state == GLUT_DOWN)
	{
	  rbuttondown = true;
	}
      else
	{
	  rbuttondown = false;
	}
    }
  

}

void motion(int x, int y)
{
  if (lbuttondown)
    {
      float diffx=(x-lastx)/70; //check the difference between the current x and the last x position  
      float diffy=(y-lasty)/70;

      lastx = x;
      lasty = y;

      glTranslatef(diffx, -diffy, 0);

      glutPostRedisplay();     
    }

  if (rbuttondown)
    {     

      GLint _viewport[4];
      GLdouble _modelMatrix[16];
      GLdouble _projMatrix[16];
      GLdouble objx, objy, objz;

      glGetIntegerv(GL_VIEWPORT,_viewport);
      glGetDoublev(GL_PROJECTION_MATRIX, _projMatrix);
      glGetDoublev(GL_MODELVIEW_MATRIX, _modelMatrix);

      gluUnProject(WINDOW_WIDTH/2,WINDOW_HEIGHT/2,0,_modelMatrix,_projMatrix,_viewport,&objx, &objy, &objz);

      float dy=(y-lasty); //check the difference between the current y and the last y position  

      double s = exp((double)dy*0.01);

      glTranslatef(objx,objy,0);
      glScalef(s,s,s);
      glTranslatef(-objx,-objy,0);

      lasty = y;
      lastx = x;

      glutPostRedisplay();  



    }
  
}


void motionPassive(int x, int y)
{
  lastx = x;
  lasty = y;
}


void readMessage( void )
{

  while(true)
    {         
      int blobId, blobLen;
      
      // Look at the latest data
      if (sensnet_peek(sensnet, SENSNET_RADAR, SENSNET_RADAR_BLOB, &blobId, &blobLen) != 0)
	continue;
      
      // Is the data valid?
      if (blobId < 0)	
	continue;            
      
      // read in the data
      memset(blob, 0, sizeof(*blob)); // is this necessary? 
      if (sensnet_read(sensnet, SENSNET_RADAR, SENSNET_RADAR_BLOB, &blobId, sizeof(*blob), blob) != 0)
	  continue;            
    }
}



void idle (void)
{

  // read message then plot
  readMessage();
  glutPostRedisplay();
  
}

int init(void)
{
  // Fill out the spread name
  if (getenv("SPREAD_DAEMON"))
    {
      spreadDaemon = getenv("SPREAD_DAEMON");
    }
  else
    {
      printf("unknown Spread daemon: please set SPREAD_DAEMON");
      return(-1);
    }

  // Fill out the skynet key
  if (getenv("SKYNET_KEY"))
    {
      skynetKey = atoi(getenv("SKYNET_KEY"));      
    }
  else
    {
      printf("unknown SKYNET_KEY: please set SKYNET_KEY");
      return(-1);
    }

  // Create sensnet interface
  sensnet = sensnet_alloc();
  assert(sensnet);
  if (sensnet_connect(sensnet, spreadDaemon, skynetKey, 300) != 0) //some random moduleID number
    return(-1);

  // Join group
  if(sensnet_join(sensnet, SENSNET_RADAR, SENSNET_RADAR_BLOB, sizeof(*blob))!=0)
    {
     printf("unable to join group... \n");
     return(-1);
    }

  memset(blob,0,sizeof(*blob));  

  return(0);
  
}

int main(int argc, char **argv)
{  

  if(argc!=2)
    {
      printf("--- number of arguments invalid!\n");
      printf("    syntax should be:  ./radarViewer <option>\n");
      printf("    where <option> is either 'tracks' or 'targets'\n");      
    }
  else
    {
      if(strcmp(argv[1],"tracks")==0)
	{
	  TRACKS=true;
	  TARGETS=false;
	}
      else if(strcmp(argv[1],"targets")==0)
	{
	  TARGETS=true;
	  TRACKS=false;
	}
      else
	{
	  printf("--- argument invalid!\n");
	  printf("    syntax should be:  ./radarViewer <option>\n");
	  printf("    where <option> is either 'tracks' or 'targets'\n");
	}
    }
  

  if(init()!=0)
    return(-1);

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
  glutInitWindowPosition(40, 40);
  glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
  glutCreateWindow("RadarViewer");
  
  glClearColor(0.0, 0.0, 0.0, 0.0); //black bkgrnd
  glColor3f(1.0, 1.0, 1.0);
  
  glutDisplayFunc(display); 

  glutMouseFunc(mouse);
  glutMotionFunc(motion);
  glutPassiveMotionFunc(motionPassive);
  
  glutReshapeFunc(resize);
  glutIdleFunc(idle);
  
  glutMainLoop();
  
  return 0;
  
}
