/*!
 * \file radar_diag.hh
 * \brief Diagnostic/interface program for the TRW radar
 * \author Tamas Szalay
 * \date 10 July 2007
 *
 * Shamelessly copied from Jeremy's radar driver (for serial code), 
 * this is a basic program for reading/writing the radar's 
 * internal configuration values (various offsets, thresholds, etc)
*/

#include "radar_diag.hh"
#include <assert.h>

using namespace std;

// Default constructor
RadarDiag::RadarDiag()
{

  memset(this, 0, sizeof(this));

  // ----------------------------------------------
  // Initialize private variables to default values
  // ----------------------------------------------
  yawcounter = 0;
  
  baud = NTCAN_BAUD_500;

  net=0;                  /* logical netnumber */
  mode=0;                 /* mode for canOpen */
  txqueuesize=40;         /* maximum number of messages to transmit = 40 since 20 moving objects and 2 frames/object */
  rxqueuesize=40;         /* maximum number of messages to receive  = 40 since 20 moving objects and 2 frames/object */
  txtimeout=100;          /* timeout for transmit */
  rxtimeout=10000;        /* timeout for receiving data */  

  return;
}

// Destructor
RadarDiag::~RadarDiag()
{
  // Disconnect from CAN
  canClose(txhandle);
  canClose(rxhandle_targets);
  canClose(rxhandle_tracks);
  canClose(rxhandle_param);
  canClose(rxhandle_status);

  // Clean up console windows
  delwin(data_win);
  delwin(input_win);
  endwin();

  return;
}

// Initialize the radar
int RadarDiag::initRadarDiag()
{
  int retvalue;
  
  // ------------------------------------
  // Open CAN handle for sending messages
  // ------------------------------------
  retvalue = canOpen(net,
		     mode,
		     txqueuesize,
		     rxqueuesize,
		     txtimeout,
		     rxtimeout,
		     &txhandle);
  
  if(retvalue!=NTCAN_SUCCESS)
    {      
      dout << "canOpen() transmit handle failed with error " 
	   << retvalue << endl;
      return(-1);
    }
  else    
    dout << "opened transmit handle OK !\n";
  
  // ------------------------
  // Set Baudrate for sending
  // ------------------------
  retvalue = canSetBaudrate(txhandle,baud);
  if(retvalue!=0)
    {
      dout << "canSetBaudrate() failed with error " 
	   << retvalue << endl;
      canClose(txhandle);
      return(-1);
    }
  else    
    dout << "function canSetBaudrate() returned OK ! \n";
    


  /// ----------------------------------
  /// Open CAN handle for reading tracks
  /// ----------------------------------
  retvalue = canOpen(net,
		     mode,
		     txqueuesize,
		     rxqueuesize,
		     txtimeout,
		     rxtimeout,
		     &rxhandle_tracks);

  if(retvalue!=NTCAN_SUCCESS)
    {      
      dout << "canOpen() track handle failed with error "
	   << retvalue << endl;
      return(-1);
    }
  else
    dout << "opened track handle OK !\n";

  // -------------------------------
  // Add handle ID for track message
  // -------------------------------
  retvalue = canIdAdd(rxhandle_tracks,0x663);
  if(retvalue!=NTCAN_SUCCESS)
    {
      printf("canIdAdd() failed with error %d!\n", retvalue);
      printf(" could not add handle ID for general status message.\n");
      canClose(rxhandle_tracks);
      return(-1);
    }
 
  /// ------------------------------------------
  /// Open CAN handle for reading target message
  /// ------------------------------------------
  retvalue = canOpen(net,
		     mode,
		     txqueuesize,
		     rxqueuesize,
		     txtimeout,
		     rxtimeout,
		     &rxhandle_targets);

  if(retvalue!=NTCAN_SUCCESS)
    {      
      printf("canOpen() target handle failed with error %d!\n", retvalue);
      return(-1);
    }
  else
    printf("opened target handle OK !\n");

  // --------------------------------
  // Add handle ID for target message
  // --------------------------------
  retvalue = canIdAdd(rxhandle_targets,0x662);
  if(retvalue!=NTCAN_SUCCESS)
    {
      printf("canIdAdd() failed with error %d!\n", retvalue);
      printf(" could not add handle ID for general status message.\n");
      canClose(rxhandle_targets);
      return(-1);
    }

  /// ------------------------------------------
  /// Open CAN handle for reading status message
  /// ------------------------------------------
  retvalue = canOpen(net,
		     mode,
		     txqueuesize,
		     rxqueuesize,
		     txtimeout,
		     rxtimeout,
		     &rxhandle_status);

  if(retvalue!=NTCAN_SUCCESS)
    {      
      printf("canOpen() target handle failed with error %d!\n", retvalue);
      return(-1);
    }
  else
    printf("opened target handle OK !\n");

  // --------------------------------
  // Add handle ID for status message
  // --------------------------------
  retvalue = canIdAdd(rxhandle_status,0x661);
  if(retvalue!=NTCAN_SUCCESS)
    {
      printf("canIdAdd() failed with error %d!\n", retvalue);
      printf(" could not add handle ID for general status message.\n");
      canClose(rxhandle_status);
      return(-1);
    }

  /// ------------------------------------------
  /// Open CAN handle for reading param message
  /// ------------------------------------------
  retvalue = canOpen(net,
		     mode,
		     txqueuesize,
		     rxqueuesize,
		     txtimeout,
		     rxtimeout,
		     &rxhandle_param);

  if(retvalue!=NTCAN_SUCCESS)
    {      
      printf("canOpen() target handle failed with error %d!\n", retvalue);
      return(-1);
    }
  else
    printf("opened target handle OK !\n");

  // --------------------------------
  // Add handle ID for param message
  // --------------------------------
  retvalue = canIdAdd(rxhandle_param,0x664);
  if(retvalue!=NTCAN_SUCCESS)
    {
      printf("canIdAdd() failed with error %d!\n", retvalue);
      printf(" could not add handle ID for general status message.\n");
      canClose(rxhandle_param);
      return(-1);
    }


  /// ------------------------------------------
  /// Open CAN handle for reading CCP messages
  /// ------------------------------------------
  retvalue = canOpen(net,
		     mode,
		     txqueuesize,
		     rxqueuesize,
		     txtimeout,
		     rxtimeout,
		     &rxhandle_CCP);

  if(retvalue!=NTCAN_SUCCESS)
    {      
      printf("canOpen() target handle failed with error %d!\n", retvalue);
      return(-1);
    }
  else
    printf("opened target handle OK !\n");

  // --------------------------------
  // Add handle ID for CCP DSP message
  // --------------------------------
  retvalue = canIdAdd(rxhandle_CCP,0x710);
  if(retvalue!=NTCAN_SUCCESS)
    {
      printf("canIdAdd() failed with error %d!\n", retvalue);
      printf(" could not add handle ID for general status message.\n");
      canClose(rxhandle_param);
      return(-1);
    }
  // handle ID for H8 message, whatever that is
  retvalue = canIdAdd(rxhandle_CCP,0x6FF);
  if(retvalue!=NTCAN_SUCCESS)
    {
      printf("canIdAdd() failed with error %d!\n", retvalue);
      printf(" could not add handle ID for general status message.\n");
      canClose(rxhandle_param);
      return(-1);
    }
  // handle ID for just DSP
  retvalue = canIdAdd(rxhandle_CCP,0x48);
  if(retvalue!=NTCAN_SUCCESS)
    {
      printf("canIdAdd() failed with error %d!\n", retvalue);
      printf(" could not add handle ID for general status message.\n");
      canClose(rxhandle_param);
      return(-1);
    }
  // handle ID for just H8
  retvalue = canIdAdd(rxhandle_CCP,0x49);
  if(retvalue!=NTCAN_SUCCESS)
    {
      printf("canIdAdd() failed with error %d!\n", retvalue);
      printf(" could not add handle ID for general status message.\n");
      canClose(rxhandle_param);
      return(-1);
    }

  return(0);
}

/// --------------------------
/// Initialize ncurses console
/// --------------------------
int RadarDiag::initConsole()
{
  initscr();
  // Curses returns each character as it is typed, so no backspace and such
  cbreak();
  noecho();

  // Time-out after 0.2 seconds
  halfdelay(2);
  
  // Create data, input windows
  input_win = newwin(2, COLS, LINES-2, 0);
  data_win = newwin(LINES-2, COLS, 0, 0);
  scrollok(data_win, TRUE);
  wsetscrreg(data_win, 0, LINES-3);
  
  // Refresh to set up everything
  wrefresh(data_win);

  // Set output to hex mode
  dout << hex;

  // Erase string
  inputStr.clear();

  // Initialize input
  startUserInput();

  return 0;
}

/// --------------------------
/// Begin user input cycle
/// --------------------------
void RadarDiag::startUserInput()
{
  werase(input_win);

  mvwhline(input_win, 0, 0, '-', COLS);

  wmove(input_win, 1, 0);
  waddstr(input_win, ">> ");

  wrefresh(input_win);
}

/// --------------------------
/// Capture user input
/// --------------------------
bool RadarDiag::getUserInput(string &result)
{
  int rc;

  rc = wgetch(input_win);
  if (rc == ERR)
    return false;

  if (isprint(rc)) {
    inputStr += rc;
    waddch(input_win, rc);
    return false;
  }
  else if (rc == ASCII_Enter) {
    if (inputStr.size() > 0) {
      result = inputStr;
      inputStr.clear();
      startUserInput();
      return true;
    }
  }
  else if (rc == ASCII_Backspace) {
    if (inputStr.length() > 0)
      inputStr = inputStr.substr(0,inputStr.length()-1);
    startUserInput();
    waddstr(input_win, inputStr.c_str());
  }
  else if (rc == ASCII_Escape) {
    inputStr.clear();
    startUserInput();
  }

  return false;
}

/// --------------------------
/// Outputs message buffer to main window
/// --------------------------
void RadarDiag::refreshOutput()
{
  long p = dout.tellp();
  if (p == 0)
    return;

  string msg = dout.str();    
  waddstr(data_win, msg.c_str());

  wrefresh(data_win);
  dout.str("");
}

/// --------------------------
/// Send State to radar
/// --------------------------
int RadarDiag::sendState(uint8_t speed, uint8_t yawrate)
{
  uint8_t MSB_speed = 0;
  uint8_t LSB_speed = speed;

  int len, retvalue;                 /* Bufsize in number of messages for canRead() */
  CMSG wheelSpeed, yawSpeed;              /* can message buffer */

  dout << "SEND state (" << (int)speed << "," << (int)yawrate << ")" << endl;

  //-----------------------
  //send wheel speed
  //-----------------------
  wheelSpeed.id=0x4A0;
  wheelSpeed.len=0x08;

  // next two bytes deal with FL wheel speed
  wheelSpeed.data[0] = LSB_speed;
  wheelSpeed.data[1] = MSB_speed; 

  // next two bytes deal with FR wheel speed
  wheelSpeed.data[2] = LSB_speed;
  wheelSpeed.data[3] = MSB_speed;

  // next two bytes deal with RL wheel speed
  wheelSpeed.data[4] = LSB_speed;
  wheelSpeed.data[5] = MSB_speed;

  // next two bytes deal with RR wheel speed
  wheelSpeed.data[6] = LSB_speed;
  wheelSpeed.data[7] = MSB_speed;
  
  len = 1;
  
  retvalue = canSend(txhandle,&wheelSpeed,&len);
  if(retvalue != NTCAN_SUCCESS)
    {
      dout << "ERROR: unable to send wheel speed!!\n";  
      return(-1);   
    }

  // now we store the yawrate
  uint8_t MSB_yaw = 0;
  uint8_t LSB_yaw = yawrate;
  uint8_t send_yawcounter = 0;
  uint8_t checksum = 0;

  //-----------------------
  //need to set the valid sign 
  //-----------------------
  // in MSB_yaw bit 6 (for bits 0-7); 0 is valid, 1 is invalid
  // need to mask the MSB_yaw with 10111111(i.e. 0xBF)
  MSB_yaw = (MSB_yaw & 0xBF);


  //-----------------------
  //send yaw rate
  //-----------------------
  yawSpeed.id=0x4A8;
  yawSpeed.len=0x08;

  // next two bytes deal with the yawrate
  yawSpeed.data[0] = LSB_yaw; 
  yawSpeed.data[1] = MSB_yaw;
  
  // next 4 bytes are not used
  yawSpeed.data[2] = 0x00;
  yawSpeed.data[3] = 0x00;
  yawSpeed.data[4] = 0x00;
  yawSpeed.data[5] = 0x00;

  // next byte deals with counter for overflowing message counter
  // as sign of life
  send_yawcounter = yawcounter & 0xFF;
  send_yawcounter = send_yawcounter << 4;
  yawSpeed.data[6] = yawcounter;

  // last byte is checksum
  for(int i=0; i<8; i++)
  {
    checksum = yawSpeed.data[i] + checksum;
  }
  yawSpeed.data[7] = checksum;

  len = 1;
  
  retvalue = canSend(txhandle,&yawSpeed,&len);
  if(retvalue != NTCAN_SUCCESS)
    {
      dout << "ERROR: unable to send yaw rate!!\n";
      return(-1);
    }

  yawcounter++;

  if(yawcounter>15)
    yawcounter=0;

  return(0);

}

//-----------------------------
// Send request for tracks
//-----------------------------
int RadarDiag::sendTrackRequest(uint8_t count, uint8_t repeats)
{
  CMSG command;
  int len; 
  int retvalue;

  dout << "SEND track request (" << (int)count << "," << (int)repeats << ")" << endl;

  command.id=0x660; //ID for sending command
  command.len=0x08; //length of command is 8 bytes
  command.data[0] = 0x07; // command to request tracks (0x01 is targets; 0x07 is tracks)
  command.data[1] = count; // number of tracks to detect; 0x14 is max
  command.data[2] = 0x00; // normally set to 0
  command.data[3] = 0x00; // normally set to 0; 1 if requesting extended status message
  command.data[4] = 0x00; // normally set to 0; 1 if requestion extended target message
  command.data[5] = 0x00; // not used
  command.data[6] = repeats; // continuous sending of this message 0xFF
  command.data[7] = 0x00; // not used
  
  len = 1;
  
  retvalue = canSend( txhandle,
		      &command,
		      &len);
  
  if (retvalue != NTCAN_SUCCESS)
    {
      dout << "ERROR: canSend failed() with error "
	   << retvalue << endl;
      return(-1);
    }
  

  return(0);
}


//-----------------------------
// Send request for targets
//-----------------------------
int RadarDiag::sendTargetRequest(uint8_t count, uint8_t repeats)
{
  CMSG command;
  int len; 
  int retvalue;

  dout << "SEND target request (" << (int)count << "," << (int)repeats << ")" << endl;

  command.id=0x660; //ID for sending command
  command.len=0x08; //length of command is 8 bytes
  command.data[0] = 0x01; // command to request tracks (0x01 is targets; 0x07 is tracks)
  command.data[1] = count; // number of targets to detect; 0x14 is max
  command.data[2] = 0x00; // normally set to 0
  command.data[3] = 0x00; // normally set to 0; 1 if requesting extended status message
  command.data[4] = 0x00; // normally set to 0; 1 if requestion extended target message
  command.data[5] = 0x00; // not used
  command.data[6] = repeats; // continuous sending of this message 0xFF
  command.data[7] = 0x00; // not used
  
  len = 1;
  
  retvalue = canSend( txhandle,
		      &command,
		      &len);
  
  if (retvalue != NTCAN_SUCCESS)
    {
      dout << "ERROR: canSend failed() with error "
	   << retvalue << endl;
      return(-1);
    }
  

  return(0);
}

//-----------------------------
// Send abort command
//-----------------------------
int RadarDiag::sendAbort()
{
  CMSG command;
  int len; 
  int retvalue;

  dout << "SEND abort" << endl;

  command.id=0x660; //ID for sending command
  command.len=0x08; //length of command is 8 bytes
  command.data[0] = 0x00; // Abort
  command.data[1] = 0x00;
  command.data[2] = 0x00;
  command.data[3] = 0x00;
  command.data[4] = 0x00;
  command.data[5] = 0x00;
  command.data[6] = 0x00;
  command.data[7] = 0x00;
  
  len = 1;
  
  retvalue = canSend( txhandle,
		      &command,
		      &len);
  
  if (retvalue != NTCAN_SUCCESS)
    {
      dout << "ERROR: canSend failed() with error "
	   << retvalue << endl;
      return(-1);
    }
  

  return(0);
}

//-----------------------------
// Send validate command
//-----------------------------
int RadarDiag::sendValidate()
{
  CMSG command;
  int len; 
  int retvalue;

  dout << "SEND validate" << endl;

  command.id=0x660; //ID for sending command
  command.len=0x08; //length of command is 8 bytes
  command.data[0] = 0x04; // Validate
  command.data[1] = 0x00; 
  command.data[2] = 0x00; 
  command.data[3] = 0x00; 
  command.data[4] = 0x00; 
  command.data[5] = 0x00; 
  command.data[6] = 0x00; 
  command.data[7] = 0x00;
  
  len = 1;
  
  retvalue = canSend( txhandle,
		      &command,
		      &len);
  
  if (retvalue != NTCAN_SUCCESS)
    {
      dout << "ERROR: canSend failed() with error "
	   << retvalue << endl;
      return(-1);
    }
  

  return(0);
}

//-----------------------------
// Send clear faults command
//-----------------------------
int RadarDiag::sendClearFaults()
{
  CMSG command;
  int len; 
  int retvalue;

  dout << "SEND clear faults" << endl;

  command.id=0x660; //ID for sending command
  command.len=0x08; //length of command is 8 bytes
  command.data[0] = 0x06; // Clear faults
  command.data[1] = 0x00; 
  command.data[2] = 0x00; 
  command.data[3] = 0x00; 
  command.data[4] = 0x00; 
  command.data[5] = 0x00; 
  command.data[6] = 0x00; 
  command.data[7] = 0x00;
  
  len = 1;
  
  retvalue = canSend( txhandle,
		      &command,
		      &len);
  
  if (retvalue != NTCAN_SUCCESS)
    {
      dout << "ERROR: canSend failed() with error "
	   << retvalue << endl;
      return(-1);
    }
  

  return(0);
}

//-----------------------------
// Send read parameter command
//-----------------------------
int RadarDiag::sendReadParameter(uint8_t paramID)
{
  CMSG command;
  int len; 
  int retvalue;

  dout << "SEND read param " << (int)paramID << endl;

  command.id=0x660; //ID for sending command
  command.len=0x08; //length of command is 8 bytes
  command.data[0] = 0x08; // Read single parameter
  command.data[1] = paramID; // ID of parameter
  command.data[2] = 0x00; 
  command.data[3] = 0x00; 
  command.data[4] = 0x00; 
  command.data[5] = 0x00; 
  command.data[6] = 0x00; 
  command.data[7] = 0x00;
  
  len = 1;
  
  retvalue = canSend( txhandle,
		      &command,
		      &len);
  
  if (retvalue != NTCAN_SUCCESS)
    {
      dout << "ERROR: canSend failed() with error "
	   << retvalue << endl;
      return(-1);
    }
  

  return(0);
}

//-----------------------------
// Send write parameter command
//-----------------------------
int RadarDiag::sendWriteParameter(uint8_t paramID, uint8_t *value)
{
  CMSG command;
  int len; 
  int retvalue;

  dout << "SEND write param " << (int)paramID << endl;

  command.id=0x660; //ID for sending command
  command.len=0x08; //length of command is 8 bytes
  command.data[0] = 0x09; // Write single parameter
  command.data[1] = paramID; // ID of parameter
  command.data[2] = value[0]; // 32-bit parameter value
  command.data[3] = value[1];
  command.data[4] = value[2];
  command.data[5] = value[3];
  command.data[6] = 0x00; 
  command.data[7] = 0x00;
  
  len = 1;
  
  retvalue = canSend( txhandle,
		      &command,
		      &len);
  
  if (retvalue != NTCAN_SUCCESS)
    {
      dout << "ERROR: canSend failed() with error "
	   << retvalue << endl;
      return(-1);
    }
  

  return(0);
}

//-----------------------------
// Send CCP message
//-----------------------------
int RadarDiag::sendCCP(string msg)
{
  CMSG command;
  int len; 
  int retvalue;

  istringstream iss;
  string cmd;
  iss.str(msg);
  iss >> cmd;
  iss >> hex;

  int x=0,y=0;

  // read ID and length, mandatory - tries to read uint8_t as char
  iss >> x >> y;
  command.id = x;
  command.len = (uint8_t)y;

  for (int i=0;i<command.len;i++) {
    iss >> x;
    command.data[i] = (uint8_t)x;
  }

  for (int i=command.len; i<8; i++)
    command.data[i] = 0x00;

  dout << "SEND cpp id=" << (int)command.id << "  len="
       << (int)command.len << "   data: ";
  for (int i=0;i<8;i++)
    dout << (int)command.data[i] << " ";
  dout << endl;

  len = 1;
  
  retvalue = canSend( txhandle,
		      &command,
		      &len);
  
  if (retvalue != NTCAN_SUCCESS)
    {
      dout << "ERROR: canSend failed() with error "
	   << retvalue << endl;
      return(-1);
    }
  

  return(0);
}

//-----------------------------
// Check for parameter messages
//-----------------------------
int RadarDiag::captureParams()
{
  int len=1;
  CMSG msg;
  
  // Non-blocking message read
  int retvalue = canTake(rxhandle_param, &msg, &len);
  if(retvalue==NTCAN_RX_TIMEOUT)
  {
    printf("canTake() returned timeout\n");
    printf("is the radar on?\n");
    return(-1);
  }
  else if(retvalue!=NTCAN_SUCCESS)
  {
    printf("canTake() failed with error %d!\n", retvalue);
    return(-1);
  }
  
  // When buffer is empty, len = 0
  while (len != 0) {
    //    int index = getParamIndex(msg.data[1]);
    int value = *(int*)(msg.data+2);
    
    if (value == 0x5A5A5A5A) {
      dout << "PARAM READ " << (int)msg.data[1]
	   << ": INVALID PARAM" << endl;
      len = 1;
      canTake(rxhandle_param, &msg, &len);
      continue;
    }

    dout << "PARAM READ: " << (int)msg.data[1] 
	 << "\t    HEX: " << value << "   DEC: " 
	 << dec << value << hex;

    bool isString = true;
    // Check if the param could be a string
    for (int i=0;i<4;i++)
      if (msg.data[i+2] < 32)
	isString = false;
    if (isString)
      dout << "     STR: " << msg.data[2] << msg.data[3]
	   << msg.data[4] << msg.data[5];
    dout << endl;

    len = 1;
    canTake(rxhandle_param, &msg, &len);
  }

  return 0;
}

//-----------------------------
// Check for status messages
//-----------------------------
int RadarDiag::captureStatus()
{
  int len=1;
  CMSG msg;
  
  // Non-blocking message read
  int retvalue = canTake(rxhandle_status, &msg, &len);
  if(retvalue==NTCAN_RX_TIMEOUT)
  {
    printf("canTake() returned timeout\n");
    printf("is the radar on?\n");
    return(-1);
  }
  else if(retvalue!=NTCAN_SUCCESS)
  {
    printf("canTake() failed with error %d!\n", retvalue);
    return(-1);
  }

  // When buffer is empty, len = 0
  while (len != 0) {
    //int index = getParamIndex(msg.data[1]);
    //int value = *(int*)(msg.data+2);
    
    dout << "STATUS MESSAGE RECEIVED" << endl;

    len = 1;
    canTake(rxhandle_status, &msg, &len);
  }

  return 0;
}

//-----------------------------
// Check for CCP messages
//-----------------------------
int RadarDiag::captureCCP()
{
  int len=1;
  CMSG msg;
  
  // Non-blocking message read
  int retvalue = canTake(rxhandle_CCP, &msg, &len);
  if(retvalue==NTCAN_RX_TIMEOUT)
  {
    printf("canTake() returned timeout\n");
    printf("is the radar on?\n");
    return(-1);
  }
  else if(retvalue!=NTCAN_SUCCESS)
  {
    printf("canTake() failed with error %d!\n", retvalue);
    return(-1);
  }

  // When buffer is empty, len = 0
  while (len != 0) {
    dout << "CCP MSG: ID=" << msg.id;
    dout << "\t\t";
    for (int i=0;i<8;i++)
      dout << (int)msg.data[i] << " ";
    dout << endl;
    len = 1;
    canTake(rxhandle_CCP, &msg, &len);
  }

  return 0;
}

//-----------------------------
// Capture radar track information
//-----------------------------
int RadarDiag::captureTracks()
{
  CMSG cmsg[40];

  int numFramesExpected = 10;
  int numFramesGiven = numFramesExpected; //for upper limit
  int frameCounter=0;
  int retvalue;

  bool foundStart = false;
  
  char tmpByte;
  int tmpFrameNumber;
  uint tmpTrackNumber;

  uint8_t j;
  uint16_t rawVel = 0;
  int16_t rawVel_int = 0;
  double realVel = 0;

  uint temp_dist1, temp_dist2;
  uint rawDist;

  double realDist;
  double realYaw;
  int credibility;
  int trackId;

  retvalue = canTake(rxhandle_tracks, &cmsg[0], &numFramesGiven);
  dout << dec;
  if (numFramesGiven > 0)
    dout << numFramesGiven << " track messages received" << endl;
  dout << hex;
  return 0;

  //this loop is so that we continually read off the CAN until our buffer
  //is filled with the right number of frames corresponding to the number 
  //of tracks we're expecting
  while(true)
  {  
    retvalue = canRead(rxhandle_tracks, &cmsg[0],&numFramesGiven,NULL);
    if(retvalue==NTCAN_RX_TIMEOUT)
    {
      printf("canRead() returned timeout\n");
      printf("is the radar on?\n");
      return(-1);
    }
    else if(retvalue!=NTCAN_SUCCESS)
    {
      printf("canRead() failed with error %d!\n", retvalue);
      return(-1);
    }
     
    tmpByte = cmsg[0].data[0];
    tmpFrameNumber = (int)( (tmpByte)&0x07 );
    tmpTrackNumber = (uint)( (tmpByte)>> 3 );
      
    //the start of the message will have frameNumber=4 & trackNumber=0
    if(tmpFrameNumber==4 && tmpTrackNumber==0)
      foundStart=true;	

    if(foundStart==true)
    {	  
      for(int k=0; k<numFramesGiven; k++) // len is length of frames received
      {
	for(int i=0; i<(int)(cmsg[k].len & 0x0f); i++) // each frame has 8 bytes
	{
	  frame_buffer[frameCounter][i] = cmsg[k].data[i];		  
	}	      
	
	frameCounter++;	      
	
	if(frameCounter==numFramesExpected)				 
	  break;		
	
      }
    }
    
    if(frameCounter==numFramesExpected)       
      break;    
  }

  //now that frame buffer is full and ordered, let's extract the data    
  for(uint8_t m=0; m<10; m++)
  {
    j = m*2;      
      
    // trackId can be either the assigned order number or a unique identifier
    trackId = (uint)((uint8_t) ((frame_buffer[j+1][5])&0x1F)); //unique identifier

    // extract velocity
    rawVel = frame_buffer[j][2];	  
    rawVel = rawVel << 8; 
    rawVel = rawVel | frame_buffer[j][3];
    rawVel_int = (int16_t)rawVel;
    realVel = rawVel_int*0.01f;
         
    // extract range
    temp_dist1 =  (uint)((uint8_t)frame_buffer[j][4]);
    temp_dist2 =  (uint)((uint8_t)frame_buffer[j][5]);      
    rawDist = temp_dist1*256+temp_dist2;      
    realDist = rawDist*0.01; 
      
    // extract yaw
    realYaw = (signed char)frame_buffer[j+1][4]*0.05f;

      
    credibility = (int)( ((frame_buffer[j][1])&0xFC)>>2 );

    /*    tracks[trackId].velocity =  realVel;
    tracks[trackId].range = realDist;
    tracks[trackId].yaw = realYaw*M_PI/180;	           
    tracks[trackId].credibility = credibility;
    tracks[trackId].status = 1;*/
	
  }

  return(0);
}

//-----------------------------
// Capture radar target information
//-----------------------------
int RadarDiag::captureTargets()
{

  CMSG cmsg[40];

  int numFramesExpected = 10;
  int numFramesGiven = numFramesExpected; //for upper limit
  int frameCounter=0;
  int retvalue;

  bool foundStart = false;
  
  char tmpByte;
  int tmpFrameNumber;
  uint tmpTargetNumber;

  uint8_t j;
  uint16_t rawVel = 0;
  int16_t rawVel_int = 0;
  double realVel = 0;
  uint temp_dist1, temp_dist2;
  uint rawDist;
  double realDist;

  double realYaw;

  retvalue = canTake(rxhandle_targets, &cmsg[0], &numFramesGiven);
  dout << dec;
  if (numFramesGiven > 0)
    dout << numFramesGiven << " target messages received" << endl;
  dout << hex;
  return 0;

  //this loop is so that we continually read off the CAN until our buffer
  //is filled with the right number of frames corresponding to the number 
  //of targets we're expecting
  while(true)
  {  
    retvalue = canRead(rxhandle_targets, &cmsg[0],&numFramesGiven,NULL);
    if(retvalue==NTCAN_RX_TIMEOUT)
    {
      printf("canRead() returned timeout\n");
      printf("is the radar on?\n");
      return(-1);
    }
    else if(retvalue!=NTCAN_SUCCESS)
    {
      printf("canRead() failed with error %d!\n", retvalue);
      return(-1);
    }
     
    tmpByte = cmsg[0].data[0];
    tmpFrameNumber = (int)( (tmpByte)&0x07 );
    tmpTargetNumber = (uint)( (tmpByte)>> 3 );
      
    //the start of the message will have frameNumber=4 & targetNumber=0
    if(tmpFrameNumber==4 && tmpTargetNumber==0)
      foundStart=true;	

    if(foundStart==true)
    {	  
      for(int k=0; k<numFramesGiven; k++) // len is length of frames received
	    {
	      for(int i=0; i<(int)(cmsg[k].len & 0x0f); i++) // each frame has 8 bytes
        {
          frame_buffer[frameCounter][i] = cmsg[k].data[i];		  
        }	      

	      frameCounter++;	      

	      if(frameCounter==numFramesExpected)
          break;	      
	      
	    }
    }
	 
    if(frameCounter==numFramesExpected)       
      break;
  }

  //now that frame buffer is full and ordered, let's extract the data    
  for(uint8_t m=0; m<10; m++)
  {
    j = m*2;      

    // there is a second frame in j+1 but there is no important info there

    // extract velocity
    rawVel = frame_buffer[j][2];	  
    rawVel = rawVel << 8; 
    rawVel = rawVel | frame_buffer[j][3];
    rawVel_int = (int16_t)rawVel;
    realVel = rawVel_int*0.01f;
         
    // extract range
    temp_dist1 =  (uint)((uint8_t)frame_buffer[j][4]);
    temp_dist2 =  (uint)((uint8_t)frame_buffer[j][5]);      
    //rawDist = temp_dist1*256+temp_dist2;      
    rawDist = (temp_dist1 << 8) | temp_dist2;
    assert(rawDist>=0);
    realDist = (int)(rawDist)*0.01;       
    assert(realDist>=0);

      
    // extract yaw
    realYaw = (signed char)frame_buffer[j][1]*0.05f;
      
    /*    targets[m].velocity =  realVel;
    targets[m].range = realDist;
    targets[m].yaw = realYaw*M_PI/180;	           
    targets[m].status = 1;*/
	
  }

  return(0);
}

// -----------------------
// Close all handles 
// -----------------------
int RadarDiag::disconnect()
{
  canClose(txhandle);
  canClose(rxhandle_targets);
  canClose(rxhandle_tracks);
  canClose(rxhandle_param);
  canClose(rxhandle_status);
  canClose(rxhandle_CCP);
  return(0);
}

int getParamIndex(uint8_t id)
{
  return 0;
}

int getParamIndex(char * name)
{
  return 0;
}

int main(int argc, char *argv[])
{

  int retvalue;

  RadarDiag radar;  

  /*  double velocity[ALLOWABLE_NUM_TARGETS];
  double range[ALLOWABLE_NUM_TARGETS];
  double yaw[ALLOWABLE_NUM_TARGETS];
  int credibility[ALLOWABLE_NUM_TARGETS];
  int status[ALLOWABLE_NUM_TARGETS];
  */

  // Initialize radar 
  retvalue = radar.initRadarDiag();  
  if(retvalue==-1) 
    { 
      printf("could not initialize radar. Abort.\n"); 
      return(-1);
    }

  radar.initConsole();

  string input;

  // Start infinite loop
  for(;;) {      
    // grab tracks
    radar.captureTracks();
    radar.captureTargets();
    radar.captureStatus();
    radar.captureParams();
    radar.captureCCP();

    radar.refreshOutput();

    // Process user input
    if (radar.getUserInput(input)) {
      istringstream iss;
      iss.str(input);
      string cmd;
      iss >> cmd;
      uint8_t param1;
      uint8_t param2;
      int x=0, y=0;
      iss >> hex >> x >> y;
      param1 = (uint8_t)x;
      param2 = (uint8_t)y;

      if (cmd == "abort")
	radar.sendAbort();
      else if (cmd == "validate")
	radar.sendValidate();
      else if (cmd == "clear_faults")
	radar.sendClearFaults();
      else if (cmd == "read")
	radar.sendReadParameter(param1);
      else if (cmd == "track")
	radar.sendTrackRequest(param1, param2);
      else if (cmd == "target")
	radar.sendTargetRequest(param1, param2);
      else if (cmd == "state")
	radar.sendState(param1, param2);
      else if (cmd == "ccp")
	radar.sendCCP(input);
      else if (cmd == "list_params") {
	for (int i=0;i<256;i++) {
	  radar.sendReadParameter((uint8_t)i);
	  usleep(100000);
	  radar.captureParams();
	  radar.refreshOutput();
	}
      }
      else if (cmd == "quit")
	break;
    }
    /*if(retvalue==-1) {
      printf("getTracks failed. Abort.\n");
      return(-1);
      }*/
    
    // extract data from captured tracks
    /*    for(int i=0; i<ALLOWABLE_NUM_TARGETS;i++)
	{
	  velocity[i] = radar.getTrackVelocity(i);
	  range[i] = radar.getTrackRange(i);
	  yaw[i] = radar.getTrackYaw(i);
	  credibility[i] = radar.getTrackCredibility(i);
	  status[i] = radar.getTrackStatus(i);
	}      
    */
      // send state
      /*retvalue = radar.sendState(0,0);
      if(retvalue==-1)
	{
	  printf("sendState failed. Abort.\n");
	  return(-1);
	}          
      */
      // print tracks
    /* for(int i=0; i<ALLOWABLE_NUM_TARGETS; i++)
	{
	  printf("track[%d].velocity: %f\n", i, velocity[i]);
	  printf("track[%d].range: %f\n", i, range[i]);
	  printf("track[%d].yaw: %f\n", i, yaw[i]);
	  printf("track[%d].credibility: %d\n", i, credibility[i]);
	  printf("track[%d].status: %d\n", i, status[i]);
	}
    */
  }

  return 0;
}
