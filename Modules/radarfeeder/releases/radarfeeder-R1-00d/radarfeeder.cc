
/* 
 * Desc: Radar feeder module
 * Date: 24 Apr 2007
 * Author: Jeremy Ma
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include <math.h>

#include <alice/AliceConstants.h>
#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <frames/pose3.h>
#include <frames/mat44.h>
#include <skynet/sn_msg.hh>
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_log.h>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ProcessState.h>
#include <interfaces/RadarRangeBlob.h>
#include <cotk/cotk.h>

#include "radar_driver.hh"
#include "cmdline.h"


/// @brief Radar feeder class
class RadarFeeder
{
  public:   

  /// Default constructor
  RadarFeeder();

  /// Default destructor
  ~RadarFeeder();

  /// Parse the command line
  int parseCmdLine(int argc, char **argv);
  
  /// Parse the config file
  int parseConfigFile(const char *configPath);

  /// Initialize feeder for live capture
  int initLive(const char *configPath);

  /// Finalize the feeder for live capture
  int finiLive();

  /// Capture a scan 
  int captureLive();

  /// Initialize sensnet
  int initSensnet(const char *configPath);

  /// Finalize sensnet
  int finiSensnet();
  
  /// Publish data over sensnet
  int writeSensnet();

  /// Process a scan
  int process();

  /// Get the process state
  int getProcessState();

  /// Get the predicted vehicle state
  int getState(uint64_t timestamp);

  public:

  // Initialize console display
  int initConsole();

  // Finalize console display
  int finiConsole();

  /// Console button callback
  static int onUserQuit(cotk_t *console, RadarFeeder *self, const char *token);

  /// Console button callback
  static int onUserPause(cotk_t *console, RadarFeeder *self, const char *token);
  
  /// Console button callback
  static int onUserLog(cotk_t *console, RadarFeeder *self, const char *token);

  public:

  // Program options
  gengetopt_args_info options;

  // Default configuration path
  char *defaultConfigPath;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;

  // Our module id (SkyNet)
  modulename moduleId;

  // Our sensor id (SensNet)
  sensnet_id_t sensorId;

  // Should we quit?
  bool quit;

  // Should we pause?
  bool pause;

  // Id of the radar
  int radarId;

  // Radar driver
  Radar* radar;
  
  // Sensor-to-vehicle transform
  float sens2veh[4][4];

  // Calibration stats (display only).
  // Sensor position (x,y,z) and rotation (roll,pitch,yaw).
  vec3_t sensPos, sensRot;
  
  // Log file name
  char logName[1024];
  
  // Is logging enabled?
  bool enableLog;

  // SensNet handle
  sensnet_t *sensnet;

  // SensNet log handle
  sensnet_log_t *sensnet_log;
  
  // Blob buffer
  RadarRangeBlob *blob;

  // Console text display
  cotk_t *console;

  // Current scan id
  int scanId;

  // Current scan time (microseconds)
  uint64_t scanTime;

  // Current vehicle state data
  VehicleState state;
  
  // Start time for computing stats
  uint64_t startTime;
  
  // Capture stats
  int capCount;
  int capCountThisSec;
  uint64_t capTime;
  uint64_t lastSec; //last second measured
  double capRate, capPeriod;

  // Logging stats
  int logCount, logSize;

  // Number of tracks
  uint8_t numberOfTracks;

  // Number of targets
  uint8_t numberOfTargets;

};


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
RadarFeeder::RadarFeeder()
{
  memset(this, 0, sizeof(*this));

  this->scanId = -1;

  return;
}


// Default destructor
RadarFeeder::~RadarFeeder()
{
  return;
}


// Parse the command line
int RadarFeeder::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;
    
  // Fill out the default config path
  this->defaultConfigPath = dgcFindConfigDir("radarfeeder");

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out sensor id
  this->sensorId = sensnet_id_from_name(this->options.sensor_id_arg);
  if (this->sensorId <= SENSNET_NULL_SENSOR)
    return ERROR("invalid sensor id: %s", this->options.sensor_id_arg);

  // Fill out the number of tracks
  this->numberOfTracks = (uint8_t)(this->options.tracks_arg);

  // Fill out the number of targets
  this->numberOfTargets = (uint8_t)(this->options.targets_arg);

  return 0;
}


// Parse the config file
int RadarFeeder::parseConfigFile(const char *configPath)
{  
  // Load options from the configuration file
  char filename[256];
  snprintf(filename, sizeof(filename), "%s/%s.CFG",
           configPath, sensnet_id_to_name(this->sensorId));
  if (cmdline_parser_configfile(filename, &this->options, false, false, false) != 0)
    MSG("unable to process configuration file %s", filename);

  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);

  // Parse transform
  float px, py, pz;
  float rx, ry, rz;
  if (sscanf(this->options.sens_pos_arg, "%f, %f, %f", &px, &py, &pz) < 3)
    return ERROR("syntax error in sensor pos argument");
  if (sscanf(this->options.sens_rot_arg, "%f, %f, %f", &rx, &ry, &rz) < 3)
    return ERROR("syntax error in sensor rot argument");
    
  pose3_t pose;
  pose.pos = vec3_set(px, py, pz);
  pose.rot = quat_from_rpy(rx, ry, rz);
  pose3_to_mat44f(pose, this->sens2veh);
  
  // Record euler angles for display only
  this->sensPos = vec3_set(px, py, pz);
  this->sensRot.x = rx * 180/M_PI;
  this->sensRot.y = ry * 180/M_PI;
  this->sensRot.z = rz * 180/M_PI;

  return 0;
}

// Initialize feeder for live capture
int RadarFeeder::initLive(const char *configPath)
{
  int retvalue;

  // Load configuration file
  if (this->parseConfigFile(configPath) != 0)
    return -1;

  // Initialize radar object
  this->radar = new Radar();
  
  // Initialize radar 
  retvalue = this->radar->initRadar();
  if(retvalue==-1) 
    { 
      printf("could not initialize radar. Abort.\n"); 
      return(-1);
    }
  
  // Set number of tracks to be numberOfTracks
  this->radar->setNumTracks(this->numberOfTracks);
  printf("set the number of tracks to %d\n", (int)(this->numberOfTracks));

  // Set number of targets to be numberOfTargets
  this->radar->setNumTracks(this->numberOfTracks);
  printf("set the number of targets to %d\n", (int)(this->numberOfTracks));
 
  if(this->numberOfTracks!=0)
    {
      // Start send track request
      retvalue = this->radar->sendTrackRequest();
      if(retvalue==-1)
	{
	  printf("could not send track request. Abort.\n");
	  return(-1);
	}
      printf("sent initial track request OK\n");
    }
   
  if(this->numberOfTargets!=0)
    {
      // Start send track request
      retvalue = this->radar->sendTargetRequest();
      if(retvalue==-1)
	{
	  printf("could not send track request. Abort.\n");
	  return(-1);
	}
      printf("sent initial track request OK\n");
    }
  
  // Send initial state of 0,0
  retvalue = this->radar->sendState(0,0);
  if(retvalue==-1)
    {
      printf("sendState failed. Abort.\n");
      return(-1);
    }
  printf("sent state information OK\n");
  
  return 0;
}

int RadarFeeder::finiLive()
{
  this->radar->disconnect();
  return 0;
}

// Capture a scan 
int RadarFeeder::captureLive()
{
  /* REMOVE
  double speed, theta; 
  double x_yaw, y_yaw;
  double dot_prod, mag;
  */

  this->scanId += 1;
  this->scanTime = DGCgettime();

  // Get the matching state data
  if (this->getState(this->scanTime) != 0)
    return MSG("unable to get state; ignoring scan");

  /* REMOVE
  // Calculate speed
  speed =  pow( pow(this->state.localXVel,2)+pow(this->state.localYVel,2), 1/2);  
  
  // create unit vector in yaw direction
  x_yaw = cos(state.localYaw);
  y_yaw = sin(state.localYaw);

  // take dot product of unit vector and velocity vector
  dot_prod = (state.localXVel*x_yaw + state.localYVel*y_yaw);

  // find product of magnitudes
  mag = pow( pow(state.localXVel,2) + pow(state.localYVel,2), 1/2);
  mag = mag*pow( pow(x_yaw,2) + pow(y_yaw,2), 1/2);

  if(mag==0)
  mag=1;

  theta = acos(dot_prod/mag);

  // going in reverse ?
  if(cos(theta)<0)
  speed = -speed;
  */

  // Send state

  double speed = this->state.vehSpeed;
  double yaw = this->state.vehYawRate;

  // If command-line param given for sending fake state data, use it
  if (this->options.yawrate_given)
    yaw = this->options.yawrate_arg;
  if (this->options.velocity_given)
    speed = this->options.velocity_arg;

  if(this->radar->sendState( speed, yaw ) !=0 )
    return -1;

  if(this->numberOfTracks!=0)
  {
    // Read data from sensor.
    if(this->radar->captureTracks() != 0)
      return -1;
      
    // extract data from captured tracks
    memset(this->blob->tracks, 0, sizeof(this->blob->tracks));
    for(int i=0; i<RADAR_BLOB_MAX_TRACKS;i++)
    {
      this->blob->tracks[i].velocity = this->radar->getTrackVelocity(i);
      this->blob->tracks[i].range = this->radar->getTrackRange(i);
      this->blob->tracks[i].yaw = this->radar->getTrackYaw(i);
      this->blob->tracks[i].credibility = this->radar->getTrackCredibility(i);
      this->blob->tracks[i].status = this->radar->getTrackStatus(i);
    }    
  }
  
  if(this->numberOfTargets!=0)
  {
    // Read data from sensor.
    if(this->radar->captureTargets() != 0)
      return -1;
      
    // extract data from captured targets
    memset(this->blob->targets, 0, sizeof(this->blob->targets));
    for(int i=0; i<RADAR_BLOB_MAX_TARGETS;i++)
    {
      this->blob->targets[i].velocity = this->radar->getTargetVelocity(i);
      this->blob->targets[i].range = this->radar->getTargetRange(i);
      this->blob->targets[i].yaw = this->radar->getTargetYaw(i);
      this->blob->targets[i].credibility = 0; // target returns give no credibility
      this->blob->targets[i].status = this->radar->getTargetStatus(i);
    }
  }

  if (this->console)
  {
    cotk_printf(this->console, "%capid%", A_NORMAL, "%5d %9.3f",
                this->scanId, fmod((double) this->scanTime * 1e-6, 10000));
    if (this->state.timestamp > 0)
      cotk_printf(this->console, "%slat%", A_NORMAL,
                  "%+6dms", (int) (this->state.timestamp - this->scanTime) / 1000);
  }  
  return 0;
}


// Initialize sensnet
int RadarFeeder::initSensnet(const char *configPath)
{    
  // Check that blob size is a multiple of [something].  This allows
  // for DMA transfers.
  if (sizeof(RadarRangeBlob) % 512 != 0)
    return ERROR("invalid blob size %d; needs padding of %d",
                 sizeof(RadarRangeBlob),
                 512 - sizeof(RadarRangeBlob) % 512);

  // Create page-aligned blob to enable DMA logging
  this->blob = (RadarRangeBlob*) valloc(sizeof(RadarRangeBlob));

  // Initialize SensNet
  this->sensnet = sensnet_alloc();
  if (sensnet_connect(this->sensnet,
                      this->spreadDaemon, this->skynetKey, this->sensorId) != 0)
    return ERROR("unable to connect to sensnet");    
  
  // Subscribe to vehicle state messages
  if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, sizeof(VehicleState)) != 0)
    return ERROR("unable to join state group");

  // Subscribe to process state messages
  if (sensnet_join(this->sensnet, this->moduleId, SNprocessRequest, sizeof(ProcessRequest)) != 0)
    return ERROR("unable to join process group");


  // Initialize logging
  if (this->options.enable_log_flag)
  {
    time_t t;
    char timestamp[64];
    char cmd[256];
    sensnet_log_header_t header;

    // Construct log name
    t = time(NULL);
    strftime(timestamp, sizeof(timestamp), "%F-%a-%H-%M", localtime(&t));
    snprintf(this->logName, sizeof(this->logName), "%s/%s-%s",
             this->options.log_path_arg, timestamp, sensnet_id_to_name(this->sensorId));

    MSG("opening log %s", this->logName);

    // Initialize sensnet logging
    this->sensnet_log = sensnet_log_alloc();
    assert(this->sensnet_log);
    memset(&header, 0, sizeof(header));
    if (sensnet_log_open_write(this->sensnet_log, this->logName, &header, true) != 0)
      return ERROR("unable to open log: %s", this->logName);

    // Copy configuration files
    snprintf(cmd, sizeof(cmd), "cp %s/%s.CFG %s",
             configPath, sensnet_id_to_name(this->sensorId), this->logName);
    system(cmd);
        
  }

  return 0;
}


// Finalize sensnet
int RadarFeeder::finiSensnet()
{  

  if (this->sensnet_log)
  {
    sensnet_log_close(this->sensnet_log);
    sensnet_log_free(this->sensnet_log);
    this->sensnet_log = NULL;
  }

  sensnet_leave(this->sensnet, this->moduleId, SNprocessRequest);
  sensnet_leave(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate);
  sensnet_disconnect(this->sensnet);
  sensnet_free(this->sensnet);
  this->sensnet = NULL;

  free(this->blob);
  this->blob = NULL;
  
  return 0;
}


// Publish data
int RadarFeeder::writeSensnet()
{
  pose3_t pose;
  RadarRangeBlob *blob;

  blob = this->blob;

  // Construct the blob header
  blob->blobType = SENSNET_RADAR_BLOB;
  blob->sensorId = this->sensorId;
  blob->scanId = this->scanId;
  blob->timestamp = this->scanTime;
  blob->state = this->state;

  // Sensor to vehicle transform
  memcpy(blob->sens2veh, this->sens2veh, sizeof(this->sens2veh));
  mat44f_inv(blob->veh2sens, blob->sens2veh);

  // Vehicle to local transform
  pose.pos = vec3_set(blob->state.localX,
                      blob->state.localY,
                      blob->state.localZ);
  pose.rot = quat_from_rpy(blob->state.localRoll,
                           blob->state.localPitch,
                           blob->state.localYaw);  
  pose3_to_mat44f(pose, blob->veh2loc);  
  mat44f_inv(blob->loc2veh, blob->veh2loc);

  // Reset reseved values
  memset(blob->reserved, 0, sizeof(blob->reserved));

  // Copy the scan data
  memcpy(blob->tracks, this->blob->tracks, sizeof(this->blob->tracks));

  // Write blob
  if (sensnet_write(this->sensnet, SENSNET_METHOD_CHUNK, this->sensorId, SENSNET_RADAR_BLOB,
                    this->scanId, sizeof(*blob), blob) != 0)
    return ERROR("unable to write blob");
  
  // Write to log
  if (this->sensnet_log && (this->enableLog || this->options.always_log_flag))
  {
    if (sensnet_log_write(this->sensnet_log, DGCgettime(),
                          this->sensorId, SENSNET_RADAR_BLOB,
                          this->scanId, sizeof(*blob), blob) != 0)
      return ERROR("unable to write blob");
  }

  if (this->sensnet_log && (this->enableLog || this->options.always_log_flag) && this->console)
  {    
    // Keep some stats on logging
    this->logCount += 1;
    this->logSize += sizeof(*blob);
    cotk_printf(this->console, "%log%", A_NORMAL, "%df %dMb",
                this->logCount, this->logSize / 1024 / 1024);
  }
  return 0;
}


// Get the predicted vehicle state
int RadarFeeder::getState(uint64_t timestamp)
{
  int blobId;

  // Default to all zeros in state
  memset(&this->state, 0, sizeof(this->state));
  
  // Get the current state value
  if (sensnet_read(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate,
		   &blobId, sizeof(this->state), &this->state) != 0)
    return MSG("unable to read state data");
  if(blobId < 0)
    return ERROR("state is invalid");

  // Update the display
  if (this->console)
  {
    cotk_printf(this->console, "%stime%", A_NORMAL, "%9.3f",
                fmod((double) state.timestamp * 1e-6, 10000));
    cotk_printf(this->console, "%spos%", A_NORMAL, "%+09.3f %+09.3f %+09.3f",
                state.localX, state.localY, state.localZ);
    cotk_printf(this->console, "%srot%", A_NORMAL, "%+06.1f %+06.1f %+06.1f",
                this->state.localRoll*180/M_PI, 
                this->state.localPitch*180/M_PI, 
                this->state.localYaw*180/M_PI);
    cotk_printf(this->console, "%sspeed%", A_NORMAL, "%+06.3f %+06.1f",
                state.vehSpeed, state.vehYawRate*180/M_PI);
  }
  
  return 0;
}

// Get the process state
int RadarFeeder::getProcessState()
{
  int blobId;
  ProcessRequest request;
  ProcessResponse response;

  // Send heart-beat message
  memset(&response, 0, sizeof(response));  
  response.moduleId = this->moduleId;
  response.timestamp = DGCgettime();
  response.logSize = this->logSize / 1024;
  sensnet_write(sensnet, SENSNET_METHOD_CHUNK,
                this->moduleId, SNprocessResponse, 0, sizeof(response), &response);
  
  // Read process request
  if (sensnet_read(this->sensnet, this->moduleId, SNprocessRequest,
                   &blobId, sizeof(request), &request) != 0)
    return 0;
  if (blobId < 0)
    return 0;

  // If we have request data, override the console values
  this->quit = request.quit;
  if (this->quit)
    MSG("Quit requested");

  this->enableLog = request.enableLog;  
  
  return 0;
}


// Process a scan
int RadarFeeder::process()
{
  // Processing currently does nothing for a radar scan  
  return 0;
}


// Template for console
//234567890123456789012345678901234567890123456789012345678901234567890123456789
static char *consoleTemplate =
"RadarFeeder $Revision$                                                     \n"
"                                                                           \n"
"Skynet: %spread%                                                           \n"
"Sensor: %sensor%                                                           \n"
"No. of Tracks : %trackNum%                                                 \n"
"No. of Targets: %targetNum%                                                \n"
"                                                                           \n"
"Capture                           State                                    \n"
"Scan     : %capid%                     Time   : %stime% %slat%             \n"
"Log      : %log%                       Pos    : %spos%                     \n"
"         : %logname%                   Rot    : %srot%                     \n"
"Cap Rate : %caprate%                   Speed  : %sspeed%                   \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%|%LOG%]                                                     \n";


// Initialize console display
int RadarFeeder::initConsole()
{
  char filename[1024];
    
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
  cotk_bind_toggle(this->console, "%LOG%", " LOG ", "Ll",
                   (cotk_callback_t) onUserLog, this);

  // Initialize the display
  snprintf(filename, sizeof(filename), "%s/%s.msg",
           this->options.log_path_arg, sensnet_id_to_name(this->sensorId));
  if (cotk_open(this->console, filename) != 0)
    return -1;


  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));
  cotk_printf(this->console, "%sensor%", A_NORMAL, sensnet_id_to_name(this->sensorId));    
  cotk_printf(this->console, "%logname%", A_NORMAL, this->logName);
  cotk_printf(this->console, "%trackNum%", A_NORMAL, "%d",(int)(this->numberOfTracks));
  cotk_printf(this->console, "%targetNum%", A_NORMAL, "%d",(int)(this->numberOfTargets));

  return 0;
}


// Finalize console display
int RadarFeeder::finiConsole()
{
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Handle button callbacks
int RadarFeeder::onUserQuit(cotk_t *console, RadarFeeder *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int RadarFeeder::onUserPause(cotk_t *console, RadarFeeder *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}


// Handle user events; occurs in sparrow thread
int RadarFeeder::onUserLog(cotk_t *console, RadarFeeder *self, const char *token)
{
  assert(self);
  self->enableLog = !self->enableLog;
  MSG("log %s", (self->enableLog ? "on" : "off"));
  return 0;
}


// Main program thread
int main(int argc, char **argv)
{
  int status;
  RadarFeeder *feeder;

  // Create feeder
  feeder = new RadarFeeder();
  assert(feeder);
 
  // Parse command line options
  if (feeder->parseCmdLine(argc, argv) != 0)
    return -1;

  // Initialize for live capture
  if (feeder->initLive(feeder->defaultConfigPath) != 0)
    return -1;

  // Initialize sensnet
  if (feeder->initSensnet(feeder->defaultConfigPath) != 0)
    return -1;


  // Initialize console
  if (!feeder->options.disable_console_flag)
  {
    if (feeder->initConsole() != 0)
      return -1;
  }

  feeder->startTime = DGCgettime();
  feeder->lastSec = feeder->startTime;

  int err=0;

  // Start processing
  while (!feeder->quit)
  {
    // Do heartbeat occasionally
    if (feeder->capCount % 15 == 0)
      feeder->getProcessState();

    // Capture incoming scan directly into the radar buffer
    if (feeder->captureLive() != 0) {
      err = 1;
      break;    
    }

    // Compute some diagnostics
    feeder->capCount += 1;
    feeder->capCountThisSec += 1;
    feeder->capTime = DGCgettime() - feeder->startTime;
    if ((DGCgettime() - feeder->lastSec) > 1000000) {
      feeder->lastSec = DGCgettime();
      feeder->capRate = feeder->capCountThisSec;
      feeder->capPeriod = 1000.0 / feeder->capRate;
      // Output capture rate
      cotk_printf(feeder->console, "%caprate%", A_NORMAL, "%05.1f Hz    ",
		  feeder->capRate);
      feeder->capCountThisSec=0;
    }
    //feeder->capRate = (float) 1000 * feeder->capCount / feeder->capTime;

    // Update the console
    if (feeder->console)
      cotk_update(feeder->console);
    
    // If paused, give up our time slice.
    if (feeder->pause)
    {
      usleep(0);
      continue;
    }
    
    // Process one scan
    status = feeder->process();
    if (status != 0) {
      err = 2;
      break;
    }

    // Publish data
    if (feeder->writeSensnet() != 0) {
      err = 3;
      break;    
    }
  }
  
  // Clean up
  feeder->finiConsole();
  feeder->finiSensnet();
  feeder->finiLive();
  cmdline_parser_free(&feeder->options);  
  delete feeder;

  if (err == 1)
    MSG("error during live capture");
  if (err == 2)
    MSG("error processing scan");
  if (err == 3)
    MSG("error writing sensnet");

  MSG("feeder->quit=%s", (feeder->quit?"true":"false"));
  MSG("program exited cleanly");
  
  return 0;
}
