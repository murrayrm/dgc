#ifndef RADAR_DIAG_H
#define RADAR_DIAG_H

#include <stdio.h>
#include <ntcan/ntcan.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <iostream>
#include <curses.h>
#include <string>
#include <sstream>

using namespace std;

#define ALLOWABLE_NUM_TARGETS 20

// Some character definitions
#define ASCII_Enter 10
#define ASCII_Backspace 0x7F
#define ASCII_Escape 0x1B

class RadarDiag
{
  
public:

  // Default constructor
  RadarDiag();

  // Default destructor
  ~RadarDiag();

  // Initialize the radar
  int initRadarDiag();

  // Initialize console
  int initConsole();

  // Clear user input stuffs
  void startUserInput();

  // Capture user input
  bool getUserInput(string &result);

  // Refreshes output display
  void refreshOutput();

  // Send state information to radar
  int sendState(uint8_t speed, uint8_t yawrate);

  // Send request for tracks
  int sendTrackRequest(uint8_t count, uint8_t repeats);

  // Send request for targets
  int sendTargetRequest(uint8_t count, uint8_t repeats);

  // Send abort command
  int sendAbort();

  // Send request to validate parameter set (?)
  int sendValidate();

  // Clear all fault values
  int sendClearFaults();

  // Send request for single parameter read
  int sendReadParameter(uint8_t paramID);

  // Send request for single parameter write
  int sendWriteParameter(uint8_t paramID, uint8_t *value);

  // Send CCP command with custom ID and data
  int sendCCP(string msg);

  // Capture radar track information
  int captureTracks();

  // Capture radar target information
  int captureTargets();

  // Capture radar status information
  int captureStatus();
  
  // Capture parameter value information
  int captureParams();

  // Capture CCP information
  int captureCCP();

  // Close all handles 
  int disconnect();

private:

  // Return the index of a parameter, based on uint8 id
  int getParamIndex(uint8_t id);

  // Return the index of a parameter, based on name
  int getParamIndex(char * name);

  // Window for receiving user input
  WINDOW *input_win;

  // Window for displaying received data
  WINDOW *data_win;

  // Current input string
  string inputStr;

  // StringStream for output
  ostringstream dout;
  
  // CAN Handle for transmission of commands
  int32_t txhandle;

  // CAN Handle for receiving target messages
  int32_t rxhandle_targets;

  // CAN Handle for receiving track messages
  int32_t rxhandle_tracks;

  // CAN Handle for receiving status messages
  int32_t rxhandle_status;

  // CAN Handle for receiving parameter messages
  int32_t rxhandle_param;

  // CAN Handle for receiving CCP messages
  int32_t rxhandle_CCP;

  // frame buffer for storing data frames from the radar
  char frame_buffer[2*ALLOWABLE_NUM_TARGETS][8]; //2 frames per object and 8 bytes in each frame

  // baud rate (not actual baud rate, just an enum)
  unsigned long baud;
  
  // logical netnumber
  int net;

  // mode for canOpen
  unsigned long mode; 

  // maximum number of messages to transmit = 40 since 20 moving objects and 2 frames/object
  long txqueuesize;  

  // maximum number of messages to receive = 40 since 20 moving objects and 2 frames/object
  long rxqueuesize;  

  // timeout for transmit
  long txtimeout;   

  // timeout for receiving data
  long rxtimeout;          

  // Counter for yaw rate state message
  uint8_t yawcounter;

  // are we logging all communications?
  bool logging;
};

#endif
