% Master File: darpa06-dgc.tex
\section{Team Description}

\noinstructions{The team description section (8 pages maximum) will
discuss the background of the key individuals who will participate in
this program, the amount of time they will spend on the program, and
whether they will be paid with Urban Challenge award funds.  This
section should describe the role of all major subcontractors,
including company or business unit backgrounds in this area.  Past
performance on relevant programs by the prime and major subcontractors
should include references and points of contact.  A brief description
of available test facilities should be included.}

\subsection{Team Caltech organizational structure}

Several of our core team members have experience with both the 2004 and 2005
DARPA Grand Challenges, as well as experience in research and development
for DARPA.  We are adding new team members with substantial experience in 
system development and autonomy, as well as DARPA-funded programs.

The organization structure for Team Caltech is shown in
Figure~\ref{fig:orgchart}. It is an adaptation of the approach that has 
served us well in the last Grand Challenge effort.
\begin{figure}
  \centerline{\epsfig{figure=orgchart.eps,width=0.8\textwidth}}
  \caption{Team Caltech organizational structure.  The overall team consists of
  four ``race teams'' that are responsible for the technology that is
  used in the race.  An integrated product team (IPT) consists of the
  heads of each race team as well as the project manager and systems
  engineering activity.  Two additional teams are responsible for
  helping maintain the team infrastructure (computers, shop,
  vehicles) and an independent test team (ITT) will design, supervise
  and evaluate formal field trials.}
  \label{fig:orgchart}
\end{figure}
The primary organizational structure for Team Caltech are the ``race teams.''
Each team is lead by a senior researcher with prior Grand Challenge
experience, or prior DARPA/NASA system development experience.  Each race
team is responsible for implementing a specific set of hardware and/or
software capabilities that are part of Alice.  The planned race teams and their
responsibilities are:
\begin{itemize}
  \item {\em Operations team} - responsible for the mechanical and
  computing hardware in Alice, including software interfaces to
  actuation subsystems, and for all infrastructure required for
  efficient operations and testing.  The operations team
  also maintains the detailed computer simulation models for the
  system.  The operations team will be lead by Tony Fender.

  \item {\em Guidance, Navigation, and Control} (GNC) team -
  responsible for guidance, navigation and control software, including
  the networked control systems (NCS) architecture and major modules
  for state estimation, route, traffic and path planning, defensive
  driving, and path following. The GNC team will be lead by Joel Burdick.

  \item {\em Sensing and Mapping} team - responsible for sensing
  hardware and software, including environment classification and
  mapping; moving obstacle detection, classification, tracking and
  prediction; and sensor fusion and situational awareness.  The
  sensing and mapping team will be led by Andrew Howard.

  \item {\em Mission and contingency management} team - responsible
  for supervisory control, including mission and contingency
  management logic.  The mission and contingency management team will
  be lead by Steve Chien.
\end{itemize}

In addition to the four race teams, a set of additional teams are
used to carry out activities that are required for the race but that
do not directly involve hardware or software that is part of Alice:
\begin{itemize}
  \item {Independent test team} (ITT) - the ITT will be led by JPL personnel
  and will design, supervise, and evaluate formal field trials, as
  described in our test plan.  The design of these trials will follow
  best practices learned from DARPA UGV programs (Perceptor, LAGR) and
  NASA planetary rover programs (Pathfinder, MER, MSL).  The ITT will
  be led by Rich Petras.

  \item {\em System administration} team - responsible for administering the
  development and testing computers, including backups.  This team
  will report to the project manager.

  \item {\em Vehicle/shop} team - responsible for maintaining vehicle and shop
  infrastructure.  This team will report to the project manager and
  will primarily consist of undergraduate work-study students.
\end{itemize}

Overall management of Team Caltech's activities is the responsibility
of the team leader (Richard Murray) working with a full-time project
manager (to be hired).  Together 
with the heads of each of the race teams, this group forms the
{\em Integrated Product Team} (IPT). The IPT is responsible for all
resources in the project, and thus makes and implements
decisions affecting the project.  The IPT drives activities within the
team based on a quarterly test plan, described in more detail in
Section~\ref{sec:testplan}.

\subsection{Primary partners}

\paragraph{California Institute of Technology} Caltech is a private
university with strengths in robotics, control, machine vision and
distributed systems.  5 faculty/instructors will serve as active
members of Team Caltech, devoting 10-20\% of their time to the effort
(at no charge to the program).  We are requesting funding for 1
full-time postdoc, 4 full-time graduate students and 6 part-time
undergraduates as part of this proposal.  In addition, through
coursework, summer research projects and fellowship support (internal
and external), we anticipate participation by more than 30
undergraduate students and 4--8 additional graduate students.

\paragraph{Jet Propulsion Laboratory (JPL)} JPL is an operating
division of Caltech that has primary responsible for NASA's planetary
exploration missions, including the Mars Exploratory Rovers (MER) and
the Mars Science Laboratory (MSL).  Over a dozen engineers and
researchers are current participating as members of Team Caltech.  As
part of this proposal, 8 JPL researchers will devote 10-25\% of their
time to transitioning technology to the proposal, as well as
helping mentor undergraduate and graduate
students and transition additional technology that is not part of the
proposed effort but that may be included on the vehicle.

\paragraph{Northrop Grumman Corporation (NGC)} Northrop Grumman
Corporation is a global defense company headquartered in Los Angeles.
Northrop Grumman provides a broad array of technologically
advanced, innovative products, services and solutions in systems
integration, defense electronics, information technology, advanced
aircraft, shipbuilding, and space technology.  Three sectors of
Northrop Grumman will participate as members of Team Caltech.

Northrop Grumman's Electronic Systems sector (NGES) will participate
through their Navigation Systems Division (NSD).  NSD provides
situational awareness electronic systems and products for defense,
civil, and commercial markets.  They will participate in Team Caltech
by providing expertise in navigational systems and systems engineering.

Northrop Grumman's Integrated Systems sector (NGIS) will provide direct
support for graduate students working on this project, and a volunteer
engineering staff who will aid and advise students in systems
engineering and integration of the primary technical tasks, guidance,
sensing, reasoning, and testing.  The volunteer engineers, who have
experience in multidisciplinary projects and technical expertise in
control systems, vision systems, sensor integration and fusion,
contingency management, will form an advisory panel that will
participate in design reviews and offer advice to the Caltech/JPL/NGC
team.  The engineers may also advise students in requirements
development, scheduling, and test matrix generation.

Northrop Grumman's Space Technology sector (NGST) will provide a
royalty-free license for use of its OTGX software and provide internal
support for researchers and engineers to support Team Caltech's
activities in navigation and planning.  NGST will provide a license to
use its OTGX software package as part of the path planning module and
will provide technical support for integrating OTGX capabilities into
the GNC subsystem.

\subsection{Key personnel}

The following personnel from Caltech/JPL and NGC will spend at least
10\% of their time working with Team Caltech, either directly
supported under the contract, through internal IR\&D funds, or as
volunteers.  Time charged to the contract will be for technical
activities that directly tie to project deliverables; additional time
advising and mentoring students will be covered by other sources of
funds (e.g., Caltech faculty salaries) or done on a voluntary basis.

\paragraph{Joel Burdick.} Joel Burdick is a Professor of Mechanical Engineering
at Caltech, and one of Team Caltech's faculty sponsors in the 2004 and 2005
Grand Challenges. Burdick has 25 years experience in developing robotics
systems.  He has worked for over 10 years in the area of sensor-guided robotic
navigation and control.  He also has significant experience in management,
having been the co-director of the Caltech Center for Neuromorphic systems
engineering, and is currently the Executive Office (equivalent of chairman)
for Caltech's Department of BioEngineering.  Burdick has previously been
involved in 3 DARPA funded projects.  Burdick will devote 15\% of his time
to the project as part of his research and teaching responsibilities, at no
cost to the program.

\paragraph{Steve Chien.} Steve Chien is a Principal Computer Scientist
in Autonomous Systems at JPL.  He has led numerous efforts in
autonomous systems, most recently as the Principal Investigator of the
Autonomous Sciencecraft (ase.jpl.nasa.gov) and the Earth Observing
Sensorweb (sensorweb.jpl.nasa.gov).  Chien is a three time honoree in the
NASA Software of the Year Competition, most recently as the team lead
for the Autonomous Sciencecraft, co-winner of the 2005 competition.  He
has also been awarded NASA Medals in 1997, 2000, and 2006, for his
work in autonomous systems for NASA.  Chien and his group will devote
0.25 FTE to the program.

\paragraph{Alex Fax.} Alex Fax manages the Networked Navigation
Systems group at Northrop Grumman's Navigation Systems Division in
Woodland Hills, CA.  Fax's group develops novel navigation
architectures to meet the emerging needs of the military.  Prior to
joining Northrop Grumman, Fax received his Ph.D. from Caltech in
Control and Dynamical Systems, where he focused on cooperative control
of unmanned vehicles.  Fax supported Team Caltech during the 2004
and 2005 Grand Challenge competitions through participation in design
reviews, supplying inertial hardware, and assisting in the development
of navigation software.

\paragraph{Andrew Howard.} Andrew Howard is a Senior Member of Technical Staff
with the Computer Vision Group at JPL, and Adjunct Assistant Professor
(Robotics) at the University of Southern California.  He has extensive
experience with defense robotics programs, including Software for
Distributed Robotics (DARPA SDR), Mobile Autonomous Robot Systems
(DARPA MARS), Learning Applied to Ground Robots (DARPA LAGR), BigDog
(DARPA Biodynotics) and the Collaborative Technology Alliance/Robotics
(ARL CTA).  He is co-creator of the widely used Player/Stage/Gazebo
robotics server/simulation packages.  Howard will devote 0.25 FTE to
the program.

\paragraph{Larry Matthies.} Larry Matthies is a Senior Research
Scientist at JPL and is the Supervisor of the Computer Vision Group in
the Mobility and Robotic Systems Section. His also an Adjunct
Professor in Computer Science at the University of Southern California
and is a member of the editorial boards for the Autonomous Robots
journal and the Journal of Field Robotics.  Matthies has a PhD in
Computer Science from Carnegie Mellon University and has served as the
principal investigator/task manager of numerous research tasks in
computer vision funded by NASA, DARPA, U.S. Army, and other sponsors
since 1992.  Matthies will participate through design
reviews and mentoring of students, as well as coordinating activities
within the JPL computer vision group.

\paragraph{Kenneth Meyer.} Kenneth Meyer is Supervisor of the Systems
Engineering and Architecture Group in the Flight Software and Data
System Section.  He is also Manager of the Software Management and
Engineering Task that's provides software engineering support for
NASA's Constellation Program.  Meyer has 20+ years experience as a
software developer and has worked at JPL since 1998.  His assignments
at JPL include the Mission Data System (MDS), X2000 and the Mars
Science Laboratory Projects.  He has served as X2000 Development
Infrastructure and Tools Lead, the MDS Process Lead, MDS Outreach
Lead, and the Task Manager for the MDS-05 project. In addition, he has
been a co-Investigator for the High Dependability Computing Program, a
co-investigator in the JPL's research into real-time Java, and Task
Lead for the Real-time Java evaluation effort that supported
Raytheon's analysis of Real-time Java for the Navy's DD(X) program.
Meyer's group will devote 0.25 FTE to the program, in addition to
providing technical guidance for students.

\paragraph{Mark Milam.} Mark Milam received a Ph.D. from Caltech
and has over 17 years of experience working on guidance, navigation
and control systems. Dr Milam currently serves as PI on the Agile
Space Vehicle IRAD program at Northrop Grumman Space Technology
(NGST). In this capacity, he has led NGST's efforts in developing
emerging Rendezvous and Proximity Operations, Formation Flying, and
Autonomy technologies. Prior work includes a key role in adapting the
OTG software package to successfully run on the SEC T-33 demonstration
program.

\paragraph{Richard Murray (PI).} Richard Murray is a Professor of Control and
Dynamical Systems at Caltech and was the the lead faculty sponsor and
organizer for Team Caltech's 2004 and 2005 Grand Challenge entries.  Murray
has substantial experience in both academic and industry management: he worked
for two years as the Director for Mechatronic Systems at the United
Technologies Research Center (UTRC) and for five years as the Division Chair
(dean) of Engineering and Applied Science at Caltech.  Murray has served as
the PI for a DARPA contract in the Software Enabled Control (SEC) program,
which provided funding of over \$3M and resulted in a flight test of
optimization-based control and verifiable lost-wingman protocols.  Murray's
technical expertise in the area of real-time, optimization-based planning and
networked control systems.  Murray will devote 25\% of this time to the
project as part of his Caltech research and teaching responsibilities, at no
cost to the program.

\paragraph{Issa Nesnas.} Issa A.D. Nesnas, Ph.D., is the Group
Supervisor for the Robotic Software Systems and the Principal
Investigator for the CLARAty multi-center project
(\url{http://claraty.jpl.nasa.gov}).  He has led several technology
projects in autonomous sensor-based robotics systems both at JPL and
in industry.  He received several awards for his robotic development
including the One NASA JPL Center Best Award for the CLARAty.

\paragraph{Han Park.}  Han Park is a senior technologist in the
Vehicle Management Systems and Flight Controls department at Northrop
Grumman Integrated Systems sector.  Prior to joining NGC, he was a
senior staff at JPL in the Flight Software and Data Systems Section.
Currently, he is the PI in the Autonomous Terminal Area Operations of
unmanned aerial vehicles program at NGC.  Park has extensive
experience in the areas of health management, automated recovery, and
automated reasoning.  He will volunteer his time to the project and
serves as the technical POC for the Northrop Grumman Integrated
Systems sector.

\paragraph{Richard Petras.} Richard Petras is currently leading the
Distributed Rover Avionics Software Research and Technology
Development Task which is developing a software architecture for
advanced distributed avionics modules.  Past work at JPL includes
testing and operation of the autonomous navigation software for the
twin MER rovers Spirit and Opportunity, leading the development of the
Rocky 8 research rover and helping to develop the CLARAty robotic
architecture for planetary rovers. Petra will lead the Independent
Test Team (ITT).

\paragraph{Pietro Perona.} Pietro Perona is a Professor of Electrical
Engineering at Caltech. He is the PI on an ONR-MURI grant started May
2006 developing visual recognition for urban surveillance from
networks of cameras. Perona's research focuses on visual recognition
of scenes and categories and on unsupervised visual learning. Perona
will dedicate 15\% of his time to the project. In 2006--07 Perona will
teach two Caltech courses (visual navigation and visual recognition)
which will be coordinated with the activities of the Challenge
team. Perona will also be involved in developing visual recognition
algorithms for classifying vehicles and other obstacles in urban
scenes. Perona, in collaboration with Andrew Howard, will help
coordinate the sensory systems activities of the project.

\paragraph{Bob Rasmussen.}  Robert Rasmussen is Chief Engineer for the
Systems and Software Division of JPL.  He has broad experience in
spacecraft systems engineering, attitude control and avionics, test
and flight operations, and automation and autonomy - particularly in
the area of spacecraft fault tolerance.  Recent roles have included
co-initiation of the Remote Agent experiment in spacecraft autonomy on
DS-1, which won NASA's software of the Year award in 1999; cognizant
engineer throughout development for the Attitude and Articulation
Control Subsystem on the highly successful Cassini mission to Saturn;
and chief architect for the Mission Data System project to an develop
advanced, reusable flight and ground software system.  Rasmussen holds a
Ph.D. in Electrical Engineering from Iowa State University.  He will
assist in advising students in mission and contingency
management. 

\paragraph{Richard Volpe (JPL PI)} Richard Volpe is manager of the JPL
Mobility and Robotic Systems Section, a team of over 80 robotics
engineers doing research and spaceflight implementation of robotic
systems.  Applications include Roving, Ballooning, Drilling, and other
modes of in-situ planetary exploration. From 2001 through 2004,
Richard served as the manager of Mars Regional Mobility and Subsurface
Access in JPL's Space Exploration Technology Program Office. In
addition to guiding technology development for future robotic
exploration of Mars and the Moon, he has been actively involved in
2003 and 2009 rover mission development. This has included managing
internal JPL rover technology development, as well as external
university research funded by the Mars Technology Program. Volpe's
background and interests include manipulator force control, path
planning, software architectures, and mobile robot system
development.  Volpe will serve as the point of contact for JPL
activities.

\subsection{Additional Partners}

In addition to the primary partners listed above and the key personnel
indicated, Team Caltech will also interact with several other
organizations. 

\paragraph{Walt Disney Imagineering}
Walt Disney Imagineering (WDI) is the master planning, creative development,
design, engineering, production, project management and research and
development arm of The Walt Disney Company. Its talented corps of
Imagineers is responsible for the creation---from concept initiation
through installation---of all Disney resorts, theme parks and
attractions, real estate developments, regional entertainment venues
and new media projects. Imagineering is headquartered in Glendale,
California.  Researchers and engineers from WDI R\&D will participate in
design reviews, advise students on projects related to robotics and
autonomy, and provide experience and insight in systems engineering.

\paragraph{Motion Picture Marine} 
Motion Picture Marine (MPM) is a production services copmany and the
creator of the the Perfect Horizon system, a
state-of-the-art digital camera stabilization head that provides roll
and pitch control.  MPM will loan a Perfect Horizon system to Team
Caltech and will provide support for modifying it for use as a
gimbaled sensor (through a new product that they are developing).

%\paragraph{Raytheon}
%Raytheon is an industry leader in defense and government electronics,
%space, information technology, technical services, and business
%aviation and special mission aircraft.  They will assist Team Caltech
%by exploring possible sensors for use in urban environments, including 
%EO/IR imagers, RADARs and flash LADAR, stereo imaging and GPS
%navigation.

\subsection{Test facilities}

As its primary local test facility, Team Caltech will make use of the
former St.\ Luke Medical Center, shown in Figure~\ref{fig:stluke}.
\begin{figure}
  \centerline{\epsfig{figure=stluke.eps,width=0.75\textwidth}}
  \caption{Overhead view of the former St\. Luke Medical Center, with
  a sample RNDF superimposed.}
  \label{fig:stluke}
\end{figure}
This property is owned by Caltech and has a set of parking lots and
access roads that provide many of the types of scenarios that will
need to be demonstrated for the project.  The facility is available to
us at all times and the parking lots are lightly used during weekdays
and unused on evenings and weekends.

In addition to St.~Luke, we will also use test sites in the nearby
desert, where there are many areas having small road networks and
where we can erect simple structures to block lines of site.

We intend to cooperate with the Golem Group, UCLA and possibly other local
teams in some of our major tests, to simplify the logistics of
acquiring a test site and to provide some friendly competition as
motivation for the teams involved.  This approach will also provide
our respective vehicles with the opportunity to operate in the
presence of autonomous vehicles well before the NQE.  We are currently
planning to share tests in December and April.

\subsection{Simulation Facilities}

One of the key elements to an integrated environment
for autonomous vehicle research is a simulation environment that is
capable of representing the main features of the situations we expect
to encounter. The simulation will be built on the Gazebo simulation
environment and will include the vehicle dynamics, sensing and
actuation systems, imaging systems, and environments with moving
obstacles (e.g., other vehicles). The simulation will emulate the
hardware interfaces of the vehicle, so that identical software can be
tested in simulation and experiment.  A first iteration of this
software has already been completed and was used for the 2005 Grand
Challenge.  

In addition to Gazebo, Caltech will investigate one other possible tool
for use in our simulation testing: the Rover Analysis, Modeling and
Simulation (ROAMS) simulation model developed at JPL.  ROAMS is used
to carry out surface system trade studies, development of new rover
technologies, closed-loop development and test of on-board flight
software, and for use during mission operations. ROAMS includes (a)
detailed physics based models of the rover mechanical platform
including its kinematics and dynamics, (b) a broad suite of actuators
and sensors such as wheel \& steering motors and encoders, inertial
measurement units (IMUs), sun sensors, cameras, (c) models of the
terrain environment and the rover's interactions with the terrain
environment and (d) extensive instrumentation for simulation data
access and visualization.

\subsection{Past programs}

As part of the DARPA Software Enabled Control (SEC) project, Caltech
and Colorado developed and tested two major advances in
software enabled control: optimization-based control using real-time
trajectory generation and logical programming environments for formal analysis
of distributed control systems.  These two activities, funded under the OCC
and HSCC tasks of the SEC, where integrated and tested on the industry-led
demonstration using the F-15 and T-33 flight tests.  Contact: Helen
Gill (NSF) and John Bay (AFRL/IF).

JPL has been a performer in most major ground robotics programs in
DARPA and the Army since the 1980s, including the DARPA Demo II,
Tactical Mobile Robotics, Mobile Autonomous Robot Software (MARS),
MARS 2020, PerceptOR, Unmanned Ground Combat Vehicle (UGCV), and
UGCV-PerceptOR Integration, plus the Army Demo I, Demo III, Robotics
Collaborative Technology Alliance, and Future Combat
Systems/Autonomous Navigation System programs. JPL's focus in these
programs has included 3-D perception and terrain classification for
obstacle detection, visual odometry, and vision-guided control. Key
achievements include the most extensive experience with stereo
vision-based autonomous navigation in the U.S. and the first
vision-guided autonomous stair-climbing.  Recently, JPL has also begun
to apply its vision technologies to Navy applications, such as
autonomous navigation of Unmanned Sea Surface Vehicles.  JPL's
extensive involvement in DARPA, Army, Navy, and NASA robotics programs
enables JPL to rapidly transfer technology developed in DARPA programs
to user services.  Contact: Larry Jackel (DARPA IPTO).
