%Master file: follower07.tex

\section{Controller Design \label{sec: controller_design}}

%=== premises ====================================================
Before describing the control strategy, we give the premises under which it was evaluated: The trajectories sent to the controller are reference paths for the center of the rear axle. Each point along the trajectory is associated with reference direction, curvature, speed and acceleration. Velocity profiles are feasible with respect to steer dynamics, cf.~Section~\ref{sec: prototype_vehicle}.
%A velocity planner in connection with a longitudinal controller assures that the  vehicle's speed through turns is slow enough to facilitate accurate trajectory tracking. 
Delays are assumed to be constant and known. These premises are not indespensible. However, relaxing them leads to degraded performance. 

%=== controller design ===========================================
The proposed lateral control strategy is intuitively appealing and easily explained using Figure~\ref{fig: lateral_forward}. The real vehicle is projected orthogonally onto the closest point of the reference trajectory. The  rear axle center, $O$, is projected onto $R$ and the yaw of the arising \emph{virtual} vehicle is aligned with the tangent of the trajectory at $R$. The front wheels of the projected vehicle are turned so that its turning radius coincides with the curvature of the reference trajectory at $R$, thus keeping it on the reference trajectory. 

The angle of the real vehicle's front wheels with respect to its yaw is set to $\phi$, defined in Figure~\ref{fig: lateral_forward}. For the special case of $l_{1} = L$ this is equivalent to pulling a wagon with the handle pointing towards $S$.
\begin{figure}[h]
   \centering
   \includegraphics[width=8cm]{figs/lateral_forward.pdf}\\
   \caption{Graphical illustration of the proposed control strategy.}
   \label{fig: lateral_forward}
\end{figure}

In a system subject to computational and actuation delays~$\tau$, the steering angle of the virtual reference vehicle is not computed from the reference trajectory's curvature at $R$, but rather its curvature at $F$. Assuming the vehicle travels with forward speed $v$, $F$ is chosen such that the trajectory arc distance $RF_{traj}$ is traversed in time~$\tau$:
\begin{equation}
  \label{equ: delay} 
  RF_{traj} = v(t) \cdot \tau(t).
\end{equation} 
Exploiting symmetry, the controller can easily be modified for reverse trajectory tracking. This is done by mirroring the (real) vehicle through its rear axle and applying the control strategy to the mirrored vehicle, as shown in Figure~\ref{fig: lateral_reverse}.
\begin{figure}[h]
  \centering
  \includegraphics[width=8cm]{figs/lateral_reverse.pdf}\\
  \caption{Illustration of how the proposed control strategy is extended to the case of reverse driving.}
  \label{fig: lateral_reverse}
\end{figure}

\subsection{Nonlinear Region \label{sec: nonlinear}} 
Analysis of the nonlinear region was done numerically, facilitating phase portraits. Figure~\ref{fig: phase_large} shows a phase portrait generated using the kinematic model (\ref{equ: kinematic lateral}), (\ref{equ: kinematic yaw}), with parameters $l_{1}=3.55~\textnormal{m},l_{2}=4.00~\textnormal{m}$ and $r_{c}= 20.00~\textnormal{m}$. The origin is a stable stationary point. The vertical curves seen at $e_{\theta}\approx \pm \pi$ are sets of stationary points, however, unstable. They arise when the control is in the limit between full right and left, as a result of the yaw being off the direction $\overrightarrow{OS}$ by $(2n+1)\pi$, as shown in Figure~\ref{fig: lateral_forward}. The phase plot shows that, except for the curves of unstable equilibra, all states converge to the origin.  
\begin{figure}[h]
  \centering
  \includegraphics[width=8cm]{figs/phase_large.pdf}\\
  \caption{Phase plot showing region in $e_{\perp},e_{\theta}$-space, generated with parameters $l_{1}=3.55~\textnormal{m},l_{2}=4.00~\textnormal{m}, r_{c}=20.00~\textnormal{m}$.}
  \label{fig: phase_large}
\end{figure}

\noindent The phase portrait shown in Figure~\ref{fig: phase_small} is equivalent to  Figure~\ref{fig: phase_large}, except that $r_{c} = 4~\textnormal{m}$. Notice that the origin is no longer a stationary point and that limit cycles arise as a consequence of $r_{c}$ being smaller than the minimal turning radius $r_{min}~=~7.35$~m.
\begin{figure}[h]
  \centering
  \includegraphics[width=8cm]{figs/phase_small.pdf}
  \caption{Phase plot showing region in $e_{\perp},e_{\theta}$-space, generated with parameters $l_{1}=3.55~\textnormal{m},l_{2}=4.00~\textnormal{m},r_{c}=4.00~\textnormal{m}$.}
  \label{fig: phase_small}
\end{figure}

\subsection{Linear Region}
The controller is nominally operating around $(0,0)$ in $e_{\theta},e_{\perp}$-space. Linearizing the closed loop system around this point when $r_c \rightarrow \infty$ yields a double integrator
\begin{eqnarray}
  \label{equ: linear model}
  \left[
    \begin{array}{c}
      \frac{de_{\perp}}{ds} \\[1 ex]
      \frac{de_{\theta}}{ds}
    \end{array}
    \right] 
  &=&
  \left[
    \begin{array}{cc}
      0 & 1 \\
      0 & 0
    \end{array}
    \right]
  \left[
    \begin{array}{c}
      e_{\perp} \\
      e_{\theta}
    \end{array}
    \right]
  +
  \left[
    \begin{array}{c}
      0 \\
      \frac{1}{L}
    \end{array}
    \right] \phi \\
  \phi & = & -\frac{1}{l_{2}}\left(e_{\perp} + e_{\theta} l_{1} \right) - e_{\theta} \nonumber.
\end{eqnarray}
A Bode plot for $l_{1}=3.55~\textnormal{m},l_{2}=4.00~\textnormal{m}$ is shown in Figure~\ref{fig: bode}. Figure~\ref{fig: cutoff_freq} shows the cutoff 'frequency' [rad m$^{-1}$] as a function of $l_{1},l_{2}$. In Figure~\ref{fig: max_speed} the maximal allowed speed for maintaining a $30^{\circ}$ phase margin, assuming a constant~0.4~s actuation delay (in accordance with Alice) is given as a function of $l_{1},l_{2}$. 

\begin{figure}[h]
  \centering
  \includegraphics[width=8cm]{figs/bode.pdf}\\
  \caption{Bode plot for the closed loop system with $l_{1}=3.55~\textnormal{m},l_{2}=4.00~\textnormal{m}$, linearized around $e_{\perp}=0~\textnormal{m},e_{\theta}=0~\textnormal{rad}$.}
  \label{fig: bode}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=8cm]{figs/cutoff_freq.pdf}\\
  \caption{Cutoff 'frequency' [rad m$^{-1}$] as a function of $l_{1},l_{2}$ with a contour plot in the $l_{1},l_{2}$-plane.}
  \label{fig: cutoff_freq}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=8cm]{figs/max_speed.pdf}\\
  \caption{Maximal allowed speed to maintain a phase margin of $30^{\circ}$ as a function of $l_{1}$ and $l_{2}$, when actuation is subject to a constant 0.4 s delay. Equipotential curves are shown in the $l_{1},l_{2}$-plane.}
  \label{fig: max_speed}
\end{figure}

\subsection{Choice of Parameters}
The parameter $l_{1}$ tells which point along the central axis of the vehicle, will be controlled. Choosing $l_{1}=L$ yields a controller which does not result in overshooting of the guiding wheels, when recovering from an error. Shorter $l_{1}$ will enable faster control, but may result in overshooting of the vehicle front, whereas longer $l_{1}$ yield a slower, overdamped system. 

The parameter $l_{2}$ acts as a 'gain' for the control signal. Small values of $l_{2}$ result in a fast closed loop system, but also large control signals, possibly saturating the steering angle and degrading performance. Up to this point the analysis has assumed no bound on $\dot{\phi}$, but in practice it is limited to $|\dot{\phi}|_{\max} = 0.2~\textnormal{rad s}^{-1}$ on Alice. With this in mind a reasonable gain schedule for $l_{2}$ is $l_{2}= k \cdot v$, where $v$ is the speed of the vehicle along its path and $k$ is a constant. This gives approximately the same $\dot{\phi}$, indenpendently of $v$, for given lateral and angular errors. 

\subsection{Integral Action}
The proposed controller was augmented with integral action,
\begin{eqnarray}
  \label{equ: integral} 
  \frac{dI}{dt} &=& \frac{\left[e_{\perp}(t) + l_{I} \cdot \sin \left( e_{\theta}(t) \right) \right] v(t)^{\rho}}{T_{i}}, \\
  \phi &=& \phi_{nom} + I \nonumber.
\end{eqnarray} 
The error metric, where $e_{\perp}$ is the lateral rear axle error and $e_{\theta}$ is the yaw error, is equivalent to measuring the lateral error of the vehicle's center line, a distance $l_{I}$ in front of $O$.

The update is gain scheduled with respect to current velocity, through $\rho$. Empirically $\rho = 0.5$ was found to work well.

The power steering PID loop sucessfully depresses load disturbances. Thus, the only role of the integral action is to account for miscalibrations in steer angle  measurement. It was possible to make the integral slow enough not cause noticable overshoots, becaue of the low frequency nature of the miscalibration.   
