log = load('/home/users/soltesz/followerInternalLog/2007-10-14-Sun-eltoro/followerInternalLog-.2007-10-14-Sun-16-16.log');
%log = load('/home/users/soltesz/followerInternalLog/2007-09-04-Tue/followerInternalLog-.2007-09-04-Tue-12-14.log');

time = (log(:,1)-log(1,1))./1e6;

%time1 =2355; % seconds
%time2 =2415; 
%duration = time2-time1;

%log = log((find(time>time1,1):find(time>time2,1)),:);
%time = (log(:,1)-log(1,1))./1e6;


la_phi = log(:,5);
la_I = log(:,6);

curN = log(:,16);
curNd = log(:,17);
curE = log(:,18);
curEd = log(:,19);
curYaw = log(:,20);

closestN = log(:,22);
closestNd = log(:,23);
closestE = log(:,25);
closestEd = log(:,26);

errorSign = sign(closestEd.*(curN-closestN)-closestNd.*(curE-closestE));
crossError = errorSign.*sqrt((curN-closestN).^2+(curE-closestE).^2);

yawError = curYaw-atan(closestE./closestN)-pi;

figure(1)
subplot(2,1,1)
%plot([0,duration],[0 0],'Color','black')
hold on
[AX,H1,H2] = plotyy(time, crossError, time, yawError);
set(get(AX(1),'Ylabel'),'String', 'cross track error / m   (solid)');
set(get(AX(2),'Ylabel'),'String', 'yaw error / rad   (dashed)');
%axes(AX(1)); 
%axis([0 60 -0.3 0.3])
%set(gca,'YTick',-0.3:0.1:0.3)
%axes(AX(2)); 
%axis([0 60 -0.2 0.2])
%set(gca,'YTick',-0.2:0.1:0.2)
%set(H1,'LineStyle','-');
%set(H2,'LineStyle','--');
xlabel('time / s');
title('control errors');

subplot(2,1,2)
%plot([0,duration],[0 0],'Color','black')
hold on
[AX,H1,H2] = plotyy(time, la_phi, time, la_I);
set(get(AX(1),'Ylabel'),'String', 'steer angle / rad   (solid)');
set(get(AX(2),'Ylabel'),'String', 'integral part / rad   (dashed)');
%axes(AX(1)); 
%axis([0 60 -0.2 0.2])
%set(gca,'YTick',-0.2:0.1:0.2)
%axes(AX(2)); 
%axis([0 60 -0.04 0.04])
%set(gca,'YTick',-0.04:0.02:0.04)
%set(H1,'LineStyle','-');
%set(H2,'LineStyle','--');
xlabel('time / s');
title('steer angle and integral part');


%figure(2)
%hist(crossError)