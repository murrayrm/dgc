% Master File: dgc07-final.tex
\section{Results}
\label{sec:results}

Extensive testing on Alice was used to validate its capabilities and
tune its performance.  This section summarizes the major results of
this testing.

\subsection{Site Visit}
\instructions{Describe performance at the site visit, and changes
undertaken to mitigate identified short-falls.}

The site visit consisted of four separate runs around a simple
course consisting of a single intersection and a circular loop, as
shown in Figure~\ref{fig:sitevisit}.  
\begin{figure}
  \centering
  \includegraphics[height=2.5in]{sitevisit-course.eps} \quad
  \includegraphics[height=2.5in]{sitevisit-photo.eps}
  \caption{Site visit course.}
  \label{fig:sitevisit}
\end{figure}
After initial safety inspection and e-stop test, the first run
consisted of driving around the loop once and was performed
successfully.

The second run was a path planning run, in which a set of sparse
waypoints were given and a route had to be planned that included
performing U-turn operations in the stubs.  On our first attempt at
this run, the vehicle failed to perform the U-turn successfully, with
an apparent loss of steering.  A combination of high temperatures and
a road surface that created large frictional forces with the tires
caused a torque limit to be reached in the motor controller,
resulting in a reset in the steering controller.  This problem was
remedied in a second attempt (after the fourth run) by reseting an
internal parameters that lowered the commanded steering at slow
speeds.  After this change the test was performed successfully.

The third run involved driving multiple times around the loop with
obstacles (stationary cars) placed at various points on the route.
Alice detected and avoided all obstacles, and completed the run.  For
tests in which a vehicle was in the lane of travel, Alice signaled
properly to move out of the lane and transitioned out of the lane at
the required distances.  Alice did not transition back into the lanes
within the required distances, an artifact of the way in which the
planning algorithm was implemented (there was an insufficiently high
cost associated with gradually returning to the proper lane).

The fourth run focused on intersection operations.  The run
consisted of driving multiple times around the loop, with cars
positioned at the intersection in different situations for each loop.
Alice properly detected vehicles and respected the precedence order
except for two occasion. 
\begin{itemize}
\itemsep 0pt
\item In one instance, there were two cars queued up at the
intersection opposite Alice. When Alice approached the intersection,
it stopped, waiting a few seconds, and then continued through the
intersection. According to the safety driver (who was in Alice), we had a 
small return coming up to the intersection and then the LADARs tilted
down when we stopped. This caused the obstacles to disappear
completely (the map subsystem had no memory at this point) and then
reappear, so Alice decided that we were the first vehicle at the
intersection. 

\item In the second instance, two vehicles were queued to the left of
Alice. We stopped at the intersection and waited for the first
vehicle. After that vehicle passed, we continued to wait at the
intersection. After waiting for a while, DARPA motioned the second
vehicle to go through and at that point Alice properly continued
through the intersection. According to the internal logs, the second
vehicle was partially in the opposing lane and that Alice interpreted
this as a vehicle in the intersection, so it remained stopped.
\end{itemize}

While the site visit was executed more or less successfully, we
identified several 
of limitations in the design.  A major difficulty in preparing for
the site visit was the brittleness of the finite state machine logic
that accounted for traffic rules.  Even with the limited complexity of
the site visit tasks, the planner had dozens of states to account for
different environmental conditions and driving modes.  This made the
planner very hard to debug.  Some of the lower level control functions
(including path following) were also found to be lower performance
that we desired for the race.  And finally, the accuracy and
persistence of the sensed data need to be improvied.

The primary changes that were made after the site visit were (1) to
simplify the traffic logic to use a very small number of modes; (2) to
redesign the planning subsystem so that it made use of a graph-based
planner instead of the originally proposed NURBS-based planner and (3)
to streamline the planner software structure so that it acted as a
single CSA module rather than separate modules for each internal
function.  These changes coincided with a decision to separate the
path planning problem into a spatial planner (rail-planner) and a
temporal planner (velocity-planner), rather than the originally
planned spatio-temporal planner (dplanner).  In addition, we rewrote
the low-level control algorithms (follower) and implemented more
robust functionality for detecting and tracking objects.

\subsection{Summer Testing}

During the summer of 2007, extensive testing and development was
performed at two primary test sites: the former St.\ Luke Medical
Center in Pasadena, CA and El Toro Marine Corps Air Station in Irvine,
CA.  Over the course of three months, approximately 300 miles of fully
autonomous driving was performed in these locations.

Testing at the St.\ Luke Medical Center was performed in the (empty)
parking lot of the facility, shown in Figure~\ref{fig:stluke}.
\begin{figure}
  \centering
  \includegraphics[height=2.3in]{stluke-rndf.eps} \quad
  \includegraphics[height=2.3in]{stluke-photo.eps} 
  \caption{St. Luke Medical Center (Pasadena, CA)}
  \label{fig:stluke}
\end{figure}
While this area was quite small for testing, its proximity to Caltech
allowed us to use the facility frequently and easily.  A standard
course was set up which could be used to verify the basic driving
functionality and track performance.  Some of the features of this
course included tight turns, sparse waypoint areas, parking zones,
overhanging buildings and trees, and tight maneuvering between
structures.

El Toro Marine Corps Air Station was used for more extensive testing.
This base is no longer in active use and was available for lease
through a property management corporation.  The primary RNDF used for
testing is shown in Figure~\ref{fig:eltoro}
\begin{figure}
  \centering
  \includegraphics[height=2.3in]{eltoro-rndf.eps} \quad
  \includegraphics[height=2.3in]{eltoro-photo.eps} 
  \caption{El Toro Marine Core Air Station test area (Irvine, CA).}
  \label{fig:eltoro}
\end{figure}
This facility had all of the features specified in the DARPA Technical
Criteria, including multiple types of intersections, multi-lane roads,
parking zones, off-road sections, sparse waypoints, overhanging trees
and tightly spaced buildings.

A total of 15 days of testing at El Toro were used to help tune the
performance of the vehicle.  The first long run with no manual
interventions was a run of 11 miles on 19 September 2007,
approximately 6 weeks before the race.  The most number of miles
driven in a single day was 40.5 miles on 16 October 2007.  The highest
average speed on a run of over 5 miles was 9.7 miles/hour on 16
October 2007.  Additional testing included intersection testing with
up to five vehicles, merging across traffic with cars coming from both
directions, and defensive driving with traffic coming into the lane
from a driveway and oncoming traffic driving in the incorrect lane.

\subsection{National Qualifying Event}
\instructions{Describe the team's performance at each of the three NQE
test areas.  This may include difficulties encountered in completing
these tests and changes made.}

In this section we describe Team Caltech's performance in each of the
three NQE test areas.  We present each run in chronological order.

\paragraph{Run 1: Area B, Attempt 1}
Area B consisted of tasks in basic navigation, including route
planning, staying in lanes, parking and obstacle avoidance.  An
overview of Area B is shown in Figure~\ref{fig:areab}.
Basic navigation, stay in lane, parking. 
\begin{figure}
  \centering
  \includegraphics[height=2.3in]{areab-rndf.eps} \quad
  \includegraphics[height=2.3in]{areab-photo.eps} 
  \caption{Test Area B}
  \label{fig:areab}
\end{figure}
The MDF started in the starting chute area, then directed Alice to
proceed down a road onto the main course.  From there, the MDF direted
Alice to drive down several different roads on the interiod of the
course and eventually return to the starting area.

Alice encountered several difficulties on this run.  First, the
K-rails (concrete traffic barriers) in the startup chute were less
than 1m away from Alice and the vehicle did not want to leave the
chutes immediately. The same problem occurred at the exit of the
startup area where K-rails formed a narrow gate.  In order to proceed
through the area, Alice had to progress through a series of internal
planning failures before finally driving with reduced buffers on each
side of the vehicle.  After successfully leaving the area after about
5 minutes, Alice was performing well on the roads and entered the
parking zone in the south part of the course.  The spacing of the
vehicles to each side of Alice was less than the required 1 meter
buffer and Alice spent substantial time attempting to reorient itself
to park in the spot.  Once in the spot, Alice was unable to pull fully
into the parking spot because the car in front of it was closer than
the required 2 meter buffer.  Alice was then manually repositioned and
continued its run for a short period before the 30 minute time limit
was reached.

As a result of this run, the code was changed to allow passing
obstacles that are closer than 1m away from the vehicle. In addition,
the tolerance of reaching waypoints in parking zones was relaxed.

\paragraph{Run 2: Area A: Attempt 1}
This test consisted of merging into traffic with 10--12 manned vehicles
circling around a ``block'', as shown in Figure~\ref{fig:areaa}.
\begin{figure}
  \centering
  \includegraphics[height=2.3in]{areaa-rndf.eps} \quad
  \includegraphics[height=2.3in]{areaa-photo.eps} 
  \caption{Test Area A}
  \label{fig:areaa}
\end{figure}
Vehicles are started in the center lane of the course and are supposed
to make constant left turns, proceeding around the left loop of the
course in the counterclockwise direction.  Four vehicles with
approximately equal spacing are circling around the larger loop in the
counterclockwise direction.  Six or more vehicles clustered together
in groups of 1, 2 or 3 vehicles are circling the opposite direction.
At the south intersection, Alice needs to merge into traffic after
crossing one lane.  At the north intersection Alice is supposed to
make a left turn into oncoming traffic. The manned vehicles had a
separation distance of 2 to 20 seconds. Therefore Alice had to sense a
10 second or longer gap and merge quickly into the gap.

In the first attempt of NQE run A, several bugs were uncovered.  The
first occured when Alice entered the intersection after determining
that the path was clear.  The proximity of a set of concrete barriers
to the road meant that Alice could not complete the turn without
coming close the the barriers.  The low-level reactive obstacle
avoidance logic was using a different threshold for safe operation on
the side of the vehicle (2 meters instead of 1 meter) and hence it
would stop the vehicle partway through the intersection.  This caused
the intesection to become jammed (and generated lots of honks).

A second, related bug occurred in the logic planner that affected our
wait at the intersection properly.  While the intersection handler was
active, another part of the higher-level logic planner could switch
into the STOPOBS state if it detected a nearby vehicle (e.g, one of
the human-driven cars was predicted to collide with Alice or its
buffer region).  This change in state de-activated the intersection
handler and could cause the vehicle to enter the intersection when the
path became clear (without invoking the proper merge logic).
Table~\ref{tbl:areaa1} gives a detailed analysis of the operation at 
each intersection.
\begin{table}
\caption{Analysis of performance in Area A, Attempt 1.}
\label{tbl:areaa1}
\centering\small
\begin{tabular}{|r|c|c|c|r|l|}
  \hline
  & Location & Vehicles & Missed & Time & Comments \\
  & & passed &  gaps & passed & \\
  \hline
  \#1 & S & 8 & N/A & 32.0s & \parbox{2.9in}{Interrupted by STOPOBS;
    prediction active} \\
  \hline
  \#2 & N & 1 & 0 & 7.7s & Clean merge \\
  \hline
  \#3 & S & 6 & N/A & 17.0s & \parbox{2.9in}{Interrupted by STOPOBS;
    prediction active} \\
  \hline
  \#4 & N & 2 & 0 & 14.6s & Clean merge \\
  \hline
  \#5 & S & 11 & 1 & $>$60.0s & \parbox{2.9in}{Canceled by STOPOBS;
    prediction not active.  Almost hit vehicle} \\
  \hline
  \#6 & N & 0 & 0& 21.0s & \parbox{2.9in}{Stopped too far left; other
    vehicles stopped} \\
  \hline
  \#7 & S & 3 & N/A & 23.0s & \parbox{2.9in}{Canceled by STOPOBS; prediction not
    active} \\
  \hline
  \#8 & N & 3 & 0 & 16.3s & Clean merge \\
  \hline
  \#9 & S & 6 & 0 & 38.2s & Clean merge \\
  \hline
  \#10 & N & 1 & 0 & 7.9s & Clean merge \\
  \hline
  \#11 & S & 4 & N/A & 12.0s & \parbox{2.9in}{Canceled by STOPOBS;
    prediction not activated.  Almost hit vehicle} \\
  \hline
  \#12 & N & 0 & 0 & 9.4s & \parbox{2.9in}{Stopped too far left; other vehicles
    stopped} \\
  \hline
  \#13 & S & 6 & N/A & 34.0s & \parbox{2.9in}{Interrupted by STOPOBS;
    clean merge} \\
  \hline
  \#14 & N & 0 & 0 & 4.2s& Clean merge \\
  \hline
\end{tabular}
\end{table}
While in some cases the intersection handler was just interrupted but
called again, it was canceled completely in other cases. If it was
canceled, prediction was also not active. In these cases it almost
caused two accidents with manned vehicles.  At the north intersection,
the software bug did not occur as logic planner did not switch into
STOPOBS. This can be explained by the fact that by the nature of this
intersection, no vehicle was crossing in front of Alice. As a result,
merging was clean in all 7 scenarios at the north intersection.

Fixing the issues that were uncovered during this test required
extensive changes at the NQE.  First, the logic for reactive obstacle
avoidance had to be changed to use a different safety buffer length in
the front of the vehicle versus the sides (consistent with the logic
used by the planner).  Secondly, a rather major restructuring of the
logic planner was required to insure that it did not skip the
intersection handling logic until it had actually cleared an
intersection.  The changes were difficult to test at the NQE, even
with extensive use of the testing areas (where no live traffic was
allowed) and simulation.

\paragraph{Run 3: Area C, Attempt 1}
Area C was designed to test intersection precedence, route planning
and U-turn capabilities.  The RNDF consisted of two intersections
connected by a set of three roads, as shown in Figure~\ref{fig:areac}.
\begin{figure}
  \centering
  \includegraphics[height=2.3in]{areac-rndf.eps} \quad
  \includegraphics[height=2.3in]{areac-photo.eps} 
  \caption{Test Area C}
  \label{fig:areac}
\end{figure}
The major task in NQE run C was the correct handling of intersections
with vehicles having precedence and to perform a U-turn at a road
block.  At the start of the run, the inner road between the
intersection is blocked and the other roads are opened.  The vehicle
is commanded to go in a loop between the two intersections.  At each
successive intersection, a more complicated scenario is established.
On the final run, one of the outer paths is blocked, requiring the
vehicle to replan and choose a different route.

Table~\ref{tbl:areac1} gives an analysis of Alice's performance.
\begin{table}
\caption{Analysis of performance in Area C, Attempt 1.}
\label{tbl:areac1}
\centering\small
\begin{tabular}{|r|c|c|c|c|r|l|}
  \hline
  & Loc & \# Veh & \# Veh & Lost & Dur. & Comments \\
  & & w/ prec & seen & visibility & & \\
  \hline
  \#1 & N & 0 & 0 & 0 & 2.4s & \parbox{3in}{Empty intersection.
    Correct execution} \\ 
  \hline
  \#2 & S & 1 & 1 & N/A & N/A & \parbox{3in}{Interrupted
  by steering fault.  Correct execution} \\
  \hline
  \#3 & N & 2 & 2 & 0 & 25.8s & Correct execution \\
  \hline
  \#4 & S & 2 & 2 & 1 & 25.8s & \parbox{3in}{Correct
  execution. One vehicle was blocking Alice's view while passing
  through intersection} \\
  \hline
  \#5 & N & 3 & 3 & 1 & 34.8s & \parbox{3in}{Correct execution.
    One vehicle was blocking Alice's view while passing through
    intersection} \\
  \hline
  \#6 & S & 1 & 1 & 2 & 25.8s & \parbox{3in}{Following at
    intersection, then giving precedence.  Correct execution.} \\
  \hline
  \#7 & N & 3 & 3 & 2 & 37.8s & \parbox{3in}{Correct
  execution.  One vehicle was blocking Alice's view while passing
  through intersection} \\
  \hline
\end{tabular}  
\end{table}
The columns of the table indicate the intersection that was
encountered, the number of vehicles at the intersection that had
precedence at the time Alice arrived, the number of vehicles detected
by Alice, and the number of times visibility was occluded by another
vehicle.

Alice gave precedence correctly at all 7 intersections. At
intersection \#2 it was by accident that the power steering problems
occurred when the other vehicle had already passed the
intersection. While Alice was stationary, a troque fault in the steering caused
a lower-level module to pause Alice for safety reasons. This event
also triggers planner to switch into the state PAUSE which stops the
intersection handling algorithm. After the system started up again and
the intersection handler was called, the intersection was clear and
Alice passed the intersection. Otherwise Alice might have made wrong
assumptions about the time of arrival of other vehicles.  
The results from this run also demonstrate that ID tracking, checking
for lost IDs and checking for visibility are crucial to the correct
execution of the precedence.  Without those backup algorithms, Alice
would have misinterpreted the precedence order at intersections or
would have lost vehicles in its internal precedence list.

After the intersection tests, Alice had to demonstrate correct
execution of U-turns in front of road blocks.  A bug was introduced in
implementing the changes from Area A that caused the mission planner
to crash during certain U-turn operations.  The process controller
properly restarted the mission planner after the crash, but Alice lost
information regarding which part of the road was blocked.  It thus
alternated between the two road blocks and the run could not be
finished within the time out limit but was considered a successful
clean run.

The bug that caused the mission planner to crash was fixed in response
to the results from this run.

\paragraph{Run 4: Area B, Attempt 2}
Despite fixing the problems near the starting chute based on the
previous attempt in Area B, Alice still had difficulty initializing to
the properly state when it was placed in the startup chute.  Due to
delays in the launch of the vehicle by DARPA, we were able to correct
the logic in the chute and launch the vehicle correctly.

With the changes in the buffer region, Alice was able to traverse
through the start area and onto the course with little difficulty.  At
one point toward the beginning of the run, the control vehicle paused
Alice because it appeared to be headed toward a barrier.  This appears
to be due to a checkpoint that was close to a barrier and hence Alice
was coming close to the barrier in order to cross over the
checkpoint.  Alice was put back into run mode and continued properly.

The remainder of the run was completed with only minor errors.  Alice
properly parked in the parking lot (the cars on the sides had been
removed) and proceeded through the ``gaunltet'', a stretch of road in
which a variety of obstacles had been placed.  It then continued
driving down several streets and through the northern zone, which had
an opening in the fence.  At several points in the run Alice ran over
the curb after turning at intersections.  Alice completed the mission
in about 23 minutes.

\paragraph{Run 5: Area A, Attempt 2}
In the second attempt at Area A, Alice's logic had been updated to
ensure that intersection handler would not be overwritten by changing
into another state within the logic planner's state machine.
Unfortunately, an unrelated set of bugs caused problems on the second
attempt.  Table~\ref{tbl:areaa2} summarizes the major events on this
run.
\begin{table}
\caption{Analysis of performance in Area A, Attempt 2.}
\label{tbl:areaa2}
\centering\small
\begin{tabular}{|r|r|l|l|l|}
 \hline
  & Time & Action & Failure & Comments \\
 \hline
 \#1 & 14:14:42 & Merging, S inters'n & & 
   Clean merging after 14.9 s \\
 \hline
 \#2 & 14:15:05& Exit of inters'n & Alice stopped &
    \parbox{2in}{Stopped because of close obstacles and prediction} \\
 \hline
 \#3 & 14:15:56 & Left turn & \parbox{1.6in}{Problems following tight
    left turn, hit curb} & Path/follower problems \\
 \hline
 \#4 & 14:16:21 & Merging, N inters'n & & Clean merging after 38.6s \\ 
 \hline
 \#5 & 14:17:21 & Stopping, S inters'n & Stop line problems
    & \parbox{2in}{Didn't stop at stop line and drove into
    intersection. Paused by DARPA} \\
 \hline
 \#6 & 14:19:21 & Merging, N inters'n & & Clean merging after 20.1s \\
 \hline
 \#7 & 14:20:01 & Stopping, S inters'n & Stop line problems
    & \parbox{2in}{Didn't stop at stop line and drove into
    intersection. Paused by DARPA} \\
 \hline
 \#8 & 14:21:04 & Exit out of inters'n & \parbox{1.6in}{Pulling into on coming
    lane} & \parbox{2in}{Prediction stopped Alice, but manned car
    performed evasive maneuver. Paused by DARPA} \\
 \hline
 \#9 & 14:22:28 & End of run & & \\
 \hline
\end{tabular}
\end{table}
The primary errors in this run consisted of properly detecting the
stop lines, which appeared in the logs to jump around in a manner that
had not been previously seen (either in testing or other NQE runs).

To understand what happened at the stop lines, a bit more detail is
required.  The following steps and conditions that are necessary to
stop at stop lines:
\begin{itemize}
\itemsep 0pt
\item Path planner - Creates path to stay in lane and to follow turns 
\item Planner - Search for stop lines close to path and store
stop line information within the path structure 
\item Logic planner - Computes distance between Alice and next stop 
line found within path structure 
\item Logic planner - Depending on distance to stop line, switch in 
state STOP INTERSECTION 
\item Velocity planner - Detects state STOP INTERSECTION and 
modify velocity plan 
\end{itemize}
These steps are necessary as the (spatial) path planner does not take
into account stop lines, but instead relies on the velocity planner to
bring the vehicle to a stop. Therefore the function to find the
closest stop line is the critical part of the algorithm.

All modules communicate with the skynet framework. During the race all 
skynet messages were written into a log file. Therefore the complete run can 
be replayed. Watching this replay and checking the log files, it became obvious 
that the main problem happened in computing the distance between Alice
and the next stop line.  To find the closest stop line, Alice performs
the following actions:
\begin{itemize}
\itemsep 0pt  
\item Search for all RNDF stop lines within this rectangle 
\item Project found RNDF stop lines onto path and choose closest 
\item Query map to obtain sensed stop line position for this stop line 
\item Choose closest node within path, required for velocity planner 
\end{itemize}
In previous runs, sensed stop lines were only stored for 5 cycles
after they were not picked up anymore by the sensors. This threshold
was increased during the NQE as stop lines could not be seen by
sensors when Alice's body was hiding the stop lines.  Having a longer
time-to-live value, 
false-positives were stored longer in the map.  At this time, wrong
data association in the map lead to jumping stop lines.  When the
vehicle approached the stop line in lap \#2 and lap \#3 the data
association was right while approaching the intersection. As it came
closer to the real stop line, the mapper bug assigned a false-positive
stop line that was 3.2 meters behind Alice.  In this case, Alice is
assumed to have passed the stop line and did not stop as the threshold
for passing a stop line was set to 3.0 meters. In lap \#3 the stop
line was moved ahead so that Alice was aiming for a stop line that was
in the middle of the intersection. There was no algorithm in place
detecting sudden changes in stop line positions.
