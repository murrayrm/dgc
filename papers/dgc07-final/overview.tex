% Master File: dgc07-final.tex
\section{System Overview}
\label{sec:overview}

\instructions{Provide a detailed description of the software
architecture, including a description of each module.  This
description should account for all modules that are being submitted as
part of the program deliverables, as well as the modules identified
previously as an attachment to the Track A proposal.  If modules were
identified previously but not used in the final implementation, this
should be noted.

\medskip\noindent
Note: This information will be distributed across the next four
sections and the appendix.
}

\subsection{System Architecture}
A key element of our system is the use of a networked control systems
(NCS) architecture that we developed in the first two grand challenge
competitions.  Building on the open source {\em Spread} group
communications protocol, we have developed a modular software
architecture that provides inter-computer communications between sets
of linked processes~\cite{Cre+06-jfr}.  This approach allows the use
of significant amounts of distributed computing for sensor processing
and optimization-based planning, as well as providing a very flexible
backbone for building autonomous systems and fault tolerant computing
systems.  This architecture also allows us to include new components
in a flexible way, including modules that make use of planning and
sensing modules from the Jet Propulsion Laboratory (JPL).

A schematic of the high-level system architecture that we developed
for the Urban Challenge is shown in Figure~\ref{fig:sysarch}.
\begin{figure}
  \centerline{\includegraphics[width=\textwidth]{swarch.eps}}
  \caption{Systems architecture for operation of Alice in the 2007
  Challenge.  The sensing subsystem is responsible for building a
  representation of the local environment and passing this to the
  navigation subsystems, which computes and commands the motion of the
  vehicle.  Additional functionality is provided for process and
  health management, along with data logging and simulation.}  
  \label{fig:sysarch}
\end{figure}
This architecture shares the same underlying approach as the software
used for the 2005 Grand Challenge, but with three new elements:

\medskip\noindent{\em Canonical Software Architecture for mission and
contingency management}. The complexity and dynamic nature of the
urban driving problem make centralized goal and contingency management
impractical.  For the navigation functions of our system, we have
developed a decentralized approach 
where each module only communicates with the modules directly above
and below it in the hierarchy.  Each module is capable of handling the
faults in its own domain, and anything the module is unable to handle
is propagated ``up the chain'' until the correct level has been
reached to resolve the fault or conflict.  This architecture is
described in more detail in Section~\ref{sec:systems} and builds on
previous work at JPL~\cite{DRRS00-aero, Ing+05-jacic, Ras01-aero}.

\medskip\noindent{\em Mapping and Situational Awareness}.  The sensing
subsystem is responsible for maintaining both a detailed geometric
model of the vehicle's environment, as well as a higher level
representation of the environment around the vehicle, including
knowledge of moving obstacles and road features.  It associates sensed
data with prior information and broadcasts a structured representation
of the environment to the navigation subsystem.  The mapping module
maintains a vectorized representation of static and dynamic sensed
obstacles, as well as detected lane lines, stop lines and waypoints.
The map uses a 2.5 dimensional representation where the world is
projected into a flat 2D plane, but individual elements may have some
non-zero height.  Each sensed element is tracked over time and when
multiple sensors overlap in field of view, the elements are fused to
improve robustness to false positives as well as overall accuracy.
These methods are described in more detail in
Section~\ref{sec:sensing}.

\medskip\noindent{\em Route, Traffic and Path Planning}.  The planning
subsystem determines desired motion of the system, taking into account
the current route network and mission goals, traffic patterns and
driving rules and terrain features (including static obstacles).
This subsystem is also responsible for predicting motion of moving
obstacles, based on sensed data and road information, and for
implementing defensive driving techniques. The planning problem is
divided into three subproblems (route, traffic, and path planning) and
implemented in separate modules. This decomposition was well-suited to
implementation by a large development team since modules could be
developed and tested using earlier revisions of the code base as well
as using simulation environments. Additional details are provided in
Section~\ref{sec:navigation}.

\subsection{Project Modifications}
\instructions{Detail changes to approach described in technical paper.}

The overall approach described in our original proposal and technical
paper were maintained through the development cycle.  After the site
visit, the planning subsystem was modified due to problems in getting
our original optimization-based software to run in real-time.
Specifically, the NURBS-based dynamic planner described in the technical
paper was replaced by a graph search-based planner.  At a high level,
these two planner both generated a path that obeyed the currently
active traffic rules, avoided all static and dynamic obstacles, and
optimized a cost function based on road features and the local
environment.  However, the rail-based planner separated the spatial
(path) planning problem from the temporal (velocity) planning problem
and made use of a partially pre-computed graph to allow a coarse plan to be
developed very quickly.  The revised planner is described in more detail
in Section~\ref{sec:traj-planning}.

In addition, the internal structure of the planning stack was
reorganized to streamline the processing of information and minimize
the number of internal states of the planner.  The probabilistic
finite state machine used to estimate traffic state was replaced with
a simpler finite state machine implementation.

Other differences from the technical paper include:
\begin{itemize}
\itemsep 0pt
\item The final sensor suite included two RADAR units mounted on
separate pan-tilt units and no IR camera.  This approach was used to
allow long-range detection of vehicles at intersection (one RADAR was
pointed in each direction down the road).

\item Rather than using separate obstacles maps from individual LADARs
and fusing them, a single algorithm that processed all LADAR data was
developed.  This approach improved robustness of the system,
especially differentiating static and moving vehicles.

\item The sensor fusion algorithms for certain objects were moved from
the map object directly into the planner.  This allowed better
spatio-temporal fusion and persistence of intermittent objects.
\end{itemize}