% Master File: dgc07-final.tex
\section{Mission and Contingency Management}
\label{sec:systems}

Due to the complexity of the system and a wide range of environments
in which the system must be able to operate, an unpredictable
performance degradation of the system can quickly cause critical system
failure. In a distributed system such as Alice, performance degradation of the
system may result from changes in the environment, hardware and
software failures, inconsistency in the states of different software
modules, and faulty behaviors of a software module. To ensure vehicle
safety and mission success, there is a need for the system to be able
to properly detect and respond to unexpected events related to vehicle's
operational capabilities.

Mission and contingency management is often achieved using a centralized
approach where a central module communicates with nearly every software module
in the system and directs each module sequentially through its
various modes in order to recover from failures. As a result, this
central module has so much functionality and responsibility and easily
becomes unmanageable and error prone as the system gets more
complicated. In fact, our failure in the 2005 Grand Challenge was
mainly due to an inability of this central module to reason and
respond properly to certain combination of faults in the system. This
results from the difficulty in verifying this module due to its
complexity.

The contingency management subsystem comprises the mission planner,
the health monitor and the process  
control modules.   The Canonical Software Architecture (CSA) was
developed to allow mission and contingency management to be achieved
in a distributed manner.  This function works in conjunction with the
planning subsystem to dynamically replan in reaction to
contingencies. The health monitor module actively monitors the health
of the hardware and software to dynamically assess the
vehicle's operational capabilities throughout the course of
mission. It communicates directly with the mission planner
module which replans the mission goals based on the current vehicle's
capabilities. The process control module ensures that all the software
modules run properly by listening to the heartbeat messages from all
the modules. A heartbeat message includes the health status of the
software. The process control restarts a software module that quits
unexpectedly and a software module that identifies itself as
unhealthy. The CSA ensures the consistency of the states of all the
software modules in the planning subsystem. System faults are
identified and replanning strategies are performed distributedly in
the planning subsystem through the CSA. Together these
mechanisms make the system capable of exhibiting a fail-ops/fail-safe
and intelligent responses to a number different types of failures in
the system.

\subsection{Canonical Software Architecture}
\label{sec:csa}

The modules that make up the planning system are responsible for
reasoning at different levels of abstraction. Hence the planning
system is decomposed into a hierarchical framework. To support this
decomposition and separation of functionality while maintaining
communication and contingency management, we implemented the planning
subsystem in a canonical software architecture (CSA) as shown in
Figure~\ref{F:CSSPlanningSystem}. 
\begin{figure}
   \centering 
   \includegraphics[width=1\textwidth]{CSSPlanningSystem.eps}
   \caption{The planning subsystem in the Canonical Software Architecture.
     Boxes with double lined borders are subsystems that will be broken up
     into multiple CSA modules.}
   \label{F:CSSPlanningSystem}
\end{figure}
This architecture builds on the
state analysis framework developed at JPL~\cite{DRRS00-aero} and takes
the approach of 
clearly delineating state estimation and control determination. To
prevent the modules from getting out of sync because of the
inconsistency in state knowledge, we require that there is only one
source of state knowledge although it may be captured in different
abstractions for different modules.

A control module receives inputs and delivers outputs. The inputs
consist of sensory reports (about the system state), status reports
(about the status of other modules), directives/instructions (from
other modules wishing to control this module), sensory requests (from
other modules wishing to know about this modules estimate of the
system state) and status requests (from other modules wishing to know
about this module status). The outputs are the same type as the
inputs, but in the reverse direction (reports of the system state from
this module, status reports from this module, directives/instructions
to other modules, etc).

For modularity, each module in the planning subsystem may be broken
down into multiple CSA modules. A CSA module consists of three
components---\textit{Arbitration}, \textit{Control} and
\textit{Tactics}---and communicates with
its neighbors through directive and response messages, as shown in Figure
\ref{F:CSSControlModule}.
\begin{figure}
   \centering 
   \includegraphics[width=1\textwidth]{CSSControlModule.eps}
   \caption{A generic control module in the Canonical Software Architecture.}
   \label{F:CSSControlModule}
\end{figure}
\textit{Arbitration} is responsible for (1)
managing the overall behavior of the module by issuing a merged directive,
computed from all the received directives, to the \textit{Control}; and (2)
reporting failure, rejection, acceptance and completeness of a
received directive to the \textit{Control} of the issuing
module. \textit{Control} is
responsible for (1) computing the output directives to the controlled
module(s) based on the merged directive, received response and state
information; and (2) reporting failure and completeness of a merged
directive to the \textit{Arbitration}. \textit{Tactics} provides the
core functionality of the module and is responsible for generating a
control tactic or a contiguous series of control tactics, as requested
by the \textit{Control}.

\subsection{Health Monitor and Vehicle Capabilities}
\label{sec:health-monitor}

The health monitor module is an estimation module that continuously
gathers the health of the software and the hardware of the vehicle
(GPS, sensors and actuators) and abstracts the multitudes of
information about these devices into a 
form usable for the mission planner. This form can most easily
be thought of as vehicle capability. For example, we may start the
race with perfect functionality, but somewhere along the line lose a
right front LADAR. The intelligent choice in this situation would be
to try to limit the number of left and straight turns we do at
intersections and slow down the vehicle. Another example arises if the
vehicle becomes unable to shift into reverse. In this case we would
not like to purposely plan paths that require a U-turn. 

From the health of the sensors and sensing modules, the health monitor
estimates the sensing coverage. The information about sensing
coverage and the health of the GPS unit and actuators allow the
health monitor to determine the following vehicle capabilities: (1)
turning right at intersection; (2) turning left at intersection; (3)
going straight at intersection; (4) nominal driving forward; (5)
stopping the vehicle; (6) making a U-turn that involves reverse; (7)
zone region operation; and (8) navigation in new areas.

\subsection{Mission Planner}
\label{sec:mission-planner}

The mission planner module receives the vehicle capabilities
from the health monitor module, the position of obstacles with respect
to the RNDF from the mapper module and the MDF and sends the segment-level
goals to the planner module. It has three main responsibilities and is
broken up into one estimation and two CSA control modules.

\paragraph{Traversibility Graph Estimator}
The traversibility graph estimator module estimates the traversibility
graph which represents the connectivity of the route network. The
traversibility graph is determined based on the vehicle capabilities
and the position of the obstacles with respect to the RNDF. For
example, if the capability for making a left or straight turn
decreases due to the failure of the right front LADAR, the cost of the
edges in the graph corresponding to making a left or straight turn
will increase, and the route involving the less number of these
maneuvers will be preferred. If the vehicle is not able to shift into
reverse, the cost of the edges in the graph corresponding to making a
U-turn will be removed.

\paragraph{Mission Control}
The mission control module computes the mission goals that specify
how Alice will satisfy the mission specified in the MDF and conditions
under which we can safely continue the race. It also detects the lack
of forward progress and replans the mission goals accordingly. The
mission goals are computed based on the vehicle capabilities, the MDF,
and the response from the route planner module. For example, if the
nominal driving forward capability decreases, the mission control will
decrease the allowable maximum speed which is specified in the mission
goals, and if this capability falls below certain value due to the
failure in any critical component such as the GPS unit, the brake
actuator or the steering actuator, the mission control will send a
pause directive down the planning stack, causing the vehicle to stop.

\paragraph{Route Planner}   
The route planner module receives the mission goals from the mission
control module and the traversibility graph from the traversibility
graph estimator module. It determines the segment-level goals which
include the initial and final conditions which specify the RNDF
segment/zone Alice has to navigate and the constraints, represented by
the type of segment (road, zone, off-road, intersection, U-turn,
pause, backup, end of mission) which basically defines a set of
traffic rules to be imposed during the execution of this segment-level
goals, in order to satisfy the mission goals. The segment-level goals
are transmitted to the planner module using the common CSA interface
protocols. Thus, the route planner will be notified by the planner
when a segment-level goal directive is rejected, accepted, completed
or failed. For example, since one of the rules specified in a
segment-level goal directive is to avoid obstacles, when a road is
blocked, the directive will fail. Since the default behavior of the
planner is to keep the vehicle in pause, the vehicle will stay in
pause while the route planner replans the route. When the failure of a
segment-level goal directive is received, the route planner will
request an updated traversibility graph from the traversibility graph
estimator module. Since this graph is built from the same map used by
the planner, the obstacle that blocks the road will also show up in
the traversibility graph, resulting in the removal of all the edges
corresponding to going forward, leaving only the U-turn edges from the
current position node. Thus, the new segment-level goal directive
computed by the \textit{Control} of the route planner will be making a
U-turn and following all the U-turn rules. This directive will go down
the planning hierarchy and get refined to the point where the
corresponding actuators are commanded to make a legal U-turn.

\subsection{Fault Handling in the Planning Subsystem}
\label{sec:fault-handling}

In our distributed mission and contingency management framework, fault
handling is embedded into all the modules and their communication
interfaces in the planning subsystem hierarchy through the CSA. Each
module has a set of different control strategies which allow it
to identify and resolve faults in its domain and certain types of
failures propagated from below. If all the possible
strategies fail, the failure will be propagated up the hierarchy along
with the associated reason. The next module in the hierarchy will then
attempt to resolve the failure. This approach allows each module to be
isolated so it can be tested and verified much more fully for robustness.

\paragraph{Planner} 
The logic planner is the component that is responsible for fault
handling inside the planner. Based on the error from the path planner,
the velocity planner and the follower, the logic planner either tells
the path planner to replan or reset, or specifies a different planning
problem (or strategy) such as allowing passing or reversing, using the
off-road path planner, or reducing the allowable minimum distance from
obstacles. The logic for dealing with these failures can be described
by a two-level finite state machine. First, the high-level state (road
region, zone region, off-road, intersection, U-turn, failed and
paused) is determined based on the directive from the mission planner
and the current position with respect to the RNDF. The high-level
state indicates the path planner (rail planner, clothoid planner, or
off-road rail planner) to be used. Each of the high-level states can
be further extended to the second-level state which completely
specifies the planning problem described by the drive state, the
allowable maneuvers, and the allowable distance from obstacles.

\begin{itemize}
\itemsep 0pt
\item \textit{Road region} The logic planner transitions to the road
region state when the type of segment specified by the mission planner
is road. In this state, the rail planner is is the default path
planner although the clothoid planner may be used if all the
strategies involving using the rail planner fail. There are thirteen
states and twenty seven transitions within the road region state as
shown in Figure \ref{F:LogicPlannerFSMRoad}. The DR,NP state is
considered to be the nominal state. The logic planner only transitions
to other states due to obstacles blocking the desired lane or errors
from the other planners.
\begin{figure}
   \centering 
   \includegraphics[width=1.1\textwidth]{LogicPlannerFSMRoad.eps}
   \caption{The logic planner finite state machine for the road
   region. Each state defines the drive state (DR
   $\equiv$ drive, BACKUP, and STO $\equiv$ stop
   when Alice is at the right distance from the closest obstacle as
   specified by the associated minimum allowable distance from
   obstacles), the allowable maneuvers (NP $\equiv$ no
   passing or reversing allowed, P $\equiv$ passing allowed but
   reversing not allowed, PR $\equiv$ both passing and reversing
   allowed), and the minimum allowable distance from obstacles (S
   $\equiv$ safety, A $\equiv$ aggressive, and B $\equiv$ bare).}
   \label{F:LogicPlannerFSMRoad}
\end{figure}

\item \textit{Zone region} The logic planner transitions to the zone
region state when the type of segment specified by the mission planner
is zone. Reversing is allowed and since the clothoid planner is the
default path planner for this state, the trajectory is planned such
that Alice will stop at the right distance from the obstacle by
default, so only three states and four transitions are necessary
within the zone region state as shown in Figure
\ref{F:LogicPlannerFSMZoneAndOffroad}(a).
\begin{figure}
   \centering 
   \includegraphics[width=1\textwidth]{LogicPlannerFSMZoneAndOffroad.eps}
   \caption{The logic planner finite state machine for the zone
   region (a) and off-road (b). Each state defines the drive state (DR
   $\equiv$ drive, and STO $\equiv$ stop
   when Alice is at the right distance from the closest obstacle as
   specified by the associated minimum allowable distance from
   obstacles) and the minimum allowable distance from obstacles (S
   $\equiv$ safety, A $\equiv$ aggressive, and B $\equiv$ bare).}
   \label{F:LogicPlannerFSMZoneAndOffroad}
\end{figure}

\item \textit{Off-road} The logic planner transitions to the off-road
state when the type of segment specified by the mission planner is
off-road. Since passing and reversing are allowed by default, six
states and ten transitions are necessary within the off-road state as
shown in Figure \ref{F:LogicPlannerFSMZoneAndOffroad}(b).

\item \textit{Intersection} The logic planner transitions to the
intersection state when Alice approaches an intersection. In this
state, passing and reversing maneuvers are not allowed and the
trajectory is planned such that Alice stops at the stop line. The rail
planner is the default path planner. Once Alice is within a certain
distance from the stop line and is stopped, the intersection handler,
a finite state machine comprising five states (reset, waiting for
precedence, waiting for merging, waiting for the intersection to
clear, jammed intersection, and go), will be reset and start checking
for precedence. The logic planner will transition out of the
intersection state if Alice is too far from the stop line, when Alice
has been stopped in this state for too long, or when the intersection
handler transitions to the go or jammed intersection state. If the
intersection is jammed, the logic planner will transition to the state
where passing is allowed.

\item \textit{U-turn} The logic planner transitions to the U-turn
state when the type of segment specified by the mission planner is
U-turn. In this state, the default path planner is the clothoid
planner. Once the U-turn is completed, the logic planner will
transition to the paused state and wait for the next command from the
mission planner. If Alice fails to execute the U-turn due to an
obstacle or a hardware failure, the logic planner will transition to
the failed state and wait for the mission planner to replan.

\item \textit{Failed} The logic planner transitions to the failed
state when all the strategies in the current high-level state have
been tried. In this state, failure is reported to the mission planner
along with the associated reason. The logic planner then resets itself
and transitions to the paused state. The mission planner will then
replan and send a new directive such as making a U-turn, switching to
the off-road mode, or backing up in order to allow the route planner
to change the route. As a result, the logic planner will transition to
a different high-level state.  These mechanisms ensure that Alice will
keep moving as long as it is safe to do so.

\item \textit{Paused} The logic planner transitions to the paused
state when it does not have any segment-level goals from the mission
planner or when the type of segment specified by the mission planner
is pause or end of mission. In this state, the logic planner is reset
and the trajectory is planned such that Alice comes to a complete stop
as soon as possible.
\end{itemize}

\paragraph{Follower} 
Although a reference trajectory computed by the planner is guaranteed
to be collision-free, since Alice cannot track the trajectory
perfectly, it may get too close or even collide with an obstacle if
the tracking error is too large. To address this issue, we allow
follower to request a replan from the planner through the CSA
directive/response mechanism when the deviation from the reference
trajectory is too large. In addition, we have implemented the reactive
obstacle avoidance (ROA) component to deal with unexpected or pop-up
obstacles. The ROA component takes the information directly from the
perceptors (which can be noisy but faster) and can override the
acceleration command if the projected position of Alice collides with
an obstacle. The projection distance depends on the velocity of
Alice. The follower will report failure to the planner if the ROA is
triggered, in which case the logic planner can replan the trajectory
or temporarily disable the ROA. We have also formally verified that
through the use of the CSA, follower either has the right knowledge
about the gear Alice is currently in even though it does not talk to
the actuator directly and the sensor may fail; otherwise, it will send
a full brake command to the gcdrive.
