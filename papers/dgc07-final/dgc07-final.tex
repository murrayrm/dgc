% Master File: dgc07-final.tex
% Document Type: LaTeX
\documentclass[12pt]{article}
\usepackage{times,fullpage}
\usepackage{fancyhdr}
\usepackage{url}
\usepackage[dvips]{color}
\usepackage{epsfig}
\usepackage[small]{caption}
\usepackage[font=small]{subfig}
\usepackage{graphicx}
\usepackage{amsmath,amsfonts}

% Tighten up figures
\renewcommand{\textfraction}{0.0}
\renewcommand{\floatpagefraction}{0.9}
\renewcommand{\topfraction}{0.9}
\renewcommand{\paragraph}[1]{\medskip\noindent{\bf #1.}\ \relax}

% Some commands for formatting information
\newcommand\noinstructions[1]{}
\newcommand\instructions[1]{
  {\color{blue}\noindent\small\it #1}\medskip\noindent\relax}
\newcommand\actionpar[2]{%
  \par\medskip\noindent{\color{red}\bf #1:
  \marginpar{\center\color{red} \small #1} #2}\par\medskip}
\newcommand\action[2]{%
  {\color{red}[{\bf #1:}\marginpar{\center \small #1} #2]}\relax}
\renewcommand\instructions[1]{}
\renewcommand\actionpar[2]{}
\renewcommand\action[2]{}

% Set up headers to put required information at top
\pagestyle{fancyplain}
\renewcommand{\headrulewidth}{0pt}
\fancyhead{}
\addtolength\voffset{-25pt}
\setlength\headsep{25pt}
\chead{\small Team Caltech}

\begin{document}
\title{\vspace*{-0.8in}Sensing, Navigation and Reasoning Technologies \\
  for the DARPA Urban Challenge}
\author{
  Joel W. Burdick \and
  Noel duToit \and 
  Andrew Howard \and 
  Christian Looman \and
  Jeremy Ma \and 
  Richard M. Murray\thanks{Corresponding author: murray@cds.caltech.edu} \and
  Tichakorn Wongpiromsarn
  \\[0.5ex]\and
  California Institute of Technology/Jet Propulsion Laboratory
}
\date{DARPA Urban Challenge Final Report \\ 31 December 2007, Team Caltech}
\maketitle
\thispagestyle{empty}

\vspace*{-6ex}
\begin{center}
Approved for public release; distribution is unlimited.
\end{center}
\vspace*{-4ex}

\subsection*{Abstract} This report describes Team Caltech's technical
approach and results for the 2007 DARPA Urban Challenge.  Our primary
technical thrusts were in three areas: (1) mission and contingency
management for autonomous systems; (2) distributed sensor fusion,
mapping and situational awareness; and (3) optimization-based
guidance, navigation and control.  Our autonomous vehicle, Alice,
demonstrated new capabiliites in each of these areas and drove
approximate 300 autonomous miles in preparation for the race.  The
vehicle completed 2 of the 3 qualification tests, but did not
ultimately qualify for the race due to poor performance in the merging
tests at the National Qualifying Event.


\section{Introduction and Overview}
\label{sec:intro}

\instructions{This section will contain a 1--2 page overview of the
approach that we took and summarize our progress against the
milestones that we original proposed.  Authors: Richard + IPT}

Team Caltech was formed in February of 2003 with the goal of designing
a vehicle that could compete in the 2004 DARPA Grand Challenge.  Our
2004 vehicle, Bob, completed the qualification course and traveled
approximately 1.3 miles of the 142-mile 2004 course.  In 2004-05, Team
Caltech developed a new vehicle---Alice, shown in
Figure~\ref{fig:alice}---to participate in the 2005
DARPA Grand Challenge.  
\begin{figure}
  \centering
  \includegraphics[height=2.2in]{dgc07-alice.eps} \quad
  \includegraphics[height=2.2in]{dgc07-team.eps}
  \caption{Alice, Team Caltech's entry in the 2007 Urban Challenge.}
  \label{fig:alice}
\end{figure}
Alice utilized a highly networked control
system architecture to provide high performance, autonomous driving in
unknown environments.  The system successfully completed several runs
in the National Qualifying Event, but encountered a combination of
sensing and control issues in the Grand Challenge Event that led to a
critical failure after traversing approximately 8 miles.

As part of the 2007 Urban Challenge, Team Caltech developed
new technology for Alice in three key areas:
  (1) mission and contingency management for autonomous systems; 
  (2) distributed sensor fusion, mapping and situational awareness; and
  (3) optimization-based guidance, navigation and control.
This section provides a summary of the capabilities of our 
vehicle and describes the framework that we used the 2007 Urban Challenge.

% \paragraph{Problem Description and Approach}
For the 2007 Urban Challenge, we built on the basic architecture
that was deployed by Caltech in the 2005 race, but provided
significant extensions and major additions that allowed operation in the
more complicated (and uncertain) urban driving environment.  Our
primary approach in the desert competition was to construct an
elevation map of the terrain sounding the vehicle and then convert
this map into a cost function that could be used to plan a high speed
path through the environment.  A supervisory controller provided
contingency management by identifying selected situations (such as
loss of GPS or lack of forward progress) and implementing tactics to
overcome these situations.

To allow driving in urban environments, several new challenges had to
be addressed.  Road location had to be determined based on lane and
road features, static and moving obstacles must be avoided, and
intersections must be successfully navigated.  We chose a deliberative
planning architecture, in which a representation of the environment
was built up through sensor data and motion planning was done using
this representation.  A significant issue was the need to reason about
traffic situations in which we interact with other vehicles or have
inconsistent data about the local environment or traffic state.

The following technical accomplishments were achieved as part of this
program:
\begin{enumerate}
\itemsep 0pt
  \item A highly distributed, information-rich sensory system was
  developed that allowed real-time processing of large amounts of raw
  data to obtain information required for driving in urban
  environments.  The distributed nature of our system allowed easy
  integration of new sensors, but required sensor fusion in both time
  and space across a distributed set of processes.

  \item A hierarchical planner was developed for driving in urban
  environments that allowed complex interactions with other vehicles,
  including following, passing and queuing operations.  A rail-based
  planner was used to allow rapid evaluation of maneuvers and choice of
  paths that optimized competing objectives while insuring safe operation
  in the presence of other vehicles and static obstacles.

  \item A canonical software structure was developed for use in the
  planning stack to insure that contingencies could be handled and
  that the vehicle would continue to make forward progress towards its
  goals for as long as possible. The combination of a
  directive/response mechanism for intermodule communication and
  fault-handling algorithms provide a rich set of behaviors in complex
  driving situations.
\end{enumerate}
The features of our system were demonstrated in approximately 300 miles of
testing performed in the months before the race, including the first
known interaction between two autonomous vehicles (with MIT, in joint
testing at the El Toro Marine Corps Air Station).

A number of shortfalls in our approach led to our vehicle being
disqualified for the final race:
\itemsep 0pt
\begin{enumerate}
\item Inconsistencies in the methods by which obstacles were handled
  that led to incorrect behavior in situations with tight obstacles;
\item Inadequate testing of low-level feature extraction of stop lines
  and the corresponding fusion into the existing map;
\item Complex logic for handling intersections and obstacles that was
  difficult to modify and test in the qualification event.
\end{enumerate}
Despite these limitations in our design, Alice was able to perform
well on 2 out of the 3 test areas at the NQE, demonstrating the
ability to handle a variety of complex traffic situations.

This report is organized as follows: Section~\ref{sec:overview}
provides a high level overview of our approach and system
architecture, including a description of some of the key
infrastructure used throughout the problems.
Sections~\ref{sec:sensing}--\ref{sec:systems} describes in the main
software subsystems in more detail, including more detailed
descriptions of the algorithms used for specific tasks.  A description
of the primary software modules used in the system is included.
Section~\ref{sec:results} provides a summary of the results from the
site visit, testing leading up to the NQE and the team's performance
in each of the NQE tests.  Finally, Section~\ref{sec:conclusions}
summarizes the main accomplishments of the project, captures lessons
learned and describes potential transition opportunities.
Appendix~\ref{sec:additional} provides a listing of additional modules that
are referenced in the report along with a short description of the
module.

\input{overview.tex}
\input{sensing.tex}
\input{navigation.tex}
\input{mission.tex}
\input{results.tex}
\input{accomplishments.tex}

\paragraph{Acknowledgments} The research in this paper was supported
bin part by the Defense Advanced Research Projects Agency (DARPA) under
contract HR0011-06-C-0146, the California Institute of Technology, Big
Dog Ventures, Northrop Grumman Corporation, Mohr Davidow Ventures and
Applanix Inc.

The authors would also like to thank the following members of Team
Caltech who contributed to the work described here:
Daniel Alvarez,
Mohamed Aly,
Brandt Belson,
Philipp Boettcher,
Julia Braman,
% Joel Burdick,
William David Carrillo,
Vanessa Carson,
Arthur Chang,
Edward Chen,
Steve Chien,
Jay Conrod,
Iain Cranston,
Lars Cremean,
Stefano Di Cairano,
Josh Doubleday,
Tom Duong,
Luke Durant,
% Noel duToit,
Josh Feingold,
Matthew Feldman,
Tony \& Sandie Fender,
Nicholas Fette,
Ken Fisher,
Melvin Flores,
Brent Goldman,
Jessica Gonzalez,
Scott Goodfriend,
Sven Gowal,
Steven Gray,
Rob Grogan,
Jerry He,
Phillip Ho,
% Andrew Howard,
Mitch Ingham,
Nikhil Jain,
Michael Kaye,
Aditya Khosla,
Magnus Linderoth,
Laura Lindzey,
% Christian Looman,
Ghyrn Loveness,
% Jeremy Ma,
Justin McAllister,
Joe McDonnell,
Mark Milam,
% Richard Murray,
Russell Newman,
Noele Norris,
Josh Oreman,
Kenny Oslund,
Robbie Paolini,
Jimmy Paulos,
Humberto Pereira,
Rich Petras,
Sam Pfister,
Christopher Rasmussen,
Bob Rasumussen,
Dominic Rizzo,
Miles Robinson,
Henrik Sandberg,
Chris Schantz,
Jeremy Schwartz,
Kristian Soltesz,
Chess Stetson,
Sashko Stubailo,
Tamas Szalay,
Daniel Talancon,
Daniele Tamino,
Pete Trautman,
David Trotz,
Glenn Wagner,
Yi Wang,
% Nok Wongpiromsarn,
Albert Wu,
Francisco Zabala and
Johnny Zhang.


\bibliography{fullabv,citrefs,jplrefs}
\bibliographystyle{plain}

\appendix
\section{Additional Software Modules}
\label{sec:additional}

In addition to the software modules described in the main text, a
number of other modules were used as part of our system.  Those
modules are briefly described here.

\paragraph{ASim} Asim is a dynamic simulator for Alice that replaces
the astate module.  It accepts comments from gcdrive, simuilates the
dynamics of the vehicle (including wheel slippage), and broadcasts the
current vehicle state in a format compatible with astate.

\paragraph{Cotk} CoTK (Console Tool Kit) is a very basic display
toolkit for text consoles.  Implemented as a very thin layer over
ncurses.

\paragraph{Circle Planner}
The circle planner was one of two backup planners for the unstructured
regions. This planner also constructed a graph from a family of
curves. The curves considered in this case was circular arcs and
straight line segments. This was a feasibility planner, and did not
incorporate cost from the cost map. It considered obstacles as hard
constraints. The graph search was done with an A* algorithm. This
planner was very fast, and produced dynamically feasible solutions,
but the solutions looked rather crude due to the family of curves
used, which could easily have been remedied.  This planner was tested
but not used in the race.

\paragraph{DPlanner} An optimization-based planner was developed based
on the use of NURBS basis functions combined with differential
flatness, as described in the original proposal.  This planner relied
on a set of proprietary optimization algorithms that were developed by
Northrop Grumman.  The planner solves the complete spatio-temporal
problem and is thus capable of accounting for the dynamic obstacles in
the environment explicitly. The planner operated on a cost map, but
also enforced hard constraints for obstacles. The solution obtained (a
trajectory) satisfies the dynamics of the vehicle, as well as
constraints on the inputs and state of Alice, while minimizing
some cost function.  The NURBS-based planner was not able to execute
quickly enough to run in real-time, and so a rail-based planner
was developed to replace it.  The dplanner module
was not used in the race.

\paragraph{MapViewer} A lightweight 2-D map and map object viewer
built using FLTK.  Mapviewer can be used to visualize map elements
sent in and out of the mapper module.

\paragraph{RNDF-editor} A JAVA GUI program for editing RNDF files.

\paragraph{Skynet} The skynet library is used for group communications
in Alice.  It is a fairly thin wrapper around Spread.  It supports
broadcasting of messages to a given group name and subscribing to
groups to receive relevant messages.

\paragraph{Sparrow} Sparrow is a collection of programs and a library
of C functions intended to aid in the implementation of real-time
controllers on Linux-based data acquisition and control systems.  It
contains functions for executing control algorithms at a fixed rate,
communicating with hardware interface cards, and displaying data in
real-time.  For the DGC, the real-time data display was the primary
usage.

% \input{ip.tex}

\end{document}
