% Master File: dgc07-final.tex
\section{Sensing Subsystem}
\label{sec:sensing}

The sensing subsystem was developed from approaches for sensing,
mapping and situational awareness that built off past work at
Caltech/JPL. As can be seen from Figure~\ref{fig:sysarch}, there are
essentially three layers to the sensing subsystem. The flow of sensory
data begins with the feeders in function block F4 and travels (via the
SensNet interface) to the perceptors in function block F5. The perceptors then
apply their respective perception algorithms and pass the perceived
features to the map in function block F2, where fusion and
classification is performed. In short, we can classify these layers as
follows: 

\medskip\noindent
\textit{Sensing Hardware \& Feeders}: This layer consists of the
low-level drivers and feeders that make the raw sensed data available
to the perception algorithms. Each sensor is identified by a unique
sensor-ID to keep things organized and allows the ease of
incorporating new sensors as they get introduced. 

\medskip\noindent
\textit{Perception Software}: This layer consists of the
perception algorithms that take in the raw sensed data from the
feeders and apply detection and tracking to extracted features from
the data; such features include lines on the road, static obstacles,
traversable hazards and moving vehicles. 

\medskip\noindent
\textit{Fused Map}: This final layer consists of the vectorized
representation of the world which we refer to as the ``map''. The map
receives all the detected and tracked features that have been extracted
at the perceptor level and fuses them to form a vectorized map
database which is used by the planners.

\medskip
Figure~\ref{fig:viewers} contains screen shots illustrating the
features of the sensing systems.
\begin{figure}
  \centering
  \includegraphics[height=2.4in]{sensviewer.eps} \quad
  \includegraphics[height=2.4in]{mapviewer.eps} 
  \caption{Screen shots from SensViewer (left) and MapViewer (right).}
  \label{fig:viewers}
\end{figure}
The left figures shows the direct data from the various sensors
represented in a 3D view.  Images from the short and medium range
cameras are shown at the top of the figure, and the LADAR, RADAR and
stereo obstacle data are shown below.  This information is processed
by the perceptors, resulting in the fused data shown in the right
figure.  In this representation, obstacles are classified as static or
moving.  Lane data (not shown) is also stored in the map.

The following subsections address each layer of the sensing subsystem,
with detailed descriptions of associated modules that were
respectively used during the qualifying events at the NQE.

\subsection{Sensing Hardware \& Feeders}
\label{sec:sensing-hardware} 

\paragraph{SensNet}  Data from feeders is transmitted to
perceptors using a specialized high-bandwidth, low-latency
communication package called SensNet. SensNet's connection model is
many-to-many, such that each feeder can supply multiple perceptors and
each perceptor can subscribe to multiple feeders.  Perceptors can
therefore draw on any combination of sensors and/or sensor modalities
to accomplish their task (e.g., a road perceptor can use both
forward-facing cameras and forward-facing LADARs). SensNet will also
choose an appropriate interprocess communication method based on the
location of the communicating modules. For modules on the same
physical machine, the method is shared memory; for modules on
different machines, the method is Spread/TCP/Ethernet. 

\paragraph{LADAR Feeder} The LADAR-feeder module works by interfacing
with a variety of laser range finders using existing drivers which had
been written (or rewritten) from previous races. For this particular
race, the laser range finders we had used were the SICK LMS-221, the
SICK LMS-291, and the RIEGL LMS-Q120. Depending on the sensor-ID given
(specified as a command line argument to the module), this module
calls the correct driver to interface with the desired scanner and
broadcasts the resulting scan measurements across SensNet. The range
scan is referenced in the sensor frame yet tagged with the vehicle
state and appropriate matrix transforms to allow for flexibility in
transforming the range points into any desired frame (i.e. sensor,
local, vehicle). 

\paragraph{Stereo Feeder} The stereo-feeder module works by reading in
the raw images from all four cameras (two sets of stereo-camera pairs:
one long-baseline and one medium-baseline). With the known baselines
between cameras in a given stereo-camera pair, a disparity and range
value is calculated for all corresponding pixels in both images using
JPL stereo software. Finally, the raw image with disparity and range
values is then sent across SensNet to all listening modules. 

\paragraph{RADAR Feeder} The radar-feeder module works by interfacing
with a TRW AC20 Autocruise RADAR unit and publishing the data to
SensNet. The AC20 can report up to ten ``targets'' (intermittent,
single-cycle returns) and ten ``tracks''---internally tracked
objects---at a refresh rate of 26 Hz. The internal tracking is fairly
accurate, and when supplied with the vehicle's yaw rate and velocity, can
compensate for its own motion. It filters out stationary returns,
making it ideal for a car perceptor.

\paragraph{PTU Feeder} The Pan-Tilt-Unit (PTU) feeder is the
controlling software that receives panning and tilting commands for
one of two pan-tilt unit devices on the vehicle: the
Amtec Robotics Powercube pan-tilt-unit or the Directed
Perception PTU-D46-17. The panning and tilting commands can be
specified in one of two ways:
\begin{itemize}
\itemsep 0pt
\item specifying pan-tilt angles in the PTU reference frame;
\item specifying a point in the local-frame such that the line-of-site
vector of the pan-tilt unit intersects that point (i.e. such that the
pan-tilt-unit is looking at that point in space).  
\end{itemize}
There are elements of this module that make it work like a feeder and
elements that make it work like an actuator. The module is a feeder in
the sense that it continually broadcasts its pan and tilt angles
across SensNet. It is an actuator in the sense that it listens for
messages that command a movement and executes those commands.  

\subsection{Perception Software}
\label{sec:perception-software}

\paragraph{Line/Road Perceptor} There were essentially two line
perceptors that were written for the DARPA Urban Challenge. The first
line perceptor was identified as the ``Line Perceptor'' module and by
an abuse of notation, the second line perceptor module was identified
as the ``Road Perceptor'' module. A description of each module is
provided below.

The Line Perceptor takes in raw sensory image data from the stereo
camera feeders and applies a perception algorithm that filters out
line features (e.g. stop lines, lane lines, and parking-lane lines)
and sends them to the Map module. The details of the algorithm are
outlined in the following steps: 
\begin{enumerate}
\itemsep 0pt
\item The image is transformed into Inverse Perspective Mapping view
(IPM) given the camera external calibration parameters. IPM works by
assuming the road is a flat plane, and using the camera intrinsic
(focal length and optical center) and extrinsic (pitch angle, yaw
angle, and height above ground) parameters to take a top view of the
road. This makes the width of any line uniform and independent of its
position in the image, and only dependent on its real width in
reality. It also removes the perspective effect, so that lines
parallel to the optical axis will be parallel and vertical. 

\item Spatial filtering is then done on the IPM image, using steerable
filters, to detect horizontal lines (in case of stop lines) or
vertical lines (in case of lanes). The filters are Gaussian in one
direction and second derivative of Gaussian in the perpendicular
direction, which is best suited to detect light line on dark
background.   

\item Thresholding is then performed on the image to extract the
highest values from the filtered image. This is done be selecting the
highest $k$th percentile of values from the image. 

\item Line grouping is done on the thresholded image, using either of:
\begin{itemize}
\itemsep 0pt
\item Hough transform: for detecting straight lines;
\item RANSAC lines: for detecting straight lines;
\item Histograms: a simplified version of Hough transform for
detecting only horizontal and vertical lines;
\item RANSAC splines: for fitting splines to thresholded image.
\end{itemize}
The Hough transform approach is the default mode of operation which
provides flexibility in detecting lines of any orientation or position
in the image. The orientations are searched between $\pm$ 10 degrees
of horizontal or vertical, which allows for lane features that are not
orthogonally aligned to the vehicle to be detected.

\item Validation and localization is then performed on detected lines
and splines to better fit the image and to get end-points for the
lines before finally sending to the Map module. We isolate the pixels
belonging to the detected stop line using Bresenham's line drawing
algorithm, and then convolve a smoothed version of the pixel values on
the line with two kernels representing a rising and a falling edge,
and getting the points of maximum response. The detected lines and
splines are transformed back to the original image plane, and then to
the local frame, which are then sent to the Map module. 
\end{enumerate}

The Road Perceptor module has a very similar architecture to the Line
Perceptor module described above and can be briefly summarized in the
following seven steps:  
\begin{enumerate}
\itemsep 0pt
\item The first step loads images from the left camera taken from the
stereo-feeder modules, which can either be from the middle baseline
pair or the longer baseline pair. The default setting was to use the
middle baseline pair.

\item The next step applies some pre-processing to enhance the loaded
images (e.g. removing the hood of the vehicle from the bottom of the
image and color separation for white vs. yellow lines).

\item Edge detection is next applied to the enhanced image which is
done by applying an operator that extracts the main road hints in the
form of edges or contours from the images.

\item Line extraction is then applied to the detected edges to extract
candidate lines using the probabilistic Hough transform.

\item Line association then classifies the extracted lines as good
lines or bad lines according to their color, geometrical properties,
and relation with other lines.

\item Perspective Translation is then applied to the valid lines in
much the same way described for the Line Perceptor module.

\item Line output is then executed on the translated lines, which are
parameterized and broken into points then sent to the Map module.
\end{enumerate}
Additional details on this algorithm are given in \cite{Per07-report}.

\paragraph{LADAR-Based Obstacle Perceptor} The LADAR-based Obstacle
Perceptor has two main threads, preprocessing and tracking. The
preprocessing thread is in charge of retrieving the latest laser range
data (from all available LADARs) from the SensNet module and
transforming it to the local frame. Once in the local frame, the data
is then incorporated into a couple of occupancy grid maps: 
\begin{itemize}
\itemsep 0pt
\item \textit{Static Map} The static map is a map of free and occupied
space represented by cells. All cells are first initialized to a
negative value and when the actual returns are read in, corresponding
cells are given a positive value. This map is used for grouping noisy
obstacles like bushes and berms in an easy manner.

\item \textit{Road Map} The road map contains a map of large smooth
surfaces likely to be road (stored as elevation data). 

\item \textit{Groundstrike Map} The ``groundstrike'' map has cumulative
information about what areas are likely to be LADAR scans generated by
striking the ground. The data in this map is generated primarily from
the elevation data gathered from the sweeping pan-tilt unit. 
\end{itemize} 

The second thread, the tracking thread, relies on discrete tracking of
segments using a vectorized representation instead of a grid-based
representation. For each LADAR, every incoming scan is segmented based
on discontinuities (as a function of distance) and evaluated as a
potential ``groundstrike'' based on the groundstrike probabilities of
its constituent points. If a segment is accepted, it is associated
with existing tracked segments or a new track is created. The function
of the tracking is primarily to detect dynamic obstacles, since
everything not dynamic is treated equivalently as static. Thus tracks
are weighted by how likely they are to be cars (reflectivity, size,
etc). The tracking just uses a standard Kalman filter for the
centroid, though extra care is taken when initializing tracks to avoid
misidentifying changing geometry as a high initial velocity. To
combine the individual LADAR scans, each new set of scans is broken up
into clusters of points (based on point-point distance), again to
merge noisy obstacles into one large blob. For each cluster, the
velocity is computed as the weighted average of each point's
associated segment track, weighted by the status (confidence) of that
track. Basic nearest obstacle association is done between scans; this
is to maintain a consistent obstacle ID throughout the lifetime of a
given obstacle. Dynamic obstacle classification occurs at this level,
and is determined by the distance an obstacle has moved from its
initial position, its velocity, and how long it has been
visible. After a certain amount of time, an obstacle cannot be
unclassified as dynamic. A reasonable geometry for each obstacle is
then computed and the data is sent to the Map module.

\paragraph{LADAR-Based Curb Perceptor}
The ladar-curb-perceptor (LCP) module is intended to detect and track
the right side of the road.  The ``curb'' in question need not be an
engineered street curb, but rather denotes any sufficiently long,
linear cluster of LADAR-visible obstacles that is nearly aligned with
the vehicle's current direction of travel.  This could be a row of
bushes or sandbags, a berm, a building facade, or even a ditch.   The
LCP uses two sensors: the roof-mounted Riegl laser range finder (the
beam which intersects the ground plane $\sim$15 m in front of Alice), and
the middle front bumper SICK laser range finder (the sweep plane of
which is parallel to the ground).  The Riegl enables the LCP to see
obstacles of any height, including negative, but only out to its
ground-intersection distance.  The SICK sees only obstacles over $\sim$1 m
high, but out to a much greater range.  Each scan of the LADARs is
laterally filtered for step-edge detection; points that pass are
considered obstacles which may be part of a curb (this procedure is
robust to ground strikes due to pitching).    Obstacle locations are
converted to vehicle coordinates for aggregation over time, yielding a
2-D distribution as Alice moves forward.  The current set of obstacles
is clipped to a rectangular region of interest (ROI) about 30 m long
along the direction of travel and several lanes wide, with its left
edge aligned with Alice's centerline.  From the entire set of
obstacles, a subset of ``nearest on the right'' is extracted, one for
each of thirty 1 m-deep strips orthogonal to the long axis of the ROI.
A RANSAC line-fitting is performed on these nearest obstacles and the
number of inliers is thresholded.  If below threshold, the
instantaneous decision is that no curb is detected; if above, the
RANSAC-estimated line parameters are the instantaneous measurement of
the curb location.  Both the curb/no-curb decision and the curb line
parameters are temporally filtered for smoothness.

\paragraph{RADAR-Based Obstacle Perceptor} The radar-obs-perceptor
module is simply a wrapper around existing software that is
embedded within the TRW-AC20 Autocruise RADAR. As described earlier
the AC20 can report up to ten ``targets'' and ten ``tracks''; however,
only the tracks are used and sent to the Map module as the targets
have been found to be quite noisy. Since both RADARs are attached to
pan-tilt units, some pre-filtering of the tracks is required. This is
necessary because if the base of the RADAR is moving with respect to
the vehicle, the resulting tracks can be corrupted. To mitigate this,
the RADAR-based obstacle perceptor subscribes to the associated PTU
Feeder module through SensNet and marks detected tracks from the RADAR
as void if the pan-tilt unit was found to have a non-zero velocity at
the timestamp of the detected track. For those tracks that are not
marked as void, they get packaged and stamped with an associated map
element ID and sent to the Map module.

\paragraph{Stereo-Based Obstacle Perceptor} The stereo-obs-perceptor module
uses disparity information provided by the stereo-feeder 
modules to detect generic obstacles. It uses a very simple algorithm,
but works reasonably well. The following outlines the algorithm used
for this perceptor: 
\begin{enumerate}
\itemsep 0pt
\item Given the disparity image $I_{d}$, a buffer $H$ is generated to
accumulate votes from points with the same disparity occurring on a
given image column (similar to a Hough Transform). 

\item The accumulator buffer, $H$, is then searched for peaks higher
than a threshold $T_{H}$. For each peak, it searches for the connected
region of points above a lower threshold $T_{L}$ containing the
peak. One of the interesting features of this accumulator buffer
approach is that it finds most of the vertical features since these
will result in a peak, while flat features (like roads or lane lines)
won't appear as peaks.

\item The next step is to fit a convex hull on the set of points found
and transform the coordinates to local frame. 

\item In this last step, each identified obstacle is tracked in time
and a confidence value (probability of existence) is assigned/updated
based on whether the track was associated with some measure or
not. Only obstacles with a high confidence are sent to the mapper. The
initial confidence is fairly low (0.4 in a scale from 0 to 1), so it
takes a couple of frames before a new obstacle is sent to the map.
\end{enumerate}

When implementing this algorithm, it became quite clear that a major
hindrance in performance was due a bottleneck in the computing of the
disparity of the stereo images. To account for this bottleneck and
increase overall speed of this module, measures were taken to crop
certain regions of the image that usually pertain to the sky or the
hood of the vehicle. This reduced the search space of the disparity
image and increased the cycle time by a few Hz. Other limitations to
this algorithm were also identified and should be noted as well:
\begin{itemize}
\itemsep 0pt
\item Tracking and data association is very basic, but works well for
static obstacles (i.e. no Kalman filter or other Bayesian filter).

\item No velocity information is provided for the tracked obstacles.

\item Sometimes, an obstacle is seen as two or more blobs, and
sometimes two or more obstacles are seen as one. The tracking can deal
with the first situation, but doesn't deal very well with the second,
which is pretty uncommon though.
\end{itemize}

\paragraph{Attention} The Attention module is not so much a perceptor
in that it does not \textit{percept} any particular feature from a
given data set. Instead, the Attention module interfaces directly with
the Planning module and the PTU Feeder module to govern where the
associated pan-tilt unit should be facing. The Attention module
performs in the following manner:
\begin{enumerate}
\itemsep 0pt
\item We receive a directive from the Planning module about which
waypoint the vehicle is planning to go to next and what the current
``planning'' mode is. The planning mode can be either:
\begin{itemize}
\itemsep 0pt
\item INTERSECT LEFT - the vehicle is planning a left turn at an
upcoming intersection 
\item INTERSECT RIGHT - the vehicle is planning a right turn at an
upcoming intersection 
\item INTERSECT STRAIGHT - the vehicle is planning to go straight at
the upcoming intersection 
\item PASS LEFT - the vehicle is planning to pass left 
\item PASS RIGHT - the vehicle is planning to pass right
\item U-TURN - the vehicle is planning a u-turn maneuver
\item NOMINAL - the vehicle is in a nominal driving mode
\end{itemize}

\item A grid-based ``gist'' map is next generated based on the
received directives from the planning module. (The ``gist''
nomenclature comes from work developed in the visual attention
community and is an abstract meaning of the scene referring
to the semantic scene category.) The gist map details which cells in
the grid-based map actually represent the ``scene'', whether the scene
be an intersection, the adjacent lane or a stopped obstacle. For
example, when making a right turn at an intersection, the gist of the
scene would be all lanes associated with the intersection that can
potentially turn into the desired lane. 

\item A weighted cost is then applied to the gist map grid cells that
is dependent on the planning mode. The weighting is chosen such that
areas with high traffic flow and high potential of vehicle collision
are given large weighting while those that are not, are given lower
weighting.  Using the above example for the right turn, the weighting
of the cells associated with those lanes in the gist map would be
chosen such that if any one lane did not have a stop line, a large
weighting would be assigned; if all lanes had stop lines, a uniform
weighting would then be applied.

\item A cost map is then generated which is initialized to the
weighted gist map but keeps a memory of which cells have been
\textit{attended} (explained in the next step). Once a cell in the
cost map has been visited, the cost of that cell is reduced but
allowed to grow at a rate dependent on a specified growth
constant. This allows for the revisiting of heavily weighted cells
while still allowing lesser weighted cells to be visited.

\item Once the cost map is generated, the peak value of the cost map
is then determined as a coordinate point in the local frame and sent
to the PTU Feeder module. The PTU Feeder module will then execute the
necessary motions to \textit{attend} to the desired location. While in
motion, the Attention module is restricted from sending any additional
attention commands to prevent an overflow of pan-tilt commands which
could cause a hardware failure of the unit.

\item Updating the cost map is the final step in the algorithm. The
PTU pan and tilt angles are continually read in from SensNet and the
corresponding line-of-site vector for the pan-tilt unit is
calculated. The grid cells in the cost map that are found to intersect
with the line-of-site vector are then reduced to a zero cost so that
the peak-cell search will not select this already attended
cell. However, the growth of the cell (as described earlier) will then
begin and will grow up to the maximum cost specified by the weighted
gist map.
\end{enumerate}
With regard to the pan-tilt unit on the roof and in the special case
of the NOMINAL planning mode (which is the most common mode where the
vehicle is doing basic lane following), a gist map is not
generated. Instead, a series of open-loop commands are sent to the PTU
Feeder module governing the roof PTU to sweep the area in front of the
vehicle. This behavior allows the LADAR scans from the LADAR range
finder attached to the PTU to generate a terrain map that is then used
to filter out ``groundstrikes'' in the LADAR-Based Obstacle Perceptor
(described earlier).

\subsection{Fused Map}
\label{sec:map}

\paragraph{Mapper} The map is structured as a database of \textit{map
elements} and implements a query based interface to this database. Map
elements are used to represent the world around Alice. A fundamental
design choice was to move away from a 3-dimensional world
representation to a simpler, but less accurate 2.5-dimensional world
representation. Map elements are defined in the MapElement class and
form the basis for this representation. The map elements represent
planar geometry but with a possible uniform nonzero height, and are
used to represent lines in the road as well as obstacles and
vehicles. 

The Mapper module maintains the representation of the environment
which is used by the planning stack for sensor based navigation. It
receives data in the form of map elements from the various perceptors
on a specific \textit{channel} of communication and fuses that data
with any prior data of the course route in Route Network Definition
File (RNDF) format. It then
outputs a reduced set of map elements on a different channel across
the network.The use of \textit{channels} for map element communication
made it easy to isolate which map elements were sent by which
perceptors. This often helped in identifying bugs in the software and
isolating it to either the fusion side of the map or the perception
side of the map. This design choice also allowed for multiple modules
to maintain individual copies of the map, which proved extremely
useful for visualization tools. 

Sensor fusion between different perceptors is also performed in the
mapper module.  This allows objects reported by multiple perceptors to
be reported as a single object.  The sensor fusion algorithm is based
on proximity of objects of the same type.  Groundstrike filtering can
also be done at this stage by ``fusing'' elevation data with obstacle
data (so that obstacles in the same location as the ground plane are
removed.)  Additional logic is required with a moving vehicle object is
in the same location as a static obstacle; in this case the moving
object type takes precedence since some perceptors do not sense
motion.

In the final race configuration, the Mapper module was eventually
absorbed into the Planning module, where it ran as a separate
thread. The purpose of this was twofold: (1) It vastly reduced network
traffic that was increased by the sending and receiving of thousands
of map-elements when Mapper existed as it's own module.  (2) It kept
the only centralized copy of the map within the Planning module, where
it was needed most.
