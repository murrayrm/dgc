% Master File: dgc07-final.tex
\section{Navigation Subsystem}
\label{sec:navigation}

The problem of planning for the Urban Challenge was complicated by
three factors: first, Alice needed to operate in a dynamic
environment with other vehicles. Not only was detection and tracking
of these mobile 
objects necessary, but also their behavior was unknown and needed to be
estimated and incorporated in the plan. Second, the requirement to
obey traffic rules imposed specific behavior on Alice in
specific situations. This meant that the Alice's situation (context) 
needed to be estimated and Alice had to act accordingly. However, 
since there were other vehicless on the course, Alice needed to be
able to recover from situations where other vehicles did not behave as
expected and thus adjust its own behavior. Lastly, Alice needed
to be capable of planning in a very uncertain environment. Since the
environment was not known a priori, Alice had to determine its own
state, as well as the state of the world. Since this state cannot be
measured directly, it needed to be estimated. This estimation process
introduced uncertainty into the problem. Furthermore, 
the behavior of the dynamic obstacles is not known in advance, thus
there was some uncertainty associated with their predicted future
states. Another source of uncertainty is the fact that no model of a
vehicle is perfect, and thus there is some process noise (i.e., given
some action the outcome is not perfectly predictable).

The approach that Team Caltech followed in the planning was a three
layer planning process, illustrated in Figure~\ref{fig:planarch}.
\begin{figure}
 \centerline{\input{planarch.pst}}
  \caption{Three-layered planning approach: The mission-level planner
  takes the mission goal and specifies intermediate goals. These
  intermediate goals are passed to the tactical planner, which
  combined with the map information and the traffic rules, generates a
  trajectory. This trajectory is passed to the low-level planner,
  which converts the trajectory into actuator commands. The
  data-preprocessing step is also shown.}  
  \label{fig:planarch}
\end{figure}
At the highest level, the mission data is used to plan a route to the next
checkpoint, as specified in the Mission Data File (MDF). This route is
divided into 
intermediate goals, a subset of which is passed to the tactical level
planner. The tactical planner is responsible for taking this intermediate
goal and the map (which is Alice's representation of the world),
and designing a trajectory that satisfies all the constraints in the
problem. These constraints include traffic rules, vehicle dynamics and
constraints imposed by the world (obstacles, road, etc.). The
trajectory is then passed to a low-level trajectory tracker. This
feedback controller
converts the trajectory into actuator commands that control the
vehicle. There is also a data preprocessing step, which is responsible
for converting the map into a format accessible to the planner, and a
prediction step that estimates the future states of the mobile
agents. These different pieces of the planning problem are
described in this section.

A note on the coordinate system used is in order. For the planning
problem, there are two frames of interest. The first coordinate
system, called the world frame, is the geo-rectified frame (i.e., the
frame that GPS data is returned in). This coordinate system is
translated to waypoint 1.1.1 to make the coordinates more
manageable. This is the coordinate system in which the tactical
planning is conducted in since it is the coordinate frame used by
DARPA in the Route Network Definition File (RNDF). The second
coordinate frame of interest is the 
local frame. This frame is initialized to waypoint 1.1.1 as well, but
is allowed to drift to account for state jumps. This is the frame that
the sensors returned values in and is used in the low-level planning.
The local frame ensures that obstacle positions are properly maintain
relative to the vehicle, even if the GPS-reported state position jumps
due to GPS-outages or changing satellite coverage.

\subsection{Mission Level Planning}
\label{sec:mission-planning}

The mission-level planning has a number of functions. First, it is the
interface between the mission management and health management
systems.  Second, it maintains a history of where we have driven
before, which routes are temporarily blocked, etc. Third, it is
responsible for converting the mission files into a set of
intermediate goals and feeding these goals to the tactical planner as
the mission progresses. The first function is discussed in
Section~\ref{sec:systems} as part of the system-level mission and
contingency management. The latter two functions are discussed here.

\paragraph{Route Planner} The route planner is the module that is
responsible for finding a route given the RNDF and MDF. The RNDF is
parsed into a graph structure, called the travGraph. The planner uses
a Dijkstra algorithm to search this graph. Furthermore, the graph is
used to store information about previous traverses of roads, including
information about road blockages, etc. The route-planner is part of
the mission-planner module, which encompassed these functions, as well
as the interface with the mission and health management. The mission
planner is implemented in the Canonical Software Architecture
(CSA) and described in more detail in Section~\ref{sec:csa}.

\subsection{Trajectory Planning}
\label{sec:traj-planning}

The trajectory planning level is responsible for generating a
trajectory based on the intermediate goals from the route planner, the
map from the sensed data and the traffic rules. Before the planner can
be executed, a number of preprocessing steps are necessary. First, the
map data must to be converted into the appropriate data format used by
the planner. Second, the future states of the detected mobile objects
need to be determined. The planning approach followed
here is known as receding horizon control (RHC). In this approach, a
plan is obtained that stretches from the current location to the
goal. This plan is executed only for a short time and then revised at
the next planning cycle, using an updated planning horizon.

The first step in the trajectory planning algorithm is to set up the
planning problem. Traffic rules specify behaviors, and it is
necessary to enforce these behaviors on Alice. The behavior
currently required is determined via a finite state machine. This
behavior included intersection handling and is implemented in the
logic planner module. The behavior is enforced by setting up a
specific planning problem. For example, the problem might not allow
changing lanes in a region of the road where there is a double yellow
line lane separator. The idea is to be able to solve multiple
planning problems in parallel and then choose the best solution. This
would have been useful when the estimate of the current situation
cannot be obtained with sufficient certainty. This planning problem is
then passed to the appropriate path planner.

Two types of path planning problems needed to be solved: planning in
structured regions (such as roads, intersections, etc.) and
unstructured regions (such as obstacle fields and parking lots). In
our approach we used two distinct approaches for these problems, both
of which are based on the receding horizon control approach. 

For
planning in structured regions, a graph is constructed offline, based
on the RNDF. In this graph, the road geometry
is inferred. The motivation behind this planning scheme is that the
traffic rules imposed a lot of structure on the planning problem. This
is one attempt to leverage this structure optimally. A second
motivation is that, given that the graph defined the rail and lane
changes and turns, it is possible to verify that we could complete a
large portion of the course beforehand. The limitations of this
approach are the assumed geometry of the road and potential state
offsets. Given that we had aerial imagery of the test course, the
first limitation is not overly constraining. Also, the planner had a
mode that allowed it to switch to an ``off-road'' mode, where the
planner is not constrained to the precomputed graph, but would
navigated an obstacle field and try to reach a final pose. The second
limitation is more worrisome, and it was decided to add multiple
rails to each lane to allow the planner to choose the best rail, based
on the detected road markings. This planner was implemented as the
Rail Planner and is discussed some more below. 

For path planning in
unstructured regions, three parallel approaches were developed. The
first approach is based on a probabilistic roadmap approach where a
graph is constructed online. The approach followed is described below
in the clothoid-planner section. The second approach, called the
circle planner, constructed paths consisting of circular arcs and
straight line segments. This approach was not actually used during the
race. Both of these planners where spatial planners. The third
approach is an optimal receding horizon control planner. This is a
spatio-temporal planner (i.e., plans the trajectory directly). This
planner, called the dynamic planner, was not used during the race, but
is outlined in the sections below. 

In order to plan in a dynamic environment, we separated the planning
problem into a spatial planning problem and a spatio-temporal
planning problem. This greatly simplifies the planning problem. Also,
it is important to note that planning for dynamic obstacles vs. static
obstacles is fundamentally different. For example, when following a
car, one wants to plan where you want to drive and then adjust your
velocity to obtain a safe trajectory. Thus, the separation of planning
problems is justified. Also, in dealing with dynamic obstacles one
did not necessarily want to adjust your spatial path. There are some
cases, however, where adjustment of the spatial path is required. For
example, when passing an obstacle and there is a vehicle approaching
from the rear in the lane we want to change into, it is not sufficient
to only consider where that mobile object is currently, but we have to
account for the future states of this mobile object. Similarly, when
driving down a lane and there is a mobile object driving towards us,
it is not sufficient to only adjust the velocity profile. This is
accomplished by using the prediction information in two ways: first,
to define regions prohibited to planning, and second to do a dynamic
conflict analysis to determine possible collisions and avoid these
early on.


The finer details of the modules used in the planning stack are given next.

\paragraph{Planner}
The planner module functioned as the interface with the other
mission-level planner and the map. The planner is implemented in the
Canonical Software Architecture. It is responsible for maintaining a
queue of intermediate goals, maintaining histories of some pervasive
properties and sequencing the calls to the modules to solve the
planning problem. In the case where multiple planning problems are
set up, it would also maintain these different plans and select the
best one (though this was not implemented). The planner module is
also responsible for sending the trajectory that is obtained to the
low-level planner for execution.

Since the planner has to interface with the different libraries, it was
convenient to generate a module that maintained these
interfaces. These interfaces are discussed next, before focusing on
the functionality of the the individual library modules.

\paragraph{Planner Interfaces}
The interfaces between the Planner module and the libraries needed to
be maintained in a central location. These interfaces are maintained
in a module called the temp-planner-interfaces, with the exception of
the planners used for planning in unstructured regions. The reason for
this separation was that the unstructured region planners used some
objects that are slow to compile and this separation allowed a more efficient
decomposition of the software.

Some of the interfaces defined in the temp-planner-interfaces module
include the status data structure, which is used to report the status of the
different libraries used in the planning problem. Also, the graph
structure used for planning in the structured region is defined here,
together with the path. Since the trajectory is an interface between
the tactical- and low-level planners, this interface is defined
elsewhere. 

\paragraph{Logic Planner} The logic planner is responsible for
high-level decision making in Alice. It has two functions: (1) to
determine the current situation and come up with an appropriate
planning problem to solve and (2) to do internal fault handling. These
functions are not independent of each other, but we focus here on the
the first function and discuss fault handling in more detail in
Section~\ref{sec:systems}. 

The logic planner is implemented as two finite state machines. The
first state machine is responsible for determining the current
situation, by considering Alice's position in the world (e.g.,
proximity to intersections) and the status of the previous attempt at
trajectory planning (i.e., if the planner failed due to blockage by an
obstacle). These elements are factored in when setting up the planning
problem to be solved in the current cycle. As an example of how this
might work, consider a situation where Alice is on a two-lane, two-way
road with a yellow divider. The initial problem is to drive down the
lane to some goal location. This is given to the path planner to
solve, but an obstacle blocks the lane. The path planner returns a
status saying that it cannot solve the problem and avoid obstacles
(one of the constraints). In the next planning cycle, the logic
planner can adjust the plan to now allow passing, at which point the
planner will evaluate paths that move into the other lane.

The second state machine is used for intersection handling. Here we
must account for the current map, the road geometry and Alice's
position in the world to determine the correct intersection
behavior. This behavior is then encoded in a planning problem, which
is passed to the rest of the planning stack.  A detailed desription of
the intersection handling logic is available in~\cite{Loo07-ms}.

A note on dealing with uncertainty is in order at this point. The
logic planner is susceptible to uncertainty in the current situation,
as well as potentially uncertainty in the map. To overcome this
hurdle, we had hoped to implement a probabilistic finite state
machine. However, for this case it is conceivable that of the state
transitions defined out of some state, none of these transitions are
valid with high enough confidence. In this case, one approach would be
to set up planning problems for the relevant transitions, solve the
problems and evaluate the solutions. Unfortunately, this was never
implemented due to lack of time.

\paragraph{Rail Planner} The rail planner's main function is to search
over the precomputed graph to find the optimal path to the goal. Since
this graph is defined in the world frame, the planner has to plan in
the frame.  The first step is to preprocess the map data. This data
must be converted to either fields associated with the nodes of the
graphs or weights associated with edges. Thus, the precomputed graph
node locations are calculated and fixed offline, but the graph is
updated online to reflect the latest sensing information. Once this
step is completed, the optimal path to the goal can be calculated. To
accomplish this, the planner uses an A* algorithm to search the
graph. The cost function used in the optimization penalizes curvature,
which is useful to avoid sharp maneuvers at high speed.  Furthermore,
the cost function tends to keep the vehicle in the center of the
perceived lane.  In addition, the obstacles are included in the cost
generate plans that stay further away from obstacles when possible. In
this way the uncertainty associated with the sensed objects is
accounted for.

Figure~\ref{fig:railplanner} shows the different graphs created by the
Rail Planner.
\begin{figure}
  \centering
  \subfloat[]{\includegraphics[width=0.45\textwidth]{railplanner1.eps}} \quad
  \subfloat[]{\includegraphics[width=0.45\textwidth]{railplanner2.eps}} \\
  \subfloat[]{\includegraphics[width=0.45\textwidth]{railplanner3.eps}} \quad
  \subfloat[]{\includegraphics[width=0.45\textwidth]{railplanner4.eps}}
  \caption{Operation of the rail planner.}
  \label{fig:railplanner}
\end{figure}
The RNDF is first used to infer the geometry of the road and a single
rail is placed down the (inferred) center of the lane (a). Turns
through intersections are also defined. This is called the road graph.
Since the road geometry is only approximately known, more rails are
added to each lane to make the set of solutions to be searched less
restrictive (b). Rail changes and lane changes are then defined on
what is now the planning graph (c).

It was found that in some cases the precomputed graph was too
constraining. This was because the rail change, lane changes and turns
where precomputed. However, it was quite possible to have to deal with
an obstacle between these predefined maneuvers. A function was
implemented in this case to locally generate maneuvers (paths), called
the vehicle-subgraph, that generated plans from the current location
and connected to the precomputed graph as quickly as possible. This is
shown in Figure~\ref{fig:railplanner}d.  This normally gives the
planner enough flexibility to navigate these cases. The planning
algorithm is also able to plan in reverse, when allowed, making the
planner very capable.

As mentioned before, one of the concerns with using this planner is
the inference of the road geometry from the RNDF. A mode of the
planner was implemented where a local graph is generated online. This
graph is much more elaborate that the vehicle subgraph discussed above
and this capability allowed the planner to handle cases where the road
did not line up with the expected geometry, including obstacle
fields. The difficult problem became to determine when is it
appropriate to switch into this mode. Unfortunately, this problem was
never addressed and the segments for using this mode was hard coded
base on manual inspection of the RNDF.

\paragraph{Clothoid Planner}
The clothoid planner is the main planner used for planning in
unstructured regions and is implemented in the s1planner
module.  This is a graph-search based planner, where the graph is
generated online. The graph is constructed using a family of clothoid
curves. Clothoid curves are curves with constant angular velocity and
are commonly used for road layout design. The graph is constructed by
expanding a tree of these families of curves. The tree is expanded
until a relatively uniform covering of the state space is obtained. At
this point, the graph is searched to find the optimal solution. A cost
map is queried at each node (pose) to guide the search. 
At each pose considered, an obstacle overlap check is performed to
ensure that the obstacles are avoided. Thus, obstacles are handled
both as soft constraints, to push solutions away from obstacles, and
as hard constraints. The output of this planner is a path in the same
format as the rail planner.

\paragraph{Velocity Planner}
The velocity planner accepts a spatial path and time parameterizes this
path to obtain a trajectory. The velocity planner takes into account
path features such as stop lines, obstacles on the path and 
obstacles close to the path. The planner considers the path,
which has all the information necessary for velocity planning encoded
in it, and specifies a desired velocity profile. For example, it
will bring Alice to a stop at a desired deceleration and at a
desired distance away from an obstacle. For obstacles on the side of
the path, it will slow Alice down when passing close to these
obstacles. Lastly, the velocity planner considers the curvature
of the path and adjusts the velocities along the path accordingly. The
velocity planner is compatible with the rail-, clothoid- and
circle-planners. The output of the planner is a trajectory.

\paragraph{Prediction}
Planning in an environment where the agents move at high speed
requires some form of prediction of the future states of the mobile
objects. Prediction of cars driving in urban environments is eased
by the structure imposed on the environment, but is complicated by
noisy sensory data and partial knowledge of the world state. The
world state (map) and the mobile object's position in this world are
necessary to determine behavior. Two approaches for prediction where
investigated: (1) prediction based on particle filters and (2) prediction
utilizing the structure in the environment and simple assumptions on
the velocities of the mobile agents. The former approach was dropped
since the data representation was not easily incorporated into the
current planning approach. The latter approach has the disadvantage of
not being of much use in unstructured regions. 

The prediction information is used in two ways: first, the data is
used to define restricted regions around mobile agents. This is
especially useful when planning in intersections (such as merging) or
planning lane changes. The second use is for dynamic conflict
analysis. Here, the predicted future states of the mobile objects are
compared to the planned trajectory of Alice. If a collision is
predicted, an obstacle is placed in the map that alters Alice's plan
and thus avoids a potential collision. Noisy measurements of the
mobile object's state can cause prediction to sometimes be very
conservative (when the velocity is off) or simply wrong (when the
obstacle position in the partially known road network is estimated
wrong).

\subsection{Low-level Control and Vehicle Interface}
\label{sec:low-level-planning}

The lower-level functions of the navigation system were accomplished
by a set of tightly linked modules that controlled the motion of the
vehicle along the desired path and broadcast the current state of the
vehicle to other modules.

\paragraph{Follower} The follower module receives a trajectory data
structure from planner and state information from astate. It sends
actuation commands to gcdrive. Follower uses decoupled longitudinal
and lateral PID controllers, to keep Alice on the trajectory.  The
lateral controller uses a nonlinear controller that accounts for
limits on the steering rate and angle, and modifies its gains based on
the speed of the vehicle~\cite{LSM08-acc}. 

\paragraph{Gcdrive} Gcdrive is the overall driving software for
Alice. It works by receiving directives from follower over the
network, checking the directives to determine if they can be executed
and, if so, sending the appropriate commands to the actuators. Gcdrive
also performs checking on the state of the actuators, resets the
actuators that fail, implements the estop functionality for Alice and
broadcasts the actuator state. Also included in the role of gcdrive
was the implemention of physical protections for the hardware to
prevent the vehicle from hurting itself. This includes three
functions: limiting the steering rate at low speeds, preventing
shifting from occurring while the vehicle is moving, transitioning to
the paused state in which the brakes are depressed and commands to any
actuator except steering are rejected. (Steering commands are still
accepted so that obstacle avoidance is still possible while being
paused) when any of the critical actuators such as steering and brake
fail.

\paragraph{Astate} The astate module was responsible for broadcasting
the vehicle position (position, orientation, rates) data.  This module
read data from the Applanix hardware and processed the data to account
for state jumps.  It then broadcast the world and local frame
coordinate for the vehicle.

\paragraph{Reactive Obstacle Avoidance}
To ensure safe operation, it was decided to implement a low-level
reactive obstacle avoidance (ROA) mechanism. This mechanism is the
reason why the low-level planner needed to plan in the local
frame. The ROA would evaluate LADAR data directly and when an object
is detected within some box around Alice (which is velocity
dependent), it would adjust the reference velocity of the trajectory
to bring Alice to a stop in front of this object. One of the key
issues that needed to be faced was making this mechanism sensitive
enough to prevent collisions, but not so sensitive that it reacts to
every false positive detection. Furthermore, the rest of the planner
stack needed to be told that ROA is active (otherwise Alice stops
for no apparent reason). Lastly, their needed to be a mechanism to
override the ROA, otherwise there are situations where Alice would
just be stuck indefinitely.
