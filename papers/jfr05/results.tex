% Master File: jfr06.tex

\section{Experimental Results}
\label{sec:results}
In the previous six sections we have described in detail Team
Caltech's approach to designing its unmanned ground vehicle, Alice.
Unfortunately, due to
the nature of the Grand Challenge, very few controlled experiments
were performed on the vehicle as a whole.  Although many subsystems
were individually tested to make sure they met certain specifications,
the measure of success for the end-to-end system was a more
qualitative one, focused on making sure that the vehicle could drive
autonomously in a wide variety of situations including open desert,
parking lots, rainy and wet conditions, dirt roads of varying degrees
of roughness, rolling hills, winding roads and mountain passes.  The
following sections describe the nature and results of the testing of
Alice leading up to the National Qualifying Event, for the National
Qualifying Event itself, and in the Grand Challenge Event.  Although
the data presented is somewhat qualitative, we believe it makes clear
Alice's capabilities (and weaknesses) as an autonomous vehicle.

\subsection{Desert Testing}

Team Caltech documented over 300 miles of fully autonomous desert
driving with Alice from June 2005 to the National Qualifying Event in
Fontana in September, all in the Mojave Desert.  
Figure~\ref{fig:desert_rddfs} shows some of the RDDFs used during testing.
\begin{figure}
  \centerline{
  \includegraphics[width=0.75\textwidth]{desert_rddfs.eps} }
  \caption{A compilation of several RDDFs used during testing within
    the Stoddard Wells area of the Mojave Desert.  The grid lines are
    10 km apart.  The RDDF leaving the top of the figure is the 2004
    race RDDF, and the line cutting across the top right corner is the
    boundary of the no-go zone given by DARPA.  These RDDFs covered a
    wide variety of desert terrains, including dirt roads, rocky
    hills, dry lake beds, bumpy trails, smooth pavement, and mountain
    passes.  During the majority of testing the RDDF speed limit over
    the entire RDDF was set to be unlimited, and the vehicle's speed
    was chosen automatically as it traveled.  
  }
  \label{fig:desert_rddfs}
\end{figure}


Approximately 33 of these miles were driven on the 2004 Grand
Challenge race route during the week of June 13th, 2005.  Alice
traversed these miles with a testing team of four people inside,
scrutinizing its performance and making software improvements and
corrections.  Over the course of three days, Alice suffered three flat
tires including a debilitating crash into a short rocky wall that blew
out the inside of its front left tire and split its rim into two
pieces.  This crash was determined to be caused primarily by a lack of
accurate altitude estimates when cresting large hills.  Along with
several related bug fixes, an improved capability to estimate
elevation was added to the state estimator.

The majority (approximately 169 miles) of autonomous operation for
Alice took place in the two weeks leading into the National Qualifying
Event.  This operation included a full traversal of Daggett Ridge at 4
m/s average speed, and significant operation in hilly and mountainous
terrain (see Figure~\ref{fig:dagget_ridge_costmap}).  
\begin{figure}
  \centerline{
  \includegraphics[width=0.75\textwidth]{dagget_ridge_costmap.eps} }
  \caption{A sample speed limit map taken in Daggett Ridge during
  testing on the 2004 Grand Challenge course.  For scale, the grid
  lines are 40m apart.  The light-colored areas along the sides of the
  corridor are obstacles (cliff-faces taller than the vehicle, or
  berms about 0.5m high), the actual road is the darker area in the
  center, and the confetti-like coloring of some parts of the road
  indicates bumpier sections.  Despite the narrowness of the corridor
  (around 3-4m wide) and the difficulty of the terrain, Alice (the
  rectangle near the top of the figure) was able to pick out a safe
  course (the line extending back down the figure and to the left) at
  an average speed of 4m/s.}  \label{fig:dagget_ridge_costmap}
\end{figure}
The top speed
attained over all autonomous operations was 35 mph.  The longest
uninterrupted autonomous run was approximately 25 miles.
\subsection{National Qualifying Event}

As one of 43 teams at the California Speedway in Fontana, Alice
successfully completed three of its four qualifying runs.  Several of
its runs were characterized by frequent stopping as a result of the
performance of the state estimator under conditions of intermittent
GPS.  Specifically, under degraded GPS conditions its state estimate
would drift considerably, partially due to miscalibration of IMU
angular biases (especially yaw) and partially due to lack of odometry
inputs to the state estimator.  However, zero-speed corrections were
applied to the Kalman filter when Alice was stopped, which served to
correct errors in its state estimate due to drift quite well.

During Alice's first NQE run, zero-speed corrections were not applied
in the state estimator.  Accordingly, drift accumulating in the state
estimator was not corrected adequately when Alice stopped.  Travel
through the man-made tunnel produced drift substantial enough for
Alice's estimate to be outside the RDDF, which at the time resulted in
a reverse action.  Alice performed a series of reverse actions in this
state, going outside the actual corridor, and resulting in DARPA pause
and the end of its first run.

Zero-speed corrections were added to the state estimator after Alice's
first run, enabling it to successfully complete all subsequent runs,
clearing all obstacles and 137 out of a total 150 gates.  Completion
times were slow for runs 2, 3 and 4 partially due to frequent stopping as a
result of state estimator corrections.  Figure~\ref{fig:alicefontana}
provides a summary of Alice's NQE run performances.
Figure~\ref{fig:nqe_costmap} shows a snapshot of Alice's map and
followed 
paths during the third NQE run.
\begin{figure}
  \centerline{
    \includegraphics[width=0.315\textwidth]{alice-nqe.eps}
    \quad\small
    \begin{tabular}[b]{|c|c|c|c|p{4cm}|}
    \hline
    Run & Gates & Obst & Time & Issues \\
    \hline
    1 & 21/50 & 0/4 & DNF & State estimate problems after tunnel \\
    \hline
    2 & 44/50 & 4/4 & 12:45 & \\
    \hline
    3 & 49/50 & 5/5 & 16:21 & Multiple PAUSEs due to short in E-Stop
    wiring \\
    \hline
    4 & 44/50 & 5/5 & 16:59 & Frequent stops due to state drift \\
    \hline
    \end{tabular}
  }
  \caption{Alice on the National Qualifying Event course (left).  Table of results from Alice's four runs at the NQE (right).}
  \label{fig:alicefontana}
\end{figure}

\begin{figure}
  \centerline{ \includegraphics[width=0.75\textwidth]{nqe_costmap.eps}
  } \caption{A sample speed limit map taken during the third NQE run.
  For scale, the grid-lines are 40 m apart.  The solidly-colored areas
  in the center are the intended corridor, the lighter areas above and
  below the central section are lines of hay bales (around 0.5m tall),
  and the small spots near those hay bales are additional obstacles
  laid out by DARPA for Alice to avoid (such as wooden planks and
  tires).  Alice (the rectangle to the left) was easily able to detect
  and avoid the obstacles (even though it likely could have traversed
  them easily)---the line extending from Alice to the right of the
  figure is the path that was followed.  (Direction of travel is from
  right to left.)  }
\label{fig:nqe_costmap}
\end{figure}


Alice was one of only eleven teams to complete at least three of four
NQE runs.  As a result of Alice's performance at the NQE, it was
preselected as one of ten teams to qualify for a position on the
starting line in Primm, Nevada.

\subsection{Grand Challenge Event}

When Alice left the starting line on October 8th, 2005, all of its
systems were functioning properly.  However, a series of failures
caused it to drive off course, topple a concrete barrier and 
disqualify itself from the race as a result.  Although as mentioned above, the
system we have described performed well over the course of hundreds of
miles of testing in the desert prior to the Grand Challenge, we believe
the pathological nature of this particular failure scenario
demonstrates a few of the more important weaknesses of the system and
exemplifies the need for further ongoing research.  We will begin by
providing a brief chronological timeline of the events of the race
leading up to Alice's failure, followed by an analysis of what
weaknesses contributed to the failure.

Alice's timeline of events in the Grand Challenge is as follows:
\begin{itemize}

\item Zero minutes into the race, Alice leaves the starting line with
all systems functioning normally.

\item Approximately four minutes into the race, two of its midrange
LADARs enter an error mode from which they cannot recover, despite
repeated attempts by the software to reset.  Alice continues driving
using its long and short-range sensors.

\item Approximately 30 minutes into the race, Alice passes under a set
of high-voltage power lines.  Signals from the power lines interfere
with its ability to receive GPS signals, and its state estimate begins
to rely heavily on data from its Inertial Measurement Unit (IMU).

\item Approximately 31 minutes into the race, Alice approaches a
section of the course lined by concrete barriers.  Because new GPS
measurements are far from its current state estimate, the state
estimator requests and is granted a stop from supervisory control to
correct approximately 3 meters of state drift.  This is done and the 
map is cleared to prevent blurred obstacles from remaining.

\item GPS measurements report large signal errors and the state
estimator consequently converges very slowly, mistakenly determining
that the state has converged after a few seconds.  With the state
estimate in an unconverged state, Alice proceeds forward.  
\begin{figure}
  \begin{tabular}{cc}
    \includegraphics[width=0.45\textwidth]{yawcrash.eps} &
    \includegraphics[width=0.5\hsize]{sc-gce-crash.eps} \\
    (a) & (b)
  \end{tabular}
  \caption{(a) Alice's estimated heading and yaw, both measured clockwise
  from north, in its final moments of the GCE.  Yaw is from the state
  estimator and heading is computed directly as the arctangent of the
  easting and northing speeds of the rear axle.  The times at which
  the state estimator requests a vehicle pause (A), the map is cleared
  and pause is released (B), Alice speeds up after clearing a region
  of no data (C) and impact (D) are shown.  Between A and B the
  direction of motion is noisy as expected as Alice is stopped.
  Between B and D the state estimate is converged around 180 degree
  yaw, which we know to be about 8 degrees off leading into the
  crash. (b) The supervisory controller mode (strategy) during the same
  period.} 
  \label{fig:gce_yaw}
\end{figure}

\item A considerable eastward drift of the state estimate results from a low 
confidence placed on the GPS measurements.  This causes the velocity vector and 
yaw angle to converge to values that are a few degrees away from their true 
values.  Based on the placement of the north-south aligned row of K-rails in 
the map by the short-range LADAR (see Figure~\ref{fig:gce_gui}(a)), Alice's 
actual average yaw for the 5 or so seconds leading into the crash---between 
times C and D on Figure~\ref{fig:gce_yaw}---appears to be about 8 degrees west 
of south ($-172$ degrees).  For the same period, our average estimated yaw was 
about $-174$ degrees and our average heading (from $\dot N$, $\dot E$)
was about $-178$ 
degrees.
Roughly, as Alice drives south-southwest, its state estimate says it is
driving due south, straight down the race corridor 
(Figure~\ref{fig:gce_gui}(a)).
\begin{figure}
  \hspace*{-2\tabcolsep}
  \begin{tabular}{ccc}
    \multicolumn{3}{c}{
      \includegraphics[width=1.0\textwidth]{alicecrashgui.eps}
    } \\
    \hspace*{1.2in} (a) \hspace*{0.5in} &
    \hspace*{1in} (b) \hspace*{1in} &
    \hspace*{0.7in} (c) \hspace*{1in} 
  \end{tabular}
  \caption{Alice's speed limit maps over the last few seconds of the
  race, with notation added.  (For scale, the grid lines are 40m
  apart.)  For all three diagrams, the center dark area is the road,
  the light vertically-oriented rectangular areas are the concrete
  barriers, the hollow rectangle is Alice (traveling downward on the
  page), and the light-colored line is its perceived yaw (direction of
  travel). The color-bar on the right indicates the speed limit that
  was assigned to a given cell, where brown is highest and blue is
  lowest.  The leftmost diagram indicates Alice's expected yaw and
  Alice's actual yaw during the last few seconds of the race.  The
  center diagram is Alice's speed limit map less than one second
  before it crashed, indicating the differing placement of the
  obstacles by the short and long-range sensors---indicated as two
  parallel lines of concrete barriers (light-colored rectangles). In
  fact, there was only a single line of them, and that line went
  directly north-south, not angled as Alice perceived.  The rightmost
  diagram is Alice's final estimate of its location: accurate, but
  thirty seconds too late.}  \label{fig:gce_gui}
\end{figure}

\item Alice's long-range sensors detect the concrete barriers and
place them improperly in the map due to the error in state estimate.
Alice's mid-range sensors are still in an error mode.  Alice picks up
speed and is now driving at 10--15 mph.

\item At 32 minutes, because it is not driving where it thinks it is,
Alice crashes into a concrete barrier.  Its short-range sensors detect
the barrier, but not until it is virtually on top of it
(Figure~\ref{fig:gce_gui}(b) and Figure~\ref{fig:crash}).
\begin{figure}
  \centerline{
    \includegraphics[width=0.8\textwidth]{alice-crash.eps}
  }
  \caption{Alice as it ``clears'' a concrete barrier during the GCE.}
  \label{fig:crash}
\end{figure}

\item Almost simultaneously, DARPA gives an E-Stop pause, the front
right wheel collides with the barrier, the power steering gearbox is
damaged, and the driving software detects this and executes its own
pause also, independent of that from DARPA.  The strong front bumper
prevents Alice from suffering any major damage as it drives over the
barrier.

\item Once Alice has come to a stop, the state-estimation software
once again attempts to re-converge to get a more accurate state
estimate---this time it corrects about 9.5 meters and converges close to the
correct location, outside the race corridor.  Alice is subsequently
issued an E-Stop DISABLE (Figure~\ref{fig:gce_gui}(c)).

\end{itemize}
Figure~\ref{fig:gce_yaw}(b) shows the SuperCon state during this final
segment, including a Slow Advance through some spurious obstacles, a brief
Lone Ranger push to clear them, the GPS reacquisition while stopped, and the
final Slow Advance only after Alice has picked up speed and is near to 
crashing.

It is clear that while Alice's ultimate demise was rooted in its
incorrect state estimates (due to poor GPS signals), other factors
also contributed to its failure, or could conceivably have prevented it.
These include the mid-range LADAR sensor failures, the lack of a system-level 
response to such failures, and the high speeds assigned to long range sensor 
data even in the face of state uncertainty.  Additionally, in race 
configuration the forward facing bumper LADAR sensor was only used to assist in 
detection of the boundaries of the roads for the road following module.  This 
could have helped in assigning appropriate speeds in the map for the row of K-rails.

